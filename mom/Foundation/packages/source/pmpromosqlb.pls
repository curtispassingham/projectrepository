
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY PM_PROMO_SQL IS

--------------------------------------------------------------------------------
-- This is a stub package that should be replaced once the price management
-- apis are developed.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

FUNCTION GET_PROMO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_promotion     IN     ORDHEAD.PROMOTION%TYPE,
                   O_promo_rec        OUT PM_PROMO_SQL.PROMO_REC)
RETURN BOOLEAN IS

L_program     VARCHAR2(50) := 'PM_PROMO_SQL.GET_PROMO';
--
L_promo_rec   PM_PROMO_SQL.PROMO_REC := null;

cursor C_PROMO_REC is
select rp.promo_id,
       rp.name,
       rpe.promo_event_id,
       rpe.description
  from rpm_promo rp,
       rpm_promo_event rpe
 where rpe.promo_event_id (+) = rp.promo_event_id
   and rp.promo_id = I_promotion;

BEGIN

   open C_PROMO_REC;
   fetch C_PROMO_REC into L_promo_rec;
   close C_PROMO_REC;

   if L_promo_rec.promo_id is not null then
      O_promo_rec := L_promo_rec;
   else
      O_promo_rec := null;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_PROMO;


--------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
--------------------------------------------------------------------------------


END PM_PROMO_SQL;
/
