create or replace PACKAGE BODY LIKE_ITEM_SQL AS
-------------------------------------------------------------------------------
FUNCTION SET_RPM_ITEM_ZONE_PRICE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION LIKE_ITEM(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_supplier_ind            IN OUT   VARCHAR2,
                   O_price_ind               IN OUT   VARCHAR2,
                   O_store_ind               IN OUT   VARCHAR2,
                   O_wh_ind                  IN OUT   VARCHAR2,
                   O_repl_ind                IN OUT   VARCHAR2,
                   O_uda_ind                 IN OUT   VARCHAR2,
                   O_seasons_ind             IN OUT   VARCHAR2,
                   O_ticket_ind              IN OUT   VARCHAR2,
                   O_req_doc_ind             IN OUT   VARCHAR2,
                   O_hts_ind                 IN OUT   VARCHAR2,
                   O_tax_code_ind            IN OUT   VARCHAR2,
                   O_children_ind            IN OUT   VARCHAR2,
                   O_internal_finisher_ind   IN OUT   VARCHAR2,
                   O_external_finisher_ind   IN OUT   VARCHAR2,
                   I_item                    IN       ITEM_MASTER.ITEM%TYPE)

   RETURN BOOLEAN IS

   L_program    VARCHAR2(50)  := 'LIKE_ITEM_SQL.LIKE_ITEM';
   L_dummy      VARCHAR2(1)   := 'N';
   L_exists     BOOLEAN;
   L_hts_flg    BOOLEAN;
   L_on_repl    BOOLEAN;
   ---
   cursor C_CHECK_ITEM_SUPPLIER is
      select 'Y'
        from item_supplier s
       where exists (select 'x'
                       from item_master m
                      where (m.item                = I_item
                             or m.item_parent      = I_item
                             or m.item_grandparent = I_item)
                        and s.item                 = m.item);
   ---
   cursor C_CHECK_ITEM_TICKET is
      select 'Y'
        from item_ticket t
       where exists (select 'x'
                       from item_master m
                      where (m.item                = I_item
                             or m.item_parent      = I_item
                             or m.item_grandparent = I_item)
                        and t.item                 = m.item);
   ---
   cursor C_CHECK_ITEM_SEASONS is
      select 'Y'
        from item_seasons s
       where exists (select 'x'
                       from item_master m
                      where (m.item                = I_item
                             or m.item_parent      = I_item
                             or m.item_grandparent = I_item)
                        and s.item                 = m.item);
   ---
   cursor C_CHECK_ITEM_LOC_ST is
      select 'Y'
        from item_loc
       where (item                = I_item
              or item_parent      = I_item
              or item_grandparent = I_item)
         and loc_type             = 'S';


   cursor C_CHECK_UDA_ITEM_FF is
      select 'Y'
        from uda_item_ff u
       where exists (select 'x'
                       from item_master m
                      where (m.item                = I_item
                             or m.item_parent      = I_item
                             or m.item_grandparent = I_item)
                        and u.item                 = m.item);
   ---
   cursor C_CHECK_UDA_ITEM_LOV is
      select 'Y'
        from uda_item_lov u
       where exists (select 'x'
                       from item_master m
                      where (m.item                = I_item
                             or m.item_parent      = I_item
                             or m.item_grandparent = I_item)
                        and u.item                 = m.item);
   ---
   cursor C_CHECK_UDA_ITEM_DATE is
      select 'Y'
        from uda_item_date u
       where exists (select 'x'
                       from item_master m
                      where (m.item = I_item
                         or m.item_parent = I_item
                         or m.item_grandparent = I_item)
                        and u.item = m.item);
   ---
   cursor C_CHECK_REQ_DOC is
      select 'Y'
        from req_doc
       where module = 'IT'
         and exists (select 'x'
                       from item_master m
                      where (m.item                = I_item
                             or m.item_parent      = I_item
                             or m.item_grandparent = I_item)
                        and key_value_1            = m.item);
   ---
   cursor C_CHECK_HTS is
      select 'Y'
        from item_hts h
       where exists (select 'x'
                       from item_master m
                      where (m.item                = I_item
                             or m.item_parent      = I_item
                             or m.item_grandparent = I_item)
                        and h.item                 = m.item);
   ---
   cursor C_CHECK_REPL is
      select 'Y'
        from repl_item_loc r
       where exists (select 'x'
                       from item_master m
                      where (m.item                = I_item
                             or m.item_parent      = I_item
                             or m.item_grandparent = I_item)
                        and r.item                 = m.item);
   ---
   cursor C_CHECK_FOR_CHILD is
      select 'Y'
        from item_master
       where item_parent       = I_item
         and item_number_type in ('ITEM','UPC-A','UPC-AS');

   ---
   cursor C_CHECK_ITEM_LOC_WH is
      select 'Y'
        from item_loc i,
             wh w
       where (i.item                = I_item
              or i.item_parent      = I_item
              or i.item_grandparent = I_item)
         and i.loc_type             = 'W'
         and i.loc                  = w.wh
         and w.finisher_ind         = 'N'
         and rownum                 = 1;
   ---
   cursor C_CHECK_ITEM_LOC_INT_FIN is
      select 'Y'
        from item_loc i,
             wh w
       where (i.item                = I_item
              or i.item_parent      = I_item
              or i.item_grandparent = I_item)
         and i.loc_type             = 'W'
         and i.loc                  = w.wh
         and w.finisher_ind         ='Y'
         and rownum = 1;
   ---
   cursor C_CHECK_ITEM_LOC_EXT_FIN is
      select 'Y'
        from item_loc
       where (item                = I_item
              or item_parent      = I_item
              or item_grandparent = I_item)
         and loc_type             = 'E'
         and rownum = 1;
   ---

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_supplier_ind := 'N';
   O_ticket_ind   := 'N';
   O_tax_code_ind := 'N';
   O_seasons_ind  := 'N';
   O_store_ind    := 'N';
   O_wh_ind       := 'N';
   O_uda_ind      := 'N';
   O_price_ind    := 'N';
   O_req_doc_ind  := 'N';
   O_hts_ind      := 'N';
   O_repl_ind     := 'N';
   O_children_ind := 'N';

   O_internal_finisher_ind := 'N';
   O_external_finisher_ind := 'N';

   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_SUPPLIER',
                    'ITEM_SUPPLIER',
                    'ITEM: '||I_item);
   open C_CHECK_ITEM_SUPPLIER;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ITEM_SUPPLIER',
                    'ITEM_SUPPLIER',
                    'ITEM: '||I_item);
   fetch C_CHECK_ITEM_SUPPLIER into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ITEM_SUPPLIER',
                    'ITEM_SUPPLIER',
                    'ITEM: '||I_item);
   close C_CHECK_ITEM_SUPPLIER;
   ---
   if L_dummy = 'Y' then
      O_supplier_ind := 'Y';
      L_dummy        := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_TICKET',
                    'ITEM_TICKET',
                    'ITEM: '||I_item);
   open C_CHECK_ITEM_TICKET;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ITEM_TICKET',
                    'ITEM_TICKET',
                    'ITEM: '||I_item);
   fetch C_CHECK_ITEM_TICKET into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ITEM_TICKET',
                    'ITEM_TICKET',
                    'ITEM: '||I_item);
   close C_CHECK_ITEM_TICKET;
   ---
   if L_dummy = 'Y' then
      O_ticket_ind := 'Y';
      L_dummy      := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_SEASONS',
                    'ITEM_SEASONS',
                    'ITEM: '||I_item);
   open C_CHECK_ITEM_SEASONS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ITEM_SEASONS',
                    'ITEM_SEASONS',
                    'ITEM: '||I_item);
   fetch C_CHECK_ITEM_SEASONS into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ITEM_SEASONS',
                    'ITEM_SEASONS',
                    'ITEM: '||I_item);
   close C_CHECK_ITEM_SEASONS;
   ---
   if L_dummy = 'Y' then
      O_seasons_ind := 'Y';
      L_dummy       := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_LOC_ST',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   open C_CHECK_ITEM_LOC_ST;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ITEM_LOC_ST',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   fetch C_CHECK_ITEM_LOC_ST into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ITEM_LOC_ST',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   close C_CHECK_ITEM_LOC_ST;
   ---
   if L_dummy = 'Y' then
      O_store_ind := 'Y';
      L_dummy     := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_LOC_WH',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   open C_CHECK_ITEM_LOC_WH;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ITEM_LOC_WH',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   fetch C_CHECK_ITEM_LOC_WH into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ITEM_LOC_WH',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   close C_CHECK_ITEM_LOC_WH;
   ---
   if L_dummy = 'Y' then
      O_wh_ind := 'Y';
      L_dummy  := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_UDA_ITEM_FF',
                    'UDA_ITEM_FF',
                    'ITEM: '||I_item);
   open C_CHECK_UDA_ITEM_FF;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_UDA_ITEM_FF',
                    'UDA_ITEM_FF',
                    'ITEM: '||I_item);
   fetch C_CHECK_UDA_ITEM_FF into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_UDA_ITEM_FF',
                    'UDA_ITEM_FF',
                    'ITEM: '||I_item);
   close C_CHECK_UDA_ITEM_FF;
   ---
   if L_dummy = 'N' then  -- C_CHECK_UDA_ITEM_FF%NOTFOUND then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_UDA_ITEM_LOV',
                       'UDA_ITEM_LOV',
                       'ITEM: '||I_item);
      open C_CHECK_UDA_ITEM_LOV;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_UDA_ITEM_LOV',
                       'UDA_ITEM_LOV',
                       'ITEM: '||I_item);
      fetch C_CHECK_UDA_ITEM_LOV into L_dummy;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_UDA_ITEM_LOV',
                       'UDA_ITEM_LOV',
                       'ITEM: '||I_item);
      close C_CHECK_UDA_ITEM_LOV;
      ---
      if L_dummy = 'N' then  --C_CHECK_UDA_ITEM_LOV%NOTFOUND then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_UDA_ITEM_DATE',
                          'UDA_ITEM_DATE',
                          'ITEM: '||I_item);
         open C_CHECK_UDA_ITEM_DATE;
         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_UDA_ITEM_DATE',
                          'UDA_ITEM_DATE',
                          'ITEM: '||I_item);
         fetch C_CHECK_UDA_ITEM_DATE into L_dummy;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_UDA_ITEM_DATE',
                          'UDA_ITEM_DATE',
                          'ITEM: '||I_item);
         close C_CHECK_UDA_ITEM_DATE;
         ---
         if L_dummy = 'Y' then  --C_CHECK_UDA_ITEM_DATE%NOTFOUND then
            O_uda_ind := 'Y';
            L_dummy   := 'N';
         end if;
      else
         O_uda_ind := 'Y';
         L_dummy   := 'N';
      end if;
   else
      O_uda_ind := 'Y';
      L_dummy   := 'N';
   end if;
   ---
   if PM_RETAIL_API_SQL.CHECK_RETAIL_EXISTS(O_error_message,
                                            O_price_ind,
                                            I_item) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REQ_DOC',
                    'REQ_DOC',
                    'ITEM: '||I_item);
   open C_CHECK_REQ_DOC;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REQ_DOC',
                    'REQ_DOC',
                    'ITEM: '||I_item);
   fetch C_CHECK_REQ_DOC into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REQ_DOC',
                    'REQ_DOC',
                    'ITEM: '||I_item);
   close C_CHECK_REQ_DOC;
   ---
   if L_dummy = 'Y' then
      O_req_doc_ind := 'Y';
      L_dummy       := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_HTS',
                    'ITEM_HTS',
                    'ITEM: '||I_item);
   open C_CHECK_HTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_HTS',
                    'ITEM_HTS',
                    'ITEM: '||I_item);
   fetch C_CHECK_HTS into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_HTS',
                    'ITEM_HTS',
                    'ITEM: '||I_item);
   close C_CHECK_HTS;
   ---
   if L_dummy = 'Y' then
      O_hts_ind := 'Y';
      L_dummy   := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REPL',
                    'REPL_ITEM_LOC',
                    'ITEM: '||I_item);
   open C_CHECK_REPL;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REPL',
                    'REPL_ITEM_LOC',
                    'ITEM: '||I_item);
   fetch C_CHECK_REPL into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REPL',
                    'REPL_ITEM_LOC',
                    'ITEM: '||I_item);
   close C_CHECK_REPL;
   ---
   if L_dummy = 'Y' then
      O_repl_ind := 'Y';
      L_dummy    := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_FOR_CHILD',
                    'ITEM_MASTER',
                    'ITEM: '||I_item);
   open C_CHECK_FOR_CHILD;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_FOR_CHILD',
                    'ITEM_MASTER',
                    'ITEM: '||I_item);
   fetch C_CHECK_FOR_CHILD into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_FOR_CHILD',
                    'ITEM_MASTER',
                    'ITEM: '||I_item);
   close C_CHECK_FOR_CHILD;


   ---
   if L_dummy = 'Y' then
      O_children_ind := 'Y';
      L_dummy        := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_LOC_INT_FIN',
                    'ITEM_LOC, WH',
                    'ITEM: '||I_item);
   open C_CHECK_ITEM_LOC_INT_FIN;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ITEM_LOC_INT_FIN',
                    'ITEM_LOC, WH',
                    'ITEM: '||I_item);
   fetch C_CHECK_ITEM_LOC_INT_FIN into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ITEM_LOC_INT_FIN',
                    'ITEM_LOC, WH',
                    'ITEM: '||I_item);
   close C_CHECK_ITEM_LOC_INT_FIN;
   ---
   if L_dummy = 'Y' then
      O_internal_finisher_ind := 'Y';
      L_dummy        := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_LOC_EXT_FIN',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   open C_CHECK_ITEM_LOC_EXT_FIN;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_ITEM_LOC_EXT_FIN',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   fetch C_CHECK_ITEM_LOC_EXT_FIN into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_ITEM_LOC_EXT_FIN',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   close C_CHECK_ITEM_LOC_EXT_FIN;
   ---
   if L_dummy = 'Y' then
      O_external_finisher_ind := 'Y';
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END LIKE_ITEM;
-------------------------------------------------------------------------------
FUNCTION LIKE_ITEM_INSERT(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_new_item            IN       ITEM_MASTER.ITEM%TYPE,
                          I_existing_item       IN       ITEM_MASTER.ITEM%TYPE,
                          I_item_desc           IN       ITEM_MASTER.ITEM_DESC%TYPE,
                          I_number_type         IN       ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                          I_format_id           IN       ITEM_MASTER.FORMAT_ID%TYPE,
                          I_prefix              IN       ITEM_MASTER.PREFIX%TYPE,
                          I_diff_1              IN       ITEM_MASTER.DIFF_1%TYPE,
                          I_diff_2              IN       ITEM_MASTER.DIFF_2%TYPE,
                          I_diff_3              IN       ITEM_MASTER.DIFF_3%TYPE,
                          I_diff_4              IN       ITEM_MASTER.DIFF_4%TYPE,
                          I_children_ind        IN       VARCHAR2,
                          I_supplier_ind        IN       VARCHAR2,
                          I_price_ind           IN       VARCHAR2,
                          I_store_ind           IN       VARCHAR2,
                          I_wh_ind              IN       VARCHAR2,
                          I_repl_ind            IN       VARCHAR2,
                          I_uda_ind             IN       VARCHAR2,
                          I_seasons_ind         IN       VARCHAR2,
                          I_ticket_ind          IN       VARCHAR2,
                          I_req_doc_ind         IN       VARCHAR2,
                          I_hts_ind             IN       VARCHAR2,
                          I_internal_finisher   IN       VARCHAR2,
                          I_external_finisher   IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)    := 'LIKE_ITEM_SQL.LIKE_ITEM_INSERT';
   L_item          ITEM_MASTER.ITEM%TYPE;
   L_new_item      ITEM_MASTER.ITEM%TYPE;
   L_item_parent   ITEM_MASTER.ITEM%TYPE;
   L_item_num_type ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE;
   L_item_level    ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_item_desc     ITEM_MASTER.ITEM_DESC%TYPE;
   L_diff_1        ITEM_MASTER.DIFF_1%TYPE;
   L_diff_2        ITEM_MASTER.DIFF_2%TYPE;
   L_diff_3        ITEM_MASTER.DIFF_3%TYPE;
   L_diff_4        ITEM_MASTER.DIFF_4%TYPE;
   L_table         VARCHAR2(20);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_GET_ITEM_INFO is
      select item_level,
             item_parent,
             diff_1,
             diff_2,
             diff_3,
             diff_4
        from item_master
       where item = I_existing_item;
   ---
   cursor C_GET_CHILD_ITEMS is
      select item,
             item_parent,
             item_number_type,
             item_level,
             item_desc,
             diff_1,
             diff_2,
             diff_3,
             diff_4
        from item_master
       where (item_parent         = I_existing_item
              or item_grandparent = I_existing_item)
         and item_number_type in ('ITEM','UPC-A','UPC-AS')
       order by item_level,
                item;
   ---
   cursor C_LOCK_ITEM_TEMP is
      select 'x'
        from item_temp
         for update nowait;

BEGIN
   if I_new_item is NULL or I_existing_item is NULL
      or I_item_desc is NULL or I_number_type is NULL then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_ITEM_INFO','ITEM_MASTER','Item: '||I_existing_item);
   open C_GET_ITEM_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_ITEM_INFO','ITEM_MASTER','Item: '||I_existing_item);
   fetch C_GET_ITEM_INFO into L_item_level,
                              L_item_parent,
                              L_diff_1,
                              L_diff_2,
                              L_diff_3,
                              L_diff_4;

   SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_INFO','ITEM_MASTER','Item: '||I_existing_item);
   close C_GET_ITEM_INFO;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_TEMP',NULL);
   insert into item_temp(item,
                         item_number_type,
                         format_id,
                         prefix,
                         item_level,
                         item_desc,
                         diff_1,
                         diff_2,
                         diff_3,
                         diff_4,
                         like_existing_item,
                         existing_item_parent)
                  values(I_new_item,
                         I_number_type,
                         I_format_id,
                         I_prefix,
                         L_item_level,
                         I_item_desc,
                         NVL(I_diff_1, L_diff_1),
                         NVL(I_diff_2, L_diff_2),
                         NVL(I_diff_3, L_diff_3),
                         NVL(I_diff_4, L_diff_4),
                         I_existing_item,
                         L_item_parent);
   ---
   if I_children_ind = 'Y'  and I_supplier_ind = 'Y' then
      for rec in C_GET_CHILD_ITEMS loop
         L_item          := rec.item;
         L_item_parent   := rec.item_parent;
         L_item_num_type := rec.item_number_type;
         L_item_level    := rec.item_level;
         L_item_desc     := rec.item_desc;
         L_diff_1        := rec.diff_1;
         L_diff_2        := rec.diff_2;
         L_diff_3        := rec.diff_3;
         L_diff_4        := rec.diff_4;
         ---
         if ITEM_NUMBER_TYPE_SQL.GET_NEXT(O_error_message,
                                          L_new_item,
                                          L_item_num_type) = FALSE then
            return FALSE;
         end if;
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_TEMP',NULL);
         insert into item_temp(item,
                               item_number_type,
                               item_level,
                               item_desc,
                               diff_1,
                               diff_2,
                               diff_3,
                               diff_4,
                               like_existing_item,
                               existing_item_parent)
                        values(L_new_item,
                               L_item_num_type,
                               L_item_level,
                               L_item_desc,
                               NVL(I_diff_1, L_diff_1),
                               NVL(I_diff_2, L_diff_2),
                               NVL(I_diff_3, L_diff_3),
                               NVL(I_diff_4, L_diff_4),
                               L_item,
                               L_item_parent);
      end loop;
   end if;
   ---
   if LIKE_ITEM_SQL.ITEM_MASTER_DETAIL_INSERT(O_error_message,
                                              I_new_item,
                                              I_existing_item,
                                              I_item_desc,
                                              I_number_type,
                                              I_children_ind,
                                              I_supplier_ind,
                                              I_price_ind,
                                              I_store_ind,
                                              I_wh_ind,
                                              I_repl_ind,
                                              I_uda_ind,
                                              I_seasons_ind,
                                              I_ticket_ind,
                                              I_req_doc_ind,
                                              I_hts_ind,
                                              I_internal_finisher,
                                              I_external_finisher) = FALSE then
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_TEMP';
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_TEMP','ITEM_TEMP',NULL);
   open C_LOCK_ITEM_TEMP;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_TEMP','ITEM_TEMP',NULL);
   close C_LOCK_ITEM_TEMP;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_TEMP',NULL);
   delete from item_temp;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            to_char(SQLCODE));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END LIKE_ITEM_INSERT;
-------------------------------------------------------------------------------
FUNCTION ITEM_MASTER_DETAIL_INSERT(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_new_item            IN       ITEM_MASTER.ITEM%TYPE,
                                   I_existing_item       IN       ITEM_MASTER.ITEM%TYPE,
                                   I_item_desc           IN       ITEM_MASTER.ITEM_DESC%TYPE,
                                   I_number_type         IN       ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                                   I_children_ind        IN       VARCHAR2,
                                   I_supplier_ind        IN       VARCHAR2,
                                   I_price_ind           IN       VARCHAR2,
                                   I_store_ind           IN       VARCHAR2,
                                   I_wh_ind              IN       VARCHAR2,
                                   I_repl_ind            IN       VARCHAR2,
                                   I_uda_ind             IN       VARCHAR2,
                                   I_seasons_ind         IN       VARCHAR2,
                                   I_ticket_ind          IN       VARCHAR2,
                                   I_req_doc_ind         IN       VARCHAR2,
                                   I_hts_ind             IN       VARCHAR2,
                                   I_internal_finisher   IN       VARCHAR2,
                                   I_external_finisher   IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program                      VARCHAR2(255)  := 'LIKE_ITEM_SQL.ITEM_MASTER_DETAIL_INSERT';
   L_user                         VARCHAR2(30)  := GET_USER;
   L_sysdate                      PERIOD.VDATE%TYPE  := SYSDATE;
   L_item_desc                    ITEM_MASTER.ITEM_DESC%TYPE;
   L_diff_1                       ITEM_MASTER.DIFF_1%TYPE;
   L_diff_2                       ITEM_MASTER.DIFF_2%TYPE;
   L_diff_3                       ITEM_MASTER.DIFF_3%TYPE;
   L_diff_4                       ITEM_MASTER.DIFF_4%TYPE;
   L_new_item_desc                ITEM_MASTER.ITEM_DESC%TYPE;
   L_import_ind                   SYSTEM_OPTIONS.IMPORT_IND%TYPE;
   L_elc_ind                      SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_std_av_ind                   SYSTEM_OPTIONS.STD_AV_IND%TYPE;
   L_default_tax_type             SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;
   L_wh                           ITEM_LOC.LOC%TYPE;
   L_store                        ITEM_LOC.LOC%TYPE;
   L_tran_level                   ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_item_level                   ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_item                         ITEM_MASTER.ITEM%TYPE;
   L_like_item                    ITEM_MASTER.ITEM%TYPE;
   L_new_item                     ITEM_MASTER.ITEM%TYPE;
   L_existing_item                ITEM_MASTER.ITEM%TYPE;
   L_diff_1_desc                  V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff1_type                   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_id_group_ind1                V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_diff_2_desc                  V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff2_type                   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_id_group_ind2                V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_diff_3_desc                  V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff3_type                   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_id_group_ind3                V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_diff_4_desc                  V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff4_type                   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_id_group_ind4                V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_vdate                        PERIOD.VDATE%TYPE := GET_VDATE;
   L_dept                         ITEM_MASTER.DEPT%TYPE;
   L_class                        ITEM_MASTER.CLASS%TYPE;
   L_subclass                     ITEM_MASTER.SUBCLASS%TYPE;
   L_short_desc                   ITEM_MASTER.SHORT_DESC%TYPE;
   L_cost_zone_group_id           ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE;
   L_standard_uom                 ITEM_MASTER.STANDARD_UOM%TYPE;
   L_uom_conv_factor              ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_store_ord_mult               ITEM_MASTER.STORE_ORD_MULT%TYPE;
   L_supplier                     ITEM_SUPPLIER.SUPPLIER%TYPE;
   L_vpn                          ITEM_SUPPLIER.VPN%TYPE;
   L_supp_diff_1                  ITEM_SUPPLIER.SUPP_DIFF_1%TYPE;
   L_supp_diff_2                  ITEM_SUPPLIER.SUPP_DIFF_2%TYPE;
   L_supp_diff_3                  ITEM_SUPPLIER.SUPP_DIFF_3%TYPE;
   L_supp_diff_4                  ITEM_SUPPLIER.SUPP_DIFF_4%TYPE;
   L_origin_country_id            ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_manu_country_id              ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE;
   L_lead_time                    ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_unit_cost                    ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_supp_pack_size               ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_inner_pack_size              ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
   L_min_order_qty                ITEM_SUPP_COUNTRY.MIN_ORDER_QTY%TYPE;
   L_max_order_qty                ITEM_SUPP_COUNTRY.MAX_ORDER_QTY%TYPE;
   L_packing_method               ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE;
   L_default_uop                  ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE;
   L_ti                           ITEM_SUPP_COUNTRY.TI%TYPE;
   L_hi                           ITEM_SUPP_COUNTRY.HI%TYPE;
   L_unit_length                  ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_unit_width                   ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_unit_height                  ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_unit_lwh_uom                 ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_net_unit_weight              ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_gross_unit_weight            ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_unit_weight_uom              ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_unit_liquid_vol              ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_unit_liquid_uom              ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_case_length                  ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_case_width                   ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_case_height                  ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_case_lwh_uom                 ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_net_case_weight              ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_gross_case_weight            ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_case_weight_uom              ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_case_liquid_vol              ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_case_liquid_uom              ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_pallet_length                ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_pallet_width                 ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_pallet_height                ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_pallet_lwh_uom               ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_net_pallet_weight            ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_gross_pallet_weight          ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_pallet_weight_uom            ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_unit_dim                     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE;
   L_case_dim                     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE;
   L_pallet_dim                   ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE;
   L_item_parent                  ITEM_MASTER.ITEM%TYPE;
   L_bracket_level                SUPS.INV_MGMT_LVL%TYPE;
   L_no_locs                      VARCHAR2(1)                             := 'N';
   L_bracket_exists               VARCHAR2(1)                             := 'N';
   L_default_bracket_ind          VARCHAR2(1)                             := 'N';
   L_like_selling_uom             ITEM_LOC.SELLING_UOM%TYPE;
   L_like_daily_waste_pct         ITEM_LOC.DAILY_WASTE_PCT%TYPE;
   L_like_taxable_ind             ITEM_LOC.TAXABLE_IND%TYPE;
   L_like_meas_of_each            ITEM_LOC.MEAS_OF_EACH%TYPE;
   L_like_store_ord_mult          ITEM_LOC.STORE_ORD_MULT%TYPE;
   L_like_meas_of_price           ITEM_LOC.MEAS_OF_PRICE%TYPE;
   L_like_uom_of_price            ITEM_LOC.UOM_OF_PRICE%TYPE;
   L_like_receive_as_type         ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_prev_loc                     ITEM_LOC.LOC%TYPE;
   L_ph_wh                        WH.PHYSICAL_WH%TYPE;
   L_cost_loop_ind                VARCHAR(1)                              := 'N';
   L_sup_dept_seq_no              ITEM_SUPP_COUNTRY_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE;
   L_reqd_no_value                VARCHAR(1)                              := 'Y';
   L_dummy                        VARCHAR2(6);
   L_tax_info_tbl                 OBJ_TAX_INFO_TBL                        := OBJ_TAX_INFO_TBL();
   L_tax_info_tbl_temp            OBJ_TAX_INFO_TBL                        := OBJ_TAX_INFO_TBL();
   L_tax_info_rec                 OBJ_TAX_INFO_REC                        := OBJ_TAX_INFO_REC();
   L_run_report                   BOOLEAN;

   L_update                VARCHAR2(1)    := NULL;
   L_min_loc               ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_rpm_ind      VARCHAR2(1):=NULL;
   cursor C_RPM_IND  is 
      select RPM_IND 
	    from system_options;	
   cursor C_GET_SYS_OPTS is
      select import_ind,
             elc_ind,
             std_av_ind,
             default_tax_type
        from system_options;
   ---
   cursor C_GET_TEMP_ITEMS is
      select item,
             item_level,
             item_number_type,
             format_id,
             prefix,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             like_existing_item,
             existing_item_parent
        from item_temp
       order by item_level;
   ---
   cursor C_GET_STORES is
      select il.loc,
             il.loc_type,
             il.selling_uom,
             il.taxable_ind,
             il.daily_waste_pct,
             il.meas_of_each,
             il.meas_of_price,
             il.uom_of_price,
             il.receive_as_type,
             il.store_ord_mult
        from item_loc il,
             item_temp it
       where il.item = it.like_existing_item
         and il.loc_type = 'S';
   ---
   cursor C_GET_WH is
      select il.loc,
             il.loc_type,
             il.selling_uom,
             il.taxable_ind,
             il.daily_waste_pct,
             il.meas_of_each,
             il.meas_of_price,
             il.uom_of_price,
             il.receive_as_type,
             il.store_ord_mult
        from item_loc il,
             item_temp it
       where il.item = it.like_existing_item
         and il.loc_type = 'W';
   ---
 cursor C_GET_GTAX_VALID_INFO is
      select OBJ_TAX_INFO_REC(gi.item,
                              gi.item_parent,
                              gi.item_grandparent,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              gi.from_entity,
                              gi.from_entity_type,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              gi.tax_amount,
                              gi.tax_rate,
                              NULL,
                              NULL,
                              gi.active_date,
                              gi.inventory_ind,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              gi.currency)
        from TABLE(CAST(L_tax_info_tbl AS OBJ_TAX_INFO_TBL)) gi
       where gi.from_entity_type = decode(I_store_ind,'Y','ST','WH');

   cursor C_COPY_RELATED_ITEM is
   select it.item,
          it.like_existing_item
     from item_temp it, related_item_head rih
    where it.like_existing_item = rih.item
 group by it.item,
          it.like_existing_item;

   cursor C_GET_MIN_ISC_LOC is
      select MIN(isl.loc)
        from item_supp_country_loc isl
       where isl.item = I_new_item;

   cursor C_UPDATE_PRIM_LOC_IND is
      select 'x'
        from item_supp_country_loc isl
       where isl.loc               = L_min_loc
         and isl.item              = I_new_item
         and not exists (select 'x'
                           from item_supp_country_loc isl3
                          where isl3.loc              != L_min_loc
                            and isl3.item              = isl.item
                            and isl3.primary_loc_ind   = 'Y'
                            and rownum = 1)
          for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_SYS_OPTS','SYSTEM_OPTIONS',NULL);
   open C_GET_SYS_OPTS;
   SQL_LIB.SET_MARK('FETCH','C_GET_SYS_OPTS','SYSTEM_OPTIONS',NULL);
   fetch C_GET_SYS_OPTS into L_import_ind,
                             L_elc_ind,
                             L_std_av_ind,
                             L_default_tax_type;
   SQL_LIB.SET_MARK('CLOSE','C_GET_SYS_OPTS','SYSTEM_OPTIONS',NULL);
   close C_GET_SYS_OPTS;
   ---

   if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                 L_item_level,
                                 L_tran_level,
                                 I_existing_item) = FALSE then
      return FALSE;
   end if;
   ---
   for rec in C_GET_TEMP_ITEMS loop
      L_new_item      := rec.item;
      L_diff_1        := rec.diff_1;
      L_diff_2        := rec.diff_2;
      L_diff_3        := rec.diff_3;
      L_diff_4        := rec.diff_4;
      L_existing_item := rec.like_existing_item;
      ---
      if L_diff_1 is not NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff_1_desc,
                                   L_diff1_type,
                                   L_id_group_ind1,
                                   L_diff_1) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_diff_2 is not NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff_2_desc,
                                   L_diff2_type,
                                   L_id_group_ind2,
                                   L_diff_2) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_diff_3 is not NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff_3_desc,
                                   L_diff3_type,
                                   L_id_group_ind3,
                                   L_diff_3) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_diff_4 is not NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff_4_desc,
                                   L_diff4_type,
                                   L_id_group_ind4,
                                   L_diff_4) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if NVL(L_id_group_ind1,'xx') =  'ID'  then
         L_new_item_desc := RTRIM(SUBSTRB(I_item_desc,1,126))||':'||RTRIM(SUBSTRB(L_diff_1_desc,1,30));
         ---
         if NVL(L_id_group_ind2,'xx') = 'ID' then
            L_new_item_desc := RTRIM(SUBSTRB(L_new_item_desc,1,157))||':'||RTRIM(SUBSTRB(L_diff_2_desc,1,30));
         end if;
         ---
         if NVL(L_id_group_ind3,'xx') = 'ID' then
            L_new_item_desc := RTRIM(SUBSTRB(L_new_item_desc,1,188))||':'||RTRIM(SUBSTRB(L_diff_3_desc,1,30));
         end if;
         ---
         if NVL(L_id_group_ind4,'xx') = 'ID' then
            L_new_item_desc := RTRIM(SUBSTRB(L_new_item_desc,1,219))||':'||RTRIM(SUBSTRB(L_diff_4_desc,1,30));
         end if;
      else
         L_new_item_desc := I_item_desc;
      end if;
      ---

      if L_item_level = 1 then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_MASTER',NULL);
         insert into item_master(item,
                                 item_number_type,
                                 format_id,
                                 prefix,
                                 item_parent,
                                 item_grandparent,
                                 pack_ind,
                                 item_level,
                                 tran_level,
                                 item_aggregate_ind,
                                 diff_1,
                                 diff_1_aggregate_ind,
                                 diff_2,
                                 diff_2_aggregate_ind,
                                 diff_3,
                                 diff_3_aggregate_ind,
                                 diff_4,
                                 diff_4_aggregate_ind,
                                 dept,
                                 class,
                                 subclass,
                                 status,
                                 item_desc,
                                 short_desc,
                                 desc_up,
                                 primary_ref_item_ind,
                                 cost_zone_group_id,
                                 standard_uom,
                                 uom_conv_factor,
                                 package_size,
                                 package_uom,
                                 merchandise_ind,
                                 store_ord_mult,
                                 forecast_ind,
                                 original_retail,
                                 mfg_rec_retail,
                                 retail_label_type,
                                 retail_label_value,
                                 handling_temp,
                                 handling_sensitivity,
                                 catch_weight_ind,
                                 waste_type,
                                 waste_pct,
                                 default_waste_pct,
                                 const_dimen_ind,
                                 simple_pack_ind,
                                 contains_inner_ind,
                                 sellable_ind,
                                 orderable_ind,
                                 pack_type,
                                 order_as_type,
                                 comments,
                                 gift_wrap_ind,
                                 ship_alone_ind,
                                 check_uda_ind,
                                 create_datetime,
                                 last_update_id,
                                 last_update_datetime,
                                 item_xform_ind,
                                 inventory_ind,
                                 deposit_item_type,
                                 item_desc_secondary,
                                 item_service_level,
                                 order_type,
                                 sale_type,
                                 container_item,
                                 deposit_in_price_per_uom,
                                 aip_case_type,
                                 perishable_ind,
                                 notional_pack_ind,
                                 soh_inquiry_at_pack_ind,
                                 catch_weight_uom,
                                 product_classification,
                                 brand_name,
								 curr_selling_unit_retail,
								 curr_selling_uom
                                 )
                          select distinct L_new_item,
                                 rec.item_number_type,
                                 rec.format_id,
                                 rec.prefix,
                                 DECODE(rec.item_level,2,I_new_item,3,it2.item,NULL),
                                 DECODE(rec.item_level,3,I_new_item,NULL),
                                 im.pack_ind,
                                 im.item_level,
                                 im.tran_level,
                                 im.item_aggregate_ind,
                                 L_diff_1,
                                 im.diff_1_aggregate_ind,
                                 L_diff_2,
                                 im.diff_2_aggregate_ind,
                                 L_diff_3,
                                 im.diff_3_aggregate_ind,
                                 L_diff_4,
                                 im.diff_4_aggregate_ind,
                                 im.dept,
                                 im.class,
                                 im.subclass,
                                 'W',
                                 DECODE(im.item_level,1,I_item_desc,
                                                   2,L_new_item_desc,
                                                   3,L_new_item_desc),
                                 DECODE(im.item_level,1,RTRIM(SUBSTRB(I_item_desc,1,120)),
                                                   2,RTRIM(SUBSTRB(L_new_item_desc,1,120)),
                                                   3,RTRIM(SUBSTRB(L_new_item_desc,1,120))),
                                 UPPER(DECODE(im.item_level,1,I_item_desc,
                                                   2,L_new_item_desc,
                                                   3,L_new_item_desc)),
                                 im.primary_ref_item_ind,
                                 im.cost_zone_group_id,
                                 im.standard_uom,
                                 im.uom_conv_factor,
                                 im.package_size,
                                 im.package_uom,
                                 im.merchandise_ind,
                                 im.store_ord_mult,
                                 im.forecast_ind,
                                 NULL, --im.original_retail,
                                 im.mfg_rec_retail,
                                 im.retail_label_type,
                                 im.retail_label_value,
                                 im.handling_temp,
                                 im.handling_sensitivity,
                                 im.catch_weight_ind,
                                 im.waste_type,
                                 im.waste_pct,
                                 im.default_waste_pct,
                                 im.const_dimen_ind,
                                 im.simple_pack_ind,
                                 im.contains_inner_ind,
                                 im.sellable_ind,
                                 im.orderable_ind,
                                 im.pack_type,
                                 im.order_as_type,
                                 im.comments,
                                 im.gift_wrap_ind,
                                 im.ship_alone_ind,
                                 I_uda_ind,
                                 sysdate,
                                 L_user,
                                 sysdate,
                                 im.item_xform_ind,
                                 im.inventory_ind,
                                 im.deposit_item_type,
                                 NULL,
                                 im.item_service_level,
                                 im.order_type,
                                 im.sale_type,
                                 im.container_item,
                                 im.deposit_in_price_per_uom,
                                 im.aip_case_type,
                                 im.perishable_ind,
                                 im.notional_pack_ind,
                                 im.soh_inquiry_at_pack_ind,
                                 im.catch_weight_uom,
                                 im.product_classification,
                                 im.brand_name,
						         im.curr_selling_unit_retail,
								 im.curr_selling_uom
                            from item_master im,
                                 item_temp it1,
                                 item_temp it2
                           where im.item = it1.like_existing_item
                             and it1.item = L_new_item
                             and (it2.like_existing_item = it1.existing_item_parent
                              or it1.existing_item_parent is NULL);
      elsif L_item_level = 2 then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_MASTER',NULL);
         insert into item_master(item,
                                 item_number_type,
                                 format_id,
                                 prefix,
                                 item_parent,
                                 item_grandparent,
                                 pack_ind,
                                 item_level,
                                 tran_level,
                                 item_aggregate_ind,
                                 diff_1,
                                 diff_1_aggregate_ind,
                                 diff_2,
                                 diff_2_aggregate_ind,
                                 diff_3,
                                 diff_3_aggregate_ind,
                                 diff_4,
                                 diff_4_aggregate_ind,
                                 dept,
                                 class,
                                 subclass,
                                 status,
                                 item_desc,
                                 short_desc,
                                 desc_up,
                                 primary_ref_item_ind,
                                 cost_zone_group_id,
                                 standard_uom,
                                 uom_conv_factor,
                                 package_size,
                                 package_uom,
                                 merchandise_ind,
                                 store_ord_mult,
                                 forecast_ind,
                                 original_retail,
                                 mfg_rec_retail,
                                 retail_label_type,
                                 retail_label_value,
                                 handling_temp,
                                 handling_sensitivity,
                                 catch_weight_ind,
                                 waste_type,
                                 waste_pct,
                                 default_waste_pct,
                                 const_dimen_ind,
                                 simple_pack_ind,
                                 contains_inner_ind,
                                 sellable_ind,
                                 orderable_ind,
                                 pack_type,
                                 order_as_type,
                                 comments,
                                 gift_wrap_ind,
                                 ship_alone_ind,
                                 check_uda_ind,
                                 create_datetime,
                                 last_update_id,
                                 last_update_datetime,
                                 item_xform_ind,
                                 inventory_ind,
                                 deposit_item_type,
                                 item_desc_secondary,
                                 item_service_level,
                                 order_type,
                                 sale_type,
                                 container_item,
                                 deposit_in_price_per_uom,
                                 aip_case_type,
                                 perishable_ind,
                                 notional_pack_ind,
                                 soh_inquiry_at_pack_ind,
                                 catch_weight_uom,
                                 product_classification,
                                 brand_name,
								 curr_selling_unit_retail,
								 curr_selling_uom
                                 )
                          select L_new_item,
                                 rec.item_number_type,
                                 rec.format_id,
                                 rec.prefix,
                                 DECODE(rec.item_level,2,rec.existing_item_parent,3,I_new_item,NULL),
                                 DECODE(rec.item_level,3,im.item_grandparent,NULL),
                                 im.pack_ind,
                                 rec.item_level,
                                 im.tran_level,
                                 im.item_aggregate_ind,
                                 L_diff_1,
                                 im.diff_1_aggregate_ind,
                                 L_diff_2,
                                 im.diff_2_aggregate_ind,
                                 L_diff_3,
                                 im.diff_3_aggregate_ind,
                                 L_diff_4,
                                 im.diff_4_aggregate_ind,
                                 im.dept,
                                 im.class,
                                 im.subclass,
                                 'W',
                                 L_new_item_desc,
                                 RTRIM(SUBSTRB(L_new_item_desc,1,120)),
                                 UPPER(L_new_item_desc),
                                 DECODE(im.tran_level, 2, im.primary_ref_item_ind, 'N'),
                                 im.cost_zone_group_id,
                                 im.standard_uom,
                                 im.uom_conv_factor,
                                 im.package_size,
                                 im.package_uom,
                                 im.merchandise_ind,
                                 im.store_ord_mult,
                                 im.forecast_ind,
                                 NULL, --im.original_retail,
                                 im.mfg_rec_retail,
                                 im.retail_label_type,
                                 im.retail_label_value,
                                 im.handling_temp,
                                 im.handling_sensitivity,
                                 im.catch_weight_ind,
                                 im.waste_type,
                                 im.waste_pct,
                                 im.default_waste_pct,
                                 im.const_dimen_ind,
                                 im.simple_pack_ind,
                                 im.contains_inner_ind,
                                 im.sellable_ind,
                                 im.orderable_ind,
                                 im.pack_type,
                                 im.order_as_type,
                                 im.comments,
                                 im.gift_wrap_ind,
                                 im.ship_alone_ind,
                                 I_uda_ind,
                                 sysdate,
                                 L_user,
                                 sysdate,
                                 im.item_xform_ind,
                                 im.inventory_ind,
                                 im.deposit_item_type,
                                 NULL,
                                 im.item_service_level,
                                 im.order_type,
                                 im.sale_type,
                                 im.container_item,
                                 im.deposit_in_price_per_uom,
                                 im.aip_case_type,
                                 im.perishable_ind,
                                 im.notional_pack_ind,
                                 im.soh_inquiry_at_pack_ind,
                                 im.catch_weight_uom,
                                 im.product_classification,
                                 im.brand_name,
								 im.curr_selling_unit_retail,
								 im.curr_selling_uom
                            from item_master im
                           where im.item = L_existing_item;

      elsif L_item_level = 3 then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_MASTER',NULL);
         insert into item_master(item,
                                 item_number_type,
                                 format_id,
                                 prefix,
                                 item_parent,
                                 item_grandparent,
                                 pack_ind,
                                 item_level,
                                 tran_level,
                                 item_aggregate_ind,
                                 diff_1,
                                 diff_1_aggregate_ind,
                                 diff_2,
                                 diff_2_aggregate_ind,
                                 diff_3,
                                 diff_3_aggregate_ind,
                                 diff_4,
                                 diff_4_aggregate_ind,
                                 dept,
                                 class,
                                 subclass,
                                 status,
                                 item_desc,
                                 short_desc,
                                 desc_up,
                                 primary_ref_item_ind,
                                 cost_zone_group_id,
                                 standard_uom,
                                 uom_conv_factor,
                                 package_size,
                                 package_uom,
                                 merchandise_ind,
                                 store_ord_mult,
                                 forecast_ind,
                                 original_retail,
                                 mfg_rec_retail,
                                 retail_label_type,
                                 retail_label_value,
                                 handling_temp,
                                 handling_sensitivity,
                                 catch_weight_ind,
                                 waste_type,
                                 waste_pct,
                                 default_waste_pct,
                                 const_dimen_ind,
                                 simple_pack_ind,
                                 contains_inner_ind,
                                 sellable_ind,
                                 orderable_ind,
                                 pack_type,
                                 order_as_type,
                                 comments,
                                 gift_wrap_ind,
                                 ship_alone_ind,
                                 check_uda_ind,
                                 create_datetime,
                                 last_update_id,
                                 last_update_datetime,
                                 item_xform_ind,
                                 inventory_ind,
                                 deposit_item_type,
                                 item_desc_secondary,
                                 item_service_level,
                                 order_type,
                                 sale_type,
                                 container_item,
                                 deposit_in_price_per_uom,
                                 aip_case_type,
                                 perishable_ind,
                                 notional_pack_ind,
                                 soh_inquiry_at_pack_ind,
                                 catch_weight_uom,
                                 product_classification,
                                 brand_name,
								 curr_selling_unit_retail,
								 curr_selling_uom
                                 )
                          select L_new_item,
                                 rec.item_number_type,
                                 rec.format_id,
                                 rec.prefix,
                                 im.item_parent,
                                 im.item_grandparent,
                                 im.pack_ind,
                                 rec.item_level,
                                 im.tran_level,
                                 im.item_aggregate_ind,
                                 L_diff_1,
                                 im.diff_1_aggregate_ind,
                                 L_diff_2,
                                 im.diff_2_aggregate_ind,
                                 L_diff_3,
                                 im.diff_3_aggregate_ind,
                                 L_diff_4,
                                 im.diff_4_aggregate_ind,
                                 im.dept,
                                 im.class,
                                 im.subclass,
                                 'W',
                                 L_new_item_desc,
                                 RTRIM(SUBSTRB(L_new_item_desc,1,120)),
                                 UPPER(L_new_item_desc),
                                 'N',
                                 im.cost_zone_group_id,
                                 im.standard_uom,
                                 im.uom_conv_factor,
                                 im.package_size,
                                 im.package_uom,
                                 im.merchandise_ind,
                                 im.store_ord_mult,
                                 im.forecast_ind,
                                 NULL, --im.original_retail,
                                 im.mfg_rec_retail,
                                 im.retail_label_type,
                                 im.retail_label_value,
                                 im.handling_temp,
                                 im.handling_sensitivity,
                                 im.catch_weight_ind,
                                 im.waste_type,
                                 im.waste_pct,
                                 im.default_waste_pct,
                                 im.const_dimen_ind,
                                 im.simple_pack_ind,
                                 im.contains_inner_ind,
                                 im.sellable_ind,
                                 im.orderable_ind,
                                 im.pack_type,
                                 im.order_as_type,
                                 im.comments,
                                 im.gift_wrap_ind,
                                 im.ship_alone_ind,
                                 I_uda_ind,
                                 sysdate,
                                 L_user,
                                 sysdate,
                                 im.item_xform_ind,
                                 im.inventory_ind,
                                 im.deposit_item_type,
                                 NULL,
                                 im.item_service_level,
                                 im.order_type,
                                 im.sale_type,
                                 im.container_item,
                                 im.deposit_in_price_per_uom,
                                 im.aip_case_type,
                                 im.perishable_ind,
                                 im.notional_pack_ind,
                                 im.soh_inquiry_at_pack_ind,
                                 im.catch_weight_uom,
                                 im.product_classification,
                                 im.brand_name,
								 im.curr_selling_unit_retail,
								 im.curr_selling_uom
                            from item_master im
                           where im.item = L_existing_item;

      end if;
   end loop;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_MASTER_CFA_EXT',NULL);
   insert into item_master_cfa_ext(item,
                                   group_id,
                                   varchar2_1,
                                   varchar2_2,
                                   varchar2_3,
                                   varchar2_4,
                                   varchar2_5,
                                   varchar2_6,
                                   varchar2_7,
                                   varchar2_8,
                                   varchar2_9,
                                   varchar2_10,
                                   number_11,
                                   number_12,
                                   number_13,
                                   number_14,
                                   number_15,
                                   number_16,
                                   number_17,
                                   number_18,
                                   number_19,
                                   number_20,
                                   date_21,
                                   date_22,
                                   date_23,
                                   date_24,
                                   date_25)
                            select it.item,
                                   imce.group_id,
                                   imce.varchar2_1,
                                   imce.varchar2_2,
                                   imce.varchar2_3,
                                   imce.varchar2_4,
                                   imce.varchar2_5,
                                   imce.varchar2_6,
                                   imce.varchar2_7,
                                   imce.varchar2_8,
                                   imce.varchar2_9,
                                   imce.varchar2_10,
                                   imce.number_11,
                                   imce.number_12,
                                   imce.number_13,
                                   imce.number_14,
                                   imce.number_15,
                                   imce.number_16,
                                   imce.number_17,
                                   imce.number_18,
                                   imce.number_19,
                                   imce.number_20,
                                   imce.date_21,
                                   imce.date_22,
                                   imce.date_23,
                                   imce.date_24,
                                   imce.date_25
                              from item_master_cfa_ext imce,
                                   item_temp it
                             where imce.item = it.like_existing_item;
   ---
   if L_import_ind = 'Y' then
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_IMPORT_ATTR',NULL);
      insert into item_import_attr(item,
                                   tooling,
                                   first_order_ind,
                                   amortize_base,
                                   open_balance,
                                   commodity,
                                   import_desc)
                            select it.item,
                                   i.tooling,
                                   i.first_order_ind,
                                   i.amortize_base,
                                   i.open_balance,
                                   i.commodity,
                                   i.import_desc
                              from item_import_attr i,
                                   item_temp it
                             where i.item = it.like_existing_item;
   end if;
   ---

   if I_supplier_ind = 'Y' then
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPPLIER',NULL);
      insert into item_supplier(item,
                                supplier,
                                primary_supp_ind,
                                vpn,
                                supp_label,
                                consignment_rate,
                                supp_diff_1,
                                supp_diff_2,
                                supp_diff_3,
                                supp_diff_4,
                                pallet_name,
                                case_name,
                                inner_name,
                                supp_discontinue_date,
                                direct_ship_ind,
                                last_update_datetime,
                                last_update_id,
                                create_datetime,
                                concession_rate,
                                primary_case_size)
                         select it.item,
                                s.supplier,
                                s.primary_supp_ind,
                                s.vpn,
                                s.supp_label,
                                s.consignment_rate,
                                supp_diff_1,
                                supp_diff_2,
                                supp_diff_3,
                                supp_diff_4,
                                s.pallet_name,
                                s.case_name,
                                s.inner_name,
                                s.supp_discontinue_date,
                                s.direct_ship_ind,
                                sysdate,
                                L_user,
                                sysdate,
                                s.concession_rate,
                                s.primary_case_size
                           from item_supplier s,
                                item_temp it
                          where s.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPPLIER_CFA_EXT',NULL);
      insert into item_supplier_cfa_ext(item,
                                        supplier,
                                        group_id,
                                        varchar2_1,
                                        varchar2_2,
                                        varchar2_3,
                                        varchar2_4,
                                        varchar2_5,
                                        varchar2_6,
                                        varchar2_7,
                                        varchar2_8,
                                        varchar2_9,
                                        varchar2_10,
                                        number_11,
                                        number_12,
                                        number_13,
                                        number_14,
                                        number_15,
                                        number_16,
                                        number_17,
                                        number_18,
                                        number_19,
                                        number_20,
                                        date_21,
                                        date_22,
                                        date_23,
                                        date_24,
                                        date_25)
                                 select it.item,
                                        isce.supplier,
                                        isce.group_id,
                                        isce.varchar2_1,
                                        isce.varchar2_2,
                                        isce.varchar2_3,
                                        isce.varchar2_4,
                                        isce.varchar2_5,
                                        isce.varchar2_6,
                                        isce.varchar2_7,
                                        isce.varchar2_8,
                                        isce.varchar2_9,
                                        isce.varchar2_10,
                                        isce.number_11,
                                        isce.number_12,
                                        isce.number_13,
                                        isce.number_14,
                                        isce.number_15,
                                        isce.number_16,
                                        isce.number_17,
                                        isce.number_18,
                                        isce.number_19,
                                        isce.number_20,
                                        isce.date_21,
                                        isce.date_22,
                                        isce.date_23,
                                        isce.date_24,
                                        isce.date_25
                                   from item_temp it,
                                        item_supplier_cfa_ext isce
                                  where isce.item=it.like_existing_item;
   ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY',NULL);
      insert into item_supp_country(item,
                                    supplier,
                                    origin_country_id,
                                    unit_cost,
                                    lead_time,
                                    supp_pack_size,
                                    inner_pack_size,
                                    round_lvl,
                                    round_to_inner_pct,
                                    round_to_case_pct,
                                    round_to_layer_pct,
                                    round_to_pallet_pct,
                                    min_order_qty,
                                    max_order_qty,
                                    packing_method,
                                    primary_supp_ind,
                                    primary_country_ind,
                                    default_uop,
                                    ti,
                                    hi,
                                    supp_hier_type_1,
                                    supp_hier_lvl_1,
                                    supp_hier_type_2,
                                    supp_hier_lvl_2,
                                    supp_hier_type_3,
                                    supp_hier_lvl_3,
                                    pickup_lead_time,
                                    last_update_datetime,
                                    last_update_id,
                                    create_datetime,
                                    cost_uom,
                                    negotiated_item_cost,
                                    extended_base_cost,
                                    inclusive_cost,
                                    base_cost)
                             select it.item,
                                    s.supplier,
                                    s.origin_country_id,
                                    s.unit_cost,
                                    s.lead_time,
                                    s.supp_pack_size,
                                    s.inner_pack_size,
                                    s.round_lvl,
                                    s.round_to_inner_pct,
                                    s.round_to_case_pct,
                                    s.round_to_layer_pct,
                                    s.round_to_pallet_pct,
                                    s.min_order_qty,
                                    s.max_order_qty,
                                    s.packing_method,
                                    s.primary_supp_ind,
                                    s.primary_country_ind,
                                    s.default_uop,
                                    s.ti,
                                    s.hi,
                                    s.supp_hier_type_1,
                                    s.supp_hier_lvl_1,
                                    s.supp_hier_type_2,
                                    s.supp_hier_lvl_2,
                                    s.supp_hier_type_3,
                                    s.supp_hier_lvl_3,
                                    s.pickup_lead_time,
                                    sysdate,
                                    L_user,
                                    sysdate,
                                    s.cost_uom,
                                    s.negotiated_item_cost,
                                    s.extended_base_cost,
                                    s.inclusive_cost,
                                    s.base_cost
                               from item_supp_country s,
                                    item_temp it
                              where s.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_CFA_EXT',NULL);
      insert into item_supp_country_cfa_ext(item,
                                                supplier,
                                                origin_country_id,
                                                group_id,
                                                varchar2_1,
                                                varchar2_2,
                                                varchar2_3,
                                                varchar2_4,
                                                varchar2_5,
                                                varchar2_6,
                                                varchar2_7,
                                                varchar2_8,
                                                varchar2_9,
                                                varchar2_10,
                                                number_11,
                                                number_12,
                                                number_13,
                                                number_14,
                                                number_15,
                                                number_16,
                                                number_17,
                                                number_18,
                                                number_19,
                                                number_20,
                                                date_21,
                                                date_22,
                                                date_23,
                                                date_24,
                                                date_25)
                                         select it.item,
                                                iscce.supplier,
                                                iscce.origin_country_id,
                                                iscce.group_id,
                                                iscce.varchar2_1,
                                                iscce.varchar2_2,
                                                iscce.varchar2_3,
                                                iscce.varchar2_4,
                                                iscce.varchar2_5,
                                                iscce.varchar2_6,
                                                iscce.varchar2_7,
                                                iscce.varchar2_8,
                                                iscce.varchar2_9,
                                                iscce.varchar2_10,
                                                iscce.number_11,
                                                iscce.number_12,
                                                iscce.number_13,
                                                iscce.number_14,
                                                iscce.number_15,
                                                iscce.number_16,
                                                iscce.number_17,
                                                iscce.number_18,
                                                iscce.number_19,
                                                iscce.number_20,
                                                iscce.date_21,
                                                iscce.date_22,
                                                iscce.date_23,
                                                iscce.date_24,
                                                iscce.date_25
                                           from item_supp_country_cfa_ext iscce,
                                                item_temp it
                                          where it.like_existing_item = iscce.item;
   ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COST_HEAD',NULL);
      insert into item_cost_head(item,
                                 supplier,
                                 origin_country_id,
                                 delivery_country_id,
                                 prim_dlvy_ctry_ind,
                                 nic_static_ind,
                                 base_cost,
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 inclusive_cost)
                          select it.item,
                                 ich.supplier,
                                 ich.origin_country_id,
                                 ich.delivery_country_id,
                                 ich.prim_dlvy_ctry_ind,
                                 ich.nic_static_ind,
                                 ich.base_cost,
                                 ich.negotiated_item_cost,
                                 ich.extended_base_cost,
                                 ich.inclusive_cost
                            from item_cost_head ich,
                                 item_temp it
                           where ich.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COST_DETAIL',NULL);
      insert into item_cost_detail(item,
                                   supplier,
                                   origin_country_id,
                                   delivery_country_id,
                                   cond_type,
                                   cond_value,
                                   applied_on,
                                   modified_taxable_base,
                                   comp_rate,
                                   calculation_basis,
                                   recoverable_amount
                                   )
                            select it.item,
                                   icd.supplier,
                                   icd.origin_country_id,
                                   icd.delivery_country_id,
                                   icd.cond_type,
                                   icd.cond_value,
                                   icd.applied_on,
                                   icd.modified_taxable_base,
                                   icd.comp_rate,
                                   icd.calculation_basis,
                                   icd.recoverable_amount
                              from item_cost_detail icd,
                                   item_temp it
                             where icd.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_MANU_COUNTRY',NULL);
      insert into item_supp_manu_country(item,
                                         supplier,
                                         manu_country_id,
                                         primary_manu_ctry_ind)
                                  select it.item,
                                         s.supplier,
                                         s.manu_country_id,
                                         s.primary_manu_ctry_ind
                                    from item_supp_manu_country s,
                                         item_temp it
                                   where s.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_UOM',NULL);
      insert into item_supp_uom(item,
                                supplier,
                                uom,
                                value,
                                last_update_datetime,
                                last_update_id,
                                create_datetime)
                         select it.item,
                                s.supplier,
                                s.uom,
                                s.value,
                                sysdate,
                                L_user,
                                sysdate
                           from item_supp_uom s,
                                item_temp it
                          where s.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_BRACKET_COST',NULL);
      insert into item_supp_country_bracket_cost(item,
                                                 supplier,
                                                 origin_country_id,
                                                 location,
                                                 bracket_value1,
                                                 loc_type,
                                                 default_bracket_ind,
                                                 unit_cost,
                                                 bracket_value2,
                                                 sup_dept_seq_no)
                                          select it.item,
                                                 bc.supplier,
                                                 bc.origin_country_id,
                                                 bc.location,
                                                 bc.bracket_value1,
                                                 bc.loc_type,
                                                 bc.default_bracket_ind,
                                                 bc.unit_cost,
                                                 bc.bracket_value2,
                                                 bc.sup_dept_seq_no
                                            from item_supp_country_bracket_cost bc,
                                                 item_temp it
                                           where bc.location is NULL
                                             and it.like_existing_item = bc.item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM',NULL);
      insert into item_supp_country_dim(item,
                                        supplier,
                                        origin_country,
                                        dim_object,
                                        presentation_method,
                                        length,
                                        width,
                                        height,
                                        lwh_uom,
                                        weight,
                                        net_weight,
                                        weight_uom,
                                        liquid_volume,
                                        liquid_volume_uom,
                                        stat_cube,
                                        tare_weight,
                                        tare_type,
                                        last_update_datetime,
                                        last_update_id,
                                        create_datetime)
                                 select it.item,
                                        s.supplier,
                                        s.origin_country,
                                        s.dim_object,
                                        s.presentation_method,
                                        s.length,
                                        s.width,
                                        s.height,
                                        s.lwh_uom,
                                        s.weight,
                                        s.net_weight,
                                        s.weight_uom,
                                        s.liquid_volume,
                                        s.liquid_volume_uom,
                                        s.stat_cube,
                                        s.tare_weight,
                                        s.tare_type,
                                        sysdate,
                                        L_user,
                                        sysdate
                                   from item_supp_country_dim s,
                                        item_temp it
                                  where s.item = it.like_existing_item;      
      ---
      if L_elc_ind = 'Y' then
            SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_EXP_HEAD',NULL);
            insert into item_exp_head(item,
                                      supplier,
                                      item_exp_type,
                                      item_exp_seq,
                                      origin_country_id,
                                      zone_id,
                                      lading_port,
                                      discharge_port,
                                      zone_group_id,
                                      base_exp_ind,
                                      last_update_datetime,
                                      last_update_id,
                                      create_datetime)
                               select it.item,
                                      ie.supplier,
                                      ie.item_exp_type,
                                      ie.item_exp_seq,
                                      ie.origin_country_id,
                                      ie.zone_id,
                                      ie.lading_port,
                                      ie.discharge_port,
                                      ie.zone_group_id,
                                      ie.base_exp_ind,
                                      sysdate,
                                      L_user,
                                      sysdate
                                 from item_exp_head ie,
                                      item_temp it
                                where ie.item = it.like_existing_item;
            ---
            SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_EXP_DETAIL',NULL);
            insert into item_exp_detail(item,
                                        supplier,
                                        item_exp_type,
                                        item_exp_seq,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        comp_currency,
                                        per_count,
                                        per_count_uom,
                                        est_exp_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order,
                                        last_update_datetime,
                                        last_update_id,
                                        create_datetime,
                                        defaulted_from,
                                        key_value_1,
                                        key_value_2)
                                 select it.item,
                                        ie.supplier,
                                        ie.item_exp_type,
                                        ie.item_exp_seq,
                                        ie.comp_id,
                                        ie.cvb_code,
                                        ie.comp_rate,
                                        ie.comp_currency,
                                        ie.per_count,
                                        ie.per_count_uom,
                                        ie.est_exp_value,
                                        ie.nom_flag_1,
                                        ie.nom_flag_2,
                                        ie.nom_flag_3,
                                        ie.nom_flag_4,
                                        ie.nom_flag_5,
                                        ie.display_order,
                                        sysdate,
                                        L_user,
                                        sysdate,
                                        ie.defaulted_from,
                                        ie.key_value_1,
                                        ie.key_value_2
                                   from item_exp_detail ie,
                                        item_temp it
                                  where ie.item = it.like_existing_item;
      end if;

      if L_case_dim is not null then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM',NULL);
         insert into item_supp_country_dim(item,
                                        supplier,
                                        origin_country,
                                        dim_object,
                                        presentation_method,
                                        length,
                                        width,
                                        height,
                                        lwh_uom,
                                        weight,
                                        net_weight,
                                        weight_uom,
                                        liquid_volume,
                                        liquid_volume_uom,
                                        stat_cube,
                                        tare_weight,
                                        tare_type,
                                        last_update_datetime,
                                        last_update_id,
                                        create_datetime)
                                 select it.item,
                                        s.supplier,
                                        s.origin_country,
                                        s.dim_object,
                                        s.presentation_method,
                                        L_case_length,
                                        L_case_width,
                                        L_case_height,
                                        L_case_lwh_uom,
                                        L_gross_case_weight,
                                        L_net_case_weight,
                                        L_case_weight_uom,
                                        L_case_liquid_vol,
                                        L_case_liquid_uom,
                                        s.stat_cube,
                                        s.tare_weight,
                                        s.tare_type,
                                        sysdate,
                                        L_user,
                                        sysdate
                                   from item_supp_country_dim s,
                                        item_temp it
                                  where s.item = it.like_existing_item
                                    and s.supplier = L_supplier
                                    and s.origin_country = L_origin_country_id
                                    and s.dim_object = L_case_dim;
      end if;
      if L_pallet_dim is not null then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM',NULL);
         insert into item_supp_country_dim(item,
                                        supplier,
                                        origin_country,
                                        dim_object,
                                        presentation_method,
                                        length,
                                        width,
                                        height,
                                        lwh_uom,
                                        weight,
                                        net_weight,
                                        weight_uom,
                                        liquid_volume,
                                        liquid_volume_uom,
                                        stat_cube,
                                        tare_weight,
                                        tare_type,
                                        last_update_datetime,
                                        last_update_id,
                                        create_datetime)
                                 select it.item,
                                        s.supplier,
                                        s.origin_country,
                                        s.dim_object,
                                        s.presentation_method,
                                        L_pallet_length,
                                        L_pallet_width,
                                        L_pallet_height,
                                        L_pallet_lwh_uom,
                                        L_gross_pallet_weight,
                                        L_net_pallet_weight,
                                        L_pallet_weight_uom,
                                        Null,
                                        Null,
                                        s.stat_cube,
                                        s.tare_weight,
                                        s.tare_type,
                                        sysdate,
                                        L_user,
                                        sysdate
                                   from item_supp_country_dim s,
                                        item_temp it
                                  where s.item = it.like_existing_item
                                    and s.supplier = L_supplier
                                    and s.origin_country = L_origin_country_id
                                    and s.dim_object = L_pallet_dim;
      end if;     
   end if; -- Supplier Ind end
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY',NULL);
   insert into item_country(item,
                            country_id)
                     select it.item,
                            ic.country_id
                       from item_country ic,
                            item_temp it
                      where ic.item = it.like_existing_item;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY_L10N_EXT',NULL);
   insert into item_country_l10n_ext(item,
                                     country_id,
                                     l10n_country_id,
                                     group_id,
                                     varchar2_1,
                                     varchar2_2,
                                     varchar2_3,
                                     varchar2_4,
                                     varchar2_5,
                                     varchar2_6,
                                     varchar2_7,
                                     varchar2_8,
                                     varchar2_9,
                                     varchar2_10,
                                     number_11,
                                     number_12,
                                     number_13,
                                     number_14,
                                     number_15,
                                     number_16,
                                     number_17,
                                     number_18,
                                     number_19,
                                     number_20,
                                     date_21,
                                     date_22)
                              select it.item,
                                     ice.country_id,
                                     ice.l10n_country_id,
                                     ice.group_id,
                                     ice.varchar2_1,
                                     ice.varchar2_2,
                                     ice.varchar2_3,
                                     ice.varchar2_4,
                                     ice.varchar2_5,
                                     ice.varchar2_6,
                                     ice.varchar2_7,
                                     ice.varchar2_8,
                                     ice.varchar2_9,
                                     ice.varchar2_10,
                                     ice.number_11,
                                     ice.number_12,
                                     ice.number_13,
                                     ice.number_14,
                                     ice.number_15,
                                     ice.number_16,
                                     ice.number_17,
                                     ice.number_18,
                                     ice.number_19,
                                     ice.number_20,
                                     ice.date_21,
                                     ice.date_22
                                from item_country_l10n_ext ice,
                                     item_temp it
                               where ice.item = it.like_existing_item;
   ---
   if I_price_ind = 'Y' then
	  open C_RPM_IND;
      fetch C_RPM_IND into L_rpm_ind;
      close C_RPM_IND;
	  ---
	  if L_rpm_ind = 'Y' then
         if NOT SET_RPM_ITEM_ZONE_PRICE(O_error_message) then
            return false;
         end if;
      end if;
	  ---
   end if;
   ---
   for rec in C_GET_TEMP_ITEMS loop
      L_item := rec.item;
      L_like_item := rec.like_existing_item;
      ---
      if I_req_doc_ind = 'Y' then
         if DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                       'IT',
                                       'IT',
                                       L_like_item,
                                       L_item,
                                       NULL,
                                       NULL) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      L_tax_info_rec                       := OBJ_TAX_INFO_REC();
      L_tax_info_tbl.DELETE;
      L_tax_info_tbl.EXTEND;
      L_tax_info_rec.item                  := rec.item;
      L_tax_info_rec.item_parent           := rec.like_existing_item;
      L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;


      if TAX_SQL.GET_TAX_INFO(O_error_message,
                              L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;

      -- In gtax environment we are copying store to gtax_item_rollup records only if I_store_ind is 'Y,
      -- warehouse records only if I_wh_ind is 'Y' and both if I_store_ind and I_store_ind are 'Y'

      if L_default_tax_type = 'GTAX' and
             (( I_store_ind = 'N' and I_wh_ind = 'Y') or ( I_store_ind = 'Y' and I_wh_ind = 'N')) then

         open C_GET_GTAX_VALID_INFO;
         L_tax_info_tbl_temp.delete();
         fetch C_GET_GTAX_VALID_INFO bulk collect into L_tax_info_tbl_temp;
         close C_GET_GTAX_VALID_INFO;

         if TAX_SQL.INSERT_UPDATE_TAX_SKU(O_error_message,
                                          L_run_report,
                                          L_tax_info_tbl_temp) = FALSE then
            return FALSE;
         end if;
      elsif (L_default_tax_type = 'GTAX' and ( I_store_ind = 'Y' and I_wh_ind = 'Y')) or L_default_tax_type != 'GTAX' then
         if TAX_SQL.INSERT_UPDATE_TAX_SKU(O_error_message,
                                          L_run_report,
                                          L_tax_info_tbl) = FALSE then
            return FALSE;
         end if;
      end if;

   end loop;
   ---
   --- Location information ---
   if I_store_ind = 'Y' then
      if I_supplier_ind = 'Y' then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC',NULL);
         insert into item_supp_country_loc(item,
                                           supplier,
                                           origin_country_id,
                                           loc,
                                           loc_type,
                                           primary_loc_ind,
                                           unit_cost,
                                           round_lvl,
                                           round_to_inner_pct,
                                           round_to_case_pct,
                                           round_to_layer_pct,
                                           round_to_pallet_pct,
                                           supp_hier_type_1,
                                           supp_hier_lvl_1,
                                           supp_hier_type_2,
                                           supp_hier_lvl_2,
                                           supp_hier_type_3,
                                           supp_hier_lvl_3,
                                           pickup_lead_time,
                                           last_update_datetime,
                                           last_update_id,
                                           create_datetime,
                                           negotiated_item_cost,
                                           extended_base_cost,
                                           inclusive_cost,
                                           base_cost)
                                    select it.item,
                                           s.supplier,
                                           s.origin_country_id,
                                           s.loc,
                                           s.loc_type,
                                           s.primary_loc_ind,
                                           s.unit_cost,
                                           s.round_lvl,
                                           s.round_to_inner_pct,
                                           s.round_to_case_pct,
                                           s.round_to_layer_pct,
                                           s.round_to_pallet_pct,
                                           s.supp_hier_type_1,
                                           s.supp_hier_lvl_1,
                                           s.supp_hier_type_2,
                                           s.supp_hier_lvl_2,
                                           s.supp_hier_type_3,
                                           s.supp_hier_lvl_3,
                                           s.pickup_lead_time,
                                           sysdate,
                                           L_user,
                                           sysdate,
                                           s.negotiated_item_cost,
                                           s.extended_base_cost,
                                           s.inclusive_cost,
                                           s.base_cost
                                      from item_supp_country_loc s,
                                           item_temp it,
                                           store st,
                                           item_loc il
                                     where s.item                     = it.like_existing_item
                                       and st.store                   = s.loc
                                       and s.loc_type                 = 'S'
                                       and it.like_existing_item = il.item
                                       and s.loc = il.loc
                                       and (I_wh_ind = 'N' and
                                              (
                                                (st.store_type='F' and il.source_method != 'W' and il.costing_loc_type != 'W' and il.costing_loc in (select il2.loc
                                                                                                                                                      from item_loc il2,
                                                                                                                                                              store st2
                                                                                                                                                      where il2.loc = st2.store
                                                                                                                                                        and il2.item = it.like_existing_item
                                                                                                                                                        and (
                                                                                                                                                             (st2.store_type='F' and il2.source_method != 'W' and il2.costing_loc_type != 'W')
                                                                                                                                                              or
                                                                                                                                                             (st2.store_type='C' and il2.source_method != 'W')
                                                                                                                                                            )
                                                                                                                                                    ))
                                                 or
                                                (st.store_type='C' and il.source_method != 'W')
                                               )
                                             or
                                             I_wh_ind = 'Y'
                                           );
---
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC_CFA_EXT',NULL);
         insert into item_supp_country_loc_cfa_ext(item,
                                                   supplier,
                                                   origin_country_id,
                                                   loc,
                                                   group_id,
                                                   varchar2_1,
                                                   varchar2_2,
                                                   varchar2_3,
                                                   varchar2_4,
                                                   varchar2_5,
                                                   varchar2_6,
                                                   varchar2_7,
                                                   varchar2_8,
                                                   varchar2_9,
                                                   varchar2_10,
                                                   number_11,
                                                   number_12,
                                                   number_13,
                                                   number_14,
                                                   number_15,
                                                   number_16,
                                                   number_17,
                                                   number_18,
                                                   number_19,
                                                   number_20,
                                                   date_21,
                                                   date_22,
                                                   date_23,
                                                   date_24,
                                                   date_25)
                                            select it.item,
                                                   isce.supplier,
                                                   isce.origin_country_id,
                                                   isce.loc,
                                                   isce.group_id,
                                                   isce.varchar2_1,
                                                   isce.varchar2_2,
                                                   isce.varchar2_3,
                                                   isce.varchar2_4,
                                                   isce.varchar2_5,
                                                   isce.varchar2_6,
                                                   isce.varchar2_7,
                                                   isce.varchar2_8,
                                                   isce.varchar2_9,
                                                   isce.varchar2_10,
                                                   isce.number_11,
                                                   isce.number_12,
                                                   isce.number_13,
                                                   isce.number_14,
                                                   isce.number_15,
                                                   isce.number_16,
                                                   isce.number_17,
                                                   isce.number_18,
                                                   isce.number_19,
                                                   isce.number_20,
                                                   isce.date_21,
                                                   isce.date_22,
                                                   isce.date_23,
                                                   isce.date_24,
                                                   isce.date_25
                                              from item_supp_country_loc_cfa_ext isce,
                                                   item_temp it,
                                                   store st,
                                                   item_loc il
                                              where it.like_existing_item          = isce.item
                                                and st.store                       = isce.loc
                                                and isce.item                      = il.item
                                                and isce.loc                       = il.loc
                                                and il.loc_type                    = 'S'
                                                and (I_wh_ind = 'N' and
                                              (
                                                (st.store_type='F' and il.source_method != 'W' and il.costing_loc_type != 'W' and il.costing_loc in (select il2.loc
                                                                                                                                                      from item_loc il2,
                                                                                                                                                              store st2
                                                                                                                                                      where il2.loc = st2.store
                                                                                                                                                        and il2.item = it.like_existing_item
                                                                                                                                                        and (
                                                                                                                                                             (st2.store_type='F' and il2.source_method != 'W' and il2.costing_loc_type != 'W')
                                                                                                                                                              or
                                                                                                                                                             (st2.store_type='C' and il2.source_method != 'W')
                                                                                                                                                            )
                                                                                                                                                    ))
                                                 or
                                                (st.store_type='C' and il.source_method != 'W')
                                               )
                                             or
                                             I_wh_ind = 'Y'
                                           );
      ---
      end if;  -- if supplier_ind = 'Y'
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC',NULL);
      insert into item_loc(item,
                           loc,
                           item_parent,
                           item_grandparent,
                           loc_type,
                           unit_retail,
                           clear_ind,
                           taxable_ind,
                           local_item_desc,
                           local_short_desc,
                           ti,
                           hi,
                           store_ord_mult,
                           status,
                           status_update_date,
                           daily_waste_pct,
                           meas_of_each,
                           meas_of_price,
                           uom_of_price,
                           primary_variant,
                           primary_supp,
                           primary_cntry,
                           selling_unit_retail,
                           selling_uom,
                           last_update_datetime,
                           last_update_id,
                           create_datetime,
                           inbound_handling_days,
                           store_price_ind,
                           source_method,
                           source_wh,
                           rpm_ind,
                           multi_units,
                           multi_unit_retail,
                           multi_selling_uom,
                           regular_unit_retail,
                           uin_type,
                           uin_label,
                           capture_time,
                           ext_uin_ind,
                           ranged_ind,
                           costing_loc,
                           costing_loc_type)
                    select it.item,
                           il.loc,
                           im.item_parent,
                           im.item_grandparent,
                           'S',
                           DECODE(I_price_ind, 'Y', il.regular_unit_retail, NULL),
                           'N',
                           il.taxable_ind,
                           im.item_desc,
                           im.short_desc,
                           il.ti,
                           il.hi,
                           il.store_ord_mult,
                           il.status,
                           L_vdate,
                           il.daily_waste_pct,
                           il.meas_of_each,
                           il.meas_of_price,
                           il.uom_of_price,
                           NULL,  -- primary variant
                           DECODE(I_supplier_ind,'Y',il.primary_supp, NULL),
                           DECODE(I_supplier_ind, 'Y', il.primary_cntry, NULL),
                           il.selling_unit_retail,
                           il.selling_uom,
                           sysdate,
                           L_user,
                           sysdate,
                           il.inbound_handling_days,
                           il.store_price_ind,
                           il.source_method,
                           il.source_wh,
                           'N',
                           il.multi_units,
                           il.multi_unit_retail,
                           il.multi_selling_uom,
                           DECODE(I_price_ind, 'Y', il.regular_unit_retail, NULL),
                           il.uin_type,
                           il.uin_label,
                           il.capture_time,
                           il.ext_uin_ind,
                           il.ranged_ind,
                           il.costing_loc,
                           il.costing_loc_type
                      from item_loc il,
                           item_temp it,
                           item_master im,
                           store s
                     where il.item         = it.like_existing_item
                       and it.item         = im.item
                       and s.store         = il.loc
                       and il.loc_type     = 'S'
                       and (I_wh_ind = 'N' and
                                              (
                                                (s.store_type='F' and il.source_method != 'W' and il.costing_loc_type != 'W' and il.costing_loc in (select il2.loc
                                                                                                                                                      from item_loc il2,
                                                                                                                                                              store st2
                                                                                                                                                      where il2.loc = st2.store
                                                                                                                                                        and il2.item = it.like_existing_item
                                                                                                                                                        and (
                                                                                                                                                             (st2.store_type='F' and il2.source_method != 'W' and il2.costing_loc_type != 'W')
                                                                                                                                                              or
                                                                                                                                                             (st2.store_type='C' and il2.source_method != 'W')
                                                                                                                                                            )
                                                                                                                                                    )
                                                 )
                                                 or
                                                (s.store_type='C' and il.source_method != 'W')
                                               )
                           or
                           I_wh_ind = 'Y'
                           );
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_CFA_EXT',NULL);
      insert into item_loc_cfa_ext(item,
                                   loc,
                                   group_id,
                                   varchar2_1,
                                   varchar2_2,
                                   varchar2_3,
                                   varchar2_4,
                                   varchar2_5,
                                   varchar2_6,
                                   varchar2_7,
                                   varchar2_8,
                                   varchar2_9,
                                   varchar2_10,
                                   number_11,
                                   number_12,
                                   number_13,
                                   number_14,
                                   number_15,
                                   number_16,
                                   number_17,
                                   number_18,
                                   number_19,
                                   number_20,
                                   date_21,
                                   date_22,
                                   date_23,
                                   date_24,
                                   date_25)
                            select it.item,
                                   ilce.loc,
                                   ilce.group_id,
                                   ilce.varchar2_1,
                                   ilce.varchar2_2,
                                   ilce.varchar2_3,
                                   ilce.varchar2_4,
                                   ilce.varchar2_5,
                                   ilce.varchar2_6,
                                   ilce.varchar2_7,
                                   ilce.varchar2_8,
                                   ilce.varchar2_9,
                                   ilce.varchar2_10,
                                   ilce.number_11,
                                   ilce.number_12,
                                   ilce.number_13,
                                   ilce.number_14,
                                   ilce.number_15,
                                   ilce.number_16,
                                   ilce.number_17,
                                   ilce.number_18,
                                   ilce.number_19,
                                   ilce.number_20,
                                   ilce.date_21,
                                   ilce.date_22,
                                   ilce.date_23,
                                   ilce.date_24,
                                   ilce.date_25
                              from item_temp it,
                                   item_loc_cfa_ext ilce,
                                   store s,
                                   item_loc il
                             where it.like_existing_item = ilce.item
                               and s.store               = ilce.loc
                               and ilce.item             = il.item
                               and ilce.loc              = il.loc
                               and il.loc_type           = 'S'
                               and (I_wh_ind = 'N' and
                                              (
                                                (s.store_type='F' and il.source_method != 'W' and il.costing_loc_type != 'W' and il.costing_loc in (select il2.loc
                                                                                                                                                      from item_loc il2,
                                                                                                                                                              store st2
                                                                                                                                                      where il2.loc = st2.store
                                                                                                                                                        and il2.item = it.like_existing_item
                                                                                                                                                        and (
                                                                                                                                                             (st2.store_type='F' and il2.source_method != 'W' and il2.costing_loc_type != 'W')
                                                                                                                                                              or
                                                                                                                                                             (st2.store_type='C' and il2.source_method != 'W')
                                                                                                                                                            )
                                                                                                                                                   )
                                                )
                                                 or
                                                (s.store_type='C' and il.source_method != 'W')
                                              )
                                     or
                                     I_wh_ind = 'Y'
                                   );
   ---
      if (I_supplier_ind = 'Y') then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_SOH',NULL);
         insert into item_loc_soh(item,
                                  item_parent,
                                  item_grandparent,
                                  loc,
                                  loc_type,
                                  av_cost,
                                  unit_cost,
                                  stock_on_hand,
                                  soh_update_datetime,
                                  last_hist_export_date,
                                  in_transit_qty,
                                  pack_comp_intran,
                                  pack_comp_soh,
                                  tsf_reserved_qty,
                                  pack_comp_resv,
                                  tsf_expected_qty,
                                  pack_comp_exp,
                                  rtv_qty,
                                  non_sellable_qty,
                                  customer_resv,
                                  customer_backorder,
                                  pack_comp_cust_resv,
                                  pack_comp_cust_back,
                                  pack_comp_non_sellable,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  primary_supp,
                                  primary_cntry,
                                  average_weight)
                           select inner.item, 
                                  inner.item_parent, 
                                  inner.item_grandparent, 
                                  inner.loc, 
                                  inner.loc_type, 
                                  DECODE(I_supplier_ind, 'Y', DECODE(L_default_tax_type, 'GTAX', inner.extended_base_cost, inner.unit_cost), NULL), 
                                  DECODE(I_supplier_ind, 'Y', inner.unit_cost, NULL),
                                  0,
                                  NULL,
                                  NULL,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  sysdate,
                                  sysdate,
                                  L_user,
                                  DECODE(I_supplier_ind,'Y', inner.primary_supp, NULL),
                                  DECODE(I_supplier_ind, 'Y', inner.primary_cntry, NULL),
                                  inner.average_weight 
                             from (select it.item,
                                         il.item_parent,
                                         il.item_grandparent,
                                         il.loc,
                                         il.loc_type,
                                         il.primary_supp,
                                         il.primary_cntry,
                                         ils.unit_cost,
                                         ils.average_weight,
                                         iscl.extended_base_cost * mvc.exchange_rate extended_base_cost,
                                         mvc.effective_date,
                                         rank() over
                                            (PARTITION BY it.item,
                                                          il.item_parent,
                                                          il.item_grandparent,
                                                          il.loc,
                                                          il.loc_type,
                                                          il.primary_supp,
                                                          il.primary_cntry,
                                                          ils.unit_cost,
                                                          ils.average_weight,
                                                          iscl.extended_base_cost
                                                 ORDER BY mvc.effective_date DESC) date_rank
                                    from item_loc il,
                                         item_loc_soh ils,
                                         item_temp it,
                                         item_master im,
                                         item_supp_country_loc iscl,
                                         store s,
                                         sups sup,
                                         mv_currency_conversion_rates mvc
                                   where il.item                = it.item
                                     and it.item                = im.item
                                     and im.item_level          = im.tran_level
                                     and s.store                = il.loc
                                     and il.loc_type            = 'S'
                                     and ils.item               = it.like_existing_item
                                     and ils.loc                = il.loc
                                     and iscl.item              = it.like_existing_item
                                     and iscl.loc               = ils.loc
                                     and iscl.supplier          = NVL(L_supplier, il.primary_supp)
                                     and iscl.origin_country_id = NVL(L_origin_country_id, il.primary_cntry)
                                     and ((s.store_type         = 'C'
                                           and I_wh_ind         = 'N')
                                      or I_wh_ind               = 'Y')
                                     and iscl.supplier          = sup.supplier
                                     and mvc.from_currency      = sup.currency_code
                                     and mvc.to_currency        = s.currency_code
                                     and mvc.exchange_type      = 'C'
                                     and mvc.effective_date    <= L_vdate) inner
                              where inner.date_rank = 1;

      elsif (I_supplier_ind = 'N') then
         insert into item_loc_soh(item,
                                  item_parent,
                                  item_grandparent,
                                  loc,
                                  loc_type,
                                  av_cost,
                                  unit_cost,
                                  stock_on_hand,
                                  soh_update_datetime,
                                  last_hist_export_date,
                                  in_transit_qty,
                                  pack_comp_intran,
                                  pack_comp_soh,
                                  tsf_reserved_qty,
                                  pack_comp_resv,
                                  tsf_expected_qty,
                                  pack_comp_exp,
                                  rtv_qty,
                                  non_sellable_qty,
                                  customer_resv,
                                  customer_backorder,
                                  pack_comp_cust_resv,
                                  pack_comp_cust_back,
                                  pack_comp_non_sellable,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  primary_supp,
                                  primary_cntry,
                                  average_weight)
                           select it.item,
                                  il.item_parent,
                                  il.item_grandparent,
                                  il.loc,
                                  il.loc_type,
                                  NULL,
                                  NULL,
                                  0,
                                  NULL,
                                  NULL,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  sysdate,
                                  sysdate,
                                  L_user,
                                  NULL,
                                  NULL,
                                  ils.average_weight
                            from item_loc il,
                                 item_loc_soh ils,
                                 item_temp it,
                                 item_master im,
                                 store s
                           where il.item                = it.item
                             and it.item                = im.item
                             and im.item_level          = im.tran_level
                             and s.store                = il.loc
                             and il.loc_type            = 'S'
                             and ils.item               = it.like_existing_item
                             and ils.loc                = il.loc
                             and (I_wh_ind = 'N' and
                                              (
                                                (s.store_type='F' and il.source_method != 'W' and il.costing_loc_type != 'W' and il.costing_loc in (select il2.loc
                                                                                                                                                      from item_loc il2,
                                                                                                                                                              store st2
                                                                                                                                                      where il2.loc = st2.store
                                                                                                                                                        and il2.item = it.like_existing_item
                                                                                                                                                        and (
                                                                                                                                                             (st2.store_type='F' and il2.source_method != 'W' and il2.costing_loc_type != 'W')
                                                                                                                                                              or
                                                                                                                                                             (st2.store_type='C' and il2.source_method != 'W')
                                                                                                                                                            )
                                                                                                                                                   )
                                                )
                                                or
                                                (s.store_type='C' and il.source_method != 'W')
                                              )
                                 or
                                 I_wh_ind = 'Y'
                                 );
      end if;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_TRAITS',NULL);
      insert into item_loc_traits(item,
                                  loc,
                                  launch_date,
                                  qty_key_options,
                                  manual_price_entry,
                                  deposit_code,
                                  food_stamp_ind,
                                  wic_ind,
                                  proportional_tare_pct,
                                  fixed_tare_value,
                                  fixed_tare_uom,
                                  reward_eligible_ind,
                                  natl_brand_comp_item,
                                  return_policy,
                                  stop_sale_ind,
                                  elect_mtk_clubs,
                                  report_code,
                                  req_shelf_life_on_selection,
                                  req_shelf_life_on_receipt,
                                  ib_shelf_life,
                                  store_reorderable_ind,
                                  rack_size,
                                  full_pallet_item,
                                  in_store_market_basket,
                                  storage_location,
                                  alt_storage_location,
                                  returnable_ind,
                                  refundable_ind,
                                  back_order_ind,
                                  create_datetime,
                                  last_update_id,
                                  last_update_datetime )
                           select it.item,
                                  ilt.loc,
                                  ilt.launch_date,
                                  ilt.qty_key_options,
                                  ilt.manual_price_entry,
                                  ilt.deposit_code,
                                  ilt.food_stamp_ind,
                                  ilt.wic_ind,
                                  ilt.proportional_tare_pct,
                                  ilt.fixed_tare_value,
                                  ilt.fixed_tare_uom,
                                  ilt.reward_eligible_ind,
                                  ilt.natl_brand_comp_item,
                                  ilt.return_policy,
                                  ilt.stop_sale_ind,
                                  ilt.elect_mtk_clubs,
                                  ilt.report_code,
                                  ilt.req_shelf_life_on_selection,
                                  ilt.req_shelf_life_on_receipt,
                                  ilt.ib_shelf_life,
                                  ilt.store_reorderable_ind,
                                  ilt.rack_size,
                                  ilt.full_pallet_item,
                                  ilt.in_store_market_basket,
                                  ilt.storage_location,
                                  ilt.alt_storage_location,
                                  ilt.returnable_ind,
                                  ilt.refundable_ind,
                                  ilt.back_order_ind,
                                  sysdate,
                                  L_user,
                                  sysdate
                             from store s,
                                  item_loc_traits ilt,
                                  item_temp it,
                                  item_loc il
                            where ilt.loc = s.store
                              and it.like_existing_item = ilt.item
                              and ilt.item              = il.item
                              and ilt.loc               = il.loc
                              and il.loc_type           = 'S'
                              and (I_wh_ind = 'N' and
                                              (
                                                (s.store_type='F' and il.source_method != 'W' and il.costing_loc_type != 'W' and il.costing_loc in (select il2.loc
                                                                                                                                                      from item_loc il2,
                                                                                                                                                              store st2
                                                                                                                                                      where il2.loc = st2.store
                                                                                                                                                        and il2.item = it.like_existing_item
                                                                                                                                                        and (
                                                                                                                                                             (st2.store_type='F' and il2.source_method != 'W' and il2.costing_loc_type != 'W')
                                                                                                                                                              or
                                                                                                                                                             (st2.store_type='C' and il2.source_method != 'W')
                                                                                                                                                            )
                                                                                                                                                    )
                                                )
                                                or
                                                (s.store_type='C' and il.source_method != 'W')
                                               )
                                  or
                                  I_wh_ind = 'Y');
   end if;
   ---
   if I_wh_ind = 'Y' then
      if I_supplier_ind = 'Y' then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC',NULL);
         insert into item_supp_country_loc(item,
                                           supplier,
                                           origin_country_id,
                                           loc,
                                           loc_type,
                                           primary_loc_ind,
                                           unit_cost,
                                           round_lvl,
                                           round_to_inner_pct,
                                           round_to_case_pct,
                                           round_to_layer_pct,
                                           round_to_pallet_pct,
                                           supp_hier_type_1,
                                           supp_hier_lvl_1,
                                           supp_hier_type_2,
                                           supp_hier_lvl_2,
                                           supp_hier_type_3,
                                           supp_hier_lvl_3,
                                           pickup_lead_time,
                                           last_update_datetime,
                                           last_update_id,
                                           create_datetime,
                                           negotiated_item_cost,
                                           extended_base_cost,
                                           inclusive_cost,
                                           base_cost)
                                    select it.item,
                                           s.supplier,
                                           s.origin_country_id,
                                           s.loc,
                                           s.loc_type,
                                           s.primary_loc_ind,
                                           s.unit_cost,
                                           s.round_lvl,
                                           s.round_to_inner_pct,
                                           s.round_to_case_pct,
                                           s.round_to_layer_pct,
                                           s.round_to_pallet_pct,
                                           s.supp_hier_type_1,
                                           s.supp_hier_lvl_1,
                                           s.supp_hier_type_2,
                                           s.supp_hier_lvl_2,
                                           s.supp_hier_type_3,
                                           s.supp_hier_lvl_3,
                                           s.pickup_lead_time,
                                           sysdate,
                                           L_user,
                                           sysdate,
                                           s.negotiated_item_cost,
                                           s.extended_base_cost,
                                           s.inclusive_cost,
                                           s.base_cost
                                      from item_supp_country_loc s,
                                           item_temp it,
                                           wh
                                     where s.item                     = it.like_existing_item
                                       and wh.wh                      = s.loc
                                       and wh.finisher_ind            = 'N'
                                       and s.loc_type                 = 'W';
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC_CFA_EXT',NULL);
         insert into item_supp_country_loc_cfa_ext(item,
                                                   supplier,
                                                   origin_country_id,
                                                   loc,
                                                   group_id,
                                                   varchar2_1,
                                                   varchar2_2,
                                                   varchar2_3,
                                                   varchar2_4,
                                                   varchar2_5,
                                                   varchar2_6,
                                                   varchar2_7,
                                                   varchar2_8,
                                                   varchar2_9,
                                                   varchar2_10,
                                                   number_11,
                                                   number_12,
                                                   number_13,
                                                   number_14,
                                                   number_15,
                                                   number_16,
                                                   number_17,
                                                   number_18,
                                                   number_19,
                                                   number_20,
                                                   date_21,
                                                   date_22,
                                                   date_23,
                                                   date_24,
                                                   date_25)
                                            select it.item,
                                                   isce.supplier,
                                                   isce.origin_country_id,
                                                   isce.loc,
                                                   isce.group_id,
                                                   isce.varchar2_1,
                                                   isce.varchar2_2,
                                                   isce.varchar2_3,
                                                   isce.varchar2_4,
                                                   isce.varchar2_5,
                                                   isce.varchar2_6,
                                                   isce.varchar2_7,
                                                   isce.varchar2_8,
                                                   isce.varchar2_9,
                                                   isce.varchar2_10,
                                                   isce.number_11,
                                                   isce.number_12,
                                                   isce.number_13,
                                                   isce.number_14,
                                                   isce.number_15,
                                                   isce.number_16,
                                                   isce.number_17,
                                                   isce.number_18,
                                                   isce.number_19,
                                                   isce.number_20,
                                                   isce.date_21,
                                                   isce.date_22,
                                                   isce.date_23,
                                                   isce.date_24,
                                                   isce.date_25
                                              from item_supp_country_loc_cfa_ext isce,
                                                   item_temp it,
                                                   wh
                                              where it.like_existing_item          = isce.item
                                                and wh.wh                          = isce.loc
                                                and wh.finisher_ind                = 'N';
      ---
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_BRACKET_COST',NULL);
         insert into item_supp_country_bracket_cost(item,
                                                    supplier,
                                                    origin_country_id,
                                                    location,
                                                    bracket_value1,
                                                    loc_type,
                                                    default_bracket_ind,
                                                    unit_cost,
                                                    bracket_value2,
                                                    sup_dept_seq_no)
                                             select it.item,
                                                    s.supplier,
                                                    s.origin_country_id,
                                                    s.location,
                                                    s.bracket_value1,
                                                    s.loc_type,
                                                    s.default_bracket_ind,
                                                    s.unit_cost,
                                                    s.bracket_value2,
                                                    s.sup_dept_seq_no
                                               from item_supp_country_bracket_cost s,
                                                    item_temp it,
                                                    wh
                                              where s.item                    = it.like_existing_item
                                                and wh.wh                     = s.location
                                                and wh.finisher_ind           = 'N'
                                                and s.loc_type                = 'W';
      end if; -- if supplier_ind = 'Y'
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC',NULL);
      insert into item_loc(item,
                           loc,
                           item_parent,
                           item_grandparent,
                           loc_type,
                           unit_retail,
                           clear_ind,
                           taxable_ind,
                           local_item_desc,
                           local_short_desc,
                           ti,
                           hi,
                           store_ord_mult,
                           status,
                           status_update_date,
                           daily_waste_pct,
                           meas_of_each,
                           meas_of_price,
                           uom_of_price,
                           primary_variant,
                           primary_supp,
                           primary_cntry,
                           selling_unit_retail,
                           selling_uom,
                           last_update_datetime,
                           last_update_id,
                           create_datetime,
                           inbound_handling_days,
                           store_price_ind,
                           source_method,
                           source_wh,
                           rpm_ind,
                           multi_units,
                           multi_unit_retail,
                           multi_selling_uom,
                           regular_unit_retail,
                           uin_type,
                           uin_label,
                           capture_time,
                           ext_uin_ind,
                           ranged_ind,
                           costing_loc,
                           costing_loc_type)
                    select it.item,
                           il.loc,
                           im.item_parent,
                           im.item_grandparent,
                           'W',
                           DECODE(I_price_ind, 'Y', il.regular_unit_retail, NULL),
                           'N',
                           il.taxable_ind,
                           im.item_desc,
                           im.short_desc,
                           il.ti,
                           il.hi,
                           il.store_ord_mult,
                           il.status,
                           L_vdate,
                           il.daily_waste_pct,
                           il.meas_of_each,
                           il.meas_of_price,
                           il.uom_of_price,
                           NULL,  -- primary variant
                           DECODE(I_supplier_ind,'Y', il.primary_supp, NULL),
                           DECODE(I_supplier_ind, 'Y', il.primary_cntry, NULL),
                           il.selling_unit_retail,
                           il.selling_uom,
                           sysdate,
                           L_user,
                           sysdate,
                           il.inbound_handling_days,
                           'N',
                           il.source_method,
                           il.source_wh,
                           'N',
                           il.multi_units,
                           il.multi_unit_retail,
                           il.multi_selling_uom,
                           DECODE(I_price_ind, 'Y', il.regular_unit_retail, NULL),
                           il.uin_type,
                           il.uin_label,
                           il.capture_time,
                           il.ext_uin_ind,
                           il.ranged_ind,
                           il.costing_loc,
                           il.costing_loc_type
                      from item_loc il,
                           item_temp it,
                           item_master im,
                           wh
                     where il.item     = it.like_existing_item
                       and it.item     = im.item
                       and wh.wh       = il.loc
                       and il.loc_type = 'W'
                       and wh.finisher_ind = 'N';

      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_CFA_EXT',NULL);
      insert into item_loc_cfa_ext(item,
                                   loc,
                                   group_id,
                                   varchar2_1,
                                   varchar2_2,
                                   varchar2_3,
                                   varchar2_4,
                                   varchar2_5,
                                   varchar2_6,
                                   varchar2_7,
                                   varchar2_8,
                                   varchar2_9,
                                   varchar2_10,
                                   number_11,
                                   number_12,
                                   number_13,
                                   number_14,
                                   number_15,
                                   number_16,
                                   number_17,
                                   number_18,
                                   number_19,
                                   number_20,
                                   date_21,
                                   date_22,
                                   date_23,
                                   date_24,
                                   date_25)
                            select it.item,
                                   ilce.loc,
                                   ilce.group_id,
                                   ilce.varchar2_1,
                                   ilce.varchar2_2,
                                   ilce.varchar2_3,
                                   ilce.varchar2_4,
                                   ilce.varchar2_5,
                                   ilce.varchar2_6,
                                   ilce.varchar2_7,
                                   ilce.varchar2_8,
                                   ilce.varchar2_9,
                                   ilce.varchar2_10,
                                   ilce.number_11,
                                   ilce.number_12,
                                   ilce.number_13,
                                   ilce.number_14,
                                   ilce.number_15,
                                   ilce.number_16,
                                   ilce.number_17,
                                   ilce.number_18,
                                   ilce.number_19,
                                   ilce.number_20,
                                   ilce.date_21,
                                   ilce.date_22,
                                   ilce.date_23,
                                   ilce.date_24,
                                   ilce.date_25
                              from item_temp it,
                                   item_loc_cfa_ext ilce,
                                   wh
                             where it.like_existing_item =ilce.item
                               and wh.wh                 = ilce.loc
                               and wh.finisher_ind       = 'N';

      ---
      if (I_supplier_ind='Y') then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_SOH',NULL);
         insert into item_loc_soh(item,
                                  item_parent,
                                  item_grandparent,
                                  loc,
                                  loc_type,
                                  av_cost,
                                  unit_cost,
                                  stock_on_hand,
                                  soh_update_datetime,
                                  last_hist_export_date,
                                  in_transit_qty,
                                  pack_comp_intran,
                                  pack_comp_soh,
                                  tsf_reserved_qty,
                                  pack_comp_resv,
                                  tsf_expected_qty,
                                  pack_comp_exp,
                                  rtv_qty,
                                  non_sellable_qty,
                                  customer_resv,
                                  customer_backorder,
                                  pack_comp_cust_resv,
                                  pack_comp_cust_back,
                                  pack_comp_non_sellable,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  primary_supp,
                                  primary_cntry,
                                  average_weight)
                           select inner.item, 
                                  inner.item_parent, 
                                  inner.item_grandparent, 
                                  inner.loc, 
                                  inner.loc_type, 
                                  DECODE(I_supplier_ind, 'Y', DECODE(L_default_tax_type, 'GTAX', inner.extended_base_cost, inner.unit_cost), NULL), 
                                  DECODE(I_supplier_ind, 'Y', inner.unit_cost, NULL),
                                  0,
                                  NULL,
                                  NULL,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  sysdate,
                                  sysdate,
                                  L_user,
                                  DECODE(I_supplier_ind,'Y', inner.primary_supp, NULL),
                                  DECODE(I_supplier_ind, 'Y', inner.primary_cntry, NULL),
                                  inner.average_weight
                             from (select it.item, 
                                          il.item_parent, 
                                          il.item_grandparent, 
                                          il.loc, 
                                          il.loc_type, 
                                          il.primary_supp, 
                                          il.primary_cntry, 
                                          ils.unit_cost, 
                                          ils.average_weight, 
                                          iscl.extended_base_cost * mvc.exchange_rate extended_base_cost, 
                                          mvc.effective_date, 
                                          rank() over 
                                             (PARTITION BY it.item, 
                                                           il.item_parent, 
                                                           il.item_grandparent, 
                                                           il.loc, 
                                                           il.loc_type, 
                                                           il.primary_supp, 
                                                           il.primary_cntry, 
                                                           ils.unit_cost, 
                                                           ils.average_weight, 
                                                           iscl.extended_base_cost 
                                                  ORDER BY mvc.effective_date DESC) date_rank 
                              from item_loc il,
                                   item_loc_soh ils,
                                   item_temp it,
                                   item_master im,
                                   item_supp_country_loc iscl,
                                   wh, 
                                   sups sup, 
                                   mv_currency_conversion_rates mvc
                             where il.item                = it.item
                               and it.item                = im.item
                               and im.item_level          = im.tran_level
                               and wh.wh                  = il.loc
                               and il.loc_type            = 'W'
                               and ils.item               = it.like_existing_item
                               and ils.loc                = il.loc
                               and wh.finisher_ind        = 'N'
                               and iscl.item              = it.like_existing_item
                               and iscl.loc               = ils.loc
                               and iscl.supplier          = NVL(L_supplier, il.primary_supp)
                               and iscl.origin_country_id = NVL(L_origin_country_id, il.primary_cntry) 
                               and iscl.supplier          = sup.supplier 
                               and mvc.from_currency      = sup.currency_code 
                               and mvc.to_currency        = wh.currency_code 
                               and mvc.exchange_type      = 'C' 
                               and mvc.effective_date    <= L_vdate) inner 
                          where inner.date_rank = 1; 
                               
      elsif (I_supplier_ind = 'N') then
         insert into item_loc_soh(item,
                                  item_parent,
                                  item_grandparent,
                                  loc,
                                  loc_type,
                                  av_cost,
                                  unit_cost,
                                  stock_on_hand,
                                  soh_update_datetime,
                                  last_hist_export_date,
                                  in_transit_qty,
                                  pack_comp_intran,
                                  pack_comp_soh,
                                  tsf_reserved_qty,
                                  pack_comp_resv,
                                  tsf_expected_qty,
                                  pack_comp_exp,
                                  rtv_qty,
                                  non_sellable_qty,
                                  customer_resv,
                                  customer_backorder,
                                  pack_comp_cust_resv,
                                  pack_comp_cust_back,
                                  pack_comp_non_sellable,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  primary_supp,
                                  primary_cntry,
                                  average_weight)
                           select it.item,
                                  il.item_parent,
                                  il.item_grandparent,
                                  il.loc,
                                  il.loc_type,
                                  NULL,
                                  NULL,
                                  0,
                                  NULL,
                                  NULL,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  sysdate,
                                  sysdate,
                                  L_user,
                                  NULL,
                                  NULL,
                                  ils.average_weight
                              from item_loc il,
                                   item_loc_soh ils,
                                   item_temp it,
                                   item_master im,
                                   wh
                             where il.item                = it.item
                               and it.item                = im.item
                               and im.item_level          = im.tran_level
                               and wh.wh                  = il.loc
                               and il.loc_type            = 'W'
                               and ils.item               = it.like_existing_item
                               and ils.loc                = il.loc
                               and wh.finisher_ind        = 'N';
      end if;
      ---

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_TRAITS',NULL);
      insert into item_loc_traits(item,
                                  loc,
                                  launch_date,
                                  qty_key_options,
                                  manual_price_entry,
                                  deposit_code,
                                  food_stamp_ind,
                                  wic_ind,
                                  proportional_tare_pct,
                                  fixed_tare_value,
                                  fixed_tare_uom,
                                  reward_eligible_ind,
                                  natl_brand_comp_item,
                                  return_policy,
                                  stop_sale_ind,
                                  elect_mtk_clubs,
                                  report_code,
                                  req_shelf_life_on_selection,
                                  req_shelf_life_on_receipt,
                                  ib_shelf_life,
                                  store_reorderable_ind,
                                  rack_size,
                                  full_pallet_item,
                                  in_store_market_basket,
                                  storage_location,
                                  alt_storage_location,
                                  returnable_ind,
                                  refundable_ind,
                                  back_order_ind,
                                  create_datetime,
                                  last_update_id,
                                  last_update_datetime )
                           select it.item,
                                  ilt.loc,
                                  ilt.launch_date,
                                  ilt.qty_key_options,
                                  ilt.manual_price_entry,
                                  ilt.deposit_code,
                                  ilt.food_stamp_ind,
                                  ilt.wic_ind,
                                  ilt.proportional_tare_pct,
                                  ilt.fixed_tare_value,
                                  ilt.fixed_tare_uom,
                                  ilt.reward_eligible_ind,
                                  ilt.natl_brand_comp_item,
                                  ilt.return_policy,
                                  ilt.stop_sale_ind,
                                  ilt.elect_mtk_clubs,
                                  ilt.report_code,
                                  ilt.req_shelf_life_on_selection,
                                  ilt.req_shelf_life_on_receipt,
                                  ilt.ib_shelf_life,
                                  ilt.store_reorderable_ind,
                                  ilt.rack_size,
                                  ilt.full_pallet_item,
                                  ilt.in_store_market_basket,
                                  ilt.storage_location,
                                  ilt.alt_storage_location,
                                  ilt.returnable_ind,
                                  ilt.refundable_ind,
                                  ilt.back_order_ind,
                                  sysdate,
                                  L_user,
                                  sysdate
                             from wh,
                                  item_loc_traits ilt,
                                  item_temp it
                            where ilt.loc = wh.wh
                              and it.like_existing_item = ilt.item

                              and wh.finisher_ind       = 'N';
   end if;
   ---

-- Update the primary loc ind 

   L_update := 'Y';
      ---
   SQL_LIB.SET_MARK('OPEN','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_new_item);
   open C_GET_MIN_ISC_LOC;
   SQL_LIB.SET_MARK('FETCH','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_new_item);
   fetch C_GET_MIN_ISC_LOC into L_min_loc;
    ---
   if C_GET_MIN_ISC_LOC%NOTFOUND then
      L_update := 'N';
   end if;
      ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_new_item);
   close C_GET_MIN_ISC_LOC;

   if L_update = 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_UPDATE_PRIM_LOC_IND','ITEM_SUPP_COUNTRY_LOC','Item: '||I_new_item);
      open C_UPDATE_PRIM_LOC_IND;
      SQL_LIB.SET_MARK('CLOSE','C_UPDATE_PRIM_LOC_IND','ITEM_SUPP_COUNTRY_LOC','Item: '||I_new_item);
      close C_UPDATE_PRIM_LOC_IND;
         ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_new_item);

      update item_supp_country_loc isl
         set primary_loc_ind      = 'Y',
             last_update_datetime = sysdate,
             last_update_id       = get_user
       where isl.item              = I_new_item
         and isl.loc               = L_min_loc
         and not exists (select 'x'
                           from item_supp_country_loc isl3
                          where isl3.item              = isl.item
                            and isl3.loc              != isl.loc
                            and isl3.primary_loc_ind   = 'Y'
                            and rownum = 1);
   end if;

   if I_internal_finisher = 'Y' then
      if I_supplier_ind = 'Y' then
         SQL_LIB.SET_MARK('INSERT',
                           NULL,
                          'ITEM_SUPP_COUNTRY_LOC',
                           NULL);
         insert into item_supp_country_loc(item,
                                           supplier,
                                           origin_country_id,
                                           loc,
                                           loc_type,
                                           primary_loc_ind,
                                           unit_cost,
                                           round_lvl,
                                           round_to_inner_pct,
                                           round_to_case_pct,
                                           round_to_layer_pct,
                                           round_to_pallet_pct,
                                           supp_hier_type_1,
                                           supp_hier_lvl_1,
                                           supp_hier_type_2,
                                           supp_hier_lvl_2,
                                           supp_hier_type_3,
                                           supp_hier_lvl_3,
                                           pickup_lead_time,
                                           last_update_datetime,
                                           last_update_id,
                                           create_datetime,
                                           negotiated_item_cost,
                                           extended_base_cost,
                                           inclusive_cost,
                                           base_cost)
                                    select it.item,
                                           s.supplier,
                                           s.origin_country_id,
                                           s.loc,
                                           s.loc_type,
                                           s.primary_loc_ind,
                                           s.unit_cost,
                                           s.round_lvl,
                                           s.round_to_inner_pct,
                                           s.round_to_case_pct,
                                           s.round_to_layer_pct,
                                           s.round_to_pallet_pct,
                                           s.supp_hier_type_1,
                                           s.supp_hier_lvl_1,
                                           s.supp_hier_type_2,
                                           s.supp_hier_lvl_2,
                                           s.supp_hier_type_3,
                                           s.supp_hier_lvl_3,
                                           s.pickup_lead_time,
                                           sysdate,
                                           L_user,
                                           sysdate,
                                           s.negotiated_item_cost,
                                           s.extended_base_cost,
                                           s.inclusive_cost,
                                           s.base_cost
                                      from item_supp_country_loc s,
                                           item_temp it,
                                           wh
                                     where s.item                     = it.like_existing_item
                                       and wh.wh                      = s.loc
                                       and wh.finisher_ind            = 'Y'
                                       and s.loc_type                 = 'W';
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC_CFA_EXT',NULL);
         insert into item_supp_country_loc_cfa_ext(item,
                                                   supplier,
                                                   origin_country_id,
                                                   loc,
                                                   group_id,
                                                   varchar2_1,
                                                   varchar2_2,
                                                   varchar2_3,
                                                   varchar2_4,
                                                   varchar2_5,
                                                   varchar2_6,
                                                   varchar2_7,
                                                   varchar2_8,
                                                   varchar2_9,
                                                   varchar2_10,
                                                   number_11,
                                                   number_12,
                                                   number_13,
                                                   number_14,
                                                   number_15,
                                                   number_16,
                                                   number_17,
                                                   number_18,
                                                   number_19,
                                                   number_20,
                                                   date_21,
                                                   date_22,
                                                   date_23,
                                                   date_24,
                                                   date_25)
                                            select it.item,
                                                   isce.supplier,
                                                   isce.origin_country_id,
                                                   isce.loc,
                                                   isce.group_id,
                                                   isce.varchar2_1,
                                                   isce.varchar2_2,
                                                   isce.varchar2_3,
                                                   isce.varchar2_4,
                                                   isce.varchar2_5,
                                                   isce.varchar2_6,
                                                   isce.varchar2_7,
                                                   isce.varchar2_8,
                                                   isce.varchar2_9,
                                                   isce.varchar2_10,
                                                   isce.number_11,
                                                   isce.number_12,
                                                   isce.number_13,
                                                   isce.number_14,
                                                   isce.number_15,
                                                   isce.number_16,
                                                   isce.number_17,
                                                   isce.number_18,
                                                   isce.number_19,
                                                   isce.number_20,
                                                   isce.date_21,
                                                   isce.date_22,
                                                   isce.date_23,
                                                   isce.date_24,
                                                   isce.date_25
                                              from item_supp_country_loc_cfa_ext isce,
                                                   item_temp it,
                                                   wh
                                              where it.like_existing_item=isce.item
                                                and wh.wh                      = isce.loc
                                                and wh.finisher_ind            = 'Y';
        ---
         SQL_LIB.SET_MARK('INSERT',
                           NULL,
                          'ITEM_SUPP_COUNTRY_BRACKET_COST',
                           NULL);
         insert into item_supp_country_bracket_cost(item,
                                                    supplier,
                                                    origin_country_id,
                                                    location,
                                                    bracket_value1,
                                                    loc_type,
                                                    default_bracket_ind,
                                                    unit_cost,
                                                    bracket_value2,
                                                    sup_dept_seq_no)
                                             select it.item,
                                                    s.supplier,
                                                    s.origin_country_id,
                                                    s.location,
                                                    s.bracket_value1,
                                                    s.loc_type,
                                                    s.default_bracket_ind,
                                                    s.unit_cost,
                                                    s.bracket_value2,
                                                    s.sup_dept_seq_no
                                               from item_supp_country_bracket_cost s,
                                                    item_temp it,
                                                    wh
                                              where s.item                    = it.like_existing_item
                                                and wh.wh                     = s.location
                                                and wh.finisher_ind           = 'Y'
                                                and s.loc_type                = 'W';
      end if; -- if I_supplier_ind = 'Y'

      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'ITEM_LOC',
                        NULL);
      insert into item_loc(item,
                           loc,
                           item_parent,
                           item_grandparent,
                           loc_type,
                           unit_retail,
                           clear_ind,
                           taxable_ind,
                           local_item_desc,
                           local_short_desc,
                           ti,
                           hi,
                           store_ord_mult,
                           status,
                           status_update_date,
                           daily_waste_pct,
                           meas_of_each,
                           meas_of_price,
                           uom_of_price,
                           primary_variant,
                           primary_supp,
                           primary_cntry,
                           selling_unit_retail,
                           selling_uom,
                           last_update_datetime,
                           last_update_id,
                           create_datetime,
                           inbound_handling_days,
                           store_price_ind,
                           source_method,
                           source_wh,
                           rpm_ind,
                           multi_units,
                           multi_unit_retail,
                           multi_selling_uom,
                           regular_unit_retail,
                           uin_type,
                           uin_label,
                           capture_time,
                           ext_uin_ind,
                           ranged_ind,
                           costing_loc,
                           costing_loc_type)
                    select it.item,
                           il.loc,
                           im.item_parent,
                           im.item_grandparent,
                           'W',
                           DECODE(I_price_ind, 'Y', il.unit_retail, NULL),
                           'N',
                           il.taxable_ind,
                           im.item_desc,
                           im.short_desc,
                           il.ti,
                           il.hi,
                           il.store_ord_mult,
                           il.status,
                           L_vdate,
                           il.daily_waste_pct,
                           il.meas_of_each,
                           il.meas_of_price,
                           il.uom_of_price,
                           NULL,  -- primary variant
                           DECODE(I_supplier_ind,'Y', il.primary_supp, NULL),
                           DECODE(I_supplier_ind, 'Y', il.primary_cntry, NULL),
                           il.selling_unit_retail,
                           il.selling_uom,
                           sysdate,
                           L_user,
                           sysdate,
                           il.inbound_handling_days,
                           'N',
                           il.source_method,
                           il.source_wh,
                           'N',
                           il.multi_units,
                           il.multi_unit_retail,
                           il.multi_selling_uom,
                           il.regular_unit_retail,
                           il.uin_type,
                           il.uin_label,
                           il.capture_time,
                           il.ext_uin_ind,
                           il.ranged_ind,
                           il.costing_loc,
                           il.costing_loc_type
                      from item_loc il,
                           item_temp it,
                           item_master im,
                           wh
                     where il.item         = it.like_existing_item
                       and it.item         = im.item
                       and wh.wh           = il.loc
                       and il.loc_type     = 'W'
                       and wh.finisher_ind = 'Y';
      ---
	  
	  SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'ITEM_LOC_L10N_EXT',
                       NULL);
      insert into item_loc_l10n_ext(item,
                                    location,
                                    l10n_country_id,
                                    group_id,
                                    varchar2_1,
                                    varchar2_2,
                                    varchar2_3,
                                    varchar2_4,
                                    varchar2_5,
                                    varchar2_6,
                                    varchar2_7,
                                    varchar2_8,
                                    varchar2_9,
                                    varchar2_10,
                                    number_11,
                                    number_12,
                                    number_13,
                                    number_14,
                                    number_15,
                                    number_16,
                                    number_17,
                                    number_18,
                                    number_19,
                                    number_20,
                                    date_21,
                                    date_22)
                             select it.item,
                                    ile.location,
                                    ile.l10n_country_id,
                                    ile.group_id,
                                    ile.varchar2_1,
                                    ile.varchar2_2,
                                    ile.varchar2_3,
                                    ile.varchar2_4,
                                    ile.varchar2_5,
                                    ile.varchar2_6,
                                    ile.varchar2_7,
                                    ile.varchar2_8,
                                    ile.varchar2_9,
                                    ile.varchar2_10,
                                    ile.number_11,
                                    ile.number_12,
                                    ile.number_13,
                                    ile.number_14,
                                    ile.number_15,
                                    ile.number_16,
                                    ile.number_17,
                                    ile.number_18,
                                    ile.number_19,
                                    ile.number_20,
                                    ile.date_21,
                                    ile.date_22
                               from item_loc_l10n_ext ile,
                                    item_temp it,
                                    wh
                              where ile.item = it.like_existing_item
                                and wh.wh    = ile.location
                                and wh.finisher_ind = 'Y';
      ---

	  
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_CFA_EXT',NULL);
      insert into item_loc_cfa_ext(item,
                                   loc,
                                   group_id,
                                   varchar2_1,
                                   varchar2_2,
                                   varchar2_3,
                                   varchar2_4,
                                   varchar2_5,
                                   varchar2_6,
                                   varchar2_7,
                                   varchar2_8,
                                   varchar2_9,
                                   varchar2_10,
                                   number_11,
                                   number_12,
                                   number_13,
                                   number_14,
                                   number_15,
                                   number_16,
                                   number_17,
                                   number_18,
                                   number_19,
                                   number_20,
                                   date_21,
                                   date_22,
                                   date_23,
                                   date_24,
                                   date_25)
                            select it.item,
                                   ilce.loc,
                                   ilce.group_id,
                                   ilce.varchar2_1,
                                   ilce.varchar2_2,
                                   ilce.varchar2_3,
                                   ilce.varchar2_4,
                                   ilce.varchar2_5,
                                   ilce.varchar2_6,
                                   ilce.varchar2_7,
                                   ilce.varchar2_8,
                                   ilce.varchar2_9,
                                   ilce.varchar2_10,
                                   ilce.number_11,
                                   ilce.number_12,
                                   ilce.number_13,
                                   ilce.number_14,
                                   ilce.number_15,
                                   ilce.number_16,
                                   ilce.number_17,
                                   ilce.number_18,
                                   ilce.number_19,
                                   ilce.number_20,
                                   ilce.date_21,
                                   ilce.date_22,
                                   ilce.date_23,
                                   ilce.date_24,
                                   ilce.date_25
                              from item_temp it,
                                   item_loc_cfa_ext ilce,
                                   wh
                             where it.like_existing_item = ilce.item
                               and wh.wh                 = ilce.loc
                               and wh.finisher_ind       = 'Y';
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'ITEM_LOC_SOH',
                        NULL);
      insert into item_loc_soh(item,
                               item_parent,
                               item_grandparent,
                               loc,
                               loc_type,
                               av_cost,
                               unit_cost,
                               stock_on_hand,
                               soh_update_datetime,
                               last_hist_export_date,
                               in_transit_qty,
                               pack_comp_intran,
                               pack_comp_soh,
                               tsf_reserved_qty,
                               pack_comp_resv,
                               tsf_expected_qty,
                               pack_comp_exp,
                               rtv_qty,
                               non_sellable_qty,
                               customer_resv,
                               customer_backorder,
                               pack_comp_cust_resv,
                               pack_comp_cust_back,
                               pack_comp_non_sellable,
                               create_datetime,
                               last_update_datetime,
                               last_update_id,
                               primary_supp,
                               primary_cntry,
                               average_weight)
                        select inner.item,
                               inner.item_parent,
                               inner.item_grandparent,
                               inner.loc,
                               inner.loc_type,
                               DECODE(I_supplier_ind, 'Y', DECODE(L_default_tax_type,'SVAT',inner.unit_cost,'SALES', inner.unit_cost, inner.extended_base_cost), NULL),
                               DECODE(I_supplier_ind, 'Y', inner.unit_cost, NULL),
                               0,
                               NULL,
                               NULL,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               sysdate,
                               sysdate,
                               L_user,
                               DECODE(I_supplier_ind,'Y', inner.primary_supp, NULL),
                               DECODE(I_supplier_ind, 'Y', inner.primary_cntry, NULL),
                               inner.average_weight
                             from (select it.item,
                                          il.item_parent,
                                          il.item_grandparent,
                                          il.loc,
                                          il.loc_type,
                                          il.primary_supp,
                                          il.primary_cntry,
                                          ils.unit_cost,
                                          ils.average_weight,
                                          iscl.extended_base_cost * mvc.exchange_rate extended_base_cost,
                                          mvc.effective_date,
                                          rank() over
                                             (PARTITION BY it.item,
                                                           il.item_parent,
                                                           il.item_grandparent,
                                                           il.loc,
                                                           il.loc_type,
                                                           il.primary_supp,
                                                           il.primary_cntry,
                                                           ils.unit_cost,
                                                           ils.average_weight,
                                                           iscl.extended_base_cost
                                                  ORDER BY mvc.effective_date DESC) date_rank
                          from item_loc il,
                               item_loc_soh ils,
                               item_temp it,
                               item_master im,
                               item_supp_country_loc iscl,
                               wh,
                               sups sup,
                               mv_currency_conversion_rates mvc
                         where il.item                = it.item
                           and it.item                = im.item
                           and im.item_level          = im.tran_level
                           and wh.wh                  = il.loc
                           and il.loc_type            = 'W'
                           and ils.item               = it.like_existing_item
                           and ils.loc                = il.loc
                           and wh.finisher_ind        = 'Y'
                           and iscl.item              = it.like_existing_item
                           and iscl.loc               = ils.loc
                           and iscl.supplier          = NVL(L_supplier, il.primary_supp)
                           and iscl.origin_country_id = NVL(L_origin_country_id, il.primary_cntry)
                                      and iscl.supplier          = sup.supplier
                                      and mvc.from_currency      = sup.currency_code
                                      and mvc.to_currency        = wh.currency_code
                                      and mvc.exchange_type      = 'C'
                                      and mvc.effective_date    <= L_vdate) inner
                                where inner.date_rank = 1;
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'ITEM_LOC_TRAITS',
                        NULL);

      insert into item_loc_traits(item,
                                  loc,
                                  launch_date,
                                  qty_key_options,
                                  manual_price_entry,
                                  deposit_code,
                                  food_stamp_ind,
                                  wic_ind,
                                  proportional_tare_pct,
                                  fixed_tare_value,
                                  fixed_tare_uom,
                                  reward_eligible_ind,
                                  natl_brand_comp_item,
                                  return_policy,
                                  stop_sale_ind,
                                  elect_mtk_clubs,
                                  report_code,
                                  req_shelf_life_on_selection,
                                  req_shelf_life_on_receipt,
                                  ib_shelf_life,
                                  store_reorderable_ind,
                                  rack_size,
                                  full_pallet_item,
                                  in_store_market_basket,
                                  storage_location,
                                  alt_storage_location,
                                  returnable_ind,
                                  refundable_ind,
                                  back_order_ind,
                                  create_datetime,
                                  last_update_id,
                                  last_update_datetime )
                           select it.item,
                                  ilt.loc,
                                  ilt.launch_date,
                                  ilt.qty_key_options,
                                  ilt.manual_price_entry,
                                  ilt.deposit_code,
                                  ilt.food_stamp_ind,
                                  ilt.wic_ind,
                                  ilt.proportional_tare_pct,
                                  ilt.fixed_tare_value,
                                  ilt.fixed_tare_uom,
                                  ilt.reward_eligible_ind,
                                  ilt.natl_brand_comp_item,
                                  ilt.return_policy,
                                  ilt.stop_sale_ind,
                                  ilt.elect_mtk_clubs,
                                  ilt.report_code,
                                  ilt.req_shelf_life_on_selection,
                                  ilt.req_shelf_life_on_receipt,
                                  ilt.ib_shelf_life,
                                  ilt.store_reorderable_ind,
                                  ilt.rack_size,
                                  ilt.full_pallet_item,
                                  ilt.in_store_market_basket,
                                  ilt.storage_location,
                                  ilt.alt_storage_location,
                                  ilt.returnable_ind,
                                  ilt.refundable_ind,
                                  ilt.back_order_ind,
                                  sysdate,
                                  L_user,
                                  sysdate
                             from wh,
                                  item_loc_traits ilt,
                                  item_temp it
                            where ilt.loc               = wh.wh
                              and it.like_existing_item = ilt.item
                              and wh.finisher_ind       = 'Y';
   end if;
   ---

   if I_external_finisher = 'Y' then
      if I_supplier_ind = 'Y' then
         SQL_LIB.SET_MARK('INSERT',
                           NULL,
                          'ITEM_SUPP_COUNTRY_LOC',
                           NULL);
         insert into item_supp_country_loc(item,
                                           supplier,
                                           origin_country_id,
                                           loc,
                                           loc_type,
                                           primary_loc_ind,
                                           unit_cost,
                                           round_lvl,
                                           round_to_inner_pct,
                                           round_to_case_pct,
                                           round_to_layer_pct,
                                           round_to_pallet_pct,
                                           supp_hier_type_1,
                                           supp_hier_lvl_1,
                                           supp_hier_type_2,
                                           supp_hier_lvl_2,
                                           supp_hier_type_3,
                                           supp_hier_lvl_3,
                                           pickup_lead_time,
                                           last_update_datetime,
                                           last_update_id,
                                           create_datetime,
                                           negotiated_item_cost,
                                           extended_base_cost,
                                           inclusive_cost,
                                           base_cost)
                                    select it.item,
                                           s.supplier,
                                           s.origin_country_id,
                                           s.loc,
                                           s.loc_type,
                                           s.primary_loc_ind,
                                           s.unit_cost,
                                           s.round_lvl,
                                           s.round_to_inner_pct,
                                           s.round_to_case_pct,
                                           s.round_to_layer_pct,
                                           s.round_to_pallet_pct,
                                           s.supp_hier_type_1,
                                           s.supp_hier_lvl_1,
                                           s.supp_hier_type_2,
                                           s.supp_hier_lvl_2,
                                           s.supp_hier_type_3,
                                           s.supp_hier_lvl_3,
                                           s.pickup_lead_time,
                                           sysdate,
                                           L_user,
                                           sysdate,
                                           s.negotiated_item_cost,
                                           s.extended_base_cost,
                                           s.inclusive_cost,
                                           s.base_cost
                                      from item_supp_country_loc s,
                                           item_temp it,
                                           partner p
                                     where s.item                     = it.like_existing_item
                                       and s.loc                      = p.partner_id
                                       and p.partner_type             = 'E'
                                       and s.loc_type                 = 'E';
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC_CFA_EXT',NULL);
         insert into item_supp_country_loc_cfa_ext(item,
                                                   supplier,
                                                   origin_country_id,
                                                   loc,
                                                   group_id,
                                                   varchar2_1,
                                                   varchar2_2,
                                                   varchar2_3,
                                                   varchar2_4,
                                                   varchar2_5,
                                                   varchar2_6,
                                                   varchar2_7,
                                                   varchar2_8,
                                                   varchar2_9,
                                                   varchar2_10,
                                                   number_11,
                                                   number_12,
                                                   number_13,
                                                   number_14,
                                                   number_15,
                                                   number_16,
                                                   number_17,
                                                   number_18,
                                                   number_19,
                                                   number_20,
                                                   date_21,
                                                   date_22,
                                                   date_23,
                                                   date_24,
                                                   date_25)
                                            select it.item,
                                                   isce.supplier,
                                                   isce.origin_country_id,
                                                   isce.loc,
                                                   isce.group_id,
                                                   isce.varchar2_1,
                                                   isce.varchar2_2,
                                                   isce.varchar2_3,
                                                   isce.varchar2_4,
                                                   isce.varchar2_5,
                                                   isce.varchar2_6,
                                                   isce.varchar2_7,
                                                   isce.varchar2_8,
                                                   isce.varchar2_9,
                                                   isce.varchar2_10,
                                                   isce.number_11,
                                                   isce.number_12,
                                                   isce.number_13,
                                                   isce.number_14,
                                                   isce.number_15,
                                                   isce.number_16,
                                                   isce.number_17,
                                                   isce.number_18,
                                                   isce.number_19,
                                                   isce.number_20,
                                                   isce.date_21,
                                                   isce.date_22,
                                                   isce.date_23,
                                                   isce.date_24,
                                                   isce.date_25
                                              from item_supp_country_loc_cfa_ext isce,
                                                   item_temp it,
                                                   partner p
                                              where it.like_existing_item        = isce.item
                                              and isce.loc                     = p.partner_id
                                              and p.partner_type                 = 'E';
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'ITEM_SUPP_COUNTRY_BRACKET_COST',
                          NULL);
         insert into item_supp_country_bracket_cost(item,
                                                    supplier,
                                                    origin_country_id,
                                                    location,
                                                    bracket_value1,
                                                    loc_type,
                                                    default_bracket_ind,
                                                    unit_cost,
                                                    bracket_value2,
                                                    sup_dept_seq_no)
                                             select it.item,
                                                    s.supplier,
                                                    s.origin_country_id,
                                                    s.location,
                                                    s.bracket_value1,
                                                    s.loc_type,
                                                    s.default_bracket_ind,
                                                    s.unit_cost,
                                                    s.bracket_value2,
                                                    s.sup_dept_seq_no
                                               from item_supp_country_bracket_cost s,
                                                    item_temp it,
                                                    partner p
                                              where s.item                    = it.like_existing_item
                                                and TO_CHAR(s.location)       = p.partner_id
                                                and p.partner_type            = 'E'
                                                and s.loc_type                = 'E';
      end if;  -- if supplier_ind = 'Y'

      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'ITEM_LOC',
                       NULL);
      insert into item_loc(item,
                           loc,
                           item_parent,
                           item_grandparent,
                           loc_type,
                           unit_retail,
                           clear_ind,
                           taxable_ind,
                           local_item_desc,
                           local_short_desc,
                           ti,
                           hi,
                           store_ord_mult,
                           status,
                           status_update_date,
                           daily_waste_pct,
                           meas_of_each,
                           meas_of_price,
                           uom_of_price,
                           primary_variant,
                           primary_supp,
                           primary_cntry,
                           selling_unit_retail,
                           selling_uom,
                           last_update_datetime,
                           last_update_id,
                           create_datetime,
                           inbound_handling_days,
                           store_price_ind,
                           source_method,
                           source_wh,
                           rpm_ind,
                           multi_units,
                           multi_unit_retail,
                           multi_selling_uom,
                           regular_unit_retail,
                           uin_Type,
                           uin_label,
                           capture_time,
                           ext_uin_ind,
                           ranged_ind,
                           costing_loc,
                           costing_loc_type)
                    select it.item,
                           il.loc,
                           im.item_parent,
                           im.item_grandparent,
                           'E',
                           DECODE(I_price_ind, 'Y', il.unit_retail, NULL),
                           'N',
                           il.taxable_ind,
                           im.item_desc,
                           im.short_desc,
                           il.ti,
                           il.hi,
                           il.store_ord_mult,
                           il.status,
                           L_vdate,
                           il.daily_waste_pct,
                           il.meas_of_each,
                           il.meas_of_price,
                           il.uom_of_price,
                           NULL,  -- primary variant
                           DECODE(I_supplier_ind,'Y', il.primary_supp, NULL),
                           DECODE(I_supplier_ind, 'Y', il.primary_cntry, NULL),
                           il.selling_unit_retail,
                           il.selling_uom,
                           sysdate,
                           L_user,
                           sysdate,
                           il.inbound_handling_days,
                           'N',
                           il.source_method,
                           il.source_wh,
                           'N',
                           il.multi_units,
                           il.multi_unit_retail,
                           il.multi_selling_uom,
                           il.regular_unit_retail,
                           il.uin_type,
                           il.uin_label,
                           il.capture_time,
                           il.ext_uin_ind,
                           il.ranged_ind,
                           il.costing_loc,
                           il.costing_loc_type
                      from item_loc il,
                           item_temp it,
                           item_master im,
                           partner p
                     where il.item         = it.like_existing_item
                       and it.item         = im.item
                       and TO_CHAR(il.loc) = p.partner_id
                       and p.partner_type  = 'E'
                       and il.loc_type     = 'E';
      ---
	        SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'ITEM_LOC_L10N_EXT',
                       NULL);
      insert into item_loc_l10n_ext(item,
                                    location,
                                    l10n_country_id,
                                    group_id,
                                    varchar2_1,
                                    varchar2_2,
                                    varchar2_3,
                                    varchar2_4,
                                    varchar2_5,
                                    varchar2_6,
                                    varchar2_7,
                                    varchar2_8,
                                    varchar2_9,
                                    varchar2_10,
                                    number_11,
                                    number_12,
                                    number_13,
                                    number_14,
                                    number_15,
                                    number_16,
                                    number_17,
                                    number_18,
                                    number_19,
                                    number_20,
                                    date_21,
                                    date_22)
                             select it.item,
                                    ile.location,
                                    ile.l10n_country_id,
                                    ile.group_id,
                                    ile.varchar2_1,
                                    ile.varchar2_2,
                                    ile.varchar2_3,
                                    ile.varchar2_4,
                                    ile.varchar2_5,
                                    ile.varchar2_6,
                                    ile.varchar2_7,
                                    ile.varchar2_8,
                                    ile.varchar2_9,
                                    ile.varchar2_10,
                                    ile.number_11,
                                    ile.number_12,
                                    ile.number_13,
                                    ile.number_14,
                                    ile.number_15,
                                    ile.number_16,
                                    ile.number_17,
                                    ile.number_18,
                                    ile.number_19,
                                    ile.number_20,
                                    ile.date_21,
                                    ile.date_22
                               from item_loc_l10n_ext ile,
                                    item_temp it,
                                    partner p
                              where ile.item = it.like_existing_item
                                and ile.location = p.partner_id
                                and p.partner_type = 'E';
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_CFA_EXT',NULL);
      insert into item_loc_cfa_ext(item,
                                   loc,
                                   group_id,
                                   varchar2_1,
                                   varchar2_2,
                                   varchar2_3,
                                   varchar2_4,
                                   varchar2_5,
                                   varchar2_6,
                                   varchar2_7,
                                   varchar2_8,
                                   varchar2_9,
                                   varchar2_10,
                                   number_11,
                                   number_12,
                                   number_13,
                                   number_14,
                                   number_15,
                                   number_16,
                                   number_17,
                                   number_18,
                                   number_19,
                                   number_20,
                                   date_21,
                                   date_22,
                                   date_23,
                                   date_24,
                                   date_25)
                            select it.item,
                                   ilce.loc,
                                   ilce.group_id,
                                   ilce.varchar2_1,
                                   ilce.varchar2_2,
                                   ilce.varchar2_3,
                                   ilce.varchar2_4,
                                   ilce.varchar2_5,
                                   ilce.varchar2_6,
                                   ilce.varchar2_7,
                                   ilce.varchar2_8,
                                   ilce.varchar2_9,
                                   ilce.varchar2_10,
                                   ilce.number_11,
                                   ilce.number_12,
                                   ilce.number_13,
                                   ilce.number_14,
                                   ilce.number_15,
                                   ilce.number_16,
                                   ilce.number_17,
                                   ilce.number_18,
                                   ilce.number_19,
                                   ilce.number_20,
                                   ilce.date_21,
                                   ilce.date_22,
                                   ilce.date_23,
                                   ilce.date_24,
                                   ilce.date_25
                              from item_temp it,
                                   item_loc_cfa_ext ilce,
                                   partner p
                             where it.like_existing_item=ilce.item
                             and TO_CHAR(ilce.loc) = p.partner_id
                             and p.partner_type  = 'E';
     ---
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'ITEM_LOC_SOH',
                        NULL);
      insert into item_loc_soh(item,
                               item_parent,
                               item_grandparent,
                               loc,
                               loc_type,
                               av_cost,
                               unit_cost,
                               stock_on_hand,
                               soh_update_datetime,
                               last_hist_export_date,
                               in_transit_qty,
                               pack_comp_intran,
                               pack_comp_soh,
                               tsf_reserved_qty,
                               pack_comp_resv,
                               tsf_expected_qty,
                               pack_comp_exp,
                               rtv_qty,
                               non_sellable_qty,
                               customer_resv,
                               customer_backorder,
                               pack_comp_cust_resv,
                               pack_comp_cust_back,
                               pack_comp_non_sellable,
                               create_datetime,
                               last_update_datetime,
                               last_update_id,
                               primary_supp,
                               primary_cntry,
                               average_weight)
                        select inner.item, 
                               inner.item_parent, 
                               inner.item_grandparent, 
                               inner.loc, 
                               inner.loc_type, 
                               DECODE(I_supplier_ind, 'Y', DECODE(L_default_tax_type, 'GTAX', inner.extended_base_cost, inner.unit_cost), NULL), 
                               DECODE(I_supplier_ind, 'Y', inner.unit_cost, NULL),
                               0,
                               NULL,
                               NULL,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               sysdate,
                               sysdate,
                               L_user,
                               DECODE(I_supplier_ind,'Y', inner.primary_supp, NULL),
                               DECODE(I_supplier_ind, 'Y', inner.primary_cntry, NULL),
                               inner.average_weight
                          from (select it.item, 
                                              il.item_parent, 
                                              il.item_grandparent, 
                                              il.loc, 
                                              il.loc_type, 
                                              il.primary_supp, 
                                              il.primary_cntry, 
                                              ils.unit_cost, 
                                              ils.average_weight, 
                                              iscl.extended_base_cost * mvc.exchange_rate extended_base_cost, 
                                              mvc.effective_date, 
                                              rank() over 
                                                 (PARTITION BY it.item, 
                                                               il.item_parent, 
                                                               il.item_grandparent, 
                                                               il.loc, 
                                                               il.loc_type, 
                                                               il.primary_supp, 
                                                               il.primary_cntry, 
                                                               ils.unit_cost, 
                                                               ils.average_weight, 
                                                               iscl.extended_base_cost 
                                                      ORDER BY mvc.effective_date DESC) date_rank 
                                  from item_loc il,
                                       item_loc_soh ils,
                                       item_temp it,
                                       item_master im,
                                       item_supp_country_loc iscl,
                                       partner p,
                                       sups sup, 
                                       mv_currency_conversion_rates mvc 
                                 where il.item                = it.item
                                   and it.item                = im.item
                                   and im.item_level          = im.tran_level
                                   and p.partner_id           = il.loc
                                   and p.partner_type         = 'E'
                                   and il.loc_type            = 'E'
                                   and ils.item               = it.like_existing_item
                                   and ils.loc                = il.loc
                                   and iscl.item              = it.like_existing_item
                                   and iscl.loc               = ils.loc
                                   and iscl.supplier          = NVL(L_supplier, il.primary_supp)
                                   and iscl.origin_country_id = NVL(L_origin_country_id, il.primary_cntry) 
                                                  and iscl.supplier          = sup.supplier 
                                                  and mvc.from_currency      = sup.currency_code 
                                                  and mvc.to_currency        = p.currency_code 
                                                  and mvc.exchange_type      = 'C' 
                                                  and mvc.effective_date    <= L_vdate) inner 
                     where inner.date_rank = 1; 

      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'ITEM_LOC_TRAITS',
                        NULL);
      insert into item_loc_traits(item,
                                  loc,
                                  launch_date,
                                  qty_key_options,
                                  manual_price_entry,
                                  deposit_code,
                                  food_stamp_ind,
                                  wic_ind,
                                  proportional_tare_pct,
                                  fixed_tare_value,
                                  fixed_tare_uom,
                                  reward_eligible_ind,
                                  natl_brand_comp_item,
                                  return_policy,
                                  stop_sale_ind,
                                  elect_mtk_clubs,
                                  report_code,
                                  req_shelf_life_on_selection,
                                  req_shelf_life_on_receipt,
                                  ib_shelf_life,
                                  store_reorderable_ind,
                                  rack_size,
                                  full_pallet_item,
                                  in_store_market_basket,
                                  storage_location,
                                  alt_storage_location,
                                  returnable_ind,
                                  refundable_ind,
                                  back_order_ind,
                                  create_datetime,
                                  last_update_id,
                                  last_update_datetime )
                           select it.item,
                                  ilt.loc,
                                  ilt.launch_date,
                                  ilt.qty_key_options,
                                  ilt.manual_price_entry,
                                  ilt.deposit_code,
                                  ilt.food_stamp_ind,
                                  ilt.wic_ind,
                                  ilt.proportional_tare_pct,
                                  ilt.fixed_tare_value,
                                  ilt.fixed_tare_uom,
                                  ilt.reward_eligible_ind,
                                  ilt.natl_brand_comp_item,
                                  ilt.return_policy,
                                  ilt.stop_sale_ind,
                                  ilt.elect_mtk_clubs,
                                  ilt.report_code,
                                  ilt.req_shelf_life_on_selection,
                                  ilt.req_shelf_life_on_receipt,
                                  ilt.ib_shelf_life,
                                  ilt.store_reorderable_ind,
                                  ilt.rack_size,
                                  ilt.full_pallet_item,
                                  ilt.in_store_market_basket,
                                  ilt.storage_location,
                                  ilt.alt_storage_location,
                                  ilt.returnable_ind,
                                  ilt.refundable_ind,
                                  ilt.back_order_ind,
                                  sysdate,
                                  L_user,
                                  sysdate
                             from item_loc_traits ilt,
                                  item_temp it,
                                  partner p
                            where TO_CHAR(ilt.loc)      = p.partner_id
                              and p.partner_type        = 'E'
                              and it.like_existing_item = ilt.item;
   end if;

   ---
   if I_repl_ind = 'Y' then
      SQL_LIB.SET_MARK('INSERT',NULL,'REPL_ITEM_LOC',NULL);
      --- NULL fields will be updated in the replenishment attribute update process
      insert into repl_item_loc(item,
                                location,
                                loc_type,
                                item_parent,
                                item_grandparent,
                                primary_repl_supplier,
                                origin_country_id,
                                review_cycle,
                                stock_cat,
                                repl_order_ctrl,
                                source_wh,
                                activate_date,
                                deactivate_date,
                                pres_stock,
                                demo_stock,
                                repl_method,
                                min_stock,
                                max_stock,
                                incr_pct,
                                min_supply_days,
                                max_supply_days,
                                time_supply_horizon,
                                inv_selling_days,
                                service_level,
                                lost_sales_factor,
                                reject_store_ord_ind,
                                non_scaling_ind,
                                max_scale_value,
                                pickup_lead_time,
                                wh_lead_time,
                                terminal_stock_qty,
                                season_id,
                                phase_id,
                                dept,
                                class,
                                subclass,
                                store_ord_mult,
                                unit_cost,
                                supp_lead_time,
                                inner_pack_size,
                                supp_pack_size,
                                ti,
                                hi,
                                round_lvl,
                                round_to_inner_pct,
                                round_to_case_pct,
                                round_to_layer_pct,
                                round_to_pallet_pct,
                                primary_pack_no,
                                primary_pack_qty,
                                unit_tolerance,
                                pct_tolerance,
                                item_season_seq_no,
                                use_tolerance_ind,
                                create_datetime,
                                last_update_datetime,
                                last_update_id,
                                service_level_type,
                                mult_runs_per_day_ind,
                                tsf_zero_soh_ind,
                                add_lead_time_ind)
                         select it.item,
                                r.location,
                                r.loc_type,
                                im.item_parent,
                                im.item_grandparent,
                                r.primary_repl_supplier,
                                r.origin_country_id,
                                r.review_cycle,
                                r.stock_cat,
                                r.repl_order_ctrl,
                                r.source_wh,
                                r.activate_date,
                                r.deactivate_date,
                                r.pres_stock,
                                r.demo_stock,
                                r.repl_method,
                                r.min_stock,
                                r.max_stock,
                                r.incr_pct,
                                r.min_supply_days,
                                r.max_supply_days,
                                r.time_supply_horizon,
                                r.inv_selling_days,
                                r.service_level,
                                r.lost_sales_factor,
                                r.reject_store_ord_ind,
                                r.non_scaling_ind,
                                r.max_scale_value,
                                r.pickup_lead_time,
                                r.wh_lead_time,
                                r.terminal_stock_qty,
                                r.season_id,
                                r.phase_id,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                r.unit_tolerance,
                                r.pct_tolerance,
                                r.item_season_seq_no,
                                r.use_tolerance_ind,
                                sysdate,
                                sysdate,
                                L_user,
                                r.service_level_type,
                                r.mult_runs_per_day_ind,
                                r.tsf_zero_soh_ind,
                                r.add_lead_time_ind
                           from repl_item_loc r,
                                item_temp it,
                                item_master im,
                                item_loc il
                          where r.item     = it.like_existing_item
                            and im.item    = it.item
                            and r.location = il.loc
                            and r.loc_type = il.loc_type
                            and il.item    = it.item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'REPL_DAY',NULL);
      insert into repl_day(item,
                           location,
                           weekday,
                           loc_type)
                    select it.item,
                           r.location,
                           r.weekday,
                           r.loc_type
                      from repl_day r,
                           item_temp it,
                           item_loc il
                     where r.item     = it.like_existing_item
                       and r.location = il.loc
                       and r.loc_type = il.loc_type
                       and il.item    = it.item;

      SQL_LIB.SET_MARK('INSERT',NULL,'MASTER_REPL_ATTR',NULL);
      insert into master_repl_attr(item,
                                   location,
                                   loc_type,
                                   item_parent,
                                   item_grandparent,
                                   primary_repl_supplier,
                                   origin_country_id,
                                   review_cycle,
                                   stock_cat,
                                   repl_order_ctrl,
                                   source_wh,
                                   pres_stock,
                                   demo_stock,
                                   repl_method,
                                   min_stock,
                                   max_stock,
                                   incr_pct,
                                   min_supply_days,
                                   max_supply_days,
                                   time_supply_horizon,
                                   inv_selling_days,
                                   service_level,
                                   lost_sales_factor,
                                   reject_store_ord_ind,
                                   non_scaling_ind,
                                   max_scale_value,
                                   pickup_lead_time,
                                   wh_lead_time,
                                   terminal_stock_qty,
                                   season_id,
                                   phase_id,
                                   last_review_date,
                                   next_review_date,
                                   primary_pack_no,
                                   primary_pack_qty,
                                   unit_tolerance,
                                   pct_tolerance,
                                   item_season_seq_no,
                                   use_tolerance_ind,
                                   last_delivery_date,
                                   next_delivery_date,
                                   mbr_order_qty,
                                   adj_pickup_lead_time,
                                   adj_supp_lead_time,
                                   tsf_po_link_no,
                                   last_roq,
                                   status,
                                   dept,
                                   class,
                                   subclass,
                                   store_ord_mult,
                                   unit_cost,
                                   supp_lead_time,
                                   inner_pack_size,
                                   supp_pack_size,
                                   ti,
                                   hi,
                                   round_lvl,
                                   round_to_inner_pct,
                                   round_to_case_pct,
                                   round_to_layer_pct,
                                   round_to_pallet_pct,
                                   service_level_type,
                                   update_days_ind,
                                   monday_ind,
                                   tuesday_ind,
                                   wednesday_ind,
                                   thursday_ind,
                                   friday_ind,
                                   saturday_ind,
                                   sunday_ind,
                                   create_datetime,
                                   last_update_datetime,
                                   last_update_id,
                                   mult_runs_per_day_ind,
                                   tsf_zero_soh_ind)
                            select it.item,
                                   mr.location,
                                   mr.loc_type,
                                   im.item_parent,
                                   im.item_grandparent,
                                   mr.primary_repl_supplier,
                                   mr.origin_country_id,
                                   mr.review_cycle,
                                   mr.stock_cat,
                                   mr.repl_order_ctrl,
                                   mr.source_wh,
                                   mr.pres_stock,
                                   mr.demo_stock,
                                   mr.repl_method,
                                   mr.min_stock,
                                   mr.max_stock,
                                   mr.incr_pct,
                                   mr.min_supply_days,
                                   mr.max_supply_days,
                                   mr.time_supply_horizon,
                                   mr.inv_selling_days,
                                   mr.service_level,
                                   mr.lost_sales_factor,
                                   mr.reject_store_ord_ind,
                                   mr.non_scaling_ind,
                                   mr.max_scale_value,
                                   mr.pickup_lead_time,
                                   mr.wh_lead_time,
                                   mr.terminal_stock_qty,
                                   mr.season_id,
                                   mr.phase_id,
                                   mr.last_review_date,
                                   mr.next_review_date,
                                   mr.primary_pack_no,
                                   mr.primary_pack_qty,
                                   mr.unit_tolerance,
                                   mr.pct_tolerance,
                                   mr.item_season_seq_no,
                                   mr.use_tolerance_ind,
                                   mr.last_delivery_date,
                                   mr.next_delivery_date,
                                   mr.mbr_order_qty,
                                   mr.adj_pickup_lead_time,
                                   mr.adj_supp_lead_time,
                                   mr.tsf_po_link_no,
                                   mr.last_roq,
                                   mr.status,
                                   mr.dept,
                                   mr.class,
                                   mr.subclass,
                                   mr.store_ord_mult,
                                   mr.unit_cost,
                                   mr.supp_lead_time,
                                   mr.inner_pack_size,
                                   mr.supp_pack_size,
                                   mr.ti,
                                   mr.hi,
                                   mr.round_lvl,
                                   mr.round_to_inner_pct,
                                   mr.round_to_case_pct,
                                   mr.round_to_layer_pct,
                                   mr.round_to_pallet_pct,
                                   mr.service_level_type,
                                   mr.update_days_ind,
                                   mr.monday_ind,
                                   mr.tuesday_ind,
                                   mr.wednesday_ind,
                                   mr.thursday_ind,
                                   mr.friday_ind,
                                   mr.saturday_ind,
                                   mr.sunday_ind,
                                   sysdate,
                                   sysdate,
                                   L_user,
                                   mr.mult_runs_per_day_ind,
                                   mr.tsf_zero_soh_ind
            from item_master im,
                 master_repl_attr mr,
                 item_temp it,
                 item_loc il
            where im.item     = it.item
            and   mr.item     = it.like_existing_item
            and   mr.location = il.loc
            and   mr.loc_type = il.loc_type
            and   il.item     = it.item;
   end if;
   ---
   if I_seasons_ind = 'Y' then
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SEASONS',NULL);
      insert into item_seasons(item,
                               season_id,
                               phase_id,
                               item_season_seq_no,
                               diff_id,
                               create_datetime,
                               last_update_datetime,
                               last_update_id)
                        select it.item,
                               s.season_id,
                               s.phase_id,
                               s.item_season_seq_no,
                               s.diff_id,
                               sysdate,
                               sysdate,
                               L_user
                          from item_seasons s,
                               item_temp it
                         where s.item     = it.like_existing_item;
   end if;
   ---
   if I_repl_ind = 'Y' then
      insert into repl_item_loc_updates(item,
                                        supplier,
                                        origin_country_id,
                                        location,
                                        loc_type,
                                        change_type)
                                 select it.item,
                                        NULL,
                                        NULL,
                                        -1,
                                        NULL,
                                        'LKITEM'
                                   from item_temp it;
   end if;
   ---
   if I_ticket_ind = 'Y' then
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_TICKET',NULL);
      insert into item_ticket(item,
                              ticket_type_id,
                              po_print_type,
                              print_on_pc_ind,
                              ticket_over_pct,
                              create_datetime,
                              last_update_datetime,
                              last_update_id)
                       select it.item,
                              i.ticket_type_id,
                              i.po_print_type,
                              i.print_on_pc_ind,
                              i.ticket_over_pct,
                              sysdate,
                              sysdate,
                              L_user
                         from item_ticket i,
                              item_temp it
                        where i.item = it.like_existing_item;
   end if;
   ---
   if I_uda_ind = 'Y' then
      SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_LOV',NULL);
      insert into uda_item_lov(item,
                               uda_id,
                               uda_value,
                               create_datetime,
                               last_update_datetime,
                               last_update_id)
                        select it.item,
                               u.uda_id,
                               u.uda_value,
                               sysdate,
                               sysdate,
                               L_user
                          from uda_item_lov u,
                               item_temp it
                         where u.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_FF',NULL);
      insert into uda_item_ff(item,
                              uda_id,
                              uda_text,
                              create_datetime,
                              last_update_datetime,
                              last_update_id)
                       select it.item,
                              u.uda_id,
                              u.uda_text,
                              sysdate,
                              sysdate,
                              L_user
                         from uda_item_ff u,
                              item_temp it
                        where u.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_DATE',NULL);
      insert into uda_item_date(item,
                                uda_id,
                                uda_date,
                                create_datetime,
                                last_update_datetime,
                                last_update_id)
                         select it.item,
                                u.uda_id,
                                U.Uda_Date,
                                u.create_datetime,
                                last_update_datetime,
                                last_update_id
                           from uda_item_date u,
                                item_temp it
                          where u.item = it.like_existing_item;
   else
      -- Determine if there are required UDAs for the hierarchy.
      -- If there aren't any, set item_master.check_uda_ind = Y
      -- so the user won't be prompted to enter them when opening
      -- the itemuda form.
      if L_dept is NULL or
         L_class is NULL or
         L_subclass is NULL then
         if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                           I_new_item,
                                           L_dept,
                                           L_class,
                                           L_subclass) = FALSE then
            return FALSE;
         end if;
      end if;
      if UDA_SQL.CHECK_REQD_NO_VALUE( O_error_message,
                                      L_reqd_no_value,
                                      I_new_item,
                                      L_dept,
                                      L_class,
                                      L_subclass) = FALSE then
         return FALSE;
      end if;
      if L_reqd_no_value = 'N' then
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_MASTER',NULL);
         update item_master
            set check_uda_ind = 'Y'
          where item = I_new_item;
      end if;

   end if;
   ---
   if I_hts_ind = 'Y' and L_import_ind = 'Y' then
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_HTS',NULL);
      insert into item_hts(item,
                           hts,
                           import_country_id,
                           origin_country_id,
                           effect_from,
                           effect_to,
                           clearing_zone_id,
                           status,
                           create_datetime,
                           last_update_datetime,
                           last_update_id)
                    select it.item,
                           ih.hts,
                           ih.import_country_id,
                           ih.origin_country_id,
                           ih.effect_from,
                           ih.effect_to,
                           clearing_zone_id,
                           status,
                           sysdate,
                           sysdate,
                           L_user
                      from item_hts ih,
                           item_temp it
                     where ih.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_HTS_ASSESS',NULL);
      insert into item_hts_assess(item,
                                  hts,
                                  import_country_id,
                                  origin_country_id,
                                  effect_from,
                                  effect_to,
                                  comp_id,
                                  cvb_code,
                                  comp_rate,
                                  per_count,
                                  per_count_uom,
                                  est_assess_value,
                                  nom_flag_1,
                                  nom_flag_2,
                                  nom_flag_3,
                                  nom_flag_4,
                                  nom_flag_5,
                                  display_order,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
                           select it.item,
                                  ih.hts,
                                  ih.import_country_id,
                                  ih.origin_country_id,
                                  ih.effect_from,
                                  ih.effect_to,
                                  ih.comp_id,
                                  ih.cvb_code,
                                  ih.comp_rate,
                                  ih.per_count,
                                  ih.per_count_uom,
                                  ih.est_assess_value,
                                  ih.nom_flag_1,
                                  ih.nom_flag_2,
                                  ih.nom_flag_3,
                                  ih.nom_flag_4,
                                  ih.nom_flag_5,
                                  ih.display_order,
                                  sysdate,
                                  sysdate,
                                  L_user
                             from item_hts_assess ih,
                                  item_temp it
                            where ih.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'COND_TARIFF_TREATMENT',NULL);
      insert into cond_tariff_treatment(item,
                                        tariff_treatment,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id)
                                 select it.item,
                                        c.tariff_treatment,
                                        sysdate,
                                        sysdate,
                                        L_user
                                   from cond_tariff_treatment c,
                                        item_temp it
                                  where c.item = it.like_existing_item;
   end if;
   ---
   if L_elc_ind = 'Y' then
      ---
      -- Insert Item Up Charges
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_CHRG_HEAD',NULL);
      insert into item_chrg_head(item,
                                 from_loc,
                                 to_loc,
                                 from_loc_type,
                                 to_loc_type)
                          select it.item,
                                 c.from_loc,
                                 c.to_loc,
                                 c.from_loc_type,
                                 c.to_loc_type
                            from item_chrg_head c,
                                 item_temp it
                           where c.item = it.like_existing_item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_CHRG_DETAIL',NULL);
      insert into item_chrg_detail(item,
                                   from_loc,
                                   to_loc,
                                   comp_id,
                                   from_loc_type,
                                   to_loc_type,
                                   comp_rate,
                                   per_count,
                                   per_count_uom,
                                   up_chrg_group,
                                   comp_currency,
                                   display_order)
                            select it.item,
                                   c.from_loc,
                                   c.to_loc,
                                   c.comp_id,
                                   c.from_loc_type,
                                   c.to_loc_type,
                                   c.comp_rate,
                                   c.per_count,
                                   c.per_count_uom,
                                   c.up_chrg_group,
                                   c.comp_currency,
                                   c.display_order
                              from item_chrg_detail c,
                                   item_temp it
                             where c.item = it.like_existing_item;
   end if;
   ---
   for rec in C_COPY_RELATED_ITEM loop
      if RELATED_ITEM_SQL.COPY_LIKE_ITEM(o_error_message,
                                         rec.item,
                                         rec.like_existing_item) = FALSE then
         return FALSE;
      end if;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_MASTER_DETAIL_INSERT;
-------------------------------------------------------------------------------
FUNCTION CHECK_DUPL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_flag          IN OUT VARCHAR2,
                    I_item          IN     ITEM_MASTER.ITEM%TYPE,
                    I_diff_1        IN     ITEM_MASTER.DIFF_1%TYPE,
                    I_diff_2        IN     ITEM_MASTER.DIFF_2%TYPE,
                    I_diff_3        IN     ITEM_MASTER.DIFF_3%TYPE,
                    I_diff_4        IN     ITEM_MASTER.DIFF_4%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program        VARCHAR2(50)          := 'LIKE_ITEM_SQL.CHECK_DUPL';
   L_exists         VARCHAR2(1)           := 'N';
   L_existing_item  ITEM_MASTER.ITEM%TYPE;
   ---
   cursor C_GET_CHILDREN is
      select im1.item
        from item_master im1
       where im1.item_parent in (select im2.item_parent
                                   from item_master im2
                                  where item = I_item);
   ---
   cursor C_CHECK_DUPL is
      select 'Y'
        from item_master
       where item   = L_existing_item
         and I_diff_4 is not NULL
         and diff_1 = I_diff_1
         and diff_2 = I_diff_2
         and diff_3 = I_diff_3
         and diff_4 = I_diff_4
   union all
      select 'Y'
        from item_master
       where item   = L_existing_item
         and I_diff_4 is NULL
         and I_diff_3 is not NULL
         and diff_1 = I_diff_1
         and diff_2 = I_diff_2
         and diff_3 = I_diff_3
   union all
      select 'Y'
        from item_master
       where item   = L_existing_item
         and I_diff_3 is NULL
         and I_diff_2 is not NULL
         and diff_1 = I_diff_1
         and diff_2 = I_diff_2
   union all
      select 'Y'
        from item_master
       where item   = L_existing_item
         and I_diff_2 is NULL
         and I_diff_1 is not NULL
         and diff_1 = I_diff_1;

BEGIN
   for rec in C_GET_CHILDREN loop
      L_existing_item := rec.item;
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_DUPL','ITEM_MASTER','ITEM: '||L_existing_item);
      open C_CHECK_DUPL;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_DUPL','ITEM_MASTER','ITEM: '||L_existing_item);
      fetch C_CHECK_DUPL into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_DUPL','ITEM_MASTER','ITEM: '||L_existing_item);
      close C_CHECK_DUPL;
      ---
      if L_exists = 'Y' then
         O_flag := 'Y';
         return TRUE;
      end if;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_DUPL;
-------------------------------------------------------------------------------
FUNCTION SET_RPM_ITEM_ZONE_PRICE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program        VARCHAR2(50)          := 'LIKE_ITEM_SQL.SET_RPM_ITEM_ZONE_PRICE';
   ---
   L_obj_item_pricing_table  OBJ_ITEM_PRICING_TBL;
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_success                 VARCHAR2(1);
   L_dept                    ITEM_MASTER.DEPT%TYPE;
   L_class                   ITEM_MASTER.CLASS%TYPE;
   L_subclass                ITEM_MASTER.SUBCLASS%TYPE;
   L_existing_item           ITEM_MASTER.ITEM%TYPE;
   L_new_item                ITEM_MASTER.ITEM%TYPE;
   L_item_parent             ITEM_MASTER.ITEM%TYPE;
   L_cost_currency_code      CURRENCIES.CURRENCY_CODE%TYPE := 'USD';

   cursor C_ITEM_TEMP is
   select im.item_parent,
          im.dept,
          im.class,
          im.subclass,
          it.like_existing_item,
          it.item
     from item_master im,
          item_temp it
    where im.item = it.like_existing_item;

BEGIN

   open C_ITEM_TEMP;

   LOOP

      fetch C_ITEM_TEMP
       into L_item_parent,
            L_dept,
            L_class,
            L_subclass,
            L_existing_item,
            L_new_item;

      if C_ITEM_TEMP%NOTFOUND then
         EXIT;
      end if;

      if NOT PM_RETAIL_API_SQL.GET_RPM_PRICING_WRAPPER(L_error_message,
                                                       L_obj_item_pricing_table,
                                                       L_item_parent,
                                                       L_existing_item,
                                                       L_dept,
                                                       L_class,
                                                       L_subclass,
                                                       L_cost_currency_code,
                                                       0) then
         close C_ITEM_TEMP;
         O_error_message := L_error_message;
         return false;
      end if;

      for i in 1..L_obj_item_pricing_table.count loop
         L_obj_item_pricing_table(i).item := L_new_item;
      end loop;

      if NOT PM_RETAIL_API_SQL.SET_RPM_PRICING_WRAPPER(L_error_message,
                                                       L_obj_item_pricing_table) then
         close C_ITEM_TEMP;
         O_error_message := L_error_message;
         return false;
      end if;

   END LOOP;

   close C_ITEM_TEMP;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ITEM_TEMP%ISOPEN then
         close C_ITEM_TEMP;
      End If;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SET_RPM_ITEM_ZONE_PRICE;
-------------------------------------------------------------------------------
END LIKE_ITEM_SQL;
/