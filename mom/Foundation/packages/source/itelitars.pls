
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_TARIFF_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
-- Function Name : COPY_DOWN_PARENT_TARIFF
-- Purpose       : Remove old COND_TARIFF_TREATMENT values of all tran level
--               : or above children or grandchildren of the passed in item,
--               : get latest values for the item, and insert new COND_TARIFF_TREATMENT
--               : values for the valid children and grandchildren.
-----------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_TARIFF (O_error_message  IN OUT VARCHAR2,
                               I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
END ITEM_TARIFF_SQL;
/


