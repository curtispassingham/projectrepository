CREATE OR REPLACE PACKAGE BODY UDA_SQL AS
--------------------------------------------------------------------
FUNCTION NEXT_SEQUENCE_NO( O_error_message  IN OUT  VARCHAR2,
                           O_sequence_no    IN OUT  UDA_ITEM_DEFAULTS.SEQ_NO%TYPE)
RETURN BOOLEAN IS

BEGIN

   select uda_def_sequence.NEXTVAL
     into O_sequence_no
     from sys.dual;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.NEXT_SEQUENCE_NO',
                                             to_char(SQLCODE));
      RETURN FALSE;

END NEXT_SEQUENCE_NO;
--------------------------------------------------------------------
FUNCTION NEXT_UDA( O_error_message  IN OUT  VARCHAR2,
                   O_uda_id         IN OUT  UDA.UDA_ID%TYPE)
                   RETURN BOOLEAN IS

   L_wrap_sequence_number   UDA.UDA_ID%TYPE  := NULL;
   L_first_time             VARCHAR2(3)      := 'Yes';
   L_dummy                  VARCHAR2(1);


   cursor C_UDA_EXISTS is
       select  'x'
         from uda
        where uda_id = O_uda_id;

BEGIN
    LOOP
        SELECT uda_sequence.NEXTVAL
          INTO O_uda_id
          FROM sys.dual;

         if L_first_time = 'Yes' then
            L_wrap_sequence_number := O_uda_id;
            L_first_time := 'No';
         elsif O_uda_id = L_wrap_sequence_number then
            O_error_message := 'Fatal error - no available UDA numbers';
            RETURN FALSE;
         end if;

         open  C_UDA_EXISTS;
         fetch C_UDA_EXISTS into L_dummy;
         if C_UDA_EXISTS%NOTFOUND then
             close C_UDA_EXISTS;
             RETURN TRUE;
         end if;
         close C_UDA_EXISTS;
    END LOOP;

EXCEPTION
    WHEN OTHERS THEN
       O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                              SQLERRM,
                                              'UDA_SQL.NEXT_UDA',
                                              to_char(SQLCODE));
       RETURN FALSE;

END NEXT_UDA;

---------------------------------------------------------------------------------------------
FUNCTION GET_UDA_DESC( O_error_message  IN OUT  VARCHAR2,
                       O_uda_desc       IN OUT  UDA.UDA_DESC%TYPE,
                       I_uda_id         IN      UDA.UDA_ID%TYPE)
                       RETURN BOOLEAN IS

   cursor C_GET_UDA_DESC is
      select uda_desc
        from v_uda_tl
       where uda_id = I_uda_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_UDA_DESC',
                    'UDA_DESC',
                    'UDA ID: ' || to_char(I_uda_id));

   open  C_GET_UDA_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_UDA_DESC',
                    'UDA_DESC',
                    'UDA ID: ' || to_char(I_uda_id));

   fetch C_GET_UDA_DESC into O_uda_desc;
   if C_GET_UDA_DESC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_UDA_DESC',
                       'UDA_DESC',
                       'UDA ID: ' || to_char(I_uda_id));

      close C_GET_UDA_DESC;
      O_error_message := SQL_LIB.CREATE_MSG( 'UDA_ERROR',
                                             to_char(I_uda_id),
                                             NULL,
                                             NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_UDA_DESC',
                    'UDA_DESC',
                    'UDA ID: ' || to_char(I_uda_id));

   close C_GET_UDA_DESC;


   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.GET_UDA_DESC',
                                             to_char(SQLCODE));
      RETURN FALSE;

END GET_UDA_DESC;

---------------------------------------------------------------------------------------------
FUNCTION GET_VALUE_DESC( O_error_message   IN OUT  VARCHAR2,
                         O_uda_value_desc  IN OUT  UDA_VALUES.UDA_VALUE_DESC%TYPE,
                         I_uda_id          IN      UDA_VALUES.UDA_ID%TYPE,
                         I_uda_value       IN      UDA_VALUES.UDA_VALUE%TYPE)
                         RETURN BOOLEAN IS

   cursor C_GET_UDA_VALUE_DESC is
      select uda_value_desc
        from v_uda_values_tl
       where uda_id    = I_uda_id
         and uda_value = I_uda_value;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_UDA_VALUE_DESC',
                    'V_UDA_VALUES_TL',
                    'UDA ID: '      || to_char(I_uda_id) ||
                    ', UDA VALUE: ' || to_char(I_uda_value));

   open  C_GET_UDA_VALUE_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_UDA_VALUE_DESC',
                    'V_UDA_VALUES_TL',
                    'UDA ID: '      || to_char(I_uda_id) ||
                    ', UDA VALUE: ' || to_char(I_uda_value));

   fetch C_GET_UDA_VALUE_DESC into O_uda_value_desc;
   if C_GET_UDA_VALUE_DESC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_UDA_VALUE_DESC',
                       'V_UDA_VALUES_TL',
                       'UDA ID: '      || to_char(I_uda_id) ||
                       ', UDA VALUE: ' || to_char(I_uda_value));

      close C_GET_UDA_VALUE_DESC;

      O_error_message := SQL_LIB.CREATE_MSG( 'UDA_VALUE_ERROR',
                                             to_char(I_uda_value),
                                             NULL,
                                             NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_UDA_VALUE_DESC',
                    'UDA_VALUES',
                    'UDA ID: '      || to_char(I_uda_id) ||
                    ', UDA VALUE: ' || to_char(I_uda_value));

   close C_GET_UDA_VALUE_DESC;


   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.GET_VALUE_DESC',
                                             to_char(SQLCODE));
      RETURN FALSE;

END GET_VALUE_DESC;

---------------------------------------------------------------------------------------------
FUNCTION GET_SINGLE_VAL_IND( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_single_val_ind   IN OUT   UDA.SINGLE_VALUE_IND%TYPE,
                             I_uda_id           IN       UDA.UDA_ID%TYPE,
                             I_display_type     IN       UDA.DISPLAY_TYPE%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_SINGLE_VAL_IND is
      select single_value_ind
        from uda
       where uda_id       = I_uda_id
         and display_type = I_display_type;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SINGLE_VAL_IND',
                    'SINGLE_VALUE_IND',
                    'UDA ID: ' || TO_CHAR(I_uda_id));

   open  C_GET_SINGLE_VAL_IND;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SINGLE_VAL_IND',
                    'SINGLE_VALUE_IND',
                    'UDA ID: ' || TO_CHAR(I_uda_id));

   fetch C_GET_SINGLE_VAL_IND into O_single_val_ind;

   if O_single_val_ind is NULL then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SINGLE_VAL_IND',
                       'SINGLE_VALUE_IND',
                       'UDA ID: ' || TO_CHAR(I_uda_id));
      close C_GET_SINGLE_VAL_IND;

      O_error_message := SQL_LIB.CREATE_MSG( 'UDA_ERROR',
                                             TO_CHAR(I_uda_id),
                                             NULL,
                                             NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SINGLE_VAL_IND',
                    'SINGLE_VALUE_IND',
                    'UDA ID: ' || TO_CHAR(I_uda_id));

   close C_GET_SINGLE_VAL_IND;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.GET_SINGLE_VAL_IND',
                                             TO_CHAR(SQLCODE));
      RETURN FALSE;

END GET_SINGLE_VAL_IND;
---------------------------------------------------------------------------------------------
FUNCTION RELATION_EXISTS( O_error_message  IN OUT  VARCHAR2,
                          O_exists         IN OUT  BOOLEAN,
                          I_uda_id         IN      UDA_VALUES.UDA_ID%TYPE,
                          I_uda_value      IN      UDA_VALUES.UDA_VALUE%TYPE)
                          RETURN BOOLEAN IS

   L_display_type  UDA.DISPLAY_TYPE%TYPE  := NULL;
   L_dummy         VARCHAR2(3);

   cursor C_GET_DISPLAY_TYPE is
      select display_type
        from uda
       where uda_id = I_uda_id;

   cursor C_GET_UDA_LOV is
      select 'x'
        from uda_item_lov
       where uda_id = I_uda_id;

   cursor C_GET_UDA_FF is
      select 'x'
        from uda_item_ff
       where uda_id = I_uda_id;

   cursor C_GET_UDA_DATE is
      select 'x'
        from uda_item_date
       where uda_id = I_uda_id;

   cursor C_GET_UDA_TICKET_TYPE is
      select 'x'
        from ticket_type_detail
       where uda_id = I_uda_id;

   cursor C_GET_UDA_VALUE_LOV is
      select 'x'
        from uda_item_lov
       where uda_id    = I_uda_id
         and uda_value = I_uda_value;

BEGIN
   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DISPLAY_TYPE',
                    'UDA',
                    'UDA ID: ' || to_char(I_uda_id));

   open  C_GET_DISPLAY_TYPE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DISPLAY_TYPE',
                    'UDA',
                    'UDA ID: ' || to_char(I_uda_id));

   fetch C_GET_DISPLAY_TYPE into L_display_type;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DISPLAY_TYPE',
                    'UDA',
                    'UDA ID: ' || to_char(I_uda_id));

   close C_GET_DISPLAY_TYPE;

   if I_uda_value is NULL then
      if L_display_type = 'LV' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_UDA_LOV',
                          'UDA_ITEM_LOV',
                          'UDA ID: ' || to_char(I_uda_id));

         open  C_GET_UDA_LOV;

         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_UDA_LOV',
                          'UDA_ITEM_LOV',
                          'UDA ID: ' || to_char(I_uda_id));

         fetch C_GET_UDA_LOV into L_dummy;

         if C_GET_UDA_LOV%FOUND then
            O_exists := TRUE;
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_UDA_LOV',
                          'UDA_ITEM_LOV',
                          'UDA ID: ' || to_char(I_uda_id));

         close C_GET_UDA_LOV;
      elsif L_display_type = 'FF' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_UDA_FF',
                          'UDA_ITEM_FF',
                          'UDA ID: ' || to_char(I_uda_id));

         open  C_GET_UDA_FF;

         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_UDA_FF',
                          'UDA_ITEM_FF',
                          'UDA ID: ' || to_char(I_uda_id));

         fetch C_GET_UDA_FF into L_dummy;

         if C_GET_UDA_FF%FOUND then
            O_exists := TRUE;
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_UDA_FF',
                          'UDA_ITEM_FF',
                          'UDA ID: ' || to_char(I_uda_id));

         close C_GET_UDA_FF;
      else
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_UDA_DATE',
                          'UDA_ITEM_DATE',
                          'UDA ID: ' || to_char(I_uda_id));

         open  C_GET_UDA_DATE;

         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_UDA_DATE',
                          'UDA_ITEM_DATE',
                          'UDA ID: ' || to_char(I_uda_id));

         fetch C_GET_UDA_DATE into L_dummy;

         if C_GET_UDA_DATE%FOUND then
            O_exists := TRUE;
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_UDA_DATE',
                          'UDA_ITEM_DATE',
                          'UDA ID: ' || to_char(I_uda_id));

         close C_GET_UDA_DATE;
      end if;

      if O_exists = FALSE then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_UDA_TICKET_TYPE',
                          'TICKET_TYPE_DETAIL',
                          'UDA ID: ' || to_char(I_uda_id));

         open  C_GET_UDA_TICKET_TYPE;

         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_UDA_TICKET_TYPE',
                          'TICKET_TYPE_DETAIL',
                          'UDA ID: ' || to_char(I_uda_id));

         fetch C_GET_UDA_TICKET_TYPE into L_dummy;
         if C_GET_UDA_TICKET_TYPE%FOUND then
            O_exists := TRUE;
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_UDA_TICKET_TYPE',
                          'TICKET_TYPE_DETAIL',
                          'UDA ID: ' || to_char(I_uda_id));

         close C_GET_UDA_TICKET_TYPE;
      end if;
   else  -- uda_value is not NULL
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_UDA_VALUE_LOV',
                       'UDA_ITEM_LOV',
                       'UDA ID: '      || to_char(I_uda_id) ||
                       ', UDA VALUE: ' || to_char(I_uda_value));

      open  C_GET_UDA_VALUE_LOV;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_UDA_VALUE_LOV',
                       'UDA_ITEM_LOV',
                       'UDA ID: '      || to_char(I_uda_id) ||
                       ', UDA VALUE: ' || to_char(I_uda_value));

      fetch C_GET_UDA_VALUE_LOV into L_dummy;

      if C_GET_UDA_VALUE_LOV%FOUND then
         O_exists := TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_UDA_VALUE_LOV',
                       'UDA_ITEM_LOV',
                       'UDA ID: '      || to_char(I_uda_id) ||
                       ', UDA VALUE: ' || to_char(I_uda_value));

      close C_GET_UDA_VALUE_LOV;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.RELATION_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;

END RELATION_EXISTS;

---------------------------------------------------------------------------------------------
FUNCTION CHECK_SINGLE_UDA( O_error_message  IN OUT  VARCHAR2,
                           O_exists         IN OUT  BOOLEAN,
                           I_uda_id         IN      UDA.UDA_ID%TYPE,
                           I_seq_no         IN      UDA_ITEM_DEFAULTS.SEQ_NO%TYPE,
                           I_item           IN      UDA_ITEM_LOV.ITEM%TYPE,
                           I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                           I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                           I_subclass       IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                           RETURN BOOLEAN IS

   L_display_type  UDA.DISPLAY_TYPE%TYPE          := NULL;
   L_dummy         VARCHAR2(3);

   cursor C_GET_DISPLAY_TYPE is
      select display_type
        from uda
       where uda_id = I_uda_id;

   cursor C_CHECK_HIER_MULTIPLE_UDAS is
      select 'x'
        from uda_item_defaults
       where uda_id       =  I_uda_id
         and seq_no       != nvl(I_seq_no, -1)
         and required_ind =  'N'
         and dept         =  I_dept
         and ((I_class is NULL and class is NULL)
              or (class = I_class))
         and ((I_subclass is NULL and subclass is NULL)
              or (subclass = I_subclass));

   cursor C_CHECK_ITEM_LOV is
      select 'x'
        from uda_item_lov
       where uda_id = I_uda_id
         and item   = I_item;

   cursor C_CHECK_ITEM_FF is
      select 'x'
        from uda_item_ff
       where uda_id = I_uda_id
         and item   = I_item;

   cursor C_CHECK_ITEM_DATE is
      select 'x'
        from uda_item_date
       where uda_id = I_uda_id
         and item   = I_item;

BEGIN
   if I_item is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_HIER_MULTIPLE_UDAS',
                       'UDA_ITEM_DEFAULTS',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      open  C_CHECK_HIER_MULTIPLE_UDAS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_HIER_MULTIPLE_UDAS',
                       'UDA_ITEM_DEFAULTS',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      fetch C_CHECK_HIER_MULTIPLE_UDAS into L_dummy;

      if C_CHECK_HIER_MULTIPLE_UDAS%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_HIER_MULTIPLE_UDAS',
                       'UDA_ITEM_DEFAULTS',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      close C_CHECK_HIER_MULTIPLE_UDAS;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_DISPLAY_TYPE',
                       'UDA',
                       'UDA ID: ' || to_char(I_uda_id));

      open  C_GET_DISPLAY_TYPE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_DISPLAY_TYPE',
                       'UDA',
                       'UDA ID: ' || to_char(I_uda_id));

      fetch C_GET_DISPLAY_TYPE into L_display_type;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_DISPLAY_TYPE',
                       'UDA',
                       'UDA ID: ' || to_char(I_uda_id));

      close C_GET_DISPLAY_TYPE;

      if L_display_type = 'LV' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_ITEM_LOV',
                          'UDA_ITEM_LOV',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         open  C_CHECK_ITEM_LOV;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_ITEM_LOV',
                          'UDA_ITEM_LOV',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         fetch C_CHECK_ITEM_LOV into L_dummy;
         if C_CHECK_ITEM_LOV%FOUND then
            O_exists := TRUE;
         else
            O_exists := FALSE;
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_ITEM_LOV',
                          'UDA_ITEM_LOV',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         close C_CHECK_ITEM_LOV;
      elsif L_display_type = 'FF' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_ITEM_FF',
                          'UDA_ITEM_FF',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         open  C_CHECK_ITEM_FF;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_ITEM_FF',
                          'UDA_ITEM_FF',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         fetch C_CHECK_ITEM_FF into L_dummy;
         if C_CHECK_ITEM_FF%FOUND then
            O_exists := TRUE;
         else
            O_exists := FALSE;
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_ITEM_FF',
                          'UDA_ITEM_FF',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         close C_CHECK_ITEM_FF;
      else
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_ITEM_DATE',
                          'UDA_ITEM_DATE',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         open  C_CHECK_ITEM_DATE;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_ITEM_DATE',
                          'UDA_ITEM_DATE',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         fetch C_CHECK_ITEM_DATE into L_dummy;
         if C_CHECK_ITEM_DATE%FOUND then
            O_exists := TRUE;
         else
            O_exists := FALSE;
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_ITEM_DATE',
                          'UDA_ITEM_DATE',
                          'UDA ID: ' || to_char(I_uda_id) ||
                          'ITEM: '   || I_item);

         close C_CHECK_ITEM_DATE;
      end if;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.CHECK_SINGLE_UDA',
                                             to_char(SQLCODE));
      RETURN FALSE;

END CHECK_SINGLE_UDA;

---------------------------------------------------------------------------------------------
FUNCTION CHECK_DEFAULT_NO_VALUE( O_error_message     IN OUT  VARCHAR2,
                                 O_default_no_value  IN OUT  BOOLEAN,
                                 I_dept              IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                                 I_class             IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                                 I_subclass          IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                                 RETURN BOOLEAN IS

   L_dummy    VARCHAR2(3);

   cursor C_CHECK_DEFAULT_NO_UDA_VALUE is
      select 'x'
        from uda_item_defaults a
       where a.uda_value is NULL
         and a.dept                      = I_dept
         and nvl(a.class, I_class)       = I_class
         and nvl(a.subclass, I_subclass) = I_subclass
         and a.hierarchy_value = (select max(b.hierarchy_value)
                                    from uda_item_defaults b
                                   where b.dept                      = I_dept
                                     and nvl(b.class, I_class)       = I_class
                                     and nvl(b.subclass, I_subclass) = I_subclass
                                     and b.uda_id                    = a.uda_id);

BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG( 'MUST_ENTER_DEPT',
                                             NULL,
                                             NULL,
                                             NULL);

      RETURN FALSE;
   elsif I_class is NULL and
      I_subclass is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG( 'INV_SUBCLASS',
                                             NULL,
                                             NULL,
                                             NULL);

      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DEFAULT_NO_UDA_VALUE',
                    'UDA_ITEM_DEFAULTS',
                    'DEPT: '       || to_char(I_dept)   ||
                    ', CLASS: '    || to_char(I_class)  ||
                    ', SUBCLASS: ' || to_char(I_subclass));

   open  C_CHECK_DEFAULT_NO_UDA_VALUE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DEFAULT_NO_UDA_VALUE',
                    'UDA_ITEM_DEFAULTS',
                    'DEPT: '       || to_char(I_dept)   ||
                    ', CLASS: '    || to_char(I_class)  ||
                    ', SUBCLASS: ' || to_char(I_subclass));

   fetch C_CHECK_DEFAULT_NO_UDA_VALUE into L_dummy;
   if C_CHECK_DEFAULT_NO_UDA_VALUE%FOUND then
      O_default_no_value := TRUE;
   else
      O_default_no_value := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DEFAULT_NO_UDA_VALUE',
                    'UDA_ITEM_DEFAULTS',
                    'DEPT: '       || to_char(I_dept)   ||
                    ', CLASS: '    || to_char(I_class)  ||
                    ', SUBCLASS: ' || to_char(I_subclass));

   close C_CHECK_DEFAULT_NO_UDA_VALUE;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.CHECK_DEFAULT_NO_VALUE',
                                             to_char(SQLCODE));
      RETURN FALSE;

END CHECK_DEFAULT_NO_VALUE;

---------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS_NO_UDA( O_error_message  IN OUT  VARCHAR2,
                             O_exists         IN OUT  BOOLEAN,
                             I_uda_id         IN      UDA_ITEM_DEFAULTS.UDA_ID%TYPE,
                             I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                             I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                             I_subclass       IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                             RETURN BOOLEAN IS

   L_display_type  UDA.DISPLAY_TYPE%TYPE  := NULL;
   L_dummy         VARCHAR2(3);
   NO_RECORDS      EXCEPTION;

   cursor C_GET_DISPLAY_TYPE is
      select display_type
        from uda
       where uda_id = I_uda_id;

   cursor C_CHECK_REQUIRED_ITEMS_LOV is
      select 'x'
        from item_master
       where dept     = I_dept
         and class    = nvl(I_class, class)
         and subclass = nvl(I_subclass, subclass)
         and item_level <= tran_level
         and item not in(select item
                           from uda_item_lov
                          where uda_id = I_uda_id);

   cursor C_CHECK_REQUIRED_ITEMS_FF is
      select 'x'
        from item_master
       where dept     = I_dept
         and class    = nvl(I_class, class)
         and subclass = nvl(I_subclass, subclass)
         and item_level <= tran_level
         and item not in(select item
                           from uda_item_ff
                          where uda_id = I_uda_id);

   cursor C_CHECK_REQUIRED_ITEMS_DATE is
      select 'x'
        from item_master
       where dept = I_dept
         and class    = nvl(I_class, class)
         and subclass = nvl(I_subclass, subclass)
         and item_level <= tran_level
         and item not in(select item
                           from uda_item_date
                          where uda_id = I_uda_id);

BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG( 'MUST_ENTER_DEPT',
                                             NULL,
                                             NULL,
                                             NULL);

      RETURN FALSE;
   elsif I_class is NULL and
      I_subclass is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG( 'INV_SUBCLASS',
                                             NULL,
                                             NULL,
                                             NULL);

      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DISPLAY_TYPE',
                    'UDA',
                    'UDA ID: ' || to_char(I_uda_id));

   open  C_GET_DISPLAY_TYPE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DISPLAY_TYPE',
                    'UDA',
                    'UDA ID: ' || to_char(I_uda_id));

   fetch C_GET_DISPLAY_TYPE into L_display_type;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DISPLAY_TYPE',
                    'UDA',
                    'UDA ID: ' || to_char(I_uda_id));

   close C_GET_DISPLAY_TYPE;

   if L_display_type = 'LV' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_REQUIRED_ITEMS_LOV',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      open  C_CHECK_REQUIRED_ITEMS_LOV;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_REQUIRED_ITEMS_LOV',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      fetch C_CHECK_REQUIRED_ITEMS_LOV into L_dummy;
      if C_CHECK_REQUIRED_ITEMS_LOV%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_REQUIRED_ITEMS_LOV',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      close C_CHECK_REQUIRED_ITEMS_LOV;
   elsif L_display_type = 'FF' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_REQUIRED_ITEMS_FF',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      open  C_CHECK_REQUIRED_ITEMS_FF;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_REQUIRED_ITEMS_FF',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      fetch C_CHECK_REQUIRED_ITEMS_FF into L_dummy;
      if C_CHECK_REQUIRED_ITEMS_FF%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_REQUIRED_ITEMS_FF',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      close C_CHECK_REQUIRED_ITEMS_FF;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_REQUIRED_ITEMS_DATE',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      open  C_CHECK_REQUIRED_ITEMS_DATE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_REQUIRED_ITEMS_DATE',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      fetch C_CHECK_REQUIRED_ITEMS_DATE into L_dummy;
      if C_CHECK_REQUIRED_ITEMS_DATE%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_REQUIRED_ITEMS_DATE',
                       'ITEM_MASTER',
                       'UDA ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(I_dept)   ||
                       ', CLASS: '    || to_char(I_class)  ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      close C_CHECK_REQUIRED_ITEMS_DATE;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.CHECK_ITEMS_NO_UDA',
                                             to_char(SQLCODE));
      RETURN FALSE;

END CHECK_ITEMS_NO_UDA;

---------------------------------------------------------------------------------------------
FUNCTION ASSIGN_DEFAULTS( O_error_message  IN OUT  VARCHAR2,
                          I_item           IN      UDA_ITEM_LOV.ITEM%TYPE,
                          I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                          I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                          I_subclass       IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                          RETURN BOOLEAN IS

   L_item_level         ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level         ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_parent             ITEM_MASTER.ITEM_PARENT%TYPE;
   L_parent_desc        ITEM_MASTER.ITEM_DESC%TYPE;
   L_grandparent        ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_grandparent_desc   ITEM_MASTER.ITEM_DESC%TYPE;


BEGIN
   if I_dept is NULL or
      I_class is NULL or
      I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG( 'ENT_DEPT_CLASS_SUB',
                                             NULL,
                                             NULL,
                                             NULL);

      RETURN FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                 L_item_level,
                                 L_tran_level,
                                 I_item) = FALSE then
      RETURN FALSE;
   end if;

   if L_item_level = 1 then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'UDA_ITEM_LOV',
                       'ITEM: ' || (I_item) ||
                       ', DEPT: ' || to_char(I_dept) ||
                       ', CLASS: ' || to_char(I_class) ||
                       ', SUBCLASS: ' || to_char(I_subclass));

      insert into uda_item_lov(item,
                               uda_id,
                               uda_value,
                               create_datetime,
                               last_update_datetime,
                               last_update_id)
      select I_item,
             a.uda_id,
             a.uda_value,
             sysdate,
             sysdate,
             GET_USER
        from uda_item_defaults a,
             uda
       where a.dept = I_dept
         and nvl(a.class, I_class) = I_class
         and nvl(a.subclass, I_subclass) = I_subclass
         and a.uda_value is not NULL
         and uda.uda_id = a.uda_id
         and uda.display_type = 'LV'
         and a.hierarchy_value = (select max(b.hierarchy_value)
                                    from uda_item_defaults b
                                   where b.dept = I_dept
                                     and nvl(b.class, I_class) = I_class
                                     and nvl(b.subclass, I_subclass) = I_subclass
                                     and b.uda_id = a.uda_id);

   else
      if ITEM_ATTRIB_SQL.GET_PARENT_INFO(O_error_message,
                                         L_parent,
                                         L_parent_desc,
                                         L_grandparent,
                                         L_grandparent_desc,
                                         I_item) = FALSE then
         RETURN FALSE;
      end if;

      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'UDA_ITEM_LOV',
                       'ITEM: ' || (I_item));

      insert into uda_item_lov(item,
                               uda_id,
                               uda_value,
                               create_datetime,
                               last_update_datetime,
                               last_update_id)
      select distinct(I_item),
             a.uda_id,
             a.uda_value,
             sysdate,
             sysdate,
             GET_USER
        from uda_item_lov a,
             item_master m
       where (m.item = L_parent
          or m.item = L_grandparent)
         and m.item = a.item;

      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'UDA_ITEM_FF',
                       'ITEM: ' || (I_item));

      insert into uda_item_ff(item,
                              uda_id,
                              uda_text,
                              create_datetime,
                              last_update_datetime,
                              last_update_id)
      select distinct(I_item),
             a.uda_id,
             a.uda_text,
             sysdate,
             sysdate,
             GET_USER
        from uda_item_ff a,
             item_master m
       where (m.item = L_parent
          or m.item = L_grandparent)
         and m.item = a.item;

      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'UDA_ITEM_DATE',
                       'ITEM: ' || (I_item));

      insert into uda_item_date(item,
                                uda_id,
                                uda_date,
                                create_datetime,
                                last_update_datetime,
                                last_update_id)
      select distinct(I_item),
             a.uda_id,
             a.uda_date,
             sysdate,
             sysdate,
             GET_USER
        from uda_item_date a,
             item_master m
       where (m.item = L_parent
          or m.item = L_grandparent)
         and m.item = a.item;

   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.ASSIGN_DEFAULTS',
                                             to_char(SQLCODE));
      RETURN FALSE;

END ASSIGN_DEFAULTS;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_UDA( O_error_message  IN OUT  VARCHAR2,
                             O_required       IN OUT  BOOLEAN,
                             I_uda_id         IN      UDA_ITEM_LOV.UDA_ID%TYPE,
                             I_item           IN      UDA_ITEM_LOV.ITEM%TYPE,
                             I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                             I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                             I_subclass       IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                             RETURN BOOLEAN IS

   L_dept             UDA_ITEM_DEFAULTS.DEPT%TYPE             := I_dept;
   L_class            UDA_ITEM_DEFAULTS.CLASS%TYPE            := I_class;
   L_subclass         UDA_ITEM_DEFAULTS.SUBCLASS%TYPE         := I_subclass;
   L_dummy  VARCHAR2(3);

   cursor C_CHECK_REQUIRED_UDA_ITEM is
      select 'x'
        from uda_item_defaults
       where uda_id                    = I_uda_id
         and dept                      = L_dept
         and nvl(class, L_class)       = L_class
         and nvl(subclass, L_subclass) = L_subclass
         and required_ind              = 'Y'
         and ROWNUM = 1;

   cursor C_CHECK_REQUIRED_UDA_HIER is
      select 'x'
        from uda_item_defaults
       where uda_id = I_uda_id
         and dept   = I_dept
         and (((nvl(class, I_class) = I_class)
               and (I_subclass is not NULL
                    and subclass is NULL))
               or (class is NULL
                   and subclass is NULL
                   and I_subclass is NULL))
         and required_ind = 'Y';

BEGIN
   if I_item is not NULL then
      if L_dept is NULL or L_class is NULL or L_subclass is NULL then
         if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                           I_item,
                                           L_dept,
                                           L_class,
                                           L_subclass) = FALSE then
            return FALSE;
         end if;
      end if;
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_REQUIRED_UDA_ITEM',
                       'UDA_ITEM_DEFAULTS',
                       'UDA_ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(L_dept)   ||
                       ', CLASS: '    || to_char(L_class)  ||
                       ', SUBCLASS: ' || to_char(L_subclass));

      open  C_CHECK_REQUIRED_UDA_ITEM;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_REQUIRED_UDA_ITEM',
                       'UDA_ITEM_DEFAULTS',
                       'UDA_ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(L_dept)   ||
                       ', CLASS: '    || to_char(L_class)  ||
                       ', SUBCLASS: ' || to_char(L_subclass));

      fetch C_CHECK_REQUIRED_UDA_ITEM into L_dummy;
      if C_CHECK_REQUIRED_UDA_ITEM%FOUND then
         O_required := TRUE;
      else  -- Hierarchy check.
         O_required := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_REQUIRED_UDA_ITEM',
                       'UDA_ITEM_DEFAULTS',
                       'UDA_ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(L_dept)   ||
                       ', CLASS: '    || to_char(L_class)  ||
                       ', SUBCLASS: ' || to_char(L_subclass));

      close C_CHECK_REQUIRED_UDA_ITEM;
   else
      if I_dept is NULL then
         O_error_message := SQL_LIB.CREATE_MSG( 'MUST_ENTER_DEPT',
                                                NULL,
                                                NULL,
                                                NULL);

         RETURN FALSE;
      elsif I_class is NULL then
         O_error_message := SQL_LIB.CREATE_MSG( 'MUST_ENTER_CLASS',
                                                NULL,
                                                NULL,
                                                NULL);

         RETURN FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_REQUIRED_UDA_HIER',
                       'UDA_ITEM_DEFAULTS',
                       'UDA_ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(L_dept)   ||
                       ', CLASS: '    || to_char(L_class)  ||
                       ', SUBCLASS: ' || to_char(L_subclass));

      open  C_CHECK_REQUIRED_UDA_HIER;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_REQUIRED_UDA_HIER',
                       'UDA_ITEM_DEFAULTS',
                       'UDA_ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(L_dept)   ||
                       ', CLASS: '    || to_char(L_class)  ||
                       ', SUBCLASS: ' || to_char(L_subclass));

      fetch C_CHECK_REQUIRED_UDA_HIER into L_dummy;
      if C_CHECK_REQUIRED_UDA_HIER%FOUND then
         O_required := TRUE;
      else
         O_required := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_REQUIRED_UDA_HIER',
                       'UDA_ITEM_DEFAULTS',
                       'UDA_ID: '     || to_char(I_uda_id) ||
                       ', DEPT: '     || to_char(L_dept)   ||
                       ', CLASS: '    || to_char(L_class)  ||
                       ', SUBCLASS: ' || to_char(L_subclass));

      close C_CHECK_REQUIRED_UDA_HIER;
   end if;

   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.CHECK_REQUIRED_UDA',
                                             to_char(SQLCODE));
      RETURN FALSE;
END CHECK_REQUIRED_UDA;

---------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_HIER( O_error_message  IN OUT  VARCHAR2,
                              O_not_required   IN OUT  BOOLEAN,
                              I_uda_id         IN      UDA_ITEM_LOV.UDA_ID%TYPE,
                              I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                              I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE)
                              RETURN BOOLEAN IS

   L_dummy  VARCHAR2(3);

--
---  If the class is Null, the department level is being checked for non-
---  required Defaults BELOW the department level.  To prevent finding the
---  record at the department level, the class level on the table should
---  not be Null.  This logic also holds for the class level.  If coming in
---  at the class level, the subclass on the table should not be Null.
--

   cursor C_CHECK_REQUIRED_HIER is
      select 'x'
        from uda_item_defaults
       where uda_id       = I_uda_id
         and uda_value    is NULL
         and dept         = I_dept
         and ((class      = I_class
               and subclass is not NULL)
              or (I_class is NULL
                  and class is not NULL))
         and required_ind = 'N';

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_REQUIRED_HIER',
                    'UDA_ITEM_DEFAULTS',
                    'UDA_ID: '     || to_char(I_uda_id) ||
                    ', DEPT: '     || to_char(I_dept)   ||
                    ', CLASS: '    || to_char(I_class));

   open  C_CHECK_REQUIRED_HIER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_REQUIRED_HIER',
                    'UDA_ITEM_DEFAULTS',
                    'UDA_ID: '     || to_char(I_uda_id) ||
                    ', DEPT: '     || to_char(I_dept)   ||
                    ', CLASS: '    || to_char(I_class));

   fetch C_CHECK_REQUIRED_HIER into L_dummy;

   if C_CHECK_REQUIRED_HIER%FOUND then
      O_not_required := TRUE;
   else
      O_not_required := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_REQUIRED_HIER',
                    'UDA_ITEM_DEFAULTS',
                    'UDA_ID: '     || to_char(I_uda_id) ||
                    ', DEPT: '     || to_char(I_dept)   ||
                    ', CLASS: '    || to_char(I_class));

   close C_CHECK_REQUIRED_HIER;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.CHECK_REQUIRED_HIER',
                                             to_char(SQLCODE));
      RETURN FALSE;

END CHECK_REQUIRED_HIER;

---------------------------------------------------------------------------------------------
FUNCTION UPDATE_REQUIRED_HIER( O_error_message  IN OUT  VARCHAR2,
                               I_uda_id         IN      UDA_ITEM_DEFAULTS.UDA_ID%TYPE,
                               I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                               I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE)
                               RETURN BOOLEAN IS

   L_uda_value        UDA_ITEM_DEFAULTS.UDA_VALUE%TYPE        := NULL;
   L_class            UDA_ITEM_DEFAULTS.CLASS%TYPE            := NULL;
   L_subclass         UDA_ITEM_DEFAULTS.SUBCLASS%TYPE         := NULL;
   L_seq_no           UDA_ITEM_DEFAULTS.SEQ_NO%TYPE           := NULL;
   L_hierarchy_value  UDA_ITEM_DEFAULTS.HIERARCHY_VALUE%TYPE  := NULL;
   Record_Locked  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_UDA_ITEM_DEFAULTS is
      select 'x'
        from uda_item_defaults
       where uda_id       = I_uda_id
         and required_ind = 'N'
         and dept         = I_dept
         and ((class is not NULL
              and I_class is NULL)
              or (class = I_class
                  and subclass is not NULL))
         and uda_value is NULL
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_UDA_ITEM_DEFAULTS',
                    'UDA_ITEM_DEFAULTS',
                    'UDA ID: '  || to_char(I_uda_id) ||
                    ', DEPT: '  || to_char(I_dept)   ||
                    ', CLASS: ' || to_char(I_class));

   open C_LOCK_UDA_ITEM_DEFAULTS;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_UDA_ITEM_DEFAULTS',
                    'UDA_ITEM_DEFAULTS',
                    'UDA ID: '  || to_char(I_uda_id) ||
                    ', DEPT: '  || to_char(I_dept)   ||
                    ', CLASS: ' || to_char(I_class));

   close C_LOCK_UDA_ITEM_DEFAULTS;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'UDA_ITEM_DEFAULTS',
                    'UDA ID: '     || to_char(I_uda_id) ||
                    ', DEPT: '     || to_char(I_dept)   ||
                    ', CLASS: '    || to_char(I_class));

   update uda_item_defaults
      set required_ind = 'Y'
    where uda_id       = I_uda_id
      and required_ind = 'N'
      and dept         = I_dept
      and ((class is not NULL
           and I_class is NULL)
           or (class = I_class
               and subclass is not NULL))
      and uda_value is NULL;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_UDA_ITEM_DEFAULTS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_UDA_ITEM_DEFAULTS',
                          'UDA_ITEM_DEFAULTS',
                          'UDA ID: '  || to_char(I_uda_id) ||
                          ', DEPT: '  || to_char(I_dept)   ||
                          ', CLASS: ' || to_char(I_class));

         close C_LOCK_UDA_ITEM_DEFAULTS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG( 'UDA_ITEM_DEFAULT_REC_LOCK',
                                             to_char(I_dept),
                                             to_char(L_class),
                                             to_char(L_subclass));

      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.UPDATE_REQUIRED_UDA',
                                             to_char(SQLCODE));
      RETURN FALSE;

END UPDATE_REQUIRED_HIER;

---------------------------------------------------------------------------------------------
FUNCTION PERFORM_MC( O_error_message       IN OUT  VARCHAR2,
                     O_reject              IN OUT  BOOLEAN,
                     I_itemlist            IN      SKULIST_DETAIL.SKULIST%TYPE,
                     I_maint_type          IN      VARCHAR2,
                     I_display_type        IN      UDA.DISPLAY_TYPE%TYPE,
                     I_uda_id              IN      UDA_VALUES.UDA_ID%TYPE,
                     I_new_uda_value       IN      VARCHAR2,
                     I_existing_uda_value  IN      VARCHAR2)
                     RETURN BOOLEAN IS

   L_user_id         VARCHAR2(255)                    := get_user;
   L_item            SKULIST_DETAIL.ITEM%TYPE         := NULL;
   L_system_ind      SKULIST_DETAIL.PACK_IND%TYPE     := NULL;
   L_required        BOOLEAN     := FALSE;
   L_single_val_ind  VARCHAR2(3) := 'N';
   L_dummy           VARCHAR2(1);
   L_count           NUMBER(11);
   L_item_level      SKULIST_DETAIL.ITEM_LEVEL%TYPE   := NULL;
   L_tran_level      SKULIST_DETAIL.TRAN_LEVEL%TYPE   := NULL;
   L_child_item      ITEM_MASTER.ITEM%TYPE            := NULL;


   cursor C_EXPLODE_CHILDLIST is
      select item
        from item_master
       where item_parent = L_item or
         item_grandparent=L_item and
         tran_level >= item_level;


   cursor C_EXPLODE_ITEMLIST is
      select item,
             item_level,
             tran_level
        from skulist_detail
       where skulist = I_itemlist;

   cursor C_GET_SINGLE_VAL_IND is
      select single_value_ind
        from uda
       where uda_id = I_uda_id;

   cursor C_COUNT_UDA_VALS_FOR_ITEM_LOV is
      select count(*)
        from uda_item_lov
       where uda_id = I_uda_id
         and item   = L_item;

   cursor C_COUNT_UDA_VALS_FOR_ITEM_FF is
      select count(*)
        from uda_item_ff
       where uda_id = I_uda_id
         and item = L_item;

   cursor C_COUNT_UDA_VALS_FOR_ITEM_DT is
      select count(*)
        from uda_item_date
       where uda_id = I_uda_id
         and item = L_item;    

-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_AND_INSERT(I_item        IN  SKULIST_DETAIL.ITEM%TYPE)
                             RETURN BOOLEAN IS
-- Sub function of PERFORM MC.
-- Validates an Item and calls INSERT_RECORD to Insert the Item.

   L_exists          BOOLEAN     := FALSE;

-------------------------------------------------------------------------------------------------
FUNCTION INSERT_RECORD (I_item        IN  SKULIST_DETAIL.ITEM%TYPE)
                        RETURN BOOLEAN IS
-- Sub function of VALIDATE_AND_INSERT.

   L_dummy  VARCHAR2(3);

   cursor C_CHECK_IF_EXISTS is
      select 'x'
        from uda_item_lov
       where item   = I_item
         and uda_id = I_uda_id;

   cursor C_CHECK_FF_IF_EXISTS is
     select 'x'
       from uda_item_ff uif,
           uda
      where item = I_item
        and uif.uda_id = I_uda_id
        and uif.uda_id = uda.uda_id
        and uif.uda_text = decode(uda.single_value_ind,'Y',uif.uda_text,I_new_uda_value);
   cursor C_CHECK_DT_IF_EXISTS is
     select 'x'
      from uda_item_date uida,
           uda
     where item = I_item
       and uida.uda_id = I_uda_id
       and uida.uda_id = uda.uda_id
       and uida.uda_date = decode(uda.single_value_ind,'Y',uida.uda_date,I_new_uda_value);         

-------------------------------------------------------------------------------------------------
--------
BEGIN

   if I_display_type = 'LV' then
      if I_maint_type = 'ADD' then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'UDA_ITEM_LOV',
                          'ITEM: '                      || (I_item)   ||
                          ', UDA_ID: '                  || to_char(I_uda_id));

         insert into uda_item_lov(item,
                                  uda_id,
                                  uda_value,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
         values(I_item,
                I_uda_id,
                to_number(I_new_uda_value),
                sysdate,
                sysdate,
                get_user);
      elsif I_maint_type = 'CREATE' then
      -- 'Create' only if item and uda_id combination does not exist.

         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_IF_EXISTS',
                          'UDA_ITEM_LOV',
                          'ITEM: '                      || (I_item)   ||
                          ', UDA_ID: '                  || to_char(I_uda_id) ||
                          ', UDA_VALUE: '               || to_number(I_new_uda_value));

         open C_CHECK_IF_EXISTS;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_IF_EXISTS',
                          'UDA_ITEM_LOV',
                          'ITEM: '                      || (I_item)   ||
                          ', UDA_ID: '                  || to_char(I_uda_id) ||
                          ', UDA_VALUE: '               || to_number(I_new_uda_value));

         fetch C_CHECK_IF_EXISTS into L_dummy;

         if C_CHECK_IF_EXISTS%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_IF_EXISTS',
                          'UDA_ITEM_LOV',
                          'ITEM: '                      || (I_item)   ||
                          ', UDA_ID: '                  || to_char(I_uda_id) ||
                          ', UDA_VALUE: '               || to_number(I_new_uda_value));

            close C_CHECK_IF_EXISTS;

            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'UDA_ITEM_LOV',
                          'ITEM: '                      || (I_item)   ||
                          ', UDA_ID: '                  || to_char(I_uda_id) ||
                          ', UDA_VALUE: '               || to_number(I_new_uda_value));

            insert into uda_item_lov(item,
                                     uda_id,
                                     uda_value,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id)
            values(I_item,
                   I_uda_id,
                   to_number(I_new_uda_value),
                   sysdate,
                   sysdate,
                   get_user);
         else  -- UDA/Item relation already exists.
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_IF_EXISTS',
                             'UDA_ITEM_LOV',
                             'ITEM: '                   || (I_item)   ||
                             ', UDA_ID: '               || to_char(I_uda_id) ||
                          ', UDA_VALUE: '               || to_number(I_new_uda_value));


            close C_CHECK_IF_EXISTS;

            RETURN TRUE;
         end if;  -- C_CHECK_IF_EXISTS
      end if;  -- I_maint_type = 'CREATE'

   elsif I_display_type = 'FF' then
      if I_maint_type = 'ADD' then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'UDA_ITEM_FF',
                          'ITEM: '        || (I_item)   ||
                          ', UDA_ID: '    || to_char(I_uda_id));

         insert into uda_item_ff(item,
                                 uda_id,
                                 uda_text,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
         values(I_item,
                I_uda_id,
                I_new_uda_value,
                sysdate,
                sysdate,
                get_user);
      elsif I_maint_type = 'CREATE' then
      -- 'Create' only if item and uda_id combination does not exist.
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_FF_IF_EXISTS',
                          'UDA_ITEM_FF',
                          'ITEM: ' || (I_item) ||
                          ', UDA_ID: ' || to_char(I_uda_id));
         open C_CHECK_FF_IF_EXISTS;
         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_FF_IF_EXISTS',
                          'UDA_ITEM_FF',
                          'ITEM: ' || (I_item) ||
                          ', UDA_ID: ' || to_char(I_uda_id));
         fetch C_CHECK_FF_IF_EXISTS into L_dummy;
         if C_CHECK_FF_IF_EXISTS%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHE   CK_FF_IF_EXISTS',
                             'UDA_I   TEM_FF',
                             'ITEM: ' || (I_item) ||
                             ', UDA_ID: ' || to_char(I_uda_id));
            close C_CHECK_FF_IF_EXISTS;
            SQL_LIB.SET_MARK('INSERT',
                             'C_CHECK_FF_IF_EXISTS',
                             'UDA_ITEM_FF',
                             'ITEM: ' || (I_item) ||
                             ', UDA_ID: ' || to_char(I_uda_id));
            insert into uda_item_ff(item,
                        uda_id,
                        uda_text,
                        create_datetime,
                        last_update_datetime,
                        last_update_id)
                        values(I_item,
                        I_uda_id,
                        I_new_uda_value,
                        sysdate,
                        sysdate,
                        get_user);
         else -- UDA/Item relation already exists.
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_FF_IF_EXISTS',
                             'UDA_ITEM_FF',
                             'ITEM: ' || (I_item) ||
                             ', UDA_ID: ' || to_char(I_uda_id));
            close C_CHECK_FF_IF_EXISTS;
            RETURN TRUE;
         end if; -- C_CHECK_FF_IF_EXISTS
      end if; -- I_maint_type = 'CREATE'

   else
      if I_maint_type = 'ADD' then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'UDA_ITEM_DATE',
                          'ITEM: '       || (I_item)   ||
                          ', UDA_ID: '   || to_char(I_uda_id));

         insert into uda_item_date(item,
                                   uda_id,
                                   uda_date,
                                   create_datetime,
                                   last_update_datetime,
                                   last_update_id)
         values(I_item,
                I_uda_id,
                to_date(I_new_uda_value,'DD-MM-YYYY'),
                sysdate,
                sysdate,
                get_user);
      elsif I_maint_type = 'CREATE' then
      -- 'Create' only if item and uda_id combination does not exist.
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_DT_IF_EXISTS',
                             'UDA_ITEM_DATE',
                             'ITEM: ' || (I_item) ||
                             ', UDA_ID: ' || to_char(I_uda_id));
            open C_CHECK_DT_IF_EXISTS;
            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_DT_IF_EXISTS',
                             'UDA_ITEM_DATE',
                             'ITEM: ' || (I_item) ||
                             ', UDA_ID: ' || to_char(I_uda_id));
            fetch C_CHECK_DT_IF_EXISTS into L_dummy;
         if C_CHECK_DT_IF_EXISTS%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_DT_IF_EXISTS',
                             'UDA_ITEM_DATE',
                             'ITEM: ' || (I_item) ||
                             ', UDA_ID: ' || to_char(I_uda_id));
                             close C_CHECK_DT_IF_EXISTS;
                             SQL_LIB.SET_MARK('INSERT',
                             'C_CHECK_DT_IF_EXISTS',
                             'UDA_ITEM_DATE',
                             'ITEM: ' || (I_item) ||
                             ', UDA_ID: ' || to_char(I_uda_id));
            insert into uda_item_date(item,
                                      uda_id,
                                      uda_date,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id)
                                      values(I_item,
                                      I_uda_id,
                                      to_date(I_new_uda_value,'DD-MM-YYYY'),
                                      sysdate,
                                      sysdate,
                                      get_user);
                                      else -- UDA/Item relation already exists.
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_DT_IF_EXISTS',
                             'UDA_ITEM_DATE',
                             'ITEM: ' || (I_item) ||
                             ', UDA_ID: ' || to_char(I_uda_id));
            close C_CHECK_DT_IF_EXISTS;
            RETURN TRUE;
         end if; -- C_CHECK_DT_IF_EXISTS
      end if; -- I_maint_type = 'CREATE'
   end if;

   RETURN TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then

      RETURN TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.INSERT_RECORD',
                                             to_char(SQLCODE));
      RETURN FALSE;

END INSERT_RECORD;

-------------------------------------------------------------------------------------------------
BEGIN
   if L_single_val_ind = 'Y'
      and I_maint_type = 'ADD' then

      if CHECK_SINGLE_UDA(O_error_message,
                          L_exists,
                          I_uda_id,
                          NULL,
                          I_item,
                          NULL,
                          NULL,
                          NULL) = FALSE then
         RETURN FALSE;
      end if;

      if L_exists = TRUE then
         O_reject := TRUE;

         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_item,
                                                   NULL,
                                                   NULL,
                                                   'U',
                                                   'SINGLE_UDA_EXISTS',
                                                   L_user_id,
                                                   I_item,
                                                   I_uda_id,
                                                   NULL) = FALSE then
            RETURN FALSE;
         end if;

      else  -- not L_exists
        if INSERT_RECORD(I_item) = FALSE then
            RETURN FALSE;
        end if;

        if L_tran_level > L_item_level then
           SQL_LIB.SET_MARK ('FETCH',
                             'C_EXPLODE_CHILDLIST',
                             'ITEM_MASTER',
                             ',ITEM: '   || (I_ITEM));

           FOR C_rec_child in C_EXPLODE_CHILDLIST LOOP
              L_child_item       := C_rec_child.item;


              if INSERT_RECORD(L_child_item) = FALSE then
                 RETURN FALSE;
              end if;

           END LOOP;  -- C_rec_child
        end if;

      end if;  -- not L_exists

    else  -- single_value_ind != 'Y' or I_maint_type = 'CREATE'
      if INSERT_RECORD(I_item) = FALSE then
         RETURN FALSE;
      end if;

      if L_tran_level > L_item_level then
         SQL_LIB.SET_MARK ('FETCH',
                           'C_EXPLODE_CHILDLIST',
                           'ITEM_MASTER',
                           ',ITEM: '   || (I_ITEM));

         FOR C_rec_child in C_EXPLODE_CHILDLIST LOOP
            L_child_item := C_rec_child.item;

            if INSERT_RECORD(L_child_item) = FALSE then
               RETURN FALSE;
            end if;

         END LOOP;  -- C_rec_child
      end if;

end if; -- single_value_ind != 'Y'

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.VALIDATE_AND_INSERT',
                                             to_char(SQLCODE));
      RETURN FALSE;

END VALIDATE_AND_INSERT;

-------------------------------------------------------------------------------------------------
FUNCTION UPDATE_RECORD RETURN BOOLEAN IS
-- Used to Change OR Delete a record.

   L_delete        BOOLEAN  := FALSE;
   L_dummy         VARCHAR2(1);
   L_exception_id  NUMBER(1);
   Record_Locked   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_CHECK_ITEM_LOV_DUPLICATES is
      select 'x'
        from uda_item_lov
       where uda_id    = I_uda_id
         and uda_value = to_number(I_new_uda_value)
         and item      = L_item;

   cursor C_CHECK_ICHILD_LOV_DUPLICATES is
      select 'x'
        from uda_item_lov
       where uda_id    = I_uda_id
         and uda_value = to_number(I_new_uda_value)
         and item      = L_child_item;

   cursor C_LOCK_UDA_ITEM_LOV is
      select 'x'
        from uda_item_lov
       where uda_id    = I_uda_id
         and uda_value = to_number(I_existing_uda_value)
         and item      = L_item
         for update nowait;

   cursor C_LOCK_UDA_ICHILD_LOV is
      select 'x'
        from uda_item_lov
       where uda_id    = I_uda_id
         and uda_value = to_number(I_existing_uda_value)
         and item      = L_child_item
         for update nowait;

   cursor C_LOCK_UDA_ITEM_FF_TL is
      select 'x'
        from uda_item_ff_tl
       where uda_id   = I_uda_id
         and uda_text = I_existing_uda_value
         and item     = L_item
         for update nowait;
       
   cursor C_LOCK_UDA_ITEM_FF is
      select 'x'
        from uda_item_ff
       where uda_id   = I_uda_id
         and uda_text = I_existing_uda_value
         and item     = L_item
         for update nowait;

   cursor C_LOCK_UDA_ICHILD_FF_TL is
      select 'x'
        from uda_item_ff_tl
       where uda_id   = I_uda_id
         and uda_text = I_existing_uda_value
         and item     = L_child_item
         for update nowait;

   cursor C_LOCK_UDA_ICHILD_FF is
      select 'x'
        from uda_item_ff
       where uda_id   = I_uda_id
         and uda_text = I_existing_uda_value
         and item     = L_child_item
         for update nowait;

   cursor C_LOCK_UDA_ITEM_DATE is
      select 'x'
        from uda_item_date
       where uda_id   = I_uda_id
         and uda_date = to_date(I_existing_uda_value,'DD-MM-YYYY')
         and item     = L_item
         for update nowait;

   cursor C_LOCK_UDA_ICHILD_DATE is
      select 'x'
        from uda_item_date
       where uda_id   = I_uda_id
         and uda_date = to_date(I_existing_uda_value,'DD-MM-YYYY')
         and item     = L_child_item
         for update nowait;

BEGIN
   if I_display_type = 'LV' then
      L_exception_id := 1;

      if I_maint_type = 'CHANGE' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_ITEM_LOV_DUPLICATES',
                          'UDA_ITEM_LOV',
                          'UDA ID: '      || to_char(I_uda_id) ||
                          ', ITEM: '      || (L_item));

         open C_CHECK_ITEM_LOV_DUPLICATES;

          SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_ITEM_LOV_DUPLICATES',
                          'UDA_ITEM_LOV',
                          'UDA ID: '      || to_char(I_uda_id) ||
                          ', ITEM: '      || (L_item));

         fetch C_CHECK_ITEM_LOV_DUPLICATES into L_dummy;

         if C_CHECK_ITEM_LOV_DUPLICATES%FOUND then
            L_delete := TRUE;
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_ITEM_LOV_DUPLICATES',
                          'UDA_ITEM_LOV',
                          'UDA ID: '      || to_char(I_uda_id) ||
                          ', ITEM: '      || (L_item));

         close C_CHECK_ITEM_LOV_DUPLICATES;
      end if;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_UDA_ITEM_LOV',
                       'UDA_ITEM_LOV',
                       'UDA ID: '      || to_char(I_uda_id)    ||
                       ', ITEM: '      || (L_item));

      open C_LOCK_UDA_ITEM_LOV;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_UDA_ITEM_LOV',
                       'UDA_ITEM_LOV',
                       'UDA ID: '      || to_char(I_uda_id)    ||
                       ', ITEM: '      || (L_item));

      close C_LOCK_UDA_ITEM_LOV;

      if I_maint_type = 'CHANGE' and not L_delete then

         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'UDA_ITEM_LOV',
                          'ITEM: '        || (L_item)   ||
                          ', UDA_ID: '    || to_char(I_uda_id));

         update uda_item_lov
            set uda_value = to_number(I_new_uda_value),
                last_update_datetime = sysdate,
                last_update_id = get_user
          where uda_id    = I_uda_id
            and uda_value = to_number(I_existing_uda_value)
            and item      = L_item;

      elsif I_maint_type = 'DELETE' then
         SQL_LIB.SET_MARK('DELETE',
                          NULL,
                          'UDA_ITEM_LOV',
                          'ITEM: '       || (L_item)   ||
                          ', UDA_ID: '   || to_char(I_uda_id));

         delete from uda_item_lov
          where uda_id    = I_uda_id
            and uda_value = to_number(I_existing_uda_value)
            and item      = L_item;
      end if;

      --- This process is for child item ---
      if L_tran_level > L_item_level then

         SQL_LIB.SET_MARK ('FETCH',
                           'C_EXPLODE_CHILDLIST',
                           'ITEM_MASTER',
                           ',ITEM: '   || (L_ITEM));

         FOR C_rec_child in C_EXPLODE_CHILDLIST LOOP
                L_child_item       := C_rec_child.item;

            if I_maint_type = 'CHANGE' then

               SQL_LIB.SET_MARK('OPEN',
                                'C_CHECK_ICHILD_LOV_DUPLICATES',
                                'UDA_ITEM_LOV',
                                'UDA ID: '      || to_char(I_uda_id) ||
                                ', ITEM: '      || (L_child_item));

               open C_CHECK_ICHILD_LOV_DUPLICATES;

               SQL_LIB.SET_MARK('FETCH',
                                'C_CHECK_ICHILD_LOV_DUPLICATES',
                                'UDA_ITEM_LOV',
                                'UDA ID: '      || to_char(I_uda_id) ||
                                ', ITEM: '      || (L_child_item));

               fetch C_CHECK_ICHILD_LOV_DUPLICATES into L_dummy;

               if C_CHECK_ICHILD_LOV_DUPLICATES%FOUND then
                  L_delete := TRUE;
               end if;

               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_ICHILD_LOV_DUPLICATES',
                                'UDA_ITEM_LOV',
                                'UDA ID: '      || to_char(I_uda_id) ||
                                ', ITEM: '      || (L_child_item));

               close C_CHECK_ICHILD_LOV_DUPLICATES;
            end if;

            SQL_LIB.SET_MARK('OPEN',
                             'C_LOCK_UDA_ICHILD_LOV',
                             'UDA_ITEM_LOV',
                             'UDA ID: '      || to_char(I_uda_id)    ||
                             ', ITEM: '      || (L_child_item));

            open C_LOCK_UDA_ICHILD_LOV;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_LOCK_UDA_ITEM_LOV',
                             'UDA_ICHILD_LOV',
                             'UDA ID: '      || to_char(I_uda_id)    ||
                             ', ITEM: '      || (L_child_item));

            close C_LOCK_UDA_ICHILD_LOV;

            if I_maint_type = 'CHANGE' and not L_delete then
                  SQL_LIB.SET_MARK('UPDATE',
                                   NULL,
                                   'UDA_ITEM_LOV',
                                   'ITEM: '        || (L_item)   ||
                                   ', UDA_ID: '    || to_char(I_uda_id));

                     update uda_item_lov
                     set uda_value = to_number(I_new_uda_value),
                         last_update_datetime = sysdate,
                         last_update_id = get_user
                   where uda_id    = I_uda_id
                     and uda_value = to_number(I_existing_uda_value)
                     and item      = L_child_item;
            elsif I_maint_type = 'DELETE' then
               SQL_LIB.SET_MARK('DELETE',
                                NULL,
                               'UDA_ITEM_LOV',
                               'ITEM: '       || (L_child_item)   ||
                               ', UDA_ID: '   || to_char(I_uda_id));

               delete from uda_item_lov
                where uda_id    = I_uda_id
                  and uda_value = to_number(I_existing_uda_value)
                  and item      = L_child_item;
            end if;

         END LOOP;  -- C_rec_child
      end if;

   elsif I_display_type = 'FF' then
      L_exception_id := 2;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_UDA_ITEM_FF_TL',
                       'UDA_ITEM_FF_TL',
                       'UDA ID: '      || to_char(I_uda_id)    ||
                       ', ITEM: '      || (L_item));
      open C_LOCK_UDA_ITEM_FF_TL;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_UDA_ITEM_FF_TL',
                       'UDA_ITEM_FF_TL',
                       'UDA ID: '      || to_char(I_uda_id)    ||
                       ', ITEM: '      || (L_item));
      close C_LOCK_UDA_ITEM_FF_TL;
      
      if I_maint_type = 'CHANGE' or I_maint_type = 'DELETE' then
         -- Delete from translation table in both change or delete because item uda_text is part of primary key.
         SQL_LIB.SET_MARK('DELETE',
                          NULL,
                          'UDA_ITEM_FF_TL',
                          'ITEM: '       || (L_item)   ||
                          ', UDA_ID: '   || to_char(I_uda_id));
         delete from uda_item_ff_tl
          where uda_id   = I_uda_id
            and uda_text = I_existing_uda_value
            and item     = L_item;
      end if;
      
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_UDA_ITEM_FF',
                       'UDA_ITEM_FF',
                       'UDA ID: '      || to_char(I_uda_id)    ||
                       ', ITEM: '      || (L_item));

      open C_LOCK_UDA_ITEM_FF;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_UDA_ITEM_FF',
                       'UDA_ITEM_FF',
                       'UDA ID: '      || to_char(I_uda_id)    ||
                       ', ITEM: '      || (L_item));

      close C_LOCK_UDA_ITEM_FF;

      if I_maint_type = 'CHANGE' then
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'UDA_ITEM_FF',
                          'ITEM: '       || (L_item)   ||
                          ', UDA_ID: '   || to_char(I_uda_id));

         update uda_item_ff
            set uda_text = I_new_uda_value,
                last_update_datetime = sysdate,
                last_update_id = get_user
          where uda_id   = I_uda_id
            and uda_text = I_existing_uda_value
            and item     = L_item;

      elsif I_maint_type = 'DELETE' then
         SQL_LIB.SET_MARK('DELETE',
                          NULL,
                          'UDA_ITEM_FF',
                          'ITEM: '       || (L_item)   ||
                          ', UDA_ID: '   || to_char(I_uda_id));

         delete from uda_item_ff
          where uda_id   = I_uda_id
            and uda_text = I_existing_uda_value
            and item     = L_item;
      end if;

      --- This process is for child item ---
      if L_tran_level > L_item_level then
         SQL_LIB.SET_MARK ('FETCH',
                           'C_EXPLODE_CHILDLIST',
                           'ITEM_MASTER',
                           ',ITEM: '   || (L_ITEM));
         FOR C_rec_child in C_EXPLODE_CHILDLIST LOOP
            L_child_item       := C_rec_child.item;
            SQL_LIB.SET_MARK('OPEN',
                             'C_LOCK_UDA_ICHILD_FF_TL',
                             'UDA_ITEM_FF_TL',
                             'UDA ID: '      || to_char(I_uda_id)    ||
                             ', ITEM: '      || (L_child_item));
            open C_LOCK_UDA_ICHILD_FF_TL;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_LOCK_UDA_ICHILD_FF_TL',
                             'UDA_ITEM_FF_TL',
                             'UDA ID: '      || to_char(I_uda_id)    ||
                             ', ITEM: '      || (L_child_item));
            close C_LOCK_UDA_ICHILD_FF_TL;
            
            if I_maint_type = 'CHANGE' or I_maint_type = 'DELETE' then
               SQL_LIB.SET_MARK('DELETE',
                                NULL,
                                'UDA_ITEM_FF_TL',
                                'ITEM: '       || (L_child_item)   ||
                                ', UDA_ID: '   || to_char(I_uda_id));
               delete from uda_item_ff_tl
                where uda_id   = I_uda_id
                  and uda_text = I_existing_uda_value
                  and item     = L_child_item;
            end if;

            SQL_LIB.SET_MARK('OPEN',
                             'C_LOCK_UDA_ICHILD_FF',
                             'UDA_ITEM_FF',
                             'UDA ID: '      || to_char(I_uda_id)    ||
                             ', ITEM: '      || (L_child_item));

            open C_LOCK_UDA_ICHILD_FF;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_LOCK_UDA_ICHILD_FF',
                             'UDA_ITEM_FF',
                             'UDA ID: '      || to_char(I_uda_id)    ||
                             ', ITEM: '      || (L_child_item));

            close C_LOCK_UDA_ICHILD_FF;

            if I_maint_type = 'CHANGE' then
                  SQL_LIB.SET_MARK('UPDATE',
                                   NULL,
                                   'UDA_ITEM_FF',
                                   'ITEM: '       || (L_child_item)   ||
                                   ', UDA_ID: '   || to_char(I_uda_id));

               update uda_item_ff
                  set uda_text = I_new_uda_value,
                      last_update_datetime = sysdate,
                      last_update_id = get_user
                where uda_id   = I_uda_id
                  and uda_text = I_existing_uda_value
                  and item     = L_child_item;

            elsif I_maint_type = 'DELETE' then
               SQL_LIB.SET_MARK('DELETE',
                                NULL,
                                'UDA_ITEM_FF',
                                'ITEM: '       || (L_child_item)   ||
                                ', UDA_ID: '   || to_char(I_uda_id));

               delete from uda_item_ff
                where uda_id   = I_uda_id
                  and uda_text = I_existing_uda_value
                  and item     = L_child_item;

            end if;

         END LOOP;  -- C_rec_child
      end if;

   elsif I_display_type = 'DT' then
      L_exception_id := 3;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_UDA_ITEM_DATE',
                       'UDA_ITEM_DATE',
                       'UDA ID: '      || to_char(I_uda_id)    ||
                       ', ITEM: '      || (L_item));

      open C_LOCK_UDA_ITEM_DATE;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_UDA_ITEM_DATE',
                       'UDA_ITEM_DATE',
                       'UDA ID: '      || to_char(I_uda_id)    ||
                       ', ITEM: '      || (L_item));

      close C_LOCK_UDA_ITEM_DATE;

      if I_maint_type = 'CHANGE' then
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'UDA_ITEM_DATE',
                          'ITEM: '       || (L_item)   ||
                          ', UDA_ID: '   || to_char(I_uda_id));

         update uda_item_date
            set uda_date = to_date(I_new_uda_value,'DD-MM-YYYY'),
                last_update_datetime = sysdate,
                last_update_id = get_user
          where uda_id   = I_uda_id
            and uda_date = to_date(I_existing_uda_value,'DD-MM-YYYY')
            and item     = L_item;
      elsif I_maint_type = 'DELETE' then
         SQL_LIB.SET_MARK('DELETE',
                          NULL,
                          'UDA_ITEM_DATE',
                          'ITEM: '       || (L_item)   ||
                          ', UDA_ID: '   || to_char(I_uda_id));

         delete from uda_item_date
          where uda_id   = I_uda_id
            and uda_date = to_date(I_existing_uda_value,'DD-MM-YYYY')
            and item     = L_item;
      end if;

      --- This process is for child item ---
      if L_tran_level > L_item_level then
         SQL_LIB.SET_MARK ('FETCH',
                           'C_EXPLODE_CHILDLIST',
                           'ITEM_MASTER',
                           ',ITEM: '   || (L_ITEM));
         FOR C_rec_child in C_EXPLODE_CHILDLIST LOOP
            L_child_item       := C_rec_child.item;

            SQL_LIB.SET_MARK('OPEN',
                             'C_LOCK_UDA_ICHILD_DATE',
                             'UDA_ITEM_DATE',
                             'UDA ID: '      || to_char(I_uda_id)    ||
                             ', ITEM: '      || (L_child_item));

            open C_LOCK_UDA_ICHILD_DATE;

            SQL_LIB.SET_MARK('CLOSE',
                             'C_LOCK_UDA_ICHILD_DATE',
                             'UDA_ITEM_DATE',
                             'UDA ID: '      || to_char(I_uda_id)    ||
                             ', ITEM: '      || (L_child_item));

            close C_LOCK_UDA_ICHILD_DATE;

            if I_maint_type = 'CHANGE' then
                  SQL_LIB.SET_MARK('UPDATE',
                                   NULL,
                                   'UDA_ITEM_DATE',
                                   'ITEM: '       || (L_child_item)   ||
                                   ', UDA_ID: '   || to_char(I_uda_id));

               update uda_item_date
                  set uda_date = to_date(I_new_uda_value,'DD-MM-YYYY'),
                      last_update_datetime = sysdate,
                      last_update_id = get_user
                where uda_id   = I_uda_id
                  and uda_date = to_date(I_existing_uda_value,'DD-MM-YYYY')
                  and item     = L_child_item;

            elsif I_maint_type = 'DELETE' then
               SQL_LIB.SET_MARK('DELETE',
                                NULL,
                                'UDA_ITEM_DATE',
                                'ITEM: '       || (L_child_item)   ||
                                ', UDA_ID: '   || to_char(I_uda_id));

               delete from uda_item_date
                where uda_id   = I_uda_id
                  and uda_date = to_date(I_existing_uda_value,'DD-MM-YYYY')
                  and item     = L_child_item;

            end if;

         END LOOP;  -- C_rec_child
      end if;

   end if;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if L_exception_id = 1 then
         O_error_message := SQL_LIB.CREATE_MSG( 'UDA_ITEM_LOV_REC_LOCK',
                                                to_char(I_uda_id),
                                                (L_item));
      elsif L_exception_id = 2 then
         O_error_message := SQL_LIB.CREATE_MSG( 'UDA_ITEM_FF_REC_LOCK',
                                                to_char(I_uda_id),
                                                (L_item));
      elsif L_exception_id = 3 then
         O_error_message := SQL_LIB.CREATE_MSG( 'UDA_ITEM_DATE_REC_LOCK',
                                                to_char(I_uda_id),
                                                (L_item));
      end if;

      RETURN FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.DELETE_RECORD',
                                             to_char(SQLCODE));
      RETURN FALSE;

END UPDATE_RECORD;

-------------------------------------------------------------------------------------------------
BEGIN
   if I_maint_type = 'ADD' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SINGLE_VAL_IND',
                       'UDA',
                       'UDA ID: ' || to_char(I_uda_id));

      open C_GET_SINGLE_VAL_IND;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SINGLE_VAL_IND',
                       'UDA',
                       'UDA ID: ' || to_char(I_uda_id));

      fetch C_GET_SINGLE_VAL_IND into L_single_val_ind;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SINGLE_VAL_IND',
                       'UDA',
                       'UDA ID: ' || to_char(I_uda_id));

      close C_GET_SINGLE_VAL_IND;

      SQL_LIB.SET_MARK('FETCH',
                       'C_EXPLODE_ITEMLIST',
                       'SKULIST_DETAIL',
                       'ITEM LIST: ' || to_char(I_itemlist));

      FOR C_rec_item in C_EXPLODE_ITEMLIST LOOP
         L_item       := C_rec_item.item;
         L_item_level := C_rec_item.item_level;
         L_tran_level := C_rec_item.tran_level;

         if VALIDATE_AND_INSERT(L_item) = FALSE then
            RETURN FALSE;
         end if;

      END LOOP;  -- C_rec_item
   elsif I_maint_type = 'DELETE' then
      SQL_LIB.SET_MARK('FETCH',
                       'C_EXPLODE_ITEMLIST',
                       'SKULIST_DETAIL',
                       'ITEM LIST: ' || to_char(I_itemlist));

      FOR C_rec_item in C_EXPLODE_ITEMLIST LOOP
         L_item       := C_rec_item.item;
         L_item_level := C_rec_item.item_level;
         L_tran_level := C_rec_item.tran_level;

         if CHECK_REQUIRED_UDA(O_error_message,
                               L_required,
                               I_uda_id,
                               L_item,
                               NULL,
                               NULL,
                               NULL) = FALSE then
            RETURN FALSE;
         end if;

         if L_required then
            if I_display_type = 'LV' then -- count number of values for given item.
               SQL_LIB.SET_MARK('OPEN',
                                'C_COUNT_UDA_VALS_FOR_ITEM_LOV',
                                'UDA_ITEM_LOV',
                                'UDA ID: '      || to_char(I_uda_id)    ||
                                ', ITEM: '      || (L_item));

               open C_COUNT_UDA_VALS_FOR_ITEM_LOV;

               SQL_LIB.SET_MARK('FETCH',
                                'C_COUNT_UDA_VALS_FOR_ITEM_LOV',
                                'UDA_ITEM_LOV',
                                'UDA ID: '      || to_char(I_uda_id)    ||
                                ', ITEM: '      || (L_item));

               fetch C_COUNT_UDA_VALS_FOR_ITEM_LOV into L_count;

               if L_count > 1 then
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_COUNT_UDA_VALS_FOR_ITEM_LOV',
                                   'UDA_ITEM_LOV',
                                   'UDA ID: '      || to_char(I_uda_id)    ||
                                   ', ITEM: '      || (L_item));

                  close C_COUNT_UDA_VALS_FOR_ITEM_LOV;

                  if UPDATE_RECORD = FALSE then
                     RETURN FALSE;
                  end if;
               else  -- Count of UDA Value is 1:
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_COUNT_UDA_VALS_FOR_ITEM_LOV',
                                   'UDA_ITEM_LOV',
                                   'UDA ID: '      || to_char(I_uda_id)    ||
                                   ', ITEM: '      || (L_item));

                  close C_COUNT_UDA_VALS_FOR_ITEM_LOV;

                  O_reject := TRUE;

                  if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                            L_item,
                                                            NULL,
                                                            NULL,
                                                            'U',
                                                            'NO_REQUIRED_UDA',
                                                            L_user_id,
                                                            L_item,
                                                            I_uda_id,
                                                            NULL) = FALSE then
                      RETURN FALSE;
                  end if;

               end if;  -- Count UDA Values.
            elsif I_display_type = 'FF' then  -- count number of values for given item.
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_COUNT_UDA_VALS_FOR_ITEM_FF',
                                   'UDA_ITEM_FF',
                                   'UDA ID: '      || to_char(I_uda_id)    ||
                                   ', ITEM: '      || (L_item));
                  open C_COUNT_UDA_VALS_FOR_ITEM_FF;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_COUNT_UDA_VALS_FOR_ITEM_FF',
                                   'UDA_ITEM_FF',
                                   'UDA ID: '      || to_char(I_uda_id)    ||
                                   ', ITEM: '      || (L_item));
                  fetch C_COUNT_UDA_VALS_FOR_ITEM_FF into L_count;
                  SQL_LIB.SET_MARK('CLOSE',
                                    'C_COUNT_UDA_VALS_FOR_ITEM_FF',
                                    'UDA_ITEM_FF',
                                    'UDA ID: '      || to_char(I_uda_id)    ||
                                    ', ITEM: '      || (L_item));
                     
                  close C_COUNT_UDA_VALS_FOR_ITEM_FF;
                  if L_count > 1 then
                  
                     if UPDATE_RECORD = FALSE then
                        RETURN FALSE;
                     end if;
                  else -- Count of UDA Value is 1:
                     O_reject := TRUE;                   
                     
                     if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                               L_item,
                                                               NULL,
                                                               NULL,
                                                               'U',
                                                               'NO_REQUIRED_UDA',
                                                               L_user_id,
                                                               L_item,
                                                               I_uda_id,
                                                               NULL) = FALSE then
                         RETURN FALSE;
                     end if;
                  end if;
            else -- Display type is 'DT'
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_COUNT_UDA_VALS_FOR_ITEM_DT',
                                   'UDA_ITEM_DATE',
                                   'UDA ID: '      || to_char(I_uda_id)    ||
                                   ', ITEM: '      || (L_item));
                  open C_COUNT_UDA_VALS_FOR_ITEM_DT;

                  SQL_LIB.SET_MARK('FETCH',
                                   'C_COUNT_UDA_VALS_FOR_ITEM_DT',
                                   'UDA_ITEM_DATE',
                                   'UDA ID: '      || to_char(I_uda_id)    ||
                                   ', ITEM: '      || (L_item));
                  fetch C_COUNT_UDA_VALS_FOR_ITEM_DT into L_count;
                  
                  SQL_LIB.SET_MARK('CLOSE',
                                      'C_COUNT_UDA_VALS_FOR_ITEM_DT',
                                      'UDA_ITEM_DATE',
                                      'UDA ID: '      || to_char(I_uda_id)    ||
                                      ', ITEM: '      || (L_item));
                     
                  close C_COUNT_UDA_VALS_FOR_ITEM_DT;
                  if L_count > 1 then
                     
                     if UPDATE_RECORD = FALSE then
                        RETURN FALSE;
                     end if;
                  else -- Count of UDA Value is 1:
                     O_reject := TRUE;                  
                     if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                               L_item,
                                                               NULL,
                                                               NULL,
                                                               'U',
                                                               'NO_REQUIRED_UDA',
                                                               L_user_id,
                                                               L_item,
                                                               I_uda_id,
                                                               NULL) = FALSE then
                         RETURN FALSE;
                     end if;
                  end if;
            end if;  -- Display type.
         else  -- L_required is FALSE:
            if UPDATE_RECORD = FALSE then
               RETURN FALSE;
            end if;
         end if;  -- L_required.
      END LOOP;  -- C_rec_item
   elsif I_maint_type = 'CREATE' then
      SQL_LIB.SET_MARK('FETCH',
                       'C_EXPLODE_ITEMLIST',
                       'SKULIST_DETAIL',
                       'ITEM LIST: ' || to_char(I_itemlist));

      FOR C_rec_item in C_EXPLODE_ITEMLIST LOOP
         L_item       := C_rec_item.item;
         L_item_level := C_rec_item.item_level;
         L_tran_level := C_rec_item.tran_level;

         if VALIDATE_AND_INSERT(L_item) = FALSE then
            RETURN FALSE;
         end if;
      END LOOP;  -- C_rec_item
   elsif I_maint_type = 'CHANGE' then
      SQL_LIB.SET_MARK('FETCH',
                       'C_EXPLODE_ITEMLIST',
                       'SKULIST_DETAIL',
                       'ITEM LIST: ' || to_char(I_itemlist));

      FOR C_rec_item in C_EXPLODE_ITEMLIST LOOP
         L_item       := C_rec_item.item;
         L_item_level := C_rec_item.item_level;
         L_tran_level := C_rec_item.tran_level;

         if UPDATE_RECORD = FALSE then
            RETURN FALSE;
         end if;
      END LOOP;  -- C_rec_item
   end if;  --I_maint_type = ...

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.PERFORM_MC',
                                             to_char(SQLCODE));
      RETURN FALSE;

END PERFORM_MC;
------------------------------------------------------------------------------------------
FUNCTION CHECK_REQD_NO_VALUE( O_error_message     IN OUT  VARCHAR2,
                              O_reqd_no_value     IN OUT  VARCHAR2,
                              I_item              IN      UDA_ITEM_LOV.ITEM%TYPE,
                              I_dept              IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                              I_class             IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                              I_subclass          IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                              RETURN BOOLEAN IS

   L_dummy         VARCHAR2(3);
   L_uda_id        UDA.UDA_ID%TYPE;
   L_uda_value     UDA_ITEM_DEFAULTS.UDA_VALUE%TYPE;

   cursor C_GET_DEFAULTS is
      select udid.uda_id,
             u.display_type,
             udid.uda_value
        from uda_item_defaults udid,
             uda u
       where udid.uda_id                      = u.uda_id
         and udid.dept                        = I_dept
         and nvl(udid.class, I_class)         = I_class
         and nvl(udid.subclass, I_subclass)   = I_subclass
         and udid.required_ind                = 'Y';

   cursor C_CHECK_LV IS
      select 'x'
        from uda_item_lov
       where item   = I_item
         and uda_id = L_uda_id
         and (   uda_value = L_uda_value
              or L_uda_value is NULL);

   cursor C_CHECK_FF IS
      select 'x'
        from uda_item_ff
       where item   = I_item
         and uda_id = L_uda_id;

   cursor C_CHECK_DT IS
      select 'x'
        from uda_item_date
       where item   = I_item
         and uda_id = L_uda_id;

BEGIN
   for C_REC in C_GET_DEFAULTS LOOP
      L_uda_id := C_REC.uda_id;
      L_uda_value := C_REC.uda_value;

      if C_REC.DISPLAY_TYPE = 'LV' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_LV',
                          'UDA_ITEM_LOV',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id) ||
                          ', UDA_VALUE: '   || to_char(L_uda_value));

         open  C_CHECK_LV;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_LV',
                          'UDA_ITEM_LOV',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id) ||
                          ', UDA_VALUE: '   || to_char(L_uda_value));

         fetch C_CHECK_LV into L_dummy;

         if C_CHECK_LV%NOTFOUND then
            O_reqd_no_value := 'Y';
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_LV',
                          'UDA_ITEM_LOV',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id) ||
                          ', UDA_VALUE: '   || to_char(L_uda_value));

         close C_CHECK_LV;

         if O_reqd_no_value = 'Y' then
            exit;
         end if;

      elsif C_REC.DISPLAY_TYPE = 'FF' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_FF',
                          'UDA_ITEM_FF',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id));

         open  C_CHECK_FF;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_FF',
                          'UDA_ITEM_FF',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id));

         fetch C_CHECK_FF into L_dummy;

         if C_CHECK_FF%NOTFOUND then
            O_reqd_no_value := 'Y';
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_FF',
                          'UDA_ITEM_FF',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id));

         close C_CHECK_FF;

         if O_reqd_no_value = 'Y' then
            exit;
         end if;

      elsif C_REC.DISPLAY_TYPE = 'DT' then

         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_DT',
                          'UDA_ITEM_DATE',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id));

         open  C_CHECK_DT;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_DT',
                          'UDA_ITEM_DATE',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id));

         fetch C_CHECK_DT into L_dummy;

         if C_CHECK_DT%NOTFOUND then
            O_reqd_no_value := 'Y';
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_DT',
                          'UDA_ITEM_DATE',
                          ' ITEM: '         || to_char(I_item)   ||
                          ', UDA_ID: '      || to_char(L_uda_id));

         close C_CHECK_DT;

         if O_reqd_no_value = 'Y' then
            exit;
         end if;

      end if; -- C_REC.display_type
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.CHECK_REQD_NO_VALUE',
                                             to_char(SQLCODE));
      RETURN FALSE;

END CHECK_REQD_NO_VALUE;

---------------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_UDA(O_error_message  IN OUT  VARCHAR2,
                              I_item           IN      UDA_ITEM_LOV.ITEM%TYPE)
                              return BOOLEAN IS

   L_program      VARCHAR2(60) := 'UDA_SQL.COPY_DOWN_PARENT_UDA';
   L_table        VARCHAR2(65);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_UDA_ITEM_LOV is
      select 'x'
        from uda_item_lov uda1
       where uda1.item in (select item
                             from item_master
                            where (item_parent = I_item
                               or item_grandparent = I_item)
                              and item_level <= tran_level)
                              and not exists (select 1
                                                from uda_item_lov uda2,
                                                     item_master im1
                                               where (uda2.item = im1.item_parent
                                                  or uda2.item =  im1.item_grandparent)
                                                 and uda2.uda_id = uda1.uda_id
                                                 and im1.item = uda1.item
                                                 and uda2.uda_value = uda1.uda_value)
         for update nowait;

   cursor C_LOCK_UDA_ITEM_DATE is
      select 'x'
        from uda_item_date uda1
       where uda1.item in (select item
                             from item_master
                            where (item_parent = I_item
                               or item_grandparent = I_item)
                              and item_level <= tran_level)
                              and not exists (select 1
                                                from uda_item_date uda2,
                                                     item_master im1
                                               where (uda2.item = im1.item_parent
                                                  or uda2.item = im1.item_grandparent)
                                                 and uda2.uda_id = uda1.uda_id
                                                 and im1.item = uda1.item
                                                 and uda2.uda_date = uda1.uda_date)
         for update nowait;

   cursor C_LOCK_UDA_ITEM_FF_TL is
      select 'x'
        from uda_item_ff_tl uda1
       where uda1.item in (select item
                             from item_master
                            where (item_parent = I_item
                               or item_grandparent = I_item)
                              and item_level <= tran_level)
                              and not exists (select 1
                                                from uda_item_ff uda2,
                                                     item_master im1
                                               where (uda2.item = im1.item_parent
                                                  or uda2.item = im1.item_grandparent)
                                                 and uda2.uda_id = uda1.uda_id
                                                 and im1.item = uda1.item
                                                 and uda2.uda_text = uda1.uda_text)
         for update nowait;
       
   cursor C_LOCK_UDA_ITEM_FF is
      select 'x'
        from uda_item_ff uda1
       where uda1.item in (select item
                             from item_master
                            where (item_parent = I_item
                               or item_grandparent = I_item)
                              and item_level <= tran_level)
                              and not exists (select 1
                                                from uda_item_ff uda2,
                                                     item_master im1
                                               where (uda2.item = im1.item_parent
                                                  or uda2.item = im1.item_grandparent)
                                                 and uda2.uda_id = uda1.uda_id
                                                 and im1.item = uda1.item
                                                 and uda2.uda_text = uda1.uda_text)
         for update nowait;

BEGIN
   L_table := 'UDA_ITEM_LOV';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_UDA_ITEM_LOV',
                    L_table,
                    'Item: '||I_item);
   open C_LOCK_UDA_ITEM_LOV;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_UDA_ITEM_LOV',
                    L_table,
                    'Item: '||I_item);
   close C_LOCK_UDA_ITEM_LOV;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   delete from uda_item_lov uda1
         where uda1.item in (select im.item
                               from item_master im
                              where (im.item_parent = I_item or im.item_grandparent = I_item)
                                and im.item_level <= im.tran_level)
                                and not exists (select 1
                                                  from uda_item_lov uda2,
                                                       item_master im1
                                                 where (uda2.item = I_item)
                                                   and uda2.uda_id = uda1.uda_id
                                                   and im1.item = uda1.item
                                                   and uda2.uda_value = uda1.uda_value);
   ---
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   insert into uda_item_lov(item,
                            uda_id,
                            uda_value,
                            create_datetime,
                            last_update_datetime,
                            last_update_id)
                     select im.item,
                            uda1.uda_id,
                            uda1.uda_value,
                            sysdate,
                            sysdate,
                            get_user
                       from uda_item_lov uda1,
                            item_master im
                      where (im.item_parent = uda1.item
                         or im.item_grandparent = uda1.item)
                        and im.item_level <= im.tran_level
                        and uda1.item = I_item
                        and not exists (select 1
                                          from uda_item_lov uda2,
                                               item_master im1
                                         where im.item = im1.item
                                           and im1.item = uda2.item
                                           and uda2.uda_id = uda1.uda_id
                                           and uda2.uda_value = uda1.uda_value);
   ---
   L_table := 'UDA_ITEM_DATE';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_UDA_ITEM_DATE',
                    L_table,
                    'Item: '||I_item);
   open C_LOCK_UDA_ITEM_DATE;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_UDA_ITEM_DATE',
                    L_table,
                    'Item: '||I_item);
   close C_LOCK_UDA_ITEM_DATE;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   delete from uda_item_date uda1
         where uda1.item in (select im.item
                               from item_master im
                              where (im.item_parent = I_item
                                 or im.item_grandparent = I_item)
                                and im.item_level <= im.tran_level)
                                and not exists (select 1
                                                  from uda_item_date uda2,
                                                       item_master im1
                                                 where uda2.item = I_item
                                                   and uda2.uda_id = uda1.uda_id
                                                   and im1.item = uda1.item
                                                   and uda2.uda_date = uda1.uda_date);
   ---
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   insert into uda_item_date(item,
                             uda_id,
                             uda_date,
                             create_datetime,
                             last_update_datetime,
                             last_update_id)
                      select im.item,
                             uda1.uda_id,
                             uda1.uda_date,
                             sysdate,
                             sysdate,
                             get_user
                        from item_master im,
                             uda_item_date uda1
                       where (im.item_parent = uda1.item
                          or im.item_grandparent = uda1.item)
                         and im.item_level <= im.tran_level
                         and uda1.item = I_item
                         and not exists (select 1
                                           from uda_item_date uda2,
                                                item_master im1
                                          where im.item = im1.item
                                            and im1.item = uda2.item
                                            and uda2.uda_id = uda1.uda_id
                                            and uda2.uda_date = uda1.uda_date);
   ---
   L_table := 'UDA_ITEM_FF_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_UDA_ITEM_FF_TL',
                    L_table,
                    'Item: '||I_item);
   open C_LOCK_UDA_ITEM_FF_TL;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_UDA_ITEM_FF_TL',
                    L_table,
                    'Item: '||I_item);
   close C_LOCK_UDA_ITEM_FF_TL;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   
   delete from uda_item_ff_tl uda1
         where uda1.item in (select im.item
                               from item_master im
                              where (im.item_parent = I_item
                                 or im.item_grandparent = I_item)
                                and im.item_level <= im.tran_level)
                                and not exists (select 1
                                                  from uda_item_ff uda2,
                                                       item_master im1
                                                 where uda2.item = I_item
                                                   and uda2.uda_id = uda1.uda_id
                                                   and im1.item = uda1.item
                                                   and uda2.uda_text = uda1.uda_text);
   ---
   L_table := 'UDA_ITEM_FF';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_UDA_ITEM_FF',
                    L_table,
                    'Item: '||I_item);
   open C_LOCK_UDA_ITEM_FF;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_UDA_ITEM_FF',
                    L_table,
                    'Item: '||I_item);
   close C_LOCK_UDA_ITEM_FF;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   delete from uda_item_ff uda1
         where uda1.item in (select im.item
                               from item_master im
                              where (im.item_parent = I_item
                                 or im.item_grandparent = I_item)
                                and im.item_level <= im.tran_level)
                                and not exists (select 1
                                                  from uda_item_ff uda2,
                                                       item_master im1
                                                 where uda2.item = I_item
                                                   and uda2.uda_id = uda1.uda_id
                                                   and im1.item = uda1.item
                                                   and uda2.uda_text = uda1.uda_text);
   ---
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   insert into uda_item_ff(item,
                           uda_id,
                           uda_text,
                           create_datetime,
                           last_update_datetime,
                           last_update_id)
                    select im.item,
                           uda1.uda_id,
                           uda1.uda_text,
                           sysdate,
                           sysdate,
                           get_user
                      from item_master im,
                           uda_item_ff uda1
                      where (im.item_parent = uda1.item
                         or im.item_grandparent = uda1.item)
                        and im.item_level <= im.tran_level
                        and uda1.item = I_item
                        and not exists (select 1
                                           from uda_item_ff uda2,
                                                item_master im1
                                          where im.item = im1.item
                                            and im1.item = uda2.item
                                            and uda2.uda_id = uda1.uda_id
                                            and uda2.uda_text = uda1.uda_text);
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COPY_DOWN_PARENT_UDA;
--------------------------------------------------------------------------------------------
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ITEM_MASTER
-- which only returns data that the user has permission to access.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMUDA_MERCH_HIER(O_error_message    IN OUT VARCHAR2,
                                     O_valid            IN OUT BOOLEAN,
                                     I_item             IN     V_ITEM_MASTER.ITEM%TYPE,
                                     I_uda_id           IN     UDA.UDA_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'UDA_SQL.VALIDATE_ITEMUDA_MERCH_HIER';
   L_exists       VARCHAR2(1) := 'N';

   cursor C_item_uda_exist is
      select 'Y'
        from v_item_master vim,
             uda u,
             system_options so
       where ((so.uda_merch_level_code = 'D' and vim.division = nvl(u.filter_merch_id, vim.division))
              or (so.uda_merch_level_code = 'G' and vim.group_no = nvl(u.filter_merch_id, vim.group_no))
	      or (so.uda_merch_level_code = 'P' and vim.dept = nvl(u.filter_merch_id, vim.dept))
	      or (so.uda_merch_level_code = 'C' and vim.dept = nvl(u.filter_merch_id, vim.dept)
	                                        and vim.class = nvl(u.filter_merch_id_class, vim.class))
	      or (so.uda_merch_level_code = 'S' and vim.dept = nvl(u.filter_merch_id, vim.dept)
	                                        and vim.class = nvl(u.filter_merch_id_class, vim.class)
	                                        and vim.subclass = nvl(u.filter_merch_id_subclass, vim.subclass)))
         and u.uda_id = I_uda_id
         and vim.item = I_item;

BEGIN
   ---
   if I_item IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_uda_id IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_uda_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_item_uda_exist;
   fetch C_item_uda_exist into L_exists;
   close C_item_uda_exist;
   ---
   if L_exists = 'Y' then
      O_valid := TRUE;
   else
      O_valid := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_UDA_MERCH_HIER',
                                            I_item,
                                            I_uda_id,
                                            NULL);
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_ITEMUDA_MERCH_HIER;
--------------------------------------------------------------------------------------------
FUNCTION GET_UDA_INFO(O_error_message    IN OUT VARCHAR2,
                      O_uda_row          IN OUT UDA%ROWTYPE,
                      I_uda_id           IN     UDA.UDA_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'UDA_SQL.GET_UDA_INFO';

   cursor C_uda_row is
      select *
        from uda
       where uda_id = I_uda_id;

BEGIN
   ---
   if I_uda_id IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_uda_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_uda_row;
   fetch C_uda_row into O_uda_row;
   ---
   if C_uda_row%NOTFOUND then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('NO_UDA_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_uda_row;
      return FALSE;
   end if;
   ---
   close C_uda_row;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_UDA_INFO;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_UDA(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists        IN OUT  BOOLEAN,
                   I_item          IN      UDA_ITEM_LOV.ITEM%TYPE,
                   I_uda_id        IN      UDA.UDA_ID%TYPE,
                   I_uda_value     IN      UDA_ITEM_LOV.UDA_VALUE%TYPE,
                   I_uda_text      IN      UDA_ITEM_FF.UDA_TEXT%TYPE,
                   I_uda_date      IN      UDA_ITEM_DATE.UDA_DATE%TYPE)
RETURN BOOLEAN IS

   L_dummy  VARCHAR2(3);

   cursor C_CHECK_ITEM_LOV is
      select 'x'
        from uda_item_lov
       where item      = I_item
         and uda_id    = I_uda_id
         and uda_value = I_uda_value;

   cursor C_CHECK_ITEM_FF is
      select 'x'
        from uda_item_ff
       where item                  = I_item
         and uda_id                = I_uda_id
         and UPPER(TRIM(uda_text)) = UPPER(TRIM(I_uda_text)); -- to match two long strings as far as possible

   cursor C_CHECK_ITEM_DATE is
      select 'x'
        from uda_item_date
       where item      = I_item
         and uda_id    = I_uda_id
         and uda_date = I_uda_date;

BEGIN

   if I_uda_value is not NULL then

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_ITEM_LOV',
                       'UDA_ITEM_LOV',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      open  C_CHECK_ITEM_LOV;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_ITEM_LOV',
                       'UDA_ITEM_LOV',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      fetch C_CHECK_ITEM_LOV into L_dummy;

      if C_CHECK_ITEM_LOV%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_ITEM_LOV',
                       'UDA_ITEM_LOV',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      close C_CHECK_ITEM_LOV;

   elsif I_uda_text is not NULL then

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_ITEM_FF',
                       'UDA_ITEM_FF',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      open  C_CHECK_ITEM_FF;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_ITEM_FF',
                       'UDA_ITEM_FF',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      fetch C_CHECK_ITEM_FF into L_dummy;

      if C_CHECK_ITEM_FF%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_ITEM_FF',
                       'UDA_ITEM_FF',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      close C_CHECK_ITEM_FF;

   else

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_ITEM_DATE',
                       'UDA_ITEM_DATE',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      open  C_CHECK_ITEM_DATE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_ITEM_DATE',
                       'UDA_ITEM_DATE',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      fetch C_CHECK_ITEM_DATE into L_dummy;

      if C_CHECK_ITEM_DATE%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_ITEM_DATE',
                       'UDA_ITEM_DATE',
                       'UDA ID: ' || to_char(I_uda_id) ||
                       'ITEM: '   || I_item);

      close C_CHECK_ITEM_DATE;

   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.CHECK_UDA',
                                             to_char(SQLCODE));
      RETURN FALSE;

END CHECK_UDA;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_DEFAULTS(O_error_message  IN OUT  VARCHAR2,
                         I_item_tbl       IN      ITEM_TBL,
                         I_dept_tbl       IN      DEPT_TBL,
                         I_class_tbl      IN      DEPT_TBL,
                         I_subclass_tbl   IN      DEPT_TBL)
   RETURN BOOLEAN IS

BEGIN

   if I_item_tbl is NOT NULL and I_item_tbl.COUNT > 0 then
      FORALL i IN I_item_tbl.FIRST.. I_item_tbl.LAST
         insert into uda_item_lov(item,
                                  uda_id,
                                  uda_value,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
                           select I_item_tbl(i),
                                  a.uda_id,
                                  a.uda_value,
                                  sysdate,
                                  sysdate,
                                  get_user
                             from uda_item_defaults a,
                                  uda
                            where a.dept = I_dept_tbl(i)
                              and nvl(a.class, I_class_tbl(i)) = I_class_tbl(i)
                              and nvl(a.subclass, I_subclass_tbl(i)) = I_subclass_tbl(i)
                              and a.uda_value is not NULL
                              and uda.uda_id = a.uda_id
                              and uda.display_type = 'LV'
                              and a.hierarchy_value = (select max(b.hierarchy_value)
                                                         from uda_item_defaults b
                                                        where b.dept = I_dept_tbl(i)
                                                          and nvl(b.class, I_class_tbl(i)) = I_class_tbl(i)
                                                          and nvl(b.subclass, I_subclass_tbl(i)) = I_subclass_tbl(i)
                                                          and b.uda_id = a.uda_id);
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.INSERT_DEFAULTS',
                                             to_char(SQLCODE));
      RETURN FALSE;

END INSERT_DEFAULTS;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_UDA_EXISTS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists        IN OUT  BOOLEAN,
                          I_item          IN      UDA_ITEM_LOV.ITEM%TYPE)
RETURN BOOLEAN IS

   L_dummy  VARCHAR2(3);

   cursor C_CHECK_UDA_EXISTS is
      select 'x'
        from uda_item_lov
       where item = I_item
	  union all
      select 'x'
        from uda_item_ff
       where item = I_item
      union all
      select 'x'
        from uda_item_date
       where item = I_item;

BEGIN

   O_exists := FALSE;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_UDA_EXISTS',
					          'UDA_ITEM_LOV,'||' UDA_ITEM_FF and'||' UDA_ITEM_DATE',
                    'Item: '||I_item);

   open  C_CHECK_UDA_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_UDA_EXISTS',
                    'UDA_ITEM_LOV,'||' UDA_ITEM_FF and'||' UDA_ITEM_DATE',
                    'Item: '||I_item);
   
   fetch C_CHECK_UDA_EXISTS into L_dummy;

   if C_CHECK_UDA_EXISTS%FOUND then
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_UDA_EXISTS',
					          'UDA_ITEM_LOV,'||' UDA_ITEM_FF and'||' UDA_ITEM_DATE',
                    'Item: '||I_item);
   
   close C_CHECK_UDA_EXISTS;

   RETURN TRUE;

EXCEPTION
   when OTHERS then

      if C_CHECK_UDA_EXISTS%ISOPEN then
         close C_CHECK_UDA_EXISTS;
      end if;
   
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'UDA_SQL.CHECK_UDA_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;

END CHECK_UDA_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_UDA_ITEM_FF_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item          IN       UDA_ITEM_FF_TL.ITEM%TYPE, 
                                 I_uda_id        IN       UDA_ITEM_FF_TL.UDA_ID%TYPE,
                                 I_uda_text      IN       UDA_ITEM_FF_TL.UDA_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'UDA_SQL.DELETE_UDA_ITEM_FF_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_UDA_ITEM_FF_TL is
      select 'x'
        from uda_item_ff_tl
       where item = I_item 
       and uda_id = I_uda_id 
       and uda_text = I_uda_text
       for update nowait;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_uda_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_uda_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_uda_text is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_uda_text',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   L_table := 'UDA_ITEM_FF_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_UDA_ITEM_FF_TL',
                    L_table,
                    'I_item '||I_item||' I_uda_id '||I_uda_id || ' I_uda_text ' ||I_uda_text);
                    
   open C_LOCK_UDA_ITEM_FF_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_UDA_ITEM_FF_TL',
                    L_table,
                   'I_item '||I_item||' I_uda_id '||I_uda_id || ' I_uda_text ' ||I_uda_text);
   close C_LOCK_UDA_ITEM_FF_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'UDA_ITEM_FF_TL',
                    'I_item '||I_item||' I_uda_id '||I_uda_id || ' I_uda_text ' ||I_uda_text);
                    
   delete from uda_item_ff_tl
    where item = I_item 
    and uda_id = I_uda_id
    and uda_text = I_uda_text;

   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_UDA_ITEM_FF_TL;
---------------------------------------------------------------------------------------------
END UDA_SQL;
/