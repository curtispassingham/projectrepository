
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SUPPLIER_FIND_SQL AS

--------------------------------------------------------------------------------------------
-- Function: QUERY_PROCEDURE
-- Purpose : This procedure will return a table type with data required to be displayed on the
--           Supplier Find screen - results section. The data is fetched from sups table
--           for the condition passed as input and is ordered based on the input.
--------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE(IO_supplier_tbl   IN OUT   SUPPLIER_FIND_SQL.SUPPLIER_TBL,
                          I_where_clause    IN       VARCHAR2,
                          I_order_by        IN       VARCHAR2,
                          I_order_type      IN       VARCHAR2)

IS
   L_main_cursor           VARCHAR2(2000);
   L_where_clause          VARCHAR2(2000) := I_where_clause;
   L_order_by              VARCHAR2(2000);
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE   := NULL;
   L_package_call_failed   EXCEPTION;

BEGIN
   L_main_cursor := 'select '||
                     'TO_NUMBER(NVL(sup.supplier_parent,sup.supplier)) supplier,'||
                     'NVL(sups2.sup_name,sup.sup_name) sup_name,'||
                     'NVL(sups2.sup_name_secondary,sup.sup_name_secondary) sup_secondary_name,'||
                     'TO_NUMBER(DECODE(sup.supplier_parent,NULL,NULL,sup.supplier)) supplier_site,'||
                     'DECODE(sup.supplier_parent,NULL,NULL,sup.sup_name) sup_site_name,'||
                     'sup.contact_name,'||
                     'sup.contact_phone,'||
                     'sup.contact_fax,'||
                     'sup.contact_pager,'||
                     'sup.sup_status,'||
                     'NULL,'||
                     'sup.qc_ind,'||
                     'sup.qc_pct,'||
                     'sup.qc_freq,'||
                     'sup.vc_ind,'||
                     'sup.vc_pct,'||
                     'sup.vc_freq,'||
                     'sup.currency_code,'||
                     'NULL,'||
                     'sup.lang,'||
                     'sup.terms,'||
                     'NULL,'||
                     'NULL,'||
                     'sup.freight_terms,'||
                     'NULL,'||
                     'sup.ret_allow_ind,'||
                     'sup.ret_auth_req,'||
                     'sup.ret_min_dol_amt,'||
                     'sup.ret_courier,'||
                     'sup.handling_pct,'||
                     'sup.edi_po_ind,'||
                     'sup.edi_po_chg,'||
                     'sup.edi_po_confirm,'||
                     'sup.edi_asn,'||
                     'sup.edi_sales_rpt_freq,'||
                     'NULL,'||
                     'sup.edi_supp_available_ind,'||
                     'sup.edi_contract_ind,'||
                     'sup.edi_invc_ind,'||
                     'sup.edi_channel_id,'||
                     'sup.cost_chg_pct_var,'||
                     'sup.cost_chg_amt_var,'||
                     'sup.replen_approval_ind,'||
                     'sup.ship_method,'||
                     'sup.payment_method,'||
                     'sup.contact_telex,'||
                     'sup.contact_email,'||
                     'sup.settlement_code,'||
                     'sup.pre_mark_ind,'||
                     'sup.auto_appr_invc_ind,'||
                     'sup.dbt_memo_code,'||
                     'sup.freight_charge_ind,'||
                     'sup.auto_appr_dbt_memo_ind,'||
                     'sup.prepay_invc_ind,'||
                     'sup.backorder_ind,'||
                     'sup.vat_region,'||
                     'sup.inv_mgmt_lvl,'||
                     'sup.service_perf_req_ind,'||
                     'sup.invc_pay_loc,'||
                     'sup.invc_receive_loc,'||
                     'sup.addinvc_gross_net,'||
                     'sup.delivery_policy,'||
                     'sup.comment_desc,'||
                     'sup.default_item_lead_time,'||
                     'sup.duns_number,'||
                     'sup.duns_loc,'||
                     'sup.bracket_costing_ind,'||
                     'NULL,'||
                     'sup.vmi_order_status,'||
                     'sup.dsd_ind,'||
                     'sups2.external_ref_id,'||
                     'NULL,'||
                     '0 '||
                     'from v_sups sup, ' ||
                     '(select s.supplier, suptl.sup_name,suptl.sup_name_secondary,s.external_ref_id ' ||
                     'from sups s,v_sups_tl suptl ' ||
                     'where s.supplier = suptl.supplier) sups2 '||
                     'where sup.supplier_parent=sups2.supplier(+)';
   

   L_order_by := 'ORDER BY '||sys.DBMS_ASSERT.SIMPLE_SQL_NAME(I_order_by)||' '||sys.DBMS_ASSERT.SIMPLE_SQL_NAME(I_order_type);
   
   EXECUTE IMMEDIATE L_main_cursor||' '||L_where_clause||' '||L_order_by bulk collect into IO_supplier_tbl ;
   

   for i in 1..IO_supplier_tbl.COUNT
   loop

      if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                    'SPST',
                                    IO_supplier_tbl(i).sup_status,
                                    IO_supplier_tbl(i).sup_status_desc) = FALSE then
         raise L_package_call_failed;
      end if;
      ---
      if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                    'YSNO',
                                    IO_supplier_tbl(i).bracket_costing_ind,
                                    IO_supplier_tbl(i).bracket_costing_desc) = FALSE then
         raise L_package_call_failed;
      end if;
      ---
   
      
      if SUPP_ATTRIB_SQL.GET_PAYMENT_DESC(L_error_message,
                                          IO_supplier_tbl(i).currency_code,
                                          IO_supplier_tbl(i).terms,
                                          IO_supplier_tbl(i).freight_terms,
                                          IO_supplier_tbl(i).currency_desc,
                                          IO_supplier_tbl(i).terms_code,
                                          IO_supplier_tbl(i).terms_desc,
                                          IO_supplier_tbl(i).freight_terms_desc) = FALSE then
         raise L_package_call_failed;
      end if;
      --translate for the current value of edi_sales_rpt_freq
      if IO_supplier_tbl(i).edi_sales_rpt_freq is NOT NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                       'ESDF', --code type
                                       IO_supplier_tbl(i).edi_sales_rpt_freq,  --code
                                       IO_supplier_tbl(i).edi_sales_rpt_freq_desc) = FALSE then
            raise L_package_call_failed;
         end if;
      end if;
      ---
   end loop;

EXCEPTION
   when L_package_call_failed then
      IO_supplier_tbl.DELETE;
      IO_supplier_tbl(1).error_message := L_error_message;
      IO_supplier_tbl(1).return_code := -1;
   when OTHERS then
      IO_supplier_tbl.DELETE;
      IO_supplier_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                             SQLERRM,
                                                             'SUPPLIER_FIND_SQL.QUERY_PROCEDURE',
                                                             to_char(SQLCODE));
      IO_supplier_tbl(1).return_code := sqlcode;
END QUERY_PROCEDURE;
------------------------------------------------------------------------------------------------------------------

END SUPPLIER_FIND_SQL;
/

