CREATE OR REPLACE PACKAGE CORESVC_COUNTRY_ATTR AUTHID CURRENT_USER AS
   template_key           CONSTANT VARCHAR2(25) := 'COUNTRY_ATTR_DATA';
   template_category      CONSTANT VARCHAR2(25) := 'RMSFND';
   action_new             VARCHAR2(25)          := 'NEW';
   action_mod             VARCHAR2(25)          := 'MOD';
   action_del             VARCHAR2(25)          := 'DEL';
   COUNTRY_ATTR_sheet                  VARCHAR2(25)          := 'COUNTRY_ATTR';
   COUNTRY_ATTR$Action                 NUMBER                := 1;
   COUNTRY_ATTR$DEFAULT_LOC_TYP        NUMBER                := 10;
   COUNTRY_ATTR$DEFAULT_LOC            NUMBER                := 9;
   CNTR_ATTR$DEF_COST_COMP_COST        NUMBER                := 8;
   COUNTRY_ATTR$DEFAULT_DEAL_COST      NUMBER                := 7;
   COUNTRY_ATTR$DEFAULT_PO_COST        NUMBER                := 6;
   CNTR_ATTR$ITM_CST_TAX_INCL_IND      NUMBER                := 5;
   COUNTRY_ATTR$LOCALIZED_IND          NUMBER                := 4;
   COUNTRY_ATTR$COUNTRY_DESC           NUMBER                := 3;
   COUNTRY_ATTR$COUNTRY_ID             NUMBER                := 2;
   CTRY_TL_sheet                       VARCHAR2(25)          := 'COUNTRY_TL';
   CTRY_TL$Action                      NUMBER                := 1;
   CTRY_TL$LANG                        NUMBER                := 2;
   CTRY_TL$COUNTRY_ID                  NUMBER                := 3;
   CTRY_TL$COUNTRY_DESC                NUMBER                := 4;

   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(25) := 'ACTION';


FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN;

FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN;

FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN;

FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2;

END CORESVC_COUNTRY_ATTR;

/
