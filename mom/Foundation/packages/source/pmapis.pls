CREATE OR REPLACE PACKAGE PM_API_SQL AUTHID CURRENT_USER IS

--------------------------------------------------------------------------------
-- Data Types
--------------------------------------------------------------------------------

-- These record types should have the same columns as the Oracle objects.  We
-- need to use these record types because Oracle objects are not supported in
-- forms.  Any changes to the objects should be reflected here, and vice versa.

TYPE PROMOTION_REC IS RECORD(PROMO_ID          NUMBER(10),
                             PROMO_DISPLAY_ID  VARCHAR2(10),
                             STATE             VARCHAR2(50),
                             NAME              VARCHAR2(160),
                             CURRENCY_CODE     VARCHAR2(3),
                             PROMO_EVENT_ID    NUMBER(6),
                             START_DATE        DATE,
                             END_DATE          DATE);

TYPE COMPONENT_REC IS RECORD(PROMO_COMP_ID    NUMBER(10),
                             COMP_DISPLAY_ID  VARCHAR2(10),
                             PROMO_ID         NUMBER(10),
                             NAME             VARCHAR2(160));

TYPE RPM_SYSTEM_OPTIONS_REC IS RECORD(COMPLEX_PROMO_ALLOWED_IND  NUMBER(6));

--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_RPM_SYSTEM_OPTIONS(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_rpm_system_options_rec  IN OUT  PM_API_SQL.RPM_SYSTEM_OPTIONS_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_PROMOTION(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_promo_display_id IN      GTT_PROMO_TEMP.PROMO_DISPLAY_ID%TYPE,
                       O_promo_id         IN OUT  SA_TRAN_DISC.PROMOTION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_PROMO_DISPLAY_ID(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_promo_id         IN      GTT_PROMO_TEMP.PROMO_ID%TYPE,
                              O_promo_display_id IN OUT  SA_TRAN_DISC.PROMOTION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMOTION(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid           IN OUT  VARCHAR2,
                            O_promo_rec       IN OUT  PM_API_SQL.PROMOTION_REC,
                            I_check_status    IN      VARCHAR2,
                            I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMOTION_WRP(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT  VARCHAR2,
                                O_promo_rec       IN OUT  OBJ_PROMO_REC,
                                I_check_status    IN      VARCHAR2,
                                I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMO_COMP(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid           IN OUT  VARCHAR2,
                             O_promo_comp_rec  IN OUT  PM_API_SQL.COMPONENT_REC,
                             I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                             I_promo_comp_id   IN      SA_TRAN_DISC.PROMO_COMP%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMO_COMP_WRP(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid           IN OUT  VARCHAR2,
                                 O_promo_comp_rec  IN OUT  OBJ_PROMO_COMP_REC,
                                 I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                 I_promo_comp_id   IN      SA_TRAN_DISC.PROMO_COMP%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
-- Determines if a given deal exists on a promotion.  This function is used in
-- the driving cursor of dealprg.pc.
-- *** NOTE: This is a non-standard function in that it returns a VARCHAR2   ***
-- *** value which is used to indicate a deal/promo association              ***
--------------------------------------------------------------------------------
FUNCTION DEAL_PROMO_EXISTS(I_deal_id  IN  DEAL_HEAD.DEAL_ID%TYPE)
RETURN VARCHAR2;

PRAGMA RESTRICT_REFERENCES(DEAL_PROMO_EXISTS, WNDS);
--------------------------------------------------------------------------------
-- Function Name: VALIDATE_PROMOTION_EXIST
-- Purpose: This function will be used to check for the existence of a promotion.
--          This will be used to allow search of all promotions (open or closed)
--          in RMS find forms.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMOTION_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_valid           IN OUT   BOOLEAN,
                                  O_promo_name         OUT   GTT_PROMO_TEMP.NAME%TYPE,
                                  I_promo_id        IN       GTT_PROMO_TEMP.PROMO_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEMLIST_ACTIVE
-- Purpose: This function will be used to check for the existence of a itemlist.
--          This will be used to allow search of active itemlist in RPM
--          before allowing deletion in RMS.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMLIST_ACTIVE(O_error_message            OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_itemlist_active_ind      OUT   BOOLEAN,
                                  I_itemlist_id           IN       SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMOTION_FROM_HIST(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_valid           IN OUT  VARCHAR2,
                                      O_promo_rec       IN OUT  PM_API_SQL.PROMOTION_REC,
                                      I_check_status    IN      VARCHAR2,
                                      I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                      I_need_hist       IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VAL_PROMOTION_FROM_HIST_WRP(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_valid           IN OUT  VARCHAR2,
                                     O_promo_rec       IN OUT  OBJ_PROMO_REC,
                                     I_check_status    IN      VARCHAR2,
                                     I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                     I_need_hist       IN      NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMO_COMP_FROM_HIST(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_valid           IN OUT  VARCHAR2,
                                       O_promo_comp_rec  IN OUT  PM_API_SQL.COMPONENT_REC,
                                       I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                       I_promo_comp_id   IN      SA_TRAN_DISC.PROMO_COMP%TYPE,
                                       I_need_hist       IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VAL_PROMO_COMP_FROM_HIST_WRP(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_valid           IN OUT  VARCHAR2,
                                      O_promo_comp_rec  IN OUT  OBJ_PROMO_COMP_REC,
                                      I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                      I_promo_comp_id   IN      SA_TRAN_DISC.PROMO_COMP%TYPE,
                                      I_need_hist       IN      NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_PROMO_DETAILS (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid            IN OUT  VARCHAR2,
                            O_promo_display_id IN OUT  VARCHAR2,
                            O_promo_name       IN OUT  VARCHAR2,
                            O_currency_code    IN OUT  VARCHAR2,
                            O_start_date       IN OUT  DATE,
                            O_end_date         IN OUT  DATE,
                            I_promo_id         IN      SA_TRAN_DISC.PROMOTION%TYPE,
                            I_check_status     IN      VARCHAR2,
                            I_need_hist        IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_ZONE_GROUP_LOC(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_location         IN OUT  RPM_ZONE_LOCATION.LOCATION%TYPE,
                                    I_item             IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_RETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_unit_retail        IN OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                            O_currency_code      IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                            I_item               IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                            I_location           IN     RPM_FUTURE_RETAIL.LOCATION%TYPE,
                            I_effective_date     IN     DATE DEFAULT NULL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END PM_API_SQL;
/