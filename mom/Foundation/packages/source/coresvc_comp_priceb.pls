CREATE OR REPLACE PACKAGE BODY CORESVC_COMP_PRICE AS
----------------------------------------------------------------------------------------------
   cursor C_SVC_COMP_SHOP_LIST(I_process_id   NUMBER,
                               I_chunk_id     NUMBER) is
      select uk_comp_shop_list.rowid                         as uk_comp_shop_list_rid,
             st.rowid as st_rid,
             cd.rowid as cd_rid,
             st.shopper,
             st.shop_date,
             UPPER(st.item)                                  as item,
             uk_comp_shop_list.item_desc,
             st.ref_item,
             uk_comp_shop_list.competitor                    as old_competitor,
             nvl(st.competitor,uk_comp_shop_list.competitor) as competitor,
             uk_comp_shop_list.comp_name,
             st.comp_store,
             uk_comp_shop_list.comp_store_name,
             st.rec_date,
             st.comp_retail,
             uk_comp_shop_list.comp_retail                   as old_comp_retail,
             st.comp_retail_type,
             st.multi_units,
             st.multi_unit_retail,
             st.prom_start_date,
             st.prom_end_date,
             st.offer_type,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)                                as action,
             st.process$status
        from svc_comp_shop_list st,
             comp_shop_list uk_comp_shop_list,
             code_detail cd
       where st.process_id       = I_process_id
         and st.chunk_id         = I_chunk_id
         and st.process$status   = 'N'
         and st.comp_store       = uk_comp_shop_list.comp_store (+)
         and st.ref_item is NOT NULL
         and st.ref_item         = uk_comp_shop_list.ref_item (+)         
         and UPPER(st.item)      = UPPER(uk_comp_shop_list.item (+))
         and st.shop_date        = uk_comp_shop_list.shop_date (+)
         and st.shopper          = uk_comp_shop_list.shopper (+)
         and st.comp_retail_type = cd.code(+)
         and cd.code_type(+)     = 'PCTY'
       union all
      select uk_comp_shop_list.rowid                         as uk_comp_shop_list_rid,
             st.rowid as st_rid,
             cd.rowid as cd_rid,
             st.shopper,
             st.shop_date,
             UPPER(st.item)                                  as item,
             uk_comp_shop_list.item_desc,
             st.ref_item,
             uk_comp_shop_list.competitor                    as old_competitor,
             nvl(st.competitor,uk_comp_shop_list.competitor) as competitor,
             uk_comp_shop_list.comp_name,
             st.comp_store,
             uk_comp_shop_list.comp_store_name,
             st.rec_date,
             st.comp_retail,
             uk_comp_shop_list.comp_retail                   as old_comp_retail,
             st.comp_retail_type,
             st.multi_units,
             st.multi_unit_retail,
             st.prom_start_date,
             st.prom_end_date,
             st.offer_type,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)                                as action,
             st.process$status
        from svc_comp_shop_list st,
             comp_shop_list uk_comp_shop_list,
             code_detail cd
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.process$status     = 'N'
         and st.comp_store         = uk_comp_shop_list.comp_store (+)
         and st.ref_item is NULL
         and NVL(st.ref_item,'-1') = NVL(uk_comp_shop_list.ref_item,'-1')         
         and UPPER(st.item)        = UPPER(uk_comp_shop_list.item (+))
         and st.shop_date          = uk_comp_shop_list.shop_date (+)
         and st.shopper            = uk_comp_shop_list.shopper (+)
         and st.comp_retail_type   = cd.code(+)
         and cd.code_type(+)       = 'PCTY';

   cursor C_SVC_COMP_PRICE_HIST(I_process_id   NUMBER,
                                I_chunk_id     NUMBER) is
      select uk_comp_price_hist.rowid  as uk_comp_price_hist_rid,
             st.rowid as st_rid,
             cd.rowid as cd_rid,
             st.rec_date,
             st.comp_retail,
             st.comp_retail_type,
             st.multi_units,
             st.multi_unit_retail,
             st.prom_start_date,
             st.prom_end_date,
             st.offer_type,
             UPPER(st.item)            as item,
             st.ref_item,
             st.comp_store,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)          as action,
             st.process$status
        from svc_comp_price_hist st,
             comp_price_hist uk_comp_price_hist,
             code_detail cd
       where st.process_id       = I_process_id
         and st.chunk_id         = I_chunk_id
         and st.process$status   = 'N'
         and st.rec_date         = uk_comp_price_hist.rec_date (+)
         and st.comp_store       = uk_comp_price_hist.comp_store (+)
         and st.ref_item is NOT NULL
         and st.ref_item         = uk_comp_price_hist.ref_item (+)
         and UPPER(st.item)      = UPPER(uk_comp_price_hist.item (+))
         and st.comp_retail_type = cd.code(+)
         and cd.code_type(+)     = 'PCTY'
       union all
      select uk_comp_price_hist.rowid  as uk_comp_price_hist_rid,
             st.rowid as st_rid,
             cd.rowid as cd_rid,
             st.rec_date,
             st.comp_retail,
             st.comp_retail_type,
             st.multi_units,
             st.multi_unit_retail,
             st.prom_start_date,
             st.prom_end_date,
             st.offer_type,
             UPPER(st.item),
             st.ref_item,
             st.comp_store,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)          as action,
             st.process$status
        from svc_comp_price_hist st,
             comp_price_hist uk_comp_price_hist,
             code_detail cd
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.process$status     = 'N'
         and st.rec_date           = uk_comp_price_hist.rec_date (+)
         and st.comp_store         = uk_comp_price_hist.comp_store (+)
         and st.ref_item is NULL
         and NVL(st.ref_item,'-1') = NVL(uk_comp_price_hist.ref_item,'-1')
         and UPPER(st.item)        = UPPER(uk_comp_price_hist.item (+))
         and st.comp_retail_type   = cd.code(+)
         and cd.code_type(+)       = 'PCTY';         

   TYPE LP_errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab LP_errors_tab_typ;
   TYPE LP_s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab LP_s9t_errors_tab_typ;
   LP_user                  SVCPROV_CONTEXT.USER_NAME%TYPE     := get_user;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE) IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   IN   NUMBER) IS
   L_sheets             s9t_pkg.names_map_typ;
   COMP_SHOP_LIST_cols  s9t_pkg.names_map_typ;
   COMP_PRICE_HIST_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                        := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   COMP_SHOP_LIST_cols             := S9T_PKG.GET_COL_NAMES(I_file_id,
                                                            COMP_SHOP_LIST_sheet);
   COMP_SHOP_LIST$ACTION           := COMP_SHOP_LIST_cols('ACTION');
   COMP_SHOP_LIST$SHOPPER          := COMP_SHOP_LIST_cols('SHOPPER');
   COMP_SHOP_LIST$SHOP_DATE        := COMP_SHOP_LIST_cols('SHOP_DATE');
   COMP_SHOP_LIST$ITEM             := COMP_SHOP_LIST_cols('ITEM');
   COMP_SHOP_LIST$ITEM_DESC        := COMP_SHOP_LIST_cols('ITEM_DESC');
   COMP_SHOP_LIST$REF_ITEM         := COMP_SHOP_LIST_cols('REF_ITEM');
   COMP_SHOP_LIST$COMPETITOR       := COMP_SHOP_LIST_cols('COMPETITOR');
   COMP_SHOP_LIST$COMP_NAME        := COMP_SHOP_LIST_cols('COMP_NAME');
   COMP_SHOP_LIST$COMP_STORE       := COMP_SHOP_LIST_cols('COMP_STORE');
   COMP_SHOP_LIST$COMP_STORE_NAME  := COMP_SHOP_LIST_cols('COMP_STORE_NAME');
   COMP_SHOP_LIST$REC_DATE         := COMP_SHOP_LIST_cols('REC_DATE');
   COMP_SHOP_LIST$COMP_RETAIL      := COMP_SHOP_LIST_cols('COMP_RETAIL');
   COMP_SHOP_LIST$COMP_RTL_TYP     := COMP_SHOP_LIST_cols('COMP_RETAIL_TYPE');
   COMP_SHOP_LIST$MULTI_UNITS      := COMP_SHOP_LIST_cols('MULTI_UNITS');
   COMP_SHOP_LIST$MULTI_UNIT_RTL   := COMP_SHOP_LIST_cols('MULTI_UNIT_RETAIL');
   COMP_SHOP_LIST$PROM_START_DATE  := COMP_SHOP_LIST_cols('PROM_START_DATE');
   COMP_SHOP_LIST$PROM_END_DATE    := COMP_SHOP_LIST_cols('PROM_END_DATE');
   COMP_SHOP_LIST$OFFER_TYPE       := COMP_SHOP_LIST_cols('OFFER_TYPE');
   COMP_PRICE_HIST_cols            := S9T_PKG.GET_COL_NAMES(I_file_id,
                                                            COMP_PRICE_HIST_sheet);
   COMP_PRICE_HIST$ACTION          := COMP_PRICE_HIST_cols('ACTION');
   COMP_PRICE_HIST$REC_DATE        := COMP_PRICE_HIST_cols('REC_DATE');
   COMP_PRICE_HIST$COMP_RETAIL     := COMP_PRICE_HIST_cols('COMP_RETAIL');
   COMP_PRICE_HIST$COMP_RTL_TYP    := COMP_PRICE_HIST_cols('COMP_RETAIL_TYPE');
   COMP_PRICE_HIST$MULTI_UNITS     := COMP_PRICE_HIST_cols('MULTI_UNITS');
   COMP_PRICE_HIST$MULTI_UNIT_RTL  := COMP_PRICE_HIST_cols('MULTI_UNIT_RETAIL');
   COMP_PRICE_HIST$PROM_STRT_DATE  := COMP_PRICE_HIST_cols('PROM_START_DATE');
   COMP_PRICE_HIST$PROM_END_DATE   := COMP_PRICE_HIST_cols('PROM_END_DATE');
   COMP_PRICE_HIST$OFFER_TYPE      := COMP_PRICE_HIST_cols('OFFER_TYPE');
   COMP_PRICE_HIST$ITEM            := COMP_PRICE_HIST_cols('ITEM');
   COMP_PRICE_HIST$REF_ITEM        := COMP_PRICE_HIST_cols('REF_ITEM');
   COMP_PRICE_HIST$COMP_STORE      := COMP_PRICE_HIST_cols('COMP_STORE');
END POPULATE_NAMES;
-------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COMP_SHOP_LIST(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = COMP_SHOP_LIST_sheet)
      select s9t_row(s9t_cells(CORESVC_COMP_PRICE.action_mod,
                               shopper,
                               shop_date,
                               item,
                               item_desc,
                               ref_item,
                               competitor,
                               comp_name,
                               comp_store,
                               comp_store_name,
                               rec_date,
                               comp_retail,
                               comp_retail_TYPE,
                               multi_units,
                               multi_unit_retail,
                               prom_start_date,
                               prom_end_date,
                               offer_type))
        from comp_shop_list
       where comp_retail is NULL;
END POPULATE_COMP_SHOP_LIST;
---------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COMP_PRICE_HIST(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id = I_file_id
                        and ss.sheet_name = COMP_PRICE_HIST_sheet)
      select s9t_row(s9t_cells(CORESVC_COMP_PRICE.action_mod,
                               rec_date,
                               item,
                               ref_item,
                               comp_store,
                               comp_retail,
                               comp_retail_TYPE,
                               multi_units,
                               multi_unit_retail,
                               prom_start_date,
                               prom_end_date,
                               offer_type))
        from comp_price_hist;
END POPULATE_COMP_PRICE_HIST;
---------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||LP_user||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(COMP_SHOP_LIST_sheet);
   L_file.sheets(L_file.get_sheet_index(COMP_SHOP_LIST_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                            'SHOPPER',
                                                                                            'SHOP_DATE',
                                                                                            'ITEM',
                                                                                            'ITEM_DESC',
                                                                                            'REF_ITEM',
                                                                                            'COMPETITOR',
                                                                                            'COMP_NAME',
                                                                                            'COMP_STORE',
                                                                                            'COMP_STORE_NAME',
                                                                                            'REC_DATE',
                                                                                            'COMP_RETAIL',
                                                                                            'COMP_RETAIL_TYPE',
                                                                                            'MULTI_UNITS',
                                                                                            'MULTI_UNIT_RETAIL',
                                                                                            'PROM_START_DATE',
                                                                                            'PROM_END_DATE',
                                                                                            'OFFER_TYPE');
   L_file.add_sheet(COMP_PRICE_HIST_sheet);
   L_file.sheets(L_file.get_sheet_index(COMP_PRICE_HIST_sheet)).column_headers := s9t_cells('ACTION',
                                                                                            'REC_DATE',
                                                                                            'ITEM',
                                                                                            'REF_ITEM',
                                                                                            'COMP_STORE',
                                                                                            'COMP_RETAIL',
                                                                                            'COMP_RETAIL_TYPE',
                                                                                            'MULTI_UNITS',
                                                                                            'MULTI_UNIT_RETAIL',
                                                                                            'PROM_START_DATE',
                                                                                            'PROM_END_DATE',
                                                                                            'OFFER_TYPE');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COMP_PRICE.CREATE_S9T';
   L_file      s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
        template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_COMP_SHOP_LIST(O_file_id);
      POPULATE_COMP_PRICE_HIST(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COMP_SHOP_LIST(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_COMP_SHOP_LIST.PROCESS_ID%TYPE) IS

   TYPE svc_comp_shop_list_col_typ IS TABLE OF SVC_COMP_SHOP_LIST%ROWTYPE;
   L_temp_rec               SVC_COMP_SHOP_LIST%ROWTYPE;
   svc_comp_shop_list_col   svc_comp_shop_list_col_typ := NEW svc_comp_shop_list_col_typ();
   L_process_id             SVC_COMP_SHOP_LIST.PROCESS_ID%TYPE;
   L_error                  BOOLEAN                    := FALSE;
   L_default_rec            SVC_COMP_SHOP_LIST%ROWTYPE;

   cursor C_MANDATORY_IND is
      select shopper_mi,
             shop_date_mi,
             item_mi,
             item_desc_mi,
             ref_item_mi,
             competitor_mi,
             comp_name_mi,
             comp_store_mi,
             comp_store_name_mi,
             rec_date_mi,
             comp_retail_mi,
             comp_retail_TYPE_mi,
             multi_units_mi,
             multi_unit_retail_mi,
             prom_start_date_mi,
             prom_end_date_mi,
             offer_type_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'COMP_SHOP_LIST')
               PIVOT (MAX(mandatory) as mi
                      FOR (column_key) IN ('SHOPPER'           as shopper,
                                           'SHOP_DATE'         as shop_date,
                                           'ITEM'              as item,
                                           'ITEM_DESC'         as item_desc,
                                           'REF_ITEM'          as ref_item,
                                           'COMPETITOR'        as competitor,
                                           'COMP_NAME'         as comp_name,
                                           'COMP_STORE'        as comp_store,
                                           'COMP_STORE_NAME'   as comp_store_name,
                                           'REC_DATE'          as rec_date,
                                           'COMP_RETAIL'       as comp_retail,
                                           'COMP_RETAIL_TYPE'  as comp_retail_type,
                                           'MULTI_UNITS'       as multi_units,
                                           'MULTI_UNIT_RETAIL' as multi_unit_retail,
                                           'PROM_START_DATE'   as prom_start_date,
                                           'PROM_END_DATE'     as prom_end_date,
                                           'OFFER_TYPE'        as offer_type,
                                           null as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMP_SHOP_LIST';
   L_pk_columns    VARCHAR2(255)  := 'Shopper,Shop Date,Item,Competitior Store';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   -- Get default values.
   FOR rec IN (select  shopper_dv,
                       shop_date_dv,
                       item_dv,
                       item_desc_dv,
                       ref_item_dv,
                       competitor_dv,
                       comp_name_dv,
                       comp_store_dv,
                       comp_store_name_dv,
                       rec_date_dv,
                       comp_retail_dv,
                       comp_retail_TYPE_dv,
                       multi_units_dv,
                       multi_unit_retail_dv,
                       prom_start_date_dv,
                       prom_end_date_dv,
                       offer_type_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                         = template_key
                          and wksht_key                            = 'COMP_SHOP_LIST') 
                        PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'SHOPPER'           as shopper,
                                                      'SHOP_DATE'         as shop_date,
                                                      'ITEM'              as item,
                                                      'ITEM_DESC'         as item_desc,
                                                      'REF_ITEM'          as ref_item,
                                                      'COMPETITOR'        as competitor,
                                                      'COMP_NAME'         as comp_name,
                                                      'COMP_STORE'        as comp_store,
                                                      'COMP_STORE_NAME'   as comp_store_name,
                                                      'REC_DATE'          as rec_date,
                                                      'COMP_RETAIL'       as comp_retail,
                                                      'COMP_RETAIL_TYPE'  as comp_retail_TYPE,
                                                      'MULTI_UNITS'       as multi_units,
                                                      'MULTI_UNIT_RETAIL' as multi_unit_retail,
                                                      'PROM_START_DATE'   as prom_start_date,
                                                      'PROM_END_DATE'     as prom_end_date,
                                                      'OFFER_TYPE'        as offer_type,
                                                       NULL as dummy)))
   LOOP
      BEGIN
         L_default_rec.shopper := rec.shopper_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'SHOPPER' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.shop_date := rec.shop_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'SHOP_DATE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'ITEM',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.item_desc := rec.item_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'ITEM_DESC',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ref_item := rec.ref_item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'REF_ITEM',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.competitor := rec.competitor_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'COMPETITOR',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_name := rec.comp_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'COMP_NAME',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_store := rec.comp_store_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'COMP_STORE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_store_name := rec.comp_store_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'COMP_STORE_NAME',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.rec_date := rec.rec_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'REC_DATE' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_retail := rec.comp_retail_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'COMP_RETAIL',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_retail_type := rec.comp_retail_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'COMP_RETAIL_TYPE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.multi_units := rec.multi_units_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'MULTI_UNITS',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.multi_unit_retail := rec.multi_unit_retail_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'MULTI_UNIT_RETAIL',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.prom_start_date := rec.prom_start_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'PROM_START_DATE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.prom_end_date := rec.prom_end_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'PROM_END_DATE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.offer_type := rec.offer_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_SHOP_LIST',
                            NULL,
                           'OFFER_TYPE',
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(COMP_SHOP_LIST$ACTION)           as action,
                      r.get_cell(COMP_SHOP_LIST$SHOPPER)          as shopper,
                      r.get_cell(COMP_SHOP_LIST$SHOP_DATE)        as shop_date,
                      UPPER(r.get_cell(COMP_SHOP_LIST$ITEM))      as item,
                      r.get_cell(COMP_SHOP_LIST$ITEM_DESC)        as item_desc,
                      r.get_cell(COMP_SHOP_LIST$REF_ITEM)         as ref_item,
                      r.get_cell(COMP_SHOP_LIST$COMPETITOR)       as competitor,
                      r.get_cell(COMP_SHOP_LIST$COMP_NAME)        as comp_name,
                      r.get_cell(COMP_SHOP_LIST$COMP_STORE)       as comp_store,
                      r.get_cell(COMP_SHOP_LIST$COMP_STORE_NAME)  as comp_store_name,
                      r.get_cell(COMP_SHOP_LIST$REC_DATE)         as rec_date,
                      r.get_cell(COMP_SHOP_LIST$COMP_RETAIL)      as comp_retail,
                      r.get_cell(COMP_SHOP_LIST$COMP_RTL_TYP)     as comp_retail_TYPE,
                      r.get_cell(COMP_SHOP_LIST$MULTI_UNITS)      as multi_units,
                      r.get_cell(COMP_SHOP_LIST$MULTI_UNIT_RTL)   as multi_unit_retail,
                      r.get_cell(COMP_SHOP_LIST$PROM_START_DATE)  as prom_start_date,
                      r.get_cell(COMP_SHOP_LIST$PROM_END_DATE)    as prom_end_date,
                      r.get_cell(COMP_SHOP_LIST$OFFER_TYPE)       as offer_type,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(COMP_SHOP_LIST_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.shopper := rec.shopper;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'SHOPPER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.shop_date := rec.shop_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'SHOP_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item_desc := rec.item_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'ITEM_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ref_item := rec.ref_item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'REF_ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.competitor := rec.competitor;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'COMPETITOR',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_name := rec.comp_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'COMP_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_store := rec.comp_store;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'COMP_STORE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_store_name := rec.comp_store_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'COMP_STORE_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.rec_date := rec.rec_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'REC_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_retail := rec.comp_retail;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'COMP_RETAIL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_retail_type := rec.comp_retail_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'COMP_RETAIL_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.multi_units := rec.multi_units;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'MULTI_UNITS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.multi_unit_retail := rec.multi_unit_retail;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'MULTI_UNIT_RETAIL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.prom_start_date := rec.prom_start_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'PROM_START_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.prom_end_date := rec.prom_end_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'PROM_END_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.offer_type := rec.offer_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOP_LIST_sheet,
                            rec.row_seq,
                            'OFFER_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if NOT (L_temp_rec.comp_store is NOT NULL and
              L_temp_rec.item is NOT NULL and
              L_temp_rec.shop_date is NOT NULL and
              L_temp_rec.shopper is NOT NULL and
              1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         COMP_SHOP_LIST_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_comp_shop_list_col.extend();
         svc_comp_shop_list_col(svc_comp_shop_list_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   
   BEGIN
      forall i IN 1..svc_comp_shop_list_col.COUNT SAVE EXCEPTIONS
      merge into SVC_COMP_SHOP_LIST st
      using(select
                  (case
                   when L_mi_rec.shopper_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.shopper is NULL then
                           mt.shopper
                   else s1.shopper
                   end) as shopper,
                  (case
                   when L_mi_rec.shop_date_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.shop_date is NULL then
                           mt.shop_date
                   else s1.shop_date
                   end) as shop_date,
                  (case
                   when L_mi_rec.item_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.item is NULL then
                           mt.item
                   else s1.item
                   end) as item,
                  (case
                   when L_mi_rec.item_desc_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.item_desc is NULL then
                           mt.item_desc
                   else s1.item_desc
                   end) as item_desc,
                  (case
                   when L_mi_rec.ref_item_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.ref_item is NULL then
                           mt.ref_item
                   else s1.ref_item
                   end) as ref_item,
                  (case
                   when L_mi_rec.competitor_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.competitor is NULL then
                           mt.competitor
                   else s1.competitor
                   end) as competitor,
                  (case
                   when L_mi_rec.comp_name_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.comp_name is NULL then
                           mt.comp_name
                   else s1.comp_name
                   end) as comp_name,
                  (case
                   when L_mi_rec.comp_store_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.comp_store is NULL then
                           mt.comp_store
                   else s1.comp_store
                   end) as comp_store,
                  (case
                   when L_mi_rec.comp_store_name_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.comp_store_name is NULL then
                           mt.comp_store_name
                   else s1.comp_store_name
                   end) as comp_store_name,
                  (case
                   when L_mi_rec.rec_date_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.rec_date is NULL then
                           mt.rec_date
                   else s1.rec_date
                   end) as rec_date,
                  (case
                   when L_mi_rec.comp_retail_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.comp_retail is NULL then
                           mt.comp_retail
                   else s1.comp_retail
                   end) as comp_retail,
                  (case
                   when L_mi_rec.comp_retail_type_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.comp_retail_type is NULL then
                           mt.comp_retail_type
                   else s1.comp_retail_type
                   end) as comp_retail_type,
                  (case
                   when L_mi_rec.multi_units_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.multi_units is NULL then
                           mt.multi_units
                   else s1.multi_units
                   end) as multi_units,
                  (case
                   when L_mi_rec.multi_unit_retail_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.multi_unit_retail is NULL then
                           mt.multi_unit_retail
                   else s1.multi_unit_retail
                   end) as multi_unit_retail,
                  (case
                   when L_mi_rec.prom_start_date_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.prom_start_date is NULL then
                           mt.prom_start_date
                   else s1.prom_start_date
                   end) as prom_start_date,
                  (case
                   when L_mi_rec.prom_end_date_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.prom_end_date is NULL then
                           mt.prom_end_date
                   else s1.prom_end_date
                   end) as prom_end_date,
                  (case
                   when L_mi_rec.offer_type_mi = 'N'
                    and svc_comp_shop_list_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.offer_type is NULL then
                           mt.offer_type
                   else s1.offer_type
                   end) as offer_type,
                  NULL as dummy
              from (select svc_comp_shop_list_col(i).shopper           as shopper,
                           svc_comp_shop_list_col(i).shop_date         as shop_date,
                           svc_comp_shop_list_col(i).item              as item,
                           svc_comp_shop_list_col(i).item_desc         as item_desc,
                           svc_comp_shop_list_col(i).ref_item          as ref_item,
                           svc_comp_shop_list_col(i).competitor        as competitor,
                           svc_comp_shop_list_col(i).comp_name         as comp_name,
                           svc_comp_shop_list_col(i).comp_store        as comp_store,
                           svc_comp_shop_list_col(i).comp_store_name   as comp_store_name,
                           svc_comp_shop_list_col(i).rec_date          as rec_date,
                           svc_comp_shop_list_col(i).comp_retail       as comp_retail,
                           svc_comp_shop_list_col(i).comp_retail_type  as comp_retail_type,
                           svc_comp_shop_list_col(i).multi_units       as multi_units,
                           svc_comp_shop_list_col(i).multi_unit_retail as multi_unit_retail,
                           svc_comp_shop_list_col(i).prom_start_date   as prom_start_date,
                           svc_comp_shop_list_col(i).prom_end_date     as prom_end_date,
                           svc_comp_shop_list_col(i).offer_type        as offer_type,
                           NULL as dummy
                      from dual) s1,
                  COMP_SHOP_LIST mt
             where mt.comp_store (+) = s1.comp_store
               and mt.ref_item (+)   = s1.ref_item
               and mt.item (+)       = s1.item
               and mt.shop_date (+)  = s1.shop_date
               and mt.shopper (+)    = s1.shopper
               and 1 = 1) sq
                on (st.comp_store  = sq.comp_store and
                    st.ref_item    = sq.ref_item and
                    st.item        = sq.item and
                    st.shop_date   = sq.shop_date and
                    st.shopper     = sq.shopper and
                    svc_comp_shop_list_col(i).ACTION IN (CORESVC_COMP_PRICE.action_mod,
                                                         CORESVC_COMP_PRICE.action_del))
      when matched then
      update
         set process_id        = svc_comp_shop_list_col(i).process_id,
             chunk_id          = svc_comp_shop_list_col(i).chunk_id,
             row_seq           = svc_comp_shop_list_col(i).row_seq,
             action            = svc_comp_shop_list_col(i).action,
             process$status    = svc_comp_shop_list_col(i).process$status,
             comp_store_name   = sq.comp_store_name,
             comp_retail       = sq.comp_retail,
             comp_name         = sq.comp_name,
             item_desc         = sq.item_desc,
             multi_unit_retail = sq.multi_unit_retail,
             prom_end_date     = sq.prom_end_date,
             competitor        = sq.competitor,
             comp_retail_type  = sq.comp_retail_type,
             offer_type        = sq.offer_type,
             multi_units       = sq.multi_units,
             prom_start_date   = sq.prom_start_date,
             rec_date          = sq.rec_date,
             create_id         = svc_comp_shop_list_col(i).create_id,
             create_datetime   = svc_comp_shop_list_col(i).create_datetime,
             last_upd_id       = svc_comp_shop_list_col(i).last_upd_id,
             last_upd_datetime = svc_comp_shop_list_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id,
             row_seq,
             action,
             process$status,
             shopper,
             shop_date,
             item,
             item_desc,
             ref_item,
             competitor,
             comp_name,
             comp_store,
             comp_store_name,
             rec_date,
             comp_retail,
             comp_retail_type,
             multi_units,
             multi_unit_retail,
             prom_start_date,
             prom_end_date,
             offer_type,
             create_id,
             create_datetime,
             last_upd_id,
             last_upd_datetime)
      values(svc_comp_shop_list_col(i).process_id,
             svc_comp_shop_list_col(i).chunk_id,
             svc_comp_shop_list_col(i).row_seq,
             svc_comp_shop_list_col(i).action,
             svc_comp_shop_list_col(i).process$status,
             sq.shopper,
             sq.shop_date,
             sq.item,
             sq.item_desc,
             sq.ref_item,
             sq.competitor,
             sq.comp_name,
             sq.comp_store,
             sq.comp_store_name,
             sq.rec_date,
             sq.comp_retail,
             sq.comp_retail_type,
             sq.multi_units,
             sq.multi_unit_retail,
             sq.prom_start_date,
             sq.prom_end_date,
             sq.offer_type,
             svc_comp_shop_list_col(i).create_id,
             svc_comp_shop_list_col(i).create_datetime,
             svc_comp_shop_list_col(i).last_upd_id,
             svc_comp_shop_list_col(i).last_upd_datetime);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          COMP_SHOP_LIST_sheet,
                          svc_comp_shop_list_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);                
      END LOOP;
   END;
END PROCESS_S9T_COMP_SHOP_LIST;
-----------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COMP_PRICE_HIST(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id   IN   SVC_COMP_PRICE_HIST.PROCESS_ID%TYPE) IS
   TYPE svc_comp_price_hist_col_typ IS TABLE OF SVC_COMP_PRICE_HIST%ROWTYPE;
   L_temp_rec                SVC_COMP_PRICE_HIST%ROWTYPE;
   svc_comp_price_hist_col   svc_comp_price_hist_col_typ := NEW svc_comp_price_hist_col_typ();
   L_process_id              SVC_COMP_PRICE_HIST.PROCESS_ID%TYPE;
   L_error                   BOOLEAN                     := FALSE;
   L_default_rec             SVC_COMP_PRICE_HIST%ROWTYPE;

   cursor C_MANDATORY_IND is
      select rec_date_mi,
             comp_store_mi,
             ref_item_mi,
             item_mi,
             offer_type_mi,
             prom_end_date_mi,
             prom_start_date_mi,
             multi_unit_retail_mi,
             multi_units_mi,
             comp_retail_type_mi,
             comp_retail_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'COMP_PRICE_HIST')
               PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('REC_DATE'          as rec_date,
                                            'COMP_STORE'        as comp_store,
                                            'REF_ITEM'          as ref_item,
                                            'ITEM'              as item,
                                            'OFFER_TYPE'        as offer_type,
                                            'PROM_END_DATE'     as prom_end_date,
                                            'PROM_START_DATE'   as prom_start_date,
                                            'MULTI_UNIT_RETAIL' as multi_unit_retail,
                                            'MULTI_UNITS'       as multi_units,
                                            'COMP_RETAIL_TYPE'  as comp_retail_type,
                                            'COMP_RETAIL'       as comp_retail,
                                            NULL as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMP_PRICE_HIST';
   L_pk_columns    VARCHAR2(255)  := 'Item,Competitor Store,Recorded Date';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;      
BEGIN
   -- Get default values.
   FOR rec IN (select rec_date_dv,
                      comp_store_dv,
                      ref_item_dv,
                      item_dv,
                      offer_type_dv,
                      prom_end_date_dv,
                      prom_start_date_dv,
                      multi_unit_retail_dv,
                      multi_units_dv,
                      comp_retail_type_dv,
                      comp_retail_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                        = template_key
                          and wksht_key                           = 'COMP_PRICE_HIST' )
                        PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'REC_DATE'          as rec_date,
                                                      'COMP_STORE'        as comp_store,
                                                      'REF_ITEM'          as ref_item,
                                                      'ITEM'              as item,
                                                      'OFFER_TYPE'        as offer_type,
                                                      'PROM_END_DATE'     as prom_end_date,
                                                      'PROM_START_DATE'   as prom_start_date,
                                                      'MULTI_UNIT_RETAIL' as multi_unit_retail,
                                                      'MULTI_UNITS'       as multi_units,
                                                      'COMP_RETAIL_TYPE'  as comp_retail_type,
                                                      'COMP_RETAIL'       as comp_retail,
                                                       NULL as dummy)))
   LOOP
      BEGIN
         L_default_rec.rec_date := rec.rec_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'REC_DATE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_store := rec.comp_store_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'COMP_STORE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ref_item := rec.ref_item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'REF_ITEM',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.item := rec.item_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'ITEM ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.offer_type := rec.offer_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'OFFER_TYPE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.prom_end_date := rec.prom_end_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'PROM_END_DATE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.prom_start_date := rec.prom_start_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'PROM_START_DATE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.multi_unit_retail := rec.multi_unit_retail_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'MULTI_UNIT_RETAIL',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.multi_units := rec.multi_units_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'MULTI_UNITS',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_retail_type := rec.comp_retail_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'COMP_RETAIL_TYPE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_retail := rec.comp_retail_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_PRICE_HIST',
                            NULL,
                           'COMP_RETAIL',
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(COMP_PRICE_HIST$ACTION)         as action,
                      r.get_cell(COMP_PRICE_HIST$REC_DATE)       as rec_date,
                      r.get_cell(COMP_PRICE_HIST$COMP_STORE)     as comp_store,
                      r.get_cell(COMP_PRICE_HIST$REF_ITEM)       as ref_item,
                      UPPER(r.get_cell(COMP_PRICE_HIST$ITEM))    as item,
                      r.get_cell(COMP_PRICE_HIST$OFFER_TYPE)     as offer_type,
                      r.get_cell(COMP_PRICE_HIST$PROM_END_DATE)  as prom_end_date,
                      r.get_cell(COMP_PRICE_HIST$PROM_STRT_DATE) as prom_start_date,
                      r.get_cell(COMP_PRICE_HIST$MULTI_UNIT_RTL) as multi_unit_retail,
                      r.get_cell(COMP_PRICE_HIST$MULTI_UNITS)    as multi_units,
                      r.get_cell(COMP_PRICE_HIST$COMP_RTL_TYP)   as comp_retail_type,
                      r.get_cell(COMP_PRICE_HIST$COMP_RETAIL)    as comp_retail,
                      r.get_row_seq()                            as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(COMP_PRICE_HIST_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.rec_date := rec.rec_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'REC_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_store := rec.comp_store;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'COMP_STORE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ref_item := rec.ref_item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'REF_ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.offer_type := rec.offer_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'OFFER_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.prom_end_date := rec.prom_end_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'PROM_END_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.prom_start_date := rec.prom_start_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'PROM_START_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.multi_unit_retail := rec.multi_unit_retail;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'MULTI_UNIT_RETAIL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.multi_units := rec.multi_units;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'MULTI_UNITS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_retail_type := rec.comp_retail_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'COMP_RETAIL_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_retail := rec.comp_retail;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_PRICE_HIST_sheet,
                            rec.row_seq,
                            'COMP_RETAIL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
   if NOT (L_temp_rec.rec_date is NOT NULL and
              L_temp_rec.comp_store is NOT NULL and
              L_temp_rec.item is NOT NULL and
              1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         COMP_PRICE_HIST_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_comp_price_hist_col.extend();
         svc_comp_price_hist_col(svc_comp_price_hist_col.COUNT()):= L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_comp_price_hist_col.COUNT SAVE EXCEPTIONS
      merge into SVC_COMP_PRICE_HIST st
      using(select
                  (case
                   when L_mi_rec.rec_date_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.rec_date is NULL then
                           mt.rec_date
                   else s1.rec_date
                   end) as rec_date,
                  (case
                   when L_mi_rec.comp_store_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.comp_store is NULL then
                           mt.comp_store
                   else s1.comp_store
                   end) as comp_store,
                  (case
                   when L_mi_rec.ref_item_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.ref_item is NULL then
                           mt.ref_item
                   else s1.ref_item
                   end) as ref_item,
                  (case
                   when L_mi_rec.item_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.item is NULL then
                           mt.item
                   else s1.item
                   end) as item,
                  (case
                   when L_mi_rec.offer_type_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.offer_type is NULL then
                           mt.offer_type
                   else s1.offer_type
                   end) as offer_type,
                  (case
                   when L_mi_rec.prom_end_date_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.prom_end_date is NULL then
                           mt.prom_end_date
                   else s1.prom_end_date
                   end) as prom_end_date,
                  (case
                   when L_mi_rec.prom_start_date_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.prom_start_date is NULL then
                           mt.prom_start_date
                   else s1.prom_start_date
                   end) as prom_start_date,
                  (case
                   when L_mi_rec.multi_unit_retail_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.multi_unit_retail is NULL then
                           mt.multi_unit_retail
                   else s1.multi_unit_retail
                   end) as multi_unit_retail,
                  (case
                   when L_mi_rec.multi_units_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.multi_units is NULL then
                           mt.multi_units
                   else s1.multi_units
                   end) as multi_units,
                  (case
                   when L_mi_rec.comp_retail_type_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.comp_retail_type is NULL then
                           mt.comp_retail_type
                   else s1.comp_retail_type
                   end) as comp_retail_type,
                  (case
                   when L_mi_rec.comp_retail_mi = 'N'
                    and svc_comp_price_hist_col(i).action = CORESVC_COMP_PRICE.action_mod
                    and s1.comp_retail is NULL then
                           mt.comp_retail
                   else s1.comp_retail
                   end) as comp_retail,
                  null as dummy
              from (select svc_comp_price_hist_col(i).rec_date          as rec_date,
                           svc_comp_price_hist_col(i).comp_store        as comp_store,
                           svc_comp_price_hist_col(i).ref_item          as ref_item,
                           svc_comp_price_hist_col(i).item              as item,
                           svc_comp_price_hist_col(i).offer_type        as offer_type,
                           svc_comp_price_hist_col(i).prom_end_date     as prom_end_date,
                           svc_comp_price_hist_col(i).prom_start_date   as prom_start_date,
                           svc_comp_price_hist_col(i).multi_unit_retail as multi_unit_retail,
                           svc_comp_price_hist_col(i).multi_units       as multi_units,
                           svc_comp_price_hist_col(i).comp_retail_type  as comp_retail_type,
                           svc_comp_price_hist_col(i).comp_retail       as comp_retail,
                           null as dummy
                      from dual) s1,
                   COMP_PRICE_HIST mt
             where mt.rec_date (+)   = s1.rec_date
               and mt.comp_store (+) = s1.comp_store
               and mt.ref_item (+)   = s1.ref_item
               and mt.item (+)       = s1.item
               and 1 = 1 )sq
                on (st.rec_date   = sq.rec_date   and
                    st.comp_store = sq.comp_store and
                    st.ref_item   = sq.ref_item   and
                    st.item       = sq.item       and
                    svc_comp_price_hist_col(i).ACTION IN (CORESVC_COMP_PRICE.action_mod,
                                                          CORESVC_COMP_PRICE.action_del))
      when matched then
      update
         set process_id        = svc_comp_price_hist_col(i).process_id,
             chunk_id          = svc_comp_price_hist_col(i).chunk_id,
             row_seq           = svc_comp_price_hist_col(i).row_seq,
             action            = svc_comp_price_hist_col(i).action,
             process$status    = svc_comp_price_hist_col(i).process$status,
             multi_unit_retail = sq.multi_unit_retail,
             prom_end_date     = sq.prom_end_date,
             comp_retail_type  = sq.comp_retail_type,
             offer_type        = sq.offer_type,
             comp_retail       = sq.comp_retail,
             multi_units       = sq.multi_units,
             prom_start_date   = sq.prom_start_date,
             create_id         = svc_comp_price_hist_col(i).create_id,
             create_datetime   = svc_comp_price_hist_col(i).create_datetime,
             last_upd_id       = svc_comp_price_hist_col(i).last_upd_id,
             last_upd_datetime = svc_comp_price_hist_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id,
             row_seq,
             action,
             process$status,
             rec_date,
             comp_store,
             ref_item,
             item,
             offer_type,
             prom_end_date,
             prom_start_date,
             multi_unit_retail,
             multi_units,
             comp_retail_type,
             comp_retail,
             create_id,
             create_datetime,
             last_upd_id,
             last_upd_datetime)
      values(svc_comp_price_hist_col(i).process_id,
             svc_comp_price_hist_col(i).chunk_id,
             svc_comp_price_hist_col(i).row_seq,
             svc_comp_price_hist_col(i).action,
             svc_comp_price_hist_col(i).process$status,
             sq.rec_date,
             sq.comp_store,
             sq.ref_item,
             sq.item,
             sq.offer_type,
             sq.prom_end_date,
             sq.prom_start_date,
             sq.multi_unit_retail,
             sq.multi_units,
             sq.comp_retail_type,
             sq.comp_retail,
             svc_comp_price_hist_col(i).create_id,
             svc_comp_price_hist_col(i).create_datetime,
             svc_comp_price_hist_col(i).last_upd_id,
             svc_comp_price_hist_col(i).last_upd_datetime);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
      L_error_code:=sql%bulk_exceptions(i).error_code;
      if L_error_code=1 then
         L_error_code:=NULL;
         L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                           L_pk_columns);
      end if;
      WRITE_S9T_ERROR( I_file_id,
                       COMP_PRICE_HIST_sheet,
                       svc_comp_price_hist_col(sql%bulk_exceptions(i).error_index).row_seq,
                       NULL,
                       L_error_code,
                       L_error_msg);
      END LOOP;
   END;
END PROCESS_S9T_COMP_PRICE_HIST;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_COMP_PRICE.PROCESS_S9T';
   L_file             s9t_file;
   L_sheets           s9t_pkg.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR       EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);   
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_COMP_SHOP_LIST(I_file_id,
                                 I_process_id);
      PROCESS_S9T_COMP_PRICE_HIST(I_file_id,
                                  I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert into s9t_errors
         values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status     = 'PE',
             file_id    = i_file_id
       where process_id = i_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
  insert into s9t_errors
       values Lp_s9t_errors_tab(i);

      update svc_process_tracker
  set status = 'PE',
      file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_COMP_SHOP_LIST_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error           IN OUT   BOOLEAN,
                                    I_rec             IN       C_SVC_COMP_SHOP_LIST%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_COMP_PRICE.PROCESS_COMP_SHOP_LIST_VAL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_SHOP_LIST';
   L_code_desc   CODE_DETAIL.CODE_DESC%TYPE;

BEGIN
   if I_rec.old_comp_retail is NOT NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'COMP_RETAIL',
                  'COMP_RETAIL_EXIST'); 
      O_error := TRUE;
   else
      --competitor if not null must match with the comp_store in comp_shop_list table
      if I_rec.competitor is NOT NULL and
         I_rec.old_competitor <> I_rec.competitor then 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMPETITOR',
                     'INV_COMPETITOR');
         O_error := TRUE;
      end if;
      if I_rec.comp_retail_type is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_RETAIL_TYPE',
                     'MUST_ENT_COMP_RET_TYPE');
         O_error := TRUE;
      end if;
      if I_rec.comp_retail_type is NOT NULL and
         I_rec.cd_rid is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_RETAIL_TYPE',
                     'CHK_COMP_RET_TYPE');
         O_error := TRUE;
      end if; 
      --comp retail must be null to allow update and new value must not be null and it should be greater than 0
      if I_rec.comp_retail is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_RETAIL',
                     'MUST_ENTER_COMP_PRICE');
         O_error := TRUE;
      elsif I_rec.comp_retail <= 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_RETAIL',
                     'GREATER_0');
         O_error := TRUE;
      end if;
      --shop_date must be less than or equal to vdate
      if I_rec.shop_date is NOT NULL and
         I_rec.shop_date > GET_VDATE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SHOP_DATE',
                     'DATE_LESS');
         O_error := TRUE;
      end if;
      --rec_date cannot be null and must be less than or equal to vdate
      if I_rec.rec_date is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REC_DATE',
                     'MUST_ENT_COMP_REC_DATE');
         O_error := TRUE;
      elsif I_rec.rec_date > GET_VDATE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REC_DATE',
                     'DATE_LESS');
         O_error := TRUE;
      end if;
   --multi unit_retail if present must be greater than 0 and greater than comp_retail
      if I_rec.multi_unit_retail is NOT NULL then
         if I_rec.multi_unit_retail <= 0 then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'MULTI_UNIT_RETAIL',
                        'GREATER_0');
            O_error := TRUE;
         elsif I_rec.multi_unit_retail < I_rec.comp_retail then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'MULTI_UNIT_RETAIL',
                        'MULTI_RETAIL_LESS_SINGLE');
            O_error := TRUE;
         end if;
      end if;
      --multi unit if present must be greater than 0 and retail/unit less than comp_retail
      if I_rec.multi_units is NOT NULL and
         I_rec.multi_units <= 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MULTI_UNITS',
                     'GREATER_0');
         O_error := TRUE;
      end if;
      if I_rec.multi_unit_retail is NOT NULL and
         I_rec.multi_unit_retail > 0 and
         I_rec.multi_units is NOT NULL and
         I_rec.multi_units > 0 and
         (I_rec.multi_unit_retail/I_rec.multi_units) > I_rec.comp_retail then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MULTI_UNITS',
                     'M_LESS_S_RETAIL');
         O_error := TRUE;
      end if;
      --both multi_units and multi_unit_retail must be not null or both should be NULL
      if I_rec.multi_units is NOT NULL and
         I_rec.multi_unit_retail is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MULTI_UNIT_RETAIL',
                     'MUST_ENTER_MULT_RETAIL');
         O_error := TRUE;
      end if;
      if I_rec.multi_unit_retail is NOT NULL and
         I_rec.multi_units is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MULTI_UNITS',
                     'MUST_ENTER_MULT_UNIT');
         O_error := TRUE;
      end if;
      --if comp_retail_type is P and prom_start_date and prom_end_date are both NOT NULL, prom_start_date should not be greater than prom_end_date
      if I_rec.comp_retail_type = 'P' then
         if I_rec.prom_end_date is NOT NULL and
            I_rec.prom_start_date is NULL then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'PROM_START_DATE',
                        'ENTER_DATE');
            O_error := TRUE;
         end if;
         if I_rec.prom_end_date is NOT NULL and
            I_rec.prom_start_date is NOT NULL and
            I_rec.prom_start_date > I_rec.prom_end_date then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'PROM_START_DATE',
                        'START_DATE_LESS_END');
            O_error := TRUE;
         end if;
      end if;

      if I_rec.comp_retail_type in ('P','R') and
         I_rec.offer_type is NOT NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'OFTP', 
                                       I_rec.offer_type ,
                                       L_code_desc ) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'OFFER_TYPE',
                        'CHK_OFFER_TYPE');
            O_error := TRUE;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COMP_SHOP_LIST_VAL;
----------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_SHOP_LIST_UPD(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,  
                                 I_comp_shop_list_temp_rec   IN       COMP_SHOP_LIST%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_COMP_PRICE.EXEC_COMP_SHOP_LIST_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMP_SHOP_LIST';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_SHOP is
      select 'x'
        from comp_shop_list
       where shopper            = I_comp_shop_list_temp_rec.shopper
         and shop_date          = I_comp_shop_list_temp_rec.shop_date
         and item               = I_comp_shop_list_temp_rec.item
         and comp_store         = I_comp_shop_list_temp_rec.comp_store
         and comp_retail        is NULL
         and NVL(ref_item,'-1') = NVL(I_comp_shop_list_temp_rec.ref_item,'-1')
         for update nowait;

BEGIN
   open C_LOCK_SHOP;
   close C_LOCK_SHOP;
   
   update comp_shop_list
      set row = I_comp_shop_list_temp_rec
    where 1 = 1
      and shopper            = I_comp_shop_list_temp_rec.shopper
      and shop_date          = I_comp_shop_list_temp_rec.shop_date
      and item               = I_comp_shop_list_temp_rec.item
      and comp_store         = I_comp_shop_list_temp_rec.comp_store
      and NVL(ref_item,'-1') = NVL(I_comp_shop_list_temp_rec.ref_item,'-1');

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_comp_shop_list_temp_rec.item,
                                             I_comp_shop_list_temp_rec.comp_store);
      return FALSE;
   when OTHERS then
      if C_LOCK_SHOP%ISOPEN then
         close C_LOCK_SHOP;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMP_SHOP_LIST_UPD;
------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMP_SHOP_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_COMP_SHOP_LIST.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_COMP_SHOP_LIST.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64)                      := 'CORESVC_COMP_PRICE.PROCESS_COMP_SHOP_LIST';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_SHOP_LIST';
   L_error                     BOOLEAN;
   L_process_error             BOOLEAN                           := FALSE;
   L_comp_shop_list_temp_rec   COMP_SHOP_LIST%ROWTYPE;

BEGIN
   FOR rec IN C_SVC_COMP_SHOP_LIST(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      if rec.action is NULL or
         rec.action NOT IN (action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.action = action_mod and
         rec.shopper is NOT NULL and
         rec.shop_date is NOT NULL and
         rec.comp_store is NOT NULL and
         rec.uk_comp_shop_list_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Shopper,Shop Date,Item,Competitor Store',
                     'COMP_SHOP_LIST_MISSING');
         L_error := TRUE;
      end if;

      if PROCESS_COMP_SHOP_LIST_VAL(O_error_message,
                                    L_error,
                                    rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_comp_shop_list_temp_rec.shopper           := rec.shopper;
         L_comp_shop_list_temp_rec.shop_date         := rec.shop_date;
         L_comp_shop_list_temp_rec.item              := rec.item;
         L_comp_shop_list_temp_rec.item_desc         := rec.item_desc;
         L_comp_shop_list_temp_rec.ref_item          := rec.ref_item;
         L_comp_shop_list_temp_rec.competitor        := rec.competitor;
         L_comp_shop_list_temp_rec.comp_name         := rec.comp_name;
         L_comp_shop_list_temp_rec.comp_store        := rec.comp_store;
         L_comp_shop_list_temp_rec.comp_store_name   := rec.comp_store_name;
         L_comp_shop_list_temp_rec.rec_date          := rec.rec_date;
         L_comp_shop_list_temp_rec.comp_retail       := rec.comp_retail;
         L_comp_shop_list_temp_rec.comp_retail_type  := rec.comp_retail_type;
         L_comp_shop_list_temp_rec.multi_units       := rec.multi_units;
         L_comp_shop_list_temp_rec.multi_unit_retail := rec.multi_unit_retail;
         L_comp_shop_list_temp_rec.prom_start_date   := rec.prom_start_date;
         L_comp_shop_list_temp_rec.prom_end_date     := rec.prom_end_date;
         L_comp_shop_list_temp_rec.offer_type        := rec.offer_type;

         if rec.comp_retail_type != 'P' then
            L_comp_shop_list_temp_rec.prom_start_date := NULL;
            L_comp_shop_list_temp_rec.prom_end_date   := NULL;
         end if;

         if rec.comp_retail_type = 'C' then
            L_comp_shop_list_temp_rec.offer_type      := NULL;
         end if;

         if rec.action = action_mod then
            if EXEC_COMP_SHOP_LIST_UPD(O_error_message,
                                       L_comp_shop_list_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COMP_SHOP_LIST;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMP_PRICE_HIST_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error           IN OUT   BOOLEAN,
                                     I_rec             IN       C_SVC_COMP_PRICE_HIST%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_COMP_PRICE.PROCESS_COMP_PRICE_HIST_VAL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_PRICE_HIST';
   L_code_desc   CODE_DETAIL.CODE_DESC%TYPE;

BEGIN
   if I_rec.action = action_mod then
      if I_rec.comp_retail_type is NOT NULL and
         I_rec.cd_rid is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_RETAIL_TYPE',
                     'CHK_COMP_RET_TYPE');
         O_error := TRUE;
      end if; 
      --comp retail must be null to allow update and new value must not be null and it should be greater than 0
      if I_rec.comp_retail is NOT NULL and
         I_rec.comp_retail <= 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_RETAIL',
                     'GREATER_0');
         O_error := TRUE;
      end if;
      --multi unit_retail if present must be greater than 0 and greater than comp_retail
      if I_rec.multi_unit_retail is NOT NULL then
         if I_rec.multi_unit_retail <= 0 then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'MULTI_UNIT_RETAIL',
                        'GREATER_0');
            O_error := TRUE;
         elsif I_rec.multi_unit_retail < I_rec.comp_retail then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'MULTI_UNIT_RETAIL',
                        'MULTI_RETAIL_LESS_SINGLE');
            O_error := TRUE;
         end if;
      end if;
      --multi unit if present must be greater than 0 and retail/unit less than comp_retail
      if I_rec.multi_units is NOT NULL and
         I_rec.multi_units <= 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MULTI_UNITS',
                     'GREATER_0');
         O_error := TRUE;
      end if;
      if I_rec.multi_unit_retail is NOT NULL and
         I_rec.multi_unit_retail > 0 and
         I_rec.multi_units is NOT NULL and
         I_rec.multi_units > 0 and
         (I_rec.multi_unit_retail/I_rec.multi_units) > I_rec.comp_retail then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MULTI_UNITS',
                     'M_LESS_S_RETAIL');
         O_error := TRUE;
      end if;
      --both multi_units and multi_unit_retail must be NOT NULL or both should be NULL
      if I_rec.multi_units is NOT NULL and
         I_rec.multi_unit_retail is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MULTI_UNIT_RETAIL',
                     'MUST_ENTER_MULT_RETAIL');
         O_error := TRUE;
      end if;
      if I_rec.multi_units is NULL and
         I_rec.multi_unit_retail is NOT NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MULTI_UNITS',
                     'MUST_ENTER_MULT_UNIT');
         O_error := TRUE;
      end if;
      --if comp_retail_type is P and prom_start_date and prom_end_date are both NOT NULL, prom_start_date should not be greater than prom_end_date
      if I_rec.comp_retail_type = 'P' then
         if I_rec.prom_end_date is NOT NULL and
            I_rec.prom_start_date is NULL then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'PROM_START_DATE',
                        'ENTER_DATE');
            O_error := TRUE;
         end if;
         if I_rec.prom_end_date is NOT NULL and
            I_rec.prom_start_date is NOT NULL and
            I_rec.prom_start_date > I_rec.prom_end_date then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'PROM_START_DATE',
                        'START_DATE_LESS_END');
            O_error := TRUE;
         end if;
      end if;

      if I_rec.comp_retail_type in ('P','R') and
         I_rec.offer_type is NOT NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'OFTP',
                                       I_rec.offer_type,
                                       L_code_desc) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'OFFER_TYPE',
                        'CHK_OFFER_TYPE');
            O_error := TRUE;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COMP_PRICE_HIST_VAL;
---------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_PRICE_HIST_UPD(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_comp_price_hist_temp_rec   IN       COMP_PRICE_HIST%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_COMP_PRICE.EXEC_COMP_PRICE_HIST_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMP_PRICE_HIST';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_HIST is
      select 'x'
        from comp_price_hist
       where rec_date           = I_comp_price_hist_temp_rec.rec_date
         and item               = I_comp_price_hist_temp_rec.item
         and comp_store         = I_comp_price_hist_temp_rec.comp_store
         and NVL(ref_item,'-1') = NVL(I_comp_price_hist_temp_rec.ref_item,'-1')
         for update nowait;

BEGIN
   open C_LOCK_HIST;
   close C_LOCK_HIST;

   update comp_price_hist
      set row = I_comp_price_hist_temp_rec
    where 1 = 1
      and rec_date           = I_comp_price_hist_temp_rec.rec_date
      and item               = I_comp_price_hist_temp_rec.item
      and comp_store         = I_comp_price_hist_temp_rec.comp_store
      and NVL(ref_item,'-1') = NVL(I_comp_price_hist_temp_rec.ref_item,'-1');

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_comp_price_hist_temp_rec.item,
                                             I_comp_price_hist_temp_rec.comp_store);
      return FALSE;
   when OTHERS then
      if C_LOCK_HIST%ISOPEN then
         close C_LOCK_HIST;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMP_PRICE_HIST_UPD;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_PRICE_HIST_DEL(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_comp_price_hist_temp_rec   IN       COMP_PRICE_HIST%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_COMP_PRICE.EXEC_COMP_PRICE_HIST_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMP_PRICE_HIST';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_HIST is
      select 'x'
        from comp_price_hist
       where rec_date           = I_comp_price_hist_temp_rec.rec_date
         and item               = I_comp_price_hist_temp_rec.item
         and comp_store         = I_comp_price_hist_temp_rec.comp_store
         and NVL(ref_item,'-1') = NVL(I_comp_price_hist_temp_rec.ref_item,'-1')
   for update nowait;

BEGIN
   open C_LOCK_HIST;
   close C_LOCK_HIST;

   delete from comp_price_hist
      where 1 = 1
        and rec_date           = I_comp_price_hist_temp_rec.rec_date
        and item               = I_comp_price_hist_temp_rec.item
        and comp_store         = I_comp_price_hist_temp_rec.comp_store
        and NVL(ref_item,'-1') = NVL(I_comp_price_hist_temp_rec.ref_item,'-1');

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_comp_price_hist_temp_rec.item,
                                             I_comp_price_hist_temp_rec.comp_store);
      return FALSE;
   when OTHERS then
      if C_LOCK_HIST%ISOPEN then
         close C_LOCK_HIST;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMP_PRICE_HIST_DEL;
-----------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMP_PRICE_HIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN       SVC_COMP_PRICE_HIST.PROCESS_ID%TYPE,
                                 I_chunk_id        IN       SVC_COMP_PRICE_HIST.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64)                      := 'CORESVC_COMP_PRICE.PROCESS_COMP_PRICE_HIST';
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_PRICE_HIST';
   L_error                      BOOLEAN;
   L_process_error              BOOLEAN      := FALSE;
   L_comp_price_hist_temp_rec   COMP_PRICE_HIST%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_COMP_PRICE_HIST(I_process_id,
                                    I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.uk_comp_price_hist_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Item,Competitor Store,Recorded Date',
                     'COMP_PRICE_HIST_MISSING');
         L_error := TRUE;
      end if;
      if rec.action = action_mod then
         if rec.comp_retail is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMP_RETAIL',
                        'MUST_ENTER_COMP_PRICE');
            L_error := TRUE;
         end if;
         if rec.comp_retail_type is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMP_RETAIL_TYPE',
                        'MUST_ENT_COMP_RET_TYPE');
            L_error := TRUE;
         end if;
      end if;

      if PROCESS_COMP_PRICE_HIST_VAL(O_error_message,
                                     L_error,
                                     rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_comp_price_hist_temp_rec.rec_date          := rec.rec_date;
         L_comp_price_hist_temp_rec.comp_retail       := rec.comp_retail;
         L_comp_price_hist_temp_rec.comp_retail_type  := rec.comp_retail_type;
         L_comp_price_hist_temp_rec.multi_units       := rec.multi_units;
         L_comp_price_hist_temp_rec.multi_unit_retail := rec.multi_unit_retail;
         L_comp_price_hist_temp_rec.prom_start_date   := rec.prom_start_date;
         L_comp_price_hist_temp_rec.prom_end_date     := rec.prom_end_date;
         L_comp_price_hist_temp_rec.offer_type        := rec.offer_type;
         L_comp_price_hist_temp_rec.item              := rec.item;
         L_comp_price_hist_temp_rec.ref_item          := rec.ref_item;
         L_comp_price_hist_temp_rec.comp_store        := rec.comp_store;
         L_comp_price_hist_temp_rec.post_date         := SYSDATE;
         L_comp_price_hist_temp_rec.rpm_pull          := 'N';

         if rec.action = action_mod then
            if rec.comp_retail_type != 'P' then
               L_comp_price_hist_temp_rec.prom_start_date := NULL;
               L_comp_price_hist_temp_rec.prom_end_date   := NULL;
            end if;

            if rec.comp_retail_type = 'C' then
               L_comp_price_hist_temp_rec.offer_type := NULL;
            end if;

            if EXEC_COMP_PRICE_HIST_UPD(O_error_message,
                                        L_comp_price_hist_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_COMP_PRICE_HIST_DEL(O_error_message,
                                        L_comp_price_hist_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;

      
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COMP_PRICE_HIST;
------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete 
  from svc_comp_shop_list
 where process_id = I_process_id;
   delete 
  from svc_comp_price_hist
 where process_id = I_process_id;
 
END;
----------------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_COMP_PRICE.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
BEGIN
   LP_errors_tab := NEW LP_errors_tab_typ();
   if PROCESS_COMP_SHOP_LIST(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_COMP_PRICE_HIST(O_error_message,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into svc_admin_upld_er
         values LP_errors_tab(i);
   LP_errors_tab := NEW LP_errors_tab_typ();if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
   L_process_status := 'PE';
   end if;
  
   update svc_process_tracker
   set status = (CASE
              when status = 'PE'
              then 'PE'
                    else L_process_status
                    END),
    action_date = sysdate
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------
END CORESVC_COMP_PRICE;
/