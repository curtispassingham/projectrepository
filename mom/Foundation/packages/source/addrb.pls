CREATE OR REPLACE PACKAGE BODY ADDRESS_SQL AS
-------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message          IN OUT VARCHAR2,
                  O_module                 IN OUT ADDR.MODULE%TYPE,
                  O_key_value_1            IN OUT ADDR.KEY_VALUE_1%TYPE,
                  O_key_value_2            IN OUT ADDR.KEY_VALUE_2%TYPE,
                  O_addr_type              IN OUT ADDR.ADDR_TYPE%TYPE,
                  O_primary_addr_ind       IN OUT ADDR.PRIMARY_ADDR_IND%TYPE,
                  O_add_1                  IN OUT ADDR.ADD_1%TYPE,
                  O_add_2                  IN OUT ADDR.ADD_2%TYPE,
                  O_add_3                  IN OUT ADDR.ADD_3%TYPE,
                  O_city                   IN OUT ADDR.CITY%TYPE,
                  O_state                  IN OUT ADDR.STATE%TYPE,
                  O_country_id             IN OUT ADDR.COUNTRY_ID%TYPE,
                  O_post                   IN OUT ADDR.POST%TYPE,
                  O_jurisdiction_code      IN OUT ADDR.JURISDICTION_CODE%TYPE,
                  O_contact_name           IN OUT ADDR.CONTACT_NAME%TYPE,
                  O_contact_phone          IN OUT ADDR.CONTACT_PHONE%TYPE,
                  O_contact_telex          IN OUT ADDR.CONTACT_TELEX%TYPE,
                  O_contact_fax            IN OUT ADDR.CONTACT_FAX%TYPE,
                  O_contact_email          IN OUT ADDR.CONTACT_EMAIL%TYPE,
                  O_oracle_vendor_site_id  IN OUT ADDR.ORACLE_VENDOR_SITE_ID%TYPE,
                  I_add_key                IN     ADDR.ADDR_KEY%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ADDR_SQL.GET_INFO';

   cursor C_GET_INFO is
      select module,
             key_value_1,
             key_value_2,
             addr_type,
             primary_addr_ind,
             add_1,
             add_2,
             add_3,
             city,
             state,
             country_id,
             post,
             jurisdiction_code,
             contact_name,
             contact_phone,
             contact_telex,
             contact_fax,
             contact_email,
             oracle_vendor_site_id
        from addr
       where addr.addr_key = I_add_key;

BEGIN


   open C_GET_INFO;

   fetch C_GET_INFO into O_module,
                         O_key_value_1,
                         O_key_value_2,
                         O_addr_type,
                         O_primary_addr_ind,
                         O_add_1,
                         O_add_2,
                         O_add_3,
                         O_city,
                         O_state,
                         O_country_id,
                         O_post,
                         O_jurisdiction_code,
                         O_contact_name,
                         O_contact_phone,
                         O_contact_telex,
                         O_contact_fax,
                         O_contact_email,
                         O_oracle_vendor_site_id;
   if C_GET_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ADD_KEY',
                                           NULL,
                                           NULL,
                                           NULL);

      close C_GET_INFO;
   return FALSE;
   end if;
   ---

   close C_GET_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_INFO;
-----------------------------------------------------------------------------------
FUNCTION VALID_INVC_ADDR(O_error_message          IN OUT VARCHAR2,
                         O_exists                 IN OUT BOOLEAN,
                         I_supplier               IN     SUPS.SUPPLIER%TYPE,
                         I_partner_id             IN     PARTNER.PARTNER_ID%TYPE,
                         I_partner_type           IN     PARTNER.PARTNER_TYPE%TYPE)
RETURN BOOLEAN IS
   L_exists     varchar2(1) := 'N';
   L_program             VARCHAR2(64):= 'ADDRESS_SQL.VALID_INVC_ADDR';

   cursor C_SUPP_INVC_ADDR is
       select 'Y'
         from addr
        where key_value_1 = to_char(I_supplier)
          and module = 'SUPP'
          and addr_type = '05';
   cursor C_PART_INVC_ADDR is
          select 'Y'
            from addr
           where (key_value_1 = I_partner_type
          and key_value_2 = I_partner_id)
          and module = 'PTNR'
          and addr_type = '05';

BEGIN
   if I_supplier is NULL AND I_partner_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'I_supplier',
                                             'I_partner_id',
                                             'NOT NULL');
      return FALSE;
   end if;
   if I_partner_type is NULL and I_partner_id is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_partner_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is not null then
      open C_SUPP_INVC_ADDR;

      fetch C_SUPP_INVC_ADDR into L_exists;
         if L_exists = 'Y' then
            O_exists := TRUE;
         else
            O_exists := FALSE;
         end if;
      close C_SUPP_INVC_ADDR;
   else
      open C_PART_INVC_ADDR;
      fetch C_PART_INVC_ADDR into L_exists;
         if L_exists = 'Y' then
            O_exists := TRUE;
         else
            O_exists := FALSE;
         end if;
      close C_PART_INVC_ADDR;
   end if;
  ---
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END VALID_INVC_ADDR;
------------------------------------------------------------------------------------
FUNCTION VALID_ORD_ADDR(O_error_message          IN OUT VARCHAR2,
                        O_exists                 IN OUT BOOLEAN,
                        I_supplier               IN     SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS
   L_exists              VARCHAR2(1) := NULL;
   L_program             VARCHAR2(64):= 'ADDRESS_SQL.VALID_ORD_ADDR';

   cursor C_SUPP_ORD_ADDR is
       select 'x'
         from addr
        where key_value_1 = to_char(I_supplier)
          and module = 'SUPP'
          and addr_type = '04';
BEGIN
   if I_supplier is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                             'I_supplier',
                                             NULL,
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is not null then
      open C_SUPP_ORD_ADDR;
      fetch C_SUPP_ORD_ADDR into L_exists;
      if C_SUPP_ORD_ADDR%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      close C_SUPP_ORD_ADDR;
    end if;
  ---
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END VALID_ORD_ADDR;
------------------------------------------------------------------------------------
FUNCTION PREV_EXISTS(O_error_message       IN OUT VARCHAR2,
                     O_exists              IN OUT BOOLEAN,
                     I_key_value_1         IN     ADDR.KEY_VALUE_1%TYPE,
                     I_key_value_2         IN     ADDR.KEY_VALUE_2%TYPE,
                     I_addr_type           IN     ADDR.ADDR_TYPE%TYPE,
                     I_seq_no              IN     ADDR.SEQ_NO%TYPE)

RETURN BOOLEAN IS

   L_dummy               VARCHAR2(1) := NULL;
   L_program             VARCHAR2(64):= 'ADDRESS_SQL.PREV_EXISTS';

   cursor C_CHECK_PREV is
      select 'x'
        from addr
       where key_value_1      = I_key_value_1
         and (key_value_2     = I_key_value_2
          or I_key_value_2 is NULL)
         and addr_type        = I_addr_type
         and seq_no           < I_seq_no;

BEGIN

   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_1',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_addr_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_seq_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open  C_CHECK_PREV;
   fetch C_CHECK_PREV into L_dummy;
   if C_CHECK_PREV%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   close C_CHECK_PREV;

  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PREV_EXISTS;
-----------------------------------------------------------------------------------
FUNCTION DELETE_ALLOWED(O_error_message       IN  OUT VARCHAR2,
                        O_allowed             IN  OUT BOOLEAN,
                        I_key_value_1         IN      ADDR.KEY_VALUE_1%TYPE,
                        I_key_value_2         IN      ADDR.KEY_VALUE_2%TYPE,
                        I_addr_type           IN      ADDR.ADDR_TYPE%TYPE,
                        I_seq_no              IN      ADDR.SEQ_NO%TYPE,
                        I_primary_addr_ind    IN      ADDR.PRIMARY_ADDR_IND%TYPE,
                        I_module              IN      ADDR.MODULE%TYPE,
                        I_mandatory           IN      ADD_TYPE_MODULE.MANDATORY_IND%TYPE)
RETURN BOOLEAN IS

   L_dummy               VARCHAR2(1) := NULL;
   L_program             VARCHAR2(64):= 'ADDRESS_SQL.DELETE_ALLOWED';
   
   cursor C_CHECK_MULTIPLE_EXISTS is
         select 'x'
           from addr
          where key_value_1      = I_key_value_1
            and (key_value_2     = I_key_value_2
             or I_key_value_2 is NULL)
            and addr_type        = I_addr_type
            AND module           = I_module
         and seq_no          != I_seq_no;

BEGIN

   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_1',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_addr_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_seq_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_module',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   if I_mandatory = 'N' then
      -- if address is a non-mandatory address type, it can be deleted regardless
      -- of whether it is the primary address for that address type
      O_allowed := TRUE;
   else
      open C_CHECK_MULTIPLE_EXISTS;
      fetch C_CHECK_MULTIPLE_EXISTS into L_dummy;
   
         if (C_CHECK_MULTIPLE_EXISTS%FOUND and I_primary_addr_ind = 'Y') or (I_primary_addr_ind = 'Y')then
            -- can't delete primary address if multiple addresses exist for type
            O_allowed := FALSE;
         else
            O_allowed := TRUE;
         end if;
      close C_CHECK_MULTIPLE_EXISTS;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CHECK_MULTIPLE_EXISTS%ISOPEN then
         close C_CHECK_MULTIPLE_EXISTS;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ALLOWED;
-----------------------------------------------------------------------------------

FUNCTION UPDATE_PRIM_ADD_IND(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_module           IN     ADDR.MODULE%TYPE,
                             I_key_value_1      IN     ADDR.KEY_VALUE_1%TYPE,
                             I_key_value_2      IN     ADDR.KEY_VALUE_2%TYPE,
                             I_addr_type        IN     ADDR.ADDR_TYPE%TYPE,
                             I_seq_no           IN     ADDR.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ADDRESS_SQL.UPDATE_PRIM_ADD_IND';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   cursor C_LOCK_FORMER_PRIM_ADD is
      select 'x'
        from addr
       where module           = I_module
         and key_value_1      = I_key_value_1
         and (key_value_2     = I_key_value_2 or
             (key_value_2 is NULL and I_key_value_2 is NULL))
         and addr_type        = I_addr_type
         and primary_addr_ind = 'Y'
         and seq_no          != I_seq_no
         for update nowait;

BEGIN

   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_1',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_module',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_addr_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_seq_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   open C_LOCK_FORMER_PRIM_ADD;

   close C_LOCK_FORMER_PRIM_ADD;
   update addr
      set primary_addr_ind = 'N'
    where module           = I_module
      and key_value_1      = I_key_value_1
      and (key_value_2     = I_key_value_2 or
          (key_value_2 is NULL and I_key_value_2 is NULL))
      and addr_type        = I_addr_type
      and primary_addr_ind = 'Y'
      and seq_no          != I_seq_no;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_FORMER_PRIM_ADD%ISOPEN then
         close C_LOCK_FORMER_PRIM_ADD;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED','ADDR',
                                            I_module);
      return FALSE;
   when OTHERS then
      if C_LOCK_FORMER_PRIM_ADD%ISOPEN then
         close C_LOCK_FORMER_PRIM_ADD;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_PRIM_ADD_IND;
-----------------------------------------------------------------------------------
FUNCTION GET_MAX_SEQ_NO(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_max_seq_no       IN OUT ADDR.SEQ_NO%TYPE,
                        I_module           IN     ADDR.MODULE%TYPE,
                        I_key_value_1      IN     ADDR.KEY_VALUE_1%TYPE,
                        I_key_value_2      IN     ADDR.KEY_VALUE_2%TYPE,
                        I_addr_type        IN     ADDR.ADDR_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ADDRESS_SQL.GET_MAX_SEQ_NO';

   cursor C_MAX_SEQ_NO IS
      select NVL(MAX(SEQ_NO), 0)
        from addr
       where module       = I_module
         and key_value_1  = I_key_value_1
         and (key_value_2 = I_key_value_2 or
             (key_value_2 is NULL and I_key_value_2 is NULL))
         and addr_type    = I_addr_type;

BEGIN

   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_1',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_module',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_addr_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   open C_MAX_SEQ_NO;
   fetch C_MAX_SEQ_NO into O_max_seq_no;
   close C_MAX_SEQ_NO;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_MAX_SEQ_NO;
------------------------------------------------------------------------------------
FUNCTION GET_ADD_TYPE_MODULE_ROW(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_add_type_module  IN OUT ADD_TYPE_MODULE%ROWTYPE,
                                 I_module           IN     ADD_TYPE_MODULE.MODULE%TYPE,
                                 I_addr_type        IN     ADD_TYPE_MODULE.ADDRESS_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ADDRESS_SQL.GET_ADD_TYPE_MODULE_ROW';

   cursor C_ADD_TYPE_ROW is
      select *
        from add_type_module
       where module       = I_module
         and address_type = I_addr_type
         and rownum       = 1;

BEGIN

   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_addr_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_module',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   O_add_type_module := NULL;

   open C_ADD_TYPE_ROW;

   fetch C_ADD_TYPE_ROW into O_add_type_module;

   close C_ADD_TYPE_ROW;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_ADD_TYPE_MODULE_ROW;
------------------------------------------------------------------------------------
FUNCTION DELETE_ADDRESSES(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_key_value_1      IN     ADDR.KEY_VALUE_1%TYPE,
                          I_key_value_2      IN     ADDR.KEY_VALUE_2%TYPE,
                          I_addr_type        IN     ADDR.ADDR_TYPE%TYPE,
                          I_seq_no           IN     ADDR.SEQ_NO%TYPE,
                          I_primary_addr_ind IN     ADDR.PRIMARY_ADDR_IND%TYPE,
                          I_module           IN     ADDR.MODULE%TYPE,
                          I_mandatory        IN     ADD_TYPE_MODULE.MANDATORY_IND%TYPE
                          )
RETURN BOOLEAN IS
   L_program            VARCHAR2(64) := 'ADDRESS_SQL.DELETE_ADDRESSES';
   L_allowed            BOOLEAN      := FALSE;
   L_seq_no             ADDR.SEQ_NO%TYPE := NULL;
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ADDR_TL is
      select 'x'
        from addr_tl
       where addr_key in (select addr_key 
                            from addr
                           where key_value_1  = I_key_value_1
                             and (key_value_2 = I_key_value_2 or
                                 (key_value_2 is NULL and I_key_value_2 is NULL))
                             and module       = I_module
                             and addr_type    = I_addr_type
                             and seq_no       = I_seq_no)
         for update nowait;
         
   cursor C_LOCK_ADDR is
      select 'x'
        from addr
       where key_value_1  = I_key_value_1
         and (key_value_2 = I_key_value_2 or
             (key_value_2 is NULL and I_key_value_2 is NULL))
         and module       = I_module
         and addr_type    = I_addr_type
         and seq_no       = I_seq_no
         for update nowait;

   cursor C_CHECK_SEQ is
      select min(seq_no)
        from addr
       where key_value_1      = I_key_value_1
         and (key_value_2     = I_key_value_2
          or I_key_value_2 is NULL)
         and addr_type        = I_addr_type
         and module           = I_module
         and seq_no          != I_seq_no;
         
   cursor C_LOCK_ADDR_CFA_EXT is
      select 'x' 
        from addr_cfa_ext
       where addr_key in (select addr_key 
                            from addr
                           where key_value_1  = I_key_value_1
                             and (key_value_2 = I_key_value_2 or
                                 (key_value_2 is NULL and I_key_value_2 is NULL))
                             and module       = I_module
                             and addr_type    = I_addr_type
                             and seq_no       = I_seq_no)
         for update nowait;         
      
BEGIN

   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_1',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_module',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_addr_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_seq_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_seq_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if DELETE_ALLOWED(O_error_message,
                     L_allowed,
                     I_key_value_1,
                     I_key_value_2,
                     I_addr_type,
                     I_seq_no,
                     I_primary_addr_ind,
                     I_module,
                     I_mandatory
                     ) = FALSE then
      return FALSE;
   end if;
   
   if L_allowed = TRUE then
      -- delete from cfa extension table before deleting from addr table
      
      open C_LOCK_ADDR_CFA_EXT;
      close C_LOCK_ADDR_CFA_EXT;

      delete from addr_cfa_ext 
            where addr_key in (select addr_key 
                                from ADDR
                               where key_value_1  = I_key_value_1
                                 and (key_value_2 = I_key_value_2 or
                                     (key_value_2 is NULL and I_key_value_2 is NULL))
                                 and module       = I_module
                                 and addr_type    = I_addr_type
                                 and seq_no       = I_seq_no);
      --
      open C_LOCK_ADDR_TL;
      close C_LOCK_ADDR_TL;
      --
      delete from ADDR_TL
            where addr_key in (select addr_key 
                                from ADDR
                               where key_value_1  = I_key_value_1
                                 and (key_value_2 = I_key_value_2 or
                                     (key_value_2 is NULL and I_key_value_2 is NULL))
                                 and module       = I_module
                                 and addr_type    = I_addr_type
                                 and seq_no       = I_seq_no);
      open C_LOCK_ADDR;
      close C_LOCK_ADDR;
      --
      
      delete from ADDR
            where key_value_1  = I_key_value_1
              and (key_value_2 = I_key_value_2 or
                  (key_value_2 is NULL and I_key_value_2 is NULL))
              and module       = I_module
              and addr_type    = I_addr_type
              and seq_no       = I_seq_no;
      -- If the address type is non-mandatory and it is a primary address, return the seq_no
      -- for the earliest non-primary key address for the address type and assign that record
      -- to now be the primary address record for the address type
      if I_mandatory = 'N' and I_primary_addr_ind = 'Y' then
         -- Retrieve the next created sequence for the non-mandatoty address type
         -- if it exists
         open C_CHECK_SEQ;
         fetch C_CHECK_SEQ into L_seq_no;
         close C_CHECK_SEQ;
         if L_seq_no is not null then
            update ADDR
               set primary_addr_ind = 'Y'
             where key_value_1      = I_key_value_1
               and (key_value_2     = I_key_value_2
                or I_key_value_2 is NULL)
               and addr_type        = I_addr_type
               and module           = I_module
               and seq_no           = L_seq_no;
         end if;
      end if;
      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
   else
     O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_PRIM_ADDR',
                                           NULL,
                                           NULL,
                                           NULL);
     return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ADDR%ISOPEN then
         close C_LOCK_ADDR;
      end if;
      if C_CHECK_SEQ%ISOPEN then
         close C_CHECK_SEQ;
      end if;
      if C_LOCK_ADDR_CFA_EXT%ISOPEN then
         close C_LOCK_ADDR_CFA_EXT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED','ADDR',
                                            I_module);
      return FALSE;
   when OTHERS then
      if C_LOCK_ADDR%ISOPEN then
         close C_LOCK_ADDR;
      end if;
      if C_CHECK_SEQ%ISOPEN then
         close C_CHECK_SEQ;
      end if;
      if C_LOCK_ADDR_CFA_EXT%ISOPEN then
         close C_LOCK_ADDR_CFA_EXT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ADDRESSES;
------------------------------------------------------------------------------------
FUNCTION MAND_ADD_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_address_type    IN OUT   ADDR.ADDR_TYPE%TYPE,
                         I_module          IN       ADDR.MODULE%TYPE,
                         I_key_value_1     IN       ADDR.KEY_VALUE_1%TYPE,
                         I_key_value_2     IN       ADDR.KEY_VALUE_2%TYPE)
RETURN BOOLEAN IS
   cursor C_MAND_ADD_TYPE is
      select address_type
        from add_type_module
       where module        = I_module
         and mandatory_ind = 'Y'
         and not exists (select 'x'
                           from addr
                          where addr.key_value_1  = I_key_value_1
                            and (addr.key_value_2 = I_key_value_2 or
                                (addr.key_value_2 is NULL and I_key_value_2 is NULL))
                            and addr.addr_type    = add_type_module.address_type
                            and addr.module       = I_module
                            and rownum            = 1)
         and rownum        = 1;

   L_program         VARCHAR2(64) := 'ADDRESS_SQL.MAND_ADD_EXISTS';
   L_mand_add_type   ADD_TYPE_MODULE.ADDRESS_TYPE%TYPE;

BEGIN

   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_1',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_module',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   open C_MAND_ADD_TYPE;

   fetch C_MAND_ADD_TYPE into L_mand_add_type;

   close C_MAND_ADD_TYPE;

   if L_mand_add_type is NULL then
      O_address_type := NULL;
   else
      O_address_type := L_mand_add_type;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END MAND_ADD_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ADDR_KEY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_addr_key        OUT      ADDR.ADDR_KEY%TYPE,
                      I_store           IN       STORE.STORE%TYPE,
                      I_module          IN       ADD_TYPE_MODULE.MODULE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ADDRESS_SQL.GET_ADDR_KEY';
   L_module          ADD_TYPE_MODULE.MODULE%TYPE;
   L_store_type      STORE.STORE_TYPE%TYPE;


   cursor C_ADDR is
      select a.addr_key
        from addr a, add_type_module atm
       where atm.primary_ind = 'Y'
         and atm.mandatory_ind = 'Y'
         and a.module = L_module
         and a.primary_addr_ind = 'Y'
         and a.module = atm.module
         and a.key_value_1 = to_char(I_store);

   cursor C_STORE is
      select st.store_type
        from store  st
       where st.store = I_store
       union all
      select sa.store_type
        from store_add  sa
       where sa.store = I_store;

BEGIN
   if I_module = 'ST' then
      open C_STORE;
      fetch C_STORE into L_store_type;
      if C_STORE%NOTFOUND then
         close C_STORE;
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                               NULL, NULL, NULL);
         return FALSE;
      end if;
      close C_STORE;

      if L_store_type in ('F') then
         L_module := 'WFST';
      else
         L_module := I_module;
      end if;
   else
      L_module := I_module;
   end if;
   open C_ADDR;
   fetch C_ADDR into O_addr_key;
   if C_ADDR%NOTFOUND then
      CLOSE C_ADDR;
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_ADDRESS',
                                            NULL, NULL, NULL);
      return FALSE;
   end if;
   close C_ADDR;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ADDR%ISOPEN then
         close C_ADDR;
      end if; 
      if C_STORE%ISOPEN then
         close C_STORE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_ADDR_KEY;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ADDR_KEY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_addr_key          IN OUT   ADDR.ADDR_KEY%TYPE,
                      I_external_ref_id   IN       ADDR.EXTERNAL_REF_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ADDRESS_SQL.GET_ADDR_KEY';

   cursor C_ADDR is
      select addr_key
        from addr
       where external_ref_id = I_external_ref_id;

BEGIN
   if I_external_ref_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_external_ref_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_ADDR;
   fetch C_ADDR into O_addr_key;
   close C_ADDR;   
   
   if O_addr_key is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_EXTERNAL_REF_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ADDR%ISOPEN then
         close C_ADDR;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_ADDR_KEY;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_ADDR_TYPE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_primary_addr_type   OUT      ADD_TYPE_MODULE.ADDRESS_TYPE%TYPE,
                               I_module              IN       ADD_TYPE_MODULE.MODULE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ADDRESS_SQL.GET_PRIMARY_ADDR_TYPE';

   cursor C_ADD_TYPE_MODULE is
      select address_type
       from add_type_module
      where primary_ind = 'Y'
        and mandatory_ind = 'Y'
        and module = I_module;

BEGIN
   open C_ADD_TYPE_MODULE;
   fetch C_ADD_TYPE_MODULE into O_primary_addr_type;
   if C_ADD_TYPE_MODULE%NOTFOUND then
      close C_ADD_TYPE_MODULE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_ADDR_TYPE',
                                             NULL, 'ADDRESS_SQL.GET_PRIMARY_ADDR_TYPE', NULL);
      return FALSE;
   end if;
   close C_ADD_TYPE_MODULE;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ADD_TYPE_MODULE%ISOPEN then
         close C_ADD_TYPE_MODULE;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_PRIMARY_ADDR_TYPE;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       TYPE_ADDRESS_TBL)
RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'ADDRESS_SQL.INSERT_ADDR';
   L_addr_seq              NUMBER(10) := 0;
   L_primary_addr_exists   VARCHAR2(1);
   L_addr_type             ADDR.ADDR_TYPE%TYPE;
   L_store                 STORE.STORE%TYPE;

BEGIN
   if I_address_tbl.count > 0 then
      FOR i IN I_address_tbl.FIRST..I_address_tbl.LAST LOOP
      
         INSERT into addr(ADDR_KEY,
                          MODULE,
                          KEY_VALUE_1,
                          KEY_VALUE_2,
                          SEQ_NO,
                          ADDR_TYPE,
                          PRIMARY_ADDR_IND,
                          ADD_1,
                          ADD_2,
                          ADD_3,
                          CITY,
                          STATE,
                          COUNTRY_ID,
                          POST,
                          JURISDICTION_CODE,
                          CONTACT_NAME,
                          CONTACT_PHONE,
                          CONTACT_TELEX,
                          CONTACT_FAX,
                          CONTACT_EMAIL,
                          ORACLE_VENDOR_SITE_ID,
                          EDI_ADDR_CHG,
                          COUNTY,
                          PUBLISH_IND,
                          EXTERNAL_REF_ID)
                   VALUES(addr_sequence.NEXTVAL,
                          I_address_tbl(i).module,
                          I_address_tbl(i).key_value_1,
                          I_address_tbl(i).key_value_2,
                          (SELECT NVL(MAX(seq_no) + 1, 1)
                            FROM addr
                           WHERE key_value_1 = I_address_tbl(i).key_value_1
                             AND (key_value_2 = I_address_tbl(i).key_value_2 or
                                  (key_value_2 is NULL and I_address_tbl(i).key_value_2 is NULL))
                             AND addr_type = I_address_tbl(i).addr_type
                             AND module = I_address_tbl(i).module),
                          I_address_tbl(i).addr_type,
                          I_address_tbl(i).primary_addr_ind,
                          I_address_tbl(i).add_1,
                          I_address_tbl(i).add_2,
                          I_address_tbl(i).add_3,
                          I_address_tbl(i).city,
                          I_address_tbl(i).state,
                          I_address_tbl(i).country_id,
                          I_address_tbl(i).post,
                          I_address_tbl(i).jurisdiction_code,
                          I_address_tbl(i).contact_name,
                          I_address_tbl(i).contact_phone,
                          I_address_tbl(i).contact_telex,
                          I_address_tbl(i).contact_fax,
                          I_address_tbl(i).contact_email,
                          I_address_tbl(i).oracle_vendor_site_id,
                          I_address_tbl(i).edi_addr_chg,
                          I_address_tbl(i).county,
                          I_address_tbl(i).publish_ind,
                          I_address_tbl(i).external_ref_id);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ADDR;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       TYPE_ADDRESS_TBL)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'ADDRESS_SQL.UPDATE_ADDR';

BEGIN
   FOR i IN I_address_tbl.FIRST..I_address_tbl.LAST LOOP
      if not LOCK_ADDR(O_error_message,
                       I_address_tbl(i).addr_key) then
         return FALSE;
      end if;

      UPDATE addr
         SET ADD_1             = I_address_tbl(i).add_1,
             ADD_2             = I_address_tbl(i).add_2,
             ADD_3             = I_address_tbl(i).add_3,
             CITY              = I_address_tbl(i).city,
             STATE             = I_address_tbl(i).state,
             COUNTRY_ID        = I_address_tbl(i).country_id,
             POST              = I_address_tbl(i).post,
             CONTACT_NAME      = I_address_tbl(i).contact_name,
             CONTACT_PHONE     = I_address_tbl(i).contact_phone,
             CONTACT_TELEX     = I_address_tbl(i).contact_telex,
             CONTACT_FAX       = I_address_tbl(i).contact_fax,
             CONTACT_EMAIL     = I_address_tbl(i).contact_email,
             JURISDICTION_CODE = I_address_tbl(i).jurisdiction_code,
             COUNTY            = I_address_tbl(i).county,
             PRIMARY_ADDR_IND  = I_address_tbl(i).primary_addr_ind
       WHERE addr_key = I_address_tbl(i).addr_key;

       if SQL%NOTFOUND then
          O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
          return FALSE;
       end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ADDR;
-----------------------------------------------------------------------------------------------
FUNCTION LOCK_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_addr_key        IN       ADDR.ADDR_KEY%TYPE)

RETURN BOOLEAN IS

   L_program      VARCHAR2(64)  := 'ADDRESS_SQL.LOCK_ADDR';
   L_table        VARCHAR2(20)  := 'ADDR';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ADDR is
      select 'x'
        from addr
       where addr_key = I_addr_key
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ADDR', 'ADDR', 'addr: '||I_addr_key);
   open C_LOCK_ADDR;

   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ADDR', 'ADDR', 'addr: '||I_addr_key);
   close C_LOCK_ADDR;

   ---
   return TRUE;
EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'ADDR: '||I_addr_key,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_ADDR;
------------------------------------------------------------------------------------
FUNCTION GET_ADDR_TYPE_DESC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_addr_type_desc   IN OUT   ADD_TYPE_TL.TYPE_DESC%TYPE,
                            I_addr_type        IN       ADD_TYPE.ADDRESS_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64)  := 'ADDRESS_SQL.GET_ADDR_TYPE_DESC';
   L_table        VARCHAR2(20)  := 'V_ADD_TYPE_TL';

   cursor C_ADDR_TYPE_DESC is
      select type_desc
        from v_add_type_tl
       where address_type = I_addr_type;

BEGIN

   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_addr_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_ADDR_TYPE_DESC',
                    L_table,
                    'Add_type: '||I_addr_type);
   open C_ADDR_TYPE_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ADDR_TYPE_DESC',
                    L_table,
                    'Add_type: '||I_addr_type);
   fetch C_ADDR_TYPE_DESC into O_addr_type_desc;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ADDR_TYPE_DESC',
                    L_table,
                    'Add_type: '||I_addr_type);
   close C_ADDR_TYPE_DESC;

   if O_addr_type_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ADDR_TYPE',
                                            I_addr_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      if C_ADDR_TYPE_DESC%ISOPEN then
         close C_ADDR_TYPE_DESC;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ADDR_TYPE_DESC;
-------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ADDR_TYPE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_addr_type_desc   IN OUT   ADD_TYPE_TL.TYPE_DESC%TYPE,
                               O_addr_type        IN OUT   ADD_TYPE.ADDRESS_TYPE%TYPE,
                               I_module           IN       ADD_TYPE_MODULE.MODULE%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)  := 'ADDRESS_SQL.GET_DEFAULT_ADDR_TYPE';
   L_table        VARCHAR2(20)  := 'V_ADD_TYPE_TL';

   cursor C_ADDR_DEFAULT_TYPE_DESC is
      select vatt.type_desc,
             vatt.address_type
        from v_add_type_tl vatt,
             add_type_module atm
       where vatt.address_type = atm.address_type
         and decode(atm.primary_ind, 'N', atm.mandatory_ind, atm.primary_ind) = 'Y'
         and atm.module = I_module;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_ADDR_DEFAULT_TYPE_DESC',
                    L_table,
                    'Module: '||I_module);
   open C_ADDR_DEFAULT_TYPE_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ADDR_DEFAULT_TYPE_DESC',
                    L_table,
                    'Module: '||I_module);

   fetch C_ADDR_DEFAULT_TYPE_DESC into O_addr_type_desc,
                                       O_addr_type;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ADDR_DEFAULT_TYPE_DESC',
                    L_table,
                    'Module: '||I_module);
   close C_ADDR_DEFAULT_TYPE_DESC;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ADDR_DEFAULT_TYPE_DESC%ISOPEN then
         close C_ADDR_DEFAULT_TYPE_DESC;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
END GET_DEFAULT_ADDR_TYPE;
-------------------------------------------------------------------------------------
FUNCTION CREATE_UPDATE_ALLOWED(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_allowed          IN OUT   BOOLEAN,
                               I_addr_type        IN       ADD_TYPE.ADDRESS_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)  := 'ADDRESS_SQL.CREATE_UPDATE_ALLOWED';
   L_external_addr_ind  ADD_TYPE.EXTERNAL_ADDR_IND%TYPE;

   cursor C_EXT_IND_CHECK is
      select external_addr_ind
        from add_type
       where address_type = I_addr_type;

BEGIN
   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_addr_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_allowed := TRUE;

   open C_EXT_IND_CHECK;
   fetch C_EXT_IND_CHECK into L_external_addr_ind;
   close C_EXT_IND_CHECK;

   if L_external_addr_ind = 'Y' then
      O_allowed := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_EXT_IND_CHECK%ISOPEN then
         close C_EXT_IND_CHECK;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
END CREATE_UPDATE_ALLOWED;
-------------------------------------------------------------------------------------
FUNCTION GET_SUPP_PRIMARY_ADDR_TYPE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_primary_addr_type    OUT   ADD_TYPE_MODULE.ADDRESS_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ADDRESS_SQL.GET_SUPP_PRIMARY_ADDR_TYPE';

   cursor C_ADD_TYPE_MODULE is
      select address_type
       from add_type_module
      where primary_ind = 'Y'
        and mandatory_ind = 'Y'
        and module = 'SUPP';

BEGIN
   open C_ADD_TYPE_MODULE;
   fetch C_ADD_TYPE_MODULE into O_primary_addr_type;
   if C_ADD_TYPE_MODULE%NOTFOUND then
      close C_ADD_TYPE_MODULE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_ADDR_TYPE',
                                             NULL, 'ADDRESS_SQL.GET_PRIMARY_ADDR_TYPE', NULL);
      return FALSE;
   end if;
   close C_ADD_TYPE_MODULE;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ADD_TYPE_MODULE%ISOPEN then
         close C_ADD_TYPE_MODULE;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_SUPP_PRIMARY_ADDR_TYPE;
--------------------------------------------------------------------------------------------
--FUNCTION NAME: GET_COUNTRY_ID
--Purpose:       This function will return the country ID for the passed in Supplier
--               and Address Type
-------------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_ID (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_country_id        IN OUT   ADDR.COUNTRY_ID%TYPE,
                         I_supplier          IN       SUPS.SUPPLIER %TYPE,
                         I_addr_type         IN       ADDR.ADDR_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ADDRESS_SQL.GET_COUNTRY_ID';

   cursor C_GET_COUNTRY_ID is
     select country_id
       from addr
      where primary_addr_ind = 'Y'
        and module = 'SUPP'
        and addr_type = I_addr_type
        and key_value_1 = to_char(I_supplier);

BEGIN
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'addr_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_COUNTRY_ID;
   fetch C_GET_COUNTRY_ID into O_country_id;
   if C_GET_COUNTRY_ID%NOTFOUND then
      close C_GET_COUNTRY_ID;
      O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_ADDRESS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   close C_GET_COUNTRY_ID;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_COUNTRY_ID%ISOPEN then
         close C_GET_COUNTRY_ID;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_COUNTRY_ID;
---------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_add_1           IN OUT   ADDR.ADD_1%TYPE,
                       O_add_2           IN OUT   ADDR.ADD_2%TYPE,
                       O_add_3           IN OUT   ADDR.ADD_3%TYPE,
                       O_city            IN OUT   ADDR.CITY%TYPE,
                       O_state           IN OUT   ADDR.STATE%TYPE,
                       O_country_id      IN OUT   ADDR.COUNTRY_ID%TYPE,
                       O_post            IN OUT   ADDR.POST%TYPE,
                       I_module          IN       ADDR.MODULE%TYPE,
                       I_key_value_1     IN       ADDR.KEY_VALUE_1%TYPE,
                       I_key_value_2     IN       ADDR.KEY_VALUE_2%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'ADDRESS_SQL.GET_PRIM_ADDR';
   L_jurisdiction_code   ADDR.JURISDICTION_CODE%TYPE := NULL;

BEGIN
   if GET_PRIM_ADDR(O_error_message,
                    O_add_1,
                    O_add_2,
                    O_add_3,
                    O_city,
                    O_state,
                    O_country_id,
                    O_post,
                    L_jurisdiction_code,
                    I_module,
                    I_key_value_1,
                    I_key_value_2) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END GET_PRIM_ADDR;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_ADDR (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_country_id          IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                     IO_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                     IO_state               IN OUT   STATE.STATE%TYPE,
                     IO_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                     I_jurisdiction         IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                     IO_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                     IO_dup_exists          IN OUT   VARCHAR2,
                     I_calling_module       IN       VARCHAR2,
                     I_calling_context      IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)         := 'ADDRESS_SQL.CHECK_ADDR';
   L_jurisdiction_code   COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE;
   L_jurisdiction_desc   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   L_state               STATE.STATE%TYPE;
   L_country_id          COUNTRY.COUNTRY_ID%TYPE;
   L_state_desc          STATE.DESCRIPTION%TYPE;
   L_country_desc        COUNTRY.COUNTRY_DESC%TYPE;
   L_rowcount            NUMBER := 0;

   cursor C_JURISDICTION is
      select jurisdiction_code,
             jurisdiction_desc,
             state,
             country_id
        from country_tax_jurisdiction
       where jurisdiction_code = NVL(I_jurisdiction,jurisdiction_code)
         and state = NVL(IO_state,state)
         and country_id = NVL(IO_country_id,country_id);

   cursor C_STATE is
      select description,
             country_id
        from state
       where state = NVL(IO_state,state)
         and country_id = NVL(IO_country_id,country_id);

BEGIN
   if I_calling_context = ADDRESS_SQL.JURIS then
      open C_JURISDICTION;
      LOOP
         fetch C_JURISDICTION into L_jurisdiction_code,
                                   L_jurisdiction_desc,
                                   L_state,
                                   L_country_id;
         L_rowcount := C_JURISDICTION%ROWCOUNT;
         exit when C_JURISDICTION%ROWCOUNT > 1 or C_JURISDICTION%NOTFOUND;
      END LOOP;
      close C_JURISDICTION;
      if L_rowcount = 1 then
         IO_dup_exists := 'N';
         IO_jurisdiction_desc  := L_jurisdiction_desc;
         IO_state      := L_state;
         IO_country_id := L_country_id;
         if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                          IO_country_id,
                                          IO_country_desc) = FALSE then
            return FALSE;
         end if;
         if GEOGRAPHY_SQL.STATE_DESC(O_error_message,
                                     IO_state_desc,
                                     IO_state,
                                     IO_country_id) = FALSE then
            return FALSE;
         end if;
      end if;
   elsif I_calling_context = ADDRESS_SQL.ST then
      open C_STATE;
      LOOP
         fetch C_STATE into L_state_desc,
                            L_country_id;
         L_rowcount := C_STATE%ROWCOUNT;
         Exit when C_STATE%ROWCOUNT > 1 or C_STATE%NOTFOUND;
      END LOOP;
      close C_STATE;
      if L_rowcount = 1 then
         IO_dup_exists := 'N';
         IO_state_desc := L_state_desc;
         IO_country_id := L_country_id;
         if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                          IO_country_id,
                                          IO_country_desc) = FALSE then
            return FALSE;
         end if;
      end if;
   elsif I_calling_context = ADDRESS_SQL.CNTRY then
      if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                       IO_country_id,
                                       IO_country_desc) = FALSE then
         return FALSE;
      end if;
      L_rowcount := 1;
   end if;
   if L_rowcount = 0 then
      if I_calling_module = ADDRESS_SQL.JURIS then
         if IO_state is NOT NULL or IO_country_id is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_CNTRY_ST_JURIS',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         else
            O_error_message := SQL_LIB.CREATE_MSG('INV_TAX_JURIS',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      elsif I_calling_module = ADDRESS_SQL.ST then
         if IO_country_id is NOT NULL and I_jurisdiction is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_COUNTRY_STATE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         elsif IO_country_id is NOT NULL and I_jurisdiction is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_CNTRY_ST_JURIS',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         else
            O_error_message := SQL_LIB.CREATE_MSG('INV_STATE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      elsif I_calling_module = ADDRESS_SQL.CNTRY then
         if I_calling_context = ADDRESS_SQL.ST then
            O_error_message := SQL_LIB.CREATE_MSG('NO_COUNTRY_STATE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         elsif I_jurisdiction is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_CNTRY_ST_JURIS',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         else
            O_error_message := SQL_LIB.CREATE_MSG('INV_COUNTRY',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;
   elsif L_rowcount > 1 then
      if I_calling_context = ADDRESS_SQL.JURIS then
         IO_jurisdiction_desc := NULL;
         O_error_message := SQL_LIB.CREATE_MSG('MULTI_ADDR_COMB',
                                               NULL,
                                               NULL,
                                               NULL);
      end if;
      ---
      if I_calling_module = ADDRESS_SQL.ST then
         IO_state_desc := NULL;
         O_error_message := SQL_LIB.CREATE_MSG('MULT_ST_CTRY_COMB_EXISTS',
                                               NULL,
                                               NULL,
                                               NULL);
      elsif I_calling_module = ADDRESS_SQL.CNTRY then
         if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                          IO_country_id,
                                          IO_country_desc) = FALSE then
            return FALSE;
         end if;
      end if;

      IO_dup_exists := 'Y';

   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_ADDR;
---------------------------------------------------------------------------------------------
FUNCTION STATE_CON RETURN VARCHAR2 IS
BEGIN
   return ADDRESS_SQL.ST;
END STATE_CON;
---------------------------------------------------------------------------------------------
FUNCTION COUNTRY_CON RETURN VARCHAR2 IS
BEGIN
   return ADDRESS_SQL.CNTRY;
END COUNTRY_CON;
---------------------------------------------------------------------------------------------
FUNCTION JURIS_CON RETURN VARCHAR2 IS
BEGIN
   return ADDRESS_SQL.JURIS;
END JURIS_CON;
---------------------------------------------------------------------------------------------
--FUNCTION Name: VALID_ADDR
--Purpose:       This funtion will validate if there exists an address for the values passed in.
---------------------------------------------------------------------------------------------
FUNCTION VALID_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists          IN OUT   BOOLEAN,
                    I_module          IN       ADD_TYPE_MODULE.MODULE%TYPE,
                    I_addr_type       IN       ADD_TYPE.ADDRESS_TYPE%TYPE,
                    I_key_value_1     IN       ADDR.KEY_VALUE_1%TYPE,
                    I_key_value_2     IN       ADDR.KEY_VALUE_2%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ADDRESS_SQL.VALID_ADDR';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHECK_EXISTS is
     select 'x'
       from addr
      where module = I_module
        and addr_type = I_addr_type
        and key_value_1 = I_key_value_1
        and ((I_key_value_2 is NULL and key_value_2 is NULL) or
              key_value_2 = I_key_value_2);

BEGIN
   ---
   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_module',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_addr_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_addr_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_key_value_1',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_exists;
   close C_CHECK_EXISTS;
   --
   if L_exists is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CHECK_EXISTS%ISOPEN then
         close C_CHECK_EXISTS;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
      return FALSE;
END VALID_ADDR;

---------------------------------------------------------------------------------------------

FUNCTION GET_PRIM_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_add_1           IN OUT   ADDR.ADD_1%TYPE,
                       O_add_2           IN OUT   ADDR.ADD_2%TYPE,
                       O_add_3           IN OUT   ADDR.ADD_3%TYPE,
                       O_city            IN OUT   ADDR.CITY%TYPE,
                       O_state           IN OUT   ADDR.STATE%TYPE,
                       O_country_id      IN OUT   ADDR.COUNTRY_ID%TYPE,
                       O_post            IN OUT   ADDR.POST%TYPE,
                       O_jurisdiction    IN OUT   ADDR.JURISDICTION_CODE%TYPE,
                       I_module          IN       ADDR.MODULE%TYPE,
                       I_key_value_1     IN       ADDR.KEY_VALUE_1%TYPE,
                       I_key_value_2     IN       ADDR.KEY_VALUE_2%TYPE)
RETURN BOOLEAN IS
    
    L_program         VARCHAR2(64) := 'ADDRESS_SQL.GET_PRIM_ADDR';
 
    -- for locations - store, warehouse, wholesale/franchise stores and partners that are external finishers
    cursor C_GET_LOC_PRIM_ADDR is
       select add_1,
              add_2,
              add_3,
              city,
              state,
              country_id,
              post,
              jurisdiction_code
         from mv_loc_prim_addr mv
        where mv.loc = DECODE(I_module,'PTNR',TO_NUMBER(I_key_value_2),
                                              TO_NUMBER(I_key_value_1));

   cursor C_GET_SUPP_PRIM_ADDR is
      select add_1,
             add_2,
             add_3,
             city,
             state,
             country_id,
             post,
             jurisdiction_code
        from addr
       where primary_addr_ind = 'Y'
         and addr_type = '01'
         and module = I_module
         and key_value_1 = I_key_value_1;

   -- for partners that are NOT external finisher
   cursor C_GET_PTNR_PRIM_ADDR is
      select add_1,
             add_2,
             add_3,
             city,
             state,
             country_id,
             post,
             jurisdiction_code
        from addr
       where primary_addr_ind = 'Y'
         and addr_type = '01'
         and module = I_module
         and key_value_1 = I_key_value_1
         and key_value_2 = I_key_value_2
         and key_value_1 != 'E';

    cursor C_GET_COMP_ADDR is
       select co_add1,
              co_add2,
              co_add3,
              co_city,
              co_state,
              co_country,
              co_post,
              co_jurisdiction_code
         from comphead
        where company = I_key_value_1;
 
    cursor C_GET_OLOC_ADDR is
       select outloc_add1,
              outloc_add2,
              outloc_city,
              outloc_state,
              outloc_country_id,
              outloc_post,
              outloc_jurisdiction_code
         from outloc
        where outloc_id = I_key_value_1
          and outloc_type = I_key_value_2;
 
BEGIN

   if I_module is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_module',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_key_value_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_1',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_module = 'PTNR' and (I_key_value_1 is NULL or I_key_value_2 is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_key_value_2',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_module in ('WFST', 'WH', 'ST') or                 --store, wh, wholesale/franchise store
      (I_module = 'PTNR' and I_key_value_1 = 'E') then    --external finisher
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_LOC_PRIM_ADDR',
                       'mv_loc_prim_addr',
                       NULL);
      open C_GET_LOC_PRIM_ADDR;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_LOC_PRIM_ADDR',
                       'mv_loc_prim_addr',
                       NULL);
      fetch C_GET_LOC_PRIM_ADDR into O_add_1,
                                     O_add_2,
                                     O_add_3,
                                     O_city,
                                     O_state,
                                     O_country_id,
                                     O_post,
                                     O_jurisdiction;

      if C_GET_LOC_PRIM_ADDR%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_ADDRESS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_LOC_PRIM_ADDR',
                       'mv_loc_prim_addr',
                       NULL);
      close C_GET_LOC_PRIM_ADDR;
   ---
   elsif I_module = 'SUPP' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SUPP_PRIM_ADDR',
                       'addr',
                       NULL);
      open C_GET_SUPP_PRIM_ADDR;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SUPP_PRIM_ADDR',
                       'addr',
                       NULL);
      fetch C_GET_SUPP_PRIM_ADDR into O_add_1,
                                      O_add_2,
                                      O_add_3,
                                      O_city,
                                      O_state,
                                      O_country_id,
                                      O_post,
                                      O_jurisdiction;

      if C_GET_SUPP_PRIM_ADDR%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_ADDRESS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SUPP_PRIM_ADDR',
                       'addr',
                       NULL);
      close C_GET_SUPP_PRIM_ADDR;

   elsif I_module = 'PTNR' and I_key_value_1 != 'E' then   --partners that are NOT external finishers

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_PTNR_PRIM_ADDR',
                       'addr',
                       NULL);
      open C_GET_PTNR_PRIM_ADDR;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_PTNR_PRIM_ADDR',
                       'addr',
                       NULL);
      fetch C_GET_PTNR_PRIM_ADDR into O_add_1,
                                      O_add_2,
                                      O_add_3,
                                      O_city,
                                      O_state,
                                      O_country_id,
                                      O_post,
                                      O_jurisdiction;

      if C_GET_PTNR_PRIM_ADDR%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PRIMARY_ADDRESS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PTNR_PRIM_ADDR',
                       'addr',
                       NULL);
      close C_GET_PTNR_PRIM_ADDR;

   elsif I_module = 'COMP' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_COMP_ADDR',
                       'comphead',
                       NULL);
      open C_GET_COMP_ADDR;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_COMP_ADDR',
                       'comphead',
                       NULL);
      fetch C_GET_COMP_ADDR into O_add_1,
                                 O_add_2,
                                 O_add_3,
                                 O_city,
                                 O_state,
                                 O_country_id,
                                 O_post,
                                 O_jurisdiction;

      if C_GET_COMP_ADDR%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_COMPANY_ADDRESS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_COMP_ADDR',
                       'comphead',
                       NULL);
      close C_GET_COMP_ADDR;
   ---
   elsif I_module = 'OLOC' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_OLOC_ADDR',
                       'outloc',
                       NULL);
      open C_GET_OLOC_ADDR;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_OLOC_ADDR',
                       'outloc',
                       NULL);
      fetch C_GET_OLOC_ADDR into O_add_1,
                                 O_add_2,
                                 O_city,
                                 O_state,
                                 O_country_id,
                                 O_post,
                                 O_jurisdiction;
      
      if C_GET_OLOC_ADDR%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_OUTSIDE_LOCATION',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_OLOC_ADDR',
                       'outloc',
                       NULL);
      close C_GET_OLOC_ADDR;
   ---
   else
      O_error_message := SQL_LIB.CREATE_MSG('MODULE_NOT_EXISTS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_LOC_PRIM_ADDR%ISOPEN then
         close C_GET_LOC_PRIM_ADDR;
      end if;   
      if C_GET_SUPP_PRIM_ADDR%ISOPEN then
         close C_GET_SUPP_PRIM_ADDR;
      end if;
      if C_GET_PTNR_PRIM_ADDR%ISOPEN then
         close C_GET_PTNR_PRIM_ADDR;
      end if;
      if C_GET_COMP_ADDR%ISOPEN then
         close C_GET_COMP_ADDR;
      end if;
      if C_GET_OLOC_ADDR%ISOPEN then
         close C_GET_OLOC_ADDR;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PRIM_ADDR;
---------------------------------------------------------------------------------------
FUNCTION REFRESH_MV_LOC_PRIM_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ADDRESS_SQL.REFRESH_MV_LOC_PRIM_ADDR';
   
BEGIN

   DBMS_MVIEW.REFRESH('MV_LOC_PRIM_ADDR','C');
   
   return TRUE;
   
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
      
END REFRESH_MV_LOC_PRIM_ADDR;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_STATE_COUNTRY (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_country_id          IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                                 IO_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                                 IO_state               IN OUT   STATE.STATE%TYPE,
                                 IO_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                                 I_jurisdiction         IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                                 IO_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                 IO_count               IN OUT   NUMBER)
RETURN BOOLEAN IS
   L_program             VARCHAR2(64)         := 'ADDRESS_SQL.VALIDATE_STATE_COUNTRY';
   L_jurisdiction_code   COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE;
   L_jurisdiction_desc   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   L_state               STATE.STATE%TYPE;
   L_country_id          COUNTRY.COUNTRY_ID%TYPE;
   L_state_desc          STATE.DESCRIPTION%TYPE;
   L_country_desc        COUNTRY.COUNTRY_DESC%TYPE;
   
   cursor C_COUNTRY is
      select country_id
        from country
       where country_id = IO_country_id;
       
   cursor C_STATE_EXIST is
      select distinct state
        from state
       where state = IO_state;
    
   cursor C_STATE is
      select description,
             country_id
        from state
       where state = NVL(IO_state,state)
         and country_id = NVL(IO_country_id,country_id);

   cursor C_JURISDICTION is
      select jurisdiction_code,
             jurisdiction_desc,
             state,
             country_id
        from country_tax_jurisdiction
       where jurisdiction_code = NVL(I_jurisdiction,jurisdiction_code)
         and state = NVL(IO_state,state)
         and country_id = NVL(IO_country_id,country_id);

BEGIN
   IO_count := 0; 
   if IO_country_id is NOT NULL then
   
      open C_COUNTRY;
      fetch C_COUNTRY into L_country_id;
      IO_count := C_COUNTRY%ROWCOUNT;
      close C_COUNTRY;
      
      if IO_count = 0 then
         IO_state :=  NULL;
         IO_state_desc := NULL;
         IO_country_desc := NULL;
         O_error_message := SQL_LIB.CREATE_MSG('INV_COUNTRY',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      else
         if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                          IO_country_id,
                                          IO_country_desc) = FALSE then
            IO_country_desc :=  NULL;                                             
            return FALSE;
         end if;
      end if;     
   end if;
   
   if IO_state is NOT NULL  then
      open C_STATE_EXIST;
      fetch C_STATE_EXIST into L_state;
      close C_STATE_EXIST;
      
      if L_state IS NULL then
         IO_state_desc := NULL;
         O_error_message := SQL_LIB.CREATE_MSG('INV_STATE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
           
      else
         open C_STATE;
         LOOP
            fetch C_STATE into L_state_desc,
                               L_country_id;
            IO_count := C_STATE%ROWCOUNT;
         Exit when C_STATE%ROWCOUNT > 1 or C_STATE%NOTFOUND;
         END LOOP;
         close C_STATE;
      end if;
   end if;
   
   if I_jurisdiction is NOT NULL  then
      open C_JURISDICTION;
      LOOP
         fetch C_JURISDICTION into L_jurisdiction_code,
                                   L_jurisdiction_desc,
                                   L_state,
                                   L_country_id;
         IO_count := C_JURISDICTION%ROWCOUNT;
         exit when C_JURISDICTION%ROWCOUNT > 1 or C_JURISDICTION%NOTFOUND;
      END LOOP;
      close C_JURISDICTION;
   end if;
  
  if IO_count = 1 and L_state is NOT NULL then
   
      IO_jurisdiction_desc  := L_jurisdiction_desc;
      IO_state      := L_state;
      IO_country_id := L_country_id;
      if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                       IO_country_id,
                                       IO_country_desc) = FALSE then
         return FALSE;
      end if;
      if GEOGRAPHY_SQL.STATE_DESC(O_error_message,
                                  IO_state_desc,
                                  IO_state,
                                  IO_country_id) = FALSE then
         return FALSE;
      end if;
   end if;
   
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_COUNTRY%ISOPEN then 
         close C_COUNTRY;
      end if;
      if C_STATE_EXIST%ISOPEN then 
         close C_STATE_EXIST;
      end if;
      if C_STATE%ISOPEN then 
         close C_STATE;
      end if;
      if C_JURISDICTION%ISOPEN then 
         close C_JURISDICTION;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;   
END VALIDATE_STATE_COUNTRY;

--------------------------------------------------------------------------------------------
FUNCTION VALIDATE_JURISDICTION (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_country_id          IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                                IO_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                                IO_state               IN OUT   STATE.STATE%TYPE,
                                IO_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                                I_jurisdiction         IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                                IO_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                IO_count               IN OUT   NUMBER)
RETURN BOOLEAN IS
   L_program             VARCHAR2(64)         := 'ADDRESS_SQL.VALIDATE_JURISDICTION';
   L_jurisdiction_code   COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE;
   L_jurisdiction_desc   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   L_state               STATE.STATE%TYPE;
   L_country_id          COUNTRY.COUNTRY_ID%TYPE;
   L_state_desc          STATE.DESCRIPTION%TYPE;
   L_country_desc        COUNTRY.COUNTRY_DESC%TYPE;
   
   cursor C_JURISDICTION_EXIST is
         select distinct jurisdiction_code
           from country_tax_jurisdiction
       where jurisdiction_code = I_jurisdiction;
       
   cursor C_JURISDICTION is
      select jurisdiction_code,
             jurisdiction_desc,
             state,
             country_id
        from country_tax_jurisdiction
       where jurisdiction_code = NVL(I_jurisdiction,jurisdiction_code)
         and state = NVL(IO_state,state)
         and country_id = NVL(IO_country_id,country_id);

BEGIN
   if I_jurisdiction is NOT NULL   then
      open C_JURISDICTION_EXIST;
      fetch C_JURISDICTION_EXIST into L_jurisdiction_code;
      close C_JURISDICTION_EXIST;
      if L_jurisdiction_code IS NULL then 
         IO_jurisdiction_desc :=  NULL;
         IO_state :=  NULL;
         IO_state_desc := NULL;
         IO_country_desc := NULL;
         IO_country_id :=  NULL;
         O_error_message := SQL_LIB.CREATE_MSG('INV_TAX_JURIS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if; 
      ---
      open C_JURISDICTION;
      LOOP
         fetch C_JURISDICTION into L_jurisdiction_code,
                                   L_jurisdiction_desc,
                                   L_state,
                                   L_country_id;
         IO_count := C_JURISDICTION%ROWCOUNT;
      exit when C_JURISDICTION%ROWCOUNT > 1 or C_JURISDICTION%NOTFOUND;
      END LOOP;
      close C_JURISDICTION;
      if IO_count = 1 then
         IO_jurisdiction_desc  := L_jurisdiction_desc;
         IO_state      := L_state;
         IO_country_id := L_country_id;
         if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                          IO_country_id,
                                          IO_country_desc) = FALSE then
            return FALSE;
         end if;
         if GEOGRAPHY_SQL.STATE_DESC(O_error_message,
                                     IO_state_desc,
                                     IO_state,
                                     IO_country_id) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_JURISDICTION_EXIST%ISOPEN then 
         close C_JURISDICTION_EXIST;
      end if;
      if C_JURISDICTION%ISOPEN then 
         close C_JURISDICTION;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;   
END VALIDATE_JURISDICTION;

--------------------------------------------------------------------------------------------
END;
/