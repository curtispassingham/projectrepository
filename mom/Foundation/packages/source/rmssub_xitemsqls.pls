WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XITEM_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message_type    IN       VARCHAR2,
                 I_message         IN       "RIB_XItemDesc_REC",
                 I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message_type    IN       VARCHAR2,
                 I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                 I_message         IN       "RIB_XItemRef_REC")
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XITEM_SQL;
/
