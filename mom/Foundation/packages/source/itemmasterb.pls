CREATE OR REPLACE PACKAGE BODY ITEM_MASTER_SQL AS
-------------------------------------------------------------------------------------------------------
-- Function:    ITEM_IMAGE
-- Purpose:     Checks to determine if any item image records exist for the item
-------------------------------------------------------------------
FUNCTION ITEM_IMAGE (O_error_message  IN OUT    VARCHAR2,
                   O_exist          IN OUT    BOOLEAN,
                   I_ITEM           IN        ITEM_MASTER.ITEM%TYPE)
         RETURN BOOLEAN IS

   L_program        VARCHAR2(60) := 'ITEM_MASTER_SQL.ITEM_IMAGE';
   L_record_exists  VARCHAR2(1) := 'N';
   cursor C_ITEM_IMAGE is
      select 'Y'
        from item_image
       where item = I_item
         and ROWNUM = 1;
BEGIN

   open C_ITEM_IMAGE;
   fetch C_ITEM_IMAGE into L_record_exists;
   close C_ITEM_IMAGE;
   ---
   if L_record_exists = 'Y' then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
 when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
END ITEM_IMAGE;
-------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_COST_ZONE(O_error_message IN OUT VARCHAR2,
                                 I_new_cost_zone IN     ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE,
                                 I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_table       VARCHAR2(30) := 'ITEM_MASTER';
   L_program     VARCHAR2(60) := 'ITEM_MASTER_SQL.DEFAULT_CHILD_COST_ZONE';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_CHILDREN is
      select 'x'
        from item_master
       where item_parent = I_item
          or item_grandparent = I_item
             for update nowait;

BEGIN

   if I_new_cost_zone is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_cost_zone',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_LOCK_CHILDREN;
   close C_LOCK_CHILDREN;

   update item_master
      set cost_zone_group_id = I_new_cost_zone,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where item_parent = I_item
       or item_grandparent = I_item;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
END DEFAULT_CHILD_COST_ZONE;
-------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_REF_ITEM_IND(O_error_message    IN OUT VARCHAR2,
                                     I_item_parent      IN     ITEM_MASTER.ITEM%TYPE,
                                     I_item             IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(100) := 'ITEM_MASTER_SQL.UPDATE_PRIMARY_REF_ITEM_IND';
   L_table            VARCHAR2(30)  := 'ITEM_MASTER';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_RECS is
      select 'x'
        from item_master im
       where im.item                 != I_item
         and im.item_parent           = I_item_parent
         and im.primary_ref_item_ind  = 'Y'
         and im.item_level > im.tran_level
       for update nowait;

BEGIN

   if I_item_parent is NULL
      or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_RECS;
   close C_LOCK_RECS;
   ---
   update item_master im
      set im.primary_ref_item_ind  = 'N',
          im.last_update_id = get_user,
          im.last_update_datetime = sysdate
    where im.item                 != I_item
      and im.item_parent           = I_item_parent
      and im.primary_ref_item_ind  = 'Y'
      and im.item_level > im.tran_level;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
END UPDATE_PRIMARY_REF_ITEM_IND;
--------------------------------------------------------------------
-- Function:   DEFAULT_CHILD_GROC_ATTRIB
-- Purpose: Defaults grocery attributes to all of an item's children.
-----------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_GROC_ATTRIB(O_error_message            IN OUT VARCHAR2,
                                   I_item                     IN ITEM_MASTER.ITEM%TYPE,
                                   I_package_size             IN ITEM_MASTER.PACKAGE_SIZE%TYPE,
                                   I_package_uom              IN ITEM_MASTER.PACKAGE_UOM%TYPE,
                                   I_retail_label_type        IN ITEM_MASTER.RETAIL_LABEL_TYPE%TYPE,
                                   I_retail_label_value       IN ITEM_MASTER.RETAIL_LABEL_VALUE%TYPE,
                                   I_handling_sensitivity     IN ITEM_MASTER.HANDLING_SENSITIVITY%TYPE,
                                   I_handling_temp            IN ITEM_MASTER.HANDLING_TEMP%TYPE,
                                   I_waste_type               IN ITEM_MASTER.WASTE_TYPE%TYPE,
                                   I_default_waste_pct        IN ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                                   I_waste_pct                IN ITEM_MASTER.WASTE_PCT%TYPE,
                                   I_container_item           IN ITEM_MASTER.CONTAINER_ITEM%TYPE,
                                   I_deposit_in_price_per_uom IN ITEM_MASTER.DEPOSIT_IN_PRICE_PER_UOM%TYPE,
                                   I_sale_type                IN ITEM_MASTER.SALE_TYPE%TYPE,
                                   I_order_type               IN ITEM_MASTER.ORDER_TYPE%TYPE,

                                   
								   I_perishable_ind           IN ITEM_MASTER.PERISHABLE_IND%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'ITEM_MASTER_SQL.DEFAULT_CHILD_GROC_ATTRIB';
   L_table        VARCHAR2(65):= 'ITEM_MASTER';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where (item_parent = I_item
          or item_grandparent = I_item)
         and item_level <= tran_level
         for update nowait;

   cursor C_UPDATED_CONTAINER_ITEM is
      select item
        from item_master
       where (item_parent = I_item
          or item_grandparent = I_item)
         and item_level <= tran_level
         and NVL(container_item,'x') != NVL(I_container_item,'x');

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_MASTER',
                    L_table,
                    'Item: '||I_item);
   open C_LOCK_ITEM_MASTER;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_MASTER',
                    L_table,
                    'Item: '||I_item);
   close C_LOCK_ITEM_MASTER;
   ---
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    L_table,
                    'Item: '||I_item);

   update item_master
      set package_size = I_package_size,
          package_uom = I_package_uom ,
          retail_label_type = I_retail_label_type,
          retail_label_value = I_retail_label_value,
          handling_sensitivity = I_handling_sensitivity,
          handling_temp = I_handling_temp,
          waste_type = I_waste_type,
          default_waste_pct = I_default_waste_pct,
          waste_pct = I_waste_pct,
          container_item = I_container_item,
          deposit_in_price_per_uom = I_deposit_in_price_per_uom,
          order_type = I_order_type,
          sale_type = I_sale_type,
          perishable_ind = I_perishable_ind,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where (item_parent = I_item
       or item_grandparent  = I_item)
      and item_level <= tran_level;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CHILD_GROC_ATTRIB;
-----------------------------------------------------------------------
-- Function:   DEFAULT_CHILD_RCOM_ATTRIB
-- Purpose: Defaults RCOM attributes to all of an item's children.
-----------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_RCOM_ATTRIB(O_error_message          IN OUT VARCHAR2,
                                   I_item                   IN     ITEM_MASTER.ITEM%TYPE,
                                   I_item_service_level     IN     ITEM_MASTER.ITEM_SERVICE_LEVEL%TYPE,
                                   I_product_classification IN     ITEM_MASTER.PRODUCT_CLASSIFICATION%TYPE,
                                   I_brand_name             IN     ITEM_MASTER.BRAND_NAME%TYPE,
                                   I_gift_wrap_ind          IN     ITEM_MASTER.GIFT_WRAP_IND%TYPE,
                                   I_ship_alone_ind         IN     ITEM_MASTER.SHIP_ALONE_IND%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'ITEM_MASTER_SQL.DEFAULT_CHILD_RCOM_ATTRIB';
   L_table        VARCHAR2(65) := 'ITEM_MASTER';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where item_parent = I_item
          or item_grandparent = I_item
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_MASTER',
                    L_table,
                    'Item: '||I_item);
   open C_LOCK_ITEM_MASTER;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_MASTER',
                    L_table,
                    'Item: '||I_item);
   close C_LOCK_ITEM_MASTER;
   ---
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   update item_master
      set item_service_level = I_item_service_level,
          product_classification = I_product_classification,
          brand_name = I_brand_name,
          gift_wrap_ind = I_gift_wrap_ind,
          ship_alone_ind = I_ship_alone_ind,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where item_parent = I_item
       or item_grandparent  = I_item;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CHILD_RCOM_ATTRIB;
-----------------------------------------------------------------------
-- Function:   UPDATE_CHECK_UDA_IND
-- Purpose: Sets the item_master.check_uda_ind = Y.
--              Called when users click OK on the itemuda form for the
--              first time.
-----------------------------------------------------------------------
FUNCTION UPDATE_CHECK_UDA_IND(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item               IN     ITEM_MASTER.ITEM%TYPE,
                              I_check_uda_ind      IN     ITEM_MASTER.CHECK_UDA_IND%TYPE DEFAULT 'Y')
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'ITEM_MASTER_SQL.UPDATE_CHECK_UDA_IND';
   L_table        VARCHAR2(65) := 'ITEM_MASTER';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where item = I_item
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_MASTER',
                    L_table,
                    'Item: '||I_item);
   open C_LOCK_ITEM_MASTER;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_MASTER',
                    L_table,
                    'Item: '||I_item);
   close C_LOCK_ITEM_MASTER;
   ---
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    L_table,
                    'Item: '||I_item);
   update item_master
      set check_uda_ind = I_check_uda_ind,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where item = I_item;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_CHECK_UDA_IND;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_MASTER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec      IN ITEM_MASTER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_MASTER_SQL.UPDATE_ITEM_MASTER';

BEGIN
   if not LOCK_ITEM_MASTER(O_error_message,
                           I_item_rec.item) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_MASTER','item: '||I_item_rec.item);

   update item_master
      set  item_desc = I_item_rec.item_desc,
           short_desc = nvl(I_item_rec.short_desc,short_desc),
           desc_up = upper(I_item_rec.item_desc),
           store_ord_mult = I_item_rec.store_ord_mult,
           forecast_ind = I_item_rec.forecast_ind,
           comments = I_item_rec.comments,
           last_update_id = I_item_rec.last_update_id,
           last_update_datetime = sysdate,
           aip_case_type = I_item_rec.aip_case_type,
           
           primary_ref_item_ind = DECODE(I_item_rec.primary_ref_item_ind, 'Y', 'Y', primary_ref_item_ind),
           container_item = I_item_rec.container_item,
		   
           mfg_rec_retail = I_item_rec.mfg_rec_retail,
           package_size = I_item_rec.package_size,
           package_uom = I_item_rec.package_uom,
           uom_conv_factor = I_item_rec.uom_conv_factor,
           handling_temp = I_item_rec.handling_temp,
           handling_sensitivity = I_item_rec.handling_sensitivity,
           item_desc_secondary = nvl(I_item_rec.item_desc_secondary,item_desc_secondary),
           item_service_level = I_item_rec.item_service_level,
           deposit_in_price_per_uom = I_item_rec.deposit_in_price_per_uom,
           original_retail = I_item_rec.original_retail,
           retail_label_type = I_item_rec.retail_label_type,
           retail_label_value = I_item_rec.retail_label_value,
           default_waste_pct = I_item_rec.default_waste_pct,           
   		   brand_name = nvl(I_item_rec.brand_name,brand_name),
           product_classification = nvl(I_item_rec.product_classification,product_classification),
           gift_wrap_ind = NVL(I_item_rec.gift_wrap_ind,gift_wrap_ind)
    where item = I_item_rec.item;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ITEM_MASTER;
-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_MASTER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_MASTER_SQL.LOCK_ITEM_MASTER';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   CURSOR C_LOCK_ITEM_MASTER IS
      select 'x'
        from item_master
       where item = I_item
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_MASTER', 'ITEM_MASTER','item: '||I_item);
   open C_LOCK_ITEM_MASTER;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_MASTER', 'ITEM_MASTER','item: '||I_item);
   close C_LOCK_ITEM_MASTER;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_MASTER',
                                            I_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_ITEM_MASTER;
-------------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_STORE_ORD_MULT (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                       I_store_ord_mult    IN     ITEM_MASTER.STORE_ORD_MULT%TYPE)
   RETURN BOOLEAN IS

   L_table       VARCHAR2(30) := 'ITEM_MASTER';
   L_program     VARCHAR2(60) := 'ITEM_MASTER_SQL.DEFAULT_CHILD_STORE_ORD_MULT';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_CHILDREN is
      select 'x'
        from item_master
       where item_parent = I_item
          or item_grandparent = I_item
             for update nowait;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_store_ord_mult is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_ord_mult',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_CHILDREN', 'ITEM_MASTER','item: '||I_item);
   open C_LOCK_CHILDREN;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_CHILDREN', 'ITEM_MASTER','item: '||I_item);
   close C_LOCK_CHILDREN;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_MASTER','item: '||I_item);

   update item_master
      set store_ord_mult = I_store_ord_mult,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where item_parent = I_item
       or item_grandparent = I_item;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CHILD_STORE_ORD_MULT;
-------------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_FORECAST_IND(O_error_message   IN OUT VARCHAR2,
                                    I_forecast_ind    IN     ITEM_MASTER.FORECAST_IND%TYPE,
                                    I_item            IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_table         VARCHAR2(30) := 'ITEM_MASTER';
   L_program       VARCHAR2(64) := 'ITEM_MASTER_SQL.DEFAULT_CHILD_FORECAST_IND';
   L_forecast_ind  ITEM_MASTER.FORECAST_IND%TYPE := NULL;
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_CHILDREN is
      select 'x'
        from item_master
       where item_parent = I_item
          or item_grandparent = I_item
         for update nowait;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_CHILDREN;
   close C_LOCK_CHILDREN;

   update item_master
      set forecast_ind = I_forecast_ind,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where item_parent  = I_item
       or item_grandparent = I_item;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_CHILD_FORECAST_IND;
-------------------------------------------------------------------------
END ITEM_MASTER_SQL;
/


