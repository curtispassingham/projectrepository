
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRRCLS_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE,
                 I_message_type          IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRRCLS_SQL;
/
