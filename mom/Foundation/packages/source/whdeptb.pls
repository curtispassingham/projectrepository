
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY WH_DEPT_SQL AS
-----------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_NO(O_error_message   IN OUT   VARCHAR2,
                         O_seq_no          IN OUT   WH_DEPT.SEQ_NO%TYPE,
                         I_wh              IN       WH_DEPT.WH%TYPE)
RETURN BOOLEAN IS

   cursor C_SEQ_NO is
      select NVL(max(seq_no + 1),1)
        from wh_dept
       where wh = I_wh;

BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            'WH_DEPT_SQL.GET_NEXT_SEQ_NO',
                                            NULL);
         return FALSE;
   end if;

   open C_SEQ_NO;
   fetch C_SEQ_NO into O_seq_no;
   close C_SEQ_NO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'WH_DEPT_SQL.GET_NEXT_SEQ_NO',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_NEXT_SEQ_NO; 
-----------------------------------------------------------------------------
FUNCTION WH_DEPT_EXISTS(O_error_message   IN OUT   VARCHAR2,
                        O_exists          IN OUT   BOOLEAN,
                        I_wh              IN       WH_DEPT.WH%TYPE,
                        I_dept            IN       WH_DEPT.DEPT%TYPE)
RETURN BOOLEAN IS

   L_exists_ind   VARCHAR2(1) := 'N';

   cursor C_WH_DEPT_EXISTS is
      select 'Y'
        from wh_dept
       where wh = I_wh
         and dept = I_dept;
            
BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            'WH_DEPT_SQL.WH_DEPT_EXISTS',
                                            NULL);
         return FALSE;
   end if;

   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            'WH_DEPT_SQL.WH_DEPT_EXISTS',
                                             NULL);
      return FALSE;
   end if;

   open C_WH_DEPT_EXISTS;
   fetch C_WH_DEPT_EXISTS into L_exists_ind; 
   close C_WH_DEPT_EXISTS;

   if L_exists_ind = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'WH_DEPT_SQL.WH_DEPT_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END WH_DEPT_EXISTS;
-----------------------------------------------------------------------------
FUNCTION WH_EXISTS(O_error_message         IN OUT   VARCHAR2,
                   O_exists                IN OUT   BOOLEAN,
                   I_wh                    IN       WH_DEPT.WH%TYPE,
                   I_check_null_dept_ind   IN       VARCHAR2) 

RETURN BOOLEAN IS

   L_query   VARCHAR2(500);
   L_count   INTEGER;

BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            'WH_DEPT_SQL.WH_EXISTS',
                                             NULL);
         return FALSE;
   end if;

   if I_check_null_dept_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_check_null_dept_ind',
                                            'WH_DEPT_SQL.WH_EXISTS',
                                             NULL);
      return FALSE;
   end if;

   L_query :=  'select count(*) '
             ||'from wh_dept '
             ||'where wh = :l_wh'; 

   -- if the check null dept ind is sent as 'Y', check
   -- if a wh-only level record (i.e. one with dept column NULL)
   -- exists.
   if I_check_null_dept_ind = 'Y' then
      L_query := L_query||' and dept is NULL';
   end if;

   EXECUTE IMMEDIATE L_query into L_count using I_wh;
   if L_count > 0 then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'WH_DEPT_SQL.WH_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END WH_EXISTS;
-----------------------------------------------------------------------------
END WH_DEPT_SQL;
/
