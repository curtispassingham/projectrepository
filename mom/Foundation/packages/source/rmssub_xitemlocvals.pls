
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XITEMLOC_VALIDATE AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_message       IN     "RIB_XItemLocDesc_REC",
                       I_message_type  IN     VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

END RMSSUB_XITEMLOC_VALIDATE;
/
