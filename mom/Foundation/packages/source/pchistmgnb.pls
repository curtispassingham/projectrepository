CREATE OR REPLACE PACKAGE BODY PCHISTMGN_SQL AS
---
LP_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
---
FUNCTION validate_inputs (O_error_message          IN OUT VARCHAR2,
                          I_from_date1             IN     PRICE_HIST.ACTION_DATE%TYPE,
                          I_grp_type               IN     VARCHAR2,
                          I_loc_type               IN     PRICE_HIST.LOC_TYPE%TYPE,
                          I_loc                    IN     PRICE_HIST.LOC%TYPE,
                          I_item                   IN     PRICE_HIST.ITEM%TYPE,
                          I_item_type              IN     VARCHAR2) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message          IN OUT VARCHAR2,
                  I_from_date              IN     PRICE_HIST.ACTION_DATE%TYPE,
                  I_to_date                IN     PRICE_HIST.ACTION_DATE%TYPE,
                  I_grp_type               IN     VARCHAR2,
                  I_loc_type               IN     PRICE_HIST.LOC_TYPE%TYPE,
                  I_loc                    IN     PRICE_HIST.LOC%TYPE,
                  I_tran_type              IN     CODE_DETAIL.CODE%TYPE,
                  I_item_type              IN     VARCHAR2,
                  I_item                   IN     PRICE_HIST.ITEM%TYPE,
                  I_item_desc              IN     ITEM_MASTER.ITEM_DESC%TYPE,
                  I_prim_lang              IN     LANG.LANG%TYPE,
                  I_user_lang              IN     LANG.LANG%TYPE,
                  I_prim_currency          IN     CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(40)   := 'PCHISTMGN_SQL.GET_INFO';
   L_sql_stmt1                VARCHAR2(4000);
   L_sql_stmt10               VARCHAR2(4000);
   L_sql_stmt11               VARCHAR2(4000);
   L_sql_stmt2                VARCHAR2(4000);
   L_sql_stmt3                VARCHAR2(4000);
   L_ins_stmt                 VARCHAR2(4000);
   I_prim_lang1               LANG.LANG%TYPE;
   I_user_lang1               LANG.LANG%TYPE;
   I_prim_currency1           CURRENCIES.CURRENCY_CODE%TYPE;
   I_from_date1               PRICE_HIST.ACTION_DATE%TYPE;

BEGIN

   --- Get the number of historical events from the system options table.
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_rec)=FALSE then
      return FALSE;
   end if;
   ---

   -- Refresh table
   delete from gtt_price_hist_mgn;
   delete from gtt_price_hist_mgn_backup;
   
   i_from_date1:=nvl(i_from_date,get_vdate-LP_system_options_rec.price_hist_retention_days);
   I_prim_lang1      := nvl(I_prim_lang,LANGUAGE_SQL.GET_PRIMARY_LANGUAGE);
   I_user_lang1      := nvl(I_user_lang, LANGUAGE_SQL.GET_USER_LANGUAGE);
   I_prim_currency1  := nvl(I_prim_currency,LP_system_options_rec.currency_code);
   if I_user_lang IS NULL AND I_prim_lang IS NULL AND I_prim_currency IS NULL then
     -- called from adf
    
      if i_from_date1    < (get_vdate - LP_system_options_rec.price_hist_retention_days) then
         O_error_message:=sql_lib.create_msg('HIST_NOT_AVAILABLE', LP_system_options_rec.price_hist_retention_days);
         RETURN false;
      end if;
      if NOT validate_inputs(O_error_message , I_from_date1 , I_grp_type , I_loc_type , I_loc , I_item , I_item_type ) then
         RETURN false;
      end if;
    end if;

   insert into gtt_price_hist_mgn_backup(item,ref_item,vpn,item_desc,tl_item_desc,dept,class,subclass,pack_ind)
              select  distinct im.item item,
                      decode(sign(im.tran_level - im.item_level),
                             -1,
                             NULL,
                             im.item_parent) ref_item,
                      its.vpn vpn,
                      im.item_desc item_desc,
                      vim.item_desc tl_item_desc,
                      im.dept dept,
                      im.class class,
                      im.subclass subclass,
                      im.pack_ind pack_ind
                from  v_item_master im,
                      item_supplier its,
                      v_item_master_tl vim
                where im.item = its.item
                  and im.status = 'A'
                  and its.primary_supp_ind ='Y'
                  and im.item = NVL(I_item, im.item)
                  and im.item = vim.item;
   
   --constructing an insert statement which will be concatenated with the separately
   --prepared select statement, to insert the data into the table gtt_price_hist_mgn
   L_ins_stmt := 'insert into gtt_price_hist_mgn(action_date, '||
                                                'item, '||
                                                'item_desc, '||
                                                'tran_type, '||
                                                'tran_type_desc, '||
                                                'loc_type, '||
                                                'loc, '||
                                                'pricing_cost, '||
                                                'pricing_cost_prim, '||
                                                'unit_retail, '||
                                                'unit_retail_prim, '||
                                                'selling_unit_retail, '||
                                                'selling_unit_retail_prim, '||
                                                'selling_uom, '||
                                                'multi_units, '||
                                                'multi_unit_retail, '||
                                                'multi_unit_retail_prim, '||
                                                'multi_selling_uom, '||
                                                'local_curr, '||
                                                'primary_curr, '||
                                                'reason, '||
                                                'reason_desc, '||
                                                'ref_item, '||
                                                'vpn, '||
                                                'unit_retail_euro, '||
                                                'selling_unit_retail_euro, '||
                                                'multi_unit_retail_euro, '||
                                                'pricing_cost_euro, '||
                                                'margin_pct, ' || 
                                                'wh_margin_pct, ' ||
                                                'store_type) ';
   --constructing the select statement common for all group types

   if I_grp_type = 'S' and I_loc is not NULL and I_item is NULL then
      L_sql_stmt1 := 'select a.action_date, '||
                            'a.item, '||
                            'tbl.item_desc, '||
                            'a.tran_type,'||
                            '(select code_desc '||
                               'from v_code_detail_tl '||
                               'where code_type = '||'''PCST'''||' '||
                               'and code = to_char(a.tran_type)), '||
                            'a.loc_type, '||
                            'a.loc, '||
                            'NULL, '||
                            'NULL, '||
                            'a.unit_retail, '||
                            'a.unit_retail * a.exchange_rate1, '||
                            'a.selling_unit_retail, '||
                            'a.selling_unit_retail * a.exchange_rate1, '||
                            'a.selling_uom, '||
                            'a.multi_units, '||
                            'a.multi_unit_retail, '||
                            'a.multi_unit_retail * a.exchange_rate1, '||
                            'a.multi_selling_uom, '||
                            'a.currency_code, '||
                            'a.bind_prim_currency, '||
                            'a.reason, '||
                            'case when a.tran_type in (4,8,9,10,11) then  '||
                               '(select MERCH_RETAIL_API_SQL.GET_REASON_CODE_DESC(a.reason)  '||
                                  'from DUAL  '||
                                 'where rownum <= 1)  '||
                            'else '|| 
                               '(select reason_desc  '||
                                  'from v_cost_chg_reason_tl  '||
                                 'where reason = a.reason)  '|| 
                            'end,  '||
                            'tbl.ref_item, '||
                            'tbl.vpn, '||
                            'a.unit_retail * a.exchange_rate2, '||
                            'a.selling_unit_retail * a.exchange_rate2, '||
                            'a.multi_unit_retail * a.exchange_rate2, '||
                            'NULL, '||
                            'NULL, '||
                            'NULL, '||
                            'NULL ';

     L_sql_stmt11 :=  'from (select ph.item, '||
                                    'ph.post_date, '||
                                    'ph.action_date, '||
                                    'ph.tran_type ,'||
                                    'ph.loc_type ,'||
                                    'ph.loc, '||
                                    'ph.unit_retail, '||
                                    'ph.selling_unit_retail, '||
                                    'ph.selling_uom, '||
                                    'ph.multi_units, '||
                                    'ph.multi_unit_retail, '||
                                    'ph.multi_selling_uom, '||
                                    'ph.reason, '||
                                    'ph.unit_cost, '||
                                    'mv1.effective_date, '||
                                    'mv1.exchange_rate exchange_rate1, '||
                                    'mv1.from_currency from_currency1, '||
                                    'mv1.to_currency to_currency1, '||
                                    'mv2.from_currency from_currency2, '||
                                    'mv2.to_currency to_currency2, '||
                                    'mv2.exchange_rate exchange_rate2, '||
                                    'locations.currency_code, '||
                                    'bind.from_date bind_from_date, '||
                                    'bind.to_date bind_to_date, '||
                                    'bind.loc bind_loc, '||
                                    'bind.tran_type bind_tran_type, '||
                                    'bind.item bind_item, '||
                                    'upper(bind.item_desc) bind_item_desc, '||
                                    'bind.prim_lang bind_prim_lang, '||
                                    'bind.user_lang bind_user_lang, '||
                                    'bind.prim_currency bind_prim_currency, '||
                                    '(rank() over(partition by ph.item, '||
                                                              'ph.loc, '||
                                                              'ph.action_date '||
                                             'order by mv1.effective_date desc)) ranking1, '||
                                    '(rank() over(partition by ph.item, '||
                                                               'ph.loc, '||
                                                               'ph.action_date '||
                                             'order by mv2.effective_date desc)) ranking2, '||
                                                       ' 1 ranking3, '||
                                                       ' 1 ranking4, '||
                                                       ' 1 fc_ranking '||
                               'from price_hist ph, '||
                                    'mv_currency_conversion_rates mv1, '||
                                    'mv_currency_conversion_rates mv2, '||
                                    '(select store location, '||
                                            'currency_code '||
                                       'from v_store '||
                                     'union all '||
                                     'select wh location, '||
                                            'currency_code '||
                                       'from v_wh) locations, '||
                                    '(select :I_from_date1 from_date, '||
                                            ':I_to_date to_date, '||
                                            ':I_loc loc, '||
                                            ':I_tran_type tran_type, '||
                                            ':I_item item, '||
                                            ':I_item_desc item_desc, '||
                                            ':I_prim_lang1 prim_lang, '||
                                            ':I_user_lang1 user_lang, '||
                                            ':I_prim_currency1 prim_currency '||
                                       'from dual) bind ';
   else
      L_sql_stmt1 := 'select p.action_date, '||
                            'p.item, '||
                            'tbl.item_desc, '||
                            'p.tran_type,'||
                            '(select code_desc '||
                               'from v_code_detail_tl '||
                              'where code_type = '||'''PCST'''||' '||
                                'and code = to_char(p.tran_type)), '||
                            'p.loc_type, '||
                            'p.loc, '||
                            'p.pricing_cost * p.exchange_rate4, '||
                            'p.pricing_cost * p.exchange_rate3, '||
                            'p.unit_retail, '||
                            'p.unit_retail * p.exchange_rate1, '||
                            'p.selling_unit_retail, '||
                            'p.selling_unit_retail * p.exchange_rate1, '||
                            'p.selling_uom, '||
                            'p.multi_units, '||
                            'p.multi_unit_retail, '||
                            'p.multi_unit_retail * p.exchange_rate1, '||
                            'p.multi_selling_uom, '||
                            'p.loc_curr_code, '||
                            'p.bind_prim_currency, '||
                            'p.reason, '||
                            'case when p.tran_type in (4,8,9,10,11) then  '||
                               '(select MERCH_RETAIL_API_SQL.GET_REASON_CODE_DESC(p.reason)  '||
                                  'from DUAL  '||
                                 'where rownum <= 1)  '||
                            'else '|| 
                               '(select reason_desc  '||
                                  'from v_cost_chg_reason_tl  '||
                                 'where reason = p.reason)  '|| 
                            'end ,  '||
                            'tbl.ref_item, '||
                            'tbl.vpn, '||
                            'p.unit_retail * p.exchange_rate2, '||
                            'p.selling_unit_retail * p.exchange_rate2, '||
                            'p.multi_unit_retail * p.exchange_rate2, '||
                            'p.unit_cost * p.exchange_rate2, '||
                            'PCHISTMGN_SQL.CALC_MARGIN(p.item, '||
                                                      'tbl.dept, '|| 
                                                      'tbl.class, '||
                                                      'tbl.subclass, '||
                                                      'tbl.pack_ind, '||                                                      
                                                      'p.class_vat_ind, '||
                                                      'p.loc, '||
                                                      'p.loc_type, '||
                                                      'p.action_date, '||
                                                      '(p.pricing_cost / p.exchange_rate1), '||
                                                      '(p.unit_retail / p.exchange_rate3)), '||
                            'PCHISTMGN_SQL.CALC_MARGIN(p.item, '||
                                                      'tbl.dept, '|| 
                                                      'tbl.class, '||
                                                      'tbl.subclass, '||
                                                      'tbl.pack_ind, '||                                                      
                                                      'p.class_vat_ind, '||
                                                      'p.loc, '||
                                                      'p.loc_type, '||
                                                      'p.action_date, '||
                                                      '(p.acquisition_cost / p.exchange_rate1), '||
                                                      '(p.pricing_cost / p.exchange_rate3)), '||
                            'p.store_type ';
                                                        
      L_sql_stmt10 := 'FROM (select '||
                                                        'e.item, '||
                                                        'e.location, '||
                                                        'e.dept, '||
                                                        'e.post_date, '||
                                                        'e.action_date, '||
                                                        'e.tran_type , '||
                                                        'e.loc_type , '||
                                                        'e.loc, '||
                                                        'e.unit_retail, '||
                                                        'e.selling_unit_retail, '||
                                                        'e.selling_uom, '||
                                                        'e.multi_units, '||
                                                        'e.multi_unit_retail, '||
                                                        'e.multi_selling_uom, '||
                                                        'e.reason, '||
                                                        'e.unit_cost, '||
                                                        'e.active_date, '||
                                                        'e.pricing_cost, '||
                                                        'e.effective_date1, '||
                                                        'e.exchange_rate1, '||
                                                        'e.from_currency1, '||
                                                        'e.to_currency1, '||
                                                        'e.from_currency2, '||
                                                        'e.to_currency2, '||
                                                        'e.exchange_rate2, '||
                                                        'e.from_currency3, '||
                                                        'e.to_currency3, '||
                                                        'e.exchange_rate3, '||
                                                        'e.from_currency4, '||
                                                        'e.to_currency4, '||
                                                        'e.exchange_rate4, '||
                                                        'e.class_vat_ind, '||
                                                        'e.loc_curr_code, '||
                                                        'e.bind_from_date, '||
                                                        'e.bind_to_date, '||
                                                        'e.loc bind_loc, '||
                                                        'e.bind_tran_type, '||
                                                        'e.item bind_item, '||
                                                        'e.bind_item_desc, '||
                                                        'e.bind_prim_lang, '||
                                                        'e.bind_user_lang, '||
                                                        'e.bind_prim_currency, '||
                                                        'e.acquisition_cost, '||
                                                        'e.store_type '||
                                                'from '||
                                                '( '||
                                                'select d.*,mv4.effective_date effective_date4,mv4.exchange_rate exchange_rate4,mv4.from_currency from_currency4,mv4.to_currency to_currency4,(rank() over(partition BY d.item, d.supplier, d.origin_country_id, d.location, d.action_date order by mv4.effective_date DESC)) ranking4 '||
                                                'from '||
                                                '( '||
                                                'select c.*,mv3.effective_date effective_date3,mv3.exchange_rate exchange_rate3,mv3.from_currency from_currency3,mv3.to_currency to_currency3,(rank() over(partition BY c.item, c.supplier, c.origin_country_id, c.location, c.action_date order by mv3.effective_date DESC)) ranking3 '||
                                                'from '||
                                                '( '||
                                                'select b.*,mv2.effective_date effective_date2,mv2.exchange_rate exchange_rate2,mv2.from_currency from_currency2,mv2.to_currency to_currency2,(rank() over(partition BY b.item, b.supplier, b.origin_country_id, b.location, b.action_date order by mv2.effective_date DESC)) ranking2 '||
                                                'from '||
                                                '( '||
                                                'select a.*,mv1.effective_date effective_date1,mv1.exchange_rate exchange_rate1,mv1.from_currency from_currency1,mv1.to_currency to_currency1,(rank() over(partition BY a.item, a.supplier, a.origin_country_id, a.location, a.action_date order by mv1.effective_date DESC)) ranking1 ' ;
                                                                                                
      L_sql_stmt11 := 'from (select fc.item, '||
                                    'fc.location, '||
                                    'fc.dept, '||
                                    'fc.origin_country_id, '|| 
                                    'fc.supplier, '||
                                    'ph.post_date, '||
                                    'ph.action_date, '||
                                    'ph.tran_type ,'||
                                    'ph.loc_type ,'||
                                    'ph.loc, '||
                                    'ph.unit_retail, '||
                                    'ph.selling_unit_retail, '||
                                    'ph.selling_uom, '||
                                    'ph.multi_units, '||
                                    'ph.multi_unit_retail, '||
                                    'ph.multi_selling_uom, '||
                                    'ph.reason, '||
                                    'ph.unit_cost, '||
                                    'fc.active_date, '||
                                    'fc.pricing_cost, '||
                                    'cls.class_vat_ind, '||
                                    'locations.currency_code loc_curr_code, '||
                                    'bind.from_date bind_from_date, '||
                                    'bind.to_date bind_to_date, '||
                                    'bind.loc bind_loc, '||
                                    'bind.tran_type bind_tran_type, '||
                                    'bind.item bind_item, '||
                                    'upper(bind.item_desc) bind_item_desc, '||
                                    'bind.prim_lang bind_prim_lang, '||
                                    'bind.user_lang bind_user_lang, '||
                                    'bind.prim_currency bind_prim_currency, '||
                                    'fc.acquisition_cost, '||
                                    'fc.store_type, '||  
                                    'fc.currency_code fc_currency_code, '||
                                    '(rank() over(partition by fc.item, '||
                                                  'fc.supplier, '||
                                                  'fc.origin_country_id, '||
                                                  'fc.location, '||
                                                  'ph.action_date '||
                                             'order by fc.active_date desc )) fc_ranking '||
                               'from price_hist ph, '||
                                    'future_cost fc, '||
                                    'class cls, '||
                                    '(select store location, '||
                                            'currency_code '||
                                       'from v_store '||
                                     'union '||
                                     'select wh location, '||
                                            'currency_code '||
                                       'from wh) locations, '||
                                    '(select :I_from_date1 from_date, '||
                                            ':I_to_date to_date, '||
                                            ':I_loc loc, '||
                                            ':I_tran_type tran_type, '||
                                            ':I_item item, '||
                                            ':I_item_desc item_desc, '||
                                            ':I_prim_lang1 prim_lang, '||
                                            ':I_user_lang1 user_lang, '||
                                            ':I_prim_currency1 prim_currency '||
                                       'from dual) bind '||
                              'where ph.item = fc.item ';
   end if;

   if I_item is not NULL then
      if I_item_type = 'I' then
         L_sql_stmt11 := L_sql_stmt11 || ' and ph.item = bind.item ';
      elsif I_item_type = 'P' then
         L_sql_stmt11 := L_sql_stmt11 || ' and exists (select '||'''x'''||' '||
                                                        'from v_item_master im '||
                                                       'where im.item_level <= im.tran_level '||
                                                         'and im.item_parent = bind.item '||
                                                         'and im.item = ph.item '||
                                                         'and rownum = 1) ';
      end if;
   end if;

   if I_grp_type = 'S' and I_loc is not NULL then
      if I_item is NULL then
         L_sql_stmt11 := L_sql_stmt11 || ' where ph.loc = bind.loc ';
      else
         L_sql_stmt11 := L_sql_stmt11 || 'and ph.loc = bind.loc ';
      end if;
   end if;

   if I_grp_type = 'S' and I_loc is not NULL and I_item is NULL then
      L_sql_stmt11 := L_sql_stmt11 || 'and locations.location = ph.loc '||
                                      'and ph.action_date >= mv1.effective_date '||
                                      'and mv1.exchange_type = decode ('''||LP_system_options_rec.consolidation_ind||''','||'''Y'''||','||'''C'''||','||'''O'''||') '||
                                      'and mv1.from_currency = locations.currency_code '||
                                      'and mv1.to_currency = bind.prim_currency '||
                                      'and ph.action_date >= mv2.effective_date '||
                                      'and mv2.exchange_type = decode ('''||LP_system_options_rec.consolidation_ind||''','||'''Y'''||','||'''C'''||','||'''O'''||') '||
                                      'and mv2.from_currency = bind.prim_currency '||
                                      'and mv2.to_currency = '||'''EUR'''||') a, '||
                                      'gtt_price_hist_mgn_backup tbl ';
   else
       L_sql_stmt11 := L_sql_stmt11 || 'and ph.loc = fc.location '||
                                       'and cls.class = fc.class '||
                                       'and cls.dept = fc.dept '||
                                       'and locations.location = fc.location '||
                                       'and ph.action_date >= fc.active_date '||
                                       'and fc.primary_supp_country_ind = '||'''Y'''||' '||
                                       ') a, '|| 
                                       'mv_currency_conversion_rates mv1 '|| 
                                       'where fc_ranking=1 '|| 
                                       'and a.action_date >= mv1.effective_date '|| 
                                       'and mv1.exchange_type = decode ('''||LP_system_options_rec.consolidation_ind||''','||'''Y'''||','||'''C'''||','||'''O'''||') '||
                                       'and mv1.from_currency = a.loc_curr_code '|| 
                                       'and mv1.to_currency = bind_prim_currency '|| 
                                       ') b, '|| 
                                       'mv_currency_conversion_rates mv2 '|| 
                                       'where b.ranking1=1 '|| 
                                       'and b.active_date >= mv2.effective_date '|| 
                                       'and mv2.exchange_type = decode ('''||LP_system_options_rec.consolidation_ind||''','||'''Y'''||','||'''C'''||','||'''O'''||') '||
                                       'and mv2.from_currency = loc_curr_code '|| 
                                       'and mv2.to_currency = '||'''EUR'''||' '|| 
                                       ') c , '|| 
                                       'mv_currency_conversion_rates mv3 '|| 
                                       'where c.ranking2=1 '|| 
                                       'and c.active_date >= mv3.effective_date '||                                       
                                       'and mv3.exchange_type = decode ('''||LP_system_options_rec.consolidation_ind||''','||'''Y'''||','||'''C'''||','||'''O'''||') '||
                                       'and mv3.from_currency = c.fc_currency_code '|| 
                                       'and mv3.to_currency = bind_prim_currency '|| 
                                       ') d, '|| 
                                       'mv_currency_conversion_rates mv4 '|| 
                                       'where d.ranking3=1 '|| 
                                       'and d.action_date >= mv4.effective_date '||                                       
                                       'and mv4.exchange_type = decode ('''||LP_system_options_rec.consolidation_ind||''','||'''Y'''||','||'''C'''||','||'''O'''||') '||
                                       'and mv4.from_currency = d.fc_currency_code '|| 
                                       'and mv4.to_currency = loc_curr_code '|| 
                                       ') e where e.ranking4=1 '|| 
                                       ') p, '||
                                       'gtt_price_hist_mgn_backup tbl ';
   end if;

   --constructing the select statement for individual group types
   if I_grp_type = 'L' then
      L_sql_stmt2 :=     ', loc_list_detail lld '||
                     'where p.item = tbl.item '|| 
                       'and lld.loc_list = p.bind_loc '|| 
                       'and p.loc = lld.location'; 
   elsif I_grp_type in ('S', 'A') then
      if (I_grp_type = 'S' and I_loc is not NULL and I_item is NULL) then
         L_sql_stmt2 := 'where a.ranking1 = 1 '||
                          'and a.ranking2 = 1 '||
                          'and a.ranking3 = 1 '||
                          'and a.ranking4 = 1 '||
                          'and a.fc_ranking = 1 '||
                          'and a.item = tbl.item';
      else 
         L_sql_stmt2 := 'where p.item = tbl.item '; 
      end if;
   end if;

   if I_from_date1 is not NULL then
      if (I_grp_type = 'S' and I_loc is not NULL and I_item is NULL) then 
           L_sql_stmt3 := L_sql_stmt3 || ' and a.action_date >= a.bind_from_date'; 
       else 
           L_sql_stmt3 := L_sql_stmt3 || ' and p.action_date >= p.bind_from_date';   
       end if;
   end if;
   if I_to_date is not NULL then
      if (I_grp_type = 'S' and I_loc is not NULL and I_item is NULL) then 
         L_sql_stmt3 := L_sql_stmt3 || ' and a.action_date <= a.bind_to_date'; 
      else 
         L_sql_stmt3 := L_sql_stmt3 || ' and p.action_date <= p.bind_to_date'; 
       end if;
   end if;

   if I_item is NULL then
      if I_item_desc is not NULL then
         if I_item_type = 'I' and I_grp_type = 'S' and I_loc is not NULL then
            L_sql_stmt3 := L_sql_stmt3 || ' and upper(decode(a.bind_prim_lang, a.bind_user_lang, tbl.item_desc, tbl.tl_item_desc)) like '|| '''%''' || '|' || '|' ||'a.bind_item_desc' || '|' || '|' || '''%''';
         elsif I_item_type = 'I' then 
            L_sql_stmt3 := L_sql_stmt3 || ' and upper(decode(p.bind_prim_lang, p.bind_user_lang, tbl.item_desc, tbl.tl_item_desc)) like '|| '''%''' || '|' || '|' ||'p.bind_item_desc' || '|' || '|' || '''%'''; 
         elsif I_item_type = 'P' and I_grp_type = 'S' and I_loc is not NULL then 
            L_sql_stmt3 := L_sql_stmt3 || ' and exists (select '||'''x'''||' '||
                                                         'from gtt_price_hist_mgn_backup itbl, '||
                                                               'v_item_master im '||
                                                        'where upper(decode(a.bind_prim_lang, a.bind_user_lang, itbl.item_desc, itbl.tl_item_desc)) like '|| '''%''' || '|' || '|' ||'a.bind_item_desc' || '|' || '|' || '''%'''||
                                                         ' and im.item_parent = itbl.item '||
                                                         ' and im.item = a.item '||
                                                         ' and rownum = 1) ';
         elsif I_item_type = 'P' then 
             L_sql_stmt3 := L_sql_stmt3 || ' and exists (select '||'''x'''||' '|| 
                                                          'from gtt_price_hist_mgn_backup itbl, '|| 
                                                                'v_item_master im '|| 
                                                         'where upper(decode(p.bind_prim_lang, p.bind_user_lang, itbl.item_desc, itbl.tl_item_desc)) like '|| '''%''' || '|' || '|' ||'p.bind_item_desc' || '|' || '|' || '''%'''|| 
                                                          ' and im.item_parent = itbl.item '|| 
                                                          ' and im.item = p.item '|| 
                                                          ' and rownum = 1) '; 
         end if;
      end if;
   end if;

   if I_tran_type is not NULL then
      if (I_grp_type = 'S' and I_loc is not NULL and I_item is NULL) then
         L_sql_stmt3 := L_sql_stmt3 || ' and ((to_char(a.tran_type) = a.bind_tran_type and a.bind_tran_type != '||'''R'''||') '||
                                    'or '||
                                    '(a.tran_type in (0,4,8,9,10,11) and a.bind_tran_type = '||'''R'''||' ))';
       else 
         L_sql_stmt3 := L_sql_stmt3 || ' and ((to_char(p.tran_type) = p.bind_tran_type and p.bind_tran_type != '||'''R'''||') '|| 
                                    'or '|| 
                                    '(p.tran_type in (0,4,8,9,10,11) and p.bind_tran_type = '||'''R'''||' ))'; 
       end if;  

   end if;

   if I_item is null and I_item_desc is not null and I_item_type = 'P' then
      EXECUTE IMMEDIATE L_ins_stmt || L_sql_stmt1 || L_sql_stmt11 || L_sql_stmt2 || L_sql_stmt3
         USING I_from_date1,
               I_to_date,
               I_loc,
               I_tran_type,
               I_item,
               I_item_desc,
               I_prim_lang1,
               I_user_lang1,
               I_prim_currency1;
   else
      EXECUTE IMMEDIATE L_ins_stmt || L_sql_stmt1 || L_sql_stmt10 || L_sql_stmt11 || L_sql_stmt2 || L_sql_stmt3
         USING I_from_date1,
               I_to_date,
               I_loc,
               I_tran_type,
               I_item,
               I_item_desc,
               I_prim_lang1,
               I_user_lang1,
               I_prim_currency1;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_INFO;
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM(O_error_message         IN OUT VARCHAR2,
                       O_exists                IN OUT BOOLEAN,
                       I_item                  IN     PRICE_HIST.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'PCHISTMGN_SQL.VALIDATE_ITEM';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_ITEM is
      select 'X' 
        from price_hist
       where item = I_item
         and rownum = 1;
BEGIN

   open C_CHECK_ITEM;
   fetch C_CHECK_ITEM into L_dummy;

   if C_CHECK_ITEM%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_CHECK_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_ITEM;
----------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message          IN OUT VARCHAR2,
                           O_exists                 IN OUT BOOLEAN,
                           I_loc                    IN     PRICE_HIST.LOC%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'PCHISTMGN_SQL.VALIDATE_LOCATION';
   L_dummy VARCHAR2(1);

   cursor C_CHECK_LOC is
      select 'X' 
        from price_hist
       where loc = I_loc
         and rownum = 1;
BEGIN

   open C_CHECK_LOC;
   fetch C_CHECK_LOC into L_dummy;

   if C_CHECK_LOC%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   close C_CHECK_LOC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_LOCATION;
----------------------------------------------------------------------------------------
FUNCTION CALC_MARGIN(I_item            IN   ITEM_MASTER.ITEM%TYPE,
                     I_dept            IN   ITEM_MASTER.DEPT%TYPE,
                     I_class           IN   ITEM_MASTER.CLASS%TYPE,
                     I_subclass        IN   ITEM_MASTER.SUBCLASS%TYPE,
                     I_pack_ind        IN   ITEM_MASTER.PACK_IND%TYPE,                     
                     I_class_vat_ind   IN   CLASS.CLASS_VAT_IND%TYPE,
                     I_location        IN   ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                     I_loc_type        IN   FUTURE_COST.LOC_TYPE%TYPE, 
                     I_action_date     IN   PRICE_HIST.ACTION_DATE%TYPE,
                     I_pricing_cost    IN   FUTURE_COST.PRICING_COST%TYPE,
                     I_unit_retail     IN   PRICE_HIST.UNIT_RETAIL%TYPE)
   RETURN NUMBER IS

   L_program                VARCHAR2(40) := 'PCHISTMGN_SQL.CALC_MARGIN';
   L_error_message          RTK_ERRORS.RTK_TEXT%TYPE;
   L_regular_zone_group     RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE;
   L_clearance_zone_group   RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE;
   L_markup_calc_type       RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE;
   L_markup_percent         RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE;
   L_get_vat                VARCHAR2(1) := 'Y';
   L_vat_region             VAT_REGION.VAT_REGION%TYPE;
   L_vat_code               VAT_CODE_RATES.VAT_CODE%TYPE;
   L_vat_rate               VAT_CODE_RATES.VAT_RATE%TYPE;
   L_unit_retail            ITEM_LOC.UNIT_RETAIL%TYPE := I_unit_retail;
   L_margin_percent         RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE;
   L_sellable_ind           ITEM_MASTER.SELLABLE_IND%TYPE;
   L_bud_mkup               DEPS.BUD_MKUP%TYPE;
   L_bud_int                DEPS.BUD_INT%TYPE;
   L_markup_dept_type  DEPS.MARKUP_CALC_TYPE%TYPE;
   L_rpm_ind                VARCHAR2(1):=NULL;
   
   cursor C_RPM_IND  is 
      select RPM_IND 
	     from 
	    system_options;	   
   
   -- cursor to fetch sellable indicator for an item
   cursor C_GET_SELLABLE_IND is
         select im.sellable_ind 
           from item_master im
          where im.item = I_item;
      
BEGIN
     -- 
    open c_rpm_ind;
    fetch c_rpm_ind into L_rpm_ind;
    close c_rpm_ind;
    -- 
   if L_rpm_ind= 'Y' then 
      --- Get markup calc type
      if MERCH_RETAIL_API_SQL.GET_MERCH_PRICING_DEFS(L_error_message,
                                                     I_dept,
                                                     I_class,
                                                     I_subclass,
                                                     L_regular_zone_group,
                                                     L_clearance_zone_group,
                                                     L_markup_calc_type,
                                                     L_markup_percent) = FALSE then
         raise_application_error(-20000, L_error_message);
      end if;
      ---
   else 
      --- Get markup calc type when RPM_IND is 'N'
      if DEPT_ATTRIB_SQL.GET_MARKUP (L_error_message,
                                     L_markup_dept_type,
                                     L_bud_int,
									 L_bud_mkup,
                                     I_dept) = FALSE then
         raise_application_error(-20000, L_error_message);		 
      end if;	  
      ---
	  if L_markup_dept_type = 'C' then
         L_markup_calc_type:=0;
      else
         L_markup_calc_type:=1;
      end if;
   end if;  
   open C_GET_SELLABLE_IND;
   fetch C_GET_SELLABLE_IND into L_sellable_ind;
   close C_GET_SELLABLE_IND;
   ---
   if I_pack_ind = 'Y' and L_sellable_ind = 'N' then
      L_get_vat := 'N';
   else
      L_get_vat := 'Y';
   end if;
   ---
   if (LP_system_options_rec.default_tax_type = 'SVAT' and
      I_class_vat_ind = 'Y' and
      L_get_vat = 'Y') then
      ---
      if VAT_SQL.GET_VAT_RATE(L_error_message,
                              L_vat_region,
                              L_vat_code,
                              L_vat_rate,
                              I_item,
                              I_dept,
                              I_loc_type,
                              I_location,
                              I_action_date,
                              'R',
                              case when I_loc_type = 'S' then --store_ind
                                 'Y'
                              else
                                 'N'
                              end ) = FALSE then     
         raise_application_error(-20000, L_error_message);
      end if;
      --- Exclude applicable VAT from the Unit retail before calculating MARGIN%
      if L_vat_rate is not null  and (NVL(L_unit_retail,0) != 0) then
         L_unit_retail := L_unit_retail / (1 + L_vat_rate/100);
      end if;
      ---
   end if;
   ---
   --L_markup_calc_type = 0 is Cost Markup and 1 is Retail Markup
   ---
   if (L_markup_calc_type = 0 and NVL(I_pricing_cost,0) != 0) then
      L_margin_percent := round(100*(L_unit_retail - I_pricing_cost)/I_pricing_cost,2);
   elsif (L_markup_calc_type = 1 and NVL(L_unit_retail,0) != 0) then
      L_margin_percent := round(100*(L_unit_retail - I_pricing_cost)/L_unit_retail,2);
   end if;

   return L_margin_percent;
   
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      raise_application_error(-20000, L_error_message);
END CALC_MARGIN;
---------------------------------------------------------------------------------------------------------------
FUNCTION validate_inputs(O_error_message IN OUT VARCHAR2,
                         I_from_date1     IN PRICE_HIST.ACTION_DATE%TYPE,
                         I_grp_type      IN VARCHAR2,
                         I_loc_type      IN PRICE_HIST.LOC_TYPE%TYPE,
                         I_loc           IN PRICE_HIST.LOC%TYPE,
                         I_item          IN PRICE_HIST.ITEM%TYPE,
                         I_item_type     IN VARCHAR2)
   RETURN BOOLEAN
IS
   L_valid BOOLEAN;
   L_item_master_row V_ITEM_MASTER%ROWTYPE;
   L_wh PRICE_HIST.LOC%TYPE;
   L_loc_name STORE.STORE_NAME%TYPE;
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
   L_stockholding_ind V_WH.STOCKHOLDING_IND%TYPE;
   L_loc_list_desc V_LOC_LIST_HEAD.LOC_LIST_DESC%TYPE;
   L_program   VARCHAR2(64) := 'PCHISTMGN_SQL.VALIDATE_INPUTS';
BEGIN
   if I_item  IS NOT NULL then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_ITEM_MASTER(O_error_message, L_valid, L_item_master_row, I_item) = FALSE then
         RETURN false;
      end if;
      if L_valid        = FALSE then
         O_error_message:='INV_ITEM';
         RETURN false;
      end if;
      if I_item_type                     = 'I' then
         if L_item_master_row.tran_level != L_item_master_row.item_level then
            O_error_message               :='NOT_TRAN_LVL_ITEM';
            RETURN false;
         end if;
      else
         if L_item_master_row.tran_level - 1 != L_item_master_row.item_level then
             O_error_message                   :='NOT_VALID_PARENT';
             RETURN false;
         end if;
      end if;
      if PCHISTMGN_SQL.VALIDATE_ITEM(O_error_message, L_valid, i_item) = FALSE then
         RETURN false;
      end if;
      if L_valid        = FALSE then
         O_error_message:='NOT_PH_ITEM';
         RETURN false;
      end if;
   end if;
  
  ----loc validation
   if I_loc IS NOT NULL then
      if I_grp_type  = 'S' then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION(O_error_message, L_valid, L_loc_name, L_stockholding_ind, I_loc) = FALSE then
            RETURN false;
         end if;
         if L_valid        = FALSE then
            O_error_message:='INV_LOC' ;
            RETURN false;
         end if;
         if I_loc_type     = 'W' AND L_stockholding_ind = 'N' then
            O_error_message:='NON_STOCKHOLDING_WH';
            RETURN false;
           
         end if;
         if PCHISTMGN_SQL.VALIDATE_LOCATION(O_error_message, L_valid, I_loc) = FALSE then
            RETURN false;
         end if;
         if L_valid        = FALSE then
            O_error_message:='NOT_PH_LOC';
            RETURN false;
         end if;
      elsif I_grp_type   = 'L' then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOC_LIST_HEAD(O_error_message, L_valid, L_loc_list_desc, I_loc) = FALSE then
            RETURN false;
         end if;
         if L_valid        = FALSE then
            O_error_message:= 'INV_LOC_LIST';
            RETURN false;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END validate_inputs;            

---------------------------------------------------------------------------------------------------------------
END PCHISTMGN_SQL;
/

