/******************************************************************************
* Service Name     : ItemManagementService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/ItemManagementService/v1
* Description      : Item Management.
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE BODY ItemManagementServiceProviderI AS


/******************************************************************************
 *
 * Operation       : reserveItemNumber
 * Description     : 
        Reserve a chunk of RMS item numbers for the calling application.
               
 * 
 * Input           : "RIB_ItemNumCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ItemNumCriVo/v1
 * Description     : 
            Item number request object contains the details of the item numbers to reserve in RMS.
        
 * 
 * Output          : "RIB_ItemNumColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/ItemNumColDesc/v1
 * Description     : 
            ItemNumColDesc object is a collection of Item Numbers reserved in RMS based on the inputs provided.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE reserveItemNumber(I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                            I_businessObject          IN  "RIB_ItemNumCriVo_REC",
                            O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                            O_businessObject          OUT "RIB_ItemNumColDesc_REC")
   IS
   L_program             VARCHAR2(60)            := 'ItemmanagementServiceProviderI.reserveItemNumber';
   L_status              "RIB_SuccessStatus_REC" := NULL;
   L_successStatus_TBL   "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail_rec            "RIB_FailStatus_REC"    := NULL;
   L_fail_tbl            "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
   L_error_msg           RTK_ERRORS.RTK_TEXT%TYPE;
	
BEGIN
	
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   
   if CORESVC_ITEM_RESERVE_SQL.RESERVE_ITEM_NUMBER(L_error_msg,
                                                   O_businessObject,
                                                   I_serviceOperationContext,
                                                   I_businessObject) = FALSE then
      L_fail_rec := "RIB_FailStatus_REC"(0,L_error_msg,'error','error',null);
      L_fail_tbl.EXTEND;
      L_fail_tbl(L_fail_tbl.count) := L_fail_rec;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, null, L_fail_tbl);
      return;
   end if;
		
   L_status := "RIB_SuccessStatus_REC"(0, 'reserveItemNumber service call was successful.');
   L_successStatus_TBL.EXTEND;
   L_successStatus_TBL(L_successStatus_TBL.COUNT) := L_status;
   O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);


END reserveItemNumber;
/******************************************************************************/



END ItemManagementServiceProviderI;
/



