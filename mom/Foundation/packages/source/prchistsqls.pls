SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-- /****************************************************************************************/
-- ** The following nested tables in the item_locs_rec are needed to enhance performance
-- ** for the pricing subscription APIs:
-- **
-- **   original_unit_retails   UNIT_RETAIL_TBL,
-- **   units                   QTY_TBL,
-- **   clearance_inds          INDICATOR_TBL,
-- **   izp_rowids              ROWID_TBL,
-- **   item_loc_rowids         ROWID_TBL
-- **
-- ** These do not need to be populated for the insert into the price_hist table. However,
-- ** this function uses the same record that the API uses for the performance enhancement.
-- ** The new_price_rec record is only used for retail changes not cost changes.
-- /****************************************************************************************/

CREATE OR REPLACE PACKAGE PRICE_HIST_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   TYPE ROWID_TBL IS TABLE OF ROWID;

   TYPE ITEM_LOCS_REC    IS RECORD(items                 ITEM_TBL,
                                   locations             LOC_TBL,
                                   loc_type              LOC_TYPE_TBL,
                                   current_unit_retails  UNIT_RETAIL_TBL,
                                   original_unit_retails UNIT_RETAIL_TBL,
                                   unit_costs            UNIT_COST_TBL,
                                   units                 QTY_TBL,
                                   clearance_inds        INDICATOR_TBL,
                                   izp_rowids            ROWID_TBL,
                                   item_loc_rowids       ROWID_TBL);

   TYPE NEW_PRICE_REC    IS RECORD(single_changed        BOOLEAN,
                                   multi_changed         BOOLEAN,
                                   tran_type             PRICE_HIST.TRAN_TYPE%TYPE,
                                   standard_unit_retail  ITEM_LOC.UNIT_RETAIL%TYPE,
                                   selling_unit_retail   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                   selling_uom           ITEM_LOC.SELLING_UOM%TYPE,
                                   multi_units           ITEM_LOC.MULTI_UNITS%TYPE,
                                   multi_unit_retail     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                   multi_selling_uom     ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                   clearance_ind         ITEM_LOC.CLEAR_IND%TYPE);

--------------------------------------------------------------------------------
-- Function name: BULK_INSERT_PRICE_HIST
--
--------------------------------------------------------------------------------
FUNCTION BULK_INSERT_PRICE_HIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_new_price_attr    IN     NEW_PRICE_REC,
                                I_record_type       IN     VARCHAR2,
                                I_reason            IN     PRICE_HIST.REASON%TYPE,
                                I_loc_type          IN     PRICE_HIST.LOC_TYPE%TYPE)

RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function name: INSERT_PRICE_HIST
--
--------------------------------------------------------------------------------
FUNCTION INSERT_PRICE_HIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item_rec        IN     RMSSUB_XITEM.ITEM_API_REC,
                           I_unit_cost       IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------------
END PRICE_HIST_SQL;
/