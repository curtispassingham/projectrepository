CREATE OR REPLACE PACKAGE BODY SupplierServiceProviderImpl AS
---------------------------------------------------------------------------------------------
PROCEDURE assign_ref_to_rec(I_inputobject IN "RIB_SupplierColRef_REC",
                            I_referenceobject IN "RIB_SupplierColDesc_REC",
                            O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                            O_outputobject OUT "RIB_SupplierColDesc_REC" );
---------------------------------------------------------------------------------------------
PROCEDURE assign_ref_to_rec(I_inputobject IN "RIB_SupplierColRef_REC",
                            I_referenceobject IN "RIB_SupplierColDesc_REC",
                            O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                            O_outputobject OUT "RIB_SupplierColDesc_REC" )
IS
   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.assign_ref_to_rec'; 
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   
   CURSOR prepare_data IS
   SELECT "RIB_SupplierColDesc_REC"
          (
             I_referenceobject.rib_oid,-- rib_oid
             I_referenceobject.collection_size, -- collection_size
             CAST( MULTISET( SELECT "RIB_SupplierDesc_REC" 
                                    (
                                       SupDesc.rib_oid,
                                       SupDesc.sup_xref_key,
                                       SupRef.supplier_id, -- From the ref
                                       SupDesc.SupAttr, -- "RIB_SupAttr_REC"
                                       CAST( MULTISET( SELECT "RIB_SupSite_REC"
                                                              ( 
                                                                 SupSiteDesc.rib_oid,
                                                                 SupSiteDesc.supsite_xref_key,
                                                                 SupSiteRef.supplier_site_id, -- From the ref
                                                                 SupSiteDesc.SupAttr,  -- "RIB_SupAttr_REC"
                                                                 SupSiteDesc.SupSiteOrgUnit_TBL, -- "RIB_SupSiteOrgUnit_TBL"
                                                                 CAST( MULTISET(SELECT "RIB_SupSiteAddr_REC"
                                                                                       (
                                                                                          SupSiteAddrDesc.rib_oid,
                                                                                          NVL(SupSiteAddrDesc.addr_xref_key, 'NULL_FROM_AIA'),
                                                                                          SupSiteAddrRef.addr_key, -- From the ref
                                                                                          SupSiteAddrDesc.Addr -- "RIB_Addr_REC"
                                                                                        ) -- "RIB_SupSiteAddr_REC"
                                                                                  FROM TABLE (SupSiteDesc.SupSiteAddr_TBL) SupSiteAddrDesc,
                                                                                       TABLE (SupSiteRef.SupplierSiteAddr_TBL) SupSiteAddrRef
                                                                                 WHERE SupSiteAddrDesc.addr_xref_key = SupSiteAddrRef.addr_xref_key(+)       
                                                                                ) AS "RIB_SupSiteAddr_TBL"
                                                                      )
                                                               ) -- "RIB_SupSite_REC"
                                                        FROM TABLE (Supdesc.SupSite_TBL) SupSiteDesc,
                                                             TABLE (Supref.SupplierSite_TBL) SupSiteRef
                                                       WHERE SupSiteDesc.supsite_xref_key = SupSiteRef.supsite_xref_key
                                                      ) AS "RIB_SupSite_TBL"
                                            )
                                    ) -- "RIB_SupplierDesc_REC"
                               FROM TABLE (I_referenceobject.SupplierDesc_TBL) SupDesc,
                                    TABLE (I_inputobject.SupplierRef_TBL) SupRef
                              WHERE SupDesc.sup_xref_key = SupRef.sup_xref_key   
                            ) AS "RIB_SupplierDesc_TBL"
                 )
          ) -- RIB_SupplierColDesc_REC
     FROM dual;

BEGIN
   OPEN prepare_data;
   FETCH prepare_data INTO O_outputobject;
   CLOSE prepare_data;

EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);

END assign_ref_to_rec;                          
---------------------------------------------------------------------------------------------
PROCEDURE createSupplierDesc(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                             I_businessobject           IN   "RIB_SupplierDesc_REC",
                             O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                             O_businessobject           OUT  "RIB_SupplierRef_REC") 
IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.createSupplierDesc'; 
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   
   L_supplier_collection_rec   "RIB_SupplierColDesc_REC" := NULL;
   L_supplier_collection_ref   "RIB_SupplierColRef_REC" := NULL;
   L_supplier_desc_tbl         "RIB_SupplierDesc_TBL" := NULL;
   L_supplier_desc_rec         "RIB_SupplierDesc_REC" := NULL;

BEGIN
   --- Populate first Records for "RIB_SupplierColDesc_REC" objects
   if I_businessobject is not NULL then
      L_supplier_desc_rec :=  I_businessobject;
      L_supplier_desc_tbl := "RIB_SupplierDesc_TBL"();
      L_supplier_desc_tbl.EXTEND;
      L_supplier_desc_tbl(1) := L_supplier_desc_rec;

      L_supplier_collection_rec := "RIB_SupplierColDesc_REC"(
                                                 0,             --- rib_oid
                                                 1,             --- Collection Size
                                                 L_supplier_desc_tbl     --- "RIB_SupplierDesc_TBL"
                                                );
   end if;

   RMSAIASUB_SUPPLIER.CONSUME (L_status_code,
                               L_error_message,
                               L_supplier_collection_ref,
                               L_supplier_collection_rec,
                               'suppadd');
   
   O_serviceoperationstatus := NULL;

   if L_status_code = 'S' then   --- Successful completion
      O_businessobject := L_supplier_collection_ref.SupplierRef_TBL(1);
   end if;

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);

END createSupplierDesc;
---------------------------------------------------------------------------------------------
PROCEDURE createSupSiteUsingSupplierDesc(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                                         I_businessobject           IN   "RIB_SupplierDesc_REC",
                                         O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                                         O_businessobject           OUT  "RIB_SupplierRef_REC") IS
   
   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.createSupSiteUsingSupplierDesc';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   O_businessobject := NULL;
   
   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);

EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));

      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END createSupSiteUsingSupplierDesc;
--------------------------------------------------------------------------------------------------------------
PROCEDURE createSupSiteAddrUsingSupplier(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                                         I_businessobject           IN   "RIB_SupplierDesc_REC",
                                         O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                                         O_businessobject           OUT  "RIB_SupplierRef_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.createSupSiteAddrUsingSupplier';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   O_businessobject := NULL;

   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END createSupSiteAddrUsingSupplier;
---------------------------------------------------------------------------------------------
PROCEDURE updateSupplierDesc(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                             I_businessobject           IN   "RIB_SupplierDesc_REC",
                             O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                             O_businessobject           OUT  "RIB_SupplierDesc_REC") 
IS

   L_program                   VARCHAR2(64) := 'SupplierServiceProviderImpl.updateSupplierDesc';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

   L_supplier_collection_rec   "RIB_SupplierColDesc_REC" := NULL;
   L_supplier_collection_ref   "RIB_SupplierColRef_REC" := NULL;

   L_supplier_desc_tbl         "RIB_SupplierDesc_TBL" := NULL;
   L_supplier_desc_rec         "RIB_SupplierDesc_REC" := NULL;
   L_businessobject            "RIB_SupplierColDesc_REC" := NULL;

BEGIN
   --- Populate first Records for "RIB_SupplierColDesc_REC" objects  
   if I_businessobject is not NULL then
      L_supplier_desc_rec :=  I_businessobject;
      L_supplier_desc_tbl := "RIB_SupplierDesc_TBL"();
      L_supplier_desc_tbl.EXTEND;
      L_supplier_desc_tbl(1) := L_supplier_desc_rec;

      L_supplier_collection_rec := "RIB_SupplierColDesc_REC" 
                                 (
                                  0,                      --- rib_oid
                                  1,                      --- Collection Size
                                  L_supplier_desc_tbl     --- "RIB_SupplierDesc_TBL"
                                  );
   end if;

   RMSAIASUB_SUPPLIER.CONSUME (L_status_code,
                               L_error_message,
                               L_supplier_collection_ref,
                               L_supplier_collection_rec,
                               'suppmod');

   O_serviceoperationstatus := NULL;
   
   -- For update messages send back a RIB_SupplierDesc_REC, so use I_businessObject.
   assign_ref_to_rec(L_supplier_collection_ref,
                     L_supplier_collection_rec,
                     O_serviceoperationstatus,
                     L_businessobject);
      
   O_businessobject := L_businessobject.SupplierDesc_TBL(1);

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);

END updateSupplierDesc;
----------------------------------------------------------------------------------------------------------
PROCEDURE updateSupSiteUsingSupplierDesc(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                                         I_businessobject           IN   "RIB_SupplierDesc_REC",
                                         O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                                         O_businessobject           OUT  "RIB_SupplierDesc_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.updateSupSiteUsingSupplierDesc';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN
   
   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
EXCEPTION
   WHEN OTHERS then
      L_status_code   := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END updateSupSiteUsingSupplierDesc;
--------------------------------------------------------------------------------------------------------------
PROCEDURE updateSupSiteOrgUnitUsingSuppl(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                                         I_businessobject           IN   "RIB_SupplierDesc_REC",
                                         O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                                         O_businessobject           OUT  "RIB_SupplierDesc_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.updateSupSiteOrgUnitUsingSuppl';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END updateSupSiteOrgUnitUsingSuppl;
--------------------------------------------------------------------------------------------------------------
PROCEDURE updateSupSiteAddrUsingSupplier(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                                         I_businessobject           IN   "RIB_SupplierDesc_REC",
                                         O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                                         O_businessobject           OUT  "RIB_SupplierDesc_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.updateSupSiteAddrUsingSupplier';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END updateSupSiteAddrUsingSupplier;
-------------------------------------------------------------------------------------------
PROCEDURE findSupplierDesc(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                           I_businessobject           IN   "RIB_SupplierRef_REC",
                           O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                           O_businessobject           OUT  "RIB_SupplierDesc_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.findSupplierDesc';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   O_businessobject := NULL;

   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END findSupplierDesc;
---------------------------------------------------------------------------------------------
PROCEDURE deleteSupplierDesc(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                             I_businessobject           IN   "RIB_SupplierRef_REC",
                             O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                             O_businessobject           OUT  "RIB_SupplierRef_REC") IS
   
   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.deleteSupplierDesc';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END deleteSupplierDesc;
-------------------------------------------------------------------------------------------------------
PROCEDURE createSupplierColDesc(I_serviceoperationcontext  IN      "RIB_ServiceOpContext_REC",
                                I_businessobject           IN      "RIB_SupplierColDesc_REC",
                                O_serviceoperationstatus      OUT  "RIB_ServiceOpStatus_REC",
                                O_businessobject              OUT  "RIB_SupplierColRef_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.createSupplierColDesc';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status_code     VARCHAR2(1) := NULL;

BEGIN

   RMSAIASUB_SUPPLIER.CONSUME (L_status_code,
                               L_error_message,
                               O_businessobject,
                               I_businessobject,
                               'suppadd');

   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);

EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END createSupplierColDesc;
-------------------------------------------------------------------------------------------------------
PROCEDURE updateSupplierColDesc(I_serviceoperationcontext  IN   "RIB_ServiceOpContext_REC",
                                       I_businessobject           IN   "RIB_SupplierColDesc_REC",
                                       O_serviceoperationstatus   OUT  "RIB_ServiceOpStatus_REC",
                                       O_businessobject           OUT  "RIB_SupplierColDesc_REC") IS

   L_program            VARCHAR2(64) := 'SupplierServiceProviderImpl.updateSupplierColDesc';
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_status_code        VARCHAR2(1) := NULL;
   L_supplier_collection_ref   "RIB_SupplierColRef_REC" := NULL;

BEGIN

   RMSAIASUB_SUPPLIER.CONSUME (L_status_code,
               L_error_message,
               L_supplier_collection_ref,
               I_businessobject,
               'suppmod');

   -- For update messages send back a RIB_SupplierColDesc_REC, so use I_businessObject.
   assign_ref_to_rec(L_supplier_collection_ref,
                     I_businessobject,
                     O_serviceoperationstatus,
                     O_businessobject);

   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                      L_status_code,
                                      L_error_message,
                                      L_program);
                                      
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END updateSupplierColDesc;
-------------------------------------------------------------------------------------------------------
PROCEDURE findSupplierColDesc(I_serviceOperationContext IN      "RIB_ServiceOpContext_REC",
                              I_businessObject          IN      "RIB_SupplierColRef_REC",
                              O_serviceOperationStatus     OUT  "RIB_ServiceOpStatus_REC",
                              O_businessObject             OUT  "RIB_SupplierColDesc_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.findSupplierColDesc';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN

   O_serviceoperationstatus := NULL;
   O_businessObject := NULL;
   
   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
                                        
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END findSupplierColDesc;
-------------------------------------------------------------------------------------------------------
PROCEDURE deleteSupplierColDesc(I_serviceoperationcontext    IN   "RIB_ServiceOpContext_REC",
                                       I_businessobject             IN   "RIB_SupplierColRef_REC",
                                       O_serviceoperationstatus     OUT  "RIB_ServiceOpStatus_REC",
                                       O_businessobject             OUT  "RIB_SupplierColRef_REC") IS

   L_program         VARCHAR2(64) := 'SupplierServiceProviderImpl.deleteSupplierColDesc';
   L_status_code     VARCHAR2(1) := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;

BEGIN
   
   O_serviceoperationstatus := NULL;

   RMSAIA_LIB.BUILD_NOSERVICE_OP_STATUS(O_serviceoperationstatus,
                                        L_error_message,
                                        L_program);
                                        
EXCEPTION
   WHEN OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      
      RMSAIA_LIB.BUILD_SERVICE_OP_STATUS(O_serviceoperationstatus,
                                         L_status_code,
                                         L_error_message,
                                         L_program);
END deleteSupplierColDesc;
-------------------------------------------------------------------------------------------------------
END SupplierServiceProviderImpl;
/