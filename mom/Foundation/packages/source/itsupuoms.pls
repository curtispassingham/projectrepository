
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_SUPP_UOM_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------
-- Name       :  COPY_DOWN_PARENT_INFO
-- Purpose    :  This function copies the parent item information over the child and
--               grandchild information.
--
FUNCTION COPY_DOWN_PARENT_INFO(O_error_message     IN OUT VARCHAR2,
                               I_item              IN     ITEM_SUPP_UOM.ITEM%TYPE, 
                               I_supplier          IN     ITEM_SUPP_UOM.SUPPLIER%TYPE) 
        RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
-- Name       :  CHILD_ITEM_FOR_SUPP_EXISTS
-- Purpose    :  This function determines if there are any child items that are supplied by
--               the specified supplier. If there are such child items, then unit of measure 
--               records may be updated or inserted.
--
FUNCTION CHILD_ITEM_FOR_SUPP_EXISTS(O_error_message IN OUT VARCHAR2,
                                    O_exists        IN OUT BOOLEAN,
                                    I_item          IN     ITEM_SUPP_UOM.ITEM%TYPE,
                                    I_supplier      IN     ITEM_SUPP_UOM.SUPPLIER%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------------
END ITEM_SUPP_UOM_SQL;
/
                


