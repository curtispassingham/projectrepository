CREATE OR REPLACE PACKAGE BODY ITEMLIST_COUNT_SQL AS
---------------------------------------------------------------------------
FUNCTION COUNT_LIST (I_itemlist         IN     SKULIST_HEAD.SKULIST%TYPE,
                     I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                     I_item             IN     ITEM_MASTER.ITEM%TYPE,
                     I_item_parent      IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                     I_item_grandparent IN     ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                     I_dept             IN     ITEM_MASTER.DEPT%TYPE,
                     I_class            IN     ITEM_MASTER.CLASS%TYPE,
                     I_subclass         IN     ITEM_MASTER.SUBCLASS%TYPE,
                     I_supplier         IN     SKULIST_CRITERIA.SUPPLIER%TYPE,                     
                     I_diff_1           IN     ITEM_MASTER.DIFF_1%TYPE,
                     I_diff_2           IN     ITEM_MASTER.DIFF_2%TYPE,
                     I_diff_3           IN     ITEM_MASTER.DIFF_3%TYPE,
                     I_diff_4           IN     ITEM_MASTER.DIFF_4%TYPE,
                     I_uda_id           IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                     I_uda_value        IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                     I_uda_max_date     IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                     I_uda_min_date     IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                     I_season_id        IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                     I_phase_id         IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                     I_item_level       IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                     O_no_items         IN OUT NUMBER,
                     O_error_message    IN OUT VARCHAR2) RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'ITEMLIST_COUNT.COUNT_LIST';
   L_select           VARCHAR2(50);
   L_from_clause      VARCHAR2(200);
   L_where_clause     VARCHAR2(3000);
   L_statement        VARCHAR2(3250);
   L_exist_1          VARCHAR2(3000);
   L_exist_2          VARCHAR2(3000);
   L_between          VARCHAR2(50);
   L_dummy_where      VARCHAR2(3000);
   
   L_data_level_security_ind  SYSTEM_OPTIONS.DATA_LEVEL_SECURITY_IND%TYPE;
   
   cursor C_DATA_LEVEL_SECURITY is
      SELECT data_level_security_ind
        FROM system_options;

BEGIN
   --
   L_select := 'select count(distinct ima.item) ';
   L_from_clause := 'from item_master ima';
   L_where_clause := ' where ima.item_level <= ima.tran_level ';

   open C_DATA_LEVEL_SECURITY;
   fetch C_DATA_LEVEL_SECURITY INTO L_data_level_security_ind;
   close C_DATA_LEVEL_SECURITY;

   if L_data_level_security_ind = 'Y' then
      L_from_clause := L_from_clause || ', skulist_dept sld';
      L_where_clause := L_where_clause || ' and sld.skulist =  TO_CHAR(:bind_I_itemlist) '||'and ima.dept = sld.dept and ima.class = sld.class and ima.subclass = sld.subclass';
   end if;

   if I_item_parent is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '((ima.item = ''' || I_item_parent || ''') or ' ||
                        '(ima.item_parent = '''||I_item_parent||''')) ';
   end if;

   if I_item_grandparent is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '((ima.item = '''||I_item_grandparent||''') or ' ||
                        '(ima.item_parent = '''||I_item_grandparent||''') or ' ||
                        '(ima.item_grandparent = '''||I_item_grandparent||''')) ';
   end if;

   if I_item is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = '''||I_item||'''';
   end if;

   if I_dept is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.dept = '||I_dept;
   end if;

   if I_class is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.class = '||I_class;
   end if;

   if I_subclass is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.subclass = '||I_subclass;
   end if;

   if I_diff_1 is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_1   = ''' ||I_diff_1|| ''' or ' ||
                        '(ima.diff_2   = ''' ||I_diff_1|| ''') or ' ||
                        '(ima.diff_3   = ''' ||I_diff_1|| ''') or ' || 
                        '(ima.diff_4   = ''' ||I_diff_1|| '''))'; 

      if I_diff_2 is NOT NULL then
         L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_2   = ''' ||I_diff_2|| ''' or ' ||
                        '(ima.diff_3   = ''' ||I_diff_2|| ''') or ' ||
                        '(ima.diff_4   = ''' ||I_diff_2|| ''') or ' || 
                        '(ima.diff_1   = ''' ||I_diff_2|| '''))'; 

         if I_diff_3 is NOT NULL then
            L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_3   = ''' ||I_diff_3|| ''' or ' ||
                        '(ima.diff_4   = ''' ||I_diff_3|| ''') or ' ||
                        '(ima.diff_1   = ''' ||I_diff_3|| ''') or ' ||
                        '(ima.diff_2   = ''' ||I_diff_3|| '''))'; 

            if I_diff_4 is NOT NULL then
               L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_4   = ''' ||I_diff_4|| ''' or ' ||
                        '(ima.diff_1   = ''' ||I_diff_4|| ''') or ' ||
                        '(ima.diff_2   = ''' ||I_diff_4|| ''') or ' ||
                        '(ima.diff_3   = ''' ||I_diff_4|| '''))'; 
            end if;
         end if;
      end if;
   end if;
  
   if I_pack_ind is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.pack_ind = '''||I_pack_ind||'''';
   end if;

   if I_item_level is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item_level = '||I_item_level;
   end if;

   if I_supplier is NOT NULL then
      L_from_clause := L_from_clause ||
                       ', item_supplier isu';
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = isu.item and ' ||
                        'isu.supplier = '||I_supplier;
   end if;

   if I_season_id is NOT NULL then
      L_from_clause := L_from_clause ||
                       ', item_seasons ise';
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = ise.item and ' ||
                        'ise.season_id = '||I_season_id||' and ' ||
                        'ise.phase_id = nvl('''||I_phase_id||''', ise.phase_id)';
   end if;

   if I_uda_id is NOT NULL then
      L_dummy_where := L_where_clause;

      L_exist_1 := '(exists (select 1' ||
                             ' from uda_item_lov uil' ||
                            ' where ima.item = uil.item and '||
                                   'uil.uda_id = '||I_uda_id;
      L_exist_2 :=  'exists (select 1'||
                             ' from uda_item_date ud'||
                            ' where ima.item = ud.item and '||
                                   'ud.uda_id = '||I_uda_id; 
      L_between := ') or ';

      L_where_clause := L_where_clause || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;

   if I_uda_value is NOT NULL then
      L_exist_1 := L_exist_1 || ' and ' || ' uil.uda_value = '||I_uda_value;
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;

   if I_uda_min_date is NOT NULL then
      L_exist_2 := L_exist_2 || ' and ' || ' ud.uda_date >= '''||I_uda_min_date||'''';
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
                        
   end if;

   if I_uda_max_date is NOT NULL then
      L_exist_2 := L_exist_2 || ' and ' || ' ud.uda_date <= '''||I_uda_max_date||'''';
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;

   L_statement := L_select || L_from_clause || L_where_clause;

   if L_data_level_security_ind = 'Y' then
      EXECUTE IMMEDIATE L_statement  into O_no_items using I_itemlist;
   else
      EXECUTE IMMEDIATE L_statement into O_no_items;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
			  	             SQLERRM,
				             L_program,
				             to_char(SQLCODE));
      RETURN FALSE;
END COUNT_LIST;
------------------------------------------------------------------------------------
FUNCTION COUNT_EXISTS (I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE,
                       I_pack_ind          IN     ITEM_MASTER.PACK_IND%TYPE,
                       I_item              IN     ITEM_MASTER.ITEM%TYPE,
                       I_item_parent       IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_item_grandparent  IN     ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                       I_dept              IN     ITEM_MASTER.DEPT%TYPE,
                       I_class             IN     ITEM_MASTER.CLASS%TYPE,
                       I_subclass          IN     ITEM_MASTER.SUBCLASS%TYPE,
                       I_supplier          IN     SKULIST_CRITERIA.SUPPLIER%TYPE,                       
                       I_diff_1            IN     ITEM_MASTER.DIFF_1%TYPE,
                       I_diff_2            IN     ITEM_MASTER.DIFF_2%TYPE,
                       I_diff_3            IN     ITEM_MASTER.DIFF_3%TYPE,
                       I_diff_4            IN     ITEM_MASTER.DIFF_4%TYPE,
                       I_uda_id            IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                       I_uda_value         IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                       I_uda_max_date      IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                       I_uda_min_date      IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                       I_season_id         IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                       I_phase_id          IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                       I_item_level        IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                       O_no_items          IN OUT NUMBER,
                       O_error_message     IN OUT VARCHAR2) RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'ITEMLIST_COUNT.COUNT_EXISTS';
   L_select           VARCHAR2(50);
   L_from_clause      VARCHAR2(200);
   L_where_clause     VARCHAR2(3000);
   L_statement        VARCHAR2(3250);
   L_exist_1          VARCHAR2(3000);
   L_exist_2          VARCHAR2(3000);
   L_between          VARCHAR2(50);
   L_dummy_where      VARCHAR2(3000);
   TYPE ORD_CURSOR is REF CURSOR;
   C_ITEM             ORD_CURSOR;

BEGIN
   --
   L_select := 'select count(distinct ima.item) ';
   L_from_clause := 'from item_master ima, skulist_detail sd';
   L_where_clause := ' where ima.item_level <= ima.tran_level ' ||
                        'and ima.item = sd.item ' ||
                        'and sd.skulist = ' || I_itemlist;

   if I_item_parent is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '((ima.item = ''' || I_item_parent || ''') or ' ||
                        '(ima.item_parent = '''||I_item_parent||''')) ';
   end if;

   if I_item_grandparent is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '((ima.item = '''||I_item_grandparent||''') or ' ||
                        '(ima.item_parent = '''||I_item_grandparent||''') or ' ||
                        '(ima.item_grandparent = '''||I_item_grandparent||''')) ';
   end if;

   if I_item is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = '''||I_item||'''';
   end if;

   if I_dept is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.dept = '||I_dept;
   end if;

   if I_class is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.class = '||I_class;
   end if;

   if I_subclass is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.subclass = '||I_subclass;
   end if;

   if I_diff_1 is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_1   = ''' ||I_diff_1|| ''' or ' ||
                        '(ima.diff_2   = ''' ||I_diff_1|| ''') or ' ||
                        '(ima.diff_3   = ''' ||I_diff_1|| ''') or ' || 
                        '(ima.diff_4   = ''' ||I_diff_1|| '''))'; 

      if I_diff_2 is NOT NULL then
         L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_2   = ''' ||I_diff_2|| ''' or ' ||
                        '(ima.diff_3   = ''' ||I_diff_2|| ''') or ' ||
                        '(ima.diff_4   = ''' ||I_diff_2|| ''') or ' || 
                        '(ima.diff_1   = ''' ||I_diff_2|| '''))'; 

         if I_diff_3 is NOT NULL then
            L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_3   = ''' ||I_diff_3|| ''' or ' ||
                        '(ima.diff_4   = ''' ||I_diff_3|| ''') or ' ||
                        '(ima.diff_1   = ''' ||I_diff_3|| ''') or ' ||
                        '(ima.diff_2   = ''' ||I_diff_3|| '''))'; 

            if I_diff_4 is NOT NULL then
               L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_4   = ''' ||I_diff_4|| ''' or ' ||
                        '(ima.diff_1   = ''' ||I_diff_4|| ''') or ' ||
                        '(ima.diff_2   = ''' ||I_diff_4|| ''') or ' ||
                        '(ima.diff_3   = ''' ||I_diff_4|| '''))'; 
            end if;
         end if;
      end if;
   end if; 

   if I_pack_ind is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.pack_ind = '''||I_pack_ind||'''';
   end if;

   if I_item_level is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item_level = '||I_item_level;
   end if;

   if I_supplier is NOT NULL then
      L_from_clause := L_from_clause ||
                       ', item_supplier isu';
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = isu.item and ' ||
                        'isu.supplier = '||I_supplier;
   end if;

   if I_season_id is NOT NULL then
      L_from_clause := L_from_clause ||
                       ', item_seasons ise';
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = ise.item and ' ||
                        'ise.season_id = '||I_season_id||' and ' ||
                        'ise.phase_id = nvl('''||I_phase_id||''', ise.phase_id)';
   end if;

   if I_uda_id is NOT NULL then
      L_dummy_where := L_where_clause;

      L_exist_1 := '(exists (select 1' ||
                             ' from uda_item_lov uil' ||
                            ' where ima.item = uil.item and '||
                                   'uil.uda_id = '||I_uda_id;
      L_exist_2 :=  'exists (select 1'||
                             ' from uda_item_date ud'||
                            ' where ima.item = ud.item and '||
                                   'ud.uda_id = '||I_uda_id; 
      L_between := ') or ';

      L_where_clause := L_where_clause || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;

   if I_uda_value is NOT NULL then
      L_exist_1 := L_exist_1 || ' and ' || ' uil.uda_value = '||I_uda_value;
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;

   if I_uda_min_date is NOT NULL then
      L_exist_2 := L_exist_2 || ' and ' || ' ud.uda_date >= '''||I_uda_min_date||'''';
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
                        
   end if;

   if I_uda_max_date is NOT NULL then
      L_exist_2 := L_exist_2 || ' and ' || ' ud.uda_date <= '''||I_uda_max_date||'''';
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;

   L_statement := L_select || L_from_clause || L_where_clause;

   EXECUTE IMMEDIATE L_statement;
   open C_ITEM for L_statement;
   fetch C_ITEM into O_no_items;
   close C_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
			  	             SQLERRM,
				             L_program,
				             to_char(SQLCODE));
      RETURN FALSE;
END COUNT_EXISTS;
------------------------------------------------------------------------------------
END;
/