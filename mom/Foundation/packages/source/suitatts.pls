
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUPP_ITEM_ATTRIB_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------
-- Function Name: GET_VPN
-- Purpose  : takes a sku and a supplier and gets the vendor
--            product number for that sku and supplier
-- Calls    : none
-- Created  : 07-OCT-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION GET_VPN ( O_error_message IN OUT  VARCHAR2,
                   I_item          IN      item_supplier.item%TYPE,
                   I_supplier      IN      item_supplier.supplier%TYPE,
                   O_vpn           IN OUT  item_supplier.vpn%TYPE)
            RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: GET_PRIMARY_SUPP
-- Purpose  : takes a sku and gets the primary supplier for that sku.
-- Calls    : none
-- Created  : 07-OCT-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION GET_PRIMARY_SUPP( O_error_message  IN OUT  VARCHAR2,
                           O_exists         IN OUT  BOOLEAN,
                           O_supplier       IN OUT  ITEM_SUPPLIER.SUPPLIER%TYPE,
                           I_item           IN      ITEM_SUPPLIER.ITEM%TYPE)
               RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function Name: GET_PRIMARY_COUNTRY
-- Purpose  : Determines the primary country for a given Item/Supplier
--            relationship
-- Calls    : none
-------------------------------------------------------------------
FUNCTION GET_PRIMARY_COUNTRY(O_error_message  IN OUT  VARCHAR2,
                             O_origin_country IN OUT  ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_item           IN      ITEM_SUPP_COUNTRY.ITEM%TYPE,
                             I_supplier       IN      ITEM_SUPPLIER.SUPPLIER%TYPE)
               RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: GET_PACK_SIZES
-- Purpose: Retrieves the pack sizes for an item/supplier/country of origin
--          relationship.
-------------------------------------------------------------------------------------------
FUNCTION GET_PACK_SIZES(O_error_message      IN OUT VARCHAR2,
                        O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        O_pallet_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_case_desc          IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_inner_desc         IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN; 
-------------------------------------------------------------------------------------------
-- Function Name: GET_PACK_SIZES
-- Purpose: This overloaded version returns the ti-hi, pack sizes and item_supp_country names
-------------------------------------------------------------------------------------------
FUNCTION GET_PACK_SIZES(O_error_message      IN OUT VARCHAR2,
                        O_ti                 IN OUT ITEM_SUPP_COUNTRY.TI%TYPE,
                        O_hi                 IN OUT ITEM_SUPP_COUNTRY.HI%TYPE,
                        O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        O_pallet_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_case_desc          IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_inner_desc         IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN; 
-------------------------------------------------------------------------------------------
-- Function Name: GET_PACK_SIZES
-- Purpose: This overloaded version returns the pack sizes, item_supp_country names
--          and decoded code_detail packsizes;
-------------------------------------------------------------------------------------------
FUNCTION GET_PACK_SIZES(O_error_message      IN OUT VARCHAR2,
                        O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        O_pallet_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_case_desc          IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_inner_desc         IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_pallet_name        IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                        O_case_name          IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                        O_inner_name         IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: GET_PACK_SIZES
-- Purpose: This overloaded version returns the ti-hi, pack sizes, item_supp_country names
--          and decoded code_detail packsizes;
-------------------------------------------------------------------------------------------
FUNCTION GET_PACK_SIZES(O_error_message      IN OUT VARCHAR2,
                        O_ti                 IN OUT ITEM_SUPP_COUNTRY.TI%TYPE,
                        O_hi                 IN OUT ITEM_SUPP_COUNTRY.HI%TYPE,
                        O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        O_pallet_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_case_desc          IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_inner_desc         IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_pallet_name        IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                        O_case_name          IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                        O_inner_name         IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: GET_CARTON_INFO
-- Purpose: Retrieves the carton dimensions and the UOM for an item/supplier/country of
--          origin relationship.
-------------------------------------------------------------------------------------------
FUNCTION GET_CARTON_INFO(O_error_message     IN OUT VARCHAR2,
                         O_length               OUT ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                         O_height               OUT ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                         O_width                OUT ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE,
                         O_dim_uom              OUT ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                         O_weight               OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                         O_weight_uom           OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                         O_net_weight           OUT ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE,
                         O_liquid_volume        OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
                         O_volume_uom           OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE,
                         O_stat_cube            OUT ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE,
                         O_tare_wgt             OUT ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE,
                         O_tare_ind             OUT ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                         I_origin_country    IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                         I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
--Funtion: GET_PRIMARY_SUPP_COUNTRY
--Purpose: retrieve the passed item's primary supplier/origin country.
-----------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_SUPP_COUNTRY(O_error_message  IN OUT VARCHAR2,
                                  O_supplier       IN OUT ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                  O_origin_country IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                  I_item           IN     ITEM_SUPP_COUNTRY.ITEM%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
--Funtion: GET_ATTRIB_FOR_ORDSKU
--Purpose: retrieve the primary country ID, supplier pack size and unit cost for the
--         item/supplier combination.
-----------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_FOR_ORDSKU(O_error_message     IN OUT VARCHAR2,
                               O_unit_cost         IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                               O_supp_pack_size    IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                               O_origin_country_id IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                               I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                               I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
--Function: GET_SUPP_PACK_SIZE
--Purpose:  retrieves the supplier pack size for an item for the primary supp and the primary country
--          unless primary supp and country are specified.
-----------------------------------------------------------------------------------------
FUNCTION GET_SUPP_PACK_SIZE(O_error_message      IN OUT VARCHAR2,
                            O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                            I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                            I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                            I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- Function Name: GET_LEAD_TIME
-- Purpose  : Determines the lead_time for a given
--            Item/Supplier/Origin Country relationship
-----------------------------------------------------------------------------------------
FUNCTION GET_LEAD_TIME(O_error_message     IN OUT  VARCHAR2,
                       O_lead_time         IN OUT  ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                       I_item              IN      ITEM_SUPP_COUNTRY.ITEM%TYPE,
                       I_supplier          IN      ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                       I_origin_country_id IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GET_INFO
-- Purpose      : Output the information from the item_supplier table based on the 
--                specified item-supplier combination. If the specified item-supplier
--                combination does not exist, then the function will set the message, 
--                assign FALSE for the relationship exists variable, and still return
--                TRUE.
--
FUNCTION GET_INFO ( O_error_message         IN OUT VARCHAR2,
                    O_relationship_exists   IN OUT BOOLEAN,
                    O_primary_supp_ind      IN OUT ITEM_SUPPLIER.PRIMARY_SUPP_IND%TYPE,
                    O_vpn                   IN OUT ITEM_SUPPLIER.VPN%TYPE,
                    O_supp_label            IN OUT ITEM_SUPPLIER.SUPP_LABEL%TYPE,
                    O_consignment_rate      IN OUT ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
                    O_supp_diff_1           IN OUT ITEM_SUPPLIER.SUPP_DIFF_1%TYPE,
                    O_supp_diff_2           IN OUT ITEM_SUPPLIER.SUPP_DIFF_2%TYPE,
                    O_supp_diff_3           IN OUT ITEM_SUPPLIER.SUPP_DIFF_3%TYPE,
                    O_supp_diff_4           IN OUT ITEM_SUPPLIER.SUPP_DIFF_4%TYPE,
                    O_pallet_name           IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                    O_case_name             IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                    O_inner_name            IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                    O_supp_discontinue_date IN OUT ITEM_SUPPLIER.SUPP_DISCONTINUE_DATE%TYPE,
                    O_concession_rate       IN OUT ITEM_SUPPLIER.CONCESSION_RATE%TYPE,
                    I_item                  IN     ITEM_SUPPLIER.ITEM%TYPE,
                    I_supplier              IN     ITEM_SUPPLIER.SUPPLIER%TYPE)                    
   return BOOLEAN;
-----------------------------------------------------------------------------------------
--Function Name: GET_PRIMARY_CASE_SIZE
--Purpose      : retrieve the primary case size for the item/supplier combination.
------------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_CASE_SIZE(O_error_message     IN OUT VARCHAR2,
                               O_primary_case_size IN OUT ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE,
                               I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                               I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
--Function Name: GET_SUPP_SOB
--Purpose      : Get supplier related to the input Item that has the same org unit as 
--               the Input Location. Primary Supplier having higher priority.
------------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_SOB (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_supplier          IN OUT ITEM_LOC.PRIMARY_SUPP%TYPE,
                       I_item              IN     ITEM_LOC.ITEM%TYPE,
                       I_location          IN     ITEM_LOC.LOC%TYPE,
                       I_loc_type          IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
--Function Name: CHECK_ITEM_SUPP_PARENT_EXIST
--Purpose      : Checks if any one of the Supplier Sites related to the input Supplier is associated 
--               with the item passed as input
------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_SUPP_PARENT_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists            IN OUT BOOLEAN,
                                      I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                                      I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                      I_parent_ind        IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
--Function Name: CHECK_VPN_SUPP_EXIST 
--Purpose      : Checks if an item record exists in Item_supplier table for the input supplier
--               and VPN
------------------------------------------------------------------------------------------------
FUNCTION CHECK_VPN_SUPP_EXIST (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists            IN OUT BOOLEAN,
                               I_vpn               IN     ITEM_SUPPLIER.VPN%TYPE,
                               I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
--Function Name: ITEMSUPP_FILTER_LIST 
--Purpose      : Checks if any of the supplier associated to the item doesn't exist in V_SUPS
--               i.e : User doesn't have access to the supplier
------------------------------------------------------------------------------------------------
FUNCTION ITEMSUPP_FILTER_LIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_diff_ind        IN OUT VARCHAR2,
                              I_item            IN     ITEM_SUPPLIER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END SUPP_ITEM_ATTRIB_SQL;
/
