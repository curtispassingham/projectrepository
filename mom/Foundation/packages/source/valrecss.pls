
CREATE OR REPLACE PACKAGE VALIDATE_RECORDS_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
-- Function : DEL_ITEM
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            item not being deleted.
-- Calls    : None
----------------------------------------------------------------
FUNCTION DEL_ITEM(I_key_value        IN      VARCHAR2,
                  O_relations_exist  IN OUT  VARCHAR2,
                  L_error_message    IN OUT  VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Function : DEL_DEPS
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            department not being deleted.
-- Calls    : None
----------------------------------------------------------------

FUNCTION DEL_DEPS(I_key_value         IN       VARCHAR2,
                  O_relations_exist   IN OUT   VARCHAR2,
                  O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------

-- Function : DEL_CLASS
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            class not being deleted.
-- Calls    : None
----------------------------------------------------------------

FUNCTION DEL_CLASS(I_key_value         IN       VARCHAR2,
                   O_relations_exist   IN OUT   VARCHAR2,
                   O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------
-- Function : DEL_SUBCLASS
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            subclass not being deleted.
-- Calls    : None
----------------------------------------------------------------

FUNCTION DEL_SUBCLASS(I_key_value         IN       VARCHAR2,
                      O_relations_exist   IN OUT   VARCHAR2,
                      O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------
-- Function : DEL_STORE
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            store not being deleted.
-- Calls    : None
----------------------------------------------------------------

FUNCTION DEL_STORE(I_key_value         IN       VARCHAR2,
                   O_relations_exist   IN OUT   VARCHAR2,
                   O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------
-- Function : DEL_WH
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            warehouse not being deleted.
-- Calls    : None
----------------------------------------------------------------
FUNCTION DEL_WH(i_key_value       IN     VARCHAR2,
                o_relations_exist IN OUT VARCHAR2,
                error_message     IN OUT VARCHAR2) RETURN BOOLEAN;
------------------------------------------------------------------
-- Function : DEL_ZONE_GROUP
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            pricing zone group not being deleted.
-- Calls    : None
----------------------------------------------------------------
-- Function : DEL_COST_ZONE_GROUP
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            cost zone group not being deleted.
-- Calls    : None
----------------------------------------------------------------
FUNCTION DEL_COST_ZONE_GROUP(i_key_value       IN     VARCHAR2,
                             o_relations_exist IN OUT VARCHAR2,
                             error_message     IN OUT VARCHAR2) RETURN BOOLEAN;
------------------------------------------------------------------
-- Function : DEL_SHIPMENT
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            shipment not being deleted.
-- Calls    : None
----------------------------------------------------------------
FUNCTION DEL_SHIPMENT(i_key_value       IN     VARCHAR2,
                      o_relations_exist IN OUT VARCHAR2,
                      error_message     IN OUT VARCHAR2) RETURN BOOLEAN;
------------------------------------------------------------------
-- Function   : DEL_PACK_TEMPLATE
-- Purpose    : This function checks if a pack template is attached to a pack item.
--              If it is then it cannot be deleted.
-- Calls      : None
-- Created By : Angela Determan
-- Date       : 06-OCT-97
-----------------------------------------------------------------------------------
FUNCTION DEL_PACK_TEMPLATE(i_key_value       IN      VARCHAR2,
                           o_relations_exist IN OUT  VARCHAR2,
                           error_message     IN OUT  VARCHAR2) RETURN BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION VALIDATE_SELLABLE(I_sellable          IN       ITEM_MASTER.ITEM%TYPE,
                           O_delete_sellable   OUT      BOOLEAN,
                           O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE) RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function : DEL_EXTERNAL_FINISHER
-- Purpose  : This function checks for foreign key relationships
--            which, if they exist, would result in the chosen
--            external finsiher not being deleted.
-- Calls    : None
----------------------------------------------------------------
FUNCTION DEL_EXTERNAL_FINISHER(i_key_value       IN     VARCHAR2,
                               o_relations_exist IN OUT VARCHAR2,
                               error_message     IN OUT VARCHAR2) RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: VALIDATE_PRIMARY_PACK_NO
-- Purpose      : This function checks if the item is used as primary pack for replenishment.
----------------------------------------------------------------
FUNCTION VALIDATE_PRIMARY_PACK_NO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists            IN OUT   VARCHAR2,
                                  I_item              IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name : DEL_DAILY_PURGE_ERROR_LOG
-- Purpose  : This function deletes the validation error written
--            in the DAILY_PURGE_ERROR_LOG table for the given key Value.
-- Calls    : None
-------------------------------------------------------------------------
FUNCTION DEL_DAILY_PURGE_ERROR_LOG(I_key_value        IN      VARCHAR2,
                                   O_error_message    IN OUT  VARCHAR2) RETURN BOOLEAN;
-------------------------------------------------------------------------
END;
/
