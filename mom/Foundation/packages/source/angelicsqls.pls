CREATE OR REPLACE PACKAGE ANGELIC_PKG AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
--- Package Variables
-------------------------------------------------------------------------------
--- Function Name ANG_GET_CHAIN_NAME
--- Purpose       This function will change the chain name in desired format.
--------------------------------------------------------------------------------

--- Public function and procedure declarations
  FUNCTION ANG_GET_CHAIN_NAME(Is_chain_name IN CHAIN.CHAIN%TYPE) 
     RETURN VARCHAR2;

END ANGELIC_PKG;
/