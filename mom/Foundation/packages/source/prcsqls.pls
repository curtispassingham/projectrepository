CREATE OR REPLACE PACKAGE PRICING_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   TYPE ROWID_TBL IS TABLE OF ROWID;

   TYPE SUP_DATA_REC  IS RECORD(depts                 DEPT_TBL,
                                primary_suppliers     SUPPLIER_TBL,
                                tran_types            TRAN_TYPE_TBL,
                                amounts               UNIT_RETAIL_TBL);
   TYPE TRAN_DATA_REC IS RECORD(items                 ITEM_TBL,
                                locs                  LOC_TBL,
                                tran_codes            TRAN_TYPE_TBL,
                                unit_retail_changes   UNIT_RETAIL_TBL,
                                units                 QTY_TBL,
                                total_retails         UNIT_RETAIL_TBL,
                                old_unit_retails      UNIT_RETAIL_TBL,
                                new_unit_retails      UNIT_RETAIL_TBL);

-------------------------------------------------------------------------------
-- Function name: BULK_UPDATE_ITEM_LOC_PC
-- Purpose      : This function will bulk update the item_loc retail info (for
--                single and multi units) for all item_loc rowids in the passed 
--                rec.  this function will be called by the Xpricechange subscription
--                programs. 
--------------------------------------------------------------------------------
FUNCTION BULK_UPDATE_ITEM_LOC_PC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item_locs       IN       PRICE_HIST_SQL.ITEM_LOCS_REC,
                                 I_new_prices      IN       PRICE_HIST_SQL.NEW_PRICE_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function name: BULK_INSERT_SUP_DATA
-- Purpose      : This function will bulk insert records into sup_data table
--------------------------------------------------------------------------------
FUNCTION BULK_INSERT_SUP_DATA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_sup_data_rec    IN       SUP_DATA_REC)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END PRICING_SQL;
/
