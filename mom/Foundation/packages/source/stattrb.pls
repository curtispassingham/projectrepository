CREATE OR REPLACE PACKAGE BODY STORE_ATTRIB_SQL AS

--------------------------------------------------------------------
   FUNCTION ACTIVE (I_store          IN     NUMBER,
                           I_date           IN     DATE,
                           O_open_close_ind IN OUT VARCHAR2,
                           O_error_message  IN OUT VARCHAR2)
                           return           BOOLEAN IS

   L_start_date      DATE;
   L_stop_date       DATE;
   L_program   VARCHAR(64)  := 'STORE_ATTRIB_SQL.ACTIVE';

   cursor C_GET_DATES is
          select (store_open_date - start_order_days),
                (store_close_date - stop_order_days)
           from store
          where store = I_store;

   BEGIN

      open C_GET_DATES;
      fetch C_GET_DATES into L_start_date, L_stop_date;
      if C_GET_DATES%NOTFOUND then
         close C_GET_DATES;
         O_error_message :=
            sql_lib.create_msg('INV_STORE_ARG',to_char(I_store),NULL, NULL);
         return FALSE;
      end if;
      close C_GET_DATES;

      if I_date < L_start_date then
         O_error_message :=
            sql_lib.create_msg('?STORE_NOT_OPEN',to_char(I_store),NULL,NULL);
            O_open_close_ind := 'N';
            return TRUE;
      elsif L_stop_date is NOT NULL then
         if I_date > L_stop_date then
            O_error_message :=
               sql_lib.create_msg('STORE_CLOSED',to_char(I_store),NULL,NULL);
            O_open_close_ind := 'C';
            return TRUE;
         end if;
      else
         O_open_close_ind := 'O';
         return TRUE;
      end if;

   return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,L_program,NULL);
         return FALSE;

   END ACTIVE;
---------------------------------------------------------------------------------------

-----------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message IN OUT VARCHAR2,
                  I_store         IN     NUMBER,
                  O_store_name    IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'STORE_ATTRIB_SQL.GET_NAME';

   cursor C_STORE is
      select store_name
        from v_store_tl
       where store = I_store
      UNION ALL
      select store_name
        from v_store_add_tl
       where store = I_store;
BEGIN

   open C_STORE;
   fetch C_STORE into O_store_name;

   if C_STORE%NOTFOUND then
      close C_STORE;
      O_error_message := sql_lib.create_msg('INV_STORE',null,null,null);
      RETURN FALSE;

   else
      close C_STORE;
      RETURN TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
   RETURN FALSE;

END GET_NAME;
--------------------------------------------------------------------


FUNCTION STORE_NAME10(  O_error_message   IN OUT   VARCHAR2,
            I_store     IN NUMBER,
            O_desc      IN OUT VARCHAR2)

RETURN BOOLEAN IS

   cursor C_ATTRIB is
   select store_name10
     from store
    where store = I_store;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_ATTRIB', 'STORE', to_char(I_store));
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH' , 'C_ATTRIB','STORE', to_char(I_store));
   fetch C_ATTRIB into O_desc;
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_STORE',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'STORE', to_char(I_store));
      close C_ATTRIB;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'STORE', to_char(I_store));
   close C_ATTRIB;

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                        SQLERRM,
                        'STORE_ATTRIB_SQL.STORE_NAME10',
                        to_char(SQLCODE));
   RETURN FALSE;

END STORE_NAME10;
--------------------------------------------------------------------


FUNCTION MALL_NAME(O_error_message  IN OUT   VARCHAR2,
         I_store     IN NUMBER,
         O_desc      IN OUT VARCHAR2)

RETURN BOOLEAN IS

   cursor C_ATTRIB is
   select mall_name
     from store
    where store = I_store;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_ATTRIB', 'STORE', to_char(I_store));
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH' , 'C_ATTRIB','STORE', to_char(I_store));
   fetch C_ATTRIB into O_desc;
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_STORE',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'STORE', to_char(I_store));
      close C_ATTRIB;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'STORE', to_char(I_store));
   close C_ATTRIB;

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                   SQLERRM,
                   'STORE_ATTRIB_SQL.MALL_NAME',
                   to_char(SQLCODE));
   RETURN FALSE;

END MALL_NAME;
--------------------------------------------------------------------


FUNCTION FORMAT_NAME(O_error_message   IN OUT   VARCHAR2,
           I_format     IN NUMBER,
           O_desc    IN OUT VARCHAR2)

RETURN BOOLEAN IS

   cursor C_ATTRIB is
   select format_name
     from store_format
    where store_format = I_format;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_ATTRIB', 'STORE_FORMAT', to_char(I_format));
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH' , 'C_ATTRIB','STORE_FORMAT', to_char(I_format));
   fetch C_ATTRIB into O_desc;
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_STR_FORMAT',to_char(I_format),NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB','STORE_FORMAT', to_char(I_format));
      close C_ATTRIB;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'STORE_FORMAT', to_char(I_format));
   close C_ATTRIB;

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                        SQLERRM,
                        'STORE_ATTRIB_SQL.FORMAT_NAME',
                        to_char(SQLCODE));
   RETURN FALSE;

END FORMAT_NAME;
--------------------------------------------------------------------
FUNCTION GET_TSF_ZONE(O_error_message IN OUT VARCHAR2,
                      O_tsf_zone      IN OUT store.transfer_zone%TYPE,
                      O_store_in_zone IN OUT BOOLEAN,
                      I_store         IN     store.store%TYPE)
                      return BOOLEAN IS

   cursor C_GET_TSF_ZONE is
      select transfer_zone
        from store
       where store = I_store;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_store',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_GET_TSF_ZONE', 'store', 'store: '||to_char(I_store));
   open C_GET_TSF_ZONE;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TSF_ZONE', 'store', 'store: '||to_char(I_store));
   fetch C_GET_TSF_ZONE into O_tsf_zone;
   ---
   if C_GET_TSF_ZONE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_ZONE', 'store', 'store: '||to_char(I_store));
      close C_GET_TSF_ZONE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TSF_ZONE', 'store', 'store: '||to_char(I_store));
   close C_GET_TSF_ZONE;
   ---
   if O_tsf_zone is NOT NULL then
      O_store_in_zone := TRUE;
   else
      O_store_in_zone := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'STORE_ATTRIB_SQL.GET_TSF_ZONE',
                                             to_char(SQLCODE));
   RETURN FALSE;

END GET_TSF_ZONE;
--------------------------------------------------------------------

FUNCTION GET_INFO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_store_name      IN OUT   STORE.STORE_NAME%TYPE,
                   O_store_add1      IN OUT   ADDR.ADD_1%TYPE,          
                   O_store_add2      IN OUT   ADDR.ADD_2%TYPE,          
                   O_store_city      IN OUT   ADDR.CITY%TYPE,          
                   O_state           IN OUT   ADDR.STATE%TYPE,         
                   O_country_id      IN OUT   ADDR.COUNTRY_ID%TYPE,    
                   O_store_pcode     IN OUT   ADDR.POST%TYPE,          
                   O_fax_number      IN OUT   STORE.FAX_NUMBER%TYPE,
                   O_phone_number    IN OUT   STORE.PHONE_NUMBER%TYPE,
                   I_store           IN       STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)          := 'STORE_ATTRIB_SQL.GET_INFO';
   L_store_rec STORE_RECTYPE         := NULL;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_store', 'NULL', 'NOT NULL');
      return FALSE;
   end if;
   
   if GET_INFO(O_error_message,
               L_store_rec,
               I_store) = FALSE then
      return FALSE;
   end if;
   
   O_store_name   := L_store_rec.store_name;
   O_store_add1   := L_store_rec.store_add1;
   O_store_add2   := L_store_rec.store_add2;
   O_store_city   := L_store_rec.store_city;
   O_state        := L_store_rec.state;
   O_country_id   := L_store_rec.country_id;
   O_store_pcode  := L_store_rec.store_pcode;
   O_fax_number   := L_store_rec.fax_number;
   O_phone_number := L_store_rec.phone_number;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END GET_INFO;
--------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_store_rec       IN OUT   STORE_RECTYPE,
                   I_store           IN       STORE.STORE%TYPE)
return BOOLEAN IS

   L_program   VARCHAR2(64)          := 'STORE_ATTRIB_SQL.GET_INFO';

   cursor C_STORE_INFO is
      SELECT s.store_name,
             a.add_1,
             a.add_2,
             a.city,
             a.state,
             a.country_id,
             a.post,
             s.fax_number,
             s.phone_number,
             s.store_type,
             s.stockholding_ind
        FROM store s,
             addr a,
             add_type_module at
       WHERE s.store = I_store
         AND TO_CHAR(s.store) = a.key_value_1 
         AND a.module = decode(s.store_type,'C','ST','WFST')
         AND a.module = at.module
         AND a.primary_addr_ind = 'Y'
         AND at.primary_ind = 'Y'
         AND at.mandatory_ind = 'Y'
         AND a.addr_type = at.address_type;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_store', 'NULL', 'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_STORE_INFO', 'STORE', TO_CHAR(I_store));
   open C_STORE_INFO;
   
   SQL_LIB.SET_MARK('FETCH', 'C_STORE_INFO', 'STORE', TO_CHAR(I_store));
   fetch C_STORE_INFO into O_store_rec;
   
   if C_STORE_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_STORE_INFO', 'STORE', TO_CHAR(I_store));
      close C_STORE_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE', NULL, NULL, NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_STORE_INFO', 'STORE', TO_CHAR(I_store));
   close C_STORE_INFO;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END GET_INFO;
--------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE(O_error_message  IN OUT VARCHAR2,
                           O_currency_code  IN OUT STORE.CURRENCY_CODE%TYPE,
                           I_store          IN     STORE.STORE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program     VARCHAR2(64) := 'STORE_ATTRIB_SQL.GET_CURRENCY_CODE';
   ---
   cursor C_STORE is
      select currency_code
        from store
       where store = I_store;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_STORE','STORE','Store: '||to_char(I_store));
   open C_STORE;
   SQL_LIB.SET_MARK('FETCH','C_STORE','STORE','Store: '||to_char(I_store));
   fetch C_STORE into O_currency_code;
   if C_STORE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_STORE','STORE','Store: '||to_char(I_store));
      close C_STORE;
      O_error_message := sql_lib.create_msg('INV_STORE',
                                            null,null,null);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_STORE','STORE','Store: '||to_char(I_store));
   close C_STORE;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_CURRENCY_CODE;
---------------------------------------------------------------------------------------
FUNCTION GET_STORE_DISTRICT (O_error_message IN OUT VARCHAR2,
                             O_district      IN OUT store.district%TYPE,
                             I_store         IN     store.store%TYPE)
RETURN BOOLEAN IS

   cursor C_DISTRICT is
      select district
        from store
       where store = I_store;

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_DISTRICT','STORE','STORE'||I_store);
   open C_DISTRICT;
   SQL_LIB.SET_MARK('FETCH','C_DISTRICT','STORE','STORE'||I_store);
   fetch C_DISTRICT into O_district;
   ---
   if C_DISTRICT%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                            NULL,
                                            NULL,
                                            NULL);

      SQL_LIB.SET_MARK('CLOSE','C_DISTRICT','STORE','STORE'||I_store);
      close C_DISTRICT;

      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_DISTRICT','STORE','STORE'||I_store);
   close C_DISTRICT;
   ---

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STORE_ATTRIB_SQL.GET_STORE_DISTRICT',
                                            to_char(SQLCODE));
   return FALSE;

END GET_STORE_DISTRICT ;
------------------------------------------------------------------------
FUNCTION STORE_CLASS_EXIST(O_error_message           IN OUT VARCHAR2,
                           O_exists                  IN OUT BOOLEAN,
                           I_store_class             IN     STORE.STORE_CLASS%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR(50) := 'STORE_ATTRIB_SQL.STORE_CLASS_EXIST';
   L_s_class  VARCHAR(1);

   CURSOR C_STORE_CLASS is
   select 'x'
     from store
    where I_store_class = store_class;

BEGIN


   if I_store_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_store_class', L_program, NULL);
      return FALSE;
   end if;

   ---
   SQL_LIB.SET_MARK('OPEN','C_STORE_CLASS','STORE_CLASS', I_store_class);
   open C_STORE_CLASS;

   SQL_LIB.SET_MARK('FETCH','C_STORE_CLASS','STORE_CLASS', I_store_class);
   fetch C_STORE_CLASS into L_s_class;

   if C_STORE_CLASS%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_STORE_CLASS','STORE_CLASS', I_store_class);
      close C_STORE_CLASS;
      O_exists := FALSE;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_STORE_CLASS','STORE_CLASS', I_store_class);
   close C_STORE_CLASS;
   O_exists := TRUE;
   return TRUE;
   ---

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END STORE_CLASS_EXIST;

---------------------------------------------------------------------------------
FUNCTION STORE_VALID_FOR_CURRENCY(O_error_message   IN OUT  VARCHAR2,
                                  O_valid           IN OUT  BOOLEAN,
                                  O_store_name      IN OUT  STORE.STORE_NAME%TYPE,
                                  I_currency_code   IN      STORE.CURRENCY_CODE%TYPE,
                                  I_store           IN      STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR(50) := 'STORE_ATTRIB_SQL.STORE_VALID_FOR_CURRENCY';
   L_valid    VARCHAR2(1) := 'N';

   cursor C_VALIDATE_STORE_CURRENCY is
      select 'Y'
        from store
       where store         = I_store
         and currency_code = I_currency_code;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_store', L_program, NULL);
      return FALSE;
   elsif I_currency_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_currency_code', L_program, NULL);
      return FALSE;
   end if;

   O_valid := FALSE;

   SQL_LIB.SET_MARK('OPEN','C_VALIDATE_STORE_CURRENCY','store/currency = ', I_store||'/'||I_currency_code);
   open C_VALIDATE_STORE_CURRENCY;
   SQL_LIB.SET_MARK('FETCH','C_VALIDATE_STORE_CURRENCY','store/currency = ', I_store||'/'||I_currency_code);
   fetch C_VALIDATE_STORE_CURRENCY into L_valid;
   SQL_LIB.SET_MARK('CLOSE','C_VALIDATE_STORE_CURRENCY','store/currency = ', I_store||'/'||I_currency_code);
   close C_VALIDATE_STORE_CURRENCY;

   if L_valid = 'Y' then
      if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                   I_store,
                                   O_store_name) = FALSE then
         return FALSE;
      end if;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END STORE_VALID_FOR_CURRENCY;
---------------------------------------------------------------------------------
FUNCTION GET_STORE_CLOSE_DATE(O_error_message   IN OUT  VARCHAR2,
                              O_close_date      IN OUT  STORE.STORE_CLOSE_DATE%TYPE,
                              I_store           IN      STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR(50) := 'STORE_ATTRIB_SQL.GET_STORE_CLOSE_DATE';

   cursor C_GET_STORE_CLOSE_DATE is
      select store_close_date
        from store
       where store = I_store;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_store', L_program, NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_GET_STORE_CLOSE_DATE', 'store', 'store: '||to_char(I_store));
   open C_GET_STORE_CLOSE_DATE;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_STORE_CLOSE_DATE', 'store', 'store: '||to_char(I_store));
   fetch C_GET_STORE_CLOSE_DATE into O_close_date;
   if C_GET_STORE_CLOSE_DATE%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_STORE',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_STORE_CLOSE_DATE', 'store', 'store: '||to_char(I_store));
      close C_GET_STORE_CLOSE_DATE;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_STORE_CLOSE_DATE', 'store', 'store: '||to_char(I_store));
   close C_GET_STORE_CLOSE_DATE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_STORE_CLOSE_DATE;
---------------------------------------------------------------------------------

FUNCTION VALIDATE_HIERARCHY(O_error_message  IN OUT   VARCHAR2,
                  I_store     IN NUMBER,
                  I_district     IN NUMBER,
                  I_region    IN NUMBER,
                  I_area      IN NUMBER,
                  I_chain     IN NUMBER)

RETURN BOOLEAN IS

   L_store NUMBER;

   cursor C_VALID_HIERARCHY is
      select store
      from   store_hierarchy
      where  store = nvl(I_store,store)
      and    district = nvl(I_district,district)
      and    region = nvl(I_region,region)
      and    area = nvl(I_area,area)
      and    chain = nvl(I_chain,chain);

BEGIN
   open C_VALID_HIERARCHY;
   fetch C_VALID_HIERARCHY into L_store;
   if C_VALID_HIERARCHY%NOTFOUND then
      O_error_message := sql_lib.create_msg('ENTERED_STORE_INVALID',NULL,NULL,NULL);
      close C_VALID_HIERARCHY;
      RETURN FALSE;

   end if;
   close C_VALID_HIERARCHY;

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                   SQLERRM,
                   'STORE_ATTRIB_SQL.VALIDATE_HIERARCHY',
                   to_char(SQLCODE));
   RETURN FALSE;

END VALIDATE_HIERARCHY;
--------------------------------------------------------------------

FUNCTION VALIDATE_WALKTHROUGH(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid                IN OUT BOOLEAN,
                              O_store_name           IN OUT STORE.STORE_NAME%TYPE,
                              I_store                IN     STORE.STORE%TYPE,
                              I_walk_through_store   IN     WALK_THROUGH_STORE.WALK_THROUGH_STORE%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50)                   := 'STORE_ATTRIB_SQL.VALIDATE_WALKTHROUGH';
   L_stockholding_ind  STORE.STOCKHOLDING_IND%TYPE;
   L_invalid_param     VARCHAR2(50);
   L_duplicate         BOOLEAN;

   cursor C_GET_STORE is
      select stockholding_ind
        from store
       where store = I_walk_through_store;

BEGIN

   if I_store is NULL then
      L_invalid_param := 'I_store';
   elsif I_walk_through_store is NULL then
      L_invalid_param := 'I_walk_through_store';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', L_invalid_param, L_program, NULL);
      return FALSE;
   end if;

   O_valid := FALSE;

   if I_store = I_walk_through_store then
      O_error_message := SQL_LIB.CREATE_MSG('STORE_NOT_WT_STORE', NULL, NULL, NULL);
      return TRUE;
   end if;

   open C_GET_STORE;
   fetch C_GET_STORE into L_stockholding_ind;
   close C_GET_STORE;

   if L_stockholding_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE', NULL, NULL, NULL);
      return TRUE;
   elsif L_stockholding_ind = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('NON_STOCKHOLDING', NULL, NULL, NULL);
      return TRUE;
   end if;

   if STORE_ATTRIB_SQL.CHECK_DUP_WALKTHROUGH(O_error_message,
                                             L_duplicate,
                                             I_store,
                                             I_walk_through_store) = FALSE then
      return FALSE;
   end if;

   if L_duplicate = TRUE then
      return TRUE;
   end if;

   if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                I_walk_through_store,
                                O_store_name) = FALSE then
      return FALSE;
   end if;

   O_valid := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                    L_program,
                    NULL);
      return FALSE;

END VALIDATE_WALKTHROUGH;
--------------------------------------------------------------------

FUNCTION CHECK_DUP_WALKTHROUGH(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_duplicate            IN OUT BOOLEAN,
                               I_store                IN     STORE.STORE%TYPE,
                               I_walk_through_store   IN     WALK_THROUGH_STORE.WALK_THROUGH_STORE%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50)                   := 'STORE_ATTRIB_SQL.CHECK_DUP_WALKTHROUGH';
   L_dup_exists        VARCHAR2(1)                    := 'N';
   L_invalid_param     VARCHAR2(50);

   cursor C_CHECK_DUP is
      select 'Y'
        from walk_through_store
       where store = I_store
         and walk_through_store = I_walk_through_store;

BEGIN

   if I_store is NULL then
      L_invalid_param := 'I_store';
   elsif I_walk_through_store is NULL then
      L_invalid_param := 'I_walk_through_store';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', L_invalid_param, L_program, NULL);
      return FALSE;
   end if;

   open C_CHECK_DUP;
   fetch C_CHECK_DUP into L_dup_exists;
   close C_CHECK_DUP;

   O_duplicate := FALSE;

   if L_dup_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_LOC_DEL', NULL, NULL, NULL);
      O_duplicate := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                    L_program,
                    NULL);
      return FALSE;
END CHECK_DUP_WALKTHROUGH;
-------------------------------------------------------------------------------
FUNCTION CHECK_WALKTHROUGH_EXISTS (O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists               IN OUT BOOLEAN,
                                   I_store                IN     STORE.STORE%TYPE)

RETURN BOOLEAN IS

   L_program                   VARCHAR2(50)           := 'STORE_ATTRIB_SQL.CHECK_WALKTHROUGH_EXISTS';
   L_walkthrough_exists        VARCHAR2(1)            := 'N';

   cursor C_WALKTHROUGH_EXISTS is
      select 'Y'
        from walk_through_store
       where store = I_store;

BEGIN

   open C_WALKTHROUGH_EXISTS;
   fetch C_WALKTHROUGH_EXISTS into L_walkthrough_exists;
   close C_WALKTHROUGH_EXISTS;

   O_exists := (L_walkthrough_exists = 'Y');

return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                    SQLERRM,
                    L_program,
                    NULL);
      return FALSE;

END CHECK_WALKTHROUGH_EXISTS;
-------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists         IN OUT  BOOLEAN,
                 O_store_row      IN OUT  STORE%ROWTYPE,
                 I_store          IN      STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'STORE_ATTRIB_SQL.GET_ROW';

   cursor C_GET_ROW is
      select *
        from store
       where store = I_store;

BEGIN

   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_store', L_program, NULL);
      return FALSE;
   end if;

   --- Initialize output variable
   O_store_row := NULL;

   open  C_GET_ROW;
   fetch C_GET_ROW into O_store_row;
   close C_GET_ROW;

   O_exists := (O_store_row.store is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_ROW;
---------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_country_id      IN OUT   ADDR.COUNTRY_ID%TYPE,
                        I_store           IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'STORE_ATTRIB_SQL.GET_COUNTRY_ID';
   
   cursor C_STORE is
      select country_id
        from addr 
       where key_value_1 = to_char(I_store)
         and module in ('ST','WFST')
         and primary_addr_ind = 'Y'
    group by key_value_1, 
             country_id;
             
BEGIN

   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if; 
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_STORE',
                    'ADDR',
                    'Store: '||to_char(I_store));
   open C_STORE;    

   SQL_LIB.SET_MARK('FETCH',
                    'C_STORE',
                    'ADDR',
                    'Store: '||to_char(I_store));
   fetch C_STORE into O_country_id;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_STORE',
                    'ADDR',
                    'Store: '||to_char(I_store));
   close C_STORE;    
   
   if O_country_id is NULL then  
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_COUNTRY_ID;
-------------------------------------------------------------------------------
FUNCTION GET_STOCKHOLDING_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stkhldg_ind   OUT    STORE.STOCKHOLDING_IND%TYPE,
                              I_store         IN     STORE.STORE%TYPE)
RETURN BOOLEAN IS

   cursor C_STORE is
      select stockholding_ind
        from store
       where store = I_store;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_STORE','STORE','Store: '||to_char(I_store));
   open C_STORE;
   SQL_LIB.SET_MARK('FETCH','C_STORE','STORE','Store: '||to_char(I_store));
   fetch C_STORE into O_stkhldg_ind;
   if C_STORE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_STORE','STORE','Store: '||to_char(I_store));
      close C_STORE;
      O_error_message := sql_lib.create_msg('INV_STORE',null,null,null);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE','C_STORE','STORE','Store: '||to_char(I_store));
      close C_STORE;
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'STORE_ATTRIB_SQL.GET_STOCKHOLDING_IND',
                                            NULL);
      return FALSE;
END GET_STOCKHOLDING_IND;
--------------------------------------------------------------------
END STORE_ATTRIB_SQL;
/
