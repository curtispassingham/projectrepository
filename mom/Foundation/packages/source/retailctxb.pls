CREATE OR REPLACE PACKAGE BODY RETAIL_CTX_PKG AS
----------------------------------------------------------------
-- Name:    SET_APP_CTX
-- Purpose: This procedure will be called from ReSA for data security
--          purpose. This Will set the context for the application user .
----------------------------------------------------------------
  PROCEDURE SET_APP_CTX(p_attribute_name VARCHAR2, p_attribute_value VARCHAR2) AS
  BEGIN
    DBMS_SESSION.SET_CONTEXT('RETAIL_CTX', p_attribute_name, UPPER(p_attribute_value));
    DBMS_SESSION.SET_IDENTIFIER(UPPER(p_attribute_value));
  END SET_APP_CTX;
END RETAIL_CTX_PKG;
/