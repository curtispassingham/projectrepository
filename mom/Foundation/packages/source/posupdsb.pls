CREATE OR REPLACE PACKAGE BODY POS_UPDATE_SQL AS
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_POS_STORE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_pos_config_id     IN       POS_STORE.POS_CONFIG_ID%TYPE,
                          I_pos_config_type   IN       POS_STORE.POS_CONFIG_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program_name   VARCHAR2(60) := 'POS_UPDATE_SQL.DELETE_POS_STORE';

BEGIN

   if I_pos_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_id',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   if I_pos_config_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_type',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   delete pos_store
    where pos_config_id = I_pos_config_id
      and pos_config_type = I_pos_config_type;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_POS_STORE;
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_POS_STORE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_pos_config_id     IN       POS_STORE.POS_CONFIG_ID%TYPE,
                          I_pos_config_type   IN       POS_STORE.POS_CONFIG_TYPE%TYPE,
                          I_location          IN       NUMBER,
                          I_loc_type          IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program_name   VARCHAR2(60) := 'POS_UPDATE_SQL.INSERT_POS_STORE';

BEGIN
   if I_pos_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_id',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   if I_pos_config_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_type',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   if I_pos_config_type NOT IN ('COUP', 'PRES') then
      O_error_message := SQL_LIB.CREATE_MSG('POS_INV_CONFIG_TYPE',
                                            'I_pos_config_type',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;    
   
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type NOT IN ('S', 'AS', 'A','R', 'D', 'LL') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE',
                                            'I_loc_type',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;  

   if I_location is NULL and I_loc_type != 'AS' then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   if I_loc_type = 'A' then
      if I_pos_config_type = 'COUP' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_coupon_head pch
                 where s.currency_code = pch.currency_code
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pch.coupon_id = I_pos_config_id
                   and s.area = I_location) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      elsif I_pos_config_type = 'PRES' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_prod_rest_head pprh
                 where s.currency_code = NVL(pprh.currency_code, s.currency_code)
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pprh.pos_prod_rest_id = I_pos_config_id
                   and s.area = I_location) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      end if;
   elsif I_loc_type = 'R' then
      if I_pos_config_type = 'COUP' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_coupon_head pch
                 where s.currency_code = pch.currency_code
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pch.coupon_id = I_pos_config_id
                   and s.region = I_location) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      elsif I_pos_config_type = 'PRES' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_prod_rest_head pprh
                 where s.currency_code = NVL(pprh.currency_code, s.currency_code)
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pprh.pos_prod_rest_id = I_pos_config_id
                   and s.region = I_location) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      end if;
   elsif I_loc_type = 'D' then
      if I_pos_config_type = 'COUP' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_coupon_head pch
                 where s.currency_code = pch.currency_code
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pch.coupon_id = I_pos_config_id
                   and s.district = I_location) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      elsif I_pos_config_type = 'PRES' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_prod_rest_head pprh
                 where s.currency_code = NVL(pprh.currency_code, s.currency_code)
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pprh.pos_prod_rest_id = I_pos_config_id
                   and s.district = I_location) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      end if;
   elsif I_loc_type = 'LL' then
      if I_pos_config_type = 'COUP' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       loc_list_detail lld,
                       pos_coupon_head pch
                 where s.currency_code = pch.currency_code
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pch.coupon_id = I_pos_config_id
                   and s.store = lld.location
                   and lld.loc_list = I_location
                   and lld.loc_type = 'S') s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      elsif I_pos_config_type = 'PRES' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       loc_list_detail lld,
                       pos_prod_rest_head pprh
                 where s.currency_code = NVL(pprh.currency_code, s.currency_code)
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pprh.pos_prod_rest_id = I_pos_config_id
                   and s.store = lld.location
                   and lld.loc_list = I_location
                   and lld.loc_type = 'S') s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      end if;
   elsif I_loc_type = 'S' then
      if I_pos_config_type = 'COUP' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_coupon_head pch
                 where s.currency_code = pch.currency_code
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pch.coupon_id = I_pos_config_id
                   and s.store = I_location) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      elsif I_pos_config_type = 'PRES' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_prod_rest_head pprh
                 where s.currency_code = NVL(pprh.currency_code, s.currency_code)
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pprh.pos_prod_rest_id = I_pos_config_id
                   and s.store = I_location) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      end if;
   elsif I_loc_type = 'AS' then
      if I_pos_config_type = 'COUP' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_coupon_head pch
                 where s.currency_code = pch.currency_code
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pch.coupon_id = I_pos_config_id) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      elsif I_pos_config_type = 'PRES' then
         merge into pos_store p
         using (select s.store
                  from v_store s,
                       pos_prod_rest_head pprh
                 where s.currency_code = NVL(pprh.currency_code, s.currency_code)
                   and ((s.store_type = 'C') or
                        (s.store_type = 'F' and
                         s.stockholding_ind = 'Y'))
                   and pprh.pos_prod_rest_id = I_pos_config_id) s
            on (p.store = s.store and
                p.pos_config_id = I_pos_config_id and
                p.pos_config_type = I_pos_config_type)
          when matched then update set p.status = 'A'
                                 where p.status = 'D'
          when not matched then insert(p.pos_config_id,
                                       p.pos_config_type,
                                       p.store,
                                       p.status)
                                values(I_pos_config_id,
                                       I_pos_config_type,
                                       s.store,
                                       'A');
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_POS_STORE;
---------------------------------------------------------------------------------------------------------------
END POS_UPDATE_SQL;
/
