



CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRDEPT_VALIDATE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This overloaded private function will check all required fields for the create and
   --                modify messages to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrDeptDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This overloaded private function will check the required field for the delete
   --                message to ensure that it is not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DEPARTMENT_EXISTS
   -- Purpose      : This overloaded function will verify that the department and the node being modified
   --                already exist on the database.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DEPARTMENT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_message         IN       "RIB_XMrchHrDeptDesc_REC",
                                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DEPARTMENT_EXISTS
   -- Purpose      : This overloaded function will verify that the department to be deleted already
   --                exist on the database.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DEPARTMENT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_message         IN       "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_TOTAL_MARKET_AMT
   -- Purpose      : This function will check if the passed in total market amount is at least 1000.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TOTAL_MARKET_AMT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_message         IN       "RIB_XMrchHrDeptDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_CHILD_NODES
   -- Purpose      : This function will check all the required fields for the child node of the message
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_CHILD_NODES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_message         IN       "RIB_XMrchHrDeptDesc_REC",
                                    I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DELETE
   -- Purpose      : This function will verify that the department can be deleted.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_DEFAULT_FIELDS
   -- Purpose      : This function will retrieve values not defined in the message but required for
   --                RMS integrity,
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEFAULT_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_vat_ind        IN OUT NOCOPY   DEPS.DEPT_VAT_INCL_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This overloaded function will convert the RIB department object into a department
   --                record .
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_dept_rec        OUT    NOCOPY   MERCH_SQL.DEPT_REC,
                         I_message         IN              "RIB_XMrchHrDeptDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This overloaded function will convert the RIB department object into a department
   --                record .
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_dept_rec        OUT    NOCOPY   MERCH_SQL.DEPT_REC,
                         I_message         IN              "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dept_rec        OUT    NOCOPY   MERCH_SQL.DEPT_REC,
                       I_message         IN              "RIB_XMrchHrDeptDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60)  := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;

   if I_message_type in (RMSSUB_XMRCHHRDEPT.LP_cre_type, RMSSUB_XMRCHHRDEPT.LP_mod_type) then
      if not CHECK_TOTAL_MARKET_AMT(O_error_message,
                                    I_message) then
         return FALSE;
      end if;
   end if;

   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if not CHECK_REQUIRED_CHILD_NODES(O_error_message,
                                     I_message,
                                     I_message_type) then
      return FALSE;
   end if;

   if I_message_type != RMSSUB_XMRCHHRDEPT.LP_cre_type then
      if not CHECK_DEPARTMENT_EXISTS(O_error_message,
                                     I_message,
                                     I_message_type) then
         return FALSE;
      end if;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_dept_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dept_rec        OUT    NOCOPY   MERCH_SQL.DEPT_REC,
                       I_message         IN              "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_DEPARTMENT_EXISTS(O_error_message,
                                  I_message) then
      return FALSE;
   end if;

   if not CHECK_DELETE(O_error_message,
                       I_message) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_dept_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrDeptDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN
   -- check for required fields for all message types
   if I_message.dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Dept', NULL, NULL);
      return FALSE;
   end if;

   if I_message_type in (RMSSUB_XMRCHHRDEPT.LP_cre_type, RMSSUB_XMRCHHRDEPT.LP_mod_type) then
      if I_message.dept_name is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Dept Name', NULL, NULL);
         return FALSE;
      end if;

      if I_message.group_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Group Number', NULL, NULL);
         return FALSE;
      end if;

      if I_message.markup_calc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Markup Calculation Type', NULL, NULL);
         return FALSE;
      end if;

      if I_message.otb_calc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'OTB Calculation Type', NULL, NULL);
         return FALSE;
      end if;

      if I_message.profit_calc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Profit Calculation Type', NULL, NULL);
         return FALSE;
      end if;

      if I_message.purchase_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Purchase Type', NULL, NULL);
         return FALSE;
      end if;

      -- table check constraints
      if I_message.markup_calc_type NOT in ('C', 'R') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_MARKUP_TYPE_VALUE', I_message.markup_calc_type, NULL, NULL);
         return FALSE;
      end if;

      if I_message.otb_calc_type NOT in ('C', 'R') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_OTB_CALC_TYPE', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.profit_calc_type NOT in (1, 2) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PROFIT_CALC_TYPE', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.purchase_type NOT in (0, 1, 2) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PURCHASE_TYPE', NULL, NULL, NULL);
         return FALSE;
      end if;

      -- check if a department is set up as Direct Cost and Consignment
      if I_message.profit_calc_type = 1 and I_message.purchase_type = 1 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CALC_TYPE', NULL, NULL, NULL);
         return FALSE;
      end if;

      -- check whether budgeted markup or budgeted intake is null.
      if I_message.bud_mkup is NULL and I_message.bud_int is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Budgeted Intake Percentage and Budgeted Markup Percentage', NULL, NULL);
         return FALSE;
      end if;


      if I_message.max_avg_counter is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Maximum Average Days', NULL, NULL);
         return FALSE;
      end if;

      if I_message.avg_tolerance_pct is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Average Tolerance Percent', NULL, NULL);
         return FALSE;
      end if;

      if I_message.max_avg_counter <= 0 then
        O_error_message := SQL_LIB.CREATE_MSG('NO_LESS_ZERO', NULL, NULL, NULL);
        return FALSE;
      end if;

      if I_message.avg_tolerance_pct <= 0 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_LESS_ZERO', NULL, NULL, NULL);
         return FALSE;
      end if;

   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN IS

   L_program        VARCHAR2(60) := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_REQUIRED_FIELDS';
   L_exists         BOOLEAN      := FALSE;

BEGIN

   if I_message.dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Dept', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DEPARTMENT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_message         IN       "RIB_XMrchHrDeptDesc_REC",
                                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(60)             := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_DEPARTMENT_EXISTS';
   L_exist                BOOLEAN                  := FALSE;
   L_table_name           VARCHAR2(10)             := 'DEPS';
   L_tax_info_tbl         OBJ_TAX_INFO_TBL         := OBJ_TAX_INFO_TBL();
   L_tax_info_rec         OBJ_TAX_INFO_REC         := OBJ_TAX_INFO_REC();
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;

BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   -- Check for daily_purge existence
   if NOT DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                       L_exist,
                                       I_message.dept,
                                       L_table_name) then
      return FALSE;
   end if;

   if L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('EXIST_IN_DAILY_PURGE',
                                            I_message.dept,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Check for Department
   L_exist := FALSE;
   if not DEPT_VALIDATE_SQL.EXIST(O_error_message,
                                  I_message.dept,
                                  L_exist) then
      return FALSE;
   end if;

   if not L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Department', I_message.dept, NULL);
      return FALSE;
   end if;

   -- Check for Detail
   if I_message.xmrchhrdeptvat_tbl is NOT NULL then
      if L_system_options_row.default_tax_type ='SVAT' then
         FOR i in I_message.xmrchhrdeptvat_tbl.first..I_message.xmrchhrdeptvat_tbl.last LOOP
            L_exist := FALSE;    -- Reset exist flag
            L_tax_info_rec                       := OBJ_TAX_INFO_REC();
            L_tax_info_tbl.DELETE;
            L_tax_info_tbl.EXTEND;
            L_tax_info_rec.merch_hier_value      := I_message.dept;
            L_tax_info_rec.from_tax_region       := I_message.xmrchhrdeptvat_tbl(i).vat_region;
            L_tax_info_rec.cost_retail_ind       := I_message.xmrchhrdeptvat_tbl(i).vat_type;
            L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;

            if TAX_SQL.TAX_DEPT_REGION_EXISTS(O_error_message,
                                              L_tax_info_tbl) = FALSE then
               return FALSE;
            end if;

            if L_tax_info_tbl.COUNT > 0 then
               L_exist := TRUE;
            end if;

            if NOT L_exist and I_message_type = RMSSUB_XMRCHHRDEPT.LP_dtl_mod_type then
               O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_DEPT', NULL, NULL, NULL);
               return FALSE;
            end if;
         end LOOP;
      elsif L_system_options_row.default_tax_type in ('GTAX','SALES') then
         return TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DEPARTMENT_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DEPARTMENT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_message         IN       "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_DEPARTMENT_EXISTS';
   L_exist        BOOLEAN      := FALSE;
   L_table_name   VARCHAR2(10) := 'DEPS';

BEGIN
   -- Check for daily_purge existence
   if NOT DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                       L_exist,
                                       I_message.dept,
                                       L_table_name) then
      return FALSE;
   end if;

   if L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('EXIST_IN_DAILY_PURGE',
                                            I_message.dept,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Check for Department
   L_exist  := FALSE;
   if not DEPT_VALIDATE_SQL.EXIST(O_error_message,
                                  I_message.dept,
                                  L_exist) then
      return FALSE;
   end if;

   if not L_exist  then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Department', I_message.dept, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DEPARTMENT_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TOTAL_MARKET_AMT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_message         IN       "RIB_XMrchHrDeptDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_TOTAL_MARKET_AMT';

BEGIN

   if NVL(I_message.total_market_amt, 1000) < 1000 then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MARKET_AMT', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_TOTAL_MARKET_AMT;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_CHILD_NODES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_message         IN       "RIB_XMrchHrDeptDesc_REC",
                                    I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_REQUIRED_CHILD_NODES';
   L_exist        BOOLEAN      := FALSE;

BEGIN

   -- check for NULL detail table specific to detail create and detail modify message types.
   if I_message_type in (RMSSUB_XMRCHHRDEPT.LP_dtl_cre_type, RMSSUB_XMRCHHRDEPT.LP_dtl_mod_type) then
      if I_message.xmrchhrdeptvat_tbl is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   if I_message.xmrchhrdeptvat_tbl is not NULL then
      FOR i in I_message.xmrchhrdeptvat_tbl.first..I_message.xmrchhrdeptvat_tbl.last LOOP
         -- check required fields for detail table.
         if I_message.xmrchhrdeptvat_tbl(i).vat_region is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Vat Region', NULL, NULL);
            return FALSE;
         end if;

         if I_message.xmrchhrdeptvat_tbl(i).vat_code is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Vat Code', NULL, NULL);
            return FALSE;
         end if;

         if I_message.xmrchhrdeptvat_tbl(i).vat_type is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Vat Type', NULL, NULL);
            return FALSE;
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_CHILD_NODES;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN IS

   L_program             VARCHAR2(60)  := 'RMSSUB_XMRCHHRDEPT_VALIDATE.CHECK_DELETE';
   L_exist               VARCHAR2(1)   := 'N';
   L_dept_exist          BOOLEAN       := FALSE;
   L_filter_merch_level  FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE := 'P';

BEGIN

   -- checks if the Merchandise Hierarchy exists in one of the Data Element.
   if not FILTER_GROUP_HIER_SQL.VALIDATE_GROUP_MERCH(O_error_message,
                                                     L_dept_exist,
                                                     L_filter_merch_level,
                                                     I_message.dept,
                                                     NULL,
                                                     NULL) then
      return FALSE;
   end if;

   if L_dept_exist then
      O_error_message := SQL_LIB.CREATE_MSG('CNT_DEL_REC', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DELETE;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEFAULT_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_vat_ind        IN OUT NOCOPY   DEPS.DEPT_VAT_INCL_IND%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60)  := 'RMSSUB_XMRCHHRDEPT_VALIDATE.POPULATE_DEFAULT_FIELDS';

BEGIN

   if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type in ('SVAT','GTAX') and
      SYSTEM_OPTIONS_SQL.GP_system_options_row.class_level_vat_ind = 'N' then
      IO_vat_ind := 'Y';
   else
      IO_vat_ind := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_DEFAULT_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_dept_rec        OUT    NOCOPY   MERCH_SQL.DEPT_REC,
                         I_message         IN              "RIB_XMrchHrDeptDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XMRCHHRDEPT_VALIDATE.POPULATE_RECORD';

BEGIN

   if not POPULATE_DEFAULT_FIELDS(O_error_message,
                                  O_dept_rec.dept_row.dept_vat_incl_ind) then
      return FALSE;
   end if;

   if I_message.bud_mkup is not NULL then
      O_dept_rec.dept_row.bud_int  := round((I_message.bud_mkup * 100/(100 + I_message.bud_mkup)), 4);
      O_dept_rec.dept_row.bud_mkup := I_message.bud_mkup;
   else
      O_dept_rec.dept_row.bud_mkup := round((I_message.bud_int * 100/(100 - I_message.bud_int)), 4);
      O_dept_rec.dept_row.bud_int  := I_message.bud_int;
   end if;

   O_dept_rec.dept_row.dept               := I_message.dept;
   O_dept_rec.dept_row.dept_name          := I_message.dept_name;
   O_dept_rec.dept_row.buyer              := I_message.buyer;
   O_dept_rec.dept_row.merch              := I_message.merch;
   O_dept_rec.dept_row.profit_calc_type   := I_message.profit_calc_type;
   O_dept_rec.dept_row.purchase_type      := I_message.purchase_type;
   O_dept_rec.dept_row.group_no           := I_message.group_no;
   O_dept_rec.dept_row.total_market_amt   := I_message.total_market_amt;
   O_dept_rec.dept_row.markup_calc_type   := I_message.markup_calc_type;
   O_dept_rec.dept_row.otb_calc_type      := I_message.otb_calc_type;
   O_dept_rec.dept_row.max_avg_counter    := I_message.max_avg_counter;
   O_dept_rec.dept_row.avg_tolerance_pct  := I_message.avg_tolerance_pct;

   if I_message.xmrchhrdeptvat_tbl is not NULL then

      -- initialize collection
      O_dept_rec.vat_detail := MERCH_SQL.VAT_DEPT_DETAIL_TBL();

      FOR i in I_message.xmrchhrdeptvat_tbl.first..I_message.xmrchhrdeptvat_tbl.last LOOP
         O_dept_rec.vat_detail.EXTEND;
         O_dept_rec.vat_detail(i).vat_region        := I_message.xmrchhrdeptvat_tbl(i).vat_region;
         O_dept_rec.vat_detail(i).dept              := I_message.dept;
         O_dept_rec.vat_detail(i).vat_code          := I_message.xmrchhrdeptvat_tbl(i).vat_code;
         O_dept_rec.vat_detail(i).vat_type          := I_message.xmrchhrdeptvat_tbl(i).vat_type;
         O_dept_rec.vat_detail(i).reverse_vat_ind   := I_message.xmrchhrdeptvat_tbl(i).reverse_vat_ind;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_dept_rec        OUT    NOCOPY   MERCH_SQL.DEPT_REC,
                         I_message         IN              "RIB_XMrchHrDeptRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XMRCHHRDEPT_VALIDATE.POPULATE_RECORD';

BEGIN

   O_dept_rec.dept_row.dept  :=  I_message.dept;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRDEPT_VALIDATE;
/
