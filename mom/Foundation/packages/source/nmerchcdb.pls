CREATE OR REPLACE PACKAGE BODY NON_MERCH_CODE_SQL AS
-----------------------------------------------------------------------------------------------------
FUNCTION MERGE_NON_MERCH_CODE_HEAD_TL( O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_non_merch_code      IN     NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE%TYPE,
                                       I_lang                IN     NON_MERCH_CODE_HEAD_TL.LANG%TYPE,
                                       I_non_merch_code_desc IN     NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE_DESC%TYPE) 
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)     := 'NON_MERCH_CODE_SQL.MERGE_NON_MERCH_CODE_HEAD_TL';

   cursor C_NON_MERCH_CODE is
      select 'X'
        from non_merch_code_head_tl
       where non_merch_code = I_non_merch_code
         and lang           = I_lang
         for update nowait;

   L_orig_lang_ind              INV_ADJ_REASON_TL.ORIG_LANG_IND%TYPE;
   L_reviewed_ind               INV_ADJ_REASON_TL.REVIEWED_IND%TYPE;
   L_non_merch_code_found       VARCHAR2(1);

BEGIN
   SQL_LIB.SET_MARK('OPEN', 
                    'C_NON_MERCH_CODE', 
                    'non_merch_code_head_tl', 
                     NULL);
   open C_NON_MERCH_CODE;
   SQL_LIB.SET_MARK('FETCH', 
                    'C_NON_MERCH_CODE', 
                    'non_merch_code_head_tl', 
                    NULL);
   fetch C_NON_MERCH_CODE into L_non_merch_code_found;
   if C_NON_MERCH_CODE%NOTFOUND then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind := 'Y';
   end if;

   merge into non_merch_code_head_tl nmchtl
      using(select  I_non_merch_code non_merch_code,
                    I_lang lang,
                    I_non_merch_code_desc non_merch_code_desc,
                    L_orig_lang_ind orig_lang_ind, --used for inserts only
                    L_reviewed_ind reviewed_ind,   --used for inserts only
                    user create_id,
                    sysdate create_datetime,
                    user last_update_id,
                    sysdate last_update_datetime
               from dual) use_this
         on (nmchtl.non_merch_code = use_this.non_merch_code and
             nmchtl.lang   = use_this.lang)
      when matched then
         update
            set nmchtl.non_merch_code_desc = use_this.non_merch_code_desc,
                nmchtl.reviewed_ind = decode(nmchtl.orig_lang_ind, 'Y', 'N', reviewed_ind), --when description is changed for the original language, set the entry for translation review
                nmchtl.last_update_id = use_this.last_update_id,
                nmchtl.last_update_datetime = use_this.last_update_datetime
      when NOT matched then
         insert (non_merch_code,
                 lang,
                 non_merch_code_desc,
                 orig_lang_ind,
                 reviewed_ind,
                 create_id,
                 create_datetime,
                 last_update_id,
                 last_update_datetime)
         values (use_this.non_merch_code,
                 use_this.lang,
                 use_this.non_merch_code_desc,
                 use_this.orig_lang_ind,
                 use_this.reviewed_ind,
                 use_this.create_id,
                 use_this.create_datetime,
                 use_this.last_update_id,
                 use_this.last_update_datetime);

   SQL_LIB.SET_MARK('CLOSE', 
                    'C_NON_MERCH_CODE', 
                    'non_merch_code_head_tl', 
                    NULL);
   close C_NON_MERCH_CODE;
   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;

END MERGE_NON_MERCH_CODE_HEAD_TL;
--------------------------------------------------------------------------------
FUNCTION DEL_NON_MERCH_CODE_HEAD_TL( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_non_merch_code  IN     NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE%TYPE ) 
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)     := 'NON_MERCH_CODE_SQL.DEL_NON_MERCH_CODE_HEAD_TL';

   cursor C_LOCK_NON_MERCH_CODE_TL is
      select 'X'
        from non_merch_code_head_tl
       where non_merch_code = I_non_merch_code         
         for update nowait;

   L_non_merch_code_found       VARCHAR2(1);
BEGIN
   SQL_LIB.SET_MARK('OPEN', 
                    'C_LOCK_NON_MERCH_CODE_TL', 
                    'non_merch_code_head_tl', 
                     NULL);
   open C_LOCK_NON_MERCH_CODE_TL;
   SQL_LIB.SET_MARK('FETCH', 
                    'C_LOCK_NON_MERCH_CODE_TL', 
                    'non_merch_code_head_tl', 
                    NULL);
   fetch C_LOCK_NON_MERCH_CODE_TL into L_non_merch_code_found;
   if C_LOCK_NON_MERCH_CODE_TL%FOUND then
      delete non_merch_code_head_tl
       where non_merch_code = I_non_merch_code;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 
                    'C_LOCK_NON_MERCH_CODE_TL', 
                    'non_merch_code_head_tl', 
                    NULL);
   close C_LOCK_NON_MERCH_CODE_TL;

   return TRUE;
EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DEL_NON_MERCH_CODE_HEAD_TL;
--------------------------------------------------------------------------------
FUNCTION DEL_NON_MERCH_CODE_COMP( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_non_merch_code  IN     NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE%TYPE ) 
RETURN BOOLEAN IS

   L_program              VARCHAR2(64)     := 'NON_MERCH_CODE_SQL.DEL_NON_MERCH_CODE_COMP';

   cursor C_LOCK_NON_MERCH_CODE_COMP is
      select 'X'
        from non_merch_code_comp
       where non_merch_code = I_non_merch_code         
         for update nowait;

   L_non_merch_code_found       VARCHAR2(1);
BEGIN
   SQL_LIB.SET_MARK('OPEN', 
                    'C_LOCK_NON_MERCH_CODE_COMP', 
                    'non_merch_code_comp', 
                     NULL);
   open C_LOCK_NON_MERCH_CODE_COMP;
   SQL_LIB.SET_MARK('FETCH', 
                    'C_LOCK_NON_MERCH_CODE_COMP', 
                    'non_merch_code_comp', 
                    NULL);
   fetch C_LOCK_NON_MERCH_CODE_COMP into L_non_merch_code_found;
   if C_LOCK_NON_MERCH_CODE_COMP%FOUND then
      delete non_merch_code_comp
       where non_merch_code = I_non_merch_code;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 
                    'C_LOCK_NON_MERCH_CODE_COMP', 
                    'non_merch_code_comp', 
                    NULL);
   close C_LOCK_NON_MERCH_CODE_COMP;

   return TRUE;
EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DEL_NON_MERCH_CODE_COMP;
--------------------------------------------------------------------------------
END NON_MERCH_CODE_SQL;
/
