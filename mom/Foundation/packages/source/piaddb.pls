CREATE OR REPLACE PACKAGE BODY PACKITEM_ADD_SQL AS

L_import_country_id  ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
LP_item_cost_tbl OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();

------------------------------------------------------------------------
FUNCTION VALID_ITEM (O_error_message  IN OUT VARCHAR2,
                     O_valid          IN OUT BOOLEAN,
                     O_item_desc      IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                     I_pack_no        IN     PACKITEM.PACK_NO%TYPE,
                     I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is
   ---
   L_program  VARCHAR2(64)  := 'PACKITEM_ADD_SQL.VALID_ITEM';
   ---
   L_dept_level_orders       PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE;
   L_supplier                ITEM_SUPPLIER.SUPPLIER%TYPE           := NULL;
   L_origin_country_id       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_cursor_item             ITEM_MASTER.ITEM%TYPE;
   L_exists                  VARCHAR2(1);
   L_exists_in_pack          BOOLEAN;
   ---
   L_pack_dept               ITEM_MASTER.DEPT%TYPE                 := NULL;
   L_pack_pack_ind           ITEM_MASTER.PACK_IND%TYPE             := NULL;
   L_pack_pack_type          ITEM_MASTER.PACK_TYPE%TYPE            := NULL;
   L_pack_order_as_type      ITEM_MASTER.ORDER_AS_TYPE%TYPE        := NULL;
   L_pack_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_contains_inner_ind ITEM_MASTER.CONTAINS_INNER_IND%TYPE;
   L_pack_item_desc          ITEM_MASTER.ITEM_DESC%TYPE;
   L_pack_purchase_type      DEPS.PURCHASE_TYPE%TYPE;
   ---
   L_item_dept               ITEM_MASTER.DEPT%TYPE                 := NULL;
   L_item_pack_ind           ITEM_MASTER.PACK_IND%TYPE             := NULL;
   L_item_pack_type          ITEM_MASTER.PACK_TYPE%TYPE            := NULL;
   L_item_order_as_type      ITEM_MASTER.ORDER_AS_TYPE%TYPE        := NULL;
   L_item_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_item_contains_inner_ind ITEM_MASTER.CONTAINS_INNER_IND%TYPE;
   L_item_purchase_type      DEPS.PURCHASE_TYPE%TYPE;
   L_count                   NUMBER;
   L_seq_exists              VARCHAR2(1);
   L_system_options_rec      SYSTEM_OPTIONS%ROWTYPE;
   ---
   cursor C_PROCUR_UNIT_OPTIONS is
      select dept_level_orders
        from procurement_unit_options;
   ---
   cursor C_ITEM_ATTRIB is
      select im.dept,
             im.pack_ind,
             im.pack_type,
             im.order_as_type,
             im.orderable_ind,
             im.contains_inner_ind,
             im.item_desc,
             d.purchase_type
        from item_master im,
             deps d
       where im.item = L_cursor_item
         and im.dept = d.dept;
   ---
   cursor C_ITEM_SUPP_COUNTRY is
      select isc.supplier,
             isc.origin_country_id
        from item_supp_country isc
       where isc.item = I_pack_no;
   ---
   cursor C_CHECK_COMPONENT_ITEMS is
      select 'x'
        from packitem pi
       where pi.pack_no = I_item --- inner pack
         and not exists( select 'x'
                           from item_supp_country isc
                          where isc.item              = pi.item
                            and isc.supplier          = L_supplier
                            and isc.origin_country_id = L_origin_country_id );
   ---
   cursor C_CHECK_COMPONENT_ITEMSUPPCTRY is
      select 'x'
        from item_supp_country isc
       where isc.item              = I_item
         and isc.supplier          = L_supplier
         and isc.origin_country_id = L_origin_country_id;
   ---
   cursor GET_INVALID_INNER_PACK_DEPT is
      select 'x'
        from packitem pi,
             item_master im
       where pi.pack_no = I_item
         and pi.item    = im.item
         and im.dept   != L_pack_dept;
   ---
   cursor CHECK_SEQ_NO_EXISTS is
      select 'x'
        from packitem
       where pack_no = I_pack_no;
   ---
   cursor CHECK_ITEM_COUNTRY is
      select count(*) from
      (select distinct country_id
        from item_country
       where item = I_pack_no
      INTERSECT
      select distinct country_id
        from item_country
       where item = (select item
                       from packitem
                      where pack_no = I_pack_no
                        and rownum = 1)
          or item = (select item_parent
                       from packitem
                      where pack_no = I_pack_no
                        and rownum = 1)
      INTERSECT
      select distinct country_id
              from item_country
       where item = I_item);
   ---
   cursor CHECK_ITEM_COUNTRY_INIT is
      select count(*) from
        (select distinct country_id
        from item_country
       where item = I_pack_no
      INTERSECT
      select distinct country_id
              from item_country
       where item = I_item);

BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;

   -- In GTAX environment atleast one delivery country should be common among
   -- all the components and the pack.

   if L_system_options_rec.default_tax_type = 'GTAX' then
      SQL_LIB.SET_MARK('OPEN',
                       'CHECK_SEQ_NO_EXISTS',
                       'PACK_ITEM',
                        NULL);
      open CHECK_SEQ_NO_EXISTS;

      SQL_LIB.SET_MARK('FETCH',
                       'CHECK_SEQ_NO_EXISTS',
                       'PACK_ITEM',
                        NULL);
      fetch CHECK_SEQ_NO_EXISTS into L_seq_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'CHECK_ITEM_COUNTRY',
                       'PACK_ITEM',
                        NULL);
      close CHECK_SEQ_NO_EXISTS;

      if L_seq_exists is not NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'CHECK_ITEM_COUNTRY',
                          'ITEM_COUNTRY',
                          NULL);
         open CHECK_ITEM_COUNTRY;

         SQL_LIB.SET_MARK('FETCH',
                          'CHECK_ITEM_COUNTRY',
                          'ITEM_COUNTRY',
                          NULL);
         fetch CHECK_ITEM_COUNTRY into L_count;

         SQL_LIB.SET_MARK('CLOSE',
                          'CHECK_ITEM_COUNTRY',
                          'ITEM_COUNTRY',
                          NULL);
         close CHECK_ITEM_COUNTRY;
      else
         SQL_LIB.SET_MARK('OPEN',
                          'CHECK_ITEM_COUNTRY_INIT',
                          'ITEM_COUNTRY',
                          NULL);
         open CHECK_ITEM_COUNTRY_INIT;

         SQL_LIB.SET_MARK('FETCH',
                          'CHECK_ITEM_COUNTRY_INIT',
                          'ITEM_COUNTRY',
                          NULL);
         fetch CHECK_ITEM_COUNTRY_INIT into L_count;

         SQL_LIB.SET_MARK('CLOSE',
                          'CHECK_ITEM_COUNTRY_INIT',
                          'ITEM_COUNTRY',
                          NULL);
         close CHECK_ITEM_COUNTRY_INIT;
      end  if;

      if L_count <= 0 then
         O_error_message := SQL_LIB.CREATE_MSG('COMP_NO_COUNTRY',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   --- Validation fails if the specified item is already in the pack.
   if PACKITEM_ADD_SQL.EXISTS_IN_PACK(O_error_message,
                                      L_exists_in_pack,
                                      I_pack_no,
                                      I_item ) = FALSE then
      return FALSE;
   end if;
   ---
   if L_exists_in_pack = TRUE then
      --- The component item already exists in the pack.
      O_error_message := SQL_LIB.CREATE_MSG('EXISTS_IN_PACK',
                                            I_item,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   --- Get the department level orders.
   SQL_LIB.SET_MARK('OPEN',
                    'C_PROCUR_UNIT_OPTIONS',
                    'PROCUREMENT_UNIT_OPTIONS',
                    NULL);
   open C_PROCUR_UNIT_OPTIONS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_PROCUR_UNIT_OPTIONS',
                    'PROCUREMENT_UNIT_OPTIONS',
                    NULL);
   fetch C_PROCUR_UNIT_OPTIONS into L_dept_level_orders;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_PROCUR_UNIT_OPTIONS',
                    'PROCUREMENT_UNIT_OPTIONS',
                    NULL);
   close C_PROCUR_UNIT_OPTIONS;
   ---
   --- Get the item attributes for the specified pack.
   L_cursor_item := I_pack_no;
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_ATTRIB',
                    'ITEM_MASTER, DEPS',
                    'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
   open C_ITEM_ATTRIB;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_ATTRIB',
                    'ITEM_MASTER, DEPS',
                    'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
   fetch C_ITEM_ATTRIB into L_pack_dept,
                            L_pack_pack_ind,
                            L_pack_pack_type,
                            L_pack_order_as_type,
                            L_pack_orderable_ind,
                            L_pack_contains_inner_ind,
                            L_pack_item_desc,
                            L_pack_purchase_type;
   if C_ITEM_ATTRIB%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_ATTRIB',
                       'ITEM_MASTER, DEPS',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
      close C_ITEM_ATTRIB;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_ATTRIB',
                       'ITEM_MASTER, DEPS',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
      close C_ITEM_ATTRIB;
   end if;
   ---
   --- Get the item attributes for the specified potential component item.
   L_cursor_item := I_item;
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_ATTRIB',
                    'ITEM_MASTER, DEPS',
                    'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
   open C_ITEM_ATTRIB;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_ATTRIB',
                    'ITEM_MASTER, DEPS',
                    'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
   fetch C_ITEM_ATTRIB into L_item_dept,
                            L_item_pack_ind,
                            L_item_pack_type,
                            L_item_order_as_type,
                            L_item_orderable_ind,
                            L_item_contains_inner_ind,
                            O_item_desc,
                            L_item_purchase_type;
   if C_ITEM_ATTRIB%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_ATTRIB',
                       'ITEM_MASTER, DEPS',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
      close C_ITEM_ATTRIB;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_ATTRIB',
                       'ITEM_MASTER, DEPS',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
      close C_ITEM_ATTRIB;
   end if;
   ---
   --- Validation fails if the specified item is itself a pack that contains
   --- other packs.
   if L_item_pack_ind = 'Y' and L_item_contains_inner_ind = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('PACK_WITH_INNER_NOT_ALLOW',
                                            I_item,
                                            I_pack_no,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   --- Validation fails if the specified pack is a buyer pack and the specified
   --- item is both a pack and a vender pack.
   if L_pack_pack_type = 'B' and L_item_pack_ind = 'Y' and L_item_pack_type = 'V' then
      O_error_message := SQL_LIB.CREATE_MSG('VEND_INNR_NO_ADD_TO_BUYER',
                                            I_item,
                                            I_pack_no,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   --- Validation fails if the specified item is in a consignment department.
   if L_item_purchase_type = 1 then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_CONSIGN_NOT_IN_PACK',
                                            I_item,
                                            I_pack_no,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   --- Validation fails if the specified pack is orderable, its department level orders is
   --- yes, it orders as eaches, its department is not the same as the specified item's
   --- department, and either the specified item is not a pack or it is orderable.
   if L_pack_orderable_ind = 'Y' and L_dept_level_orders = 'Y' and L_pack_order_as_type = 'E' and
         L_pack_dept != L_item_dept and (L_item_pack_ind = 'N' or L_item_orderable_ind = 'Y') then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_NOT_SAME_DEPT_AS_PAC',
                                            I_item,
                                            L_pack_dept,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   if L_pack_orderable_ind = 'Y' then
      --- Validation fails if the department level orders is yes, the specified pack
      --- orders as eaches, and any component item within the specified item (inner
      --- pack) does not have the same department as the specified pack.
      if L_item_pack_ind = 'Y' and L_item_orderable_ind = 'N' and L_dept_level_orders = 'Y' and
            L_pack_order_as_type = 'E' then
         SQL_LIB.SET_MARK('OPEN',
                          'GET_INVALID_INNER_PACK_DEPT',
                          'PACKITEM, ITEM_MASTER',
                          'INNER PACK NO: '||I_item||', DEPT: '||L_pack_dept);
         open GET_INVALID_INNER_PACK_DEPT;
         SQL_LIB.SET_MARK('FETCH',
                          'GET_INVALID_INNER_PACK_DEPT',
                          'PACKITEM, ITEM_MASTER',
                          'INNER PACK NO: '||I_item||', DEPT: '||L_pack_dept);
         fetch GET_INVALID_INNER_PACK_DEPT into L_exists;
         if GET_INVALID_INNER_PACK_DEPT%FOUND then
            SQL_LIB.SET_MARK('CLOSE',
                             'GET_INVALID_INNER_PACK_DEPT',
                             'PACKITEM, ITEM_MASTER',
                             'INNER PACK NO: '||I_item||', DEPT: '||L_pack_dept);
            close GET_INVALID_INNER_PACK_DEPT;
            O_error_message := SQL_LIB.CREATE_MSG('INNR_ITEMS_NOT_SAME_DEPT',
                                                  I_item,
                                                  L_pack_dept,
                                                  I_pack_no);
            O_valid := FALSE;
            return TRUE;
         else
            SQL_LIB.SET_MARK('CLOSE',
                             'GET_INVALID_INNER_PACK_DEPT',
                             'PACKITEM, ITEM_MASTER',
                             'INNER PACK NO: '||I_item||', DEPT: '||L_pack_dept);
            close GET_INVALID_INNER_PACK_DEPT;
         end if;
      end if;
      ---
      FOR rec IN C_ITEM_SUPP_COUNTRY LOOP
         L_supplier          := rec.supplier;
         L_origin_country_id := rec.origin_country_id;
         ---
         if L_item_pack_ind = 'Y' and L_item_orderable_ind = 'N' then
            --- Validation fails if any of the component items of the specified item (inner
            --- pack) do not have the current supplier-origin country relationship for the
            --- specified pack.
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_COMPONENT_ITEMS',
                             'PACKITEM, ITEM_SUPP_COUNTRY',
                             'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
            open C_CHECK_COMPONENT_ITEMS;
            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_COMPONENT_ITEMS',
                             'PACKITEM, ITEM_SUPP_COUNTRY',
                             'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
            fetch C_CHECK_COMPONENT_ITEMS into L_exists;
            if C_CHECK_COMPONENT_ITEMS%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMPONENT_ITEMS',
                                'PACKITEM, ITEM_SUPP_COUNTRY',
                                'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
               close C_CHECK_COMPONENT_ITEMS;
               O_error_message := SQL_LIB.CREATE_MSG('INNR_ITEMS_NO_ITEMSUPCTRY',
                                                     I_item,
                                                     L_supplier||'-'||L_origin_country_id,
                                                     I_pack_no);
               O_valid := FALSE;
               return TRUE;
            else
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMPONENT_ITEMS',
                                'PACKITEM, ITEM_SUPP_COUNTRY',
                                'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
               close C_CHECK_COMPONENT_ITEMS;
            end if;
         else
            --- Validation fails if the specified item does not have the current supplier-origin
            --- country relationship for the specified pack.
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_COMPONENT_ITEMSUPPCTRY',
                             'ITEM_SUPP_COUNTRY',
                             'ITEM: '||I_item);
            open C_CHECK_COMPONENT_ITEMSUPPCTRY;
            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_COMPONENT_ITEMSUPPCTRY',
                             'ITEM_SUPP_COUNTRY',
                             'ITEM: '||I_item);
            fetch C_CHECK_COMPONENT_ITEMSUPPCTRY into L_exists;
            if C_CHECK_COMPONENT_ITEMSUPPCTRY%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMPONENT_ITEMSUPPCTRY',
                                'ITEM_SUPP_COUNTRY',
                                'ITEM: '||I_item);
               close C_CHECK_COMPONENT_ITEMSUPPCTRY;
               O_error_message := SQL_LIB.CREATE_MSG('COMP_ITEM_NO_ITEMSUPCTRY',
                                                     I_item,
                                                     L_supplier||'-'||L_origin_country_id,
                                                     I_pack_no);
               O_valid := FALSE;
               return TRUE;
            else
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMPONENT_ITEMSUPPCTRY',
                                'ITEM_SUPP_COUNTRY',
                                'ITEM: '||I_item);
               close C_CHECK_COMPONENT_ITEMSUPPCTRY;
            end if;
         end if;
      end LOOP;
   end if;
   ---
   O_valid := TRUE;
   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END VALID_ITEM;
---------------------------------------------------
FUNCTION UPDATE_SUPP_COST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_pack_no       IN     PACKITEM.PACK_NO%TYPE)
   return BOOLEAN IS
   ---
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_unit_cost          ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_total_exp          ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE;
   L_exp_currency       CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_exp       CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty          ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE;
   L_dty_currency       CURRENCIES.CURRENCY_CODE%TYPE;
   L_program            VARCHAR2(64) := 'PACKITEM_ADD_SQL.UPDATE_SUPP_COST';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   L_exists             NUMBER;
   L_country_attrib_row     COUNTRY_ATTRIB%ROWTYPE;
   L_base_cost              ITEM_SUPP_COUNTRY.BASE_COST%TYPE := NULL;
   L_extended_base_cost     ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE := NULL;
   L_inclusive_cost         ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE := NULL;
   L_negotiated_item_cost   ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE := NULL;

   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   ---
   cursor C_LOCK_RECORD is
      select 'x'
        from item_supp_country_loc isc
       where item = I_pack_no
         for update nowait;
   ---
   cursor C_GET_SUPP_COUNTRY_LOC is
      select supplier,
             origin_country_id,
             loc
        from item_supp_country_loc isc
       where item = I_pack_no;
   ---
   cursor C_LOCK_RECORD_COUNTRY is
      select 'x'
        from item_supp_country
       where item = I_pack_no
         for update nowait;
   ---
   cursor C_GET_SUPP_COUNTRY is
      select supplier,
             origin_country_id,
             unit_cost
        from item_supp_country
       where item = I_pack_no;
   ---
   cursor C_ITEM_EXISTS is
      select 1
        from item_loc
       where item = I_pack_no;
   ---
   cursor C_CHECK_INNER_PACK is
      select pack_no
        from packitem
       where item = I_pack_no;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_EXISTS',
                    'ITEM_LOC',
                    'ITEM_NO: '||I_pack_no);
   open C_ITEM_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_EXISTS',
                    'ITEM_LOC',
                    'ITEM_NO: '||I_pack_no);
   fetch C_ITEM_EXISTS into L_exists;
   ---
   if C_ITEM_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_EXISTS',
                       'ITEM_LOC',
                       'ITEM_NO: '||I_pack_no);
      close C_ITEM_EXISTS;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECORD',
                       'ITEM_SUPP_COUNTRY_LOC',
                       'ITEM: '||I_pack_no);
      open C_LOCK_RECORD;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECORD',
                       'ITEM_SUPP_COUNTRY_LOC',
                       'ITEM: '||I_pack_no);
      close C_LOCK_RECORD;
      ---
      /* Update item-supplier-origin-loc country records */
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_SUPP_COUNTRY_LOC',
                       'ITEM: '||I_pack_no);
        
      merge into item_supp_country_loc iscl
        using (select sum(isc2.unit_cost * vpq.qty) unit_cost,
                      sum(isc2.negotiated_item_cost * vpq.qty) negotiated_item_cost,
                      sum(isc2.extended_base_cost * vpq.qty) extended_base_cost,
                      sum(isc2.inclusive_cost * vpq.qty) inclusive_cost,
                      sum(isc2.base_cost * vpq.qty) base_cost,
                      vpq.pack_no,
                      isc2.supplier,
                      isc2.origin_country_id,
                      isc2.loc
                 from item_supp_country_loc isc2,
                      v_packsku_qty vpq
                where vpq.pack_no            = I_pack_no
                  and isc2.item              = vpq.item
             group by vpq.pack_no,
                      isc2.supplier,
                      isc2.origin_country_id,
                      isc2.loc ) m1
         on (iscl.item = m1.pack_no
        and iscl.supplier = m1.supplier
        and iscl.origin_country_id = m1.origin_country_id
        and iscl.loc = m1.loc)
       when matched then update
        set iscl.unit_cost            = NVL(m1.unit_cost,iscl.unit_cost),
            iscl.negotiated_item_cost = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(m1.negotiated_item_cost,iscl.negotiated_item_cost), NULL),
            iscl.extended_base_cost   = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(m1.extended_base_cost,iscl.extended_base_cost), NULL),
            iscl.inclusive_cost       = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(m1.inclusive_cost,iscl.inclusive_cost), NULL),
            iscl.base_cost            = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(m1.base_cost,iscl.base_cost), NULL),
            iscl.last_update_id       = get_user,
            iscl.last_update_datetime = sysdate;
      ------
      /* Update item-supplier-origin country-location and related item-location records */
      for rec in C_GET_SUPP_COUNTRY_LOC LOOP
         if UPDATE_BASE_COST.CHANGE_COST(O_error_message,
                                         I_pack_no, --- item
                                         rec.supplier,
                                         rec.origin_country_id,
                                         rec.loc,
                                         'N', /* PROCESS CHILDREN */
                                         'N', /* Edit child cost */
                                         NULL /* Cost Change Number */) = FALSE then
            return FALSE;
         end if;
      END LOOP;
      ---
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_EXISTS',
                       'ITEM_LOC',
                       'ITEM_NO: '||I_pack_no);
      close C_ITEM_EXISTS;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_RECORD_COUNTRY',
                       'ITEM_SUPP_COUNTRY',
                       'ITEM: '||I_pack_no);
      open C_LOCK_RECORD;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_RECORD_COUNTRY',
                       'ITEM_SUPP_COUNTRY',
                       'ITEM: '||I_pack_no);
      close C_LOCK_RECORD;
      ---
      /* Update item-supplier-origin country records */
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_SUPP_COUNTRY',
                       'ITEM: '||I_pack_no);
      merge into item_supp_country isc1
        using (select sum(isc2.unit_cost * vpq.qty) unit_cost,
                      sum(isc2.negotiated_item_cost * vpq.qty) negotiated_item_cost,
                      sum(isc2.extended_base_cost * vpq.qty) extended_base_cost,
                      sum(isc2.inclusive_cost * vpq.qty) inclusive_cost,
                      sum(isc2.base_cost * vpq.qty) base_cost,
                      vpq.pack_no,
                      isc2.supplier,
                      isc2.origin_country_id
                 from item_supp_country isc2,
                      v_packsku_qty vpq
                where vpq.pack_no            = I_pack_no
                  and isc2.item              = vpq.item
             group by vpq.pack_no,
                      isc2.supplier,
                      isc2.origin_country_id) m1
         on (isc1.item = m1.pack_no
        and isc1.supplier = m1.supplier
        and isc1.origin_country_id = m1.origin_country_id)
       when matched then update
        set isc1.unit_cost            = NVL(m1.unit_cost,isc1.unit_cost),
            isc1.negotiated_item_cost = NVL(m1.negotiated_item_cost,isc1.negotiated_item_cost),
            isc1.extended_base_cost   = NVL(m1.extended_base_cost,isc1.extended_base_cost),
            isc1.inclusive_cost       = NVL(m1.inclusive_cost,isc1.inclusive_cost),
            isc1.base_cost            = NVL(m1.base_cost,isc1.base_cost),
            isc1.last_update_id       = get_user,
            isc1.last_update_datetime = sysdate;

            ---
      if L_system_options_rec.default_tax_type in ('SVAT', 'SALES') then
         update item_supp_country isc1
            set isc1.negotiated_item_cost = isc1.unit_cost,
                isc1.extended_base_cost   = isc1.unit_cost,
                isc1.inclusive_cost       = isc1.unit_cost,
                isc1.base_cost            = isc1.unit_cost,
                isc1.last_update_id       = get_user,
                isc1.last_update_datetime = sysdate
          where isc1.item = I_pack_no;
      end if;
   end if;
   ---
   if L_system_options_rec.default_tax_type = 'GTAX' then
      for rec in C_GET_SUPP_COUNTRY LOOP
         if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                    L_country_attrib_row,
                                                    NULL,
                                                    L_system_options_rec.base_country_id) = FALSE then
            return FALSE;
         end if;

         if L_country_attrib_row.item_cost_tax_incl_ind = 'N' then
            L_base_cost := rec.unit_cost;
         else
            L_negotiated_item_cost := rec.unit_cost;
         end if;
         if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                            L_base_cost,
                                            L_extended_base_cost,
                                            L_inclusive_cost,
                                            L_negotiated_item_cost,
                                            I_pack_no,
                                            L_country_attrib_row.item_cost_tax_incl_ind,
                                            rec.supplier,
                                            L_country_attrib_row.default_loc,
                                            L_country_attrib_row.default_loc_type,
                                            'ITEMSUPPCTRY',    -- I_calling_form
                                            rec.origin_country_id,
                                            L_system_options_rec.base_country_id,
                                            'Y',   --I_prim_dlvy_ctry_ind
                                            'Y') = FALSE then   --I_update_itemcost_ind
            return FALSE;
         end if;
         if ITEM_LOC_SQL.INSERT_ITEM_COST_LOC(O_error_message,
                                              I_pack_no,
                                              rec.supplier,
                                              rec.origin_country_id,
                                              'N', --I_update_itemcost_child_ind
                                              'N') = FALSE then --I_update_loc_ind
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   /* Call UPDATE_SUPP_COST if the current pack is an inner pack */
   for rec in C_CHECK_INNER_PACK LOOP
      if UPDATE_SUPP_COST(O_error_message,
                          rec.pack_no) = FALSE then
         return FALSE;
      end if;
   end LOOP;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_COUNTRY_LOC',
                                            I_pack_no,
                                            NULL);
      return FALSE;
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END UPDATE_SUPP_COST;
---------------------------------------------------------------------------------------
FUNCTION PARENT_PACK_TMPL (O_error_message  IN OUT VARCHAR2,
                           O_item_rejected  IN OUT BOOLEAN,
                           I_pack_no        IN     ITEM_MASTER.ITEM%TYPE,
                           I_item_parent    IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                           I_pack_tmpl_id   IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                           I_qty            IN     PACKITEM.PACK_QTY%TYPE,
                           I_supplier       IN     SUPS.SUPPLIER%TYPE,
                           I_origin_country IN     COUNTRY.COUNTRY_ID%TYPE)
   return BOOLEAN IS

   L_program                VARCHAR2(64)                 := 'PACKITEM_ADD_SQL.PARENT_PACK_TMPL';
   L_exists                 VARCHAR2(1)                  := 'N';
   L_exists_status          ITEM_MASTER.STATUS%TYPE      := NULL;
   L_diff1                  PACK_TMPL_DETAIL.DIFF_1%TYPE;
   L_diff2                  PACK_TMPL_DETAIL.DIFF_2%TYPE;
   L_diff3                  PACK_TMPL_DETAIL.DIFF_3%TYPE;
   L_diff4                  PACK_TMPL_DETAIL.DIFF_4%TYPE;
   L_diff1_desc             DIFF_IDS.DIFF_DESC%TYPE;
   L_diff2_desc             DIFF_IDS.DIFF_DESC%TYPE;
   L_diff3_desc             DIFF_IDS.DIFF_DESC%TYPE;
   L_diff4_desc             DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_type              DIFF_IDS.DIFF_TYPE%TYPE;
   L_diff_id_group          VARCHAR2(5);
   L_item                   ITEM_MASTER.ITEM%TYPE;
   L_qty                    PACK_TMPL_DETAIL.QTY%TYPE;
   L_item_supp              VARCHAR2(1)                  := 'N';
   L_parent_desc            ITEM_MASTER.ITEM_DESC%TYPE;
   L_max_seq_no             PACKITEM.SEQ_NO%TYPE;
   L_supplier               SUPS.SUPPLIER%TYPE;
   L_origin_country_id      COUNTRY.COUNTRY_ID%TYPE;
   L_item_level             ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level             ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_status                 ITEM_MASTER.STATUS%TYPE;
   L_item_num_type          ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE;
   L_user                   VARCHAR2(30) := get_user;
   L_sysdate                DATE := sysdate;
   L_diff_no                NUMBER(1);
   L_parent_desc_length     NUMBER;
   L_diff_desc_length       NUMBER;

   cursor C_PACK_TMPL is
      select diff_1,
             diff_2,
             diff_3,
             diff_4,
             qty
        from pack_tmpl_detail
       where pack_tmpl_id = I_pack_tmpl_id;

   cursor C_MAX_SEQ_NO is
      select nvl(max(seq_no) + 1,1)
        from packitem_breakout
       where pack_no = I_pack_no;

   cursor C_CHECK_ITEM is
      select status
        from item_master
       where item_parent = I_item_parent
         and diff_1      = L_diff1
         and ((diff_2    = L_diff2) or (diff_2 is NULL and L_diff2 is NULL))
         and ((diff_3    = L_diff3) or (diff_3 is NULL and L_diff3 is NULL))
         and ((diff_4    = L_diff4) or (diff_4 is NULL and L_diff4 is NULL));

   cursor C_CHECK_ITEM_SUPP is
      select 'x'
        from item_supp_country isc
       where isc.item = L_item
         and exists (select 'x'
                       from item_supp_country isc2
                      where isc2.item              = I_item_parent
                        and isc2.supplier          = isc.supplier
                        and isc2.origin_country_id = isc.origin_country_id);

   cursor C_GET_ITEM_NUM_TYPE is
      select DISTINCT(item_number_type)
        from item_master
       where item_parent = I_item_parent
         and item_level = L_tran_level;

   cursor C_CHILD_ITEMS is
      select item
        from item_master
       where item_parent = I_item_parent
         and diff_1      = L_diff1
         and ((diff_2    = L_diff2) or (diff_2 is NULL and L_diff2 is NULL))
         and ((diff_3    = L_diff3) or (diff_3 is NULL and L_diff3 is NULL))
         and ((diff_4    = L_diff4) or (diff_4 is NULL and L_diff4 is NULL));

BEGIN

   open C_MAX_SEQ_NO;
   fetch C_MAX_SEQ_NO into L_max_seq_no;
   close C_MAX_SEQ_NO;
   ---
   FOR rec IN C_PACK_TMPL LOOP
      L_diff1 := rec.diff_1;
      L_diff2 := rec.diff_2;
      L_diff3 := rec.diff_3;
      L_diff4 := rec.diff_4;
      L_qty := rec.qty;

      --- Reset Variables
      L_exists_status := NULL;
      L_item_num_type := NULL;
      L_item_supp     := NULL;
      L_item          := NULL;

      if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                  L_parent_desc,
                                  L_status,
                                  L_item_level,
                                  L_tran_level,
                                  I_item_parent) = FALSE then
         return FALSE;
      end if;
      ---
      if L_diff4 is NOT NULL then
         L_diff_no := 4;
      elsif L_diff3 is NOT NULL then
         L_diff_no := 3;
      elsif L_diff2 is NOT NULL then
         L_diff_no := 2;
      else
         L_diff_no := 1;
      end if;

      if L_item_level != (L_tran_level - 1) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_GRAND_ON_PACK_TMPL',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;

      if lengthb(L_parent_desc) < 150 then
         L_parent_desc_length := lengthb(L_parent_desc);
         L_diff_desc_length := trunc( ( (250 - L_diff_no) - L_parent_desc_length) / L_diff_no );
      else
         if L_diff_no = 4 then
            L_parent_desc_length := 134;
            L_diff_desc_length   := 28;
         elsif L_diff_no = 3 then
            L_parent_desc_length := 142;
            L_diff_desc_length   := 35;
         elsif L_diff_no = 2 then
            L_parent_desc_length := 148;
            L_diff_desc_length   := 50;
         else
            L_parent_desc_length := 174;
            L_diff_desc_length   := 75;
         end if;
      end if;

      L_parent_desc := RTRIM(SUBSTRB(L_parent_desc,1,L_parent_desc_length));
      ---
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                L_diff1_desc,
                                L_diff_type,
                                L_diff_id_group,
                                L_diff1) = FALSE then
         return FALSE;
      end if;
      L_parent_desc := L_parent_desc || ':' || RTRIM(SUBSTRB(L_diff1_desc,1,L_diff_desc_length));
      ---
      if L_diff2 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff2_desc,
                                   L_diff_type,
                                   L_diff_id_group,
                                   L_diff2) = FALSE then
            return FALSE;
         end if;
         L_parent_desc := L_parent_desc || ':' || RTRIM(SUBSTRB(L_diff2_desc,1,L_diff_desc_length));
      end if;
      ---
      if L_diff3 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff3_desc,
                                   L_diff_type,
                                   L_diff_id_group,
                                   L_diff3) = FALSE then
            return FALSE;
         end if;
         L_parent_desc := L_parent_desc || ':' || RTRIM(SUBSTRB(L_diff3_desc,1,L_diff_desc_length));
      end if;
      ---
      if L_diff4 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff4_desc,
                                   L_diff_type,
                                   L_diff_id_group,
                                   L_diff4) = FALSE then
            return FALSE;
         end if;
         L_parent_desc := L_parent_desc || ':' || RTRIM(SUBSTRB(L_diff4_desc,1,L_diff_desc_length));
      end if;

      -- check if Item Parent/Diff combo is already present
      -- if not then generate a new item number and insert into
      -- the ITEM_TEMP table.
      open C_CHECK_ITEM;
      fetch C_CHECK_ITEM into L_exists_status;
      close C_CHECK_ITEM;
      ---
      if L_exists_status is NULL then
         ---
         open C_GET_ITEM_NUM_TYPE;
         fetch C_GET_ITEM_NUM_TYPE into L_item_num_type;
         close C_GET_ITEM_NUM_TYPE;
         ---
         if L_item_num_type is NULL then
            L_item_num_type := 'ITEM';
         end if;

         --- validate that the item type for items being created is an internal number
         if L_item_num_type not in ('ITEM', 'UPC-A', 'UPC-AS') then
            O_error_message := SQL_LIB.CREATE_MSG('INTERNAL_NUMBER',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return FALSE;
         end if;
         ---
         if ITEM_NUMBER_TYPE_SQL.GET_NEXT(O_error_message,
                                          L_item,
                                          L_item_num_type) = FALSE then
            return FALSE;
         end if;
         ---
         insert into item_temp(item,
                               item_number_type,
                               item_level,
                               item_desc,
                               diff_1,
                               diff_2,
                               diff_3,
                               diff_4,
                               existing_item_parent)
                       values (L_item,
                               L_item_num_type,
                               L_tran_level,
                               L_parent_desc,
                               L_diff1,
                               L_diff2,
                               L_diff3,
                               L_diff4,
                               I_item_parent);
         ---
         if ITEM_CREATE_SQL.TRAN_CHILDREN(O_error_message,
                                          'Y',
                                          'Y') = FALSE then
            return FALSE;
         end if;
         ---
         delete from item_temp
          where item = L_item;
         ---
         L_exists_status := 'A';
         O_item_rejected := TRUE;
      end if;
      ---

      ---
      --- if the item is not an approved item it should not be inserted as a pack component item.
      if L_exists_status = 'A' then
         -- Get the item number for the item being inserted as a component.
         open C_CHILD_ITEMS;
         fetch C_CHILD_ITEMS into L_item;
         close C_CHILD_ITEMS;
         if L_item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INV_SKU',
                                                   L_program,
                                                   NULL,
                                                   NULL);
            return FALSE;
         end if;

         --- Check to ensure the item being inserted as a pack component
         --- is supplied by the same supplier/country as the pack item.
         L_item_supp := NULL;
         open C_CHECK_ITEM_SUPP;
         fetch C_CHECK_ITEM_SUPP into L_item_supp;
         close C_CHECK_ITEM_SUPP;
         ---
         if L_item_supp is NOT NULL then
            insert into packitem_breakout(pack_no,
                                          seq_no,
                                          item,
                                          item_parent,
                                          pack_tmpl_id,
                                          comp_pack_no,
                                          item_qty,
                                          item_parent_pt_qty,
                                          comp_pack_qty,
                                          pack_item_qty,
                                          create_datetime,
                                          last_update_datetime,
                                          last_update_id)
              values (I_pack_no,
                      L_max_seq_no,
                      L_item,
                      I_item_parent,
                      I_pack_tmpl_id,
                      NULL,
                      L_qty,
                      I_qty,
                      NULL,
                      L_qty * I_qty,
                      L_sysdate,
                      L_sysdate,
                      L_user);
            ---
            L_max_seq_no := L_max_seq_no + 1;
         end if;
      end if;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END PARENT_PACK_TMPL;
-----------------------------------------------------------------------------------
FUNCTION INNER_PACK (O_error_message IN OUT VARCHAR2,
                     I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                     I_inner_pack    IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                     I_qty           IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE)
   return BOOLEAN IS
   ---
   L_program            VARCHAR2(64)  := 'PACKITEM_ADD_SQL.INNER_PACK';
   L_max_seq_no         PACKITEM_BREAKOUT.SEQ_NO%TYPE;
   L_item               PACKITEM_BREAKOUT.ITEM%TYPE;
   L_item_parent        PACKITEM_BREAKOUT.ITEM_PARENT%TYPE;
   L_pack_tmpl_id       PACKITEM_BREAKOUT.PACK_TMPL_ID%TYPE;
   L_item_qty           PACKITEM_BREAKOUT.ITEM_QTY%TYPE;
   L_item_parent_pt_qty PACKITEM_BREAKOUT.ITEM_PARENT_PT_QTY%TYPE;
   L_pack_item_qty      PACKITEM_BREAKOUT.PACK_ITEM_QTY%TYPE;
   ---
   cursor C_PACKITEM_BREAKOUT is
      select item,
             item_parent,
             pack_tmpl_id,
             item_qty,
             item_parent_pt_qty,
             pack_item_qty
        from packitem_breakout
       where pack_no = I_inner_pack;
   ---
   cursor C_MAX_SEQ_NO is
      select nvl(max(seq_no) + 1,1)
        from packitem_breakout
       where pack_no = I_pack_no;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_inner_pack is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_INNER_PACK',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if nvl(I_qty, 0) <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_QTY',
                                            nvl(to_char(I_qty), 'NULL'),
                                            '> 0');
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN',
                    'C_MAX_SEQ_NO',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   open C_MAX_SEQ_NO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_MAX_SEQ_NO',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   fetch C_MAX_SEQ_NO into L_max_seq_no;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_MAX_SEQ_NO',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   close C_MAX_SEQ_NO;
   ---
   for rec in C_PACKITEM_BREAKOUT LOOP
      L_item               := rec.item;
      L_item_parent        := rec.item_parent;
      L_pack_tmpl_id       := rec.pack_tmpl_id;
      L_item_qty           := rec.item_qty;
      L_item_parent_pt_qty := rec.item_parent_pt_qty;
      L_pack_item_qty      := rec.pack_item_qty;
      ---
      SQL_LIB.SET_MARK('INSERT',
                       'PACKITEM_BREAKOUT',
                       'PACK_NO: '||I_pack_no||', INNER_PACK: '||I_inner_pack,
                       NULL);
      insert into PACKITEM_BREAKOUT( pack_no,
                                     seq_no,
                                     item,
                                     item_parent,
                                     pack_tmpl_id,
                                     comp_pack_no,
                                     item_qty,
                                     item_parent_pt_qty,
                                     comp_pack_qty,
                                     pack_item_qty,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id)
         values( I_pack_no,
                 L_max_seq_no,
                 L_item,
                 L_item_parent,
                 L_pack_tmpl_id,
                 I_inner_pack,
                 L_pack_item_qty,
                 L_item_parent_pt_qty,
                 I_qty,
                 nvl(L_pack_item_qty,1) * nvl(L_item_parent_pt_qty,1) * I_qty,
                 sysdate,
                 sysdate,
                 get_user);
      L_max_seq_no := L_max_seq_no + 1;
   END LOOP;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END INNER_PACK;
---------------------------------------------------------------------
FUNCTION DELETE_PACKITEM_BREAKOUT (O_error_message IN OUT VARCHAR2,
                                   I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                                   I_item          IN     PACKITEM_BREAKOUT.ITEM%TYPE,
                                   I_item_parent   IN     PACKITEM_BREAKOUT.ITEM_PARENT%TYPE,
                                   I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE)
   return BOOLEAN is
   ---
   L_program            VARCHAR2(64)    := 'PACKITEM_ADD_SQL.DELETE_PACKITEM_BREAKOUT';
   L_table              VARCHAR2(30)    := 'PACKITEM_BREAKOUT';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_RECORD is
      select 'x'
        from packitem_breakout
       where pack_no = I_pack_no
         for update nowait;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_IND',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECORD',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   open C_LOCK_RECORD;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECORD',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   close C_LOCK_RECORD;
   ---
   if I_pack_ind = 'Y' then
      SQL_LIB.SET_MARK('DELETE',
                      'PACKITEM_BREAKOUT',
                      'PACK_NO: '||I_pack_no||', ITEM: '||I_item,
                      NULL);
      delete from packitem_breakout
       where pack_no      = I_pack_no
         and comp_pack_no = I_item;
   else
      SQL_LIB.SET_MARK('DELETE',
                      'PACKITEM_BREAKOUT',
                      'PACK_NO: '||I_pack_no||', ITEM: '||I_item,
                      NULL);
      delete from packitem_breakout
       where comp_pack_no is NULL
         and pack_no       = I_pack_no
         and (item = I_item or item_parent = I_item_parent);
   end if;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_pack_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_PACKITEM_BREAKOUT;
---------------------------------------------------------------------------------
FUNCTION UPDATE_PACKITEM_BREAKOUT (O_error_message IN OUT VARCHAR2,
                                   I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                                   I_item          IN     PACKITEM_BREAKOUT.ITEM%TYPE,
                                   I_pack_tmpl_id  IN     PACKITEM_BREAKOUT.PACK_TMPL_ID%TYPE,
                                   I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                                   I_qty           IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE)
   return BOOLEAN is
   ---
   L_program            VARCHAR2(64)    := 'PACKITEM_ADD_SQL.UPDATE_PACKITEM_BREAKOUT';
   L_table              VARCHAR2(30)    := 'PACKITEM_BREAKOUT';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_RECORDS is
      select 'x'
        from PACKITEM_BREAKOUT
       where pack_no = I_pack_no
         for update nowait;
BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_IND',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if nvl(I_qty, 0) <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_QTY',
                                            nvl(to_char(I_qty), 'NULL'),
                                            '> 0');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RECORDS',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   open C_LOCK_RECORDS;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RECORDS',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   close C_LOCK_RECORDS;
   ---
   if I_pack_ind = 'Y' then
      SQL_LIB.SET_MARK('UPDATE',
                       'PACKITEM_BREAKOUT',
                       'PACK_NO: '||I_pack_no||', COMP_PACK_NO: '||I_item,
                       NULL);
      update packitem_breakout
         set comp_pack_qty        = I_qty,
             pack_item_qty        = nvl(item_qty,1) * nvl(item_parent_pt_qty,1) * nvl(I_qty,1),
             last_update_datetime = sysdate,
             last_update_id       = get_user
       where pack_no      = I_pack_no
         and comp_pack_no = I_item;
   else
      if I_pack_tmpl_id is not NULL then
         SQL_LIB.SET_MARK('UPDATE',
                          'PACKITEM_BREAKOUT',
                          'PACK_NO: '||I_pack_no||', ITEM_PARENT: '||I_item||
                          ', PACK_TMPL_ID: '||I_pack_tmpl_id,
                          NULL);
         update packitem_breakout
            set item_parent_pt_qty   = I_qty,
                pack_item_qty        = nvl(I_qty,1) * nvl(item_qty,1) * nvl(comp_pack_qty,1),
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where pack_no              = I_pack_no
            and item_parent          = I_item
            and pack_tmpl_id         = I_pack_tmpl_id;
      else
         SQL_LIB.SET_MARK('UPDATE',
                          'PACKITEM_BREAKOUT',
                          'PACK_NO: '||I_pack_no||', ITEM: '||I_item,
                          NULL);
         --- Update for the item in the pack and for when the item is
         --- a component of a pack that is an inner pack.
         update packitem_breakout
            set item_qty             = nvl(I_qty,1),
                pack_item_qty        = nvl(I_qty,1) * nvl(item_parent_pt_qty,1) * nvl(comp_pack_qty,1),
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where ((pack_no = I_pack_no and comp_pack_no is NULL)
             or (comp_pack_no = I_pack_no))
            and item = I_item;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_pack_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_PACKITEM_BREAKOUT;
--------------------------------------------------------------------------
FUNCTION INSERT_PACKITEM_BREAKOUT (O_error_message IN OUT VARCHAR2,
                                   I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                                   I_item          IN     PACKITEM_BREAKOUT.ITEM%TYPE,
                                   I_pack_tmpl_id  IN     PACKITEM_BREAKOUT.PACK_TMPL_ID%TYPE,
                                   I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                                   I_qty           IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE)
   return BOOLEAN IS
   ---
   L_program            VARCHAR2(64)  := 'PACKITEM_ADD_SQL.INSERT_PACKITEM_BREAKOUT';
   L_max_seq_no   PACKITEM_BREAKOUT.SEQ_NO%TYPE;
   L_item_rejected BOOLEAN;
   ---
   cursor C_MAX_SEQ_NO is
      select nvl(max(seq_no) + 1,1)
        from packitem_breakout
       where pack_no = I_pack_no;

   cursor C_LOC_NOT_EXIST is
      select il1.loc,
             il1.loc_type,
             il1.ranged_ind
        from item_loc il1
       where il1.item = I_pack_no
         and il1.loc not in (select il2.loc
                               from item_loc il2
                              where il2.item = I_item);

BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if nvl(I_qty, 0) <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_QTY',
                                            nvl(to_char(I_qty), 'NULL'),
                                            '> 0');
      return FALSE;
   end if;
   if I_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_IND',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_pack_ind = 'Y' then
      if INNER_PACK(O_error_message,
                    I_pack_no,
                    I_item,
                    I_qty) = FALSE then
         return FALSE;
      end if;
   else
      if I_pack_tmpl_id is NOT NULL then
         if PARENT_PACK_TMPL(O_error_message,
                             L_item_rejected,
                             I_pack_no,
                             I_item,
                             I_pack_tmpl_id,
                             I_qty,
                             NULL,
                             NULL) = FALSE then
            return FALSE;
         end if;
      else
         SQL_LIB.SET_MARK('OPEN',
                          'C_MAX_SEQ_NO',
                          'PACKITEM_BREAKOUT',
                          'PACK_NO: '||I_pack_no);
         open C_MAX_SEQ_NO;
         SQL_LIB.SET_MARK('FETCH',
                          'C_MAX_SEQ_NO',
                          'PACKITEM_BREAKOUT',
                          'PACK_NO: '||I_pack_no);
         fetch C_MAX_SEQ_NO into L_max_seq_no;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_MAX_SEQ_NO',
                          'PACKITEM_BREAKOUT',
                          'PACK_NO: '||I_pack_no);
         close C_MAX_SEQ_NO;
         ---
         SQL_LIB.SET_MARK('INSERT',
                          'PACKITEM_BREAKOUT',
                          'PACK_NO: '||I_pack_no||', ITEM: '||I_item,
                          NULL);
         insert into PACKITEM_BREAKOUT( pack_no,
                                        seq_no,
                                        item,
                                        item_parent,
                                        pack_tmpl_id,
                                        comp_pack_no,
                                        item_qty,
                                        item_parent_pt_qty,
                                        comp_pack_qty,
                                        pack_item_qty,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id)
            values( I_pack_no,
                    L_max_seq_no,
                    I_item,
                    NULL,
                    NULL,
                    NULL,
                    I_qty,
                    NULL,
                    NULL,
                    I_qty,
                    sysdate,
                    sysdate,
                    get_user);
      end if;

      for rec in C_LOC_NOT_EXIST LOOP
         if NEW_ITEM_LOC(O_error_message,
                         I_item,
                         rec.loc,
                         NULL,
                         NULL,
                         rec.loc_type,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         I_ranged_ind => rec.ranged_ind) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then

      if C_LOC_NOT_EXIST%ISOPEN then
         close C_LOC_NOT_EXIST;
      end if;

      if C_MAX_SEQ_NO%ISOPEN then
         close C_MAX_SEQ_NO;
      end if;


      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_PACKITEM_BREAKOUT;
----------------------------------------------------------------------------------------
FUNCTION EXISTS_IN_PACK (O_error_message IN OUT VARCHAR2,
                         O_exist         IN OUT BOOLEAN,
                         I_pack_no       IN     PACKITEM.PACK_NO%TYPE,
                         I_item          IN     PACKITEM.ITEM%TYPE)
   return BOOLEAN IS
   ---
   L_program    VARCHAR2(64)    := 'PACKITEM_ADD_SQL.EXISTS_IN_PACK';
   L_exists     VARCHAR2(1);
   ---
   cursor C_EXISTS is
      select 'x'
        from packitem pam
       where pam.pack_no = I_pack_no
         and ((pam.item  = I_item)
          or ((pam.item is NULL) and (I_item = pam.item_parent))

          or (pam.item   = (select im.item_parent
                              from item_master im
                             where im.item = I_item))
          or (pam.item   = (select im.item_grandparent
                              from item_master im
                             where im.item = I_item)));
BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
   fetch C_EXISTS into L_exists;
   if C_EXISTS%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS',
                       'PACKITEM',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
      close C_EXISTS;
      O_exist := FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_EXISTS',
                       'PACKITEM',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
      close C_EXISTS;
      O_exist := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXISTS_IN_PACK;
---------------------------------------------
FUNCTION PARENT_TMPL_COST_RETAIL (O_error_message       IN OUT VARCHAR2,
                                  O_unit_retail_prim    IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                  O_unit_cost_prim      IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                  I_pack_no             IN     PACKITEM.PACK_NO%TYPE,
                                  I_item_parent         IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                                  I_pack_tmpl_id        IN     PACKITEM.PACK_TMPL_ID%TYPE,
                                  I_pack_orderable_ind  IN     ITEM_MASTER.ORDERABLE_IND%TYPE,
                                  I_currency_code_prim  IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
   return BOOLEAN IS

   L_program                   VARCHAR2(64) := 'PACKITEM_ADD_SQL.PARENT_TMPL_COST_RETAIL';
   L_unit_cost_prim            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_unit_cost_sup             ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_currency_code_sup         SUPS.CURRENCY_CODE%TYPE;
   L_currency_code_prim        SUPS.CURRENCY_CODE%TYPE;
   L_supplier                  ITEM_SUPPLIER.SUPPLIER%TYPE;
   L_item                      PACKITEM.ITEM%TYPE;
   L_item_qty                  PACKITEM_BREAKOUT.ITEM_QTY%TYPE;
   L_unit_retail_prim          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_prim         ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_prim  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_prim          ITEM_LOC.SELLING_UOM%TYPE;
   L_origin_country_id         COUNTRY.COUNTRY_ID%TYPE;

   -- Needed for call to PRICING_ATTRIB_SQL
   L_unit_retail_zon          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_zon          ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_zon   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_zon           ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_zon           ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_zon     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_zon     ITEM_LOC.MULTI_SELLING_UOM%TYPE;

   cursor C_ITEM_SUPP_COUNTRY is
      select i.supplier, s.currency_code, i.origin_country_id
        from item_supp_country i, sups s
       where i.item                = I_pack_no
         and i.primary_supp_ind    = 'Y'
         and s.supplier            = i.supplier
         and i.primary_country_ind = 'Y';

   cursor C_UNIT_COST_SUPP is
      select sum(i.unit_cost * p.item_qty)
        from item_supp_country i, packitem_breakout p
       where i.item              = p.item
         and i.supplier          = L_supplier
         and p.pack_no           = I_pack_no
         and p.item_parent       = I_item_parent
         and p.pack_tmpl_id      = I_pack_tmpl_id
         and i.origin_country_id = L_origin_country_id;

   cursor C_PACKITEM is
      select item, item_qty
        from packitem_breakout
       where pack_no      = I_pack_no
         and item_parent  = I_item_parent
         and pack_tmpl_id = I_pack_tmpl_id;

BEGIN

   O_unit_retail_prim := 0;
   O_unit_cost_prim   := 0;

   if I_pack_orderable_ind = 'Y' then
      -- fetch the primary supplier and supplier's currency code for the pack
      open C_ITEM_SUPP_COUNTRY;
      fetch C_ITEM_SUPP_COUNTRY into L_supplier, L_currency_code_sup, L_origin_country_id;
      close C_ITEM_SUPP_COUNTRY;

      -- fetch the total unit cost for all items in the pack in supplier currency
      open C_UNIT_COST_SUPP;
      fetch C_UNIT_COST_SUPP into L_unit_cost_sup;
      close C_UNIT_COST_SUPP;

      --  convert the unit cost from supplier currency to primary currency
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_unit_cost_sup,
                              L_currency_code_sup,
                              I_currency_code_prim,
                              O_unit_cost_prim,
                              'C',
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
      ---
      FOR prec IN C_PACKITEM LOOP
          if PRICING_ATTRIB_SQL.GET_BASE_ZONE_RETAIL(O_error_message,
                                                     L_unit_retail_prim,
                                                     L_unit_retail_zon,
                                                     L_standard_uom_zon,
                                                     L_selling_unit_retail_zon,
                                                     L_selling_uom_zon,
                                                     L_multi_units_zon,
                                                     L_multi_unit_retail_zon,
                                                     L_multi_selling_uom_zon,
                                                     prec.item) = FALSE then
             return FALSE;
          end if;
          O_unit_retail_prim := O_unit_retail_prim + (L_unit_retail_prim * prec.item_qty);
       END LOOP;

   else    -- pack is not orderable
      FOR C_REC in C_PACKITEM LOOP
         -- get the total unit cost for all items in the pack
         L_item        := C_REC.item;
         L_item_qty    := C_REC.item_qty;

         -- get the unit cost in item's primary supplier's currency
         -- Get Base retail for each component, this package calls PRICING_ATTRIB_SQL.
         -- GET_BASE_ZONE_RETAIL function
         if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                                 L_unit_cost_prim,
                                                 L_unit_retail_prim,
                                                 L_standard_uom_prim,
                                                 L_selling_unit_retail_prim,
                                                 L_selling_uom_prim,
                                                 L_item) = FALSE then
            return FALSE;
         end if;
         ---
         O_unit_cost_prim := O_unit_cost_prim + (L_unit_cost_prim * L_item_qty);
          --- Compute pack retail
         O_unit_retail_prim := O_unit_retail_prim + (L_unit_retail_prim * L_item_qty);
      END LOOP;
   end if;
   ---
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END PARENT_TMPL_COST_RETAIL;
--------------------------------------------------------------------------------------
FUNCTION COMP_COST_RETAIL(O_error_message     IN OUT VARCHAR2,
                          O_unit_retail_prim  IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                          O_unit_cost_prim    IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          I_pack_no           IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                          I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                          I_pack_tmpl_id      IN     PACKITEM_BREAKOUT.PACK_TMPL_ID%TYPE)
   return BOOLEAN IS
   ---
   L_program   VARCHAR2(64) := 'PACKITEM_ADD_SQL.COMP_COST_RETAIL';
   L_pack_ind                  ITEM_MASTER.PACK_IND%TYPE;
   L_item_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_item_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE;
   L_item_pack_type            ITEM_MASTER.PACK_TYPE%TYPE;
   L_unit_cost_supp            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_currency_code_supp        CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_code_prim        CURRENCIES.CURRENCY_CODE%TYPE;
   L_unit_cost_temp            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_standard_uom_temp         ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_temp  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_temp          ITEM_LOC.SELLING_UOM%TYPE;
   ---
   L_system_options_rec        SYSTEM_OPTIONS%ROWTYPE;
   L_dlvry_country       ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;


   cursor C_GET_UNIT_COST_INFO is
      select isc.unit_cost,
             s.currency_code
        from item_supp_country isc,
             sups s
       where isc.supplier            = s.supplier
         and isc.item                = I_item
         and isc.primary_supp_ind    = 'Y'
         and isc.primary_country_ind = 'Y';

   cursor C_GET_UNIT_COST_INFO_ORD_SUPP is
      select isc2.unit_cost,
             s.currency_code
        from item_supp_country isc1,
             item_supp_country isc2,
             sups s
       where isc1.item                = I_pack_no                  /* get pack's primary supplier/country */
         and isc1.primary_supp_ind    = 'Y'                        /* and the associated currency         */
         and isc1.primary_country_ind = 'Y'
         and isc1.supplier            = s.supplier
         and isc2.item                = I_item                     /* get the component item's unit cost  */
         and isc2.supplier            = isc1.supplier              /* for the same supplier/country as    */
         and isc2.origin_country_id   = isc1.origin_country_id;    /* the pack's primary supplier/country */

                                                                   /* currency will be the same for the   */
                                                                   /* pack and the component since we're  */
                                                                   /* using the same supplier             */

   CURSOR C_GET_UNIT_COST_INFO_COST_HEAD IS
      select decode(ca.default_po_cost, 'NIC', ich.negotiated_item_cost,
                                               ich.base_cost),
             s.currency_code
        from country_attrib ca,
             sups s,
             item_cost_head ich,
             item_supp_country isc
       where isc.supplier            = s.supplier
         and ich.supplier            = isc.supplier
         and ich.item                = isc.item
         and isc.origin_country_id   = ich.origin_country_id
         and ich.delivery_country_id = ca.country_id
         and ich.item                = I_item
         and isc.primary_supp_ind    = 'Y'
         and isc.primary_country_ind = 'Y'
         and (ich.delivery_country_id  = L_dlvry_country or
             (ich.prim_dlvy_ctry_ind  = 'Y' and not exists (select 'x'
                                                              from item_cost_head ich2
                                                             where ich2.item = I_item
                                                               and isc.supplier = ich2.supplier
                                                               and isc.origin_country_id = ich2.origin_country_id
                                                               and ich2.delivery_country_id  = L_dlvry_country)));

   cursor C_GET_UNIT_COSTINFO_CH_ORDINFO is
      select decode(ca.default_po_cost, 'NIC', ich.negotiated_item_cost,
                                               ich.base_cost),
             s.currency_code
        from country_attrib ca,
             sups s,
             item_cost_head ich,
             item_supp_country isc
       where isc.supplier             = s.supplier
         and isc.item                 = I_pack_no
         and isc.primary_supp_ind     = 'Y'
         and isc.primary_country_ind  = 'Y'
         and ich.item                 = I_item
         and isc.supplier             = ich.supplier
         and isc.origin_country_id    = ich.origin_country_id
         and ich.delivery_country_id  = ca.country_id
         and (ich.delivery_country_id  = L_dlvry_country or
             (ich.prim_dlvy_ctry_ind  = 'Y' and not exists (select 'x'
                                                              from item_cost_head ich2
                                                             where ich2.item = I_item
                                                               and isc.supplier = ich2.supplier
                                                               and isc.origin_country_id = ich2.origin_country_id
                                                               and ich2.delivery_country_id  = L_dlvry_country)));

   cursor C_GET_DLVRY_COUNTRY is
      select ich.delivery_country_id
        from item_cost_head ich,
             item_supp_country isc
         where ich.item                      = I_pack_no
           and ich.item                      = isc.item
           and ich.supplier                  = isc.supplier
           and isc.primary_supp_ind          = 'Y'
           and isc.primary_country_ind       = 'Y'
           and isc.origin_country_id         = ich.origin_country_id
           and ich.prim_dlvy_ctry_ind        = 'Y';


BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE (O_error_message,
                                        L_currency_code_prim) = FALSE then
      return FALSE;
   end if;
   ---
   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   ---
   open C_GET_DLVRY_COUNTRY;
   fetch C_GET_DLVRY_COUNTRY into L_dlvry_country;
   close C_GET_DLVRY_COUNTRY;

   if L_dlvry_country is NULL then
      L_dlvry_country := L_system_options_rec.base_country_id;
   end if;

   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_item_sellable_ind,
                                    L_item_orderable_ind,
                                    L_item_pack_type,
                                    I_pack_no) = FALSE then
         return FALSE;
   end if;

   if I_pack_tmpl_id is NOT NULL then
      if PARENT_TMPL_COST_RETAIL(O_error_message,
                                 O_unit_retail_prim,
                                 O_unit_cost_prim,
                                 I_pack_no,
                                 I_item,
                                 I_pack_tmpl_id,
                                 L_item_orderable_ind,
                                 L_currency_code_prim) = FALSE then
         return FALSE;
      end if;
      ---
      return TRUE;
   else
      ---
      if (L_pack_ind = 'Y' and L_item_orderable_ind = 'N') then
         if L_system_options_rec.default_tax_type = 'GTAX' then
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_UNIT_COST_INFO_COST_HEAD',
                             'ITEM_COST_HEAD, ITEM_SUPP_COUNTRY',
                             'ITEM: '||I_item);
            open C_GET_UNIT_COST_INFO_COST_HEAD;
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_UNIT_COST_INFO_COST_HEAD',
                             'ITEM_COST_HEAD, ITEM_SUPP_COUNTRY',
                             'ITEM: '||I_item);
            fetch C_GET_UNIT_COST_INFO_COST_HEAD into L_unit_cost_supp,
                                                      L_currency_code_supp;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_UNIT_COST_INFO_COST_HEAD',
                             'ITEM_COST_HEAD, ITEM_SUPP_COUNTRY',
                             'ITEM: '||I_item);
            close C_GET_UNIT_COST_INFO_COST_HEAD;
         elsif L_system_options_rec.default_tax_type in ('SVAT', 'SALES') then
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_UNIT_COST_INFO',
                             'ITEM_SUPP_COUNTRY, SUPS',
                             'ITEM: '||I_item);
            open C_GET_UNIT_COST_INFO;
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_UNIT_COST_INFO',
                             'ITEM_SUPP_COUNTRY, SUPS',
                             'ITEM: '||I_item);
            fetch C_GET_UNIT_COST_INFO into L_unit_cost_supp,
                                            L_currency_code_supp;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_UNIT_COST_INFO',
                             'ITEM_SUPP_COUNTRY, SUPS',
                             'ITEM: '||I_item);
            close C_GET_UNIT_COST_INFO;
          end if;
          ---
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_unit_cost_supp,
                                 L_currency_code_supp,
                                 L_currency_code_prim,
                                 O_unit_cost_prim,
                                 'C',
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                                 L_unit_cost_temp,
                                                 O_unit_retail_prim,
                                                 L_standard_uom_temp,
                                                 L_selling_unit_retail_temp,
                                                 L_selling_uom_temp,
                                                 I_item,
                                                 'R') = FALSE then
            return FALSE;
         end if;
      elsif (L_pack_ind = 'Y' and L_item_orderable_ind = 'Y') or
             L_pack_ind = 'N' then
         if L_system_options_rec.default_tax_type = 'GTAX' then 
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_UNIT_COSTINFO_CH_ORDINFO',
                             'ITEM_COST_HEAD, ITEM_SUPP_COUNTRY, SUPS, PACKITEM',
                             'ITEM: '||I_item);
            open C_GET_UNIT_COSTINFO_CH_ORDINFO;
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_UNIT_COSTINFO_CH_ORDINFO',
                             'ITEM_COST_HEAD, ITEM_SUPP_COUNTRY, SUPS, PACKITEM',
                             'ITEM: '||I_item);
            fetch C_GET_UNIT_COSTINFO_CH_ORDINFO into L_unit_cost_supp,
                                                      L_currency_code_supp;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_UNIT_COSTINFO_CH_ORDINFO',
                             'ITEM_COST_HEAD, ITEM_SUPP_COUNTRY, SUPS, PACKITEM',
                             'ITEM: '||I_item);
            close C_GET_UNIT_COSTINFO_CH_ORDINFO;
         elsif L_system_options_rec.default_tax_type in ('SALES', 'SVAT') then
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_UNIT_COST_INFO_ORD_SUPP',
                             'ITEM_SUPP_COUNTRY, SUPS, PACKITEM',
                             'ITEM: '||I_item);
            open C_GET_UNIT_COST_INFO_ORD_SUPP;
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_UNIT_COST_INFO_ORD_SUPP',
                             'ITEM_SUPP_COUNTRY, SUPS, PACKITEM',
                             'ITEM: '||I_item);
            fetch C_GET_UNIT_COST_INFO_ORD_SUPP into L_unit_cost_supp,
                                                     L_currency_code_supp;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_UNIT_COST_INFO_ORD_SUPP',
                             'ITEM_SUPP_COUNTRY, SUPS, PACKITEM',
                             'ITEM: '||I_item);
            close C_GET_UNIT_COST_INFO_ORD_SUPP;
         end if;
         ---
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_unit_cost_supp,
                                 L_currency_code_supp,
                                 L_currency_code_prim,
                                 O_unit_cost_prim,
                                 'C',
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                                 L_unit_cost_temp,
                                                 O_unit_retail_prim,
                                                 L_standard_uom_temp,
                                                 L_selling_unit_retail_temp,
                                                 L_selling_uom_temp,
                                                 I_item,
                                                 'R') = FALSE then
            return FALSE;
         end if;
      else
         if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                                 O_unit_cost_prim,
                                                 O_unit_retail_prim,
                                                 L_standard_uom_temp,
                                                 L_selling_unit_retail_temp,
                                                 L_selling_uom_temp,
                                                 I_item,
                                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END COMP_COST_RETAIL;
-------------------------------------------------------------------------------------
FUNCTION BUILD_COMP_COST_RETAIL (O_error_message    IN OUT VARCHAR2,
                                 O_unit_cost_prim   IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                 O_unit_retail_prim IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                 I_pack_no          IN     PACKITEM.PACK_NO%TYPE)
   return BOOLEAN IS
   ---
   L_program        VARCHAR2(64)  := 'PACKITEM_ADD_SQL.BUILD_COMP_COST_RETAIL';
   L_unit_cost_prim   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_unit_retail_prim ITEM_LOC.UNIT_RETAIL%TYPE;
   ---
   cursor C_PACKITEM is
      select nvl(item, item_parent) item,
             pack_tmpl_id,
             pack_qty
        from packitem
       where pack_no = I_pack_no;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   O_unit_cost_prim   := 0;
   O_unit_retail_prim := 0;
   ---
   FOR rec in C_PACKITEM LOOP
      if PACKITEM_ADD_SQL.COMP_COST_RETAIL(O_error_message,
                                           L_unit_retail_prim,
                                           L_unit_cost_prim,
                                           I_pack_no,
                                           rec.item,
                                           rec.pack_tmpl_id) = FALSE then
         return FALSE;
      end if;
      ---
      O_unit_cost_prim   := O_unit_cost_prim   + L_unit_cost_prim   * rec.pack_qty;
      O_unit_retail_prim := O_unit_retail_prim + L_unit_retail_prim * rec.pack_qty;
   end LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END BUILD_COMP_COST_RETAIL;
---------------------------------------------------------------
FUNCTION NEW_PACK_TMPL(O_error_message       IN OUT   VARCHAR2,
                       O_item_rejected       IN OUT   BOOLEAN,
                       I_pack_no             IN       PACKITEM.PACK_NO%TYPE,
                       I_item_parent         IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_template_id         IN       PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                       I_pack_type           IN       PACK_TMPL_HEAD.PACK_TYPE%TYPE,
                       I_supplier            IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                       I_description         IN       ITEM_MASTER.ITEM_DESC%TYPE,
                       I_short_desc          IN       ITEM_MASTER.SHORT_DESC%TYPE,
                       I_vendor_desc         IN       SUPS_PACK_TMPL_DESC.SUPP_PACK_DESC%TYPE,
                       I_order_as_type       IN       ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                       I_origin_country      IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_unit_cost           IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                       I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'PACKITEM_ADD_SQL.NEW_PACK_TMPL';

BEGIN

   L_import_country_id := I_import_country_id;

   IF PACKITEM_ADD_SQL.NEW_PACK_TMPL(O_error_message,
                                     O_item_rejected,
                                     I_pack_no,
                                     I_item_parent,
                                     I_template_id,
                                     I_pack_type,
                                     I_supplier,
                                     I_description,
                                     NULL, -- optional secondary description
                                     I_short_desc,
                                     I_vendor_desc,
                                     I_order_as_type,
                                     I_origin_country,
                                     I_unit_cost,
                                     L_import_country_id) = FALSE THEN
      return FALSE;
   END IF;
   ---
   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END NEW_PACK_TMPL;

---------------------------------------------------------------
FUNCTION NEW_PACK_TMPL(O_error_message        IN OUT   VARCHAR2,
                       O_item_rejected        IN OUT   BOOLEAN,
                       I_pack_no              IN       PACKITEM.PACK_NO%TYPE,
                       I_item_parent          IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_template_id          IN       PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                       I_pack_type            IN       PACK_TMPL_HEAD.PACK_TYPE%TYPE,
                       I_supplier             IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                       I_description          IN       ITEM_MASTER.ITEM_DESC%TYPE,
                       I_description_sec      IN       ITEM_MASTER.ITEM_DESC_SECONDARY%TYPE,
                       I_short_desc           IN       ITEM_MASTER.SHORT_DESC%TYPE,
                       I_vendor_desc          IN       SUPS_PACK_TMPL_DESC.SUPP_PACK_DESC%TYPE,
                       I_order_as_type        IN       ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                       I_origin_country       IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_unit_cost            IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                       I_import_country_id    IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN AS
   ---
   L_run_report             BOOLEAN := TRUE;
   L_vendor_desc            SUPS_PACK_TMPL_DESC.SUPP_PACK_DESC%TYPE;
   L_unit_cost_sup          ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_unit_cost_prim         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_sup_curr               SUPS.CURRENCY_CODE%TYPE;
   L_vdate                  PERIOD.VDATE%TYPE := DATES_SQL.GET_VDATE;
   L_obj_tax_info_rec       OBJ_TAX_INFO_REC   :=   OBJ_TAX_INFO_REC();
   L_obj_tax_info_tbl       OBJ_TAX_INFO_TBL   :=   OBJ_TAX_INFO_TBL();
   ---
   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   L_country_attrib_row     COUNTRY_ATTRIB%ROWTYPE;
   L_default_loc            COUNTRY_ATTRIB.DEFAULT_LOC%TYPE := NULL;
   L_item_cost_head_ip      ITEM_COST_HEAD%ROWTYPE;
   L_base_cost              ITEM_COST_HEAD.BASE_COST%TYPE;
   L_extended_base_cost     ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE;
   L_negotiated_item_cost   ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE;
   L_inclusive_cost         ITEM_COST_HEAD.INCLUSIVE_COST%TYPE;
   L_item_cost_rec          OBJ_COMP_ITEM_COST_REC := OBJ_COMP_ITEM_COST_REC();
   L_item_tbl               ITEM_TBL := ITEM_TBL(I_pack_no);
   ---
   CURSOR C_UNIT_COST IS
      SELECT SUM(v.qty * i.unit_cost)
        FROM item_supp_country i,
             V_packsku_qty v
       WHERE v.pack_no           = I_pack_no
         AND v.item              = i.item
         AND i.supplier          = I_supplier
         AND i.origin_country_id = I_origin_country;
   ---
   CURSOR C_PACK_UNIT_COST IS
      SELECT SUM(p.qty * i.unit_cost)
        FROM item_supp_country i,
             pack_tmpl_detail p
       WHERE p.pack_tmpl_id      = I_template_id
         AND i.item              = I_item_parent
         AND i.supplier          = I_supplier
         AND i.origin_country_id = I_origin_country;
   ---
   CURSOR C_SUPP_DESC IS
      SELECT supp_pack_desc
        FROM sups_pack_tmpl_desc
       WHERE supplier     = I_supplier
         AND pack_tmpl_id = I_template_id;
   ---
   CURSOR C_ITEM_COST_HEAD_IP IS
      SELECT *
        FROM item_cost_head
       WHERE item                = I_item_parent
         AND supplier            = I_supplier
         AND origin_country_id   = I_origin_country
         AND delivery_country_id = L_import_country_id;
   ---
   CURSOR C_ITEM_SUPP_COUNTRY_ROW IS
      SELECT *
        FROM item_supp_country
       WHERE item                = I_item_parent
         AND supplier            = I_supplier
         AND origin_country_id   = I_origin_country;

BEGIN

   SQL_LIB.SET_MARK('INSERT', NULL, 'ITEM_MASTER','ITEM_PARENT: '||I_item_parent);
   INSERT INTO item_master(item,
                           item_number_type,
                           pack_ind,
                           item_level,
                           tran_level,
                           item_aggregate_ind,
                           diff_1_aggregate_ind,
                           diff_2_aggregate_ind,
                           diff_3_aggregate_ind,
                           diff_4_aggregate_ind,
                           dept,
                           CLASS,
                           subclass,
                           status,
                           item_desc,
                           item_desc_secondary,
                           short_desc,
                           desc_up,
                           primary_ref_item_ind,
                           cost_zone_group_id,
                           standard_uom,
                           merchandise_ind,
                           store_ord_mult,
                           forecast_ind,
                           catch_weight_ind,
                           const_dimen_ind,
                           simple_pack_ind,
                           contains_inner_ind,
                           sellable_ind,
                           orderable_ind,
                           pack_type,
                           order_as_type,
                           gift_wrap_ind,
                           ship_alone_ind,
                           check_uda_ind,
                           item_xform_ind,
                           inventory_ind,
                           create_datetime,
                           last_update_id,
                           last_update_datetime,
                           aip_case_type,
                           perishable_ind)
                    SELECT I_pack_no,
                           item_number_type,
                           'Y',
                           1,  -- item_level
                           1,  -- tran_level
                           'N',
                           'N',
                           'N',
                           'N',
                           'N',
                           dept,
                           CLASS,
                           subclass,
                           'A',  -- Approved status
                           I_description,
                           I_description_sec,
                           I_short_desc,
                           UPPER(I_description),
                           'N',
                           DECODE(I_pack_type,'V',cost_zone_group_id,NULL),
                           standard_uom,
                           merchandise_ind,
                           'E',
                           forecast_ind,
                           catch_weight_ind,
                           const_dimen_ind,
                           'N',  --simple pack ind
                           'N',  --contains inner ind
                           'N',  --sellable_ind
                           'Y',  --orderable_ind
                           I_pack_type,
                           DECODE(I_pack_type,'V', NULL, I_order_as_type),
                           gift_wrap_ind,
                           ship_alone_ind,
                           'Y',  --check_uda_ind
                           'N',  --item_xform_ind
                           'Y',  --inventory_ind
                           SYSDATE,
                           GET_USER,
                           SYSDATE,
                           NULL,
                           'N'
                      FROM item_master
                     WHERE item = I_item_parent;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'PACKITEM','PACK_NO: '||I_pack_no);
   INSERT INTO packitem(pack_no,
                        seq_no,
                        item_parent,
                        pack_tmpl_id,
                        pack_qty,
                        create_datetime,
                        last_update_datetime,
                        last_update_id)
                 VALUES(I_pack_no,
                        1,
                        I_item_parent,
                        I_template_id,
                        1,
                        SYSDATE,
                        SYSDATE,
                        GET_USER);
   ---
   IF Packitem_Add_Sql.PARENT_PACK_TMPL(O_error_message,
                                        O_item_rejected,
                                        I_pack_no,
                                        I_item_parent,
                                        I_template_id,
                                        1,
                                        I_supplier,
                                        I_origin_country ) = FALSE THEN
      return FALSE;
   END IF;
   ---
   IF I_unit_cost IS NULL THEN
      SQL_LIB.SET_MARK('OPEN', 'C_UNIT_COST', 'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                       'PACK_NO: '||I_pack_no ||' SUPPLIER: '||
                       TO_CHAR(I_supplier));
      OPEN C_UNIT_COST;
      SQL_LIB.SET_MARK('FETCH', 'C_UNIT_COST', 'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                       'PACK_NO: '|| I_pack_no ||' SUPPLIER: '||
                       TO_CHAR(I_supplier));
      FETCH C_UNIT_COST INTO L_unit_cost_sup;
      SQL_LIB.SET_MARK('CLOSE', 'C_UNIT_COST', 'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                       'PACK_NO: '|| I_pack_no ||' SUPPLIER: '||
                       TO_CHAR(I_supplier));
      CLOSE C_UNIT_COST;
   ELSIF I_unit_cost = 0 THEN
      SQL_LIB.SET_MARK('OPEN', 'C_PACK_UNIT_COST', 'ITEM_SUPP_COUNTRY, PACK_TMPL_DETAIL',
                       'PACK_NO: '||I_pack_no ||' SUPPLIER: '||
                       TO_CHAR(I_supplier));
      OPEN C_PACK_UNIT_COST;
      SQL_LIB.SET_MARK('FETCH', 'C_PACK_UNIT_COST', 'ITEM_SUPP_COUNTRY, PACK_TMPL_DETAIL',
                       'PACK_NO: '|| I_pack_no ||' SUPPLIER: '||
                       TO_CHAR(I_supplier));
      FETCH C_PACK_UNIT_COST INTO L_unit_cost_sup;
      SQL_LIB.SET_MARK('CLOSE', 'C_PACK_UNIT_COST', 'ITEM_SUPP_COUNTRY, PACK_TMPL_DETAIL',
                       'PACK_NO: '|| I_pack_no ||' SUPPLIER: '||
                       TO_CHAR(I_supplier));
      CLOSE C_PACK_UNIT_COST;
   ELSE
      L_unit_cost_sup := I_unit_cost;
   END IF;
   ---
   if L_system_options_rec.default_tax_type = 'GTAX' then
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY','ITEM: '|| I_pack_no
                    ||' COUNTRY: '|| TO_CHAR(I_origin_country));
      INSERT INTO item_country(item,
                               country_id)
                        values(I_pack_no,
                               L_import_country_id);
   ---
      if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                 L_country_attrib_row,
                                                 L_default_loc,
                                                 L_import_country_id)=FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPPLIER','ITEM: '|| I_pack_no
                    ||' SUPPLIER: '|| TO_CHAR(I_supplier));
   INSERT INTO item_supplier(item,
                             supplier,
                             primary_supp_ind,
                             vpn,
                             supp_label,
                             consignment_rate,
                             pallet_name,
                             case_name,
                             inner_name,
                             direct_ship_ind,
                             create_datetime,
                             last_update_datetime,
                             last_update_id,
                             primary_case_size)
                      SELECT DISTINCT I_pack_no,
                             I_supplier,
                             'Y',
                             NULL, --vpn
                             NULL, --supp_label
                             NULL, --consignment rate
                             pallet_name,
                             case_name,
                             inner_name,
                             direct_ship_ind,
                             SYSDATE,
                             SYSDATE,
                             GET_USER,
                             NULL
                        FROM item_supplier
                       WHERE item = I_item_parent
                         and supplier = I_supplier;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY','ITEM: '|| I_pack_no
                    ||' SUPPLIER: '|| TO_CHAR(I_supplier)||'COUNTRY: '||I_origin_country);
   ---
   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   ---
   if L_system_options_rec.default_tax_type in ('SALES','SVAT') then
      if I_pack_type = 'V' then
         L_obj_tax_info_tbl.EXTEND();

         L_obj_tax_info_rec.item          :=   I_pack_no;
         L_obj_tax_info_rec.item_parent   :=   I_item_parent;
         L_obj_tax_info_tbl(L_obj_tax_info_tbl.COUNT) := L_obj_tax_info_rec;
         ---
         if TAX_SQL.GET_TAX_INFO(O_error_message,
                                 L_obj_tax_info_tbl)=FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if I_pack_type = 'B' then
         ---
         L_obj_tax_info_tbl.EXTEND();

         L_obj_tax_info_rec.item          :=   I_pack_no;
         L_obj_tax_info_rec.item_parent   :=   I_item_parent;

         L_obj_tax_info_tbl(L_obj_tax_info_tbl.COUNT) := L_obj_tax_info_rec;
         ---
         if TAX_SQL.GET_TAX_INFO(O_error_message,
                                 L_obj_tax_info_tbl)=FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if TAX_SQL.INSERT_UPDATE_TAX_SKU(O_error_message,
                                       L_run_report,
                                       L_obj_tax_info_tbl)=FALSE then

         return FALSE;
      end if;
      ---
      IF CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   I_supplier,
                                   'V',
                                   NULL, -- zone group id
                                   L_sup_curr) = FALSE THEN
         return FALSE;
      END IF;
      IF CURRENCY_SQL.CONVERT(O_error_message,
                              L_unit_cost_sup,
                              L_sup_curr,
                              NULL, --convert to primary currency
                              L_unit_cost_prim,
                              'C',
                              NULL, --use today's exchange rate
                              NULL) = FALSE THEN
         return FALSE;
      END IF;
   end if;
   --
   if L_system_options_rec.default_tax_type = 'GTAX' then
      if L_country_attrib_row.item_cost_tax_incl_ind = 'Y' then
         L_negotiated_item_cost := L_unit_cost_sup;
      else
         L_base_cost := L_unit_cost_sup;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ITEM_COST_HEAD_IP','ITEM_COST_HEAD','SUPPLIER: '||
                       TO_CHAR(I_supplier) ||' ITEM: '||
                       TO_CHAR(I_item_parent));
      OPEN C_ITEM_COST_HEAD_IP;
      SQL_LIB.SET_MARK('FETCH','C_ITEM_COST_HEAD_IP','ITEM_COST_HEAD','SUPPLIER: '||
                       TO_CHAR(I_supplier) ||' ITEM: '||
                       TO_CHAR(I_item_parent));
      FETCH C_ITEM_COST_HEAD_IP INTO L_item_cost_head_ip;
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_COST_HEAD_IP','ITEM_COST_HEAD','SUPPLIER: '||
                       TO_CHAR(I_supplier) ||' ITEM: '||
                       TO_CHAR(I_item_parent));
      CLOSE C_ITEM_COST_HEAD_IP;

      L_item_cost_rec.I_item := I_pack_no;
      L_item_cost_rec.I_nic_static_ind := 'Y';
      L_item_cost_rec.I_supplier := I_supplier;
      L_item_cost_rec.I_location := L_country_attrib_row.default_loc;
      L_item_cost_rec.I_loc_type := L_country_attrib_row.default_loc_type;
      L_item_cost_rec.I_effective_date := GET_VDATE;
      L_item_cost_rec.I_calling_form := 'ITEMSUPPCTRY';
      L_item_cost_rec.I_origin_country_id := I_origin_country;
      L_item_cost_rec.I_delivery_country_id := L_import_country_id;
      L_item_cost_rec.I_prim_dlvy_ctry_ind := NVL(L_item_cost_head_ip.prim_dlvy_ctry_ind,'N');
      L_item_cost_rec.I_update_itemcost_ind := 'Y';
      L_item_cost_rec.I_update_itemcost_child_ind := 'N';
      L_item_cost_rec.I_item_cost_tax_incl_ind := 'Y';
      L_item_cost_rec.O_base_cost := L_unit_cost_sup;
      L_item_cost_rec.O_extended_base_cost := NULL;
      L_item_cost_rec.O_inclusive_cost := NULL;
      L_item_cost_rec.O_negotiated_item_cost := L_unit_cost_sup;
      L_item_cost_rec.svat_tax_rate := NULL;
      L_item_cost_rec.tax_loc_type := NULL;
      L_item_cost_rec.pack_ind := NULL;
      L_item_cost_rec.pack_type := NULL;
      L_item_cost_rec.dept := NULL;
      L_item_cost_rec.prim_supp_currency_code := NULL;
      L_item_cost_rec.loc_prim_country := NULL;
      L_item_cost_rec.loc_prim_country_tax_incl_ind := NULL;
      L_item_cost_rec.gtax_total_tax_amount := NULL;
      L_item_cost_rec.gtax_total_tax_amount_nic := NULL;
      L_item_cost_rec.gtax_total_recover_amount := NULL;
      LP_item_cost_tbl.EXTEND();
      LP_item_cost_tbl(LP_item_cost_tbl.COUNT) := L_item_cost_rec;
   else
      L_base_cost            := NULL;
      L_negotiated_item_cost := NULL;
      L_extended_base_cost   := NULL;
      L_inclusive_cost       := NULL;
   end if;
   ---

   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY','ITEM: '|| I_pack_no
                    ||' SUPPLIER: '|| TO_CHAR(I_supplier));
   INSERT INTO item_supp_country(item,
                                 supplier,
                                 origin_country_id,
                                 unit_cost,
                                 lead_time,
                                 supp_pack_size,
                                 inner_pack_size,
                                 round_lvl,
                                 round_to_inner_pct,
                                 round_to_case_pct,
                                 round_to_layer_pct,
                                 round_to_pallet_pct,
                                 min_order_qty,
                                 max_order_qty,
                                 packing_method,
                                 primary_supp_ind,
                                 primary_country_ind,
                                 default_uop,
                                 ti,
                                 hi,
                                 supp_hier_type_1,
                                 supp_hier_lvl_1,
                                 supp_hier_type_2,
                                 supp_hier_lvl_2,
                                 supp_hier_type_3,
                                 supp_hier_lvl_3,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id,
                                 cost_uom,
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 inclusive_cost,
                                 base_cost)
                          SELECT I_pack_no,
                                 I_supplier,
                                 I_origin_country,
                                 L_unit_cost_sup,
                                 lead_time,
                                 1,
                                 1,
                                 round_lvl,
                                 round_to_inner_pct,
                                 round_to_case_pct,
                                 round_to_layer_pct,
                                 round_to_pallet_pct,
                                 NULL, --min order qty
                                 NULL, --max order qty
                                 packing_method,
                                 'Y', --prim supp. ind
                                 'Y', --prim country ind
                                 default_uop,
                                 ti,
                                 hi,
                                 supp_hier_type_1,
                                 supp_hier_lvl_1,
                                 supp_hier_type_2,
                                 supp_hier_lvl_2,
                                 supp_hier_type_3,
                                 supp_hier_lvl_3,
                                 SYSDATE,
                                 SYSDATE,
                                 GET_USER,
                                 cost_uom,
                                 L_negotiated_item_cost,
                                 L_extended_base_cost,
                                 L_inclusive_cost,
                                 L_base_cost
                            FROM item_supp_country
                           WHERE item              = I_item_parent
                             AND supplier          = I_supplier
                             AND origin_country_id = I_origin_country;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_MANU_COUNTRY','ITEM: '|| I_pack_no
                    ||' SUPPLIER: '|| TO_CHAR(I_supplier));
   INSERT INTO item_supp_manu_country(item,
                                      supplier,
                                      manu_country_id,
                                      primary_manu_ctry_ind)
                               SELECT I_pack_no,
                                      I_supplier,
                                      manu_country_id,
                                      'Y'
                                 FROM item_supp_manu_country
                                WHERE item                  = I_item_parent
                                  AND supplier              = I_supplier
                                  AND primary_manu_ctry_ind = 'Y';

   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST','ITEM: '||I_pack_no);
   INSERT INTO price_hist(tran_type,
                          reason,
                          event,
                          item,
                          loc,
                          loc_type,
                          unit_cost,
                          unit_retail,
                          selling_unit_retail,
                          selling_uom,
                          action_date,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom)
                   VALUES(0,
                          0,
                          NULL, --event
                          I_pack_no,
                          0, -- loc
                          NULL,  -- loc_type
                          L_unit_cost_prim,
                          NULL,
                          NULL,
                          NULL,
                          L_vdate,
                          NULL,
                          NULL,
                          NULL);
   ---
   SQL_LIB.SET_MARK('OPEN','C_SUPP_DESC','SUPS_PACK_TMPL_DESC','SUPPLIER: '||
                    TO_CHAR(I_supplier) ||' PACK_TMPL_ID: '||
                    TO_CHAR(I_template_id));
   OPEN C_SUPP_DESC;
   SQL_LIB.SET_MARK('FETCH','C_SUPP_DESC','SUPS_PACK_TMPL_DESC','SUPPLIER: '||
                    TO_CHAR(I_supplier) ||' PACK_TMPL_ID: '||
                    TO_CHAR(I_template_id));
   FETCH C_SUPP_DESC INTO L_vendor_desc;
   IF C_SUPP_DESC%NOTFOUND THEN
      SQL_LIB.SET_MARK('INSERT',NULL,'SUPS_PACK_TMPL_DESC','SUPPLIER: '||
                       TO_CHAR(I_supplier) ||' PACK_TMPL_ID: '||
                       TO_CHAR(I_template_id));
      INSERT INTO sups_pack_tmpl_desc(supplier,
                                      pack_tmpl_id,
                                      supp_pack_desc)
                               VALUES(I_supplier,
                                      I_template_id,
                                      RTRIM(SUBSTRB(I_vendor_desc,1, 250)));
   END IF;
   SQL_LIB.SET_MARK('CLOSE','C_SUPP_DESC','SUPS_PACK_TMPL_DESC','SUPPLIER: '||
                    TO_CHAR(I_supplier) ||' PACK_TMPL_ID: '||
                    TO_CHAR(I_template_id));
   CLOSE C_SUPP_DESC;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SEASONS','ITEM: '||I_pack_no);
   INSERT INTO item_seasons(item,
                            season_id,
                            phase_id,
                            item_season_seq_no,
                            create_datetime,
                            last_update_datetime,
                            last_update_id)
                    (SELECT I_pack_no,
                            season_id,
                            phase_id,
                            MIN(item_season_seq_no),
                            SYSDATE,
                            SYSDATE,
                            GET_USER
                       FROM item_seasons
                      WHERE item = I_item_parent
                      GROUP BY I_pack_no, season_id, phase_id);
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_FF','ITEM: '||I_pack_no);
   INSERT INTO uda_item_ff(item,
                           uda_id,
                           uda_text,
                           create_datetime,
                           last_update_datetime,
                           last_update_id)
                   (SELECT I_pack_no,
                           uda_id,
                           uda_text,
                           SYSDATE,
                           SYSDATE,
                           GET_USER
                      FROM uda_item_ff
                     WHERE item = I_item_parent);
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_LOV','ITEM: '||I_pack_no);
   INSERT INTO uda_item_lov(item,
                            uda_id,
                            uda_value,
                            create_datetime,
                            last_update_datetime,
                            last_update_id)
                    (SELECT I_pack_no,
                            uda_id,
                            uda_value,
                            SYSDATE,
                            SYSDATE,
                            GET_USER
                       FROM uda_item_lov
                      WHERE item = I_item_parent);
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_DATE','ITEM: '||I_pack_no);
   INSERT INTO uda_item_date(item,
                             uda_id,
                             uda_date,
                             create_datetime,
                             last_update_datetime,
                             last_update_id)
                     (SELECT I_pack_no,
                             uda_id,
                             uda_date,
                             SYSDATE,
                             SYSDATE,
                             GET_USER
                        FROM uda_item_date
                       WHERE item = I_item_parent);
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_TICKET','ITEM: '||I_pack_no);
   INSERT INTO item_ticket(item,
                           ticket_type_id,
                           po_print_type,
                           print_on_pc_ind,
                           ticket_over_pct,
                           create_datetime,
                           last_update_datetime,
                           last_update_id)
                   (SELECT I_pack_no,
                           ticket_type_id,
                           po_print_type,
                           print_on_pc_ind,
                           ticket_over_pct,
                           SYSDATE,
                           SYSDATE,
                           GET_USER
                      FROM item_ticket
                     WHERE item = I_item_parent);

  if ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE(O_error_message,
                                      L_item_tbl) = FALSE then
     return FALSE;
    end if;
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PACKITEM_ADD_SQL.NEW_PACK_TMPL',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END NEW_PACK_TMPL;

---------------------------------------------------------------------------
FUNCTION UPDATE_INSERT_ITEM(O_error_message    IN OUT VARCHAR2,
                            O_partner_upd_fail IN OUT BOOLEAN,
                            I_item             IN     item_supplier.item%TYPE,
                            I_item_parent      IN     item_master.item_parent%TYPE,
                            I_supplier         IN     item_supplier.supplier%TYPE,
                            I_origin_country   IN     item_supp_country.origin_country_id%TYPE,
                            I_unit_cost        IN     item_supp_country.unit_cost%TYPE,
                            I_primary_sup_ind  IN     item_supplier.primary_supp_ind%TYPE,
                            I_primary_cnt_ind  IN     item_supp_country.primary_country_ind%TYPE,
                            I_vpn              IN     item_supplier.vpn%TYPE,
                            I_supp_label       IN     item_supplier.supp_label%TYPE,
                            I_lead_time        IN     item_supp_country.lead_time%TYPE,
                            I_supp_pack_size   IN     item_supp_country.supp_pack_size%TYPE,
                            I_inner_pack_size  IN     item_supp_country.inner_pack_size%TYPE,
                            I_consignment_rate IN     item_supplier.consignment_rate%TYPE,
                            I_min_order_qty    IN     item_supp_country.min_order_qty%TYPE,
                            I_max_order_qty    IN     item_supp_country.max_order_qty%TYPE,
                            I_dimension_uom    IN     uom_class.uom%TYPE,
                            I_packing_method   IN     item_supp_country.packing_method%TYPE,
                            I_weight_uom       IN     uom_class.uom%TYPE,
                            I_pallet_name      IN     item_supplier.pallet_name%TYPE,
                            I_ti               IN     item_supp_country.ti%TYPE,
                            I_hi               IN     item_supp_country.hi%TYPE,
                            I_case_name        IN     item_supplier.case_name%TYPE,
                            I_inner_name       IN     item_supplier.inner_name%TYPE,
                            I_default_uop      IN     item_supp_country.default_uop%TYPE,
                            I_supp_hier_lvl_1  IN     item_supp_country.supp_hier_lvl_1%TYPE,
                            I_supp_hier_lvl_2  IN     item_supp_country.supp_hier_lvl_2%TYPE,
                            I_supp_hier_lvl_3  IN     item_supp_country.supp_hier_lvl_3%TYPE)
   RETURN BOOLEAN IS

   L_exist             BOOLEAN;
   L_country_exist     BOOLEAN;
   L_cost_ctrl_level   VARCHAR2(21);
   L_loc               ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_primary_loc       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_program           VARCHAR2(64) := 'PACKITEM_ADD_SQL.UPDATE_INSERT_ITEM';
   L_supp_hier_lvl_1   ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_1%TYPE;
   L_supp_hier_lvl_2   ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_2%TYPE;
   L_supp_hier_lvl_3   ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_3%TYPE;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);
   L_base_cost            ITEM_COST_HEAD.BASE_COST%TYPE;
   L_negotiated_item_cost ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE;
   L_extended_base_cost   ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost       ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE;
   L_item_cost_head       ITEM_COST_HEAD%ROWTYPE;
   L_country_attrib_row   COUNTRY_ATTRIB%ROWTYPE;
   L_default_loc          COUNTRY_ATTRIB.DEFAULT_LOC%TYPE;
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   L_default_po_cost      COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;
   L_dlvry_country_id     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;

   cursor C_LOCK_ITEM_SUPPLIER is
      select 'x'
        from item_supplier
       where item = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY is
      select 'x'
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country
         for update nowait;
   ---
   cursor C_LOCK_ITEM_COST_HEAD is
      select *
        from item_cost_head
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country
         and delivery_country_id = L_import_country_id
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
      select 'x'
        from item_supp_country_loc
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country
         and loc = NVL(L_loc, loc)
         for update nowait;
   ---
   cursor C_GET_DLVRY_COUNTRY_ID is
      select delivery_country_id
        from item_cost_head
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country
         and prim_dlvy_ctry_ind = 'Y';

BEGIN
   if not SUPP_ITEM_SQL.EXIST( O_error_message,
                               L_exist,
                               I_item,
                               I_supplier) then
      return FALSE;
   end if;
   ---
   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   ---
   if L_exist then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPPLIER','item_supplier','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier));
      open C_LOCK_ITEM_SUPPLIER;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPPLIER','item_supplier','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier));
      close C_LOCK_ITEM_SUPPLIER;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'item_supplier','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier));
      ---
      update item_supplier
         set vpn                  = NVL(I_vpn,vpn),
             primary_supp_ind     = NVL(I_primary_sup_ind,primary_supp_ind),
             supp_label           = NVL(I_supp_label,supp_label),
             consignment_rate     = NVL(I_consignment_rate,consignment_rate),
             pallet_name          = NVL(I_pallet_name,pallet_name),
             case_name            = NVL(I_case_name,case_name),
             inner_name           = NVL(I_inner_name,inner_name),
             last_update_datetime = sysdate,
             last_update_id       = get_user
       where item     = I_item
         and supplier = I_supplier;
   else
      SQL_LIB.SET_MARK('INSERT',NULL,'item_supplier', NULL);
      ---
      insert into item_supplier ( item,
                                  supplier,
                                  primary_supp_ind,
                                  vpn,
                                  supp_label,
                                  pallet_name,
                                  case_name,
                                  inner_name,
                                  consignment_rate,
                                  direct_ship_ind,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  primary_case_size)
                         values ( I_item,
                                  I_supplier,
                                  I_primary_sup_ind,
                                  I_vpn,
                                  I_supp_label,
                                  I_pallet_name,
                                  I_case_name,
                                  I_inner_name,
                                  I_consignment_rate,
                                  'N',
                                  sysdate,
                                  sysdate,
                                  get_user,
                                  NULL);
   end if;  -- item exists on item_supplier
   ---
   open C_GET_DLVRY_COUNTRY_ID;
   fetch C_GET_DLVRY_COUNTRY_ID into L_dlvry_country_id;
   close C_GET_DLVRY_COUNTRY_ID;
   ---
   if ITEM_COST_SQL.GET_DEFAULT_PO_COST(O_error_message,
                                        L_default_po_cost,
                                        L_dlvry_country_id,
                                        NULL) = FALSE then
      return FALSE;
   end if;
   ---
   if not SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS( O_error_message,
                                                  L_country_exist,
                                                  I_item,
                                                  I_supplier,
                                                  I_origin_country) then
      return FALSE;
   end if;
   ---
   if L_country_exist then
      --- determine the cost level of the item (item/supp/country or item/supp/country/loc)
      if not SUPP_ITEM_SQL.COST_CONTROL_LEVEL( O_error_message,
                                               L_cost_ctrl_level,
                                               I_item,
                                               I_supplier,
                                               I_origin_country) then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
      open C_LOCK_ITEM_SUPP_COUNTRY;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
      close C_LOCK_ITEM_SUPP_COUNTRY;
      ---
      if I_supp_hier_lvl_1 is not null then
         ---
         if DEAL_VALIDATE_SQL.CHK_ACTIVE_DEALS_SUP_HIER(O_error_message,
                                                        L_exist,
                                                        I_item,
                                                        I_origin_country,
                                                        I_supp_hier_lvl_1,
                                                        'S1') = FALSE then
            return FALSE;
         end if;
         ---
         if L_exist = TRUE then
            O_partner_upd_fail := TRUE;
            L_supp_hier_lvl_1 := NULL;
         else
            O_partner_upd_fail := FALSE;
            L_supp_hier_lvl_1 := I_supp_hier_lvl_1;
         end if;
         ---
      end if;
      ---
      if I_supp_hier_lvl_2 is not null then
         ---
         if DEAL_VALIDATE_SQL.CHK_ACTIVE_DEALS_SUP_HIER(O_error_message,
                                                        L_exist,
                                                        I_item,
                                                        I_origin_country,
                                                        I_supp_hier_lvl_2,
                                                        'S2') = FALSE then
            return FALSE;
         end if;
         ---
         if L_exist = TRUE then
            O_partner_upd_fail := TRUE;
            L_supp_hier_lvl_2 := NULL;
         else
            O_partner_upd_fail := FALSE;
            L_supp_hier_lvl_2 := I_supp_hier_lvl_2;
         end if;
         ---
      end if;
      ---
      if I_supp_hier_lvl_3 is not null then
         ---
         if DEAL_VALIDATE_SQL.CHK_ACTIVE_DEALS_SUP_HIER(O_error_message,
                                                        L_exist,
                                                        I_item,
                                                        I_origin_country,
                                                        I_supp_hier_lvl_3,
                                                        'S3') = FALSE then
            return FALSE;
         end if;
         ---
         if L_exist = TRUE then
            O_partner_upd_fail := TRUE;
            L_supp_hier_lvl_3 := NULL;
         else
            O_partner_upd_fail := FALSE;
            L_supp_hier_lvl_3 := I_supp_hier_lvl_3;
         end if;
         ---
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_COST_HEAD','ITEM_COST_HEAD','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country ||
                       ' Delivery Country: '|| L_import_country_id);
      open C_LOCK_ITEM_COST_HEAD;
      SQL_LIB.SET_MARK('FETCH','C_LOCK_ITEM_COST_HEAD','ITEM_COST_HEAD','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country ||
                       ' Delivery Country: '|| L_import_country_id);
      fetch C_LOCK_ITEM_COST_HEAD into L_item_cost_head;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_COST_HEAD','ITEM_COST_HEAD','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country ||
                       ' Delivery Country: '|| L_import_country_id);
      close C_LOCK_ITEM_COST_HEAD;
      ---
      if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                 L_country_attrib_row,
                                                 L_default_loc,
                                                 L_import_country_id)=FALSE then
         return FALSE;
      end if;
      ---
      if L_system_options_rec.default_tax_type = 'GTAX' then
         if L_country_attrib_row.item_cost_tax_incl_ind = 'Y' then
            L_negotiated_item_cost := I_unit_cost;
         else
            L_base_cost := I_unit_cost;
         end if;

         if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                             L_base_cost,
                                             L_extended_base_cost,
                                             L_inclusive_cost,
                                             L_negotiated_item_cost,
                                             I_item,
                                             L_item_cost_head.nic_static_ind,
                                             I_supplier,
                                             L_country_attrib_row.default_loc,
                                             L_country_attrib_row.default_loc_type,
                                             'ITEMSUPPCTRY',
                                             I_origin_country,
                                             L_import_country_id,
                                             L_item_cost_head.prim_dlvy_ctry_ind,
                                             'Y',
                                             'N') = FALSE then
               return FALSE;
         end if;
      else
         L_base_cost             := NULL;
         L_negotiated_item_cost  := NULL;
         L_extended_base_cost    := NULL;
         L_inclusive_cost        := NULL;
      end if;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);

      update item_supp_country
         set origin_country_id   = NVL(I_origin_country,origin_country_id),
             unit_cost           = DECODE(L_default_po_cost,'NIC',DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_negotiated_item_cost, unit_cost), NVL(I_unit_cost,unit_cost)),
                                                            'BC', DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_base_cost, unit_cost), NVL(I_unit_cost,unit_cost))),
             lead_time           = NVL(I_lead_time,lead_time),
             supp_pack_size      = NVL(I_supp_pack_size,supp_pack_size),
             inner_pack_size     = NVL(I_inner_pack_size,inner_pack_size),
             min_order_qty       = NVL(I_min_order_qty,min_order_qty),
             max_order_qty       = NVL(I_max_order_qty,max_order_qty),
             packing_method      = NVL(I_packing_method,packing_method),
             primary_supp_ind    = NVL(I_primary_sup_ind,primary_supp_ind),
             primary_country_ind = NVL(I_primary_cnt_ind,primary_country_ind),
             supp_hier_lvl_1     = NVL(L_supp_hier_lvl_1, supp_hier_lvl_1),
             supp_hier_lvl_2     = NVL(L_supp_hier_lvl_2, supp_hier_lvl_2),
             supp_hier_lvl_3     = NVL(L_supp_hier_lvl_3, supp_hier_lvl_3),
             last_update_id       = get_user,
             last_update_datetime = sysdate,
             negotiated_item_cost = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_negotiated_item_cost, negotiated_item_cost), NULL),
             extended_base_cost   = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_extended_base_cost, extended_base_cost), NULL),
             inclusive_cost       = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_inclusive_cost, inclusive_cost), NULL),
             base_cost            = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_base_cost,base_cost), NULL)
       where item     = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country;
      ---
   else
      ---
      if L_system_options_rec.default_tax_type = 'GTAX' then
         if L_country_attrib_row.item_cost_tax_incl_ind = 'Y' then
            L_negotiated_item_cost := I_unit_cost;
         else
            L_base_cost := I_unit_cost;
         end if;
         if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                             L_base_cost,
                                             L_extended_base_cost,
                                             L_inclusive_cost,
                                             L_negotiated_item_cost,
                                             I_item,
                                             L_item_cost_head.nic_static_ind,
                                             I_supplier,
                                             L_country_attrib_row.default_loc,
                                             L_country_attrib_row.default_loc_type) = FALSE then
               return FALSE;
         end if;
      else
         L_base_cost             := NULL;
         L_negotiated_item_cost  := NULL;
         L_extended_base_cost    := NULL;
         L_inclusive_cost        := NULL;
      end if;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY',NULL);
      ---
      insert into item_supp_country(item,
                                    supplier,
                                    origin_country_id,
                                    unit_cost,
                                    lead_time,
                                    supp_pack_size,
                                    inner_pack_size,
                                    min_order_qty,
                                    max_order_qty,
                                    round_lvl,
                                    round_to_inner_pct,
                                    round_to_case_pct,
                                    round_to_layer_pct,
                                    round_to_pallet_pct,
                                    packing_method,
                                    primary_supp_ind,
                                    primary_country_ind,
                                    default_uop,
                                    ti,
                                    hi,
                                    supp_hier_type_1,
                                    supp_hier_lvl_1,
                                    supp_hier_type_2,
                                    supp_hier_lvl_2,
                                    supp_hier_type_3,
                                    supp_hier_lvl_3,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id,
                                    negotiated_item_cost,
                                    extended_base_cost,
                                    inclusive_cost,
                                    base_cost)
                             select I_item,
                                    I_supplier,
                                    I_origin_country,
                                    DECODE(L_default_po_cost,'NIC',DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_negotiated_item_cost, unit_cost), NVL(I_unit_cost,unit_cost)),
                                                             'BC', DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_base_cost, unit_cost), NVL(I_unit_cost,unit_cost))),
                                    I_lead_time,
                                    I_supp_pack_size,
                                    I_inner_pack_size,
                                    I_min_order_qty,
                                    I_max_order_qty,
                                    isc.round_lvl,
                                    isc.round_to_inner_pct,
                                    isc.round_to_case_pct,
                                    isc.round_to_layer_pct,
                                    isc.round_to_pallet_pct,
                                    I_packing_method,
                                    I_primary_sup_ind,
                                    I_primary_cnt_ind,
                                    I_default_uop,
                                    I_ti,
                                    I_hi,
                                    'S1',
                                    I_supp_hier_lvl_1,
                                    'S2',
                                    I_supp_hier_lvl_2,
                                    'S3',
                                    I_supp_hier_lvl_3,
                                    sysdate,
                                    sysdate,
                                    get_user,
                                    L_negotiated_item_cost,
                                    L_extended_base_cost,
                                    L_inclusive_cost,
                                    L_base_cost
                               from item_supp_country isc
                              where isc.item              = I_item_parent
                                and isc.supplier          = I_supplier
                                and isc.origin_country_id = I_origin_country;

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_MANU_COUNTRY',NULL);
      insert into item_supp_manu_country(item,
                                         supplier,
                                         manu_country_id,
                                         primary_manu_ctry_ind)
                                  select I_item,
                                         I_supplier,
                                         manu_country_id,
                                         'Y'
                                    from item_supp_manu_country
                                   where item                  = I_item_parent
                                     and supplier              = I_supplier
                                     and primary_manu_ctry_ind = 'Y';
   end if;
   ---
   if not ITEM_SUPP_COUNTRY_LOC_SQL.GET_PRIMARY_LOC(O_error_message,
                                                    L_exist,
                                                    L_primary_loc,
                                                    I_item,
                                                    I_supplier,
                                                    I_origin_country) then
      return FALSE;
   end if;
   ---
   if L_exist then
      if L_cost_ctrl_level = 'Item_Supp_Country_Loc' then
         L_loc := L_primary_loc;
      else
         L_loc := NULL;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                       'ITEM_SUPP_COUNTRY_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country||' Location: '||nvl(to_char(L_loc), 'ALL') );
      open C_LOCK_ITEM_SUPP_COUNTRY_LOC;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                       'ITEM_SUPP_COUNTRY_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country||' Location: '||nvl(to_char(L_loc), 'ALL') );
      close C_LOCK_ITEM_SUPP_COUNTRY_LOC;
      ---
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_SUPP_COUNTRY_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country||' Location: '||nvl(to_char(L_loc), 'ALL') );
      ---
      if L_system_options_rec.default_tax_type = 'GTAX' then
         if L_country_attrib_row.item_cost_tax_incl_ind = 'Y' then
            L_negotiated_item_cost := I_unit_cost;
         else
            L_base_cost := I_unit_cost;
         end if;
         ---
         if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                             L_base_cost,
                                             L_extended_base_cost,
                                             L_inclusive_cost,
                                             L_negotiated_item_cost,
                                             I_item,
                                             L_item_cost_head.nic_static_ind,
                                             I_supplier,
                                             L_country_attrib_row.default_loc,
                                             L_country_attrib_row.default_loc_type) = FALSE then
               return FALSE;
         end if;
      else
         L_base_cost                := NULL;
         L_negotiated_item_cost     := NULL;
         L_extended_base_cost       := NULL;
         L_inclusive_cost           := NULL;
      end if;
      ---
      update item_supp_country_loc
         set unit_cost            = DECODE(L_default_po_cost,'NIC',DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_negotiated_item_cost, unit_cost), NVL(I_unit_cost,unit_cost)),
                                                             'BC',DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_base_cost, unit_cost), NVL(I_unit_cost,unit_cost))),
             supp_hier_lvl_1      = NVL(L_supp_hier_lvl_1, supp_hier_lvl_1),
             supp_hier_lvl_2      = NVL(L_supp_hier_lvl_2, supp_hier_lvl_2),
             supp_hier_lvl_3      = NVL(L_supp_hier_lvl_3, supp_hier_lvl_3),
             last_update_id       = get_user,
             last_update_datetime = sysdate,
             negotiated_item_cost = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_negotiated_item_cost, negotiated_item_cost), NULL),
             extended_base_cost   = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_extended_base_cost, extended_base_cost), NULL),
             inclusive_cost       = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_inclusive_cost, inclusive_cost), NULL),
             base_cost            = DECODE(L_system_options_rec.default_tax_type, 'GTAX', NVL(L_base_cost, base_cost), NULL)
       where item     = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country
         and loc = NVL(L_loc, loc);
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_SUPPLIER_REC_LOCK',
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_INSERT_ITEM;
---------------------------------------------------------------------------
FUNCTION UPDATE_FASHPACK(O_error_message     IN OUT VARCHAR2,
                         O_item_rejected     IN OUT BOOLEAN,
                         I_item_parent       IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_no           IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_tmpl_id      IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                         I_order_no          IN     ORDSKU.ORDER_NO%TYPE,
                         I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                         I_item_parent_desc  IN     ITEM_MASTER.ITEM_DESC%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_unit_cost         IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   return BOOLEAN IS

   L_program         VARCHAR2(64) := 'PACKITEM_ADD_SQL.UPDATE_FASHPACK';
   L_location        ORDLOC.LOCATION%TYPE            := NULL;
   L_supp_unit_cost  ORDLOC.UNIT_COST%TYPE;
   L_unit_cost_ord   ORDLOC.UNIT_COST%TYPE;
   L_unit_retail     ORDLOC.UNIT_RETAIL%TYPE;
   L_return_code     VARCHAR2(5)                     := NULL;
   L_dba_flag        VARCHAR2(1)                     := NULL;
   L_diff_1          PACK_TMPL_DETAIL.DIFF_1%TYPE;
   L_diff_1_desc     DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_2          PACK_TMPL_DETAIL.DIFF_2%TYPE;
   L_diff_2_desc     DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_3          PACK_TMPL_DETAIL.DIFF_3%TYPE;
   L_diff_3_desc     DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_4          PACK_TMPL_DETAIL.DIFF_4%TYPE;
   L_diff_4_desc     DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_type       DIFF_IDS.DIFF_TYPE%TYPE;
   L_diff_id_group   VARCHAR2(5);
   L_pack_tmpl_desc  PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE := NULL;
   L_seq_no          PACK_TMPL_DETAIL.SEQ_NO%TYPE;
   L_partner         BOOLEAN;
   L_diff1_mult      VARCHAR2(1)                     := NULL;
   L_diff2_mult      VARCHAR2(1)                     := NULL;
   L_diff3_mult      VARCHAR2(1)                     := NULL;
   L_diff4_mult      VARCHAR2(1)                     := NULL;
   L_no_of_diff1     NUMBER(3);
   L_no_of_diff2     NUMBER(3);
   L_no_of_diff3     NUMBER(3);
   L_no_of_diff4     NUMBER(3);
   L_diff_no                NUMBER(1);
   L_parent_desc_length     NUMBER;
   L_diff_desc_length       NUMBER;
   L_currency_code_ord      ORDHEAD.CURRENCY_CODE%TYPE;
   L_exchange_rate_ord      ORDHEAD.EXCHANGE_RATE%TYPE;
   L_currency_code_supp     SUPS.CURRENCY_CODE%TYPE;
  

   cursor C_diff_mult is
      select count(distinct diff_1),
             count(distinct diff_2),
             count(distinct diff_3),
             count(distinct diff_4)
        from pack_tmpl_detail
       where pack_tmpl_id = I_pack_tmpl_id;

   cursor C_PACK_TMPL is
      select diff_1,
             diff_2,
             diff_3,
             diff_4
        from pack_tmpl_detail
       where pack_tmpl_id = I_pack_tmpl_id;

   cursor C_seq_no is
      select seq_no
        from pack_tmpl_detail
       where pack_tmpl_id = I_pack_tmpl_id;

   cursor C_SUPP_UNIT_COST is
      select sum(v.qty * i.unit_cost)
        from item_supp_country i,
             v_packsku_qty v
       where v.pack_no = I_pack_no
         and v.item = i.item
         and i.supplier = I_supplier
         and origin_country_id = I_origin_country_id;

   cursor C_LOC_EXISTS is
      select v.item,
             ol.location,
             ol.loc_type
        from ordloc ol,
             v_packsku_qty v
       where ol.order_no = I_order_no
         and ol.item = I_pack_no
         and v.pack_no = I_pack_no;

   cursor C_ORDLOC is
      select location,
             loc_type
        from ordloc
       where order_no = I_order_no
         and item     = I_pack_no
         for update of unit_retail nowait;

   cursor C_UNIT_RETAIL_LOC is
      select sum(v.qty * r.unit_retail)
        from item_loc r,
             v_packsku_qty v
       where v.pack_no = I_pack_no
         and v.item = r.item
         and r.loc = L_location;

   cursor C_ORDHEAD is
     select import_country_id,
            currency_code,
            exchange_rate
       from ordhead
      where order_no = I_order_no;
      
   cursor C_SUPS is
     select currency_code
       from sups
      where supplier = I_supplier;
      

BEGIN

   open C_ORDHEAD;
   fetch C_ORDHEAD into L_import_country_id,
                        L_currency_code_ord,
                        L_exchange_rate_ord;
   close C_ORDHEAD;

   open C_SUPS;
   fetch C_SUPS into L_currency_code_supp;
   close C_SUPS;
   

   ---call PACKITEM_ADD_SQL.DELETE_PACKITEM_BREAKOUT with I_sku = NULL
   if PACKITEM_ADD_SQL.DELETE_PACKITEM_BREAKOUT(O_error_message,
                                                I_pack_no,
                                                NULL,
                                                I_item_parent,
                                                'F') = FALSE then
      return FALSE;
   end if;

   if PACKITEM_ADD_SQL.PARENT_PACK_TMPL(O_error_message,
                                        O_item_rejected,
                                        I_pack_no,
                                        I_item_parent,
                                        I_pack_tmpl_id,
                                        1,
                                        I_supplier,
                                        I_origin_country_id) = FALSE then
      return FALSE;
   end if;


   open C_diff_mult;
   fetch C_diff_mult into L_no_of_diff1,
                          L_no_of_diff2,
                          L_no_of_diff3,
                          L_no_of_diff4;
   close C_diff_mult;
   ---

   if L_no_of_diff1 > 1 then
      L_diff1_mult := '+';
   end if;
   if L_no_of_diff2 > 1 then
      L_diff2_mult := '+';
   end if;
   if L_no_of_diff3 > 1 then
      L_diff3_mult := '+';
   end if;
   if L_no_of_diff4 > 1 then
      L_diff4_mult := '+';
   end if;


   open C_seq_no;
   fetch C_seq_no into L_seq_no;
   close C_seq_no;


   open C_pack_tmpl;
   fetch C_pack_tmpl into L_diff_1, L_diff_2, L_diff_3, L_diff_4;
   close C_pack_tmpl;

   if L_diff_4 is NOT NULL then
      L_diff_no := 4;
   elsif L_diff_3 is NOT NULL then
      L_diff_no := 3;
   elsif L_diff_2 is NOT NULL then
      L_diff_no := 2;
   else
      L_diff_no := 1;
   end if;

   if lengthb(I_item_parent_desc) < 150 then
      L_parent_desc_length := lengthb(I_item_parent_desc);
      L_diff_desc_length := trunc( ( (246 - (L_diff_no * 3) - 1) - L_parent_desc_length) / L_diff_no );
   else
      if L_diff_no = 4 then
         L_parent_desc_length := 137;
         L_diff_desc_length   := 24;
      elsif L_diff_no = 3 then
         L_parent_desc_length := 143;
         L_diff_desc_length   := 31;
      elsif L_diff_no = 2 then
         L_parent_desc_length := 149;
         L_diff_desc_length   := 45;
      else
         L_parent_desc_length := 167;
         L_diff_desc_length   := 75;
      end if;
   end if;

   if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                             L_diff_1_desc,
                             L_diff_type,
                             L_diff_id_group,
                             L_diff_1) = FALSE then
      return FALSE;
   end if;
   L_pack_tmpl_desc := L_pack_tmpl_desc || RTRIM(SUBSTRB(L_diff_1_desc,1,L_diff_desc_length))||':'||L_diff1_mult||':';
   ---
   if L_diff_2 is NOT NULL then
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                L_diff_2_desc,
                                L_diff_type,
                                L_diff_id_group,
                                L_diff_2) = FALSE then
         return FALSE;
      end if;
      L_pack_tmpl_desc := L_pack_tmpl_desc || RTRIM(SUBSTRB(L_diff_2_desc,1,L_diff_desc_length))||':'||L_diff2_mult||':';
   end if;
   ---
   if L_diff_3 is NOT NULL then
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                L_diff_3_desc,
                                L_diff_type,
                                L_diff_id_group,
                                L_diff_3) = FALSE then
         return FALSE;
      end if;
      L_pack_tmpl_desc := L_pack_tmpl_desc || RTRIM(SUBSTRB(L_diff_3_desc,1,L_diff_desc_length))||':'||L_diff3_mult||':';
   end if;
   ---
   if L_diff_4 is NOT NULL then
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                L_diff_4_desc,
                                L_diff_type,
                                L_diff_id_group,
                                L_diff_4) = FALSE then
         return FALSE;
      end if;
      L_pack_tmpl_desc := L_pack_tmpl_desc || RTRIM(SUBSTRB(L_diff_4_desc,1,L_diff_desc_length))||':'||L_diff4_mult||':';
   end if;
   ---
   L_pack_tmpl_desc := RTRIM(SUBSTRB(I_item_parent_desc,1,L_parent_desc_length))||':'
                    || L_pack_tmpl_desc
                    || to_char(L_seq_no);

   update pack_tmpl_head
      set pack_tmpl_desc = L_pack_tmpl_desc
    where pack_tmpl_id = I_pack_tmpl_id;


   update sups_pack_tmpl_desc
      set supp_pack_desc = L_pack_tmpl_desc
    where pack_tmpl_id = I_pack_tmpl_id
      and supplier = I_supplier;


   if I_unit_cost is NULL then
      open C_SUPP_UNIT_COST;
      fetch C_SUPP_UNIT_COST into L_supp_unit_cost;
      close C_SUPP_UNIT_COST;
   else
      L_supp_unit_cost := I_unit_cost;
   end if;


   if PACKITEM_ADD_SQL.UPDATE_INSERT_ITEM(O_error_message,
                                          L_partner,
                                          I_pack_no,
                                          I_item_parent,
                                          I_supplier,
                                          I_origin_country_id,
                                          L_supp_unit_cost,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL) = FALSE then
      return FALSE;
   end if;
   ---
   for C_rec in C_LOC_EXISTS LOOP
      if NEW_ITEM_LOC(O_error_message,
                        C_rec.item,
                        C_rec.location,
                        NULL,
                        NULL,
                        C_rec.loc_type,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        I_ranged_ind => 'Y') = FALSE then
         return FALSE;
      end if;
   end LOOP;
   ---
   for rec1 in C_ORDLOC LOOP
      L_location := rec1.location;
      ---
      open C_UNIT_RETAIL_LOC;
      fetch C_UNIT_RETAIL_LOC into L_unit_retail;
      close C_UNIT_RETAIL_LOC;
      ---
      update ordloc
        set  unit_retail = L_unit_retail
       where current of C_ORDLOC;
   end LOOP;
   ---
   if L_currency_code_supp <> L_currency_code_ord then
      --  convert the unit cost from supplier currency to order currency
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_supp_unit_cost,
                              L_currency_code_supp,
                              L_currency_code_ord,
                              L_unit_cost_ord,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              L_exchange_rate_ord) = FALSE then
         return FALSE;
      end if;
   else
      L_unit_cost_ord := L_supp_unit_cost;
   end if;
   if ORDER_ITEM_ATTRIB_SQL.UPDATE_UNIT_COST(O_error_message,
                                             I_order_no,
                                             I_pack_no,
                                             L_unit_cost_ord,
                                             'NORM') = FALSE then
      return FALSE;
   end if;
   ---
   if DEAL_SQL.INSERT_DEAL_CALC_QUEUE(O_error_message,
                                      I_order_no,
                                      'N',
                                      'N',
                                      'N') = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_FASHPACK;
--------------------------------------------------------------------------
FUNCTION INSERT_FASHPACK(O_error_message        IN OUT VARCHAR2,
                         O_item_rejected        IN OUT BOOLEAN,
                         I_order_no             IN ORDSKU.ORDER_NO%TYPE,
                         I_item_parent          IN ITEM_MASTER.ITEM_PARENT%TYPE,
                         I_supplier             IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                         I_pack_type            IN PACK_TMPL_HEAD.PACK_TYPE%TYPE,
                         I_order_as_type        IN ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                         I_receive_as_type      IN ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_item_parent_desc     IN ITEM_MASTER.ITEM_DESC%TYPE,
                         I_origin_country_id    IN ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_unit_cost            IN ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   return BOOLEAN IS

   L_program         VARCHAR2(64) := 'PACKITEM_ADD_SQL.INSERT_FASHPACK';
   L_ret_code        VARCHAR2(6);
   L_qty             temp_pack_tmpl.qty%TYPE;
   L_seq_no          pack_tmpl_head.pack_tmpl_id%TYPE;
   L_pack_tmpl_id    pack_tmpl_head.pack_tmpl_id%TYPE  := NULL;
   L_pack_tmpl_desc  pack_tmpl_head.pack_tmpl_desc%TYPE;
   L_short_desc      pack_tmpl_head.pack_tmpl_desc%TYPE;
   L_pack_no         packitem.pack_no%TYPE;
   L_diff1_mult      VARCHAR2(1);
   L_diff2_mult      VARCHAR2(1);
   L_diff3_mult      VARCHAR2(1);
   L_diff4_mult      VARCHAR2(1);
   L_no_of_diff1     NUMBER(3);
   L_no_of_diff2     NUMBER(3);
   L_no_of_diff3     NUMBER(3);
   L_no_of_diff4     NUMBER(3);
   L_diff_1          PACK_TMPL_DETAIL.DIFF_1%TYPE;
   L_diff_1_desc     DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_2          PACK_TMPL_DETAIL.DIFF_2%TYPE;
   L_diff_2_desc     DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_3          PACK_TMPL_DETAIL.DIFF_3%TYPE;
   L_diff_3_desc     DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_4          PACK_TMPL_DETAIL.DIFF_4%TYPE;
   L_diff_4_desc     DIFF_IDS.DIFF_DESC%TYPE;
   L_diff_type       DIFF_IDS.DIFF_TYPE%TYPE;
   L_diff1_id_group  VARCHAR2(10);
   L_diff2_id_group  VARCHAR2(10);
   L_diff3_id_group  VARCHAR2(10);
   L_diff4_id_group  VARCHAR2(10);
   L_wh              WH.WH%TYPE;
   L_dept            ITEM_MASTER.DEPT%TYPE;
   L_class           ITEM_MASTER.CLASS%TYPE;
   L_sclass          ITEM_MASTER.SUBCLASS%TYPE;
   L_vdate           PERIOD.VDATE%TYPE    :=  GET_VDATE;
   L_parent_diff1    ITEM_MASTER.DIFF_1%TYPE;
   L_parent_diff2    ITEM_MASTER.DIFF_2%TYPE;
   L_parent_diff3    ITEM_MASTER.DIFF_3%TYPE;
   L_parent_diff4    ITEM_MASTER.DIFF_4%TYPE;
   L_diff_no                NUMBER(1);
   L_parent_desc_length     NUMBER;
   L_diff_desc_length       NUMBER;
   L_item_cost_tbl           OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();
   L_item_loc_tbl            OBJ_ITEMLOC_TBL := OBJ_ITEMLOC_TBL();
   L_item_loc_tbl_temp       OBJ_ITEMLOC_TBL := OBJ_ITEMLOC_TBL();
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_system_options_rec      SYSTEM_OPTIONS%ROWTYPE;
   
   cursor C_pack_head is
      select distinct pack_tmpl_id
        from temp_pack_tmpl;

   cursor C_diff_mult is
      select count(distinct diff1),
             count(distinct diff2),
             count(distinct diff3),
             count(distinct diff4)
        from temp_pack_tmpl
       where pack_tmpl_id = L_seq_no;

   cursor C_PARENT_DIFFS is
      select diff_1,
             diff_2,
             diff_3,
             diff_4
        from item_master
       where item = I_item_parent;

   cursor C_pack_detail is
      select diff1,
             diff2,
             diff3,
             diff4,
             qty
        from temp_pack_tmpl
       where pack_tmpl_id = L_seq_no;

   cursor C_ordhead is
     select import_country_id
       from ordhead
      where order_no = I_order_no;

BEGIN
   ---
   --dbms_session.set_sql_trace(TRUE);
   LP_item_cost_tbl.DELETE();
   ---
   open C_ordhead;
   fetch C_ordhead into L_import_country_id;
   close C_ordhead;
   ---

   FOR C_rec in C_pack_head LOOP
      L_seq_no := C_rec.pack_tmpl_id;


      L_diff1_mult := NULL;
      L_diff2_mult := NULL;
      L_diff3_mult := NULL;
      L_diff4_mult := NULL;
      ---
      open C_diff_mult;
      fetch C_diff_mult into L_no_of_diff1,
                             L_no_of_diff2,
                             L_no_of_diff3,
                             L_no_of_diff4;
      close C_diff_mult;
      ---
      if L_no_of_diff1 > 1 then
         L_diff1_mult := '+';
      end if;
      ---
      if L_no_of_diff2 > 1 then
         L_diff2_mult := '+';
      end if;
      ---
      if L_no_of_diff3 > 1 then
         L_diff3_mult := '+';
      end if;
      ---
      if L_no_of_diff4 > 1 then
         L_diff4_mult := '+';
      end if;


      PACK_TEMPLATE_SQL.NEXT_PACK_TEMPLATE_ID(O_error_message,
                                              L_pack_tmpl_id,
                                              L_ret_code);
      if L_ret_code = 'FALSE' then
         return FALSE;
      end if;


      open C_PACK_DETAIL;
      fetch C_PACK_DETAIL into L_diff_1,
                               L_diff_2,
                               L_diff_3,
                               L_diff_4,
                               L_qty;
      close C_PACK_DETAIL;

      if L_diff_4 is NOT NULL then
         L_diff_no := 4;
      elsif L_diff_3 is NOT NULL then
         L_diff_no := 3;
      elsif L_diff_2 is NOT NULL then
         L_diff_no := 2;
      else
         L_diff_no := 1;
      end if;

      if lengthb(I_item_parent_desc) < 150 then
         L_parent_desc_length := lengthb(I_item_parent_desc);
         L_diff_desc_length := trunc( ( (246 - (L_diff_no * 3) - 1) - L_parent_desc_length) / L_diff_no );
      else
         if L_diff_no = 4 then
            L_parent_desc_length := 137;
            L_diff_desc_length   := 24;
         elsif L_diff_no = 3 then
            L_parent_desc_length := 143;
            L_diff_desc_length   := 31;
         elsif L_diff_no = 2 then
            L_parent_desc_length := 149;
            L_diff_desc_length   := 45;
         else
            L_parent_desc_length := 167;
            L_diff_desc_length   := 75;
         end if;
      end if;


      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                L_diff_1_desc,
                                L_diff_type,
                                L_diff1_id_group,
                                L_diff_1) = FALSE then
         return FALSE;
      end if;
      L_pack_tmpl_desc := RTRIM(SUBSTRB(L_diff_1_desc,1,L_diff_desc_length))||':'||L_diff1_mult||':';
      ---
      if L_diff_2 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff_2_desc,
                                   L_diff_type,
                                   L_diff2_id_group,
                                   L_diff_2) = FALSE then
            return FALSE;
         end if;
         L_pack_tmpl_desc :=  L_pack_tmpl_desc || RTRIM(SUBSTRB(L_diff_2_desc,1,L_diff_desc_length))||':'||L_diff2_mult||':';
      end if;
      ---
      if L_diff_3 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff_3_desc,
                                   L_diff_type,
                                   L_diff3_id_group,
                                   L_diff_3) = FALSE then
            return FALSE;
         end if;
         L_pack_tmpl_desc :=  L_pack_tmpl_desc || RTRIM(SUBSTRB(L_diff_3_desc,1,L_diff_desc_length))||':'||L_diff3_mult||':';
      end if;
      ---
      if L_diff_4 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                   L_diff_4_desc,
                                   L_diff_type,
                                   L_diff4_id_group,
                                   L_diff_4) = FALSE then
            return FALSE;
         end if;
         L_pack_tmpl_desc := L_pack_tmpl_desc || RTRIM(SUBSTRB(L_diff_4_desc,1,L_diff_desc_length))||':'||L_diff4_mult||':';
      end if;


      L_pack_tmpl_desc := RTRIM(SUBSTRB(I_item_parent_desc,1,L_parent_desc_length))||':'
                       || L_pack_tmpl_desc
                       || to_char(L_seq_no);
      ---
      L_short_desc := RTRIM(SUBSTRB(L_pack_tmpl_desc,1,120));

      open C_PARENT_DIFFS;
      fetch C_PARENT_DIFFS into L_parent_diff1,
                                L_parent_diff2,
                                L_parent_diff3,
                                L_parent_diff4;
      close C_PARENT_DIFFS;

      if PACK_TEMPLATE_SQL.INSERT_HEAD(O_error_message,
                                       L_pack_tmpl_id,
                                       L_pack_tmpl_desc,
                                       I_pack_type,
                                       L_parent_diff1,
                                       L_parent_diff2,
                                       L_parent_diff3,
                                       L_parent_diff4,
                                       'Y',
                                       I_receive_as_type) = FALSE then
         return FALSE;
      end if;
      ---
      FOR C_rec in C_pack_detail LOOP
         if PACK_TEMPLATE_SQL.INSERT_UPDATE_PACK_TMPL_DETAIL(O_error_message,
                                                             L_pack_tmpl_id,
                                                             C_rec.diff1,
                                                             C_rec.diff2,
                                                             C_rec.diff3,
                                                             C_rec.diff4,
                                                             C_rec.qty) = FALSE then
            return FALSE;
         end if;
      END LOOP;
      ---
      if ITEM_ATTRIB_SQL.NEXT_ITEM(O_error_message,
                                   L_pack_no) = FALSE then
         return FALSE;
      end if;
      ---
      if PACKITEM_ADD_SQL.NEW_PACK_TMPL(O_error_message,
                                        O_item_rejected,
                                        L_pack_no,
                                        I_item_parent,
                                        L_pack_tmpl_id,
                                        I_pack_type,
                                        I_supplier,
                                        L_pack_tmpl_desc,
                                        L_short_desc,
                                        L_pack_tmpl_desc,
                                        I_order_as_type,
                                        I_origin_country_id,
                                        I_unit_cost,
                                        L_import_country_id) = FALSE then
         return FALSE;
      end if;
      ---
      insert into ordloc_wksht (order_no,
                                item,
                                origin_country_id,
                                uop,
                                standard_uom,
                                supp_pack_size)
                        values (I_order_no,
                                L_pack_no,
                                I_origin_country_id,
                                'EA',
                                'EA',
                                1);
   END LOOP;
   ---
   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;

   if L_system_options_rec.default_tax_type = 'GTAX' then
       
    --- calculate cost and insert into item_cost_head and item_cost_detail
    if LP_item_cost_tbl is NOT NULL and LP_item_cost_tbl.count > 0 then
    if ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK(O_error_message,
              LP_item_cost_tbl) = FALSE then
    return FALSE;
    end if;

    merge into item_supp_country i
    using (select ic.I_item item,
      ic.I_supplier supplier,
      ic.I_origin_country_id origin_country_id,
      ic.O_base_cost,
      ic.O_extended_base_cost,
      ic.O_inclusive_cost,
      ic.O_negotiated_item_cost,
      c.default_po_cost
       from TABLE(CAST(LP_item_cost_tbl as OBJ_COMP_ITEM_COST_TBL)) ic,
      country_attrib c
      where ic.I_origin_country_id = c.country_id
    ) use_this
    on (i.item = use_this.item and
     i.supplier = use_this.supplier and
     i.origin_country_id = use_this.origin_country_id)
    when matched then
    update
    set i.unit_cost = DECODE(use_this.default_po_cost,
         'NIC',NVL(use_this.O_negotiated_item_cost,i.unit_cost),
         'BC',NVL(use_this.O_base_cost,i.unit_cost)),
     i.negotiated_item_cost = use_this.O_negotiated_item_cost,
     i.extended_base_cost = use_this.O_extended_base_cost,
     i.inclusive_cost = use_this.O_inclusive_cost,
     i.base_cost = use_this.O_base_cost;
    end if;
   end if;    
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_FASHPACK;
-----------------------------------------------------------------------------------
FUNCTION CHECK_INSERT(O_error_message IN OUT VARCHAR2,
                      O_valid         IN OUT BOOLEAN,
                      I_item_parent   IN     ITEM_MASTER.ITEM%TYPE,
                      I_pack_tmpl_id  IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
   RETURN BOOLEAN is

   L_program VARCHAR2(50) := 'PACKITEM_ADD_SQL.CHECK_INSERT';
   L_exists  VARCHAR2(1);

   cursor C_CHECK_EXISTING is
      select 'x'
        from pack_tmpl_detail ptd,
             item_master im,
             item_supp_country isc
       where im.item_parent             = I_item_parent
         and ptd.pack_tmpl_id           = I_pack_tmpl_id
         and im.item                    = isc.item
         and ptd.diff_1                 = im.diff_1
         and NVL(ptd.diff_2, im.diff_2) = im.diff_2
         and NVL(ptd.diff_3, im.diff_3) = im.diff_3
         and NVL(ptd.diff_4, im.diff_4) = im.diff_4
         and im.status                  = 'A'
         and im.item_level              = im.tran_level
         and exists (select 'x'
                       from item_supp_country isc2
                      where isc.supplier          = isc2.supplier
                        and isc.origin_country_id = isc2.origin_country_id
                        and isc2.item             = im.item_parent);

   cursor C_CHECK_NEW is
      select 'x'
        from pack_tmpl_detail ptd
       where ptd.pack_tmpl_id           = I_pack_tmpl_id
         and not exists (select 'x'
                           from item_master im
                          where im.item_parent = I_item_parent
                            and im.diff_1      = ptd.diff_1
                            and im.diff_2      = NVL(ptd.diff_2, im.diff_2)
                            and im.diff_3      = NVL(ptd.diff_3, im.diff_3)
                            and im.diff_4      = NVL(ptd.diff_4, im.diff_4));

BEGIN

   if I_item_parent is NULL or
      I_pack_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   O_valid := TRUE;
   ---
   --- Check for any diff_1/diff_2 combinations not existing for the parent/pack template
   open C_CHECK_NEW;
   fetch C_CHECK_NEW into L_exists;
   if C_CHECK_NEW%NOTFOUND then
      close C_CHECK_NEW;
      ---
      --- Check for existing diff_1/diff_2 combos that are approved and supplied by the same
      --- supplier/origin country ID as the parent.
      open C_CHECK_EXISTING;
      fetch C_CHECK_EXISTING into L_exists;
      if C_CHECK_EXISTING%NOTFOUND then
         O_valid := FALSE;
      end if;
      ---
      close C_CHECK_EXISTING;
   else
      close C_CHECK_NEW;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_INSERT;
------------------------------------------------------------------------
FUNCTION VALID_ITEM (O_error_message  IN OUT VARCHAR2,
                     O_valid          IN OUT BOOLEAN,
                     O_item_pack_ind  IN OUT VARCHAR2,
                     I_pack_rec       IN     ITEM_MASTER%ROWTYPE,
                     I_isc_tbl        IN     ISC_TBLTYPE,
                     I_pack_no        IN     PACKITEM.PACK_NO%TYPE,
                     I_item           IN     ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN is
   ---
   L_program  VARCHAR2(64)  := 'PACKITEM_ADD_SQL.VALID_ITEM';
   ---
   L_dept_level_orders       PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE;
   L_exists                  VARCHAR2(1);
   L_exists_in_pack          BOOLEAN;
   ---
   L_supplier                ITEM_SUPPLIER.SUPPLIER%TYPE;
   L_origin_country_id       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_pack_no                 ITEM_MASTER.ITEM%TYPE                 := I_pack_rec.item;
   L_pack_dept               ITEM_MASTER.DEPT%TYPE                 := I_pack_rec.dept;
   L_pack_pack_ind           ITEM_MASTER.PACK_IND%TYPE             := I_pack_rec.pack_ind;
   L_pack_pack_type          ITEM_MASTER.PACK_TYPE%TYPE            := I_pack_rec.pack_type;
   L_pack_order_as_type      ITEM_MASTER.ORDER_AS_TYPE%TYPE        := I_pack_rec.order_as_type;
   L_pack_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE        := I_pack_rec.orderable_ind;
   L_pack_contains_inner_ind ITEM_MASTER.CONTAINS_INNER_IND%TYPE   := I_pack_rec.contains_inner_ind;
   ---
   L_item_dept               ITEM_MASTER.DEPT%TYPE                 := NULL;
   L_item_level              ITEM_MASTER.ITEM_LEVEL%TYPE           := NULL;
   L_tran_level              ITEM_MASTER.TRAN_LEVEL%TYPE           := NULL;
   L_item_pack_ind           ITEM_MASTER.PACK_IND%TYPE             := NULL;
   L_item_pack_type          ITEM_MASTER.PACK_TYPE%TYPE            := NULL;
   L_item_order_as_type      ITEM_MASTER.ORDER_AS_TYPE%TYPE        := NULL;
   L_item_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_item_contains_inner_ind ITEM_MASTER.CONTAINS_INNER_IND%TYPE;
   L_item_purchase_type      DEPS.PURCHASE_TYPE%TYPE;
   ---
   cursor C_PROCUR_UNIT_OPTIONS is
      select dept_level_orders
        from procurement_unit_options;
   ---
   cursor C_ITEM_ATTRIB is
      select im.dept,
             im.item_level,
             im.tran_level,
             im.pack_ind,
             im.pack_type,
             im.order_as_type,
             im.orderable_ind,
             im.contains_inner_ind,
             d.purchase_type
        from item_master im,
             deps d
       where im.item = I_item
         and im.dept = d.dept;
   ---
   cursor C_CHECK_COMPONENT_ITEMS is
      select 'x'
        from packitem pi
       where pi.pack_no = I_item --- inner pack
         and not exists( select 'x'
                           from item_supp_country isc
                          where isc.item              = pi.item
                            and isc.supplier          = L_supplier
                            and isc.origin_country_id = L_origin_country_id );
   ---
   cursor C_CHECK_COMPONENT_ITEMSUPPCTRY is
      select 'x'
        from item_supp_country isc
       where isc.item              = I_item
         and isc.supplier          = L_supplier
         and isc.origin_country_id = L_origin_country_id;
   ---
   cursor GET_INVALID_INNER_PACK_DEPT is
      select 'x'
        from packitem pi,
             item_master im
       where pi.pack_no = I_item
         and pi.item    = im.item
         and im.dept   != L_pack_dept;

BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_isc_tbl is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ISC_TBL',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_pack_rec.item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_REC',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   --- Get the department level orders.
   SQL_LIB.SET_MARK('OPEN',
                    'C_PROCUR_UNIT_OPTIONS',
                    'PROCUREMENT_UNIT_OPTIONS',
                    NULL);
   open C_PROCUR_UNIT_OPTIONS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_PROCUR_UNIT_OPTIONS',
                    'UNIT_OPTIONS',
                    NULL);
   fetch C_PROCUR_UNIT_OPTIONS into L_dept_level_orders;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_PROCUR_UNIT_OPTIONS',
                    'PROCUREMENT_UNIT_OPTIONS',
                    NULL);
   close C_PROCUR_UNIT_OPTIONS;
   ---
   --- Get the item attributes for the specified potential component item.
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_ATTRIB',
                    'ITEM_MASTER, DEPS',
                    'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
   open C_ITEM_ATTRIB;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_ATTRIB',
                    'ITEM_MASTER, DEPS',
                    'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
   fetch C_ITEM_ATTRIB into L_item_dept,
                            L_item_level,
                            L_tran_level,
                            L_item_pack_ind,
                            L_item_pack_type,
                            L_item_order_as_type,
                            L_item_orderable_ind,
                            L_item_contains_inner_ind,
                            L_item_purchase_type;
   if C_ITEM_ATTRIB%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_ATTRIB',
                       'ITEM_MASTER, DEPS',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
      close C_ITEM_ATTRIB;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_ATTRIB',
                       'ITEM_MASTER, DEPS',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
      close C_ITEM_ATTRIB;
   end if;
   ---
   if L_item_level != L_tran_level then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_TRAN_LEVEL',
                                            NULL,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   --- Validation fails if the specified item is itself a pack that contains
   --- other packs.
   if L_item_pack_ind = 'Y' and L_item_contains_inner_ind = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('PACK_WITH_INNER_NOT_ALLOW',
                                            I_item,
                                            I_pack_no,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   --- Validation fails if the specified pack is a buyer pack and the specified
   --- item is both a pack and a vender pack.
   if L_pack_pack_type = 'B' and L_item_pack_ind = 'Y' and L_item_pack_type = 'V' then
      O_error_message := SQL_LIB.CREATE_MSG('VEND_INNR_NO_ADD_TO_BUYER',
                                            I_item,
                                            I_pack_no,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   --- Validation fails if the specified item is in a consignment department.
   if L_item_purchase_type = 1 then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_CONSIGN_NOT_IN_PACK',
                                            I_item,
                                            I_pack_no,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   --- Validation fails if the specified pack is orderable, its department level orders is
   --- yes, it orders as eaches, its department is not the same as the specified item's
   --- department, and either the specified item is not a pack or it is orderable.
   if L_pack_orderable_ind = 'Y' and L_dept_level_orders = 'Y' and L_pack_order_as_type = 'E' and
         L_pack_dept != L_item_dept and (L_item_pack_ind = 'N' or L_item_orderable_ind = 'Y') then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_NOT_SAME_DEPT_AS_PAC',
                                            I_item,
                                            L_pack_dept,
                                            NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   if L_pack_orderable_ind = 'Y' then
      --- Validation fails if the department level orders is yes, the specified pack
      --- orders as eaches, and any component item within the specified item (inner
      --- pack) does not have the same department as the specified pack.
      if L_item_pack_ind = 'Y' and L_item_orderable_ind = 'N' and L_dept_level_orders = 'Y' and
            L_pack_order_as_type = 'E' then
         SQL_LIB.SET_MARK('OPEN',
                          'GET_INVALID_INNER_PACK_DEPT',
                          'PACKITEM, ITEM_MASTER',
                          'INNER PACK NO: '||I_item||', DEPT: '||L_pack_dept);
         open GET_INVALID_INNER_PACK_DEPT;
         SQL_LIB.SET_MARK('FETCH',
                          'GET_INVALID_INNER_PACK_DEPT',
                          'PACKITEM, ITEM_MASTER',
                          'INNER PACK NO: '||I_item||', DEPT: '||L_pack_dept);
         fetch GET_INVALID_INNER_PACK_DEPT into L_exists;
         if GET_INVALID_INNER_PACK_DEPT%FOUND then
            SQL_LIB.SET_MARK('CLOSE',
                             'GET_INVALID_INNER_PACK_DEPT',
                             'PACKITEM, ITEM_MASTER',
                             'INNER PACK NO: '||I_item||', DEPT: '||L_pack_dept);
            close GET_INVALID_INNER_PACK_DEPT;
            O_error_message := SQL_LIB.CREATE_MSG('INNR_ITEMS_NOT_SAME_DEPT',
                                                  I_item,
                                                  L_pack_dept,
                                                  I_pack_no);
            O_valid := FALSE;
            return TRUE;
         else
            SQL_LIB.SET_MARK('CLOSE',
                             'GET_INVALID_INNER_PACK_DEPT',
                             'PACKITEM, ITEM_MASTER',
                             'INNER PACK NO: '||I_item||', DEPT: '||L_pack_dept);
            close GET_INVALID_INNER_PACK_DEPT;
         end if;
      end if;
      ---
      ---
      FOR i IN I_isc_tbl.FIRST..I_isc_tbl.LAST LOOP
         L_supplier          := I_isc_tbl(i).supplier;
         L_origin_country_id := I_isc_tbl(i).country;
         ---
         if L_item_pack_ind = 'Y' and L_item_orderable_ind = 'N' then
            --- Validation fails if any of the component items of the specified item (inner
            --- pack) do not have the current supplier-origin country relationship for the
            --- specified pack.
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_COMPONENT_ITEMS',
                             'PACKITEM, ITEM_SUPP_COUNTRY',
                             'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
            open C_CHECK_COMPONENT_ITEMS;
            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_COMPONENT_ITEMS',
                             'PACKITEM, ITEM_SUPP_COUNTRY',
                             'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
            fetch C_CHECK_COMPONENT_ITEMS into L_exists;
            if C_CHECK_COMPONENT_ITEMS%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMPONENT_ITEMS',
                                'PACKITEM, ITEM_SUPP_COUNTRY',
                                'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
               close C_CHECK_COMPONENT_ITEMS;
               O_error_message := SQL_LIB.CREATE_MSG('INNR_ITEMS_NO_ITEMSUPCTRY',
                                                     I_item,
                                                     L_supplier||'-'||L_origin_country_id,
                                                     I_pack_no);
               O_valid := FALSE;
               return TRUE;
            else
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMPONENT_ITEMS',
                                'PACKITEM, ITEM_SUPP_COUNTRY',
                                'PACK_NO: '||I_pack_no||', ITEM: '||I_item);
               close C_CHECK_COMPONENT_ITEMS;
            end if;
         else
            --- Validation fails if the specified item does not have the current supplier-origin
            --- country relationship for the specified pack.
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_COMPONENT_ITEMSUPPCTRY',
                             'ITEM_SUPP_COUNTRY',
                             'ITEM: '||I_item);
            open C_CHECK_COMPONENT_ITEMSUPPCTRY;
            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_COMPONENT_ITEMSUPPCTRY',
                             'ITEM_SUPP_COUNTRY',
                             'ITEM: '||I_item);
            fetch C_CHECK_COMPONENT_ITEMSUPPCTRY into L_exists;
            if C_CHECK_COMPONENT_ITEMSUPPCTRY%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMPONENT_ITEMSUPPCTRY',
                                'ITEM_SUPP_COUNTRY',
                                'ITEM: '||I_item);
               close C_CHECK_COMPONENT_ITEMSUPPCTRY;
               O_error_message := SQL_LIB.CREATE_MSG('COMP_ITEM_NO_ITEMSUPCTRY',
                                                     I_item,
                                                     L_supplier||'-'||L_origin_country_id,
                                                     I_pack_no);
               O_valid := FALSE;
               return TRUE;
            else
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CHECK_COMPONENT_ITEMSUPPCTRY',
                                'ITEM_SUPP_COUNTRY',
                                'ITEM: '||I_item);
               close C_CHECK_COMPONENT_ITEMSUPPCTRY;
            end if;
         end if;
      end LOOP;
   end if;
   ---
   O_item_pack_ind := L_item_pack_ind;

   O_valid := TRUE;
   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END VALID_ITEM;
------------------------------------------------------------------------------------------
FUNCTION INSERT_PACKITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_packitem_rec  IN PACKITEM%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'TAX_SQL.ITEMLIST_TAX_EXPLODE';
   L_user         VARCHAR2(30) := get_user;

BEGIN
   SQL_LIB.SET_MARK('INSERT', NULL, 'PACKITEM','pack: '||I_packitem_rec.pack_no);

   insert into PACKITEM( pack_no,
                         seq_no,
                         item,
                         item_parent,
                         pack_tmpl_id,
                         pack_qty,
                         create_datetime,
                         last_update_datetime,
                         last_update_id)
                 values( I_packitem_rec.pack_no,
                         I_packitem_rec.seq_no,
                         I_packitem_rec.item,
                         I_packitem_rec.item_parent,
                         I_packitem_rec.pack_tmpl_id,
                         I_packitem_rec.pack_qty,
                         I_packitem_rec.create_datetime,
                         sysdate,
                         L_user);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_PACKITEM;
--------------------------------------------------------------------------
FUNCTION INSERT_PI_BREAKOUT_NOLOCS(O_error_message IN OUT VARCHAR2,
                                   I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                                   I_item          IN     PACKITEM_BREAKOUT.ITEM%TYPE,
                                   I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                                   I_qty           IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE)
   return BOOLEAN IS
   ---
   L_program            VARCHAR2(64)  := 'PACKITEM_ADD_SQL.INSERT_PACKITEM_BREAKOUT_NOLOCS';
   L_max_seq_no         PACKITEM_BREAKOUT.SEQ_NO%TYPE;
   L_user               VARCHAR2(30) := get_user;
   ---
   cursor C_MAX_SEQ_NO is
      select nvl(max(seq_no) + 1,1)
        from packitem_breakout
       where pack_no = I_pack_no;

BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if nvl(I_qty, 0) <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_QTY',
                                            nvl(to_char(I_qty), 'NULL'),
                                            '> 0');
      return FALSE;
   end if;
   if I_pack_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_IND',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_pack_ind = 'Y' then
      if INNER_PACK(O_error_message,
                    I_pack_no,
                    I_item,
                    I_qty) = FALSE then
         return FALSE;
      end if;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_MAX_SEQ_NO',
                       'PACKITEM_BREAKOUT',
                       'PACK_NO: '||I_pack_no);
      open C_MAX_SEQ_NO;
      SQL_LIB.SET_MARK('FETCH',
                       'C_MAX_SEQ_NO',
                       'PACKITEM_BREAKOUT',
                       'PACK_NO: '||I_pack_no);
      fetch C_MAX_SEQ_NO into L_max_seq_no;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_MAX_SEQ_NO',
                       'PACKITEM_BREAKOUT',
                       'PACK_NO: '||I_pack_no);
      close C_MAX_SEQ_NO;
      ---
      SQL_LIB.SET_MARK('INSERT',
                       'PACKITEM_BREAKOUT',
                       'PACK_NO: '||I_pack_no||', ITEM: '||I_item,
                       NULL);
      insert into PACKITEM_BREAKOUT( pack_no,
                                     seq_no,
                                     item,
                                     item_parent,
                                     pack_tmpl_id,
                                     comp_pack_no,
                                     item_qty,
                                     item_parent_pt_qty,
                                     comp_pack_qty,
                                     pack_item_qty,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id)
            values( I_pack_no,
                    L_max_seq_no,
                    I_item,
                    NULL,
                    NULL,
                    NULL,
                    I_qty,
                    NULL,
                    NULL,
                    I_qty,
                    sysdate,
                    sysdate,
                    L_user);
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_PI_BREAKOUT_NOLOCS;
-------------------------------------------------------------------------------------------------------
FUNCTION PACK_COMP_EXISTS(O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          I_pack_item       IN       PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'PACKITEM_ADD_SQL.PACK_COMP_EXISTS';
   L_dummy     VARCHAR2(1)  := NULL;

   cursor C_CHECK_PACK_COMPONENTS is
     select 'x'
       from packitem
      where pack_no = I_pack_item
        and rownum = 1;

BEGIN
   if I_pack_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_PACK_COMPONENTS',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_item);
   open C_CHECK_PACK_COMPONENTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_PACK_COMPONENTS',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_item);
   fetch C_CHECK_PACK_COMPONENTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_PACK_COMPONENTS',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_item);
   close C_CHECK_PACK_COMPONENTS;

   if L_dummy is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PACK_COMP_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION PACK_COSTING_EXISTS(O_error_message  IN OUT   VARCHAR2,
                             O_exists            IN OUT   BOOLEAN,
                             I_pack_item         IN       PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'PACKITEM_ADD_SQL.PACK_COSTING_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHECK_PACK_COSTING_RECORD is
     select 'x'
       from item_cost_head
      where item = I_pack_item
        and rownum = 1;

BEGIN
   if I_pack_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_PACK_COSTING_RECORD',
                    'item_cost_head',
                    'PACK_NO: '||I_pack_item);
   open C_CHECK_PACK_COSTING_RECORD;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_PACK_COSTING_RECORD',
                    'item_cost_head',
                    'PACK_NO: '||I_pack_item);
   fetch C_CHECK_PACK_COSTING_RECORD into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_PACK_COSTING_RECORD',
                    'item_cost_head',
                    'PACK_NO: '||I_pack_item);
   close C_CHECK_PACK_COSTING_RECORD;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PACK_COSTING_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_INNER_EXISTS (O_error_message IN OUT VARCHAR2,
                             O_exists        IN OUT VARCHAR2,
                             I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE)
   return BOOLEAN IS

   L_program            VARCHAR2(64)  := 'PACKITEM_ADD_SQL.CHECK_INNER_EXISTS';
   ---
   cursor C_INNER_EXISTS is
     SELECT 'Y'
       FROM packitem_breakout
      WHERE comp_pack_no = I_pack_no;

BEGIN
   O_exists := 'N';
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_INNER_EXISTS',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   open C_INNER_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_INNER_EXISTS',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   fetch C_INNER_EXISTS into O_exists;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_INNER_EXISTS',
                    'PACKITEM_BREAKOUT',
                    'PACK_NO: '||I_pack_no);
   close C_INNER_EXISTS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_INNER_EXISTS;
------------------------------------------------------------------------------------------------
FUNCTION UPD_ISC_PACK_TMPL (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS
   L_program            VARCHAR2(64)  := 'PACKITEM_ADD_SQL.UPD_ISC_PACK_TMPL';
BEGIN

   if LP_item_cost_tbl is NOT NULL and LP_item_cost_tbl.count > 0 then
      if ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK(O_error_message,
                                              LP_item_cost_tbl) = FALSE then
         return FALSE;
      end if;

      merge into item_supp_country i
      using (select ic.I_item item,
                    ic.I_supplier supplier,
                    ic.I_origin_country_id origin_country_id,
                    ic.O_base_cost,
                    ic.O_extended_base_cost,
                    ic.O_inclusive_cost,
                    ic.O_negotiated_item_cost,
                    c.default_po_cost
               from TABLE(CAST(LP_item_cost_tbl as OBJ_COMP_ITEM_COST_TBL)) ic,
                    country_attrib c
              where ic.I_origin_country_id = c.country_id
            ) use_this
      on (i.item              = use_this.item and
          i.supplier          = use_this.supplier and
          i.origin_country_id = use_this.origin_country_id)
      when matched then
         update
            set i.unit_cost = DECODE(use_this.default_po_cost,
                                'NIC',NVL(use_this.O_negotiated_item_cost,i.unit_cost),
                                'BC',NVL(use_this.O_base_cost,i.unit_cost)),
                i.negotiated_item_cost = use_this.O_negotiated_item_cost,
                i.extended_base_cost   = use_this.O_extended_base_cost,
                i.inclusive_cost       = use_this.O_inclusive_cost,
                i.base_cost            = use_this.O_base_cost;
   end if;

   LP_item_cost_tbl.DELETE();
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPD_ISC_PACK_TMPL;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_BUYER_PACK(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item               IN     ITEM_COST_HEAD.ITEM%TYPE,
                                I_component_costs    IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                I_base_country_id    IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN AS
  L_program                VARCHAR2(50) := 'PACKITEM_ADD_SQL.INSERT_COST_BUYER_PACK';

  L_country_attrib_row     COUNTRY_ATTRIB%ROWTYPE;
  L_base_cost              ITEM_SUPP_COUNTRY.BASE_COST%TYPE := NULL;
  L_extended_base_cost     ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE := NULL;
  L_inclusive_cost         ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE := NULL;
  L_negotiated_item_cost   ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE := NULL;
  L_unit_cost              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := NULL;
  L_supplier               ITEM_SUPP_COUNTRY.SUPPLIER%TYPE := NULL;
  L_origin_country_id      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE :=  NULL;
  L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;


BEGIN

   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;

   FOR rec in (select origin_country_id,
                      supplier
                 from item_supp_country  isc
                where isc.item = I_item) LOOP

      if PRICING_ATTRIB_SQL.BUILD_PACK_COST(O_error_message,
                                            L_unit_cost,
                                            rec.supplier,
                                            rec.origin_country_id,
                                            I_item,
                                            NULL) = FALSE then
        return FALSE;
      end if;
      if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                 L_country_attrib_row,
                                                 NULL,
                                                 I_base_country_id) = FALSE then
         return FALSE;
      end if;

      if L_system_options_rec.default_tax_type = 'GTAX' then

         if L_country_attrib_row.item_cost_tax_incl_ind = 'N' then
            L_base_cost := L_unit_cost;
         else
            L_negotiated_item_cost := L_unit_cost;
         end if; 
 
         if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                            L_base_cost,
                                            L_extended_base_cost,
                                            L_inclusive_cost,
                                            L_negotiated_item_cost,
                                            I_item,
                                            L_country_attrib_row.item_cost_tax_incl_ind,
                                            rec.supplier,
                                            L_country_attrib_row.default_loc,
                                            L_country_attrib_row.default_loc_type,
                                            'ITEMSUPPCTRY',    -- I_calling_form
                                            rec.origin_country_id,
                                            I_base_country_id,
                                            'Y',   --I_prim_dlvy_ctry_ind
                                            'Y') = FALSE then   --I_update_itemcost_ind

            return FALSE;
         end if;
         if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST(O_error_message,
                                                     I_item,
                                                     rec.supplier,
                                                     rec.origin_country_id,
                                                     L_country_attrib_row.default_loc,
                                                     L_base_cost,
                                                     L_extended_base_cost,
                                                     L_negotiated_item_cost,
                                                     L_inclusive_cost,
                                                     'ITEMSUPPCTRY') = FALSE then
            return FALSE;
         end if;
      elsif L_system_options_rec.default_tax_type = 'SVAT' then 
         if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST(O_error_message,
                                                     I_item,
                                                     rec.supplier,
                                                     rec.origin_country_id,
                                                     L_country_attrib_row.default_loc,
                                                     L_unit_cost,
                                                     'ITEMSUPPCTRY') = FALSE then
            return FALSE;
         end if;
      end if; 
   end loop;

   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_COST_BUYER_PACK;
------------------------------------------------------------------------------------------------
FUNCTION DELETE_SUPS_PACK_TMPL_DESC_TL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                             I_supplier       IN       SUPS_PACK_TMPL_DESC_TL.SUPPLIER%TYPE,
                                             I_pack_tmpl_id   IN       SUPS_PACK_TMPL_DESC_TL.PACK_TMPL_ID%TYPE )
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'PACKITEM_ADD_SQL.DELETE_SUPS_PACK_TMPL_DESC_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SUPS_PACK_TMPL_DESC_TL is
      select 'x'
        from sups_pack_tmpl_desc_tl
       where supplier = I_supplier 
       and pack_tmpl_id = I_pack_tmpl_id
         for update nowait;
BEGIN

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier ',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
    if I_pack_tmpl_id is NULL  then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_tmpl_id',
                                             L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'SUPS_PACK_TMPL_DESC_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_SUPS_PACK_TMPL_DESC_TL',
                    L_table,
                    'I_supplier '|| I_supplier ||' I_pack_tmpl_id '|| I_pack_tmpl_id);
   open C_LOCK_SUPS_PACK_TMPL_DESC_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_SUPS_PACK_TMPL_DESC_TL',
                    L_table,
                    'I_supplier '|| I_supplier ||' I_pack_tmpl_id '|| I_pack_tmpl_id);
   close C_LOCK_SUPS_PACK_TMPL_DESC_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SUPS_PACK_TMPL_DESC_TL',
                    'I_supplier '|| I_supplier ||' I_pack_tmpl_id '|| I_pack_tmpl_id);
                    
   delete from sups_pack_tmpl_desc_tl
          where supplier = I_supplier 
          and pack_tmpl_id = I_pack_tmpl_id;
    
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_SUPS_PACK_TMPL_DESC_TL;
---------------------------------------------------------------------------------------------
END;
/