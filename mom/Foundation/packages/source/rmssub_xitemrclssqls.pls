
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XITEMRCLS_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_itemreclass_rec   IN       RECLASS_SQL.RECLASS_REC,
                 I_message_type      IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XITEMRCLS_SQL;
/

