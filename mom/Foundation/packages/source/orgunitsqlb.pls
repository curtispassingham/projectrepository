
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORG_UNIT_SQL AS
--------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_desc                  IN OUT ORG_UNIT.DESCRIPTION%TYPE,
                  I_org_unit_id           IN     ORG_UNIT.ORG_UNIT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program_name            VARCHAR2(50) := 'ORG_UNIT_SQL.GET_DESC';

   cursor C_GET_DESC is
      select description
        from v_org_unit_tl
       where org_unit_id  = I_org_unit_id;

BEGIN
   
   open C_GET_DESC;          
   fetch C_GET_DESC into O_desc;
   close C_GET_DESC;

   if O_desc is NULL then
      O_error_message := 'INV_ORGUNIT';
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END GET_DESC;
--------------------------------------------------------------------------------
FUNCTION CHECK_DUP_ORGUNIT(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid                 IN OUT BOOLEAN,
                           I_org_unit_id           IN     ORG_UNIT.ORG_UNIT_ID%TYPE)
   RETURN BOOLEAN IS 

   L_program_name            VARCHAR2(50) := 'ORG_UNIT_SQL.CHECK_DUP_ORGUNIT';
   L_exists                  VARCHAR2(1)  := 'N';

   cursor C_ORGUNIT_EXIST is
      select 'Y'
        from org_unit
       where org_unit_id  = I_org_unit_id;
  
BEGIN

   open C_ORGUNIT_EXIST;
   fetch C_ORGUNIT_EXIST into L_exists;
   close C_ORGUNIT_EXIST;

   if L_exists = 'Y' then
      O_error_message := 'DUP_ORGUNIT';
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_DUP_ORGUNIT;
--------------------------------------------------------------------------------
FUNCTION GET_LOC_VENDOR_SITE_ASSOC(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_oracle_vendor_site_id  IN OUT ORG_UNIT_ADDR_SITE.ORACLE_VENDOR_SITE_ID%TYPE,
                                   I_supplier               IN     VARCHAR2,
                                   I_location               IN     ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program_name VARCHAR2(50) := 'ORG_UNIT_SQL.GET_LOC_VENDOR_SITE_ASSOC';
   L_primary_addr_ind ADDR.PRIMARY_ADDR_IND%TYPE := NULL;

   -- Find the Oracle Vendor Site Id for the input Supplier/Location combination.
   -- First find the Org Unit Id associated to the input location.  Use this Org Unit Id
   -- to find the set of related Oracle Vendor Site Ids.  Then narrow this set down
   -- to those Vendor Site Ids associated to one or more of the Supplier's Remittance
   -- address type.  If the input supplier is associated to more than one Remittance
   -- address that share the same Org Unit Id as the input Location, which is unlikely,
   -- then pick the primary Remittance Address, if it is in this set.
   cursor C_GET_VENDOR_SITE is
      SELECT ouas.oracle_vendor_site_id,
             a.primary_addr_ind
        FROM (SELECT org_unit_id
                FROM store
               WHERE store = I_location
               UNION ALL
              SELECT org_unit_id
                FROM wh
               WHERE wh = I_location) loc,
             org_unit_addr_site ouas,
             addr a
       WHERE loc.org_unit_id = ouas.org_unit_id
         AND a.key_value_1 = TO_CHAR(I_supplier)
         AND a.module = 'SUPP'
         AND a.addr_type = '06'
         AND a.addr_key = ouas.addr_key
       ORDER BY 2 DESC;

BEGIN

   if I_supplier IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   if I_location IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program_name,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_VENDOR_SITE;
   fetch C_GET_VENDOR_SITE into O_oracle_vendor_site_id,
                                L_primary_addr_ind;

   if C_GET_VENDOR_SITE%NOTFOUND then
      O_oracle_vendor_site_id := NULL;
   end if;

   close C_GET_VENDOR_SITE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END GET_LOC_VENDOR_SITE_ASSOC;
--------------------------------------------------------------------------------
FUNCTION GET_ORG_UNIT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_org_unit_record     IN OUT ORG_UNIT%ROWTYPE,
                      I_org_unit            IN     ORG_UNIT.ORG_UNIT_ID%TYPE)
   RETURN BOOLEAN IS
   L_program_name VARCHAR2(64) := 'ORG_UNIT_SQL.GET_ORG_UNIT';

   CURSOR C_GET_ORG_UNIT is
      select * 
      from org_unit
      where org_unit_id = I_org_unit;
BEGIN
   open  C_GET_ORG_UNIT;

   fetch C_GET_ORG_UNIT into O_org_unit_record;

   if C_GET_ORG_UNIT%NOTFOUND then
      close C_GET_ORG_UNIT;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORGUNIT',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_ORG_UNIT;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORG_UNIT;
-------------------------------------------------------------------------------

FUNCTION CHECK_ORG_UNIT_ASSOC_EXISTS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists          IN OUT  BOOLEAN,
                                     I_org_unit_id     IN      ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN IS

   L_loc_exists   VARCHAR2(1)   := 'N';
   L_program      VARCHAR2(64)  := 'ORG_UNIT_SQL.CHECK_ORG_UNIT_ASSOC_EXISTS';

   cursor C_OU_EXISTS is
      select 'Y'
        from store
       where org_unit_id = I_org_unit_id
   union all
      select 'Y'
        from store_add
       where org_unit_id = I_org_unit_id
   union all
      select 'Y'
        from wh
       where org_unit_id = I_org_unit_id
   union all
      select 'Y'
        from partner
       where org_unit_id = I_org_unit_id
   union all
      select 'Y'
        from partner_org_unit
       where org_unit_id = I_org_unit_id
   union all
      select 'Y'
        from tsf_entity_org_unit_sob
       where org_unit_id = I_org_unit_id;

BEGIN
   O_exists := FALSE;

   if I_org_unit_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_org_unit_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_OU_EXISTS;
   fetch C_OU_EXISTS into L_loc_exists;
   close C_OU_EXISTS;

   if L_loc_exists = 'Y' then
      O_exists := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('ORG_UNIT_ASSOC_EXISTS',
                                            I_org_unit_id,
                                            NULL,
                                            NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ORG_UNIT_ASSOC_EXISTS;
---------------------------------------------------------------------------------
END ORG_UNIT_SQL;
/
