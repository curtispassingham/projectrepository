CREATE OR REPLACE PACKAGE BODY ITEM_SUPP_COUNTRY_SQL AS
-------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_COUNTRY(O_error_message     IN OUT VARCHAR2,
                             O_exists            IN OUT BOOLEAN,
                             O_origin_country_id IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                             I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
   RETURN BOOLEAN IS


   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY';


   cursor C_GET_PRIMARY is
      select origin_country_id
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and primary_country_ind = 'Y';

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_GET_PRIMARY;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIMARY','ITEM_SUPP_COUNTRY','ITEM: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   fetch C_GET_PRIMARY into O_origin_country_id;
      if C_GET_PRIMARY%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIMARY','ITEM_SUPP_COUNTRY','ITEM: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_GET_PRIMARY;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIMARY_COUNTRY;
-----------------------------------------------------------------------------------------------------------
FUNCTION ALL_PACK_COMPONENTS_EXIST(O_error_message     IN OUT VARCHAR2,
                                   O_exists            IN OUT BOOLEAN,
                                   I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                   I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                   I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN IS


   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.ALL_PACK_COMPONENTS_EXIST';
   L_pack_item             ITEM_MASTER.ITEM%TYPE;
   L_exists                VARCHAR2(1);


    cursor C_PACK_ITEMS_EXIST is
      select 'x'
        from v_packsku_qty vpq
       where vpq.pack_no = I_item
         and not exists( select 'x'
                           from item_supp_country isc
                          where isc.item              = vpq.item
                            and isc.supplier          = I_supplier
                            and isc.origin_country_id = I_origin_country_id
                            and rownum =1 );


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_PACK_ITEMS_EXIST','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   open C_PACK_ITEMS_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_PACK_ITEMS_EXIST','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   fetch C_PACK_ITEMS_EXIST into L_exists;
      if C_PACK_ITEMS_EXIST%FOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
   SQL_LIB.SET_MARK('CLOSE','C_PACK_ITEMS_EXIST','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   close C_PACK_ITEMS_EXIST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ALL_PACK_COMPONENTS_EXIST;
--------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_CONST_DIMENSIONS(O_error_message     IN OUT VARCHAR2,
                                 I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                 I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                 I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                                 I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE,
                                 I_default_children  IN     VARCHAR2)
   return BOOLEAN IS


   L_program                     VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.UPDATE_CONST_DIMENSIONS';
   L_table                       VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY_DIM';
   L_dim_supplier                ITEM_SUPP_COUNTRY.SUPPLIER%TYPE;
   L_dim_origin_country_id       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_presentation_method         ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD%TYPE;
   L_length                      ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_width                       ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_height                      ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_lwh_uom                     ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_weight                      ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_net_weight                  ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_weight_uom                  ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_liquid_volume               ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_liquid_volume_uom           ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_stat_cube                   ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE;
   L_tare_weight                 ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE;
   L_tare_type                   ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE;
   L_child_item                  ITEM_MASTER.ITEM%TYPE;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_DIMENSIONS is
      select isd.presentation_method,
             isd.length,
             isd.width,
             isd.height,
             isd.lwh_uom,
             isd.weight,
             isd.net_weight,
             isd.weight_uom,
             isd.liquid_volume,
             isd.liquid_volume_uom,
             isd.stat_cube,
             isd.tare_weight,
             isd.tare_type
        from item_supp_country_dim isd
       where isd.item = I_item
         and isd.supplier = I_supplier
         and isd.origin_country = I_origin_country_id
         and isd.dim_object = I_dim_object;

   cursor C_LOCK_DIMENSIONS is
      select 'x'
        from item_supp_country_dim isd
       where isd.item = I_item
         and (isd.supplier != I_supplier
              or isd.origin_country != I_origin_country_id)
         and isd.dim_object = I_dim_object
         for update nowait;

   cursor C_LOCK_CHILD_DIMENSIONS is
      select 'x'
        from item_supp_country_dim isd
       where isd.item = L_child_item
         and isd.supplier = L_dim_supplier
         and isd.origin_country = L_dim_origin_country_id
         for update nowait;

   cursor C_GET_SUPP_COUNTRIES_INSERT is
      select supplier,
             origin_country_id
        from item_supp_country isc
       where isc.item = I_item
         and (isc.supplier != I_supplier
              or isc.origin_country_id != I_origin_country_id);

    cursor C_GET_CHILD_ITEMS is
      select im.item
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                       from item_supp_country isc
                       where isc.item = im.item
                         and isc.supplier = L_dim_supplier
                         and isc.origin_country_id = L_dim_origin_country_id)
         and exists (select 'x'
                       from item_supp_country_dim isd
                      where isd.item = I_item
                        and isd.supplier = L_dim_supplier
                        and isd.origin_country = L_dim_origin_country_id);

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
   open C_GET_DIMENSIONS;
   SQL_LIB.SET_MARK('FETCH','C_GET_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
   fetch C_GET_DIMENSIONS into L_presentation_method,
                               L_length,
                               L_width,
                               L_height,
                               L_lwh_uom,
                               L_weight,
                               L_net_weight,
                               L_weight_uom,
                               L_liquid_volume,
                               L_liquid_volume_uom,
                               L_stat_cube,
                               L_tare_weight,
                               L_tare_type;
   SQL_LIB.SET_MARK('CLOSE','C_GET_DIMENSIONS ','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
   close C_GET_DIMENSIONS;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
   open C_LOCK_DIMENSIONS;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
   close C_LOCK_DIMENSIONS;
   ---
   SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
   update item_supp_country_dim isd
      set isd.presentation_method = NVL(L_presentation_method, presentation_method),
          isd.length = NVL(L_length, length),
          isd.width = NVL(L_width, width),
          isd.height = NVL(L_height, height),
          isd.lwh_uom = NVL(L_lwh_uom, lwh_uom),
          isd.weight = NVL(L_weight, weight),
          isd.net_weight = NVL(L_net_weight, net_weight),
          isd.weight_uom = NVL(L_weight_uom, weight_uom),
          isd.liquid_volume =  NVL(L_liquid_volume, liquid_volume),
          isd.liquid_volume_uom = NVL(L_liquid_volume_uom, liquid_volume_uom),
          isd.stat_cube =  NVL(L_stat_cube, stat_cube),
          isd.tare_weight =  NVL(L_tare_weight, tare_weight),
          isd.tare_type =  NVL(L_tare_type, tare_type),
          isd.last_update_datetime = sysdate,
          isd.last_update_id = get_user
    where isd.item = I_item
      and (isd.supplier != I_supplier
           or isd.origin_country != I_origin_country_id)
      and isd.dim_object = I_dim_object
      and exists (select 'x'
                    from item_supp_country_dim isd2
                   where isd.item = I_item
                     and (isd.supplier != I_supplier
                          or isd.origin_country != I_origin_country_id)
                     and isd.dim_object = I_dim_object);

   SQL_LIB.SET_MARK('OPEN','C_GET_SUPP_COUNTRIES_INSERT','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);                        
   ---
   for c_rec in C_GET_SUPP_COUNTRIES_INSERT LOOP
      L_dim_supplier := c_rec.supplier;
      L_dim_origin_country_id := c_rec.origin_country_id;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
      insert into item_supp_country_dim(item,
                                        supplier,
                                        origin_country,
                                        dim_object,
                                        presentation_method,
                                        length,
                                        width,
                                        height,
                                        lwh_uom,
                                        weight,
                                        net_weight,
                                        weight_uom,
                                        liquid_volume,
                                        liquid_volume_uom,
                                        stat_cube,
                                        tare_weight,
                                        tare_type,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        create_id)
                                 select I_item,
                                        L_dim_supplier,
                                        L_dim_origin_country_id,
                                        I_dim_object,
                                        isd.presentation_method,
                                        isd.length,
                                        isd.width,
                                        isd.height,
                                        isd.lwh_uom,
                                        isd.weight,
                                        isd.net_weight,
                                        isd.weight_uom,
                                        isd.liquid_volume,
                                        isd.liquid_volume_uom,
                                        isd.stat_cube,
                                        isd.tare_weight,
                                        isd.tare_type,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        get_user
                                      from item_supp_country_dim isd
                                     where isd.item = I_item
                                       and isd.supplier = I_supplier
                                       and isd.origin_country = I_origin_country_id
                                       and isd.dim_object = I_dim_object
                                       and not exists (select 'x'
                                                         from item_supp_country_dim isd2
                                                        where isd2.item = isd.item
                                                          and isd2.supplier = L_dim_supplier
                                                          and isd2.origin_country = L_dim_origin_country_id
                                                          and isd2.dim_object = isd.dim_object);
      if I_default_children = 'Y' then
         for c_rec in C_GET_CHILD_ITEMS LOOP
            L_child_item := c_rec.item;
            ---
            SQL_LIB.SET_MARK('OPEN','C_LOCK_CHILD_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                             ' Supplier: '||to_char(L_dim_supplier)||' Origin_Country: '|| L_dim_origin_country_id);
            open C_LOCK_CHILD_DIMENSIONS;
            SQL_LIB.SET_MARK('CLOSE','C_LOCK_CHILD_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                             ' Supplier: '||to_char(L_dim_supplier)||' Origin_Country: '|| L_dim_origin_country_id);
            close C_LOCK_CHILD_DIMENSIONS;
            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                             ' Supplier: '||to_char(L_dim_supplier)||' Origin_Country: '|| L_dim_origin_country_id||' Dim_Object: '||I_dim_object);
            update item_supp_country_dim isd
               set isd.presentation_method = NVL(L_presentation_method, presentation_method),
                   isd.length = NVL(L_length, length),
                   isd.width = NVL(L_width, width),
                   isd.height = NVL(L_height, height),
                   isd.lwh_uom = NVL(L_lwh_uom, lwh_uom),
                   isd.weight = NVL(L_weight, weight),
                   isd.net_weight = NVL(L_net_weight, net_weight),
                   isd.weight_uom = NVL(L_weight_uom, weight_uom),
                   isd.liquid_volume =  NVL(L_liquid_volume, liquid_volume),
                   isd.liquid_volume_uom = NVL(L_liquid_volume_uom, liquid_volume_uom),
                   isd.stat_cube =  NVL(L_stat_cube, stat_cube),
                   isd.tare_weight =  NVL(L_tare_weight, tare_weight),
                   isd.tare_type =  NVL(L_tare_type, tare_type),
                   isd.last_update_datetime = sysdate,
                   isd.last_update_id = get_user
            where isd.item = L_child_item
              and isd.supplier = L_dim_supplier
              and isd.origin_country = L_dim_origin_country_id
              and isd.dim_object = I_dim_object
              and exists (select 'x'
                            from item_supp_country_dim isd2
                           where isd.item = I_item
                             and (isd.supplier = L_dim_supplier
                                  or isd.origin_country = L_dim_origin_country_id)
                             and isd.dim_object = I_dim_object);
            ---
            SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                             ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
            insert into item_supp_country_dim(item,
                                              supplier,
                                              origin_country,
                                              dim_object,
                                              presentation_method,
                                              length,
                                              width,
                                              height,
                                              lwh_uom,
                                              weight,
                                              net_weight,
                                              weight_uom,
                                              liquid_volume,
                                              liquid_volume_uom,
                                              stat_cube,
                                              tare_weight,
                                              tare_type,
                                              create_datetime,
                                              last_update_datetime,
                                              last_update_id,
                                              create_id)
                                       select L_child_item,
                                              L_dim_supplier,
                                              L_dim_origin_country_id,
                                              I_dim_object,
                                              isd.presentation_method,
                                              isd.length,
                                              isd.width,
                                              isd.height,
                                              isd.lwh_uom,
                                              isd.weight,
                                              isd.net_weight,
                                              isd.weight_uom,
                                              isd.liquid_volume,
                                              isd.liquid_volume_uom,
                                              isd.stat_cube,
                                              isd.tare_weight,
                                              isd.tare_type,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              get_user
                                         from item_supp_country_dim isd
                                        where isd.item = I_item
                                          and isd.supplier = I_supplier
                                          and isd.origin_country = I_origin_country_id
                                          and isd.dim_object = I_dim_object
                                          and not exists (select 'x'
                                                           from item_supp_country_dim isd2
                                                          where isd2.item = L_child_item
                                                            and isd2.supplier = L_dim_supplier
                                                            and isd2.origin_country = L_dim_origin_country_id
                                                            and isd2.dim_object = isd.dim_object);
         end LOOP; -- retrieve child items.
      end if;
   end LOOP; -- retrieve non-primary suppliers and countries.
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Item: '||I_item,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_CONST_DIMENSIONS;
----------------------------------------------------------------------------------------------------------
FUNCTION INSERT_CONST_DIMENSIONS(O_error_message     IN OUT VARCHAR2,
                                 I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                 I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                 I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE)
   return BOOLEAN IS


   L_program                     VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.INSERT_CONST_DIMENSIONS';
   L_primary_supplier            ITEM_SUPP_COUNTRY.SUPPLIER%TYPE;
   L_primary_origin_country_id   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_dim_object                  ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE;


   cursor C_GET_PRIMARY_INFO is
      select supplier,
             origin_country_id
        from item_supp_country
       where item = I_item
         and primary_supp_ind = 'Y'
         and primary_country_ind = 'Y';

   cursor C_GET_DIMENSIONS is
      select isd.dim_object
        from item_supp_country_dim isd
       where isd.item = I_item
         and isd.supplier = L_primary_supplier
         and isd.origin_country = L_primary_origin_country_id;


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_INFO','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   open C_GET_PRIMARY_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIMARY_INFO','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   fetch C_GET_PRIMARY_INFO into L_primary_supplier,
                                 L_primary_origin_country_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIMARY_INFO','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   close C_GET_PRIMARY_INFO;
   ---
   for c_rec in C_GET_DIMENSIONS LOOP
      L_dim_object := c_rec.dim_object;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||L_dim_object);
      insert into item_supp_country_dim(item,
                                        supplier,
                                        origin_country,
                                        dim_object,
                                        presentation_method,
                                        length,
                                        width,
                                        height,
                                        lwh_uom,
                                        weight,
                                        net_weight,
                                        weight_uom,
                                        liquid_volume,
                                        liquid_volume_uom,
                                        stat_cube,
                                        tare_weight,
                                        tare_type,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        create_id)
                                 select I_item,
                                        I_supplier,
                                        I_origin_country_id,
                                        L_dim_object,
                                        isd.presentation_method,
                                        isd.length,
                                        isd.width,
                                        isd.height,
                                        isd.lwh_uom,
                                        isd.weight,
                                        isd.net_weight,
                                        isd.weight_uom,
                                        isd.liquid_volume,
                                        isd.liquid_volume_uom,
                                        isd.stat_cube,
                                        isd.tare_weight,
                                        isd.tare_type,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        get_user
                                   from item_supp_country_dim isd
                                  where isd.item = I_item
                                    and isd.supplier = L_primary_supplier
                                    and isd.origin_country = L_primary_origin_country_id
                                    and isd.dim_object = L_dim_object
                                    and not exists (select 'x'
                                                      from item_supp_country_dim isd2
                                                     where isd2.item = isd.item
                                                       and isd2.supplier = I_supplier
                                                       and isd2.origin_country = I_origin_country_id
                                                       and isd2.dim_object = L_dim_object);
   end LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_CONST_DIMENSIONS;
----------------------------------------------------------------------------------------------------------
FUNCTION INSERT_DIMENSION_TO_CHILDREN(O_error_message     IN OUT VARCHAR2,
                                      I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                      I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                      I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                                      I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   return BOOLEAN IS


   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.INSERT_DIMENSION_TO_CHILDREN';
   L_child_item            ITEM_MASTER.ITEM%TYPE;

   cursor C_GET_CHILD_ITEMS is
      select im.item
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                       from item_supp_country isc
                       where isc.item = im.item
                         and isc.supplier = I_supplier
                         and isc.origin_country_id = I_origin_country_id)
         and exists (select 'x'
                       from item_supp_country_dim isd
                      where isd.item = I_item
                        and isd.supplier = I_supplier
                        and isd.origin_country = I_origin_country_id);
 BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS','ITEM_MASTER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);      
   for c_rec in C_GET_CHILD_ITEMS LOOP
      L_child_item := c_rec.item;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
      insert into item_supp_country_dim(item,
                                        supplier,
                                        origin_country,
                                        dim_object,
                                        presentation_method,
                                        length,
                                        width,
                                        height,
                                        lwh_uom,
                                        weight,
                                        net_weight,
                                        weight_uom,
                                        liquid_volume,
                                        liquid_volume_uom,
                                        stat_cube,
                                        tare_weight,
                                        tare_type,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        create_id)
                                 select L_child_item,
                                        I_supplier,
                                        I_origin_country_id,
                                        NVL(I_dim_object, dim_object),
                                        isd.presentation_method,
                                        isd.length,
                                        isd.width,
                                        isd.height,
                                        isd.lwh_uom,
                                        isd.weight,
                                        isd.net_weight,
                                        isd.weight_uom,
                                        isd.liquid_volume,
                                        isd.liquid_volume_uom,
                                        isd.stat_cube,
                                        isd.tare_weight,
                                        isd.tare_type,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        get_user
                                   from item_supp_country_dim isd
                                  where isd.item = I_item
                                    and isd.supplier = I_supplier
                                    and isd.origin_country = I_origin_country_id
                                    and isd.dim_object = NVL(I_dim_object, dim_object)
                                    and not exists (select 'x'
                                                      from item_supp_country_dim isd
                                                     where isd.item = L_child_item
                                                       and isd.supplier = I_supplier
                                                       and isd.origin_country = I_origin_country_id
                                                       and isd.dim_object = NVL(I_dim_object, dim_object));
   end LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_DIMENSION_TO_CHILDREN;
-----------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIMENSION_TO_CHILDREN(O_error_message     IN OUT VARCHAR2,
                                      I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                      I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                      I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                                      I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   return BOOLEAN IS

   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.DELETE_DIMENSION_TO_CHILDREN';
   L_table                 VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY_DIM';
   L_exists                BOOLEAN;
   L_child_item            ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE;

   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_CHILD_DIM is
       select im.item
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                      from item_supp_country_dim isd
                     where isd.item = im.item
                       and isd.supplier = I_supplier
                       and isd.origin_country = I_origin_country_id
                       and isd.dim_object = I_dim_object);

   cursor C_LOCK_DIMENSIONS is
      select 'x'
        from item_supp_country_dim
       where supplier = I_supplier
         and origin_country = I_origin_country_id
         and dim_object = I_dim_object
         and item = L_child_item;


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_dim_object is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dim_object',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_DIM','ITEM_MASTER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id|| 'Dim_Object: '||I_dim_object);      
   for c_dim_rec in C_GET_CHILD_DIM LOOP
      L_child_item := c_dim_rec.item;
      ---
      if I_dim_object = 'CA' then
         if CHECK_CASE_DIMENSION(O_error_message,
                                 L_exists,
                                 L_child_item,
                                 I_supplier,
                                 I_origin_country_id) = FALSE then
             return FALSE;
          end if;
          ---
          if L_exists = TRUE then
             O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DELETE_DIM_CHILD',
                                                   'Program :'||L_program,
                                                   'Table : ITEM_SUPP_COUNTRY_DIM',
                                                   'Item : '||L_child_item||' Supplier: '||I_supplier||' Origin_Country: '||I_origin_country_id);             return FALSE;
          end if;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
      open C_LOCK_DIMENSIONS;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
      close C_LOCK_DIMENSIONS;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '||I_dim_object);
      delete from item_supp_country_dim
         where supplier = I_supplier
           and origin_country = I_origin_country_id
           and dim_object = I_dim_object
           and item = L_child_item;
   end LOOP;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'Item: '||L_child_item,
                                            'Supplier: '||to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_DIMENSION_TO_CHILDREN;
----------------------------------------------------------------------------------------
FUNCTION INSERT_COUNTRY_TO_CHILDREN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                    I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                    I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                    I_primary_country_ind IN     VARCHAR2,
                                    I_replace_ind    IN     VARCHAR2 DEFAULT 'N')
   return BOOLEAN IS

   L_program                VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.INSERT_COUNTRY_TO_CHILDREN';
   L_child_item             ITEM_MASTER.ITEM%TYPE;
   L_bracket_ind            VARCHAR2(1)    := NULL;
   L_inv_mgmt_level         VARCHAR2(6)    := NULL;

   -- Retrieving the child items for which there is no entry in the item_supp_country for that item/supplier/country combination and does not have primary_country_ind=Y

   cursor C_GET_CHILD_ITEMS_0 is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and not exists (select 'x'
                           from item_supp_country isc
                          where isc.item = im.item
                            and isc.supplier = I_supplier
                            and isc.origin_country_id = I_origin_country_id)
         and not exists (select 'x'
                           from item_supp_country isc
                          where isc.item = im.item
                            and isc.supplier = i_supplier
                            and isc.primary_country_ind='Y');

   -- Retrieving the child items for which there is no entry in the item_supp_country for that item/supplier/country combination.

   cursor C_GET_CHILD_ITEMS is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and not exists (select 'x'
                           from item_supp_country isc
                          where isc.item = im.item
                            and isc.supplier = I_supplier
                            and isc.origin_country_id = I_origin_country_id);

   -- Retrieving the child items for which there is no entry in the item_supp_country for that item/supplier combination.

   cursor C_GET_CHILD_ITEMS_1 is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and not exists (select 'x'
                            from item_supp_country isc
                           where isc.item = im.item
                             and isc.supplier = I_supplier);

   -- Retrieving the child items for which different origin countries exist but there is no entry for parent's
   -- primary origin country.

   cursor C_GET_CHILD_ITEMS_2 is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and exists (select 'x'
                        from item_supp_country isc
                       where isc.item = im.item
                         and isc.supplier = I_supplier)
       MINUS
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and exists (select 'x'
                        from item_supp_country isc
                       where isc.item = im.item
                         and isc.supplier = I_supplier
                         and isc.origin_country_id = I_origin_country_id);

   -- Retrieving the child items for which there exist an entry in the item_supp_country corresponding to
   -- parent's primary origin country.

   cursor C_GET_CHILD_ITEMS_3 is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and exists (select 'x'
                        from item_supp_country isc
                       where isc.item = im.item
                         and isc.supplier = I_supplier
                         and isc.origin_country_id = I_origin_country_id
                         and isc.primary_country_ind = 'N');

   FUNCTION INSERT_FC_EVENT_TO_CHILDREN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_childitem       IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                        I_supp            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                        I_orig_ctry_id    IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
      return BOOLEAN IS

      L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
      L_status                  ITEM_MASTER.STATUS%TYPE;
      L_desc                    ITEM_MASTER.ITEM_DESC%TYPE;
      L_item_loc_tbl            OBJ_ITEMLOC_TBL;
      L_sc_detail_rec           OBJ_SC_COST_EVENT_REC;
      L_sc_detail_tbl           OBJ_SC_COST_EVENT_TBL;

   BEGIN

      if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                  L_desc,
                                  L_status,
                                  I_childitem) = FALSE then
            return FALSE;
      end if;

      if L_status = 'A' then
         if COST_EVENT_LOG_SQL.GET_ITEM_LOCS(O_error_message,
                                             L_item_loc_tbl,
                                             I_childitem) = FALSE then
            return FALSE;
         end if;

         if L_item_loc_tbl.count > 0 then
            L_sc_detail_tbl := new OBJ_SC_COST_EVENT_TBL();
            FOR i in L_item_loc_tbl.first..L_item_loc_tbl.last LOOP
               L_sc_detail_rec := new OBJ_SC_COST_EVENT_REC(I_childitem,
                                                            L_item_loc_tbl(i).loc,
                                                            I_supp,
                                                            I_orig_ctry_id);
               L_sc_detail_tbl.extend;
               L_sc_detail_tbl(L_sc_detail_tbl.count) := L_sc_detail_rec;
            END LOOP;

            if FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY(O_error_message,
                                                      L_cost_event_process_id,
                                                      'ADD',
                                                      L_sc_detail_tbl,
                                                      GET_USER) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      return TRUE;
   END;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_primary_country_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_primary_country_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- get info so that we can check (in for loop) to see if its bracket costing
   if SUPP_ATTRIB_SQL.GET_BRACKET_COSTING_IND(O_error_message,
                                              L_bracket_ind,
                                              I_supplier) = FALSE then
      return FALSE;
   end if;
   if L_bracket_ind = 'Y' then
      if SUP_INV_MGMT_SQL.GET_INV_MGMT_LEVEL(O_error_message,
                                             L_inv_mgmt_level,
                                             I_supplier) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if I_primary_country_ind = 'N' then
      SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS_0','ITEM_MASTER','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id); 
      for c_rec in C_GET_CHILD_ITEMS_0 LOOP
         L_child_item := c_rec.item;
         if INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message,
                                           I_item,
                                           L_child_item,
                                           I_supplier,
                                           I_origin_country_id,
                                           'Y',
                                           'N',
                                           'Y',
                                           L_bracket_ind,
                                           L_inv_mgmt_level) = FALSE then
            return FALSE;
         end if;
      end LOOP;

      SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS','ITEM_MASTER','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
      for c_rec in C_GET_CHILD_ITEMS LOOP
         L_child_item := c_rec.item;
                  if INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message,
                                           I_item,
                                           L_child_item,
                                           I_supplier,
                                           I_origin_country_id,
                                           'N',
                                           'N',
                                           'Y',
                                           L_bracket_ind,
                                           L_inv_mgmt_level) = FALSE then
            return FALSE;
         end if;

         if INSERT_FC_EVENT_TO_CHILDREN(O_error_message,
                                 L_child_item,
                                 I_supplier,
                                 I_origin_country_id) = FALSE then
            return FALSE;
         end if;

      end LOOP;
      ---
   else
      if I_replace_ind = 'N' then
         SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS_1','ITEM_MASTER','Item: '||I_item||' Supplier: '||to_char(I_supplier));       
         for c_rec in C_GET_CHILD_ITEMS_1 LOOP
            L_child_item := c_rec.item;
            if INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message,
                                              I_item,
                                              L_child_item,
                                              I_supplier,
                                              I_origin_country_id,
                                              'Y',
                                              'N',
                                              'Y',
                                              L_bracket_ind,
                                              L_inv_mgmt_level) = FALSE then
               return FALSE;
            end if;

            if INSERT_FC_EVENT_TO_CHILDREN(O_error_message,
                                    L_child_item,
                                    I_supplier,
                                    I_origin_country_id) = FALSE then
               return FALSE;
            end if;

         end LOOP;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS_2','ITEM_MASTER','Item: '||I_item||' Supplier: '||to_char(I_supplier));         
         for c_rec in C_GET_CHILD_ITEMS_2 LOOP
            L_child_item := c_rec.item;
            if INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message,
                                              I_item,
                                              L_child_item,
                                              I_supplier,
                                              I_origin_country_id,
                                              'N',
                                              'N',
                                              'Y',
                                              L_bracket_ind,
                                              L_inv_mgmt_level) = FALSE then
               return FALSE;
            end if;

            if INSERT_FC_EVENT_TO_CHILDREN(O_error_message,
                                    L_child_item,
                                    I_supplier,
                                    I_origin_country_id) = FALSE then
               return FALSE;
            end if;

         end LOOP;
         ---
      else
         SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS_1','ITEM_MASTER','Item: '||I_item||' Supplier: '||to_char(I_supplier));          
         for c_rec in C_GET_CHILD_ITEMS_1 LOOP
            L_child_item := c_rec.item;
            if INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message,
                                              I_item,
                                              L_child_item,
                                              I_supplier,
                                              I_origin_country_id,
                                              'Y',
                                              'N',
                                              'Y',
                                              L_bracket_ind,
                                              L_inv_mgmt_level) = FALSE then
               return FALSE;
            end if;

            if INSERT_FC_EVENT_TO_CHILDREN(O_error_message,
                                    L_child_item,
                                    I_supplier,
                                    I_origin_country_id) = FALSE then
               return FALSE;
            end if;

         end LOOP;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS_2','ITEM_MASTER','Item: '||I_item||' Supplier: '||to_char(I_supplier));          
         for c_rec in C_GET_CHILD_ITEMS_2 LOOP
            L_child_item := c_rec.item;
            if INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message,
                                              I_item,
                                              L_child_item,
                                              I_supplier,
                                              I_origin_country_id,
                                              'Y',
                                              'Y',
                                              'Y',
                                              L_bracket_ind,
                                              L_inv_mgmt_level) = FALSE then
               return FALSE;
            end if;

            if INSERT_FC_EVENT_TO_CHILDREN(O_error_message,
                                    L_child_item,
                                    I_supplier,
                                    I_origin_country_id) = FALSE then
               return FALSE;
            end if;

         end LOOP;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS','ITEM_MASTER','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);         
         for c_rec in C_GET_CHILD_ITEMS_3 LOOP
            L_child_item := c_rec.item;
            if INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message,
                                              I_item,
                                              L_child_item,
                                              I_supplier,
                                              I_origin_country_id,
                                              'Y',
                                              'Y',
                                              'N',
                                              L_bracket_ind,
                                              L_inv_mgmt_level) = FALSE then
               return FALSE;
            end if;

            if INSERT_FC_EVENT_TO_CHILDREN(O_error_message,
                                    L_child_item,
                                    I_supplier,
                                    I_origin_country_id) = FALSE then
               return FALSE;
            end if;

         end LOOP;
         ---
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_COUNTRY_TO_CHILDREN;
-------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                        I_child_item          IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                        I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                        I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                        I_primary_country_ind IN     VARCHAR2,
                                        I_update_ind          IN     VARCHAR2,
                                        I_insert_ind          IN     VARCHAR2,
                                        I_bracket_ind         IN     VARCHAR2,
                                        I_inv_mgmt_level      IN     VARCHAR2)
   return BOOLEAN IS

   L_program                VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.INSERT_COUNTRY_IND_TO_CHILDREN';
   L_child_prim_supp_ind    ITEM_SUPP_COUNTRY.PRIMARY_SUPP_IND%TYPE;
   L_exists                 BOOLEAN;

   cursor C_LOCK_COUNTRY_Y is
      select 'x'
        from item_supp_country
       where primary_country_ind = 'Y'
         and origin_country_id   != I_origin_country_id
         and supplier            = I_supplier
         and item                = I_child_item
         for update nowait;

   cursor C_LOCK_COUNTRY_N is
      select 'x'
        from item_supp_country
       where primary_country_ind = 'N'
         and origin_country_id   = I_origin_country_id
         and supplier            = I_supplier
         and item                = I_child_item
         for update nowait;

   cursor C_PRIMARY_SUPPLIER is
      select primary_supp_ind
        from item_supp_country
       where item = I_child_item
         and supplier = I_supplier;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_child_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_child_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_primary_country_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_primary_country_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_update_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_update_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_insert_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_insert_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_bracket_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_bracket_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_insert_ind = 'Y' then
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_PRIMARY_SUPPLIER','ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier));
      open C_PRIMARY_SUPPLIER;
      SQL_LIB.SET_MARK('FETCH', 'C_PRIMARY_SUPPLIER','ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier));
      fetch C_PRIMARY_SUPPLIER into L_child_prim_supp_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_PRIMARY_SUPPLIER','ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier));
      close C_PRIMARY_SUPPLIER;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
      insert into item_supp_country(item,
                                    supplier,
                                    origin_country_id,
                                    unit_cost,
                                    lead_time,
                                    supp_pack_size,
                                    inner_pack_size,
                                    round_lvl,
                                    round_to_inner_pct,
                                    round_to_case_pct,
                                    round_to_layer_pct,
                                    round_to_pallet_pct,
                                    min_order_qty,
                                    max_order_qty,
                                    packing_method,
                                    primary_supp_ind,
                                    primary_country_ind,
                                    default_uop,
                                    ti,
                                    hi,
                                    supp_hier_type_1,
                                    supp_hier_lvl_1,
                                    supp_hier_type_2,
                                    supp_hier_lvl_2,
                                    supp_hier_type_3,
                                    supp_hier_lvl_3,
                                    pickup_lead_time,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id,
                                    cost_uom,
                                    negotiated_item_cost,
                                    extended_base_cost,
                                    inclusive_cost,
                                    base_cost,
                                    create_id)
                             select I_child_item,
                                    I_supplier,
                                    I_origin_country_id,
                                    isc.unit_cost,
                                    isc.lead_time,
                                    isc.supp_pack_size,
                                    isc.inner_pack_size,
                                    isc.round_lvl,
                                    isc.round_to_inner_pct,
                                    isc.round_to_case_pct,
                                    isc.round_to_layer_pct,
                                    isc.round_to_pallet_pct,
                                    isc.min_order_qty,
                                    isc.max_order_qty,
                                    isc.packing_method,
                                    nvl(L_child_prim_supp_ind, isc.primary_supp_ind),
                                    I_primary_country_ind,
                                    isc.default_uop,
                                    isc.ti,
                                    isc.hi,
                                    isc.supp_hier_type_1,
                                    isc.supp_hier_lvl_1,
                                    isc.supp_hier_type_2,
                                    isc.supp_hier_lvl_2,
                                    isc.supp_hier_type_3,
                                    isc.supp_hier_lvl_3,
                                    isc.pickup_lead_time,
                                    sysdate,
                                    sysdate,
                                    get_user,
                                    isc.cost_uom,
                                    isc.negotiated_item_cost,
                                    isc.extended_base_cost,
                                    isc.inclusive_cost,
                                    isc.base_cost,
                                    get_user
                               from item_supp_country isc
                              where isc.item = I_item
                                and isc.supplier = I_supplier
                                and isc.origin_country_id = I_origin_country_id;

      --- check to see if brackets exist for this child already
      if ITEM_BRACKET_COST_SQL.BRACKETS_EXIST(O_error_message,
                                              L_exists,
                                              I_child_item,
                                              I_supplier,
                                              I_origin_country_id,
                                              NULL) = FALSE then
         return FALSE;
      end if;
      --- create country level bracket cost records for children
      if (I_bracket_ind = 'Y') and
         (L_exists = FALSE) and
         (I_inv_mgmt_level = 'S' or I_inv_mgmt_level = 'D') then
         if ITEM_BRACKET_COST_SQL.CREATE_BRACKET(O_error_message,
                                                 I_child_item,
                                                 I_supplier,
                                                 I_origin_country_id,
                                                 NULL,
                                                 NULL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   --
      if I_update_ind = 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_Y','ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
      open C_LOCK_COUNTRY_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_Y','ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
      close C_LOCK_COUNTRY_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);                       
      update item_supp_country isc1
         set isc1.primary_country_ind  = 'N',
             isc1.last_update_id       = get_user,
             isc1.last_update_datetime = sysdate
       where isc1.primary_country_ind  = 'Y'
         and isc1.origin_country_id   != I_origin_country_id
         and isc1.supplier             = I_supplier
         and isc1.item                 = I_child_item;
      ---
   end if;
   ---
   if I_insert_ind = 'N' and I_update_ind = 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_N','ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
      open C_LOCK_COUNTRY_N;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_N','ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
      close C_LOCK_COUNTRY_N;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY','Item: '||I_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);                       
      update item_supp_country isc1
         set isc1.primary_country_ind  = 'Y',
             isc1.last_update_id       = get_user,
             isc1.last_update_datetime = sysdate
       where isc1.primary_country_ind  = 'N'
         and isc1.origin_country_id    = I_origin_country_id
         and isc1.supplier             = I_supplier
         and isc1.item                 = I_child_item;
      ---
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_COUNTRY_IND_TO_CHILDREN;
-------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PRIMARY_TO_CHILDREN(O_error_message            IN OUT VARCHAR2,
                                   O_child_primary_exists_ind IN OUT VARCHAR2,
                                   I_item                     IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                   I_supplier                 IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                   I_origin_country_id        IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN IS


   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.CHECK_PRIMARY_TO_CHILDREN';
   L_exists                ITEM_MASTER.ITEM%TYPE;

   cursor C_PRIMARY_EXISTS is
      select im.item
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                           from item_supp_country isc
                          where isc.item = im.item
                            and isc.supplier = I_supplier
                            and isc.origin_country_id != I_origin_country_id
                            and isc.primary_country_ind = 'Y');

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_PRIMARY_EXISTS','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   open C_PRIMARY_EXISTS;
   SQL_LIB.SET_MARK('OPEN','C_PRIMARY_EXISTS','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   fetch C_PRIMARY_EXISTS into L_exists;
      if C_PRIMARY_EXISTS%FOUND then
         O_child_primary_exists_ind := 'Y';
      else
         O_child_primary_exists_ind := 'N';
      end if;
   SQL_LIB.SET_MARK('OPEN','C_PRIMARY_EXISTS','ITEM_SUPP_COUNTRY','Item: '||I_item||
                   ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   close C_PRIMARY_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_PRIMARY_TO_CHILDREN;
--------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_DIMENSION_TO_CHILDREN(O_error_message     IN OUT VARCHAR2,
                                      I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                      I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                      I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                                      I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE,
                                      I_insert            IN     VARCHAR2)
   return BOOLEAN IS


   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.UPDATE_DIMENSION_TO_CHILDREN';
   L_table                 VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY_DIM';
   L_child_item            ITEM_MASTER.ITEM%TYPE := NULL;
   L_presentation_method   ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD%TYPE;
   L_length                ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_width                 ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_height                ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_lwh_uom               ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_weight                ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_net_weight            ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_weight_uom            ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_liquid_volume         ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_liquid_volume_uom     ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_stat_cube             ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE;
   L_tare_weight           ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE;
   L_tare_type             ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE;
   ---
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);



   cursor C_GET_CHILD_ITEMS is
      select im.item
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                       from item_supp_country_dim isd
                      where isd.item = im.item
                        and isd.supplier = I_supplier
                        and isd.origin_country = I_origin_country_id);

   cursor C_GET_CHILD_ITEMS_INSERT is
      select im.item
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                       from item_supp_country isc
                       where isc.item = im.item
                         and isc.supplier = I_supplier
                         and isc.origin_country_id = I_origin_country_id)
         and exists (select 'x'
                       from item_supp_country_dim isd
                      where isd.item = I_item
                        and isd.supplier = I_supplier
                        and isd.origin_country = I_origin_country_id);

   cursor C_GET_DIMENSIONS is
      select isd.presentation_method,
             isd.length,
             isd.width,
             isd.height,
             isd.lwh_uom,
             isd.weight,
             isd.net_weight,
             isd.weight_uom,
             isd.liquid_volume,
             isd.liquid_volume_uom,
             isd.stat_cube,
             isd.tare_weight,
             isd.tare_type
        from item_supp_country_dim isd
       where isd.item = I_item
         and isd.supplier = I_supplier
         and isd.origin_country = I_origin_country_id
         and isd.dim_object = I_dim_object;


   cursor C_LOCK_DIMENSIONS is
      select 'x'
        from item_supp_country_dim isd
       where isd.item = L_child_item
         and isd.supplier = I_supplier
         and isd.origin_country = I_origin_country_id
         and isd.dim_object = I_dim_object;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_dim_object is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dim_object',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_insert = 'N' then
      SQL_LIB.SET_MARK('OPEN','C_GET_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
      open C_GET_DIMENSIONS;
      SQL_LIB.SET_MARK('FETCH','C_GET_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
      fetch C_GET_DIMENSIONS into L_presentation_method,
                                  L_length,
                                  L_width,
                                  L_height,
                                  L_lwh_uom,
                                  L_weight,
                                  L_net_weight,
                                  L_weight_uom,
                                  L_liquid_volume,
                                  L_liquid_volume_uom,
                                  L_stat_cube,
                                  L_tare_weight,
                                  L_tare_type;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DIMENSIONS ','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
      close C_GET_DIMENSIONS;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS','ITEM_MASTER','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);      
      for c_rec in C_GET_CHILD_ITEMS LOOP
         L_child_item := c_rec.item;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
         update item_supp_country_dim isd
            set isd.presentation_method = NVL(L_presentation_method, presentation_method),
                isd.length = NVL(L_length, length),
                isd.width = NVL(L_width, width),
                isd.height = NVL(L_height, height),
                isd.lwh_uom = NVL(L_lwh_uom, lwh_uom),
                isd.weight = NVL(L_weight, weight),
                isd.net_weight = NVL(L_net_weight, net_weight),
                isd.weight_uom = NVL(L_weight_uom, weight_uom),
                isd.liquid_volume =  NVL(L_liquid_volume, liquid_volume),
                isd.liquid_volume_uom = NVL(L_liquid_volume_uom, liquid_volume_uom),
                isd.stat_cube =  NVL(L_stat_cube, stat_cube),
                isd.tare_weight =  NVL(L_tare_weight, tare_weight),
                isd.tare_type =  NVL(L_tare_type, tare_type),
                isd.last_update_datetime = sysdate,
                isd.last_update_id = get_user
          where isd.item = L_child_item
            and isd.supplier = I_supplier
            and isd.origin_country = I_origin_country_id
            and isd.dim_object = I_dim_object;
      end LOOP;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS_INSERT','ITEM_MASTER','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);           
      for c_rec in C_GET_CHILD_ITEMS_INSERT LOOP
         L_child_item := c_rec.item;
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
         insert into item_supp_country_dim(item,
                                           supplier,
                                           origin_country,
                                           dim_object,
                                           presentation_method,
                                           length,
                                           width,
                                           height,
                                           lwh_uom,
                                           weight,
                                           net_weight,
                                           weight_uom,
                                           liquid_volume,
                                           liquid_volume_uom,
                                           stat_cube,
                                           tare_weight,
                                           tare_type,
                                           create_datetime,
                                           last_update_datetime,
                                           last_update_id,
                                           create_id)
                                    select L_child_item,
                                           I_supplier,
                                           I_origin_country_id,
                                           I_dim_object,
                                           isd.presentation_method,
                                           isd.length,
                                           isd.width,
                                           isd.height,
                                           isd.lwh_uom,
                                           isd.weight,
                                           isd.net_weight,
                                           isd.weight_uom,
                                           isd.liquid_volume,
                                           isd.liquid_volume_uom,
                                           isd.stat_cube,
                                           isd.tare_weight,
                                           isd.tare_type,
                                           sysdate,
                                           sysdate,
                                           get_user,
                                           get_user
                                      from item_supp_country_dim isd
                                     where isd.item = I_item
                                       and isd.supplier = I_supplier
                                       and isd.origin_country = I_origin_country_id
                                       and isd.dim_object = I_dim_object
                                       and not exists (select 'x'
                                                         from item_supp_country_dim isd
                                                        where isd.item = L_child_item
                                                          and isd.supplier = I_supplier
                                                          and isd.origin_country = I_origin_country_id
                                                          and isd.dim_object = I_dim_object);
      end LOOP;
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS_INSERT','ITEM_MASTER','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);      
      for c_rec in C_GET_CHILD_ITEMS_INSERT LOOP
         L_child_item := c_rec.item;
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
         insert into item_supp_country_dim(item,
                                           supplier,
                                           origin_country,
                                           dim_object,
                                           presentation_method,
                                           length,
                                           width,
                                           height,
                                           lwh_uom,
                                           weight,
                                           net_weight,
                                           weight_uom,
                                           liquid_volume,
                                           liquid_volume_uom,
                                           stat_cube,
                                           tare_weight,
                                           tare_type,
                                           create_datetime,
                                           last_update_datetime,
                                           last_update_id,
                                           create_id)
                                    select L_child_item,
                                           I_supplier,
                                           I_origin_country_id,
                                           I_dim_object,
                                           isd.presentation_method,
                                           isd.length,
                                           isd.width,
                                           isd.height,
                                           isd.lwh_uom,
                                           isd.weight,
                                           isd.net_weight,
                                           isd.weight_uom,
                                           isd.liquid_volume,
                                           isd.liquid_volume_uom,
                                           isd.stat_cube,
                                           isd.tare_weight,
                                           isd.tare_type,
                                           sysdate,
                                           sysdate,
                                           get_user,
                                           get_user
                                      from item_supp_country_dim isd
                                     where isd.item = I_item
                                       and isd.supplier = I_supplier
                                       and isd.origin_country = I_origin_country_id
                                       and isd.dim_object = I_dim_object
                                       and not exists (select 'x'
                                                         from item_supp_country_dim isd
                                                        where isd.item = L_child_item
                                                          and isd.supplier = I_supplier
                                                          and isd.origin_country = I_origin_country_id
                                                          and isd.dim_object = I_dim_object);
      end LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'I_item',
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_DIMENSION_TO_CHILDREN;
-----------------------------------------------------------------------------------------------------------
FUNCTION FIRST_SUPPLIER(O_error_message      IN OUT VARCHAR2,
                        I_item               IN     item_supp_country.item%TYPE,
                        I_supplier           IN     item_supp_country.supplier%TYPE,
                        I_origin_country_id  IN     item_supp_country.origin_country_id%TYPE,
                        I_unit_cost          IN     item_supp_country.unit_cost%TYPE,
                        I_ti                 IN     item_supp_country.ti%TYPE,
                        I_hi                 IN     item_supp_country.hi%TYPE,
                        I_source             IN     VARCHAR2 DEFAULT 'N')
return BOOLEAN IS

   L_program                     VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_SQL.FIRST_SUPPLIER';
   L_table                       VARCHAR2(64) := NULL;
   RECORD_LOCKED                 EXCEPTION;
   PRAGMA                        EXCEPTION_INIT(Record_Locked, -54);

   L_system_options_rec          SYSTEM_OPTIONS%ROWTYPE;
   L_supp_currency               STORE.CURRENCY_CODE%TYPE := NULL;

   L_loc                         item_loc.loc%TYPE := NULL;
   L_loc_type                    item_loc.loc_type%TYPE := NULL;
   L_local_currency              store.currency_code%TYPE := NULL;

   L_local_cost                  item_loc_soh.unit_cost%TYPE := NULL;
   L_elc_cost                    item_loc_soh.unit_cost%TYPE := NULL;
   L_pack_ind                    item_master.pack_ind%TYPE := NULL;
   L_sellable                    item_master.sellable_ind%TYPE := NULL;
   L_orderable                   item_master.orderable_ind%TYPE := NULL;
   L_pack_type                   item_master.pack_type%TYPE := NULL;

   /* not used, place holders for package call */
   L_total_exp                   item_supp_country.unit_cost%TYPE := NULL;
   L_exp_currency                currencies.currency_code%TYPE := NULL;
   L_exchange_rate_exp           currency_rates.exchange_rate%TYPE := NULL;
   L_total_duty                  item_supp_country.unit_cost%TYPE := NULL;
   L_dty_currency                currencies.currency_code%TYPE := NULL;

   cursor C_CHECK_ITEM_LOC is
       select il.loc,
              il.loc_type,
              st.currency_code
         from item_loc il,
              store st
        where (il.item = I_item or
               il.item_parent = I_item or
               il.item_grandparent = I_item)
          and il.loc = st.store
          and ((il.primary_supp is NULL and I_source = 'N')
               or I_source = 'Y')
    union all
       select il.loc,
              il.loc_type,
              w.currency_code
         from item_loc il,
              wh w
        where (il.item = I_item or
               il.item_parent = I_item or
               il.item_grandparent = I_item)
          and il.loc = w.wh
          and ((il.primary_supp is NULL and I_source = 'N')
               or I_source = 'Y');

   cursor C_LOCK_ITEM_LOC is
       select 'x'
         from item_loc
        where item = I_item
          and loc = L_loc
          for update nowait;

   cursor C_LOCK_ITEM_LOC_SOH is
       select 'x'
         from item_loc_soh
        where item = I_item
          and loc = L_loc
          for update nowait;
   cursor C_LOCK_FUTURE_COST (L_primary_supplier   ITEM_LOC.PRIMARY_SUPP%TYPE,
                              L_primary_country    ITEM_LOC.PRIMARY_CNTRY%TYPE) is
      select 'N' upd_primary_supp_cntry_ind,
             rowid upd_rowid
        from future_cost
       where item = I_item
         and location = L_loc
         and ( supplier <> L_primary_supplier or
               origin_country_id <> L_primary_country)
         for update of primary_supp_country_ind nowait;
    

   TYPE primary_supp_cntry_ind_tab  IS TABLE OF FUTURE_COST.PRIMARY_SUPP_COUNTRY_IND%TYPE;
   TYPE future_cost_upd_rowid_tab   IS TABLE OF ROWID;
   L_primary_supp_cntry_ind_arr     primary_supp_cntry_ind_tab := primary_supp_cntry_ind_tab();
   L_future_cost_upd_rowid_arr      future_cost_upd_rowid_tab := future_cost_upd_rowid_tab();
          


BEGIN

   /* make sure required parameters are populated */
   if I_item is NULL then
      O_error_message:= SQL_LIB.create_msg('REQUIRED_INPUT_IS_NULL', 'I_item',
                                           L_program, NULL);
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.create_msg('REQUIRED_INPUT_IS_NULL', 'I_supplier',
                                           L_program, NULL);
      return FALSE;
   end if;
   if I_origin_country_id is NULL then
      O_error_message:= SQL_LIB.create_msg('REQUIRED_INPUT_IS_NULL', 'I_origin_country_id',
                                           L_program, NULL);
      return FALSE;
   end if;
   if I_unit_cost is NULL then
      O_error_message:= SQL_LIB.create_msg('REQUIRED_INPUT_IS_NULL', 'I_unit_cost',
                                           L_program, NULL);
      return FALSE;
   end if;
   ---
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   ---
   if not SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                            L_supp_currency,
                                            I_supplier) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_ITEM_LOC','ITEM_LOC','Item: '||I_item||' Source: '||I_source); 
   FOR c_rec in C_CHECK_ITEM_LOC LOOP

      L_loc := c_rec.loc;
      L_loc_type := c_rec.loc_type;
      L_local_currency := c_rec.currency_code;

      if L_system_options_rec.elc_ind = 'Y' then
         if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                         L_elc_cost,
                                         L_total_exp,
                                         L_exp_currency,
                                         L_exchange_rate_exp,
                                         L_total_duty,
                                         L_dty_currency,
                                         NULL,                 --order_no
                                         I_item,
                                         NULL,                 --comp_sku
                                         NULL,                 --zone_id
                                         NULL,                 --location
                                         I_supplier,
                                         I_origin_country_id,
                                         NULL,                 --import_country_id
                                         I_unit_cost) then
            return FALSE;
         end if;

         /* convert the elc cost back to the location's currency */
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_loc,
                                             L_loc_type,
                                             NULL,
                                             L_elc_cost,
                                             L_local_cost,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
             return FALSE;
         end if;

      else /* elc is not being used */

         if L_local_currency != L_supp_currency then
            if not CURRENCY_SQL.CONVERT(O_error_message,
                                        I_unit_cost,
                                        L_supp_currency,
                                        L_local_currency,
                                        L_local_cost,
                                        'C',
                                        NULL,
                                        NULL) then
               return FALSE;
            end if;
         else
            L_local_cost := I_unit_cost;
         end if;

      end if;

      /* if item is a pack item (vendor or buyer), then unit cost won't be updated on item_loc */
      if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                       L_pack_ind,
                                       L_sellable,
                                       L_orderable,
                                       L_pack_type,
                                       I_item) = FALSE then
         return FALSE;
      end if;
      ---
      if L_pack_ind = 'Y' then
         L_local_cost := NULL;
      end if;
      ---
      if I_source = 'N' then
         L_table := 'ITEM_LOC';
         SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_LOC','ITEM_LOC','Item: '||I_item||' Loc: '||to_char(L_loc)); 
         open C_LOCK_ITEM_LOC;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_LOC','ITEM_LOC','Item: '||I_item||' Loc: '||to_char(L_loc));          
         close C_LOCK_ITEM_LOC;
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_LOC', 'Item: '||I_item ||' Supplier: '||to_char(I_supplier)|| ' Loc: ' ||to_char(L_loc));         
         update item_loc il
            set primary_supp         = I_supplier,
                primary_cntry        = I_origin_country_id,
                ti                   = NVL(ti, I_ti),
                hi                   = NVL(hi, I_hi),
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where (il.item             = I_item or
                 il.item_parent      = I_item or
                 il.item_grandparent = I_item)
            and loc = L_loc;
      end if;
      L_table := 'FUTURE_COST';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Location: '|| to_char(L_loc)||
                       ' Supplier: '|| to_char(I_supplier)||
                  ' Origin_Country: '||I_origin_country_id);
      open C_LOCK_FUTURE_COST(I_supplier,
                              I_origin_country_id);
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Location: '|| to_char(L_loc)||
                       ' Supplier: '|| to_char(I_supplier)||
                  ' Origin_Country: '||I_origin_country_id);
      fetch C_LOCK_FUTURE_COST bulk collect into L_primary_supp_cntry_ind_arr,
                                                 L_future_cost_upd_rowid_arr;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Location: '|| to_char(L_loc)||
                       ' Supplier: '|| to_char(I_supplier)||
                  ' Origin_Country: '||I_origin_country_id);
      close C_LOCK_FUTURE_COST;
      --- Set primary_supp_country_ind for non primary Supplier/Country ,
      --- future cost engine will process records for primary Supplier/Country
      forall i in  L_future_cost_upd_rowid_arr.first..L_future_cost_upd_rowid_arr.last
         update future_cost
            set primary_supp_country_ind = L_primary_supp_cntry_ind_arr(i)
          where rowid                    = L_future_cost_upd_rowid_arr(i) ;
      ---
      L_table := 'ITEM_LOC_SOH';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_LOC','ITEM_LOC','Item: '||I_item||' Loc: '||to_char(L_loc)); 
      open C_LOCK_ITEM_LOC_SOH;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_LOC','ITEM_LOC','Item: '||I_item||' Loc: '||to_char(L_loc));       
      close C_LOCK_ITEM_LOC_SOH;
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_LOC_SOH', 'Item: '||I_item || ' Loc: ' ||to_char(L_loc)||' Supplier: '|| to_char(I_supplier));
      update item_loc_soh
         set unit_cost = I_unit_cost,
             av_cost = I_unit_cost,
             last_update_datetime = sysdate,
             last_update_id = get_user,
             primary_supp  = I_supplier,
             primary_cntry = I_origin_country_id
       where item = I_item
         and loc = L_loc;
      ---
   end LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               'Item: '||I_item,
                                                               'Loc: '||to_char(L_loc));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END FIRST_SUPPLIER;
---------------------------------------------------------------------------------------------------
FUNCTION GET_TI_HI(O_error_message OUT VARCHAR2,
                   O_ti            OUT item_supp_country.ti%TYPE,
                   O_hi            OUT item_supp_country.hi%TYPE,
                   I_item          IN item_master.item%TYPE)
return BOOLEAN IS

L_program VARCHAR2(62):= 'ITEM_SUPP_COUNTRY_SQL.GET_TI_HI';

cursor C_GET_PRIM_TI_HI is
   select ti,
          hi
     from item_supp_country
    where primary_supp_ind = 'Y'
      and primary_country_ind = 'Y'
      and item = I_item;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item);    
   open C_GET_PRIM_TI_HI;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item);    
   fetch C_GET_PRIM_TI_HI into O_ti,
                               O_hi;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item);                                 
   close C_GET_PRIM_TI_HI;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TI_HI;
---------------------------------------------------------------------------------------------------
FUNCTION GET_TI_HI(O_error_message OUT VARCHAR2,
                   O_ti            OUT item_supp_country.ti%TYPE,
                   O_hi            OUT item_supp_country.hi%TYPE,
                   I_item          IN  item_master.item%TYPE,
                   I_supplier      IN  sups.supplier%TYPE)
return BOOLEAN IS

L_program VARCHAR2(62):= 'ITEM_SUPP_COUNTRY_SQL.GET_TI_HI';

cursor C_GET_PRIM_TI_HI is
   select ti,
          hi
     from item_supp_country
    where supplier = I_supplier
      and primary_country_ind = 'Y'
      and item = I_item;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item||' Supplier: '||to_char(I_supplier));    
   open C_GET_PRIM_TI_HI;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item||' Supplier: '||to_char(I_supplier));    
   fetch C_GET_PRIM_TI_HI into O_ti,
                               O_hi;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_GET_PRIM_TI_HI;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TI_HI;
---------------------------------------------------------------------------------------------------
FUNCTION GET_TI_HI(O_error_message OUT VARCHAR2,
                   O_ti            OUT item_supp_country.ti%TYPE,
                   O_hi            OUT item_supp_country.hi%TYPE,
                   I_item          IN  item_master.item%TYPE,
                   I_supplier      IN  sups.supplier%TYPE,
                   I_country       IN  country.country_id%TYPE)
return BOOLEAN IS

L_program VARCHAR2(62):= 'ITEM_SUPP_COUNTRY_SQL.GET_TI_HI';

cursor C_GET_PRIM_TI_HI is
   select ti,
          hi
     from item_supp_country
    where supplier = I_supplier
      and origin_country_id = I_country
      and item = I_item;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_country',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_country); 
   open C_GET_PRIM_TI_HI;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_country);    
   fetch C_GET_PRIM_TI_HI into O_ti,
                               O_hi;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIM_TI_HI','ITEM_SUPP_COUNTRY','Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_country);                               
   close C_GET_PRIM_TI_HI;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TI_HI;
---------------------------------------------------------------------------------------------------
FUNCTION EXPENSES_EXIST(O_error_message      IN OUT VARCHAR2,
                        O_exists             IN OUT BOOLEAN,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
 return BOOLEAN IS

 L_program          VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_SQL.EXPENSES_EXIST';
 L_expense          VARCHAR2(1);

 cursor C_ITEM_EXP is
     select 'x'
       from item_exp_head ieh
      where ieh.supplier = I_supplier
        and ieh.item     = I_item
        and ieh.origin_country_id = I_origin_country_id;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);      


      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_EXP','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   open C_ITEM_EXP;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_EXP','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   fetch C_ITEM_EXP into L_expense;
      if C_ITEM_EXP%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_EXP','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_origin_country_id);
   close C_ITEM_EXP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPENSES_EXIST;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_SUPP_CNTRY(O_error_message OUT VARCHAR2,
                             O_prim_supp     OUT sups.supplier%TYPE,
                             O_prim_cntry    OUT country.country_id%TYPE,
                             I_item          IN  item_master.item%TYPE)
  return BOOLEAN is

L_program VARCHAR2(62):= 'ITEM_SUPP_COUNTRY_SQL.GET_PRIM_SUPP_CNTRY';

cursor C_GET_PRIM_SUPP_CNTRY is
   select supplier,
          origin_country_id
     from item_supp_country
    where item = I_item
      and primary_supp_ind = 'Y'
      and primary_country_ind = 'Y';

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIM_SUPP_CNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);   
   open C_GET_PRIM_SUPP_CNTRY;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIM_SUPP_CNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);   
   fetch C_GET_PRIM_SUPP_CNTRY into O_prim_supp,
                                    O_prim_cntry;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIM_SUPP_CNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);                                    
   close C_GET_PRIM_SUPP_CNTRY;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIM_SUPP_CNTRY;
---------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_PRIM_CASE_SIZE(O_error_message      IN OUT VARCHAR2,
                                O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.supp_pack_size%TYPE,
                                O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.inner_pack_size%TYPE,
                                I_item               IN     ITEM_SUPP_COUNTRY.item%TYPE)

return BOOLEAN is

L_program VARCHAR2(62):= 'ITEM_SUPP_COUNTRY_SQL.DEFAULT_PRIM_CASE_SIZE';

cursor C_GET_PRIM_CASE_SIZE is
   select supp_pack_size,
          inner_pack_size
     from item_supp_country
    where item = I_item
      and primary_supp_ind = 'Y'
      and primary_country_ind = 'Y';


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
    ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIM_CASE_SIZE','ITEM_SUPP_COUNTRY','Item: '||I_item);
   open C_GET_PRIM_CASE_SIZE;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIM_CASE_SIZE','ITEM_SUPP_COUNTRY','Item: '||I_item);
   fetch C_GET_PRIM_CASE_SIZE into O_supp_pack_size,
                                   O_inner_pack_size;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIM_CASE_SIZE','ITEM_SUPP_COUNTRY','Item: '||I_item);
   close C_GET_PRIM_CASE_SIZE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_PRIM_CASE_SIZE;
---------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_PRIM_CASE_DIMENSIONS(O_error_message       IN OUT VARCHAR2,
                                      I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                      I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                      I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                      O_exists              IN OUT BOOLEAN,
                                      O_dim_object          IN OUT ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE,
                                      O_presentation_method IN OUT ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD%TYPE,
                                      O_length              IN OUT ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                                      O_width               IN OUT ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE,
                                      O_height              IN OUT ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                                      O_lwh_uom             IN OUT ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                                      O_weight              IN OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                                      O_net_weight          IN OUT ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE,
                                      O_weight_uom          IN OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                                      O_liquid_volume       IN OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
                                      O_liquid_volume_uom   IN OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE,
                                      O_stat_cube           IN OUT ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE,
                                      O_tare_weight         IN OUT ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE,
                                      O_tare_type           IN OUT ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE)

return BOOLEAN is

L_program VARCHAR2(62):= 'ITEM_SUPP_COUNTRY_SQL.DEFAULT_PRIM_CASE_DIMENSIONS';

L_supplier             ITEM_SUPP_COUNTRY.SUPPLIER%TYPE;
L_origin_country_id    ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;

cursor C_GET_PRIMARY is
   select origin_country_id,
          supplier
     from item_supp_country isc
    where isc.item = I_item
      and isc.primary_supp_ind = 'Y'
      and isc.primary_country_ind = 'Y';

cursor C_GET_PRIM_CASE_DIMENSIONS is
   select dim_object,
          presentation_method,
          length,
          width,
          height,
          lwh_uom,
          weight,
          net_weight,
          weight_uom,
          liquid_volume,
          liquid_volume_uom,
          stat_cube,
          tare_weight,
          tare_type
     from item_supp_country_dim isd
    where isd.item = I_item
      and isd.supplier = L_supplier
      and isd.origin_country = L_origin_country_id
      and isd.dim_object = 'CA';

BEGIN
    if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
     ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY','ITEM_SUPP_COUNTRY','Item: '||I_item);   
   for c_rec in C_GET_PRIMARY LOOP
      L_supplier := c_rec.supplier;
      L_origin_country_id := c_rec.origin_country_id;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_PRIM_CASE_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item);
      open C_GET_PRIM_CASE_DIMENSIONS;
      SQL_LIB.SET_MARK('FETCH','C_GET_PRIM_CASE_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item);
      fetch C_GET_PRIM_CASE_DIMENSIONS into O_dim_object,
                                         O_presentation_method,
                                         O_length,
                                         O_width,
                                         O_height,
                                         O_lwh_uom,
                                         O_weight,
                                         O_net_weight,
                                         O_weight_uom,
                                         O_liquid_volume,
                                         O_liquid_volume_uom,
                                         O_stat_cube,
                                         O_tare_weight,
                                         O_tare_type;
         if C_GET_PRIM_CASE_DIMENSIONS%FOUND then
            O_exists := TRUE;
         else
            O_exists := FALSE;
         end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PRIM_CASE_DIMENSIONS','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item);
      close C_GET_PRIM_CASE_DIMENSIONS;
   end LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_PRIM_CASE_DIMENSIONS;
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_CASE_DIMENSION(O_error_message      IN OUT VARCHAR2,
                              O_exists             IN OUT BOOLEAN,
                              I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                              I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                              I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)

return BOOLEAN IS

   L_program         VARCHAR2(64)  := 'ITEM_SUPP_COUNTRY_SQL.CHECK_CASE_DIMENSION';
   L_country_exists  BOOLEAN;
   L_exists          VARCHAR2(1);


   cursor CHECK_STANDARD_UOM is
      select 'x'
        from item_master im
       where im.item = I_item
         and exists( select 'x'
                       from uom_class uc1
                      where uc1.uom = im.standard_uom
                        and uc1.uom_class = 'QTY' );
   cursor CHECK_PRIMARY is
      select 'x'
        from item_supp_country isc
       where isc.item                 = I_item
         and isc.supplier             = I_supplier
         and isc.origin_country_id    = I_origin_country_id
         and isc.primary_country_ind  = 'Y'
         and isc.primary_supp_ind     = 'Y';

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ITEM',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_SUPPLIER',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'CHECK_PRIMARY',
                    'ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
   open CHECK_PRIMARY;
   SQL_LIB.SET_MARK('FETCH',
                    'CHECK_PRIMARY',
                    'ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
   fetch CHECK_PRIMARY into L_exists;
      if CHECK_PRIMARY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'CHECK_PRIMARY',
                          'ITEM_SUPP_COUNTRY',
                          'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
         close CHECK_PRIMARY;
         O_exists := FALSE;
         return TRUE;
      else
         SQL_LIB.SET_MARK('CLOSE',
                          'CHECK_PRIMARY',
                          'ITEM_SUPP_COUNTRY',
                          'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
         close CHECK_PRIMARY;
      end if;

   --- Determine if there is a need to check for the CASE dimension.
   SQL_LIB.SET_MARK('OPEN',
                    'CHECK_STANDARD_UOM',
                    'ITEM_MASTER, UOM_CLASS',
                    'Item: '||I_item);
   open CHECK_STANDARD_UOM;
   SQL_LIB.SET_MARK('FETCH',
                    'CHECK_STANDARD_UOM',
                    'ITEM_MASTER, UOM_CLASS',
                    'Item: '||I_item);
   fetch CHECK_STANDARD_UOM into L_exists;
   if CHECK_STANDARD_UOM%FOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'CHECK_STANDARD_UOM',
                       'ITEM_MASTER, UOM_CLASS',
                       'Item: '||I_item);
      close CHECK_STANDARD_UOM;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DELETE_CASE_DIM',
                                             'Program : '||L_program,
                                             'Table : ITEM_SUPP_COUNTRY_DIM',
                                             'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
      ---
      O_exists := TRUE;
      return TRUE;
   else
      O_exists := FALSE;
      SQL_LIB.SET_MARK('CLOSE',
                       'CHECK_STANDARD_UOM',
                       'ITEM_MASTER, UOM_CLASS',
                       'Item: '||I_item);
      close CHECK_STANDARD_UOM;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_CASE_DIMENSION;
-----------------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_PARAMETERS(O_error_message     IN OUT VARCHAR2,
                                O_primary_ind       IN OUT ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE,
                                O_supp_pack_size    IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN IS


   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.GET_COUNTRY_PARAMETERS';


   cursor C_GET_PRIMARY_IND is
      select primary_country_ind,
             supp_pack_size
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_IND','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);
   open C_GET_PRIMARY_IND;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIMARY_IND','ITEM_SUPP_COUNTRY','ITEM: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);
   fetch C_GET_PRIMARY_IND into O_primary_ind,
                                O_supp_pack_size;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIMARY_IND','ITEM_SUPP_COUNTRY','ITEM: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);
   close C_GET_PRIMARY_IND;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_COUNTRY_PARAMETERS;
-----------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_COUNTRY_TO_CHILDREN(O_error_message       IN OUT VARCHAR2,
                                    I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                    I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                    I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                    I_edit_cost           IN     VARCHAR2)
   return BOOLEAN IS


   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.UPDATE_COUNTRY_TO_CHILDREN';
   L_table                 VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY';
   L_child_item            ITEM_MASTER.ITEM%TYPE;
   L_parent_cost           ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := NULL;
   L_child_cost            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := NULL;
   L_lead_time             ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_pickup_lead_time      ITEM_SUPP_COUNTRY.PICKUP_LEAD_TIME%TYPE;
   L_supp_pack_size        ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_inner_pack_size       ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE;
   L_min_order_qty         ITEM_SUPP_COUNTRY.MIN_ORDER_QTY%TYPE;
   L_max_order_qty         ITEM_SUPP_COUNTRY.MAX_ORDER_QTY%TYPE;
   L_packing_method        ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE;
   L_default_uop           ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE;
   L_ti                    ITEM_SUPP_COUNTRY.TI%TYPE;
   L_hi                    ITEM_SUPP_COUNTRY.HI%TYPE;
   L_supp_hier_lvl_1       ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_1%TYPE;
   L_supp_hier_type_1      ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_1%TYPE;
   L_supp_hier_lvl_2       ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_2%TYPE;
   L_supp_hier_type_2      ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_2%TYPE;
   L_supp_hier_lvl_3       ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_3%TYPE;
   L_supp_hier_type_3      ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_3%TYPE;
   L_round_level           ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE;
   L_to_inner_pct          ITEM_SUPP_COUNTRY.ROUND_TO_INNER_PCT%TYPE;
   L_to_case_pct           ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE;
   L_to_layer_pct          ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE;
   L_to_pallet_pct         ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE;
   ---
   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   L_negotiated_item_cost   ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE;
   L_extended_base_cost     ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost         ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE;
   L_base_cost              ITEM_SUPP_COUNTRY.BASE_COST%TYPE;
   ---
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);



   cursor C_GET_CHILD_ITEMS is
      select im.item
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                       from item_supp_country isc
                      where isc.item = im.item
                        and isc.supplier = I_supplier
                        and isc.origin_country_id = I_origin_country_id);

   cursor C_GET_COUNTRY_DETAILS is
      select isc.lead_time,
             isc.pickup_lead_time,
             isc.supp_pack_size,
             isc.inner_pack_size,
             isc.round_lvl,
             isc.round_to_inner_pct,
             isc.round_to_case_pct,
             isc.round_to_layer_pct,
             isc.round_to_pallet_pct,
             isc.min_order_qty,
             isc.max_order_qty,
             isc.packing_method,
             isc.default_uop,
             isc.ti,
             isc.hi,
             isc.supp_hier_lvl_1,
             isc.supp_hier_type_1,
             isc.supp_hier_lvl_2,
             isc.supp_hier_type_2,
             isc.supp_hier_lvl_3,
             isc.supp_hier_type_3
        from item_supp_country isc
       where isc.item = I_item
         and isc.supplier = I_supplier
         and isc.origin_country_id = I_origin_country_id;

   cursor C_GET_PARENT_COST is
      select isc.unit_cost
        from item_supp_country isc
       where isc.item = I_item
         and isc.supplier = I_supplier
         and isc.origin_country_id = I_origin_country_id;

   cursor C_GET_CHILD_COST is
      select isc.unit_cost
        from item_supp_country isc
       where isc.item = L_child_item
         and isc.supplier = I_supplier
         and isc.origin_country_id = I_origin_country_id;

   cursor C_LOCK_COUNTRY_DETAILS is
      select 'x'
        from item_supp_country isc
       where isc.item = L_child_item
         and isc.supplier = I_supplier
         and isc.origin_country_id = I_origin_country_id;


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_COUNTRY_DETAILS','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   open C_GET_COUNTRY_DETAILS;
   SQL_LIB.SET_MARK('FETCH','C_GET_COUNTRY_DETAILS','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   fetch C_GET_COUNTRY_DETAILS into L_lead_time,
                                    L_pickup_lead_time,
                                    L_supp_pack_size,
                                    L_inner_pack_size,
                                    L_round_level,
                                    L_to_inner_pct,
                                    L_to_case_pct,
                                    L_to_layer_pct,
                                    L_to_pallet_pct,
                                    L_min_order_qty,
                                    L_max_order_qty,
                                    L_packing_method,
                                    L_default_uop,
                                    L_ti,
                                    L_hi,
                                    L_supp_hier_lvl_1,
                                    L_supp_hier_type_1,
                                    L_supp_hier_lvl_2,
                                    L_supp_hier_type_2,
                                    L_supp_hier_lvl_3,
                                    L_supp_hier_type_3;
   SQL_LIB.SET_MARK('CLOSE','C_GET_COUNTRY_DETAILS','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   close C_GET_COUNTRY_DETAILS;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS','ITEM_MASTER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);   
   for c_rec in C_GET_CHILD_ITEMS LOOP
      L_child_item := c_rec.item;
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_DETAILS','ITEM_SUPP_COUNTRY','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
      open C_LOCK_COUNTRY_DETAILS;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_DETAILS','ITEM_SUPP_COUNTRY','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
      close C_LOCK_COUNTRY_DETAILS;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
      update item_supp_country isc
         set isc.lead_time = L_lead_time,
             isc.pickup_lead_time = L_pickup_lead_time,
             isc.supp_pack_size = L_supp_pack_size,
             isc.inner_pack_size = L_inner_pack_size,
             isc.round_lvl = L_round_level,
             isc.round_to_inner_pct = L_to_inner_pct,
             isc.round_to_case_pct = L_to_case_pct,
             isc.round_to_layer_pct = L_to_layer_pct,
             isc.round_to_pallet_pct = L_to_pallet_pct,
             isc.min_order_qty = L_min_order_qty,
             isc.max_order_qty = L_max_order_qty,
             isc.packing_method = L_packing_method,
             isc.default_uop = L_default_uop,
             isc.ti = L_ti,
             isc.hi = L_hi,
             isc.supp_hier_lvl_1 = L_supp_hier_lvl_1,
             isc.supp_hier_type_1 = L_supp_hier_type_1,
             isc.supp_hier_lvl_2 = L_supp_hier_lvl_2,
             isc.supp_hier_type_2 = L_supp_hier_type_2,
             isc.supp_hier_lvl_3 = L_supp_hier_lvl_3,
             isc.supp_hier_type_3 = L_supp_hier_type_3,
             isc.last_update_datetime = sysdate,
             isc.last_update_id = get_user
       where isc.item = L_child_item
         and isc.supplier = I_supplier
         and isc.origin_country_id = I_origin_country_id;
      ---
      if I_edit_cost = 'Y' then
         SQL_LIB.SET_MARK('OPEN','C_GET_PARENT_COST','ITEM_SUPP_COUNTRY','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
         open C_GET_PARENT_COST;
         SQL_LIB.SET_MARK('FETCH','C_GET_PARENT_COST','ITEM_SUPP_COUNTRY','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
         fetch C_GET_PARENT_COST into L_parent_cost;
         SQL_LIB.SET_MARK('CLOSE','C_GET_PARENT_COST', 'ITEM_SUPP_COUNTRY','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
         close C_GET_PARENT_COST;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
         ---
         if L_system_options_rec.default_tax_type = 'GTAX' then 

            update item_supp_country isc
               set isc.unit_cost = L_parent_cost
             where isc.item = L_child_item
               and isc.supplier = I_supplier
               and isc.origin_country_id = I_origin_country_id;

         else -- default_tax_type SALES, SVAT or NULL
            update item_supp_country isc
               set isc.unit_cost = L_parent_cost,
                   negotiated_item_cost = NULL,
                   extended_base_cost   = NULL,
                   inclusive_cost       = NULL,
                   base_cost            = NULL
             where isc.item = L_child_item
               and isc.supplier = I_supplier
               and isc.origin_country_id = I_origin_country_id;
         end if;--End if Default Tax Type
      end if;
   end LOOP;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'Item : '||L_child_item,
                                            ' Supplier: '||to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_COUNTRY_TO_CHILDREN;
-----------------------------------------------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message       OUT VARCHAR2,
                       O_unit_cost           OUT item_supp_country.unit_cost%TYPE,
                       I_item                IN  item_master.item%TYPE,
                       I_supplier            IN  sups.supplier%TYPE,
                       I_origin_country_id   IN  country.country_id%TYPE)
return BOOLEAN IS

L_program VARCHAR2(62):= 'ITEM_SUPP_COUNTRY_SQL.GET_UNIT_COST';

cursor C_GET_UNIT_COST is
   select unit_cost
     from item_supp_country
    where supplier = I_supplier
      and origin_country_id = I_origin_country_id
      and item = I_item;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   open C_GET_UNIT_COST;
   SQL_LIB.SET_MARK('FETCH','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   fetch C_GET_UNIT_COST into O_unit_cost;
   SQL_LIB.SET_MARK('CLOSE','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id);
   close C_GET_UNIT_COST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_UNIT_COST;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_CONST_DIMENSIONS(O_error_message       OUT VARCHAR2,
                                 I_item                IN  item_master.item%TYPE,
                                 I_supplier            IN  sups.supplier%TYPE,
                                 I_origin_country_id   IN  country.country_id%TYPE,
                                 I_dim_object          IN  item_supp_country_dim.dim_object%TYPE,
                                 I_delete_children     IN  VARCHAR2)
   return BOOLEAN IS

   L_program               VARCHAR2(62):= 'ITEM_SUPP_COUNTRY_SQL.DELETE_CONST_DIMENSIONS';
   L_table                 VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY_DIM';
   L_child_item            ITEM_MASTER.ITEM%TYPE;
   ---
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);



   cursor C_LOCK_DIM_OBJECT is
      select 'x'
        from item_supp_country_dim
       where item = I_item
         and supplier != I_supplier
         and origin_country != I_origin_country_id
         and dim_object = NVL(I_dim_object, dim_object);

   cursor C_GET_CHILD_ITEMS is
         select im.item
           from item_master im
          where (im.item_parent = I_item
                 or im.item_grandparent = I_item)
            and im.item_level <= im.tran_level
            and exists (select 'x'
                          from item_supp_country_dim isd
                         where isd.item = im.item
                           and supplier != I_supplier
                           and origin_country != I_origin_country_id);

   cursor C_LOCK_CHILD_DIM_OBJECT is
      select 'x'
        from item_supp_country_dim
       where item = L_child_item
         and supplier != I_supplier
         and origin_country != I_origin_country_id
         and dim_object = NVL(I_dim_object, dim_object);


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_DIM_OBJECT','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '|| I_dim_object);
   open C_LOCK_DIM_OBJECT;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_DIM_OBJECT','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '|| I_dim_object);
   close C_LOCK_DIM_OBJECT;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||
                    ' Dim_Object: '|| I_dim_object);
   delete from item_supp_country_dim
      where item = I_item
        and (supplier != I_supplier
             or origin_country != I_origin_country_id)
        and dim_object = NVL(I_dim_object, dim_object);
   ---
   if I_delete_children = 'Y' then
      for c_rec in C_GET_CHILD_ITEMS LOOP
         ---
         L_child_item := c_rec.item;
         ---
         SQL_LIB.SET_MARK('OPEN','C_LOCK_CHILD_DIM_OBJECT','ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '|| I_dim_object);
         open C_LOCK_DIM_OBJECT;
                  SQL_LIB.SET_MARK('CLOSE','C_LOCK_CHILD_DIM_OBJECT','ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||' Dim_Object: '|| I_dim_object);
         close C_LOCK_DIM_OBJECT;
         ---
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_DIM','Item: '||L_child_item||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '|| I_origin_country_id||
                          ' Dim_Object: '|| I_dim_object);
        delete from item_supp_country_dim
           where item = L_child_item
             and (supplier != I_supplier
                 or origin_country != I_origin_country_id)
             and dim_object = NVL(I_dim_object, dim_object);
      end LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'Item : '||I_item,
                                            ' Supplier: '||to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_CONST_DIMENSIONS;
-------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_PRIMARY_EXISTS(O_error_message            IN OUT VARCHAR2,
                                    O_exists                   IN OUT VARCHAR2,
                                    I_item                     IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                    I_supplier                 IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
   return BOOLEAN IS


   L_program               VARCHAR2(62)   := 'ITEM_SUPP_COUNTRY_SQL.CHECK_CHILD_PRIMARY_EXISTS';
   L_exists                ITEM_MASTER.ITEM%TYPE;

   cursor C_PRIMARY_EXISTS is
      select im.item
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                           from item_supp_country isc
                          where isc.item = im.item
                            and isc.supplier = I_supplier
                            and isc.primary_country_ind = 'Y');

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_PRIMARY_EXISTS','ITEM_MASTER,ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_PRIMARY_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_PRIMARY_EXISTS','ITEM_MASTER,ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   fetch C_PRIMARY_EXISTS into L_exists;
      if C_PRIMARY_EXISTS%FOUND then
         O_exists := 'Y';
      else
         O_exists := 'N';
      end if;
   SQL_LIB.SET_MARK('CLOSE','C_PRIMARY_EXISTS','ITEM_MASTER,ITEM_SUPP_COUNTRY','Item: '||I_item||
                   ' Supplier: '||to_char(I_supplier));
   close C_PRIMARY_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CHILD_PRIMARY_EXISTS;
--------------------------------------------------------------------------------------------------------------
FUNCTION GET_DFLT_RND_LVL_ITEM (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_round_level     IN OUT  SUP_INV_MGMT.ROUND_LVL%TYPE,
                                O_to_inner_pct    IN OUT  SUP_INV_MGMT.ROUND_TO_INNER_PCT%TYPE,
                                O_to_case_pct     IN OUT  SUP_INV_MGMT.ROUND_TO_CASE_PCT%TYPE,
                                O_to_layer_pct    IN OUT  SUP_INV_MGMT.ROUND_TO_LAYER_PCT%TYPE,
                                O_to_pallet_pct   IN OUT  SUP_INV_MGMT.ROUND_TO_PALLET_PCT%TYPE,
                                I_item            IN      ITEM_MASTER.ITEM%TYPE,
                                I_supplier        IN      SUP_INV_MGMT.SUPPLIER%TYPE,
                                I_dept            IN      SUP_INV_MGMT.DEPT%TYPE,
                                I_origin_country  IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                I_location        IN      ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(62)               := 'ITEM_SUPP_COUNTRY_SQL.GET_DFLT_RND_LVL_ITEM';
   L_im_lvl  SUPS.INV_MGMT_LVL%TYPE;
   L_dept    ITEM_MASTER.DEPT%TYPE      := I_dept;

   CURSOR C_get_im_lvl is
      select inv_mgmt_lvl
        from sups
       where supplier = I_supplier;

   CURSOR C_get_dept is
      select dept
        from item_master
       where item = I_item;

   CURSOR C_get_info_sys is
      select round_lvl, round_to_inner_pct, round_to_case_pct,
             round_to_layer_pct, round_to_pallet_pct
        from system_options;

   CURSOR C_get_info_sup is
      select round_lvl, round_to_inner_pct, round_to_case_pct,
             round_to_layer_pct, round_to_pallet_pct
        from sup_inv_mgmt
       where supplier = I_supplier
         and dept is NULL
         and location is NULL;

   CURSOR C_get_info_sup_dep is
      select round_lvl, round_to_inner_pct, round_to_case_pct,
             round_to_layer_pct, round_to_pallet_pct
        from sup_inv_mgmt
       where supplier = I_supplier
         and dept = L_dept
         and location is NULL;

   CURSOR C_get_info_sup_dep_loc is
      select sim.round_lvl, sim.round_to_inner_pct, sim.round_to_case_pct,
             sim.round_to_layer_pct, sim.round_to_pallet_pct
        from sup_inv_mgmt sim
       where (sim.location = I_location or
              sim.location = (select physical_wh
                                from wh
                               where wh = I_location))
         and sim.supplier = I_supplier
         and (   (L_im_lvl = 'A' and sim.dept = L_dept)
              or  L_im_lvl = 'L');

   CURSOR C_get_info_isc is
      select round_lvl, round_to_inner_pct, round_to_case_pct,
             round_to_layer_pct, round_to_pallet_pct
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country;


BEGIN
   O_round_level := 'ZIP';  -- flag value to determine whether records are found
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   /*Determine Supp. Inv. Mgmt. Level
     For Passed-in Supplier         */
   SQL_LIB.SET_MARK('OPEN','C_GET_IM_LVL','SUPS',' Supplier: '||to_char(I_supplier));   
   open C_get_im_lvl;
   SQL_LIB.SET_MARK('FETCH','C_GET_IM_LVL','SUPS',' Supplier: '||to_char(I_supplier)); 
   fetch C_get_im_lvl into L_im_lvl;
      ---
      if C_get_im_lvl%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUPP_SUPP',
                                               to_char(I_supplier),
                                               'Program: '||L_program,
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE','C_GET_IM_LVL','SUPS',' Supplier: '||to_char(I_supplier));                                                                                
         close C_get_im_lvl;
         return FALSE;
      end if;
      ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_IM_LVL','SUPS',' Supplier: '||to_char(I_supplier));            
   close C_get_im_lvl;
   ---
   /* Fetch Rounding Info.,
      Defaulting As Necessary */
   if I_location is NULL then
      if L_im_lvl in ('D','A') then
         if I_dept is NULL then
            /*Determine Department
              For Passed-in Item */
            SQL_LIB.SET_MARK('OPEN','C_GET_DEPT','DEPT',' Item: '||I_item);              
            open C_get_dept;
            SQL_LIB.SET_MARK('FETCH','C_GET_DEPT','DEPT',' Item: '||I_item);
            fetch C_get_dept into L_dept;
            ---
            if C_get_dept%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
                                                     I_item,
                                                     'Program: '||L_program,
                                                     NULL);
               SQL_LIB.SET_MARK('CLOSE','C_GET_DEPT','DEPT',' Item: '||I_item);                                                     
               close C_get_dept;
               return FALSE;
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE','C_GET_DEPT','DEPT',' Item: '||I_item);            
            close C_get_dept;
         end if;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_INFO_SUP_DEP','SUP_INV_MGMT',' Supplier: '||to_char(I_supplier)||' Dept: '||to_char(L_dept));
         open C_get_info_sup_dep;
         SQL_LIB.SET_MARK('FETCH','C_GET_INFO_SUP_DEP','SUP_INV_MGMT',' Supplier: '||to_char(I_supplier)||' Dept: '||to_char(L_dept));         
         fetch C_get_info_sup_dep into O_round_level, O_to_inner_pct, O_to_case_pct,
                                       O_to_layer_pct, O_to_pallet_pct;
         SQL_LIB.SET_MARK('CLOSE','C_GET_INFO_SUP_DEP','SUP_INV_MGMT',' Supplier: '||to_char(I_supplier)||' Dept: '||to_char(L_dept));                                           
         close C_get_info_sup_dep;
      end if;
      ---
      if O_round_level = 'ZIP' then -- either SIM Level is 'S'/'L' or Dept.-level info. wasn't found
         SQL_LIB.SET_MARK('OPEN','C_GET_INFO_SUP','SUP_INV_MGMT',' Supplier: '||to_char(I_supplier));      
         open C_get_info_sup;
         SQL_LIB.SET_MARK('FETCH','C_GET_INFO_SUP','SUP_INV_MGMT',' Supplier: '||to_char(I_supplier));         
         fetch C_get_info_sup into O_round_level, O_to_inner_pct, O_to_case_pct,
                                   O_to_layer_pct, O_to_pallet_pct;
         SQL_LIB.SET_MARK('CLOSE','C_GET_INFO_SUP','SUP_INV_MGMT',' Supplier: '||to_char(I_supplier));                                   
         close C_get_info_sup;
      end if;
      ---
      if O_round_level = 'ZIP' then -- Supplier-level info. wasn't found, so default from System Options
         SQL_LIB.SET_MARK('OPEN','C_GET_INFO_SYS','SYSTEM_OPTIONS',NULL);      
         open C_get_info_sys;
         SQL_LIB.SET_MARK('FETCH','C_GET_INFO_SYS','SYSTEM_OPTIONS',NULL);         
         fetch C_get_info_sys into O_round_level, O_to_inner_pct, O_to_case_pct,
                                   O_to_layer_pct, O_to_pallet_pct;
         SQL_LIB.SET_MARK('CLOSE','C_GET_INFO_SYS','SYSTEM_OPTIONS',NULL);                                   
         close C_get_info_sys;
      end if;
   else
      if L_im_lvl in ('L','A') then
         if I_dept is NULL and L_im_lvl = 'A' then
            /*Determine Department
              For Passed-in Item */
            SQL_LIB.SET_MARK('OPEN','C_GET_DEPT','DEPT',' Item: '||I_item);             
            open C_get_dept;
            SQL_LIB.SET_MARK('FETCH','C_GET_DEPT','DEPT',' Item: '||I_item);
            fetch C_get_dept into L_dept;
            ---
            if C_get_dept%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM',
                                                     I_item,
                                                     'Program: '||L_program,
                                                     NULL);
               SQL_LIB.SET_MARK('CLOSE','C_GET_DEPT','DEPT',' Item: '||I_item);                                                      
               close C_get_dept;
               return FALSE;
            end if;
            ---
            close C_get_dept;
         end if;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_INFO_SUP_DEP_LOC','SUP_INV_MGMT',' Loc: '||to_char(I_location)||' Dept: '||to_char(L_dept)||
                          ' Supplier: '||to_char(I_supplier));         
         open C_get_info_sup_dep_loc;
         SQL_LIB.SET_MARK('FETCH','C_GET_INFO_SUP_DEP_LOC','SUP_INV_MGMT',' Loc: '||to_char(I_location)||' Dept: '||to_char(L_dept)||
                          ' Supplier: '||to_char(I_supplier));           
         fetch C_get_info_sup_dep_loc into O_round_level, O_to_inner_pct, O_to_case_pct,
                                           O_to_layer_pct, O_to_pallet_pct;
         SQL_LIB.SET_MARK('CLOSE','C_GET_INFO_SUP_DEP_LOC','SUP_INV_MGMT',' Loc: '||to_char(I_location)||' Dept: '||to_char(L_dept)||
                        ' Supplier: '||to_char(I_supplier));                                                     
                                                  



         close C_get_info_sup_dep_loc;
      end if;
      ---
      if O_round_level = 'ZIP' then  -- either Loc. is a Store, or SIM Level is 'S'/'D'
         SQL_LIB.SET_MARK('OPEN','C_GET_INFO_ISC','ITEM_SUPP_COUNTRY',' Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||
                          ' Origin_Country: '||I_origin_country);      
         open C_get_info_isc;
         SQL_LIB.SET_MARK('FETCH','C_GET_INFO_ISC','ITEM_SUPP_COUNTRY',' Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||
                          ' Origin_Country: '||I_origin_country);         
         fetch C_get_info_isc into O_round_level, O_to_inner_pct, O_to_case_pct,
                                   O_to_layer_pct, O_to_pallet_pct;
         SQL_LIB.SET_MARK('CLOSE','C_GET_INFO_ISC','ITEM_SUPP_COUNTRY',' Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||
                          ' Origin_Country: '||I_origin_country);                                                             


         close C_get_info_isc;
      end if;
   end if;
   ---
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DFLT_RND_LVL_ITEM;
--------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COUNTRY_TO_COMP_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_pack_no           IN     ITEM_MASTER.ITEM%TYPE,
                                     I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                     I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                     I_pack_cost         IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                     I_ti                IN     ITEM_SUPP_COUNTRY.TI%TYPE,
                                     I_hi                IN     ITEM_SUPP_COUNTRY.HI%TYPE,
                                     I_uom               IN     ITEM_SUPP_COUNTRY.COST_UOM%TYPE)
   return BOOLEAN is

   L_item          ITEM_MASTER.ITEM%TYPE;
   L_qty           V_PACKSKU_QTY.QTY%TYPE;
   L_exists        BOOLEAN;
   L_prim_country  ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE;
   L_prim_supp     ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE;
   L_const_dim_ind ITEM_MASTER.CONST_DIMEN_IND%TYPE;

   L_default_uop   ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE;
   L_cost_uom      ITEM_SUPP_COUNTRY.COST_UOM%TYPE;

   L_negotiated_item_cost  ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE := 0;
   L_extended_base_cost    ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE := 0;
   L_inclusive_cost        ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE := 0;
   L_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
   L_base_cost             ITEM_SUPP_COUNTRY.BASE_COST%TYPE := 0;

   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_status                  ITEM_MASTER.STATUS%TYPE;
   L_desc                    ITEM_MASTER.ITEM_DESC%TYPE;
   L_item_loc_tbl            OBJ_ITEMLOC_TBL;
   L_sc_detail_rec           OBJ_SC_COST_EVENT_REC;
   L_sc_detail_tbl           OBJ_SC_COST_EVENT_TBL;
   L_tax_calc_rec            OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_tax_calc_tbl            OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_tax_rate                ITEM_COST_DETAIL.COMP_RATE%TYPE := 0;
 


   cursor C_ITEM is
      select vpq.item,
             vpq.qty,
             isp.primary_supp_ind,
             im.const_dimen_ind
        from v_packsku_qty vpq,
             item_supplier isp,
             item_master im
       where vpq.pack_no = I_pack_no
         and im.item = vpq.item
         and im.item = isp.item
         and isp.supplier = I_supplier;

   cursor C_GET_COSTS is
      select isc.negotiated_item_cost,
             isc.extended_base_cost,
             isc.inclusive_cost,
             isc.base_cost
        from v_packsku_qty vpq,
             item_supp_country isc,
             item_master im
       where vpq.pack_no = I_pack_no
         and im.item = vpq.item
         and im.item = isc.item 
         and isc.supplier = I_supplier
         and isc.origin_country_id = I_origin_country_id;

   cursor C_UOP_COSTUOM is
      select default_uop, cost_uom
        from item_supp_country
       where primary_supp_ind = 'Y'
         and primary_country_ind = 'Y'
         and item=L_item;
  


BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ITEM','V_PACKSKU_QTY,ITEM_SUPPLIER,ITEM_MASTER',' Pack_No: '||I_pack_no||' Supplier: '||to_char(I_supplier));
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH','C_ITEM','V_PACKSKU_QTY,ITEM_SUPPLIER,ITEM_MASTER',' Pack_No: '||I_pack_no||' Supplier: '||to_char(I_supplier));   
   fetch C_ITEM into L_item, L_qty, L_prim_supp, L_const_dim_ind;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM','V_PACKSKU_QTY,ITEM_SUPPLIER,ITEM_MASTER',' Pack_No: '||I_pack_no||' Supplier: '||to_char(I_supplier));   
   close C_ITEM;

   SQL_LIB.SET_MARK('OPEN','C_UOP_COSTUOM','ITEM_SUPP_COUNTRY',' Item: '||L_item);
   open C_UOP_COSTUOM;
   SQL_LIB.SET_MARK('FETCH','C_UOP_COSTUOM','ITEM_SUPP_COUNTRY',' Item: '||L_item);   
   fetch C_UOP_COSTUOM into L_default_uop,
                            L_cost_uom;
   SQL_LIB.SET_MARK('CLOSE','C_UOP_COSTUOM','ITEM_SUPP_COUNTRY',' Item: '||L_item);                                               
   close C_UOP_COSTUOM;

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;

   if L_system_options_rec.default_tax_type = 'GTAX'  then
      SQL_LIB.SET_MARK('OPEN','C_GET_COSTS','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY,ITEM_MASTER',' Pack_No: '||I_pack_no||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);   
      open C_GET_COSTS;
      SQL_LIB.SET_MARK('FETCH','C_GET_COSTS','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY,ITEM_MASTER',' Pack_No: '||I_pack_no||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);         
      fetch C_GET_COSTS into L_negotiated_item_cost, L_extended_base_cost, L_inclusive_cost, L_base_cost;
      SQL_LIB.SET_MARK('CLOSE','C_GET_COSTS','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY,ITEM_MASTER',' Pack_No: '||I_pack_no||
                       ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);            
      close C_GET_COSTS;
   end if;

   if L_item is NOT NULL then
      if NOT SUPP_ITEM_SQL.COUNTRY_EXISTS(O_error_message,
                                          L_exists,
                                          L_item,
                                          I_supplier) then
         return FALSE;
      end if;

      if L_exists then
         L_prim_country := 'N';
      else
         L_prim_country := 'Y';
      end if;

      if L_system_options_rec.default_tax_type in ('SALES','SVAT') then
         L_negotiated_item_cost := NULL; 
         L_extended_base_cost   := NULL; 
         L_inclusive_cost       := NULL; 
         L_base_cost            := NULL; 
      end if;

      BEGIN
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY',' Pack_No: '||I_pack_no||
                          ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);      
         insert into item_supp_country(item,
                                       supplier,
                                       origin_country_id,
                                       unit_cost,
                                       lead_time,
                                       pickup_lead_time,
                                       supp_pack_size,
                                       inner_pack_size,
                                       round_lvl,
                                       round_to_inner_pct,
                                       round_to_pallet_pct,
                                       round_to_layer_pct,
                                       round_to_case_pct,
                                       min_order_qty,
                                       max_order_qty,
                                       packing_method,
                                       primary_supp_ind,
                                       primary_country_ind,
                                       default_uop,
                                       ti,
                                       hi,
                                       supp_hier_type_1,
                                       supp_hier_lvl_1,
                                       supp_hier_type_2,
                                       supp_hier_lvl_2,
                                       supp_hier_type_3,
                                       supp_hier_lvl_3,
                                       create_datetime,
                                       last_update_datetime,
                                       last_update_id,
                                       cost_uom,
                                       negotiated_item_cost,
                                       extended_base_cost,
                                       inclusive_cost,
                                       base_cost,
                                       create_id)
                                select L_item,
                                       I_supplier,
                                       I_origin_country_id,
                                       I_pack_cost / L_qty,
                                       lead_time,
                                       pickup_lead_time,
                                       L_qty,
                                       1,
                                       round_lvl,
                                       round_to_inner_pct,
                                       round_to_pallet_pct,
                                       round_to_layer_pct,
                                       round_to_case_pct,
                                       min_order_qty,
                                       max_order_qty,
                                       packing_method,
                                       L_prim_supp,
                                       L_prim_country,
                                       L_default_uop,
                                       I_ti,
                                       I_hi,
                                       supp_hier_type_1,
                                       supp_hier_lvl_1,
                                       supp_hier_type_2,
                                       supp_hier_lvl_2,
                                       supp_hier_type_3,
                                       supp_hier_lvl_3,
                                       sysdate,
                                       sysdate,
                                       get_user,
                                       L_cost_uom,
                                       L_negotiated_item_cost / L_qty,
                                       L_extended_base_cost / L_qty,
                                       L_inclusive_cost / L_qty,
                                       L_base_cost / L_qty,
                                       get_user
                                  from item_supp_country
                                 where supplier = I_supplier
                                   and origin_country_id = I_origin_country_id
                                   and item = I_pack_no;

      EXCEPTION
         when DUP_VAL_ON_INDEX then
            return TRUE;
      END;

      if ITEM_SUPP_COUNTRY_LOC_SQL.CREATE_LOCATION(O_error_message,
                                                   L_item,
                                                   I_supplier,
                                                   I_origin_country_id,
                                                   NULL) = FALSE then
         return FALSE;
      end if;

      if L_prim_country = 'Y' then
         if L_prim_supp = 'Y' then
            if ITEM_SUPP_COUNTRY_SQL.FIRST_SUPPLIER(O_error_message,
                                                    L_item,
                                                    I_supplier,
                                                    I_origin_country_id,
                                                    I_pack_cost,
                                                    I_ti,
                                                    I_hi) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if L_const_dim_ind = 'Y' then
         if L_prim_supp = 'N' or
           (L_prim_supp = 'Y' and
            L_prim_country = 'N') then
            -- Insert default dimensions.
            if ITEM_SUPP_COUNTRY_SQL.INSERT_CONST_DIMENSIONS(O_error_message,
                                                             L_item,
                                                             I_supplier,
                                                             I_origin_country_id) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;

      if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                  L_desc,
                                  L_status,
                                  L_item) = FALSE then
            return FALSE;
      end if;

      if L_status = 'A' then
         if COST_EVENT_LOG_SQL.GET_ITEM_LOCS(O_error_message,
                                             L_item_loc_tbl,
                                             L_item) = FALSE then
            return FALSE;
         end if;

         if L_item_loc_tbl.count > 0 then
            L_sc_detail_tbl := new OBJ_SC_COST_EVENT_TBL();
            FOR i in L_item_loc_tbl.first..L_item_loc_tbl.last LOOP
               L_sc_detail_rec := new OBJ_SC_COST_EVENT_REC(L_item,
                                                            L_item_loc_tbl(i).loc,
                                                            I_supplier,
                                                            I_origin_country_id);
               L_sc_detail_tbl.extend;
               L_sc_detail_tbl(L_sc_detail_tbl.count) := L_sc_detail_rec;
            END LOOP;

            if FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY(O_error_message,
                                                      L_cost_event_process_id,
                                                      'ADD',
                                                      L_sc_detail_tbl,
                                                      GET_USER) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;   
   end if;
      

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_SUPP_COUNTRY_SQL.INSERT_COUNTRY_TO_COMP_ITEM',
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_COUNTRY_TO_COMP_ITEM;
--------------------------------------------------------------------------------
FUNCTION INSERT_EXP_TO_COMP_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_pack_no           IN     ITEM_MASTER.ITEM%TYPE,
                                 I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                 I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN is

   L_item          ITEM_MASTER.ITEM%TYPE;
   L_prim_country  ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE;

   cursor C_ITEM is
      select vpq.item, isc.primary_country_ind
        from v_packsku_qty vpq, item_supp_country isc
       where vpq.pack_no = I_pack_no
         and vpq.item = isc.item
         and isc.supplier = I_supplier
         and isc.origin_country_id = I_origin_country_id;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ITEM','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY',' Pack_No: '||I_pack_no||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH','C_ITEM','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY',' Pack_No: '||I_pack_no||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);   
   fetch C_ITEM into L_item, L_prim_country;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY',' Pack_No: '||I_pack_no||
                    ' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_origin_country_id);      
   close C_ITEM;

   if L_item is NOT NULL then
      if L_prim_country = 'Y' then
         if ITEM_EXPENSE_SQL.DEFAULT_EXPENSES(O_error_message,
                                              L_item,
                                              I_supplier,
                                              NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_EXPENSE_SQL.DEFAULT_GROUP_EXP(O_error_message,
                                               L_item,
                                               I_supplier,
                                               NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_EXPENSE_SQL.DEFAULT_EXPENSES(O_error_message,
                                              L_item,
                                              I_supplier,
                                              I_origin_country_id) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_EXPENSE_SQL.DEFAULT_GROUP_EXP(O_error_message,
                                               L_item,
                                               I_supplier,
                                               I_origin_country_id) = FALSE then
            return FALSE;
         end if;
         ---
      else
         if ITEM_EXPENSE_SQL.DEFAULT_EXPENSES(O_error_message,
                                              L_item,
                                              I_supplier,
                                              I_origin_country_id) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_EXPENSE_SQL.DEFAULT_GROUP_EXP(O_error_message,
                                               L_item,
                                               I_supplier,
                                               I_origin_country_id) = FALSE then
            return FALSE;
         end if;
      end if; -- Primary country
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_SUPP_COUNTRY_SQL.INSERT_EXP_TO_COMP_ITEM',
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_EXP_TO_COMP_ITEM;
--------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_UOP(O_error_message     IN OUT VARCHAR2,
                         O_default_uop       IN OUT ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN is

   cursor C_GET_DEFAULT_UOP is
      select default_uop
        from item_supp_country
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_DEFAULT_UOP', 'ITEM_SUPP_COUNTRY', 'item = '||I_item||
                                                                      ', supplier = '||to_char(I_supplier)||
                                                                      ', origin_country_id = '||I_origin_country_id);
   open C_GET_DEFAULT_UOP;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_DEFAULT_UOP', 'ITEM_SUPP_COUNTRY', 'item = '||I_item||
                                                                      ', supplier = '||to_char(I_supplier)||
                                                                      ', origin_country_id = '||I_origin_country_id);
   fetch C_GET_DEFAULT_UOP into O_default_uop;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_DEFAULT_UOP', 'ITEM_SUPP_COUNTRY', 'item = '||I_item||
                                                                      ', supplier = '||to_char(I_supplier)||
                                                                      ', origin_country_id = '||I_origin_country_id);
   close C_GET_DEFAULT_UOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_SUPP_COUNTRY_SQL.GET_DEFAULT_UOP',
                                            to_char(SQLCODE));

END GET_DEFAULT_UOP;
---------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPP_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_isc_rec       IN ITEM_SUPP_COUNTRY%ROWTYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.INSERT_ITEM_SUPP_COUNTRY';
   L_pack_method   ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE   := NULL;

   cursor C_PACK_METHOD is
      select default_packing_method
        from system_options;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_PACK_METHOD', 'SYSTEM_OPTIONS',NULL);
   open C_PACK_METHOD;
   SQL_LIB.SET_MARK('FETCH', 'C_PACK_METHOD', 'SYSTEM_OPTIONS',NULL);   
   fetch C_PACK_METHOD into L_pack_method;
   SQL_LIB.SET_MARK('CLOSE', 'C_PACK_METHOD', 'SYSTEM_OPTIONS',NULL);   
   close C_PACK_METHOD;

   SQL_LIB.SET_MARK('INSERT', NULL, 'ITEM_SUPP_COUNTRY','item: '||I_isc_rec.item||
                                                        ' supplier: '||I_isc_rec.supplier||
                                                        ' country: '||I_isc_rec.origin_country_id);
   insert into ITEM_SUPP_COUNTRY(item,
                                 supplier,
                                 origin_country_id,
                                 unit_cost,
                                 lead_time,
                                 pickup_lead_time,
                                 supp_pack_size,
                                 inner_pack_size,
                                 round_lvl,
                                 round_to_inner_pct,
                                 round_to_case_pct,
                                 round_to_layer_pct,
                                 round_to_pallet_pct,
                                 min_order_qty,
                                 max_order_qty,
                                 packing_method,
                                 primary_supp_ind,
                                 primary_country_ind,
                                 default_uop,
                                 ti,
                                 hi,
                                 supp_hier_type_1,
                                 supp_hier_lvl_1,
                                 supp_hier_type_2,
                                 supp_hier_lvl_2,
                                 supp_hier_type_3,
                                 supp_hier_lvl_3,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id,
                                 cost_uom,
                                 tolerance_type,
                                 max_tolerance,
                                 min_tolerance,
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 inclusive_cost,
                                 base_cost,
                                 create_id)
                         values( I_isc_rec.item,
                                 I_isc_rec.supplier,
                                 I_isc_rec.origin_country_id,
                                 I_isc_rec.unit_cost,
                                 I_isc_rec.lead_time,
                                 I_isc_rec.pickup_lead_time,
                                 I_isc_rec.supp_pack_size,
                                 I_isc_rec.inner_pack_size,
                                 I_isc_rec.round_lvl,
                                 I_isc_rec.round_to_inner_pct,
                                 I_isc_rec.round_to_case_pct,
                                 I_isc_rec.round_to_layer_pct,
                                 I_isc_rec.round_to_pallet_pct,
                                 I_isc_rec.min_order_qty,
                                 I_isc_rec.max_order_qty,
                                 NVL(I_isc_rec.packing_method,L_pack_method),
                                 I_isc_rec.primary_supp_ind,
                                 I_isc_rec.primary_country_ind,
                                 I_isc_rec.default_uop,
                                 I_isc_rec.ti,
                                 I_isc_rec.hi,
                                 DECODE(I_isc_rec.supp_hier_type_1,NULL,DECODE(I_isc_rec.supp_hier_lvl_1,NULL,NULL,'S1'),I_isc_rec.supp_hier_type_1),
                                 I_isc_rec.supp_hier_lvl_1,
                                 DECODE(I_isc_rec.supp_hier_type_2,NULL,DECODE(I_isc_rec.supp_hier_lvl_2,NULL,NULL,'S2'),I_isc_rec.supp_hier_type_2),
                                 I_isc_rec.supp_hier_lvl_2,
                                 DECODE(I_isc_rec.supp_hier_type_3,NULL,DECODE(I_isc_rec.supp_hier_lvl_3,NULL,NULL,'S3'),I_isc_rec.supp_hier_type_3),                                 
                                 I_isc_rec.supp_hier_lvl_3,
                                 I_isc_rec.create_datetime,
                                 sysdate,
                                 I_isc_rec.last_update_id,
                                 I_isc_rec.cost_uom,
                                 I_isc_rec.tolerance_type,
                                 I_isc_rec.max_tolerance,
                                 I_isc_rec.min_tolerance,
                                 I_isc_rec.negotiated_item_cost,
                                 I_isc_rec.extended_base_cost,
                                 I_isc_rec.inclusive_cost,
                                 I_isc_rec.base_cost,
                                 I_isc_rec.create_id);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ITEM_SUPP_COUNTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_YES_PRIM_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_isc_rec       IN ITEM_SUPP_COUNTRY%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.UPDATE_YES_PRIM_IND';

--- NOTE:  only used for XITEM subscription API after UPDATE_PRIMARY_INDICATORS is called
---
BEGIN
   if not LOCK_ITEM_SUPP_COUNTRY(O_error_message,
                                 I_isc_rec.item,
                                 I_isc_rec.supplier,
                                 I_isc_rec.origin_country_id) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_SUPP_COUNTRY','item: '||I_isc_rec.item||
                                                    ' supplier: '||I_isc_rec.supplier||
                                                    ' country: '||I_isc_rec.origin_country_id);

   update ITEM_SUPP_COUNTRY
      set  primary_country_ind = 'Y',
           last_update_datetime = sysdate,
           last_update_id = I_isc_rec.last_update_id
    where item = I_isc_rec.item
      and supplier = I_isc_rec.supplier
      and origin_country_id = I_isc_rec.origin_country_id;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC',
                                            'Program: '||L_program,
                                            'Table : ITEM_SUPP_COUNTRY',
                                            'Item: '||I_isc_rec.item||' Supplier: '||to_char(I_isc_rec.supplier)||' Origin_Country: '||I_isc_rec.origin_country_id);
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_YES_PRIM_IND;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_SUPP_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item          IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                  I_supplier      IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                  I_country       IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.DELETE_ITEM_SUPP_COUNTRY';

   CURSOR C_LOCK_ITEM_SUPP_COUNTRY_CE IS
      select 'x'
        from item_supp_country_cfa_ext
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_country
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_SUPP_COUNTRY_CE', 'ITEM_SUPP_COUNTRY_CFA_EXT','item: '||I_item||
                                                    ' supplier: '||I_supplier||
                                                    ' country: '||I_country);
   open C_LOCK_ITEM_SUPP_COUNTRY_CE;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_SUPP_COUNTRY_CE', 'ITEM_SUPP_COUNTRY_CFA_EXT','item: '||I_item||
                                                    ' supplier: '||I_supplier||
                                                    ' country: '||I_country);
   close C_LOCK_ITEM_SUPP_COUNTRY_CE;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPP_COUNTRY_CFA_EXT','item: '||I_item||
                                                             ' supplier: '||I_supplier||
                                                             ' country: '||I_country);
   delete from item_supp_country_cfa_ext
    where item = I_item
      and supplier = I_supplier
      and origin_country_id = I_country;
   ---
   if not LOCK_ITEM_SUPP_COUNTRY(O_error_message,
                                 I_item,
                                 I_supplier,
                                 I_country) then
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPP_COUNTRY','item: '||I_item||
                                                    ' supplier: '||I_supplier||
                                                    ' country: '||I_country);
   delete from ITEM_SUPP_COUNTRY
    where item = I_item
      and supplier = I_supplier
      and origin_country_id = I_country;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS',
                                            'Program: '||L_program,
                                            'Item : '||I_item,
                                            'Supplier : '||I_supplier||' Origin_Country: '||I_country);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ITEM_SUPP_COUNTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_SUPP_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item          IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                I_supplier      IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                I_country       IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.LOCK_ITEM_SUPP_COUNTRY';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_ITEM_SUPP_COUNTRY IS
      select 'x'
        from ITEM_SUPP_COUNTRY
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_country
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_SUPP_COUNTRY', 'ITEM_SUPP_COUNTRY','item: '||I_item||
                                                    ' supplier: '||I_supplier||
                                                    ' country: '||I_country);
   open C_LOCK_ITEM_SUPP_COUNTRY;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_SUPP_COUNTRY', 'ITEM_SUPP_COUNTRY','item: '||I_item||
                                                    ' supplier: '||I_supplier||
                                                    ' country: '||I_country);
   close C_LOCK_ITEM_SUPP_COUNTRY;
   ---
   return TRUE;
EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'Item: '||I_item,
                                            'Supplier: '||I_supplier);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_ITEM_SUPP_COUNTRY;
---------------------------------------------------------------------------------------------
FUNCTION CONVERT_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_unit_cost          IN OUT   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                      I_item                IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                      I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                      I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                      I_cost_type           IN       VARCHAR2,
                      I_from_uom            IN        UOM_CONVERSION.FROM_UOM%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.CONVERT_COST';

   L_isc_row          ITEM_SUPP_COUNTRY%ROWTYPE;
   L_exists_isc       BOOLEAN  := FALSE;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;

   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;

   if I_cost_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_type',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;

   --retrieve the row from ITEM_SUPP_COUNTRY and ITEM_SUPP_COUNTRY_DIM tables
   if ITEM_SUPP_COUNTRY_SQL.GET_ROW(O_error_message,
                                    L_exists_isc,
                                    L_isc_row,
                                    I_item,
                                    I_supplier,
                                    I_origin_country_id) = FALSE then
      return FALSE;
   end if;

   if L_exists_isc then
      if ITEM_SUPP_COUNTRY_SQL.CONVERT_COST(O_error_message,
                                            IO_unit_cost,
                                            I_item,
                                            I_supplier,
                                            I_origin_country_id,
                                            I_cost_type,
                                            I_from_uom,
                                            L_isc_row.cost_uom,
                                            L_isc_row.supp_pack_size) = FALSE then
         return FALSE;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC',
                                            'Program: '||L_program,
                                            'Item: '||I_item,
                                            'Supplier: '||I_supplier);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END CONVERT_COST;
----------------------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists              IN OUT   BOOLEAN,
                 O_item_supp_country   IN OUT   ITEM_SUPP_COUNTRY%ROWTYPE,
                 I_item                IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                 I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                 I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.GET_ROW';

   cursor C_GET_ROW is
   select *
     from item_supp_country
    where item              = I_item
      and supplier          = I_supplier
      and origin_country_id = I_origin_country_id;

BEGIN

   O_item_supp_country := NULL;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY',
                    'Item: '||I_item || ' Supplier:'||to_char(I_supplier)||' Origin_country_id: '||I_origin_country_id);
   open C_GET_ROW;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY',
                    'Item: '||I_item || ' Supplier:'||to_char(I_supplier)||' Origin_country_id: '||I_origin_country_id);
   fetch C_GET_ROW into O_item_supp_country;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY',
                    'Item: '||I_item || ' Supplier:'||to_char(I_supplier)||' Origin_country_id: '||I_origin_country_id);
   close C_GET_ROW;

   O_exists := (O_item_supp_country.item is NOT NULL);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ROW;
----------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_LOC_ROW(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists                  IN OUT   BOOLEAN,
                             O_item_supp_country_dim   IN OUT   ITEM_SUPP_COUNTRY_DIM%ROWTYPE,
                             I_item                    IN       ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                             I_loc                     IN       ITEM_LOC.LOC%TYPE,
                             I_dim_object              IN       ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_LOC_ROW';

   cursor C_GET_ROW is
   select iscd.*
     from item_supp_country_dim iscd,
          item_loc il
    where iscd.item           = I_item
      and iscd.dim_object     = I_dim_object
      and il.loc              = I_loc
      and iscd.item           = il.item
      and iscd.supplier       = il.primary_supp
      and iscd.origin_country = il.primary_cntry;

BEGIN

   O_item_supp_country_dim := NULL;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_dim_object is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dim_object',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item: '||I_item||' Dim_object: '||I_dim_object||'Loc: '||to_char(I_loc));
   open C_GET_ROW;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item: '||I_item||' Dim_object: '||I_dim_object||'Loc: '||to_char(I_loc));

   fetch C_GET_ROW into O_item_supp_country_dim;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item: '||I_item||' Dim_object: '||I_dim_object||'Loc: '||to_char(I_loc));

   close C_GET_ROW;

   O_exists := (O_item_supp_country_dim.item is NOT NULL);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_PRIMARY_LOC_ROW;
---------------------------------------------------------------------
FUNCTION CONVERT_COST(I_unit_cost           IN   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                      I_item                IN   ITEM_SUPP_COUNTRY.ITEM%TYPE,
                      I_supplier            IN   ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                      I_origin_country_id   IN   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                      I_cost_type           IN   VARCHAR2)
   RETURN NUMBER IS

   L_error_message    RTK_ERRORS.RTK_TEXT%TYPE;
   L_converted_cost   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := I_unit_cost;
   I_from_uom         UOM_CONVERSION.FROM_UOM%TYPE := NULL;

BEGIN

   if ITEM_SUPP_COUNTRY_SQL.CONVERT_COST(L_error_message,
                                         L_converted_cost,
                                         I_item,
                                         I_supplier,
                                         I_origin_country_id,
                                         I_cost_type,
                                         I_from_uom
                                         ) = FALSE then
      raise_application_error(-20000, L_error_message);
   end if;

   return L_converted_cost;


EXCEPTION
   when OTHERS then
      raise;

END CONVERT_COST;
---------------------------------------------------------------------------------------------
FUNCTION GET_COST_UOM(O_error_message IN OUT VARCHAR2,
                      O_cuom          IN OUT ITEM_SUPP_COUNTRY.COST_UOM%TYPE,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.GET_COST_UOM';

   cursor C_COST_UOM is
      select cost_uom
        from item_supp_country
       where primary_supp_ind = 'Y'
         and primary_country_ind = 'Y'
         and item = I_item;
BEGIN
   ---
   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_COST_UOM','ITEM_SUPP_COUNTRY','Item: '||I_item);      
   open C_COST_UOM;
   SQL_LIB.SET_MARK('FETCH','C_COST_UOM','ITEM_SUPP_COUNTRY','Item: '||I_item);      
   fetch C_COST_UOM into O_cuom;
   SQL_LIB.SET_MARK('CLOSE','C_COST_UOM','ITEM_SUPP_COUNTRY','Item: '||I_item);      
   close C_COST_UOM;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_COST_UOM;
---------------------------------------------------------------------------------------------
FUNCTION CONVERT_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_unit_cost          IN OUT   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                      I_item                IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                      I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                      I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                      I_cost_type           IN       VARCHAR2,
                      I_from_uom            IN       UOM_CONVERSION.FROM_UOM%TYPE DEFAULT NULL,
                      I_cost_uom            IN       ITEM_SUPP_COUNTRY.COST_UOM%TYPE,
                      I_supp_pack_size      IN       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'ITEM_SUPP_COUNTRY_SQL.CONVERT_COST';

   L_dimension_uom     ITEM_SUPP_COUNTRY.COST_UOM%TYPE;
   L_std_in_cost_uom   NUMBER;
   L_std_dimensions    NUMBER;
   L_dim_object        ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE := 'CA';
   L_std_uom           ITEM_MASTER.STANDARD_UOM%TYPE;
   L_cost_uom_class    UOM_CLASS.UOM_CLASS%TYPE;
   L_std_uom_class     UOM_CLASS.UOM_CLASS%TYPE;
   L_isc_dim_row       ITEM_SUPP_COUNTRY_DIM%ROWTYPE;
   L_exists_iscd       BOOLEAN;
   L_convert           BOOLEAN;

   cursor C_GET_STD_UOM is
      select standard_uom
        from item_master
       where item = I_item;

   cursor C_GET_UOM_CLASS is
      select uom_class
        from uom_class
       where uom = I_cost_uom;

   cursor C_GET_STD_UOM_CLASS is
       select uom_class
         from uom_class
        where uom = L_std_uom;

BEGIN

   --validate required input parameters
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;

   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;

   if I_cost_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_type',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;

   if I_cost_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_uom',
                                            L_program,
                                            NULL);
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STD_UOM',
                    'ITEM_MASTER',
                    'Item: '||I_item);
   open C_GET_STD_UOM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STD_UOM',
                    'ITEM_MASTER',
                    'Item: '||I_item);
   fetch C_GET_STD_UOM into L_std_uom;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STD_UOM',
                    'ITEM_MASTER',
                    'Item: '||I_item);
   close C_GET_STD_UOM;

   --if standard UOM is equal to the
   --cost UOM, no need to convert

   if L_std_uom = I_cost_uom then
      return TRUE;
   end if;

   --retrieve the standard UOM's class
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_UOM_CLASS',
                    'UOM_CLASS',
                    'UOM: '||I_cost_uom);
   open C_GET_UOM_CLASS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_UOM_CLASS',
                    'UOM_CLASS',
                    'UOM: '||I_cost_uom);
   fetch C_GET_UOM_CLASS into L_cost_uom_class;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_UOM_CLASS',
                    'UOM_CLASS',
                    'UOM: '||I_cost_uom );
   close C_GET_UOM_CLASS;

   --retrieve the cost UOM's class
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STD_UOM_CLASS',
                    'UOM_CLASS',
                    'uom: '||L_std_uom);
   open C_GET_STD_UOM_CLASS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STD_UOM_CLASS',
                    'UOM_CLASS',
                    'uom: '||L_std_uom);
   fetch C_GET_STD_UOM_CLASS into L_std_uom_class;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STD_UOM_CLASS',
                    'UOM_CLASS',
                    'uom: '||L_std_uom);
   close C_GET_STD_UOM_CLASS;

   --if the standard and cost UOM belong to
   --the same class, utilize UOM_SQL.WITHIN_CLASS
   --to retrieve the conversion factor

   if L_cost_uom_class = L_std_uom_class then

      --call UOM_SQL.WITHIN_CLASS
      If UOM_SQL.WITHIN_CLASS(O_error_message,
                              L_std_in_cost_uom,
                              I_cost_uom,
                              1,
                              L_std_uom,
                              L_cost_uom_class ) = FALSE then --since both costs belong to the same class, both the standard and cost's class is valid
         return FALSE;
      end if;

   else --use the dimension values in retrieving the conversion factor


      if I_supp_pack_size is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_supp_pack_size',
                                               L_program,
                                               NULL);
      end if;

      --retrieve the dimension values of the item-supplier

      if ITEM_SUPP_COUNTRY_DIM_SQL.GET_ROW(O_error_message,
                                           L_exists_iscd,
                                           L_isc_dim_row,
                                           I_item,
                                           I_supplier,
                                           I_origin_country_id,
                                           L_dim_object) = FALSE then
         return FALSE;
      end if;

      --verify if dimension values already exist for the item-supplier
      if L_exists_iscd = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('UOM_CLASS_DIFF_MISS_DIM',
                                                'Program: '||L_program,
                                                'Item: '||I_item,
                                                'Supplier: '||I_supplier||' Origin_Country: '||I_origin_country_id);
         return FALSE;
      else

         if UOM_SQL.VALIDATE_CONVERSION(O_error_message,
                                        L_convert,
                                        L_cost_uom_class,
                                        L_cost_uom_class,
                                        L_isc_dim_row.lwh_uom,
                                        L_isc_dim_row.weight_uom,
                                        L_isc_dim_row.liquid_volume_uom) = FALSE or
            L_convert = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('UOM_CLASS_DIFF_MISS_DIM',
                                                  'Program: '||L_program,
                                                  'Cost_Uom : '||L_cost_uom_class,
                                                  NULL);
            return FALSE;
         else

            --calculate standard UOM dimensions
            --this portion converts the standard UOM to
            --the pack size and then to the dimension UOM

            if L_cost_uom_class = 'MASS' then
               L_dimension_uom  := L_isc_dim_row.weight_uom;
               L_std_dimensions := L_isc_dim_row.net_weight/I_supp_pack_size;
            elsif L_cost_uom_class = 'LVOL' then
               L_dimension_uom  := L_isc_dim_row.liquid_volume_uom;
               L_std_dimensions := L_isc_dim_row.liquid_volume/I_supp_pack_size;
            elsif L_cost_uom_class = 'DIMEN' then
               L_dimension_uom  := L_isc_dim_row.lwh_uom;
               L_std_dimensions := L_isc_dim_row.length/I_supp_pack_size;
            elsif L_cost_uom_class = 'VOL' then
               L_dimension_uom  := L_isc_dim_row.lwh_uom || '3';
               L_std_dimensions := (L_isc_dim_row.length * L_isc_dim_row.width * L_isc_dim_row.height)/I_supp_pack_size;
            end if;

            --if the cost UOM is different from the
            --dimensin UOM, convert from the dimension
            --UOM to the cost UOM to get the
            --conversion factor

            if I_cost_uom != L_dimension_uom then

               If UOM_SQL.WITHIN_CLASS(O_error_message,
                                       L_std_in_cost_uom,
                                       I_cost_uom,
                                       L_std_dimensions,
                                       L_dimension_uom,
                                       L_cost_uom_class) = FALSE then --since both cost belong to the same class, both the standard and cost's class is valid
                  return FALSE;
               end if;

            else

            --otherwise, use the calculated dimension
            --as the conversion factor
               L_std_in_cost_uom := L_std_dimensions;
            end if;

         end if;

      end if;

   end if;

   --now that the conversion factor has
   --been retrieved, do the actual conversion here
   --based on the cost type

   if I_cost_type = 'S' then
      IO_unit_cost := IO_unit_cost / L_std_in_cost_uom;
   else
      IO_unit_cost := IO_unit_cost * L_std_in_cost_uom;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CONVERT_COST;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ISC_DIM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_iscd_rec        IN       ITEM_SUPP_COUNTRY_DIM%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.INSERT_ISC_DIM';

BEGIN

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'ITEM_SUPP_COUNTRY_DIM',
                    'item: '       || I_iscd_rec.item           ||
                    ' supplier: '  || to_char(I_iscd_rec.supplier)||
                    ' country: '   || I_iscd_rec.origin_country ||
                    ' dim_object: '|| I_iscd_rec.dim_object);

   insert into ITEM_SUPP_COUNTRY_DIM(item,
                                     supplier,
                                     origin_country,
                                     dim_object,
                                     presentation_method,
                                     length,
                                     width,
                                     height,
                                     lwh_uom,
                                     weight,
                                     net_weight,
                                     weight_uom,
                                     liquid_volume,
                                     liquid_volume_uom,
                                     stat_cube,
                                     tare_weight,
                                     tare_type,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     create_id)
                              values(I_iscd_rec.item,
                                     I_iscd_rec.supplier,
                                     I_iscd_rec.origin_country,
                                     I_iscd_rec.dim_object,
                                     I_iscd_rec.presentation_method,
                                     I_iscd_rec.length,
                                     I_iscd_rec.width,
                                     I_iscd_rec.height,
                                     I_iscd_rec.lwh_uom,
                                     I_iscd_rec.weight,
                                     I_iscd_rec.net_weight,
                                     I_iscd_rec.weight_uom,
                                     I_iscd_rec.liquid_volume,
                                     I_iscd_rec.liquid_volume_uom,
                                     I_iscd_rec.stat_cube,
                                     I_iscd_rec.tare_weight,
                                     I_iscd_rec.tare_type,
                                     sysdate,
                                     I_iscd_rec.create_datetime,
                                     I_iscd_rec.last_update_id,
                                     I_iscd_rec.create_id);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ISC_DIM;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ISC_DIM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_iscd_rec        IN       ITEM_SUPP_COUNTRY_DIM%ROWTYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.UPDATE_ISC_DIM';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   L_rowid           ROWID := NULL;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_DIM is
      select rowid
        from item_supp_country_dim
       where item           = I_iscd_rec.item
         and supplier       = I_iscd_rec.supplier
         and origin_country = I_iscd_rec.origin_country
         and dim_object     = I_iscd_rec.dim_object
         for update nowait;
BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_SUPP_COUNTRY_DIM',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item : '||I_iscd_rec.item||' Supplier: '||to_char(I_iscd_rec.supplier)||
                    ' Origin_Country: '||I_iscd_rec.origin_country||' Dim_Object: '||I_iscd_rec.dim_object);
   open C_LOCK_ITEM_SUPP_COUNTRY_DIM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCK_ITEM_SUPP_COUNTRY_DIM',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item : '||I_iscd_rec.item||' Supplier: '||to_char(I_iscd_rec.supplier)||
                    ' Origin_Country: '||I_iscd_rec.origin_country||' Dim_Object: '||I_iscd_rec.dim_object);
   fetch C_LOCK_ITEM_SUPP_COUNTRY_DIM into L_rowid;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_SUPP_COUNTRY_DIM',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item : '||I_iscd_rec.item||' Supplier: '||to_char(I_iscd_rec.supplier)||
                    ' Origin_Country: '||I_iscd_rec.origin_country||' Dim_Object: '||I_iscd_rec.dim_object);
   close C_LOCK_ITEM_SUPP_COUNTRY_DIM;

   if L_rowid is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC',
                                            'Program: '||L_program,
                                            'Table: ITEM_SUPP_COUNTRY_DIM',
                                            'Item: '||I_iscd_rec.item||' Supplier: '||to_char(I_iscd_rec.supplier)||' Origin_Country: '||I_iscd_rec.origin_country);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ITEM_SUPP_COUNTRY',
                    'item: '     ||I_iscd_rec.item    ||
                    ' supplier: '||to_char(I_iscd_rec.supplier) ||
                    ' country: ' ||I_iscd_rec.origin_country);

   update item_supp_country_dim
      set dim_object           = I_iscd_rec.dim_object,
          tare_weight          = I_iscd_rec.tare_weight,
          tare_type            = I_iscd_rec.tare_type,
          lwh_uom              = I_iscd_rec.lwh_uom,
          length               = I_iscd_rec.length,
          width                = I_iscd_rec.width,
          height               = I_iscd_rec.height,
          liquid_volume        = I_iscd_rec.liquid_volume,
          liquid_volume_uom    = I_iscd_rec.liquid_volume_uom,
          stat_cube            = I_iscd_rec.stat_cube,
          weight_uom           = I_iscd_rec.weight_uom,
          weight               = I_iscd_rec.weight,
          net_weight           = I_iscd_rec.net_weight,
          presentation_method  = I_iscd_rec.presentation_method,
          last_update_datetime = sysdate,
          last_update_id       = I_iscd_rec.last_update_id
    where rowid = L_rowid;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ISC_DIM;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ISC_DIM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                        I_supplier        IN       ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                        I_country         IN       ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                        I_dim_object      IN       ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.DELETE_ISC_DIM';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_rowid        ROWID := NULL;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_DIM is
      select rowid
        from item_supp_country_dim
       where item           = I_item
         and supplier       = I_supplier
         and origin_country = I_country
         and dim_object     = I_dim_object
         for update nowait;
BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_SUPP_COUNTRY_DIM',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item : '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Origin_Country: '||I_country||' Dim_Object: '||I_dim_object);
   open C_LOCK_ITEM_SUPP_COUNTRY_DIM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCK_ITEM_SUPP_COUNTRY_DIM',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item : '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Origin_Country: '||I_country||' Dim_Object: '||I_dim_object);
   fetch C_LOCK_ITEM_SUPP_COUNTRY_DIM into L_rowid;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_SUPP_COUNTRY_DIM',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item : '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Origin_Country: '||I_country||' Dim_Object: '||I_dim_object);
   close C_LOCK_ITEM_SUPP_COUNTRY_DIM;

   if L_rowid is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_DELETE_REC',
                                            'Program: '||L_program,
                                            'Table: ITEM_SUPP_COUNTRY_DIM',
                                            'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Origin_Country: '||I_country);      
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ITEM_SUPP_COUNTRY',
                    'item: '     ||I_item    ||
                    ' supplier: '||to_char(I_supplier) ||
                    ' country: ' ||I_country);

   delete from item_supp_country_dim
    where rowid = L_rowid;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ISC_DIM;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_isc_rec         IN       ITEM_SUPP_COUNTRY%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_SQL.UPDATE_ISC';
--- NOTE:  only used for XITEM subscription API

BEGIN
   if I_isc_rec.item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_isc_rec.item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_isc_rec.supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_isc_rec.supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_isc_rec.supp_pack_size is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_isc_rec.supp_pack_size',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_isc_rec.inner_pack_size is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_isc_rec.inner_pack_size',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_isc_rec.ti is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_isc_rec.ti',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_isc_rec.hi is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_isc_rec.hi',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   if NOT LOCK_ITEM_SUPP_COUNTRY(O_error_message,
                                 I_isc_rec.item,
                                 I_isc_rec.supplier,
                                 I_isc_rec.origin_country_id) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_SUPP_COUNTRY','item: '||I_isc_rec.item||
                                                        ' supplier: '||to_char(I_isc_rec.supplier)||
                                                        ' country: '||I_isc_rec.origin_country_id);

   update item_supp_country isc
      set isc.lead_time = I_isc_rec.lead_time,
          isc.pickup_lead_time = I_isc_rec.pickup_lead_time,
          isc.supp_pack_size = I_isc_rec.supp_pack_size,
          isc.inner_pack_size = I_isc_rec.inner_pack_size,
          isc.min_order_qty = I_isc_rec.min_order_qty,
          isc.max_order_qty = I_isc_rec.max_order_qty,
          isc.default_uop = I_isc_rec.default_uop,
          isc.ti = I_isc_rec.ti,
          isc.hi = I_isc_rec.hi,
          isc.supp_hier_type_1 = DECODE(isc.supp_hier_type_1,NULL,DECODE(I_isc_rec.supp_hier_lvl_1,NULL,NULL,'S1'),isc.supp_hier_type_1),
          isc.supp_hier_type_2 = DECODE(isc.supp_hier_type_2,NULL,DECODE(I_isc_rec.supp_hier_lvl_2,NULL,NULL,'S2'),isc.supp_hier_type_2),
          isc.supp_hier_type_3 = DECODE(isc.supp_hier_type_3,NULL,DECODE(I_isc_rec.supp_hier_lvl_3,NULL,NULL,'S3'),isc.supp_hier_type_3),     
          isc.last_update_datetime = sysdate,
          isc.last_update_id = get_user,
          isc.round_lvl = nvl(I_isc_rec.round_lvl,round_lvl),
          isc.round_to_inner_pct = nvl(I_isc_rec.round_to_inner_pct,round_to_inner_pct),
          isc.round_to_case_pct = nvl(I_isc_rec.round_to_case_pct,round_to_case_pct),
          isc.round_to_layer_pct = nvl(I_isc_rec.round_to_layer_pct,round_to_layer_pct),
          isc.round_to_pallet_pct = nvl(I_isc_rec.round_to_pallet_pct,round_to_pallet_pct),
          isc.packing_method = nvl(I_isc_rec.packing_method,packing_method),
          isc.supp_hier_lvl_1 = I_isc_rec.supp_hier_lvl_1,
          isc.supp_hier_lvl_2 = I_isc_rec.supp_hier_lvl_2,
          isc.supp_hier_lvl_3 = I_isc_rec.supp_hier_lvl_3
    where isc.item = I_isc_rec.item
      and isc.supplier = I_isc_rec.supplier
      and isc.origin_country_id = I_isc_rec.origin_country_id;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC',
                                            'Program: '||L_program,
                                            'Table: ITEM_SUPP_COUNTRY',
                                            'Item: '||I_isc_rec.item||' Supplier: '||to_char(I_isc_rec.supplier)||' Origin_Country: '||I_isc_rec.origin_country_id);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ISC;
--------------------------------------------------------------------------------------------------------
FUNCTION GET_AVERAGE_WEIGHT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_average_weight      IN OUT ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                            I_item                IN     ITEM_LOC.ITEM%TYPE,
                            I_loc                 IN     ITEM_LOC.LOC%TYPE,   
                            I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                            I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE, 
                            I_dim_object          IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(50)                            := 'ITEM_SUPP_COUNTRY_SQL.GET_AVERAGE_WEIGHT';   
   L_item_supp_country       ITEM_SUPP_COUNTRY%ROWTYPE;
   L_item_supp_country_dim   ITEM_SUPP_COUNTRY_DIM%ROWTYPE;
   L_exists                  BOOLEAN;
   L_item_master_row         ITEM_MASTER%ROWTYPE;
   L_valid                   VARCHAR2(1)                             := null;
   
   cursor C_CHECK_ITEM is 
   select 'x'
     from item_master
    where item = I_item
      and catch_weight_ind = 'Y'
      and simple_pack_ind = 'Y';
      
BEGIN 
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_dim_object is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dim_object',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;    
   
   open C_CHECK_ITEM;
   fetch C_CHECK_ITEM into L_valid;
   close C_CHECK_ITEM;
   
   if L_valid is not null and I_supplier is not null and I_origin_country_id is not null and I_loc_type = 'W' then
      if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_LOC_ROW(O_error_message,
                                                   L_exists,
                                                   L_item_supp_country_dim,
                                                   I_item,
                                                   I_loc,
                                                   I_dim_object) = FALSE then
         return FALSE;
      end if;

      if L_exists then
         if ITEM_SUPP_COUNTRY_SQL.GET_ROW(O_error_message,
                                          L_exists,
                                          L_item_supp_country,
                                          I_item,
                                          I_supplier,
                                          I_origin_country_id) = FALSE then
            return FALSE;
         end if;

         O_average_weight := L_item_supp_country_dim.net_weight/L_item_supp_country.supp_pack_size;
      end if;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
END GET_AVERAGE_WEIGHT; 
--------------------------------------------------------------------------------------------------------
END ITEM_SUPP_COUNTRY_SQL;
/
