CREATE OR REPLACE PACKAGE CORESVC_COST_COMPONENT AUTHID CURRENT_USER AS
   template_key                  CONSTANT VARCHAR2(255)   := 'COST_COMP_DATA';
   template_category             CODE_DETAIL.CODE%TYPE    := 'RMSFND';

   action_new                    VARCHAR2(25)             := 'NEW';
   action_mod                    VARCHAR2(25)             := 'MOD';
   action_del                    VARCHAR2(25)             := 'DEL';

   ELC_COMP_sheet                VARCHAR2(255)   := 'ELC_COMP';
   elc_comp$action               NUMBER          :=1;
   elc_comp$comp_id              NUMBER          :=2;
   elc_comp$comp_desc            NUMBER          :=3;
   elc_comp$comp_type            NUMBER          :=4;
   elc_comp$assess_type          NUMBER          :=5;
   elc_comp$import_country_id    NUMBER          :=6;
   elc_comp$expense_type         NUMBER          :=7;
   elc_comp$up_chrg_type         NUMBER          :=8;
   elc_comp$up_chrg_group        NUMBER          :=9;
   elc_comp$cvb_code             NUMBER          :=10;
   elc_comp$calc_basis           NUMBER          :=11;
   elc_comp$cost_basis           NUMBER          :=12;
   elc_comp$exp_category         NUMBER          :=13;
   elc_comp$comp_rate            NUMBER          :=14;
   elc_comp$comp_level           NUMBER          :=15;
   elc_comp$display_order        NUMBER          :=16;
   elc_comp$always_default_ind   NUMBER          :=17;
   elc_comp$comp_currency        NUMBER          :=18;
   elc_comp$per_count            NUMBER          :=19;
   elc_comp$per_count_uom        NUMBER          :=20;
   elc_comp$nom_flag_1           NUMBER          :=21;
   elc_comp$nom_flag_2           NUMBER          :=22;
   elc_comp$nom_flag_3           NUMBER          :=23;
   elc_comp$nom_flag_4           NUMBER          :=24;
   elc_comp$nom_flag_5           NUMBER          :=25;

   ELC_COMP_TL_sheet             VARCHAR2(255)   := 'ELC_COMP_TL';
   elc_comp_TL$action            NUMBER          :=1;
   elc_comp_TL$lang              NUMBER          :=2;
   elc_comp_TL$comp_id           NUMBER          :=3;
   elc_comp_TL$comp_desc         NUMBER          :=4;

   ELC_COMP_CFA_EXT_sheet        VARCHAR2(255)   :='ELC_COMP_CFA_EXT';
   ELC_COMP_CFA_EXT$ACTION       NUMBER          :=1;
   ELC_COMP_CFA_EXT$COMP_ID      NUMBER          :=2;
   ELC_COMP_CFA_EXT$GROUP_ID     NUMBER          :=3;
   ELC_COMP_CFA_EXT$VARCHAR2_1   NUMBER          :=4;
   ELC_COMP_CFA_EXT$VARCHAR2_2   NUMBER          :=5;
   ELC_COMP_CFA_EXT$VARCHAR2_3   NUMBER          :=6;
   ELC_COMP_CFA_EXT$VARCHAR2_4   NUMBER          :=7;
   ELC_COMP_CFA_EXT$VARCHAR2_5   NUMBER          :=8;
   ELC_COMP_CFA_EXT$VARCHAR2_6   NUMBER          :=9;
   ELC_COMP_CFA_EXT$VARCHAR2_7   NUMBER          :=10;
   ELC_COMP_CFA_EXT$VARCHAR2_8   NUMBER          :=11;
   ELC_COMP_CFA_EXT$VARCHAR2_9   NUMBER          :=12;
   ELC_COMP_CFA_EXT$VARCHAR2_10  NUMBER          :=13;
   ELC_COMP_CFA_EXT$NUMBER_11    NUMBER          :=14;
   ELC_COMP_CFA_EXT$NUMBER_12    NUMBER          :=15;
   ELC_COMP_CFA_EXT$NUMBER_13    NUMBER          :=16;
   ELC_COMP_CFA_EXT$NUMBER_14    NUMBER          :=17;
   ELC_COMP_CFA_EXT$NUMBER_15    NUMBER          :=18;
   ELC_COMP_CFA_EXT$NUMBER_16    NUMBER          :=19;
   ELC_COMP_CFA_EXT$NUMBER_17    NUMBER          :=20;
   ELC_COMP_CFA_EXT$NUMBER_18    NUMBER          :=21;
   ELC_COMP_CFA_EXT$NUMBER_19    NUMBER          :=22;
   ELC_COMP_CFA_EXT$NUMBER_20    NUMBER          :=23;
   ELC_COMP_CFA_EXT$DATE_21      NUMBER          :=24;
   ELC_COMP_CFA_EXT$DATE_22      NUMBER          :=25;
   ELC_COMP_CFA_EXT$DATE_23      NUMBER          :=26;
   ELC_COMP_CFA_EXT$DATE_24      NUMBER          :=27;
   ELC_COMP_CFA_EXT$DATE_25      NUMBER          :=28;

   COST_COMP_UPD_STG_sheet       VARCHAR2(255)   := 'COST_COMP_UPD_STG';
   CCUS$Action                   NUMBER          :=1;
   CCUS$COMP_ID                  NUMBER          :=4;
   CCUS$COMP_TYPE                NUMBER          :=5;
   CCUS$EXPENSE_TYPE             NUMBER          :=6;
   CCUS$NEW_COMP_RATE            NUMBER          :=24;
   CCUS$NEW_COMP_CURRENCY        NUMBER          :=25;
   CCUS$NEW_PER_COUNT            NUMBER          :=26;
   CCUS$NEW_PER_COUNT_UOM        NUMBER          :=27;
   CCUS$CNTRY_DEFAULT_IND        NUMBER          :=28;
   CCUS$SUPP_DEFAULT_IND         NUMBER          :=29;
   CCUS$PTNR_DEFAULT_IND         NUMBER          :=30;
   CCUS$ITEM_DEFAULT_IND         NUMBER          :=31;
   CCUS$ORDER_DEFAULT_IND        NUMBER          :=32;
   CCUS$TSF_ALLOC_DEFAULT_IND    NUMBER          :=33;
   CCUS$DEPT_DEFAULT_IND         NUMBER          :=34;
   CCUS$EFFECTIVE_DATE           NUMBER          :=37;
   CCUS$EFFECTIVE_DATE_UPD       NUMBER          :=38;

   action_column                 VARCHAR2(255)   := 'ACTION';

   sheet_name_trans S9T_PKG.TRANS_MAP_TYP;
   TYPE ELC_COMP_rec_tab IS TABLE OF ELC_COMP%ROWTYPE;
--------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_COST_COMPONENT;
/
