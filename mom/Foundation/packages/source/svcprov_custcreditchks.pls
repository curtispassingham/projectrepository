CREATE OR REPLACE PACKAGE SVCPROV_CUSTCREDITCHK AUTHID CURRENT_USER IS
--------------------------------------------------------------------------------
--FUNCTION NAME : CREATE_CREDIT_IND
--PURPOSE       : The public function which exposes the credit_ind update
--                functionality.
--------------------------------------------------------------------------------
PROCEDURE CREATE_CREDIT_IND (O_serviceOperationStatus    IN OUT "RIB_ServiceOpStatus_REC",
                            I_businessObject            IN     "RIB_CustCreditChkCol_REC");
END SVCPROV_CUSTCREDITCHK;
/