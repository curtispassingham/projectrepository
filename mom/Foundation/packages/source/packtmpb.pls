SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

CREATE OR REPLACE PACKAGE BODY PACK_TEMPLATE_SQL AS
---------------------------------------------------------------------------------------------
--GLOBAL VARIABLE
   LP_rowid         ROWID;
   LP_exception_id  NUMBER(2);

---------------------------------------------------------------------------------------------
   PROCEDURE NEXT_PACK_TEMPLATE_ID (O_error_message    OUT VARCHAR2,
                                    IO_pack_tmpl_id IN OUT PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                                    O_return_code      OUT VARCHAR2) IS

      L_program                     VARCHAR2(64) := 'NEXT_PACK_TEMPLATE_ID';
      L_pack_template_sequence      pack_tmpl_head.pack_tmpl_id%TYPE;
      wrap_sequence_number          pack_tmpl_head.pack_tmpl_id%TYPE;
      first_time                    VARCHAR2(3) := 'Yes';
      dummy                         VARCHAR2(1);

      CURSOR c_pack_template_exists(pack_tmpl_id_param NUMBER) IS
         SELECT 'x'
           FROM pack_tmpl_head
          WHERE pack_tmpl_head.pack_tmpl_id = pack_tmpl_id_param;

   BEGIN

       LOOP
           SELECT pack_template_sequence.NEXTVAL
             INTO L_pack_template_sequence
             FROM sys.dual;

           IF (first_time = 'Yes') THEN
               wrap_sequence_number := L_pack_template_sequence;
               first_time := 'No';
           ELSIF (L_pack_template_sequence = wrap_sequence_number) THEN
               O_error_message := 'Fatal error - no available Pack Template ID';
               O_return_code := 'FALSE';
               Exit;
           END IF;

           IO_pack_tmpl_id := L_pack_template_sequence;

           OPEN  c_pack_template_exists(IO_pack_tmpl_id);
           FETCH c_pack_template_exists into dummy;
           IF (c_pack_template_exists%notfound) THEN
               O_return_code := 'TRUE';
               CLOSE c_pack_template_exists;
               Exit;
           END IF;
           CLOSE c_pack_template_exists;
       END LOOP;

   EXCEPTION
       WHEN OTHERS THEN
           O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                 SQLERRM,
                                                 L_program,
                                                 to_char(SQLCODE));
           O_return_code := 'FALSE';
   END NEXT_PACK_TEMPLATE_ID;
---------------------------------------------------------------------------------------------
   FUNCTION GET_DESC(O_error_message   IN OUT VARCHAR2,
                     O_pack_tmpl_desc  IN OUT PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE,
                     I_pack_tmpl_id    IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
      RETURN BOOLEAN IS

      L_program      VARCHAR2(64) := 'PACK_TEMPLATE_SQL.GET_DESC';
      L_pack_tmpl_desc pack_tmpl_head.pack_tmpl_desc%TYPE;

      cursor C_PACK_TMPL_ID_EXIST is
         SELECT pack_tmpl_desc
           from v_pack_tmpl_head_tl
          where pack_tmpl_id = I_pack_tmpl_id;

   BEGIN
      SQL_LIB.SET_MARK('OPEN',
                       'C_pack_tmpl_id_exist',
                       'v_pack_tmpl_head_tl',
                       'ID: ' || to_char(I_pack_tmpl_id));
      open  C_PACK_TMPL_ID_EXIST;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_pack_tmpl_id_exist',
                       'v_pack_tmpl_head_tl',
                       'ID: ' || to_char(I_pack_tmpl_id));
      fetch C_PACK_TMPL_ID_EXIST into O_pack_tmpl_desc;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_pack_tmpl_id_exist',
                       'v_pack_tmpl_head_tl',
                       'ID: ' || to_char(I_pack_tmpl_id));
      close C_PACK_TMPL_ID_EXIST;
      ---
     
      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END GET_DESC;
---------------------------------------------------------------------------------------------
FUNCTION POP_TEMP_PACK_TMPL(O_error_message     IN OUT VARCHAR2,
                            I_pack_tmpl_id      IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'PACK_TEMPLATE_SQL.POP_TEMP_PACK_TMPL';

BEGIN

   if POP_TEMP_PACK_TMPL(O_error_message,
                         I_pack_tmpl_id,
                         NULL) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POP_TEMP_PACK_TMPL;
-----------------------------------------------------------------------------------------------
FUNCTION POP_TEMP_PACK_TMPL(O_error_message     IN OUT VARCHAR2,
                            I_pack_tmpl_id      IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                            I_like_pack_tmpl_id IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'PACK_TEMPLATE_SQL.POP_TEMP_PACK_TMPL';
   L_display_seq1  TEMP_PACK_TMPL.DIFF1_SEQ%TYPE;
   L_display_seq2  TEMP_PACK_TMPL.DIFF2_SEQ%TYPE;
   L_display_seq3  TEMP_PACK_TMPL.DIFF3_SEQ%TYPE;
   L_display_seq4  TEMP_PACK_TMPL.DIFF4_SEQ%TYPE;
   L_diff_group    DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE;
   L_diff_id       DIFF_GROUP_DETAIL.DIFF_ID%TYPE;
   L_pack_tmpl_id  PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE := NVL(I_like_pack_tmpl_id, I_pack_tmpl_id);
   ---
   cursor C_TMPL_DETAILS is
      select ph.diff_group_1,
             pd.diff_1,
             ph.diff_group_2,
             pd.diff_2,
             ph.diff_group_3,
             pd.diff_3,
             ph.diff_group_4,
             pd.diff_4,
             pd.qty
        from pack_tmpl_head ph, pack_tmpl_detail pd
       where ph.pack_tmpl_id = pd.pack_tmpl_id
         and ph.pack_tmpl_id = L_pack_tmpl_id;
   ---
   cursor C_GET_SEQ_NO is
      select display_seq
        from diff_group_detail
       where diff_group_id = L_diff_group
         and diff_id       = L_diff_id;

BEGIN

   FOR c_rec in C_TMPL_DETAILS LOOP
      if c_rec.diff_group_1 = c_rec.diff_1 then
         L_display_seq1 := NULL;
      else
         L_diff_group := c_rec.diff_group_1;
         L_diff_id    := c_rec.diff_1;
         ---
         open C_GET_SEQ_NO;
         fetch C_GET_SEQ_NO into L_display_seq1;
         close C_GET_SEQ_NO;
      end if;
      ---
      if c_rec.diff_group_2 = c_rec.diff_2 then
         L_display_seq2 := NULL;
      else
         L_diff_group := c_rec.diff_group_2;
         L_diff_id    := c_rec.diff_2;
         ---
         open C_GET_SEQ_NO;
         fetch C_GET_SEQ_NO into L_display_seq2;
         close C_GET_SEQ_NO;
      end if;
      ---
      if c_rec.diff_group_3 = c_rec.diff_3 then
         L_display_seq3 := NULL;
      else
         L_diff_group := c_rec.diff_group_3;
         L_diff_id    := c_rec.diff_3;
         ---
         open C_GET_SEQ_NO;
         fetch C_GET_SEQ_NO into L_display_seq3;
         close C_GET_SEQ_NO;
      end if;
      ---
      if c_rec.diff_group_4 = c_rec.diff_4 then
         L_display_seq4 := NULL;
      else
         L_diff_group := c_rec.diff_group_4;
         L_diff_id    := c_rec.diff_4;
         ---
         open C_GET_SEQ_NO;
         fetch C_GET_SEQ_NO into L_display_seq4;
         close C_GET_SEQ_NO;
      end if;
      ---
      insert into temp_pack_tmpl(pack_tmpl_id,
                                 diff1,
                                 diff1_seq,
                                 diff2,
                                 diff2_seq,
                                 diff3,
                                 diff3_seq,
                                 diff4,
                                 diff4_seq,
                                 qty,
                                 built_ind,
                                 selected_ind)
                          values(I_pack_tmpl_id,
                                 c_rec.diff_1,
                                 L_display_seq1,
                                 c_rec.diff_2,
                                 L_display_seq2,
                                 c_rec.diff_3,
                                 L_display_seq3,
                                 c_rec.diff_4,
                                 L_display_seq4,
                                 c_rec.qty,
                                 'N',
                                 'N');
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POP_TEMP_PACK_TMPL;
---------------------------------------------------------------------------------------------
 FUNCTION DELETE_PACK_TEMPLATE(O_error_message IN OUT VARCHAR2,
                               I_pack_tmpl_id  IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                               I_head_and_tail IN     BOOLEAN)
      RETURN BOOLEAN IS

      L_program          VARCHAR2(64) := 'PACK_TEMPLATE_SQL.DELETE_PACK_TEMPLATE';
      L_dummy            VARCHAR2(1);
      RECORD_LOCKED      EXCEPTION;
      PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

      cursor C_PACK_TEMPLATE is
         select 'x'
           from packitem
          where pack_tmpl_id = I_pack_tmpl_id;
      
      cursor C_RAG_LOCK is
         select 'x'
           from pack_tmpl_detail
          where pack_tmpl_id = I_pack_tmpl_id
            FOR UPDATE NOWAIT;
            
      cursor C_LOCK_RAG_HD_TL is
         SELECT 'x'
           from pack_tmpl_head_tl
          WHERE pack_tmpl_id = I_pack_tmpl_id
            for update nowait;

      cursor C_LOCK_RAG_HD is
         select 'x'
           from pack_tmpl_head
          where pack_tmpl_id = I_pack_tmpl_id
            for update nowait;

   BEGIN
      LP_exception_id := 1;
	  
	  SQL_LIB.SET_MARK('OPEN','C_PACK_TEMPLATE','PACKITEM','ID: ' || to_char(I_pack_tmpl_id));
      open C_PACK_TEMPLATE;
      
      fetch C_PACK_TEMPLATE into L_dummy;
      
      if C_PACK_TEMPLATE%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('PCK_TEMP_EXISTS_PACKITEM',NULL,NULL,NULL);
      
      SQL_LIB.SET_MARK('CLOSE','C_PACK_TEMPLATE','PACKITEM','ID: ' || to_char(I_pack_tmpl_id));
      close C_PACK_TEMPLATE;
      return FALSE;
      end if;
      
      SQL_LIB.SET_MARK('CLOSE','C_PACK_TEMPLATE','PACKITEM','ID: ' || to_char(I_pack_tmpl_id));
      close C_PACK_TEMPLATE;

      SQL_LIB.SET_MARK('OPEN','C_rag_lock','pack_tmpl_detail','ID: ' || to_char(I_pack_tmpl_id));
      open C_RAG_LOCK;
      SQL_LIB.SET_MARK('CLOSE','C_rag_lock','pack_tmpl_detail','ID: ' || to_char(I_pack_tmpl_id));
      close C_RAG_LOCK;

      SQL_LIB.SET_MARK('DELETE',NULL,'pack_tmpl_detail','ID: ' || to_char(I_pack_tmpl_id));
      delete from pack_tmpl_detail
       where pack_tmpl_id = I_pack_tmpl_id;

      if I_head_and_tail then
         LP_exception_id := 2;

         SQL_LIB.SET_MARK('OPEN','C_LOCK_RAG_HD_TL','pack_tmpl_head_tl',NULL);
         open C_LOCK_RAG_HD_TL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_RAG_HD_TL','pack_tmpl_head_tl',NULL);
         close C_LOCK_RAG_HD_TL;

         SQL_LIB.SET_MARK('DELETE',
                          NULL,
                          'pack_tmpl_head_tl',
                          'ID: ' || to_char(I_pack_tmpl_id));
         delete from pack_tmpl_head_tl
          where pack_tmpl_id = I_pack_tmpl_id;
            
         SQL_LIB.SET_MARK('OPEN','C_LOCK_RAG_HD','pack_tmpl_head',NULL);
         open C_LOCK_RAG_HD;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_RAG_HD','pack_tmpl_head',NULL);
         close C_LOCK_RAG_HD;

         SQL_LIB.SET_MARK('DELETE',
                          NULL,
                          'pack_tmpl_head',
                          'ID: ' || to_char(I_pack_tmpl_id));
         delete from pack_tmpl_head
          where pack_tmpl_id = I_pack_tmpl_id;
      end if;

      return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
         if LP_exception_id = 1 then
            O_error_message := SQL_LIB.CREATE_MSG('REC_LOCK_RAG_PP',
                                                  I_pack_tmpl_id);
         elsif LP_exception_id = 2 then
            O_error_message := SQL_LIB.CREATE_MSG('REC_LOCK_RAG_PP_HD', I_pack_tmpl_id);
         end if;
         return FALSE;
      when OTHERS then
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END DELETE_PACK_TEMPLATE;
---------------------------------------------------------------------------------------------
   FUNCTION DELETE_TEMP_PACK_TMPL(O_error_message IN OUT VARCHAR2,
                                  I_pack_tmpl_id  IN     TEMP_PACK_TMPL.PACK_TMPL_ID%TYPE,
                                  I_diff_1        IN     TEMP_PACK_TMPL.DIFF1%TYPE,
                                  I_diff_2        IN     TEMP_PACK_TMPL.DIFF2%TYPE,
                                  I_diff_3        IN     TEMP_PACK_TMPL.DIFF3%TYPE,
                                  I_diff_4        IN     TEMP_PACK_TMPL.DIFF4%TYPE)
      RETURN BOOLEAN IS

      L_program          VARCHAR2(64) := 'PACK_TEMPLATE_SQL.DELETE_TEMP_PACK_TMPL';
      RECORD_LOCKED      EXCEPTION;
      PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

      cursor C_LOCK_TEMP is
         select 'x'
           from temp_pack_tmpl
          where (I_pack_tmpl_id is NULL
                  or pack_tmpl_id = I_pack_tmpl_id)
            and (I_diff_1 is NULL
                  or diff1 = I_diff_1)
            and (I_diff_2 is NULL
                  or diff2 = I_diff_2)
            and (I_diff_3 is NULL
                  or diff3 = I_diff_3)
            and (I_diff_4 is NULL
                  or diff4 = I_diff_4)
            for update nowait;

   BEGIN
      SQL_LIB.SET_MARK('OPEN','C_LOCK_TEMP','pack_tmpl_detail','ID: ' || to_char(I_pack_tmpl_id));
      open C_LOCK_TEMP;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_TEMP','pack_tmpl_detail','ID: ' || to_char(I_pack_tmpl_id));
      close C_LOCK_TEMP;

      delete from temp_pack_tmpl
       where (I_pack_tmpl_id is NULL
               or pack_tmpl_id = I_pack_tmpl_id)
         and (I_diff_1 is NULL
               or diff1 = I_diff_1)
         and (I_diff_2 is NULL
               or diff2 = I_diff_2)
         and (I_diff_3 is NULL
               or diff3 = I_diff_3)
         and (I_diff_4 is NULL
               or diff4 = I_diff_4);

      return TRUE;

   EXCEPTION
       when RECORD_LOCKED then
            O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                  'TEMP_PACK_TMPL',
                                                  NULL,
                                                  NULL);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END DELETE_TEMP_PACK_TMPL;
---------------------------------------------------------------------------------------------
   FUNCTION INSERT_HEAD(O_error_message      IN OUT   VARCHAR2,
                        I_pack_tmpl_id       IN       PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                        I_pack_tmpl_desc     IN       PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE,
                        I_pack_type          IN       PACK_TMPL_HEAD.PACK_TYPE%TYPE,
                        I_diff_group_1       IN       PACK_TMPL_HEAD.DIFF_GROUP_1%TYPE,
                        I_diff_group_2       IN       PACK_TMPL_HEAD.DIFF_GROUP_2%TYPE,
                        I_diff_group_3       IN       PACK_TMPL_HEAD.DIFF_GROUP_3%TYPE,
                        I_diff_group_4       IN       PACK_TMPL_HEAD.DIFF_GROUP_4%TYPE,
                        I_fash_prepack_ind   IN       PACK_TMPL_HEAD.FASH_PREPACK_IND%TYPE,
                        I_receive_as_type    IN       PACK_TMPL_HEAD.REC_AS_TYPE%TYPE)
      RETURN BOOLEAN IS

      L_program VARCHAR2(64) := 'PACK_TEMPLATE_SQL.INSERT_HEAD';

   BEGIN
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'pack_tmpl_head',
                       'ID: ' || to_char(I_pack_tmpl_id) ||
                       ', DIFF GROUP 1: ' || I_diff_group_1 ||
                       ', DIFF GROUP 2: ' || I_diff_group_2 ||
                       ', DIFF GROUP 3: ' || I_diff_group_3 ||
                       ', DIFF GROUP 4: ' || I_diff_group_4);

      insert into pack_tmpl_head(pack_tmpl_id,
                                 pack_tmpl_desc,
                                 diff_group_1,
                                 diff_group_2,
                                 diff_group_3,
                                 diff_group_4,
                                 fash_prepack_ind,
                                 pack_type,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id,
                                 rec_as_type)
                          values(I_pack_tmpl_id,
                                 I_pack_tmpl_desc,
                                 I_diff_group_1,
                                 I_diff_group_2,
                                 I_diff_group_3,
                                 I_diff_group_4,
                                 I_fash_prepack_ind,
                                 I_pack_type,
                                 sysdate,
                                 sysdate,
                                 get_user,
                                 I_receive_as_type);
      return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END INSERT_HEAD;
---------------------------------------------------------------------------------------------
  FUNCTION INSERT_UPDATE_PACK_TMPL_DETAIL(O_error_message IN OUT VARCHAR2,
                                          I_pack_tmpl_id  IN     PACK_TMPL_DETAIL.PACK_TMPL_ID%TYPE,
                                          I_diff_1        IN     PACK_TMPL_DETAIL.DIFF_1%TYPE,
                                          I_diff_2        IN     PACK_TMPL_DETAIL.DIFF_2%TYPE,
                                          I_diff_3        IN     PACK_TMPL_DETAIL.DIFF_3%TYPE,
                                          I_diff_4        IN     PACK_TMPL_DETAIL.DIFF_4%TYPE,
                                          I_qty           IN     PACK_TMPL_DETAIL.QTY%TYPE)
      RETURN BOOLEAN IS

      L_program        VARCHAR2(49) := 'PACK_TEMPLATE_SQL.INSERT_UPDATE_PACK_TMPL_DETAIL';
      RECORD_LOCKED    EXCEPTION;
      PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
      L_seq_no         pack_tmpl_detail.seq_no%TYPE;

      cursor C_LOCK_RAG is
         select rowid
           from pack_tmpl_detail
          where pack_tmpl_id = I_pack_tmpl_id
            and diff_1        = I_diff_1
            and ((I_diff_2 is NULL and diff_2 is NULL)
                 or diff_2    = I_diff_2)
            and ((I_diff_3 is NULL and diff_3 is NULL)
                 or diff_3    = I_diff_3)
            and ((I_diff_4 is NULL and diff_4 is NULL)
                 or diff_4    = I_diff_4)
            for update nowait;

      cursor C_NEXT_SEQ_NO is
         select nvl(max(p.seq_no),0) + 1
           from pack_tmpl_detail p
          where p.pack_tmpl_id = I_pack_tmpl_id;

   BEGIN
      SQL_LIB.SET_MARK('OPEN',
                       'C_lock_rag',
                       'pack_tmpl_detail',
                       'ID: ' || to_char(I_pack_tmpl_id) ||
                       ', DIFF1: ' || I_diff_1 ||
                       ', DIFF2: ' || I_diff_2 ||
                       ', DIFF3: ' || I_diff_3 ||
                       ', DIFF4: ' || I_diff_4);
      open C_LOCK_RAG;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_lock_rag',
                       'pack_tmpl_detail',
                       'ID: ' || to_char(I_pack_tmpl_id) ||
                       ', DIFF1: ' || I_diff_1 ||
                       ', DIFF2: ' || I_diff_2 ||
                       ', DIFF3: ' || I_diff_3 ||
                       ', DIFF4: ' || I_diff_4);
      fetch C_LOCK_RAG into LP_rowid;
      ---
      if C_LOCK_RAG%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_lock_rag',
                          'pack_tmpl_detail',
                          'ID: ' || to_char(I_pack_tmpl_id) ||
                          ', DIFF1: ' || I_diff_1 ||
                          ', DIFF2: ' || I_diff_2 ||
                          ', DIFF3: ' || I_diff_3 ||
                          ', DIFF4: ' || I_diff_4);

         close C_LOCK_RAG;
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_next_seq_no',
                          'pack_tmpl_detail',
                          'ID: ' || to_char(I_pack_tmpl_id));
         open C_NEXT_SEQ_NO;
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_next_seq_no',
                          'pack_tmpl_detail',
                          'ID: ' || to_char(I_pack_tmpl_id));
         fetch C_NEXT_SEQ_NO into L_seq_no;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_next_seq_no',
                          'pack_tmpl_detail',
                          'ID: ' || to_char(I_pack_tmpl_id));
         close C_NEXT_SEQ_NO;
         ---
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'pack_tmpl_detail',
                          'ID: ' || to_char(I_pack_tmpl_id) ||
                          ', DIFF1: ' || I_diff_1 ||
                          ', DIFF2: ' || I_diff_2 ||
                          ', DIFF3: ' || I_diff_3 ||
                          ', DIFF4: ' || I_diff_4);

         insert into pack_tmpl_detail(pack_tmpl_id,
                                      seq_no,
                                      diff_1,
                                      diff_2,
                                      diff_3,
                                      diff_4,
                                      qty,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id)
                               values(I_pack_tmpl_id,
                                      L_seq_no,
                                      I_diff_1,
                                      I_diff_2,
                                      I_diff_3,
                                      I_diff_4,
                                      I_qty,
                                      sysdate,
                                      sysdate,
                                      get_user);
      else
         SQL_LIB.SET_MARK('CLOSE',
                          'C_lock_rag',
                          'pack_tmpl_detail',
                          'ID: ' || to_char(I_pack_tmpl_id) ||
                          ', DIFF1: ' || I_diff_1 ||
                          ', DIFF2: ' || I_diff_2 ||
                          ', DIFF3: ' || I_diff_3 ||
                          ', DIFF4: ' || I_diff_4);

         close C_LOCK_RAG;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,'pack_tmpl_detail','ROWID: ' || LP_rowid);
         update pack_tmpl_detail
            set pack_tmpl_detail.qty = I_qty,
            last_update_datetime = sysdate,
            last_update_id       = get_user
         where rowid = LP_rowid;
      end if;

      return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('REC_LOCK_RAG_PP',
                                               I_pack_tmpl_id,
                                               I_diff_1,
                                               I_diff_2);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END INSERT_UPDATE_PACK_TMPL_DETAIL;
---------------------------------------------------------------------------------------
   FUNCTION GET_FASH_PREPACK_IND(O_error_message IN OUT VARCHAR2,
                                 O_exists        IN OUT BOOLEAN,
                                 I_pack_tmpl_id  IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
      RETURN BOOLEAN IS

      L_program VARCHAR2(64) := 'PACK_TEMPLATE_SQL.GET_FASH_PREPACK_IND';
      L_fash_prepack_ind     pack_tmpl_head.fash_prepack_ind%TYPE;

      cursor C_FASH_IND is
         select fash_prepack_ind
           from pack_tmpl_head
          where pack_tmpl_id = I_pack_tmpl_id;

   BEGIN
      SQL_LIB.SET_MARK('OPEN',
                       'C_fash_ind',
                       'pack_tmpl_head',
                       'ID: '||to_char(I_pack_tmpl_id));

      open C_FASH_IND;

      SQL_LIB.SET_MARK('FETCH',
                       'C_fash_ind',
                       'pack_tmpl_head',
                       'ID: '||to_char(I_pack_tmpl_id));

      fetch C_FASH_IND into L_fash_prepack_ind;

      if C_FASH_IND%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('PACK_TMPL_NOT_FOUND',NULL,NULL,NULL);
         return FALSE;
      else
         if L_fash_prepack_ind = 'Y' then
            O_exists := TRUE;
         else
            O_exists := FALSE;
         end if;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_fash_ind',
                       'pack_tmpl_head',
                       'ID: '||to_char(I_pack_tmpl_id));

      close C_FASH_IND;

      return TRUE;
   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END GET_FASH_PREPACK_IND;
---------------------------------------------------------------------------------
   FUNCTION DELETE_PACK_TMPL_DETAIL(O_error_message  IN OUT VARCHAR2,
                                    I_pack_tmpl_id   IN     PACK_TMPL_DETAIL.PACK_TMPL_ID%TYPE,
                                    I_diff_1         IN     PACK_TMPL_DETAIL.DIFF_1%TYPE,
                                    I_diff_2         IN     PACK_TMPL_DETAIL.DIFF_2%TYPE,
                                    I_diff_3         IN     PACK_TMPL_DETAIL.DIFF_3%TYPE,
                                    I_diff_4         IN     PACK_TMPL_DETAIL.DIFF_4%TYPE)

      RETURN BOOLEAN IS

      L_program         VARCHAR2(64) := 'PACK_TEMPLATE_SQL.DELETE_PACK_TMPL_DETAIL';
      L_dummy           VARCHAR2(1)  := NULL;
      L_table           VARCHAR2(30) := 'PACK_TMPL_DETAIL';
      RECORD_LOCKED     EXCEPTION;
      PRAGMA            EXCEPTION_INIT(Record_locked, -54);

      cursor C_DETAIL_LOCK is
         select 'x'
           from pack_tmpl_detail
          where pack_tmpl_id = I_pack_tmpl_id
            and diff_1 = I_diff_1
            and ((I_diff_2 is NULL and diff_2 is NULL)
                or diff_2 = I_diff_2)
            and ((I_diff_3 is NULL and diff_3 is NULL)
                or diff_3 = I_diff_3)
            and ((I_diff_4 is NULL and diff_4 is NULL)
                or diff_4 = I_diff_4)
            for update nowait;

   BEGIN

      SQL_LIB.SET_MARK('OPEN',
                       'C_detail_lock',
                       'pack_tmpl_detail',
                       'ID: '||to_char(I_pack_tmpl_id));

      open C_DETAIL_LOCK;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_detail_lock',
                       'pack_tmpl_detail',
                       'ID: '||to_char(I_pack_tmpl_id));

      close C_DETAIL_LOCK;

      SQL_LIB.SET_MARK('DELETE',
                       NULL,
                       'pack_tmpl_detail',
                       'ID: '||to_char(I_pack_tmpl_id));

      delete from pack_tmpl_detail
       where pack_tmpl_id = I_pack_tmpl_id
         and diff_1        = I_diff_1
         and ((I_diff_2 is NULL and diff_2 is NULL)
             or diff_2 = I_diff_2)
         and ((I_diff_3 is NULL and diff_3 is NULL)
             or diff_3 = I_diff_3)
         and ((I_diff_4 is NULL and diff_4 is NULL)
             or diff_4 = I_diff_4);

      return TRUE;
   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                                               to_char(I_pack_tmpl_id),
                                               I_diff_1);
         return FALSE;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
   END DELETE_PACK_TMPL_DETAIL;
------------------------------------------------------------------
FUNCTION BUILD_FILTER_WHERE (O_error_message   IN OUT VARCHAR2,
                             O_where_clause    IN OUT FILTER_TEMP.WHERE_CLAUSE%TYPE,
                             I_clear_filter    IN     VARCHAR2)

   RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'PACK_TEMPLATE_SQL.BUILD_FILTER_WHERE';
   L_seq_no           FASHPACK_FILTER_TEMP.SEQ_NO%TYPE;
   L_diff_1           FASHPACK_FILTER_TEMP.DIFF_1%TYPE;
   L_diff_2           FASHPACK_FILTER_TEMP.DIFF_2%TYPE;
   L_diff_3           FASHPACK_FILTER_TEMP.DIFF_3%TYPE;
   L_diff_4           FASHPACK_FILTER_TEMP.DIFF_4%TYPE;
   L_qty              FASHPACK_FILTER_TEMP.QTY%TYPE;
   ---
   L_text             VARCHAR2(9000) := NULL;
   L_where_clause     FILTER_TEMP.WHERE_CLAUSE%TYPE := NULL;
   ---

   cursor C_GET_FASHPACK is
      select seq_no, diff_1, diff_2, diff_3, diff_4, qty
        from fashpack_filter_temp;

BEGIN
   if I_clear_filter = 'Y' then
      delete from fashpack_filter_temp;
      O_where_clause := NULL;
      return TRUE;
   else
      L_where_clause := '';
      FOR rec in C_GET_FASHPACK LOOP
         L_seq_no := rec.seq_no;
         L_diff_1 := rec.diff_1;
         L_diff_2 := rec.diff_2;
         L_diff_3 := rec.diff_3;
         L_diff_4 := rec.diff_4;
         L_qty    := rec.qty;
         ---
         L_text             := '(';
         ---
         if L_seq_no = -999 then
            L_text := L_text||'temp_pack_tmpl.pack_tmpl_id is null and ';
         elsif L_seq_no is not NULL then
            L_text := L_text||'temp_pack_tmpl.pack_tmpl_id = '|| L_seq_no ||' and ';
         end if;
         ---
         if L_diff_1 = 'NULL' then
            L_text := L_text||'temp_pack_tmpl.diff1 is null and ';
         elsif L_diff_1 is not NULL then
            L_text := L_text||'temp_pack_tmpl.diff1 = '''|| L_diff_1||''' and ';
         end if;
         ---
         if L_diff_2 = 'NULL' then
            L_text := L_text||'temp_pack_tmpl.diff2 is null and ';
         elsif L_diff_2 is not NULL then
            L_text := L_text||'temp_pack_tmpl.diff2 = '''|| L_diff_2||''' and ';
         end if;
         ---
         if L_diff_3 = 'NULL' then
            L_text := L_text||'temp_pack_tmpl.diff3 is null and ';
         elsif L_diff_3 is not NULL then
            L_text := L_text||'temp_pack_tmpl.diff3 = '''|| L_diff_3||''' and ';
         end if;
         ---
         if L_diff_4 = 'NULL' then
            L_text := L_text||'temp_pack_tmpl.diff4 is null and ';
         elsif L_diff_4 is not NULL then
            L_text := L_text||'temp_pack_tmpl.diff4 = '''|| L_diff_4||''' and ';
         end if;
         ---
         if L_qty = -999 then
            L_text := L_text||'temp_pack_tmpl.qty is null and ';
         elsif L_qty is not NULL then
            L_text := L_text||'temp_pack_tmpl.qty = '|| L_qty||' and ';
         end if;
         ---
         if (NVL(length(L_where_clause), 0) + nvl(NVL(length(L_text), 0),0)) > 13000 then
            O_error_message := SQL_LIB.CREATE_MSG('WHERE_CLAUSE_TOO_LONG',
                                                  NULL,
                                                  NULL,
                                                  NULL);

            return FALSE;
         else
            L_where_clause := L_where_clause||L_text;
            L_where_clause := SUBSTR(L_where_clause, 1, NVL(LENGTH(L_where_clause), 0)-4);
            L_where_clause := L_where_clause||') or ';
         end if;
         ---
      END LOOP;

      L_where_clause := SUBSTR(L_where_clause, 1, NVL(LENGTH(L_where_clause), 0)-4);

      if L_where_clause is not NULL then
         O_where_clause := '('||L_where_clause||')';
      else
         O_where_clause := NULL;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BUILD_FILTER_WHERE;
--------------------------------------------------------------------------------
FUNCTION BUILD_SUP_COST (O_error_message     IN OUT VARCHAR2,
                         O_sup_cost          IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                         I_pack_template_id  IN     TEMP_PACK_TMPL.PACK_TMPL_ID%TYPE,
                         I_supplier          IN     SUPS.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_item_parent       IN     ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'PACK_TEMPLATE_SQL.BUILD_SUP_COST';

   cursor C_PREPACK_COST is
      select SUM(isp.unit_cost * t.qty)
        from temp_pack_tmpl t,
             item_master im,
             item_supp_country isp
       where t.pack_tmpl_id = I_pack_template_id
         and I_item_parent = im.item_parent (+)
         and t.diff1 is NOT NULL
         and t.diff1 = im.diff_1 (+)
         and nvl(t.diff2, -999) = nvl(im.diff_2(+), -999)
         and nvl(t.diff3, -999) = nvl(im.diff_3(+), -999)
         and nvl(t.diff4, -999) = nvl(im.diff_4(+), -999)
         and isp.supplier = I_supplier
         and isp.origin_country_id = I_origin_country_id
         and isp.item = NVL(im.item, I_item_parent)
         and t.qty is NOT NULL;
BEGIN

   open C_PREPACK_COST;
   fetch C_PREPACK_COST into O_sup_cost;
   close C_PREPACK_COST;
   ---
   if O_sup_cost is NULL then
      O_sup_cost := 0;
   end if;

return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BUILD_SUP_COST;
---------------------------------------------------------------------------------------
FUNCTION CHECK_ALL_RECORDS (O_error_message  IN OUT VARCHAR2,
                            O_valid          IN OUT BOOLEAN,
                            I_num_of_diffs   IN     NUMBER)
   RETURN BOOLEAN IS

   L_dummy    VARCHAR2(1) := 'N';

   cursor C_CHECK is
      select 'Y'
        from temp_pack_tmpl
       where pack_tmpl_id is NULL
          or diff1 is NULL
          or (I_num_of_diffs = 2 and (diff1 is null or diff2 is NULL))
          or (I_num_of_diffs = 3 and (diff1 is null or diff2 is NULL or diff3 is NULL))
          or (I_num_of_diffs = 4 and (diff1 is null or diff2 is NULL or diff3 is NULL or diff4 is NULL))
          or qty is NULL
          or qty = 0;

BEGIN

   open C_CHECK;
   fetch C_CHECK into L_dummy;
   close C_CHECK;

   if L_dummy = 'Y' then
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PACK_TEMPLATE_SQL.CHECK_ALL_RECORDS',
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ALL_RECORDS;
---------------------------------------------------------------------------------------
FUNCTION DELETE_PACK_ITEM_EXPENSES(O_error_message IN OUT VARCHAR2,
                                   I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                                   I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                                   I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                                   I_diff_1        IN     ITEM_MASTER.DIFF_1%TYPE,
                                   I_diff_2        IN     ITEM_MASTER.DIFF_2%TYPE,
                                   I_diff_3        IN     ITEM_MASTER.DIFF_3%TYPE,
                                   I_diff_4        IN     ITEM_MASTER.DIFF_4%TYPE)
RETURN BOOLEAN IS
 L_program         VARCHAR2(64) := 'PACK_TEMPLATE_SQL.DELETE_PACK_ITEM_EXPENSES';
 L_item            ITEM_MASTER.ITEM%TYPE;

 cursor C_GET_CHILD is
    select item
      from item_master
     where (item_parent = I_item_parent
           or item_grandparent = I_item_parent)
       and diff_1 = I_diff_1
       and ((diff_2 is NULL and I_diff_2 is NULL)
            or (diff_2 = I_diff_2))
       and ((diff_3 is NULL and I_diff_3 is NULL)
            or (diff_3 = I_diff_3))
       and ((diff_4 is NULL and I_diff_4 is NULL)
            or (diff_4 = I_diff_4));

BEGIN
   if I_order_no is NULL or I_pack_no is NULL or
    I_item_parent is NULL or I_diff_1 is NULL then
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                    || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
   open C_GET_CHILD;

   SQL_LIB.SET_MARK('FETCH','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                    || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
   fetch C_GET_CHILD into L_item;

   SQL_LIB.SET_MARK('CLOSE','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                    || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
   close C_GET_CHILD;
   ---
   if not ORDER_SETUP_SQL.DELETE_COMPS(O_error_message,
                                           I_order_no,
                                           I_pack_no,
                                           L_item,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) then
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_PACK_ITEM_EXPENSES;
---------------------------------------------------------------------------------------
FUNCTION UPDATE_PACK_ITEM_EXPENSES(O_error_message IN OUT VARCHAR2,
                                   I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                                   I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                                   I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                                   I_diff_1        IN     ITEM_MASTER.DIFF_1%TYPE,
                                   I_diff_2        IN     ITEM_MASTER.DIFF_2%TYPE,
                                   I_diff_3        IN     ITEM_MASTER.DIFF_3%TYPE,
                                   I_diff_4        IN     ITEM_MASTER.DIFF_4%TYPE)

RETURN BOOLEAN IS
 L_program         VARCHAR2(64) := 'PACK_TEMPLATE_SQL.UPDATE_PACK_ITEM_EXPENSES';
 L_item            ITEM_MASTER.ITEM%TYPE;
 L_dummy           VARCHAR2(1) := NULL;

 cursor C_CHECK_ORDLOC_EXP is
   select 'X'
     from item_master im,
          ordloc_exp oe
    where (im.item_parent = I_item_parent or im.item_grandparent = I_item_parent)
      and oe.pack_item = I_pack_no
      and oe.order_no  = I_order_no
      and im.diff_1    = I_diff_1
      and (im.diff_2   = I_diff_2
           or (im.diff_2 is NULL and I_diff_2 is NULL))
      and (im.diff_3   = I_diff_3
           or (im.diff_3 is NULL and I_diff_3 is NULL))
      and (im.diff_4   = I_diff_4
           or (im.diff_4 is NULL and I_diff_4 is NULL))
      and oe.item      = im.item;

 cursor C_CHECK_ORDSKU_HTS is
    select 'X'
      from item_master im,
           ordsku_hts os
     where (im.item_parent = I_item_parent or im.item_grandparent = I_item_parent)
       and os.pack_item = I_pack_no
       and os.order_no  = I_order_no
       and im.diff_1    = I_diff_1
       and (im.diff_2   = I_diff_2
           or (im.diff_2 is NULL and I_diff_2 is NULL))
       and (im.diff_3   = I_diff_3
           or (im.diff_3 is NULL and I_diff_3 is NULL))
       and (im.diff_4   = I_diff_4
           or (im.diff_4 is NULL and I_diff_4 is NULL))
       and os.item      = im.item;

 cursor C_GET_CHILD is
    select item
      from item_master
     where (item_parent = I_item_parent
           or item_grandparent = I_item_parent)
       and diff_1 = I_diff_1
       and ((diff_2 is NULL and I_diff_2 is NULL)
            or (diff_2 = I_diff_2))
       and ((diff_3 is NULL and I_diff_3 is NULL)
            or (diff_3 = I_diff_3))
       and ((diff_4 is NULL and I_diff_4 is NULL)
            or (diff_4 = I_diff_4));

BEGIN
   if I_order_no is NULL or I_pack_no is NULL or
    I_item_parent is NULL or I_diff_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS', NULL, NULL, NULL);
      return FALSE;
   end if;
   --- Check Expenses
   open C_CHECK_ORDLOC_EXP;

   fetch C_CHECK_ORDLOC_EXP into L_dummy;

   if C_CHECK_ORDLOC_EXP%NOTFOUND then
      close C_CHECK_ORDLOC_EXP;
      SQL_LIB.SET_MARK('OPEN','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                       || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
      open C_GET_CHILD;

      SQL_LIB.SET_MARK('FETCH','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                       || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
      fetch C_GET_CHILD into L_item;

      SQL_LIB.SET_MARK('CLOSE','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                       || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
      close C_GET_CHILD;
      ---
      if not ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                                        I_order_no,
                                                        I_pack_no,
                                                        L_item,
                                                        NULL,
                                                        NULL) then
         return FALSE;
      end if;
   else
      close C_CHECK_ORDLOC_EXP;
   end if;

   --- Check Assessments
   open C_CHECK_ORDSKU_HTS;

   fetch C_CHECK_ORDSKU_HTS into L_dummy;

   if C_CHECK_ORDSKU_HTS%NOTFOUND then
      close C_CHECK_ORDSKU_HTS;
      if L_item is NULL then
         SQL_LIB.SET_MARK('OPEN','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                          || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
         open C_GET_CHILD;

         SQL_LIB.SET_MARK('FETCH','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                          || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
         fetch C_GET_CHILD into L_item;

         SQL_LIB.SET_MARK('CLOSE','C_GET_CHILD','ITEM_MASTER','Pack: ' || I_pack_no
                          || 'Order: ' ||to_char(I_order_no)|| 'Item Parent: '||I_item_parent);
         close C_GET_CHILD;
      end if;
      ---
      if not ORDER_HTS_SQL.DEFAULT_CALC_HTS(O_error_message,
                                            I_order_no,
                                            I_pack_no,
                                            L_item) then
         return FALSE;
      end if;
   else
      close C_CHECK_ORDSKU_HTS;
   end if;
   ---recalculate all of the expenses after assessments calculation
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PE',
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             I_order_no,
                             NULL,
                             I_pack_no,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_PACK_ITEM_EXPENSES;
---------------------------------------------------------------------------------------





---------------------------------------------------------------------------------------
FUNCTION EXIST(O_error_message    IN OUT VARCHAR2,
               O_exist            IN OUT BOOLEAN,
               I_pack_tmpl_id     IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)     := 'PACK_TEMPLATE_SQL.EXIST';
   L_exist      VARCHAR2(1)      := NULL;

   cursor C_EXIST is
      select 'x'
        from pack_tmpl_head
       where pack_tmpl_id = I_pack_tmpl_id;

BEGIN

   if I_pack_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_pack_tmpl_id', L_program, NULL);
      return FALSE;
   end if;
   ---
   open C_EXIST;
   fetch C_EXIST into L_exist;
   if C_EXIST%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;

   close C_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END EXIST;
-------------------------------------------------------------------------------------------
FUNCTION LIKE_TMPL(O_error_message        IN OUT VARCHAR2,
                   I_new_pack_tmpl_id     IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                   I_new_pack_tmpl_desc   IN     PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE,
                   I_like_tmpl            IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'PACK_TEMPLATE_SQL.LIKE_TMPL';

BEGIN

   if I_new_pack_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_new_pack_tmpl_id', L_program, NULL);
      return FALSE;
   elsif I_like_tmpl is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_like_tmpl', L_program, NULL);
      return FALSE;
   end if;
   ---
   insert into pack_tmpl_head(pack_tmpl_id,
                              pack_tmpl_desc,
                              diff_group_1,
                              diff_group_2,
                              diff_group_3,
                              diff_group_4,
                              fash_prepack_ind,
                              create_datetime,
                              last_update_datetime,
                              last_update_id)
                       select I_new_pack_tmpl_id,
                              NVL(I_new_pack_tmpl_desc, pack_tmpl_desc),
                              diff_group_1,
                              diff_group_2,
                              diff_group_3,
                              diff_group_4,
                              fash_prepack_ind,
                              sysdate,
                              sysdate,
                              get_user
                         from pack_tmpl_head
                        where pack_tmpl_id = I_like_tmpl;

   ---
   insert into pack_tmpl_detail(pack_tmpl_id,
                                seq_no,
                                diff_1,
                                diff_2,
                                diff_3,
                                diff_4,
                                qty,
                                create_datetime,
                                last_update_datetime,
                                last_update_id)
                         select I_new_pack_tmpl_id,
                                seq_no,
                                diff_1,
                                diff_2,
                                diff_3,
                                diff_4,
                                qty,
                                sysdate,
                                sysdate,
                                get_user
                           from pack_tmpl_detail
                          where pack_tmpl_id = I_like_tmpl;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END LIKE_TMPL;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE(O_error_message IN OUT VARCHAR2,
                         O_duplicate     IN OUT BOOLEAN,
                         I_diff_1        IN     PACK_TMPL_DETAIL.DIFF_1%TYPE,
                         I_diff_2        IN     PACK_TMPL_DETAIL.DIFF_2%TYPE,
                         I_diff_3        IN     PACK_TMPL_DETAIL.DIFF_3%TYPE,
                         I_diff_4        IN     PACK_TMPL_DETAIL.DIFF_4%TYPE,
                         I_pack_tmpl_id  IN     PACK_TMPL_DETAIL.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_dummy   VARCHAR2(1)  := 'N';
   L_program VARCHAR2(64) := 'PACK_TEMPLATE_SQL.CHECK_DUPLICATE';

   cursor C_DUPLICATE is
      select 'Y'
        from pack_tmpl_detail
       where (pack_tmpl_id = I_pack_tmpl_id
              or (I_pack_tmpl_id is NULL
                  and pack_tmpl_id is NULL))
         and diff_1 = I_diff_1
         and (diff_2 = I_diff_2
              or diff_2 is NULL and I_diff_2 is NULL)
         and (diff_3 = I_diff_3
              or diff_3 is NULL and I_diff_3 is NULL)
         and (diff_4 = I_diff_4
              or diff_4 is NULL and I_diff_4 is NULL);

BEGIN

   if I_diff_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_diff_1',
                                            L_program,
                                            NULL);
         return FALSE;
   elsif I_pack_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_tmpl_id',
                                            L_program,
                                            NULL);
         return FALSE;
   end if;

   open C_DUPLICATE;
   fetch C_DUPLICATE into L_dummy;
   close C_DUPLICATE;

   if L_dummy = 'Y' then
      O_duplicate := TRUE;
   else
      O_duplicate := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_DUPLICATE;
-------------------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_header_rec    IN OUT PACK_TEMPLATE_SQL.HEADER_INFO,
                         I_diff_group1   IN     PACK_TMPL_HEAD.DIFF_GROUP_1%TYPE,
                         I_diff_group2   IN     PACK_TMPL_HEAD.DIFF_GROUP_2%TYPE,
                         I_diff_group3   IN     PACK_TMPL_HEAD.DIFF_GROUP_3%TYPE,
                         I_diff_group4   IN     PACK_TMPL_HEAD.DIFF_GROUP_4%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'PACK_TEMPLATE_SQL.GET_HEADER_INFO';

   cursor C_GET_HEADER_INFO is
      select dgtl1.diff_group_desc,
             DECODE(I_diff_group2, NULL, NULL, dgtl2.diff_group_desc),
             DECODE(I_diff_group3, NULL, NULL, dgtl3.diff_group_desc),
             DECODE(I_diff_group4, NULL, NULL, dgtl4.diff_group_desc),
             dg1.diff_type,
             dt1.diff_type_desc,
             DECODE(I_diff_group2, NULL, NULL, dg2.diff_type),
             DECODE(I_diff_group2, NULL, NULL, dt2.diff_type_desc),
             DECODE(I_diff_group3, NULL, NULL, dg3.diff_type),
             DECODE(I_diff_group3, NULL, NULL, dt3.diff_type_desc),
             DECODE(I_diff_group4, NULL, NULL, dg4.diff_type),
             DECODE(I_diff_group4, NULL, NULL, dt4.diff_type_desc)
        from v_diff_group_head_tl dgtl1,
             v_diff_group_head_tl dgtl2,
             v_diff_group_head_tl dgtl3,
             v_diff_group_head_tl dgtl4,
             diff_group_head dg1,
             diff_group_head dg2,
             diff_group_head dg3,
             diff_group_head dg4,
             v_diff_type_tl dt1,
             v_diff_type_tl dt2,
             v_diff_type_tl dt3,
             v_diff_type_tl dt4
       where dg1.diff_group_id = I_diff_group1
         and (dg2.diff_group_id = I_diff_group2
              or I_diff_group2 is NULL)
         and (dg3.diff_group_id = I_diff_group3
              or I_diff_group3 is NULL)
         and (dg4.diff_group_id = I_diff_group4
              or I_diff_group4 is NULL)
         and dt1.diff_type      = dg1.diff_type
         and (dt2.diff_type     = dg2.diff_type)  /* diff_group_head-diff_type join will occur even */
         and (dt3.diff_type     = dg3.diff_type)  /* if I_diff_group2, I_diff_group3 and I_diff_group4 */
         and (dt4.diff_type     = dg4.diff_type) /* are NULL - then values are cleared later */
         and dgtl1.diff_group_id = dg1.diff_group_id
         and dgtl2.diff_group_id = dg2.diff_group_id
         and dgtl3.diff_group_id = dg3.diff_group_id
         and dgtl4.diff_group_id = dg4.diff_group_id;

BEGIN

   if I_diff_group1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_diff_group1',
                                            L_program,
                                            NULL);
         return FALSE;
   end if;

   open C_GET_HEADER_INFO;
   fetch C_GET_HEADER_INFO into O_header_rec.group1_desc,
                                O_header_rec.group2_desc,
                                O_header_rec.group3_desc,
                                O_header_rec.group4_desc,
                                O_header_rec.type1,
                                O_header_rec.type1_desc,
                                O_header_rec.type2,
                                O_header_rec.type2_desc,
                                O_header_rec.type3,
                                O_header_rec.type3_desc,
                                O_header_rec.type4,
                                O_header_rec.type4_desc;
   close C_GET_HEADER_INFO;

   if O_header_rec.group1_desc is NULL then
      -- cursor found no records
      O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_GROUP',
                                             NULL,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_HEADER_INFO;
-------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_seq_no        IN OUT PACK_TMPL_DETAIL.SEQ_NO%TYPE,
                         I_pack_tmpl_id  IN     PACK_TMPL_DETAIL.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'PACK_TEMPLATE_SQL.GET_NEXT_SEQ_NO';

   cursor C_SEQ_NO is
      select NVL(max(seq_no + 1),1)
        from pack_tmpl_detail
       where pack_tmpl_id = I_pack_tmpl_id;

BEGIN

   if I_pack_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_tmpl_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_SEQ_NO;
   fetch C_SEQ_NO into O_seq_no;
   close C_SEQ_NO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_NEXT_SEQ_NO;
-------------------------------------------------------------------------------------------
FUNCTION GET_GROUPS_AND_DESC(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists           IN OUT BOOLEAN,
                             O_pack_tmpl_desc   IN OUT PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE,
                             O_group_1          IN OUT PACK_TMPL_HEAD.DIFF_GROUP_1%TYPE,
                             O_group_2          IN OUT PACK_TMPL_HEAD.DIFF_GROUP_2%TYPE,
                             O_group_3          IN OUT PACK_TMPL_HEAD.DIFF_GROUP_3%TYPE,
                             O_group_4          IN OUT PACK_TMPL_HEAD.DIFF_GROUP_4%TYPE,
                             I_pack_tmpl_id     IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'PACK_TEMPLATE_SQL.GET_GROUPS_AND_DESC';

   cursor C_GROUPS_DESC is
      select pthtl.pack_tmpl_desc,
             pth.diff_group_1,
             pth.diff_group_2,
             pth.diff_group_3,
             pth.diff_group_4
        from pack_tmpl_head pth, 
             v_pack_tmpl_head_tl pthtl
       where pth.pack_tmpl_id = I_pack_tmpl_id
         and pthtl.pack_tmpl_id = pth.pack_tmpl_id;

BEGIN

   if I_pack_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_pack_tmpl_id', L_program, NULL);
      return FALSE;
   end if;

   open C_GROUPS_DESC;
   fetch C_GROUPS_DESC into O_pack_tmpl_desc,
                            O_group_1,
                            O_group_2,
                            O_group_3,
                            O_group_4;
   if O_pack_tmpl_desc is not NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   close C_GROUPS_DESC;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_GROUPS_AND_DESC;
-------------------------------------------------------------------------------------------
FUNCTION APPLY_RANGE(O_error_message  IN OUT  VARCHAR2,
                     I_where_clause   IN      VARCHAR2,
                     I_rec            IN      INPUT_RECTYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(40) := 'PACK_TEMPLATE_SQL.APPLY_RANGE';

   L_diff1          VARCHAR2(61);
   L_diff2          VARCHAR2(61);
   L_diff3          VARCHAR2(61);
   L_diff4          VARCHAR2(61);
   L_diff1_seq      VARCHAR2(61) := NVL(to_char(I_rec.diff1_seq),'NULL')||',';
   L_diff2_seq      VARCHAR2(61) := NVL(to_char(I_rec.diff2_seq),'NULL')||',';
   L_diff3_seq      VARCHAR2(61) := NVL(to_char(I_rec.diff3_seq),'NULL')||',';
   L_diff4_seq      VARCHAR2(61) := NVL(to_char(I_rec.diff4_seq),'NULL')||',';

   L_pack_tmpl_id   VARCHAR2(30);

   L_sum            NUMBER(12);

   L_mark_rows      VARCHAR2(6000);
   L_insert_string  VARCHAR2(5000);
   L_select1        VARCHAR2(1000);
   L_select2        VARCHAR2(1000);
   L_select         VARCHAR2(1000);
   L_qty_string     VARCHAR2(1000);
   L_from           VARCHAR2(1000);
   L_input_where    VARCHAR2(6000);
   L_where          VARCHAR2(6000);
   L_sql_statement  VARCHAR2(200);

   x                BINARY_INTEGER;

   cursor C_SAVE_ROWIDS is
      select rowid
        from temp_pack_tmpl;
   cursor C_RANGE_SUM is
      select SUM(NVL(drd.qty,1))
        from diff_range_detail drd
       where drd.diff_range = I_rec.diff_range;

BEGIN

   open C_RANGE_SUM;
   fetch C_RANGE_SUM into L_sum;
   close C_RANGE_SUM;

   if I_rec.apply_to = 'ONE' then

      L_pack_tmpl_id := NVL(to_char(I_rec.pack_tmpl_id),'NULL');

      --- Set up SQL statement
      if I_rec.row_id is NULL then
         L_select1 := 'select '|| L_pack_tmpl_id ||', ';
         L_select2 := ' ''N'', '
                   || ' ''N'' ';
         L_from    := '  from diff_range_detail drd, '
                   || '       diff_range_head drh ';
         L_where   := ' where drd.diff_range = :l_diff_range '
                   || '   and drd.diff_range = drh.diff_range ' ;
      else
         L_select1 := 'select tpt.pack_tmpl_id, ';
         L_select2 := '       tpt.built_ind, '
                   || '       tpt.selected_ind ';
         L_from    := '  from temp_pack_tmpl tpt, '
                   || '       diff_range_detail drd, '
                   || '       diff_range_head drh ';
         L_where   := ' where tpt.rowid =  :l_row_id '
                   || '   and drd.diff_range = :l_diff_range '
                   || '   and drd.diff_range = drh.diff_range ' ;
      end if;
      if  I_rec.diff1 is NOT NULL then
         L_diff1     := '''' ||I_rec.diff1 || ''', ';
      else
         if I_rec.parent_diff1 = I_rec.diff_range_group1 then
            L_diff1     := 'drd.diff_1, ';
            L_diff1_seq := 'dgd1.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd1 ';
            L_where     := L_where || ' and drh.diff_group_1 = dgd1.diff_group_id '
                                   || ' and dgd1.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         elsif I_rec.parent_diff1 = I_rec.diff_range_group2 then
            L_diff1     := 'drd.diff_2, ';
            L_diff1_seq := 'dgd1.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd1 ';
            L_where     := L_where || ' and drh.diff_group_2 = dgd1.diff_group_id '
                                   || ' and dgd1.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         elsif I_rec.parent_diff1 = I_rec.diff_range_group3 then
            L_diff1     := 'drd.diff_3, ';
            L_diff1_seq := 'dgd1.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd1 ';
            L_where     := L_where || ' and drh.diff_group_3 = dgd1.diff_group_id '
                                   || ' and dgd1.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         else
            L_diff1     := 'NULL, ';
            L_diff1_seq := 'NULL, ';
         end if;
      end if;
      if  I_rec.diff2 is NOT NULL then
         L_diff2     := '''' ||I_rec.diff2 || ''', ';
      else
         if I_rec.parent_diff2 = I_rec.diff_range_group1 then
            L_diff2     := 'drd.diff_1, ';
            L_diff2_seq := 'dgd2.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd2 ';
            L_where     := L_where || ' and drh.diff_group_1 = dgd2.diff_group_id '
                                   || ' and dgd2.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         elsif I_rec.parent_diff2 = I_rec.diff_range_group2 then
            L_diff2     := 'drd.diff_2, ';
            L_diff2_seq := 'dgd2.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd2 ';
            L_where     := L_where || ' and drh.diff_group_2 = dgd2.diff_group_id '
                                   || ' and dgd2.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         elsif I_rec.parent_diff2 = I_rec.diff_range_group3 then
            L_diff2     := 'drd.diff_3, ';
            L_diff2_seq := 'dgd2.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd2 ';
            L_where     := L_where || ' and drh.diff_group_3 = dgd2.diff_group_id '
                                   || ' and dgd2.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         else
            L_diff2     := 'NULL, ';
            L_diff2_seq := 'NULL, ';
         end if;
      end if;
      if  I_rec.diff3 is NOT NULL then
         L_diff3     := '''' ||I_rec.diff3 || ''', ';
      else
         if I_rec.parent_diff3 = I_rec.diff_range_group1 then
            L_diff3     := 'drd.diff_1,';
            L_diff3_seq := 'dgd3.display_seq,';
            L_from      := L_from  || ', diff_group_detail dgd3 ';
            L_where     := L_where || ' and drh.diff_group_1 = dgd3.diff_group_id '
                                   || ' and dgd3.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         elsif I_rec.parent_diff3 = I_rec.diff_range_group2 then
            L_diff3     := 'drd.diff_2, ';
            L_diff3_seq := 'dgd3.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd3 ';
            L_where     := L_where || ' and drh.diff_group_2 = dgd3.diff_group_id '
                                   || ' and dgd3.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         elsif I_rec.parent_diff3 = I_rec.diff_range_group3 then
            L_diff3     := 'drd.diff_3, ';
            L_diff3_seq := 'dgd3.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd3 ';
            L_where     := L_where || ' and drh.diff_group_3 = dgd3.diff_group_id '
                                   || ' and dgd3.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         else
            L_diff3     := 'NULL, ';
            L_diff3_seq := 'NULL, ';
         end if;
      end if;
      if  I_rec.diff4 is NOT NULL then
         L_diff4     := '''' ||I_rec.diff4|| ''', ';
      else
         if I_rec.parent_diff4 = I_rec.diff_range_group1 then
            L_diff4     := 'drd.diff_1,';
            L_diff4_seq := 'dgd4.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd4 ';
            L_where     := L_where || ' and drh.diff_group_1 = dgd4.diff_group_id '
                                   || ' and dgd4.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         elsif I_rec.parent_diff4 = I_rec.diff_range_group2 then
            L_diff4     := 'drd.diff_2, ';
            L_diff4_seq := 'dgd4.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd4 ';
            L_where     := L_where || ' and drh.diff_group_2 = dgd4.diff_group_id '
                                   || ' and dgd4.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         elsif I_rec.parent_diff4 = I_rec.diff_range_group3 then
            L_diff4     := 'drd.diff_3, ';
            L_diff4_seq := 'dgd4.display_seq, ';
            L_from      := L_from  || ', diff_group_detail dgd4 ';
            L_where     := L_where || ' and drh.diff_group_3 = dgd4.diff_group_id '
                                   || ' and dgd4.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
         else
            L_diff4     := 'NULL, ';
            L_diff4_seq := 'NULL, ';
         end if;
      end if;
   else -- apply_to = 'ALL'

      --- Mark all current rows so we can go back and delete them when
      --- finished applying the range.
      L_mark_rows := 'update temp_pack_tmpl '
                  ||    'set delete_ind = ''Y'' ';

      L_input_where := I_where_clause;
      if L_input_where is NOT NULL then
         L_mark_rows := L_mark_rows || 'where '|| L_input_where;
         L_input_where := ' and ' || REPLACE(L_input_where,'temp_pack_tmpl','tpt');
      end if;

      EXECUTE IMMEDIATE L_mark_rows;

      L_pack_tmpl_id := NVL(to_char(I_rec.pack_tmpl_id),'tpt.pack_tmpl_id');

      --- Set up SQL statement
      L_select1 := 'select '|| L_pack_tmpl_id ||', ';
      L_select2 := '       tpt.built_ind, '
                || '       tpt.selected_ind ';
      L_from    := '  from temp_pack_tmpl tpt, '
                || '       diff_range_detail drd, '
                || '       diff_range_head drh ';
      L_where   := ' where drd.diff_range = :l_diff_range '
                || '   and drd.diff_range = drh.diff_range '
                || L_input_where ;
      if I_rec.parent_diff1 = I_rec.diff_range_group1 then
         L_diff1     := 'drd.diff_1, ';
         L_diff1_seq := 'dgd1.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd1 ';
         L_where     := L_where || ' and drh.diff_group_1 = dgd1.diff_group_id '
                                || ' and dgd1.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      elsif I_rec.parent_diff1 = I_rec.diff_range_group2 then
         L_diff1     := 'drd.diff_2, ';
         L_diff1_seq := 'dgd1.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd1 ';
         L_where     := L_where || ' and drh.diff_group_2 = dgd1.diff_group_id '
                                || ' and dgd1.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      elsif I_rec.parent_diff1 = I_rec.diff_range_group3 then
         L_diff1     := 'drd.diff_3, ';
         L_diff1_seq := 'dgd1.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd1 ';
         L_where     := L_where || ' and drh.diff_group_3 = dgd1.diff_group_id '
                                || ' and dgd1.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      else
         L_diff1     := 'tpt.diff1, ';
         L_diff1_seq := 'tpt.diff1_seq, ';
      end if;
      if I_rec.parent_diff2 = I_rec.diff_range_group1 then
         L_diff2     := 'drd.diff_1, ';
         L_diff2_seq := 'dgd2.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd2 ';
         L_where     := L_where || ' and drh.diff_group_1 = dgd2.diff_group_id '
                                || ' and dgd2.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      elsif I_rec.parent_diff2 = I_rec.diff_range_group2 then
         L_diff2     := 'drd.diff_2, ';
         L_diff2_seq := 'dgd2.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd2 ';
         L_where     := L_where || ' and drh.diff_group_2 = dgd2.diff_group_id '
                                || ' and dgd2.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      elsif I_rec.parent_diff2 = I_rec.diff_range_group3 then
         L_diff2     := 'drd.diff_3, ';
         L_diff2_seq := 'dgd2.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd2 ';
         L_where     := L_where || ' and drh.diff_group_3 = dgd2.diff_group_id '
                                || ' and dgd2.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      else
         L_diff2     := 'tpt.diff2, ';
         L_diff2_seq := 'tpt.diff2_seq, ';
      end if;
      if I_rec.parent_diff3 = I_rec.diff_range_group1 then
         L_diff3     := 'drd.diff_1,';
         L_diff3_seq := 'dgd3.display_seq,';
         L_from      := L_from  || ', diff_group_detail dgd3 ';
         L_where     := L_where || ' and drh.diff_group_1 = dgd3.diff_group_id '
                                || ' and dgd3.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      elsif I_rec.parent_diff3 = I_rec.diff_range_group2 then
         L_diff3     := 'drd.diff_2, ';
         L_diff3_seq := 'dgd3.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd3 ';
         L_where     := L_where || ' and drh.diff_group_2 = dgd3.diff_group_id '
                                || ' and dgd3.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      elsif I_rec.parent_diff3 = I_rec.diff_range_group3 then
         L_diff3     := 'drd.diff_3, ';
         L_diff3_seq := 'dgd3.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd3 ';
         L_where     := L_where || ' and drh.diff_group_3 = dgd3.diff_group_id '
                                || ' and dgd3.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      else
         L_diff3     := 'tpt.diff3, ';
         L_diff3_seq := 'tpt.diff3_seq, ';
      end if;
      if I_rec.parent_diff4 = I_rec.diff_range_group1 then
         L_diff4     := 'drd.diff_1,';
         L_diff4_seq := 'dgd4.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd4 ';
         L_where     := L_where || ' and drh.diff_group_1 = dgd4.diff_group_id '
                                || ' and dgd4.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      elsif I_rec.parent_diff4 = I_rec.diff_range_group2 then
         L_diff4     := 'drd.diff_2, ';
         L_diff4_seq := 'dgd4.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd4 ';
         L_where     := L_where || ' and drh.diff_group_2 = dgd4.diff_group_id '
                                || ' and dgd4.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      elsif I_rec.parent_diff4 = I_rec.diff_range_group3 then
         L_diff4     := 'drd.diff_3, ';
         L_diff4_seq := 'dgd4.display_seq, ';
         L_from      := L_from  || ', diff_group_detail dgd4 ';
         L_where     := L_where || ' and drh.diff_group_3 = dgd4.diff_group_id '
                                || ' and dgd4.diff_id in (drd.diff_1, drd.diff_2, drd.diff_3) ';
      else
         L_diff4     := 'tpt.diff4, ';
         L_diff4_seq := 'tpt.diff4_seq, ';
      end if;
   end if;

   if I_rec.qty is NULL then
      L_qty_string := 'round(tpt.qty * (NVL(drd.qty,1)/'||L_sum||')),';
   else
      L_qty_string := 'round('||I_rec.qty||'* (NVL(drd.qty,1)/'||L_sum||')),';
   end if;

   L_select := L_select1
            || L_diff1
            || L_diff1_seq
            || L_diff2
            || L_diff2_seq
            || L_diff3
            || L_diff3_seq
            || L_diff4
            || L_diff4_seq
            || L_qty_string
            || L_select2;

   L_insert_string := 'insert into temp_pack_tmpl '
                   || '     ( pack_tmpl_id, '
                   || '       diff1, '
                   || '       diff1_seq, '
                   || '       diff2, '
                   || '       diff2_seq, '
                   || '       diff3, '
                   || '       diff3_seq, '
                   || '       diff4, '
                   || '       diff4_seq, '
                   || '       qty, '
                   || '       built_ind, '
                   || '       selected_ind ) '
                   || L_select
                   || L_from
                   || L_where;
   if I_rec.apply_to = 'ONE' and I_rec.row_id is NOT NULL then
      EXECUTE IMMEDIATE L_insert_string using I_rec.row_id,I_rec.diff_range;
   else
      EXECUTE IMMEDIATE L_insert_string using I_rec.diff_range;
   end if;
   if I_rec.apply_to = 'ALL' then
      delete from temp_pack_tmpl
       where delete_ind = 'Y';
   else
      delete from temp_pack_tmpl
       where rowid = I_rec.row_id;
   end if;

   ---
   return TRUE;

EXCEPTION                                                                                                               
   when OTHERS then 
      if O_error_message is NULL then                                                                                                     
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      end if;                                 
      return FALSE;                                                                                                     
END APPLY_RANGE;
------------------------------------------------------------------------------------
FUNCTION APPLY_DIFF(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_where_clause      IN      VARCHAR2,
                    I_rec               IN      INPUT_RECTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(40) := 'PACK_TEMPLATE_SQL.APPLY_DIFF';

   L_statement     VARCHAR2(6000);
   L_where_clause  VARCHAR2(5000);
   L_built_ind         VARCHAR2(1)  :='N';
   L_selected_ind      VARCHAR2(1)  :='N';
   L_delete_ind        VARCHAR2(1)  :=NULL;

BEGIN

   --- Set the local where clause
   L_where_clause := I_where_clause;
   ---
   if L_where_clause is not NULL then
      L_where_clause := 'where ' || L_where_clause;
   end if;

   if I_rec.apply_to = 'ALL' then
      L_statement := 'update temp_pack_tmpl '
                  ||    'set pack_tmpl_id = nvl(:1,pack_tmpl_id)' 
                  ||        ',diff1        = nvl(:2,diff1)' 
                  ||        ',diff1_seq    = nvl(:3,diff1_seq)' 
                  ||        ',diff2        = nvl(:4,diff2)' 
                  ||        ',diff2_seq    = nvl(:5,diff2_seq)' 
                  ||        ',diff3        = nvl(:6,diff3)'
                  ||        ',diff3_seq    = nvl(:7,diff3_seq)'
                  ||        ',diff4        = nvl(:8,diff4)'
                  ||        ',diff4_seq    = nvl(:9,diff4_seq)'
                  ||        ',qty          = nvl(:10,qty)'
                  ||        L_where_clause;          
      EXECUTE IMMEDIATE L_statement using I_rec.pack_tmpl_id, I_rec.diff1, I_rec.diff1_seq,I_rec.diff2,I_rec.diff2_seq,
                        I_rec.diff3,I_rec.diff3_seq,I_rec.diff4,I_rec.diff4_seq,I_rec.qty;
  elsif I_rec.apply_to = 'MAS' then
          L_statement := 'update temp_pack_tmpl '
                  ||    'set pack_tmpl_id  = nvl(:1,pack_tmpl_id)'
                  ||        ',diff1        = nvl(:2,diff1)'
                  ||        ',diff1_seq    = nvl(:3,diff1_seq)'
                  ||        ',diff2        = nvl(:4,diff2)'
                  ||        ',diff2_seq    = nvl(:5,diff2_seq)'
                  ||        ',diff3        = nvl(:6,diff3)'
                  ||        ',diff3_seq    = nvl(:7,diff3_seq)'
                  ||        ',diff4        = nvl(:8,diff4)'
                  ||        ',diff4_seq    = nvl(:9,diff4_seq)'
                  ||        ',qty          = nvl(:10,qty)'
                  ||        '  where nvl(diff1, ''-999'')    = nvl(:11,nvl(diff1,''-999''))'
                  ||        '   and nvl(diff2, ''-999'')    = nvl(:12,nvl(diff2,''-999''))'
                  ||        '   and nvl(diff3, ''-999'')    = nvl(:13,nvl(diff3,''-999''))'
                  ||        '   and nvl(diff4, ''-999'')    = nvl(:14,nvl(diff4,''-999''))'
                  ||        '   and nvl(pack_tmpl_id, -999) = nvl(:15,nvl(pack_tmpl_id,-999))';
        
        dbms_output.put_line(L_statement);
        EXECUTE IMMEDIATE L_statement using I_rec.pack_tmpl_id, I_rec.diff1, I_rec.diff1_seq,I_rec.diff2,I_rec.diff2_seq,
                        I_rec.diff3,I_rec.diff3_seq,I_rec.diff4,I_rec.diff4_seq,I_rec.qty,I_rec.diff1,I_rec.diff2,I_rec.diff3,I_rec.diff4,I_rec.pack_tmpl_id;
   else -- I_rec.apply_to = 'ONE'
      if I_rec.insert_or_update = 'INSERT' then
         L_statement :='insert into temp_pack_tmpl values ( :1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13)';
         EXECUTE IMMEDIATE L_statement using I_rec.pack_tmpl_id, I_rec.diff1, I_rec.diff1_seq,I_rec.diff2,I_rec.diff2_seq,
                        I_rec.diff3,I_rec.diff3_seq,I_rec.diff4,I_rec.diff4_seq,I_rec.qty,L_built_ind,L_selected_ind,L_delete_ind;

      else -- I_rec.insert_or_update = 'UPDATE'

         L_statement := 'update temp_pack_tmpl '
                     ||    'set pack_tmpl_id  = :1'
                     ||        ',diff1        = :2'
                     ||        ',diff1_seq    = :3'
                     ||        ',diff2        = :4'
                     ||        ',diff2_seq    = :5'
                     ||        ',diff3        = :6'
                     ||        ',diff3_seq    = :7'
                     ||        ',diff4        = :8'
                     ||        ',diff4_seq    = :9'
                     ||        ',qty          = :10'
                     ||        ' where rowid  = chartorowid(:11)';

         EXECUTE IMMEDIATE L_statement using I_rec.pack_tmpl_id, I_rec.diff1, I_rec.diff1_seq,I_rec.diff2,I_rec.diff2_seq,
                        I_rec.diff3,I_rec.diff3_seq,I_rec.diff4,I_rec.diff4_seq,I_rec.qty,I_rec.row_id;

      end if;  --- 'INSERT' or 'UPDATE'
   end if;  --- I_rec.apply_to
   return TRUE;
   
EXCEPTION
   when OTHERS then 
      if O_error_message is NULL then
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      end if;                                 
      return FALSE;
END APPLY_DIFF;
------------------------------------------------------------------------------------
FUNCTION CHECK_OVERLAP(O_error_message      IN OUT  VARCHAR2,
                       O_overlap            IN OUT  BOOLEAN,
                       I_pack_tmpl_id_used  IN      VARCHAR2,
                       I_diff1_used         IN      VARCHAR2,
                       I_diff2_used         IN      VARCHAR2,
                       I_diff3_used         IN      VARCHAR2,
                       I_diff4_used         IN      VARCHAR2,
                       I_where_clause       IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(40) := 'PACK_TEMPLATE_SQL.CHECK_OVERLAP';

   L_overlap          VARCHAR2(1) := 'N';
   L_statement        VARCHAR2(6000);
   L_where_clause     VARCHAR2(5000);

   type REF_CURSOR is REF CURSOR;
   C_REF_CURSOR       REF_CURSOR;

BEGIN

   --- Set the local where clause
   L_where_clause := I_where_clause;
   ---
   if L_where_clause is not NULL then
      L_where_clause := ' and ' || L_where_clause;
   end if;

   L_statement := 'select ''Y'' ' 
               ||   'from temp_pack_tmpl '
               ||  'where (   ( :l_diff1_used = ''Y'' and diff1 is NOT NULL) '
               ||         'or ( :l_diff2_used = ''Y'' and diff2 is NOT NULL) '
               ||         'or ( :l_diff3_used = ''Y'' and diff3 is NOT NULL) '
               ||         'or ( :l_diff4_used = ''Y'' and diff4 is NOT NULL) '
               ||         'or ( :l_pack_tmpl_id_used = ''Y'' and pack_tmpl_id is NOT NULL)) '
               ||         L_where_clause;

   EXECUTE IMMEDIATE L_statement using I_diff1_used,I_diff2_used,I_diff3_used,I_diff4_used,I_pack_tmpl_id_used;
   open C_REF_CURSOR for L_statement using I_diff1_used,I_diff2_used,I_diff3_used,I_diff4_used,I_pack_tmpl_id_used;
   fetch C_REF_CURSOR into L_overlap;
   close C_REF_CURSOR;
   ---
   if L_overlap = 'Y' then
      O_overlap := TRUE;
   else
      O_overlap := FALSE;
   end if;

   return TRUE;

EXCEPTION                                                                                                               
   when OTHERS then 
      if O_error_message is NULL then                                                                                                     
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      end if;                                 
      return FALSE;                                                                                                     
END CHECK_OVERLAP;
------------------------------------------------------------------------------------
FUNCTION CHECK_DUPS(O_error_message  IN OUT  VARCHAR2,
                    O_dups_exist     IN OUT  BOOLEAN,
                    I_diff_count     IN      NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(40)    := 'PACK_TEMPLATE_SQL.CHECK_DUPS';

   L_where      VARCHAR2(200)  := 'where pack_tmpl_id is NOT NULL and diff1 is NOT NULL ';
   L_group_by   VARCHAR2(200)   := 'group by pack_tmpl_id, diff1';
   L_statement  VARCHAR2(1000);
   L_dups       VARCHAR2(1)    := 'N';

   type REF_CURSOR is REF CURSOR;
   C_REF_CURSOR       REF_CURSOR;


BEGIN

   --- The cursor we use to check for duplicate records depends on the number of diffs
   --- being used on the form.  To find duplicate records we group by the pack_tmpl_id
   --- and the diffs, then count these records.  If there are more than 1 of any record
   --- then we have a duplicate.

   --- Set the 'group by' based on the number of diffs on the form
   if I_diff_count > 1 then
      L_where    := L_where    || 'and diff2 is NOT NULL ';
      L_group_by := L_group_by || ', diff2';
   end if;
   if I_diff_count > 2 then
      L_where    := L_where    || 'and diff3 is NOT NULL ';
      L_group_by := L_group_by || ', diff3';
   end if;
   if I_diff_count > 3 then
      L_where    := L_where    || 'and diff4 is NOT NULL ';
      L_group_by := L_group_by || ', diff4';
   end if;

   --- Select any records that have count > 1
   L_statement := 'select ''Y'' '
               ||   'from temp_pack_tmpl '
               ||         L_where
               || 'having count(*) > 1 '
               ||         L_group_by;

   EXECUTE IMMEDIATE L_statement;
   open C_REF_CURSOR for L_statement;
   fetch C_REF_CURSOR into L_dups;
   close C_REF_CURSOR;

   if L_dups = 'Y' then
      O_dups_exist := TRUE;
   else
      O_dups_exist := FALSE;
   end if;

   return TRUE;

EXCEPTION                                                                                                               
   when OTHERS then 
      if O_error_message is NULL then                                                                                                     
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      end if;                                 
      return FALSE;                                                                                                     
END CHECK_DUPS;
------------------------------------------------------------------------------------
FUNCTION COUNT_RECS(O_error_message  IN OUT  VARCHAR2,
                   O_rec_count      IN OUT  NUMBER,
                    I_pack_tmpl_id   IN OUT  TEMP_PACK_TMPL.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(40)    := 'PACK_TEMPLATE_SQL.COUNT_RECS';

   L_rec_count  NUMBER(10);

   cursor C_COUNT_RECS is
      select count(*)
        from temp_pack_tmpl
       where pack_tmpl_id = I_pack_tmpl_id;

BEGIN

   open C_COUNT_RECS;
   fetch C_COUNT_RECS into L_rec_count;
   close C_COUNT_RECS;

   O_rec_count := NVL(L_rec_count,0);

   return TRUE;

EXCEPTION                                                                                                               
   when OTHERS then 
      if O_error_message is NULL then                                                                                                     
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      end if;                                 
      return FALSE;                                                                                                     
END COUNT_RECS;
------------------------------------------------------------------------------------
FUNCTION PACK_TMPL_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,                                  
                       O_pack_tmpl_qty   IN OUT   PACK_TMPL_DETAIL.QTY%TYPE,                                 
                       I_pack_no         IN       PACKITEM.PACK_NO%TYPE) 

RETURN BOOLEAN IS

L_program   VARCHAR2(50)  := 'PACK_TEMPLATE_SQL.PACK_TMPL_QTY';                                           

   cursor C_PACK_TMPL_QTY is                                                                                 
      select SUM(ptd.qty)                                                                                    
        from pack_tmpl_detail ptd,                                                                           
             packitem pi                                                                                     
       where pi.pack_tmpl_id = ptd.pack_tmpl_id                                                              
         and pi.pack_no = I_pack_no;                                                                         
BEGIN                                                                                                        
   ---                                                                                                       
   if I_pack_no is NULL then                                                                                 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',                                        
                                             L_program,                                                       
                                            'I_pack_no',                                                     
                                             NULL);                                                           
      return FALSE;                                                                                          
   end if;                                                                                                   
   --                                                                                                        
   SQL_LIB.SET_MARK('OPEN', 'C_PACK_TMPL_QTY', 'PACK_TMPL_DETAIL PTD, PACKITEM', 'Pack_no: '||I_pack_no);
   open C_PACK_TMPL_QTY; 
   SQL_LIB.SET_MARK('FETCH', 'C_PACK_TMPL_QTY', 'PACK_TMPL_DETAIL PTD, PACKITEM', 'Pack_no: '||I_pack_no);
   fetch C_PACK_TMPL_QTY into O_pack_tmpl_qty;
   SQL_LIB.SET_MARK('CLOSE', 'C_PACK_TMPL_QTY', 'PACK_TMPL_DETAIL PTD, PACKITEM', 'Pack_no: '||I_pack_no);
   close C_PACK_TMPL_QTY;
   --                                                                                                        
   return TRUE;                                                                                              
   ---                                                                                                       
EXCEPTION                                                                                                    
   when OTHERS then                                                                                          
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                 
                                             SQLERRM,                                                            
                                             L_program,                                                       
                                             TO_CHAR(SQLCODE));                                                           
      return FALSE;                                                                                          
END PACK_TMPL_QTY; 
---------------------------------------------------------------------------------------------------------
FUNCTION APPLY_RANGE_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_where_clause    IN       VARCHAR2,
                         I_rec             IN       WRP_RANGE_REC)
RETURN INTEGER IS

   L_program_name   VARCHAR2(40) := 'PACK_TEMPLATE_SQL.APPLY_RANGE_WRP';
   L_range_rec  PACK_TEMPLATE_SQL.INPUT_RECTYPE;

BEGIN
--copy the data in database type I_rec to PLSQL type L_range_rec
    L_range_rec.parent_diff1        :=   I_rec.parent_diff1;
    L_range_rec.parent_diff2        :=   I_rec.parent_diff2;
    L_range_rec.parent_diff3        :=   I_rec.parent_diff3;
    L_range_rec.parent_diff4        :=   I_rec.parent_diff4;
    L_range_rec.pack_tmpl_id        :=   I_rec.pack_tmpl_id;
    L_range_rec.diff1               :=   I_rec.diff1;
    L_range_rec.diff1_seq           :=   I_rec.diff1_seq;
    L_range_rec.diff2               :=   I_rec.diff2;
    L_range_rec.diff2_seq           :=   I_rec.diff2_seq;
    L_range_rec.diff3               :=   I_rec.diff3;
    L_range_rec.diff3_seq           :=   I_rec.diff3_seq;
    L_range_rec.diff4               :=   I_rec.diff4;
    L_range_rec.diff4_seq           :=   I_rec.diff4_seq;
    L_range_rec.diff_range          :=   I_rec.diff_range;
    L_range_rec.diff_range_group1   :=   I_rec.diff_range_group1;
    L_range_rec.diff_range_group2   :=   I_rec.diff_range_group2; 
    L_range_rec.diff_range_group3   :=   I_rec.diff_range_group3;
    L_range_rec.qty                 :=   I_rec.qty;
    L_range_rec.row_id              :=   I_rec.row_id ;
    L_range_rec.apply_to            :=   I_rec.apply_to; 
    L_range_rec.insert_or_update    :=   I_rec.insert_or_update; 

   if APPLY_RANGE(O_error_message,
                  I_where_clause,
                  L_range_rec) = FALSE then
      return 0;
   end if;
   return 1;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
   return 0;
END APPLY_RANGE_WRP;
----------------------------------------------------------------------------------------------
FUNCTION APPLY_DIFF_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_where_clause    IN       VARCHAR2,
                        I_rec             IN       WRP_DIFF_REC)
RETURN INTEGER IS

   L_program_name   VARCHAR2(40) := 'PACK_TEMPLATE_SQL.APPLY_DIFF_WRP';
   L_diff_rec  PACK_TEMPLATE_SQL.INPUT_RECTYPE;

BEGIN
--copy the data in database type I_rec to PLSQL type L_diff_rec
    L_diff_rec.parent_diff1        :=   I_rec.parent_diff1;
    L_diff_rec.parent_diff2        :=   I_rec.parent_diff2;
    L_diff_rec.parent_diff3        :=   I_rec.parent_diff3;
    L_diff_rec.parent_diff4        :=   I_rec.parent_diff4;
    L_diff_rec.pack_tmpl_id        :=   I_rec.pack_tmpl_id;
    L_diff_rec.diff1               :=   I_rec.diff1;
    L_diff_rec.diff1_seq           :=   I_rec.diff1_seq;
    L_diff_rec.diff2               :=   I_rec.diff2;
    L_diff_rec.diff2_seq           :=   I_rec.diff2_seq;
    L_diff_rec.diff3               :=   I_rec.diff3;
    L_diff_rec.diff3_seq           :=   I_rec.diff3_seq;
    L_diff_rec.diff4               :=   I_rec.diff4;
    L_diff_rec.diff4_seq           :=   I_rec.diff4_seq;
    L_diff_rec.diff_range          :=   I_rec.diff_range;
    L_diff_rec.diff_range_group1   :=   I_rec.diff_range_group1;
    L_diff_rec.diff_range_group2   :=   I_rec.diff_range_group2;
    L_diff_rec.diff_range_group3   :=   I_rec.diff_range_group3;
    L_diff_rec.qty                 :=   I_rec.qty;
    L_diff_rec.row_id              :=   I_rec.row_id ;
    L_diff_rec.apply_to            :=   I_rec.apply_to;
    L_diff_rec.insert_or_update    :=   I_rec.insert_or_update;

   if APPLY_DIFF(O_error_message,
                 I_where_clause,
                 L_diff_rec) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
   return 0;
END APPLY_DIFF_WRP;
---------------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_INVALID_DIFFS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE)
RETURN BOOLEAN IS

   L_program_name   VARCHAR2(40) := 'PACK_TEMPLATE_SQL.DELETE_INVALID_DIFFS';
   
BEGIN

   if I_item_parent is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_parent',
                                             L_program_name,
                                             NULL);
      return FALSE;
   end if;
 
   -- locking is not required because the temp_pack_tmpl table will only contain data for the current session.
   delete from temp_pack_tmpl tpt
    where not exists (select 1
                        from item_master im
                       where item_parent = I_item_parent
                         and im.diff_1 = tpt.diff1
                         and NVL(im.diff_2, '-1') = NVL(tpt.diff2, '-1')
                         and NVL(im.diff_3, '-1') = NVL(tpt.diff3, '-1')
                         and NVL(im.diff_4, '-1') = NVL(tpt.diff4, '-1')
                         and rownum = 1);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            to_char(SQLCODE));
   return FALSE;

END DELETE_INVALID_DIFFS;
--------------------------------------------------------------------------------------------------------------------- 
FUNCTION DELETE_PACK_TMPL_HEAD_TL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_pack_tmpl_id        IN   PACK_TMPL_HEAD_TL.PACK_TMPL_ID%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'PACK_TEMPLATE_SQL.DELETE_PACK_TMPL_HEAD_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PACK_TMPL_HEAD_TL is
      select 'x'
        from pack_tmpl_head_tl
       where pack_tmpl_id = I_pack_tmpl_id
         for update nowait;
BEGIN

   if I_pack_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_tmpl_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'PACK_TMPL_HEAD_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_PACK_TMPL_HEAD_TL',
                    L_table,
                    'I_pack_tmpl_id '||I_pack_tmpl_id);
   open C_LOCK_PACK_TMPL_HEAD_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_PACK_TMPL_HEAD_TL',
                    L_table,
                    'I_pack_tmpl_id '||I_pack_tmpl_id);
   close C_LOCK_PACK_TMPL_HEAD_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'I_pack_tmpl_id '||I_pack_tmpl_id);
   delete from pack_tmpl_head_tl
         where pack_tmpl_id = I_pack_tmpl_id;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_PACK_TMPL_HEAD_TL;
----------------------------------------------------------------------------------------------------------
END PACK_TEMPLATE_SQL;
/
