
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORGANIZATION_VALIDATE_SQL AUTHID CURRENT_USER AS
 
----------------------------------------------------------------
-- Function Name: EXIST 
-- Purpose      :
-- Input Values : I_org_hier_type    --an IN parameter
--              : I_org_hier_value   --an IN parameter
--              : O_exists           --an IN OUT parameter
-- Created      : 09-Oct-96 Carrie Kavanaugh
----------------------------------------------------------------
FUNCTION EXIST (O_error_message      IN OUT  VARCHAR2,
                I_org_hier_type      IN      NUMBER,
                I_org_hier_value     IN      NUMBER,
                O_exists             IN OUT  BOOLEAN)
        RETURN BOOLEAN;

FUNCTION EXIST (O_error_message      IN OUT  VARCHAR2,
                I_org_hier_type      IN      NUMBER,
                I_org_hier_value     IN      NUMBER,
                O_exists             IN OUT  BOOLEAN,
                I_message_type       IN      VARCHAR2)
        RETURN BOOLEAN;

END ORGANIZATION_VALIDATE_SQL;
/
