CREATE OR REPLACE PACKAGE BODY ITEMLIST_ADD_SQL AS
----------------------------------------------------------------
-- Name:    SKULIST_ADD
-- Purpose: Inserts all styles or SKUs found given the specified criteria
--          counts and the number of styles or SKUs in the SKU list.
----------------------------------------------------------------
FUNCTION SKULIST_ADD(I_itemlist         IN     SKULIST_HEAD.SKULIST%TYPE,
                     I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                     I_item             IN     ITEM_MASTER.ITEM%TYPE,
                     I_item_parent      IN     SKULIST_CRITERIA.ITEM_PARENT%TYPE,
                     I_item_grandparent IN     SKULIST_CRITERIA.ITEM_GRANDPARENT%TYPE,
                     I_dept             IN     SKULIST_CRITERIA.DEPT%TYPE,
                     I_class            IN     SKULIST_CRITERIA.CLASS%TYPE,
                     I_subclass         IN     SKULIST_CRITERIA.SUBCLASS%TYPE,
                     I_supplier         IN     SKULIST_CRITERIA.SUPPLIER%TYPE,                                                            
                     I_diff_1           IN     SKULIST_CRITERIA.DIFF_1%TYPE,
                     I_diff_2           IN     SKULIST_CRITERIA.DIFF_2%TYPE,
                     I_diff_3           IN     SKULIST_CRITERIA.DIFF_3%TYPE,
                     I_diff_4           IN     SKULIST_CRITERIA.DIFF_4%TYPE,
                     I_uda_id           IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                     I_uda_value        IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                     I_uda_max_date     IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                     I_uda_min_date     IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                     I_season_id        IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                     I_phase_id         IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                     I_count_ind        IN     VARCHAR2,
                     I_no_add           IN     VARCHAR2,
                     I_item_level       IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                     O_no_items         IN OUT NUMBER,
                     O_error_message    IN OUT VARCHAR2)
   RETURN BOOLEAN is

   L_program               VARCHAR2(64) := 'ITEMLIST_ADD_SQL.SKULIST_ADD';
   L_item                  SKULIST_DETAIL.ITEM%TYPE;
   L_item_exist_tpglist    VARCHAR2(1);
   L_new_itemlist          SKULIST_HEAD.SKULIST%TYPE;
   L_no_add_ind            SKULIST_HEAD.STATIC_IND%TYPE;
   L_username              USER_ROLE_PRIVS.USERNAME%TYPE := GET_USER;
   L_vdate                 PERIOD.VDATE%TYPE := GET_VDATE;
   L_exists                VARCHAR2(1) := 'Y';
   L_dummy                 SKULIST_HEAD.SKULIST%TYPE;
   L_select                VARCHAR2(50);
   L_from_clause           VARCHAR2(200);
   L_between               VARCHAR2(50);
   L_exist_1               VARCHAR2(3000);
   L_exist_2               VARCHAR2(3000);
   L_dummy_where           VARCHAR2(3000);
   L_where_clause          VARCHAR2(3000);
   L_statement             VARCHAR2(3250);

   TYPE TYP_item is TABLE of ITEM_MASTER.ITEM%TYPE ;
   TBL_item   TYP_item := TYP_item();

   L_data_level_security_ind  SYSTEM_OPTIONS.DATA_LEVEL_SECURITY_IND%TYPE;

   cursor C_DATA_LEVEL_SECURITY is
      SELECT data_level_security_ind
        FROM system_options;

   cursor C_SKULIST_DETAIL_EXISTS is
      select 'x'
       from skulist_detail
      where skulist = I_itemlist
        and item = L_item;

BEGIN
   if I_itemlist is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_itemlist',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   L_new_itemlist := I_itemlist;
   L_no_add_ind   := I_no_add;
   ---
   ---
   L_select := 'select ima.item ';
   L_from_clause := 'from item_master ima';
   L_where_clause := ' where ima.item_level <= ima.tran_level and ima.item not in (select dp.key_value from daily_purge dp where table_name = ''ITEM_MASTER'')';
   
   open C_DATA_LEVEL_SECURITY;
   fetch C_DATA_LEVEL_SECURITY INTO L_data_level_security_ind;
   close C_DATA_LEVEL_SECURITY;

   if L_data_level_security_ind = 'Y' then
      L_from_clause := L_from_clause || ', (select distinct dept, class, subclass from skulist_dept where skulist =
                                            TO_CHAR(:bind_L_new_itemlist)  '||  ') sld';
      L_where_clause := L_where_clause || ' and ima.dept = sld.dept and ima.class = sld.class and ima.subclass = sld.subclass';
   end if;
   if I_item_parent is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '((ima.item = ''' || I_item_parent || ''') or ' ||
                        '(ima.item_parent = '''||I_item_parent||''')) ';
   end if;

   if I_item_grandparent is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '((ima.item = '''||I_item_grandparent||''') or ' ||
                        '(ima.item_parent = '''||I_item_grandparent||''') or ' ||
                        '(ima.item_grandparent = '''||I_item_grandparent||''')) ';
   end if;

   if I_item is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = '''||I_item||'''';
   end if;

   if I_dept is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.dept = '||I_dept;
   end if;

   if I_class is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.class = '||I_class;
   end if;

   if I_subclass is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.subclass = '||I_subclass;
   end if;

   if I_diff_1 is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_1   = ''' ||I_diff_1|| ''' or ' ||
                        '(ima.diff_2   = ''' ||I_diff_1|| ''') or ' ||
                        '(ima.diff_3   = ''' ||I_diff_1|| ''') or ' || 
                        '(ima.diff_4   = ''' ||I_diff_1|| '''))'; 

      if I_diff_2 is NOT NULL then
         L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_2   = ''' ||I_diff_2|| ''' or ' ||
                        '(ima.diff_3   = ''' ||I_diff_2|| ''') or ' ||
                        '(ima.diff_4   = ''' ||I_diff_2|| ''') or ' || 
                        '(ima.diff_1   = ''' ||I_diff_2|| '''))'; 

         if I_diff_3 is NOT NULL then
            L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_3   = ''' ||I_diff_3|| ''' or ' ||
                        '(ima.diff_4   = ''' ||I_diff_3|| ''') or ' ||
                        '(ima.diff_1   = ''' ||I_diff_3|| ''') or ' ||
                        '(ima.diff_2   = ''' ||I_diff_3|| '''))'; 

            if I_diff_4 is NOT NULL then
               L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_4   = ''' ||I_diff_4|| ''' or ' ||
                        '(ima.diff_1   = ''' ||I_diff_4|| ''') or ' ||
                        '(ima.diff_2   = ''' ||I_diff_4|| ''') or ' ||
                        '(ima.diff_3   = ''' ||I_diff_4|| '''))'; 
            end if;
         end if;
      end if;
   end if;

   if I_pack_ind is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.pack_ind = '''||I_pack_ind||'''';
   end if;

   if I_item_level is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item_level = '||I_item_level;
   end if;

   if I_supplier is NOT NULL then
      L_from_clause := L_from_clause ||
                       ', item_supplier isu';
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = isu.item and ' ||
                        'isu.supplier = '||I_supplier;
   end if;

   if I_season_id is NOT NULL then
      L_from_clause := L_from_clause ||
                       ', item_seasons ise';
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = ise.item and ' ||
                        'ise.season_id = '||I_season_id||' and ' ||
                        'ise.phase_id = nvl('''||I_phase_id||''', ise.phase_id)';
   end if;

   if I_uda_id is NOT NULL then
      L_dummy_where := L_where_clause;
      
      L_exist_1 := '(exists (select 1' ||
                             ' from uda_item_lov uil' ||
                            ' where ima.item = uil.item and '||
                                   'uil.uda_id = '||I_uda_id ||
                                         ' and rownum <= 1 ';
      L_exist_2 :=  'exists (select 1'||
                             ' from uda_item_date ud'||
                            ' where ima.item = ud.item and '||
                                   'ud.uda_id = '||I_uda_id ||
                                         ' and rownum <= 1 ';
      L_between := ') or ';

      L_where_clause := L_where_clause || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;   

   if I_uda_value is NOT NULL then
      L_exist_1 := L_exist_1 || ' and ' || ' uil.uda_value = '||I_uda_value;
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;

   if I_uda_min_date is NOT NULL then
      L_exist_2 := L_exist_2 || ' and ' || ' ud.uda_date >= '''||I_uda_min_date||'''';
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
      end if;

   if I_uda_max_date is NOT NULL then
      L_exist_2 := L_exist_2 || ' and ' || ' ud.uda_date <= '''||I_uda_max_date||'''';
      L_where_clause := L_dummy_where || ' and ' || L_exist_1 ||
                        L_between || L_exist_2 || '))';
   end if;
   L_statement := L_select || L_from_clause || L_where_clause;

   if L_data_level_security_ind = 'Y' then
      EXECUTE IMMEDIATE L_statement bulk collect into TBL_item using L_new_itemlist;
   else
      EXECUTE IMMEDIATE L_statement bulk collect into TBL_item;
   end if;   

   if TBL_item is NOT NULL and TBL_item.count > 0 then
      FOR i in TBL_item.FIRST..TBL_item.LAST LOOP
         L_item := TBL_item(i);

         open C_SKULIST_DETAIL_EXISTS;
         fetch C_SKULIST_DETAIL_EXISTS into L_exists;
         close C_SKULIST_DETAIL_EXISTS;
         if L_exists != 'x' then
            if ITEMLIST_ADD_SQL.INSERT_SKULIST_DETAIL(O_error_message,
                                                      I_itemlist,
                                                      L_item,
                                                      I_pack_ind) = FALSE then
               return FALSE;
            end if;
         end if;

         L_exists := 'Y';
      ---
      END LOOP;
   end if; 

   if I_count_ind = 'Y' then
      if ITEMLIST_ATTRIB_SQL.GET_ITEM_COUNT(O_error_message,
                                            I_itemlist,
                                            O_no_items) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      RETURN FALSE;   
END SKULIST_ADD;	
----------------------------------------------------------------
FUNCTION ITEM_ON_ITEMLIST(O_error_message   IN OUT  VARCHAR2,
                          O_exists          IN OUT  VARCHAR2,
                          I_item            IN      SKULIST_DETAIL.ITEM%TYPE,
                          I_skulist         IN      SKULIST_DETAIL.SKULIST%TYPE)
return BOOLEAN is

   L_dummy   VARCHAR2(1);
   L_program VARCHAR2(64) := 'ITEMLIST_ADD_SQL.ITEM_ON_ITEMLIST';

   cursor C_ITEM_EXISTS is
      select 'x'
        from skulist_detail
       where item = I_item
         and skulist = I_skulist;
BEGIN
   open C_ITEM_EXISTS;
   fetch C_ITEM_EXISTS into L_dummy;
   if C_ITEM_EXISTS%FOUND then
      O_exists := 'Y';
   else
      O_exists := 'N';
   end if;
   close C_ITEM_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END ITEM_ON_ITEMLIST;
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SKULIST_DETAIL(O_error_message  IN OUT VARCHAR2,
                               I_itemlist       IN     SKULIST_HEAD.SKULIST%TYPE,
                               I_item           IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_ind       IN     ITEM_MASTER.PACK_IND%TYPE)
return BOOLEAN is

   L_item_level    ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level    ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_username      USER_ROLE_PRIVS.USERNAME%TYPE := GET_USER;
   L_vdate         PERIOD.VDATE%TYPE := GET_VDATE;
   L_program       VARCHAR2(60) := 'ITEMLIST_ADD_SQL.INSERT_SKULIST_DETAIL';
   L_pack_ind      ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind  ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type     ITEM_MASTER.PACK_TYPE%TYPE;

BEGIN
   if I_pack_ind is NULL then
      ---
      if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                       L_pack_ind,
                                       L_sellable_ind,
                                       L_orderable_ind,
                                       L_pack_type,
                                       I_item) = FALSE then
         return FALSE;
      end if;
      ---
   else
      L_pack_ind := I_pack_ind;
   end if;
   ---                                 
   if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                 L_item_level,
                                 L_tran_level,
                                 I_item) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'SKULIST_DETAIL','ITEMLIST: '||to_char(I_itemlist)||' ITEM: '||(I_item)||' PACK_IND: '||I_pack_ind);
   insert into skulist_detail(skulist,
                              item,
                              item_level,
                              tran_level,
                              pack_ind,
                              insert_id,
                              insert_date,
                              create_datetime,
                              last_update_datetime,
                              last_update_id)
                       values(I_itemlist,
                              I_item,
                              L_item_level,
                              L_tran_level,
                              L_pack_ind,
                              L_username,
                              L_vdate,
                              SYSDATE,
                              SYSDATE,
                              L_username);

   if SIT_SQL.INSERT_ITEM(O_error_message,
                          I_itemlist,
                          I_item) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SKULIST_DETAIL;
---------------------------------------------------------------------------------------------------------
END;
/
