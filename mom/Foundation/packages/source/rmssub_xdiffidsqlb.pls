
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XDIFFID_SQL AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_DIFFID
   -- Purpose      :  Inserts a record in the diff_ids table.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DIFFID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec      IN       DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_DIFFID
   -- Purpose      :  Updates a record in the diff_ids table.
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIFFID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec      IN       DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_DIFFID
   -- Purpose      :  Deletes a record on the diff_ids table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIFFID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec      IN       DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_diffid_rec      IN       DIFF_IDS%ROWTYPE,
                 I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFID_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XDIFFID.LP_cre_type then
      if CREATE_DIFFID(O_error_message,
                       I_diffid_rec) = FALSE then
         return FALSE;
      end if;

   elsif I_message_type = RMSSUB_XDIFFID.LP_mod_type then
      if MODIFY_DIFFID(O_error_message,
                       I_diffid_rec) = FALSE then
         return FALSE;
      end if;

   elsif I_message_type = RMSSUB_XDIFFID.LP_del_type then
      if DELETE_DIFFID(O_error_message,
                       I_diffid_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
  -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DIFFID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec      IN       DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFID_SQL.CREATE_DIFFID';

BEGIN

   if not DIFF_ID_SQL.INSERT_DIFFID(O_error_message,
                                    I_diffid_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_DIFFID;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIFFID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec      IN       DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFID_SQL.UPDATE_DIFFID';

BEGIN

   if not DIFF_ID_SQL.UPDATE_DIFFID(O_error_message,
                                    I_diffid_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_DIFFID;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIFFID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid_rec      IN       DIFF_IDS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFID_SQL.DELETE_DIFFID';

BEGIN

   if not DIFF_ID_SQL.DELETE_DIFFID(O_error_message,
                                  I_diffid_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DIFFID;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XDIFFID_SQL;
/
