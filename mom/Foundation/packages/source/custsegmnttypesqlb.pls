CREATE OR REPLACE PACKAGE BODY CUST_SEGMENT_TYPE_SQL AS
--------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CUST_SEGMENT_TYPE_EXISTS (O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_exists                       IN OUT   BOOLEAN,
                                         O_customer_segment_type_desc   IN OUT   CUSTOMER_SEGMENT_TYPES.CUSTOMER_SEGMENT_TYPE_DESC%TYPE,
                                         I_customer_segment_type        IN       CUSTOMER_SEGMENT_TYPES.CUSTOMER_SEGMENT_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'CUST_SEGMENT_TYPE_SQL.CHECK_CUST_SEGMENT_TYPE_EXISTS';
   
   cursor C_GET_DESCRIPTION is
      select customer_segment_type_desc
        from v_customer_segment_types_tl
       where customer_segment_type = I_customer_segment_type;

BEGIN
   O_exists := TRUE;
   O_customer_segment_type_desc := NULL;
   ---
   if I_customer_segment_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_customer_segment_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DESCRIPTION',
                    'CUSTOMER_SEGMENT_TYPES',
                    I_customer_segment_type);
   open C_GET_DESCRIPTION;
   --
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DESCRIPTION',
                    'CUSTOMER_SEGMENT_TYPES',
                    I_customer_segment_type);
   fetch C_GET_DESCRIPTION into O_customer_segment_type_desc;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DESCRIPTION',
                    'CUSTOMER_SEGMENT_TYPES',
                    I_customer_segment_type);
   close C_GET_DESCRIPTION;
   ---
   if O_customer_segment_type_desc is NULL then
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CUST_SEGMENT_TYPE_EXISTS;
------------------------------------------------------------------------------------------------------------
FUNCTION CUST_SEGMENT_TYPE_EXCEP_EXISTS (O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_exists                       IN OUT   BOOLEAN,
                                         I_customer_segment_type        IN       CUSTOMER_SEGMENT_TYPES.CUSTOMER_SEGMENT_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(64) := 'CUST_SEGMENT_TYPE_SQL.CUST_SEGMENT_TYPE_EXCEP_EXISTS';
   L_record_exists  VARCHAR2(1)  := NULL;
   
   cursor C_GET_REC_EXISTS is
      select 'x'
        from customer_segments
       where customer_segment_type = I_customer_segment_type
         and rownum = 1;

BEGIN
   O_exists := FALSE;
   ---
   if I_customer_segment_type IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_customer_segment_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_REC_EXISTS',
                    'CUSTOMER_SEGMENTS',
                    I_customer_segment_type);
   open C_GET_REC_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_REC_EXISTS',
                    'CUSTOMER_SEGMENTS',
                    I_customer_segment_type);
   fetch C_GET_REC_EXISTS into L_record_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_REC_EXISTS',
                    'CUSTOMER_SEGMENTS',
                    I_customer_segment_type);
   close C_GET_REC_EXISTS;
   ---
   if L_record_exists is NOT NULL then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CUST_SEGMENT_TYPE_EXCEP_EXISTS;
------------------------------------------------------------------------------------------------------------
END CUST_SEGMENT_TYPE_SQL;
/
