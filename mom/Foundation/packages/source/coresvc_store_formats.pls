-- File Name : CORESVC_STORE_FORMAT_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_STORE_FORMAT AUTHID CURRENT_USER AS

   template_key      CONSTANT VARCHAR2(255) := 'STORE_FORMAT_DATA';
   template_category CONSTANT VARCHAR2(255) := 'RMSFND';
   action_new                 VARCHAR2(25)  := 'NEW';
   action_mod                 VARCHAR2(25)  := 'MOD';
   action_del                 VARCHAR2(25)  := 'DEL';
   STORE_FORMAT_sheet         VARCHAR2(255) := 'STORE_FORMAT';
   STORE_FORMAT$Action        NUMBER        := 1;
   STORE_FORMAT$STORE_FORMAT  NUMBER        := 2;
   STORE_FORMAT$FORMAT_NAME   NUMBER        := 3;
   
   STORE_FORMAT_TL_sheet         VARCHAR2(255) := 'STORE_FORMAT_TL';
   STORE_FORMAT_TL$Action        NUMBER        := 1;
   STORE_FORMAT_TL$LANG          NUMBER        := 2;
   STORE_FORMAT_TL$STORE_FORMAT  NUMBER        := 3;
   STORE_FORMAT_TL$FORMAT_NAME   NUMBER        := 4;

   TYPE STORE_FORMAT_rec_tab IS TABLE OF STORE_FORMAT%ROWTYPE;

   sheet_name_trans S9T_PKG.trans_map_typ;

   action_column  VARCHAR2(255) := 'ACTION';

--------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id            IN OUT  S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind  IN      CHAR DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count   IN OUT  NUMBER,
                      I_file_id       IN      S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id    IN      NUMBER )
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT  NUMBER,
                  I_process_id    IN      NUMBER,
                  I_chunk_id      IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_STORE_FORMAT;
/
