
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSMFM_BANNER AS

--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status        OUT VARCHAR2,
                 O_text          OUT VARCHAR2,
                 O_message_type  OUT VARCHAR2,
                 O_banner_id     OUT NUMBER,
                 O_channel_id    OUT NUMBER,
                 O_message       OUT CLOB);
--------------------------------------------------------------------------------

           /*** Public Program Bodies***/

--------------------------------------------------------------------------------
PROCEDURE ADDTOQ(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 I_message_type IN  BANNER_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_banner_id    IN  CHANNELS.BANNER_id%TYPE,
                 I_channel_id   IN  CHANNELS.CHANNEL_ID%TYPE,
                 I_message      IN  CLOB)
IS

   /* This procedure takes a message in CLOB format and adds it to the
      message queue table.
   */

BEGIN
   insert into banner_mfqueue(seq_no,
                            pub_status,
                            message_type,
                            banner_id,
                            channel_id,
                            message)
                     values(banner_mfsequence.NEXTVAL,
                            'U',
                            I_message_type,
                            I_banner_id,
                            I_channel_id,
                            I_message);

    O_status := API_CODES.SUCCESS;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_BANNER.ADDTOQ');

END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_STATUS_CODE   OUT VARCHAR2,
                 O_ERROR_MSG     OUT VARCHAR2, 
                 O_MESSAGE_TYPE  OUT VARCHAR2, 
                 O_MESSAGE       OUT CLOB,
                 O_banner_id     OUT NUMBER,
                 O_channel_id    OUT NUMBER)
IS

BEGIN
   GETNXT(O_STATUS_CODE,
          O_ERROR_MSG,
          O_MESSAGE_TYPE,
          O_banner_id,
          O_channel_id,
          O_MESSAGE);
          
EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_BANNER.GETNXT');

END GETNXT;
--------------------------------------------------------------------------------


      /*** Private Program Bodies ***/
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status        OUT VARCHAR2,
                 O_text          OUT VARCHAR2,
                 O_message_type  OUT VARCHAR2,
                 O_banner_id     OUT NUMBER,
                 O_channel_id    OUT NUMBER,
                 O_message       OUT CLOB)
IS

   /* This procedure fetches the row from the message queue table that has
      the lowest sequence number.  The message is retrieved, then the row
      is removed from the queue.
   */

   L_rowid    ROWID;

   cursor C_GET_MESSAGE is
      select rowid,
             message_type,
             banner_id,
             channel_id,
             message
        from BANNER_mfqueue
       where seq_no = (select min(seq_no)
                         from BANNER_mfqueue)
         for update nowait;

BEGIN
   open C_GET_MESSAGE;
   fetch C_GET_MESSAGE into L_rowid,
                            O_message_type,
                            O_banner_id,
                            O_channel_id,
                            O_message;
                            
   if C_GET_MESSAGE%NOTFOUND then
      O_status := API_CODES.NO_MSG;
      close C_GET_MESSAGE;
   else
      close C_GET_MESSAGE;

      delete from BANNER_mfqueue
       where rowid = L_rowid;

      O_status := API_CODES.SUCCESS;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_BANNER.GETNXT');

END GETNXT;
--------------------------------------------------------------------------------
END RMSMFM_BANNER;
/
