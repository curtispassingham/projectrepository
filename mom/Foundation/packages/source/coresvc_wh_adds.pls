CREATE OR REPLACE PACKAGE CORESVC_WH_ADD_SQL AUTHID CURRENT_USER AS
  ------------------------------------------------------------------------------------------------
  -- Function Name: ADD_WH
  -- Purpose      : This function contains the core logic for adding a new warehouse to RMS.
  --                The process of adding a warehouse to RMS starts with warehse.fmb form. When user
  --                creates a new wh using the form an entry is made to wh as well as wh_add table.
  --                As part of warehouse creation process, RPM tables need to be updated. So, this
  --                function calls PM_NOTIFY_API_SQL.NEW_LOCATION to create pricing records.
  --                After completion of the process, it deletes the record from wh_add table.
  ------------------------------------------------------------------------------------------------
  FUNCTION ADD_WH(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_rms_async_id  IN RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------------------------
END CORESVC_WH_ADD_SQL;
/
