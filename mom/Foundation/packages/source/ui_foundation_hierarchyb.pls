CREATE OR REPLACE PACKAGE Body FOUNDATION_HIERARCHY_SQL    IS
  ---------------------------------------------------------------------------------------------

  -- Module                   : class (Form Module)
  -- Source Object            : PROCEDURE  -> P_CHECK_UDA_cursor
  -- NEW ARGUMENTS (2)
  -- ITEM          :B_CLASS.CLASS.......................... -> I_CLASS
  -- ITEM          :B_CLASS.DEPT........................... -> I_DEPT
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_UDA_cursor (O_error_message   IN OUT   VARCHAR2,
                           I_CLASS           IN       NUMBER,
                           I_DEPT            IN       NUMBER)
    return BOOLEAN
is
  L_uda_exists   UDA_ITEM_DEFAULTS.UDA_ID%TYPE;
  L_program      VARCHAR2(64) := 'FOUNDATION_HIERARCHY_SQL.CHECK_UDA_cursor';

  cursor C_CHECK_UDA  is
     select uda_id
       from uda_item_defaults
      where dept = I_DEPT
        and class = I_CLASS;
BEGIN
  open C_CHECK_UDA;
     fetch C_CHECK_UDA into L_uda_exists;
     if C_CHECK_UDA%NOTFOUND then
       O_error_message := SQL_LIB.CREATE_MSG('NO_UDA',
                                             to_char(I_DEPT),
                                             to_char(I_CLASS),
                                             NULL);
       return FALSE;
     end if;
       return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
   return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : dept (Form Module)
-- Source Object            : FUNCTION   -> F_VAT_EXISTS
-- NEW ARGUMENTS (1)
-- ITEM          :B_DEPT.DEPT............................ -> I_DEPT
---------------------------------------------------------------------------------------------
FUNCTION VAT_EXISTS(O_error_message   IN OUT   VARCHAR2,
                    O_ret             IN OUT   BOOLEAN,
                    I_DEPT            IN       NUMBER )
   return BOOLEAN is
  L_vat        VARCHAR2(1);
  L_program    VARCHAR2(64) := 'FOUNDATION_HIERARCHY_SQL.VAT_EXISTS';
  ---
  cursor C_VAT is
    select 'x'
      from vat_deps
     where dept = I_DEPT;
BEGIN
  open C_VAT;
  fetch C_VAT into L_vat;
  if C_VAT%NOTFOUND then
     O_ret := FALSE;
  end if;
  close C_VAT;
     return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : lclstwh (Form Module)
-- Source Object            : PROCEDURE  -> P_CHECK_DUP_LOCS
-- NEW ARGUMENTS (4)
-- ITEM          :B_DETAIL.LOCATION...................... -> I_LOCATION
-- ITEM          :B_DETAIL.LOC_TYPE...................... -> I_LOC_TYPE
-- ITEM          :B_HEAD.TI_LOC_LIST..................... -> I_TI_LOC_LIST
-- Pack.Spec     INTERNAL_VARIABLES.GV_DUPLICATE......... -> P_IntrnlVrblsGvDplcte
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_LOCS(O_error_message       IN OUT   VARCHAR2,
                        I_LOCATION            IN       NUMBER ,
                        I_LOC_TYPE            IN       VARCHAR2 ,
                        I_TI_LOC_LIST         IN       NUMBER ,
                        P_IntrnlVrblsGvDplcte IN OUT   BOOLEAN )
  return BOOLEAN is
  L_program   VARCHAR2(64) := 'FOUNDATION_HIERARCHY_SQL.CHECK_DUP_LOCS';
  L_exists    VARCHAR2(1)  := 'y';
  cursor C_CHECK_DUP_RECORD IS
    select 'x'
      from loc_list_detail
     where loc_list = I_TI_LOC_LIST
       and loc_type = I_LOC_TYPE
       and location = I_LOCATION;
BEGIN
  open C_CHECK_DUP_RECORD;
  fetch C_CHECK_DUP_RECORD into L_exists;
  close C_CHECK_DUP_RECORD;
  ---
  if L_exists = 'x' then
    O_error_message := SQL_LIB.CREATE_MSG('DUP_LOC_DEL',
                                          NULL,
                                          NULL,
                                          NULL);
    P_IntrnlVrblsGvDplcte := TRUE;
    return FALSE;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : partner (Form Module)
-- Source Object            : FUNCTION   -> P_CHECK_ADDRESS
-- NEW ARGUMENTS (3)
-- ITEM          :B_PARTNER.PARTNER_ID................... -> I_PARTNER_ID
-- ITEM          :B_PARTNER.PARTNER_TYPE................. -> I_PARTNER_TYPE
-- PARAMETER     :PARAMETER.PM_MODE...................... -> P_PM_MODE
---------------------------------------------------------------------------------------------
FUNCTION CHECK_ADDRESS(O_error_message   IN OUT   VARCHAR2,
                       O_mode               OUT   VARCHAR2,
                       I_PARTNER_ID      IN       VARCHAR2,
                       I_PARTNER_TYPE    IN       VARCHAR2,
                       P_PM_MODE         IN       VARCHAR2)
  return BOOLEAN
is
  L_program VARCHAR2(64) := 'FOUNDATION_HIERARCHY_SQL.CHECK_ADDRESS';
  L_exists  VARCHAR2(1);
  cursor C_EXISTS
  is
    select 'x'
      from addr
     where module = 'PTNR'
       and key_value_1 = I_PARTNER_TYPE
       and key_value_2 = I_PARTNER_ID;
BEGIN
  open C_EXISTS;
  fetch C_EXISTS into L_exists;
  if C_EXISTS%NOTFOUND then
    O_mode := 'NEW';
  elsif P_PM_MODE = 'NEW' then
    O_mode := 'EDIT';
  else
    O_mode := P_PM_MODE;
  end if;
  close C_EXISTS;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : store (Form Module)
-- Source Object            : PROCEDURE  -> P_GET_TIMEZONE_NAME
-- NEW ARGUMENTS (3)
-- GLOBAL        :GLOBAL.USER_LANG....................... -> G_USER_LANG
-- ITEM          :B_STORE.TIMEZONE_NAME.................. -> I_TIMEZONE_NAME
-- ITEM          :B_STORE.TI_TIMEZONE_NAME............... -> I_TI_TIMEZONE_NAME
---------------------------------------------------------------------------------------------
FUNCTION GET_TIMEZONE_NAME(O_error_message      IN OUT   VARCHAR2,
                           G_USER_LANG          IN       VARCHAR2,
                           I_TIMEZONE_NAME      IN OUT   VARCHAR2,
                           I_TI_TIMEZONE_NAME   IN       VARCHAR2)
  return BOOLEAN
is
  L_program          VARCHAR2(64) := 'FOUNDATION_HIERARCHY_SQL.GET_TIMEZONE_NAME';
  L_error_message    RTK_ERRORS.RTK_TEXT%TYPE;
  L_exists           BOOLEAN;
  cursor C_TIMEZONE_NAME is
    select vtn.timezone_name
      from v_timezone_names vtn
     where vtn.timezone_name = I_TI_TIMEZONE_NAME;
BEGIN
  if I_TI_TIMEZONE_NAME is NOT NULL then
    open C_TIMEZONE_NAME;
    fetch C_TIMEZONE_NAME into I_TIMEZONE_NAME;
    if C_TIMEZONE_NAME%ROWCOUNT = 0 then
      O_error_message          := SQL_LIB.CREATE_MSG('TIMEZONE_NAME_EXIST',
                                                     NULL,
                                                     NULL,
                                                     NULL);
      close C_TIMEZONE_NAME;
      return FALSE;
    end if;
    close C_TIMEZONE_NAME;
  end if;
  if I_TIMEZONE_NAME is NOT NULL then
    if STORE_SQL.TIMEZONE_NAME_EXISTS(L_error_message,
                                      L_exists,
                                      I_TIMEZONE_NAME) = FALSE then
      return FALSE;
    end if;
    if L_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('TIMEZONE_NAME_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
    end if;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
end;
---------------------------------------------------------------------------------------------

-- Module                   : subclass (Form Module)
-- Source Object            : PROCEDURE  -> P_insert_DAILY_PURGE
-- NEW ARGUMENTS (3)
-- ITEM          :B_DEPT.CLASS........................... -> I_CLASS
-- ITEM          :B_DEPT.DEPT............................ -> I_DEPT
-- ITEM          :B_SUBCLASS.SUBCLASS.................... -> I_SUBCLASS
---------------------------------------------------------------------------------------------
FUNCTION insert_DAILY_PURGE(O_error_message   IN OUT   VARCHAR2,
                            I_CLASS           IN       NUMBER ,
                            I_DEPT            IN       NUMBER ,
                            I_SUBCLASS        IN       NUMBER )
  return BOOLEAN
is
  L_program    VARCHAR2(64) := 'FOUNDATION_HIERARCHY_SQL.insert_DAILY_PURGE';
  L_dummy      VARCHAR2(1);
  L_key_value  DAILY_PURGE.KEY_VALUE%TYPE;
  cursor C_REC_EXISTS is
    select 'x'
      from daily_purge
     where table_name = 'SUBCLASS'
       and key_value = L_key_value;
BEGIN
  -- this message informs the user that the record will be deleted in the
  -- nightly batch run.
  L_key_value := SUBSTR(to_char(I_DEPT,'0999'),2,4)||';'||SUBSTR(to_char(I_CLASS,'0999'),2,4)||';'||SUBSTR(to_char(I_SUBCLASS,'0999'),2,4);
  open C_REC_EXISTS;
  fetch C_REC_EXISTS into L_dummy;
  if C_REC_EXISTS%NOTFOUND then
    insert into daily_purge
         values(L_key_value,
                'SUBCLASS',
                'D',
                1);
  end if;
  close C_REC_EXISTS;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : subclass (Form Module)
-- Source Object            : PROCEDURE  -> P_CHECK_UDA_cursor
-- NEW ARGUMENTS (3)
-- ITEM          :B_DEPT.CLASS........................... -> I_CLASS
-- ITEM          :B_DEPT.DEPT............................ -> I_DEPT
-- ITEM          :B_SUBCLASS.SUBCLASS.................... -> I_SUBCLASS
---------------------------------------------------------------------------------------------
FUNCTION CHECK_UDA_cursor_SUBCLASS(O_error_message   IN OUT   VARCHAR2,
                                   I_CLASS           IN       NUMBER,
                                   I_DEPT            IN       NUMBER,
                                   I_SUBCLASS        IN       NUMBER)
  return BOOLEAN
is
  L_program      VARCHAR2(64) := 'FOUNDATION_HIERARCHY_SQL.CHECK_UDA_cursor_SUBCLASS';
  L_uda_exists   UDA_ITEM_DEFAULTS.UDA_ID%TYPE;
  cursor C_CHECK_UDA
  is
    select uda_id
      from uda_item_defaults
     where dept = I_DEPT
       and class = I_CLASS
       and subclass = I_SUBCLASS;
BEGIN
  open C_CHECK_UDA;
  fetch C_CHECK_UDA into L_uda_exists;
  if C_CHECK_UDA%NOTFOUND then
    O_error_message := SQL_LIB.CREATE_MSG('NO_UDA_SUBCLASS',
                                          to_char(I_DEPT),
                                          to_char(I_CLASS),
                                          to_char(I_SUBCLASS));
    return FALSE;
  end if;
  return TRUE;
end;
---------------------------------------------------------------------------------------------

-- Module                   : supvwedt (Form Module)
-- Source Object            : PROCEDURE  -> P_CONTRACTING
-- NEW ARGUMENTS (1)
-- Pack.Spec     INTERNAL_VARIABLES.GV_CONTRACT_IND...... -> P_IntrnlVrblsGvCntrctInd
---------------------------------------------------------------------------------------------
FUNCTION CONTRACTING(O_error_message            IN OUT   VARCHAR2,
                     P_IntrnlVrblsGvCntrctInd   IN OUT   VARCHAR2)
  return BOOLEAN
is
  cursor C_CONTRACT_IND
  is
    select contract_ind
      from system_options;
   L_program   VARCHAR2(64) := 'FOUNDATION_HIERARCHY_SQL.CONTRACTING';
   L_ind       VARCHAR2(1)  := NULL;
BEGIN
  open C_CONTRACT_IND;
  fetch C_CONTRACT_IND into L_ind;
  if C_CONTRACT_IND%NOTFOUND then
    O_error_message := SQL_LIB.CREATE_MSG('INV_CONTRACT_IND',
                                          NULL,
                                          NULL,
                                          NULL);
    return FALSE;
  end if;
  close C_CONTRACT_IND;
  P_IntrnlVrblsGvCntrctInd := L_ind;
  return TRUE;
EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
end;
end FOUNDATION_HIERARCHY_SQL;
/
