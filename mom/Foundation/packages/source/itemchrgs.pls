
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_CHARGE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
--Function Name:  APPLY_CHARGES
--Purpose      :  This function will insert or update item_chrg_head and
--                item_chrg_detail records.

-- note:
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_LOC_LIST_HEAD V_STORE V_WH
-- which only returns data that the user has permission to access.
-----------------------------------------------------------------------------------
FUNCTION APPLY_CHARGES(O_error_message       IN OUT   VARCHAR2,
                       I_item                IN       ITEM_MASTER.ITEM%TYPE,

                       I_item_list           IN       MC_CHRG_HEAD.ITEM_LIST%TYPE,

                       I_zone_group_id       IN       COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                       I_from_group_type     IN       CODE_DETAIL.CODE%TYPE,
                       I_from_group          IN       STORE.STORE%TYPE,
                       I_to_group_type       IN       CODE_DETAIL.CODE%TYPE,
                       I_to_group            IN       STORE.STORE%TYPE,
                       I_comp_id             IN       ELC_COMP.COMP_ID%TYPE,
                       I_up_chrg_group       IN       ITEM_CHRG_DETAIL.UP_CHRG_GROUP%TYPE,
                       I_comp_rate           IN       ELC_COMP.COMP_RATE%TYPE,
                       I_per_count           IN       ELC_COMP.PER_COUNT%TYPE,
                       I_per_count_uom       IN       ELC_COMP.PER_COUNT_UOM%TYPE,
                       I_comp_currency       IN       ELC_COMP.COMP_CURRENCY%TYPE,
                       I_display_order       IN       ELC_COMP.DISPLAY_ORDER%TYPE,
                       I_insert_update_del   IN       VARCHAR2,

                       I_maintenance_type    IN       MC_CHRG_DETAIL.MAINTENANCE_TYPE%TYPE)

   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--Function Name:  DELETE_LOCS
--Purpose      :  This function will delete the passed in item/from loc/to loc
--                combination from the item_chrg_head table after first deleting
--                all associated components from the item_chrg_detail table.
------------------------------------------------------------------------------------
FUNCTION DELETE_LOCS(O_error_message   IN OUT VARCHAR2,
                     I_item            IN     ITEM_MASTER.ITEM%TYPE,
                     I_zone_group_id   IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                     I_from_group_type IN     CODE_DETAIL.CODE%TYPE,
                     I_from_group      IN     STORE.STORE%TYPE,
                     I_to_group_type   IN     CODE_DETAIL.CODE%TYPE,
                     I_to_group        IN     STORE.STORE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
--Function Name:  DELETE_MC_LOCS
--Purpose      :  This function will delete the passed in item/from loc/to loc
--                combination from the mc_chrg_head table after first deleting
--                all associated components from the mc_chrg_detail table.
------------------------------------------------------------------------------------
FUNCTION DELETE_MC_LOCS(O_error_message   IN OUT VARCHAR2,
                        I_item_list       IN     MC_CHRG_HEAD.ITEM_LIST%TYPE,
                        I_zone_group_id   IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                        I_from_group_type IN     CODE_DETAIL.CODE%TYPE,
                        I_from_group      IN     STORE.STORE%TYPE,
                        I_to_group_type   IN     CODE_DETAIL.CODE%TYPE,
                        I_to_group        IN     STORE.STORE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CHARGES_EXIST
-- Purpose      : This function will check if up charges exist for the given
--                item/from location/to location combination.
------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                       I_from_loc      IN     ORDLOC.LOCATION%TYPE,
                       I_to_loc        IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CHARGES_EXIST
-- Purpose      : This function will check if up charges exist for the given item.
------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: DEFAULT_PARENT_CHRGS
-- Purpose      : This function will overwrite the charges of all transaction level
--                children items of the parent item passed in.
------------------------------------------------------------------------------------
FUNCTION DEFAULT_PARENT_CHRGS(O_error_message IN OUT VARCHAR2,
                              I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CHECK_HEADER_NO_DETAILS
-- Purpose      : Checks for Item Charge Header records that do not have any
--                associated Item Charge Detail records.  Sets O_exists to
--                TRUE if records found with no detail records.
------------------------------------------------------------------------------------
FUNCTION CHECK_HEADER_NO_DETAILS(O_error_message IN OUT  VARCHAR2,
                                 O_exists        IN OUT  BOOLEAN,
                                 I_item          IN      ITEM_MASTER.ITEM%TYPE,
                                 I_item_list     IN      SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN;
FUNCTION CHECK_HEADER_NO_DETAILS(O_error_message IN OUT  VARCHAR2,
                                 O_exists        IN OUT  BOOLEAN,
                                 I_item          IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: DELETE_HEADER
-- Purpose      : Deletes Item Charge Header records that do not have any
--                associated Item Charge Detail records.
------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message IN OUT  VARCHAR2,
                       I_item          IN      ITEM_MASTER.ITEM%TYPE,
                       I_item_list     IN      SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN;
FUNCTION DELETE_HEADER(O_error_message IN OUT  VARCHAR2,
                       I_item          IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: DEFAULT_CHRGS
-- Purpose      : Defaults Dept Up Charges to the passed in Item.
------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHRGS(O_error_message IN OUT VARCHAR2,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                       I_item_level    IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level    IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                       I_pack_type     IN     ITEM_MASTER.PACK_TYPE%TYPE,
                       I_dept          IN     DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: DELETE_CHRGS
-- Purpose      : Deletes Item Charge Header and Detail records for the passed
--                in item.
------------------------------------------------------------------------------------
FUNCTION DELETE_CHRGS(O_error_message IN OUT  VARCHAR2,
                      I_item          IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: DC_DEFAULT_CHRGS
-- Purpose      : Bulk defaults Dept Up Charges to the passed in Item_tbl and dept_tbl
--                Used for data conversion scripts.
------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHRGS(O_error_message IN OUT VARCHAR2,
                       I_items         IN      ITEM_TBL,
                       I_depts         IN      DEPT_TBL)

   RETURN BOOLEAN;
------------------------------------------------------------------------------------
END ITEM_CHARGE_SQL;
/
