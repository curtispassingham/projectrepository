CREATE OR REPLACE PACKAGE MARKUP_SQL AUTHID CURRENT_USER IS
-------------------------------------------------------------------------------
-- Name:    CALC_MARKUP_PERCENT
-- Purpose: Inputs are I_markup_calc_type, I_total_cost, and I_total_retail.
--          I_markup_calc_type can be either 'C' to compute markup percent
--          of Cost or 'R' to compute markup percent of Retail.
--          I_total_cost is the cost of any entity you wish, such as an order
--          total, an ITEM unit cost, etc.  Same goes for I_total_retail.
--          The function will return O_markup_percent to the calling function.
-- Created By: Shane G. Kville
-------------------------------------------------------------------------------
FUNCTION CALC_MARKUP_PERCENT (O_error_message     IN OUT VARCHAR2,
                              O_markup_percent    IN OUT NUMBER,
                              I_markup_calc_type  IN     deps.markup_calc_type%TYPE,
                              I_total_cost        IN     item_loc_soh.unit_cost%TYPE,
                              I_total_retail      IN     item_loc.unit_retail%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    CALC_MARKUP_PERCENT_ITEM
-- Purpose: This function will be created to calculate the markup % exclusive
--          of VAT for an item by stripping off the VAT amount from the total
--          retail passed into the function before calculating the markup %.
--          If retail and cost values are passed in and the
--          I_retail_includes_vat_ind is set to 'N', then VAT is assumed to
--          have been stripped off the retail total or VAT is turned off in the
--          RMS and VAT will not be stripped off the retail.
--
--          Any changes done to this function should be applied to
--          CALC_MARKUP_PERCENT_ITEM_BULK.
--          CALC_MARKUP_PERCENT_ITEM_BULK is a bulk processing version of this
--          function.
-- Created By: Shane G. Kville
-------------------------------------------------------------------------------
FUNCTION CALC_MARKUP_PERCENT_ITEM (O_error_message           IN OUT VARCHAR2,
                                   O_markup_percent          IN OUT NUMBER,
                                   I_retail_includes_vat_ind IN     VARCHAR2,
                                   I_markup_calc_type        IN     deps.markup_calc_type%TYPE,
                                   I_total_cost              IN     item_loc_soh.unit_cost%TYPE,
                                   I_total_retail            IN     item_loc.unit_retail%TYPE,
                                   I_item                    IN     item_master.item%TYPE,
                                   I_dept                    IN     deps.dept%TYPE,
                                   I_loc_type                IN     cost_zone_group_loc.loc_type%TYPE,
                                   I_location                IN     cost_zone_group_loc.location%TYPE,
                                   I_vat_region              IN     vat_region.vat_region%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------
--DUMMY CALL TO FIX ORACLE DEFAULT BUG, DON'T CHANGE EXCEPT TO MATCH
--PREVIOUS SPEC
---------------------------------------------------------------------
FUNCTION CALC_MARKUP_PERCENT_ITEM (O_error_message           IN OUT VARCHAR2,
                                   O_markup_percent          IN OUT NUMBER,
                                   I_retail_includes_vat_ind IN     VARCHAR2,
                                   I_markup_calc_type        IN     deps.markup_calc_type%TYPE,
                                   I_total_cost              IN     item_loc_soh.unit_cost%TYPE,
                                   I_total_retail            IN     item_loc.unit_retail%TYPE,
                                   I_item                    IN     item_master.item%TYPE,
                                   I_dept                    IN     deps.dept%TYPE,
                                   I_loc_type                IN     cost_zone_group_loc.loc_type%TYPE,
                                   I_location                IN     cost_zone_group_loc.location%TYPE,
                                   I_vat_region              IN     vat_region.vat_region%TYPE,
                                   I_supplier_cost_ind       IN     VARCHAR2,
                                   I_supplier                IN     sups.supplier%TYPE)
           RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       CALC_RETAIL_FROM_MARKUP
-- Purpose:    Calculate the Retail for an entity where Cost, Markup, and Markup
--             Type (Cost or Retail) are known.
-- Use:        Required input parameters are I_markup_percent, I_markup_calc_type,
--             I_unit_cost, and I_currency_code.  I_markup_percent is the markup
--             for the entity that you're using.  I_markup_calc_type is either 'C' for
--             the markup percent of Cost, or 'R' for markup percent of Retail.
--             I_unit_cost is the cost of the entity that you're calculating.  If
--             I_vat_ind is NULL, the value will be fetched from the VAT_IND column
--             on SYSTEM_OPTIONS.  If this value is 'Y', the O_unit_retail returned
--             will include the VAT in the calculation in which case the SKU must
--             be passed to find the VAT rate.  I_loc_type, I_location, I_dept,
--             I_vat_region, I_vat_code, and I_vat_ind are all optional.  If the values
--             are passed in, processing time will be faster.
-- Created By: Shane G. Kville
-- Date:       September 11, 1997
-------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_FROM_MARKUP (O_error_message           IN OUT VARCHAR2,
                                  O_unit_retail             IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_markup_percent          IN     NUMBER,
                                  I_markup_calc_type        IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                                  I_unit_cost               IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                                  I_item                    IN     ITEM_MASTER.ITEM%TYPE,
                                  I_loc_type                IN     COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                                  I_location                IN     COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                                  I_dept                    IN     DEPS.DEPT%TYPE,
                                  I_vat_region              IN     VAT_REGION.VAT_REGION%TYPE,
                                  I_currency_code           IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                                  I_supplier_cost_ind       IN     VARCHAR2,
                                  I_supplier                IN     SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------
--DUMMY CALL TO FIX ORACLE DEFAULT BUG, DON'T CHANGE EXCEPT TO MATCH
--PREVIOUS SPEC
---------------------------------------------------------------------
FUNCTION CALC_RETAIL_FROM_MARKUP (O_error_message           IN OUT VARCHAR2,
                                  O_unit_retail             IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_markup_percent          IN     NUMBER,
                                  I_markup_calc_type        IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                                  I_unit_cost               IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                                  I_item                    IN     ITEM_MASTER.ITEM%TYPE,
                                  I_loc_type                IN     COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                                  I_location                IN     COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                                  I_dept                    IN     DEPS.DEPT%TYPE,
                                  I_vat_region              IN     VAT_REGION.VAT_REGION%TYPE,
                                  I_currency_code           IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    CALC_MARKUP_PERCENT_ITEM_BULK
-- Purpose: Bulk-processing version of CALC_MARKUP_PERCENT_ITEM.
--          Any changes to CALC_MARKUP_PERCENT_ITEM should be applied here as well.
-------------------------------------------------------------------------------
FUNCTION CALC_MARKUP_PERCENT_ITEM_BULK(O_error_message       IN OUT        VARCHAR2,
                                       IO_item_pricing_table IN OUT NOCOPY PM_RETAIL_API_SQL.ITEM_PRICING_TABLE,
                                       I_total_cost          IN            ITEM_LOC_SOH.UNIT_COST%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
END;
/
