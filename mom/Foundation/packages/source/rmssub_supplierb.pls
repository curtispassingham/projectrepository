CREATE OR REPLACE PACKAGE BODY RMSSUB_SUPPLIER AS

LP_system_options SYSTEM_OPTIONS%ROWTYPE := NULL;
LP_sup_exists VARCHAR2(1) := 'N';

/* Declare private procedure and functions.*/
---------------------------------------------------------------------
FUNCTION PARSE_SUPPLIER(O_error_message          OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_supplier_record        OUT  SUPS%ROWTYPE,
                        O_end_date_active        OUT  DATE,
                        I_supplier_root       IN OUT  XMLDOM.DOMElement)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION SET_SUPPLIER_STATUS(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_supplier_record IN OUT SUPS%ROWTYPE,
                             I_end_date_active  IN     DATE,
                             I_supplier_exists  IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION PARSE_ADDRESS(O_error_message          OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_address_tbl         IN OUT  ADDRESS_DATA,
                       I_addr_node           IN OUT  XMLDOM.DOMELEMENT)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION VALIDATE_REQUIRED_ADDR_TYPES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_supplier      IN     SUPS.SUPPLIER%TYPE,
                                      IO_address_tbl  IN OUT ADDRESS_DATA)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION PROCESS_SUPPLIER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_table_locked     IN OUT BOOLEAN,
                          O_exists           IN OUT VARCHAR2,
                          IO_supplier_record IN OUT SUPS%ROWTYPE,
                          I_end_date_active  IN     DATE)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION PROCESS_ADDRESS(O_error_message          OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_table_locked           OUT  BOOLEAN,
                         I_supplier_no         IN      SUPS.SUPPLIER%TYPE,
                         I_address_record      IN      ADDR%ROWTYPE)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION INSERT_SUPPLIER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_supplier_record  IN     SUPS%ROWTYPE)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION INSERT_ADDRESS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_table_locked        OUT BOOLEAN,
                        I_supplier_no      IN     SUPS.SUPPLIER%TYPE,
                        I_address_record   IN OUT ADDR%ROWTYPE)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER_RECORD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_supplier_record  IN OUT SUPS%ROWTYPE,
                                  I_update_ind        IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION VALIDATE_ADDRESS_RECORD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_address_record    IN     ADDR%ROWTYPE,
                                 I_supplier_no       IN     SUPS.SUPPLIER%TYPE)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_table_locked     IN OUT BOOLEAN,
                          I_supplier_record  IN     SUPS%ROWTYPE)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION UPDATE_ADDRESS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_table_locked     IN OUT BOOLEAN,
                        I_supplier_no      IN     SUPS.SUPPLIER%TYPE,
                        I_address_record   IN     ADDR%ROWTYPE)

return BOOLEAN;
---------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable IN     VARCHAR2,
                      I_record_name     IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION CHECK_CODES (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable  IN     VARCHAR2,
                      I_code_value       IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------
FUNCTION CHECK_FKEYS (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable   IN     VARCHAR2,
                      I_fkey_column       IN     VARCHAR2,
                      I_fkey_table        IN     VARCHAR2,
                      I_record_variable2  IN     VARCHAR2 DEFAULT NULL,
                      I_fkey_column2      IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION CHECK_ADDR (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_supplier_no      IN     SUPS.SUPPLIER%TYPE,
                     I_ret_allow_ind    IN     SUPS.RET_ALLOW_IND%TYPE)
return BOOLEAN;
---------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_status_code              IN OUT  VARCHAR2,
                        IO_error_message            IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause                     IN      VARCHAR2,
                        I_program                   IN      VARCHAR2);
---------------------------------------------------------------------
FUNCTION VALIDATE_ORG_UNIT_RECORD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_org_unit          IN     PARTNER_ORG_UNIT%ROWTYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION PROCESS_ORGUNIT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_table_locked         IN OUT BOOLEAN,
                         I_partner_org_unit_rec IN     PARTNER_ORG_UNIT%ROWTYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION UPDATE_ORG_UNIT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_table_locked         IN OUT BOOLEAN,
                         I_partner_org_unit_rec IN     PARTNER_ORG_UNIT%ROWTYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION INSERT_ORG_UNIT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_partner_org_unit_rec IN     PARTNER_ORG_UNIT%ROWTYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER_STATUS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_supplier_record IN OUT SUPS%ROWTYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------
/* Function and Procedure Bodies */
-------------------------------------------------------------------------
FUNCTION CONSUME(O_status                  OUT  VARCHAR2,
                 O_error_message           OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 I_document             IN      CLOB)
return BOOLEAN IS

   L_supplier_rec SUPS%ROWTYPE;

   L_address_tbl ADDRESS_DATA;
   L_partner_org_unit_tbl PARTNER_ORG_UNIT_TBL;

   L_table_locked BOOLEAN := FALSE;
   L_supp_exists VARCHAR2(1);
   supplier_root XMLDOM.DOMELEMENT;
   address XMLDOM.DOMNODELIST;
   addr_node XMLDOM.DOMELEMENT;
   supplier_node XMLDOM.DOMELEMENT;
   orgunit XMLDOM.DOMNODELIST;
   orgunit_node XMLDOM.DOMELEMENT;

   PROGRAM_ERROR EXCEPTION;
   TABLE_LOCKED EXCEPTION;

   L_end_date_active DATE := NULL;

BEGIN

   -- Fetch financial application information.
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   -- parse the document and return the root element.
   -- Send in entire xml document, the xml file header name, and the following defaults (for now).
   supplier_root := API_LIBRARY.readRoot(I_document, 'VendorDesc');

   supplier_node := rib_xml.getChild(supplier_root, 'VendorHdrDesc');

   -- Process header record by retrieving the value for each associated xml tag.
   if PARSE_SUPPLIER(O_error_message,
                     L_supplier_rec,
                     L_end_date_active,
                     supplier_node) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   ---

   if PROCESS_SUPPLIER(O_error_message,
                       L_table_locked,
                       L_supp_exists,
                       L_supplier_rec,
                       L_end_date_active) = FALSE  then
      raise PROGRAM_ERROR;
   end if;

   -- the detail records are retrieved in a list (see above variable type for address)
   -- which I can then loop through and process.

   address := rib_xml.getChildren(supplier_root, 'VendorAddrDesc');

   -- for a supplier create message, at least 1 address needs to be present
   if L_supp_exists = 'N' and rib_xml.getListLength(address) <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ADDR_DETAIL_PUB',
                                            NULL,
                                            NULL,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   FOR i IN 0 .. (rib_xml.getListLength(address)-1)
   LOOP
      addr_node := rib_xml.getListElement(address, i);
      ---
      if PARSE_ADDRESS(O_error_message,
                       L_address_tbl,
                       addr_node) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---
   END LOOP; -- FOR i

   if VALIDATE_REQUIRED_ADDR_TYPES(O_error_message,
                                   L_supplier_rec.supplier,
                                   L_address_tbl) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_address_tbl.COUNT > 0 then
      FOR q IN L_address_tbl.FIRST .. L_address_tbl.LAST LOOP
         if PROCESS_ADDRESS(O_error_message,
                            L_table_locked,
                            L_supplier_rec.supplier,
                            L_address_tbl(q)) = FALSE then
            raise PROGRAM_ERROR;
         end if;
      END LOOP;
   end if;

   if UPDATE_SUPPLIER_STATUS(O_error_message,
                             L_supplier_rec) = FALSE then
      return FALSE;
   end if;

   if LP_system_options.org_unit_ind = 'Y' then
      orgunit := rib_xml.getChildren(supplier_root, 'VendorOUDesc');

      FOR i IN 0 .. (rib_xml.getListLength(orgunit)-1)
      LOOP
         -- Parse all the org unit nodes one by one
         orgunit_node := rib_xml.getListElement(orgunit, i);

         L_partner_org_unit_tbl(i).org_unit_id       := rib_xml.getChildNumber(orgunit_node,'org_unit_id');
         L_partner_org_unit_tbl(i).primary_pay_site  := rib_xml.getChildText(orgunit_node,'primary_pay_site_ind');

         L_partner_org_unit_tbl(i).partner := L_supplier_rec.supplier;

         if (L_supplier_rec.supplier_parent is NOT NULL) then
            L_partner_org_unit_tbl(i).partner_type := 'U';
         else
            L_partner_org_unit_tbl(i).partner_type := 'S';
         end if;
      END LOOP;

      -- When supplier_site_ind = 'Y', partner_org_unit only exists for supplier sites, not for parent supplier.
      if (LP_system_options.supplier_sites_ind = 'Y' and L_supplier_rec.supplier_parent IS NULL) then
         rib_xml.freeRoot( supplier_root );
         return TRUE;
      else
         if L_partner_org_unit_tbl is NOT NULL and
            L_partner_org_unit_tbl.COUNT > 0 then
            FOR r IN L_partner_org_unit_tbl.FIRST .. L_partner_org_unit_tbl.LAST
            LOOP
               if PROCESS_ORGUNIT(O_error_message,
                                  L_table_locked,
                                  L_partner_org_unit_tbl(r)) = FALSE then
                  raise PROGRAM_ERROR;
               end if;
            END LOOP;
         end if;
      end if;
   end if;  -- LP_system_options.org_unit_ind = 'Y'

   if L_table_locked = TRUE then
      raise TABLE_LOCKED;
   end if;
   ---
   ---
   rib_xml.freeRoot( supplier_root );
   ---
   O_status := API_CODES.SUCCESS;
   return TRUE;

EXCEPTION
   when TABLE_LOCKED then
      rib_xml.freeRoot( supplier_root );
      HANDLE_ERRORS(O_status,
                    O_error_message,
                    API_LIBRARY.TABLE_LOCKED,
                    'RMSSUB_SUPPLIER.CONSUME');
      return FALSE;

   when PROGRAM_ERROR then
      rib_xml.freeRoot( supplier_root );
      HANDLE_ERRORS(O_status,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    'RMSSUB_SUPPLIER.CONSUME');
      return FALSE;

   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSSUB_SUPPLIER.CONSUME');
      return FALSE;

END CONSUME;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_SUPPLIER(O_error_message          OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_supplier_record        OUT  SUPS%ROWTYPE,
                        O_end_date_active        OUT  DATE,
                        I_supplier_root       IN OUT  XMLDOM.DOMELEMENT)
return BOOLEAN IS
BEGIN

   -- The package rib_xml is called to retrieve values from the XML document.
   -- In order to do this, the specific tag names on the XML document will need to be known
   -- for each value we need to retrieve.

   -- Retrieve header record in local variable for conversion.
   -- In the future, we expect all xml files will have the correct variable types.

   O_supplier_record.supplier               := rib_xml.getChildNumber(I_supplier_root, 'supplier');
   O_supplier_record.sup_name               := rib_xml.getChildText(I_supplier_root, 'sup_name');
   O_supplier_record.sup_name_secondary     := rib_xml.getChildText(I_supplier_root, 'sup_name_secondary');
   O_supplier_record.supplier_parent        := rib_xml.getChildText(I_supplier_root, 'supplier_parent');
   O_supplier_record.contact_name           := rib_xml.getChildText(I_supplier_root, 'contact_name');
   O_supplier_record.contact_phone          := rib_xml.getChildText(I_supplier_root, 'contact_phone');
   O_supplier_record.contact_fax            := rib_xml.getChildText(I_supplier_root, 'contact_fax');
   O_supplier_record.contact_pager          := rib_xml.getChildText(I_supplier_root, 'contact_pager');
   O_supplier_record.sup_status             := rib_xml.getChildText(I_supplier_root, 'sup_status');
   O_supplier_record.qc_ind                 := rib_xml.getChildText(I_supplier_root, 'qc_ind');
   O_supplier_record.qc_pct                 := rib_xml.getChildNumber(I_supplier_root, 'qc_pct');
   O_supplier_record.qc_freq                := rib_xml.getChildNumber(I_supplier_root, 'qc_freq');
   O_supplier_record.vc_ind                 := rib_xml.getChildText(I_supplier_root, 'vc_ind');
   O_supplier_record.vc_pct                 := rib_xml.getChildNumber(I_supplier_root, 'vc_pct');
   O_supplier_record.vc_freq                := rib_xml.getChildNumber(I_supplier_root, 'vc_freq');
   O_supplier_record.currency_code          := rib_xml.getChildText(I_supplier_root, 'currency_code');
   O_supplier_record.lang                   := rib_xml.getChildNumber(I_supplier_root, 'lang');
   O_supplier_record.terms                  := rib_xml.getChildText(I_supplier_root, 'terms');
   O_supplier_record.freight_terms          := rib_xml.getChildText(I_supplier_root, 'freight_terms');
   O_supplier_record.ret_allow_ind          := rib_xml.getChildText(I_supplier_root, 'ret_allow_ind');
   O_supplier_record.ret_auth_req           := rib_xml.getChildText(I_supplier_root, 'ret_auth_req');
   O_supplier_record.ret_min_dol_amt        := rib_xml.getChildNumber(I_supplier_root, 'ret_min_dol_amt');
   O_supplier_record.ret_courier            := rib_xml.getChildText(I_supplier_root, 'ret_courier');
   O_supplier_record.handling_pct           := rib_xml.getChildNumber(I_supplier_root, 'handling_pct');
   O_supplier_record.edi_po_ind             := rib_xml.getChildText(I_supplier_root, 'edi_po_ind');
   O_supplier_record.edi_po_chg             := rib_xml.getChildText(I_supplier_root, 'edi_po_chg');
   O_supplier_record.edi_po_confirm         := rib_xml.getChildText(I_supplier_root, 'edi_po_confirm');
   O_supplier_record.edi_asn                := rib_xml.getChildText(I_supplier_root, 'edi_asn');
   O_supplier_record.edi_sales_rpt_freq     := rib_xml.getChildText(I_supplier_root, 'edi_sales_rpt_freq');
   O_supplier_record.edi_supp_available_ind := rib_xml.getChildText(I_supplier_root, 'edi_supp_available_ind');
   O_supplier_record.edi_contract_ind       := rib_xml.getChildText(I_supplier_root, 'edi_contract_ind');
   O_supplier_record.edi_invc_ind           := rib_xml.getChildText(I_supplier_root, 'edi_invc_ind');
   O_supplier_record.cost_chg_pct_var       := rib_xml.getChildNumber(I_supplier_root, 'cost_chg_pct_var');
   O_supplier_record.cost_chg_amt_var       := rib_xml.getChildNumber(I_supplier_root, 'cost_chg_amt_var');
   O_supplier_record.replen_approval_ind    := rib_xml.getChildText(I_supplier_root, 'replen_approval_ind');
   O_supplier_record.ship_method            := rib_xml.getChildText(I_supplier_root, 'ship_method');
   O_supplier_record.payment_method         := rib_xml.getChildText(I_supplier_root, 'payment_method');
   O_supplier_record.contact_telex          := rib_xml.getChildText(I_supplier_root, 'contact_telex');
   O_supplier_record.contact_email          := rib_xml.getChildText(I_supplier_root, 'contact_email');
   O_supplier_record.settlement_code        := rib_xml.getChildText(I_supplier_root, 'settlement_code');
   O_supplier_record.pre_mark_ind           := rib_xml.getChildText(I_supplier_root, 'pre_mark_ind');
   O_supplier_record.auto_appr_invc_ind     := rib_xml.getChildText(I_supplier_root, 'auto_appr_invc_ind');
   O_supplier_record.dbt_memo_code          := rib_xml.getChildText(I_supplier_root, 'dbt_memo_code');
   O_supplier_record.freight_charge_ind     := rib_xml.getChildText(I_supplier_root, 'freight_charge_ind');
   O_supplier_record.auto_appr_dbt_memo_ind := rib_xml.getChildText(I_supplier_root, 'auto_appr_dbt_memo_ind');
   O_supplier_record.prepay_invc_ind        := rib_xml.getChildText(I_supplier_root, 'prepay_invc_ind');
   O_supplier_record.backorder_ind          := rib_xml.getChildText(I_supplier_root, 'backorder_ind');
   O_supplier_record.vat_region             := rib_xml.getChildNumber(I_supplier_root, 'vat_region');
   O_supplier_record.inv_mgmt_lvl           := rib_xml.getChildText(I_supplier_root, 'inv_mgmt_lvl');
   O_supplier_record.service_perf_req_ind   := rib_xml.getChildText(I_supplier_root, 'service_perf_req_ind');
   O_supplier_record.invc_pay_loc           := rib_xml.getChildText(I_supplier_root, 'invc_pay_loc');
   O_supplier_record.invc_receive_loc       := rib_xml.getChildText(I_supplier_root, 'invc_receive_loc');
   O_supplier_record.addinvc_gross_net      := rib_xml.getChildText(I_supplier_root, 'addinvc_gross_net');
   O_supplier_record.delivery_policy        := rib_xml.getChildText(I_supplier_root, 'delivery_policy');
   O_supplier_record.comment_desc           := rib_xml.getChildText(I_supplier_root, 'comment_desc');
   O_supplier_record.default_item_lead_time := rib_xml.getChildNumber(I_supplier_root, 'default_item_lead_time');
   O_supplier_record.duns_number            := rib_xml.getChildText(I_supplier_root, 'duns_number');
   O_supplier_record.duns_loc               := rib_xml.getChildText(I_supplier_root, 'duns_loc');
   O_supplier_record.bracket_costing_ind    := rib_xml.getChildText(I_supplier_root, 'bracket_costing_ind');
   O_supplier_record.vmi_order_status       := rib_xml.getChildText(I_supplier_root, 'vmi_order_status');
   O_end_date_active                        := rib_xml.getChildDate(I_supplier_root, 'end_date_active');
   O_supplier_record.dsd_ind                := rib_xml.getChildText(I_supplier_root, 'dsd_supplier_ind');
   O_supplier_record.scale_aip_orders       := rib_xml.getChildText(I_supplier_root, 'scale_aip_orders');
   O_supplier_record.final_dest_ind         := rib_xml.getChildText(I_supplier_root, 'final_dest_ind');
   O_supplier_record.sup_qty_level          := rib_xml.getChildText(I_supplier_root, 'sup_qty_level');

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.PARSE_SUPPLIER',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_SUPPLIER;
-----------------------------------------------------------------------------------------
FUNCTION SET_SUPPLIER_STATUS(O_error_message       OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_supplier_record IN OUT SUPS%ROWTYPE,
                             I_end_date_active  IN     DATE,
                             I_supplier_exists  IN     VARCHAR2)
return BOOLEAN IS

   L_vdate PERIOD.VDATE%TYPE  := GET_VDATE;
   L_input_status    VARCHAR2(1);

   cursor C_PARENT_SUP_STATUS is
      select sup_status
        from sups 
       where supplier = IO_supplier_record.supplier_parent;

BEGIN
   -- If RMS is connected to Oracle Financials version 11.5.10, then the supplier status is calculated
   -- differently than if RMS is not connected to Oracle Financials version 11.5.10.  Please note that
   -- the value held in the SYSTEM_OPTIONS.ORACLE_FINANCIAL_VERS field is a code (code type 'ORAV').
   -- If VAT is not used in RMS, and RMS is hooked up to Oracle Financials version 11.5.10, then
   -- the input supplier status will be used.  If VAT is used in RMS, and if the input vat region
   -- is valid in RMS, then the input supplier status will be used.  If VAT is used in RMS, and the
   -- input vat region is not valid in RMS, then the supplier status will be set to 'I'nactive.
   if LP_system_options.default_tax_type = 'SVAT' then
      if IO_supplier_record.vat_region is NULL then
         IO_supplier_record.sup_status := 'I';
      end if;
   end if;

   if I_end_date_active is NOT NULL then
      if TO_DATE(TO_CHAR(I_end_date_active, 'YYYYMMDD'), 'YYYYMMDD') <=
         TO_DATE(TO_CHAR(L_vdate,'YYYYMMDD'), 'YYYYMMDD') then
         IO_supplier_record.sup_status := 'I';
      else
         IO_supplier_record.sup_status := 'A';
      end if;
   end if;

   /* check the supplier status, if inactive then make the supplier sites also as inactive */
   if (LP_system_options.supplier_sites_ind = 'Y' and IO_supplier_record.supplier_parent IS not NULL) then
     open  C_PARENT_SUP_STATUS;
     fetch C_PARENT_SUP_STATUS into L_input_status;
     close C_PARENT_SUP_STATUS;     
     
     if L_input_status = 'I' then
          IO_supplier_record.sup_status := 'I';
     end if;

   end if;
   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.SET_SUPPLIER_STATUS',
                                            to_char(SQLCODE));
      return FALSE;
END SET_SUPPLIER_STATUS;
-----------------------------------------------------------------------------------------
FUNCTION PARSE_ADDRESS(O_error_message          OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_address_tbl         IN OUT  ADDRESS_DATA,
                       I_addr_node           IN OUT  XMLDOM.DOMELEMENT)
return BOOLEAN IS

   L_address_rec ADDR%ROWTYPE;
   L_pay_site_flag VARCHAR2(1) := NULL;
   L_purchasing_site_flag VARCHAR2(1) := NULL;
   
     cursor C_LOCK_PRIMARY_ADDR is
      select 'x'
        from addr
       where key_value_1 = L_address_rec.key_value_1
         and addr_type   = L_address_rec.addr_type
         and module      = 'SUPP'
         for update nowait;

BEGIN


   L_address_rec.key_value_1       := rib_xml.getChildText(I_addr_node, 'key_value_1');
   L_address_rec.seq_no            := rib_xml.getChildText(I_addr_node, 'seq_no');
   L_address_rec.addr_type         := rib_xml.getChildText(I_addr_node, 'addr_type');
   L_address_rec.primary_addr_ind  := rib_xml.getChildText(I_addr_node, 'primary_addr_ind');
   L_address_rec.add_1             := rib_xml.getChildText(I_addr_node, 'add_1');
   L_address_rec.add_2             := rib_xml.getChildText(I_addr_node, 'add_2');
   L_address_rec.add_3             := rib_xml.getChildText(I_addr_node, 'add_3');
   L_address_rec.city              := rib_xml.getChildText(I_addr_node, 'city');
   L_address_rec.state             := rib_xml.getChildText(I_addr_node, 'state');
   L_address_rec.country_id        := rib_xml.getChildText(I_addr_node, 'country_id');
   L_address_rec.post              := rib_xml.getChildText(I_addr_node, 'post');
   L_address_rec.contact_name      := rib_xml.getChildText(I_addr_node, 'contact_name');
   L_address_rec.contact_phone     := rib_xml.getChildText(I_addr_node, 'contact_phone');
   L_address_rec.contact_telex     := rib_xml.getChildText(I_addr_node, 'contact_telex');
   L_address_rec.contact_fax       := rib_xml.getChildText(I_addr_node, 'contact_fax');
   L_address_rec.contact_email     := rib_xml.getChildText(I_addr_node, 'contact_email');
   L_address_rec.jurisdiction_code := rib_xml.getChildText(I_addr_node, 'jurisdiction_code');
   L_pay_site_flag                 := rib_xml.getChildText(I_addr_node, 'pay_site_flag');
   L_purchasing_site_flag          := rib_xml.getChildText(I_addr_node, 'purchasing_site_flag');
   ---
   O_address_tbl(O_address_tbl.COUNT) := L_address_rec;
   ---

   if L_address_rec.primary_addr_ind = 'Y' then
      open C_LOCK_PRIMARY_ADDR;
      close C_LOCK_PRIMARY_ADDR;
      ---
      update addr
         set primary_addr_ind = 'N'
       where key_value_1 = L_address_rec.key_value_1
         and addr_type   = L_address_rec.addr_type
         and module      = 'SUPP';
   end if;

   
    if L_address_rec.primary_addr_ind IS NULL then
      L_address_rec.primary_addr_ind := 'N';
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
      if C_LOCK_PRIMARY_ADDR%ISOPEN then
         close C_LOCK_PRIMARY_ADDR;
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.PARSE_ADDRESS',
                                            to_char(SQLCODE));
      return FALSE;

END PARSE_ADDRESS;
-----------------------------------------------------------------------------------------
FUNCTION VALIDATE_REQUIRED_ADDR_TYPES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_supplier      IN     SUPS.SUPPLIER%TYPE,
                                      IO_address_tbl  IN OUT ADDRESS_DATA)
return BOOLEAN IS

   CURSOR c_get_missing_addr_type IS
     SELECT address_type
       FROM add_type_module
      WHERE module = 'SUPP'
        and mandatory_ind = 'Y'
       MINUS
     SELECT addr_type
       FROM addr
      WHERE module = 'SUPP'
        AND key_value_1 = to_char(I_supplier);

   L_missing_type ADDRESS_TYPE_DATA;
   L_type_still_missing BOOLEAN := TRUE;
   L_address_tbl_index NUMBER := 0;
   L_default_address_index NUMBER := -1;

BEGIN

   OPEN c_get_missing_addr_type;
   FETCH c_get_missing_addr_type BULK COLLECT INTO L_missing_type;
   CLOSE c_get_missing_addr_type;

   if L_missing_type.COUNT > 0 then
      -- Find the address index to default from if missing addresses are found.  If an Ordering
      -- address has been passed, default addresses from the Ordering address.  If an Ordering
      -- address has not been passed, and a Remittance address has, default addresses from the
      -- Remittance address.  If neither Ordering nor Remittance address has been passed, default
      -- addresses from the first address sent.
      FOR a IN IO_address_tbl.FIRST .. IO_address_tbl.LAST LOOP
         if (IO_address_tbl(a).addr_type = '04') or
            (L_default_address_index = -1 and IO_address_tbl(a).addr_type = '06') then
            L_default_address_index := a;
         end if;
      END LOOP;

      if L_default_address_index = -1 then
         L_default_address_index := 0;
      end if;

      -- Check to see if any missing address type has just been passed into this API
      FOR b IN L_missing_type.FIRST .. L_missing_type.LAST LOOP
         L_type_still_missing := TRUE;

         FOR c IN IO_address_tbl.FIRST .. IO_address_tbl.LAST LOOP
            if L_missing_type(b) = IO_address_tbl(c).addr_type then
               L_type_still_missing := FALSE;
            end if;
         END LOOP;

         if L_type_still_missing = TRUE then
            if IO_address_tbl.COUNT >= 1 then
               L_address_tbl_index := IO_address_tbl.COUNT;
               IO_address_tbl(L_address_tbl_index) := IO_address_tbl(L_default_address_index);
               IO_address_tbl(L_address_tbl_index).addr_type := L_missing_type(b);
            end if;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER.VALIDATE_REQUIRED_ADDR_TYPES',
                                             to_char(SQLCODE));
      return FALSE;

END VALIDATE_REQUIRED_ADDR_TYPES;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_SUPPLIER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_table_locked     IN OUT BOOLEAN,
                          O_exists           IN OUT VARCHAR2,
                          IO_supplier_record IN OUT SUPS%ROWTYPE,
                          I_end_date_active  IN     DATE)
return BOOLEAN IS

   L_supplier_rec SUPS%ROWTYPE := IO_supplier_record;
   L_exists VARCHAR2(1) := 'N';

cursor C_SUPS is
      select NVL(L_supplier_rec.sup_name, sup_name),
               NVL(L_supplier_rec.sup_name_secondary, sup_name_secondary),
             NVL(L_supplier_rec.supplier_parent, supplier_parent),
             NVL(L_supplier_rec.contact_name, contact_name),
             NVL(L_supplier_rec.contact_phone, contact_phone),
             NVL(L_supplier_rec.contact_fax, contact_fax),
             NVL(L_supplier_rec.contact_pager, contact_pager),
             NVL(L_supplier_rec.sup_status, sup_status),
             NVL(L_supplier_rec.qc_ind, qc_ind),
             L_supplier_rec.qc_pct,
             L_supplier_rec.qc_freq,
             NVL(L_supplier_rec.vc_ind, vc_ind),
             L_supplier_rec.vc_pct,
             L_supplier_rec.vc_freq,
             NVL(L_supplier_rec.currency_code,currency_code),
             NVL(L_supplier_rec.lang, lang),
             NVL(L_supplier_rec.terms, terms),
             NVL(L_supplier_rec.freight_terms, freight_terms),
             NVL(L_supplier_rec.ret_allow_ind, ret_allow_ind),
             NVL(L_supplier_rec.ret_auth_req, ret_auth_req),
             NVL(L_supplier_rec.ret_min_dol_amt, ret_min_dol_amt),
             NVL(L_supplier_rec.ret_courier, ret_courier),
             NVL(L_supplier_rec.handling_pct, handling_pct),
             NVL(L_supplier_rec.edi_po_ind, edi_po_ind),
             NVL(L_supplier_rec.edi_po_chg, edi_po_chg),
             NVL(L_supplier_rec.edi_po_confirm, edi_po_confirm),
             NVL(L_supplier_rec.edi_asn, edi_asn),
             NVL(L_supplier_rec.edi_sales_rpt_freq, edi_sales_rpt_freq),
             NVL(L_supplier_rec.edi_supp_available_ind, edi_supp_available_ind),
             NVL(L_supplier_rec.edi_contract_ind, edi_contract_ind),
             NVL(L_supplier_rec.edi_invc_ind, edi_invc_ind),
             NVL(L_supplier_rec.cost_chg_pct_var, cost_chg_pct_var),
             NVL(L_supplier_rec.cost_chg_amt_var, cost_chg_amt_var),
             NVL(L_supplier_rec.replen_approval_ind, replen_approval_ind),
             NVL(L_supplier_rec.ship_method, ship_method),
             NVL(L_supplier_rec.payment_method, payment_method),
             NVL(L_supplier_rec.contact_telex, contact_telex),
             NVL(L_supplier_rec.contact_email, contact_email),
             NVL(L_supplier_rec.settlement_code, settlement_code),
             NVL(L_supplier_rec.pre_mark_ind, pre_mark_ind),
             NVL(L_supplier_rec.auto_appr_invc_ind, auto_appr_invc_ind),
             NVL(L_supplier_rec.dbt_memo_code, dbt_memo_code),
             NVL(L_supplier_rec.freight_charge_ind, freight_charge_ind),
             NVL(L_supplier_rec.auto_appr_dbt_memo_ind, auto_appr_dbt_memo_ind),
             NVL(L_supplier_rec.prepay_invc_ind, prepay_invc_ind),
             NVL(L_supplier_rec.backorder_ind, backorder_ind),
             NVL(L_supplier_rec.vat_region, vat_region),
             NVL(L_supplier_rec.inv_mgmt_lvl, inv_mgmt_lvl),
             NVL(L_supplier_rec.service_perf_req_ind, service_perf_req_ind),
             NVL(L_supplier_rec.invc_pay_loc, invc_pay_loc),
             NVL(L_supplier_rec.invc_receive_loc, invc_receive_loc),
             NVL(L_supplier_rec.addinvc_gross_net, addinvc_gross_net),
             NVL(L_supplier_rec.delivery_policy, delivery_policy),
             NVL(L_supplier_rec.comment_desc, comment_desc),
             NVL(L_supplier_rec.default_item_lead_time, default_item_lead_time),
             NVL(L_supplier_rec.duns_number, duns_number),
             NVL(L_supplier_rec.duns_loc, duns_loc),
             NVL(L_supplier_rec.bracket_costing_ind, bracket_costing_ind),
             NVL(L_supplier_rec.vmi_order_status, vmi_order_status),
             NVL(L_supplier_rec.dsd_ind, dsd_ind),
             NVL(L_supplier_rec.scale_aip_orders, scale_aip_orders),
             NVL(L_supplier_rec.final_dest_ind, final_dest_ind),
             NVL(L_supplier_rec.sup_qty_level, sup_qty_level)
        from sups
       where supplier = to_char(L_supplier_rec.supplier);

  cursor C_SUPS_EXISTS is
     select 'Y'
       from sups
      where supplier  = to_char(L_supplier_rec.supplier);

BEGIN
   ---
   -- Opening this cursor with NVLs allows us to populate the supplier record without
   -- knowledge of whether we are inserting or updating. Another strategy would be to
   -- simply default records when inserting and validate for updating (partial fetch for testing purposes)
   open C_SUPS;
   fetch C_SUPS into L_supplier_rec.sup_name,
                     L_supplier_rec.sup_name_secondary,
                     L_supplier_rec.supplier_parent,
                     L_supplier_rec.contact_name,
                     L_supplier_rec.contact_phone,
                     L_supplier_rec.contact_fax,
                     L_supplier_rec.contact_pager,
                     L_supplier_rec.sup_status,
                     L_supplier_rec.qc_ind,
                     L_supplier_rec.qc_pct,
                     L_supplier_rec.qc_freq,
                     L_supplier_rec.vc_ind,
                     L_supplier_rec.vc_pct,
                     L_supplier_rec.vc_freq,
                     L_supplier_rec.currency_code,
                     L_supplier_rec.lang,
                     L_supplier_rec.terms,
                     L_supplier_rec.freight_terms,
                     L_supplier_rec.ret_allow_ind,
                     L_supplier_rec.ret_auth_req,
                     L_supplier_rec.ret_min_dol_amt,
                     L_supplier_rec.ret_courier,
                     L_supplier_rec.handling_pct,
                     L_supplier_rec.edi_po_ind,
                     L_supplier_rec.edi_po_chg,
                     L_supplier_rec.edi_po_confirm,
                     L_supplier_rec.edi_asn,
                     L_supplier_rec.edi_sales_rpt_freq,
                     L_supplier_rec.edi_supp_available_ind,
                     L_supplier_rec.edi_contract_ind,
                     L_supplier_rec.edi_invc_ind,
                     L_supplier_rec.cost_chg_pct_var,
                     L_supplier_rec.cost_chg_amt_var,
                     L_supplier_rec.replen_approval_ind,
                     L_supplier_rec.ship_method,
                     L_supplier_rec.payment_method,
                     L_supplier_rec.contact_telex,
                     L_supplier_rec.contact_email,
                     L_supplier_rec.settlement_code,
                     L_supplier_rec.pre_mark_ind,
                     L_supplier_rec.auto_appr_invc_ind,
                     L_supplier_rec.dbt_memo_code,
                     L_supplier_rec.freight_charge_ind,
                     L_supplier_rec.auto_appr_dbt_memo_ind,
                     L_supplier_rec.prepay_invc_ind,
                     L_supplier_rec.backorder_ind,
                     L_supplier_rec.vat_region,
                     L_supplier_rec.inv_mgmt_lvl,
                     L_supplier_rec.service_perf_req_ind,
                     L_supplier_rec.invc_pay_loc,
                     L_supplier_rec.invc_receive_loc,
                     L_supplier_rec.addinvc_gross_net,
                     L_supplier_rec.delivery_policy,
                     L_supplier_rec.comment_desc,
                     L_supplier_rec.default_item_lead_time,
                     L_supplier_rec.duns_number,
                     L_supplier_rec.duns_loc,
                     L_supplier_rec.bracket_costing_ind,
                     L_supplier_rec.vmi_order_status,
                     L_supplier_rec.dsd_ind,
                     L_supplier_rec.scale_aip_orders,
                     L_supplier_rec.final_dest_ind,
                     L_supplier_rec.sup_qty_level;
   close C_SUPS;

   open C_SUPS_EXISTS;
   fetch C_SUPS_EXISTS into L_exists;
   close C_SUPS_EXISTS;

   O_exists := L_exists;
   LP_sup_exists := L_exists;

   -- Validate that all the necessary records are populated.
   if VALIDATE_SUPPLIER_RECORD(O_error_message,
                               L_supplier_rec,
                               L_exists) = FALSE then
      return FALSE;
   end if;

   if SET_SUPPLIER_STATUS(O_error_message,
                          L_supplier_rec,
                          I_end_date_active,
                          L_exists) = FALSE then
      return FALSE;
   end if;

   if LP_system_options.contract_ind = 'N' then
      L_supplier_rec.edi_supp_available_ind := 'N';
      L_supplier_rec.edi_contract_ind   := 'N';
   end if;

   -- Get code that supplies the qc percentage,only if qc_ind is 'Y' and L_supplier_rec.qc_pct value is null
   if L_supplier_rec.qc_ind = 'Y' and L_supplier_rec.qc_pct is NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'QCPC',
                                    'P',  -- this code holds a number which represents the qc percentage
                                     L_supplier_rec.qc_pct) = FALSE then
         RETURN FALSE;
      end if;
   end if;

   if L_exists = 'Y' then
      if UPDATE_SUPPLIER(O_error_message,
                         O_table_locked,
                         L_supplier_rec) = FALSE then
         return FALSE;
      end if;
   else
      if INSERT_SUPPLIER(O_error_message,
                         L_supplier_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

  EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER.PROCESS_SUPPLIER',
                                             to_char(SQLCODE));
      return FALSE;

END PROCESS_SUPPLIER;
---------------------------------------------------------------------------------
FUNCTION PROCESS_ADDRESS(O_error_message          OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_table_locked           OUT  BOOLEAN,
                         I_supplier_no         IN      SUPS.SUPPLIER%TYPE,
                         I_address_record      IN      ADDR%ROWTYPE)
return BOOLEAN IS

   L_address_record ADDR%ROWTYPE := I_address_record;
   L_found_addr_key ADDR.ADDR_KEY%TYPE := NULL;

   cursor C_ADDR_EXISTS is
      select addr_key
        from addr
       where key_value_1 = to_char(I_supplier_no)
         and addr_type = L_address_record.addr_type
         and module = 'SUPP'
         and seq_no = L_address_record.seq_no;


BEGIN

   -- Validate that all the necessary records are populated.
   if VALIDATE_ADDRESS_RECORD(O_error_message,
                              L_address_record,
                              I_supplier_no) = FALSE then
      return FALSE;
   end if;

   open C_ADDR_EXISTS;
   fetch C_ADDR_EXISTS into L_found_addr_key;
   close C_ADDR_EXISTS;
   ---

   if L_found_addr_key IS NOT NULL then
      L_address_record.addr_key := L_found_addr_key;

      if UPDATE_ADDRESS(O_error_message,
                        O_table_locked,
                        I_supplier_no,
                        L_address_record) = FALSE then
         return FALSE;
      end if;
   else
      if INSERT_ADDRESS(O_error_message,
                        O_table_locked,
                        I_supplier_no,
                        L_address_record) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER.PROCESS_ADDRESS',
                                             to_char(SQLCODE));
      return FALSE;

END PROCESS_ADDRESS;
---------------------------------------------------------------------------------
FUNCTION INSERT_SUPPLIER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_supplier_record  IN     SUPS%ROWTYPE)
return BOOLEAN IS

   L_supplier_record   SUPS%ROWTYPE                                    := I_supplier_record;
   L_dept_lvl_ord      PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE := NULL;
   L_inv_mgmt_lvl      SUPS.INV_MGMT_LVL%TYPE                          := NULL;
   L_mandatory_ind     ADD_TYPE_MODULE.MANDATORY_IND%TYPE  := NULL;
   L_ret_allow_ind     SUPS.RET_ALLOW_IND%TYPE;

   cursor C_GET_DEPT_LVL_ORD is
      select dept_level_orders
        from procurement_unit_options;

   cursor C_ADDR_MAND_IND is
      select mandatory_ind 
        from add_type_module 
       where address_type=03 
         and module='SUPP';

BEGIN

   OPEN C_GET_DEPT_LVL_ORD;
   FETCH C_GET_DEPT_LVL_ORD into L_dept_lvl_ord;
   CLOSE C_GET_DEPT_LVL_ORD;
   ---
   if L_dept_lvl_ord = 'Y' then
  
        if L_supplier_record.inv_mgmt_lvl= 'S' then
         L_inv_mgmt_lvl := 'D';
        elsif L_supplier_record.inv_mgmt_lvl= 'L' then
         L_inv_mgmt_lvl := 'A';
        else
         L_inv_mgmt_lvl := NVL(L_supplier_record.inv_mgmt_lvl,'D');
        end if;
   else
      L_inv_mgmt_lvl := NVL(L_supplier_record.inv_mgmt_lvl,'S');
   end if; 
   ---
   open C_ADDR_MAND_IND;
   fetch C_ADDR_MAND_IND into L_mandatory_ind;
   close C_ADDR_MAND_IND;
   ---
   if L_mandatory_ind ='Y' then
      L_ret_allow_ind := 'Y';
   else
      L_ret_allow_ind := 'N';
   end if;
   ---
   insert into sups (supplier,
                     sup_name,
                     sup_name_secondary,
                     supplier_parent,
                     contact_name,
                     contact_phone,
                     contact_fax,
                     contact_pager,
                     sup_status,
                     qc_ind,
                     qc_pct,
                     qc_freq,
                     vc_ind,
                     vc_pct,
                     vc_freq,
                     currency_code,
                     lang,
                     terms,
                     freight_terms,
                     ret_allow_ind,
                     ret_auth_req,
                     ret_min_dol_amt,
                     ret_courier,
                     handling_pct,
                     edi_po_ind,
                     edi_po_chg,
                     edi_po_confirm,
                     edi_asn,
                     edi_sales_rpt_freq,
                     edi_supp_available_ind,
                     edi_contract_ind,
                     edi_invc_ind,
                     cost_chg_pct_var,
                     cost_chg_amt_var,
                     replen_approval_ind,
                     ship_method,
                     payment_method,
                     contact_telex,
                     contact_email,
                     settlement_code,
                     pre_mark_ind,
                     auto_appr_invc_ind,
                     dbt_memo_code,
                     freight_charge_ind,
                     auto_appr_dbt_memo_ind,
                     prepay_invc_ind,
                     backorder_ind,
                     vat_region,
                     inv_mgmt_lvl,
                     service_perf_req_ind,
                     invc_pay_loc,
                     invc_receive_loc,
                     addinvc_gross_net,
                     delivery_policy,
                     comment_desc,
                     default_item_lead_time,
                     duns_number,
                     duns_loc,
                     bracket_costing_ind,
                     vmi_order_status,
                     dsd_ind,
                     scale_aip_orders,
                     final_dest_ind,
                     sup_qty_level)
             values (L_supplier_record.supplier,
                     L_supplier_record.sup_name,
                     L_supplier_record.sup_name_secondary,
                     L_supplier_record.supplier_parent,
                     NVL(L_supplier_record.contact_name, 'UNKNOWN'),
                     NVL(L_supplier_record.contact_phone, 1),
                     L_supplier_record.contact_fax,
                     L_supplier_record.contact_pager,
                     NVL(L_supplier_record.sup_status, 'A'),
                     NVL(L_supplier_record.qc_ind, 'N'),
                     TO_NUMBER(L_supplier_record.qc_pct),
                     L_supplier_record.qc_freq,
                     NVL(L_supplier_record.vc_ind, 'N'),
                     L_supplier_record.vc_pct,
                     L_supplier_record.vc_freq,
                     L_supplier_record.currency_code,
                     L_supplier_record.lang,
                     L_supplier_record.terms,
                     L_supplier_record.freight_terms,
                     NVL(L_supplier_record.ret_allow_ind, L_ret_allow_ind),
                     NVL(L_supplier_record.ret_auth_req, 'N'),
                     L_supplier_record.ret_min_dol_amt,
                     L_supplier_record.ret_courier,
                     L_supplier_record.handling_pct,
                     NVL(L_supplier_record.edi_po_ind, 'N'),
                     NVL(L_supplier_record.edi_po_chg, 'N'),
                     NVL(L_supplier_record.edi_po_confirm, 'N'),
                     NVL(L_supplier_record.edi_asn, 'N'),
                     decode(L_supplier_record.edi_sales_rpt_freq,'N',NULL,L_supplier_record.edi_sales_rpt_freq),
                     NVL(L_supplier_record.edi_supp_available_ind, 'N'),
                     NVL(L_supplier_record.edi_contract_ind, 'N'),
                     NVL(L_supplier_record.edi_invc_ind, 'N'),
                     L_supplier_record.cost_chg_pct_var,
                     L_supplier_record.cost_chg_amt_var,
                     NVL(L_supplier_record.replen_approval_ind, 'N'),
                     L_supplier_record.ship_method,
                     L_supplier_record.payment_method,
                     L_supplier_record.contact_telex,
                     L_supplier_record.contact_email,
                     NVL(L_supplier_record.settlement_code, 'N'),
                     NVL(L_supplier_record.pre_mark_ind, 'N'),
                     NVL(L_supplier_record.auto_appr_invc_ind,'N'),
                     NVL(L_supplier_record.dbt_memo_code, 'N'),
                     NVL(L_supplier_record.freight_charge_ind,'N'),
                     NVL(L_supplier_record.auto_appr_dbt_memo_ind,'N'),
                     NVL(L_supplier_record.prepay_invc_ind,'N'),
                     NVL(L_supplier_record.backorder_ind,'N'),                     
                     L_supplier_record.vat_region,
                     L_inv_mgmt_lvl,
                     NVL(L_supplier_record.service_perf_req_ind,'N'),
                     L_supplier_record.invc_pay_loc,
                     L_supplier_record.invc_receive_loc,
                     NVL(L_supplier_record.addinvc_gross_net,'N'),
                     NVL(L_supplier_record.delivery_policy,'NEXT'),
                     L_supplier_record.comment_desc,
                     L_supplier_record.default_item_lead_time,
                     L_supplier_record.duns_number,
                     L_supplier_record.duns_loc,
                     NVL(L_supplier_record.bracket_costing_ind,'N'),
                     L_supplier_record.vmi_order_status,
                     NVL(L_supplier_record.dsd_ind,'N'),
                     NVL(L_supplier_record.scale_aip_orders, 'N'),
                     NVL(L_supplier_record.final_dest_ind, 'N'),
                     NVL(L_supplier_record.sup_qty_level, 'EA'));

   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER.INSERT_SUPPLIER',
                                             to_char(SQLCODE));
      return FALSE;

END INSERT_SUPPLIER;
---------------------------------------------------------------------
FUNCTION INSERT_ADDRESS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_table_locked        OUT BOOLEAN,
                        I_supplier_no      IN     SUPS.SUPPLIER%TYPE,
                        I_address_record   IN OUT ADDR%ROWTYPE)

return BOOLEAN IS

   L_address_record ADDR%ROWTYPE := I_address_record;
   L_seq_no ADDR.SEQ_NO%TYPE := NULL;

   cursor C_GET_NEXT_SEQ_NO is
      select NVL(max(seq_no)+1,1)
        from addr
       where key_value_1 = to_char(I_supplier_no)
         and addr_type = L_address_record.addr_type
         and module = 'SUPP';

BEGIN

   open C_GET_NEXT_SEQ_NO;
   fetch C_GET_NEXT_SEQ_NO into L_seq_no;
   close C_GET_NEXT_SEQ_NO;

   -- The first address staged should always be primary --
   if L_seq_no = 1 then
      L_address_record.primary_addr_ind := 'Y';
   end if;

   if L_address_record.seq_no IS NULL then
      L_address_record.seq_no := L_seq_no;
   end if;


   -- Retrieve unique number for the addr_key
   if SUPP_ATTRIB_SQL.NEXT_ADDR(O_error_message,
                                I_address_record.addr_key) = FALSE then
      return FALSE;
   end if;

   INSERT INTO addr(addr_key,
                    module,
                    key_value_1,
                    key_value_2,
                    seq_no,
                    addr_type,
                    primary_addr_ind,
                    add_1,
                    add_2,
                    add_3,
                    city,
                    state,
                    country_id,
                    post,
                    contact_name,
                    contact_phone,
                    contact_fax,
                    contact_telex,
                    contact_email,
                    jurisdiction_code)
             VALUES(I_address_record.addr_key,
                    'SUPP',
                    to_char(I_supplier_no),
                    NULL,
                    L_address_record.seq_no,
                    L_address_record.addr_type,
                    L_address_record.primary_addr_ind,
                    L_address_record.add_1,
                    L_address_record.add_2,
                    L_address_record.add_3,
                    L_address_record.city,
                    L_address_record.state,
                    L_address_record.country_id,
                    L_address_record.post,
                    L_address_record.contact_name,
                    L_address_record.contact_phone,
                    L_address_record.contact_fax,
                    L_address_record.contact_telex,
                    L_address_record.contact_email,
                    L_address_record.jurisdiction_code);
   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER.INSERT_ADDRESS',
                                             to_char(SQLCODE));
      return FALSE;

END INSERT_ADDRESS;
---------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_table_locked     IN OUT BOOLEAN,
                          I_supplier_record  IN     SUPS%ROWTYPE)
return BOOLEAN IS

   L_supplier_record SUPS%ROWTYPE := I_supplier_record;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_input_status    VARCHAR2(1);

   cursor C_LOCK_SUPS is
      select 'x'
        from sups
       where supplier = to_char(I_supplier_record.supplier)
         for update nowait;

   cursor C_SUP_STATUS is
      select sup_status
        from sups 
       where supplier = I_supplier_record.supplier;

BEGIN

   -- Lock the sups table before updating the supplier --
   open C_LOCK_SUPS;
   close C_LOCK_SUPS;
   ---
   open  C_SUP_STATUS;
   fetch C_SUP_STATUS into L_input_status;
   close C_SUP_STATUS;
   
   update sups
      set sup_name               = L_supplier_record.sup_name,
          sup_name_secondary     = L_supplier_record.sup_name_secondary,
          supplier_parent        = L_supplier_record.supplier_parent,
          contact_name           = L_supplier_record.contact_name,
          contact_phone          = L_supplier_record.contact_phone,
          contact_fax            = L_supplier_record.contact_fax,
          contact_pager          = L_supplier_record.contact_pager,
          sup_status             = decode(L_input_status, 'I', 'I', NVL(L_supplier_record.sup_status,sup_status)),
          qc_ind                 = L_supplier_record.qc_ind,
          qc_pct                 = TO_NUMBER(L_supplier_record.qc_pct),
          qc_freq                = L_supplier_record.qc_freq,
          vc_ind                 = L_supplier_record.vc_ind,
          vc_pct                 = L_supplier_record.vc_pct,
          vc_freq                = L_supplier_record.vc_freq,
          currency_code          = L_supplier_record.currency_code,
          lang                   = L_supplier_record.lang,
          terms                  = L_supplier_record.terms,
          freight_terms          = L_supplier_record.freight_terms,
          ret_allow_ind          = L_supplier_record.ret_allow_ind,
          ret_auth_req           = L_supplier_record.ret_auth_req,
          ret_min_dol_amt        = L_supplier_record.ret_min_dol_amt,
          ret_courier            = L_supplier_record.ret_courier,
          handling_pct           = L_supplier_record.handling_pct,
          edi_po_ind             = L_supplier_record.edi_po_ind,
          edi_po_chg             = L_supplier_record.edi_po_chg,
          edi_po_confirm         = L_supplier_record.edi_po_confirm,
          edi_asn                = L_supplier_record.edi_asn,
          edi_sales_rpt_freq     = decode(L_supplier_record.edi_sales_rpt_freq,'N',NULL,L_supplier_record.edi_sales_rpt_freq),
          edi_supp_available_ind = L_supplier_record.edi_supp_available_ind,
          edi_contract_ind       = L_supplier_record.edi_contract_ind,
          edi_invc_ind           = L_supplier_record.edi_invc_ind,
          cost_chg_pct_var       = L_supplier_record.cost_chg_pct_var,
          cost_chg_amt_var       = L_supplier_record.cost_chg_amt_var,
          replen_approval_ind    = L_supplier_record.replen_approval_ind,
          ship_method            = L_supplier_record.ship_method,
          payment_method         = L_supplier_record.payment_method,
          contact_telex          = L_supplier_record.contact_telex,
          contact_email          = L_supplier_record.contact_email,
          settlement_code        = L_supplier_record.settlement_code,
          pre_mark_ind           = L_supplier_record.pre_mark_ind,
          auto_appr_invc_ind     = L_supplier_record.auto_appr_invc_ind,
          dbt_memo_code          = L_supplier_record.dbt_memo_code,
          freight_charge_ind     = L_supplier_record.freight_charge_ind,
          auto_appr_dbt_memo_ind = L_supplier_record.auto_appr_dbt_memo_ind,
          prepay_invc_ind        = L_supplier_record.prepay_invc_ind,
          backorder_ind          = L_supplier_record.backorder_ind,
          vat_region             = L_supplier_record.vat_region,
          inv_mgmt_lvl           = NVL(L_supplier_record.inv_mgmt_lvl,inv_mgmt_lvl),
          service_perf_req_ind   = L_supplier_record.service_perf_req_ind,
          invc_pay_loc           = L_supplier_record.invc_pay_loc,
          invc_receive_loc       = L_supplier_record.invc_receive_loc,
          addinvc_gross_net      = L_supplier_record.addinvc_gross_net,
          delivery_policy        = L_supplier_record.delivery_policy,
          comment_desc           = L_supplier_record.comment_desc,
          default_item_lead_time = L_supplier_record.default_item_lead_time,
          duns_number            = L_supplier_record.duns_number,
          duns_loc               = L_supplier_record.duns_loc,
          bracket_costing_ind    = L_supplier_record.bracket_costing_ind,
          vmi_order_status       = NVL(L_supplier_record.vmi_order_status, vmi_order_status),
          dsd_ind                = dsd_ind,
          scale_aip_orders       = L_supplier_record.scale_aip_orders,
          final_dest_ind         = L_supplier_record.final_dest_ind,
          sup_qty_level          = L_supplier_record.sup_qty_level
    where supplier               = L_supplier_record.supplier;

    -- if parent status is inactive, make the supplier sites also inactive 
    if (LP_system_options.supplier_sites_ind = 'Y' and L_supplier_record.supplier_parent IS NULL) then
        if L_input_status = 'I' or L_supplier_record.sup_status = 'I' then 
          update sups
            set sup_status = 'I'
          where supplier_parent = L_supplier_record.supplier;
        end if;
    end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
       O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'SUPS',
                                             to_char(I_supplier_record.supplier),
                                             'RMSSUB_SUPPLIER.UPDATE_SUPPLIER');
       O_table_locked := TRUE;
       return FALSE;

    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.UPDATE_SUPPLIER',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_SUPPLIER;
-------------------------------------------------------------------------------
FUNCTION UPDATE_ADDRESS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_table_locked     IN OUT BOOLEAN,
                        I_supplier_no      IN     SUPS.SUPPLIER%TYPE,
                        I_address_record   IN     ADDR%ROWTYPE)
return BOOLEAN IS

   L_address_record ADDR%ROWTYPE := I_address_record;

   RECORD_LOCKED EXCEPTION;
   PRAGMA EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ADDR is
      select 'x'
        from addr
       where addr_key = L_address_record.addr_key
         for update nowait;
BEGIN
   -- Lock the address table before updating the supplier --
   open C_LOCK_ADDR;
   close C_LOCK_ADDR;
   ---
   update addr
      set add_1             = L_address_record.add_1,
          add_2             = NVL(L_address_record.add_2, add_2),
          add_3             = NVL(L_address_record.add_3, add_3),
          city              = L_address_record.city,
          state             = L_address_record.state,
          country_id        = L_address_record.country_id,
          post              = NVL(L_address_record.post, post),
          contact_name      = NVL(L_address_record.contact_name,contact_name),
          contact_phone     = NVL(L_address_record.contact_phone,contact_phone),
          contact_fax       = NVL(L_address_record.contact_fax,contact_fax),
          contact_telex     = NVL(L_address_record.contact_telex,contact_telex),
          contact_email     = NVL(L_address_record.contact_email,contact_email),
          jurisdiction_code = NVL(L_address_record.jurisdiction_code,jurisdiction_code),
          primary_addr_ind  = L_address_record.primary_addr_ind 
    where addr_key = L_address_record.addr_key;

   return TRUE;

EXCEPTION
    when RECORD_LOCKED then
       O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'ADDR',
                                             to_char(I_supplier_no),
                                             'RMSSUB_SUPPLIER.UPDATE_ADDRESS');
       O_table_locked := TRUE;
       return FALSE;

    when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.UPDATE_ADDRESS',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ADDRESS;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER_RECORD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_supplier_record  IN OUT SUPS%ROWTYPE,
                                  I_update_ind        IN     VARCHAR2)
return BOOLEAN IS
   L_payment_method        SUPS.PAYMENT_METHOD%TYPE;
   L_exists                VARCHAR2(1) := 'Y';
   L_supplier_parent       SUPS.SUPPLIER_PARENT%TYPE;

   CURSOR C_VAT_REGION(I_vat_region VAT_REGION.VAT_REGION%TYPE) IS
      SELECT vat_region
        FROM vat_region
       WHERE vat_region = I_vat_region;

   CURSOR C_PAYMENT_METHOD is
      select 'X'
        from code_detail
       where code = L_payment_method
         and code_type = 'PAYM';

   CURSOR C_SUPPLIER_PARENT is
      select supplier_parent
        from sups
       where supplier = IO_supplier_record.supplier_parent;

BEGIN

   if CHECK_NULLS(O_error_message,
                  to_char(IO_supplier_record.supplier),
                  'SUPPLIER')= FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  IO_supplier_record.sup_name,
                  'SUP_NAME') = FALSE then
       return FALSE;
   end if;

   if CHECK_CODES(O_error_message,
                  IO_supplier_record.sup_status,
                 'SPST') = FALSE then
      return FALSE;
   end if;

   if I_update_ind != 'Y' then
      if CHECK_NULLS(O_error_message,
                     IO_supplier_record.contact_name,
                    'CONTACT_NAME') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_NULLS(O_error_message,
                     IO_supplier_record.contact_phone,
                     'CONTACT_PHONE') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_NULLS(O_error_message,
                     IO_supplier_record.terms,
                     'TERMS') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_NULLS(O_error_message,
                     IO_supplier_record.freight_terms,
                     'FREIGHT_TERMS') = FALSE then
         RETURN FALSE;
      end if;

      ---

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.qc_ind,
                    'YSNO') = FALSE then
        RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.ret_allow_ind,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.ret_auth_req,
                     'YSNO')= FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.edi_po_ind,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.edi_po_chg,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.edi_po_confirm,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.edi_asn,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.edi_sales_rpt_freq,
                     'ESDF') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.edi_supp_available_ind,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.edi_contract_ind,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.replen_approval_ind,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.settlement_code,
                     'SPSC') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_CODES(O_error_message,
                     IO_supplier_record.pre_mark_ind,
                     'YSNO') = FALSE then
         RETURN FALSE;
      end if;
      ---
      if CHECK_FKEYS(O_error_message,
                     IO_supplier_record.freight_terms,
                     'freight_terms',
                     'freight_terms') = FALSE then
         RETURN FALSE;
      end if;

      if CHECK_FKEYS(O_error_message,
                     IO_supplier_record.currency_code,
                     'currency_code',
                     'currencies') = FALSE  then
         return FALSE;
      end if;

      if CHECK_FKEYS(O_error_message,
                     to_char(IO_supplier_record.lang),
                     'lang',
                     'lang') = FALSE then
         return FALSE;
      end if;

      if CHECK_FKEYS(O_error_message,
                     IO_supplier_record.terms,
                     'terms',
                     'terms') = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options.supplier_sites_ind = 'N' and
      IO_supplier_record.supplier_parent is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SUPP_SITES',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
   end if;

   if LP_system_options.supplier_sites_ind = 'Y' and
      IO_supplier_record.supplier_parent is NOT NULL then
      -- check that supplier site's parent is a valid supplier
      open C_SUPPLIER_PARENT;
      fetch C_SUPPLIER_PARENT into L_supplier_parent;
      if C_SUPPLIER_PARENT%NOTFOUND then
         close C_SUPPLIER_PARENT;
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUPP_PARENT',
                                               IO_supplier_record.supplier_parent,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if L_supplier_parent is NOT NULL then
         close C_SUPPLIER_PARENT;
         O_error_message := SQL_LIB.CREATE_MSG('NO_SUPP_SITE_PARENT',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      End if;
      close C_SUPPLIER_PARENT;
   end if;

   if IO_supplier_record.payment_method is NOT NULL then
      L_payment_method := IO_supplier_record.payment_method;

      OPEN C_PAYMENT_METHOD;
      FETCH C_PAYMENT_METHOD into L_exists;
      CLOSE C_PAYMENT_METHOD;

      if L_exists != 'X' then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PAY_METHOD', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   if LP_system_options.default_tax_type = 'SVAT'  then
      OPEN C_VAT_REGION(IO_supplier_record.vat_region);
      FETCH C_VAT_REGION INTO IO_supplier_record.vat_region;
      if C_VAT_REGION%NOTFOUND then
         IO_supplier_record.vat_region := NULL;
      end if;
      CLOSE C_VAT_REGION;
   else
      IO_supplier_record.vat_region := NULL;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.VALIDATE_SUPPLIER_RECORD',
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SUPPLIER_RECORD;
------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ADDRESS_RECORD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_address_record    IN     ADDR%ROWTYPE,
                                 I_supplier_no       IN     SUPS.SUPPLIER%TYPE)
return BOOLEAN IS

   L_address_record ADDR%ROWTYPE := I_address_record;

BEGIN
   if CHECK_NULLS(O_error_message,
                  to_char(I_supplier_no),
                  'SUPPLIER')= FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_NULLS(O_error_message,
                  L_address_record.addr_type,
                  'ADDR_TYPE') = FALSE then
       return FALSE;
   end if;
   ---
   if CHECK_NULLS(O_error_message,
                  L_address_record.add_1,
                  'ADD_1') = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_NULLS(O_error_message,
                  L_address_record.city,
                  'CITY') = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_NULLS(O_error_message,
                  L_address_record.country_id,
                  'COUNTRY_ID') = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_CODES(O_error_message,
                  L_address_record.primary_addr_ind,
                  'YSNO') = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_FKEYS(O_error_message,
                  L_address_record.addr_type,
                  'address_type',
                  'add_type') = FALSE  then
      return FALSE;
   end if;
   ---
   if CHECK_FKEYS(O_error_message,
                  L_address_record.state,
                  'state',
                  'state') = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_FKEYS(O_error_message,
                  L_address_record.country_id,
                  'country_id',
                  'country') = FALSE then
      return FALSE;
   end if;
   
   if CHECK_FKEYS(O_error_message, 
                  L_address_record.state, 
                  'state', 
                  'state', 
                  L_address_record.country_id, 
                  'country_id') = FALSE then 
       return FALSE; 
    end if;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.VALIDATE_ADDRESS_RECORD',
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ADDRESS_RECORD;
----------------------------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable IN     VARCHAR2,
                      I_record_name     IN     VARCHAR2)
return BOOLEAN IS

BEGIN

   if I_record_variable is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_record_name,
                                            'RMSSUB_SUPPLIER.CHECK_NULLS',
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.CHECK_NULLS',
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_NULLS;
-----------------------------------------------------------------------
FUNCTION CHECK_CODES (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable  IN     VARCHAR2,
                      I_code_value       IN     VARCHAR2)
return BOOLEAN IS

   L_exists VARCHAR2(1) := NULL;

   cursor C_CODES is
      select 'x'
        from code_detail
       where code_type = I_code_value
         and code = I_record_variable;

BEGIN

   if I_record_variable is NOT NULL and I_record_variable != 'N' then
      open C_CODES;
      fetch C_CODES into L_exists;
         if C_CODES%NOTFOUND then
            O_error_message := sql_lib.create_msg('INV_CODE',
                                                  I_record_variable,
                                                  I_code_value,
                                                  'RMSSUB_SUPPLIER.CHECK_CODES');
            return FALSE;
         end if;
      close C_CODES;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.CHECK_CODES',
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CODES;
---------------------------------------------------------------------
FUNCTION CHECK_FKEYS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable   IN       VARCHAR2,
                      I_fkey_column       IN       VARCHAR2,
                      I_fkey_table        IN       VARCHAR2,
                      I_record_variable2  IN       VARCHAR2 DEFAULT NULL,
                      I_fkey_column2      IN       VARCHAR2 DEFAULT NULL) 

RETURN BOOLEAN IS

   V_block_string   VARCHAR2(255) := NULL;
   V_cursor         INTEGER       := NULL;
   L_record_variable VARCHAR2(255) := I_record_variable; 
   L_fkey_column     VARCHAR2(255) := I_fkey_column; 


BEGIN

   if I_record_variable is NOT NULL then
      V_block_string := 'select ''x'' from '
                        || sys.DBMS_ASSERT.SQL_OBJECT_NAME(I_fkey_table)
                        || ' where '
                        ||sys.DBMS_ASSERT.SIMPLE_SQL_NAME(I_fkey_column )
                        ||' = :bind_I_record_variable';
   
   if I_record_variable2 is NOT NULL then 
      V_block_string := V_block_string 
                        ||' and ' 
                        ||sys.DBMS_ASSERT.SIMPLE_SQL_NAME(I_fkey_column2 ) 
                        ||' = :bind_I_record_variable2';    
   end if; 
            
      V_block_string := V_block_string ||' and rownum = 1';  

      V_cursor := DBMS_SQL.OPEN_CURSOR;

      DBMS_SQL.PARSE(V_cursor, V_block_string, DBMS_SQL.V7);
      DBMS_SQL.BIND_VARIABLE(V_cursor, 'bind_I_record_variable', I_record_variable);
      
      if I_record_variable2 is NOT NULL then
             DBMS_SQL.BIND_VARIABLE(V_cursor, 'bind_I_record_variable2',I_record_variable2);
      end if;
      
      if DBMS_SQL.EXECUTE_AND_FETCH(V_cursor) = 0 then
         DBMS_SQL.CLOSE_CURSOR(V_cursor);
         
               if I_record_variable2 is NOT NULL then
                  L_record_variable := I_record_variable || '/' || I_record_variable2;
                  L_fkey_column := I_fkey_column || '/' || I_fkey_column2;
               end if;
               
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_FKEY',
                                               L_record_variable,
                                               L_fkey_column,
                                               I_fkey_table);
         return FALSE;
      else
         DBMS_SQL.CLOSE_CURSOR(V_cursor);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.CHECK_FKEYS',
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FKEYS;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_ADDR (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_supplier_no      IN     SUPS.SUPPLIER%TYPE,
                     I_ret_allow_ind    IN     SUPS.RET_ALLOW_IND%TYPE)
return BOOLEAN IS

   L_addr_count NUMBER(5) := 0;
   L_add_type_module_row  ADD_TYPE_MODULE%ROWTYPE;

   cursor C_ORDER_ADDR is
      select count(*)
        from addr
       where addr_type = '04'
         and key_value_1 = TO_CHAR(I_supplier_no);

   cursor C_RETURN_ADDR is
      select count(*)
        from addr
       where addr_type = '03'
         and key_value_1 = TO_CHAR(I_supplier_no);

   cursor C_INVC_ADDR is
      select count(*)
        from addr
       where addr_type = '05'
         and key_value_1 = TO_CHAR(I_supplier_no);

BEGIN

   open C_ORDER_ADDR;
   fetch C_ORDER_ADDR into L_addr_count;
   close C_ORDER_ADDR;

   if L_addr_count < 1 then
      O_error_message := SQL_LIB.CREATE_MSG('SUP_ORDER_ADDR',
                                            I_supplier_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_ret_allow_ind = 'Y' then
      -- Only check return address if return_allow_ind is 'Y' for this supplier.
      open C_RETURN_ADDR;
      fetch C_RETURN_ADDR into L_addr_count;
      close C_RETURN_ADDR;

      if L_addr_count < 1 then
         O_error_message := SQL_LIB.CREATE_MSG('SUP_RETURN_ADDR',
                                               I_supplier_no,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if ADDRESS_SQL.GET_ADD_TYPE_MODULE_ROW(O_error_message,
                                          L_add_type_module_row,
                                          'SUPP',
                                          '05') = FALSE then
      return FALSE;
   end if;

      -- only check invoice address if invoice address is mandatory.
      open C_INVC_ADDR;
      fetch C_INVC_ADDR into L_addr_count;
      close C_INVC_ADDR;

      if L_addr_count < 1 and L_add_type_module_row.mandatory_ind ='Y' then
         O_error_message := SQL_LIB.CREATE_MSG('SUP_INVC_ADDR',
                                               I_supplier_no,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.CHECK_ADDR',
                                            to_char(SQLCODE));
      return FALSE;
END;
--------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_status_code              IN OUT  VARCHAR2,
                        IO_error_message            IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause                     IN      VARCHAR2,
                        I_program                   IN      VARCHAR2)
IS
BEGIN

   API_LIBRARY.HANDLE_ERRORS(IO_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER.HANDLE_ERRORS',
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(IO_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSSUB_SUPPLIER.HANDLE_ERRORS');



END HANDLE_ERRORS;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_ORG_UNIT_RECORD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_org_unit          IN     PARTNER_ORG_UNIT%ROWTYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSSUB_SUPPLIER_VAL.VALIDATE_ORG_UNIT_RECORD';
   L_org_unit_record PARTNER_ORG_UNIT%ROWTYPE := I_org_unit;

BEGIN

   if CHECK_NULLS(O_error_message,
                  L_org_unit_record.partner,
                  'PARTNER') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  L_org_unit_record.org_unit_id,
                  'ORG_UNIT_ID') = FALSE then
      return FALSE;
   end if;

   if CHECK_FKEYS(O_error_message,
                  L_org_unit_record.org_unit_id,
                  'ORG_UNIT_ID',
                  'ORG_UNIT') = FALSE then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ORG_UNIT_RECORD;
---------------------------------------------------------------------------------
FUNCTION PROCESS_ORGUNIT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_table_locked         IN OUT BOOLEAN,
                         I_partner_org_unit_rec IN     PARTNER_ORG_UNIT%ROWTYPE)
RETURN BOOLEAN IS

   L_ou_partner   PARTNER_ORG_UNIT.PARTNER%TYPE;
   L_ou_org_unit  PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE;

   L_partner_exists VARCHAR2(1) := 'N';

   cursor C_PARTNER_EXISTS is
      select 'Y'
        from partner_org_unit
       where partner     = L_ou_partner
         and org_unit_id = L_ou_org_unit;

BEGIN

   L_ou_partner   := I_partner_org_unit_rec.partner;
   L_ou_org_unit  := I_partner_org_unit_rec.org_unit_id;

   if VALIDATE_ORG_UNIT_RECORD(O_Error_message,
                               I_partner_org_unit_rec) = FALSE then
      return FALSE;
   end if;

   open C_PARTNER_EXISTS;
   fetch C_PARTNER_EXISTS into L_partner_exists;
   close C_PARTNER_EXISTS;

   if L_partner_exists = 'Y' then
      if UPDATE_ORG_UNIT(O_error_message,
                         O_table_locked,
                         I_partner_org_unit_rec) = FALSE then
         return FALSE;
      end if;
   else
      if INSERT_ORG_UNIT(O_error_message,
                         I_partner_org_unit_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER.PROCESS_ORGUNIT',
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_ORGUNIT;
---------------------------------------------------------------------------------
FUNCTION UPDATE_ORG_UNIT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_table_locked         IN OUT BOOLEAN,
                         I_partner_org_unit_rec IN     PARTNER_ORG_UNIT%ROWTYPE)
RETURN BOOLEAN IS

   L_partner_org_record PARTNER_ORG_UNIT%ROWTYPE := I_partner_org_unit_rec;

   RECORD_LOCKED    EXCEPTION;
   PRAGMA EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_POU is
      select 'x'
        from partner_org_unit
       where partner      = L_partner_org_record.partner
         and partner_type = L_partner_org_record.partner_type
         and org_unit_id  = L_partner_org_record.org_unit_id
         for update nowait;
BEGIN
   -- Lock the partner_org_unit table before updating the supplier --
   open C_LOCK_POU;
   close C_LOCK_POU;
   ---
   update PARTNER_ORG_UNIT
      set primary_pay_site     = NVL(L_partner_org_record.primary_pay_site,'N')
    where partner      = L_partner_org_record.partner
      and partner_type = L_partner_org_record.partner_type
      and org_unit_id  = L_partner_org_record.org_unit_id;

   if L_partner_org_record.primary_pay_site = 'Y' then
      if SUPPORG_SQL.UPDATE_PRIM_PAYSITE_N (O_error_message,
                                            L_partner_org_record.partner,
                                            L_partner_org_record.org_unit_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'PARTNER_ORG_UNIT',
                                            to_char(L_partner_org_record.partner),
                                            'RMSSUB_SUPPLIER_SQL.UPDATE_ORG_UNIT');
      O_table_locked := TRUE;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_SUPPLIER_SQL.UPDATE_ORG_UNIT',
                                            to_char(SQLCODE));
     return FALSE;

END UPDATE_ORG_UNIT;
------------------------------------------------------------------------------------------
FUNCTION INSERT_ORG_UNIT(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_partner_org_unit_rec IN     PARTNER_ORG_UNIT%ROWTYPE)
RETURN BOOLEAN IS

   L_partner_org_record PARTNER_ORG_UNIT%ROWTYPE := I_partner_org_unit_rec;

BEGIN

   insert into PARTNER_ORG_UNIT (partner,
                                 org_unit_id,
                                 partner_type,
                                 primary_pay_site)
                         values (L_partner_org_record.partner,
                                 L_partner_org_record.org_unit_id,
                                 L_partner_org_record.partner_type,
                                 L_partner_org_record.primary_pay_site);

   if L_partner_org_record.primary_pay_site = 'Y' then
      if SUPPORG_SQL.UPDATE_PRIM_PAYSITE_N (O_error_message,
                                            L_partner_org_record.partner,
                                            L_partner_org_record.org_unit_id)= FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER_SQL.INSERT_ORG_UNIT',
                                             to_char(SQLCODE));
      return FALSE;

END INSERT_ORG_UNIT;
-----------------------------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER_STATUS(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_supplier_record IN OUT SUPS%ROWTYPE)
RETURN BOOLEAN is

   L_add_1           ADDR.ADD_1%TYPE;
   L_add_2           ADDR.ADD_2%TYPE;
   L_add_3           ADDR.ADD_3%TYPE;
   L_city            ADDR.CITY%TYPE;
   L_state           ADDR.STATE%TYPE;
   L_country_id      ADDR.COUNTRY_ID%TYPE;
   L_post            ADDR.POST%TYPE;
   L_localized_ind   COUNTRY_ATTRIB.LOCALIZED_IND%TYPE;
   L_sup_status      VARCHAR2(1);
   L_input_status    VARCHAR2(1);

   cursor C_CHECK_LOCALIZED_IND is
      select localized_ind
        from country_attrib
       where country_id = L_country_id;

   cursor C_SUP_STATUS is
      select sup_status
        from sups 
       where supplier = IO_supplier_record.supplier;

BEGIN
   if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                L_add_1,
                                L_add_2,
                                L_add_3,
                                L_city,
                                L_state,
                                L_country_id,
                                L_post,
                                'SUPP',
                                IO_supplier_record.supplier,
                                NULL) = FALSE then
      return FALSE;
   end if;

   open  C_SUP_STATUS;
   fetch C_SUP_STATUS into L_input_status;
   close C_SUP_STATUS;
   
   if L_country_id is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_CHECK_LOCALIZED_IND',NULL,NULL);
      open C_CHECK_LOCALIZED_IND;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_LOCALIZED_IND',NULL,NULL);
      fetch C_CHECK_LOCALIZED_IND into L_localized_ind;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_LOCALIZED_IND',NULL,NULL);
      close C_CHECK_LOCALIZED_IND;

      if L_localized_ind = 'Y' then
         if (LP_sup_exists = 'N' and IO_supplier_record.sup_status = 'A') or (LP_sup_exists = 'Y' and L_input_status = 'I') or IO_supplier_record.sup_status = 'I' then
            L_sup_status := 'I';
         else
            L_sup_status := IO_supplier_record.sup_status;
         end if;

         update sups
            set sup_status = L_sup_status
         where supplier = IO_supplier_record.supplier;
      end if ;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER_SQL.UPDATE_SUPPLIER_STATUS',
                                             to_char(SQLCODE));
      return FALSE;

END UPDATE_SUPPLIER_STATUS;
-----------------------------------------------------------------------------------------
END RMSSUB_SUPPLIER;
/