
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRDIV_SQL AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_DIVISION
   -- Purpose      :  Inserts a record on the division table
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_DIVISION
   -- Purpose      :  Updates a record on the division table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_DIVISION
   -- Purpose      :  Deletes a record on the division table
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE,
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_SQL.PERSIST_MESSAGE';

BEGIN

   if I_message_type = RMSSUB_XMRCHHRDIV.LP_cre_type then
      if CREATE_DIVISION(O_error_message,
                         I_division_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRDIV.LP_mod_type then
      if MODIFY_DIVISION(O_error_message,
                         I_division_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_SQL.PERSIST_MESSAGE';

BEGIN

   if DELETE_DIVISION(O_error_message,
                      I_division_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_SQL.CREATE_DIVISION';

BEGIN

   if not MERCH_SQL.INSERT_DIVISION(O_error_message,
                                    I_division_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_DIVISION;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_SQL.MODIFY_DIVISION';

BEGIN

   if not MERCH_SQL.MODIFY_DIVISION(O_error_message,
                                    I_division_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_DIVISION;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_SQL.DELETE_DIVISION';

BEGIN

   if not MERCH_SQL.DELETE_DIVISION(O_error_message,
                                    I_division_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DIVISION;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRDIV_SQL;
/
