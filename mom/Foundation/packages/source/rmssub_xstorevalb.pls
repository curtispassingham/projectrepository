CREATE OR REPLACE PACKAGE BODY RMSSUB_XSTORE_VALIDATE AS

  -- PRIVATE VARIABLES
-------------------------------------------------------------------------------------------------------
   LP_addr_module        add_type_module.module%type := NULL;
   
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the create and modify messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XStoreDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the delete messages
  --                 to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XStoreRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_CONSTRAINT
   -- Purpose      : This function will check the values against the check constraints of the store
   --                or store_add table.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CONSTRAINT(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN              "RIB_XStoreDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_DEFAULT_FIELDS
   -- Purpose      : This function will retrieve all values not defined in the message but required for
   --                RMS integrity.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEFAULT_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_store_rec       IN OUT NOCOPY   STORE_SQL.STORE_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_ORDER_DAYS
   -- Purpose      : This function will determine whether the passed in start order days and stop order
   --                days are greater than or equal to zero.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ORDER_DAYS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_start_order_days   IN       STORE.START_ORDER_DAYS%TYPE,
                          I_stop_order_days    IN       STORE.STOP_ORDER_DAYS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DISTRICT_CHANGE
   -- Purpose      : This function will determine whether the passed in district is different from the
   --                current district.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DISTRICT_CHANGE(O_error_message      IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                               O_current_district   OUT    NOCOPY   DISTRICT.DISTRICT%TYPE,
                               I_new_district       IN              DISTRICT.DISTRICT%TYPE,
                               I_store              IN              STORE.STORE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_LIKE_STORE
   -- Purpose      : This function will check the validity of the like store that is passed.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LIKE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_XStoreDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_CHILD_NODES
   -- Purpose      : This function will check that the child nodes are null when the message
   --                is of Store Create type or that the child nodes are not null when the message is of
   --                Loc_Trait Create or Walkthrough_Store Create types.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_NODES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_XStoreDesc_REC",
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_CHILD_NODES
   -- Purpose      : This function will check that the child nodes are not null when the message
   --                is of Loc_Trait Delete or Walkthrough_store Delete types.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_NODES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_XStoreRef_REC",
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB store object("RIB_XStoreDesc_REC") into a store rec
   --                 defined in the family package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message     IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                         O_store_rec            OUT    NOCOPY STORE_SQL.STORE_REC,
                         I_message           IN               "RIB_XStoreDesc_REC",
                         I_message_type      IN               VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB store object("RIB_XStoreRef_REC") into a store rec
   --                 defined in the family package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_store_rec       OUT    NOCOPY   STORE_SQL.STORE_REC,
                         I_message         IN              "RIB_XStoreRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_STORE_EXISTS
   -- Purpose      : This function will verify that the store and any node being modified
   --                already exist.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_STORE_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_XStoreDesc_REC",
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_STORE_EXISTS
   -- Purpose      : This function will verify that the store and any node being deleted
   --                already exist.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_STORE_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_XStoreRef_REC",
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_DETAIL
   -- Purpose      : This function will populate the detail tables.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DETAIL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_store_rec       IN OUT NOCOPY   STORE_SQL.STORE_REC,
                         I_message         IN              "RIB_XStoreDesc_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_DETAIL
   -- Purpose      : This function will populate the detail tables.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DETAIL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_store_rec       IN OUT NOCOPY   STORE_SQL.STORE_REC,
                         I_message         IN              "RIB_XStoreRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DELETE
   -- Purpose      : This function will verify that the store can be deleted.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XStoreRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ADDR_KEY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_addr_key        OUT      addr.addr_key%TYPE,
                      I_Store           IN       store.store%TYPE,
                      I_module          IN       add_type_module.module%type)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_ADDR_TYPE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_primary_addr_type   OUT      add_type_module.address_type%TYPE,
                               I_module              IN       add_type_module.module%type)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function name: CHECK_WF_CUSTOMER_ID_EXIST
   -- Purpose      : This function will validate WF_CUSTOMER_ID and it's type.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_CUSTOMER_ID_EXIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_wf_customer_id   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
                                    I_store_type       IN       STORE.STORE_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function name: CHECK_ADDRESS_RECORDS
   -- Purpose      : This function ensures that address records are valid.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ADDRESS_RECORDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XStoreDesc_REC",
                               I_message_type    IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
 
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_store_rec       OUT    NOCOPY   STORE_SQL.STORE_REC,
                       I_message         IN              "RIB_XStoreDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(50)                  := 'RMSSUB_XSTORE_VALIDATE.CHECK_MESSAGE';
   L_exists             BOOLEAN;

BEGIN

   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;

   if not CHECK_STORE_EXIST(O_error_message,
                            I_message,
                            I_message_type) then
      return FALSE;
   end if;
   if I_message_type in (RMSSUB_XSTORE.LP_cre_type, RMSSUB_XSTORE.LP_mod_type) then
      if not CHECK_CONSTRAINT(O_error_message,
                              I_message) then
         return FALSE;
      end if;
       
      if not CHECK_ORDER_DAYS(O_error_message,
                              I_message.start_order_days,
                              I_message.stop_order_days) then
         return FALSE;
      end if;
   end if; 
   if I_message_type in (RMSSUB_XSTORE.LP_cre_type, RMSSUB_XSTORE.LP_mod_type, RMSSUB_XSTORE.LP_addr_cre_type, RMSSUB_XSTORE.LP_addr_mod_type) then
      if UPPER(I_message.store_type) = 'F' then
         if not CHECK_WF_CUSTOMER_ID_EXIST(O_error_message,
                                           I_message.wf_customer_id,
                                           I_message.store_type) then
            RETURN FALSE;
          end if;
          LP_addr_module := 'WFST';
       else
          LP_addr_module := 'ST';
       end if;
       
    end if;

   if not CHECK_CHILD_NODES(O_error_message,
                            I_message,
                            I_message_type) then
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XSTORE.LP_cre_type then
      if not CHECK_LIKE_STORE(O_error_message,
                              I_message) then
         return FALSE;
      end if;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_store_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_DEFAULT_FIELDS(O_error_message,
                                  O_store_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_store_rec       OUT    NOCOPY   STORE_SQL.STORE_REC,
                       I_message         IN              "RIB_XStoreRef_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50)                  := 'RMSSUB_XSTORE_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_STORE_EXIST(O_error_message,
                           I_message,
                           I_message_type) then
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XSTORE.LP_del_type then
      if not CHECK_DELETE(O_error_message,
                          I_message) then
         return false;
      end if;
   end if;

   if not CHECK_CHILD_NODES(O_error_message,
                            I_message,
                            I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_store_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XStoreDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program                     VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store', NULL, NULL);
      return FALSE;
   end if;
   if I_message.store_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store_type', NULL, NULL);
      return FALSE;
   end if;
   if I_message_type in(RMSSUB_XSTORE.LP_cre_type, RMSSUB_XSTORE.LP_mod_type) then
      if I_message.store_name is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store_name', NULL, NULL);
         return FALSE;
      end if;

      if I_message.store_mgr_name is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store_mgr_name', NULL, NULL);
         return FALSE;
      end if;

      if I_message.store_open_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store_open_date', NULL, NULL);
         return FALSE;
      end if;

      if I_message.start_order_days is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'start_order_days', NULL, NULL);
         return FALSE;
      end if;
      if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type = 'SVAT'  then
         if I_message.vat_region is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'vat_region', NULL, NULL);
            return FALSE;
         end if;
      end if;
      if I_message.channel_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'channel_id', NULL, NULL);
         return FALSE;
      end if;
      if I_message.district is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'district', NULL, NULL);
         return FALSE;
      end if;

      if I_message.currency_code is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'currency_code', NULL, NULL);
         return FALSE;
      end if;

      if I_message.lang is NULL and I_message.iso_code is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('LANG_ISO_CODE', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.transfer_entity is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'transfer_entity', NULL, NULL);
         return FALSE;
      end if;

      if SYSTEM_OPTIONS_SQL.GP_system_options_row.org_unit_ind = 'Y' then
         if I_message.org_unit_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'org_unit_id', NULL, NULL);
            return FALSE;
         end if;
      end if;
      
      if I_message.tran_no_generated is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'tran_no_generated', NULL, NULL);
         return FALSE;
      end if;

      if I_message.stockholding_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'stockholding_ind', NULL, NULL);
         return FALSE;
      end if;

      if I_message.integrated_pos_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'integrated_pos_ind', NULL, NULL);
         return FALSE;
      end if;

      if I_message.store_class is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store_class', NULL, NULL);
         return FALSE;
      end if;

      if I_message.timezone_name is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'timezone_name', NULL, NULL);
         return FALSE;
      end if;


   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XStoreRef_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'store', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_CONSTRAINT
   -- Purpose      : This function will check the values against the check constraints of the store
   --                or store_add table.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CONSTRAINT(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN              "RIB_XStoreDesc_REC")
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_CONSTRAINT';

BEGIN
   if I_message.copy_activity_ind is not NULL then
      if UPPER(I_message.copy_activity_ind) NOT in ('Y', 'N') then
         O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_YESNO',
                                               'copy_activity_ind');
         return FALSE;
      end if;
   end if;

   if I_message.copy_dlvry_ind is not NULL then
      if UPPER(I_message.copy_dlvry_ind) NOT in ('Y', 'N') then
         O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_YESNO',
                                               'copy_dlvry_ind');
         return FALSE;
      end if;
   end if;

   if I_message.copy_repl_ind is not NULL then
      if UPPER(I_message.copy_repl_ind) NOT in ('Y', 'N') then
         O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_YESNO',
                                               'copy_repl_ind');
         return FALSE;
      end if;
   end if;

   if UPPER(I_message.integrated_pos_ind) NOT in ('Y', 'N') then
      O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_YESNO',
                                            'integrated_pos_ind');
      return FALSE;
   end if;

   if UPPER(I_message.stockholding_ind) NOT in ('Y', 'N') then
      O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_YESNO',
                                            'stockholding_ind');
      return FALSE;
   end if;

   if I_message.vat_include_ind is not NULL then
      if UPPER(I_message.vat_include_ind) NOT in ('Y', 'N') then
         O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_YESNO',
                                               'vat_include_ind');
         return FALSE;
      end if;
   end if;

   if I_message.store_class NOT in ('A', 'B', 'C', 'D', 'E', 'X') then
      O_error_message := SQL_LIB.CREATE_MSG('STORE_CLASS_A_TO_E_OR_X');
      return FALSE;
   end if;

   if I_message.tran_no_generated NOT in ('S', 'R') then
      O_error_message := SQL_LIB.CREATE_MSG('TRAN_NO_GEN_S_R');
      return FALSE;
   end if;

   if UPPER(I_message.store_type) not in ('F','C') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_TYPE');
      return FALSE;
   end if;
   
   if UPPER(I_message.customer_order_loc_ind) is not NULL then 
      if UPPER(I_message.customer_order_loc_ind) NOT in ('Y', 'N') then 
         O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_YESNO', 
                                               'customer_order_loc_ind'); 
         return FALSE; 
      end if; 
   end if; 

   if UPPER(I_message.store_type) = 'F' then
      if I_message.wf_customer_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'wf_customer_id', NULL, NULL);
         return FALSE;
      end if;
      if I_message.store_class != 'X' then
         O_error_message := SQL_LIB.CREATE_MSG('STORE_CLASS_MUST_BE_X');
         return FALSE;
      end if;
      if I_message.default_wh is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'default_wh', NULL, NULL);
         return FALSE;
      end if;
   elsif UPPER(I_message.store_type) = 'C' then
      if I_message.wf_customer_id is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL', 'wf_customer_id', NULL, NULL);
         return FALSE;
      end if;
      if I_message.store_class NOT in ('A', 'B', 'C', 'D', 'E') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_CLASS_COMPANY');
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CONSTRAINT;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEFAULT_FIELDS(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_store_rec       IN OUT NOCOPY  STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.POPULATE_DEFAULT_FIELDS';

BEGIN

   if O_store_rec.store_row.store_name10 is NULL then
      O_store_rec.store_row.store_name10 := RTRIM(SUBSTRB(O_store_rec.store_row.store_name,
                                                          0,
                                                          10));
   end if;

   if O_store_rec.store_row.store_name3 is NULL then
      O_store_rec.store_row.store_name3 := RTRIM(SUBSTRB(O_store_rec.store_row.store_name,
                                                         0,
                                                         3));
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_DEFAULT_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_ORDER_DAYS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_start_order_days   IN       STORE.START_ORDER_DAYS%TYPE,
                          I_stop_order_days    IN       STORE.STOP_ORDER_DAYS%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_ORDER_DAYS';

BEGIN

   if I_start_order_days < 0 then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_DAYS_LESS_ZERO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_stop_order_days < 0 then
      O_error_message := SQL_LIB.CREATE_MSG('STOP_ORDER_DAYS_LESS_ZERO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ORDER_DAYS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DISTRICT_CHANGE(O_error_message      IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                               O_current_district   OUT    NOCOPY   DISTRICT.DISTRICT%TYPE,
                               I_new_district       IN              DISTRICT.DISTRICT%TYPE,
                               I_store              IN              STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_DISTRICT_CHANGE';

BEGIN

   if not STORE_ATTRIB_SQL.GET_STORE_DISTRICT (O_error_message,
                                               O_current_district,
                                               I_store) then
      return FALSE;
   end if;

   if O_current_district = I_new_district then
      O_current_district := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DISTRICT_CHANGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LIKE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_XStoreDesc_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_LIKE_STORE';
   L_loc_type  VARCHAR2(1);

BEGIN

   if I_message.like_store is not NULL then
      if I_message.like_store != I_message.price_store then
         O_error_message := SQL_LIB.CREATE_MSG('MATCH_PRICE_STORE',
                                               I_message.price_store,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.cost_location is not NULL then
         if not LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                             L_loc_type,
                                             I_message.cost_location) then
            return FALSE;
         end if;

         if L_loc_type = 'S' and
            I_message.cost_location != I_message.like_store then
            O_error_message := SQL_LIB.CREATE_MSG('MATCH_COST_LOC',
                                                  I_message.cost_location,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;
   else
      if NVL(UPPER(I_message.copy_activity_ind), 'N') != 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('LIKE_STORE_REQ_ACT',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if NVL(UPPER(I_message.copy_repl_ind), 'N') != 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('LIKE_STORE_REQ',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      if NVL(UPPER(I_message.copy_dlvry_ind), 'N') != 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('LIKE_STORE_REQ_DLVRY',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_LIKE_STORE;

-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_NODES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_XStoreDesc_REC",
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_CHILD_NODES';

BEGIN

   if I_message_type = RMSSUB_XSTORE.LP_cre_type then
      if I_message.xstoreloctrt_TBL is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('DTL_REC_NULL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.xstorewt_TBL is not NULL then
         O_error_message := SQL_LIB.CREATE_MSG('DTL_REC_NULL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_loctrt_cre_type then
      if I_message.xstoreloctrt_TBL is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_wt_cre_type then
      if I_message.xstorewt_TBL is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      else
         FOR i in I_message.xstorewt_tbl.first..I_message.xstorewt_tbl.last LOOP
            if I_message.store = I_message.xstorewt_tbl(i).walk_through_store then
               O_error_message := SQL_LIB.CREATE_MSG('STORE_NOT_WT_STORE',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;
         END LOOP;
      end if;
   end if;

   if I_message_type in (RMSSUB_XSTORE.LP_cre_type, RMSSUB_XSTORE.LP_addr_cre_type, RMSSUB_XSTORE.LP_addr_mod_type) then
      if I_message.addrdesc_TBL is NULL and I_message.addrdesc_TBL.count = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      else
         if CHECK_ADDRESS_RECORDS(O_error_message,
                                  I_message,
                                  I_message_type) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CHILD_NODES;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_NODES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_XStoreRef_REC",
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_CHILD_NODES';
   L_addr_rec_exists   VARCHAR2(1);

   cursor C_ADDR_REC_EXISTS(L_external_ref_id   IN   ADDR.EXTERNAL_REF_ID%TYPE) is
     select 'x'
       from addr
      where external_ref_id = L_external_ref_id;

BEGIN

   if I_message_type = RMSSUB_XSTORE.LP_loctrt_del_type then
      if I_message.xstoreloctrt_TBL is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_wt_del_type then
      if I_message.xstorewt_TBL is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_addr_del_type then
      if I_message.addrref_TBL is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      FOR i in I_message.addrref_TBL.first..I_message.addrref_TBL.last LOOP

         open C_ADDR_REC_EXISTS(I_message.addrref_tbl(i).addr);
         fetch C_ADDR_REC_EXISTS into L_addr_rec_exists;
         close C_ADDR_REC_EXISTS;

         

         if I_message.addrref_TBL(i).addr is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                                  'addr',
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         if L_addr_rec_exists is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ADDRESS',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ADDR_REC_EXISTS%ISOPEN then
         close C_ADDR_REC_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CHILD_NODES;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message     IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                         O_store_rec            OUT    NOCOPY STORE_SQL.STORE_REC,
                         I_message           IN               "RIB_XStoreDesc_REC",
                         I_message_type      IN               VARCHAR2)
  RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.POPULATE_RECORD';
   L_loc_type            VARCHAR2(1)  := 'S';
   L_entity_name         TSF_ENTITY.TSF_ENTITY_DESC%TYPE;
   L_ok_to_change        BOOLEAN;

   L_count               NUMBER;
   L_iso_code            LANG.ISO_CODE%TYPE;
   L_lang                LANG.LANG%TYPE;
   L_addrdesc_rec        "RIB_AddrDesc_REC";
   L_addr_rec            ADDR%ROWTYPE;
   L_org_unit_record     ORG_UNIT%ROWTYPE;
   L_tsf_entity_id       TSF_ENTITY.TSF_ENTITY_ID%TYPE;
   L_exists              BOOLEAN;
   L_currency_code       FIF_GL_SETUP.CURRENCY_CODE%TYPE;
   L_set_of_books_id     FIF_GL_SETUP.set_of_books_id%TYPE;

   cursor C_COUNT_CODE is
      select count(iso_code)
        from lang
       where iso_code = L_iso_code;

   cursor C_LANG is
      select lang
        from lang
       where ISO_CODE = L_iso_code;

BEGIN
   L_iso_code            :=I_message.iso_code;

   O_store_rec.store_row.store                := I_message.store;
   O_store_rec.store_row.store_name           := I_message.store_name;
   O_store_rec.store_row.store_name10         := I_message.store_name10;
   O_store_rec.store_row.store_name3          := I_message.store_name3;
   O_store_rec.store_row.store_name_secondary := I_message.store_name_secondary;
   O_store_rec.store_row.store_class          := UPPER(I_message.store_class);
   O_store_rec.store_row.store_mgr_name       := I_message.store_mgr_name;
   O_store_rec.store_row.store_open_date      := I_message.store_open_date;
   O_store_rec.store_row.store_close_date     := I_message.store_close_date;
   O_store_rec.store_row.acquired_date        := I_message.acquired_date;
   O_store_rec.store_row.remodel_date         := I_message.remodel_date;
   O_store_rec.store_row.fax_number           := I_message.fax_number;
   O_store_rec.store_row.phone_number         := I_message.phone_number;
   O_store_rec.store_row.email                := I_message.email;
   O_store_rec.store_row.total_square_ft      := I_message.total_square_ft;
   O_store_rec.store_row.selling_square_ft    := I_message.selling_square_ft;
   O_store_rec.store_row.linear_distance      := I_message.linear_distance;
   O_store_rec.store_row.vat_region           := I_message.vat_region;

   if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type IN ('SVAT','GTAX') and
      SYSTEM_OPTIONS_SQL.GP_system_options_row.class_level_vat_ind ='N' then
      O_store_rec.store_row.vat_include_ind      := UPPER(I_message.vat_include_ind);
   else
      O_store_rec.store_row.vat_include_ind := 'N';
   end if;
   
   if (I_message.store_type = 'C' or (I_message.store_type = 'F' and UPPER(I_message.stockholding_ind) = 'Y')) 
       and I_message.customer_order_loc_ind is NULL then 
          O_store_rec.store_row.customer_order_loc_ind := 'N'; 
   else 
      if I_message.store_type = 'F' and UPPER(I_message.stockholding_ind) = 'N' then 
         O_store_rec.store_row.customer_order_loc_ind := NULL; 
      else                
         O_store_rec.store_row.customer_order_loc_ind := I_message.customer_order_loc_ind;      
      end if; 
   end if;

   O_store_rec.store_row.stockholding_ind     := UPPER(I_message.stockholding_ind);
   O_store_rec.store_row.channel_id           := I_message.channel_id;
   O_store_rec.store_row.store_format         := I_message.store_format;
   O_store_rec.store_row.mall_name            := I_message.mall_name;
   O_store_rec.store_row.district             := I_message.district;
--   O_store_rec.store_row.promo_zone           := I_message.promo_zone;     --PROMO_ZONE depracated in Retek11
   O_store_rec.store_row.transfer_zone        := I_message.transfer_zone;
   O_store_rec.store_row.default_wh           := I_message.default_wh;
   O_store_rec.store_row.stop_order_days      := I_message.stop_order_days;
   O_store_rec.store_row.start_order_days     := I_message.start_order_days;
   O_store_rec.store_row.currency_code        := UPPER(I_message.currency_code);
   O_store_rec.store_row.timezone_name        := I_message.timezone_name;
   O_store_rec.store_row.store_type           := I_message.store_type;
   O_store_rec.store_row.wf_customer_id       := I_message.wf_customer_id;

   if I_message.lang is NOT NULL then
      O_store_rec.store_row.lang                 := I_message.lang;
   else
      if I_message.iso_code is NOT NULL then
         sql_lib.set_mark('OPEN', 'C_count_code', 'lang', NULL);
         open C_count_code;
         sql_lib.set_mark('FETCH', 'C_count_code', 'lang', NULL);
         fetch C_count_code into L_count;
         if L_count = 1 then
            sql_lib.set_mark('OPEN', 'C_lang', 'lang', NULL);
            open C_lang;
            sql_lib.set_mark('FETCH', 'C_lang', 'lang', NULL);
            fetch C_lang into L_lang;
            O_store_rec.store_row.lang := L_lang;
            sql_lib.set_mark('CLOSE', 'C_lang', 'lang', NULL);
            close C_lang;
         else
            if L_count = 0 then
               O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'ISO_CODE', I_message.iso_code, NULL);
               return FALSE;
            else
               O_error_message := SQL_LIB.CREATE_MSG('ISO_NOT_UNIQUE', NULL, NULL, NULL);
               return FALSE;
            end if;

         end if;
         sql_lib.set_mark('CLOSE', 'C_count_code', 'lang', NULL);
         close C_count_code;
      end if;
   end if;

   O_store_rec.store_row.like_store           := I_message.like_store;
   O_store_rec.store_row.price_store          := I_message.price_store;
   O_store_rec.store_row.cost_location        := I_message.cost_location;
   O_store_rec.store_row.tran_no_generated    := UPPER(I_message.tran_no_generated);
   O_store_rec.store_row.integrated_pos_ind   := UPPER(I_message.integrated_pos_ind);
   ---
   O_store_rec.store_row.copy_repl_ind        := NVL(UPPER(I_message.copy_repl_ind), 'N');
   O_store_rec.store_row.copy_activity_ind    := NVL(UPPER(I_message.copy_activity_ind), 'N');
   O_store_rec.store_row.copy_dlvry_ind       := NVL(UPPER(I_message.copy_activity_ind), 'N');
   ---
   O_store_rec.store_row.duns_number          := I_message.duns_number;
   O_store_rec.store_row.duns_loc             := I_message.duns_loc;
   O_store_rec.store_row.sister_store         := I_message.sister_store;
   ---

   -- set store transfer entity
   if I_message.transfer_entity is not NULL then
      if I_message_type = RMSSUB_XSTORE.LP_cre_type then
         -- set it to what's on the message for create
         O_store_rec.store_row.tsf_entity_id := I_message.transfer_entity;
         --
      elsif I_message_type = RMSSUB_XSTORE.LP_mod_type then
         O_store_rec.store_row.tsf_entity_id := I_message.transfer_entity;

         -- validate it for mod
         -- default to the existing store tsf entity
         if not LOCATION_ATTRIB_SQL.GET_ENTITY(O_error_message,
                                               O_store_rec.store_row.tsf_entity_id,
                                               L_entity_name,
                                               TO_CHAR(I_message.store),
                                               L_loc_type) then
            return FALSE;
         end if;

         -- if changed, check if it's ok to change
         -- if yes, change it; if not, leave it as the existing
         if O_store_rec.store_row.tsf_entity_id != I_message.transfer_entity then
            if not LOCATION_ATTRIB_SQL.CHECK_TSF_ENTITY_CHANGE(O_error_message,
                                                               L_ok_to_change,
                                                               I_message.store) then
               return FALSE;
            end if;

            if L_ok_to_change = TRUE then
               O_store_rec.store_row.tsf_entity_id := I_message.transfer_entity;
            end if;
         end if;
      end if;
   end if;
   ---

   if SYSTEM_OPTIONS_SQL.GP_system_options_row.org_unit_ind = 'Y'
      and I_message.org_unit_id is not NULL then
      -- Check if org_unit exists
      if not ORG_UNIT_SQL.GET_ORG_UNIT(O_error_message,
                                       L_org_unit_record,
                                       I_message.org_unit_id) then
         return FALSE;
      end if;

      if not SET_OF_BOOKS_SQL.GET_TSF_ENTITY(O_error_message,
                                             L_tsf_entity_id,
                                             L_exists,
                                             I_message.org_unit_id,
                                             NULL) then
         return FALSE;
      end if;

      if O_store_rec.store_row.tsf_entity_id is not NULL then
         if O_store_rec.store_row.tsf_entity_id <> L_tsf_entity_id then
            O_error_message := SQL_LIB.CREATE_MSG('ORG_UNIT_NO_TSF_ENTITY', NULL, NULL, NULL);
            return FALSE;
         end if;
      else
         O_store_rec.store_row.tsf_entity_id := L_tsf_entity_id;
      end if;
      O_store_rec.store_row.org_unit_id := I_message.org_unit_id;

      if not SET_OF_BOOKS_SQL.GET_SOB_CURRENCY(O_error_message,
                                               L_currency_code,
                                               L_set_of_books_id,
                                               I_message.org_unit_id) then
         return FALSE;
      end if;
      if O_store_rec.store_row.currency_code <> L_currency_code then
         O_error_message := SQL_LIB.CREATE_MSG('NO_MATCH_SOB_CURRENCY', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   if I_message_type = RMSSUB_XSTORE.LP_mod_type then
      --null;
      if not CHECK_DISTRICT_CHANGE(O_error_message,
                                   O_store_rec.original_district_id,
                                   O_store_rec.store_row.district,
                                   O_store_rec.store_row.store) then
         return FALSE;
      end if;
   else
      if not POPULATE_DETAIL(O_error_message,
                             O_store_rec,
                             I_message,
                             I_message_type) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_COUNT_CODE%ISOPEN then
         close C_COUNT_CODE;
      end if;
      if C_LANG%ISOPEN then
         close C_LANG;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_store_rec       OUT    NOCOPY   STORE_SQL.STORE_REC,
                         I_message         IN              "RIB_XStoreRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.POPULATE_RECORD';

BEGIN

   O_store_rec.store_row.store                := I_message.store;

   if I_message_type != RMSSUB_XSTORE.LP_del_type then
      if not POPULATE_DETAIL(O_error_message,
                             O_store_rec,
                             I_message,
                             I_message_type) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_STORE_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_XStoreDesc_REC",
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_STORE_EXIST';
   L_exists    BOOLEAN;
   L_store     STORE_ADD.STORE%TYPE;
   L_dummy     VARCHAR2(1) := NULL;
   L_store_row   STORE%ROWTYPE;

   cursor C_STORE_ADD_EXISTS is
      select 'x'
        from store_add
       where store = L_store;

BEGIN

   L_store := I_message.store;

   if not LOCATION_ATTRIB_SQL.ID_EXISTS (O_error_message,
                                         L_exists,
                                         I_message.store) then
      return FALSE;
   end if;

   if L_exists then
      if I_message_type = RMSSUB_XSTORE.LP_cre_type then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE');
         return FALSE;
      else
         if not STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                         L_exists,
                                         L_store_row,
                                         I_message.store) then
            return FALSE;
         end if;

         if not L_exists then
            SQL_LIB.SET_MARK('OPEN', 'C_STORE_ADD_EXISTS', 'STORE', 'store: '||L_store);
            open C_STORE_ADD_EXISTS;
            SQL_LIB.SET_MARK('FETCH', 'C_STORE_ADD_EXISTS', 'STORE', 'store: '||L_store);
            fetch C_STORE_ADD_EXISTS into L_dummy;
            SQL_LIB.SET_MARK('CLOSE', 'C_STORE_ADD_EXISTS', 'STORE', 'store: '||L_store);
            close C_STORE_ADD_EXISTS;
            if L_dummy is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INV_STORE');
               return FALSE;
            else
               O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_ADD');
               return FALSE;
            end if;
         else
            if I_message.store_type <> L_store_row.store_type then
               O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_TYPE');
               return FALSE;
            end if;
         end if;
      end if;
   else

      if I_message_type != RMSSUB_XSTORE.LP_cre_type then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE');
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_STORE_ADD_EXISTS%ISOPEN then
         close C_STORE_ADD_EXISTS;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_STORE_EXIST;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_STORE_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_message         IN       "RIB_XStoreRef_REC",
                           I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_STORE_EXIST';
   L_exists    BOOLEAN;

BEGIN

   if not STORE_VALIDATE_SQL.EXIST(O_error_message,
                                   I_message.store,
                                   L_exists) then
      return FALSE;
   end if;

   if not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('NO_STORE_DEL');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_STORE_EXIST;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DETAIL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_store_rec       IN OUT NOCOPY   STORE_SQL.STORE_REC,
                         I_message         IN              "RIB_XStoreDesc_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.POPULATE_DETAIL';
   L_primaddr_rec                 OBJ_PRIMADDR_REC  := OBJ_PRIMADDR_REC(NULL,NULL,NULL,NULL);
   L_primaddr_tbl                 OBJ_PRIMADDR_TBL  := OBJ_PRIMADDR_TBL();
   L_primary_addr_exists          VARCHAR2(1);
   L_primary_addr_exists_type     VARCHAR2(1);
   L_cnt                          NUMBER;
   L_updaddr_rec                  OBJ_PRIMADDR_REC  := OBJ_PRIMADDR_REC(NULL,NULL,NULL,NULL);
   L_updaddr_tbl                  OBJ_PRIMADDR_TBL  := OBJ_PRIMADDR_TBL();
   
   
   
   
   cursor C_CHK_PRIM_ADDR is 
      select OBJ_PRIMADDR_REC(a.key_value_1,
             a.primary_addr_ind,
             a.addr_type,
             a.external_ref_id)
        from addr a,
             (select tbl.store,
                     tbl.primary_addr_ind,
                     tbl.addr_type,
                     tbl.external_ref_id
                 from TABLE (CAST(L_primaddr_tbl AS OBJ_PRIMADDR_TBL)) tbl
             ) prim_tbl
        where a.primary_addr_ind = prim_tbl.primary_addr_ind
          and a.primary_addr_ind ='Y'
          and a.key_value_1 = TO_CHAR(prim_tbl.store)
          and a.addr_type = prim_tbl.addr_type
          and a.module =LP_addr_module;
          
   cursor C_PRIMARY_ADDR_EXISTS(L_addr_type IN ADDR.ADDR_TYPE%TYPE) is
      select 'x'
        from addr
       where key_value_1      = TO_CHAR(I_message.store)
         and addr_type        = TO_CHAR(L_addr_type)
         and module           = LP_addr_module
         and primary_addr_ind = 'Y';
         
         
   cursor C_CHECK_PRIMARY_ADDR(L_addr_type IN ADDR.ADDR_TYPE%TYPE,L_external_ref_id IN ADDR.EXTERNAL_REF_ID%TYPE,L_prim_addr_ind IN ADDR.PRIMARY_ADDR_IND%TYPE) is
      select 'x'
        from addr
       where key_value_1      = TO_CHAR(I_message.store)
         and addr_type        = TO_CHAR(L_addr_type)
         and module           = LP_addr_module
         and primary_addr_ind = 'Y'
         and (external_ref_id not in (select external_ref_id 
                                       from addr 
                                      where external_ref_id = TO_CHAR(L_external_ref_id)
                                        and addr_type        = TO_CHAR(L_addr_type)
                                        and primary_addr_ind ='Y')
              OR L_prim_addr_ind = 'Y');
   
   cursor C_CHECK_PRIMARY_TYPE_EXISTS(L_addr_type IN ADDR.ADDR_TYPE%TYPE,L_external_ref_id IN ADDR.EXTERNAL_REF_ID%TYPE) is
        select 'x'
          from TABLE (CAST(L_primaddr_tbl AS OBJ_PRIMADDR_TBL))
         where store = TO_CHAR(I_message.store)
           and addr_type = TO_CHAR(L_addr_type)
           and primary_addr_ind ='Y'
           and external_ref_id not in (select external_ref_id 
                                       from addr 
                                      where external_ref_id = L_external_ref_id
                                        and addr_type        = TO_CHAR(L_addr_type)
                                        and primary_addr_ind ='Y') ;     
   cursor C_PRIMARY_ADDR_REQ(L_addr_type IN ADDR.ADDR_TYPE%TYPE) is
      select COUNT(*) cnt
        from TABLE (CAST(L_primaddr_tbl AS OBJ_PRIMADDR_TBL)) 
       where primary_addr_ind ='Y'
         and addr_type = L_addr_type
         and exists(select null 
                      from TABLE (CAST(L_primaddr_tbl AS OBJ_PRIMADDR_TBL)) 
                     where primary_addr_ind ='Y'
                       and addr_type = L_addr_type);
                
          
BEGIN

   O_store_rec.loc_trait_tbl                  := ORGANIZATION_SQL.LOC_TRAIT_TBL();
   O_store_rec.walk_through_tbl               := STORE_SQL.WALK_THROUGH_STORE_TBL();

   if I_message_type = RMSSUB_XSTORE.LP_wt_cre_type and I_message.xstorewt_tbl is not NULL then
      FOR i in I_message.xstorewt_tbl.first..I_message.xstorewt_tbl.last LOOP
         O_store_rec.walk_through_tbl.EXTEND;
         O_store_rec.walk_through_tbl(i).walk_through_store := I_message.xstorewt_tbl(i).walk_through_store;
         O_store_rec.walk_through_tbl(i).store              := I_message.store;
      END LOOP;
   else
      O_store_rec.walk_through_tbl := NULL;
   end if;

   if I_message_type = RMSSUB_XSTORE.LP_loctrt_cre_type and I_message.xstoreloctrt_tbl is not NULL then
      FOR i in I_message.xstoreloctrt_tbl.first..I_message.xstoreloctrt_tbl.last LOOP
         O_store_rec.loc_trait_tbl.EXTEND;
         O_store_rec.loc_trait_tbl(i).loc_trait_id          := I_message.xstoreloctrt_tbl(i).loc_trait;
      END LOOP;
   else
      O_store_rec.loc_trait_tbl := NULL;
   end if;

   if I_message_type in (RMSSUB_XSTORE.LP_cre_type, RMSSUB_XSTORE.LP_addr_cre_type, RMSSUB_XSTORE.LP_addr_mod_type) and I_message.addrdesc_tbl is not NULL then
      FOR i in I_message.addrdesc_tbl.first..I_message.addrdesc_tbl.last LOOP
         O_store_rec.address_tbl(i).external_ref_id := I_message.addrdesc_tbl(i).addr;
         O_store_rec.address_tbl(i).module := LP_addr_module;
         O_store_rec.address_tbl(i).key_value_1 := I_message.store;
         O_store_rec.address_tbl(i).addr_type := I_message.addrdesc_tbl(i).addr_type;
         O_store_rec.address_tbl(i).primary_addr_ind := I_message.addrdesc_tbl(i).primary_addr_ind;
         O_store_rec.address_tbl(i).add_1 := I_message.addrdesc_tbl(i).add_1;
         O_store_rec.address_tbl(i).add_2 := I_message.addrdesc_tbl(i).add_2;
         O_store_rec.address_tbl(i).add_3 := I_message.addrdesc_tbl(i).add_3;
         O_store_rec.address_tbl(i).city := I_message.addrdesc_tbl(i).city;
         O_store_rec.address_tbl(i).state := UPPER(I_message.addrdesc_tbl(i).state);
         O_store_rec.address_tbl(i).country_id := UPPER(I_message.addrdesc_tbl(i).country_id);
         O_store_rec.address_tbl(i).post := I_message.addrdesc_tbl(i).post;
         O_store_rec.address_tbl(i).contact_name := I_message.addrdesc_tbl(i).contact_name;
         O_store_rec.address_tbl(i).contact_phone := I_message.addrdesc_tbl(i).contact_phone;
         O_store_rec.address_tbl(i).contact_telex := I_message.addrdesc_tbl(i).contact_telex;
         O_store_rec.address_tbl(i).contact_fax := I_message.addrdesc_tbl(i).contact_fax;
         O_store_rec.address_tbl(i).contact_email := I_message.addrdesc_tbl(i).contact_email;
         O_store_rec.address_tbl(i).oracle_vendor_site_id := I_message.addrdesc_tbl(i).oracle_vendor_site_id;
         O_store_rec.address_tbl(i).county := I_message.addrdesc_tbl(i).county;
         O_store_rec.address_tbl(i).publish_ind := 'N';
         O_store_rec.address_tbl(i).jurisdiction_code := I_message.addrdesc_tbl(i).jurisdiction_code;
         
         L_primaddr_rec.store := I_message.store;
         L_primaddr_rec.primary_addr_ind := I_message.addrdesc_tbl(i).primary_addr_ind;
         L_primaddr_rec.external_ref_id := I_message.addrdesc_tbl(i).addr;
         L_primaddr_rec.addr_type       := I_message.addrdesc_tbl(i).addr_type;
         L_primaddr_tbl.EXTEND;
         L_primaddr_tbl(L_primaddr_tbl.COUNT) := L_primaddr_rec;
         
         if I_message_type = RMSSUB_XSTORE.LP_addr_mod_type then
            
            if ADDRESS_SQL.GET_ADDR_KEY(O_error_message,
                                        O_store_rec.address_tbl(i).addr_key,
                                        I_message.addrdesc_tbl(i).addr) = FALSE then
               return FALSE;
            end if;
            
         end if;
      END LOOP;
      
      if I_message_type = RMSSUB_XSTORE.LP_addr_mod_type then
         FOR i in 1..L_primaddr_tbl.COUNT LOOP
          
            FOR rec in(select count(*) cnt,
                              addr_type,
                              store,
                              primary_addr_ind
                         from TABLE (CAST(L_primaddr_tbl AS OBJ_PRIMADDR_TBL)) 
                     group by store,
                              addr_type,
                              primary_addr_ind) LOOP
               if rec.cnt > 1 and rec.primary_addr_ind ='Y' then
                  O_error_message := SQL_LIB.CREATE_MSG('MULT_PRIMARY_ADDRESS',
                                                        L_primaddr_tbl(i).addr_type,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;         
            END LOOP;              
            L_primary_addr_exists := NULL;
            open C_CHECK_PRIMARY_ADDR(L_primaddr_tbl(i).addr_type,L_primaddr_tbl(i).external_ref_id, L_primaddr_tbl(i).primary_addr_ind);
            fetch C_CHECK_PRIMARY_ADDR into L_primary_addr_exists;
            close C_CHECK_PRIMARY_ADDR;

            if L_primary_addr_exists IS NULL then
               L_primary_addr_exists_type := NULL;
               open C_CHECK_PRIMARY_TYPE_EXISTS(L_primaddr_tbl(i).addr_type,L_primaddr_tbl(i).external_ref_id);
               fetch C_CHECK_PRIMARY_TYPE_EXISTS into L_primary_addr_exists_type;
               close C_CHECK_PRIMARY_TYPE_EXISTS;

               if L_primary_addr_exists_type is  NULL  then
                  O_error_message := SQL_LIB.CREATE_MSG('PRIMARY_ADD_REQ',
                                                        L_primaddr_tbl(i).addr_type,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;
             end if;   
        END LOOP;
      end if;
      L_primary_addr_exists := NULL;
      
      if I_message_type in (RMSSUB_XSTORE.LP_cre_type, RMSSUB_XSTORE.LP_addr_cre_type) then
      
        FOR i in 1..L_primaddr_tbl.count loop
           open C_PRIMARY_ADDR_REQ(L_primaddr_tbl(i).addr_type);
           fetch C_PRIMARY_ADDR_REQ into L_cnt;
           close C_PRIMARY_ADDR_REQ;
           if L_cnt > 1 then
              O_error_message := SQL_LIB.CREATE_MSG('MULT_PRIMARY_ADDRESS',
                                                    L_primaddr_tbl(i).addr_type,
                                                    NULL,
                                                    NULL);
              return FALSE;
           end if;
         
           open C_PRIMARY_ADDR_EXISTS(L_primaddr_tbl(i).addr_type);
           fetch C_PRIMARY_ADDR_EXISTS into L_primary_addr_exists;
           close C_PRIMARY_ADDR_EXISTS;

           if l_cnt = 0 and  L_primary_addr_exists IS NULL then
              O_error_message := SQL_LIB.CREATE_MSG('PRIMARY_ADD_REQ',
                                                    L_primaddr_tbl(i).addr_type,
                                                    NULL,
                                                    NULL);
               return FALSE;
            end if  ;
            
        END LOOP;
        
      end if;
      if I_message_type in (RMSSUB_XSTORE.LP_addr_cre_type,RMSSUB_XSTORE.LP_addr_mod_type) then  
         
         open C_CHK_PRIM_ADDR;
         fetch C_CHK_PRIM_ADDR bulk collect into L_updaddr_tbl;
         close C_CHK_PRIM_ADDR;
    
         if L_updaddr_tbl is NOT NULL and L_updaddr_tbl.COUNT > 0 then
            FORALL i in L_updaddr_tbl.FIRST..L_updaddr_tbl.LAST
              update addr
                 set primary_addr_ind = 'N'
               where key_value_1 = TO_CHAR(L_updaddr_tbl(i).store)
               and  addr_type =L_updaddr_tbl(i).addr_type
               and  module = LP_addr_module
               and  primary_addr_ind ='Y'; 
         end if;
      end if;
   else
      O_store_rec.address_tbl.delete;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHK_PRIM_ADDR%ISOPEN then
         close C_CHK_PRIM_ADDR;
      end if;
      if C_PRIMARY_ADDR_EXISTS%ISOPEN then
         close C_PRIMARY_ADDR_EXISTS;
      end if;
      if C_CHECK_PRIMARY_ADDR%ISOPEN then
         close C_CHECK_PRIMARY_ADDR;
      end if;
      if C_CHECK_PRIMARY_TYPE_EXISTS%ISOPEN then
         close C_CHECK_PRIMARY_TYPE_EXISTS;
      end if;   

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DETAIL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_store_rec       IN OUT NOCOPY   STORE_SQL.STORE_REC,
                         I_message         IN              "RIB_XStoreRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.POPULATE_DETAIL';
   L_addr_key   ADDR.ADDR_KEY%TYPE;
   
BEGIN
   O_store_rec.loc_trait_tbl                  := ORGANIZATION_SQL.LOC_TRAIT_TBL();
   O_store_rec.walk_through_tbl               := STORE_SQL.WALK_THROUGH_STORE_TBL();

   if I_message_type = RMSSUB_XSTORE.LP_wt_del_type and I_message.xstorewt_tbl is not NULL then
      FOR i in I_message.xstorewt_tbl.first..I_message.xstorewt_tbl.last LOOP
         O_store_rec.walk_through_tbl.EXTEND;
         O_store_rec.walk_through_tbl(i).walk_through_store := I_message.xstorewt_tbl(i).walk_through_store;
         O_store_rec.walk_through_tbl(i).store              := I_message.store;
      END LOOP;
   else
      O_store_rec.walk_through_tbl := NULL;
   end if;

   if I_message_type = RMSSUB_XSTORE.LP_loctrt_del_type and I_message.xstoreloctrt_tbl is not NULL then
      FOR i in I_message.xstoreloctrt_tbl.first..I_message.xstoreloctrt_tbl.last LOOP
         O_store_rec.loc_trait_tbl.EXTEND;
         O_store_rec.loc_trait_tbl(i).loc_trait_id          := I_message.xstoreloctrt_tbl(i).loc_trait;
      END LOOP;
   else
      O_store_rec.loc_trait_tbl := NULL;
   end if;

   if I_message_type = RMSSUB_XSTORE.LP_addr_del_type and I_message.addrref_tbl is not NULL then
   
      FOR i in I_message.addrref_tbl.first..I_message.addrref_tbl.last LOOP
         if ADDRESS_SQL.GET_ADDR_KEY(O_error_message,
                                     L_addr_key,
                                     I_message.addrref_tbl(i).addr) = FALSE then
            return FALSE;
         end if;
         O_store_rec.address_tbl(i).key_value_1             := I_message.store;
         O_store_rec.address_tbl(i).external_ref_id         := I_message.addrref_tbl(i).addr;
         O_store_rec.address_tbl(i).addr_key                := L_addr_key;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XStoreRef_REC")
   RETURN BOOLEAN IS

   L_program             VARCHAR2(60)  := 'RMSSUB_XSTORE_VALIDATE.CHECK_DELETE';
   L_exist               VARCHAR2(1);
BEGIN

   -- checks for foreign key relationships which, if they exist, would result in the store not being deleted.
   if not VALIDATE_RECORDS_SQL.DEL_STORE(I_message.store,
                                        L_exist,
                                        O_error_message) then
      return FALSE;
   end if;
   if L_exist = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('CNT_DEL_REC', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DELETE;
-------------------------------------------------------------------------------------------------------

FUNCTION GET_PRIMARY_ADDR_TYPE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_primary_addr_type   OUT      add_type_module.address_type%TYPE,
                               I_module              IN       add_type_module.module%type)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'RMSSUB_XSTORE_VALIDATE.GET_PRIMARY_ADDR_TYPE';

BEGIN

   if ADDRESS_SQL.GET_PRIMARY_ADDR_TYPE(O_error_message,
                                        O_primary_addr_type,
                                        I_module) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIMARY_ADDR_TYPE;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ADDR_KEY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_addr_key        OUT      addr.addr_key%TYPE,
                      I_Store           IN       store.store%TYPE,
                      I_module          IN       add_type_module.module%type)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'RMSSUB_XSTORE_VALIDATE.GET_ADDR_KEY';

BEGIN

   if ADDRESS_SQL.GET_ADDR_KEY(O_error_message,
                               O_addr_key,
                               I_store,
                               I_module) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ADDR_KEY;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_WF_CUSTOMER_ID_EXIST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_wf_customer_id   IN       WF_CUSTOMER.WF_CUSTOMER_ID%TYPE,
                                    I_store_type       IN       STORE.STORE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_WF_CUSTOMER_ID_EXIST';
   L_exists           BOOLEAN;
   L_wf_customer      WF_CUSTOMER%ROWTYPE;

BEGIN

   if not WF_CUSTOMER_SQL.WF_CUSTOMER_ID_EXISTS (O_error_message,
                                                  L_exists,
                                                  I_wf_customer_id) then
      return FALSE;
   end if;

   if not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_WF_CUSTOMER_ID',I_wf_customer_id,NULL,NULL);
      return FALSE;
   else
      if WF_CUSTOMER_SQL.GET_WF_CUSTOMER_INFO(O_error_message,
                                              L_wf_customer,
                                              I_wf_customer_id) = FALSE then
         return FALSE;
      end if;
      ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_WF_CUSTOMER_ID_EXIST;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ADDRESS_RECORDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XStoreDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   TYPE add_type_tbl is TABLE OF ADD_TYPE_MODULE.ADDRESS_TYPE%TYPE;
   L_program                    VARCHAR2(50) := 'RMSSUB_XSTORE_VALIDATE.CHECK_ADDRESS_RECORDS';
   L_addr_rec_exists            VARCHAR2(1);
   L_mand_addr_type_exists      BOOLEAN := FALSE;
   L_mandatory_addr_type_tbl    ADD_TYPE_TBL := ADD_TYPE_TBL();
   L_addr_type_desc             ADD_TYPE_TL.TYPE_DESC%TYPE;
   L_req_value                  VARCHAR2(30) := NULL;
   L_desc                       COUNTRY.COUNTRY_DESC%TYPE;
   L_country                    COUNTRY.COUNTRY_ID%TYPE;
   L_state                      STATE.STATE%TYPE;
   L_external_ref_id            ADDR.EXTERNAL_REF_ID%TYPE;
   L_valid_state                VARCHAR2(1);
   L_jurisdiction_code          COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE;
   L_valid_juris                VARCHAR2(1);

   cursor C_ADDR_REC_EXISTS is
     select 'x'
       from addr
      where external_ref_id = L_external_ref_id;

   cursor C_GET_MAND_ADD_TYPE is
      select address_type
        from add_type_module
       where mandatory_ind = 'Y'
         and module = LP_addr_module;

   cursor C_JURISDICTION is
      select 'x'
        from country_tax_jurisdiction
       where jurisdiction_code = L_jurisdiction_code
         and state = L_state
         and country_id = L_country;
         
   cursor C_VALID_JURIS is
      select 'x'
         from country_tax_jurisdiction
        where jurisdiction_code = L_jurisdiction_code;
         

BEGIN
   FOR rec in C_GET_MAND_ADD_TYPE LOOP
      L_mandatory_addr_type_tbl.EXTEND;
      L_mandatory_addr_type_tbl(L_mandatory_addr_type_tbl.LAST) := rec.address_type;
   END LOOP;
   
   if I_message_type = RMSSUB_XSTORE.LP_cre_type then
       --make sure that at least one address record is a mandatory address type when creating a store
       FOR j in L_mandatory_addr_type_tbl.first..L_mandatory_addr_type_tbl.last LOOP
          L_mand_addr_type_exists := FALSE;
          FOR i in I_message.addrdesc_TBL.first..I_message.addrdesc_TBL.last LOOP
             if I_message.addrdesc_TBL(i).addr_type = L_mandatory_addr_type_tbl(j) then
                L_mand_addr_type_exists := TRUE;
             end if;
          END LOOP;
       END LOOP;
   end if;
   
   FOR i in I_message.addrdesc_TBL.first..I_message.addrdesc_TBL.last LOOP
       
      L_country := UPPER(I_message.addrdesc_TBL(i).country_id);
      L_state := UPPER(I_message.addrdesc_TBL(i).state);
      L_external_ref_id := I_message.addrdesc_TBL(i).addr;
      if I_message.addrdesc_TBL(i).jurisdiction_code is not null then
         L_jurisdiction_code := I_message.addrdesc_TBL(i).jurisdiction_code;
      end if;
      L_req_value := NULL;
     
      if I_message.addrdesc_TBL(i).addr is NULL then
         L_req_value := 'addr';
      elsif I_message.addrdesc_TBL(i).addr_type is NULL then
         L_req_value := 'addr_type';
      elsif I_message.addrdesc_TBL(i).primary_addr_ind is NULL then
         L_req_value := 'primary_addr_ind';
      elsif I_message.addrdesc_TBL(i).add_1 is NULL then
         L_req_value := 'add_1';
      elsif I_message.addrdesc_TBL(i).city is NULL then
         L_req_value := 'city';
      elsif L_country is NULL then
         L_req_value := 'country_id';
      end if;

      if L_req_value is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                               L_req_value,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
    
      open C_ADDR_REC_EXISTS;
      fetch C_ADDR_REC_EXISTS into L_addr_rec_exists;
      close C_ADDR_REC_EXISTS;

      if I_message_type IN (RMSSUB_XSTORE.LP_cre_type, RMSSUB_XSTORE.LP_addr_cre_type) then
         --check that the address does not already exist for a cre type
         if L_addr_rec_exists is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('DUP_ADDRESS',
                                                  I_message.addrdesc_TBL(i).addr,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;
      if I_message_type IN (RMSSUB_XSTORE.LP_cre_type, RMSSUB_XSTORE.LP_addr_cre_type, RMSSUB_XSTORE.LP_addr_mod_type) then
         if ADDRESS_SQL.GET_ADDR_TYPE_DESC(O_error_message,
                                           L_addr_type_desc,
                                           I_message.addrdesc_TBL(i).addr_type) = FALSE then
            return FALSE;
         end if;
         if L_jurisdiction_code is NOT NULL then
            open C_VALID_JURIS;
            fetch C_VALID_JURIS into L_valid_juris;
            close C_VALID_JURIS;
            
            if L_valid_juris is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('INV_TAX_JURIS',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if; 
          
            L_valid_juris :=  NULL;
          
            open C_JURISDICTION;
            fetch C_JURISDICTION into L_valid_juris;
            close C_JURISDICTION;
            
            if L_state is NOT NULL and L_country is NOT NULL and L_valid_juris is NULL then
               O_error_message := SQL_LIB.CREATE_MSG('NO_CNTRY_ST_JURIS',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;    
         end if;
        
         if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                          L_country,
                                          L_desc) = FALSE then
            return FALSE;
         end if;
         L_desc :=  NULL;
         if L_state is NOT NULL then
        
            if GEOGRAPHY_SQL.STATE_DESC(O_error_message,
                                        L_desc,
                                        L_state,
                                        L_country) = FALSE then
               return FALSE;
            end if;
         end if;   
      end if;

   END LOOP;
   if NOT L_mand_addr_type_exists and I_message_type = RMSSUB_XSTORE.LP_cre_type then
      O_error_message := SQL_LIB.CREATE_MSG('NO_MAND_ADD_TYPE',
                                            I_message.store,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ADDR_REC_EXISTS%ISOPEN then
         close C_ADDR_REC_EXISTS;
      end if; 
      if C_GET_MAND_ADD_TYPE%ISOPEN then
         close C_GET_MAND_ADD_TYPE;
      end if;   
      if C_JURISDICTION%ISOPEN then
         close C_JURISDICTION;
      end if;   
      if C_VALID_JURIS%ISOPEN then
         close C_VALID_JURIS;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ADDRESS_RECORDS;
------------------------------------------------------------------------------------------
END RMSSUB_XSTORE_VALIDATE;
/
