
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XITEM_POP_RECORD AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
--PUBLIC FUNCTIONS
----------------------------------------------------------------------------
---------------------------------------------------------------------------
FUNCTION POPULATE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                  IO_item_rec       IN OUT NOCOPY   RMSSUB_XITEM.ITEM_API_REC,
                  IO_message        IN OUT          "RIB_XItemDesc_REC",
                  I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION POPULATE(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE, 
                  O_item_rec        OUT   NOCOPY   RMSSUB_XITEM.ITEM_API_DEL_REC,                      
                  I_message         IN             "RIB_XItemRef_REC",
                  I_message_type    IN             VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XITEM_POP_RECORD;
/
