
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRCOMP AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHANGE_CASE
   -- Purpose      : This private function should convert specific char fields from the rib message to
   --                uppercase and convert the message type to lowercase.
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XMrchHrCompDesc_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code          IN OUT  VARCHAR2,
                        IO_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause                IN      VARCHAR2,
                        I_program              IN      VARCHAR2);
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code   IN OUT   VARCHAR2,
                  O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN       RIB_OBJECT,
                  I_message_type  IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_message               "RIB_XMrchHrCompDesc_REC";
   L_company_rec           COMPHEAD%ROWTYPE;
   L_message_type          VARCHAR2(15) := I_message_type;

BEGIN
   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   if LOWER(I_message_type) in  (LP_cre_type , LP_mod_type) then

      L_message := treat(I_MESSAGE AS "RIB_XMrchHrCompDesc_REC");

      if L_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      if CHANGE_CASE(O_error_message,
                     L_message,
                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Validate Message Contents
      if RMSSUB_XMRCHHRCOMP_VALIDATE.CHECK_MESSAGE(O_error_message,
                                                   L_company_rec,
                                                   L_message,
                                                   L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---

      -- INSERT/UPDATE table
      if RMSSUB_XMRCHHRCOMP_SQL.PERSIST(O_error_message,
                                        L_company_rec,
                                        L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type,'NULL'), NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     'RMSSUB_XMRCHHRCOMP.CONSUME');
   when OTHERS then

       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     'RMSSUB_XMRCHHRCOMP.CONSUME');

END CONSUME;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XMrchHrCompDesc_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'RMSSUB_XMRCHHRCOMP.CHANGE_CASE';

BEGIN

   IO_message_type           := LOWER(IO_message_type);
   IO_message.state          := UPPER(IO_message.state);
   IO_message.country_code   := UPPER(IO_message.country_code);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHANGE_CASE;

-------------------------------------------------------------------------------------------------------
   --PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code               IN OUT  VARCHAR2,
                        IO_error_message            IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause                     IN      VARCHAR2,
                        I_program                   IN      VARCHAR2) IS

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);



EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_XMRCHHRCOMP.HANDLE_ERRORS',
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSSUB_XMRCHHRCOMP.HANDLE_ERRORS');

END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRCOMP;
/
