CREATE OR REPLACE PACKAGE GEOGRAPHY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
FUNCTION VALIDATE_STATE(i_state       IN     VARCHAR2,
                        o_exists      IN OUT VARCHAR2,
                        error_message IN OUT VARCHAR2) 
                        RETURN BOOLEAN;
--------------------------------------------------------------------
FUNCTION COUNTRY_DESC(	O_error_message IN OUT VARCHAR2,
			I_country       IN     VARCHAR2,
			O_desc		IN OUT VARCHAR2)
			RETURN BOOLEAN;
--------------------------------------------------------------------
FUNCTION STATE_DESC(O_error_message IN OUT VARCHAR2,
                    O_desc          IN OUT state.description%TYPE,
                    I_state         IN     state.state%TYPE,
                    I_country       IN     state.country_id%TYPE)
                    RETURN BOOLEAN;
--------------------------------------------------------------------         
END;
/


