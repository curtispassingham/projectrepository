CREATE OR REPLACE PACKAGE ITEMLOC_ATTRIB_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------
-- Function Name: GET_AV_COST
-- Purpose      : This function takes a item, loc, and loc_type
--                and gets the average cost
-- Calls        : ITEMLOC_ATTRIB_SQL.GET_DETAILS
--------------------------------------------------------------------
FUNCTION GET_AV_COST(O_error_message IN OUT VARCHAR2,
                     I_item          IN     item_master.item%TYPE,
                     I_loc           IN     item_loc.loc%TYPE,
                     I_loc_type      IN     item_loc.loc_type%TYPE,
                     O_av_cost       IN OUT item_loc_soh.av_cost%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: GET_COSTS_AND_RETAILS
-- Purpose      : This function takes a item, loc, loc_type
--                and gets the costs and retails
-- Calls        : ITEMLOC_ATTRIB_SQL.GET_DETAILS
--------------------------------------------------------------------
FUNCTION GET_COSTS_AND_RETAILS(O_error_message         IN OUT VARCHAR2,
                               I_item                  IN     item_loc.item%TYPE,
                               I_loc                   IN     item_loc.loc%TYPE,
                               I_loc_type              IN     item_loc.loc_type%TYPE,
                               O_av_cost               IN OUT item_loc_soh.av_cost%TYPE,
                               O_unit_cost             IN OUT item_loc_soh.unit_cost%TYPE,
                               O_unit_retail           IN OUT item_loc.unit_retail%TYPE,
                               O_selling_unit_retail   IN OUT item_loc.selling_unit_retail%TYPE,
                               O_selling_uom           IN OUT item_loc.selling_uom%TYPE)
RETURN BOOLEAN;

FUNCTION GET_COSTS_AND_RETAILS(O_error_message                IN OUT VARCHAR2,
                               I_item                         IN     item_loc.item%TYPE,
                               I_loc                          IN     item_loc.loc%TYPE,
                               I_loc_type                     IN     item_loc.loc_type%TYPE,
                               I_nonsellable_pack_retail_ind  IN     VARCHAR2,
                               O_av_cost                      IN OUT item_loc_soh.av_cost%TYPE,
                               O_unit_cost                    IN OUT item_loc_soh.unit_cost%TYPE,
                               O_unit_retail                  IN OUT item_loc.unit_retail%TYPE,
                               O_selling_unit_retail          IN OUT item_loc.selling_unit_retail%TYPE,
                               O_selling_uom                  IN OUT item_loc.selling_uom%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: GET_AV_COST_SOH
-- Purpose      : This function takes a item, loc, and loc_type
--                and gets the average cost and stock on hand for that item/loc
-- Calls        : ITEMLOC_ATTRIB_SQL.GET_DETAILS
--  NOTE: soh is not populated for items above the transaction level 
--------------------------------------------------------------------
FUNCTION GET_AV_COST_SOH (O_error_message IN OUT  VARCHAR2,
                          I_item          IN      item_loc.item%TYPE,
                          I_loc           IN      item_loc.loc%TYPE,
                          I_loc_type      IN      item_loc.loc_type%TYPE,
                          O_av_cost       IN OUT  item_loc_soh.av_cost%TYPE,
                          O_stock_on_hand IN OUT  item_loc_soh.stock_on_hand%TYPE,
                          O_pack_comp_soh IN OUT  item_loc_soh.pack_comp_soh%TYPE)
RETURN BOOLEAN; 
--------------------------------------------------------------------
-- Function Name: LOCATIONS_EXIST 
-- Purpose      : This function takes an item.
--                It will check to see if the given item 
--                is set up at either a store or wh.  
--------------------------------------------------------------------
FUNCTION LOCATIONS_EXIST (O_error_message IN OUT  VARCHAR2,
                          I_item          IN      item_loc.item%TYPE,
                          I_loc_type      IN      item_loc.loc_type%TYPE,
                          O_exist         IN OUT  BOOLEAN)
RETURN BOOLEAN; 
-------------------------------------------------------------------- 
--FUNCTION NAME: ITEM_LOC_EXIST
--PURPOSE:  This function will take an item and a location and determine
--          if the relationship exists.  If an item/loc record is found
--          then the parameter O_exists will return TRUE.  Otherwise,
--          O_exists = FALSE.
---------------------------------------------------------------------
FUNCTION ITEM_LOC_EXIST(O_error_message IN OUT VARCHAR2,
                        I_item          IN     item_loc.item%TYPE,
                        I_loc           IN     item_loc.loc%TYPE,
                        O_exists        IN OUT BOOLEAN) 
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------- 
--FUNCTION NAME: ITEM_WH_EXIST
--PURPOSE:  This function will take an item and a physical wh and determine
--          if any virtual warehouse whose physical wh is I_pwh is associated with the item
--          or not.If an item/loc record is found then the parameter O_exists will return TRUE.
--          Otherwise,O_exists = FALSE.
-----------------------------------------------------------------------------------------------
FUNCTION ITEM_WH_EXIST(O_error_message IN OUT VARCHAR2,
                        I_item          IN     item_loc.item%TYPE,
                        I_pwh           IN     item_loc.loc%TYPE,
                        O_exists        IN OUT BOOLEAN) 
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function:  ITEM_STATUS
-- Purpose :  This function accepts an item/location and determines
--            the status of the item at that location.
---------------------------------------------------------------------------------
FUNCTION ITEM_STATUS(O_error_message IN OUT VARCHAR2,
                     I_item          IN     item_loc.item%TYPE,
                     I_loc           IN     item_loc.loc%TYPE,
                     O_status        IN OUT item_loc.status%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: ITEM_IN_ACTIVE_PACK
-- Purpose      : checks to see if the passed item is in a 
--                pack and if that pack is in active status
--                at the given location.
---------------------------------------------------------------------------------------
FUNCTION ITEM_IN_ACTIVE_PACK(O_error_message  IN OUT VARCHAR2,
                             I_item           IN     item_loc.item%type,
                             I_loc            IN     item_loc.loc%type,
                             O_active         IN OUT BOOLEAN) 
RETURN BOOLEAN; 
----------------------------------------------------------------------------------------
-- Function Name:  STOCK_EXISTS
-- Purpose      :  Checks if an item has any type of stock at
--                 any location that does not equal zero.  
---------------------------------------------------------------------------------------- 
FUNCTION STOCK_EXISTS(O_error_message      IN OUT VARCHAR2,
                      O_exists             IN OUT BOOLEAN,
                      I_item               IN     item_loc.item%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Function Name:  GET_SUPPLIER_CNTRY
--Purpose      :  retrieve an item/location's primary supplier/primary country
-------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_CNTRY(O_error_message      IN OUT VARCHAR2,
                            O_supplier           IN OUT item_loc.primary_supp%TYPE,
                            O_origin_country_id  IN OUT item_loc.primary_cntry%TYPE,
                            I_item               IN     item_loc.item%TYPE,
                            I_loc                IN     item_loc.loc%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Function Name:  GET_DAILY_WASTE_PCT
--Purpose      :  retrieve an item/location's daily wastage percentage.
-------------------------------------------------------------------------------
FUNCTION GET_DAILY_WASTE_PCT(O_error_message   IN OUT VARCHAR2,
                             O_daily_waste_pct IN OUT item_loc.daily_waste_pct%TYPE,
                             I_item            IN     item_loc.item%TYPE,
                             I_loc             IN     item_loc.loc%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name:  GET_STATUS_TAXABLE
-- Purpose      :  retrieve an item/location's status and taxable indicator for a
--                 passed item/loc.
------------------------------------------------------------------------------------
FUNCTION GET_STATUS_TAXABLE(O_error_message   IN OUT VARCHAR2,
                            O_status          IN OUT item_loc.status%TYPE,
                            O_taxable_ind     IN OUT item_loc.taxable_ind%TYPE,
                            I_item            IN     item_loc.item%TYPE,
                            I_loc             IN     item_loc.loc%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function:  ITEMLOC_STATUS
-- Purpose :  This function accepts an item/location and determines
--            the status of the item at that location.  It also handles
--            physical warehouses in a MC environment.
---------------------------------------------------------------------------------
FUNCTION ITEMLOC_STATUS(O_error_message IN OUT VARCHAR2,
                        O_status        IN OUT item_loc.status%TYPE,
                        I_item          IN     item_loc.item%TYPE,
                        I_loc           IN     item_loc.loc%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION GET_AV_UNIT_COST(O_error_message   IN OUT VARCHAR2,
                          O_av_cost         IN OUT item_loc_soh.av_cost%TYPE,
                          O_unit_cost       IN OUT item_loc_soh.unit_cost%TYPE,
                          I_item            IN     item_loc_soh.item%TYPE,
                          I_loc             IN     item_loc_soh.loc%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION GET_RECEIVE_AS_TYPE(O_error_message   IN OUT VARCHAR2,
                             O_receive_as_type IN OUT item_loc.receive_as_type%TYPE,
                             I_item            IN     item_loc.item%TYPE,
                             I_loc             IN     item_loc.loc%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function:  NON_ACTIVE_STORES_EXIST
-- Purpose :  This function returns O_exists = TRUE if any of the stores associated
--            with the passed item are in inactive status.
---------------------------------------------------------------------------------
FUNCTION NON_ACTIVE_STORES_EXIST(O_error_message IN OUT VARCHAR2,
                                 O_exist         IN OUT BOOLEAN,
                                 I_item          IN     ITEM_LOC.ITEM%TYPE,
                                 I_type          IN     CODE_DETAIL.CODE%TYPE,
                                 I_value         IN     VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function:  NON_ACTIVE_WH_EXIST
-- Purpose :  This function returns O_exists = TRUE if any of the wh associated
--            with the passed item are in inactive status.
---------------------------------------------------------------------------------
FUNCTION NON_ACTIVE_WH_EXIST(O_error_message IN OUT VARCHAR2,
                             O_exist         IN OUT BOOLEAN,
                             I_item          IN     ITEM_LOC.ITEM%TYPE,
                             I_type          IN     CODE_DETAIL.CODE%TYPE,
                             I_value         IN     VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Function: INT_FINISHER_EXIST
-- Purpose:  This function will check to see whether or not the passed-in warehouse
--           from item_loc table is an internal finisher.
---------------------------------------------------------------------------------
FUNCTION INT_FINISHER_EXIST(O_error_message IN OUT VARCHAR2,
                            O_exist         IN OUT BOOLEAN,
                            I_item          IN     ITEM_LOC.ITEM%TYPE,
                            I_location      IN     ITEM_LOC.LOC%TYPE,
                            I_value         IN     CODE_DETAIL.CODE%TYPE DEFAULT 'W')
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function: GET_ITEMLOC_INFO
--  Purpose: This function retrieves the whole row for a item/loc combination.

FUNCTION GET_ITEMLOC_INFO(O_error_message IN OUT VARCHAR2,
                          O_itemloc       IN OUT ITEM_LOC%ROWTYPE,
                          I_item          IN     ITEM_LOC.ITEM%TYPE,
                          I_loc           IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: COMP_UNIT_REATIL_EXIST
--       Purpose: This function check whether all the components of the pack have
--                set up unit retail, if yes, return TRUE to O_exist.
FUNCTION COMP_UNIT_REATIL_EXIST(O_error_message IN OUT VARCHAR2,
                                O_exist         IN OUT BOOLEAN,
                                I_pack_no       IN     ITEM_LOC.ITEM%TYPE,
                                I_loc           IN     ITEM_LOC.LOC%TYPE)    
RETURN BOOLEAN;   
----------------------------------------------------------------------------------
-- Function Name: GET_WAC
--       Purpose: This function will return the unit_cost of an item at a location
--                based on the different accounting methods: cost vs retail accounting
--                and standard cost vs average cost accounting. O_wac will be in local
--                currency. Since unit_retail does not exist at an external finisher, 
--                when I_loc_type = 'E' and I_dept has retail accounting, the alternative
--                location (I_alt_loc, I_alt_loc_type) will be used instead.
----------------------------------------------------------------------------------     
FUNCTION GET_WAC(O_error_message         IN OUT rtk_errors.rtk_text%TYPE,
                 O_wac                   IN OUT item_loc_soh.unit_cost%TYPE,
                 I_item                  IN     item_loc.item%TYPE,
                 I_dept                  IN     item_master.dept%TYPE,
                 I_class                 IN     item_master.class%TYPE,
                 I_subclass              IN     item_master.subclass%TYPE,
                 I_loc                   IN     item_loc.loc%TYPE,
                 I_loc_type              IN     item_loc.loc_type%TYPE,
                 I_tran_date             IN     DATE,
                 I_alt_loc               IN     item_loc.loc%TYPE default NULL,
                 I_alt_loc_type          IN     item_loc.loc_type%TYPE default NULL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_AVERAGE_WEIGHT
--       Purpose: This function will return the average weight on ITEM_LOC_SOH for
--                an item/location. The average weight should be defined.
----------------------------------------------------------------------------------     
FUNCTION GET_AVERAGE_WEIGHT(O_error_message         IN OUT rtk_errors.rtk_text%TYPE,
                            O_average_weight        IN OUT item_loc_soh.unit_cost%TYPE,
                            I_item                  IN     item_loc.item%TYPE,
                            I_loc                   IN     item_loc.loc%TYPE,
                            I_loc_type              IN     item_loc.loc_type%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: VALID_DEAL_ITEM
--       Purpose: This function will validate that items have an existing 
--                item/location record in item_loc with a status of other than inactive
----------------------------------------------------------------------------------
FUNCTION VALID_DEAL_ITEM(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid           IN OUT BOOLEAN,
                         I_item            IN     ITEM_LOC.ITEM%TYPE,
                         I_loc             IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_merch_lvl       IN     CODE_DETAIL.CODE%TYPE,
                         I_diff            IN     ITEM_MASTER.DIFF_1%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: LOCLIST_ITEM_LOCS_EXIST
--       Purpose: This function will determine if the item is ranged to any of the
--                locations on the location list. 
----------------------------------------------------------------------------------
FUNCTION LOCLIST_ITEM_LOCS_EXIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exist           IN OUT BOOLEAN,
                                 I_item            IN     ITEM_LOC.ITEM%TYPE,
                                 I_loc_list        IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_LOC_AV_COST
--       Purpose: This function will get compute the AV_COST for non localized
--                locations. This will serv e as NBF for the AV_COST cursor in
--                ITEMLOC_ATTRIB_SQL.GET_DETAILS.
----------------------------------------------------------------------------------
FUNCTION GET_LOC_AV_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_l10n_obj       IN OUT   L10N_OBJ)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: GET_CURRENT_SOH_WAC
--       Purpose: This function will retrieve the SOH for a given location
--                if the location is not localized.
FUNCTION GET_CURRENT_SOH_WAC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_l10n_obj       IN OUT   L10N_OBJ)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Function Name: CHECK_RANGED_IND
--       Purpose: This function is called from replattr.fmb and checks if there
--                are any incidentally ranged locations(ranged_ind = 'N') being put on
--                replenishment.
----------------------------------------------------------------------------------
FUNCTION CHECK_RANGED_IND(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists           IN OUT   BOOLEAN,
                          I_item             IN       ITEM_LOC.ITEM%TYPE,
                          I_type             IN       CODE_DETAIL.CODE%TYPE,
                          I_value            IN       VARCHAR2)
   return BOOLEAN;
----------------------------------------------------------------------------------
-- Name:       UPDATE_RANGED_IND
-- Purpose:    This function will set ITEM_LOC.RANGED_IND = 'Y for the item/location
--             combination that is incidentally ranged.
------------------------------------------------------------------------------------
FUNCTION UPDATE_RANGED_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item            IN       ITEM_LOC.ITEM%TYPE,
                           I_type            IN       CODE_DETAIL.CODE%TYPE,
                           I_value           IN       VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
END ITEMLOC_ATTRIB_SQL;
/
