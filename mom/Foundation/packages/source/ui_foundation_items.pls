CREATE OR REPLACE PACKAGE FOUNDATION_ITEM_SQL AUTHID CURRENT_USER IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : itemfind (Form Module)
  -- Source Object            : PROCEDURE  -> FORM_VALIDATION.VPN
  ---------------------------------------------------------------------------------------------
FUNCTION VPN(O_error_message      IN OUT   VARCHAR2,
             I_TI_SUPPLIER        IN       NUMBER,
             I_TI_SUPPLIER_SITE   IN       NUMBER,
             I_TI_VPN             IN       VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : itemudam (Form Module)
  -- Source Object            : PROCEDURE  -> FORM_VALIDATION.VALIDATE_ALL
  ---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ALL(O_error_message      IN OUT   VARCHAR2,
                      I_DISPLAY_TYPE       IN       VARCHAR2,
                      I_SINGLE_VALUE_IND   IN       VARCHAR2,
                      I_ITEM0101           IN       VARCHAR2,
                      I_ITEM01             IN       VARCHAR2,
                      I_ITEM               IN       VARCHAR2,
                      I_UDA_VALUE          IN       NUMBER,
                      P_PM_UDA_ID          IN       NUMBER,
                      SYSTEM_RECORD_STATUS IN       VARCHAR2,
                      I_UDA_ROWID                   ROWID)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : mclocn (Form Module)
  -- Source Object            : PROCEDURE  -> P_DELETE
  ---------------------------------------------------------------------------------------------
FUNCTION DELETE_MCLOCN(O_error_message   IN OUT   VARCHAR2,
                       I_LI_TYPE         IN       VARCHAR2,
                       I_TI_VALUE        IN       VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : mcmrhier (Form Module)
  -- Source Object            : PROCEDURE  -> P_DELETE_REJECTS
  ---------------------------------------------------------------------------------------------
FUNCTION DELETE_REJECTS(O_error_message         IN OUT   VARCHAR2,
                        P_PIntrnlVrbleIvUsrId   IN       VARCHAR2)
   return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : slhead (Form Module)
  -- Source Object            : PROCEDURE  -> P_DELETE_TPG_LIST_ITEMS
  ---------------------------------------------------------------------------------------------
FUNCTION DELETE_TPG_LIST_ITEMS(O_error_message   IN OUT   VARCHAR2,
                               O_ret             IN OUT   BOOLEAN,
                               I_SKULIST         IN       NUMBER)
   return BOOLEAN;
END FOUNDATION_ITEM_SQL;
/
