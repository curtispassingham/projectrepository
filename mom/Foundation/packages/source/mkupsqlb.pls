CREATE OR REPLACE PACKAGE BODY MARKUP_SQL AS
-------------------------------------------------------------------------------
FUNCTION CALC_MARKUP_PERCENT(O_error_message    IN OUT VARCHAR2,
                             O_markup_percent   IN OUT NUMBER,
                             I_markup_calc_type IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                             I_total_cost       IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                             I_total_retail     IN     ITEM_LOC.UNIT_RETAIL%TYPE)
   RETURN BOOLEAN IS

BEGIN

     if I_markup_calc_type = 'C' then
      if I_total_cost = 0 then
         O_markup_percent := 0;
      else
         O_markup_percent := ROUND((I_total_retail - I_total_cost) / I_total_cost * 100, 4);
      end if;
   elsif I_markup_calc_type = 'R' then
      if I_total_retail = 0 then
         O_markup_percent := 0;
      else
         O_markup_percent := ROUND((I_total_retail - I_total_cost) / I_total_retail * 100, 4);
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_MARKUP_TYPE_VALUE', I_markup_calc_type);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'MARKUP_SQL.CALC_MARKUP_PERCENT,',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_MARKUP_PERCENT;
-----------------------------------------------------------------------------------------
-- Any changes done to this function should be applied to CALC_MARKUP_PERCENT_ITEM_BULK.
-- CALC_MARKUP_PERCENT_ITEM_BULK is a bulk processing version of this function.
-----------------------------------------------------------------------------------------
FUNCTION CALC_MARKUP_PERCENT_ITEM (O_error_message           IN OUT VARCHAR2,
                                   O_markup_percent          IN OUT NUMBER,
                                   I_retail_includes_vat_ind IN     VARCHAR2,
                                   I_markup_calc_type        IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                                   I_total_cost              IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   I_total_retail            IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                                   I_item                    IN     ITEM_MASTER.ITEM%TYPE,
                                   I_dept                    IN     DEPS.DEPT%TYPE,
                                   I_loc_type                IN     COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                                   I_location                IN     COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                                   I_vat_region              IN     VAT_REGION.VAT_REGION%TYPE,
                                   I_supplier_cost_ind       IN     VARCHAR2,
                                   I_supplier                IN     SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(60) := 'MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM';
   L_tax_rate                 VAT_ITEM.VAT_RATE%TYPE;
   L_total_retail             ITEM_LOC.UNIT_RETAIL%TYPE             := I_total_retail;
   L_elc_ind                  SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_total_cost_sup           ITEM_LOC_SOH.UNIT_COST%TYPE           := I_total_cost;
   L_total_cost               ITEM_LOC_SOH.UNIT_COST%TYPE           := I_total_cost;
   L_location                 ORDLOC.LOCATION%TYPE  := I_location;
   L_store                    STORE.STORE%TYPE := I_location;
   L_pack_ind                 ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind             ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind            ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type                ITEM_MASTER.PACK_TYPE%TYPE;
   L_store_ind                VARCHAR2(1);
   L_get_vat                  VARCHAR2(1)                           := 'Y';
   L_total_exp                ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_exp_currency             CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_exp             CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty                ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_dty_currency             CURRENCIES.CURRENCY_CODE%TYPE;
   L_waste_type               ITEM_MASTER.WASTE_TYPE%TYPE;
   L_waste_pct                ITEM_MASTER.WASTE_PCT%TYPE;
   L_default_waste_pct        ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;

   L_markup_calc_type         DEPS.MARKUP_CALC_TYPE%TYPE;
   L_budgeted_intake          DEPS.BUD_INT%TYPE;
   L_item                     ITEM_MASTER.ITEM%TYPE                  := I_item;

   L_standard_unit_retail_zon   ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_zon           ITEM_LOC.SELLING_UOM%TYPE;
   L_selling_unit_retail_zon    ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_uom_zon            ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_zon            ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_zon      ITEM_LOC.UNIT_RETAIL%TYPE;
   L_multi_selling_uom_zon      ITEM_LOC.SELLING_UOM%TYPE;
   L_default_tax_type           SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;
   L_tax_amount                 GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE;
   L_tax_ind                     VARCHAR2(1) ;
   L_rpm_ind                    SYSTEM_OPTIONS.RPM_IND%TYPE;
   L_supplier                   ITEM_SUPPLIER.SUPPLIER%TYPE;

   L_vdate DATE := get_vdate();

   cursor C_SYSTEM_OPTIONS is
      select elc_ind,
             default_tax_type,
             rpm_ind
        from system_options;

   cursor C_GET_ANY_STORE is
      select store
        from store;

    cursor C_XFRM_ITEM_DET is
       select item_xform_ind, status, orderable_ind, sellable_ind, dept, class, subclass
         from item_master
        where item = I_item;

    cursor C_GET_PRIM_SUPP is
       select supplier
         from item_supplier
        where primary_supp_ind = 'Y'
          and item = I_item;

    L_xfrm_item_det      C_XFRM_ITEM_DET%ROWTYPE;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   open C_SYSTEM_OPTIONS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   fetch C_SYSTEM_OPTIONS into L_elc_ind,
                               L_default_tax_type,
                               L_rpm_ind;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   close C_SYSTEM_OPTIONS;


   SQL_LIB.SET_MARK('OPEN',
                    'C_XFRM_ITEM_DET',
                    'ITEM_MASTER',
                    L_item);
   open  C_XFRM_ITEM_DET;

   SQL_LIB.SET_MARK('FETCH',
                    'C_XFRM_ITEM_DET',
                    'ITEM_MASTER',
                    L_item);
   fetch C_XFRM_ITEM_DET into  L_xfrm_item_det;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_XFRM_ITEM_DET',
                    'ITEM_MASTER',
                    L_item);
   close C_XFRM_ITEM_DET;

   --orderable only check
   if L_xfrm_item_det.ITEM_XFORM_IND = 'Y' and
      L_xfrm_item_det.STATUS != 'W' and
      L_xfrm_item_det.ORDERABLE_IND = 'Y' then
      if NOT ITEM_XFORM_SQL.CALCULATE_RETAIL(O_error_message,
                                             L_item,
                                             I_location,
                                             L_total_retail) then
         return FALSE;
      end if;
   --sellable only check
   elsif L_xfrm_item_det.ITEM_XFORM_IND='Y' and
         L_xfrm_item_det.STATUS !='W' and
         L_xfrm_item_det.SELLABLE_IND='Y' then
      if NOT ITEM_XFORM_SQL.CALCULATE_COST(O_error_message,
                                           L_item,
                                           I_location,
                                           L_total_cost) then
         return FALSE;
      end if;
   end if;
   if NOT ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                              L_pack_ind,
                              L_sellable_ind,
                              L_orderable_ind,
                              L_pack_type,
                              L_item) then
          return false;
   end if;

   if L_pack_ind = 'Y' and L_sellable_ind = 'N' then
      L_get_vat := 'N';
   else
      ---
      --- if I_loc_type = 'Z', this will be handled by PM_RETAIL_API_SQL.GET_ZONE_VAT_RATE()
      ---
      if I_loc_type = 'S' then
      -- L_store_ind is used to pass to get_vat_rate, where vat_rate is
      -- required to be found on either vat_item or vat_deps table when it is
      -- a store that we are calculating markup percent or unit retail for.
         L_store_ind := 'Y';
      end if;
   end if;
   ---
   if TAX_SQL.GET_RETAIL_TAX_IND (O_error_message ,
                                  L_tax_ind ,
                                  I_item) = FALSE then
      return FALSE;
   end if;
   --
   if (L_default_tax_type in ('SVAT','GTAX') and L_get_vat = 'Y' and L_tax_ind = 'Y') then

      if I_loc_type = 'Z' then
         if L_rpm_ind = 'Y' then
            if PM_RETAIL_API_SQL.GET_ZONE_VAT_RATE(O_error_message,
                                                   L_tax_rate,
                                                   L_tax_amount,
                                                   I_location,     -- RPM Zone ID
                                                   L_xfrm_item_det.dept,
                                                   L_item,
                                                   L_total_retail) = FALSE then
               return FALSE;
            end if;
         else
            if I_supplier is NULL then
               open C_GET_PRIM_SUPP;
               fetch C_GET_PRIM_SUPP into L_supplier;
               close C_GET_PRIM_SUPP;
            else
               L_supplier := I_supplier;
            end if;

            if RETAIL_API_SQL.GET_SUPP_VAT_RATE(O_error_message,
                                                L_tax_rate,
                                                L_tax_amount,
                                                L_supplier,
                                                L_xfrm_item_det.dept,
                                                L_item,
                                                L_total_retail) = FALSE then
               return FALSE;
            end if;
         end if;
         -- Get the total tax exclusive retail
         if L_default_tax_type = TAX_SQL.LP_GTAX then
            L_total_retail := L_total_retail - ((L_total_retail - L_tax_amount) * (L_tax_rate/100) - L_tax_amount);
         else
            L_total_retail := L_total_retail / (1+L_tax_rate/100);
         end if;
      else

         if L_xfrm_item_det.ITEM_XFORM_IND='N' or
            (L_xfrm_item_det.ITEM_XFORM_IND='Y' and
                (L_xfrm_item_det.STATUS !='W' or
                 L_xfrm_item_det.STATUS  ='W' and L_markup_calc_type != 'R')) then

            -- L_total_retail corresponds to the unit qty, so pass in qty of 1 to tax_sql call
            if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                          L_total_retail,
                          L_item, L_xfrm_item_det.dept, L_xfrm_item_det.class, I_location, I_loc_type, L_vdate,
                          L_total_retail, 1) = FALSE then
               return FALSE;
            end if;

         end if;

      end if;

   end if;

   if  (L_elc_ind = 'Y') and
       (L_total_cost is NULL or I_supplier_cost_ind = 'Y') then

         -- If I_total_cost is passed in as NOT NULL and I_supplier_cost_ind = 'Y',
         -- I_total_cost must be in supplier currency.
         -- If passing a unit cost into elc_calc_sql.calc_totals it must be
         -- in supplier currency.
         -- The total_cost (O_total_elc) is always passed out in Primary
         -- currency.

        if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                     L_total_cost,  -- in Primary Currency
                                     L_total_exp,
                                     L_exp_currency,
                                     L_exchange_exp,
                                     L_total_dty,
                                     L_dty_currency,
                                     NULL,
                                     L_item,
                                     NULL,
                                     NULL,
                                     NULL,
                                     I_supplier,
                                     NULL,
                                     NULL,
                                     L_total_cost_sup) = FALSE then
            return FALSE;
         end if;
    elsif L_total_cost is NULL then
       L_total_cost := 0;
       -- in this situation, I_total_cost must be in primary currency
       --O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
       --                                      NULL, NULL, NULL);
       --return FALSE;

   end if;
   -- Determine if item is a wastage item.  If so, inflate the cost
   -- of the item to reflect the wastage percentage.

   if ITEM_ATTRIB_SQL.GET_WASTAGE(O_error_message,
                                     L_waste_type,
                                     L_waste_pct,
                                     L_default_waste_pct,
                                     L_item) = FALSE then
         return FALSE;
   end if;

   if L_waste_type is not NULL and L_waste_type in ('SP', 'SL') then

      L_waste_pct := (L_waste_pct / 100);
      L_total_cost := L_total_cost * (1 + NVL(L_waste_pct,0));

   end if;

   if PM_RETAIL_API_SQL.GET_RPM_MARKUP_INFO(O_error_message,
                                            L_markup_calc_type,
                                            O_markup_percent,
                                            L_xfrm_item_det.DEPT,
                                            L_xfrm_item_det.CLASS,
                                            L_xfrm_item_det.SUBCLASS) = FALSE then
      return FALSE;
   end if;
   if L_xfrm_item_det.ITEM_XFORM_IND='Y' then
      O_markup_percent := L_budgeted_intake;
   else
      if CALC_MARKUP_PERCENT(O_error_message,
                             O_markup_percent,
                             L_markup_calc_type,
                             L_total_cost,
                             L_total_retail) = FALSE then
          return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_MARKUP_PERCENT_ITEM;
-------------------------------------------------------------------------------
FUNCTION CALC_MARKUP_PERCENT_ITEM (O_error_message           IN OUT VARCHAR2,
                                   O_markup_percent          IN OUT NUMBER,
                                   I_retail_includes_vat_ind IN     VARCHAR2,
                                   I_markup_calc_type        IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                                   I_total_cost              IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                                   I_total_retail            IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                                   I_item                    IN     ITEM_MASTER.ITEM%TYPE,
                                   I_dept                    IN     DEPS.DEPT%TYPE,
                                   I_loc_type                IN     COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                                   I_location                IN     COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                                   I_vat_region              IN     VAT_REGION.VAT_REGION%TYPE)
   RETURN BOOLEAN IS

BEGIN

   if NOT CALC_MARKUP_PERCENT_ITEM(O_error_message,
                                   O_markup_percent,
                                   I_retail_includes_vat_ind,
                                   I_markup_calc_type,
                                   I_total_cost,
                                   I_total_retail,
                                   I_item,
                                   I_dept,
                                   I_loc_type,
                                   I_location,
                                   I_vat_region,
                                   NULL,NULL) then
      return FALSE;
   else
      return TRUE;
   end if;

END CALC_MARKUP_PERCENT_ITEM;
-------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_FROM_MARKUP (O_error_message           IN OUT VARCHAR2,
                                  O_unit_retail             IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_markup_percent          IN     NUMBER,
                                  I_markup_calc_type        IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                                  I_unit_cost               IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                                  I_item                    IN     ITEM_MASTER.ITEM%TYPE,
                                  I_loc_type                IN     COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                                  I_location                IN     COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                                  I_dept                    IN     DEPS.DEPT%TYPE,
                                  I_vat_region              IN     VAT_REGION.VAT_REGION%TYPE,
                                  I_currency_code           IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                                  I_supplier_cost_ind       IN     VARCHAR2,
                                  I_supplier                IN     SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(60) := 'MARKUP_SQL.CALC_RETAIL_FROM_MARKUP';
   L_tax_rate             VAT_ITEM.VAT_RATE%TYPE;
   L_currency_code        CURRENCIES.CURRENCY_CODE%TYPE := I_currency_code;
   L_elc_ind              SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_unit_cost_sup        ITEM_LOC_SOH.UNIT_COST%TYPE  := I_unit_cost;
   L_unit_cost            ITEM_LOC_SOH.UNIT_COST%TYPE  := I_unit_cost;
   L_total_retail         ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_location             ORDLOC.LOCATION%TYPE  := I_location;
   L_store                STORE.STORE%TYPE := I_location;
   L_pack_ind             ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type            ITEM_MASTER.PACK_TYPE%TYPE;
   L_store_ind            VARCHAR2(1);
   L_get_vat              VARCHAR2(1)         := 'Y';
   L_total_exp            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_exp_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_exp         CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_dty_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_waste_type           ITEM_MASTER.WASTE_TYPE%TYPE;
   L_waste_pct            ITEM_MASTER.WASTE_PCT%TYPE;
   L_default_waste_pct    ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;
   L_item                 ITEM_MASTER.ITEM%TYPE := I_item;
   L_tax_amount           GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE := 0;
   L_unit_retail_ex_tax   ITEM_LOC.UNIT_RETAIL%TYPE := 0;

   L_standard_unit_retail_zon   ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_zon           ITEM_LOC.SELLING_UOM%TYPE;
   L_selling_unit_retail_zon    ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_uom_zon            ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_zon            ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_zon      ITEM_LOC.UNIT_RETAIL%TYPE;
   L_multi_selling_uom_zon      ITEM_LOC.SELLING_UOM%TYPE;
   L_vdate                      DATE := get_vdate();
   L_default_tax_type           SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;
   QUICKEXIT                    EXCEPTION;
   L_tax_ind                     VARCHAR2(1) ;
   L_rpm_ind                    SYSTEM_OPTIONS.RPM_IND%TYPE;
   L_supplier                   ITEM_SUPPLIER.SUPPLIER%TYPE;

   cursor C_GET_ANY_STORE IS
      select store
        from store
    order by store;

   cursor C_XFRM_ITEM_DET IS
      select item_xform_ind,
             status,
             orderable_ind,
             sellable_ind,
             dept,
             class
        from item_master
       where item = I_item;

   L_xfrm_item_det      C_XFRM_ITEM_DET%ROWTYPE;

   cursor C_SYSTEM_OPTIONS is
      select elc_ind,
             default_tax_type,
             rpm_ind
        from system_options;

    cursor C_GET_PRIM_SUPP is
       select supplier
         from item_supplier
        where primary_supp_ind = 'Y'
          and item = I_item;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   open C_SYSTEM_OPTIONS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   fetch C_SYSTEM_OPTIONS into L_elc_ind,
                               L_default_tax_type,
                               L_rpm_ind;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   close C_SYSTEM_OPTIONS;

   if NOT ITEM_ATTRIB_SQL.GET_PACK_INDS (O_error_message,
                            L_pack_ind,
                            L_sellable_ind,
                            L_orderable_ind,
                            L_pack_type,
                            I_item) then
       return FALSE;
   end if;

   if L_pack_ind = 'Y' and L_sellable_ind = 'N' then
      L_get_vat := 'N';
   end if;
   ---
   if (L_elc_ind = 'Y')
      and (I_unit_cost is NULL or I_supplier_cost_ind = 'Y') then

         -- If I_unit_cost is passed into MARKUP_SQL as NOT NULL
         -- and I_supplier_cost_ind = 'Y', then
         -- I_unit_cost must be in supplier currency.
         -- If passing a unit cost into elc_calc_sql.calc_totals it must be
         -- in supplier currency.
         -- The total_cost (O_total_elc) is always passed out in Primary
         -- currency.

         if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                     L_unit_cost,  -- in Primary Currency
                                     L_total_exp,
                                     L_exp_currency,
                                     L_exchange_exp,
                                     L_total_dty,
                                     L_dty_currency,
                                     NULL,
                                     I_item,
                                     NULL,
                                     NULL,
                                     NULL,
                                     I_supplier,
                                     NULL,
                                     NULL,
                                     L_unit_cost_sup) = FALSE then
            return FALSE;
         end if;

   elsif I_unit_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                            NULL, NULL, NULL);
      return FALSE;
   end if;

   if L_currency_code is NULL then
      if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                          L_currency_code) = FALSE then
         return FALSE;
      end if;
   end if;


   --  Determine is item is a wastage item, if so, increase
   --  unit cost by the wastage percent.

   if ITEM_ATTRIB_SQL.GET_WASTAGE(O_error_message,
                                     L_waste_type,
                                     L_waste_pct,
                                     L_default_waste_pct,
                                     I_item) = FALSE then
         return FALSE;
   end if;

   if L_waste_type is not NULL and L_waste_type in ('SP', 'SL') then
      L_waste_pct := (L_waste_pct / 100);
      L_unit_cost := L_unit_cost * (1 + NVL(L_waste_pct,0));
   end if;


   if I_markup_calc_type = 'C' then
      if I_markup_percent = -100 then
         O_unit_retail := 0;
      elsif I_markup_percent < -100 then
         O_error_message := SQL_LIB.CREATE_MSG('MKDN_LESS_100PCT_COST');
         return FALSE;
      else
         L_unit_retail_ex_tax := L_unit_cost * (1 + I_markup_percent / 100);
      end if;
   elsif I_markup_calc_type = 'R' then
      if I_markup_percent = 100 then
         return TRUE;
      elsif I_markup_percent > 100 then
         O_error_message := SQL_LIB.CREATE_MSG('RETAIL_MARKUP_100');
         return FALSE;
      else
         SQL_LIB.SET_MARK('OPEN',
                          'C_XFRM_ITEM_DET',
                          'ITEM_MASTER',
                          L_item);
         open  C_XFRM_ITEM_DET;
         SQL_LIB.SET_MARK('FETCH',
                          'C_XFRM_ITEM_DET',
                          'ITEM_MASTER',
                          L_item);
         fetch C_XFRM_ITEM_DET
          into L_xfrm_item_det;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_XFRM_ITEM_DET',
                          'ITEM_MASTER',
                          L_item);
         close C_XFRM_ITEM_DET;

         L_unit_retail_ex_tax := (L_unit_cost / (1 - I_markup_percent / 100));
      end if;
   else  --- I_markup_calc_type not in ('C','R')
      O_error_message := SQL_LIB.CREATE_MSG('INV_MARKUP_TYPE_VALUE', I_markup_calc_type);
      return FALSE;
   end if;

   if TAX_SQL.GET_RETAIL_TAX_IND (O_error_message ,
                                  L_tax_ind ,
                                  I_item) = FALSE then
      return FALSE;
   end if;

   -- When there is no taxes then O_unit_retail is going to retail without tax inclusive
   O_unit_retail := L_unit_retail_ex_tax;

   if (L_default_tax_type in ('GTAX','SVAT') and L_get_vat = 'Y' and L_tax_ind = 'Y') then

      if I_loc_type = 'Z' and I_location is NOT NULL and L_rpm_ind = 'Y' then
         if PM_RETAIL_API_SQL.GET_ZONE_VAT_RATE(O_error_message,
                                                L_tax_rate,
                                                L_tax_amount,
                                                I_location,     -- RPM Zone ID
                                                L_xfrm_item_det.dept,
                                                L_item,
                                                L_unit_retail_ex_tax) = FALSE then
            return FALSE;
         end if;

         if L_default_tax_type = TAX_SQL.LP_GTAX then
            if L_tax_rate >= 100 then
               L_tax_rate := 0;
            end if;
            O_unit_retail := (L_unit_retail_ex_tax / (1 - L_tax_rate/100)) + L_tax_amount;
         else
            O_unit_retail := L_unit_retail_ex_tax * (1+L_tax_rate/100);
         end if;

      elsif I_loc_type = 'Z' and L_rpm_ind = 'N' then

         if I_supplier is NULL then
            open C_GET_PRIM_SUPP;
            fetch C_GET_PRIM_SUPP into L_supplier;
            close C_GET_PRIM_SUPP;
         else
            L_supplier := I_supplier;
         end if;

         if RETAIL_API_SQL.GET_SUPP_VAT_RATE(O_error_message,
                                             L_tax_rate,
                                             L_tax_amount,
                                             L_supplier,
                                             L_xfrm_item_det.dept,
                                             L_item,
                                             L_unit_retail_ex_tax) = FALSE then
            return FALSE;
         end if;

         if L_default_tax_type = TAX_SQL.LP_GTAX then
            if L_tax_rate >= 100 then
               L_tax_rate := 0;
            end if;
            O_unit_retail := (L_unit_retail_ex_tax / (1 - L_tax_rate/100)) + L_tax_amount;
         else
            O_unit_retail := L_unit_retail_ex_tax * (1+L_tax_rate/100);
         end if;
      else

         if TAX_SQL.ADD_RETAIL_TAX(O_error_message,
                                   O_unit_retail,
                                   L_item,
                                   L_pack_ind,
                                   L_xfrm_item_det.dept,
                                   L_xfrm_item_det.class,
                                   L_store,
                                   'S',
                                   L_vdate,
                                   L_unit_retail_ex_tax,
                                   L_currency_code,
                                   I_call_type => TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if CURRENCY_SQL.ROUND_CURRENCY(O_error_message,
                                  O_unit_retail,
                                  L_currency_code,
                                  'R') = FALSE then
      return FALSE;
   end if;

   if O_unit_retail > 9999999999999999.99 or O_unit_retail < -9999999999999999.99 then
      O_error_message := SQL_LIB.CREATE_MSG('CALC_RETL_OUTSIDE_RANGE');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when QUICKEXIT then
      return TRUE;

   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'MARKUP_SQL.CALC_RETAIL_FROM_MARKUP',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_RETAIL_FROM_MARKUP;
-------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_FROM_MARKUP (O_error_message           IN OUT VARCHAR2,
                                  O_unit_retail             IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_markup_percent          IN     NUMBER,
                                  I_markup_calc_type        IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                                  I_unit_cost               IN     ITEM_LOC_SOH.UNIT_COST%TYPE,
                                  I_item                    IN     ITEM_MASTER.ITEM%TYPE,
                                  I_loc_type                IN     COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                                  I_location                IN     COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                                  I_dept                    IN     DEPS.DEPT%TYPE,
                                  I_vat_region              IN     VAT_REGION.VAT_REGION%TYPE,
                                  I_currency_code           IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

BEGIN

   if NOT CALC_RETAIL_FROM_MARKUP(O_error_message,
                                  O_unit_retail,
                                  I_markup_percent,
                                  I_markup_calc_type,
                                  I_unit_cost,
                                  I_item,
                                  I_loc_type,
                                  I_location,
                                  I_dept,
                                  I_vat_region,
                                  I_currency_code,
                                  NULL,NULL) then
      RETURN FALSE;
   else
      return TRUE;
   end if;

END CALC_RETAIL_FROM_MARKUP;
-------------------------------------------------------------------------------------------------------
FUNCTION CALC_MARKUP_PERCENT_ITEM_BULK(O_error_message       IN OUT        VARCHAR2,
                                       IO_item_pricing_table IN OUT NOCOPY PM_RETAIL_API_SQL.ITEM_PRICING_TABLE,
                                       I_total_cost          IN            ITEM_LOC_SOH.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   L_default_tax_type      SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;
   L_class_level_vat_ind   SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE := NULL;
   L_total_retail          ITEM_LOC.UNIT_RETAIL%TYPE := NULL;
   L_total_cost            ITEM_LOC_SOH.UNIT_COST%TYPE := NULL;
   L_tax_rate              VAT_ITEM.VAT_RATE%TYPE := NULL;
   L_tax_amount            GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE := NULL;
   L_waste_pct             ITEM_MASTER.WASTE_PCT%TYPE := NULL;
   L_sum_count             NUMBER :=0;
   L_vat_rate              VAT_CODE_RATES.VAT_RATE%TYPE :=0;
   L_vdate                 DATE := get_vdate();
   L_initial_count         NUMBER(10) := 0;

   L_retail_calc_tbl       OBJ_RETAIL_CALC_TBL := OBJ_RETAIL_CALC_TBL();
   L_retail_calc_rec       OBJ_RETAIL_CALC_REC := NULL;

   L_obj_tax_calc_rec      OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_obj_tax_calc_tbl      OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();

   L_class                 CLASS.CLASS%TYPE;            -- defining to allow %TYPE declaration in T_dept_markup_calc_REC
   L_subclass              SUBCLASS.SUBCLASS%TYPE;      -- defining to allow %TYPE declaration in T_dept_markup_calc_REC

   L_program               VARCHAR2(60) := 'MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM_BULK';

   TYPE T_dept_markup_calc_REC is RECORD
   (
      item                 ITEM_MASTER.ITEM%TYPE,
      dept                 DEPS.DEPT%TYPE,
      class                L_class%TYPE,
      subclass             L_subclass%TYPE,
      markup_calc_type     DEPS.MARKUP_CALC_TYPE%TYPE,
      markup_percent       PM_RETAIL_API_SQL.MARKUP_PERCENT%TYPE
   );

   TYPE T_dept_markup_calc_TBL is TABLE OF T_dept_markup_calc_REC;
   L_dept_markup_calc_TBL T_dept_markup_calc_TBL;

   cursor C_GET_ORDERABLES is
      select *
        from retail_calc_temp
       where item_xform_ind = 'Y'
         and status != 'W'
         and orderable_ind = 'Y';

   cursor C_GET_SELLABLES is
      select *
        from retail_calc_temp
       where item_xform_ind = 'Y'
         and status != 'W'
         and sellable_ind = 'Y';

   cursor C_GET_SVAT_BULK is
      select OBJ_TAX_CALC_REC(m.item,
                              m.pack_ind,
                              zone_locs.loc,  --I_from_entity
                              zone_locs.loc_type,  --I_from_entity_type
                              NULL,  --I_TO_ENTITY,
                              NULL,  --I_TO_ENTITY_TYPE,
                              NULL,  --I_EFFECTIVE_FROM_DATE,
                              m.unit_retail,  --I_AMOUNT,
                              zones.currency_code,  --I_AMOUNT_CURR,
                              'N',   --I_AMOUNT_TAX_INCL_IND,
                              NULL,  --I_origin_country_id
                              NULL,  --O_CUM_TAX_PCT,
                              NULL,  --O_CUM_TAX_VALUE,
                              NULL,  --O_TOTAL_TAX_AMOUNT,
                              NULL,  --O_TOTAL_TAX_AMOUNT_CURR,
                              NULL,  --O_TOTAL_RECOVER_AMOUNT,
                              NULL,  --O_TOTAL_RECOVER_AMOUNT_CURR,
                              NULL,  --O_TAX_DETAIL_TBL
                              'MARKUPCALC', --I_TRAN_TYPE
                              get_vdate(),  --I_TRAN_DATE
                              NULL,         --I_TRAN_ID
                              'R')          --I_COST_RETAIL_IND
        from retail_calc_temp m,
             rpm_zone zones,  -- zone_id is the primary key
             (select rzl.zone_id,
                     l.vat_region,
                     min(l.loc) loc,
                     l.loc_type
                from rpm_zone_location rzl,
                     (select store loc, 'ST' loc_type, vat_region
                        from store
                      union
                      select wh loc, 'WH' loc_type, vat_region
                        from wh) l
               where l.loc = rzl.location
               group by rzl.zone_id, vat_region, l.loc_type) zone_locs
       where m.zone_id = zones.zone_id
         and zones.zone_id = zone_locs.zone_id
         and m.get_vat_ind = 'Y'
         and m.tax_ind = 'Y'
         and m.loc_type = 'Z';

   cursor C_GET_GTAX_BULK is
     select OBJ_TAX_CALC_REC(m.item,
                             m.pack_ind,
                             rzl.location,        --I_from_entity
                             location.loc_type,   --I_from_entity_type
                             NULL,                --I_TO_ENTITY,
                             NULL,                --I_TO_ENTITY_TYPE,
                             L_vdate,             --I_EFFECTIVE_FROM_DATE,
                             m.unit_retail_prim,  --I_AMOUNT,
                             rz.currency_code,    --I_AMOUNT_CURR,
                             'Y',                 --I_AMOUNT_TAX_INCL_IND,
                             NULL,                --I_origin_country_id
                             NULL,                --O_CUM_TAX_PCT,
                             NULL,                --O_CUM_TAX_VALUE,
                             NULL,                --O_TOTAL_TAX_AMOUNT,
                             NULL,                --O_TOTAL_TAX_AMOUNT_CURR,
                             NULL,                --O_TOTAL_RECOVER_AMOUNT,
                             NULL,                --O_TOTAL_RECOVER_AMOUNT_CURR,
                             NULL)                --O_TAX_DETAIL_TBL
        from rpm_zone_location rzl,
             rpm_zone rz,
             (select store loc,
                     'ST' loc_type
                from store
               where store_type in ('C','F')
               union all
              select wh loc,
                     'WH' loc_type
                from wh) location,
             retail_calc_temp m
       where rzl.zone_id = m.zone_id
         and rzl.zone_id = rz.zone_id
         and rzl.location = location.loc
         and m.get_vat_ind = 'Y'
         and m.tax_ind = 'Y'
         and m.loc_type = 'Z';

   cursor C_SYSTEM_OPTIONS is
   select default_tax_type,
          class_level_vat_ind
     from system_options;

   --  Since there may be multiple records for a given item in retail_calc_temp, using
   --  distinct to retrieve the non-duplicate records.

   cursor C_GET_MERCH_HIER is
      select distinct im.item,
             im.dept,
             im.class,
             im.subclass,
             NULL markup_calc_type,  -- This will be updated later by call to RPM
             NULL markup_percent     -- This will be updated later by call to RPM
        from retail_calc_temp rct,
             item_master im
       where rct.item = im.item;

   cursor MUTLI_SELLING_MARKUP is
   select rowid,
          item,
          zone_id,
          loc_type,
          total_cost,
          unit_retail,
          currency_code,
          standard_uom,
          selling_uom,
          multi_selling_uom,
          multi_unit_retail,
          multi_units,
          markup_calc_type,
          multi_selling_mark_up,
          get_vat_ind
    from  retail_calc_temp
   where  total_cost IS NOT NULL
     and  standard_uom IS NOT NULL
     and  multi_selling_uom IS NOT NULL
     and  multi_units IS NOT NULL
     and  multi_unit_retail IS NOT NULL;

   TYPE  L_multi_markup_details_TBL IS TABLE OF mutli_selling_markup%ROWTYPE  INDEX BY PLS_INTEGER;
   L_multi_markup_details  L_multi_markup_details_TBL;

   L_multi_unit_retail_prm  ITEM_LOC_SOH.UNIT_COST%TYPE;
   L_retail                 ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_qty                    ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_primary_currency_code  CURRENCIES.CURRENCY_CODE%TYPE;

BEGIN

   -- Check if the input IO_item_pricing_table has records
   if IO_item_pricing_table is NULL or IO_item_pricing_table.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'IO_item_pricing_table',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- Store record count for checking after markup calculation
   L_initial_count := IO_item_pricing_table.COUNT;

   -- Fetch system options
   open C_SYSTEM_OPTIONS;
   fetch C_SYSTEM_OPTIONS into L_default_tax_type,
                               L_class_level_vat_ind;
   close C_SYSTEM_OPTIONS;

   -- Ensure retail_calc_temp table is empty prior to insert
   delete from retail_calc_temp;

   -- Insert contents of the IO_item_pricing_table collection into the retail_calc_temp table
   FORALL i in IO_item_pricing_table.FIRST..IO_item_pricing_table.LAST
      insert into retail_calc_temp(item,
                                   rpm_zone_group_id,
                                   zone_id,
                                   zone_display_id,
                                   zone_description,
                                   loc_type,
                                   unit_retail,
                                   selling_unit_retail,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   total_cost,
                                   multi_selling_uom,
                                   base_retail_ind,
                                   currency_code,
                                   standard_uom,
                                   selling_mark_up,
                                   multi_selling_mark_up,
                                   default_to_children,
                                   unit_retail_prim,
                                   selling_unit_retail_prim,
                                   multi_unit_retail_prim,
                                   unit_retail_euro,
                                   selling_unit_retail_euro,
                                   multi_unit_retail_euro,
                                   error_message,
                                   return_code,
                                   area_diff_zone_ind,
                                   ---
                                   item_xform_ind,
                                   status,
                                   orderable_ind,
                                   sellable_ind,
                                   dept,
                                   class,
                                   pack_ind,
                                   pack_type,
                                   order_as_type,
                                   notional_pack_ind,
                                   waste_type,
                                   waste_pct,
                                   default_waste_pct,
                                   bud_int,
                                   bud_mkup,
                                   markup_calc_type,
                                   ---
                                   get_vat_ind,
                                   tax_ind,
                                   class_vat_ind,
                                   ---
                                   tax_rate,
                                   tax_amount)
      values(IO_item_pricing_table(i).item,
             IO_item_pricing_table(i).rpm_zone_group_id,
             IO_item_pricing_table(i).zone_id,
             IO_item_pricing_table(i).zone_display_id,
             IO_item_pricing_table(i).zone_description,
             'Z', -- loc_type - this is always 'Z'(zone) when called from itemretail.fmb
             IO_item_pricing_table(i).unit_retail,
             IO_item_pricing_table(i).selling_unit_retail,
             IO_item_pricing_table(i).selling_uom,
             IO_item_pricing_table(i).multi_units,
             IO_item_pricing_table(i).multi_unit_retail,
             NVL(I_total_cost, 0),  -- total_cost
             IO_item_pricing_table(i).multi_selling_uom,
             IO_item_pricing_table(i).base_retail_ind,
             IO_item_pricing_table(i).currency_code,
             IO_item_pricing_table(i).standard_uom,
             IO_item_pricing_table(i).selling_mark_up,
             IO_item_pricing_table(i).multi_selling_mark_up,
             IO_item_pricing_table(i).default_to_children,
             IO_item_pricing_table(i).unit_retail_prim,
             IO_item_pricing_table(i).selling_unit_retail_prim,
             IO_item_pricing_table(i).multi_unit_retail_prim,
             IO_item_pricing_table(i).unit_retail_euro,
             IO_item_pricing_table(i).selling_unit_retail_euro,
             IO_item_pricing_table(i).multi_unit_retail_euro,
             IO_item_pricing_table(i).error_message,
             IO_item_pricing_table(i).return_code,
             IO_item_pricing_table(i).area_diff_zone_ind,
             ---
             NULL,  -- item_xform_ind
             NULL,  -- status
             NULL,  -- orderable_ind
             NULL,  -- sellable_ind
             NULL,  -- dept
             NULL,  -- class
             NULL,  -- pack_ind
             NULL,  -- pack_type
             NULL,  -- order_as_type
             NULL,  -- notional_pack_ind
             NULL,  -- waste_type
             NULL,  -- waste_pct
             NULL,  -- default_waste_pct
             NULL,  -- bud_int
             NULL,  -- bud_mkup
             NULL,  -- markup_calc_type
             ---
             NULL,  -- get_vat_ind
             NULL,  -- tax_ind
             NULL,  -- class_vat_ind
             ---
             NULL,  -- tax_rate
             NULL); -- tax_amount


   -- Retrieve itemmaster attributes for records on retail_calc_temp.
   -- The retrieved attributes are merged back to their corresponding records on retail_calc_temp.
   -- These attributes will be used for succeeding computations.
   merge into retail_calc_temp mct
   using (
      select im.item,
             m.rpm_zone_group_id,
             m.zone_id,
             m.zone_display_id,
             im.item_xform_ind,
             im.status,
             im.orderable_ind,
             im.sellable_ind,
             im.dept,
             im.class,
             im.pack_ind,
             im.pack_type,
             im.order_as_type,
             im.notional_pack_ind,
             im.waste_type,
             NVL(im.waste_pct, 0) waste_pct,
             im.default_waste_pct,
             case
                when im.pack_ind = 'Y' and
                     im.sellable_ind = 'N' then
                   'N'
                else
                   'Y'
             end get_vat_ind,
             -- Unit retail in RMS is tax always inclusive for GTAX.
             -- Unit retail in RMS is tax always inclusive for SVAT and SALES if system's class_level_vat_ind is 'N'.
             -- For SVAT and SALES, if system's class_level_vat_ind is 'Y', unit retail in RMS is tax inclusive or
             -- tax exclusive depending on the item class's CLASS_VAT_IND.
             case
                when L_default_tax_type = TAX_SQL.LP_SALES then
                     'N'
                when L_default_tax_type = TAX_SQL.LP_SVAT and
                     L_class_level_vat_ind ='Y' and
                     c.class_vat_ind = 'N' then
                     'N'
                else
                   'Y'
             end tax_ind,
             c.class_vat_ind,
             d.bud_int,
             d.bud_mkup
        from retail_calc_temp m,
             item_master im,
             class c,
             deps d
       where m.item   = im.item
         and im.class = c.class
         and im.dept  = c.dept
         and im.dept  = d.dept) inner
   on (    inner.item              = mct.item
       and inner.rpm_zone_group_id = mct.rpm_zone_group_id
       and inner.zone_id           = mct.zone_id
       and inner.zone_display_id   = mct.zone_display_id)
   when matched then
   update set mct.item_xform_ind       = inner.item_xform_ind,
              mct.status               = inner.status,
              mct.orderable_ind        = inner.orderable_ind,
              mct.sellable_ind         = inner.sellable_ind,
              mct.dept                 = inner.dept,
              mct.class                = inner.class,
              mct.pack_ind             = inner.pack_ind,
              mct.pack_type            = inner.pack_type,
              mct.order_as_type        = inner.order_as_type,
              mct.notional_pack_ind    = inner.notional_pack_ind,
              mct.waste_type           = inner.waste_type,
              mct.waste_pct            = inner.waste_pct,
              mct.default_waste_pct    = inner.default_waste_pct,
              mct.get_vat_ind          = inner.get_vat_ind,
              mct.tax_ind              = inner.tax_ind,
              mct.class_vat_ind        = inner.class_vat_ind,
              mct.bud_int              = inner.bud_int,
              mct.bud_mkup             = inner.bud_mkup;

   -- Fetching the markup type from RPM.
   open C_GET_MERCH_HIER;
   fetch C_GET_MERCH_HIER BULK COLLECT INTO L_dept_markup_calc_tbl;
   close C_GET_MERCH_HIER;

   if L_dept_markup_calc_tbl is not NULL and L_dept_markup_calc_tbl.COUNT > 0 then
      FOR i in L_dept_markup_calc_tbl.FIRST..L_dept_markup_calc_tbl.LAST LOOP
         if PM_RETAIL_API_SQL.GET_RPM_MARKUP_INFO ( O_error_message,
                                                    L_dept_markup_calc_tbl(i).markup_calc_type,
                                                    L_dept_markup_calc_tbl(i).markup_percent,
                                                    L_dept_markup_calc_tbl(i).dept,
                                                    L_dept_markup_calc_tbl(i).class,
                                                    L_dept_markup_calc_tbl(i).subclass) = FALSE then
            return FALSE;
         end if;
      END LOOP;

      FORALL i in L_dept_markup_calc_tbl.FIRST..L_dept_markup_calc_tbl.LAST
         update retail_calc_temp
            set markup_calc_type = L_dept_markup_calc_tbl(i).markup_calc_type
          where item = L_dept_markup_calc_tbl(i).item;
   end if;


   ---------------------------------------------------------------------------------------------------------------------
   -- No need compute ELC as this has been done in the itemretail PRE-FORM trigger
   -- through a call to ELC_CALC_SQL.CALC_TOTALS
   ---------------------------------------------------------------------------------------------------------------------

   ---------------------------------------------------------------------------------------------------------------------
   -- Orderable only and sellable only transformed items needs to have their retail and cost computed.
   -- This code section calls ITEM_XFORM_SQL functions per row on the retail_calc_temp table to perform computations.
   -- The resulting retail or cost value is merged back into retail_calc_temp.
   ---------------------------------------------------------------------------------------------------------------------
   -- Orderable only item processing
   FOR rec_get_orderables in C_GET_ORDERABLES LOOP
      ---
      if NOT ITEM_XFORM_SQL.CALCULATE_RETAIL(O_error_message,
                                             rec_get_orderables.item,     --input item
                                             rec_get_orderables.zone_id,  --input location
                                             L_total_retail) then         --output unit_retail_prim
         return FALSE;
      end if;
      ---
      L_retail_calc_rec := OBJ_RETAIL_CALC_REC(rec_get_orderables.item,               -- key
                                               rec_get_orderables.rpm_zone_group_id,  -- key
                                               rec_get_orderables.zone_id,            -- key
                                               rec_get_orderables.zone_display_id,    -- key
                                               NULL,  -- zone_description
                                               NULL,  -- loc_type
                                               NULL,  -- unit_retail
                                               NULL,  -- selling_unit_retail
                                               NULL,  -- selling_uom
                                               NULL,  -- multi_units
                                               NULL,  -- multi_unit_retail
                                               rec_get_orderables.total_cost,  -- total_cost
                                               NULL,  -- multi_selling_uom
                                               NULL,  -- base_retail_ind
                                               NULL,  -- currency_code
                                               NULL,  -- standard_uom
                                               NULL,  -- selling_mark_up
                                               NULL,  -- multi_selling_mark_up
                                               NULL,  -- default_to_children
                                               NVL(L_total_retail, 0), -- unit_retail_prim
                                               NULL,  -- selling_unit_retail_prim
                                               NULL,  -- multi_unit_retail_prim
                                               NULL,  -- unit_retail_euro
                                               NULL,  -- selling_unit_retail_euro
                                               NULL,  -- multi_unit_retail_euro
                                               NULL,  -- error_message
                                               NULL,  -- return_code
                                               NULL,  -- area_diff_zone_ind
                                               NULL,  -- item_xform_ind
                                               NULL,  -- status
                                               NULL,  -- orderable_ind
                                               NULL,  -- sellable_ind
                                               NULL,  -- dept
                                               NULL,  -- class
                                               NULL,  -- pack_ind
                                               NULL,  -- pack_type
                                               NULL,  -- order_as_type
                                               NULL,  -- notional_pack_ind
                                               NULL,  -- waste_type
                                               NULL,  -- waste_pct
                                               NULL,  -- default_waste_pct
                                               NULL,  -- bud_int
                                               NULL,  -- bud_mkup
                                               NULL,  -- markup_calc_type
                                               NULL,  -- get_vat_ind
                                               NULL,  -- tax_ind
                                               NULL,  -- class_vat_ind
                                               NULL,  -- tax_rate
                                               NULL); -- tax_amount
      ---
      L_retail_calc_tbl.EXTEND;
      L_retail_calc_tbl(L_retail_calc_tbl.COUNT) := L_retail_calc_rec;
      -- reset
      L_total_retail := NULL;
   END LOOP;


   -- Sellable only item processing
   FOR rec_get_sellables in C_GET_SELLABLES LOOP
      ---
      if NOT ITEM_XFORM_SQL.CALCULATE_COST(O_error_message,
                                           rec_get_sellables.item,     --input item
                                           rec_get_sellables.zone_id,  --input location
                                           L_total_cost) then          --output total cost
         return FALSE;
      end if;
      ---
      L_retail_calc_rec := OBJ_RETAIL_CALC_REC(rec_get_sellables.item,               -- key
                                               rec_get_sellables.rpm_zone_group_id,  -- key
                                               rec_get_sellables.zone_id,            -- key
                                               rec_get_sellables.zone_display_id,    -- key
                                               NULL,  -- zone_description
                                               NULL,  -- loc_type
                                               NULL,  -- unit_retail
                                               NULL,  -- selling_unit_retail
                                               NULL,  -- selling_uom
                                               NULL,  -- multi_units
                                               NULL,  -- multi_unit_retail
                                               NVL(L_total_cost, 0),  -- total_cost
                                               NULL,  -- multi_selling_uom
                                               NULL,  -- base_retail_ind
                                               NULL,  -- currency_code
                                               NULL,  -- standard_uom
                                               NULL,  -- selling_mark_up
                                               NULL,  -- multi_selling_mark_up
                                               NULL,  -- default_to_children
                                               rec_get_sellables.unit_retail_prim,  -- unit_retail_prim
                                               NULL,  -- selling_unit_retail_prim
                                               NULL,  -- multi_unit_retail_prim
                                               NULL,  -- unit_retail_euro
                                               NULL,  -- selling_unit_retail_euro
                                               NULL,  -- multi_unit_retail_euro
                                               NULL,  -- error_message
                                               NULL,  -- return_code
                                               NULL,  -- area_diff_zone_ind
                                               NULL,  -- item_xform_ind
                                               NULL,  -- status
                                               NULL,  -- orderable_ind
                                               NULL,  -- sellable_ind
                                               NULL,  -- dept
                                               NULL,  -- class
                                               NULL,  -- pack_ind
                                               NULL,  -- pack_type
                                               NULL,  -- order_as_type
                                               NULL,  -- notional_pack_ind
                                               NULL,  -- waste_type
                                               NULL,  -- waste_pct
                                               NULL,  -- default_waste_pct
                                               NULL,  -- bud_int
                                               NULL,  -- bud_mkup
                                               NULL,  -- markup_calc_type
                                               NULL,  -- get_vat_ind
                                               NULL,  -- tax_ind
                                               NULL,  -- class_vat_ind
                                               NULL,  -- tax_rate
                                               NULL); -- tax_amount
      ---
      L_retail_calc_tbl.EXTEND;
      L_retail_calc_tbl(L_retail_calc_tbl.COUNT) := L_retail_calc_rec;
      -- reset
      L_total_cost := NULL;
   END LOOP;

   -- Persist orderable only and sellable only results to temp table
   if L_retail_calc_tbl is not NULL and L_retail_calc_tbl.COUNT > 0 then
      merge into retail_calc_temp mct
      using (
         select item,
                rpm_zone_group_id,
                zone_id,
                zone_display_id,
                total_cost,
                unit_retail_prim
           from TABLE(CAST(L_retail_calc_tbl as OBJ_RETAIL_CALC_TBL))) inner
       on (    inner.item              = mct.item
           and inner.rpm_zone_group_id = mct.rpm_zone_group_id
           and inner.zone_id           = mct.zone_id
           and inner.zone_display_id   = mct.zone_display_id)
       when matched then
       update set mct.total_cost       = inner.total_cost,
                  mct.unit_retail_prim = inner.unit_retail_prim;
   end if;

   -- Clear out_markup_calc_tbl collection
   L_retail_calc_tbl.DELETE;


   ---------------------------------------------------------------------------------------------------------------------
   -- VAT and wastage processing
   -- This section only covers Zone processing as this function currently originates only from the itemretail form.
   -- There are two major blocks of code.  One is for SALES/SVAT and the other for GTAX.
   -- Step 1: Derive the tax rate and tax amounts and compute the total retail.
   -- Step 2: Compute wastege.
   ---------------------------------------------------------------------------------------------------------------------

   ---------------------------------------------------------------------------------------------------------------------
   -- Step 1 for SALES and SVAT
   -- Compute the tax rate and tax amount for a SALES and SVAT tax configuration.
   ---------------------------------------------------------------------------------------------------------------------
   if L_default_tax_type = TAX_SQL.LP_SVAT then
      -- Create the tax object to send to the tax engine.
      -- The cursor retrieves locations across the vat_regions associated with the item and its zones.
      -- These dataset is sent to TAX_SQL for tax computation.
      open C_GET_SVAT_BULK;
      fetch C_GET_SVAT_BULK bulk collect into L_obj_tax_calc_tbl;
      close C_GET_SVAT_BULK;

      if L_obj_tax_calc_tbl is not NULL and L_obj_tax_calc_tbl.COUNT > 0 then
         -- Call TAX_SQL to compute tax for initial retail
         if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                    L_obj_tax_calc_tbl,
                                    TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
            return FALSE;
         end if;

         merge into retail_calc_temp mct
         using (
            -- This statement performs the same calculation done in the SALES/SVAT branch in GET_ZONE_VAT_RATE.
            -- Any changes to that function should be applied here as well for consistency.
            select distinct
                   m.item,
                   m.zone_id,
                   -- Get the total tax exclusive retail for the zone
                   m.unit_retail_prim / (1 +
                                         -- vat rate computation
                                         (SUM(NVL(tax_result.o_cum_tax_pct,0) * z.count_vat )
                                             OVER(PARTITION BY z.zone_id) /
                                          SUM(z.count_vat)
                                             OVER(PARTITION BY z.zone_id))
                                         -- divide by 100 to get percentage
                                         / 100) unit_retail_prim
              from TABLE(CAST(L_obj_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_result,
                   (select rzl.zone_id,
                           l.vat_region,
                           COUNT(*) count_vat,
                           min(l.loc) loc,
                           l.loc_type
                      from rpm_zone_location rzl,
                           (select store loc, 'ST' loc_type, vat_region
                              from store
                            union
                            select wh loc, 'WH' loc_type, vat_region
                              from wh) l
                     where l.loc = rzl.location
                     group by rzl.zone_id, vat_region, l.loc_type) z,
                   retail_calc_temp m,
                   currencies c
             where tax_result.I_from_entity = z.loc
               and z.zone_id         = m.zone_id
               and tax_result.I_item = m.item
               and m.currency_code   = c.currency_code) inner
         on (    inner.item    = mct.item
             and inner.zone_id = mct.zone_id)
         when matched then
         update set mct.unit_retail_prim = NVL(inner.unit_retail_prim, 0);
      end if;
   end if;


   ---------------------------------------------------------------------------------------------------------------------
   -- Step 1 for GTAX
   -- Compute the tax rate and tax amount for a GTAX  tax configuration.
   -- These tax configuration integrate with an external tax engine as such performance is a concern.
   -- All records on retail_calc_temp are sent to the tax engine in one call.
   ---------------------------------------------------------------------------------------------------------------------
   if L_default_tax_type = TAX_SQL.LP_GTAX  then
      -- Create the tax object to send to the tax engine
      open C_GET_GTAX_BULK;
      fetch C_GET_GTAX_BULK bulk collect into L_obj_tax_calc_tbl;
      close C_GET_GTAX_BULK;

      if L_obj_tax_calc_tbl is not NULL and L_obj_tax_calc_tbl.COUNT > 0 then
         -- Call the tax engine to compute tax for initial retail
         if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                    L_obj_tax_calc_tbl,
                                    TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
            return FALSE;
         end if;

         -- Compute for the total retail based on tax amounts and persist to temp table
         if L_obj_tax_calc_tbl is not NULL or L_obj_tax_calc_tbl.COUNT > 0 then
            merge into retail_calc_temp mc
            using (
               select mct.item,
                      mct.zone_id,
                      mct.unit_retail_prim - ((mct.unit_retail_prim - result.tax_amount) * (result.tax_rate/100) - result.tax_amount) total_retail
                 from retail_calc_temp mct,
                      (select m.item,
                              m.zone_id,
                              sum(NVL(tax_result.o_cum_tax_pct,0)) / COUNT(*) as tax_rate,  -- average the cum_tax_pct
                              sum(tax_result.o_cum_tax_value) / COUNT(*) as tax_amount -- average the cum_tax_valueyeah
                         from TABLE(CAST(L_obj_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_result,
                              rpm_zone_location rzl,
                              retail_calc_temp m
                        where tax_result.I_from_entity = rzl.location
                          and rzl.zone_id = m.zone_id
                        group by m.item,
                                 m.zone_id) result
                where mct.item = result.item
                  and mct.zone_id = result.zone_id) inner
            on (    inner.item    = mc.item
                and inner.zone_id = mc.zone_id)
            when matched then
            update set mc.unit_retail_prim = inner.total_retail;
         end if;
      end if;
   end if;

   -- Clear out L_obj_tax_calc_tbl
   L_obj_tax_calc_tbl.DELETE;

   ---------------------------------------------------------------------------------------------------------------------
   -- Step 2
   -- Determine if item is a wastage item.  If so, inflate the cost of the item to reflect the wastage percentage.
   -- Persist the results to the temp table.
   ---------------------------------------------------------------------------------------------------------------------
   merge into retail_calc_temp mct
   using (
      select item,
             rpm_zone_group_id,
             zone_id,
             zone_display_id,
             total_cost * (1 + NVL(waste_pct / 100, 0)) wastage_cost
        from retail_calc_temp
       where waste_type is not NULL
         and waste_type in ('SP', 'SL')) inner
   on (    inner.item              = mct.item
       and inner.rpm_zone_group_id = mct.rpm_zone_group_id
       and inner.zone_id           = mct.zone_id
       and inner.zone_display_id   = mct.zone_display_id)
   when matched then
   update set mct.total_cost = inner.wastage_cost;


   ---------------------------------------------------------------------------------------------------------------------
   -- Markup processing for remaining items.
   -- Compute markup based on retail and cost values.
   ---------------------------------------------------------------------------------------------------------------------
   merge into retail_calc_temp mct
   using (
      select item,
             rpm_zone_group_id,
             zone_id,
             zone_display_id,
             case
                when markup_calc_type = 'C' then
                   case
                      when NVL(total_cost, 0) = 0 then
                         0
                      else
                         ROUND((unit_retail_prim - total_cost) / total_cost * 100, 4)
                   end
                when markup_calc_type = 'R' then
                   case
                      when NVL(unit_retail_prim, 0) = 0 then
                         0
                      else
                         ROUND((unit_retail_prim - total_cost) / unit_retail_prim * 100, 4)
                   end
                else
                   NULL
             end selling_mark_up
        from retail_calc_temp
       where item_xform_ind != 'Y') inner
   on (    inner.item              = mct.item
       and inner.rpm_zone_group_id = mct.rpm_zone_group_id
       and inner.zone_id           = mct.zone_id
       and inner.zone_display_id   = mct.zone_display_id)
   when matched then
   update set mct.selling_mark_up = inner.selling_mark_up;

   -- update multi unit markup_percent
   open MUTLI_SELLING_MARKUP;
   fetch MUTLI_SELLING_MARKUP BULK COLLECT into L_multi_markup_details;
   close MUTLI_SELLING_MARKUP;
   FOR indx in 1.. L_multi_markup_details.COUNT LOOP
      if L_multi_markup_details(indx).multi_units IS NOT NULL
         and L_multi_markup_details(indx).multi_unit_retail IS NOT NULL
         and L_multi_markup_details(indx).multi_selling_uom IS NOT NULL then
         if UOM_SQL.CONVERT(O_error_message,
                           L_qty,
                           L_multi_markup_details(indx).multi_selling_uom,
                           L_multi_markup_details(indx).multi_unit_retail,
                           L_multi_markup_details(indx).selling_uom,
                           L_multi_markup_details(indx).item,
                           NULL,
                           NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                  L_multi_markup_details(indx).multi_selling_uom,
                                                  L_multi_markup_details(indx).selling_uom);
            return FALSE;
         end if;
         ---
         L_retail := L_qty / L_multi_markup_details(indx).multi_units;
         if L_multi_markup_details(indx).standard_uom != L_multi_markup_details(indx).selling_uom then
            L_retail := L_multi_markup_details(indx).unit_retail;
         end if;
         if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                             L_primary_currency_code) = FALSE then
            return FALSE;
         end if;
         --- primary currency information ---
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_retail,
                                     L_multi_markup_details(indx).currency_code,
                                     L_primary_currency_code,
                                     L_multi_unit_retail_prm,-- out
                                     'R',
                                     NULL,
                                     NULL)then
            return FALSE;
         end if;
         if not MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM(O_error_message,
                                                    L_multi_markup_details(indx).multi_selling_mark_up , --output
                                                    L_multi_markup_details(indx).get_vat_ind,
                                                    L_multi_markup_details(indx).markup_calc_type,
                                                    NULL,
                                                    L_multi_unit_retail_prm,
                                                    L_multi_markup_details(indx).item,
                                                    NULL,
                                                    L_multi_markup_details(indx).loc_type,
                                                    L_multi_markup_details(indx).zone_id,
                                                    NULL,
                                                    NULL,
                                                    NULL) then
            return FALSE;
         end if;
      end if;
      EXIT WHEN indx =L_multi_markup_details.COUNT;
   END LOOP;
   --update the multi_selling_mark_up
   FORALL indx in 1..L_multi_markup_details.COUNT
   update retail_calc_temp
      set multi_selling_mark_up =L_multi_markup_details(indx).multi_selling_mark_up
   where rowid =L_multi_markup_details(indx).rowid;
   --
   -- Clear out O_item_pricing_table
   IO_item_pricing_table.DELETE;

   -- Copy markup calculation results from retail_calc_temp to O_item_pricing_table
   select item,
          rpm_zone_group_id,
          zone_id,
          zone_display_id,
          zone_description,
          unit_retail,
          selling_unit_retail,
          selling_uom,
          multi_units,
          multi_unit_retail,
          multi_selling_uom,
          base_retail_ind,
          currency_code,
          standard_uom,
          selling_mark_up,
          multi_selling_mark_up,
          default_to_children,
          unit_retail_prim,
          selling_unit_retail_prim,
          multi_unit_retail_prim,
          unit_retail_euro,
          selling_unit_retail_euro,
          multi_unit_retail_euro,
          error_message,
          return_code,
          area_diff_zone_ind
   BULK COLLECT INTO IO_item_pricing_table
     from retail_calc_temp;

   -- Store record count for checking after markup calculation
   if L_initial_count != IO_item_pricing_table.COUNT then
      O_error_message := SQL_LIB.CREATE_MSG('MARKUP_CALC_BULK_ERROR',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- Clear out retail_calc_temp table
   delete from retail_calc_temp;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM_BULK',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_MARKUP_PERCENT_ITEM_BULK;
-------------------------------------------------------------------------------------------------------
END MARKUP_SQL;
/