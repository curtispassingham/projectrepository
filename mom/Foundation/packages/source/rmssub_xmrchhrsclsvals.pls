
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRSCLS_VALIDATE AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subclass_rec    OUT   NOCOPY   SUBCLASS%ROWTYPE,
                       I_message         IN             "RIB_XMrchHrSclsDesc_REC",
                       I_message_type    IN             VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subclass_rec    OUT   NOCOPY   SUBCLASS%ROWTYPE,
                       I_message         IN             "RIB_XMrchHrSclsRef_REC")
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRSCLS_VALIDATE;
/
