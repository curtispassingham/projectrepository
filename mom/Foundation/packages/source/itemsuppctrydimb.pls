CREATE OR REPLACE PACKAGE BODY ITEM_SUPP_COUNTRY_DIM_SQL AS
-------------------------------------------------------------------------------
--Function Name: GET_ROW
--Purpose      : This function retrieves item dimensions information required
--               in the calculation of Cost Per UOM.
-------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists                  IN OUT   BOOLEAN,
                 O_item_supp_country_dim   IN OUT   ITEM_SUPP_COUNTRY_DIM%ROWTYPE,
                 I_item                    IN       ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                 I_supplier                IN       ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                 I_origin_country          IN       ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                 I_dim_object              IN       ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)

   RETURN BOOLEAN IS

   L_program   VARCHAR2(62) := 'ITEM_SUPP_COUNTRY_DIM_SQL.GET_ROW';

   cursor C_GET_ROW is
   select *
     from item_supp_country_dim
    where item           = I_item
      and supplier       = I_supplier
      and origin_country = I_origin_country
      and dim_object     = I_dim_object;


BEGIN

   O_item_supp_country_dim := NULL;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      O_exists := FALSE;
      return TRUE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                             L_program,
                                             NULL);
      O_exists := FALSE;
      return TRUE;
   end if;

   if I_origin_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country',
                                             L_program,
                                             NULL);
      O_exists := FALSE;
      return TRUE;
   end if;

   if I_dim_object is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dim_object',
                                             L_program,
                                             NULL);
      O_exists := FALSE;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item: '||I_item ||'Supplier: '||to_char(I_supplier)||'Origin_country: '||I_origin_country||
                    'Dim_object: '||I_dim_object);
   open C_GET_ROW;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item: '||I_item ||'Supplier: '||to_char(I_supplier)||'Origin_country: '||I_origin_country||
                    'Dim_object: '||I_dim_object);
   fetch C_GET_ROW into O_item_supp_country_dim;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ROW',
                    'ITEM_SUPP_COUNTRY_DIM',
                    'Item: '||I_item ||'Supplier: '||to_char(I_supplier)||'Origin_country: '||I_origin_country||
                    'Dim_object: '||I_dim_object);
   close C_GET_ROW;

   O_exists := (O_item_supp_country_dim.item is NOT NULL);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ROW;
-------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists                  IN OUT   BOOLEAN,
                 O_presentation_method     IN OUT   ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD%TYPE,
                 O_length                  IN OUT   ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                 O_width		   IN OUT   ITEM_SUPP_COUNTRY_DIM.WIDTH	%TYPE,
                 O_height                  IN OUT   ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                 O_lwh_uom                 IN OUT   ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                 O_weight                  IN OUT   ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                 O_net_weight              IN OUT   ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE,
                 O_weight_uom              IN OUT   ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                 O_liquid_volume           IN OUT   ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
                 O_liquid_volume_uom       IN OUT   ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE,
                 O_stat_cube               IN OUT   ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE,
                 O_tare_weight             IN OUT   ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE,
                 O_tare_type               IN OUT   ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE,
                 I_item                    IN       ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                 I_supplier                IN       ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                 I_origin_country          IN       ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                 I_dim_object              IN       ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN IS
   
   L_item_supp_country_dim      ITEM_SUPP_COUNTRY_DIM%ROWTYPE;
   L_program   VARCHAR2(62) := 'ITEM_SUPP_COUNTRY_DIM_SQL.GET_ROW';
   
BEGIN
   if GET_ROW(O_error_message,
              O_exists,
              L_item_supp_country_dim,
              I_item,
              I_supplier,
              I_origin_country,
              I_dim_object) = FALSE then
      return FALSE;            
                 
   end if;                 
   ---
   O_presentation_method:=  L_item_supp_country_dim.presentation_method;
   O_length             :=  L_item_supp_country_dim.length;        
   O_width		:=  L_item_supp_country_dim.width;
   O_height             :=  L_item_supp_country_dim.height;
   O_lwh_uom            :=  L_item_supp_country_dim.lwh_uom;
   O_weight             :=  L_item_supp_country_dim.weight;
   O_net_weight         :=  L_item_supp_country_dim.net_weight;
   O_weight_uom         :=  L_item_supp_country_dim.weight_uom;
   O_liquid_volume      :=  L_item_supp_country_dim.liquid_volume;
   O_liquid_volume_uom  :=  L_item_supp_country_dim.liquid_volume_uom;
   O_stat_cube          :=  L_item_supp_country_dim.stat_cube;
   O_tare_weight        :=  L_item_supp_country_dim.tare_weight;
   O_tare_type          :=  L_item_supp_country_dim.tare_type;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ROW;
-------------------------------------------------------------------------------
FUNCTION GET_NOMINAL_WEIGHT(O_error_message IN OUT VARCHAR2,
                            O_weight_cuom   IN OUT item_supp_country_dim.net_weight%TYPE,
                            I_item          IN     item_master.item%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT';

   L_net_weight        item_supp_country_dim.net_weight%TYPE := NULL;
   L_weight_uom        item_supp_country_dim.weight_uom%TYPE := NULL;
   L_supp_pack_size    item_supp_country.supp_pack_size%TYPE := NULL;
   L_nominal_weight    item_supp_country_dim.net_weight%TYPE := NULL;
   L_cuom              item_supp_country.cost_uom%TYPE;

   cursor C_WEIGHT is
      select iscd.net_weight, 
             iscd.weight_uom, 
             isc.supp_pack_size
        from item_supp_country_dim iscd,
             item_supp_country isc
       where isc.item = iscd.item
         and isc.supplier = iscd.supplier
         and isc.origin_country_id = iscd.origin_country
         and iscd.dim_object = 'CA'  -- case
         and iscd.item = I_item
         and primary_supp_ind = 'Y'
         and primary_country_ind = 'Y';

BEGIN
   ---
   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                           'I_item', 
                                           L_program,
                                           NULL);
      return FALSE;
   end if;   
   ---
   open C_WEIGHT;
   fetch C_WEIGHT into L_net_weight, L_weight_uom, L_supp_pack_size;
   close C_WEIGHT;
   ---
   if L_net_weight is NULL or L_weight_uom is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('NOMINAL_WGT_NOT_FOUND', 
                                           I_item, 
                                           NULL,
                                           NULL);
      return FALSE;
   end if;   
   ---
   -- ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT is for case; nominal weight is for eaches.
   L_nominal_weight := L_net_weight/L_supp_pack_size; 
   ---
   if not CATCH_WEIGHT_SQL.CONVERT_WEIGHT(O_error_message,
                                          O_weight_cuom,
                                          L_cuom,
                                          I_item,
                                          L_nominal_weight,
                                          L_weight_uom) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_NOMINAL_WEIGHT;
-------------------------------------------------------------------------------
FUNCTION GET_NOMINAL_WEIGHT_UOM(O_error_message   IN OUT   VARCHAR2,
                                O_weight          IN OUT   ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE,
                                O_weight_uom      IN OUT   ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                                I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT_UOM';

   L_net_weight       ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE := NULL;
   L_weight_uom       ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE := NULL;
   L_supp_pack_size   ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE := NULL;
   L_nominal_weight   ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE := NULL;
   L_cuom             ITEM_SUPP_COUNTRY.COST_UOM%TYPE;

   cursor C_WEIGHT is
      select iscd.net_weight, 
             iscd.weight_uom, 
             isc.supp_pack_size
        from item_supp_country_dim iscd,
             item_supp_country isc
       where isc.item = iscd.item
         and isc.supplier = iscd.supplier
         and isc.origin_country_id = iscd.origin_country
         and iscd.dim_object = 'CA'  -- case
         and iscd.item = I_item
         and primary_supp_ind = 'Y'
         and primary_country_ind = 'Y';

BEGIN
   ---
   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                           'I_item', 
                                           L_program,
                                           NULL);
      return FALSE;
   end if;   
   ---
   open C_WEIGHT;
   fetch C_WEIGHT into L_net_weight, L_weight_uom, L_supp_pack_size;
   close C_WEIGHT;
   ---
   if L_net_weight is NULL or L_weight_uom is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('NOMINAL_WGT_NOT_FOUND', 
                                           'I_item', 
                                           NULL,
                                           NULL);
      return FALSE;
   end if;   
   ---
   -- ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT is for case; nominal weight is for eaches.
   O_weight := L_net_weight/L_supp_pack_size; 
   O_weight_uom := L_weight_uom;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_NOMINAL_WEIGHT_UOM;
-------------------------------------------------------------------------------
END ITEM_SUPP_COUNTRY_DIM_SQL;
/
