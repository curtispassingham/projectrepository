



CREATE OR REPLACE PACKAGE BODY ITEM_NUMBER_TYPE_SQL AS
FUNCTION CHECK_ALL_NUMBERS(O_error_message IN OUT VARCHAR2,
                           I_item_no       IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_number_length NUMBER(2)    := NULL;
   L_program       VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_ALL_NUMBERS';
BEGIN

   L_number_length := LENGTH(I_item_no);

   WHILE (L_number_length > 0) LOOP
      if ((ASCII(SUBSTR(I_item_no, L_number_length, 1)) > 57 or
           ASCII(SUBSTR(I_item_no, L_number_length, 1)) < 48)) then

         O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_NEW_NUMBER',
                                                     NULL,
                                                     NULL,
                                                     NULL);
         return FALSE;
      end if;

      L_number_length := L_number_length -1;
   END LOOP;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ALL_NUMBERS;
-------------------------------------------------------------------------
FUNCTION CHECK_UPCA_FMT(O_error_message IN OUT VARCHAR2,
                        I_item_no       IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_UPCA_FMT';
BEGIN

   if LENGTH(I_item_no) != 12 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_UPCA',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY_UCC(O_error_message, L_return_code, I_item_no);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_UPCA_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_UPCAS_FMT(O_error_message    OUT VARCHAR2,
                         I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_UPCAS_FMT';
BEGIN

   /* The length is the standard UPC-A (12) plus '-' and a 2-5 digit
   supplement code, which makes the codelength between 15 and 18
   */
   if LENGTH(I_item_no) < 15 OR LENGTH(I_item_no) > 18 OR SUBSTR(I_item_no, 13, 1) != '-' then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_UPC_SUPP',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, SUBSTR(I_item_no, 14, LENGTH(I_item_no)-13)) = FALSE) then
      return FALSE;
   end if;

   if CHECK_UPCA_FMT(O_error_message, SUBSTR(I_item_no, 1, 12)) = FALSE then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_UPCAS_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_UPCE_FMT(O_error_message    OUT VARCHAR2,
                        I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_UPC_A VARCHAR2(12) := NULL;
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_UPCE_FMT';
BEGIN

   if LENGTH(I_item_no) != 8 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_UPCE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   UPC_E_EXPAND(O_error_message, L_return_code, L_UPC_A, I_item_no);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_UPCE_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_UPCES_FMT(O_error_message    OUT VARCHAR2,
                         I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_UPC_A VARCHAR2(12) := NULL;
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_UPCES_FMT';
BEGIN

   /* The length is the standard UPC-E (8) plus '-' and a 2-5 digit
   supplement code, which makes the codelength between 11 and 14
   */
   if LENGTH(I_item_no) < 11 OR LENGTH(I_item_no) > 14 OR SUBSTR(I_item_no, 9, 1) != '-' then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_UPC_SUPP',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, SUBSTR(I_item_no, 1, 8)) = FALSE) then
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, SUBSTR(I_item_no, 10, LENGTH(I_item_no)-9)) = FALSE) then
      return FALSE;
   end if;

   UPC_E_EXPAND(O_error_message, L_return_code, L_UPC_A, SUBSTR(I_item_no, 1, 8));
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_UPCES_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_UCC14_FMT(O_error_message    OUT VARCHAR2,
                         I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_UCC14_FMT';
BEGIN

   if LENGTH(I_item_no) != 14 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_UCC14',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY_UCC(O_error_message, L_return_code, I_item_no);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_UCC14_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_ITEM_FMT(O_error_message    OUT VARCHAR2,
                        I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_check_digit NUMBER(2) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_ITEM_FMT';
BEGIN

   if LENGTH(I_item_no) != 9 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_ITEM_FMT',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY(L_check_digit, I_item_no);
   if L_check_digit = -1 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_CHK_DIG',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ITEM_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_EAN8_FMT(O_error_message    OUT VARCHAR2,
                        I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_EAN8_FMT';
BEGIN

   if LENGTH(I_item_no) != 8 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_EAN8',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY_UCC(O_error_message, L_return_code, I_item_no);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_EAN8_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_EAN13_FMT(O_error_message    OUT VARCHAR2,
                         I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_EAN13_FMT';
BEGIN

   if LENGTH(I_item_no) != 13 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_EAN13',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY_UCC(O_error_message, L_return_code, I_item_no);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_EAN13_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_EAN13S_FMT(O_error_message    OUT VARCHAR2,
                          I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_EAN13S_FMT';
BEGIN

   /* The length is the standard EAN13 (13) plus '-' and a 2-5 digit
   supplement code, which makes the codelength between 16 and 19
   */
   if LENGTH(I_item_no) < 16 OR LENGTH(I_item_no) > 19 OR SUBSTR(I_item_no, 14, 1) != '-' then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_UPC_SUPP',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, SUBSTR(I_item_no, 15, LENGTH(I_item_no)-14)) = FALSE) then
      return FALSE;
   end if;

   if CHECK_EAN13_FMT(O_error_message, SUBSTR(I_item_no, 1, 13)) = FALSE then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_EAN13S_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_SSCC_FMT(O_error_message    OUT VARCHAR2,
                        I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_SSCC_FMT';
BEGIN

   if LENGTH(I_item_no) != 18 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_SSCC',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY_UCC(O_error_message, L_return_code, I_item_no);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_SSCC_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_NDC_FMT(O_error_message    OUT VARCHAR2,
                       I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_NDC_FMT';
BEGIN

   if LENGTH(I_item_no) != 12 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_NDC',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY_UCC(O_error_message, L_return_code, I_item_no);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_NDC_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_PLU_FMT(O_error_message    OUT VARCHAR2,
                       I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_PLU_FMT';
BEGIN

   if LENGTH(I_item_no) < 4 OR LENGTH(I_item_no) > 5 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_PLU',
                                                  '4 or 5',
                                                  LENGTH(I_item_no),
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_PLU_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_ISBN10_FMT(O_error_message    OUT VARCHAR2,
                        I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_ISBN10_FMT';
   L_isbn_length   NUMBER;
BEGIN

   L_isbn_length := length(I_item_no);

   if L_isbn_length != 10 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ISBN_10CHAR',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, SUBSTR(I_item_no,1, L_isbn_length - 1)) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY_ISBN(I_item_no, L_return_code, O_error_message);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ISBN10_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_ISBN13_FMT(O_error_message    OUT VARCHAR2,
                        I_item_no          IN ITEM_MASTER.ITEM%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.CHECK_ISBN13_FMT';
   L_isbn_length   NUMBER;
BEGIN

   L_isbn_length := length(I_item_no);

   if L_isbn_length != 13 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('ISBN_13CHAR',
                                                  NULL,
                                                  NULL,
                                                  NULL);
      return FALSE;
   end if;

   if (CHECK_ALL_NUMBERS(O_error_message, SUBSTR(I_item_no,1, L_isbn_length - 1)) = FALSE) then
      return FALSE;
   end if;

   CHKDIG_VERIFY_ISBN(I_item_no, L_return_code, O_error_message);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ISBN13_FMT;
-------------------------------------------------------------------------
FUNCTION VALIDATE_FORMAT(O_error_message   IN OUT VARCHAR2,
                         I_item_no         IN     ITEM_MASTER.ITEM%TYPE,
                         I_item_type       IN     ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE)
return BOOLEAN
is
   L_program  VARCHAR2(50) := 'ITEM_NUMBER_TYPE_SQL.VALIDATE_FORMAT';
BEGIN

   if (I_item_type = 'UPC-A') then
      if (CHECK_UPCA_FMT(O_error_message, I_item_no) = FALSE) then
         return FALSE;
      end if;
   elsif I_item_type = 'UPC-AS' then
      if CHECK_UPCAS_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'UPC-E' then
      if CHECK_UPCE_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'UPC-ES' then
      if CHECK_UPCES_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'UCC14' then
      if CHECK_UCC14_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'ITEM' then
      if CHECK_ITEM_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'EAN8' then
      if CHECK_EAN8_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'EAN13' then
      if CHECK_EAN13_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'EAN13S' then
      if CHECK_EAN13S_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'ISBN10' then
      if CHECK_ISBN10_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'ISBN13' then
      if CHECK_ISBN13_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'NDC' then
      if CHECK_NDC_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'PLU' then
      if CHECK_PLU_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'SSCC' then
      if CHECK_SSCC_FMT(O_error_message, I_item_no) = FALSE then
         return FALSE;
      end if;
   elsif I_item_type = 'MANL' then
      return TRUE;
   ELSE
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('SYS_INV_ITEM',
                                            I_item_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_FORMAT;
-------------------------------------------------------------------------
FUNCTION CHECK_VPLU_FMT(O_error_message   IN OUT VARCHAR2,
                        I_item_no         IN ITEM_MASTER.ITEM%TYPE,
                        I_item_type       IN ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                        I_format_id       IN VAR_UPC_EAN.FORMAT_ID%TYPE)
return BOOLEAN
is
   L_return_code      VARCHAR2(5)                         := NULL;
   L_program          VARCHAR2(50)                        := 'ITEM_NUMBER_TYPE_SQL.CHECK_VPLU_FMT';
   L_begin_item_digit VAR_UPC_EAN.BEGIN_ITEM_DIGIT%TYPE;
   L_begin_var_digit  VAR_UPC_EAN.BEGIN_VAR_DIGIT%TYPE;
   L_check_digit      VAR_UPC_EAN.CHECK_DIGIT%TYPE;
   L_item_length      NUMBER(5);
   L_VPLU_length      NUMBER(2);

   cursor C_VPLU_ATTRIB is
      select begin_item_digit,
             begin_var_digit,
             check_digit
         from  var_upc_ean
         where format_id = I_format_id;
BEGIN
   if (CHECK_ALL_NUMBERS(O_error_message, I_item_no) = FALSE) then
      return FALSE;
   end if;

   if I_item_type != 'VPLU' then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('SYS_INV_ITEM',
                                            I_item_type,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_VPLU_length := LENGTH(I_item_no);

   if L_VPLU_length != 12 AND L_VPLU_length != 13 then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_VPLU',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if substr(I_item_no, 1, 1) != '2' then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_VPLU_START',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   CHKDIG_VERIFY_UCC(O_error_message, L_return_code, I_item_no);
   if L_return_code != 'TRUE' then
      return FALSE;
   end if;

   open  C_VPLU_ATTRIB;
   fetch C_VPLU_ATTRIB into L_begin_item_digit,
                            L_begin_var_digit,
                            L_check_digit;
   if (C_VPLU_ATTRIB%NOTFOUND) then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_PREFIX',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_VPLU_ATTRIB;
      return FALSE;
   end if;
   close C_VPLU_ATTRIB;

   --Calculate the length of the PLU within the VPLU
   if L_check_digit = 0 then
      L_item_length := L_begin_var_digit-L_begin_item_digit;
   ELSE
      L_item_length := L_check_digit-L_begin_item_digit;
   end if;

   if VALIDATE_FORMAT(O_error_message, substr(I_item_no, L_begin_item_digit, L_item_length), 'PLU') = FALSE then
      return FALSE;
   end if;

   if L_check_digit != 0 then
      CHKDIG_VERIFY_PRC(O_error_message, L_return_code, substr(I_item_no, L_check_digit, L_VPLU_length-L_check_digit));
      if L_return_code != 'TRUE' then
         return FALSE;
      end if;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_VPLU_FMT;
-------------------------------------------------------------------------
FUNCTION CHECK_VPLU_FMT(O_error_message   IN OUT VARCHAR2,
                        I_plu_code        IN ITEM_MASTER.ITEM%TYPE,
                        I_prefix_code     IN VAR_UPC_EAN.DEFAULT_PREFIX%TYPE,
                        I_format_id       IN VAR_UPC_EAN.FORMAT_ID%TYPE)
return BOOLEAN
is
   L_return_code      VARCHAR2(5)                          := NULL;
   L_program          VARCHAR2(50)                         := 'ITEM_NUMBER_TYPE_SQL.CHECK_VPLU_FMT';
   L_begin_item_digit VAR_UPC_EAN.BEGIN_ITEM_DIGIT%TYPE;
   L_begin_var_digit  VAR_UPC_EAN.BEGIN_VAR_DIGIT%TYPE;
   L_check_digit      VAR_UPC_EAN.CHECK_DIGIT%TYPE;
   L_item_length      NUMBER(1);

   cursor C_VPLU_ATTRIB is
      select begin_item_digit,
             begin_var_digit,
             check_digit
         from  var_upc_ean
         where format_id = I_format_id;
BEGIN
   open  C_VPLU_ATTRIB;
   fetch C_VPLU_ATTRIB into L_begin_item_digit,
                            L_begin_var_digit,
                            L_check_digit;
   if (C_VPLU_ATTRIB%NOTFOUND) then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_FORMAT_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_VPLU_ATTRIB;
      return FALSE;
   end if;
   close C_VPLU_ATTRIB;

   if L_begin_item_digit - 1 != LENGTH(I_prefix_code) then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_PREFIX_CODE',
                                            L_begin_item_digit - 1,
                                            LENGTH(I_prefix_code),
                                            NULL);
      return false;
   end if;

   if substr(I_prefix_code, 1, 1) != '2' then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_VPLU_START',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if L_check_digit = 0 then
      L_item_length := L_begin_var_digit-L_begin_item_digit;
   ELSE
      L_item_length := L_check_digit-L_begin_item_digit;
   end if;

   if L_item_length != LENGTH(I_plu_code) then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_PLU',
                                            L_item_length,
                                            LENGTH(I_plu_code),
                                            NULL);
      return false;
   end if;

   if VALIDATE_FORMAT(O_error_message, I_plu_code, 'PLU') = FALSE then
      return FALSE;
   end if;

return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_VPLU_FMT;
-------------------------------------------------------------------------
FUNCTION GET_NEXT(O_error_message   IN OUT VARCHAR2,
                  IO_item_no        IN OUT ITEM_MASTER.ITEM%TYPE,
                  I_item_type       IN ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE)
return BOOLEAN
is
   L_return_code VARCHAR2(5) := NULL;
   L_program VARCHAR2(50)  := 'ITEM_NUMBER_TYPE_SQL.GET_NEXT';
   L_ean13   ITEM_MASTER.ITEM%TYPE;
BEGIN
   if I_item_type = 'ITEM' then
      if ITEM_ATTRIB_SQL.NEXT_ITEM(O_error_message, IO_item_no) = FALSE then
         return FALSE;
      end if;
     return true;
   elsif I_item_type = 'UPC-A' then
      NEXT_UPC_A(O_error_message, L_return_code, IO_item_no);
      if L_return_code != 'TRUE' then
         return FALSE;
      end if;
   elsif I_item_type = 'UPC-AS' then
      NEXT_UPC_A(O_error_message, L_return_code, IO_item_no);
      if L_return_code != 'TRUE' then
         return FALSE;
      end if;
      IO_item_no := RPAD(IO_item_no,13,'-');
      IO_item_no := RPAD(IO_item_no,18,'0');
   elsif I_item_type = 'EAN13' then
      if ITEM_ATTRIB_SQL.NEXT_EAN(O_error_message,
                                  L_ean13) = FALSE then
         return FALSE;
      end if;   
      IO_item_no := L_ean13;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT;
END ITEM_NUMBER_TYPE_SQL;
/
