
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XDIFFID_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_diffid_rec      IN       DIFF_IDS%ROWTYPE,
                 I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN;
---------------------------------------------------------------------------
END RMSSUB_XDIFFID_SQL;
/

