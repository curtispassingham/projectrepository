
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFF_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
--  Function Name  :  GET_DIFF_INFO_BASED_ON_TYPE
--  Purpose        :  Retrieve diff and diff group types and descriptions.
--  Called By      :  DIFFGRPD.FMB
--  Input Values   :  I_id (diff or diff group ID)
--  Return Value   :  O_description, O_diff_type.
----------------------------------------------------------------
FUNCTION GET_DIFF_INFO_BASED_ON_TYPE ( O_error_message IN OUT VARCHAR2,
                                       O_description   IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                                       O_diff_type     IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                                       O_id_group_ind  IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                                       I_id            IN     V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE) return BOOLEAN ;
----------------------------------------------------------------
--  Function Name  :  GET_DIFF_INFO
--  Purpose        :  Retrieve diff and diff group types and descriptions.
--  Called By      :  DIFFID.FMB, DIFFGRP.FMB
--  Input Values   :  I_id (diff or diff group ID)
--  Return Value   :  O_description, O_diff_type.
----------------------------------------------------------------
FUNCTION GET_DIFF_INFO (O_error_message IN OUT   VARCHAR2,
                        O_description   IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                        O_diff_type     IN OUT   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                        O_id_group_ind  IN OUT   V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                        I_id            IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE) return BOOLEAN;

----------------------------------------------------------------
--  Function Name  :  DIFF_ID_IN_GROUP
--  Purpose        :  Determine whether a particular diff id exists in a diff group.
--  Called By      :  
--  Input Values   :  I_diff_id, I_diff_grp
--  Return Value   :  O_exists.
----------------------------------------------------------------
FUNCTION DIFF_ID_IN_GROUP (O_error_message  IN OUT   VARCHAR2,
                           O_exists         IN OUT   BOOLEAN,
                           I_diff_grp       IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                           I_diff_id        IN       DIFF_IDS.DIFF_ID%TYPE) return BOOLEAN;

----------------------------------------------------------------
--  Function Name  :  CHILDREN_EXIST
--  Purpose        :  Determine an item has differentiated children.
--  Called By      :  
--  Input Values   :  I_item
--  Return Value   :  O_children_exist
----------------------------------------------------------------
FUNCTION CHILDREN_EXIST (O_error_message  IN OUT   VARCHAR2,
                         O_children_exist IN OUT   BOOLEAN,
                         I_item           IN       ITEM_MASTER.ITEM%TYPE) return BOOLEAN;

----------------------------------------------------------------
--  Function Name  :  GET_DIFF_TYPE
--  Purpose        :  Retrieve diff and diff group types.
--  Called By      :  RMSPUB_ITEMB.PLS
--  Input Values   :  I_id (diff or diff group ID)
--  Return Value   :  O_diff_type.
----------------------------------------------------------------
FUNCTION GET_DIFF_TYPE (O_error_message IN OUT   VARCHAR2,
                        O_diff_type     IN OUT   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                        I_id            IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE) return BOOLEAN;

----------------------------------------------------------------
--     Name: GET_DIFF_INFO
--  Purpose: Retrieve id_group_ind, diff_type, diff_type_desc, diff_description
----------------------------------------------------------------
FUNCTION GET_DIFF_INFO (O_error_message   IN OUT   VARCHAR2,
                        O_diff_desc       IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                        O_diff_type_desc  IN OUT   DIFF_TYPE.DIFF_TYPE_DESC%TYPE,
                        O_diff_type       IN OUT   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                        O_id_group_ind    IN OUT   V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                        I_id              IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE) 
return BOOLEAN;
----------------------------------------------------------------
--     Name: GET_DIFF_DESC
--  Purpose: This function returns the description of a diff_id 
--           and should only be used in instances where security 
--           policy has already validated the diff.
----------------------------------------------------------------
FUNCTION GET_DIFF_DESC (O_error_message IN OUT VARCHAR2,
                        O_description   IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                        I_diff_id       IN     DIFF_IDS.DIFF_ID%TYPE) 
return BOOLEAN;  
----------------------------------------------------------------
END DIFF_SQL;
/
SHO ERR
