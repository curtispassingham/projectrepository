
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUPPLIER_FIND_SQL AUTHID CURRENT_USER AS

   --- Record type for all elements displayed in the supplier find results view
   TYPE SUPPLIER_REC IS RECORD(supplier                        SUPS.SUPPLIER%TYPE,
                              sup_name                         SUPS.SUP_NAME%TYPE,
                              sup_name_secondary               SUPS.SUP_NAME_SECONDARY%TYPE,
                              supplier_site                    SUPS.SUPPLIER%TYPE,
                              sup_site_name                    SUPS.SUP_NAME%TYPE,
                              contact_name                     SUPS.CONTACT_NAME%TYPE,
                              contact_phone                    SUPS.CONTACT_PHONE%TYPE,
                              contact_fax                      SUPS.CONTACT_FAX%TYPE,
                              contact_pager                    SUPS.CONTACT_PAGER%TYPE,
                              sup_status                       SUPS.SUP_STATUS%TYPE,
                              sup_status_desc                  CODE_DETAIL.CODE_DESC%TYPE,
                              qc_ind                           SUPS.QC_IND%TYPE,
                              qc_pct                           SUPS.QC_PCT%TYPE,
                              qc_freq                          SUPS.QC_FREQ%TYPE,
                              vc_ind                           SUPS.VC_IND%TYPE,
                              vc_pct                           SUPS.VC_PCT%TYPE,
                              vc_freq                          SUPS.VC_FREQ%TYPE,
                              currency_code                    SUPS.CURRENCY_CODE%TYPE,
                              currency_desc                    CURRENCIES.CURRENCY_DESC%TYPE,
                              lang                             SUPS.LANG%TYPE,
                              terms                            SUPS.TERMS%TYPE,
                              terms_code                       TERMS_HEAD_TL.TERMS_CODE%TYPE,
                              terms_desc                       TERMS_HEAD_TL.TERMS_DESC%TYPE,
                              freight_terms                    SUPS.FREIGHT_TERMS%TYPE,
                              freight_terms_desc               TERMS_HEAD_TL.TERMS_DESC%TYPE,
                              ret_allow_ind                    SUPS.RET_ALLOW_IND%TYPE,
                              ret_auth_req                     SUPS.RET_AUTH_REQ%TYPE,
                              ret_min_dol_amt                  SUPS.RET_MIN_DOL_AMT%TYPE,
                              ret_courier                      SUPS.RET_COURIER%TYPE,
                              handling_pct                     SUPS.HANDLING_PCT%TYPE,
                              edi_po_ind                       SUPS.EDI_PO_IND%TYPE,
                              edi_po_chg                       SUPS.EDI_PO_CHG%TYPE,
                              edi_po_confirm                   SUPS.EDI_PO_CONFIRM%TYPE,
                              edi_asn                          SUPS.EDI_ASN%TYPE,
                              edi_sales_rpt_freq               SUPS.EDI_SALES_RPT_FREQ%TYPE,
                              edi_sales_rpt_freq_desc          CODE_DETAIL.CODE_DESC%TYPE,
                              edi_supp_available_ind           SUPS.EDI_SUPP_AVAILABLE_IND%TYPE,
                              edi_contract_ind                 SUPS.EDI_CONTRACT_IND%TYPE,
                              edi_invc_ind                     SUPS.EDI_INVC_IND%TYPE,
                              edi_channel_id                   SUPS.EDI_CHANNEL_ID%TYPE,
                              cost_chg_pct_var                 SUPS.COST_CHG_PCT_VAR%TYPE,
                              cost_chg_amt_var                 SUPS.COST_CHG_AMT_VAR%TYPE,
                              replen_approval_ind              SUPS.REPLEN_APPROVAL_IND%TYPE,
                              ship_method                      SUPS.SHIP_METHOD%TYPE,
                              payment_method                   SUPS.PAYMENT_METHOD%TYPE,
                              contact_telex                    SUPS.CONTACT_TELEX%TYPE,
                              contact_email                    SUPS.CONTACT_EMAIL%TYPE,
                              settlement_code                  SUPS.SETTLEMENT_CODE%TYPE,
                              pre_mark_ind                     SUPS.PRE_MARK_IND%TYPE,
                              auto_appr_invc_ind               SUPS.AUTO_APPR_INVC_IND%TYPE,
                              dbt_memo_code                    SUPS.DBT_MEMO_CODE%TYPE,
                              freight_charge_ind               SUPS.FREIGHT_CHARGE_IND%TYPE,
                              auto_appr_dbt_memo_ind           SUPS.AUTO_APPR_DBT_MEMO_IND%TYPE,
                              prepay_invc_ind                  SUPS.PREPAY_INVC_IND%TYPE,
                              backorder_ind                    SUPS.BACKORDER_IND%TYPE,
                              vat_region                       SUPS.VAT_REGION%TYPE,
                              inv_mgmt_lvl                     SUPS.INV_MGMT_LVL%TYPE,
                              service_perf_req_ind             SUPS.SERVICE_PERF_REQ_IND%TYPE,
                              invc_pay_loc                     SUPS.INVC_PAY_LOC%TYPE,
                              invc_receive_loc                 SUPS.INVC_RECEIVE_LOC%TYPE,
                              addinvc_gross_net                SUPS.ADDINVC_GROSS_NET%TYPE,
                              delivery_policy                  SUPS.DELIVERY_POLICY%TYPE,
                              comment_desc                     SUPS.COMMENT_DESC%TYPE,
                              default_item_lead_time           SUPS.DEFAULT_ITEM_LEAD_TIME%TYPE,
                              duns_number                      SUPS.DUNS_NUMBER%TYPE,
                              duns_loc                         SUPS.DUNS_LOC%TYPE,
                              bracket_costing_ind              SUPS.BRACKET_COSTING_IND%TYPE,
                              bracket_costing_desc             CODE_DETAIL.CODE_DESC%TYPE,
                              vmi_order_status                 SUPS.VMI_ORDER_STATUS%TYPE,
                              dsd_ind                          SUPS.DSD_IND%TYPE,
                              external_ref_id                  SUPS.EXTERNAL_REF_ID%TYPE,
                              error_message                    RTK_ERRORS.RTK_TEXT%TYPE,
                              return_code                      NUMBER);

   --- Table Type for the above declared record

   TYPE SUPPLIER_TBL IS TABLE OF SUPPLIER_REC INDEX BY BINARY_INTEGER;

   --------------------------------------------------------------------------------------------

   -- Function: QUERY_PROCEDURE
   -- Purpose : This procedure will return a table type with data required to be displayed on the
   --           Supplier Find screen - results section. The data is fetched from sups table
   --           for the condition passed as input and is ordered based on the input.
   --------------------------------------------------------------------------------------------
   PROCEDURE QUERY_PROCEDURE(IO_supplier_tbl   IN OUT   SUPPLIER_FIND_SQL.SUPPLIER_TBL,
                             I_where_clause    IN       VARCHAR2,
                             I_order_by        IN       VARCHAR2,
                             I_order_type      IN       VARCHAR2);

END SUPPLIER_FIND_SQL;
/
