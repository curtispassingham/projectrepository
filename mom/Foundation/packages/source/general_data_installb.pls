SET DEFINE OFF
CREATE OR REPLACE PACKAGE BODY GENERAL_DATA_INSTALL AS
--------------------------------------------------------------------------------------------
FUNCTION CHANGE_PRIM_DLVY_CTRY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item                IN     ITEM_COST_HEAD.ITEM%TYPE,
                               I_supplier            IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_origin_country_id   IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                               I_prim_del_country_id IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION CALC_VAT_COST_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item            IN                ITEM_MASTER.ITEM%TYPE,
                           I_supplier        IN                SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION VAT_CODE_REGION(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   insert into vat_region values (1000, 'Vat Region 1000', 'E', 'N',NULL, NULL);
   ---
   insert into vat_codes values ('S', 'Standard','N');
   insert into vat_codes values ('E', 'Exempt','N');
   insert into vat_codes values ('C', 'Composite','N');
   insert into vat_codes values ('Z', 'Zero','N');
   ---
   insert into vat_code_rates values('S',
                                     to_date('01-01-1995', 'DD_MM_YYYY'),
                                     10.00,
                                     to_date('01-01-1995', 'DD_MM_YYYY'),
                                     'RMSDEV110');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.VAT_CODE_REGION',
                                            to_char(SQLCODE));
      return FALSE;
END VAT_CODE_REGION;
--------------------------------------------------------------------------------------------
FUNCTION SYSTEM_VARIABLES(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO SYSTEM_VARIABLES ( LAST_EOM_HALF_NO,
                                  LAST_EOM_MONTH_NO,
                                  LAST_EOM_DATE,
                                  NEXT_EOM_DATE,
                                  LAST_EOM_START_HALF,
                                  LAST_EOM_END_HALF,
                                  LAST_EOM_START_MONTH,
                                  LAST_EOM_MID_MONTH,
                                  LAST_EOM_NEXT_HALF_NO,
                                  LAST_EOM_DAY,
                                  LAST_EOM_WEEK,
                                  LAST_EOM_MONTH,
                                  LAST_EOM_YEAR,
                                  LAST_EOM_WEEK_IN_HALF,
                                  LAST_EOM_DATE_UNIT,
                                  NEXT_EOM_DATE_UNIT,
                                  LAST_EOW_DATE,
                                  LAST_EOW_DATE_UNIT,
                                  NEXT_EOW_DATE_UNIT,
                                  LAST_CONT_ORDER_DATE )
                         VALUES ( 20042, 5,  TO_Date( '10/31/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '11/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '06/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '12/25/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '11/15/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  20051, 1, 2, 11, 2004, 19,
                                  TO_Date( '01/22/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '02/26/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '10/03/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '01/22/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  TO_Date( '01/29/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                                  NULL );
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.SYSTEM_VARIABLES',
                                            to_char(SQLCODE));
      return FALSE;
END SYSTEM_VARIABLES;
--------------------------------------------------------------------------------------------
FUNCTION BANNER(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN
   INSERT INTO BANNER ( BANNER_ID, BANNER_NAME ) VALUES ( 1, 'B&M');
   ---
   INSERT INTO BANNER ( BANNER_ID, BANNER_NAME ) VALUES ( 2, 'Direct');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.BANNER',
                                            to_char(SQLCODE));
      return FALSE;
END BANNER;
--------------------------------------------------------------------------------------------
FUNCTION CHANNEL(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO CHANNELS ( CHANNEL_ID, CHANNEL_NAME, CHANNEL_TYPE,BANNER_ID )
                 VALUES ( 4, 'Brick & Mortar', 'BANDM', 1);
   INSERT INTO CHANNELS ( CHANNEL_ID, CHANNEL_NAME, CHANNEL_TYPE,BANNER_ID )
                 VALUES ( 5, 'Outlet', 'BANDM', 1);
   INSERT INTO CHANNELS ( CHANNEL_ID, CHANNEL_NAME, CHANNEL_TYPE,BANNER_ID )
                 VALUES ( 1, 'Brick & Mortar', 'BANDM', 1);
   INSERT INTO CHANNELS ( CHANNEL_ID, CHANNEL_NAME, CHANNEL_TYPE,BANNER_ID )
                 VALUES ( 2, 'Brick & Mortar', 'BANDM', 1);
   INSERT INTO CHANNELS ( CHANNEL_ID, CHANNEL_NAME, CHANNEL_TYPE,BANNER_ID )
                 VALUES ( 3, 'Direct', 'DIRECT', 2);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.CHANNEL',
                                            to_char(SQLCODE));
      return FALSE;
END CHANNEL;
--------------------------------------------------------------------------------------------
FUNCTION BUYERS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (1000,'Henry Quinton Gary Maeron Roden Xavier Elijah Stubbs III', '6125259845', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (1001,'Charles Bott', '6125252612', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (1002,'Matt Wilsman', '6125251034', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (1003,'Ann Woodley',  '6125252864', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (305,'Henry Roden ', '6125259995', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (610,'Charles Freidman', '6129952612', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (635,'Will Sheperd', '6125299034', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (307,'Matt Wilson',  '6125992864', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (670,'Henry Radison', '6125259995', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (615,'Chuck Honway', '6129952612', '6125259800');
   insert into buyer (BUYER,BUYER_NAME,BUYER_PHONE,BUYER_FAX) values (620,'Billy Sharp', '6125299034', '6125259800');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL. BUYERS',
                                            to_char(SQLCODE));
      return FALSE;
END BUYERS;
--------------------------------------------------------------------------------------------
FUNCTION BRAND (O_error_message      IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO BRAND (BRAND_NAME,BRAND_DESCRIPTION,CREATE_ID,CREATE_DATETIME,LAST_UPDATE_ID,LAST_UPDATE_DATETIME)
               VALUES ('PUMA','PUMA',GET_USER,SYSDATE,GET_USER,SYSDATE);
   INSERT INTO BRAND (BRAND_NAME,BRAND_DESCRIPTION,CREATE_ID,CREATE_DATETIME,LAST_UPDATE_ID,LAST_UPDATE_DATETIME)
               VALUES ('ADIDAS','ADIDAS',GET_USER,SYSDATE,GET_USER,SYSDATE);
   INSERT INTO BRAND (BRAND_NAME,BRAND_DESCRIPTION,CREATE_ID,CREATE_DATETIME,LAST_UPDATE_ID,LAST_UPDATE_DATETIME)
               VALUES ('NIKE','NIKE',GET_USER,SYSDATE,GET_USER,SYSDATE);
   INSERT INTO BRAND (BRAND_NAME,BRAND_DESCRIPTION,CREATE_ID,CREATE_DATETIME,LAST_UPDATE_ID,LAST_UPDATE_DATETIME)
               VALUES ('LEE','LEE',GET_USER,SYSDATE,GET_USER,SYSDATE);
   INSERT INTO BRAND (BRAND_NAME,BRAND_DESCRIPTION,CREATE_ID,CREATE_DATETIME,LAST_UPDATE_ID,LAST_UPDATE_DATETIME)
               VALUES ('REEBOK','REEBOK',GET_USER,SYSDATE,GET_USER,SYSDATE);
    ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.BRAND',
                                            to_char(SQLCODE));
      return FALSE;
END BRAND;
--------------------------------------------------------------------------------------------
FUNCTION MERCHANTS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 700, 'Women''s Casuals Merchandiser', '612-897-4100', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 701, 'Women''s Denim Merchandiser', '612-897-4101', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 702, 'Women''s Classics Merchandiser', '612-897-4102', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 721, 'Men''s Classics Merchandiser', '612-897-4121', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 703, 'Women''s Accessories Merchandiser', '612-897-4103', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 720, 'Men''s Casuals Merchandiser', '612-897-4120', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 730, 'Girls Fashion Merchandiser', '612-897-4130', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 731, 'Boys Fashions Merchandiser', '612-897-4130', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 740, 'Women''s Footwear Merchandiser', '612-897-4140', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 741, 'Men''s Footwear Merchandiser', '612-897-4150', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 743, 'Childrens Footwear Merchandiser', '612-897-4150', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 760, 'Fashion Jewelry Merchandiser', '612-897-4160', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 761, 'Fine Jewelry Merchandiser', '612-897-4162', '612-897-4000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 810, 'Consumer Electronic Merchandiser', '612-777-3810', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 815, 'Home Office Merchandiser', '612-777-3815', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 820, 'Home Fashions Merchandiser', '612-777-3820', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 825, 'Sporting Goods Merchandiser', '612-777-3825', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 830, 'Home Improvement Merchandiser', '612-777-3830', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 835, 'Hardware Merchandiser', '612-777-3835', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 840, 'Pets Merchandiser', '612-777-3840', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 845, 'Automotive Merchandiser', '612-777-3845', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 850, 'Media Merchandiser', '612-777-3850', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 855, 'Toys Merchandiser', '612-777-3855', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 860, 'Optical Merchandiser', '612-777-3860', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 865, 'Pharmacy Merchandiser', '612-777-3865', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 870, 'Craft and Hobby Merchandiser', '612-777-3870', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 875, 'Cards and Party Merchandiser', '612-777-3875', '612-777-7000');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 601, 'Beer, Liquor, Wine Merchandiser', '612 629 7632', '612 629 6542');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 602, 'Commodities Desk', '612 629 1987', '612 629 6746');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 603, 'Dairy and Refig.  Products Desk', '612 629 0101', '612 629 5567');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 604, 'Fresh Produce Desk', '612 629 0013', '612 629 1104');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 605, 'Frozen Foods and Ice Cream Desk', '612 629 1011', '612 629 2304');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 606, 'General Merchandise Desk', '612 629 6545', '612 629 8902');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 607, 'Health and Beauty Care Desk', '612 629 7544', '612 629 4792');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 609, 'Household Products Desk', '612 629 3455', '612 629 0133');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 610, 'Snack Foods Desk', '612 629 2435', '612 629 8439');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 611, 'Tabacco Desk', '612 629 9089', '612 629 6722');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 612, 'General Groceries', '612 629 9432', '612 629 8382');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 608, 'Prepared Foods Desk', '612 629 9866', '612 629 8733');
   INSERT INTO MERCHANT ( MERCH, MERCH_NAME, MERCH_PHONE, MERCH_FAX )
                 VALUES ( 800, 'General Hardlines Merchandiser', '612-777-0800', '612-777-7000');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL. MERCHANTS',
                                            to_char(SQLCODE));
      return FALSE;
END MERCHANTS;
--------------------------------------------------------------------------------------------
FUNCTION COMPHEAD(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO COMPHEAD ( COMPANY,
                          CO_NAME,
                          CO_ADD1,
                          CO_ADD2,
                          CO_ADD3,
                          CO_CITY,
                          CO_STATE,
                          CO_COUNTRY,
                          CO_POST )
                 VALUES ( 1,
                          'Retailers Ltd',
                          '1300 Avenue Of the Americas',
                          'Suite 1300',
                          NULL,
                          'New York',
                          'NY',
                          'US',
                          '10019' );
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL. COMPHEAD',
                                            to_char(SQLCODE));
      return FALSE;
END COMPHEAD;
--------------------------------------------------------------------------------------------
FUNCTION LOCHIER(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   --*********************************** CHAINS *******
   INSERT INTO CHAIN ( CHAIN, CHAIN_NAME, MGR_NAME, CURRENCY_CODE )
              VALUES ( 1, 'Chain 1*', NULL, NULL);
   INSERT INTO CHAIN ( CHAIN, CHAIN_NAME, MGR_NAME, CURRENCY_CODE )
              VALUES ( 2, 'Chain 2', NULL, NULL);
   INSERT INTO CHAIN ( CHAIN, CHAIN_NAME, MGR_NAME, CURRENCY_CODE )
              VALUES ( 3, 'Chain 3', NULL, NULL);
   --************************************ AREAS *******
   INSERT INTO AREA ( AREA, AREA_NAME, MGR_NAME, CHAIN, CURRENCY_CODE )
             VALUES ( 1, 'USA*', NULL, 1, 'USD');
   INSERT INTO AREA ( AREA, AREA_NAME, MGR_NAME, CHAIN, CURRENCY_CODE )
             VALUES ( 4, 'Latin America', NULL, 1, 'MXN');
   INSERT INTO AREA ( AREA, AREA_NAME, MGR_NAME, CHAIN, CURRENCY_CODE )
             VALUES ( 2, 'Europe', NULL, 1, 'EUR');
   INSERT INTO AREA ( AREA, AREA_NAME, MGR_NAME, CHAIN, CURRENCY_CODE )
             VALUES ( 3, 'Canada', NULL, 1, 'CAD');
   INSERT INTO AREA ( AREA, AREA_NAME, MGR_NAME, CHAIN, CURRENCY_CODE )
             VALUES ( 5, 'Asia Pacific', NULL, 1, 'JPY');
   --************************************ REGIONS *******
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 54, 'Singapore', NULL, 5, 'SGD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 55, 'Hong Kong', NULL, 5, 'HKD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 11, 'Southeastern US', NULL, 1, 'USD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 12, 'Northeastern US', NULL, 1, 'USD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 13, 'Midwestern US', NULL, 1, 'USD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 14, 'Northwestern US', NULL, 1, 'USD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 15, 'Southwestern US', NULL, 1, 'USD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 41, 'Federal District', NULL, 4, 'MXN');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 21, 'United Kingdom', NULL, 2, 'GBP');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 22, 'Southern Europe', NULL, 2, 'EUR');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 23, 'Northern Europe', NULL, 2, 'EUR');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 24, 'Scandinavia', NULL, 2, 'EUR');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 31, 'Eastern Canada', NULL, 3, 'CAD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 32, 'Canadian Lakes', NULL, 3, 'CAD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 33, 'Western Canada', NULL, 3, 'CAD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 51, 'Australia', NULL, 5, 'AUD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 52, 'New Zealand', NULL, 5, 'NZD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 53, 'Japan', NULL, 5, 'JPY');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 25, 'France', NULL, 2, 'EUR');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 16, 'Outlet', NULL, 1, 'USD');
   INSERT INTO REGION ( REGION, REGION_NAME, MGR_NAME, AREA,CURRENCY_CODE )
               VALUES ( 17, 'Direct', NULL, 1, 'USD');
   --**************************************** DISTRICTS *******
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 570, 'Victoria', NULL, 51, 'AUD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 571, 'Western Australia', NULL, 51, 'AUD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 572, 'Queensland', NULL, 51, 'AUD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 581, 'South Island', NULL, 52, 'NZD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 541, 'Singapore', NULL, 54, 'SGD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 551, 'Kowloon', NULL, 55, 'HKD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 552, 'Hong Kong Island', NULL, 55, 'HKD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 111, 'North Carolina', NULL, 11, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 112, 'Georgia', NULL, 11, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 113, 'Florida', NULL, 11, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 114, 'Tennessee', NULL, 11, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 115, 'Texas', NULL, 11, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 121, 'Massachusetts', NULL, 12, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 122, 'New York', NULL, 12, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 123, 'Pennsylvania', NULL, 12, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 124, 'Ohio', NULL, 12, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 131, 'Illinois', NULL, 13, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 132, 'Indiana', NULL, 13, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 133, 'Minnesota', NULL, 13, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 134, 'Missouri', NULL, 13, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 141, 'Washington', NULL, 14, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 142, 'Oregon', NULL, 14, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 151, 'Arizona', NULL, 15, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 152, 'New Mexico', NULL, 15, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 153, 'California', NULL, 15, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 411, 'Ciudad de Mexico', NULL, 41, 'MXN');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 211, 'England', NULL, 21, 'GBP');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 212, 'Scotland', NULL, 21, 'GBP');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 221, 'France', NULL, 22, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 222, 'Spain', NULL, 22, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 231, 'Netherlands', NULL, 23, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 232, 'Germany', NULL, 23, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 241, 'Norway', NULL, 24, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 242, 'Sweden', NULL, 24, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 311, 'Quebec', NULL, 31, 'CAD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 321, 'Ontario', NULL, 32, 'CAD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 331, 'British Columbia', NULL, 33, 'CAD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 511, 'New South Wales', NULL, 51, 'AUD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 512, 'North Island', NULL, 52, 'NZD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 513, 'Tokyo', NULL, 53, 'JPY');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 514, 'Paris', NULL, 25, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 515, 'Ile de France', NULL, 25, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 516, 'France Nord West', NULL, 25, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 517, 'France Nord East', NULL, 25, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 518, 'France Sud West', NULL, 25, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 519, 'France Sud East', NULL, 25, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 520, 'France Corse', NULL, 25, 'EUR');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 160, 'Outlet', NULL, 16, 'USD');
   INSERT INTO DISTRICT ( DISTRICT, DISTRICT_NAME, MGR_NAME, REGION,CURRENCY_CODE )
                 VALUES ( 170, 'Direct', NULL, 17, 'USD');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL. LOCHIER',
                                            to_char(SQLCODE));
      return FALSE;
END LOCHIER;

--------------------------------------------------------------------------------------------
FUNCTION TSF_ZONE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   insert into tsfzone (TRANSFER_ZONE,DESCRIPTION) values (1000, 'Transfer Zone 1');
   insert into tsfzone (TRANSFER_ZONE,DESCRIPTION) values (1001, 'Transfer Zone 2');
   insert into tsfzone (TRANSFER_ZONE,DESCRIPTION) values (1002, 'Transfer Zone 3');
   insert into tsfzone (TRANSFER_ZONE,DESCRIPTION) values (1003, 'Transfer Zone 4');
   insert into tsfzone (TRANSFER_ZONE,DESCRIPTION) values (1004, 'Transfer Zone 5 - Longer Description Value 12345678901234567890');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.TSF_ZONE',
                                            to_char(SQLCODE));
      return FALSE;
END TSF_ZONE;
--------------------------------------------------------------------------------------------
FUNCTION TSF_ENTITY(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO TSF_ENTITY ( TSF_ENTITY_ID,
                            TSF_ENTITY_DESC )
                   VALUES ( 1000,
                            'Regular Stores');
   INSERT INTO TSF_ENTITY ( TSF_ENTITY_ID,
                            TSF_ENTITY_DESC )
                   VALUES ( 2000,
                            'Outlet Stores');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.TSF_ENTITY',
                                            to_char(SQLCODE));
      return FALSE;
END TSF_ENTITY;
--------------------------------------------------------------------------------------------
FUNCTION STORE_FORMAT(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 1, 'Neighborhood');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 2, 'Conv Supermarket');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 3, 'Upscale Supermarket');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 4, 'Hypermarket');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 5, 'Speciality Store');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 6, 'Virtual Store');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 7, 'Warehouse/Club');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 8, 'Fashion');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 9, 'Mass Merchant');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 10, 'Core Business');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 11, 'Expanded Offering');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 12, 'Conventional Store');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 13, 'Upscale Store');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 14, 'Virtual Store');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 15, 'Catalog Store');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 16, 'Kiosk');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 17, 'Supermarket < 1200m2');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 18, 'Supermarket');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 19, 'Hypermarket <12000m2');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 20, 'Hypermarket >12000m2');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 21, 'Category Killer');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 22, 'Hard Discount');
   INSERT INTO STORE_FORMAT ( STORE_FORMAT, FORMAT_NAME ) VALUES ( 23, 'Outlet');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.STORE_FORMAT',
                                            to_char(SQLCODE));
      return FALSE;
END STORE_FORMAT;
--------------------------------------------------------------------------------------------
FUNCTION WHS(O_error_message      IN OUT   VARCHAR2,
             I_default_tax_type   IN       VARCHAR2)
RETURN BOOLEAN IS

   L_vat_region   WH.VAT_REGION%TYPE := NULL;
   L_rms_async_id STOCK_LEDGER_INSERTS.RMS_ASYNC_ID%TYPE;
BEGIN

   if I_default_tax_type ='SVAT' then
      L_vat_region := 1000;
   end if;

   INSERT INTO WH ( WH,
                    WH_NAME,
                    EMAIL,
                    VAT_REGION,
                    ORG_HIER_TYPE,
                    ORG_HIER_VALUE,
                    CURRENCY_CODE,
                    PHYSICAL_WH,
                    PRIMARY_VWH,
                    CHANNEL_ID,
                    STOCKHOLDING_IND,
                    BREAK_PACK_IND,
                    REDIST_WH_IND,
                    DELIVERY_POLICY,
                    RESTRICTED_IND,
                    PROTECTED_IND,
                    FORECAST_WH_IND,
                    ROUNDING_SEQ,
                    REPL_IND,
                    REPL_WH_LINK,
                    REPL_SRC_ORD,
                    IB_IND,
                    IB_WH_LINK,
                    AUTO_IB_CLEAR,
                    DUNS_NUMBER,
                    DUNS_LOC,
                    TSF_ENTITY_ID,
                    FINISHER_IND,
                    INBOUND_HANDLING_DAYS,
                    ORG_UNIT_ID,
                    VWH_TYPE,
                    CUSTOMER_ORDER_LOC_IND,
                    ORG_ENTITY_TYPE )
           VALUES ( 2, 'N. America Central', NULL, L_vat_region, NULL, NULL,'USD',
                    2, 2, NULL, 'N', 'Y', 'N', 'NDD', 'N','N','N', NULL,
                    'N', NULL, NULL, 'N', NULL, 'N', NULL, NULL, NULL, 'N', 0, NULL, 'XD_RG',NULL,'R');
   ---
   INSERT INTO WH ( WH,
                    WH_NAME,
                    EMAIL,
                    VAT_REGION,
                    ORG_HIER_TYPE,
                    ORG_HIER_VALUE,
                    CURRENCY_CODE,
                    PHYSICAL_WH,
                    PRIMARY_VWH,
                    CHANNEL_ID,
                    STOCKHOLDING_IND,
                    BREAK_PACK_IND,
                    REDIST_WH_IND,
                    DELIVERY_POLICY,
                    RESTRICTED_IND,
                    PROTECTED_IND,
                    FORECAST_WH_IND,
                    ROUNDING_SEQ,
                    REPL_IND,
                    REPL_WH_LINK,
                    REPL_SRC_ORD,
                    IB_IND,
                    IB_WH_LINK,
                    AUTO_IB_CLEAR,
                    DUNS_NUMBER,
                    DUNS_LOC,
                    TSF_ENTITY_ID,
                    FINISHER_IND,
                    INBOUND_HANDLING_DAYS,
                    ORG_UNIT_ID,
                    VWH_TYPE,
                    CUSTOMER_ORDER_LOC_IND,
                    ORG_ENTITY_TYPE)
           VALUES ( 15000, 'Internal Finisher', NULL, L_vat_region, NULL, NULL,
                    'USD', 2, NULL, 1, 'Y', 'Y', 'N', 'NDD', 'N', 'N',
                    'Y', NULL, 'N', NULL, NULL, 'N', NULL, 'N', NULL, NULL, 1000, 'Y', 0, NULL, 'XD_GS','N','R');
   ---
   INSERT INTO WH ( WH,
                    WH_NAME,
                    EMAIL,
                    VAT_REGION,
                    ORG_HIER_TYPE,
                    ORG_HIER_VALUE,
                    CURRENCY_CODE,
                    PHYSICAL_WH,
                    PRIMARY_VWH,
                    CHANNEL_ID,
                    STOCKHOLDING_IND,
                    BREAK_PACK_IND,
                    REDIST_WH_IND,
                    DELIVERY_POLICY,
                    RESTRICTED_IND,
                    PROTECTED_IND,
                    FORECAST_WH_IND,
                    ROUNDING_SEQ,
                    REPL_IND,
                    REPL_WH_LINK,
                    REPL_SRC_ORD,
                    IB_IND,
                    IB_WH_LINK,
                    AUTO_IB_CLEAR,
                    DUNS_NUMBER,
                    DUNS_LOC,
                    TSF_ENTITY_ID,
                    FINISHER_IND,
                    INBOUND_HANDLING_DAYS,
                    ORG_UNIT_ID,
                    VWH_TYPE,
                    CUSTOMER_ORDER_LOC_IND,
               ORG_ENTITY_TYPE)
           VALUES ( 20001, 'Outlet', NULL, L_vat_region, NULL, NULL, 'USD', 2,
                    NULL, 5, 'Y', 'Y', 'N', 'NDD', 'N', 'N', 'Y', NULL,
                    'N', NULL, NULL, 'N', NULL, 'N', NULL, NULL, 2000, 'N', 0, NULL, 'CS_RG','N','R');
   ---
   INSERT INTO WH ( WH,
                    WH_NAME,
                    EMAIL,
                    VAT_REGION,
                    ORG_HIER_TYPE,
                    ORG_HIER_VALUE,
                    CURRENCY_CODE,
                    PHYSICAL_WH,
                    PRIMARY_VWH,
                    CHANNEL_ID,
                    STOCKHOLDING_IND,
                    BREAK_PACK_IND,
                    REDIST_WH_IND,
                    DELIVERY_POLICY,
                    RESTRICTED_IND,
                    PROTECTED_IND,
                    FORECAST_WH_IND,
                    ROUNDING_SEQ,
                    REPL_IND,
                    REPL_WH_LINK,
                    REPL_SRC_ORD,
                    IB_IND,
                    IB_WH_LINK,
                    AUTO_IB_CLEAR,
                    DUNS_NUMBER,
                    DUNS_LOC,
                    TSF_ENTITY_ID,
                    FINISHER_IND,
                    INBOUND_HANDLING_DAYS,
                    ORG_UNIT_ID,
                    VWH_TYPE,
                    CUSTOMER_ORDER_LOC_IND,
                    ORG_ENTITY_TYPE)
           VALUES ( 10001, 'Store Supply', NULL, L_vat_region, NULL, NULL, 'USD', 2,
                    NULL, 1, 'Y', 'Y', 'N', 'NDD', 'N', 'N', 'Y', NULL, 'Y', 10001,
                    1, 'N', 10002, 'N', NULL, NULL, 1000, 'N', 0, NULL, 'CS_NT','Y','R' );
   ---
   INSERT INTO WH ( WH, WH_NAME, EMAIL, VAT_REGION, ORG_HIER_TYPE, ORG_HIER_VALUE, CURRENCY_CODE,
   PHYSICAL_WH, PRIMARY_VWH, CHANNEL_ID, STOCKHOLDING_IND, BREAK_PACK_IND, REDIST_WH_IND,
   DELIVERY_POLICY, RESTRICTED_IND, PROTECTED_IND, FORECAST_WH_IND, ROUNDING_SEQ, REPL_IND,
   REPL_WH_LINK, REPL_SRC_ORD, IB_IND, IB_WH_LINK, AUTO_IB_CLEAR, DUNS_NUMBER, DUNS_LOC, TSF_ENTITY_ID,
   FINISHER_IND, INBOUND_HANDLING_DAYS, ORG_UNIT_ID, VWH_TYPE,CUSTOMER_ORDER_LOC_IND,ORG_ENTITY_TYPE) VALUES (
   10002, 'Investment Buy', NULL, L_vat_region, NULL, NULL, 'USD', 2, NULL, 2, 'Y', 'Y', 'N'
   , 'NDD', 'N', 'N', 'Y', NULL, 'N', NULL, NULL, 'Y', 10002, 'N', NULL, NULL, 1000, 'N'
   , 0, NULL, 'CS_NT','Y','R');
   INSERT INTO WH ( WH, WH_NAME, EMAIL, VAT_REGION, ORG_HIER_TYPE, ORG_HIER_VALUE, CURRENCY_CODE,
   PHYSICAL_WH, PRIMARY_VWH, CHANNEL_ID, STOCKHOLDING_IND, BREAK_PACK_IND, REDIST_WH_IND,
   DELIVERY_POLICY, RESTRICTED_IND, PROTECTED_IND, FORECAST_WH_IND, ROUNDING_SEQ, REPL_IND,
   REPL_WH_LINK, REPL_SRC_ORD, IB_IND, IB_WH_LINK, AUTO_IB_CLEAR, DUNS_NUMBER, DUNS_LOC, TSF_ENTITY_ID,
   FINISHER_IND, INBOUND_HANDLING_DAYS, ORG_UNIT_ID, VWH_TYPE,CUSTOMER_ORDER_LOC_IND,ORG_ENTITY_TYPE ) VALUES (
   10003, 'Direct', NULL, L_vat_region, NULL, NULL, 'USD', 2, NULL, 3, 'Y', 'Y', 'N', 'NDD', 'N'
   , 'N', 'Y', NULL, 'N', NULL, NULL, 'N', NULL, 'N', NULL, NULL, 1000, 'N', 0, NULL, 'CS_RG','N','R');
   INSERT INTO WH ( WH, WH_NAME, EMAIL, VAT_REGION, ORG_HIER_TYPE, ORG_HIER_VALUE, CURRENCY_CODE,
   PHYSICAL_WH, PRIMARY_VWH, CHANNEL_ID, STOCKHOLDING_IND, BREAK_PACK_IND, REDIST_WH_IND,
   DELIVERY_POLICY, RESTRICTED_IND, PROTECTED_IND, FORECAST_WH_IND, ROUNDING_SEQ, REPL_IND,
   REPL_WH_LINK, REPL_SRC_ORD, IB_IND, IB_WH_LINK, AUTO_IB_CLEAR, DUNS_NUMBER, DUNS_LOC, TSF_ENTITY_ID,
   FINISHER_IND, INBOUND_HANDLING_DAYS, ORG_UNIT_ID, VWH_TYPE,CUSTOMER_ORDER_LOC_IND,ORG_ENTITY_TYPE ) VALUES (
   5, 'AUS Warehouse', NULL, L_vat_region, NULL, NULL, 'AUD', 5, 5, NULL, 'N', 'Y', 'N', 'NDD'
   , 'N', 'N', 'N', NULL, 'N', NULL, NULL, 'N', NULL, 'N', NULL, NULL, NULL, 'N', 0, NULL, 'XD_GS',NULL,'R');
   INSERT INTO WH ( WH, WH_NAME, EMAIL, VAT_REGION, ORG_HIER_TYPE, ORG_HIER_VALUE, CURRENCY_CODE,
   PHYSICAL_WH, PRIMARY_VWH, CHANNEL_ID, STOCKHOLDING_IND, BREAK_PACK_IND, REDIST_WH_IND,
   DELIVERY_POLICY, RESTRICTED_IND, PROTECTED_IND, FORECAST_WH_IND, ROUNDING_SEQ, REPL_IND,
   REPL_WH_LINK, REPL_SRC_ORD, IB_IND, IB_WH_LINK, AUTO_IB_CLEAR, DUNS_NUMBER, DUNS_LOC, TSF_ENTITY_ID,
   FINISHER_IND, INBOUND_HANDLING_DAYS, ORG_UNIT_ID, VWH_TYPE,CUSTOMER_ORDER_LOC_IND,ORG_ENTITY_TYPE ) VALUES (
   50001, 'AU Store Supply', NULL, L_vat_region, NULL, NULL, 'AUD', 5, NULL, 1, 'Y', 'Y', 'N'
   , 'NDD', 'N', 'N', 'Y', NULL, 'Y', 50001, 1, 'N', 50002, 'N', NULL, NULL, 1000, 'N'
   , 0, NULL, 'XD_RG','N','R');
   INSERT INTO WH ( WH, WH_NAME, EMAIL, VAT_REGION, ORG_HIER_TYPE, ORG_HIER_VALUE, CURRENCY_CODE,
   PHYSICAL_WH, PRIMARY_VWH, CHANNEL_ID, STOCKHOLDING_IND, BREAK_PACK_IND, REDIST_WH_IND,
   DELIVERY_POLICY, RESTRICTED_IND, PROTECTED_IND, FORECAST_WH_IND, ROUNDING_SEQ, REPL_IND,
   REPL_WH_LINK, REPL_SRC_ORD, IB_IND, IB_WH_LINK, AUTO_IB_CLEAR, DUNS_NUMBER, DUNS_LOC, TSF_ENTITY_ID,
   FINISHER_IND, INBOUND_HANDLING_DAYS, ORG_UNIT_ID, VWH_TYPE,CUSTOMER_ORDER_LOC_IND,ORG_ENTITY_TYPE ) VALUES (
   50002, 'AU Investment Buy', NULL, L_vat_region, NULL, NULL, 'AUD', 5, NULL, 2, 'Y', 'Y', 'N'
   , 'NDD', 'N', 'N', 'Y', NULL, 'N', NULL, NULL, 'Y', 50002, 'N', NULL, NULL, 1000, 'N'
   , 0, NULL, 'XD_GS','Y','R');
   INSERT INTO WH ( WH, WH_NAME, EMAIL, VAT_REGION, ORG_HIER_TYPE, ORG_HIER_VALUE, CURRENCY_CODE,
   PHYSICAL_WH, PRIMARY_VWH, CHANNEL_ID, STOCKHOLDING_IND, BREAK_PACK_IND, REDIST_WH_IND,
   DELIVERY_POLICY, RESTRICTED_IND, PROTECTED_IND, FORECAST_WH_IND, ROUNDING_SEQ, REPL_IND,
   REPL_WH_LINK, REPL_SRC_ORD, IB_IND, IB_WH_LINK, AUTO_IB_CLEAR, DUNS_NUMBER, DUNS_LOC, TSF_ENTITY_ID,
   FINISHER_IND, INBOUND_HANDLING_DAYS, ORG_UNIT_ID, VWH_TYPE,CUSTOMER_ORDER_LOC_IND,ORG_ENTITY_TYPE ) VALUES (
   50003, 'AU Direct', NULL, L_vat_region, NULL, NULL, 'AUD', 5, NULL, 3, 'Y', 'Y', 'N', 'NDD'
, 'N', 'N', 'Y', NULL, 'N', NULL, NULL, 'N', NULL, 'N', NULL, NULL, 1000, 'N', 0, NULL, 'CS_RG','Y','R');

   UPDATE WH
      SET PRIMARY_VWH = 10002
    WHERE PHYSICAL_WH = 2;

   UPDATE WH
      SET PRIMARY_VWH = 50001
    WHERE PHYSICAL_WH = 5;

   UPDATE WH
      SET TSF_ENTITY_ID = 1000,
            ORG_UNIT_ID = 1111111111;

---
   INSERT INTO ADDR (ADDR_KEY,
                     MODULE,
                     KEY_VALUE_1,
                     KEY_VALUE_2,
                     SEQ_NO,
                     ADDR_TYPE,
                     PRIMARY_ADDR_IND,
                     ADD_1,
                     ADD_2,
                     ADD_3,
                     CITY,
                     STATE,
                     COUNTRY_ID,
                     POST,
                     CONTACT_NAME,
                     CONTACT_PHONE,
                     CONTACT_TELEX,
                     CONTACT_FAX,
                     CONTACT_EMAIL,
                     ORACLE_VENDOR_SITE_ID,
                     EDI_ADDR_CHG,
                     COUNTY,
                     PUBLISH_IND )
                    (SELECT addr_sequence.NEXTVAL,
                            'WH',
                            wh,
                            NULL,
                             1,
                            '01',
                            'Y',
                            '123 Street',
                            'Anytown',
                            NULL,
                            'Anycity',
                            'MN',
                            'US',
                            '50250',
                            'Sue Glass',
                            '3122222473',
                            '3122222525',
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            'N'
                       FROM wh
                      WHERE physical_wh = wh);

    INSERT INTO ADDR (ADDR_KEY,
                      MODULE,
                      KEY_VALUE_1,
                      KEY_VALUE_2,
                      SEQ_NO,
                      ADDR_TYPE,
                      PRIMARY_ADDR_IND,
                      ADD_1,
                      ADD_2,
                      ADD_3,
                      CITY,
                      STATE,
                      COUNTRY_ID,
                      POST,
                      CONTACT_NAME,
                      CONTACT_PHONE,
                      CONTACT_TELEX,
                      CONTACT_FAX,
                      CONTACT_EMAIL,
                      ORACLE_VENDOR_SITE_ID,
                      EDI_ADDR_CHG,
                      COUNTY,
                      PUBLISH_IND )
                     (SELECT addr_sequence.NEXTVAL,
                             'WH',
                             wh,
                             NULL,
                              1,
                             '02',
                             'Y',
                             '123 Street',
                             'Anytown',
                             NULL,
                             'Anycity',
                             'MN',
                             'US',
                             '50250',
                             'Sue Glass',
                             '3122222473',
                             '3122222525',
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             'N'
                        FROM wh
                       WHERE physical_wh = wh);
   ---
   L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
   INSERT INTO STOCK_LEDGER_INSERTS (TYPE_CODE,
                                     DEPT,
                                     CLASS,
                                     SUBCLASS,
                                     LOCATION,
                                     RMS_ASYNC_ID)
                                    (SELECT 'W',
                                            NULL,
                                            NULL,
                                            NULL,
                                            wh,
                                            L_rms_async_id
                                       FROM wh
                                      WHERE physical_wh != wh);

    IF SET_OF_BOOKS_SQL.REFRESH_MV_LOC_SOB(O_error_message) = FALSE THEN
      RETURN false;
    END IF;
    IF L10N_SQL.REFRESH_MV_L10N_ENTITY(O_error_message) = FALSE THEN
      RETURN false;
    END IF;
    IF ADDRESS_SQL.REFRESH_MV_LOC_PRIM_ADDR(O_error_message) = FALSE THEN
      RETURN false;
    END IF;

     IF CORESVC_STKLEDGR_INSERTS_SQL.PROCESS_STKLEDGR_INSERTS(O_error_message,L_rms_async_id)=FALSE THEN
       RETURN false;
     END IF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.WHS',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END WHS;
-------------------------------------------------------------------------------------------
FUNCTION STORES(O_error_message      IN OUT   VARCHAR2,
                I_default_tax_type   IN       VARCHAR2)
RETURN BOOLEAN IS

   L_channel1                  CHANNELS.CHANNEL_ID%TYPE   := NULL;
   L_channel2                  CHANNELS.CHANNEL_ID%TYPE   := NULL;
   L_channel3                  CHANNELS.CHANNEL_ID%TYPE   := NULL;
   L_channel4                  CHANNELS.CHANNEL_ID%TYPE   := NULL;
   L_vat_region                VAT_REGION.VAT_REGION%TYPE := NULL;
   L_default_wh1               WH.WH%TYPE;
   L_default_wh2               WH.WH%TYPE;
   ---
   PROGRAM_ERROR EXCEPTION;

   L_rms_async_id STOCK_LEDGER_INSERTS.RMS_ASYNC_ID%TYPE;

BEGIN

   L_channel1 := 100;
   L_channel2 := 110;
   L_channel3 := 120;
   L_channel4 := 130;
   L_default_wh1 := 1111111112;
   L_default_wh2 := 2222222223;
   ---
   if I_default_tax_type ='SVAT' then
      L_vat_region := 1000;
   end if;
   ---
---Stores

   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS,
                       STORE_MGR_NAME, STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE,
                       REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL, TOTAL_SQUARE_FT,
                       SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND,
                       STOCKHOLDING_IND, CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT,
                       TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS, START_ORDER_DAYS,
                       CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND,
                       ORIG_CURRENCY_CODE, DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID,
                       STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
              VALUES ( 1111, 'Charlotte *', 'Charlotte', 'CHA', 'A', 'Don Wright',
                       TO_Date('01/01/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL,
                       NULL, NULL, NULL, '704-555-2121', NULL, 64400, 59500, NULL, L_vat_region,
                       NULL, 'Y', 1, 10, NULL, 111, 1000, 10001, NULL, 60, 'USD', 1, 'S', 'Y',
                       'USD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N');
   ---
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS,
                       STORE_MGR_NAME, STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE,
                       REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL, TOTAL_SQUARE_FT,
                       SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND,
                       STOCKHOLDING_IND, CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT,
                       TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS, START_ORDER_DAYS,
                       CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND,
                       ORIG_CURRENCY_CODE, DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID,
                       STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
              VALUES ( 3211, 'Toronto', 'Toronto', 'TOR', 'A', 'Rene Vistoli',
                       TO_Date( '08/01/2002 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                       NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region,
                       NULL, 'Y', 1, 10, NULL, 321, 1001, 10001, NULL, 90, 'CAD', 1, 'S', 'Y',
                       'CAD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   ---
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS,
                       STORE_MGR_NAME, STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE,
                       REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL, TOTAL_SQUARE_FT,
                       SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND,
                       STOCKHOLDING_IND, CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT,
                       TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS, START_ORDER_DAYS,
                       CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND,
                       ORIG_CURRENCY_CODE, DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID,
                       STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
              VALUES ( 3212, 'Ottawa', 'Ottawa', 'OTT', 'A', 'Judi Parish',
                       TO_Date( '03/05/1998 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                       NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region,
                       NULL, 'Y', 1, 10, NULL, 321, 1001, 10001, NULL, 90, 'CAD', 1, 'S', 'Y',
                       'CAD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   ---
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS,
                       STORE_MGR_NAME, STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE,
                       REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL, TOTAL_SQUARE_FT,
                       SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND,
                       STOCKHOLDING_IND, CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT,
                       TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS, START_ORDER_DAYS,
                       CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND,
                       ORIG_CURRENCY_CODE, DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID,
                       STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
              VALUES ( 3311, 'Vancouver', 'Vancouver', 'VNC', 'B', 'Irma Montane',
                       TO_Date( '06/01/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
                       NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region,
                       NULL, 'Y', 1, NULL, NULL, 331, NULL, 10001, NULL, 90,
                       'CAD', 1, 'S', 'Y', 'CAD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   ---

   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID,STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1131, 'Jacksonville', 'Jacksonvil', 'JAC', 'B', 'Tom Spangler',  TO_Date( '05/15/2001 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '904-277-3388', NULL, 6000, 5000, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 113, 1000, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID,STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 5111, 'Sydney*', 'Sydney5111', 'SYD', 'A', 'Rod Laverson',  TO_Date( '04/01/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, 'Y', 'Y', 1, 10, NULL
   , 511, 1002, 50001, NULL, 60, 'AUD', 1, 'S', 'Y', 'AUD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 5141, 'Melbourne', 'Melbourne', 'MEL', 'C', 'Steve Dirkson',  TO_Date( '08/01/2001 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, 'Y', 'Y', 1, 10, NULL
   , 570, 1002, 50001, NULL, 60, 'AUD', 1, 'S', 'Y', 'AUD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 6000, 'Oceania Outlet', 'AUS Outlet', 'AOT', 'A', 'Nigel Jones',  TO_Date( '01/01/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, 'Y', 'Y', 1, 23, NULL
   , 511, 1004, NULL, NULL, 90, 'AUD', 1, 'S', 'Y', 'AUD', NULL, NULL, NULL, 2000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1141, 'Nashville', 'Nashvillet', 'NAS', 'A', 'Colin Berger',  TO_Date( '03/05/2001 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '931-477-5800', NULL, 1500, 1200, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 114, 1001, 10001, NULL, 90, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N');
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1151, 'Dallas', 'DallasTex', 'DAL', 'C', 'Karen Johnson',  TO_Date( '05/31/1999 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '903-225-3377', NULL, 10000, 8500, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 115, 1002, NULL, NULL, 90, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N');
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1211, 'Boston', 'BostonMass', 'BOS', 'B', 'Mary Connelly',  TO_Date( '08/01/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '617-688-3388', NULL, 5000, 4000, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 121, 1002, 10001, NULL, 90, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1221, 'New York', 'New YorkNY', 'NYC', 'A', 'Jerry Kessler',  TO_Date( '11/01/2002 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '212-277-3399', NULL, 4000, 3500, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 122, 1002, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N');
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1231, 'Philadelphia*', 'Philadelph', 'PHI', 'B', 'Patrick Sullivan',  TO_Date( '07/15/1996 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '215-778-3300', NULL, 2000, 1500, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 123, 1002, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1241, 'Cleveland', 'ClevelandO', 'CLE', 'C', 'Rhonda Collins',  TO_Date( '05/14/2001 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '440-877-2266', NULL, 2000, 1800, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 124, 1002, 10001, NULL, 90, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1311, 'Chicago*', 'ChicagoIll', 'CHI', 'C', 'Joe Fazio',  TO_Date( '10/31/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '773-588-4400', NULL, 5000, 4500, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 131, 1003, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1321, 'Indianapolis', 'Indianapol', 'IND', 'D', 'Mary Jackson',  TO_Date( '04/02/1999 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '317-388-6699', NULL, 10000, 9000, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 132, 1003, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1331, 'Minneapolis', 'Minneapoli', 'MSP', 'A', 'Jennifer Smith',  TO_Date( '09/01/2002 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '612-665-3388', NULL, 7500, 6500, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 133, 1003, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1341, 'St. Louis', 'StlouisMo', 'STL', 'B', 'Greg Valient',  TO_Date( '03/15/1998 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '314-866-3388', NULL, 4500, 4250, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 134, 1003, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1411, 'Seattle*', 'SeattleWas', 'SEA', 'A', 'Jack Wellsey',  TO_Date( '01/01/2002 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '206-988-3388', NULL, NULL, 6500, 6000, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 141, 1004, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1421, 'Portland', 'PortlandOR', 'POR', 'B', 'David Seals',  TO_Date( '05/01/2001 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '503-566-4499', NULL, 10000, 9000, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 141, 1004, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1511, 'Phoenix', 'PhoenixAr', 'PHO', 'C', 'Sally Boyer',  TO_Date( '06/01/1998 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '602-755-3388', NULL, 7500, 7000, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 151, 1004, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1521, 'Albuquerque', 'Albuquerqu', 'ALB', 'D', 'Frank Perez',  TO_Date( '01/01/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '505-388-5577', NULL, 4500, 4000, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 152, 1004, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 1531, 'Los Angeles*', 'LosAngeles', 'LOS', 'B', 'Paul Petros',  TO_Date( '04/01/1998 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, '818-388-6600', NULL, 3500, 3250, NULL, L_vat_region, NULL, 'Y'
   , 1, 10, NULL, 153, 1004, 10001, NULL, 60, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL
   , 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 3000, 'Direct', 'Direct', 'DRT', 'A', 'Nate Howard',  TO_Date( '05/01/2003 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, NULL, 'N', 3, NULL, NULL
   , 170, NULL, 10003, NULL, 10, 'USD', 1, 'S', 'Y', 'USD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 5122, 'Auckland', 'Auckland', 'ACL', 'C', 'Amy Peters',  TO_Date( '01/20/1998 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, 'Y', 'Y', 1, 10, NULL
   , 512, 1003, 50001, NULL, 60, 'NZD', 1, 'S', 'Y', 'NZD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 5151, 'Perth', 'Perth', 'PTH', 'C', 'Kevin Beard',  TO_Date( '01/01/2002 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, 'Y', 'Y', 1, 10, NULL
   , 571, 1003, 50001, NULL, 90, 'AUD', 1, 'S', 'Y', 'AUD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 5161, 'Brisbane', 'Brisbane', 'BBN', 'C', 'John Amrstrong',  TO_Date( '01/31/1999 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, 'Y', 'Y', 1, 10, NULL
   , 572, 1003, 50001, NULL, 60, 'AUD', 1, 'S', 'Y', 'AUD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 5181, 'Christchurch', 'Christchur', 'CCH', 'C', 'Susan Jones',  TO_Date( '08/01/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, 'Y', 'Y', 1, 10, NULL
   , 581, 1003, 50001, NULL, 90, 'NZD', 1, 'S', 'Y', 'NZD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 3111, 'Montreal*', 'Montreal', 'MON', 'B', 'Guillaum LeClerc',  TO_Date( '03/01/2001 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, NULL, 'Y', 1, 10, NULL
   , 311, 1001, 10001, NULL, 60, 'CAD', 1, 'S', 'Y', 'CAD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 3112, 'Quebec', 'Quebec', 'QUE', 'B', 'Lucile Thomson',  TO_Date( '02/01/2002 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, NULL, 'Y', 1, 10, NULL
   , 311, 1001, 10001, NULL, 90, 'CAD', 1, 'S', 'Y', 'CAD', NULL, NULL, NULL, 1000, 'C', NULL,  'America/New_York', 'N' );
   INSERT INTO STORE ( STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_CLASS, STORE_MGR_NAME,
   STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
   TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
   CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
   START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
   DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
   VALUES ( 6050, 'Oceania Direct', 'Ocean Dir', 'ODR', 'A', 'Frank White',  TO_Date( '01/31/2000 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, L_vat_region, NULL, 'N', 3, NULL, NULL
   , 511, NULL, NULL, NULL, 60, 'AUD', 1, 'S', 'Y', 'AUD', NULL, NULL, NULL, 1000, 'C', NULL, 'America/New_York', 'N' );


   UPDATE STORE
      SET TSF_ENTITY_ID = 1000,
            ORG_UNIT_ID = 1111111111;


      INSERT INTO STORE
      (STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_NAME_SECONDARY, STORE_CLASS,
       STORE_MGR_NAME, STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE,
       FAX_NUMBER, PHONE_NUMBER, EMAIL, TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION,
       VAT_INCLUDE_IND, STOCKHOLDING_IND, CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE,
       DEFAULT_WH, STOP_ORDER_DAYS, START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED,
       INTEGRATED_POS_IND, ORIG_CURRENCY_CODE, DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID,
       ORG_UNIT_ID, AUTO_RCV, REMERCH_IND, STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND)
       VALUES
      (9625518, 'PCB Store Test', '9625518', '251', 'PCB Secondary Name', 'X', 'Mr. Manager',
       TO_DATE('02/01/2002 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       TO_DATE('01/01/2010 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       TO_DATE('01/01/2002 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       TO_DATE('01/01/2003 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       '398938499', '841989883', '1@email.com', 1000, 900, 500, L_vat_region, NULL,
       'N', 1, 10, 'Standalone Mall', 153, NULL, 10001, 1, 2, 'USD',
       1, 'S', 'Y', 'USD', '487487', '4587', NULL, NULL, NULL,
       'D', 'N', 'F', 1001, 'America/New_York', NULL);

      INSERT INTO STORE
      (STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_NAME_SECONDARY, STORE_CLASS, STORE_MGR_NAME,
      STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
      TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
      CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
      START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
      DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, ORG_UNIT_ID, AUTO_RCV, REMERCH_IND,
      STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
       VALUES
      (9625516, 'PCB Test Store 1 - WH', '9625516', '545', 'Test',
       'X', 'Ms. Manager', TO_DATE('01/01/2003 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('01/01/2010 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('01/01/2001 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       TO_DATE('01/01/2002 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), '458121212', '787548784', '1@test.com', 1000,
       520, 500, L_vat_region, NULL, 'N',
       1, 10, NULL, 153, NULL,
       10001, 1, 2, 'USD', 1,
       'R', 'N', 'USD', '21548754', '4578',
       NULL, NULL, NULL, 'D', 'N',
       'F', 1001, 'America/New_York', NULL);

      INSERT INTO STORE
      (STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_NAME_SECONDARY, STORE_CLASS, STORE_MGR_NAME,
      STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
      TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
      CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
      START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
      DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, ORG_UNIT_ID, AUTO_RCV, REMERCH_IND,
      STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND )
      VALUES
      (9625517, 'PCB Test Store 2 - WH', '9625517', '545', 'Test',
       'X', 'Ms. Manager', TO_DATE('01/01/2003 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('01/01/2010 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('01/01/2001 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       TO_DATE('01/01/2002 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), '458121212', '787548784', '1@test.com', 1000,
       520, 500, L_vat_region, NULL, 'N',
       1, 10, NULL, 153, NULL,
       10001, 1, 2, 'USD', 1,
       'S', 'N', 'USD', '21548754', '4578',
       NULL, NULL, NULL, 'D', 'N',
       'F', 1001, 'America/New_York', NULL );

      INSERT INTO STORE
      (STORE, STORE_NAME, STORE_NAME10, STORE_NAME3, STORE_NAME_SECONDARY, STORE_CLASS, STORE_MGR_NAME,
      STORE_OPEN_DATE, STORE_CLOSE_DATE, ACQUIRED_DATE, REMODEL_DATE, FAX_NUMBER, PHONE_NUMBER, EMAIL,
      TOTAL_SQUARE_FT, SELLING_SQUARE_FT, LINEAR_DISTANCE, VAT_REGION, VAT_INCLUDE_IND, STOCKHOLDING_IND,
      CHANNEL_ID, STORE_FORMAT, MALL_NAME, DISTRICT, TRANSFER_ZONE, DEFAULT_WH, STOP_ORDER_DAYS,
      START_ORDER_DAYS, CURRENCY_CODE, LANG, TRAN_NO_GENERATED, INTEGRATED_POS_IND, ORIG_CURRENCY_CODE,
      DUNS_NUMBER, DUNS_LOC, SISTER_STORE, TSF_ENTITY_ID, ORG_UNIT_ID, AUTO_RCV, REMERCH_IND,
      STORE_TYPE, WF_CUSTOMER_ID, TIMEZONE_NAME, CUSTOMER_ORDER_LOC_IND)
      VALUES
      (9625519, 'PCB Test Store 2', 'abdekjkjij', '121', 'Secondary Name PCB',
       'X', 'Mr. Manager Also', TO_DATE('01/01/2003 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('01/01/2010 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('01/01/2001 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       TO_DATE('01/02/2002 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), '454/87545', '48785484', '2@email.com', 1000,
       900, 500, L_vat_region, NULL, 'N',
       1, 10, 'Supah Mall', 153, NULL,
       10001, 1, 2, 'USD', 1,
       'R', 'N', 'USD', '100548789', '4578',
       NULL, NULL, NULL, 'D', 'N',
      'F', 1002, 'America/New_York', NULL );

   INSERT INTO ADDR (ADDR_KEY,
                     MODULE,
                     KEY_VALUE_1,
                     KEY_VALUE_2,
                     SEQ_NO,
                     ADDR_TYPE,
                     PRIMARY_ADDR_IND,
                     ADD_1,
                     ADD_2,
                     ADD_3,
                     CITY,
                     STATE,
                     COUNTRY_ID,
                     POST,
                     CONTACT_NAME,
                     CONTACT_PHONE,
                     CONTACT_TELEX,
                     CONTACT_FAX,
                     CONTACT_EMAIL,
                     ORACLE_VENDOR_SITE_ID,
                     EDI_ADDR_CHG,
                     COUNTY,
                     PUBLISH_IND )
                    (SELECT addr_sequence.NEXTVAL,
                            'ST',
                            store,
                            NULL,
                             1,
                            '01',
                            'Y',
                            '123 Street',
                            'Anytown',
                            NULL,
                            'Anycity',
                            'MN',
                            'US',
                            '50250',
                            'Sue Glass',
                            '3122222473',
                            '3122222525',
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            'N'
                       FROM store);

   INSERT INTO ADDR (ADDR_KEY,
                     MODULE,
                     KEY_VALUE_1,
                     KEY_VALUE_2,
                     SEQ_NO,
                     ADDR_TYPE,
                     PRIMARY_ADDR_IND,
                     ADD_1,
                     ADD_2,
                     ADD_3,
                     CITY,
                     STATE,
                     COUNTRY_ID,
                     POST,
                     CONTACT_NAME,
                     CONTACT_PHONE,
                     CONTACT_TELEX,
                     CONTACT_FAX,
                     CONTACT_EMAIL,
                     ORACLE_VENDOR_SITE_ID,
                     EDI_ADDR_CHG,
                     COUNTY,
                     PUBLISH_IND )
                    (SELECT addr_sequence.NEXTVAL,
                            'ST',
                            store,
                            NULL,
                             1,
                            '02',
                            'Y',
                            '123 Street',
                            'Anytown',
                            NULL,
                            'Anycity',
                            'MN',
                            'US',
                            '50250',
                            'Sue Glass',
                            '3122222473',
                            '3122222525',
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            'N'
                       FROM store);

   INSERT INTO ADDR (ADDR_KEY,
                     MODULE,
                     KEY_VALUE_1,
                     KEY_VALUE_2,
                     SEQ_NO,
                     ADDR_TYPE,
                     PRIMARY_ADDR_IND,
                     ADD_1,
                     ADD_2,
                     ADD_3,
                     CITY,
                     STATE,
                     COUNTRY_ID,
                     POST,
                     CONTACT_NAME,
                     CONTACT_PHONE,
                     CONTACT_TELEX,
                     CONTACT_FAX,
                     CONTACT_EMAIL,
                     ORACLE_VENDOR_SITE_ID,
                     EDI_ADDR_CHG,
                     COUNTY,
                     PUBLISH_IND )
                    (SELECT addr_sequence.NEXTVAL,
                            'WFST',
                            store,
                            NULL,
                             1,
                            '07',
                            'Y',
                            '123 Street',
                            'Anytown',
                            NULL,
                            'Anycity',
                            'MN',
                            'US',
                            '50250',
                            'Sue Glass',
                            '3122222473',
                            '3122222525',
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            'N'
                       FROM store
                      WHERE store_type = 'F');

   INSERT INTO STORE_HIERARCHY (company,
                                chain,
                                area,
                                region,
                                district,
                                store)
                                (SELECT company,
                                ar.chain,
                                re.area,
                                di.region,
                                st.district,
                                st.store
                                FROM comphead,
                                area ar,
                                region re,
                                district di,
                                store st
                                WHERE st.district = di.district
                                AND di.region = re.region
                                AND re.area = ar.area);

   ---
   -- Create stock ledger inserts records.
   ---
   L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
   INSERT INTO STOCK_LEDGER_INSERTS (type_code,
                                     dept,
                                     class,
                                     subclass,
                                     location,
                                     rms_async_id)
                              SELECT 'S',
                                     NULL,
                                     NULL,
                                     NULL,
                                     store,
                                     L_rms_async_id
                                FROM store;

   IF SET_OF_BOOKS_SQL.REFRESH_MV_LOC_SOB(O_error_message) = FALSE THEN
      RETURN FALSE;
   END IF;
   IF L10N_SQL.REFRESH_MV_L10N_ENTITY(O_error_message) = FALSE THEN
     RETURN FALSE;
   END IF;
   IF ADDRESS_SQL.REFRESH_MV_LOC_PRIM_ADDR(O_error_message) = FALSE THEN
      RETURN FALSE;
   END IF;

  IF CORESVC_STKLEDGR_INSERTS_SQL.PROCESS_STKLEDGR_INSERTS(O_error_message,L_rms_async_id)=FALSE THEN
    RETURN false;
  END IF;

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.STORES',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END STORES;
--------------------------------------------------------------------------------------------
FUNCTION SUPS_ADDR(O_error_message         IN OUT   VARCHAR2,
                   I_default_tax_type      IN       VARCHAR2)
RETURN BOOLEAN IS

   L_vat_region          NUMBER   := NULL;

BEGIN

   ---
   if I_default_tax_type ='SVAT' then
      L_vat_region := 1000;
   end if;
   ---
   --********************** SUPPLIER = 1212120000 ****
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2700, 'Tyson Fresh Meats', 'Larrie Rome', '970-222-7395', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '02', '01', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL, 3
   , NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2800, 'Minneapolis Beef Council', 'Kerry Rose', '612-484-3764', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '01', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL
   , NULL, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1800, 'Refurbishment Supplier', 'Refurb Supplier', 'n/a', 'n/a', NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '07', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '80', NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT', NULL, NULL
   , NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3020, 'Battery Supplier', 'Mark Stevenson', '859-231-1234', NULL, NULL, 'A', 'Y', 2
   , 5, 'N', NULL, NULL, 'USD', 1, '108', '04', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, 'mstevenson@battery.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 8, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2200, 'Philip Morris', 'Jacob Crest', '804 745 6241', '804 745 1212', NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '03', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, 'JCrest@phillipmorris.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 2, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1300, 'Fine Jewelry Supplier', 'Dale McComb', '212-530-2000', '212-530-2001', NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '01', '01', 'Y', 'Y', NULL, NULL
   , NULL, 'Y', 'N', 'Y', 'N', NULL, 'Y', 'N', 'N', NULL, 0, 0, 'N', '80', 'OA', NULL
   , NULL, 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL
   , 'NEXT', NULL, NULL, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2300, 'Coca Cola', 'John O''Neill', '404-233-9999', NULL, NULL, 'A', 'N', NULL, NULL
   , 'N', NULL, NULL, 'USD', 1, '01', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N', 'N'
   , NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL, NULL, NULL
   , NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2400, 'Coca Cola - Charlotte', 'John O''Reilly', '304-445-4848', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '03', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL
   , NULL, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2500, 'Coca Cola - Chicago', 'Mike Daley', '773-574-7894', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '02', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL, NULL
   , NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2600, 'Coca Cola - Los Angeles', 'Diego Santorez', '940-393-5649', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '03', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, NULL, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1000, 'Fashion Import Supplier', 'Christy Grant', '212-909-8000', '212-909-8001', NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '04', 'Y', 'Y', NULL, NULL
   , NULL, 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '11', 'LC', NULL
   , 'cgrant@fahsion.com', 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL
   , NULL, NULL, 'NDD', NULL, 90, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1200, 'Fashion Importer (Euro)', 'Antonio Carusso', '+39 98 78 65 98', '+39 98 78 65 99'
   , NULL, 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'EUR', 1, '01', '01', 'Y', 'Y', NULL
   , NULL, NULL, 'Y', 'Y', 'Y', 'N', NULL, 'N', 'Y', 'N', NULL, 0, 0, 'Y', '11', 'WT'
   , NULL, 'acarusso@fie.com', 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N'
   , NULL, NULL, NULL, 'NDD', NULL, 75, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1400, 'Fashion Domestic', 'Laura Johnson', '617-897-0900', '617-897-0911', NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '07', '03', 'Y', 'Y', NULL, NULL, NULL
   , 'Y', 'Y', 'Y', 'Y', NULL, 'Y', 'N', 'N', NULL, 10, 0, 'N', '30', 'OA', NULL, 'ljohnson@fasdom.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 20, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2000, 'Dannon', 'Alan McKenzie', '914 366 9700', '914 366 9801', NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '05', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT', NULL, 7
   , NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3000, 'Electronics Supplier 1', 'Bill Johnson', '818 990 0000', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '1', '03', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, NULL, 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD', NULL
   , 7, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3010, 'Electronics Supplier 2', 'JoAnne Halverson', '206 878 4500', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '108', '03', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, NULL
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 9, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1500, 'Lancome', 'Marie Leblanc', '212-790-2323', '212-790-5001', NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '07', '03', 'Y', 'Y', NULL, NULL, NULL, 'Y', 'Y'
   , 'Y', 'Y', NULL, 'Y', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, 'mleblanc@lancome.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT'
   , NULL, 3, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2100, 'Del Monte Foods', 'Dave Sharpton', '(756) 754-7527', '(756) 754-3230', NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '03', 'Y', 'Y', NULL, NULL
   , 2, 'Y', 'N', 'Y', 'Y', 'D', 'N', 'N', 'N', NULL, 2, 0, 'N', '30', NULL, NULL, 'dave.sharpton@delmonte.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT'
   , NULL, 2, '124057394', '1234', 'Y', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   8000, 'Concession Supplier', 'Jean Jones', '970-376-0000', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '07', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD', NULL, NULL
   , NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   6000, 'Local Grocery Supplier #1', 'Local Supplier', '999-999-9999', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '01', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, NULL
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT'
   , NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   6100, 'Local Grocery Supplier #2', 'Local Supplier #2', '999-999-9999', NULL, NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '01', 'Y', 'Y', NULL, NULL
   , NULL, 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL
   , NULL, 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL
   , 'NEXT', NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   6200, 'Local Grocery Supplier #3', 'Local Supplier #3', '999-999-9999', NULL, NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '01', 'Y', 'Y', NULL, NULL
   , NULL, 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL
   , NULL, 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL
   , 'NEXT', NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2900, 'Local Supplier #1', 'Jack Strain', '609-336-9365', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '02', '01', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NEXT', NULL, NULL
   , NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2910, 'Local Supplier #2', 'Mark Ott', '609-373-0284', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '02', '01', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NEXT', NULL, NULL
   , NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   7777, 'General Detergents Supplier', 'George Beard', '401-349-9432', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '01', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '31', NULL, NULL, NULL
   , 'N', 'N', 'N', 'Y', 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, NULL, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2950, 'Paper Products Supplier', 'Fred Smith', '555 555 1212', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '30', '02', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, NULL, 'N'
   , 'N', 'N', 'L', 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NEXT', NULL
   , 2, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3100, 'Candle Importer', 'Thomsa Kraus', '719-387-2345', NULL, NULL, 'A', 'Y', 3, 3
   , 'N', NULL, NULL, 'USD', 1, '02', '04', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '11', 'LC', NULL, 'TKraus@candle.com'
   , 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 90, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3030, 'General Book Supplier', 'Rebecca Diener', '234-112-7654', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '04', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, 'rdiener@book.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 4, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3040, 'Procter and Gamble', 'Frank Jones', '513-222-2222', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '2', '04', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, 'fjones@pg.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 3, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3050, 'Furniture Import Supplier', 'Paul Johnson', '312-770-7700', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '04', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '11', 'LC', NULL, 'pjohnson@furniture.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 90, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3060, 'Hardware Supplier', 'Jack Murphy', '612 459 9800', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '1', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '31', 'OA', NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT', NULL, 7
, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');

   ---
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6000', NULL, 1, '05', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83632', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6100', NULL, 1, '01', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83273', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6100', NULL, 1, '03', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83273', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6100', NULL, 1, '04', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83273', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6100', NULL, 1, '05', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83273', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6200', NULL, 1, '01', 'Y', 'Local Supplier #3', NULL, NULL, 'Portland'
   , 'OR', 'US', '83659', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6200', NULL, 1, '03', 'Y', 'Local Supplier #3', NULL, NULL, 'Portland'
   , 'OR', 'US', '83659', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6200', NULL, 1, '04', 'Y', 'Local Supplier #3', NULL, NULL, 'Portland'
   , 'OR', 'US', '83659', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6200', NULL, 1, '05', 'Y', 'Local Supplier #3', NULL, NULL, 'Portland'
   , 'OR', 'US', '83659', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3060', NULL, 1, '01', 'Y', '1400 Excelsior Blvd', NULL, NULL, 'Minneapolis'
   , 'MN', 'US', '55416', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3060', NULL, 1, '03', 'Y', '1400 Excelsior Blvd', NULL, NULL, 'Minneapolis'
   , 'MN', 'US', '55416', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3060', NULL, 1, '04', 'Y', '1400 Excelsior Blvd', NULL, NULL, 'Minneapolis'
   , 'MN', 'US', '55416', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3060', NULL, 1, '05', 'Y', '1400 Excelsior Blvd', NULL, NULL, 'Minneapolis'
   , 'MN', 'US', '55416', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2900', NULL, 1, '01', 'Y', 'Jack Strain', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '09475', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2900', NULL, 1, '03', 'Y', 'Jack Strain', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '09475', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2900', NULL, 1, '04', 'Y', 'Jack Strain', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '09475', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2900', NULL, 1, '05', 'Y', 'Jack Strain', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '09475', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2910', NULL, 1, '01', 'Y', 'Mark Ott', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '08363', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2910', NULL, 1, '03', 'Y', 'Mark Ott', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '08363', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2910', NULL, 1, '04', 'Y', 'Mark Ott', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '08363', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2910', NULL, 1, '05', 'Y', 'Mark Ott', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '08363', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '7777', NULL, 1, '01', 'Y', '401 Highland pkwy', NULL, NULL, 'Detroit'
   , 'MI', 'US', '93405', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '7777', NULL, 1, '03', 'Y', '401 Highland pkwy', NULL, NULL, 'Detroit'
   , 'MI', 'US', '93405', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '7777', NULL, 1, '04', 'Y', '401 Highland pkwy', NULL, NULL, 'Detroit'
   , 'MI', 'US', '93405', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '7777', NULL, 1, '05', 'Y', '401 Highland pkwy', NULL, NULL, 'Detroit'
   , 'MI', 'US', '93405', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2950', NULL, 1, '01', 'Y', '121 S First St', NULL, NULL, 'New York'
   , 'NY', 'US', '21078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2950', NULL, 1, '03', 'Y', '121 S First St', NULL, NULL, 'New York'
   , 'NY', 'US', '21078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2950', NULL, 1, '04', 'Y', '121 S First St', NULL, NULL, 'New York'
   , 'NY', 'US', '21078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2950', NULL, 1, '05', 'Y', '121 S First St', NULL, NULL, 'New York'
   , 'NY', 'US', '21078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2700', NULL, 1, '01', 'Y', '1233 Hwy 9', NULL, NULL, 'Dakota Downs'
   , 'SD', 'US', '46304', 'Larrie Rome', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2700', NULL, 1, '03', 'Y', '1233 Hwy 9', NULL, NULL, 'Dakota Downs'
   , 'SD', 'US', '46304', 'Larrie Rome', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2700', NULL, 1, '04', 'Y', '1233 Hwy 9', NULL, NULL, 'Dakota Downs'
   , 'SD', 'US', '46304', 'Larrie Rome', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2700', NULL, 1, '05', 'Y', '1233 Hwy 9', NULL, NULL, 'Dakota Downs'
   , 'SD', 'US', '46304', 'Larrie Rome', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2800', NULL, 1, '01', 'Y', '2340 Eden Prairie Parkway', NULL, NULL
   , 'Eden Prairie', 'MN', 'US', '55440u', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2800', NULL, 1, '03', 'Y', '2340 Eden Prairie Parkway', NULL, NULL
   , 'Eden Prairie', 'MN', 'US', '55440u', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2800', NULL, 1, '04', 'Y', '2340 Eden Prairie Parkway', NULL, NULL
   , 'Eden Prairie', 'MN', 'US', '55440u', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2800', NULL, 1, '05', 'Y', '2340 Eden Prairie Parkway', NULL, NULL
   , 'Eden Prairie', 'MN', 'US', '55440u', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1800', NULL, 1, '01', 'Y', 'n/a', NULL, NULL, 'n/a', NULL, 'US', NULL
   , 'N/a', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1800', NULL, 1, '03', 'Y', 'n/a', NULL, NULL, 'n/a', NULL, 'US', NULL
   , 'N/a', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1800', NULL, 1, '04', 'Y', 'n/a', NULL, NULL, 'n/a', NULL, 'US', NULL
   , 'N/a', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1800', NULL, 1, '05', 'Y', 'n/a', NULL, NULL, 'n/a', NULL, 'US', NULL
   , 'N/a', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3020', NULL, 1, '01', 'Y', '23423 Commerce Drive', NULL, NULL, 'Louisville'
   , 'KY', 'US', '40202', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3020', NULL, 1, '03', 'Y', '23423 Commerce Drive', NULL, NULL, 'Louisville'
   , 'KY', 'US', '40202', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3020', NULL, 1, '04', 'Y', '23423 Commerce Drive', NULL, NULL, 'Louisville'
   , 'KY', 'US', '40202', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3020', NULL, 1, '05', 'Y', '23423 Commerce Drive', NULL, NULL, 'Louisville'
   , 'KY', 'US', '40202', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2200', NULL, 1, '01', 'Y', '4200 Deepwater Terminal Road', NULL, NULL
   , 'Richmond', 'VA', 'US', '23234', 'Jacob Crest', NULL, NULL, NULL, NULL, NULL, 'Y'
   , NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2200', NULL, 1, '03', 'Y', '4200 Deepwater Terminal Road', NULL, NULL
   , 'Richmond', 'VA', 'US', '23234', 'Jacob Crest', NULL, NULL, NULL, NULL, NULL, 'Y'
   , NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2200', NULL, 1, '04', 'Y', '4200 Deepwater Terminal Road', NULL, NULL
   , 'Richmond', 'VA', 'US', '23234', 'Jacob Crest', NULL, NULL, NULL, NULL, NULL, 'Y'
   , NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2200', NULL, 1, '05', 'Y', '4200 Deepwater Terminal Road', NULL, NULL
   , 'Richmond', 'VA', 'US', '23234', 'Jacob Crest', NULL, NULL, NULL, NULL, NULL, 'Y'
   , NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1300', NULL, 1, '01', 'Y', '5757 E. 46th Street', NULL, NULL, 'New York City'
   , 'NY', 'US', '10090', 'Dale McComb', '212-530-2000', NULL, '212-530-2001', NULL, NULL
   , 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1300', NULL, 1, '03', 'Y', '5757 E. 46th Street', NULL, NULL, 'New York City'
   , 'NY', 'US', '10090', 'Dale McComb', '212-530-2000', NULL, '212-530-2001', NULL, NULL
   , 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1300', NULL, 1, '04', 'Y', '5757 E. 46th Street', NULL, NULL, 'New York City'
   , 'NY', 'US', '10090', 'Dale McComb', '212-530-2000', NULL, '212-530-2001', NULL, NULL
   , 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1300', NULL, 1, '05', 'Y', '5757 E. 46th Street', NULL, NULL, 'New York City'
   , 'NY', 'US', '10090', 'Dale McComb', '212-530-2000', NULL, '212-530-2001', NULL, NULL
   , 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2300', NULL, 1, '01', 'Y', '1499 Peach Tree Plaza', NULL, NULL, 'Atlanta'
   , 'GA', 'US', '30232', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2300', NULL, 1, '03', 'Y', '1499 Peach Tree Plaza', NULL, NULL, 'Atlanta'
   , 'GA', 'US', '30232', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2300', NULL, 1, '04', 'Y', '1499 Peach Tree Plaza', NULL, NULL, 'Atlanta'
   , 'GA', 'US', '30232', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2300', NULL, 1, '05', 'Y', '1499 Peach Tree Plaza', NULL, NULL, 'Atlanta'
   , 'GA', 'US', '30232', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2400', NULL, 1, '01', 'Y', ' 293 Market St', NULL, NULL, 'Charlotte'
   , 'NC', 'US', '30365', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2400', NULL, 1, '03', 'Y', ' 293 Market St', NULL, NULL, 'Charlotte'
   , 'NC', 'US', '30365', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2400', NULL, 1, '04', 'Y', ' 293 Market St', NULL, NULL, 'Charlotte'
   , 'NC', 'US', '30365', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2400', NULL, 1, '05', 'Y', ' 293 Market St', NULL, NULL, 'Charlotte'
   , 'NC', 'US', '30365', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2500', NULL, 1, '01', 'Y', '2300 Lakeshore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '37078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2500', NULL, 1, '03', 'Y', '2300 Lakeshore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '37078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2500', NULL, 1, '04', 'Y', '2300 Lakeshore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '37078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2500', NULL, 1, '05', 'Y', '2300 Lakeshore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '37078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2600', NULL, 1, '01', 'Y', '230 Ocean Pacific Hwy', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '93642', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2600', NULL, 1, '03', 'Y', '230 Ocean Pacific Hwy', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '93642', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2600', NULL, 1, '04', 'Y', '230 Ocean Pacific Hwy', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '93642', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2600', NULL, 1, '05', 'Y', '230 Ocean Pacific Hwy', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '93642', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1000', NULL, 1, '01', 'Y', '212 E. 58th Street', NULL, NULL, 'New York city'
   , 'NY', 'US', '10010', 'Christy Grant', '212-909-8000', NULL, '212-909-8001', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1000', NULL, 1, '03', 'Y', '212 E. 58th Street', NULL, NULL, 'New York city'
   , 'NY', 'US', '10010', 'Christy Grant', '212-909-8000', NULL, '212-909-8001', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1000', NULL, 1, '04', 'Y', '212 E. 58th Street', NULL, NULL, 'New York city'
   , 'NY', 'US', '10010', 'Christy Grant', '212-909-8000', NULL, '212-909-8001', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1000', NULL, 1, '05', 'Y', '212 E. 58th Street', NULL, NULL, 'New York city'
   , 'NY', 'US', '10010', 'Christy Grant', '212-909-8000', NULL, '212-909-8001', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1200', NULL, 1, '01', 'Y', '3301, via del Duomo', NULL, NULL, 'Milan'
   , NULL, 'IT', '49000', 'Antonio Caruso', '+39 78 67 54 22', NULL, '+39 78 67 54 21'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1200', NULL, 1, '03', 'Y', '3301, via del Duomo', NULL, NULL, 'Milan'
   , NULL, 'IT', '49000', 'Antonio Caruso', '+39 78 67 54 22', NULL, '+39 78 67 54 21'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1200', NULL, 1, '04', 'Y', '3301, via del Duomo', NULL, NULL, 'Milan'
   , NULL, 'IT', '49000', 'Antonio Caruso', '+39 78 67 54 22', NULL, '+39 78 67 54 21'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1200', NULL, 1, '05', 'Y', '3301, via del Duomo', NULL, NULL, 'Milan'
   , NULL, 'IT', '49000', 'Antonio Caruso', '+39 78 67 54 22', NULL, '+39 78 67 54 21'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1400', NULL, 1, '01', 'Y', '200 Ryan Way', 'Suite 100', NULL, 'Somerville'
   , 'MA', 'US', '05490', 'Laura Johnson', '617-897-0900', NULL, '617-897-0911', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1400', NULL, 1, '03', 'Y', '200 Ryan Way', 'Suite 100', NULL, 'Somerville'
   , 'MA', 'US', '05490', 'Laura Johnson', '617-897-0900', NULL, '617-897-0911', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1400', NULL, 1, '04', 'Y', '200 Ryan Way', 'Suite 100', NULL, 'Somerville'
   , 'MA', 'US', '05490', 'Laura Johnson', '617-897-0900', NULL, '617-897-0911', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1400', NULL, 1, '05', 'Y', '200 Ryan Way', 'Suite 100', NULL, 'Somerville'
   , 'MA', 'US', '05490', 'Laura Johnson', '617-897-0900', NULL, '617-897-0911', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2000', NULL, 1, '01', 'Y', '120 White Plain Road', NULL, NULL, 'Tarrytown'
   , 'NY', 'US', '10591', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2000', NULL, 1, '03', 'Y', '120 White Plain Road', NULL, NULL, 'Tarrytown'
   , 'NY', 'US', '10591', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2000', NULL, 1, '04', 'Y', '120 White Plain Road', NULL, NULL, 'Tarrytown'
   , 'NY', 'US', '10591', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2000', NULL, 1, '05', 'Y', '120 White Plain Road', NULL, NULL, 'Tarrytown'
   , 'NY', 'US', '10591', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3000', NULL, 1, '01', 'Y', '1450 Bernard Blvd', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '91401', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3000', NULL, 1, '03', 'Y', '1450 Bernard Blvd', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '91401', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3000', NULL, 1, '04', 'Y', '1450 Bernard Blvd', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '91401', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3000', NULL, 1, '05', 'Y', '1450 Bernard Blvd', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '91401', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3010', NULL, 1, '01', 'Y', '500 Waterfront Way', NULL, NULL, 'Seattle'
   , 'WA', 'US', '98101', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3010', NULL, 1, '03', 'Y', '500 Waterfront Way', NULL, NULL, 'Seattle'
   , 'WA', 'US', '98101', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3010', NULL, 1, '04', 'Y', '500 Waterfront Way', NULL, NULL, 'Seattle'
   , 'WA', 'US', '98101', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3010', NULL, 1, '05', 'Y', '500 Waterfront Way', NULL, NULL, 'Seattle'
   , 'WA', 'US', '98101', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1500', NULL, 1, '01', 'Y', '5010 W. 57th Street', 'Suite 200', NULL
   , 'NYC', 'NY', 'US', '10010', 'Marie Leblanc', '212-790-2323', NULL, '212-790-5001'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1500', NULL, 1, '03', 'Y', '5010 W. 57th Street', 'Suite 200', NULL
   , 'NYC', 'NY', 'US', '10010', 'Marie Leblanc', '212-790-2323', NULL, '212-790-5001'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1500', NULL, 1, '04', 'Y', '5010 W. 57th Street', 'Suite 200', NULL
   , 'NYC', 'NY', 'US', '10010', 'Marie Leblanc', '212-790-2323', NULL, '212-790-5001'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1500', NULL, 1, '05', 'Y', '5010 W. 57th Street', 'Suite 200', NULL
   , 'NYC', 'NY', 'US', '10010', 'Marie Leblanc', '212-790-2323', NULL, '212-790-5001'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2100', NULL, 1, '01', 'Y', 'PO Box 8326', NULL, NULL, 'Walnut Creek'
   , 'CA', 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2100', NULL, 1, '03', 'Y', 'PO Box 8326', NULL, NULL, 'Walnut Creek'
   , 'CA', 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2100', NULL, 1, '04', 'Y', 'PO Box 8326', NULL, NULL, 'Walnut Creek'
   , 'CA', 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2100', NULL, 1, '05', 'Y', 'PO Box 8326', NULL, NULL, 'Walnut Creek'
   , 'CA', 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3100', NULL, 1, '01', 'Y', '217 E 31st St', 'Suite 210', NULL, 'New York'
   , 'NY', 'US', '10010', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3100', NULL, 1, '03', 'Y', '217 E 31st St', 'Suite 210', NULL, 'New York'
   , 'NY', 'US', '10010', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3100', NULL, 1, '04', 'Y', '217 E 31st St', 'Suite 210', NULL, 'New York'
   , 'NY', 'US', '10010', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3100', NULL, 1, '05', 'Y', '217 E 31st St', 'Suite 210', NULL, 'New York'
   , 'NY', 'US', '10010', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3030', NULL, 1, '01', 'Y', '7899 Washington Blvd', 'Suite 110', NULL
   , 'Los Angeles', 'CA', 'US', '93212', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3030', NULL, 1, '03', 'Y', '7899 Washington Blvd', 'Suite 110', NULL
   , 'Los Angeles', 'CA', 'US', '93212', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3030', NULL, 1, '04', 'Y', '7899 Washington Blvd', 'Suite 110', NULL
   , 'Los Angeles', 'CA', 'US', '93212', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3030', NULL, 1, '05', 'Y', '7899 Washington Blvd', 'Suite 110', NULL
   , 'Los Angeles', 'CA', 'US', '93212', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3040', NULL, 1, '01', 'Y', '311 Vine Street', NULL, NULL, 'Cincinnati'
   , 'OH', 'US', '42332', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3040', NULL, 1, '03', 'Y', '311 Vine Street', NULL, NULL, 'Cincinnati'
   , 'OH', 'US', '42332', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3040', NULL, 1, '04', 'Y', '311 Vine Street', NULL, NULL, 'Cincinnati'
   , 'OH', 'US', '42332', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3040', NULL, 1, '05', 'Y', '311 Vine Street', NULL, NULL, 'Cincinnati'
   , 'OH', 'US', '42332', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3050', NULL, 1, '01', 'Y', '2143 Lake Shore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '32115', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3050', NULL, 1, '03', 'Y', '2143 Lake Shore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '32115', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3050', NULL, 1, '04', 'Y', '2143 Lake Shore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '32115', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3050', NULL, 1, '05', 'Y', '2143 Lake Shore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '32115', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '8000', NULL, 1, '01', 'Y', 'John Jones', NULL, NULL, 'Seattle', 'WA'
   , 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '8000', NULL, 1, '03', 'Y', 'John Jones', NULL, NULL, 'Seattle', 'WA'
   , 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '8000', NULL, 1, '04', 'Y', 'John Jones', NULL, NULL, 'Seattle', 'WA'
   , 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '8000', NULL, 1, '05', 'Y', 'John Jones', NULL, NULL, 'Seattle', 'WA'
   , 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6000', NULL, 1, '01', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83632', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6000', NULL, 1, '03', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83632', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6000', NULL, 1, '04', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
, 'US', '83632', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.SUPS_ADDR',
                                            to_char(SQLCODE));
      return FALSE;
END SUPS_ADDR;
--------------------------------------------------------------------------------------------
FUNCTION CAL454(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN
   -- Note - to change the date in the system, do not hard update the values
   -- on the period table - instead, run the batch program dtesys.pc
   ---
   insert into PERIOD values (1,
                              to_date('09-03-2001', 'DD-MM-YYYY'),
                              to_date('22-01-2001', 'DD-MM-YYYY'),
                              to_date('29-07-2001', 'DD-MM-YYYY'),
                              to_date('19-02-2001', 'DD-MM-YYYY'),
                              to_date('15-02-2001', 'DD-MM-YYYY'),
                              to_date('25-03-2001', 'DD-MM-YYYY'),
                              20011,
                              20012,
                              5,
                              3,
                              3,
                              2001,
                              2,
                              7);
   ---
   insert into calendar values(to_date('27-12-1993', 'DD-MM-YYYY'), 1994, 1, 4);
   insert into calendar values(to_date('24-01-1994', 'DD-MM-YYYY'), 1994, 2, 4);
   insert into calendar values(to_date('21-02-1994', 'DD-MM-YYYY'), 1994, 3, 5);
   insert into calendar values(to_date('28-03-1994', 'DD-MM-YYYY'), 1994, 4, 4);
   insert into calendar values(to_date('25-04-1994', 'DD-MM-YYYY'), 1994, 5, 4);
   insert into calendar values(to_date('23-05-1994', 'DD-MM-YYYY'), 1994, 6, 5);
   insert into calendar values(to_date('27-06-1994', 'DD-MM-YYYY'), 1994, 7, 5);
   insert into calendar values(to_date('01-08-1994', 'DD-MM-YYYY'), 1994, 8, 4);
   insert into calendar values(to_date('29-08-1994', 'DD-MM-YYYY'), 1994, 9, 5);
   insert into calendar values(to_date('03-10-1994', 'DD-MM-YYYY'), 1994, 10, 4);
   insert into calendar values(to_date('31-10-1994', 'DD-MM-YYYY'), 1994, 11, 4);
   insert into calendar values(to_date('28-11-1994', 'DD-MM-YYYY'), 1994, 12, 5);
   insert into calendar values(to_date('02-01-1995', 'DD-MM-YYYY'), 1995, 1, 4);
   insert into calendar values(to_date('30-01-1995', 'DD-MM-YYYY'), 1995, 2, 4);
   insert into calendar values(to_date('27-02-1995', 'DD-MM-YYYY'), 1995, 3, 5);
   insert into calendar values(to_date('03-04-1995', 'DD-MM-YYYY'), 1995, 4, 4);
   insert into calendar values(to_date('01-05-1995', 'DD-MM-YYYY'), 1995, 5, 4);
   insert into calendar values(to_date('29-05-1995', 'DD-MM-YYYY'), 1995, 6, 5);
   insert into calendar values(to_date('03-07-1995', 'DD-MM-YYYY'), 1995, 7, 4);
   insert into calendar values(to_date('31-07-1995', 'DD-MM-YYYY'), 1995, 8, 4);
   insert into calendar values(to_date('28-08-1995', 'DD-MM-YYYY'), 1995, 9, 5);
   insert into calendar values(to_date('02-10-1995', 'DD-MM-YYYY'), 1995, 10, 4);
   insert into calendar values(to_date('30-10-1995', 'DD-MM-YYYY'), 1995, 11, 4);
   insert into calendar values(to_date('27-11-1995', 'DD-MM-YYYY'), 1995, 12, 5);
   insert into calendar values(to_date('01-01-1996', 'DD-MM-YYYY'), 1996, 1, 4);
   insert into calendar values(to_date('29-01-1996', 'DD-MM-YYYY'), 1996, 2, 4);
   insert into calendar values(to_date('26-02-1996', 'DD-MM-YYYY'), 1996, 3, 5);
   insert into calendar values(to_date('01-04-1996', 'DD-MM-YYYY'), 1996, 4, 4);
   insert into calendar values(to_date('29-04-1996', 'DD-MM-YYYY'), 1996, 5, 4);
   insert into calendar values(to_date('27-05-1996', 'DD-MM-YYYY'), 1996, 6, 5);
   insert into calendar values(to_date('01-07-1996', 'DD-MM-YYYY'), 1996, 7, 4);
   insert into calendar values(to_date('29-07-1996', 'DD-MM-YYYY'), 1996, 8, 4);
   insert into calendar values(to_date('26-08-1996', 'DD-MM-YYYY'), 1996, 9, 5);
   insert into calendar values(to_date('30-09-1996', 'DD-MM-YYYY'), 1996, 10, 4);
   insert into calendar values(to_date('28-10-1996', 'DD-MM-YYYY'), 1996, 11, 4);
   insert into calendar values(to_date('25-11-1996', 'DD-MM-YYYY'), 1996, 12, 5);
   insert into calendar values(to_date('30-12-1996', 'DD-MM-YYYY'), 1997, 1, 4);
   insert into calendar values(to_date('27-01-1997', 'DD-MM-YYYY'), 1997, 2, 4);
   insert into calendar values(to_date('24-02-1997', 'DD-MM-YYYY'), 1997, 3, 5);
   insert into calendar values(to_date('31-03-1997', 'DD-MM-YYYY'), 1997, 4, 4);
   insert into calendar values(to_date('28-04-1997', 'DD-MM-YYYY'), 1997, 5, 4);
   insert into calendar values(to_date('26-05-1997', 'DD-MM-YYYY'), 1997, 6, 5);
   insert into calendar values(to_date('30-06-1997', 'DD-MM-YYYY'), 1997, 7, 4);
   insert into calendar values(to_date('28-07-1997', 'DD-MM-YYYY'), 1997, 8, 4);
   insert into calendar values(to_date('25-08-1997', 'DD-MM-YYYY'), 1997, 9, 5);
   insert into calendar values(to_date('29-09-1997', 'DD-MM-YYYY'), 1997, 10, 4);
   insert into calendar values(to_date('27-10-1997', 'DD-MM-YYYY'), 1997, 11, 4);
   insert into calendar values(to_date('24-11-1997', 'DD-MM-YYYY'), 1997, 12, 5);
   insert into calendar values(to_date('29-12-1997', 'DD-MM-YYYY'), 1998, 1, 4);
   insert into calendar values(to_date('26-01-1998', 'DD-MM-YYYY'), 1998, 2, 4);
   insert into calendar values(to_date('23-02-1998', 'DD-MM-YYYY'), 1998, 3, 5);
   insert into calendar values(to_date('30-03-1998', 'DD-MM-YYYY'), 1998, 4, 4);
   insert into calendar values(to_date('27-04-1998', 'DD-MM-YYYY'), 1998, 5, 4);
   insert into calendar values(to_date('25-05-1998', 'DD-MM-YYYY'), 1998, 6, 5);
   insert into calendar values(to_date('29-06-1998', 'DD-MM-YYYY'), 1998, 7, 4);
   insert into calendar values(to_date('27-07-1998', 'DD-MM-YYYY'), 1998, 8, 4);
   insert into calendar values(to_date('24-08-1998', 'DD-MM-YYYY'), 1998, 9, 5);
   insert into calendar values(to_date('28-09-1998', 'DD-MM-YYYY'), 1998, 10, 4);
   insert into calendar values(to_date('26-10-1998', 'DD-MM-YYYY'), 1998, 11, 4);
   insert into calendar values(to_date('23-11-1998', 'DD-MM-YYYY'), 1998, 12, 5);
   insert into calendar values(to_date('28-12-1998', 'DD-MM-YYYY'), 1999, 1, 4);
   insert into calendar values(to_date('25-01-1999', 'DD-MM-YYYY'), 1999, 2, 4);
   insert into calendar values(to_date('22-02-1999', 'DD-MM-YYYY'), 1999, 3, 5);
   insert into calendar values(to_date('29-03-1999', 'DD-MM-YYYY'), 1999, 4, 4);
   insert into calendar values(to_date('26-04-1999', 'DD-MM-YYYY'), 1999, 5, 4);
   insert into calendar values(to_date('24-05-1999', 'DD-MM-YYYY'), 1999, 6, 5);
   insert into calendar values(to_date('28-06-1999', 'DD-MM-YYYY'), 1999, 7, 4);
   insert into calendar values(to_date('26-07-1999', 'DD-MM-YYYY'), 1999, 8, 4);
   insert into calendar values(to_date('23-08-1999', 'DD-MM-YYYY'), 1999, 9, 5);
   insert into calendar values(to_date('27-09-1999', 'DD-MM-YYYY'), 1999, 10, 4);
   insert into calendar values(to_date('25-10-1999', 'DD-MM-YYYY'), 1999, 11, 4);
   insert into calendar values(to_date('22-11-1999', 'DD-MM-YYYY'), 1999, 12, 5);
   insert into calendar values(to_date('27-12-1999', 'DD-MM-YYYY'), 2000, 1, 4);
   insert into calendar values(to_date('24-01-2000', 'DD-MM-YYYY'), 2000, 2, 4);
   insert into calendar values(to_date('21-02-2000', 'DD-MM-YYYY'), 2000, 3, 5);
   insert into calendar values(to_date('27-03-2000', 'DD-MM-YYYY'), 2000, 4, 4);
   insert into calendar values(to_date('24-04-2000', 'DD-MM-YYYY'), 2000, 5, 4);
   insert into calendar values(to_date('22-05-2000', 'DD-MM-YYYY'), 2000, 6, 5);
   insert into calendar values(to_date('26-06-2000', 'DD-MM-YYYY'), 2000, 7, 4);
   insert into calendar values(to_date('24-07-2000', 'DD-MM-YYYY'), 2000, 8, 4);
   insert into calendar values(to_date('21-08-2000', 'DD-MM-YYYY'), 2000, 9, 5);
   insert into calendar values(to_date('25-09-2000', 'DD-MM-YYYY'), 2000, 10, 4);
   insert into calendar values(to_date('23-10-2000', 'DD-MM-YYYY'), 2000, 11, 4);
   insert into calendar values(to_date('20-11-2000', 'DD-MM-YYYY'), 2000, 12, 5);
   insert into calendar values(to_date('25-12-2000', 'DD-MM-YYYY'), 2001, 1, 4);
   insert into calendar values(to_date('22-01-2001', 'DD-MM-YYYY'), 2001, 2, 4);
   insert into calendar values(to_date('19-02-2001', 'DD-MM-YYYY'), 2001, 3, 5);
   insert into calendar values(to_date('26-03-2001', 'DD-MM-YYYY'), 2001, 4, 4);
   insert into calendar values(to_date('23-04-2001', 'DD-MM-YYYY'), 2001, 5, 4);
   insert into calendar values(to_date('21-05-2001', 'DD-MM-YYYY'), 2001, 6, 5);
   insert into calendar values(to_date('25-06-2001', 'DD-MM-YYYY'), 2001, 7, 5);
   insert into calendar values(to_date('30-07-2001', 'DD-MM-YYYY'), 2001, 8, 4);
   insert into calendar values(to_date('27-08-2001', 'DD-MM-YYYY'), 2001, 9, 5);
   insert into calendar values(to_date('01-10-2001', 'DD-MM-YYYY'), 2001, 10, 4);
   insert into calendar values(to_date('29-10-2001', 'DD-MM-YYYY'), 2001, 11, 4);
   insert into calendar values(to_date('26-11-2001', 'DD-MM-YYYY'), 2001, 12, 5);
   insert into calendar values(to_date('31-12-2001', 'DD-MM-YYYY'), 2002, 1, 4);
   insert into calendar values(to_date('28-01-2002', 'DD-MM-YYYY'), 2002, 2, 4);
   insert into calendar values(to_date('25-02-2002', 'DD-MM-YYYY'), 2002, 3, 5);
   insert into calendar values(to_date('01-04-2002', 'DD-MM-YYYY'), 2002, 4, 4);
   insert into calendar values(to_date('29-04-2002', 'DD-MM-YYYY'), 2002, 5, 4);
   insert into calendar values(to_date('27-05-2002', 'DD-MM-YYYY'), 2002, 6, 5);
   insert into calendar values(to_date('01-07-2002', 'DD-MM-YYYY'), 2002, 7, 4);
   insert into calendar values(to_date('29-07-2002', 'DD-MM-YYYY'), 2002, 8, 4);
   insert into calendar values(to_date('26-08-2002', 'DD-MM-YYYY'), 2002, 9, 5);
   insert into calendar values(to_date('30-09-2002', 'DD-MM-YYYY'), 2002, 10, 4);
   insert into calendar values(to_date('28-10-2002', 'DD-MM-YYYY'), 2002, 11, 4);
   insert into calendar values(to_date('25-11-2002', 'DD-MM-YYYY'), 2002, 12, 5);
   insert into calendar values(to_date('30-12-2002', 'DD-MM-YYYY'), 2003, 1, 4);
   insert into calendar values(to_date('27-01-2003', 'DD-MM-YYYY'), 2003, 2, 4);
   insert into calendar values(to_date('24-02-2003', 'DD-MM-YYYY'), 2003, 3, 5);
   insert into calendar values(to_date('31-03-2003', 'DD-MM-YYYY'), 2003, 4, 4);
   insert into calendar values(to_date('28-04-2003', 'DD-MM-YYYY'), 2003, 5, 4);
   insert into calendar values(to_date('26-05-2003', 'DD-MM-YYYY'), 2003, 6, 5);
   insert into calendar values(to_date('30-06-2003', 'DD-MM-YYYY'), 2003, 7, 4);
   insert into calendar values(to_date('28-07-2003', 'DD-MM-YYYY'), 2003, 8, 4);
   insert into calendar values(to_date('25-08-2003', 'DD-MM-YYYY'), 2003, 9, 5);
   insert into calendar values(to_date('29-09-2003', 'DD-MM-YYYY'), 2003, 10, 4);
   insert into calendar values(to_date('27-10-2003', 'DD-MM-YYYY'), 2003, 11, 4);
   insert into calendar values(to_date('24-11-2003', 'DD-MM-YYYY'), 2003, 12, 5);
   insert into calendar values(to_date('29-12-2003', 'DD-MM-YYYY'), 2004, 1, 4);
   ---
   insert into half values (19941, 'Summer 1994', 'Feb 1994 to Jul 1994');
   insert into half values (19942, 'Winter 1994', 'Aug 1994 to Jan 1995');
   insert into half values (19951, 'Summer 1995', 'Feb 1995 to Jul 1995');
   insert into half values (19952, 'Winter 1995', 'Aug 1995 to Jan 1996');
   insert into half values (19961, 'Summer 1996', 'Feb 1996 to Jul 1996');
   insert into half values (19962, 'Winter 1996', 'Aug 1996 to Jan 1997');
   insert into half values (19971, 'Summer 1997', 'Feb 1997 to Jul 1997');
   insert into half values (19972, 'Winter 1997', 'Aug 1997 to Jan 1998');
   insert into half values (19981, 'Summer 1998', 'Feb 1998 to Jul 1998');
   insert into half values (19982, 'Winter 1998', 'Aug 1998 to Jan 1999');
   insert into half values (19991, 'Summer 1999', 'Feb 1999 to Jul 1999');
   insert into half values (19992, 'Winter 1999', 'Aug 1999 to Jan 2000');
   insert into half values (20001, 'Summer 2000', 'Feb 2000 to Jul 2000');
   insert into half values (20002, 'Winter 2000', 'Aug 2000 to Jan 2001');
   insert into half values (20011, 'Summer 2001', 'Feb 2001 to Jul 2001');
   insert into half values (20012, 'Winter 2001', 'Aug 2001 to Jan 2002');
   insert into half values (20021, 'Summer 2002', 'Feb 2002 to Jul 2002');
   insert into half values (20022, 'Winter 2002', 'Aug 2002 to Jan 2003');
   insert into half values (20031, 'Summer 2003', 'Feb 2003 to Jul 2003');
   insert into half values (20032, 'Winter 2003', 'Aug 2003 to Jan 2004');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.CAL454',
                                            to_char(SQLCODE));
      return FALSE;
END CAL454;
--------------------------------------------------------------------------------------------
FUNCTION UDAS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   101, 'In Store Date', 'ITEM', 'DT', 'DATE', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   102, 'Holiday*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   9, 'Yogurt Brands*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   11, 'Canned Food Brands*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   13, 'Tobacco Brands*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   14, 'Beer Brands*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   15, 'Cheese Brands*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   17, 'HBC Brands*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   24, 'Packaging*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   25, 'Sodium Content*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   26, 'Caloric Content*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   27, 'Fat Content*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   29, 'Organically Grown*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   30, 'Tobacco*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   33, 'Whitening Agent*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   35, 'Ranging Cluster*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   39, 'Coordinate Groups*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   40, 'Material*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   42, 'Fit*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   43, 'Sleeve*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   44, 'Top Silhouette*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   46, 'Women''s Footwear Silhouette*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   47, 'Heel Height*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   48, 'Athletic Shoe Silhouette*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   51, 'Trim*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   52, 'Prints*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   53, 'Pant Silhouette*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   54, 'Lifestyle*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   55, 'Fabrication*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   56, 'Denim Silhouette*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   57, 'Care Instructions*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   59, 'Neck Silhouette*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   60, 'Ornamentation*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   61, 'Closure*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   62, 'Hem*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   65, 'Precious Metal (Jewelry)*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   66, 'Gemstones*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   67, 'Cut (Jewelry)*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   68, 'Carat Weight (Jewelry)*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   69, 'Clarity*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   70, 'Color (Diamond Jewelry)*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   71, 'Setting (Jewelry*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   80, 'DVD Sound Types*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   79, 'DVD Inputs*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   81, 'DVD Features*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   92, 'Battery Features*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   1302, 'Toothbrush Features*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   1807, 'Calibre for Fruits, Vegetables*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   1810, 'Item Role*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   1811, 'Item Classification*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   2, 'Theme*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   301, 'Furniture Finish*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   302, 'Furniture Material*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   201, 'Publisher''s Notes', 'ITEM', 'FF', 'ALPHA', NULL, 'N', NULL, NULL);
   INSERT INTO UDA ( UDA_ID, UDA_DESC, MODULE, DISPLAY_TYPE, DATA_TYPE, DATA_LENGTH, SINGLE_VALUE_IND,
   FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES ( 202, 'Toothpaste Brands*', 'ITEM', 'LV', 'ALPHA', NULL, 'N', NULL, NULL);
   -----------------------------------------
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 14, 'Silk Screened/Graphic');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   102, 1, 'New Year''s');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   102, 2, 'Easter');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   102, 3, 'Memorial Day');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   102, 4, '4th of July');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   102, 5, 'Labor Day');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   102, 6, 'Thanksgiving');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   102, 7, 'Christmas');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 34, '4 Pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 35, '8 Pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 36, 'Leather');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 37, 'Moleskin');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   15, 7, 'Stella');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 13, 'Fresh-water Pearl');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 14, 'Black Pearl');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 15, 'Iolite');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 16, 'Peridot');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 17, 'Garnet');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 18, 'Aquamarine');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 19, 'Opal');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 20, 'Amethyst');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 21, 'Citrine');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 22, 'Blue Topaz');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 23, 'Yellow Topaz');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 24, 'Pink Topaz');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 25, 'Onyx');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 26, 'Jade');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 27, 'Multi-Stone');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 1, 'Brilliant Cut (Round)');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 2, 'Princess Cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 3, 'Oval Cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 4, 'Marquise Cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 5, 'Emerald Cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 6, 'Pear Cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 7, 'Heart Cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 8, 'Cushion Cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 9, 'Baguette');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   67, 10, 'Tapered Baguette');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 1, '.01');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 2, '.02');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 3, '.05');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 4, '.10');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 5, '.15');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 6, '.20');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 7, '.25');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 8, '.33');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 9, '.40');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 10, '.50');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 11, '.60');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 12, '.66');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 13, '.70');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 14, '.75');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 15, '.80');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 16, '.90');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 17, '.95');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 18, '1.00');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 19, '1.25');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 20, '1.33');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 21, '1.50');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 22, '1.66');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 23, '1.75');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 24, '2.00');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 25, '2.25');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 26, '2.50');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 27, '2.75');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 28, '3.00');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 29, '3.25');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   68, 30, '3.50');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 1, 'I');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 2, 'I1');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 3, 'I2');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 4, 'I3');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 5, 'SI');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 6, 'SI1');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 7, 'SI2');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 8, 'SI3');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 9, 'VS');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 10, 'VS1');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 11, 'VS2');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 12, 'VVS');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 13, 'VVS1');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 14, 'VVS2');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 15, 'IF');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   69, 16, 'FL');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 1, 'D');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 2, 'E');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 3, 'F');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 4, 'G');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 5, 'H');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 6, 'I');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 7, 'J');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 8, 'K');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 9, 'L');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 10, 'M');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 11, 'N');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 12, 'O');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 13, 'P');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 14, 'Q');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 15, 'R');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 16, 'S');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 17, 'T');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 18, 'U');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 19, 'V');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 20, 'W');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 21, 'X');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 22, 'Y');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 23, 'Z');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   70, 24, 'Z+  Fancy');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   71, 1, '4 Prong');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   71, 2, '6 Prong');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   71, 3, 'Bezel Set');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   71, 4, 'Channel Set');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   71, 5, 'Pave');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   71, 6, 'Invisible Setting');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   71, 7, 'Floating');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   30, 6, '84''s');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 32, 'Hard pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 33, 'Tin');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   79, 2, '5 channel inputs');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   79, 3, '6 channel inputs');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   79, 1, '4 channel inputs');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 38, 'Cotton');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 39, 'Silk');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   80, 1, 'Dolby Digital');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   80, 2, 'Dolby Surround');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   80, 3, 'Surround EX');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   80, 4, 'Dolby Surround Prologic II');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   81, 1, 'THX rated');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   81, 2, 'Low Frequency Effects');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   81, 3, 'Current Biasing');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   81, 4, 'Bookshelf Speaker');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   81, 5, 'Floor Speaker');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   81, 6, 'A/V Receiver');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 40, 'Linen');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 41, 'Wool');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 42, 'Cotton/Lycra');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   81, 7, 'Remote Included');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   81, 8, 'Remote Not Included');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1302, 1, 'Pivoting Head');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1302, 2, 'Flexible Neck');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1302, 3, 'Control Grip');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1302, 4, 'Angled Head');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1302, 5, 'Interdental');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   92, 1, 'Rechargeable');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   92, 2, 'Self-Testing');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1302, 6, 'Bi-Level Bristles');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1302, 7, 'Rippled Bristles');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 23, 'Percale');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 24, 'Poplin');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 25, 'Jersey');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 26, 'Interlock');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 27, '1 x 1 Rib');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 28, '2 X 2 Rib');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 29, 'Varigated Rib');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 30, 'Terry');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 31, 'Poplin');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 32, 'Broadcloth');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1807, 1, 'AA');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1807, 2, 'A');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1807, 3, 'B');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1807, 4, 'C');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1807, 5, 'D');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1810, 1, 'Traffic Driver');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1810, 2, 'Profit Generator');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1810, 3, 'Winner');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1810, 4, 'Loser');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1811, 1, 'Essential in Assortment');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1811, 2, 'First Assortment Extension');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1811, 3, 'Spread Assortment');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   1811, 4, 'Local Add-ons');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   11, 4, 'Private Label');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   15, 8, 'Crystal Farms');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   9, 3, 'Yoplait');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 6, 'Holsten');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 7, 'Tuborg');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 8, 'Konig');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 9, 'Becks');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 10, 'Warsteiner');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   9, 1, 'Dannon');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   9, 2, 'Yogurt Brands');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   11, 1, 'Campbell''s');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   11, 2, 'Del Monte');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   11, 3, 'Dole');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   13, 1, 'Benson Hedges');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   13, 2, 'Marlboro');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 1, 'Budweiser');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 2, 'Corona');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 3, 'Grolsch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 4, 'Miller');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   15, 1, 'Kraft');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   15, 2, 'Land ''o Lakes');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   15, 3, 'No Brand');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   15, 4, 'Private Label');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   15, 5, 'Tillamook');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   15, 6, 'Wisconsin');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   17, 1, 'Old Spice');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   17, 2, 'Johnson''s  Johnson''s');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   14, 5, 'Boddington''s');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 1, '12 Pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 2, '18 Pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 3, '24 Pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 4, '6 Pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 5, '6 Snap Pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 6, 'Aluminum');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 7, 'Bag');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 8, 'Bottle');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 9, 'Box');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 10, 'Can');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 11, 'Cardboard');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 12, 'Carton');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 13, 'Clingwrap');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 14, 'Cup');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 15, 'Glass');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 16, 'Glass Bottle');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 17, 'Plastic');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 18, 'Plastic Bottle');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 19, 'Pre-Packed');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 20, 'Recyclable');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 21, 'Recycled Materials');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 22, 'Sachet');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 23, 'Soft Pack');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 24, 'Styrofoam');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 25, 'Tray');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 26, 'Tub');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 27, '1 S Tray');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 28, '2 S Tray');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 29, '8 S Tray');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 30, '17 S Tray');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   24, 31, 'Plastic Jar');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   25, 1, 'Full Sodium');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   25, 2, 'Low Sodium');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   25, 3, 'Sodium Free');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   26, 1, 'Diet');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   26, 2, 'Regular');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   27, 1, 'Full Fat');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   27, 2, 'Low Fat');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   27, 3, 'Nonfat');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   29, 1, 'Yes');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   29, 2, 'No');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   30, 1, 'Low Tar');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   30, 2, 'Low Nicotine');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   30, 3, '100''s');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   30, 4, 'Regular');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   30, 5, 'Filter Tip');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   33, 1, 'With Baking Soda');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   33, 2, 'Without Baking Soda');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   35, 1, 'Core Item');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   35, 2, 'Extended');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   35, 3, 'Test Item');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   35, 4, 'Emergency');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   35, 5, 'QSR');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   39, 1, 'Signature Collection');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   39, 2, 'Bridge Collection');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   39, 3, 'Hamptons');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   39, 4, 'Embellished Collection');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   39, 5, 'Weekend Wear');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   39, 6, 'Downtown Dressy');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   39, 7, 'Aspen Adventures');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   39, 8, 'Urban Unstressed');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   40, 1, 'Leather');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   40, 2, 'Canvas');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   40, 3, 'Nubuck');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   40, 4, 'Crocodile');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   40, 5, 'Synthetic');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   40, 6, 'Rubber');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   40, 7, 'Patent Leather');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   42, 1, 'Regular');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   42, 2, 'Athletic');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   42, 3, 'Full');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   42, 4, 'Slim');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   42, 5, 'Petite');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   42, 6, 'Tall');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   43, 1, 'Long Sleeve');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   43, 2, 'Short Sleeve');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   43, 3, '3/4 Sleeve');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   43, 4, 'Sleeveless');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   43, 5, 'Bandeau');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   43, 6, 'Cap Sleeve');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 1, 'Shirt Jacket');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 2, 'Halter');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 3, 'Tank');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 4, 'Cardigan');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 5, 'Hooded');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 6, 'Straps');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 7, 'Wrap');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 8, 'Button Down');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 9, 'Pullover');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 10, 'Jacket');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   44, 11, 'One Shoulder');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 1, 'Open toe');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 2, 'Closed toe');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 3, 'Box toe');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 4, 'Sling back');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 5, 'Loafer');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 6, 'Mule');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 7, 'Lug heel');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 8, 'Pump');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 9, 'Strappy');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 10, 'Platform');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 11, 'Side zip');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 12, 'Boot');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 13, 'Lace up');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 14, 'Oxford');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 15, 'Slip on');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 16, 'Monk strap');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   46, 17, 'Sandal');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   47, 1, '1/2 inch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   47, 2, '1 inch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   47, 3, '1 1/4 inch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   47, 4, '1 1/2 inch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   47, 5, '2 inch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   47, 6, '2 1/2 inch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   47, 7, '3 inch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   48, 1, 'Low cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   48, 2, 'Mid cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   48, 3, 'High cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   48, 4, 'Regular');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   51, 1, 'Collar');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   51, 2, 'Cuff');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   51, 3, 'Waist');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   51, 4, 'Bands');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   51, 5, 'Hem');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   52, 1, 'Animal print');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   52, 2, 'Leopard print');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   52, 3, 'Zebra print');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   52, 4, 'Giraffe print');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   52, 5, 'Pony print');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 1, 'Boot cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 2, 'Flair');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 3, 'Straight leg');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 4, 'Loose');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 5, 'Relaxed');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 6, 'Capri / Cropped');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 7, 'Flat front');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 8, 'Pleated');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 9, 'Side zip');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 10, 'Stretch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 11, 'Cargo');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 12, 'Carpenter');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   53, 13, 'Drawstring');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   54, 1, 'Trendy');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   54, 2, 'Classic');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   54, 3, 'Casual');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   54, 4, 'Business attire');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   54, 5, 'Formal');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   54, 6, 'Traditional');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   54, 7, 'Contemporary');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 3, 'Organza');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 4, 'Corduroy');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 11, 'Felt');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 12, 'Chenille');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 15, 'Flannel');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 18, 'Denim');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 21, 'Fleece');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 1, 'Boot cut');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 2, 'Flair');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 3, 'Straight leg');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 4, 'Capri');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 5, 'Carpenter');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 6, 'Cargo');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 7, 'Hipster flair');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 8, 'Stretch');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 9, '5 pocket');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 10, 'Loose fit');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   56, 11, 'Relaxed fit');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   57, 1, 'Dry clean only');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   57, 2, 'Wash in warm');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   57, 3, 'Hand wash');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   57, 4, 'Wash in cold');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   57, 5, 'Delicate cycle');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   57, 6, 'Hang dry');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   59, 1, 'Turtleneck');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   59, 2, 'Crew');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   59, 3, 'V-neck');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   59, 4, 'Boatneck');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 1, 'Ruffled');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 2, 'Laced');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 3, 'Beaded');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 4, 'Sequined');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 5, 'Flocking');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 6, 'Cut-outs');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 7, 'Embroidery');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 8, 'Eyelet');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 9, 'Fringe');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 10, 'Quilted');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 11, 'Zipped');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 12, 'Braids');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   60, 13, 'Piping');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 1, 'Zip');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 2, 'Button');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 3, 'Velcro');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 4, 'Drawstring');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 5, 'Tie');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 6, 'Buckle');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 7, 'Snap');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 8, 'Hook and eye');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   61, 9, 'Tab');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   62, 1, 'Tulip');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   62, 2, 'Asymmetrical');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   62, 3, 'Ruffled');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   62, 4, 'Pleated');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   62, 5, 'Cuffed');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 1, '10K White Gold');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 2, '10K Yellow Gold');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 3, '14K White Gold');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 4, '14K Yellow Gold');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 5, '18K Gold');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 6, '24K Gold');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 7, 'Platinum');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 8, 'Two-Tone');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   65, 9, 'Sterling Silver');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 1, 'Ruby');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 2, 'Sapphire');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 3, 'Emerald');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 4, 'Tanzanite');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 5, 'Alexandrite');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 6, 'Yellow Sapphire');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 7, 'Pink Sapphire');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 8, 'White Sapphire');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 9, 'Pink Tourmaline');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 10, 'Green Tourmaline');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 11, 'Cultured Pearl');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   66, 12, 'Tahitian Pearl');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 33, 'Oxford CLoth');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 34, 'Gaberdine');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   55, 35, 'Suede');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   2, 1, 'The New Classics*');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   2, 2, 'Tiki Time');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   2, 3, 'Fashion Forward');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   202, 1, 'Colgate');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   202, 2, 'Crest');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   202, 3, 'Aquafresh');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   202, 4, 'Mentadent');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   202, 5, 'Sensodyne');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   301, 1, 'Ash');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   301, 2, 'Maple');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   301, 3, 'Cherry');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   301, 4, 'Mahogany');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   301, 5, 'Oak');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   302, 1, 'Metal');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   302, 2, 'Wood');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   302, 3, 'Plastic');
   INSERT INTO UDA_VALUES ( UDA_ID, UDA_VALUE, UDA_VALUE_DESC ) VALUES (
   302, 4, 'Fiberboard');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.UDAS',
                                            to_char(SQLCODE));
      return FALSE;
END UDAS;
--------------------------------------------------------------------------------------------
FUNCTION DIFFS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN
---DIFF TYPES
---S = Size
---C = Color
---F = Flavor
---E = Scent
---P = Pattern

   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'S', 'Size');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'C', 'Color');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'F', 'Flavor');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'E', 'Scent');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'P', 'Pattern');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'HLSIZE', 'Hardlines Sizes');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'GRSIZE', 'Grocery Sizes');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'WAIST', 'Waist');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'INSEAM', 'Inseams');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'SHOE', 'Shoe Sizes');
   INSERT INTO DIFF_TYPE ( DIFF_TYPE, DIFF_TYPE_DESC ) VALUES (
   'SHOEWI', 'Shoe Width');


---COLORS

   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BEIGE', 'C', 'Beige', NULL, NULL,  TO_Date( '11/01/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '02/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BLUEBERRY', 'F', 'Blueberry', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'PEACH', 'F', 'Peach', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BANANA', 'F', 'Banana', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'CHERRY', 'F', 'Cherry', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'XS', 'S', 'Extra Small', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'SMALL', 'S', 'Small', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'MEDIUM', 'S', 'Medium', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'LARGE', 'S', 'Large', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'XL', 'S', 'Extra Large', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '2', 'S', '2', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '4', 'S', '4', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6', 'S', '6', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '8', 'S', '8', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '10', 'S', '10', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12', 'S', '12', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '14', 'S', '14', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'SHORT', 'INSEAM', 'Short', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'REGULAR', 'INSEAM', 'Regular', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'LONG', 'INSEAM', 'Long', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '28', 'INSEAM', '28', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '29', 'INSEAM', '29', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '30', 'INSEAM', '30', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '31', 'INSEAM', '31', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '32', 'INSEAM', '32', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '33', 'INSEAM', '33', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '34', 'INSEAM', '34', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '35', 'INSEAM', '35', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '36', 'INSEAM', '36', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '28W', 'WAIST', '28W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '29W', 'WAIST', '29W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '30W', 'WAIST', '30W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '31W', 'WAIST', '31W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '32W', 'WAIST', '32W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '33W', 'WAIST', '33W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '34W', 'WAIST', '34W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '35W', 'WAIST', '35W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '36W', 'WAIST', '36W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '37W', 'WAIST', '37W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '38W', 'WAIST', '38W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '39W', 'WAIST', '39W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '40W', 'WAIST', '40W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '41W', 'WAIST', '41W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '42W', 'WAIST', '42W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '43W', 'WAIST', '43W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '44W', 'WAIST', '44W', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6OZ', 'GRSIZE', '6OZ', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12 OZ', 'GRSIZE', '12 OZ', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '14.5 OZ', 'GRSIZE', '14.5 OZ', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '16 OZ', 'GRSIZE', '16 OZ', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '28 OZ', 'GRSIZE', '28 OZ', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '32OZ', 'GRSIZE', '32 OZ', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'VAN', 'E', 'VANILLA', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'CIN', 'E', 'Cinnamon', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'SPICEDPEAR', 'E', 'Spiced Pear', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'AAA', 'HLSIZE', 'AAA', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'AA', 'HLSIZE', 'AA', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'C', 'HLSIZE', 'C', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'D', 'HLSIZE', 'D', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '9V', 'HLSIZE', '9V', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6 INCH', 'HLSIZE', '6 Inch', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12 INCH', 'HLSIZE', '12 Inch', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BLACK', 'C', 'Black', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'WHITE', 'C', 'White', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'RED', 'C', 'Red', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'NAVY', 'C', 'Navy', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BROWN', 'C', 'Brown', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'GRAY', 'C', 'Gray', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'GREEN', 'C', 'Green', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'YELLOW', 'C', 'Yellow', NULL, NULL,  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/08/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'GOOSEBERRY', 'F', 'Gooseberry', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '5 1/2 FTW', 'SHOE', '5 1/2 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6 FTW', 'SHOE', '6 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6 1/2 FTW', 'SHOE', '6 1/2 FTW', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '7 FTW', 'SHOE', '7 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '7 1/2 FTW', 'SHOE', '7 1/2 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '8 FTW', 'SHOE', '8 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '8 1/2 FTW', 'SHOE', '8 1/2 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '9 FTW', 'SHOE', '9 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '9 1/2 FTW', 'SHOE', '9 1/2 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '10 FTW', 'SHOE', '10 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '10 1/2 FTW', 'SHOE', '10 1/2 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '11 FTW', 'SHOE', '11 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '11 1/2 FTW', 'SHOE', '11 1/2 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12 FTW', 'SHOE', '12 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12 1/2 FTW', 'SHOE', '12 1/2 FTW', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '13 FTW', 'SHOE', '13 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '13 1/2 FTW', 'SHOE', '13 1/2 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '14 FTW', 'SHOE', '14 Footwear', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'NARROW', 'SHOEWI', 'Narrow', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'MED', 'SHOEWI', 'Medium', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'WIDE', 'SHOEWI', 'Wide', NULL, NULL,  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '8 OZ', 'GRSIZE', '8 OZ', NULL, NULL,  TO_Date( '10/18/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/18/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '4 OZ', 'GRSIZE', '4oz', NULL, NULL,  TO_Date( '10/20/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/20/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'WASHED', 'C', 'Washed', NULL, NULL,  TO_Date( '10/19/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/19/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'ANTIQUE', 'C', 'Antique', NULL, NULL,  TO_Date( '10/19/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/19/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'DARK WASH', 'C', 'Dark Wash', NULL, NULL,  TO_Date( '10/19/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/19/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'RASPBERRY', 'F', 'Raspberry', NULL, NULL,  TO_Date( '11/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '11/12/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_IDS ( DIFF_ID, DIFF_TYPE, DIFF_DESC, INDUSTRY_CODE, INDUSTRY_SUBGROUP,
   CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'TOBACCO', 'C', 'Tobacco', NULL, NULL,  TO_Date( '02/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
, 'RMSDEM11',  TO_Date( '02/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));

---DIFF GROUPS
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'MAKEUPCLR', 'C', 'Make-up Colors',  TO_Date( '11/01/2004 11:37:18 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '11/01/2004 11:37:18 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'BATT SIZES', 'HLSIZE', 'Battery Sizes',  TO_Date( '10/12/2004 08:33:24 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 08:33:24 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 3000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'WOMEN SIZE', 'S', 'Women SIzes',  TO_Date( '10/12/2004 08:37:02 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 08:37:02 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'XS-XL', 'S', 'XS-XL',  TO_Date( '10/12/2004 08:38:29 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 08:38:29 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'GEN INSEAM', 'INSEAM', 'Generic inseam',  TO_Date( '10/12/2004 08:40:15 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 08:40:15 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'MENS INSM', 'INSEAM', 'Men''s Inseam',  TO_Date( '10/12/2004 08:41:04 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 08:41:04 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'WAIST GRP', 'WAIST', 'Waist',  TO_Date( '10/12/2004 08:43:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 08:43:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'BASIC CLRS', 'C', 'Basic Color Pallet',  TO_Date( '10/12/2004 08:52:47 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 08:52:47 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'LENGTH', 'HLSIZE', 'Length',  TO_Date( '10/12/2004 08:55:28 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 08:55:28 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 3000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'GR FLAVORS', 'F', 'Basic Flavors',  TO_Date( '10/12/2004 11:50:49 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 11:50:49 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 1000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'GR SIZE', 'GRSIZE', 'Basic Grocery Sizes',  TO_Date( '10/12/2004 11:52:08 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '10/12/2004 11:52:08 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 1000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'SHOE SIZE', 'SHOE', 'Shoe Sizes (5-14)',  TO_Date( '10/13/2004 01:15:41 PM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/13/2004 01:15:41 PM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'SHOE WIDTH', 'SHOEWI', 'Shoe Widths',  TO_Date( '10/13/2004 01:18:39 PM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/13/2004 01:18:39 PM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'DEN COLORS', 'C', 'Denim Colors',  TO_Date( '10/19/2004 07:34:40 PM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/19/2004 07:34:40 PM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 2000);
   INSERT INTO DIFF_GROUP_HEAD ( DIFF_GROUP_ID, DIFF_TYPE, DIFF_GROUP_DESC, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME, FILTER_ORG_ID, FILTER_MERCH_ID ) VALUES (
   'CANDSCENT', 'E', 'Candle Scents',  TO_Date( '10/26/2004 10:15:42 AM', 'MM/DD/YYYY HH:MI:SS AM'),
   'RMSDEM11',  TO_Date( '10/26/2004 10:15:42 AM', 'MM/DD/YYYY HH:MI:SS AM'), 1, 3000);
   ---
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BLACK', 'MAKEUPCLR', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BROWN', 'MAKEUPCLR', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'GRAY', 'MAKEUPCLR', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'NAVY', 'MAKEUPCLR', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'GREEN', 'MAKEUPCLR', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BEIGE', 'MAKEUPCLR', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '9V', 'BATT SIZES', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'AA', 'BATT SIZES', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'AAA', 'BATT SIZES', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'C', 'BATT SIZES', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'D', 'BATT SIZES', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '2', 'WOMEN SIZE', 1,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '4', 'WOMEN SIZE', 2,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6', 'WOMEN SIZE', 3,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '8', 'WOMEN SIZE', 4,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '10', 'WOMEN SIZE', 5,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12', 'WOMEN SIZE', 6,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '14', 'WOMEN SIZE', 7,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'XS', 'XS-XL', 1,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), 'RMS11_TST'
   ,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'SMALL', 'XS-XL', 2,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'MEDIUM', 'XS-XL', 3,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'LARGE', 'XS-XL', 4,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'XL', 'XS-XL', 5,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), 'RMS11_TST'
   ,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'SHORT', 'GEN INSEAM', 1,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'REGULAR', 'GEN INSEAM', 2,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'LONG', 'GEN INSEAM', 3,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '28', 'MENS INSM', 1,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '29', 'MENS INSM', 2,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '30', 'MENS INSM', 3,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '31', 'MENS INSM', 4,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '32', 'MENS INSM', 5,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '33', 'MENS INSM', 6,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '34', 'MENS INSM', 7,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '35', 'MENS INSM', 8,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '36', 'MENS INSM', 9,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '28W', 'WAIST GRP', 1,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '29W', 'WAIST GRP', 2,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '30W', 'WAIST GRP', 3,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '31W', 'WAIST GRP', 4,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '32W', 'WAIST GRP', 5,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '33W', 'WAIST GRP', 6,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '34W', 'WAIST GRP', 7,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '35W', 'WAIST GRP', 8,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '36W', 'WAIST GRP', 9,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '37W', 'WAIST GRP', 10,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '38W', 'WAIST GRP', 11,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '39W', 'WAIST GRP', 12,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '40W', 'WAIST GRP', 13,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '41W', 'WAIST GRP', 14,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '42W', 'WAIST GRP', 15,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '43W', 'WAIST GRP', 16,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '44W', 'WAIST GRP', 17,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BLACK', 'BASIC CLRS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BROWN', 'BASIC CLRS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'GRAY', 'BASIC CLRS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'GREEN', 'BASIC CLRS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'NAVY', 'BASIC CLRS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'RED', 'BASIC CLRS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'WHITE', 'BASIC CLRS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'YELLOW', 'BASIC CLRS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12 INCH', 'LENGTH', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6 INCH', 'LENGTH', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BANANA', 'GR FLAVORS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'BLUEBERRY', 'GR FLAVORS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'CHERRY', 'GR FLAVORS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'GOOSEBERRY', 'GR FLAVORS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'PEACH', 'GR FLAVORS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'RASPBERRY', 'GR FLAVORS', NULL,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6OZ', 'GR SIZE', 1,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMS11_TST',  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12 OZ', 'GR SIZE', 3,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/18/2004 04:18:51 PM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '14.5 OZ', 'GR SIZE', 4,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/18/2004 04:18:53 PM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '16 OZ', 'GR SIZE', 5,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/18/2004 04:18:53 PM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '28 OZ', 'GR SIZE', 6,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/18/2004 04:18:53 PM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '32OZ', 'GR SIZE', 7,  TO_Date( '09/27/2004 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '10/18/2004 04:18:53 PM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '8 OZ', 'GR SIZE', 2,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '5 1/2 FTW', 'SHOE SIZE', 1,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6 FTW', 'SHOE SIZE', 2,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '6 1/2 FTW', 'SHOE SIZE', 3,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '7 FTW', 'SHOE SIZE', 4,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '7 1/2 FTW', 'SHOE SIZE', 5,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '8 FTW', 'SHOE SIZE', 6,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '8 1/2 FTW', 'SHOE SIZE', 7,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '9 FTW', 'SHOE SIZE', 8,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '9 1/2 FTW', 'SHOE SIZE', 9,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '10 FTW', 'SHOE SIZE', 10,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '10 1/2 FTW', 'SHOE SIZE', 11,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '11 FTW', 'SHOE SIZE', 12,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '11 1/2 FTW', 'SHOE SIZE', 13,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12 FTW', 'SHOE SIZE', 14,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '12 1/2 FTW', 'SHOE SIZE', 15,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '13 FTW', 'SHOE SIZE', 16,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '13 1/2 FTW', 'SHOE SIZE', 17,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   '14 FTW', 'SHOE SIZE', 18,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'NARROW', 'SHOE WIDTH', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'MED', 'SHOE WIDTH', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'WIDE', 'SHOE WIDTH', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'ANTIQUE', 'DEN COLORS', 1,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'WASHED', 'DEN COLORS', 2,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'DARK WASH', 'DEN COLORS', 3,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'CIN', 'CANDSCENT', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'SPICEDPEAR', 'CANDSCENT', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'VAN', 'CANDSCENT', NULL,  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM')
   , 'RMSDEM11',  TO_Date( '01/01/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));
   INSERT INTO DIFF_GROUP_DETAIL ( DIFF_ID, DIFF_GROUP_ID, DISPLAY_SEQ, CREATE_DATETIME,
   LAST_UPDATE_ID, LAST_UPDATE_DATETIME ) VALUES (
   'TOBACCO', 'BASIC CLRS', NULL,  TO_Date( '01/29/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'),
   'RMSDEM11',  TO_Date( '01/29/2005 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.DIFFS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END DIFFS;
--------------------------------------------------------------------------------------------
FUNCTION DEAL_COMP_TYPE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO DEAL_COMP_TYPE ( DEAL_COMP_TYPE )
                       VALUES ( 'VFC');
   INSERT INTO DEAL_COMP_TYPE ( DEAL_COMP_TYPE )
                       VALUES ( 'VOL');
   INSERT INTO DEAL_COMP_TYPE ( DEAL_COMP_TYPE )
                       VALUES ( 'TP');
   INSERT INTO DEAL_COMP_TYPE ( DEAL_COMP_TYPE )
                       VALUES ( 'OTHER');
   INSERT INTO DEAL_COMP_TYPE ( DEAL_COMP_TYPE )
                       VALUES ( 'TBB');
   ---
   INSERT INTO DEAL_COMP_TYPE_TL ( DEAL_COMP_TYPE,
                                   LANG,
                                   DEAL_COMP_TYPE_DESC,
                                   ORIG_LANG_IND,
                                   REVIEWED_IND,
                                   CREATE_ID,
                                   CREATE_DATETIME,
                                   LAST_UPDATE_ID,
                                   LAST_UPDATE_DATETIME )
                          VALUES ( 'VFC', 
                                   1, 
                                   'Vendor Funded Coupon', 
                                   'Y', 
                                   'N', 
                                   GET_USER, 
                                   SYSDATE, 
                                   GET_USER, 
                                   SYSDATE); 
   INSERT INTO DEAL_COMP_TYPE_TL ( DEAL_COMP_TYPE,
                                   LANG,
                                   DEAL_COMP_TYPE_DESC,
                                   ORIG_LANG_IND,
                                   REVIEWED_IND,
                                   CREATE_ID,
                                   CREATE_DATETIME,
                                   LAST_UPDATE_ID,
                                   LAST_UPDATE_DATETIME )
                          VALUES ( 'VOL', 
                                   1, 
                                   'Volume Rebate', 
                                   'Y', 
                                   'N', 
                                   GET_USER, 
                                   SYSDATE, 
                                   GET_USER, 
                                   SYSDATE);
   INSERT INTO DEAL_COMP_TYPE_TL ( DEAL_COMP_TYPE,
                                   LANG,
                                   DEAL_COMP_TYPE_DESC,
                                   ORIG_LANG_IND,
                                   REVIEWED_IND,
                                   CREATE_ID,
                                   CREATE_DATETIME,
                                   LAST_UPDATE_ID,
                                   LAST_UPDATE_DATETIME )
                          VALUES ( 'TP', 
                                   1, 
                                   'Temporary Off Invoice', 
                                   'Y', 
                                   'N', 
                                   GET_USER, 
                                   SYSDATE, 
                                   GET_USER, 
                                   SYSDATE);
   INSERT INTO DEAL_COMP_TYPE_TL ( DEAL_COMP_TYPE,
                                   LANG,
                                   DEAL_COMP_TYPE_DESC,
                                   ORIG_LANG_IND,
                                   REVIEWED_IND,
                                   CREATE_ID,
                                   CREATE_DATETIME,
                                   LAST_UPDATE_ID,
                                   LAST_UPDATE_DATETIME )
                          VALUES ( 'OTHER', 
                                   1, 
                                   'Other Deal Component Type', 
                                   'Y', 
                                   'N', 
                                   GET_USER, 
                                   SYSDATE, 
                                   GET_USER, 
                                   SYSDATE);
   INSERT INTO DEAL_COMP_TYPE_TL ( DEAL_COMP_TYPE,
                                   LANG,
                                   DEAL_COMP_TYPE_DESC,
                                   ORIG_LANG_IND,
                                   REVIEWED_IND,
                                   CREATE_ID,
                                   CREATE_DATETIME,
                                   LAST_UPDATE_ID,
                                   LAST_UPDATE_DATETIME )
                          VALUES ( 'TBB', 
                                   1, 
                                   'Temporary Bill Back', 
                                   'Y', 
                                   'N', 
                                   GET_USER, 
                                   SYSDATE, 
                                   GET_USER, 
                                   SYSDATE);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.DEAL_COMP_TYPE',
                                            to_char(SQLCODE));
      return FALSE;
END DEAL_COMP_TYPE;
--------------------------------------------------------------------------------------------
FUNCTION DIVISION(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO DIVISION ( DIVISION,
                          DIV_NAME,
                          BUYER,
                          MERCH,
                          TOTAL_MARKET_AMT )
                 VALUES ( 2000,
                          'Fashion',
                          1000,
                          606,
                          NULL );
   INSERT INTO DIVISION ( DIVISION,
                          DIV_NAME,
                          BUYER,
                          MERCH,
                          TOTAL_MARKET_AMT )
                 VALUES ( 3000,
                          'Hardlines',
                          1001,
                          800,
                          NULL );
   INSERT INTO DIVISION ( DIVISION,
                          DIV_NAME,
                          BUYER,
                          MERCH,
                          TOTAL_MARKET_AMT )
                 VALUES ( 1000,
                          'Grocery',
                          1002,
                          612,
                          NULL );
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.DIVISION',
                                            to_char(SQLCODE));
      return FALSE;
END DIVISION;
--------------------------------------------------------------------------------------------
FUNCTION GROUPS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 314, 'Paint*', 635, 835, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 315, 'Cosmetics', 635, 703, 2000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 100, 'Grocery', 305, 602, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 101, 'Dairy', 305, 603, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 102, 'Deli', 1000, 604, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 103, 'Fresh Meat', 1000, 604, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 104, 'Packaged Meat', 1001, 609, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 105, 'Frozen Meat', 1003, 605, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 106, 'Produce', 1003, 604, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 107, 'Health Beauty Care', 305, 607, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 108, 'Liq, Beer, Wine, Tob', 305, 601, 1000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 200, 'Womens Fashion*', 305, 606, 2000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 210, 'Mens Fashion*', 305, 606, 2000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 240, 'Jewelry*', 610, 760, 2000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 220, 'Children''s', 305, 606, 2000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 230, 'Footwear*', 305, 606, 2000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 300, 'Consumer Electronic*', 610, 810, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 301, 'Home Office*', 610, 815, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 302, 'Home Fashions*', 610, 820, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 303, 'Sporting Goods', 610, 825, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 304, 'Home Improvement', 610, 830, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 305, 'Hardware*', 610, 835, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 306, 'Pets', 610, 840, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 307, 'Automotive', 610, 845, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 308, 'Media*', 610, 850, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 309, 'Toys', 1000, 855, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 310, 'Optical', 1000, 860, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 311, 'Pharmacy*', 1003, 865, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 312, 'Craft and Hobby', 1003, 870, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 313, 'Cards and Party', 1002, 875, 3000);
   INSERT INTO GROUPS ( GROUP_NO, GROUP_NAME, BUYER, MERCH, DIVISION )
               VALUES ( 109, 'Fuel', 305, 612, 1000);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.GROUPS',
                                            to_char(SQLCODE));
      return FALSE;
END GROUPS;
--------------------------------------------------------------------------------------------
FUNCTION DEPT(O_error_message      IN OUT   VARCHAR2,
              I_default_tax_type   IN       VARCHAR2,
              I_vat_class_ind      IN       VARCHAR2)
RETURN BOOLEAN IS
   L_dept_vat_incl_ind     DEPS.DEPT_VAT_INCL_IND%TYPE;
   L_rms_async_id STOCK_LEDGER_INSERTS.RMS_ASYNC_ID%TYPE;


BEGIN
   if I_default_tax_type IN ('SVAT','GTAX') then
      if I_vat_class_ind = 'Y' then
         L_dept_vat_incl_ind := 'N';
      else
         L_dept_vat_incl_ind := 'Y';
      end if;
   else
      L_dept_vat_incl_ind := 'N';
   end if;

  --************************** DEPARTMENT = 1221 *

   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3040, 'Building Materials', 635, 830, 1, 0, 304, 20, 25, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3041, ' Paint*', 635, 830, 1, 0, 304, 35, 53.8462, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3042, 'Flooring', 635, 830, 1, 0, 304, 15, 17.6471, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3043, 'Lighting', 635, 830, 1, 0, 304, 35, 53.8462, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3044, 'Kitchen Hm Imprvmnt', 1000, 830, 1, 0, 304, 15, 17.6471, NULL, 'C', 'C', 1, 1
   ,L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3045, 'Bathroom', 635, 830, 1, 0, 304, 15, 17.6471, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3046, 'Windows, Doors', 1001, 830, 1, 0, 304, 30, 42.8571, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3047, 'Lawn, Garden', 1003, 830, 1, 0, 304, 45, 81.8182, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3050, 'Tools', 635, 835, 1, 0, 305, 55, 122.2222, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3051, 'Hardware', 635, 835, 1, 0, 305, 60, 150, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3052, 'Plumbing', 635, 835, 1, 0, 305, 45, 81.8182, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3053, 'Heating/Cooling', 635, 835, 1, 0, 305, 65, 185.7143, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3054, 'Hardware Services', 635, 835, 1, 0, 305, 10, 11.1111, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3060, 'Dogs', 610, 840, 1, 0, 306, 25, 33.3333, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3061, 'Cats', 610, 840, 1, 0, 306, 25, 33.3333, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3062, 'Birds', 610, 840, 1, 0, 306, 25, 33.3333, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3070, 'EngineParts/Filters', 610, 845, 1, 0, 307, 45, 81.8182, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3071, 'Exhaust,Emission', 610, 845, 1, 0, 307, 45, 81.8182, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3072, 'Tires, Wheel Care', 610, 845, 1, 0, 307, 45, 81.8182, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3080, 'Music', 610, 850, 1, 0, 308, 35, 53.8462, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3081, 'Books Reference*', 610, 850, 1, 0, 308, 45, 81.8182, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3084, 'Newspaper Magazine', 610, 850, 2, 1, 308, 10, 11.1111, NULL, 'C', 'R', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3082, 'Video/DVD', 610, 850, 1, 0, 308, 20, 25, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3083, 'Games/Software', 670, 850, 1, 0, 308, 20, 25, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3090, 'Plush', 670, 855, 1, 0, 309, 55, 122.2222, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3091, 'Dolls', 670, 855, 1, 0, 309, 55, 122.2222, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3092, 'Activity/Learning', 670, 855, 1, 0, 309, 35, 53.8462, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3100, 'Frames', 670, 860, 1, 0, 310, 70, 233.3333, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3102, 'Lenses', 670, 860, 1, 0, 310, 60, 150, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3103, 'Cleaning/Accessory', 670, 860, 1, 0, 310, 35, 53.8462, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3110, 'Personal Care*', 670, 865, 1, 0, 311, 30, 42.8571, NULL, 'C', 'C', 10, 10, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3111, 'Health Products', 670, 865, 1, 0, 311, 35, 53.8462, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3112, 'Vitamins,Nutrition', 670, 865, 1, 0, 311, 30, 42.8571, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3113, 'PrescriptionMeds', 670, 865, 1, 0, 311, 15, 17.6471, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3120, 'Art Supplies', 670, 870, 1, 0, 312, 45, 81.8182, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3121, 'Toys,Hobby', 670, 870, 1, 0, 312, 45, 81.8182, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3122, 'Seasonal', 305, 870, 1, 0, 312, 55, 122.2222, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3123, 'Floral', 305, 870, 1, 0, 312, 55, 122.2222, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3130, 'Party Supplies', 1003, 875, 1, 0, 313, 30, 42.8571, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3131, 'Gift Giving', 1001, 875, 1, 0, 313, 45, 81.8182, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3132, 'Cards,Stationary', 1001, 875, 2, 1, 313, 10, 11.1111, NULL, 'R', 'R', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2001, 'Women''s Casuals*', 1001, 606, 2, 0, 200, 65, 185.7143, NULL, 'R', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2002, 'Women''s Denim', 1001, 606, 2, 0, 200, 70, 233.3333, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2003, 'Women''s Classics*', 1001, 606, 2, 0, 200, 70, 233.3333, NULL, 'R', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2004, 'Women''s Accessories', 1001, 606, 2, 0, 200, 60, 150, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2101, 'Men''s Casuals*', 1001, 606, 2, 0, 210, 65, 185.7143, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2102, 'Men''s Classics', 1001, 606, 2, 0, 210, 70, 233.3333, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2201, 'Girls', 1003, 606, 2, 0, 220, 65, 185.7143, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2202, 'Boys', 1003, 606, 2, 0, 220, 65, 185.7143, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2301, 'Women''s Footwear*', 1001, 606, 2, 0, 230, 70, 233.3333, NULL, 'R', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2302, 'Men''s Footwear', 1002, 606, 2, 0, 230, 70, 233.3333, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2303, 'Children''s Footwear', 1001, 606, 2, 0, 230, 65, 185.7143, NULL, 'R', 'C', 1
   , 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2401, 'Fashion Jewelry', 1001, 606, 2, 0, 240, 70, 233.3333, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2402, 'Fine Jewelry', 610, 606, 2, 1, 240, 80, 400, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1107, 'Peant Bttr,Preserv*', 305, 610, 2, 0, 100, 10.7143, 12, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1108, 'Yogurt*', 307, 603, 2, 0, 101, 23.0769, 30, NULL, 'C', 'C', 42, 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1109, 'Full Service Cheese', 307, 603, 2, 0, 102, 31.0345, 45, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1110, 'Self Service Cheese', 307, 603, 2, 0, 102, 25.9259, 35, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1111, 'Self Srv Frsh Beef*', 307, 604, 2, 0, 103, 28.5714, 40, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1112, 'Self Srve Fresh Pork', 307, 604, 2, 0, 103, 28.5714, 40, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1113, 'Fresh Fruit*', 307, 604, 2, 0, 106, 31.0345, 45, NULL, 'C', 'C', 4, 42, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1114, 'Fresh Vegatables', 307, 604, 2, 0, 106, 31.0345, 45, NULL, 'C', 'C', 4, 42
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1115, 'Health,Beauty Care*', 305, 607, 2, 0, 107, 28.5714, 40, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1116, 'Analgesics', 305, 607, 2, 0, 107, 28.5714, 40, NULL, 'C', 'C', 42, 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1117, 'Cigarettes,Tobacco*', 305, 611, 2, 0, 108, 16.6667, 20, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1118, 'Beer', 305, 601, 2, 0, 108, 16.6667, 20, NULL, 'C', 'C', 42, 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1119, 'Gasoline*', 305, 612, 2, 0, 109, 9.0909, 10, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   111, 'Alcohol and Liqueurs', 305, 601, 2, 0, 108, 16.6667, 20, NULL, 'R', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   112, 'Finest Superior Wine', 305, 601, 2, 0, 108, 16.6667, 20, NULL, 'R', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3055, 'Raw Materials*', 635, 835, 2, 0, 305, 20, 25, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   9000, 'Concession', NULL, NULL, 2, 2, 100, 50, 100, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1211, 'Sub-Primals*', NULL, NULL, 2, 0, 103, 32, 47.0588, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   2028, 'Cosmetics', 1000, 703, 2, 2, 315, 80, 400, NULL, 'R', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1100, 'Soft Drinks*', 305, 610, 2, 0, 100, 16.6667, 20, NULL, 'C', 'C', 42, 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1102, ' Detergents*', 305, 608, 2, 0, 100, 16.6667, 20, NULL, 'C', 'C', 42, 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1103, 'Cereal -Ready to Eat', 305, 602, 2, 0, 100, 13.0435, 15, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1104, 'Canned,Glass Fruit*', 305, 610, 2, 0, 100, 16.6667, 20, NULL, 'C', 'C', 42
   , 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1105, 'Salty Snacks*', 305, 610, 2, 0, 100, 20, 25, NULL, 'C', 'C', 42, 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   1106, 'Spices*', 305, 602, 2, 0, 100, 23.0769, 30, NULL, 'C', 'C', 42, 5, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3003, 'Home Video', 610, 810, 1, 0, 300, 15, 17.6471, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3001, 'Home Theater*', 610, 810, 1, 0, 300, 39.3939, 65, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3002, 'Home Audio', 610, 810, 1, 0, 300, 13.0435, 15, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3004, 'Con Elec Accessory*', 610, 810, 1, 0, 300, 30, 42.8571, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3011, 'Office Furniture*', 615, 815, 1, 0, 301, 60, 150, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3012, 'Technology', 615, 815, 1, 0, 301, 20, 25, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3013, 'Home Office Service', 615, 815, 1, 0, 301, 70, 233.3333, NULL, 'C', 'C', 1
   , 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3020, 'Bedding', 305, 820, 1, 0, 302, 65, 185.7143, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3010, 'HomeOffice Supplies', 635, 815, 1, 0, 301, 45, 81.8182, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3021, 'Kitchen Hm Fashions', 610, 820, 2, 0, 302, 45, 81.8182, NULL, 'C', 'C', 1, 1
   , L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3022, 'Bath', 610, 820, 1, 0, 302, 65, 185.7143, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3023, 'Dining', 610, 820, 1, 0, 302, 55, 122.2222, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3024, 'Home Decor''*', 610, 820, 1, 0, 302, 45, 81.8182, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3025, 'Storage  Cleaning', 610, 820, 1, 0, 302, 60, 150, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3030, 'Golf', 635, 825, 1, 0, 303, 35, 53.8462, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (
   3031, 'Tennis', 635, 825, 1, 0, 303, 35, 53.8462, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);
   INSERT INTO DEPS ( DEPT, DEPT_NAME, BUYER, MERCH, PROFIT_CALC_TYPE, PURCHASE_TYPE, GROUP_NO,
   BUD_INT, BUD_MKUP, TOTAL_MARKET_AMT, MARKUP_CALC_TYPE, OTB_CALC_TYPE, MAX_AVG_COUNTER,
   AVG_TOLERANCE_PCT, DEPT_VAT_INCL_IND ) VALUES (3032, 'Fishing', 635, 825, 1, 0, 303, 55, 122.2222, NULL, 'C', 'C', 1, 1, L_dept_vat_incl_ind);

   ---
   if I_default_tax_type = 'SVAT' then
      insert into vat_deps (vat_region,
                            dept,
                            vat_type,
                            vat_code,
                            reverse_vat_ind)
                            (select 1000,
                             dept,
                             'B',
                             'S',
                             'N'
                             from deps);

   end if;
   ---
   L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
   insert into stock_ledger_inserts(type_code,
                                     dept,
                                     class,
                                     subclass,
                                     location,
                                     rms_async_id)
                                     (select 'D',
                                           dept,
                                           null,
                                           null,
                                           null,L_rms_async_id from deps);
   ---
  IF CORESVC_STKLEDGR_INSERTS_SQL.PROCESS_STKLEDGR_INSERTS(O_error_message,L_rms_async_id)=FALSE THEN
    RETURN false;
  END IF;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.DEPT',
                                            to_char(SQLCODE));
      return FALSE;
END DEPT;
--------------------------------------------------------------------------------------------
FUNCTION CLASS_SUBCLASS(O_error_message      IN OUT   VARCHAR2,
                        I_default_tax_type   IN       VARCHAR2,
                        I_vat_class_ind      IN       VARCHAR2)
RETURN BOOLEAN IS
   L_class_vat_ind     CLASS.CLASS_VAT_IND%TYPE;
   L_rms_async_id STOCK_LEDGER_INSERTS.RMS_ASYNC_ID%TYPE;
BEGIN
   if I_default_tax_type IN ('SVAT','GTAX') then
      if I_vat_class_ind = 'Y' then
         L_class_vat_ind := 'N';
      else
         L_class_vat_ind := 'Y';
      end if;
   else
      L_class_vat_ind := 'N';
   end if;

   --************************* Classes/Subclasses *******************

   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 1105, 2, 'Cheese Snacks', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 1105, 3, 'Potato Chips', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 1106, 1, 'Meat Tenderizers', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 1106, 2, 'Spices/Seasonings*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 1106, 3, 'Salt', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 1106, 4, 'Pepper', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 3001, 1, 'Speaker Systems', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 3001, 2, 'Receivers', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 3001, 3, 'DVD*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 3002, 1, 'CD Players', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND )
              VALUES ( 3002, 2, 'MP3 Players', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3002, 3, 'Mini Systems', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3003, 1, 'Televisions', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3003, 2, 'VCR''s', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3003, 3, 'Camcorders', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3003, 4, 'Cameras', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3004, 1, 'Headphones', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3004, 2, 'Remotes', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3004, 3, 'Cables', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3004, 4, 'Batteries*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3010, 1, 'Paper, Notbooks,Pads', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3010, 2, 'Writing Instruments', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3010, 3, 'Correction', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3010, 4, 'Binders', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1107, 1, 'Peanut Butter*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1107, 2, 'Jams/Jellies/Spreads', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1108, 1, 'Fruit@Bottm Lowfat*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1108, 2, 'Light Non Fat*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1109, 1, 'Bulk*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1110, 1, 'Blue/Gorgnzla/Roqfrt', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1110, 2, 'Cheddar Vlby Chees', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1111, 1, 'Beef Premium', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1111, 2, 'Beef Choice*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1111, 3, 'Beef Select', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1112, 1, 'Pork', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1112, 2, 'Case Ready Pork', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1113, 1, 'Apples*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1113, 2, 'Grapefruit*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1113, 3, 'Grapes', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1113, 4, 'Oranges*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1114, 1, 'Cabbage', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1114, 2, 'Carrots', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1114, 3, 'Corn', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1114, 4, 'Potatoes', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1115, 1, 'Lotions, Oils*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1115, 2, 'Bath*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1115, 3, 'Antiperspirants/Deod', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1115, 4, 'Colognes/Aftershave*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1116, 1, 'Children', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1116, 2, 'Adult', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1117, 1, 'Cigarettes*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1117, 2, 'Cigars', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1117, 3, 'Smokeless', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1118, 1, 'Domestic Beer', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1118, 2, 'Import Beer', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2001, 1, 'Bottoms', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2001, 2, 'Tops*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2002, 1, 'Tops', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2002, 2, 'Bottoms*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2003, 1, 'Tops*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2003, 2, 'Bottoms*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2003, 3, 'Dresses*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2003, 4, 'Suits', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2004, 1, 'Hosiery', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2004, 2, 'Handbags*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2004, 3, 'Scarves', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2101, 1, 'Tops', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2101, 2, 'Bottoms*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2102, 1, 'Tops', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2102, 2, 'Bottoms', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2102, 3, 'Jackets', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2102, 4, 'Accesories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2201, 1, 'Tops', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2201, 2, 'Bottoms', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2201, 3, 'Dresses', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2202, 1, 'Tops', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2202, 2, 'Bottoms', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2301, 1, 'Casual*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2301, 2, 'Dressy', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2302, 1, 'Casual', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2302, 2, 'Dressy', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2303, 1, 'Girls', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2303, 2, 'Boys', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2303, 3, 'Infants, Toddlers', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2401, 1, 'Watches', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2401, 2, 'Fashion Rings', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2401, 3, 'Fashion Necklaces', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2401, 4, 'Fashion Earrings', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2402, 1, 'Gold', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2402, 2, 'Diamond', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2402, 3, 'Gemstones', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3011, 1, 'Desks*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3011, 2, 'Charis', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3011, 3, 'Lamps', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3011, 4, 'Desk Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3012, 1, 'Office Equipment', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3012, 2, 'Cartridges, Toner', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3012, 3, 'Handhelds', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3012, 4, 'Peripherals', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3013, 1, 'Copy Services', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3013, 2, 'Engraving Services', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3013, 3, 'Signs, Banners', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3013, 4, 'Presentation Media', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3020, 1, 'Sheets', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3020, 2, 'Comforters', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3020, 3, 'Pillows', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3020, 4, 'Throws', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3020, 5, 'Bedding Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3021, 1, 'Bakewear', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3021, 2, 'Cookwear', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3021, 3, 'Tea Kettles', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3021, 4, 'Kitchen Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3021, 5, 'Kitchen Towels', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3022, 1, 'Towels', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3022, 2, 'Bath Ensembles', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3022, 3, 'Shower Curtains', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3022, 4, 'Bath Rugs', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3023, 1, 'Dinnerware', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3023, 2, 'Glassware', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3023, 3, 'Table Top', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3024, 1, 'Window Treatments', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3024, 2, 'Candles*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3024, 3, 'Frames*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3024, 4, 'Clocks', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3024, 5, 'Lamps', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3025, 1, 'Storage', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3025, 2, 'Cleaning', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3025, 3, 'Equipment', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3030, 1, 'Clubs', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3030, 2, 'Bags', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3030, 3, 'Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3031, 1, 'Rackets', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3031, 2, 'Balls', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3031, 3, 'Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3032, 1, 'Ice Fishing', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3032, 2, 'Fly Fishing', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3032, 3, 'Freshwater Fishing', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3032, 4, 'Fishing Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3041, 1, 'Interior Paint*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3041, 2, 'Exterior', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3041, 3, 'Stains and Varnishes', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3041, 4, 'Decking and Flooring', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3042, 1, 'Carpeting', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3042, 2, 'Hardwood', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3042, 3, 'Ceramic Tile', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3043, 1, 'Interior', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3043, 2, 'Exterior', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3043, 3, 'Lightbulbs', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3047, 1, 'Grills', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3047, 2, 'Fertilizer', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3047, 3, 'Landscape Solutions', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3050, 1, 'Hand Tools', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3050, 2, 'Tool Storage', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3050, 3, 'Power Tools', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3051, 1, 'Builders Hardware', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3051, 2, 'Cabinet Hardware', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3051, 3, 'Specialty Hardware', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3053, 1, 'Air Filters', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3053, 2, 'Registers', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3053, 3, 'Bath Fans', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3054, 1, 'Licenses', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3054, 2, 'Keys', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3060, 1, 'Feeding', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3060, 2, 'Grooming', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3060, 3, 'Containment', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3060, 4, 'Toys, Treats', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3061, 1, 'Feeding', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3061, 2, 'CleanUp', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3061, 3, 'Specialty', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3062, 1, 'Feeding', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3062, 2, 'Cages', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3062, 3, 'Specialty', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3070, 1, 'Air Filters/Cleaners', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3070, 2, 'Oil/Oil Filters', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3070, 3, 'Engine Heathers', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3072, 1, 'Tires', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3072, 2, 'Wheel Care', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3072, 3, 'Automotive Services', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3080, 1, 'Classical', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3080, 2, 'Pop, Rock', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3080, 3, 'Country', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3080, 4, 'Jazz', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3081, 1, 'Children''s', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3081, 2, ' Travel', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3081, 3, 'Education', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3081, 4, 'Reference*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3084, 1, 'Newspapers', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3084, 2, 'Magazines', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3082, 1, 'Drama', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3082, 2, 'Comedy', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3082, 3, 'ActionAdventure', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3083, 1, 'Software', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3083, 2, 'Video Games', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3083, 3, 'Gaming Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3090, 1, 'Characters', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3090, 2, 'Collectibles', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3090, 3, 'Animals', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3091, 1, 'Barbie', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3091, 2, 'Collectibles', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3091, 3, 'Dollhouses,Accessory', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3092, 1, 'Reading/Writing', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3092, 2, 'Counting, Math/Time', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3092, 3, 'Early Development', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3100, 1, 'Childrens', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3100, 2, 'Designer*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3100, 3, 'Specialty', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3100, 4, 'Sunglasses', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3102, 1, 'Polycarbonate', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3102, 2, 'Contacts', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3103, 1, 'Cleaning', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3103, 2, 'Cases', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3103, 3, 'Holders', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3110, 1, 'Oral Care*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3110, 2, 'Shaving', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3110, 3, 'Hair Care', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3111, 1, 'Stomach Care', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3111, 2, 'Pain Relief', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3111, 3, 'First Aid', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3120, 1, 'Drawing/Sketching', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3120, 2, 'Painting', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3120, 3, 'Calligraphy', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3121, 1, 'Science/Educational', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3121, 2, 'Modelling', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3121, 3, 'Collecting', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3122, 1, 'Easter', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3122, 2, 'Halloween', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3122, 3, 'Christmas', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3123, 1, 'Flowers/Greens', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3123, 2, 'Containers/Vases', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3123, 3, 'Floral Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3130, 1, 'Tableware', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3130, 2, 'Decorations', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3130, 3, 'Cutlery', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3131, 1, 'Gift Wrap', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3131, 2, 'Gift Bags', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3131, 3, 'Gift Accessories', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3132, 1, 'Single Cards', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3132, 2, 'Packaged Cards', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3132, 3, 'Stationary', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1119, 1119, 'Gasoline*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   111, 1, 'Whiskies/Bourbon', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   111, 2, 'Cognacs/Armagnacs', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   111, 3, 'Alchl/Fruit Alchl', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   112, 1, 'Finest Red Wines', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   112, 2, 'Finest Pink Wines', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   112, 3, 'Finest White Wines', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3051, 4, 'Doors', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3051, 5, 'Windows', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3051, 6, 'Stairs', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3055, 1, 'Concrete Bricks', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3055, 2, 'Sand', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1104, 3, 'Tomatoes', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   9000, 1, 'Quick Service Food', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   9000, 2, 'Equipment Rentals', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1211, 1, 'Sub-Primal', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1100, 4, 'Recycleable Bottles*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2028, 1, 'Mascara*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2028, 2, 'Foundation', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2028, 3, 'Shadows and Liners', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2028, 4, 'Blush and Bronzers', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   2028, 5, 'Lips', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   3055, 3, 'Rope*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1100, 1, 'Carbonatd Sft Drink*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1100, 2, 'Diet Card Sft Drink*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1100, 3, 'New Age Beverages', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1102, 1, 'All Purpose*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1102, 2, 'Laundry*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1103, 1, 'Cold', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1103, 2, 'Hot', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1104, 1, 'Peaches*', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1104, 2, 'Pears', L_class_vat_ind);
   INSERT INTO CLASS ( DEPT, CLASS, CLASS_NAME, CLASS_VAT_IND ) VALUES (
   1105, 1, 'Pretzels', L_class_vat_ind);
   --*********** Subclass ****************
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2101, 2, 4, 'Athletic Shorts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3110, 1, 5, 'Whitening Products');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 2, 6, 'Beef Thin Cuts*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2028, 1, 1, 'Mascara*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3025, 3, 3, 'Handheld Vacuums');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3030, 1, 1, 'Irons');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3030, 1, 2, 'Drivers/Woods');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3030, 1, 3, 'Putters');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3030, 1, 4, 'Wedges');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3030, 1, 5, 'Sets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3031, 2, 1, 'Jumbo');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3031, 2, 2, '3-Ball Cans*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3031, 2, 3, 'Multi-Packs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3032, 3, 1, 'Rods');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3032, 3, 2, 'Reels');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3032, 3, 3, 'Rod/Reel Combos');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3041, 1, 1, 'Flat Matte*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3041, 1, 2, 'Semi Gloss*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3041, 1, 3, 'High Gloss*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3042, 3, 1, 'Tile');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3042, 3, 2, 'Adhesives');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3042, 3, 3, 'Grout');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3043, 3, 1, 'General Purpose');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3043, 3, 2, 'Decorative');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3043, 3, 3, 'Flourescent');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3047, 1, 1, 'Gas');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3047, 1, 2, 'Charcoal');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3047, 1, 3, 'Smokers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3050, 3, 1, 'Drills');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3050, 3, 2, 'Joiners');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3050, 3, 3, 'Planers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 1, 1, 'Nails, Screws');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 1, 2, 'Bolts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 1, 3, 'Staples');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3053, 1, 1, 'Pleated Electrostc');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3053, 1, 2, 'Pleated Nonelectro');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3053, 1, 3, 'Fiberglass');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3060, 1, 1, 'Feeding, Bowls');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3060, 1, 2, 'Food');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3060, 1, 3, 'Storage, Scoops');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3061, 2, 1, 'Littering');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3061, 2, 2, 'Stain, Odor Control');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3061, 2, 3, 'Hair Pick-Up');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3062, 1, 1, 'Cages, Stands*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3062, 1, 2, 'Feeding Accessories');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3062, 1, 3, 'Perches, Ladders');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3070, 2, 1, 'Motor Oil');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3070, 2, 2, 'Fuel Filters');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3070, 2, 3, 'Oil Filters');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3072, 1, 1, 'Auto');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3072, 1, 2, 'Pick-Up');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3072, 1, 3, 'SUV');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3080, 2, 1, 'Compact Discs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3080, 2, 2, 'Cassettes');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3080, 2, 3, 'Singles');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3081, 2, 1, 'North America');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3081, 2, 2, 'Europe');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3081, 2, 3, 'Asia');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3081, 2, 4, 'Middle East');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3082, 2, 1, 'VHS');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3082, 2, 2, 'DVD');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3083, 2, 1, 'Playstation2');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3083, 2, 2, 'X-Box');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3083, 2, 3, 'Game Cube');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3084, 2, 1, 'Entertainment');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3084, 2, 2, 'Financial');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3084, 2, 3, 'Fashion');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3084, 2, 4, 'Trade');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3090, 1, 1, 'Disney');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3090, 1, 2, 'Sesame Street');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3090, 1, 3, 'Looney Toons');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3091, 1, 1, 'Barbie Dolls');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3091, 1, 2, 'Generation Girls');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3091, 1, 3, 'Collectibles');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3092, 3, 1, 'Sights, Sounds');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3092, 3, 2, 'Gyms, Playmats');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3092, 3, 3, 'Push, Pull');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3100, 2, 1, 'Womens*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3100, 2, 2, 'Unisex');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3100, 2, 3, 'Mens');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3102, 1, 1, 'Regular');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3102, 1, 2, 'Bi-Focal');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3102, 1, 3, 'Tri-Focal');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3103, 2, 1, 'Hard');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3103, 2, 2, 'Soft');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3103, 2, 3, 'Designer');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3110, 1, 1, 'Toothpaste*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3110, 1, 2, 'Toothbrushes');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3110, 1, 3, 'Denture Care');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3110, 1, 4, 'Floss, Accessories*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3111, 2, 1, 'Analgesic Rubs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3111, 2, 2, 'Aspirins');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3111, 2, 3, 'Non-Aspirins');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3120, 1, 1, 'Charcoal Pencils');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3120, 1, 2, 'Watercolor Pencils');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3120, 1, 3, 'Pastels');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3121, 1, 1, 'Magic');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3121, 1, 2, 'Biology');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3121, 1, 3, 'Chemistry');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3122, 3, 1, 'Table Top');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3122, 3, 2, 'Ornaments');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3122, 3, 3, 'Wall Hangings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3123, 1, 1, 'Dried');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3123, 1, 2, 'Silk');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3123, 1, 3, 'Live');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3130, 2, 1, 'Balloons');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3130, 2, 2, 'Cut Outs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3130, 2, 3, 'Table Top');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3130, 2, 4, 'Themed Sets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3131, 2, 1, 'Themed');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3131, 2, 2, 'Solids');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3131, 2, 3, 'Decorative');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3132, 1, 1, 'Birthday');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3132, 1, 2, 'Anniversary');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3132, 1, 3, 'Wedding');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3031, 1, 1, 'Performance');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1119, 1119, 1119, 'Gasoline*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3047, 3, 1, 'Pool Supplies');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 1, 1, 'Blended under 12 yrs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 1, 2, 'Blended above 12 yrs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 1, 3, 'Scotch Pur Malt');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 1, 4, 'Bourbons');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 1, 5, 'Irish Whiskies');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 1, 6, 'Canadian Whiskies');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 1, 7, 'Other Whiskies');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 2, 1, 'Armagnacs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 2, 2, 'Cognacs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 3, 1, 'Marc/Fruit Alcohol');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 3, 2, 'Calvados');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   111, 3, 3, 'Others Alcohol');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 1, 1, 'Bordeaux');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 1, 2, 'Bourgogne');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 1, 3, 'Beaujolais');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 1, 4, 'Other French Wines');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 1, 5, 'Foreign Wines');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 1, 6, 'Loire');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 1, 7, 'Cote Du Rhone');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 2, 1, 'Bordeaux');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 2, 2, 'Bourgogne');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 2, 3, 'Beaujolais');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 2, 4, 'Other French Wines');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 2, 5, 'Foreign Wines');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 3, 1, 'Bordeaux');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 3, 2, 'Bourgogne');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 3, 3, 'Beaujolais');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 3, 4, 'Other French Wines');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   112, 3, 5, 'Foreign Wines');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 1, 'Wooden Main Access');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 2, 'Special Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 3, 'Armoured Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 4, 'PVC Garage Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 5, 'Wooden Garage Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 6, 'Metal Garage Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 7, 'Wooden Indoor Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 8, ' Indoor Plane Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 9, 'Indoor Isoplane Door');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 10, 'Indoor Moulded Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 4, 11, 'Indoor Special Doors');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 5, 1, 'Velux Roof Windows');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 5, 2, 'Classic Windows');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 5, 3, 'Glazed Bay Windows');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 5, 4, 'Special Form Windows');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 5, 5, 'Tailored Windows');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 5, 6, 'Other Windows');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 6, 1, 'Right Stair');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 6, 2, 'Turning Qtr Stair');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 6, 3, 'Wooden Step Ladder');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3051, 6, 4, 'Japanese Stairs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3055, 1, 1, 'Cellular Concrete');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3055, 1, 2, 'Concrete Brick 30cm');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3055, 1, 3, 'Concrete Brick 20cm');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3055, 2, 1, 'Sand by Weight');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3055, 2, 2, 'Sandbag');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 3, 3, 'Ketchup');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 3, 1, 'Tomatoes');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 3, 2, 'Sauce, Paste');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 2, 1, 'Taper');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 2, 2, 'Jar*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 2, 3, 'Pillar');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 2, 4, 'Seasonal*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3081, 4, 1, 'Dictionaries*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3081, 4, 2, 'Thesauruses');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3011, 1, 1, 'Wood*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3011, 1, 2, 'Laminate');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3011, 1, 3, 'Steel');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3055, 3, 1, 'Loose*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3055, 3, 2, 'Packaged');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3001, 3, 4, 'Warranties*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 3, 1, 'Wood');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 3, 2, 'Metallic*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 3, 3, 'Decorative');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 1, 1, 'Cola*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 1, 2, 'Root Beer');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 1, 3, 'Orange');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 2, 1, 'Cola*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 2, 2, 'Root Beer');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 2, 3, 'Orange');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 3, 1, 'Fruit Drinks');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 3, 2, 'Ready to Drink Tea');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1102, 1, 1, 'Liquid*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1102, 1, 2, 'Powder*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1102, 2, 1, 'Liquid*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1102, 2, 2, 'Powder*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1103, 1, 1, 'Bag');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1103, 1, 2, 'Box');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1103, 2, 1, 'Bag');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1103, 2, 2, 'Box');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 1, 1, 'Diced*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 1, 2, 'Halved*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 1, 3, 'Sliced*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 2, 1, 'Diced');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 2, 2, 'Halved');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1104, 2, 3, 'Sliced');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1105, 1, 1, 'Rings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1105, 1, 2, 'Sticks / Rods');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1105, 1, 3, 'Twists');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1105, 2, 1, 'Crunchy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1105, 2, 2, 'Puff');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1105, 3, 1, 'Bagged*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1105, 3, 2, 'Box');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1106, 1, 1, 'Marinades');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1106, 2, 1, 'Premium');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1106, 2, 2, 'Standard*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1106, 3, 1, 'Salt');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1106, 4, 1, 'Pepper');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1107, 1, 1, 'Standard*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1107, 1, 2, 'Natural');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1107, 2, 1, 'Jams');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1107, 2, 2, 'Jellies');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1107, 2, 3, 'Preserves');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1107, 2, 4, 'Fruit Spreads');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1108, 1, 1, 'Single Serving*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1108, 1, 2, 'Multi Serving*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1108, 2, 1, 'Single Serving*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1108, 2, 2, 'Multi Serving*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1109, 1, 1, 'Imported Natural');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1109, 1, 2, 'Imported Processed');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1109, 1, 3, 'Domestic Natural');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1109, 1, 4, 'Domestic Processed*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1110, 1, 1, 'Blue');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1110, 1, 2, 'Roquefort');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1110, 1, 3, 'Stilton');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1110, 1, 4, 'Gorgonzola');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1110, 2, 1, 'Cheddar');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1110, 2, 2, 'Co-Jack');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1110, 2, 3, 'Colby');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1110, 2, 4, 'English Cotswald');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 1, 1, 'Beef Chunks');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 1, 2, 'Beef Loins');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 1, 3, 'Beef Ribs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 1, 4, 'Beef Rounds');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 1, 5, 'Beef Ground');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 2, 1, 'Beef Chucks*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 2, 2, 'Beef Loins*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 2, 3, 'Beef Ribs*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 2, 4, 'Beef Rounds*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 2, 5, 'Beef Ground*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 3, 1, 'Beef Chucks');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 3, 2, 'Beef Loins');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 3, 3, 'Beef Ribs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 3, 4, 'Beef Rounds');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1111, 3, 5, 'Beef Ground');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1112, 1, 1, 'Pork Loins');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1112, 1, 2, 'Boneless Pork Loins');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1112, 1, 3, 'Pork Ribs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1112, 2, 1, 'Pork Loins');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1112, 2, 2, 'Boneless Pork Loins');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1112, 2, 3, 'Pork Ribs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 1, 1, 'Red Delicious');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 1, 2, 'Golden Delicious*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 1, 3, 'Granny Smith');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 2, 1, 'Regular');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3122, 1, 1, 'Baskets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3122, 1, 2, 'Candy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3122, 1, 3, 'Plush');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 3, 1, 'Red Delicious');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 2, 2, 'Casual Trousers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 3, 1, 'Suit Jackets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 3, 2, 'Sportcoats');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 4, 1, 'Ties');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 4, 2, 'Belts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 1, 1, 'Tees');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 1, 2, 'Vests');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 1, 3, 'Sweaters');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 1, 4, 'Knits and Pullovers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 2, 1, 'Capri''s');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 2, 2, 'Shorts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 2, 3, 'Jeans');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 3, 1, 'Dressy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2201, 3, 2, 'Everyday');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2202, 1, 1, 'Tees');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2202, 1, 2, 'Knits and Pullovers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2202, 1, 3, 'Sweaters');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2202, 2, 1, 'Shorts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2202, 2, 2, 'Jeans');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2202, 2, 3, 'Slacks');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2301, 1, 1, 'Athletic');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2301, 1, 2, 'Sandals');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2301, 1, 3, 'Boots');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2301, 1, 4, 'Loafers*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2301, 2, 1, 'Heels');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2301, 2, 2, 'Mules and Slides');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2301, 2, 3, 'Sandals');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2302, 1, 1, 'Athletic');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2302, 1, 2, 'Boots');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2302, 1, 3, 'Loafers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2302, 2, 1, 'Monk Straps');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2302, 2, 2, 'Oxfords');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2302, 2, 3, 'Loafers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 1, 1, 'Special Occasion');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 1, 2, 'Sandals');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 1, 3, 'Playtime');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 2, 1, 'Special Occasion');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 2, 2, 'Sandals');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 2, 3, 'Playtime');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 3, 1, 'First Steps');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 3, 2, 'Special Occasion');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 3, 3, 'Sandals');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2303, 3, 4, 'Playtime');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 1, 1, 'Men''s Watches');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 1, 2, 'Women''s Watches');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 2, 1, 'Sterling Rings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 2, 2, 'Faux Stone Rings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 3, 1, 'Sterling Neckalces');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 3, 2, 'Faux Stone Necklaces');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 3, 3, 'Cb Zirconia Necklace');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 4, 1, 'Sterling Earrings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 4, 2, 'Faux Stone Earrings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2401, 4, 3, 'Cb Zirconia Earrings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 1, 1, 'Gold Necklaces');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 1, 2, 'Gold Bracelets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 1, 3, 'Gold Rings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 1, 4, 'Gold Earrings');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 1, 5, 'Gd Chain by Gram');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 2, 1, 'Diamon Wedding Sets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 2, 2, 'Diamond Anniversary');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 2, 3, 'Diamond Cocktail');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 2, 4, 'Diamond Necklaces');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 2, 5, 'Diamond Bracelets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 3, 1, 'Rubies');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 3, 2, 'Sapphires');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 3, 3, 'Emeralds');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 3, 4, 'Tanzanite');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 3, 5, 'Opals');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 3, 6, 'Garnets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2402, 3, 7, 'Pearls');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3001, 3, 1, 'Single Disc*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3001, 3, 2, 'Multi Disc');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3001, 3, 3, 'Portable');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3002, 1, 1, 'Single Disc');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3002, 1, 2, 'Multi Disc');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3002, 1, 3, 'Read/Writable');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3003, 4, 1, 'Professional');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3003, 4, 2, 'Recreational');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3003, 4, 3, 'Disposable');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3004, 4, 1, 'General Purpose*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3004, 4, 2, 'Camera');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3004, 4, 3, 'Specialty');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3010, 2, 1, 'Ballpoint Pens');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3010, 2, 2, 'Rollerball Pens');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3010, 2, 3, 'Pen Refills');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3010, 2, 4, 'Markers/Highlighters');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3010, 2, 5, 'Pencils/Sharpeners');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3010, 2, 6, 'Correction Supplies');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3010, 2, 7, 'Crayons,Paints,Brush');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3011, 3, 1, 'Desk Lamps*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3011, 3, 2, 'Floor Lamps');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3011, 3, 3, 'Decorative');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3012, 1, 1, 'Printers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3012, 1, 2, 'Facsimile Machines');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3012, 1, 3, 'All-in-One');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3012, 1, 4, 'Desktop Computers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3012, 1, 5, 'Copiers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3020, 1, 1, 'Solids');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3020, 1, 2, 'Prints');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3020, 1, 3, 'Coordinates');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3020, 1, 4, 'Sets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3021, 3, 1, 'Enamel');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3021, 3, 2, 'Stainless Steel');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3021, 3, 3, 'Two-Tone');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3022, 1, 1, 'Beach/Character');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3022, 1, 2, 'Solids');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3022, 1, 3, 'Specialty/Embellish');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3022, 1, 4, 'Printed');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3023, 2, 1, 'Everyday');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3023, 2, 2, 'Barware');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3023, 2, 3, 'Stemware');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 4, 1, 'Wall Clocks');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 4, 2, 'Table Clocks*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3024, 4, 3, 'Specialty Clocks');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3025, 3, 1, 'Upright Vacuums');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3025, 3, 2, 'Canister Vacuums');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   9000, 1, 1, 'Coffee Shops');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   9000, 1, 2, 'Food');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 4, 1, 'Clear Glass*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 4, 2, 'Green Glass');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1100, 4, 3, 'Brown Glass');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1211, 1, 1, 'Sub-Primal');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1102, 2, 3, 'Other*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 3, 2, 'Black');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 3, 3, 'Green');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 4, 1, 'Naval');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 4, 2, 'Temple');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 4, 3, 'Valencia');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 1, 1, 'Green');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 1, 2, 'Red');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 2, 1, 'Green Top');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 2, 2, 'Mini');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 3, 1, 'In Husk');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 3, 2, 'Out of Husk');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 4, 1, 'Red Potatoes');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 4, 2, 'White Potatoes');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1114, 4, 3, 'Bakers/Russets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 1, 1, 'Economy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 1, 2, 'Standard*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 1, 3, 'Premium');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 2, 1, 'Economy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 2, 2, 'Standard*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 2, 3, 'Premium');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 3, 1, 'Economy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 3, 2, 'Standard*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 3, 3, 'Premium');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 4, 1, 'Economy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 4, 2, 'Standard*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1115, 4, 3, 'Premium');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1116, 1, 1, 'Acetaminophen');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1116, 1, 2, 'Ibuprofen');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1116, 1, 3, 'Aspirin');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1116, 2, 1, 'Acetaminophen');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1116, 2, 2, 'Ibuprofen');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1116, 2, 3, 'Aspirin');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1117, 1, 1, 'Premium*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1117, 1, 2, 'Economy*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1117, 1, 3, 'Generic');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1118, 1, 1, 'Economy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1118, 1, 2, 'Standard');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1118, 1, 3, 'Premium');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1118, 2, 1, 'Economy');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1118, 2, 2, 'Standard');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1118, 2, 3, 'Premium');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   3062, 2, 1, 'Cages/Stands');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2001, 1, 1, 'Trousers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2001, 1, 2, 'Capri''s');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2001, 1, 3, 'Skirts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2001, 2, 1, 'Sweaters');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2001, 2, 2, 'Tees*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2001, 2, 3, 'Blouses');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2001, 2, 4, 'Jackets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2001, 2, 5, 'Knit Tops');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2002, 1, 1, 'Shirts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2002, 1, 2, 'Jackets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2002, 2, 1, 'Jeans*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2002, 2, 2, 'Skirts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2002, 2, 3, 'Shorts*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 1, 1, 'Blouses');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   1113, 2, 2, 'Varietal');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 1, 2, 'Sweaters*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 1, 3, 'Blazers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 2, 1, 'Trousers*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 2, 2, 'Skirts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 3, 1, 'Formal*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 3, 2, 'Casual');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 4, 1, 'Classics');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2003, 4, 2, 'Fashion');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 1, 1, 'Knee Highs');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 1, 2, 'Tights');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 1, 3, 'Socks');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 2, 1, 'Classics');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 2, 2, 'Totes*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 2, 3, 'Wallets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 3, 1, 'Wool');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 3, 2, 'Silk');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2004, 3, 3, 'Cashmere');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2101, 1, 1, 'Knit Shirts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2101, 1, 2, 'Tees');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2101, 1, 3, 'Jackets');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2101, 2, 1, 'Shorts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2101, 2, 2, 'Slacks');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2101, 2, 3, 'Denim*');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 1, 1, 'Dress Shirts');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 1, 2, 'Sweaters');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 1, 3, 'Polos and Pullovers');
   INSERT INTO SUBCLASS ( DEPT, CLASS, SUBCLASS, SUB_NAME ) VALUES (
   2102, 2, 1, 'Suit Trousers');

   ---
   -- Create stock ledger inserts records.
   ---
   L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
   insert into stock_ledger_inserts(type_code,
                                    dept,
                                    class,
                                    subclass,
                                    location,
                                    rms_async_id)
                             select 'B',
                                    dept,
                                    class,
                                    subclass,
                                    NULL,
                                    L_rms_async_id
                               from subclass;
  IF CORESVC_STKLEDGR_INSERTS_SQL.PROCESS_STKLEDGR_INSERTS(O_error_message,L_rms_async_id)=FALSE THEN
    RETURN false;
  END IF;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.CLASS_SUBCLASS',
                                            to_char(SQLCODE));
      return FALSE;
END CLASS_SUBCLASS;
--------------------------------------------------------------------------------------------
FUNCTION RPM_MERCH_HIER(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

   L_zone_group_id    rpm_zone_group.zone_group_id%type := null;
   L_zone_id          rpm_zone.zone_id%type             := null;

BEGIN

   --------- ZONE GROUP INSERT -----------------------------------
   select rpm_zone_group_seq.nextval into L_zone_group_id from dual;

   INSERT INTO RPM_ZONE_GROUP ( ZONE_GROUP_ID, ZONE_GROUP_DISPLAY_ID, NAME,
   LOCK_VERSION ) VALUES (
   L_zone_group_id, 1, 'All Stores Initial Price Set', 1);

   --------- ZONE GROUP TYPE INSERT -----------------------------------
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 2, 0);

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   1, L_zone_group_id, 'USD locations', 'USD', 1, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1131, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1141, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1151, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1211, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1221, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1231, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1241, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1311, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1321, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1331, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1341, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1411, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1421, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1511, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1521, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1531, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3000, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   2, L_zone_group_id, 'CAD locations', 'CAD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3112, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3211, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3212, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3311, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   3, L_zone_group_id, 'AUD locations', 'AUD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5141, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5151, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5161, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 6000, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 6050, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) select rpm_zone_seq.nextval,
   4, L_zone_group_id, 'EUR locations', 'EUR', 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) select rpm_zone_seq.nextval,
   5, L_zone_group_id, 'GBP locations', 'GBP', 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) select rpm_zone_seq.nextval,
   6, L_zone_group_id, 'HKD locations', 'HKD', 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) select rpm_zone_seq.nextval,
   7, L_zone_group_id, 'JPY locations', 'JPY', 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) select rpm_zone_seq.nextval,
   8, L_zone_group_id, 'MXN locations', 'MXN', 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   9, L_zone_group_id, 'NZD locations', 'NZD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5122, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5181, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) select rpm_zone_seq.nextval,
   10, L_zone_group_id, 'SGD locations', 'SGD', 0, 0 from dual;

   --------- MERCH RETAIL DEF INSERT -------------------
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 9000, NULL, NULL, L_zone_group_id, NULL, 1, 50, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1100, NULL, NULL, L_zone_group_id, NULL, 0, 20, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1102, NULL, NULL, L_zone_group_id, NULL, 0, 20, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1103, NULL, NULL, L_zone_group_id, NULL, 0, 15, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1104, NULL, NULL, L_zone_group_id, NULL, 0, 20, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1105, NULL, NULL, L_zone_group_id, NULL, 0, 30, 1 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1106, NULL, NULL, L_zone_group_id, NULL, 0, 30, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3003, NULL, NULL, L_zone_group_id, NULL, 0, 17.65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3001, NULL, NULL, L_zone_group_id, NULL, 0, 65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3002, NULL, NULL, L_zone_group_id, NULL, 0, 15, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3004, NULL, NULL, L_zone_group_id, NULL, 0, 42.86, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3011, NULL, NULL, L_zone_group_id, NULL, 0, 150, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3012, NULL, NULL, L_zone_group_id, NULL, 0, 25, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3013, NULL, NULL, L_zone_group_id, NULL, 0, 233.33, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3020, NULL, NULL, L_zone_group_id, NULL, 0, 185.71, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3010, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3021, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3022, NULL, NULL, L_zone_group_id, NULL, 0, 185.71, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3023, NULL, NULL, L_zone_group_id, NULL, 0, 122.22, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3024, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3025, NULL, NULL, L_zone_group_id, NULL, 0, 150, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3030, NULL, NULL, L_zone_group_id, NULL, 0, 53.85, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3031, NULL, NULL, L_zone_group_id, NULL, 0, 53.85, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3032, NULL, NULL, L_zone_group_id, NULL, 0, 122.22, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3040, NULL, NULL, L_zone_group_id, NULL, 0, 25, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3041, NULL, NULL, L_zone_group_id, NULL, 0, 53.85, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3042, NULL, NULL, L_zone_group_id, NULL, 0, 17.65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3043, NULL, NULL, L_zone_group_id, NULL, 0, 53.85, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3044, NULL, NULL, L_zone_group_id, NULL, 0, 17.65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3045, NULL, NULL, L_zone_group_id, NULL, 0, 17.65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3046, NULL, NULL, L_zone_group_id, NULL, 0, 42.86, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3047, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3050, NULL, NULL, L_zone_group_id, NULL, 0, 122.22, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3051, NULL, NULL, L_zone_group_id, NULL, 0, 150, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3052, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3053, NULL, NULL, L_zone_group_id, NULL, 0, 185.71, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3054, NULL, NULL, L_zone_group_id, NULL, 0, 11.11, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3060, NULL, NULL, L_zone_group_id, NULL, 0, 33.33, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3061, NULL, NULL, L_zone_group_id, NULL, 0, 33.33, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3062, NULL, NULL, L_zone_group_id, NULL, 0, 33.33, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3070, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3071, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3072, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3080, NULL, NULL, L_zone_group_id, NULL, 0, 53.85, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3081, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3084, NULL, NULL, L_zone_group_id, NULL, 0, 11.11, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3082, NULL, NULL, L_zone_group_id, NULL, 0, 25, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3083, NULL, NULL, L_zone_group_id, NULL, 0, 25, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3090, NULL, NULL, L_zone_group_id, NULL, 0, 122.22, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3091, NULL, NULL, L_zone_group_id, NULL, 0, 122.22, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3092, NULL, NULL, L_zone_group_id, NULL, 0, 53.85, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3100, NULL, NULL, L_zone_group_id, NULL, 0, 233.33, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3102, NULL, NULL, L_zone_group_id, NULL, 0, 150, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3103, NULL, NULL, L_zone_group_id, NULL, 0, 53.85, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3110, NULL, NULL, L_zone_group_id, NULL, 0, 42.86, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3111, NULL, NULL, L_zone_group_id, NULL, 0, 53.85, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3112, NULL, NULL, L_zone_group_id, NULL, 0, 42.86, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3113, NULL, NULL, L_zone_group_id, NULL, 0, 17.65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3120, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3121, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3122, NULL, NULL, L_zone_group_id, NULL, 0, 122.22, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3123, NULL, NULL, L_zone_group_id, NULL, 0, 122.22, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3130, NULL, NULL, L_zone_group_id, NULL, 0, 42.86, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3131, NULL, NULL, L_zone_group_id, NULL, 0, 81.82, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3132, NULL, NULL, L_zone_group_id, NULL, 1, 10, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2001, NULL, NULL, L_zone_group_id, NULL, 1, 65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2002, NULL, NULL, L_zone_group_id, NULL, 1, 70, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2003, NULL, NULL, L_zone_group_id, NULL, 1, 70, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2004, NULL, NULL, L_zone_group_id, NULL, 1, 60, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2101, NULL, NULL, L_zone_group_id, NULL, 1, 65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2102, NULL, NULL, L_zone_group_id, NULL, 1, 70, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2201, NULL, NULL, L_zone_group_id, NULL, 1, 65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2202, NULL, NULL, L_zone_group_id, NULL, 1, 65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2301, NULL, NULL, L_zone_group_id, NULL, 1, 70, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2302, NULL, NULL, L_zone_group_id, NULL, 1, 70, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2303, NULL, NULL, L_zone_group_id, NULL, 1, 65, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2401, NULL, NULL, L_zone_group_id, NULL, 1, 70, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2402, NULL, NULL, L_zone_group_id, NULL, 1, 80, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1107, NULL, NULL, L_zone_group_id, NULL, 0, 12, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1108, NULL, NULL, L_zone_group_id, NULL, 0, 30, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1109, NULL, NULL, L_zone_group_id, NULL, 0, 45, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1110, NULL, NULL, L_zone_group_id, NULL, 0, 35, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1111, NULL, NULL, L_zone_group_id, NULL, 0, 40, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1112, NULL, NULL, L_zone_group_id, NULL, 0, 40, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1113, NULL, NULL, L_zone_group_id, NULL, 0, 45, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1114, NULL, NULL, L_zone_group_id, NULL, 0, 45, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1115, NULL, NULL, L_zone_group_id, NULL, 0, 40, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1116, NULL, NULL, L_zone_group_id, NULL, 0, 40, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1117, NULL, NULL, L_zone_group_id, NULL, 0, 20, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1118, NULL, NULL, L_zone_group_id, NULL, 0, 20, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 1119, NULL, NULL, L_zone_group_id, NULL, 1, 9.09, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 111, NULL, NULL, L_zone_group_id, NULL, 1, 16.67, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 112, NULL, NULL, L_zone_group_id, NULL, 1, 16.67, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 3055, NULL, NULL, L_zone_group_id, NULL, 1, 20, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   3, 2028, NULL, NULL, L_zone_group_id, NULL, 1, 80, 0 FROM DUAL;
   INSERT INTO RPM_MERCH_RETAIL_DEF ( MERCH_RETAIL_DEF_ID, MERCH_TYPE, DEPT, CLASS, SUBCLASS,
   REGULAR_ZONE_GROUP, CLEARANCE_ZONE_GROUP, MARKUP_CALC_TYPE, MARKUP_PERCENT,
   LOCK_VERSION ) SELECT RPM_MERCH_RETAIL_DEF_SEQ.nextval,
   1, 1211, 1, 1, L_zone_group_id, NULL, 1, 32, 0 FROM DUAL;


   --------- ZONE GROUP INSERT -----------------------------------
   select rpm_zone_group_seq.nextval into L_zone_group_id from dual;

   INSERT INTO RPM_ZONE_GROUP ( ZONE_GROUP_ID, ZONE_GROUP_DISPLAY_ID, NAME,
   LOCK_VERSION ) VALUES (
   L_zone_group_id, 11, 'US Core Stores', 0);

   --------- ZONE GROUP TYPE INSERT -----------------------------------
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 0, 0);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 1, 0);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 2, 0);

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   11, L_zone_group_id, 'US Core Stores', 'USD', 1, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1221, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1311, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1331, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1531, 0, 0 from dual;

   --------- ZONE GROUP INSERT -----------------------------------
   select rpm_zone_group_seq.nextval into L_zone_group_id from dual;

   INSERT INTO RPM_ZONE_GROUP ( ZONE_GROUP_ID, ZONE_GROUP_DISPLAY_ID, NAME,
   LOCK_VERSION ) VALUES (
   L_zone_group_id, 27, 'All Stores', 3);

   --------- ZONE GROUP TYPE INSERT -----------------------------------
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 1, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 2, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 0, NULL);

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   29, L_zone_group_id, 'US Stores', 'USD', 1, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1131, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1141, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1151, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1211, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1221, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1231, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1241, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1311, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1321, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1331, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1341, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1411, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1421, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1511, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1521, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1531, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3000, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   30, L_zone_group_id, 'Canadian Stores', 'CAD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3211, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3212, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3311, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3112, 0, 0 from dual;


   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   31, L_zone_group_id, 'Australian Stores', 'AUD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5141, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 6000, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5151, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5161, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 6050, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   32, L_zone_group_id, 'New Zealand Stores', 'NZD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5122, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5181, 0, 0 from dual;

   --------- ZONE GROUP INSERT -----------------------------------
   select rpm_zone_group_seq.nextval into L_zone_group_id from dual;

   INSERT INTO RPM_ZONE_GROUP ( ZONE_GROUP_ID, ZONE_GROUP_DISPLAY_ID, NAME,
   LOCK_VERSION ) VALUES (
   L_zone_group_id, 39, 'US Stores', 1);

   --------- ZONE GROUP TYPE INSERT -----------------------------------
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 1, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 2, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 0, NULL);

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   45, L_zone_group_id, 'NE Region', 'USD', 0, 1);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1211, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1221, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1231, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1241, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   46, L_zone_group_id, 'NW Region', 'USD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1411, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1421, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   47, L_zone_group_id, 'SE Region', 'USD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1131, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1141, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1151, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   48, L_zone_group_id, 'SW Region', 'USD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1511, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1521, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1531, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   49, L_zone_group_id, 'MW Region', 'USD', 1, 1);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1311, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1321, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1331, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1341, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   50, L_zone_group_id, 'Other Channels', 'USD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3000, 0, 0 from dual;

   --------- ZONE GROUP INSERT -----------------------------------
   select rpm_zone_group_seq.nextval into L_zone_group_id from dual;

   INSERT INTO RPM_ZONE_GROUP ( ZONE_GROUP_ID, ZONE_GROUP_DISPLAY_ID, NAME,
   LOCK_VERSION ) VALUES (
   L_zone_group_id, 40, 'Canadian Stores', 0);

   --------- ZONE GROUP TYPE INSERT -----------------------------------
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 1, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 2, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 0, NULL);

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   52, L_zone_group_id, 'Eastern Canada', 'CAD', 1, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3112, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   53, L_zone_group_id, 'Western Canada', 'CAD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3311, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   54, L_zone_group_id, 'Canadian Lakes', 'CAD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3211, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3212, 0, 0 from dual;


   --------- ZONE GROUP INSERT -----------------------------------
   select rpm_zone_group_seq.nextval into L_zone_group_id from dual;

   INSERT INTO RPM_ZONE_GROUP ( ZONE_GROUP_ID, ZONE_GROUP_DISPLAY_ID, NAME,
   LOCK_VERSION ) VALUES (
   L_zone_group_id, 41, 'Asia Pacific', 0);

   --------- ZONE GROUP TYPE INSERT -----------------------------------
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 1, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 2, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 0, NULL);

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   56, L_zone_group_id, 'Australia', 'AUD', 1, 0);

   --------- ZONE INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5141, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5151, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5161, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 6000, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 6050, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   57, L_zone_group_id, 'New Zealand', 'NZD', 0, 0);

   --------- ZONE LOCATION INSERT ----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5181, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 5122, 0, 0 from dual;

   --------- ZONE GROUP INSERT -----------------------------------
   select rpm_zone_group_seq.nextval into L_zone_group_id from dual;

   INSERT INTO RPM_ZONE_GROUP ( ZONE_GROUP_ID, ZONE_GROUP_DISPLAY_ID, NAME,
   LOCK_VERSION ) VALUES ( L_zone_group_id, 42, 'Cold Weather Stores', 0);

   --------- ZONE GROUP TYPE INSERT -----------------------------------
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 1, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 2, NULL);
   INSERT INTO RPM_ZONE_GROUP_TYPE ( ZONE_GROUP_ID, TYPE, LOCK_VERSION ) VALUES (
   L_zone_group_id, 0, NULL);

   --------- ZONE GROUP TYPE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   59, L_zone_group_id, 'CWUS', 'USD', 1, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1211, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1221, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1231, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1241, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1311, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 1331, 0, 0 from dual;

   --------- ZONE INSERT -----------------------------------
   select rpm_zone_seq.nextval into L_zone_id from dual;

   INSERT INTO RPM_ZONE ( ZONE_ID, ZONE_DISPLAY_ID, ZONE_GROUP_ID, NAME, CURRENCY_CODE, BASE_IND,
   LOCK_VERSION ) VALUES (L_zone_id,
   60, L_zone_group_id, 'CWCAD', 'CAD', 0, 0);

   --------- ZONE LOCATION INSERT -----------------------------------
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3211, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3212, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3311, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3111, 0, 0 from dual;
   INSERT INTO RPM_ZONE_LOCATION ( ZONE_LOCATION_ID, ZONE_ID, LOCATION, LOC_TYPE,
   LOCK_VERSION ) select rpm_zone_location_seq.nextval,
   L_zone_id, 3112, 0, 0 from dual;

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.RPM_MERCH_HIER',
                                            to_char(SQLCODE));
      return FALSE;
END RPM_MERCH_HIER;
--------------------------------------------------------------------------------------------
FUNCTION PO_TYPE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO PO_TYPE ( PO_TYPE, PO_TYPE_DESC )
                VALUES ( '5000', 'Manual Order');
   INSERT INTO PO_TYPE ( PO_TYPE, PO_TYPE_DESC )
                VALUES ( '6000', 'Emergency Buy');
   INSERT INTO PO_TYPE ( PO_TYPE, PO_TYPE_DESC )
                VALUES ( '1000', 'Valentines Day Sales');
   INSERT INTO PO_TYPE ( PO_TYPE, PO_TYPE_DESC )
                VALUES ( '2000', 'Christmas Sales');
   INSERT INTO PO_TYPE ( PO_TYPE, PO_TYPE_DESC )
                VALUES ( '3000', 'Fathers Day Sale');
   INSERT INTO PO_TYPE ( PO_TYPE, PO_TYPE_DESC )
                VALUES ( '4000', 'Back to School');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.PO_TYPE',
                                            to_char(SQLCODE));
      return FALSE;
END PO_TYPE;
--------------------------------------------------------------------------------------------
FUNCTION COST_ZONE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

   L_primary_zone_group       STORE.STORE%TYPE;

   cursor GET_STORE_FOR_PRIMARY is
   select min(store)
     from store;

BEGIN

   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1241, 'Cleveland', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 2, 'N. America Central', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1141, 'Nashville', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1151, 'Dallas', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1211, 'Boston', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1221, 'New York', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1231, 'Philadelphia*', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1311, 'Chicago*', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1321, 'Indianapolis', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1331, 'Minneapolis', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1341, 'St. Louis', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1411, 'Seattle*', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1421, 'Portland', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1511, 'Phoenix', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1521, 'Albuquerque', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1531, 'Los Angeles*', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 3000, 'Direct', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1111, 'Charlotte *', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 3211, 'Toronto', 'CAD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 3212, 'Ottawa', 'CAD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 3311, 'Vancouver', 'CAD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 1131, 'Jacksonville', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 5111, 'Sydney*', 'AUD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 5141, 'Melbourne', 'AUD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 6000, 'AUS Outlet', 'AUD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 5, 'AUS Warehouse', 'AUD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 5122, 'Auckland', 'NZD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 5151, 'Perth', 'AUD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 5161, 'Brisbane', 'AUD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 5181, 'Christchurch', 'NZD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 3111, 'Montreal*', 'CAD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 3112, 'Quebec', 'CAD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 9625516, 'PCB Test Store 1 - WH', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 9625517, 'PCB Test Store 2 - WH', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 9625518, 'PCB Store Test', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   1000, 9625519, 'Quebec', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 9000, 'Western US', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 9100, 'Central US', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 9200, 'Eastern US', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 9300, 'Western CN', 'CAD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 9400, 'Eastern CN', 'CAD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 9500, 'Australia', 'AUD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 9600, 'New Zealand', 'NZD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 3000, 'Direct', 'USD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES (
   2000, 6000, 'AUS Outlet', 'AUD', 'N');
   INSERT INTO COST_ZONE ( ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CURRENCY_CODE,
   BASE_COST_IND ) VALUES ( 1000, 6050, 'Oceania Direct', 'AUD', 'N');

   ---
   update cost_zone
      set base_cost_ind = 'Y'
    where zone_group_id = 1000
      and zone_id       = 2;

   update cost_zone
      set base_cost_ind = 'Y'
    where zone_group_id = 2000
      and zone_id       = 9100;

   ---

   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 15000, 'W', 2);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 15000, 'W', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 20001, 'W', 2);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 20001, 'W', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1231, 'S', 1231);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1241, 'S', 1241);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1311, 'S', 1311);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1321, 'S', 1321);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 2, 'W', 2);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 10001, 'W', 2);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 10002, 'W', 2);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 10003, 'W', 2);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1141, 'S', 1141);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1151, 'S', 1151);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1211, 'S', 1211);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1221, 'S', 1221);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1331, 'S', 1331);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1341, 'S', 1341);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1411, 'S', 1411);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1421, 'S', 1421);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1511, 'S', 1511);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1521, 'S', 1521);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1531, 'S', 1531);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 3000, 'S', 3000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1111, 'S', 1111);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 3211, 'S', 3211);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 3212, 'S', 3212);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 3311, 'S', 3311);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 1131, 'S', 1131);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 5111, 'S', 5111);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 5141, 'S', 5141);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 6000, 'S', 6000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 5, 'W', 5);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 50001, 'W', 5);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 50002, 'W', 5);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 50003, 'W', 5);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 5122, 'S', 5122);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 5151, 'S', 5151);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 5161, 'S', 5161);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 5181, 'S', 5181);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 3111, 'S', 3111);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 3112, 'S', 3112);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 9625516, 'S', 9625516);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 9625517, 'S', 9625517);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 9625518, 'S', 9625518);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 1000, 9625519, 'S', 9625519);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 2, 'W', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 10001, 'W', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 10002, 'W', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 10003, 'W', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 5, 'W', 9500);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 50001, 'W', 9500);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 50002, 'W', 9500);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 50003, 'W', 9500);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,ZONE_ID )
                            VALUES ( 2000, 1111, 'S', 9200);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1131, 'S', 9200);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1141, 'S', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1151, 'S', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1211, 'S', 9200);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1221, 'S', 9200);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1231, 'S', 9200);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1241, 'S', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1311, 'S', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1321, 'S', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1331, 'S', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1341, 'S', 9100);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1411, 'S', 9000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1421, 'S', 9000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1511, 'S', 9000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1521, 'S', 9000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 1531, 'S', 9000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 3111, 'S', 9400);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 3112, 'S', 9400);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 3211, 'S', 9400);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 3212, 'S', 9400);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 3311, 'S', 9300);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 5111, 'S', 9500);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 5122, 'S', 9600);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 5141, 'S', 9500);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 5151, 'S', 9500);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 5161, 'S', 9500);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 5181, 'S', 9600);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 3000, 'S', 3000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 6000, 'S', 6000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   1000, 6050, 'S', 6050);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 6050, 'S', 6000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 9625516, 'S', 9000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 9625517, 'S', 9000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 9625518, 'S', 9000);
   INSERT INTO COST_ZONE_GROUP_LOC ( ZONE_GROUP_ID, LOCATION, LOC_TYPE,
   ZONE_ID ) VALUES (
   2000, 9625519, 'S', 9000);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.COST_ZONE',
                                            to_char(SQLCODE));
      return FALSE;
END COST_ZONE;
--------------------------------------------------------------------------------------------
FUNCTION OUTLOC(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', 'LND', 'Dover, England', 'GBP', '110 Victoria Quay', NULL, 'Dover', NULL, 'GB'
   , 'SR13LD', NULL, 'Johnathon Ramsey', '0-1507-602-950', '0-1506-600-995', NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', 'MONT', 'Montreal, Canada', 'CAD', '3641 Prud''homme Ave', NULL, 'Montreal'
   , 'PQ', 'CA', 'H4A 3H6', NULL, 'Sigi Irwin', '805-375-0848', '805-335-0080', NULL
   , NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', 'VNCVR', 'Vancouver, Canada', 'CAD', 'PO Box 49203', '595 Burrard Street', 'Vancouver'
   , 'BC', 'CA', 'V7X 1K8', NULL, 'Carol DePaoli', '(250) 333-4144', '(250) 383-4142'
   , NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', 'PARIS', 'Paris, France', 'EUR', '38, cours Albert Ier', NULL, 'Paris', NULL
   , 'FR', '75008', NULL, 'Sandrine Falaise', '01 49 53 28 28', '01 49 53 28 20', NULL
   , NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '7', 'Guangzhou, China', 'USD', NULL, NULL, NULL, NULL, 'CN', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '8', 'Livorno, Italy', 'USD', NULL, NULL, NULL, NULL, 'IT', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '9', 'Hong Kong, Hong Kong', 'USD', NULL, NULL, NULL, NULL, 'HK', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '10', 'Shanghai, China', 'USD', NULL, NULL, NULL, NULL, 'CN', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '11', 'Yokohama, Japan', 'USD', NULL, NULL, NULL, NULL, 'JP', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '12', 'Istanbul, Turkey', 'USD', NULL, NULL, 'Istanbul', NULL, 'TR', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '480', 'Long Beach, California', 'USD', 'Skipjack Avenue', 'Port of Long Beach'
   , 'Long Beach', 'CA', 'US', '90802', NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '481', 'Oakland, California', 'USD', '530 Water Street', 'Port of Oakland', 'Oakland'
   , 'CA', 'US', '94607', NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '482', 'New Orleans', 'USD', 'Henry Clay Avenue', 'Port of New Orleans', 'New Orleans'
   , 'LA', 'US', '70160', NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '483', 'Portland, Oregon', 'USD', 'P O Bos 3529', 'Port of Portland', 'Portland'
   , 'OR', 'US', '97208', NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '484', 'New York, New York', 'USD', 'Port of New York', NULL, 'New York', 'NY'
   , 'US', '10012', NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '1', 'Singapore', 'USD', 'Singapore', NULL, NULL, NULL, 'SG', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '2', 'Kowloon, Hong Kong', 'USD', NULL, NULL, NULL, NULL, 'HK', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '3', 'Mumbai, India', 'USD', NULL, NULL, NULL, NULL, 'IN', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '4', 'Sao Paulo, Brazil', 'USD', NULL, NULL, NULL, NULL, 'BR', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', '1', 'Sao Paulo, Brazil', 'USD', NULL, NULL, NULL, NULL, 'BR', NULL, NULL, 'Belisario Franca'
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '485', 'Dover, UK', 'GBP', NULL, NULL, NULL, NULL, 'GB', NULL, NULL, NULL, NULL
   , NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '5', 'Dover, UK', 'GBP', NULL, NULL, NULL, NULL, 'GB', NULL, NULL, NULL, NULL
   , NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', '2301', 'Brownsville, TX', 'USD', NULL, NULL, 'Brownsville', 'TX', 'US', NULL
   , NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '486', 'Pusan, Korea', 'USD', NULL, NULL, NULL, NULL, 'KR', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '6', 'Pusan, Korea', 'USD', NULL, NULL, NULL, NULL, 'KR', NULL, NULL, NULL, NULL
   , NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', '4', 'Pusan, Korea', 'USD', NULL, NULL, NULL, NULL, 'KR', NULL, NULL, NULL, NULL
   , NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', 'TOKYO', 'Tokyo, Japan', 'JPY', '2-53-2,Nihonbashihamacho', 'Chuo-ku', 'Tokyo'
   , NULL, 'JP', '103-0007', NULL, ' Kyohei Tanaka', '81-3-3668-3618', '81-3-3668-3617'
   , NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', '2709', 'Long Beach, CA', 'USD', NULL, NULL, 'Long Beach', 'CA', 'US', NULL
   , NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', '2002', 'New Orleans, LA', 'USD', NULL, NULL, 'New Orleans', 'LA', 'US', NULL
   , NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', '1001', 'New York, NY', 'USD', NULL, NULL, 'New York', 'NY', 'US', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', '2811', 'Oakland, CA', 'USD', NULL, NULL, 'Oakland', 'CA', 'US', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'EP', '2904', 'Portland, OR', 'USD', NULL, NULL, 'Portland', 'OR', 'US', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '57035', 'Shang Hai', 'USD', NULL, NULL, 'Shang Hai', NULL, 'CN', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '1001', 'New York, NY', 'USD', NULL, NULL, 'Port of New York', 'NY', 'US', NULL
   , NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '2301', 'Brownsville, TX', 'USD', NULL, NULL, 'Port of Brownsville', 'TX', 'US'
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '4913', 'San Juan Int. Airport, PR', 'USD', NULL, NULL, 'San Juan', NULL, 'US'
   , 'PR', NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '47537', 'Livorno, Italy', 'USD', NULL, NULL, 'Livorno', NULL, 'IT', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '58270', 'Kowloon', 'USD', NULL, NULL, 'Kowloon', NULL, 'HK', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '57035', 'Shang Hai', 'USD', NULL, NULL, 'ShangHai', NULL, 'CN', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '57069', 'Xiamen, China', 'USD', NULL, NULL, 'Xiamen', NULL, 'CN', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '479', 'Punta Cana', 'DOP', '7899 Ocean Drive', NULL, 'Punta Cana', NULL, 'DO'
   , NULL, NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LL', '480', 'Long Beach, California', 'USD', NULL, NULL, NULL, NULL, 'US', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LL', '1001', 'New York, New York', 'USD', NULL, NULL, NULL, NULL, 'US', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LL', '481', 'Oakland, California', 'USD', NULL, NULL, NULL, NULL, 'US', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL )  VALUES (
   'LL', '485', 'Dover,UK', 'USD', NULL, NULL, NULL, NULL, 'US', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '90', 'Rotterdam - Holland', 'EUR', 'Unit 90', 'Portside', 'Rotterdam', NULL
   , 'NL', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '91', 'Hamburg', 'EUR', 'Unit 91', 'Portside', 'Hamburg', NULL, 'DE', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '92', 'Hull', 'GBP', 'Unit 92', 'Portside', 'Hull', NULL, 'GB', 'H1 8JH', NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '93', 'Le Havre', 'EUR', 'Unit 93', 'Portside', 'Le Havre', NULL, 'FR', '97854'
   , NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '94', 'Merseille', 'EUR', 'Unit 94', 'Portside', 'Merseille', NULL, 'FR', '09854'
   , NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '95', 'Southampton', 'GBP', 'Unit 95', 'Portside', 'Southampton', NULL, 'GB'
   , 'SO12 7VB', NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '90', 'Bombay', 'INR', 'Unit 90', 'Portside', 'Bombay', NULL, 'IN', '36789'
   , NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '92', 'Hamburg - Germany', 'EUR', NULL, NULL, NULL, NULL, 'DE', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '93', 'Le Havre - France', 'EUR', NULL, NULL, NULL, NULL, 'FR', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '94', 'Marseille - France', 'EUR', NULL, NULL, NULL, NULL, 'FR', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '95', 'Southampton - UK', 'GBP', NULL, NULL, NULL, NULL, 'GB', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'LP', '96', 'Cape Town - South Africa', 'ZAR', NULL, NULL, NULL, NULL, 'ZA', NULL
   , NULL, NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '97', 'Bombay - India', 'INR', NULL, NULL, NULL, NULL, 'IN', NULL, NULL, NULL
   , NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '98', 'Islamabad - Pakistan', 'PKR', NULL, NULL, NULL, NULL, 'PK', NULL, NULL
   , NULL, NULL, NULL, NULL, NULL);
   INSERT INTO OUTLOC ( OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_CURRENCY, OUTLOC_ADD1, OUTLOC_ADD2,
   OUTLOC_CITY, OUTLOC_STATE, OUTLOC_COUNTRY_ID, OUTLOC_POST, OUTLOC_VAT_REGION, CONTACT_NAME,
   CONTACT_PHONE, CONTACT_FAX, CONTACT_TELEX, CONTACT_EMAIL ) VALUES (
   'DP', '99', 'Bangladesh', 'BDT', NULL, NULL, NULL, NULL, 'BD', NULL, NULL, NULL, NULL
   , NULL, NULL, NULL);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.OUTLOC',
                                            to_char(SQLCODE));
      return FALSE;
END OUTLOC;
--------------------------------------------------------------------------------------------
FUNCTION RTK_ROLE_PRIVS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO RTK_ROLE_PRIVS ( ROLE,
                                ORD_APPR_AMT )
                       VALUES ( 'RETEK_DEVELOPER',
                                99999999999 );
   INSERT INTO RTK_ROLE_PRIVS ( ROLE,
                                ORD_APPR_AMT )
                       VALUES ( 'DEVELOPER',
                                99999999999 );
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.RTK_ROLE_PRIVS',
                                            to_char(SQLCODE));
      return FALSE;
END RTK_ROLE_PRIVS;
-----------------------------------------------------------------
FUNCTION FREIGHT_TYPE_SIZE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO FREIGHT_SIZE(FREIGHT_SIZE)
                     VALUES('20');
   INSERT INTO FREIGHT_SIZE(FREIGHT_SIZE)
                     VALUES('40');
   INSERT INTO FREIGHT_SIZE(FREIGHT_SIZE)
                     VALUES('45H');

   INSERT INTO FREIGHT_SIZE_TL(FREIGHT_SIZE,
                               FREIGHT_SIZE_DESC,
                               LANG,
                               ORIG_LANG_IND,
                               REVIEWED_IND,
                               CREATE_ID,
                               CREATE_DATETIME,
                               LAST_UPDATE_ID,
                               LAST_UPDATE_DATETIME)
                        VALUES('20',
                               '20 Foot Container',
                               GET_PRIMARY_LANG,
                               'Y',
                               'Y',
                               GET_USER,
                               SYSDATE,
                               GET_USER,
                               SYSDATE);
   INSERT INTO FREIGHT_SIZE_TL(FREIGHT_SIZE,
                               FREIGHT_SIZE_DESC,
                               LANG,
                               ORIG_LANG_IND,
                               REVIEWED_IND,
                               CREATE_ID,
                               CREATE_DATETIME,
                               LAST_UPDATE_ID,
                               LAST_UPDATE_DATETIME)
                        VALUES('40',
                               '40 Foot Container',
                               GET_PRIMARY_LANG,
                               'Y',
                               'Y',
                               GET_USER,
                               SYSDATE,
                               GET_USER,
                               SYSDATE);
   INSERT INTO FREIGHT_SIZE_TL(FREIGHT_SIZE,
                               FREIGHT_SIZE_DESC,
                               LANG,
                               ORIG_LANG_IND,
                               REVIEWED_IND,
                               CREATE_ID,
                               CREATE_DATETIME,
                               LAST_UPDATE_ID,
                               LAST_UPDATE_DATETIME)
                        VALUES('45H',
                               '45 Foot High Cube',
                               GET_PRIMARY_LANG,
                               'Y',
                               'Y',
                               GET_USER,
                               SYSDATE,
                               GET_USER,
                               SYSDATE);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.FREIGHT_TYPE_SIZE',
                                            to_char(SQLCODE));
      return FALSE;
END FREIGHT_TYPE_SIZE;
--------------------------------------------------------------------------------------------
FUNCTION DOC(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   101, 'Statement of Origin', 'REQ', 'Y', '46B', 6, 'Country of Origin');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   102, 'Fabric Content', 'REQ', 'Y', '46B', 7, 'Fabric Content');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   201, 'Safety Test Certification', 'REQ', 'N', '46B', 8, '2 copies required');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   1, 'Forwarder''s Cargo Receipt', 'REQ', 'Y', '46B', 1, 'Forwarders cargo receipt and one copy issued by union transport evidencing receipt of merchandise for shipment to the Cato Corp., 8100 Denmark Road, Charlotte, NC 28273, marked notify same, and stating that one complete set of original documents and 4 complete sets of copies of documents have been received from the beneficiary.');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   2, 'Inspection Certificate', 'AI', 'Y', '47B', 2, 'Inspection certificate signed by a rep of Excel Handbags.');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   3, 'Sender Instructions', 'SI', 'Y', '72', 3, 'Commercial Invoice in triplicate. Packing List in original plus one copy. Packing List must state that no solid wood packing material has been used.');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   5, 'Textile Declaration', 'BI', 'Y', '78', 5, 'Single or Multi-currency textile declaration of origin when required.');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   4, 'Bank Charges', 'CS', 'Y', '71B', 4, 'All banking charges except the Issuing Banks charges are for account of Beneficiary.');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   301, 'Certificate of Origin', 'REQ', 'N', '46B', 9, 'Certificate of Origin');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   302, 'Export License', 'REQ', 'N', '46B', 10, 'Export License');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES (
   303, 'Consolidation Centre', 'AI', 'N', '47B', 11, 'Consolidation Center');
   INSERT INTO DOC ( DOC_ID, DOC_DESC, DOC_TYPE, LC_IND, SWIFT_TAG, SEQ_NO,
   TEXT ) VALUES ( 304, 'Deconsolidation Centre', 'AI', 'N', '47B', 12, 'Deconsolidation Centre');
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.DOC',
                                            to_char(SQLCODE));
      return FALSE;
END DOC;
-------------------------------------------------------------------------------------------------------
FUNCTION POS_TENDER_TYPE(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

   L_prim_currency_code                    VARCHAR2(3) := NULL;

-- Define a record type to store all of the values will be inserted
TYPE pos_tender_type_head_rectype is RECORD (tender_type_id             NUMBER(6),
                                             tender_type_desc           VARCHAR2(40),
                                             tender_type_group          VARCHAR2(6),
                                             effective_date             DATE,                                          
                                             create_id                  VARCHAR2(30),
                                             create_date                DATE);

-- Define a table type based upon the record type defined above.
TYPE pos_tender_type_head_tabletype IS TABLE OF pos_tender_type_head_rectype
   INDEX BY BINARY_INTEGER;

pos_tender_type_head_list   pos_tender_type_head_tabletype;

   cursor C_GET_PRIM_CURR_CODE is
      select currency_code
        from system_options;

BEGIN

   open C_GET_PRIM_CURR_CODE;
   fetch C_GET_PRIM_CURR_CODE into L_prim_currency_code;
   close C_GET_PRIM_CURR_CODE;

   /* Fill the table.  If you need to add a new tender type ID, add a new record to the table below*/

   pos_tender_type_head_list(1).tender_type_id             := 1000;
   pos_tender_type_head_list(1).tender_type_desc           := 'Cash - Primary Currency';
   pos_tender_type_head_list(1).tender_type_group          := 'CASH';
   pos_tender_type_head_list(1).effective_date             := SYSDATE;
   pos_tender_type_head_list(1).create_id                  := GET_USER;
   pos_tender_type_head_list(1).create_date                := SYSDATE;

   pos_tender_type_head_list(2).tender_type_id             := 1010;
   pos_tender_type_head_list(2).tender_type_desc           := 'Cash Alternate Currency';
   pos_tender_type_head_list(2).tender_type_group          := 'CASH';
   pos_tender_type_head_list(2).effective_date             := SYSDATE;   
   pos_tender_type_head_list(2).create_id                  := GET_USER;
   pos_tender_type_head_list(2).create_date                := SYSDATE;
   

   pos_tender_type_head_list(3).tender_type_id             := 2000;
   pos_tender_type_head_list(3).tender_type_desc           := 'Personal Check';
   pos_tender_type_head_list(3).tender_type_group          := 'CHECK';
   pos_tender_type_head_list(3).effective_date             := SYSDATE;   
   pos_tender_type_head_list(3).create_id                  := GET_USER;
   pos_tender_type_head_list(3).create_date                := SYSDATE;
   

   pos_tender_type_head_list(4).tender_type_id             := 2010;
   pos_tender_type_head_list(4).tender_type_desc           := 'Cashier Check';
   pos_tender_type_head_list(4).tender_type_group          := 'CHECK';
   pos_tender_type_head_list(4).effective_date             := SYSDATE;   
   pos_tender_type_head_list(4).create_id                  := GET_USER;
   pos_tender_type_head_list(4).create_date                := SYSDATE;
   

   pos_tender_type_head_list(5).tender_type_id             := 2020;
   pos_tender_type_head_list(5).tender_type_desc           := 'Traveler Check';
   pos_tender_type_head_list(5).tender_type_group          := 'CHECK';
   pos_tender_type_head_list(5).effective_date             := SYSDATE;  
   pos_tender_type_head_list(5).create_id                  := GET_USER;
   pos_tender_type_head_list(5).create_date                := SYSDATE;
  

   pos_tender_type_head_list(6).tender_type_id             := 2030;
   pos_tender_type_head_list(6).tender_type_desc           := 'Electronic Check';
   pos_tender_type_head_list(6).tender_type_group          := 'CHECK';
   pos_tender_type_head_list(6).effective_date             := SYSDATE;   
   pos_tender_type_head_list(6).create_id                  := GET_USER;
   pos_tender_type_head_list(6).create_date                := SYSDATE;
   

   pos_tender_type_head_list(7).tender_type_id             := 2040;
   pos_tender_type_head_list(7).tender_type_desc           := 'Mail Bank Check';
   pos_tender_type_head_list(7).tender_type_group          := 'CHECK';
   pos_tender_type_head_list(7).effective_date             := SYSDATE;   
   pos_tender_type_head_list(7).create_id                  := GET_USER;
   pos_tender_type_head_list(7).create_date                := SYSDATE;
  

   pos_tender_type_head_list(8).tender_type_id             := 2050;
   pos_tender_type_head_list(8).tender_type_desc           := 'Personal Check Alternate Currency';
   pos_tender_type_head_list(8).tender_type_group          := 'CHECK';
   pos_tender_type_head_list(8).effective_date             := SYSDATE;   
   pos_tender_type_head_list(8).create_id                  := GET_USER;
   pos_tender_type_head_list(8).create_date                := SYSDATE;
   
   pos_tender_type_head_list(9).tender_type_id             := 2060;
   pos_tender_type_head_list(9).tender_type_desc           := 'Travelers Check Alternate Currency';
   pos_tender_type_head_list(9).tender_type_group          := 'CHECK';
   pos_tender_type_head_list(9).effective_date             := SYSDATE;  
   pos_tender_type_head_list(9).create_id                  := GET_USER;
   pos_tender_type_head_list(9).create_date                := SYSDATE;
   

   pos_tender_type_head_list(10).tender_type_id             := 3000;
   pos_tender_type_head_list(10).tender_type_desc           := 'Visa';
   pos_tender_type_head_list(10).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(10).effective_date             := SYSDATE;   
   pos_tender_type_head_list(10).create_id                  := GET_USER;
   pos_tender_type_head_list(10).create_date                := SYSDATE;
  

   pos_tender_type_head_list(11).tender_type_id             := 3010;
   pos_tender_type_head_list(11).tender_type_desc           := 'Mastercard';
   pos_tender_type_head_list(11).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(11).effective_date             := SYSDATE;   
   pos_tender_type_head_list(11).create_id                  := GET_USER;
   pos_tender_type_head_list(11).create_date                := SYSDATE;
  

   pos_tender_type_head_list(12).tender_type_id             := 3020;
   pos_tender_type_head_list(12).tender_type_desc           := 'American Express';
   pos_tender_type_head_list(12).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(12).effective_date             := SYSDATE;  
   pos_tender_type_head_list(12).create_id                  := GET_USER;
   pos_tender_type_head_list(12).create_date                := SYSDATE;
  

   pos_tender_type_head_list(13).tender_type_id             := 3030;
   pos_tender_type_head_list(13).tender_type_desc           := 'Discover';
   pos_tender_type_head_list(13).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(13).effective_date             := SYSDATE; 
   pos_tender_type_head_list(13).create_id                  := GET_USER;
   pos_tender_type_head_list(13).create_date                := SYSDATE;
  

   pos_tender_type_head_list(14).tender_type_id             := 3040;
   pos_tender_type_head_list(14).tender_type_desc           := 'Diners Club - N. America';
   pos_tender_type_head_list(14).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(14).effective_date             := SYSDATE;   
   pos_tender_type_head_list(14).create_id                  := GET_USER;
   pos_tender_type_head_list(14).create_date                := SYSDATE;
 

   pos_tender_type_head_list(15).tender_type_id             := 3045;
   pos_tender_type_head_list(15).tender_type_desc           := 'Diners Club - Non-N. America';
   pos_tender_type_head_list(15).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(15).effective_date             := SYSDATE;   
   pos_tender_type_head_list(15).create_id                  := GET_USER;
   pos_tender_type_head_list(15).create_date                := SYSDATE;
   

   pos_tender_type_head_list(16).tender_type_id             := 3049;
   pos_tender_type_head_list(16).tender_type_desc           := 'Diners Club - Ancillary';
   pos_tender_type_head_list(16).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(16).effective_date             := SYSDATE;   
   pos_tender_type_head_list(16).create_id                  := GET_USER;
   pos_tender_type_head_list(16).create_date                := SYSDATE;
   

   pos_tender_type_head_list(17).tender_type_id             := 3050;
   pos_tender_type_head_list(17).tender_type_desc           := 'WEX';
   pos_tender_type_head_list(17).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(17).effective_date             := SYSDATE;   
   pos_tender_type_head_list(17).create_id                  := GET_USER;
   pos_tender_type_head_list(17).create_date                := SYSDATE;


   pos_tender_type_head_list(18).tender_type_id             := 3060;
   pos_tender_type_head_list(18).tender_type_desc           := 'Voyageur';
   pos_tender_type_head_list(18).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(18).effective_date             := SYSDATE;  
   pos_tender_type_head_list(18).create_id                  := GET_USER;
   pos_tender_type_head_list(18).create_date                := SYSDATE;
  

   pos_tender_type_head_list(19).tender_type_id             := 3070;
   pos_tender_type_head_list(19).tender_type_desc           := 'Unocal';
   pos_tender_type_head_list(19).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(19).effective_date             := SYSDATE;   
   pos_tender_type_head_list(19).create_id                  := GET_USER;
   pos_tender_type_head_list(19).create_date                := SYSDATE;
   

   pos_tender_type_head_list(20).tender_type_id             := 3080;
   pos_tender_type_head_list(20).tender_type_desc           := 'enRoute';
   pos_tender_type_head_list(20).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(20).effective_date             := SYSDATE;   
   pos_tender_type_head_list(20).create_id                  := GET_USER;
   pos_tender_type_head_list(20).create_date                := SYSDATE;
  

   pos_tender_type_head_list(21).tender_type_id             := 3090;
   pos_tender_type_head_list(21).tender_type_desc           := 'Japanese Credit Bureau';
   pos_tender_type_head_list(21).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(21).effective_date             := SYSDATE;  
   pos_tender_type_head_list(21).create_id                  := GET_USER;
   pos_tender_type_head_list(21).create_date                := SYSDATE;
   

   pos_tender_type_head_list(22).tender_type_id             := 3100;
   pos_tender_type_head_list(22).tender_type_desc           := 'Australian Bank Card';
   pos_tender_type_head_list(22).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(22).effective_date             := SYSDATE;   
   pos_tender_type_head_list(22).create_id                  := GET_USER;
   pos_tender_type_head_list(22).create_date                := SYSDATE;
  

   pos_tender_type_head_list(23).tender_type_id             := 3110;
   pos_tender_type_head_list(23).tender_type_desc           := 'Carte Blanche - N. America';
   pos_tender_type_head_list(23).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(23).effective_date             := SYSDATE;  
   pos_tender_type_head_list(23).create_id                  := GET_USER;
   pos_tender_type_head_list(23).create_date                := SYSDATE;
  

   pos_tender_type_head_list(24).tender_type_id             := 3115;
   pos_tender_type_head_list(24).tender_type_desc           := 'Carte Blanche - Non-N. America';
   pos_tender_type_head_list(24).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(24).effective_date             := SYSDATE;  
   pos_tender_type_head_list(24).create_id                  := GET_USER;
   pos_tender_type_head_list(24).create_date                := SYSDATE;
   

   pos_tender_type_head_list(25).tender_type_id             := 3120;
   pos_tender_type_head_list(25).tender_type_desc           := 'House Card';
   pos_tender_type_head_list(25).tender_type_group          := 'CCARD';
   pos_tender_type_head_list(25).effective_date             := SYSDATE;   
   pos_tender_type_head_list(25).create_id                  := GET_USER;
   pos_tender_type_head_list(25).create_date                := SYSDATE;
   

   pos_tender_type_head_list(26).tender_type_id             := 4000;
   pos_tender_type_head_list(26).tender_type_desc           := 'Credit Voucher';
   pos_tender_type_head_list(26).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(26).effective_date             := SYSDATE;  
   pos_tender_type_head_list(26).create_id                  := GET_USER;
   pos_tender_type_head_list(26).create_date                := SYSDATE;
   

   pos_tender_type_head_list(27).tender_type_id             := 4010;
   pos_tender_type_head_list(27).tender_type_desc           := 'Manual Credit';
   pos_tender_type_head_list(27).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(27).effective_date             := SYSDATE;   
   pos_tender_type_head_list(27).create_id                  := GET_USER;
   pos_tender_type_head_list(27).create_date                := SYSDATE;
   

   pos_tender_type_head_list(28).tender_type_id             := 4020;
   pos_tender_type_head_list(28).tender_type_desc           := 'Manual Imprint';
   pos_tender_type_head_list(28).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(28).effective_date             := SYSDATE;   
   pos_tender_type_head_list(28).create_id                  := GET_USER;
   pos_tender_type_head_list(28).create_date                := SYSDATE;
   

   pos_tender_type_head_list(29).tender_type_id             := 4030;
   pos_tender_type_head_list(29).tender_type_desc           := 'Gift Certificate';
   pos_tender_type_head_list(29).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(29).effective_date             := SYSDATE;   
   pos_tender_type_head_list(29).create_id                  := GET_USER;
   pos_tender_type_head_list(29).create_date                := SYSDATE;
   

   pos_tender_type_head_list(30).tender_type_id             := 4040;
   pos_tender_type_head_list(30).tender_type_desc           := 'Gift Card';
   pos_tender_type_head_list(30).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(30).effective_date             := SYSDATE;   
   pos_tender_type_head_list(30).create_id                  := GET_USER;
   pos_tender_type_head_list(30).create_date                := SYSDATE;
   

   pos_tender_type_head_list(31).tender_type_id             := 4050;
   pos_tender_type_head_list(31).tender_type_desc           := 'Store Credit';
   pos_tender_type_head_list(31).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(31).effective_date             := SYSDATE;   
   pos_tender_type_head_list(31).create_id                  := GET_USER;
   pos_tender_type_head_list(31).create_date                := SYSDATE;
   

   pos_tender_type_head_list(32).tender_type_id             := 4060;
   pos_tender_type_head_list(32).tender_type_desc           := 'Mail Certificate';
   pos_tender_type_head_list(32).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(32).effective_date             := SYSDATE;   
   pos_tender_type_head_list(32).create_id                  := GET_USER;
   pos_tender_type_head_list(32).create_date                := SYSDATE;
   

   pos_tender_type_head_list(33).tender_type_id             := 4070;
   pos_tender_type_head_list(33).tender_type_desc           := 'Purchase Order';
   pos_tender_type_head_list(33).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(33).effective_date             := SYSDATE;   
   pos_tender_type_head_list(33).create_id                  := GET_USER;
   pos_tender_type_head_list(33).create_date                := SYSDATE;
   

   pos_tender_type_head_list(34).tender_type_id             := 4080;
   pos_tender_type_head_list(34).tender_type_desc           := 'PrePaid';
   pos_tender_type_head_list(34).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(34).effective_date             := SYSDATE;   
   pos_tender_type_head_list(34).create_id                  := GET_USER;
   pos_tender_type_head_list(34).create_date                := SYSDATE;
   

   pos_tender_type_head_list(35).tender_type_id             := 4090;
   pos_tender_type_head_list(35).tender_type_desc           := 'Store Credit Alternate Currency';
   pos_tender_type_head_list(35).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(35).effective_date             := SYSDATE;   
   pos_tender_type_head_list(35).create_id                  := GET_USER;
   pos_tender_type_head_list(35).create_date                := SYSDATE;
   

   pos_tender_type_head_list(36).tender_type_id             := 4100;
   pos_tender_type_head_list(36).tender_type_desc           := 'Gift Certificate Alternate Currency';
   pos_tender_type_head_list(36).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(36).effective_date             := SYSDATE;   
   pos_tender_type_head_list(36).create_id                  := GET_USER;
   pos_tender_type_head_list(36).create_date                := SYSDATE;
   

   pos_tender_type_head_list(37).tender_type_id             := 4110;
   pos_tender_type_head_list(37).tender_type_desc           := 'Customer Credit';
   pos_tender_type_head_list(37).tender_type_group          := 'VOUCH';
   pos_tender_type_head_list(37).effective_date             := SYSDATE;   
   pos_tender_type_head_list(37).create_id                  := GET_USER;
   pos_tender_type_head_list(37).create_date                := SYSDATE;
   

   pos_tender_type_head_list(38).tender_type_id             := 5000;
   pos_tender_type_head_list(38).tender_type_desc           := 'Manufacturers Coupons';
   pos_tender_type_head_list(38).tender_type_group          := 'COUPON';
   pos_tender_type_head_list(38).effective_date             := SYSDATE;   
   pos_tender_type_head_list(38).create_id                  := GET_USER;
   pos_tender_type_head_list(38).create_date                := SYSDATE;
   

   pos_tender_type_head_list(39).tender_type_id             := 6000;
   pos_tender_type_head_list(39).tender_type_desc           := 'Money Orders';
   pos_tender_type_head_list(39).tender_type_group          := 'MORDER';
   pos_tender_type_head_list(39).effective_date             := SYSDATE;   
   pos_tender_type_head_list(39).create_id                  := GET_USER;
   pos_tender_type_head_list(39).create_date                := SYSDATE;
   

   pos_tender_type_head_list(40).tender_type_id             := 7000;
   pos_tender_type_head_list(40).tender_type_desc           := 'Food Stamps';
   pos_tender_type_head_list(40).tender_type_group          := 'SOCASS';
   pos_tender_type_head_list(40).effective_date             := SYSDATE;   
   pos_tender_type_head_list(40).create_id                  := GET_USER;
   pos_tender_type_head_list(40).create_date                := SYSDATE;
   

   pos_tender_type_head_list(41).tender_type_id             := 7010;
   pos_tender_type_head_list(41).tender_type_desc           := 'Electronic Benefits System -EBS';
   pos_tender_type_head_list(41).tender_type_group          := 'SOCASS';
   pos_tender_type_head_list(41).effective_date             := SYSDATE;   
   pos_tender_type_head_list(41).create_id                  := GET_USER;
   pos_tender_type_head_list(41).create_date                := SYSDATE;
   

   pos_tender_type_head_list(42).tender_type_id             := 8000;
   pos_tender_type_head_list(42).tender_type_desc           := 'Debit Card';
   pos_tender_type_head_list(42).tender_type_group          := 'DCARD';
   pos_tender_type_head_list(42).effective_date             := SYSDATE;   
   pos_tender_type_head_list(42).create_id                  := GET_USER;
   pos_tender_type_head_list(42).create_date                := SYSDATE;

   pos_tender_type_head_list(43).tender_type_id             := 9000;
   pos_tender_type_head_list(43).tender_type_desc           := 'Fuel Drive Off';
   pos_tender_type_head_list(43).tender_type_group          := 'DRIVEO';
   pos_tender_type_head_list(43).effective_date             := SYSDATE;   
   pos_tender_type_head_list(43).create_id                  := GET_USER;
   pos_tender_type_head_list(43).create_date                := SYSDATE;
   

   pos_tender_type_head_list(44).tender_type_id             := 1020;
   pos_tender_type_head_list(44).tender_type_desc           := 'Rounding Tender';
   pos_tender_type_head_list(44).tender_type_group          := 'CASH';
   pos_tender_type_head_list(44).effective_date             := SYSDATE;   
   pos_tender_type_head_list(44).create_id                  := GET_USER;
   pos_tender_type_head_list(44).create_date                := SYSDATE;
   

   FOR i in 1..pos_tender_type_head_list.COUNT
   LOOP
      insert into pos_tender_type_head
               (tender_type_id,
                tender_type_desc,
                tender_type_group,
                effective_date,               
                create_id,
                create_date)
         values(pos_tender_type_head_list(i).tender_type_id,
                pos_tender_type_head_list(i).tender_type_desc,
                pos_tender_type_head_list(i).tender_type_group,
                pos_tender_type_head_list(i).effective_date,              
                pos_tender_type_head_list(i).create_id,
                pos_tender_type_head_list(i).create_date);
   END LOOP;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.POS_TENDER_TYPE',
                                            to_char(SQLCODE));
      return FALSE;
END POS_TENDER_TYPE;
---------------------------------------------------------------------------------------------
FUNCTION NON_MERCH_CODES(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   insert into non_merch_code_head ( non_merch_code, service_ind ) 
                            values ('A930', 'N');   
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ( 'A930', '1', 'Carrier Credit Allowance', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('B720', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('B720', '1', 'Coop Ad/Merch Allowance (Perf)', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('B820', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('B820', '1', 'Currency Adjustment', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('B860', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('B860', '1', 'Currency Adjustment', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('B870', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('B870', '1', 'Customs Broker Fee', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('B872', 'N');   
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('B872', '1', 'Customs Duty', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('B994', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                            values ('B994', '1', 'Declared Value for Customs', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('C000', 'N');   
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('C000', '1', 'Defective Allowance', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('C040', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('C040', '1', 'Deliver', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('C260', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('C260', '1', 'Discount - Incentive', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('C320', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('C320', '1', 'Display Allowances', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('C860', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('C860', '1', 'Expedited Shipments', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('D240', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('D240', '1', 'Freight', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('D360', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('D360', '1', 'Goods and Service Tax Charge', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('D900', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('D900', '1', 'Installation', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('E170', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('E170', '1', 'Labeling', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('E350', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('E350', '1', 'Letter Of Credit Processing', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('F155', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('F155', '1', 'Packaging', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('G740', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('G740', '1', 'Service Charge', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('G970', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('G970', '1', 'Small Order Charge', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('H090', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('H090', '1', 'Special Handling', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('H151', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('H151', '1', 'Special Services', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('H640', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('H640', '1', 'Tax - ExciseTax - Dest', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('H730', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('H730', '1', 'Tax - Local Sales Tax', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('H740', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('H740', '1', 'Tax - Sales and Use', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('H750', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('H750', '1', 'Tax - Sales Tax (St and Loc)', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('H770', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('H770', '1', 'Tax - State Tax', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('H800', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('H800', '1', 'Tax - Value Added Tax (VAT)', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);
   insert into non_merch_code_head ( non_merch_code, service_ind )
                            values ('I170', 'N');
   insert into non_merch_code_head_tl ( non_merch_code, lang, non_merch_code_desc,orig_lang_ind,reviewed_ind,create_id, create_datetime, last_update_id, last_update_datetime )
                               values ('I170', '1', 'Trade Discount', 'Y', 'N', GET_USER, SYSDATE, GET_USER, SYSDATE);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.NON_MERCH_CODES',
                                            to_char(SQLCODE));
      return FALSE;
END NON_MERCH_CODES;
--------------------------------------------------------------------------------------------
FUNCTION FIF_GL_SETUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
  --
  L_currency  currencies.currency_code%TYPE;
  --
  cursor C_GET_SYSTEM_OPTION is
      select currency_code
        from system_options;
  --
BEGIN
   --
   open C_GET_SYSTEM_OPTION;
   fetch C_GET_SYSTEM_OPTION into L_currency;
   close C_GET_SYSTEM_OPTION;
   --
   insert into FIF_GL_SETUP (set_of_books_id,
                             last_update_id,
                             sequence1_desc, sequence2_desc, sequence3_desc, sequence4_desc, sequence5_desc,
                             sequence6_desc, sequence7_desc, sequence8_desc, sequence9_desc, sequence10_desc,
                             category_id,
                             deliver_to_location_id,
                             destination_organization_id,
                             period_name,
                             set_of_books_desc,
                             currency_code)
                     VALUES (111111111111111,
                             123456,
                             'DummySeq1', 'DummySeq2', 'DummySeq3', 'DummySeq4', 'DummySeq5',
                             'DummySeq6', 'DummySeq7', 'DummySeq8', 'DummySeq9', 'DummySeq10',
                             3040,
                             50001,
                             3211,
                             NULL,
                             'Set of Books Id - NA',
                             L_currency);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.FIF_GL_SETUP',
                                            to_char(SQLCODE));
      return FALSE;
END FIF_GL_SETUP;
----------------------------------------------------------------------------------
FUNCTION ORG_UNIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO ORG_UNIT (org_unit_id,
                         description,
                         set_of_books_id)
                 VALUES (1111111111,
                         'Org Unit Id - NA',
                         111111111111111);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.ORG_UNIT',
                                            to_char(SQLCODE));
      return FALSE;
END ORG_UNIT;
----------------------------------------------------------------------------------
FUNCTION TSF_ENTITY_ORG_UNIT_SOB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO TSF_ENTITY_ORG_UNIT_SOB (TSF_ENTITY_ID,
                                        ORG_UNIT_ID,
                                        SET_OF_BOOKS_ID)
                                VALUES (1000,
                                        1111111111,
                                        111111111111111);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.TSF_ENTITY_ORG_UNIT_SOB',
                                            to_char(SQLCODE));
      return FALSE;
END TSF_ENTITY_ORG_UNIT_SOB;
----------------------------------------------------------------------------------
FUNCTION REFRESH_MV_LOC_SOB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   if not SET_OF_BOOKS_SQL.REFRESH_MV_LOC_SOB(O_error_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.REFRESH_MV_LOC_SOB',
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_MV_LOC_SOB;
--------------------------------------------------------------------------------------------
FUNCTION REFRESH_MV_CURRENCY_CONV_RATES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   if not REFRESH_MV_CURR_CONV_RATES(O_error_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.REFRESH_MV_CURRENCY_CONV_RATES',
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_MV_CURRENCY_CONV_RATES;
-----------------------------------------------------------------------------------------------
FUNCTION FREIGHT_TERMS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

   L_date   PERIOD.VDATE%TYPE := GET_VDATE;

BEGIN

   INSERT INTO FREIGHT_TERMS ( FREIGHT_TERMS,
                               START_DATE_ACTIVE,
                               END_DATE_ACTIVE,
                               ENABLED_FLAG )
                      VALUES ( '01',
                               L_date,
                               NULL,
                               'Y' );

   INSERT INTO FREIGHT_TERMS_TL ( FREIGHT_TERMS,
                                  TERM_DESC,
                                  LANG,
                                  ORIG_LANG_IND,
                                  REVIEWED_IND,
                                  CREATE_DATETIME,       
                                  CREATE_ID,             
                                  LAST_UPDATE_DATETIME,  
                                  LAST_UPDATE_ID )
                         VALUES ( '01',
                                  '2% Total Cost',
                                  GET_PRIMARY_LANG,
                                  'Y', 
                                  'N',
                                  SYSDATE,
                                  GET_USER,
                                  SYSDATE,
                                  GET_USER );

   INSERT INTO FREIGHT_TERMS ( FREIGHT_TERMS,
                               START_DATE_ACTIVE,
                               END_DATE_ACTIVE,
                               ENABLED_FLAG )
                      VALUES ( '02',
                               L_date,
                               NULL,
                               'Y' );

   INSERT INTO FREIGHT_TERMS_TL ( FREIGHT_TERMS,
                                  TERM_DESC,
                                  LANG,
                                  ORIG_LANG_IND,
                                  REVIEWED_IND,
                                  CREATE_DATETIME,       
                                  CREATE_ID,             
                                  LAST_UPDATE_DATETIME,  
                                  LAST_UPDATE_ID )
                         VALUES ( '02',
                                  '$50 Flat Fee',
                                  GET_PRIMARY_LANG,
                                  'Y', 
                                  'N',
                                  SYSDATE,
                                  GET_USER,
                                  SYSDATE,
                                  GET_USER );

   INSERT INTO FREIGHT_TERMS ( FREIGHT_TERMS,
                               START_DATE_ACTIVE,
                               END_DATE_ACTIVE,
                               ENABLED_FLAG )
                      VALUES ( '03',
                               L_date,
                               NULL,
                               'Y' );

   INSERT INTO FREIGHT_TERMS_TL ( FREIGHT_TERMS,
                                  TERM_DESC,
                                  LANG,
                                  ORIG_LANG_IND,
                                  REVIEWED_IND,
                                  CREATE_DATETIME,       
                                  CREATE_ID,             
                                  LAST_UPDATE_DATETIME,  
                                  LAST_UPDATE_ID )
                         VALUES ( '03',
                                  'Free',
                                  GET_PRIMARY_LANG,
                                  'Y', 
                                  'N',
                                  SYSDATE,
                                  GET_USER,
                                  SYSDATE,
                                  GET_USER );

   INSERT INTO FREIGHT_TERMS ( FREIGHT_TERMS,
                               START_DATE_ACTIVE,
                               END_DATE_ACTIVE,
                               ENABLED_FLAG )
                      VALUES ( '04',
                               L_date,
                               NULL,
                               'Y' );

   INSERT INTO FREIGHT_TERMS_TL ( FREIGHT_TERMS,
                                  TERM_DESC,
                                  LANG,
                                  ORIG_LANG_IND,
                                  REVIEWED_IND,
                                  CREATE_DATETIME,       
                                  CREATE_ID,             
                                  LAST_UPDATE_DATETIME,  
                                  LAST_UPDATE_ID )
                         VALUES ( '04',
                                  '3% Add to Invoice',
                                  GET_PRIMARY_LANG,
                                  'Y', 
                                  'N',
                                  SYSDATE,
                                  GET_USER,
                                  SYSDATE,
                                  GET_USER );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.freight_terms',
                                            to_char(SQLCODE));
      return FALSE;
END FREIGHT_TERMS;
-----------------------------------------------------------------------------------------------
FUNCTION PAYMENT_TERMS(O_error_message   IN OUT   VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('01', 1);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('02', 2);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('03', 3);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('04', 4);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('05', 5);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('06', 6);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('07', 7);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('1', 8);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('2', 9);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('30', 10);
   INSERT INTO TERMS_HEAD ( TERMS, RANK ) VALUES ('108', 11);
   
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '01', 'Term 1', '2.5% 30 Days', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '02', 'Term 2', '1.5% 30 Days', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '03', 'Term 3', '3.5% 15 Days', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '04', 'Term 4', 'Net 30 Days', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '05', 'Term 5', '2.5% Monthly', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '06', 'Term 6', '1.5% Monthly', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '07', 'Term 7', 'Net Monthly', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '1', '1', '01 002.50% 030 060', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '2', '2', '01 003.00% 030 031', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '30', '30', '03 000.00% 010 030', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
   INSERT INTO TERMS_HEAD_TL ( TERMS, TERMS_CODE, TERMS_DESC, LANG, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID ) VALUES (
   '108', '108', '02 001.00% 010 000', GET_PRIMARY_LANG, 'Y', 'N', SYSDATE, USER, SYSDATE, USER);
      
   
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '01', 1, 30, 1000000, 1, 12, 0, 2.5, 2, 11, NULL, 'N', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '02', 1, 30, 1000000, 1, 12, 0, 1.5, 2, 11, NULL, 'N', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '03', 1, 15, 1000000, 1, 12, 0, 3.5, 2, 11, NULL, 'N', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '04', 1, 30, 1000000, 1, 12, 0, 0, 2, 11, NULL, 'Y', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '05', 1, 30, 1000000, 1, 12, 0, 2.5, 2, 11, NULL, 'N', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '06', 1, 30, 1000000, 1, 12, 0, 1.5, 2, 11, NULL, 'N', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '07', 1, 30, 1000000, 1, 12, 0, 0, 2, 11, NULL, 'N', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '05', 2, 30, 1, 1, 1, 30, 2.5, 1, 1, NULL, 'Y', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '06', 3, 30, 1, 1, 1, 30, 1.5, 1, 1, NULL, 'Y', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '07', 4, 30, 1, 1, 1, 30, 0, 1, 1, NULL, 'Y', NULL, NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '1', 166, 60, 1, 1, 1, 30, 2.5, 1, 1, NULL, 'Y',  TO_Date( '01/10/1995 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, 1);   
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '2', 8, 31, 1, 1, 1, 30, 3, 1, 1, NULL, 'Y',  TO_Date( '01/10/1995 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '30', 38, 30, 1, 1, 1, 10, 0, 1, 1, NULL, 'Y',  TO_Date( '01/10/1995 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, 1);
   INSERT INTO TERMS_DETAIL ( TERMS, TERMS_SEQ, DUEDAYS, DUE_MAX_AMOUNT, DUE_DOM, DUE_MM_FWD, DISCDAYS,
   PERCENT, DISC_DOM, DISC_MM_FWD, FIXED_DATE, ENABLED_FLAG, START_DATE_ACTIVE, END_DATE_ACTIVE,
   CUTOFF_DAY ) VALUES (
   '108', 78, 0, 1, 1, 1, 10, 1, 1, 1, NULL, 'Y',  TO_Date( '01/10/1995 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, 1);
      
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.PAYMENT_TERMS',
                                            to_char(SQLCODE));
      return FALSE;
END PAYMENT_TERMS;
------------------------------------------------------------------------------------------------
FUNCTION SUPS_ADDR_SUP_SITE(O_error_message         IN OUT   VARCHAR2,
                            I_default_tax_type      IN       VARCHAR2)
RETURN BOOLEAN IS

   L_vat_region          NUMBER   := NULL;


BEGIN
   ---
   if I_default_tax_type = 'SVAT' then
      L_vat_region := 1000;
   end if;
   ---

   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2700, 'Tyson Fresh Meats', NULL, 'Larrie Rome', '970-222-7395', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '02', '01', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL, 3
   , NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2800, 'Minneapolis Beef Council', 2700, 'Kerry Rose', '612-484-3764', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '01', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL
   , NULL, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1800, 'Refurbishment Supplier', NULL, 'Refurb Supplier', 'n/a', 'n/a', NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '07', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '80', NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT', NULL, NULL
   , NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3020, 'Battery Supplier', NULL, 'Mark Stevenson', '859-231-1234', NULL, NULL, 'A', 'Y', 2
   , 5, 'N', NULL, NULL, 'USD', 1, '108', '04', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, 'mstevenson@battery.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 8, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2200, 'Philip Morris', 3020, 'Jacob Crest', '804 745 6241', '804 745 1212', NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '03', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, 'JCrest@phillipmorris.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 2, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1300, 'Fine Jewelry Supplier', NULL, 'Dale McComb', '212-530-2000', '212-530-2001', NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '01', '01', 'Y', 'Y', NULL, NULL
   , NULL, 'Y', 'N', 'Y', 'N', NULL, 'Y', 'N', 'N', NULL, 0, 0, 'N', '80', 'OA', NULL
   , NULL, 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL
   , 'NEXT', NULL, NULL, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2300, 'Coca Cola', NULL, 'John O''Neill', '404-233-9999', NULL, NULL, 'A', 'N', NULL, NULL
   , 'N', NULL, NULL, 'USD', 1, '01', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N', 'N'
   , NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL, NULL, NULL
   , NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2400, 'Coca Cola - Charlotte', 2300, 'John O''Reilly', '304-445-4848', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '03', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL
   , NULL, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2500, 'Coca Cola - Chicago', 2300, 'Mike Daley', '773-574-7894', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '02', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD', NULL, NULL
   , NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2600, 'Coca Cola - Los Angeles', 2300, 'Diego Santorez', '940-393-5649', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '03', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, NULL, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1000, 'Fashion Import Supplier', NULL, 'Christy Grant', '212-909-8000', '212-909-8001', NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '04', 'Y', 'Y', NULL, NULL
   , NULL, 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '11', 'LC', NULL
   , 'cgrant@fahsion.com', 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL
   , NULL, NULL, 'NDD', NULL, 90, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1200, 'Fashion Importer (Euro)', 1000, 'Antonio Carusso', '+39 98 78 65 98', '+39 98 78 65 99'
   , NULL, 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'EUR', 1, '01', '01', 'Y', 'Y', NULL
   , NULL, NULL, 'Y', 'Y', 'Y', 'N', NULL, 'N', 'Y', 'N', NULL, 0, 0, 'Y', '11', 'WT'
   , NULL, 'acarusso@fie.com', 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N'
   , NULL, NULL, NULL, 'NDD', NULL, 75, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1400, 'Fashion Domestic', 1000, 'Laura Johnson', '617-897-0900', '617-897-0911', NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '07', '03', 'Y', 'Y', NULL, NULL, NULL
   , 'Y', 'Y', 'Y', 'Y', NULL, 'Y', 'N', 'N', NULL, 10, 0, 'N', '30', 'OA', NULL, 'ljohnson@fasdom.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 20, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2000, 'Dannon', NULL, 'Alan McKenzie', '914 366 9700', '914 366 9801', NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '05', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT', NULL, 7
   , NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3000, 'Electronics Supplier 1', NULL, 'Bill Johnson', '818 990 0000', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '1', '03', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, NULL, 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD', NULL
   , 7, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3010, 'Electronics Supplier 2', 3000, 'JoAnne Halverson', '206 878 4500', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '108', '03', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, NULL
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 9, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   1500, 'Lancome', NULL, 'Marie Leblanc', '212-790-2323', '212-790-5001', NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '07', '03', 'Y', 'Y', NULL, NULL, NULL, 'Y', 'Y'
   , 'Y', 'Y', NULL, 'Y', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, 'mleblanc@lancome.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT'
   , NULL, 3, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2100, 'Del Monte Foods', NULL, 'Dave Sharpton', '(756) 754-7527', '(756) 754-3230', NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '03', 'Y', 'Y', NULL, NULL
   , 2, 'Y', 'N', 'Y', 'Y', 'D', 'N', 'N', 'N', NULL, 2, 0, 'N', '30', NULL, NULL, 'dave.sharpton@delmonte.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT'
   , NULL, 2, '124057394', '1234', 'Y', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   8000, 'Concession Supplier', NULL, 'Jean Jones', '970-376-0000', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '07', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD', NULL, NULL
   , NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   6000, 'Local Grocery Supplier #1', 8000, 'Local Supplier', '999-999-9999', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '01', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL, NULL
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT'
   , NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   6100, 'Local Grocery Supplier #2', 8000, 'Local Supplier #2', '999-999-9999', NULL, NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '01', 'Y', 'Y', NULL, NULL
   , NULL, 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL
   , NULL, 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL
   , 'NEXT', NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   6200, 'Local Grocery Supplier #3', 8000, 'Local Supplier #3', '999-999-9999', NULL, NULL
   , 'A', 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '01', 'Y', 'Y', NULL, NULL
   , NULL, 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', NULL, NULL
   , NULL, 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL
   , 'NEXT', NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2900, 'Local Supplier #1', 8000, 'Jack Strain', '609-336-9365', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '02', '01', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NEXT', NULL, NULL
   , NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2910, 'Local Supplier #2', 8000, 'Mark Ott', '609-373-0284', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '02', '01', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', NULL, NULL, NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NEXT', NULL, NULL
   , NULL, NULL, 'N', NULL, 'Y', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   7777, 'General Detergents Supplier', NULL, 'George Beard', '401-349-9432', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '04', '01', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '31', NULL, NULL, NULL
   , 'N', 'N', 'N', 'Y', 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, NULL, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   2950, 'Paper Products Supplier', NULL, 'Fred Smith', '555 555 1212', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '30', '02', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, NULL, 'N'
   , 'N', 'N', 'L', 'N', 'N', 'N', 'N', L_vat_region, 'S', 'N', NULL, NULL, NULL, 'NEXT', NULL
   , 2, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3100, 'Candle Importer', NULL, 'Thomsa Kraus', '719-387-2345', NULL, NULL, 'A', 'Y', 3, 3
   , 'N', NULL, NULL, 'USD', 1, '02', '04', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '11', 'LC', NULL, 'TKraus@candle.com'
   , 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 90, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3030, 'General Book Supplier', 2950, 'Rebecca Diener', '234-112-7654', NULL, NULL, 'A', 'N'
   , NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '04', 'Y', 'Y', NULL, NULL, NULL, 'N'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, 'rdiener@book.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 4, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3040, 'Procter and Gamble', NULL, 'Frank Jones', '513-222-2222', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '2', '04', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '30', 'OA', NULL, 'fjones@pg.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 3, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3050, 'Furniture Import Supplier', NULL, 'Paul Johnson', '312-770-7700', NULL, NULL, 'A'
   , 'N', NULL, NULL, 'N', NULL, NULL, 'USD', 1, '02', '04', 'Y', 'Y', NULL, NULL, NULL
   , 'N', 'N', 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '11', 'LC', NULL, 'pjohnson@furniture.com'
   , 'N', 'N', 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NDD'
   , NULL, 90, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');
   INSERT INTO SUPS ( SUPPLIER, SUP_NAME, SUPPLIER_PARENT, CONTACT_NAME, CONTACT_PHONE, CONTACT_FAX, CONTACT_PAGER,
   SUP_STATUS, QC_IND, QC_PCT, QC_FREQ, VC_IND, VC_PCT, VC_FREQ, CURRENCY_CODE, LANG, TERMS,
   FREIGHT_TERMS, RET_ALLOW_IND, RET_AUTH_REQ, RET_MIN_DOL_AMT, RET_COURIER, HANDLING_PCT, EDI_PO_IND,
   EDI_PO_CHG, EDI_PO_CONFIRM, EDI_ASN, EDI_SALES_RPT_FREQ, EDI_SUPP_AVAILABLE_IND, EDI_CONTRACT_IND,
   EDI_INVC_IND, EDI_CHANNEL_ID, COST_CHG_PCT_VAR, COST_CHG_AMT_VAR, REPLEN_APPROVAL_IND, SHIP_METHOD,
   PAYMENT_METHOD, CONTACT_TELEX, CONTACT_EMAIL, SETTLEMENT_CODE, PRE_MARK_IND, AUTO_APPR_INVC_IND,
   DBT_MEMO_CODE, FREIGHT_CHARGE_IND, AUTO_APPR_DBT_MEMO_IND, PREPAY_INVC_IND, BACKORDER_IND,
   VAT_REGION, INV_MGMT_LVL, SERVICE_PERF_REQ_IND, INVC_PAY_LOC, INVC_RECEIVE_LOC, ADDINVC_GROSS_NET,
   DELIVERY_POLICY, COMMENT_DESC, DEFAULT_ITEM_LEAD_TIME, DUNS_NUMBER, DUNS_LOC, BRACKET_COSTING_IND,
   VMI_ORDER_STATUS, DSD_IND, SUP_QTY_LEVEL, SCALE_AIP_ORDERS, FINAL_DEST_IND ) VALUES (
   3060, 'Hardware Supplier', NULL, 'Jack Murphy', '612 459 9800', NULL, NULL, 'A', 'N', NULL
   , NULL, 'N', NULL, NULL, 'USD', 1, '1', '03', 'Y', 'Y', NULL, NULL, NULL, 'N', 'N'
   , 'N', 'N', NULL, 'N', 'N', 'N', NULL, 0, 0, 'N', '31', 'OA', NULL, NULL, 'N', 'N'
   , 'N', NULL, 'N', 'N', 'N', 'N', L_vat_region, 'D', 'N', NULL, NULL, NULL, 'NEXT', NULL, 7
, NULL, NULL, 'N', NULL, 'N', 'EA', 'N', 'N');

   ---
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6000', NULL, 1, '05', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83632', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6100', NULL, 1, '01', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83273', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6100', NULL, 1, '03', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83273', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6100', NULL, 1, '04', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83273', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6100', NULL, 1, '05', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83273', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6200', NULL, 1, '01', 'Y', 'Local Supplier #3', NULL, NULL, 'Portland'
   , 'OR', 'US', '83659', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6200', NULL, 1, '03', 'Y', 'Local Supplier #3', NULL, NULL, 'Portland'
   , 'OR', 'US', '83659', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6200', NULL, 1, '04', 'Y', 'Local Supplier #3', NULL, NULL, 'Portland'
   , 'OR', 'US', '83659', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6200', NULL, 1, '05', 'Y', 'Local Supplier #3', NULL, NULL, 'Portland'
   , 'OR', 'US', '83659', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3060', NULL, 1, '01', 'Y', '1400 Excelsior Blvd', NULL, NULL, 'Minneapolis'
   , 'MN', 'US', '55416', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3060', NULL, 1, '03', 'Y', '1400 Excelsior Blvd', NULL, NULL, 'Minneapolis'
   , 'MN', 'US', '55416', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3060', NULL, 1, '04', 'Y', '1400 Excelsior Blvd', NULL, NULL, 'Minneapolis'
   , 'MN', 'US', '55416', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3060', NULL, 1, '05', 'Y', '1400 Excelsior Blvd', NULL, NULL, 'Minneapolis'
   , 'MN', 'US', '55416', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2900', NULL, 1, '01', 'Y', 'Jack Strain', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '09475', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2900', NULL, 1, '03', 'Y', 'Jack Strain', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '09475', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2900', NULL, 1, '04', 'Y', 'Jack Strain', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '09475', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2900', NULL, 1, '05', 'Y', 'Jack Strain', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '09475', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2910', NULL, 1, '01', 'Y', 'Mark Ott', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '08363', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2910', NULL, 1, '03', 'Y', 'Mark Ott', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '08363', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2910', NULL, 1, '04', 'Y', 'Mark Ott', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '08363', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2910', NULL, 1, '05', 'Y', 'Mark Ott', NULL, NULL, 'Vineland', 'NJ'
   , 'US', '08363', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '7777', NULL, 1, '01', 'Y', '401 Highland pkwy', NULL, NULL, 'Detroit'
   , 'MI', 'US', '93405', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '7777', NULL, 1, '03', 'Y', '401 Highland pkwy', NULL, NULL, 'Detroit'
   , 'MI', 'US', '93405', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '7777', NULL, 1, '04', 'Y', '401 Highland pkwy', NULL, NULL, 'Detroit'
   , 'MI', 'US', '93405', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '7777', NULL, 1, '05', 'Y', '401 Highland pkwy', NULL, NULL, 'Detroit'
   , 'MI', 'US', '93405', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2950', NULL, 1, '01', 'Y', '121 S First St', NULL, NULL, 'New York'
   , 'NY', 'US', '21078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2950', NULL, 1, '03', 'Y', '121 S First St', NULL, NULL, 'New York'
   , 'NY', 'US', '21078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2950', NULL, 1, '04', 'Y', '121 S First St', NULL, NULL, 'New York'
   , 'NY', 'US', '21078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2950', NULL, 1, '05', 'Y', '121 S First St', NULL, NULL, 'New York'
   , 'NY', 'US', '21078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2700', NULL, 1, '01', 'Y', '1233 Hwy 9', NULL, NULL, 'Dakota Downs'
   , 'SD', 'US', '46304', 'Larrie Rome', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2700', NULL, 1, '03', 'Y', '1233 Hwy 9', NULL, NULL, 'Dakota Downs'
   , 'SD', 'US', '46304', 'Larrie Rome', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2700', NULL, 1, '04', 'Y', '1233 Hwy 9', NULL, NULL, 'Dakota Downs'
   , 'SD', 'US', '46304', 'Larrie Rome', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2700', NULL, 1, '05', 'Y', '1233 Hwy 9', NULL, NULL, 'Dakota Downs'
   , 'SD', 'US', '46304', 'Larrie Rome', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2800', NULL, 1, '01', 'Y', '2340 Eden Prairie Parkway', NULL, NULL
   , 'Eden Prairie', 'MN', 'US', '55440u', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2800', NULL, 1, '03', 'Y', '2340 Eden Prairie Parkway', NULL, NULL
   , 'Eden Prairie', 'MN', 'US', '55440u', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2800', NULL, 1, '04', 'Y', '2340 Eden Prairie Parkway', NULL, NULL
   , 'Eden Prairie', 'MN', 'US', '55440u', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2800', NULL, 1, '05', 'Y', '2340 Eden Prairie Parkway', NULL, NULL
   , 'Eden Prairie', 'MN', 'US', '55440u', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1800', NULL, 1, '01', 'Y', 'n/a', NULL, NULL, 'n/a', NULL, 'US', NULL
   , 'N/a', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1800', NULL, 1, '03', 'Y', 'n/a', NULL, NULL, 'n/a', NULL, 'US', NULL
   , 'N/a', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1800', NULL, 1, '04', 'Y', 'n/a', NULL, NULL, 'n/a', NULL, 'US', NULL
   , 'N/a', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1800', NULL, 1, '05', 'Y', 'n/a', NULL, NULL, 'n/a', NULL, 'US', NULL
   , 'N/a', NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3020', NULL, 1, '01', 'Y', '23423 Commerce Drive', NULL, NULL, 'Louisville'
   , 'KY', 'US', '40202', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3020', NULL, 1, '03', 'Y', '23423 Commerce Drive', NULL, NULL, 'Louisville'
   , 'KY', 'US', '40202', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3020', NULL, 1, '04', 'Y', '23423 Commerce Drive', NULL, NULL, 'Louisville'
   , 'KY', 'US', '40202', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3020', NULL, 1, '05', 'Y', '23423 Commerce Drive', NULL, NULL, 'Louisville'
   , 'KY', 'US', '40202', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2200', NULL, 1, '01', 'Y', '4200 Deepwater Terminal Road', NULL, NULL
   , 'Richmond', 'VA', 'US', '23234', 'Jacob Crest', NULL, NULL, NULL, NULL, NULL, 'Y'
   , NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2200', NULL, 1, '03', 'Y', '4200 Deepwater Terminal Road', NULL, NULL
   , 'Richmond', 'VA', 'US', '23234', 'Jacob Crest', NULL, NULL, NULL, NULL, NULL, 'Y'
   , NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2200', NULL, 1, '04', 'Y', '4200 Deepwater Terminal Road', NULL, NULL
   , 'Richmond', 'VA', 'US', '23234', 'Jacob Crest', NULL, NULL, NULL, NULL, NULL, 'Y'
   , NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2200', NULL, 1, '05', 'Y', '4200 Deepwater Terminal Road', NULL, NULL
   , 'Richmond', 'VA', 'US', '23234', 'Jacob Crest', NULL, NULL, NULL, NULL, NULL, 'Y'
   , NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1300', NULL, 1, '01', 'Y', '5757 E. 46th Street', NULL, NULL, 'New York City'
   , 'NY', 'US', '10090', 'Dale McComb', '212-530-2000', NULL, '212-530-2001', NULL, NULL
   , 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1300', NULL, 1, '03', 'Y', '5757 E. 46th Street', NULL, NULL, 'New York City'
   , 'NY', 'US', '10090', 'Dale McComb', '212-530-2000', NULL, '212-530-2001', NULL, NULL
   , 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1300', NULL, 1, '04', 'Y', '5757 E. 46th Street', NULL, NULL, 'New York City'
   , 'NY', 'US', '10090', 'Dale McComb', '212-530-2000', NULL, '212-530-2001', NULL, NULL
   , 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1300', NULL, 1, '05', 'Y', '5757 E. 46th Street', NULL, NULL, 'New York City'
   , 'NY', 'US', '10090', 'Dale McComb', '212-530-2000', NULL, '212-530-2001', NULL, NULL
   , 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2300', NULL, 1, '01', 'Y', '1499 Peach Tree Plaza', NULL, NULL, 'Atlanta'
   , 'GA', 'US', '30232', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2300', NULL, 1, '03', 'Y', '1499 Peach Tree Plaza', NULL, NULL, 'Atlanta'
   , 'GA', 'US', '30232', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2300', NULL, 1, '04', 'Y', '1499 Peach Tree Plaza', NULL, NULL, 'Atlanta'
   , 'GA', 'US', '30232', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2300', NULL, 1, '05', 'Y', '1499 Peach Tree Plaza', NULL, NULL, 'Atlanta'
   , 'GA', 'US', '30232', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2400', NULL, 1, '01', 'Y', ' 293 Market St', NULL, NULL, 'Charlotte'
   , 'NC', 'US', '30365', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2400', NULL, 1, '03', 'Y', ' 293 Market St', NULL, NULL, 'Charlotte'
   , 'NC', 'US', '30365', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2400', NULL, 1, '04', 'Y', ' 293 Market St', NULL, NULL, 'Charlotte'
   , 'NC', 'US', '30365', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2400', NULL, 1, '05', 'Y', ' 293 Market St', NULL, NULL, 'Charlotte'
   , 'NC', 'US', '30365', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2500', NULL, 1, '01', 'Y', '2300 Lakeshore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '37078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2500', NULL, 1, '03', 'Y', '2300 Lakeshore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '37078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2500', NULL, 1, '04', 'Y', '2300 Lakeshore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '37078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2500', NULL, 1, '05', 'Y', '2300 Lakeshore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '37078', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2600', NULL, 1, '01', 'Y', '230 Ocean Pacific Hwy', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '93642', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2600', NULL, 1, '03', 'Y', '230 Ocean Pacific Hwy', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '93642', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2600', NULL, 1, '04', 'Y', '230 Ocean Pacific Hwy', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '93642', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2600', NULL, 1, '05', 'Y', '230 Ocean Pacific Hwy', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '93642', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1000', NULL, 1, '01', 'Y', '212 E. 58th Street', NULL, NULL, 'New York city'
   , 'NY', 'US', '10010', 'Christy Grant', '212-909-8000', NULL, '212-909-8001', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1000', NULL, 1, '03', 'Y', '212 E. 58th Street', NULL, NULL, 'New York city'
   , 'NY', 'US', '10010', 'Christy Grant', '212-909-8000', NULL, '212-909-8001', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1000', NULL, 1, '04', 'Y', '212 E. 58th Street', NULL, NULL, 'New York city'
   , 'NY', 'US', '10010', 'Christy Grant', '212-909-8000', NULL, '212-909-8001', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1000', NULL, 1, '05', 'Y', '212 E. 58th Street', NULL, NULL, 'New York city'
   , 'NY', 'US', '10010', 'Christy Grant', '212-909-8000', NULL, '212-909-8001', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1200', NULL, 1, '01', 'Y', '3301, via del Duomo', NULL, NULL, 'Milan'
   , NULL, 'IT', '49000', 'Antonio Caruso', '+39 78 67 54 22', NULL, '+39 78 67 54 21'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1200', NULL, 1, '03', 'Y', '3301, via del Duomo', NULL, NULL, 'Milan'
   , NULL, 'IT', '49000', 'Antonio Caruso', '+39 78 67 54 22', NULL, '+39 78 67 54 21'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1200', NULL, 1, '04', 'Y', '3301, via del Duomo', NULL, NULL, 'Milan'
   , NULL, 'IT', '49000', 'Antonio Caruso', '+39 78 67 54 22', NULL, '+39 78 67 54 21'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1200', NULL, 1, '05', 'Y', '3301, via del Duomo', NULL, NULL, 'Milan'
   , NULL, 'IT', '49000', 'Antonio Caruso', '+39 78 67 54 22', NULL, '+39 78 67 54 21'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1400', NULL, 1, '01', 'Y', '200 Ryan Way', 'Suite 100', NULL, 'Somerville'
   , 'MA', 'US', '05490', 'Laura Johnson', '617-897-0900', NULL, '617-897-0911', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1400', NULL, 1, '03', 'Y', '200 Ryan Way', 'Suite 100', NULL, 'Somerville'
   , 'MA', 'US', '05490', 'Laura Johnson', '617-897-0900', NULL, '617-897-0911', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1400', NULL, 1, '04', 'Y', '200 Ryan Way', 'Suite 100', NULL, 'Somerville'
   , 'MA', 'US', '05490', 'Laura Johnson', '617-897-0900', NULL, '617-897-0911', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1400', NULL, 1, '05', 'Y', '200 Ryan Way', 'Suite 100', NULL, 'Somerville'
   , 'MA', 'US', '05490', 'Laura Johnson', '617-897-0900', NULL, '617-897-0911', NULL
   , NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2000', NULL, 1, '01', 'Y', '120 White Plain Road', NULL, NULL, 'Tarrytown'
   , 'NY', 'US', '10591', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2000', NULL, 1, '03', 'Y', '120 White Plain Road', NULL, NULL, 'Tarrytown'
   , 'NY', 'US', '10591', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2000', NULL, 1, '04', 'Y', '120 White Plain Road', NULL, NULL, 'Tarrytown'
   , 'NY', 'US', '10591', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2000', NULL, 1, '05', 'Y', '120 White Plain Road', NULL, NULL, 'Tarrytown'
   , 'NY', 'US', '10591', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3000', NULL, 1, '01', 'Y', '1450 Bernard Blvd', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '91401', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3000', NULL, 1, '03', 'Y', '1450 Bernard Blvd', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '91401', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3000', NULL, 1, '04', 'Y', '1450 Bernard Blvd', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '91401', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3000', NULL, 1, '05', 'Y', '1450 Bernard Blvd', NULL, NULL, 'Los Angeles'
   , 'CA', 'US', '91401', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3010', NULL, 1, '01', 'Y', '500 Waterfront Way', NULL, NULL, 'Seattle'
   , 'WA', 'US', '98101', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3010', NULL, 1, '03', 'Y', '500 Waterfront Way', NULL, NULL, 'Seattle'
   , 'WA', 'US', '98101', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3010', NULL, 1, '04', 'Y', '500 Waterfront Way', NULL, NULL, 'Seattle'
   , 'WA', 'US', '98101', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3010', NULL, 1, '05', 'Y', '500 Waterfront Way', NULL, NULL, 'Seattle'
   , 'WA', 'US', '98101', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1500', NULL, 1, '01', 'Y', '5010 W. 57th Street', 'Suite 200', NULL
   , 'NYC', 'NY', 'US', '10010', 'Marie Leblanc', '212-790-2323', NULL, '212-790-5001'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1500', NULL, 1, '03', 'Y', '5010 W. 57th Street', 'Suite 200', NULL
   , 'NYC', 'NY', 'US', '10010', 'Marie Leblanc', '212-790-2323', NULL, '212-790-5001'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1500', NULL, 1, '04', 'Y', '5010 W. 57th Street', 'Suite 200', NULL
   , 'NYC', 'NY', 'US', '10010', 'Marie Leblanc', '212-790-2323', NULL, '212-790-5001'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '1500', NULL, 1, '05', 'Y', '5010 W. 57th Street', 'Suite 200', NULL
   , 'NYC', 'NY', 'US', '10010', 'Marie Leblanc', '212-790-2323', NULL, '212-790-5001'
   , NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2100', NULL, 1, '01', 'Y', 'PO Box 8326', NULL, NULL, 'Walnut Creek'
   , 'CA', 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2100', NULL, 1, '03', 'Y', 'PO Box 8326', NULL, NULL, 'Walnut Creek'
   , 'CA', 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2100', NULL, 1, '04', 'Y', 'PO Box 8326', NULL, NULL, 'Walnut Creek'
   , 'CA', 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '2100', NULL, 1, '05', 'Y', 'PO Box 8326', NULL, NULL, 'Walnut Creek'
   , 'CA', 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3100', NULL, 1, '01', 'Y', '217 E 31st St', 'Suite 210', NULL, 'New York'
   , 'NY', 'US', '10010', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3100', NULL, 1, '03', 'Y', '217 E 31st St', 'Suite 210', NULL, 'New York'
   , 'NY', 'US', '10010', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3100', NULL, 1, '04', 'Y', '217 E 31st St', 'Suite 210', NULL, 'New York'
   , 'NY', 'US', '10010', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3100', NULL, 1, '05', 'Y', '217 E 31st St', 'Suite 210', NULL, 'New York'
   , 'NY', 'US', '10010', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3030', NULL, 1, '01', 'Y', '7899 Washington Blvd', 'Suite 110', NULL
   , 'Los Angeles', 'CA', 'US', '93212', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3030', NULL, 1, '03', 'Y', '7899 Washington Blvd', 'Suite 110', NULL
   , 'Los Angeles', 'CA', 'US', '93212', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3030', NULL, 1, '04', 'Y', '7899 Washington Blvd', 'Suite 110', NULL
   , 'Los Angeles', 'CA', 'US', '93212', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3030', NULL, 1, '05', 'Y', '7899 Washington Blvd', 'Suite 110', NULL
   , 'Los Angeles', 'CA', 'US', '93212', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL
   , 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3040', NULL, 1, '01', 'Y', '311 Vine Street', NULL, NULL, 'Cincinnati'
   , 'OH', 'US', '42332', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3040', NULL, 1, '03', 'Y', '311 Vine Street', NULL, NULL, 'Cincinnati'
   , 'OH', 'US', '42332', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3040', NULL, 1, '04', 'Y', '311 Vine Street', NULL, NULL, 'Cincinnati'
   , 'OH', 'US', '42332', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3040', NULL, 1, '05', 'Y', '311 Vine Street', NULL, NULL, 'Cincinnati'
   , 'OH', 'US', '42332', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3050', NULL, 1, '01', 'Y', '2143 Lake Shore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '32115', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3050', NULL, 1, '03', 'Y', '2143 Lake Shore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '32115', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3050', NULL, 1, '04', 'Y', '2143 Lake Shore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '32115', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '3050', NULL, 1, '05', 'Y', '2143 Lake Shore Dr', NULL, NULL, 'Chicago'
   , 'IL', 'US', '32115', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '8000', NULL, 1, '01', 'Y', 'John Jones', NULL, NULL, 'Seattle', 'WA'
   , 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '8000', NULL, 1, '03', 'Y', 'John Jones', NULL, NULL, 'Seattle', 'WA'
   , 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '8000', NULL, 1, '04', 'Y', 'John Jones', NULL, NULL, 'Seattle', 'WA'
   , 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '8000', NULL, 1, '05', 'Y', 'John Jones', NULL, NULL, 'Seattle', 'WA'
   , 'US', '99372', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6000', NULL, 1, '01', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83632', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6000', NULL, 1, '03', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
   , 'US', '83632', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   INSERT INTO ADDR ( ADDR_KEY, MODULE, KEY_VALUE_1, KEY_VALUE_2, SEQ_NO, ADDR_TYPE, PRIMARY_ADDR_IND,
   ADD_1, ADD_2, ADD_3, CITY, STATE, COUNTRY_ID, POST, CONTACT_NAME, CONTACT_PHONE, CONTACT_TELEX,
   CONTACT_FAX, CONTACT_EMAIL, ORACLE_VENDOR_SITE_ID, EDI_ADDR_CHG, COUNTY,
   PUBLISH_IND ) VALUES (
   addr_sequence.nextval, 'SUPP', '6000', NULL, 1, '04', 'Y', '123 Main St', NULL, NULL, 'Portland', 'OR'
, 'US', '83632', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N');
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.SUPS_ADDR_SUP_SITE',
                                            to_char(SQLCODE));
      return FALSE;
END SUPS_ADDR_SUP_SITE;
--------------------------------------------------------------------------------------------
FUNCTION WF_CUSTOMER_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO WF_CUSTOMER_GROUP (WF_CUSTOMER_GROUP_ID,
                                  WF_CUSTOMER_GROUP_NAME)
                          VALUES (1001,
                                  'Customer Group 1');

   INSERT INTO WF_CUSTOMER_GROUP (WF_CUSTOMER_GROUP_ID,
                                  WF_CUSTOMER_GROUP_NAME)
                          VALUES (1002,
                                  'Customer Group 2');

   INSERT INTO WF_CUSTOMER_GROUP (WF_CUSTOMER_GROUP_ID,
                                  WF_CUSTOMER_GROUP_NAME)
                          VALUES (1003,
                                  'Customer Group 3');

   INSERT INTO WF_CUSTOMER_GROUP (WF_CUSTOMER_GROUP_ID,
                                  WF_CUSTOMER_GROUP_NAME)
                          VALUES (1004,
                                  'Customer Group 4');

   INSERT INTO WF_CUSTOMER_GROUP (WF_CUSTOMER_GROUP_ID,
                                  WF_CUSTOMER_GROUP_NAME)
                          VALUES (1005,
                                  'Customer Group 5');

   INSERT INTO WF_CUSTOMER_GROUP (WF_CUSTOMER_GROUP_ID,
                                  WF_CUSTOMER_GROUP_NAME)
                          VALUES (1006,
                                  'Customer Group 6');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.WF_CUSTOMER_GROUP',
                                            to_char(SQLCODE));
      return FALSE;
END WF_CUSTOMER_GROUP;
----------------------------------------------------------------------------------
FUNCTION WF_CUSTOMER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

BEGIN

   INSERT INTO WF_CUSTOMER(WF_CUSTOMER_ID,
                           WF_CUSTOMER_NAME,
                           CREDIT_IND,
                           WF_CUSTOMER_GROUP_ID,
                           AUTO_APPROVE_IND)
                    VALUES(1001,
                           'Customer 1',
                           'Y',
                           1001,
                           'N');


   INSERT INTO WF_CUSTOMER(WF_CUSTOMER_ID,
                           WF_CUSTOMER_NAME,
                           CREDIT_IND,
                           WF_CUSTOMER_GROUP_ID,
                           AUTO_APPROVE_IND)
                    VALUES(1002,
                           'Customer 2',
                           'N',
                           1002,
                           'N');

   INSERT INTO WF_CUSTOMER(WF_CUSTOMER_ID,
                           WF_CUSTOMER_NAME,
                           CREDIT_IND,
                           WF_CUSTOMER_GROUP_ID,
                           AUTO_APPROVE_IND)
                    VALUES(1003,
                           'Customer 3',
                           'N',
                           1003,
                           'N');

   INSERT INTO WF_CUSTOMER(WF_CUSTOMER_ID,
                           WF_CUSTOMER_NAME,
                           CREDIT_IND,
                           WF_CUSTOMER_GROUP_ID,
                           AUTO_APPROVE_IND)
                    VALUES(10041,
                           'Customer 4',
                           'Y',
                           1004,
                           'N');

   INSERT INTO WF_CUSTOMER(WF_CUSTOMER_ID,
                           WF_CUSTOMER_NAME,
                           CREDIT_IND,
                           WF_CUSTOMER_GROUP_ID,
                           AUTO_APPROVE_IND)
                    VALUES(10051,
                           'Customer 5',
                           'N',
                           1005,
                           'N');

   INSERT INTO WF_CUSTOMER(WF_CUSTOMER_ID,
                           WF_CUSTOMER_NAME,
                           CREDIT_IND,
                           WF_CUSTOMER_GROUP_ID,
                           AUTO_APPROVE_IND)
                    VALUES(10061,
                           'Customer 6',
                           'Y',
                           1006,
                           'N');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.WF_CUSTOMER',
                                            to_char(SQLCODE));
      return FALSE;
END WF_CUSTOMER;
----------------------------------------------------------------------------------
FUNCTION PARTNER_ORG_UNIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_supplier_sites_ind   VARCHAR2(1);

   cursor C_GET_SUPP_SITE_IND is
      select supplier_sites_ind
        from system_options;
BEGIN
   open C_GET_SUPP_SITE_IND;
   fetch C_GET_SUPP_SITE_IND into L_supplier_sites_ind;
   close C_GET_SUPP_SITE_IND;

   if L_supplier_sites_ind = 'Y' then
      INSERT INTO PARTNER_ORG_UNIT(PARTNER,
                                   ORG_UNIT_ID,
                                   PARTNER_TYPE,
                                   PRIMARY_PAY_SITE)
                            SELECT supplier,
                                   1111111111,
                                   'U',
                                   'N'
                              FROM SUPS
                             WHERE SUPPLIER_PARENT IS NOT NULL;
   else
      INSERT INTO PARTNER_ORG_UNIT(PARTNER,
                                   ORG_UNIT_ID,
                                   PARTNER_TYPE,
                                   PRIMARY_PAY_SITE)
                            SELECT supplier,
                                   1111111111,
                                   'S',
                                   'N'
                              FROM SUPS
                             WHERE SUPPLIER_PARENT IS NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'GENERAL_DATA_INSTALL.PARTNER_ORG_UNIT',
                                            to_char(SQLCODE));
      return FALSE;
END PARTNER_ORG_UNIT;
----------------------------------------------------------------------------------
FUNCTION FUTURE_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(50) := 'GENERAL_DATA_INSTALL.FUTURE_COST';
   L_item                    ITEM_MASTER.ITEM%TYPE;
   L_obj_itemloc_tbl         OBJ_ITEMLOC_TBL;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_add_1                   ADDR.ADD_1%TYPE;
   L_add_2                   ADDR.ADD_2%TYPE;
   L_add_3                   ADDR.ADD_3%TYPE;
   L_city                    ADDR.CITY%TYPE;
   L_state                   ADDR.STATE%TYPE;
   L_country_id              ADDR.COUNTRY_ID%TYPE;
   L_post                    ADDR.POST%TYPE;
   L_module                  ADDR.MODULE%TYPE;
   L_key_value_1             ADDR.KEY_VALUE_1%TYPE;
   L_key_value_2             ADDR.KEY_VALUE_2%TYPE;
   L_system_options_row      SYSTEM_OPTIONS%ROWTYPE;

   cursor C_GET_FUTURE_COST is
      select rowid,
             base_cost,
             extended_base_cost,
             location,
             loc_type
        from future_cost;
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   if L_system_options_row.default_tax_type = 'GTAX' then
      update future_cost
         set extended_base_cost = base_cost,
             negotiated_item_cost = base_cost,
             wac_tax = 0;
   end if;
   for rec in C_GET_FUTURE_COST LOOP
      if rec.loc_type = 'W' then
         L_module := 'WH';
         L_key_value_1 := rec.location;
      end if;
      if rec.loc_type = 'S' then
         L_module := 'ST';
         L_key_value_1 := rec.location;
      end if;
      if rec.loc_type = 'E' then
         L_module := 'PTNR';
         L_key_value_1 := rec.loc_type;
         L_key_value_2 := rec.location;
      end if;
      if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                   L_add_1,
                                   L_add_2,
                                   L_add_3,
                                   L_city,
                                   L_state,
                                   L_country_id,
                                   L_post,
                                   L_module,
                                   L_key_value_1,
                                   L_key_value_2) = FALSE then
         return FALSE;
      end if;
      update future_cost
         set default_costing_type = (select default_po_cost
                                       from country_attrib
                                      where country_id = L_country_id)
       where rowid = rec.rowid;
   end LOOP;
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END FUTURE_COST;
----------------------------------------------------------------------------------------
FUNCTION ITEM_COST(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_demo_data_ind        IN VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   L_program                    VARCHAR2(50) := 'GENERAL_DATA_INSTALL.ITEM_COST';
   L_country_attrib_row         COUNTRY_ATTRIB%ROWTYPE;
   L_system_options_row         SYSTEM_OPTIONS%ROWTYPE;
   L_item_cost_head_row         ITEM_COST_HEAD%ROWTYPE;
   L_base_cost                  ITEM_COST_HEAD.BASE_COST%TYPE;
   L_extended_base_cost         ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost             ITEM_COST_HEAD.INCLUSIVE_COST%TYPE;
   L_negotiated_item_cost       ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE;
   L_default_loc                COUNTRY_ATTRIB.DEFAULT_LOC%TYPE;
   L_default_loc_type           COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE;
   L_item                       ITEM_COUNTRY.ITEM%TYPE;
   L_prim_dlvy_ctry_ind         ITEM_COST_HEAD.PRIM_DLVY_CTRY_IND%TYPE;
   L_prim_delivery_country_id   ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_add_1                      ADDR.ADD_1%TYPE;
   L_add_2                      ADDR.ADD_2%TYPE;
   L_add_3                      ADDR.ADD_3%TYPE;
   L_city                       ADDR.CITY%TYPE;
   L_state                      ADDR.STATE%TYPE;
   L_country_id                 ADDR.COUNTRY_ID%TYPE;
   L_post                       ADDR.POST%TYPE;
   L_module                     ADDR.MODULE%TYPE;
   L_key_value_1                ADDR.KEY_VALUE_1%TYPE;
   L_key_value_2                ADDR.KEY_VALUE_2%TYPE;
   L_country                    COUNTRY_ATTRIB.COUNTRY_ID%TYPE;
   L_item_cost_tax_incl_ind     COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE;
   L_store_type                 STORE.STORE_TYPE%TYPE;
   L_loc                        STORE.STORE%TYPE;
   L_index_start                NUMBER;
   L_index_end                  NUMBER;
   L_iscl_tax_update_tbl        OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();
   L_vdate                      DATE := GET_VDATE;
   cursor C_GET_ITEM is
      select item
        from item_master
       where item > NVL(l_item, 0)
      order by item;

   TYPE item_tbl IS TABLE of C_GET_ITEM%ROWTYPE index by binary_integer;
   L_item_tbl   item_tbl;

   cursor C_GET_ITEM_SUPP_COUNTRY (l_item_first in item_master.item%type,
                                   l_item_last in item_master.item%type) is
      select item,
             supplier,
             origin_country_id,
             unit_cost
        from item_supp_country
       where item between l_item_first and l_item_last;
   cursor C_GET_ITEM_SUPPLIER (l_item in item_master.item%type) is
      select item,
             supplier
        from item_supplier
       where item = l_item;
   cursor C_GET_COUNTRY is
      select country_id
        from item_country
       where item = L_item;
   cursor C_ITEM_COST_TAX_INCL_IND is
      select item_cost_tax_incl_ind
        from country_attrib
       where country_id = L_country;
   cursor C_GET_STORE_TYPE is
     select store_type
      from store
      where store = L_loc;
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   ---
   if L_system_options_row.default_tax_type = 'GTAX' then

      open C_GET_ITEM;
      LOOP
         fetch C_GET_ITEM BULK COLLECT into L_item_tbl ;
         Exit When L_item_tbl.last is null;
         L_index_end := L_item_tbl.last;
         forall rec in L_item_tbl.first..L_item_tbl.last
            update item_supp_country
               set negotiated_item_cost = unit_cost,
                   extended_base_cost = unit_cost,
                   inclusive_cost = unit_cost,
                   base_cost = unit_cost
              where item = L_item_tbl(rec).item;
         forall rec in L_item_tbl.first..L_item_tbl.last
            update item_supp_country_loc
               set negotiated_item_cost = unit_cost,
                   extended_base_cost = unit_cost,
                   inclusive_cost = unit_cost,
                   base_cost = unit_cost
              where item = L_item_tbl(rec).item;
      end LOOP;
      close C_GET_ITEM;
   end if;
   return TRUE;
   ---
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_COST;
--------------------------------------------------------------------------------------
FUNCTION CHANGE_PRIM_DLVY_CTRY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item                IN     ITEM_COST_HEAD.ITEM%TYPE,
                               I_supplier            IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_origin_country_id   IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                               I_prim_del_country_id IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'ITEM_COST_SQL.CHANGE_PRIM_DLVY_CTRY';
   L_table            VARCHAR2(20) := 'ITEM_COST_HEAD';

   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ITEM_COST_HEAD is
     select 'x'
      from item_cost_head
      where item = I_item
       and supplier = I_supplier
       and origin_country_id = I_origin_country_id
       and delivery_country_id = I_prim_del_country_id
       for update nowait;

   cursor C_LOCK_ITEM_COST_HEAD_CHILD is
     select 'x'
       from item_cost_head ich
      where exists (select 'Y'
                      from item_master im
                     where (im.item_parent = I_item or im.item_grandparent = I_item)
                       and im.item_level <= im.tran_level
                       and ich.item = im.item
                       and ich.supplier = I_supplier
                       and ich.origin_country_id = I_origin_country_id
                       and ich.delivery_country_id = I_prim_del_country_id)
        for update nowait;

BEGIN

   if I_item is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_supplier is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_supplier',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_origin_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   if I_prim_del_country_id is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_prim_del_country_id',
                                           L_program,
                                           NULL);
     return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_COST_HEAD',
                    'ITEM_COST_HEAD',
                    NULL);

   open C_LOCK_ITEM_COST_HEAD;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_COST_HEAD',
                    'ITEM_COST_HEAD',
                    NULL);

   close C_LOCK_ITEM_COST_HEAD;

   update item_cost_head
      set prim_dlvy_ctry_ind = 'N'
    where item = I_item
      and supplier = I_supplier
      and origin_country_id = I_origin_country_id
      and delivery_country_id = I_prim_del_country_id;

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_COST_HEAD_CHILD',
                    'ITEM_COST_HEAD',
                    NULL);
   open C_LOCK_ITEM_COST_HEAD_CHILD;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_COST_HEAD_CHILD',
                    'ITEM_COST_HEAD',
                    NULL);
   close C_LOCK_ITEM_COST_HEAD_CHILD;

   update item_cost_head ich
      set prim_dlvy_ctry_ind = 'N'
    where exists (select 'x'
                    from item_master im
                   where (im.item_parent = I_item or im.item_grandparent = I_item)
                     and im.item_level <= im.tran_level
                     and ich.item = im.item
                     and ich.supplier = I_supplier
                     and ich.origin_country_id = I_origin_country_id
                     and ich.delivery_country_id = I_prim_del_country_id);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED THEN
     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                           L_table,
                                           NULL,
                                           TO_CHAR(SQLCODE));
     return FALSE;
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END CHANGE_PRIM_DLVY_CTRY;
-------------------------------------------------------------------
----------------------------------------------------------------------------------------
FUNCTION CALC_VAT_COST_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item            IN                ITEM_MASTER.ITEM%TYPE,
                           I_supplier        IN                SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'DATA_MIGRATION_SQL.CALC_VAT_COST_TAX';

   L_item               ITEM_MASTER.ITEM%TYPE;
   L_active_date        VAT_CODE_RATES.ACTIVE_DATE%TYPE  := GET_VDATE;
   L_vat_region         VAT_REGION.VAT_REGION%TYPE;
   L_location           ITEM_LOC.LOC%TYPE;
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind       ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE;
   L_packitem           ITEM_MASTER.ITEM%TYPE;
   L_packitem_unit_cost ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_pack_cost_in_vat   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := 0;
   L_pack_cost_ex_vat   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := 0;
   L_vat_code           VAT_CODE_RATES.VAT_CODE%TYPE;
   L_vat_rate           VAT_ITEM.VAT_RATE%TYPE;
   L_vat_type           VAT_ITEM.VAT_TYPE%TYPE;

   --tax type constants
   LP_TAX_TYPE_COST         VARCHAR2(20)             := 'C';
   LP_TAX_TYPE_RETAIL       VARCHAR2(20)             := 'R';
   LP_TAX_TYPE_BOTH         VARCHAR2(20)             := 'B';

   cursor C_SUPS_VAT_REGION is
      select vat_region
        from sups
       where supplier = L_location;

   cursor C_VAT_SKU (item_param item_master.item%TYPE) is
      select vat_code, vat_rate, vat_type
        from vat_item
       where item = item_param
         and vat_region  = L_vat_region
         and vat_type in (LP_TAX_TYPE_COST, LP_TAX_TYPE_BOTH)
         and active_date <= L_active_date
    order by active_date desc;  -- Much faster than then the correlated subquery.

   cursor C_COST is
      select unit_cost
        from item_supp_country
       where item_supp_country.item = L_packitem
         and item_supp_country.primary_country_ind = 'Y'
         and item_supp_country.primary_supp_ind = 'Y';

   cursor C_PACKSKU is
      select item, qty
        from v_packsku_qty
       where pack_no = L_item;

BEGIN

   L_item        :=   I_item;
   L_location    :=   I_supplier;


   SQL_LIB.SET_MARK('OPEN',
                    'C_SUPS_VAT_REGION',
                    'supplier',
                    NULL);
   open  C_SUPS_VAT_REGION;
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUPS_VAT_REGION',
                    'supplier',
                    NULL);
   fetch C_SUPS_VAT_REGION into L_vat_region;
   if C_SUPS_VAT_REGION%NOTFOUND then
      -- This location doesn't have associated vat region.
      O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                            L_program,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SUPS_VAT_REGION',
                       'supplier',
                       NULL);
      close C_SUPS_VAT_REGION;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                     'C_SUPS_VAT_REGION',
                     'supplier',
                     NULL);
   close C_SUPS_VAT_REGION;

   if L_vat_region is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                            NULL,
                                            NULL,
                                            NULL);
       return FALSE;
   end if;

   if I_item is NOT NULL then
      if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                       L_pack_ind,
                                       L_sellable_ind,
                                       L_orderable_ind,
                                       L_pack_type,
                                       L_item) = FALSE then
         return FALSE;
      end if;

      if L_pack_ind = 'Y' and (L_pack_type = 'B' or L_pack_type is NULL) then
         for rec in C_PACKSKU LOOP
            L_packitem := rec.item;
            SQL_LIB.SET_MARK('OPEN',
                             'C_cost',
                             'item_supp_country',
                             NULL);
            open C_COST;
            SQL_LIB.SET_MARK('FETCH',
                             'C_cost',
                             'item_supp_country',
                             NULL);
            fetch C_COST into L_packitem_unit_cost;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_cost',
                             'item_supp_country',
                             NULL);
            close C_COST;
            ---
            L_pack_cost_ex_vat := L_pack_cost_ex_vat
                                + L_packitem_unit_cost * rec.qty;
            ---
            SQL_LIB.SET_MARK('OPEN',
                             'C_VAT_SKU',
                             'vat_item',
                             NULL);
            open  C_VAT_SKU(L_packitem);
            SQL_LIB.SET_MARK('FETCH',
                             'C_VAT_SKU',
                             'vat_item',
                             NULL);
            fetch C_VAT_SKU into L_vat_code,
                                 L_vat_rate,
                                 L_vat_type;
            if C_VAT_SKU%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_VAT_SKU',
                                'vat_item',
                                NULL);
               close C_VAT_SKU;

               O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                     L_packitem,
                                                     To_char(L_vat_region),
                                                     NULL);
               return FALSE;

           else
               SQL_LIB.SET_MARK('CLOSE',
                                'C_vat_item',
                                'vat_item',
                                NULL);
               close C_VAT_SKU;
            end if;
               L_pack_cost_in_vat := L_pack_cost_in_vat
                                   + rec.qty * L_packitem_unit_cost * (1 + L_vat_rate /100);
      END LOOP;

            If L_pack_cost_ex_vat > 0 then
               L_vat_rate := ((L_pack_cost_in_vat / L_pack_cost_ex_vat) - 1) * 100;
               L_vat_code := NULL;
               L_vat_type := NULL;
            else
               L_vat_rate := 0;
               L_vat_code := NULL;
               L_vat_type := NULL;
            end if;
         else
            SQL_LIB.SET_MARK('OPEN',
                             ' C_VAT_SKU',
                             'vat_item',
                             NULL);
            open  C_VAT_SKU(L_item);
            SQL_LIB.SET_MARK('FETCH',
                             ' C_VAT_SKU',
                             'vat_item',
                             NULL);
            fetch C_VAT_SKU into L_vat_code,
                                 L_vat_rate,
                                 L_vat_type;
            if C_VAT_SKU%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                ' C_VAT_SKU',
                                'vat_item',
                                NULL);
               close C_VAT_SKU;
               O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                     L_item,
                                                     To_char(L_vat_region),
                                                     NULL);
                  return FALSE;
               end if;
               SQL_LIB.SET_MARK('CLOSE',
                                ' C_VAT_SKU',
                                'vat_item',
                                NULL);
               close C_VAT_SKU;
         end if; -- pack_item
         L_pack_cost_in_vat := 0;
         L_pack_cost_ex_vat := 0;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if; --item is not null

      update item_supp_country_loc
         set negotiated_item_cost = unit_cost,
             extended_base_cost   = unit_cost,
             inclusive_cost       = unit_cost + (unit_cost * L_vat_rate)/100,
             base_cost            = unit_cost
            where item     = L_item
              and supplier = L_location;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CALC_VAT_COST_TAX;
-----------------------------------------------------------------------------------
END GENERAL_DATA_INSTALL;
/
