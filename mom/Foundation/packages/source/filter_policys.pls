CREATE OR REPLACE PACKAGE FILTER_POLICY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------

GP_FILTER_ORG          VARCHAR2(1) := 'Y';
GP_FILTER_MERCH        VARCHAR2(1) := 'Y';
GP_system_options      SYSTEM_OPTIONS%ROWTYPE;

--------------------------------------------------------------------------------------
-- Name:    SET_USER_FILTER
-- Purpose: This function will be called the first time that the package
--          is called in any one session. It will set the value of the
--          GP_filter_org global variable to 'Y' if the user is asigned to
--          an Organization Level.  It will set the value of the GP_filter_merch
--          global variable to 'Y' if the user is asigned to a Merchandise Level.
--          This function will also set the filtering levels of all the data elements.
--          IF the function is successful it set the global BOOLEAN variable
--          GP_filter_set to TRUE.
--------------------------------------------------------------------------------------
FUNCTION SET_USER_FILTER(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------
-- Name:    ASSEMBLY
-- Purpose: This function accepts four string parameters and assembles them
--          based on the GP_filter_org and GP_filter_merch global variables.
--          The returned string is a complete predicate.
--------------------------------------------------------------------------------------
FUNCTION ASSEMBLY(I_org_null IN VARCHAR2,
                  I_org_exists IN VARCHAR2,
                  I_merch_null IN VARCHAR2,
                  I_merch_exists IN VARCHAR2)
RETURN STRING;

--------------------------------------------------------------------------------------
-- Name:    V_AREA_S
-- Purpose: This function will enforce user select access to the
--          V_AREA view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_AREA_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_CHAIN_S
-- Purpose: This function will enforce user select access to the
--          V_CHAIN view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org and GP_Filter_merch is
--          equal to 'N' then return a value of NULL from the function,
--          otherwise we need to return a dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_CHAIN_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_DIFF_GROUP_HEAD_S
-- Purpose: This function will enforce user select access to the V_DIFF_GROUP_HEAD
--          view. If GP_filter_set is FALSE then call the local function
--          SET_USER_FILTER. If GP_filter_org and GP_Filter_merch is equal to 'N',
--          then return a value of NULL from the function, otherwise we need to return
--          a dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_DIFF_GROUP_HEAD_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_DISTRICT_S
-- Purpose: This function will enforce user select access to the
--          V_DISTRICT view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org and GP_Filter_merch is
--          equal to 'N' then return a value of NULL from the function,
--          otherwise we need to return a dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_DISTRICT_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_GROUP_S
-- Purpose: This function will enforce user select access to the
--          V_GROUPS view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_GROUPS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_DEPS_S
-- Purpose: This function will enforce user select access to the
--          V_DEPS view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_DEPS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_CLASS_S
-- Purpose: This function will enforce user select access to the
--          V_CLASS view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_CLASS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_SUBCLASS_S
-- Purpose: This function will enforce user select access to the
--          V_SUBCLASS view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_SUBCLASS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_DIVISION_S
-- Purpose: This function will enforce user select access to the
--          V_DIVISION view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_DIVISION_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_EXTERNAL_FINISHER_S
-- Purpose: This function will enforce user select access to the
--          V_EXTERNAL_FINISHER view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_EXTERNAL_FINISHER_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_INTERNAL_FINISHER_S
-- Purpose: This function will enforce user select access to the
--          V_INTERNAL_FINISHER view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_INTERNAL_FINISHER_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_ITEM_MASTER_S
-- Purpose: This function will enforce user select access to the
--          V_ITEM_MASTER view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_ITEM_MASTER_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_LOC_LIST_HEAD_S
-- Purpose: This function will enforce user select access to the V_LOC_LIST_HEAD view.
--          If GP_filter_set is FALSE then call the local function SET_USER_FILTER. If
--          GP_filter_merch is equal to 'N' then return a value of NULL from the
--          function, otherwise we need to return a dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_LOC_LIST_HEAD_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_LOC_TRAITS_S
-- Purpose: This function will enforce user select access to the
--          V_LOC_TRAITS view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_LOC_TRAITS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_REGION_S
-- Purpose: This function will enforce user select access to the
--          V_REGION view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_REGION_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_SEASONS_S
-- Purpose: This function will enforce user select access to the
--          V_SEASONS view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org and GP_Filter_merch is
--          equal to 'N' then return a value of NULL from the function,
--          otherwise we need to return a dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_SEASONS_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_SKULIST_HEAD_S
-- Purpose: This function will enforce user select access to the V_SKULIST_HEAD view.
--          If GP_filter_set is FALSE then call the local function SET_USER_FILTER. If
--          GP_filter_merch is equal to 'N' then return a value of NULL from the
--          function, otherwise we need to return a dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_SKULIST_HEAD_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_STORE_S
-- Purpose: This function will enforce user select access to the
--          V_STORE view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_STORE_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_TICKET_TYPE_HEAD_S
-- Purpose: This function will enforce user select access to the V_TICKET_TYPE_HEAD
--          view. If GP_filter_set is FALSE then call the local function
--          SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return a value of
--          NULL from the function, otherwise we need to return a dynamic where clause
--          (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_TICKET_TYPE_HEAD_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_TRANSFER_FROM_STORE_S
-- Purpose: This function will enforce user select access to the
--          V_TRANSFER_FROM_STORE view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_TRANSFER_FROM_STORE_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_TRANSFER_FROM_WH_S
-- Purpose: This function will enforce user select access to the
--          V_TRANSFER_FROM_WH view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_TRANSFER_FROM_WH_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_TRANSFER_TO_STORE_S
-- Purpose: This function will enforce user select access to the
--          V_TRANSFER_TO_STORE VIEW. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_TRANSFER_TO_STORE_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_TRANSFER_TO_WH_S
-- Purpose: This function will enforce user select access to the
--          V_TRANSFER_TO_WH view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_TRANSFER_TO_WH_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_TSF_ENTITY_S
-- Purpose: This function will enforce user select access to the
--          V_TSF_ENTITY view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_TSF_ENTITY_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_UDA_S
-- Purpose: This function will enforce user select access to the
--          V_UDA view. If GP_filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_merch is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_UDA_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_WH_S
-- Purpose: This function will enforce user select access to the
--          V_WH view. If _filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_WH_S(d1 VARCHAR2, d2 VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name: V_MRT_ITEM_S
-- Purpose: This function is used as an oracle policy on thev_mrt_item view.
-- It ensures that a user can only access  MRT items that he has security privileges.
-- It is similar to the existing Filter_Policy_Sql functions.
--------------------------------------------------------------------------------------
FUNCTION V_Mrt_Item_S(d1 IN  VARCHAR2,
                      d2 IN VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name: V_MRT_ITEM_LOC_S
-- Purpose: This function is used as an oracle policy on the v_mrt_item_loc view.
--It ensures that a user can only access MRT items and locations that he has security privilieges for.
--It is similar to the existing Filter_Policy_Sql functions.
--This function only  filters by location, not item.
--The MRT_ITEM_LOC view is only accessed via the MRTLOC form.
--This form is only accessible from the MRT form.
--The items will already have been filtered on the MRT form so we only need to filter by location on the MRTLOC form.
--------------------------------------------------------------------------------------
FUNCTION V_Mrt_Item_Loc_S(d1 IN VARCHAR2,
                          d2 IN VARCHAR2)
RETURN VARCHAR2;

--------------------------------------------------------------------------------------
-- Name:    V_SUPS_S
-- Purpose: This function will enforce user select access to the
--          V_SUPS view. If _filter_set is FALSE then call the local
--          function SET_USER_FILTER. If GP_filter_org is equal to 'N' then return
--          a value of NULL from the function, otherwise we need to return a
--          dynamic where clause (predicate).
--------------------------------------------------------------------------------------
FUNCTION V_SUPS_S(d1 IN VARCHAR2,
                  d2 IN VARCHAR2)
RETURN VARCHAR2;
--------------------------------------------------------------------------------------
END FILTER_POLICY_SQL;
/
