
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUBCLASS_VALIDATE_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------
-- Function Name: EXIST
-- Purpose      : Determines if a given subclass exists within a
--                given department and class.
-- Called by    :
-- Calls        : <none>
-- Input Values : O_error_message
--                I_dept,
--                I_class,
--                I_subclass
--
-- Created      : 26-JUL-96 by Darcie Miller
-- Modified     : 02-AUG-96 by Matt Sniffen
--                updated to meet new standards
---------------------------------------------------------------
   FUNCTION EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_dept          IN     NUMBER,
                  I_class         IN     NUMBER,
                  I_subclass      IN     NUMBER,
                  O_exist         IN OUT BOOLEAN)
            return BOOLEAN;
--------------------------------------------------------------------
-- Function Name: CLASS_SUBCLASS_EXIST
-- Purpose      : Determines if a subclass exist for the given
--                department class combination.
-- Called by    : sublcass.fmb
---------------------------------------------------------------
   FUNCTION CLASS_SUBCLASS_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_dept          IN     DEPS.DEPT%TYPE,
                                 I_class         IN     CLASS.CLASS%TYPE,
                                 O_exist         IN OUT BOOLEAN)
            return BOOLEAN;
--------------------------------------------------------------------
END SUBCLASS_VALIDATE_SQL;
/
