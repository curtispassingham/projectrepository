
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY STORE_VALIDATE_SQL AS
----------------------------------------------------------------------
FUNCTION EXIST(	O_error_message	IN OUT	VARCHAR2,
		I_store		IN	NUMBER,
		O_exist		IN OUT	BOOLEAN)
	RETURN BOOLEAN IS

   L_dummy	VARCHAR2(1);

   cursor C_EXIST is
	select 'x'
	from store
	where store = I_store;

BEGIN
   open C_EXIST;
   fetch C_EXIST into L_dummy;  
   if C_EXIST%NOTFOUND then
	O_exist := FALSE;
	O_error_message := sql_lib.create_msg('INV_STORE',
						null,null,null);
   else
	O_exist := TRUE;
   end if;
   close C_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
	O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
						SQLERRM,
						'STORE_VALIDATE_SQL.EXIST',
						to_char(SQLCODE));
	return FALSE;
END EXIST;
-------------------------------------------------------------------------------
FUNCTION STORE_VALID_FOR_CHAIN (O_error_message  IN OUT         VARCHAR2,
                                O_store_is_valid IN OUT         BOOLEAN,
                                O_store_name     IN OUT         VARCHAR2,
                                I_chain          IN             NUMBER,
                                I_store          IN             NUMBER)

RETURN BOOLEAN is

   L_valid_input_store  VARCHAR2(1) := 'N';

   cursor C_INPUT_STORE is
      select 'Y'  
        from store
       where store = I_store and
             district in (select district
                            from district
                           where region in (select region
                                              from region
                                             where area in (select area
                                                              from area
                                                             where chain = I_chain)));



BEGIN
   ---
   if I_store is NULL or I_chain is NULL then
      O_error_message := ('INV_INPUT_GENERIC');
      return FALSE;
   else
      open C_INPUT_STORE;
      fetch C_INPUT_STORE into L_valid_input_store;
      close C_INPUT_STORE;
      ---
      if L_valid_input_store = 'Y' then
         ---
         if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                      I_store,
                                      O_store_name) = FALSE then
            return FALSE;
         end if;
         ---
         O_store_is_valid := TRUE;
      else
         O_error_message := ('INV_STORE');
         O_store_is_valid := FALSE;
      end if;
      ---
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
	O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
						SQLERRM,
						'STORE_VALIDATE_SQL.STORE_VALID_FOR_CHAIN',
						to_char(SQLCODE));
	return FALSE;
END STORE_VALID_FOR_CHAIN;
---------------------------------------------------------------------------------
FUNCTION GET_CHAIN_FOR_STORE(O_error_message      IN OUT VARCHAR2,
                             I_store              IN     STORE.STORE%TYPE,
                             O_chain              IN OUT CHAIN.CHAIN%TYPE,
                             O_chain_name         IN OUT CHAIN.CHAIN_NAME%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'STORE_VALIDATE_SQL.GET_CHAIN_FOR_STORE';

   cursor C_GET_CHAIN is
      select c.chain, c.chain_name
        from chain c, area a, region r, district d, store s
       where c.chain = a.chain
         and a.area = r.area
         and r.region = d.region
         and d.district = s.district
         and s.store = I_store;

BEGIN

   if I_store is NOT NULL then
      open C_GET_CHAIN;
      fetch C_GET_CHAIN into O_chain,
                             O_chain_name;
      close C_GET_CHAIN;
   else
      O_error_message := sql_lib.create_msg('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_CHAIN_FOR_STORE;      
----------------------------------------------------------------------------------
FUNCTION CHECK_STORE_CURRENCY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_stores          IN       LOC_TBL,
                              I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50)  := 'STORE_VALIDATE_SQL.CHECK_STORE_CURRENCY';

   L_count              NUMBER;
   L_query              VARCHAR2(500);
   L_loc_bind_no        VARCHAR2(1) := '1';
   L_currency_bind_no   VARCHAR2(1) := '2';

BEGIN
   L_query := ' select count(*) '                                                            ||
                ' from store '                                                               ||
               ' where store in (select * '                                                  ||
                                 ' from TABLE (CAST(:' || L_loc_bind_no || ' AS LOC_TBL))) ' ||
                 ' and currency_code = :' || L_currency_bind_no;

   EXECUTE IMMEDIATE L_query into L_count
      using I_stores,
            I_currency_code;

   if I_stores.COUNT != L_count then
      O_error_message := SQL_LIB.CREATE_MSG('DIFF_CURR', I_currency_code, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_STORE_CURRENCY;
----------------------------------------------------------------------------------
FUNCTION CHECK_STORE_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_stores          IN       LOC_TBL,
                             I_country_id      IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50)  := 'STORE_VALIDATE_SQL.CHECK_STORE_COUNTRY';

   L_count                NUMBER(5);

   cursor C_CHECK is
      select count(*)
        from store, addr a, add_type_module atm
       where store in (select * 
                         from TABLE (CAST(I_stores AS LOC_TBL)))
         and a.country_id = I_country_id
         and a.module = 'ST'
         and a.primary_addr_ind = 'Y'
         and a.key_value_1 = store
         and a.addr_type = atm.address_type
         and a.module = atm.module
         and atm.primary_ind = 'Y';

BEGIN

   open C_CHECK;
   fetch C_CHECK into L_count;
   close C_CHECK;

   if I_stores.count != L_count then
      O_error_message := SQL_LIB.CREATE_MSG('DIFF_COUNTRY', I_country_id, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_STORE_COUNTRY;
------------------------------------------------------------------------
FUNCTION VALIDATE_STORES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_stores          IN       LOC_TBL,
                         I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
                         I_country_id      IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50)  := 'STORE_VALIDATE_SQL.VALIDATE_STORES';

   L_count                NUMBER(5);

   cursor C_STORE is 
      select count(*)
        from store s, addr a, add_type_module atm, TABLE (CAST(I_stores AS LOC_TBL)) xloc
       where s.store = value(xloc) 
         and s.currency_code = nvl(I_currency_code,s.currency_code)
         and a.country_id = nvl(I_country_id, a.country_id)
         and a.module = 'ST'
         and a.primary_addr_ind = 'Y'
         and a.key_value_1 = s.store
         and a.addr_type = atm.address_type
         and a.module = atm.module
         and atm.primary_ind = 'Y';
BEGIN

   open C_STORE;
   fetch C_STORE into L_count;
   close C_STORE;

   if I_stores.count != L_count then
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_CURR_CNTRY', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_STORES;
----------------------------------------------------------------------------------
END STORE_VALIDATE_SQL;
/


