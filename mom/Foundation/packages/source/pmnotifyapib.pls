
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY PM_NOTIFY_API_SQL AS
--------------------------------------------------------
FUNCTION NEW_LOCATION( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_new_loc         IN     ITEM_LOC.LOC%TYPE,
                       I_new_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_new_pricing_loc IN     ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                 := 'PM_NOTIFY_API_SQL.NEW_LOCATION';
   
BEGIN
   
   IF MERCH_NOTIFY_API_SQL.NEW_LOCATION( O_error_message,
                                         I_new_loc,
                                         I_new_loc_type,
                                         I_new_pricing_loc) = FALSE THEN
      RETURN FALSE;
   END IF;
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END NEW_LOCATION;

---------------------------------------------------------------------------------------------
FUNCTION NEW_DEPARTMENT( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_new_dept        IN     ITEM_MASTER.DEPT%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                 := 'PM_NOTIFY_API_SQL.NEW_DEPARTMENT';
   
BEGIN
   
   IF MERCH_NOTIFY_API_SQL.NEW_DEPARTMENT( O_error_message,
                                           I_new_dept) = FALSE THEN
      RETURN FALSE;
   END IF;
   RETURN TRUE;   

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END NEW_DEPARTMENT;

---------------------------------------------------------------------------------------------
FUNCTION ITEM_RECLASS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                       I_reclass_date  IN     RECLASS_HEAD.RECLASS_DATE%TYPE,
                       I_new_dept      IN     ITEM_MASTER.DEPT%TYPE,
                       I_new_class     IN     ITEM_MASTER.CLASS%TYPE,
                       I_new_subclass  IN     ITEM_MASTER.SUBCLASS%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                 := 'PM_NOTIFY_API_SQL.ITEM_RECLASS';
   
BEGIN
   
   IF MERCH_NOTIFY_API_SQL.ITEM_RECLASS( O_error_message,
                                         I_item,
                                         I_reclass_date,
                                         I_new_dept,
                                         I_new_class,
                                         I_new_subclass) = FALSE THEN
      RETURN FALSE;
   END IF;
   RETURN TRUE;   

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      RETURN FALSE;
END ITEM_RECLASS;
--------------------------------------------------------------------------------
END;
/
