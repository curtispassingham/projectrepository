
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PM_NOTIFY_API_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------------
-- Function Name: NEW_LOCATION
-- Purpose      : This is a wrapper function for RMS/RPM integration. Gets called from the 
--                storeadd.pc and whadd.pc batch programs when a new store/wh is added. 
--                This will call the RPM function MERCH_NOTIFY_API_SQL.NEW_LOCATION
--                to notify RPM about the new store or warehouse created in RMS.
-- Created By   : Oracle Retail - Suman Guha Mustafi
-- Date         : 18-Oct-06
--------------------------------------------------------------------------------------------------
FUNCTION NEW_LOCATION( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_new_loc         IN     ITEM_LOC.LOC%TYPE,
                       I_new_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                       I_new_pricing_loc IN     ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Function Name: NEW_DEPARTMENT
-- Purpose      : This is a wrapper function for RMS/RPM integration. Gets called from the 
--                Trigger RMS_TABLE_RPM_DEP_BIR on table DEPS upon INSERT.
--                This will call the RPM function MERCH_NOTIFY_API_SQL.NEW_DEPARTMENT to notify
--                RPM about the new department created in RMS.
-- Created By   : Oracle Retail - Suman Guha Mustafi
-- Date         : 18-Oct-06
--------------------------------------------------------j---------------------------------------
FUNCTION NEW_DEPARTMENT( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_new_dept      IN     ITEM_MASTER.DEPT%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Function Name: ITEM_RECLASS
-- Purpose      : This is a wrapper function for RMS/RPM integration. Gets called from 
--                RECLASS_SQL.ITEM_PROCESS after updating dept/class/subclass on ITEM_MASTER. 
--                This will call the RPM function MERCH_NOTIFY_API_SQL.ITEM_RECLASS
--                to notify RPM about the item hierarchy change in RMS.
-- Created By   : Oracle Retail - Suman Guha Mustafi
-- Date         : 18-Oct-06
-----------------------------------------------------------------------------------------------
FUNCTION ITEM_RECLASS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                       I_reclass_date  IN     RECLASS_HEAD.RECLASS_DATE%TYPE,
                       I_new_dept      IN     ITEM_MASTER.DEPT%TYPE,
                       I_new_class     IN     ITEM_MASTER.CLASS%TYPE,
                       I_new_subclass  IN     ITEM_MASTER.SUBCLASS%TYPE)  
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/
