
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_STORE AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC VARIABLES
--------------------------------------------------------------------------------

FAMILY   CONSTANT RIB_SETTINGS.FAMILY%TYPE := 'STORES';

HDR_ADD   CONSTANT VARCHAR2(15) := 'storecre';
HDR_UPD   CONSTANT VARCHAR2(15) := 'storemod';
HDR_DEL   CONSTANT VARCHAR2(15) := 'storedel';
DTL_ADD   CONSTANT VARCHAR2(15) := 'storedtlcre';
DTL_UPD   CONSTANT VARCHAR2(15) := 'storedtlmod';
DTL_DEL   CONSTANT VARCHAR2(15) := 'storedtldel';


--------------------------------------------------------------------------------
-- PUBLIC TYPES
--------------------------------------------------------------------------------

TYPE STORE_KEY_REC IS RECORD
(
STORE              NUMBER,
ADDR_KEY           NUMBER,
STORE_TYPE         VARCHAR2(1),
STOCKHOLDING_IND   VARCHAR2(1),
PRICING_LOC        NUMBER,
PRICING_LOC_CURR   VARCHAR2(3)
);


--------------------------------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg           OUT VARCHAR2,
                I_message_type     IN     VARCHAR2,
                I_store_key_rec    IN     STORE_KEY_REC,
                I_addr_publish_ind IN     ADDR.PUBLISH_IND%TYPE)
RETURN BOOLEAN;

PROCEDURE GETNXT(O_status_code     OUT VARCHAR2,
                 O_error_msg       OUT VARCHAR2,
                 O_message_type    OUT VARCHAR2,
                 O_message         OUT RIB_OBJECT,
                 O_bus_obj_id      OUT RIB_BUSOBJID_TBL,
                 O_routing_info    OUT RIB_ROUTINGINFO_TBL,
                 I_num_threads  IN     NUMBER DEFAULT 1,
                 I_thread_val   IN     NUMBER DEFAULT 1);

PROCEDURE PUB_RETRY(O_status_code     OUT VARCHAR2,
                    O_error_msg       OUT VARCHAR2,
                    O_message_type IN OUT VARCHAR2,
                    O_message         OUT RIB_OBJECT,
                    O_bus_obj_id   IN OUT RIB_BUSOBJID_TBL,
                    O_routing_info IN OUT RIB_ROUTINGINFO_TBL,
                    I_ref_object   IN     RIB_OBJECT);

END RMSMFM_STORE;
/
