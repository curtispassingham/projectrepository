create or replace
PACKAGE ITEM_RETAIL_VALIDATION_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------------------------------
-- Name:    VAL_SELLING_UNIT_RETAIL
-- Purpose: This function will be called from the ADF Item Retail screen to compute Markup percentage, Landed Cost, 
--          UOM Conversion and Currency conversion.
--------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_SELLING_UNIT_RETAIL(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_selling_mark_up               IN OUT   RETAIL_CALC_TEMP.SELLING_MARK_UP%TYPE,
                                 O_selling_unit_retail_prim      IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 O_unit_retail                   IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 O_selling_unit_retail           IN OUT   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                 O_unit_retail_euro              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 O_unit_retail_prim              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 I_zone_id                       IN       COST_ZONE.ZONE_ID%TYPE,
                                 I_unit_cost_prm                 IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                 I_standard_uom                  IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                                 I_selling_uom                   IN       ITEM_LOC.SELLING_UOM%TYPE,
                                 I_item                          IN       ITEM_MASTER.ITEM%TYPE,
                                 I_currency_code                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_currency_prim                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_currency_euro                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_elc_ind                       IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                                 I_rpm_zone_group_id             IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE,
                                 I_multi_selling_uom             IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                 I_multi_unit_retail             IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                 I_multi_units                   IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                 I_standard_uom_trans            IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                                 I_block_query_executed          IN       VARCHAR2,
                                 I_markup_calc_type              IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                                 I_dept                          IN       DEPS.DEPT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- Name:    VAL_MULTI_UNITS
-- Purpose: This function will be called from the ADF Item Retail screen to validate Multi units.
-------------------------------------------------------------------------------------------------------------
FUNCTION VAL_MULTI_UNITS (O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_multi_selling_uom              IN OUT   ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                          O_multi_selling_uom_trans        IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                          O_multi_units                    IN OUT   ITEM_LOC.MULTI_UNITS%TYPE,
                          O_multi_selling_mark_up          IN OUT   RETAIL_CALC_TEMP.MULTI_SELLING_MARK_UP%TYPE,
                          O_multi_unit_retail_prim         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                          O_multi_unit_retail_euro         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                          O_selling_unit_retail_prim       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                          O_selling_unit_retail_euro       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                          O_multi_unit_retail              IN OUT   ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                          I_unit_cost_prm                  IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          I_markup_calc_type               IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                          I_selling_uom                    IN       ITEM_LOC.SELLING_UOM%TYPE,
                          I_zone_id                        IN       COST_ZONE.ZONE_ID%TYPE,
                          I_selling_uom_trans              IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                          I_elc_ind                        IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                          I_multi_unit_retail_prim         IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                          I_item                           IN       ITEM_MASTER.ITEM%TYPE,
                          I_rpm_zone_group_id              IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE,
                          I_standard_uom_trans             IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                          I_selling_unit_retail            IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                          I_standard_uom                   IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                          I_unit_retail                    IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                          I_currency_code                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                          I_currency_prim                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                          I_currency_euro                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                          I_block_query_executed           IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------
-- Name:    VAL_SELLING_MARK_UP
-- Purpose: This function will be called from the ADF Item Retail screen to validate selling mark up. 
-------------------------------------------------------------------------------------------------------------
FUNCTION VAL_SELLING_MARK_UP (O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_selling_mark_up                IN OUT   RETAIL_CALC_TEMP.SELLING_MARK_UP%TYPE,
                              O_old_mkup                       IN OUT   NUMBER,
                              O_unit_retail_prim               IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_selling_uom_trans              IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                              O_selling_unit_retail_prim       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_selling_unit_retail            IN OUT   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom                    IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                              O_multi_unit_retail_prim         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_multi_unit_retail_euro         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_unit_retail                    IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              I_unit_cost_prm                  IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              I_markup_calc_type               IN       DEPS.MARKUP_CALC_TYPE%TYPE, 
                              I_zone_id                        IN       COST_ZONE.ZONE_ID%TYPE,
                              I_standard_uom                   IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                              I_standard_uom_trans             IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                              I_elc_ind                        IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                              I_item                           IN       ITEM_MASTER.ITEM%TYPE,
                              I_rpm_zone_group_id              IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE,
                              I_currency_code                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                              I_dept                           IN       DEPS.DEPT%TYPE,
                              I_currency_prim                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                              I_multi_units                    IN       ITEM_LOC.MULTI_UNITS%TYPE,
                              I_multi_unit_retail              IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                              I_multi_selling_uom              IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                              I_currency_euro                  IN       CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------------
-- Name:    VAL_MULTI_UNIT_RETAIL
-- Purpose: This function will be called from the ADF Item Retail screen to validate multi unit retail. 
---------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_MULTI_UNIT_RETAIL( O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_multi_unit_retail_prim         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_multi_unit_retail_euro         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_unit_retail_euro               IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_multi_selling_mark_up          IN OUT   RETAIL_CALC_TEMP.MULTI_SELLING_MARK_UP%TYPE,
                                O_multi_unit_retail              IN OUT   ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                I_unit_cost_prm                  IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                I_markup_calc_type               IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                                I_zone_id                        IN       COST_ZONE.ZONE_ID%TYPE,
                                I_currency_code                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_currency_prim                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_multi_units                    IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                I_currency_euro                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_multi_selling_uom              IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                I_selling_uom                    IN       ITEM_LOC.SELLING_UOM%TYPE,
                                I_item                           IN       ITEM_MASTER.ITEM%TYPE,
                                I_standard_uom                   IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                                I_standard_uom_trans             IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                                I_selling_unit_retail            IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                I_elc_ind                        IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                                I_rpm_zone_group_id              IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE,
                                I_block_query_executed           IN       VARCHAR2)
RETURN BOOLEAN;                       
----------------------------------------------------------------------------------------------------------------------
-- Name:    VAL_SELLING_UOM_TRANS
--- Purpose: This function will be called from the ADF Item Retail screen to validate selling uom trans. 
-----------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_SELLING_UOM_TRANS (O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_uom                           IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                                O_unit_retail                   IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_unit_retail_prim              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_unit_retail_euro              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_selling_uom_trans             IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                                O_selling_uom                   IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                                O_selling_mark_up               IN OUT   RETAIL_CALC_TEMP.SELLING_MARK_UP%TYPE,
                                I_standard_uom_trans            IN       UOM_CLASS.UOM%TYPE,
                                I_zone_id                       IN       COST_ZONE.ZONE_ID%TYPE,
                                I_selling_unit_retail           IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                I_currency_code                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_markup_calc_type              IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                                I_unit_cost_prm                 IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                I_item                          IN       ITEM_MASTER.ITEM%TYPE,
                                I_standard_uom                  IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                                I_currency_prim                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_currency_euro                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_multi_unit_retail             IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                I_multi_units                   IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                I_multi_selling_uom             IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                I_block_query_executed          IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------------------------
-- Name:    VAL_MULTI_SELLING_UOM_TRANS
-- Purpose: This function will be called from the ADF Item Retail screen to validate multi selling uom trans. 
------------------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_MULTI_SELLING_UOM_TRANS(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_multi_selling_uom              IN OUT   ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                     O_multi_selling_uom_trans        IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                                     O_multi_unit_retail_prim         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                     O_selling_unit_retail_prim       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                     O_multi_unit_retail_euro         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                     O_multi_selling_mark_up          IN OUT   RETAIL_CALC_TEMP.MULTI_SELLING_MARK_UP%TYPE,
                                     O_selling_unit_retail_euro       IN OUT   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                     I_markup_calc_type               IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                                     I_zone_id                        IN       COST_ZONE.ZONE_ID%TYPE,
                                     I_unit_cost_prm                  IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                     I_multi_units                    IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                     I_multi_unit_retail              IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                     I_item                           IN       ITEM_MASTER.ITEM%TYPE,
                                     I_selling_uom                    IN       ITEM_LOC.SELLING_UOM%TYPE,
                                     I_standard_uom_trans             IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                                     I_standard_uom                   IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                                     I_selling_unit_retail            IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                     I_unit_retail                    IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                     I_currency_code                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                     I_currency_prim                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                     I_currency_euro                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                     I_selling_uom_trans              IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                                     I_block_query_executed           IN       VARCHAR2)

RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------------------
-- Name:    VAL_PRE_FORM
-- Purpose: Get item costing info for gtax/svat if supplier information has been entered for the item. 
---         (no supplier means no cost)
--          This function will be called from the ADF Item Retail screen
------------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_PRE_FORM  (O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_selling_uom                 IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                        O_standard_uom                IN OUT   ITEM_MASTER.STANDARD_UOM%TYPE,
                        O_elc_ind                     IN OUT   SYSTEM_OPTIONS.ELC_IND%TYPE,
                        O_unit_cost_prm               IN OUT   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                        I_item                        IN       ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------------
END ITEM_RETAIL_VALIDATION_SQL;
/