CREATE OR REPLACE PACKAGE BODY CORESVC_SEASONS AS
   cursor c_svc_seasons(I_process_id number,
                        I_chunk_id   number) is
      select t.pk_seasons_rid,
             t.pk_phases_rid,
             t.phs_ssn_fk_rid,
             t.s_rid,
             t.p_rid,
             t.season_id,
             LEAD(t.season_id, 1, 0) OVER (ORDER BY t.season_id) as next_season_id,
             row_number() over (partition BY t.season_id order by t.season_id) as s_rank,
             t.season_desc,
             t.start_date,
             t.end_date,
             t.old_start_date,
             t.old_end_date,
             t.p_season_id,
             t.phase_id,
             t.phase_desc,
             t.p_start_date,
             t.p_end_date,
             t.filter_merch_id,
             t.filter_org_id,
             t.filter_merch_id_class,
             t.filter_merch_id_subclass,
             t.s_process_id,
             t.s_row_seq,
             t.s_chunk_id,
             t.s_action,
             t.p_process_id,
             t.p_row_seq,
             t.p_chunk_id,
             t.p_action
from (select pk_seasons.season_id                                                  as pk_seasons_rid,
             pk_phases.rowid                                                       as pk_phases_rid,
             pk_seasons.season_id                                                  as phs_ssn_fk_rid,
             sts.rowid as s_rid,
             stp.rowid as p_rid,
             sts.season_id,
             sts.season_desc,
             sts.start_date,
             sts.end_date,
             pk_seasons.start_date                                                 as old_start_date,
             pk_seasons.end_date                                                   as old_end_date,
             stp.season_id                                                         as p_season_id,
             stp.phase_id,
             stp.phase_desc,
             stp.start_date                                                        as p_start_date,
             stp.end_date                                                          as p_end_date,
             sts.filter_merch_id,
             sts.filter_org_id,
             sts.filter_merch_id_class,
             sts.filter_merch_id_subclass,
             sts.process_id                                                        as s_process_id,
             sts.row_seq                                                           as s_row_seq,
             sts.chunk_id                                                          as s_chunk_id,
             upper(sts.action)                                                     as s_action,
             stp.process_id                                                        as p_process_id,
             stp.row_seq                                                           as p_row_seq,
             stp.chunk_id                                                          as p_chunk_id,
             upper(stp.action)                                                     as p_action
        from SVC_SEASONS  sts,
             SVC_PHASES   stp,
             V_SEASONS    pk_seasons,
             PHASES       pk_phases
       where sts.process_id   = I_process_id
         and sts.chunk_id     = I_chunk_id
         and sts.process_id   = stp.process_id(+)
         and sts.chunk_id     = stp.chunk_id(+)
         and sts.season_id    = pk_seasons.season_id(+)
         and stp.phase_id     = pk_phases.phase_id(+)
         and stp.season_id    = pk_phases.season_id(+)
         and sts.season_id    = stp.season_id(+)

      UNION ALL

      select phs_ssn_fk.season_id                                    as pk_seasons_rid,
             pk_phases.rowid                                         as pk_phases_rid,
             phs_ssn_fk.season_id                                    as phs_ssn_fk_rid,
             sts.rowid as s_rid,
             stp.rowid as p_rid,
             nvl(sts.season_id,phs_ssn_fk.season_id)                 as season_id,
             sts.season_desc,
             sts.start_date,
             sts.end_date,
             phs_ssn_fk.start_date                                   as old_start_date,
             phs_ssn_fk.end_date                                     as old_end_date,
             stp.season_id                                           as p_season_id,
             stp.phase_id,
             stp.phase_desc,
             stp.start_date                                          as p_start_date,
             stp.end_date                                            as p_end_date,
             sts.filter_merch_id,
             sts.filter_org_id,
             sts.filter_merch_id_class,
             sts.filter_merch_id_subclass,
             sts.process_id,
             sts.row_seq,
             sts.chunk_id,
             upper(sts.action)                                       as s_action,
             stp.process_id,
             stp.row_seq,
             stp.chunk_id,
             upper(stp.action)                                       as p_action
        from SVC_PHASES        stp,
             SVC_SEASONS       sts,
             PHASES            pk_phases,
             V_SEASONS         phs_ssn_fk
       where stp.process_id    = I_process_id
         and stp.chunk_id      = I_chunk_id
         and stp.process_id    = sts.process_id(+)
         and stp.chunk_id      = sts.chunk_id(+)
         and stp.phase_id      = pk_phases.phase_id (+)
         and stp.season_id     = pk_phases.season_id (+)
         and stp.season_id     = phs_ssn_fk.season_id (+)
         and stp.season_id     = sts.season_id(+)
         and sts.season_id is NULL) t;

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab         errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab     s9t_errors_tab_typ;

   LP_module_merch_level SYSTEM_OPTIONS.SEASON_MERCH_LEVEL_CODE%TYPE;
   LP_module_org_level   SYSTEM_OPTIONS.SEASON_ORG_LEVEL_CODE%TYPE;

   Type SEASONS_TL_TAB IS TABLE OF SEASONS_TL%ROWTYPE;
   Type PHASES_TL_TAB IS TABLE OF PHASES_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
   LP_user            SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);

   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
---------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets        S9T_PKG.NAMES_MAP_TYP;
   SEASONS_cols    S9T_PKG.NAMES_MAP_TYP;
   PHASES_cols     S9T_PKG.NAMES_MAP_TYP;
   SEASONS_TL_cols S9T_PKG.NAMES_MAP_TYP;
   PHASES_TL_cols  S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                        := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   SEASONS_cols                    := S9T_PKG.GET_COL_NAMES(I_file_id,SEASONS_sheet);
   SEASONS$ACTION                  := SEASONS_cols('ACTION');
   SEASONS$SEASON_ID               := SEASONS_cols('SEASON_ID');
   SEASONS$SEASON_DESC             := SEASONS_cols('SEASON_DESC');
   SEASONS$START_DATE              := SEASONS_cols('START_DATE');
   SEASONS$END_DATE                := SEASONS_cols('END_DATE');
   SEASONS$FILTER_ORG_ID           := SEASONS_cols('FILTER_ORG_ID');
   SEASONS$FILTER_MERCH_ID         := SEASONS_cols('FILTER_MERCH_ID');
   SEASONS$FILTER_MERCH_ID_CLASS   := SEASONS_cols('FILTER_MERCH_ID_CLASS');
   SEASONS$FILTER_MERCH_ID_SCLASS  := SEASONS_cols('FILTER_MERCH_ID_SUBCLASS');

   seasons_tl_cols                 := S9T_PKG.GET_COL_NAMES(I_file_id,SEASONS_TL_SHEET);
   seasons_tl$ACTION               := seasons_tl_cols('ACTION');
   seasons_tl$LANG                 := seasons_tl_cols('LANG');
   seasons_tl$SEASON_ID            := seasons_tl_cols('SEASON_ID');
   seasons_tl$SEASON_DESC          := seasons_tl_cols('SEASON_DESC');

   PHASES_cols                     := S9T_PKG.GET_COL_NAMES(I_file_id,PHASES_sheet);
   PHASES$ACTION                   := PHASES_cols('ACTION');
   PHASES$SEASON_ID                := PHASES_cols('SEASON_ID');
   PHASES$PHASE_ID                 := PHASES_cols('PHASE_ID');
   PHASES$PHASE_DESC               := PHASES_cols('PHASE_DESC');
   PHASES$START_DATE               := PHASES_cols('START_DATE');
   PHASES$END_DATE                 := PHASES_cols('END_DATE');

   PHASES_TL_cols                  := S9T_PKG.GET_COL_NAMES(I_file_id,PHASES_TL_sheet);
   PHASES_TL$ACTION                := PHASES_TL_cols('ACTION');
   PHASES_TL$LANG                  := PHASES_TL_cols('LANG');
   PHASES_TL$SEASON_ID             := PHASES_TL_cols('SEASON_ID');
   PHASES_TL$PHASE_ID              := PHASES_TL_cols('PHASE_ID');
   PHASES_TL$PHASE_DESC            := PHASES_TL_cols('PHASE_DESC');
END POPULATE_NAMES;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SEASONS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SEASONS_sheet )
   select s9t_row(s9t_cells(CORESVC_SEASONS.action_mod ,
                            SEASON_ID,
                            SEASON_DESC,
                            START_DATE,
                            END_DATE,
                            FILTER_ORG_ID,
                            FILTER_MERCH_ID,
                            FILTER_MERCH_ID_CLASS,
                            FILTER_MERCH_ID_SUBCLASS
                            ))
     from v_seasons ;
END POPULATE_SEASONS;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SEASONS_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SEASONS_TL_SHEET )
   select s9t_row(s9t_cells(CORESVC_SEASONS.action_mod ,
                            LANG,
                            SEASON_ID,
                            SEASON_DESC))
     from v_seasons_tl ;
END POPULATE_SEASONS_TL;

----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_PHASES( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = PHASES_sheet )
   select s9t_row(s9t_cells(CORESVC_SEASONS.action_mod ,
                            phases.SEASON_ID,
                            PHASE_ID,
                            PHASE_DESC,
                            phases.START_DATE,
                            phases.END_DATE
                            ))
     from phases,
          v_seasons
    where phases.season_id = v_seasons.season_id ;
END POPULATE_PHASES;
----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_PHASES_TL( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = PHASES_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_SEASONS.action_mod ,
                            lang,
                            phases_tl.season_id,
                            phase_id,
                            phase_desc))
     from phases_tl,
          v_seasons
    where phases_tl.season_id = v_seasons.season_id ;
END POPULATE_PHASES_TL;
---------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id IN OUT NUMBER) IS
   L_file      S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||LP_user||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(SEASONS_sheet);
   L_file.sheets(l_file.get_sheet_index(SEASONS_sheet)).column_headers := s9t_cells('ACTION',
                                                                                    'SEASON_ID',
                                                                                    'SEASON_DESC',
                                                                                    'START_DATE',
                                                                                    'END_DATE',
                                                                                    'FILTER_ORG_ID',
                                                                                    'FILTER_MERCH_ID',
                                                                                    'FILTER_MERCH_ID_CLASS',
                                                                                    'FILTER_MERCH_ID_SUBCLASS'
                                                                                   );

   L_file.add_sheet(SEASONS_TL_SHEET);
   L_file.sheets(l_file.get_sheet_index(SEASONS_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                       'LANG',
                                                                                       'SEASON_ID',
                                                                                       'SEASON_DESC');

   L_file.add_sheet(PHASES_sheet);
   L_file.sheets(l_file.get_sheet_index(PHASES_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                    'SEASON_ID',
                                                                                    'PHASE_ID',
                                                                                    'PHASE_DESC',
                                                                                    'START_DATE',
                                                                                    'END_DATE'
                                                                                    );

   L_file.add_sheet(PHASES_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(PHASES_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                      'LANG',
                                                                                      'SEASON_ID',
                                                                                      'PHASE_ID',
                                                                                      'PHASE_DESC');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;

----------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_SEASONS.CREATE_S9T';
   L_file    S9T_FILE;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key)= FALSE   then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_SEASONS(O_file_id);
      POPULATE_PHASES(O_file_id);
      POPULATE_SEASONS_TL(O_file_id);
      POPULATE_PHASES_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SEASONS( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id IN   SVC_SEASONS.process_id%TYPE) IS
   TYPE svc_SEASONS_col_typ IS TABLE OF SVC_SEASONS%ROWTYPE;
   L_temp_rec      SVC_SEASONS%ROWTYPE;
   svc_SEASONS_col svc_SEASONS_col_typ             :=NEW svc_SEASONS_col_typ();
   L_process_id    SVC_SEASONS.process_id%TYPE;
   L_error         BOOLEAN                         :=FALSE;
   L_default_rec SVC_SEASONS%ROWTYPE;
   cursor C_MANDATORY_IND is
      select season_id_mi,
             season_desc_mi,
             start_date_mi,
             end_date_mi,
             filter_org_id_mi,
             filter_merch_id_mi,
             filter_merch_id_class_mi,
             filter_merch_id_subclass_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'SEASONS' )
               PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ( 'SEASON_ID'                as season_id,
                                                                 'SEASON_DESC'              as season_desc,
                                                                 'START_DATE'               as start_date,
                                                                 'END_DATE'                 as end_date,
                                                                 'FILTER_ORG_ID'            as filter_org_id,
                                                                 'FILTER_MERCH_ID'          as filter_merch_id,
                                                                 'FILTER_MERCH_ID_CLASS'    as filter_merch_id_class,
                                                                 'FILTER_MERCH_ID_SUBCLASS' as filter_merch_id_subclass,
                                                                  NULL                      as dummy));
      l_mi_rec   C_MANDATORY_IND%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA     exception_init(dml_errors, -24381);
      L_table         VARCHAR2(30)   := 'SVC_SEASONS';
      L_pk_columns    VARCHAR2(255)  := 'Season';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  season_id_dv,
                       season_desc_dv,
                       start_date_dv,
                       end_date_dv,
                       filter_org_id_dv,
                       filter_merch_id_dv,
                       filter_merch_id_class_dv,
                       filter_merch_id_subclass_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key          = template_key
                          and wksht_key             = 'SEASONS')
                        PIVOT (MAX(default_value) AS dv FOR(column_key)IN('SEASON_ID'                as season_id,
                                                                          'SEASON_DESC'              as season_desc,
                                                                          'START_DATE'               as start_date,
                                                                          'END_DATE'                 as end_date,
                                                                          'FILTER_ORG_ID'            as filter_org_id,
                                                                          'FILTER_MERCH_ID'          as filter_merch_id,
                                                                          'FILTER_MERCH_ID_CLASS'    as filter_merch_id_class,
                                                                          'FILTER_MERCH_ID_SUBCLASS' as filter_merch_id_subclass,
                                                                           NULL                      as dummy)))
   LOOP
      BEGIN
         L_default_rec.filter_merch_id_subclass := rec.filter_merch_id_subclass_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SEASONS ' ,
                             NULL,
                            'FILTER_MERCH_ID_SUBCLASS ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.filter_merch_id_class := rec.filter_merch_id_class_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SEASONS ' ,
                             NULL,
                            'FILTER_MERCH_ID_CLASS ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.filter_merch_id := rec.filter_merch_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SEASONS ' ,
                             NULL,
                            'FILTER_MERCH_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.filter_org_id := rec.filter_org_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SEASONS ' ,
                             NULL,
                            'FILTER_ORG_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.season_id := rec.season_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SEASONS ' ,
                             NULL,
                            'SEASON_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.end_date := TO_DATE(rec.end_date_dv,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SEASONS ' ,
                             NULL,
                            'END_DATE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.start_date := TO_DATE(rec.start_date_dv,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SEASONS ' ,
                             NULL,
                            'START_DATE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.season_desc := rec.season_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'SEASONS ' ,
                             NULL,
                            'SEASON_DESC ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(seasons$action)                   as action,
          r.get_cell(seasons$season_id)                as season_id,
          r.get_cell(seasons$season_desc)              as season_desc,
          r.get_cell(seasons$start_date)               as start_date,
          r.get_cell(seasons$end_date)                 as end_date,
          r.get_cell(seasons$filter_org_id)            as filter_org_id,
          r.get_cell(seasons$filter_merch_id)          as filter_merch_id,
          r.get_cell(seasons$filter_merch_id_class)    as filter_merch_id_class,
          r.get_cell(seasons$filter_merch_id_sclass)   as filter_merch_id_subclass,
          r.get_row_seq()                              as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(SEASONS_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.filter_merch_id_subclass := rec.filter_merch_id_subclass;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID_SUBCLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.filter_merch_id_class := rec.filter_merch_id_class;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID_CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.filter_merch_id := rec.filter_merch_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.filter_org_id := rec.filter_org_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            'FILTER_ORG_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.season_id := rec.season_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            'SEASON_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.end_date := TO_DATE(rec.end_date,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            'END_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.start_date := TO_DATE(rec.start_date,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            'START_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.season_desc := rec.season_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_sheet,
                            rec.row_seq,
                            'SEASON_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SEASONS.action_new then
         L_temp_rec.filter_merch_id_subclass := NVL( L_temp_rec.filter_merch_id_subclass,
                                                     L_default_rec.filter_merch_id_subclass);
         L_temp_rec.filter_merch_id_class    := NVL( L_temp_rec.filter_merch_id_class,
                                                     L_default_rec.filter_merch_id_class);
         L_temp_rec.filter_merch_id          := NVL( L_temp_rec.filter_merch_id,
                                                     L_default_rec.filter_merch_id);
         L_temp_rec.filter_org_id            := NVL( L_temp_rec.filter_org_id,
                                                     L_default_rec.filter_org_id);
         L_temp_rec.season_id                := NVL( L_temp_rec.season_id    ,
                                                     L_default_rec.season_id);
         L_temp_rec.end_date                 := NVL( L_temp_rec.end_date     ,
                                                     L_default_rec.end_date);
         L_temp_rec.start_date               := NVL( L_temp_rec.start_date   ,
                                                     L_default_rec.start_date);
         L_temp_rec.season_desc              := NVL( L_temp_rec.season_desc  ,
                                                     L_default_rec.season_desc);
      end if;
      if L_temp_rec.season_id is NULL then
         WRITE_S9T_ERROR( I_file_id,
                          SEASONS_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));

         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_seasons_col.extend();
         svc_seasons_col(svc_seasons_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_SEASONS_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SEASONS st
      using(select(case
                   when L_mi_rec.season_id_mi     = 'N'
                    and svc_seasons_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.season_id is null
                   then mt.season_id
                   else s1.season_id
                   end) as season_id,
                  (case
                   when L_mi_rec.season_desc_mi    = 'N'
                    and svc_seasons_col(i).action  = CORESVC_SEASONS.action_mod
                    and s1.season_desc is null
                   then mt.season_desc
                   else s1.season_desc
                   end) as season_desc,
                  (case
                   when L_mi_rec.start_date_mi    = 'N'
                    and svc_seasons_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.start_date is null
                   then mt.start_date
                   else s1.start_date
                   end) as start_date,
                  (case
                   when L_mi_rec.end_date_mi      = 'N'
                    and svc_seasons_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.end_date is null
                   then mt.end_date
                   else s1.end_date
                   end) as end_date,
                  (case
                   when L_mi_rec.filter_org_id_mi = 'N'
                    and svc_seasons_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.filter_org_id is null
                   then mt.filter_org_id
                   else s1.filter_org_id
                   end) as filter_org_id,
                  (case
                   when L_mi_rec.filter_merch_id_mi = 'N'
                    and svc_seasons_col(i).action   = CORESVC_SEASONS.action_mod
                    and s1.filter_merch_id is null
                   then mt.filter_merch_id
                   else s1.filter_merch_id
                   end) as filter_merch_id,
                  (case
                   when L_mi_rec.filter_merch_id_class_mi  = 'N'
                    and svc_seasons_col(i).action          = CORESVC_SEASONS.action_mod
                    and s1.filter_merch_id_class is null
                   then mt.filter_merch_id_class
                   else s1.filter_merch_id_class
                   end) as filter_merch_id_class,
                  (case
                   when l_mi_rec.filter_merch_id_subclass_mi = 'N'
                    and svc_seasons_col(i).action            = CORESVC_SEASONS.action_mod
                    and s1.filter_merch_id_subclass is null
                   then mt.filter_merch_id_subclass
                   else s1.filter_merch_id_subclass
                   end) as filter_merch_id_subclass,
                  null as dummy
              from(select svc_seasons_col(i).season_id                as season_id,
                          svc_seasons_col(i).season_desc              as season_desc,
                          svc_seasons_col(i).start_date               as start_date,
                          svc_seasons_col(i).end_date                 as end_date,
                          svc_seasons_col(i).filter_org_id            as filter_org_id,
                          svc_seasons_col(i).filter_merch_id          as filter_merch_id,
                          svc_seasons_col(i).filter_merch_id_class    as filter_merch_id_class,
                          svc_seasons_col(i).filter_merch_id_subclass as filter_merch_id_subclass,
                          null as dummy
                      from dual ) s1,
                   v_seasons mt
             where mt.season_id (+)     = s1.season_id
               and 1 = 1 )sq on (st.season_id      = sq.season_id
                                 and svc_seasons_col(i).action IN (CORESVC_SEASONS.action_mod,
                                                                  CORESVC_SEASONS.action_del)
                                 )
      when matched then
      update
         set process_id                  = svc_seasons_col(i).process_id ,
             chunk_id                    = svc_seasons_col(i).chunk_id ,
             row_seq                     = svc_seasons_col(i).row_seq ,
             action                      = svc_seasons_col(i).action ,
             process$status              = svc_seasons_col(i).process$status ,
             season_desc                 = sq.season_desc ,
             start_date                  = sq.start_date ,
             end_date                    = sq.end_date ,
             filter_org_id               = sq.filter_org_id ,
             filter_merch_id             = sq.filter_merch_id ,
             filter_merch_id_class       = sq.filter_merch_id_class ,
             filter_merch_id_subclass    = sq.filter_merch_id_subclass ,
             create_id                   = svc_seasons_col(i).create_id ,
             create_datetime             = svc_seasons_col(i).create_datetime ,
             last_upd_id                 = svc_seasons_col(i).last_upd_id ,
             last_upd_datetime           = svc_seasons_col(i).last_upd_datetime
      when not matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             season_id ,
             season_desc ,
             start_date ,
             end_date ,
             filter_org_id ,
             filter_merch_id ,
             filter_merch_id_class ,
             filter_merch_id_subclass ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_seasons_col(i).process_id ,
             svc_seasons_col(i).chunk_id ,
             svc_seasons_col(i).row_seq ,
             svc_seasons_col(i).action ,
             svc_seasons_col(i).process$status ,
             sq.season_id ,
             sq.season_desc ,
             sq.start_date ,
             sq.end_date ,
             sq.filter_org_id ,
             sq.filter_merch_id ,
             sq.filter_merch_id_class ,
             sq.filter_merch_id_subclass ,
             svc_seasons_col(i).create_id ,
             svc_seasons_col(i).create_datetime ,
             svc_seasons_col(i).last_upd_id ,
             svc_seasons_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
               if L_error_code=1 then
                  L_error_code:=NULL;
                  L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
               end if;
            WRITE_S9T_ERROR( I_file_id,
                             SEASONS_sheet,
                             svc_SEASONS_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SEASONS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SEASONS_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_SEASONS_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_SEASONS_TL_COL_TYP IS TABLE OF SVC_SEASONS_TL%ROWTYPE;
   L_temp_rec            SVC_SEASONS_TL%ROWTYPE;
   SVC_SEASONS_TL_COL    SVC_SEASONS_TL_COL_TYP := NEW SVC_SEASONS_TL_COL_TYP();
   L_process_id          SVC_SEASONS_TL.PROCESS_ID%TYPE;
   L_error               BOOLEAN := FALSE;
   L_default_rec         SVC_SEASONS_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select season_desc_mi,
             season_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'SEASONS_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('SEASON_DESC' as season_desc,
                                       'SEASON_ID'   as season_id,
                                       'LANG'        as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_SEASONS_TL';
   L_pk_columns    VARCHAR2(255)  := 'Season ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select season_desc_dv,
                      season_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'SEASONS_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('SEASON_DESC' as season_desc,
                                                'SEASON_ID'   as season_id,
                                                'LANG'        as lang)))
   LOOP
      BEGIN
         L_default_rec.season_desc := rec.season_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_TL_SHEET ,
                            NULL,
                           'SEASON_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.season_id := rec.season_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           SEASONS_TL_SHEET ,
                            NULL,
                           'SEASON_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(seasons_TL$action))    as action,
                      r.get_cell(seasons_TL$season_desc)      as season_desc,
                      r.get_cell(seasons_TL$season_id)        as season_id,
                      r.get_cell(seasons_TL$lang)             as lang,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(SEASONS_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            seasons_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.season_desc := rec.season_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_TL_SHEET,
                            rec.row_seq,
                            'SEASON_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.season_id := rec.season_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_TL_SHEET,
                            rec.row_seq,
                            'SEASON_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEASONS_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_seasons.action_new then
         L_temp_rec.season_desc := NVL( L_temp_rec.season_desc,L_default_rec.season_desc);
         L_temp_rec.season_id   := NVL( L_temp_rec.season_id,L_default_rec.season_id);
         L_temp_rec.lang        := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.season_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         SEASONS_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_seasons_TL_col.extend();
         svc_seasons_TL_col(svc_seasons_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_seasons_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_seasons_TL st
      using(select
                  (case
                   when l_mi_rec.season_desc_mi = 'N'
                    and svc_seasons_TL_col(i).action = CORESVC_seasons.action_mod
                    and s1.season_desc IS NULL then
                        mt.season_desc
                   else s1.season_desc
                   end) as season_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_seasons_TL_col(i).action = CORESVC_seasons.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.season_id_mi = 'N'
                    and svc_seasons_TL_col(i).action = CORESVC_seasons.action_mod
                    and s1.season_id IS NULL then
                        mt.season_id
                   else s1.season_id
                   end) as season_id
              from (select svc_seasons_TL_col(i).season_desc as season_desc,
                           svc_seasons_TL_col(i).season_id        as season_id,
                           svc_seasons_TL_col(i).lang              as lang
                      from dual) s1,
                   seasons_TL mt
             where mt.season_id (+) = s1.season_id
               and mt.lang (+)       = s1.lang) sq
                on (st.season_id = sq.season_id and
                    st.lang = sq.lang and
                    svc_seasons_TL_col(i).ACTION IN (CORESVC_seasons.action_mod,CORESVC_seasons.action_del))
      when matched then
      update
         set process_id        = svc_seasons_TL_col(i).process_id ,
             chunk_id          = svc_seasons_TL_col(i).chunk_id ,
             row_seq           = svc_seasons_TL_col(i).row_seq ,
             action            = svc_seasons_TL_col(i).action ,
             process$status    = svc_seasons_TL_col(i).process$status ,
             season_desc = sq.season_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             season_desc ,
             season_id ,
             lang)
      values(svc_seasons_TL_col(i).process_id ,
             svc_seasons_TL_col(i).chunk_id ,
             svc_seasons_TL_col(i).row_seq ,
             svc_seasons_TL_col(i).action ,
             svc_seasons_TL_col(i).process$status ,
             sq.season_desc ,
             sq.season_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            SEASONS_TL_SHEET,
                            svc_seasons_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SEASONS_TL;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_PHASES( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id IN   SVC_PHASES.PROCESS_ID%TYPE) IS

   TYPE svc_PHASES_col_typ IS TABLE OF SVC_PHASES%ROWTYPE;
   L_temp_rec      SVC_PHASES%ROWTYPE;
   svc_PHASES_col  svc_PHASES_col_typ           :=NEW svc_PHASES_col_typ();
   L_process_id    SVC_PHASES.process_id%TYPE;
   L_error         BOOLEAN                      :=FALSE;
   L_default_rec   SVC_PHASES%ROWTYPE;

   cursor C_MANDATORY_IND is
      select season_id_mi,
             phase_id_mi,
             phase_desc_mi,
             start_date_mi,
             end_date_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'PHASES'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('SEASON_ID'  as season_id,
                                            'PHASE_ID'   as phase_id,
                                            'PHASE_DESC' as phase_desc,
                                            'START_DATE' as start_date,
                                            'END_DATE'   as end_date,
                                             null as dummy));
      L_mi_rec   C_MANDATORY_IND%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA     exception_init(dml_errors, -24381);
      L_table         VARCHAR2(30)   := 'SVC_PHASES';
      L_pk_columns    VARCHAR2(255)  := 'Season,Phase';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  season_id_dv,
                       phase_id_dv,
                       phase_desc_dv,
                       start_date_dv,
                       end_date_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                    = template_key
                          and wksht_key                                       = 'PHASES' )
                        PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('SEASON_ID'  as season_id,
                                                                             'PHASE_ID'   as phase_id,
                                                                             'PHASE_DESC' as phase_desc,
                                                                             'START_DATE' as start_date,
                                                                             'END_DATE'   as end_date,
                                                                              NULL        as dummy)))
   LOOP
      BEGIN
         L_default_rec.end_date := TO_DATE(rec.end_date_dv,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'PHASES ' ,
                             NULL,
                            'END_DATE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.start_date := TO_DATE(rec.start_date_dv,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'PHASES ' ,
                             NULL,
                            'START_DATE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.phase_desc := rec.phase_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'PHASES ' ,
                             NULL,
                            'PHASE_DESC ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.phase_id := rec.phase_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'PHASES ' ,
                             NULL,
                            'PHASE_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.season_id := rec.season_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'PHASES ' ,
                             NULL,
                            'SEASON_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(phases$action)               as action,
          r.get_cell(phases$season_id)            as season_id,
          r.get_cell(phases$phase_id)             as phase_id,
          r.get_cell(phases$phase_desc)           as phase_desc,
          r.get_cell(phases$start_date)           as start_date,
          r.get_cell(phases$end_date)             as end_date,
          r.get_row_seq()                         as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(PHASES_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.end_date := TO_DATE(rec.end_date,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_sheet,
                            rec.row_seq,
                            'END_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.start_date := TO_DATE(rec.start_date, 'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_sheet,
                            rec.row_seq,
                            'START_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.phase_desc := rec.phase_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_sheet,
                            rec.row_seq,
                            'PHASE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.phase_id := rec.phase_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_sheet,
                            rec.row_seq,
                            'PHASE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.season_id := rec.season_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_sheet,
                            rec.row_seq,
                            'SEASON_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SEASONS.action_new then
         L_temp_rec.end_date   := NVL( L_temp_rec.end_date  ,
                                       L_default_rec.end_date);
         L_temp_rec.start_date := NVL( L_temp_rec.start_date,
                                       L_default_rec.start_date);
         L_temp_rec.phase_desc := NVL( L_temp_rec.phase_desc,
                                       L_default_rec.phase_desc);
         L_temp_rec.phase_id   := NVL( L_temp_rec.phase_id  ,
                                       L_default_rec.phase_id);
         L_temp_rec.season_id  := NVL( L_temp_rec.season_id ,
                                       L_default_rec.season_id);
      end if;
      if NOT ( L_temp_rec.phase_id is NOT NULL and
               L_temp_rec.season_id is NOT NULL and
               1 = 1 )   then
         WRITE_S9T_ERROR( I_file_id,
                         PHASES_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_phases_col.extend();
         svc_phases_col(svc_phases_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_PHASES_col.COUNT SAVE EXCEPTIONS
      merge into SVC_PHASES st
      using(select(case
                   when L_mi_rec.season_id_mi    = 'N'
                    and svc_phases_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.season_id is null
                   then mt.season_id
                   else s1.season_id
                   end) as season_id,
                  (case
                   when L_mi_rec.phase_id_mi     = 'N'
                    and svc_phases_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.phase_id is null
                   then mt.phase_id
                   else s1.phase_id
                   end) as phase_id,
                  (case
                   when L_mi_rec.phase_desc_mi    = 'N'
                    and svc_phases_col(i).action  = CORESVC_SEASONS.action_mod
                    and s1.phase_desc is null
                   then mt.phase_desc
                   else s1.phase_desc
                   end) as phase_desc,
                  (case
                   when L_mi_rec.start_date_mi    = 'N'
                    and svc_phases_col(i).action  = CORESVC_SEASONS.action_mod
                    and s1.start_date is null
                   then mt.start_date
                   else s1.start_date
                   end) as start_date,
                  (case
                   when L_mi_rec.end_date_mi     = 'N'
                    and svc_phases_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.end_date is null
                   then mt.end_date
                   else s1.end_date
                   end) as end_date,
                  null as dummy
              from(select svc_phases_col(i).season_id  as season_id,
                          svc_phases_col(i).phase_id   as phase_id,
                          svc_phases_col(i).phase_desc as phase_desc,
                          svc_phases_col(i).start_date as start_date,
                          svc_phases_col(i).end_date   as end_date,
                          null as dummy
                      from dual ) s1,
                   phases mt
             where mt.phase_id (+)       = s1.phase_id
               and mt.season_id (+)     = s1.season_id
               and 1 = 1 )sq on (st.phase_id           = sq.phase_id
                                 and st.season_id      = sq.season_id
                                 and svc_phases_col(i).action IN (CORESVC_SEASONS.action_mod,
                                                                  CORESVC_SEASONS.action_del))
      when matched then
      update
         set process_id              = svc_phases_col(i).process_id ,
             chunk_id                = svc_phases_col(i).chunk_id ,
             row_seq                 = svc_phases_col(i).row_seq ,
             action                  = svc_phases_col(i).action ,
             process$status          = svc_phases_col(i).process$status ,
             phase_desc              = sq.phase_desc ,
             start_date              = sq.start_date ,
             end_date                = sq.end_date ,
             create_id               = svc_phases_col(i).create_id ,
             create_datetime         = svc_phases_col(i).create_datetime ,
             last_upd_id             = svc_phases_col(i).last_upd_id ,
             last_upd_datetime       = svc_phases_col(i).last_upd_datetime
      when not matched then
         insert(process_id,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                season_id ,
                phase_id ,
                phase_desc ,
                start_date ,
                end_date ,
                create_id ,
                create_datetime ,
                last_upd_id ,
                last_upd_datetime)
         values(svc_phases_col(i).process_id ,
                svc_phases_col(i).chunk_id ,
                svc_phases_col(i).row_seq ,
                svc_phases_col(i).action ,
                svc_phases_col(i).process$status ,
                sq.season_id ,
                sq.phase_id ,
                sq.phase_desc ,
                sq.start_date ,
                sq.end_date ,
                svc_phases_col(i).create_id ,
                svc_phases_col(i).create_datetime ,
                svc_phases_col(i).last_upd_id ,
                svc_phases_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
               if L_error_code=1 then
                  L_error_code:=NULL;
                  L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
               end if;
            WRITE_S9T_ERROR( I_file_id,
                             PHASES_sheet,
                             svc_PHASES_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_PHASES;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_PHASES_TL( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id IN   SVC_PHASES.PROCESS_ID%TYPE) IS

   TYPE SVC_PHASES_TL_COL_TYP IS TABLE OF SVC_PHASES_TL%ROWTYPE;
   L_temp_rec         SVC_PHASES_TL%ROWTYPE;
   SVC_PHASES_TL_COL  SVC_PHASES_TL_COL_TYP           :=NEW SVC_PHASES_TL_COL_TYP();
   L_process_id       SVC_PHASES_TL.PROCESS_ID%TYPE;
   L_error            BOOLEAN                      :=FALSE;
   L_default_rec      SVC_PHASES_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             season_id_mi,
             phase_id_mi,
             phase_desc_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'PHASES_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('LANG'       as lang,
                                            'SEASON_ID'  as season_id,
                                            'PHASE_ID'   as phase_id,
                                            'PHASE_DESC' as phase_desc));
      L_mi_rec   C_MANDATORY_IND%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA     exception_init(dml_errors, -24381);
      L_table         VARCHAR2(30)   := 'SVC_PHASES_TL';
      L_pk_columns    VARCHAR2(255)  := 'Season, Phase, Lang';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  lang_dv,
                       season_id_dv,
                       phase_id_dv,
                       phase_desc_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                    = template_key
                          and wksht_key                                       = 'PHASES_TL' )
                        PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('LANG'       as lang,
                                                                             'SEASON_ID'  as season_id,
                                                                             'PHASE_ID'   as phase_id,
                                                                             'PHASE_DESC' as phase_desc)))
   LOOP
      BEGIN
         L_default_rec.phase_desc := rec.phase_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             PHASES_TL_sheet,
                             NULL,
                            'PHASE_DESC ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.phase_id := rec.phase_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             PHASES_TL_sheet,
                             NULL,
                            'PHASE_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.season_id := rec.season_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             PHASES_TL_sheet,
                             NULL,
                            'SEASON_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             PHASES_TL_sheet,
                             NULL,
                            'LANG ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(phases_tl$action)               as action,
          r.get_cell(phases_tl$lang)                 as lang,
          r.get_cell(phases_tl$season_id)            as season_id,
          r.get_cell(phases_tl$phase_id)             as phase_id,
          r.get_cell(phases_tl$phase_desc)           as phase_desc,
          r.get_row_seq()                         as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(PHASES_TL_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.phase_desc := rec.phase_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_TL_sheet,
                            rec.row_seq,
                            'PHASE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.phase_id := rec.phase_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_TL_sheet,
                            rec.row_seq,
                            'PHASE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.season_id := rec.season_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            PHASES_TL_sheet,
                            rec.row_seq,
                            'SEASON_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SEASONS.action_new then
         L_temp_rec.lang       := NVL( L_temp_rec.lang,
                                       L_default_rec.lang);
         L_temp_rec.phase_desc := NVL( L_temp_rec.phase_desc,
                                       L_default_rec.phase_desc);
         L_temp_rec.phase_id   := NVL( L_temp_rec.phase_id  ,
                                       L_default_rec.phase_id);
         L_temp_rec.season_id  := NVL( L_temp_rec.season_id ,
                                       L_default_rec.season_id);
      end if;
      if NOT ( L_temp_rec.phase_id is NOT NULL and
               L_temp_rec.season_id is NOT NULL and
               L_temp_rec.lang is NOT NULL)   then
         WRITE_S9T_ERROR( I_file_id,
                         PHASES_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_phases_tl_col.extend();
         svc_phases_tl_col(svc_phases_tl_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_phases_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_PHASES_TL st
      using(select(case
                   when L_mi_rec.season_id_mi    = 'N'
                    and svc_phases_tl_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.season_id is null
                   then mt.season_id
                   else s1.season_id
                   end) as season_id,
                  (case
                   when L_mi_rec.phase_id_mi     = 'N'
                    and svc_phases_tl_col(i).action = CORESVC_SEASONS.action_mod
                    and s1.phase_id is null
                   then mt.phase_id
                   else s1.phase_id
                   end) as phase_id,
                  (case
                   when L_mi_rec.phase_desc_mi    = 'N'
                    and svc_phases_tl_col(i).action  = CORESVC_SEASONS.action_mod
                    and s1.phase_desc is null
                   then mt.phase_desc
                   else s1.phase_desc
                   end) as phase_desc,
                  (case
                   when L_mi_rec.lang_mi    = 'N'
                    and svc_phases_tl_col(i).action  = CORESVC_SEASONS.action_mod
                    and s1.lang is null
                   then mt.lang
                   else s1.lang
                   end) as lang
              from(select svc_phases_tl_col(i).season_id  as season_id,
                          svc_phases_tl_col(i).phase_id   as phase_id,
                          svc_phases_tl_col(i).phase_desc as phase_desc,
                          svc_phases_tl_col(i).lang       as lang
                      from dual ) s1,
                   phases_tl mt
             where mt.phase_id (+)  = s1.phase_id
               and mt.season_id (+) = s1.season_id
               and mt.lang (+)      = s1.lang) sq
                on (st.phase_id      = sq.phase_id
                   and st.season_id = sq.season_id
                   and st.lang      = sq.lang
                   and svc_phases_tl_col(i).action IN (CORESVC_SEASONS.action_mod,
                                                       CORESVC_SEASONS.action_del))
      when matched then
      update
         set process_id              = svc_phases_tl_col(i).process_id ,
             chunk_id                = svc_phases_tl_col(i).chunk_id ,
             row_seq                 = svc_phases_tl_col(i).row_seq ,
             action                  = svc_phases_tl_col(i).action ,
             process$status          = svc_phases_tl_col(i).process$status ,
             phase_desc              = sq.phase_desc
      when not matched then
         insert(process_id,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                lang ,
                season_id ,
                phase_id ,
                phase_desc)
         values(svc_phases_tl_col(i).process_id ,
                svc_phases_tl_col(i).chunk_id ,
                svc_phases_tl_col(i).row_seq ,
                svc_phases_tl_col(i).action ,
                svc_phases_tl_col(i).process$status ,
                sq.lang ,
                sq.season_id ,
                sq.phase_id ,
                sq.phase_desc);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
                if L_error_code=1 then
                   L_error_code:=NULL;
                   L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
                end if;
            WRITE_S9T_ERROR( I_file_id,
                             PHASES_TL_sheet,
                             svc_PHASES_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_PHASES_TL;
---------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count     IN OUT   NUMBER,
                      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) :='CORESVC_SEASONS.PROCESS_S9T';
   L_file            S9T_FILE;
   L_sheets          S9T_PKG.NAMES_MAP_TYP;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR          EXCEPTION;
   PRAGMA            EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = false then
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_SEASONS(I_file_id,I_process_id);
      PROCESS_S9T_PHASES(I_file_id,I_process_id);
      PROCESS_S9T_SEASONS_TL(I_file_id,I_process_id);
      PROCESS_S9T_PHASES_TL(I_file_id,I_process_id);
   end if;

   O_error_COUNT := LP_s9t_errors_tab.COUNT();

   forall i IN 1..O_error_COUNT
      insert into s9t_errors
         values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();

      forall i IN 1..O_error_count
         insert into s9t_errors
            values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;

      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_SEASONS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error          IN OUT  BOOLEAN,
                             I_rec            IN      C_SVC_SEASONS%ROWTYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                      := 'CORESVC_SEASONS.PROCESS_VAL_SEASONS';
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SEASONS';
   L_valid    BOOLEAN                           := TRUE;
   L_exists   BOOLEAN                           := FALSE;
   L_merchid  VARCHAR2(1);
   L_name     VARCHAR2(255);
   L_class    V_CLASS.CLASS_NAME%TYPE;
   L_subclass V_SUBCLASS.SUB_NAME%TYPE;

BEGIN
   L_valid := TRUE;
   L_merchid := LP_module_merch_level;
   if LP_module_merch_level in ('S','C') then
      L_merchid := 'P';
   end if;

   if I_rec.s_action = action_new
      or (I_rec.s_action =action_mod
          and I_rec.pk_seasons_rid is NOT NULL) then
      if I_rec.filter_merch_id is NOT NULL then
         if LP_module_merch_level IN ('C','S') then
            if I_rec.filter_merch_id_class is NULL then
               WRITE_ERROR(I_rec.s_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.s_chunk_id,
                           L_table,
                           I_rec.s_row_seq,
                           'FILTER_MERCH_ID_CLASS',
                           'ENTER_CLASS');
               O_error := TRUE;
            end if;
            if LP_module_merch_level = 'S' then
               if I_rec.filter_merch_id_subclass is NULL then
                  WRITE_ERROR(I_rec.s_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.s_chunk_id,
                              L_table,
                              I_rec.s_row_seq,
                              'FILTER_MERCH_ID_SUBCLASS',
                              'ENTER_SUBCLASS');
                  O_error := TRUE;
               end if;
            end if;
         end if;
      end if;

      if I_rec.filter_merch_id is NOT NULL then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_MERCH_LEVEL(O_error_message,
                                                         L_valid,
                                                         L_name,
                                                         L_merchid,
                                                         I_rec.filter_merch_id,
                                                         I_rec.filter_merch_id_class,
                                                         I_rec.filter_merch_id_subclass) = FALSE
            or L_valid = FALSE then
            WRITE_ERROR(I_rec.s_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.s_chunk_id,
                        L_table,
                        I_rec.s_row_seq,
                        'FILTER_MERCH_ID',
                        O_error_message);
            O_error :=TRUE;
         end if;
      end if;

      L_valid := TRUE;

      if I_rec.filter_org_id is NOT NULL then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_ORG_LEVEL(O_error_message,
                                                       L_valid,
                                                       L_name,
                                                       LP_module_org_level,
                                                       I_rec.filter_org_id) = FALSE
            or L_valid = FALSE then
            WRITE_ERROR(I_rec.s_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.s_chunk_id,
                        L_table,
                        I_rec.s_row_seq,
                        'FILTER_ORG_ID',
                        O_error_message);
            O_error :=TRUE;
         end if;
      end if;

      L_valid := TRUE;

      if LP_module_merch_level IN ('C','S') then
         if I_rec.filter_merch_id_class is NOT NULL then
            if FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS(O_error_message,
                                                      L_valid,
                                                      L_class,
                                                      I_rec.filter_merch_id,
                                                      I_rec.filter_merch_id_class) = FALSE
               or L_valid = FALSE then
               WRITE_ERROR(I_rec.s_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.s_chunk_id,
                           L_table,
                           I_rec.s_row_seq,
                           'FILTER_MERCH_ID_CLASS',
                           'INV_CLASS');
               O_error :=TRUE;
            end if;

            L_valid := TRUE;

            if I_rec.filter_merch_id_subclass is NOT NULL then
               if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUBCLASS(O_error_message,
                                                            L_valid,
                                                            L_subclass,
                                                            I_rec.filter_merch_id,
                                                            I_rec.filter_merch_id_class,
                                                            I_rec.filter_merch_id_subclass) = FALSE
                  or L_valid = FALSE then
                  WRITE_ERROR(I_rec.s_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.s_chunk_id,
                              L_table,
                              I_rec.s_row_seq,
                              'FILTER_MERCH_ID_SUBCLASS',
                              'INV_SUBCLASS');
                  O_error :=TRUE;
               end if;
            end if;
         end if;
      end if;
   end if;

   if I_rec.s_action = action_del
      and I_rec.pk_seasons_rid is NOT NULL then

      if SEASON_SQL.SEASON_PHASE_EXISTS(O_error_message,
                                        L_exists,
                                        I_rec.season_id,
                                        NULL) = FALSE then
         WRITE_ERROR(I_rec.s_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.s_chunk_id,
                     L_table,
                     I_rec.s_row_seq,
                     NULL,
                     O_error_message);
        O_error :=TRUE;
      end if;
      if L_exists then
         WRITE_ERROR(I_rec.s_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.s_chunk_id,
                     L_table,
                     I_rec.s_row_seq,
                     'SEASON_ID',
                     'SEASON_ASSOC_ITEM');
        O_error :=TRUE;
     end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_SEASONS;

------------------------------------------------------------------------------------------------
FUNCTION EXEC_SEASONS_INS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_seasons_temp_rec   IN       SEASONS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                     := 'CORESVC_SEASONS.EXEC_SEASONS_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'SVC_SEASONS';
BEGIN
   insert into seasons
      values I_seasons_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEASONS_INS;
-------------------------------------------------------------------------------------------
FUNCTION EXEC_SEASONS_UPD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_seasons_temp_rec  IN      SEASONS%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_SEASONS.EXEC_SEASONS_UPD';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SEASONS';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   cursor C_SEASONS is
      select 'x'
        from SEASONS
       where season_id = I_seasons_temp_rec.season_id
         for update nowait;
BEGIN
   open C_SEASONS;
   close C_SEASONS;

   update seasons
      set row = I_seasons_temp_rec
    where 1 = 1
      and season_id = I_seasons_temp_rec.season_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_seasons_temp_rec.season_id,
                                             NULL);

      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_SEASONS%ISOPEN   then
         close C_SEASONS;
      end if;
      return FALSE;
END EXEC_SEASONS_UPD;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_SEASONS_PRE_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_seasons_temp_rec   IN       SEASONS%ROWTYPE )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_SEASONS_PRE_DEL';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   cursor C_SKU_CRITERIA is
      select 'x'
        from SKULIST_CRITERIA
       where season_id = I_seasons_temp_rec.season_id
         for update nowait;
   cursor C_PHASES is
      select 'x'
        from PHASES
       where season_id = I_seasons_temp_rec.season_id
         for update nowait;

BEGIN
   open  C_SKU_CRITERIA;
   close C_SKU_CRITERIA;

   delete
     from SKULIST_CRITERIA
    where season_id = I_seasons_temp_rec.season_id;

   open C_PHASES;
   close C_PHASES;

   delete
     from PHASES
    where season_id= I_seasons_temp_rec.season_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_SKU_CRITERIA%ISOPEN then
         close C_SKU_CRITERIA;
      end if;
      if C_PHASES%ISOPEN then
         close C_PHASES;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'SKULIST_CRITERIA,PHASES',
                                             I_seasons_temp_rec.season_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_SKU_CRITERIA%ISOPEN then
         close C_SKU_CRITERIA;
      end if;
      if C_PHASES%ISOPEN then
         close C_PHASES;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEASONS_PRE_DEL;
----------------------------------------------------------------------------------------
FUNCTION EXEC_SEASONS_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_seasons_temp_rec   IN       SEASONS%ROWTYPE )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_SEASONS_DEL';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_SEASONS_Tl is
      select 'x'
        from seasons_tl
       where season_id = I_seasons_temp_rec.season_id
         for update nowait;

   cursor C_SEASONS is
      select 'x'
        from seasons
       where season_id = I_seasons_temp_rec.season_id
         for update nowait;
BEGIN
   if EXEC_SEASONS_PRE_DEL(O_error_message,
                           I_seasons_temp_rec) = FALSE then
      return FALSE;
   end if;

   open C_SEASONS_TL;
   close C_SEASONS_TL;

   delete
     from seasons_tl
    where season_id = I_seasons_temp_rec.season_id;

   open C_SEASONS;
   close C_SEASONS;

   delete
     from seasons
    where season_id = I_seasons_temp_rec.season_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'SEASONS',
                                             I_seasons_temp_rec.season_id,
                                             NULL);

      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_SEASONS_TL%ISOPEN   then
         close c_seasons;
      end if;
      if C_SEASONS%ISOPEN   then
         close c_seasons;
      end if;
      return FALSE;
END EXEC_SEASONS_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SEASONS_TL_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_seasons_tl_ins_tab    IN       SEASONS_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_SEASONS_TL_INS';

BEGIN
   if I_seasons_tl_ins_tab is NOT NULL and I_seasons_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_seasons_tl_ins_tab.COUNT()
         insert into seasons_tl
              values I_seasons_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_SEASONS_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SEASONS_TL_UPD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_seasons_tl_upd_tab   IN       SEASONS_TL_TAB,
                             I_seasons_tl_upd_rst   IN       ROW_SEQ_TAB,
                             I_process_id           IN       SVC_SEASONS_TL.PROCESS_ID%TYPE,
                             I_chunk_id             IN       SVC_SEASONS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_SEASONS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SEASONS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_SEASONS_TL_UPD(I_seasons  SEASONS_TL.season_id%TYPE,
                                I_lang   SEASONS_TL.LANG%TYPE) is
      select 'x'
        from seasons_tl
       where season_id = I_seasons
         and lang = I_lang
         for update nowait;

BEGIN
   if I_seasons_tl_upd_tab is NOT NULL and I_seasons_tl_upd_tab.count > 0 then
      for i in I_seasons_tl_upd_tab.FIRST..I_seasons_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_seasons_tl_upd_tab(i).lang);
            L_key_val2 := 'Season ID: '||to_char(I_seasons_tl_upd_tab(i).season_id);
            open C_LOCK_SEASONS_TL_UPD(I_seasons_tl_upd_tab(i).season_id,
                                       I_seasons_tl_upd_tab(i).lang);
            close C_LOCK_SEASONS_TL_UPD;
         
            update seasons_tl
               set season_desc = I_seasons_tl_upd_tab(i).season_desc,
                   last_update_id = I_seasons_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_seasons_tl_upd_tab(i).last_update_datetime
             where lang = I_seasons_tl_upd_tab(i).lang
               and season_id = I_seasons_tl_upd_tab(i).season_id;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SEASONS_TL',
                           I_seasons_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_SEASONS_TL_UPD%ISOPEN then
         close C_LOCK_SEASONS_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_SEASONS_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SEASONS_TL_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_seasons_tl_del_tab   IN       SEASONS_TL_TAB,
                             I_seasons_tl_del_rst   IN       ROW_SEQ_TAB,
                             I_process_id           IN       SVC_SEASONS_TL.PROCESS_ID%TYPE,
                             I_chunk_id             IN       SVC_SEASONS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_SEASONS_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SEASONS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_SEASONS_TL_DEL(I_season_id  seasons_TL.season_id%TYPE,
                                I_lang        seasons_TL.LANG%TYPE) is
      select 'x'
        from seasons_tl
       where season_id = I_season_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_seasons_tl_del_tab is NOT NULL and I_seasons_tl_del_tab.count > 0 then
      for i in I_seasons_tl_del_tab.FIRST..I_seasons_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_seasons_tl_del_tab(i).lang);
            L_key_val2 := 'Season ID: '||to_char(I_seasons_tl_del_tab(i).season_id);
            open C_LOCK_SEASONS_TL_DEL(I_seasons_tl_del_tab(i).season_id,
                                       I_seasons_tl_del_tab(i).lang);
            close C_LOCK_SEASONS_TL_DEL;
            
            delete seasons_tl
             where lang = I_seasons_tl_del_tab(i).lang
               and season_id = I_seasons_tl_del_tab(i).season_id;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SEASONS_TL',
                           I_seasons_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_SEASONS_TL_DEL%ISOPEN then
         close C_LOCK_SEASONS_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_SEASONS_TL_DEL;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_PHASES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error          IN OUT  BOOLEAN,
                            I_rec            IN      C_SVC_SEASONS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      :='CORESVC_SEASONS.PROCESS_VAL_PHASES';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_PHASES';
   L_exists    BOOLEAN                           := FALSE;

BEGIN
   if I_rec.p_action = action_del
      and I_rec.pk_phases_rid is NOT NULL then
      if SEASON_SQL.SEASON_PHASE_EXISTS(O_error_message,
                                        L_exists,
                                        I_rec.season_id,
                                        I_rec.phase_id) = FALSE then
         WRITE_ERROR(I_rec.p_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.p_chunk_id,
                     L_table,
                     I_rec.p_row_seq,
                     NULL,
                     O_error_message);
        O_error := TRUE;
      end if;
      if L_exists then
         WRITE_ERROR(I_rec.p_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.p_chunk_id,
                     L_table,
                     I_rec.p_row_seq,
                     'Season,Phase',
                     'SEA_ASSOC_ITEM');
         O_error :=TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_PHASES;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_PHASES_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_phases_temp_rec   IN       PHASES%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_PHASES_INS';
BEGIN
   insert into phases
      values I_phases_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_PHASES_INS;
-------------------------------------------------------------------------------------------------
FUNCTION EXEC_PHASES_UPD(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_phases_temp_rec  IN      PHASES%ROWTYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                      := 'CORESVC_SEASONS.EXEC_PHASES_UPD';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'PHASES';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   cursor c_phases is
      select 'x'
        from PHASES
       where season_id = I_phases_temp_rec.season_id
         and phase_id  = I_phases_temp_rec.phase_id
         for update nowait;
BEGIN
   open c_phases;
   close c_phases;

   update phases
      set row = I_phases_temp_rec
    where phase_id  = I_phases_temp_rec.phase_id
      and season_id = I_phases_temp_rec.season_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_phases_temp_rec.phase_id,
                                             I_phases_temp_rec.season_id);

      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_PHASES%ISOPEN   then
         close c_phases;
      end if;
      return FALSE;
END EXEC_PHASES_UPD;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_PHASES_PRE_DEL(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_phases_temp_rec  IN      PHASES%ROWTYPE )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)  := 'CORESVC_SEASONS.EXEC_PHASES_PRE_DEL';
   L_exists           BOOLEAN       := FALSE;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   cursor c_sku_criteria is
      select 'x'
        from SKULIST_CRITERIA
       where season_id = I_phases_temp_rec.season_id
         and phase_id  = I_phases_temp_rec.phase_id
         for update nowait;
BEGIN
   open c_sku_criteria;
   close c_sku_criteria;

   delete
     from skulist_criteria
    where season_id = I_phases_temp_rec.season_id
      and phase_id  = I_phases_temp_rec.phase_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'SKULIST_CRITERIA',
                                             I_phases_temp_rec.phase_id,
                                             I_phases_temp_rec.season_id);

      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_SKU_CRITERIA%ISOPEN   then
         close C_SKU_CRITERIA;
      end if;
      return FALSE;
END EXEC_PHASES_PRE_DEL;
---------------------------------------------------------------------------------------------------
FUNCTION EXEC_PHASES_DEL(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_phases_temp_rec  IN      PHASES%ROWTYPE )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                     := 'CORESVC_SEASONS.EXEC_PHASES_DEL';
   L_exists           BOOLEAN                          := FALSE;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   cursor c_phases_tl is
      select 'x'
        from phases_tl
       where season_id = I_phases_temp_rec.season_id
         and phase_id  = I_phases_temp_rec.phase_id
         for update nowait;

   cursor c_phases is
      select 'x'
        from PHASES
       where season_id = I_phases_temp_rec.season_id
         and phase_id  = I_phases_temp_rec.phase_id
         for update nowait;
BEGIN
   open c_phases;
   close c_phases;


   if EXEC_PHASES_PRE_DEL(O_error_message,
                          I_phases_temp_rec) = FALSE then
      return FALSE;
   end if;

   open c_phases_tl;
   close c_phases_tl;

   delete
     from phases_tl
    where phase_id  = I_phases_temp_rec.phase_id
      and season_id = I_phases_temp_rec.season_id;

   delete
     from phases
    where phase_id  = I_phases_temp_rec.phase_id
      and season_id = I_phases_temp_rec.season_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'PHASES',
                                             I_phases_temp_rec.phase_id,
                                             I_phases_temp_rec.season_id);

      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_PHASES_TL%ISOPEN   then
         close C_PHASES_TL;
      end if;
      if C_PHASES%ISOPEN   then
         close C_PHASES;
      end if;
      return FALSE;
END EXEC_PHASES_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_PHASES_TL_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_phases_tl_ins_tab     IN       PHASES_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_PHASES_TL_INS';

BEGIN
   if I_phases_tl_ins_tab is NOT NULL and I_phases_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_phases_tl_ins_tab.COUNT()
         insert into phases_tl
              values I_phases_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_PHASES_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_PHASES_TL_UPD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_phases_tl_upd_tab    IN       PHASES_TL_TAB,
                            I_phases_tl_upd_rst    IN       ROW_SEQ_TAB,
                            I_process_id           IN       SVC_PHASES_TL.PROCESS_ID%TYPE,
                            I_chunk_id             IN       SVC_PHASES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_PHASES_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'PHASES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_PHASES_TL_UPD(I_season_id  PHASES_TL.SEASON_ID%TYPE,
                               I_phase_id   PHASES_TL.PHASE_ID%TYPE,
                               I_lang       PHASES_TL.LANG%TYPE) is
      select 'x'
        from phases_tl
       where season_id = I_season_id
         and phase_id = I_phase_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_phases_tl_upd_tab is NOT NULL and I_phases_tl_upd_tab.count > 0 then
      for i in I_phases_tl_upd_tab.FIRST..I_phases_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_phases_tl_upd_tab(i).lang);
            L_key_val2 := 'Phase ID: '||to_char(I_phases_tl_upd_tab(i).phase_id);
            open C_LOCK_PHASES_TL_UPD(I_phases_tl_upd_tab(i).season_id,
                                      I_phases_tl_upd_tab(i).phase_id,
                                      I_phases_tl_upd_tab(i).lang);
            close C_LOCK_PHASES_TL_UPD;
            
            update phases_tl
               set phase_desc = I_phases_tl_upd_tab(i).phase_desc,
                   last_update_id = I_phases_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_phases_tl_upd_tab(i).last_update_datetime
             where lang = I_phases_tl_upd_tab(i).lang
               and season_id = I_phases_tl_upd_tab(i).season_id
               and phase_id = I_phases_tl_upd_tab(i).phase_id;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_PHASES_TL',
                           I_phases_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_PHASES_TL_UPD%ISOPEN then
         close C_LOCK_PHASES_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_PHASES_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_PHASES_TL_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_phases_tl_del_tab    IN       PHASES_TL_TAB,
                            I_phases_tl_del_rst    IN       ROW_SEQ_TAB,
                            I_process_id           IN       SVC_PHASES_TL.PROCESS_ID%TYPE,
                            I_chunk_id             IN       SVC_PHASES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SEASONS.EXEC_PHASES_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'PHASES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_PHASES_TL_UPD(I_season_id  PHASES_TL.SEASON_ID%TYPE,
                               I_phase_id   PHASES_TL.PHASE_ID%TYPE,
                               I_lang       PHASES_TL.LANG%TYPE) is
      select 'x'
        from phases_tl
       where season_id = I_season_id
         and phase_id = I_phase_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_phases_tl_del_tab is NOT NULL and I_phases_tl_del_tab.count > 0 then
      for i in I_phases_tl_del_tab.FIRST..I_phases_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_phases_tl_del_tab(i).lang);
            L_key_val2 := 'Phase ID: '||to_char(I_phases_tl_del_tab(i).phase_id);
            open C_LOCK_PHASES_TL_UPD(I_phases_tl_del_tab(i).season_id,
                                      I_phases_tl_del_tab(i).phase_id,
                                      I_phases_tl_del_tab(i).lang);
            close C_LOCK_PHASES_TL_UPD;
            
            delete phases_tl
             where lang = I_phases_tl_del_tab(i).lang
               and phase_id = I_phases_tl_del_tab(i).phase_id
               and season_id = I_phases_tl_del_tab(i).season_id;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_PHASES_TL',
                           I_phases_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_PHASES_TL_UPD%ISOPEN then
         close C_LOCK_PHASES_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_PHASES_TL_DEL;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SEASONS( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id     IN      SVC_SEASONS.PROCESS_ID%TYPE,
                          I_chunk_id       IN      SVC_SEASONS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program             VARCHAR2(64)                       :='CORESVC_SEASONS.PROCESS_SEASONS';
   L_s_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  :='SVC_SEASONS';
   L_p_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  :='SVC_PHASES';
   L_exists              BOOLEAN                            := FALSE;
   L_ph_count            NUMBER                             :='0';
   L_seasons_temp_rec    SEASONS%ROWTYPE;
   L_phases_temp_rec     PHASES%ROWTYPE;
   L_s_error             BOOLEAN                            :=FALSE;
   L_p_error             BOOLEAN:=FALSE;
   L_s_process_error     BOOLEAN:=FALSE;
   L_p_process_error     BOOLEAN:=FALSE;
   L_count               VARCHAR2(1);
   L_season_id           SEASONS.SEASON_ID%TYPE             ;
   L_phase_id            PHASES.PHASE_ID%TYPE;
   L_sys_opt_row         SYSTEM_OPTIONS%ROWTYPE;

   L_rowseq_count               NUMBER;
   c                            NUMBER      := 1;
   TYPE L_row_seq_tab_type IS TABLE OF SVC_PHASES.ROW_SEQ%TYPE;
   L_row_seq_tab                       L_row_seq_tab_type;

   cursor C_PH_COUNT(L_season_id SEASONS.SEASON_ID%TYPE) is
      select COUNT(SEASON_ID) into L_ph_count
        from phases
       where season_id = L_season_id;

   cursor C_PHASE_COUNT(L_season_id SEASONS.SEASON_ID%TYPE) is
      select 'x'
        from phases
       where season_id = L_season_id;

    cursor C_PHASE(L_season_id SEASONS.SEASON_ID%TYPE) is
      select NVL(max(phase_id) + 1, 1)
        from phases
       where season_id = L_season_id;

   cursor C_SEASON_PERIOD(L_season_id SEASONS.SEASON_ID%TYPE) is
      select start_date,end_date
        from SEASONS
       where season_id = L_season_id;

   cursor C_ROLL(L_season_id SEASONS.SEASON_ID%TYPE) is
      select ROW_SEQ
        from SVC_PHASES
       where season_id = L_season_id;
BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_sys_opt_row) = FALSE then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_s_table,
                  1,
                  NULL,
                  O_error_message);
      return FALSE;
   end if;

   LP_module_merch_level := L_sys_opt_row.season_merch_level_code;
   LP_module_org_level   := L_sys_opt_row.season_org_level_code;

   FOR rec IN c_svc_seasons(I_process_id,
                            I_chunk_id)
   LOOP


      L_p_error               := FALSE;
      L_p_process_error       := FALSE;
      if rec.s_rank=1 then
         L_s_error               := FALSE;
         L_s_process_error       := FALSE;
         L_row_seq_tab.DELETE;
         c:=1;
      end if;

---for SEASONS table ---
      if rec.s_rank = 1 and rec.season_id is NOT NULL  then

---for invalid action
--HEAD-DETAIL
         if (rec.s_action is NULL and rec.s_rid is NOT NULL)
            or rec.s_action NOT IN (action_new,action_mod,action_del) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_s_table,
                        rec.s_row_seq,
                        'ACTION',
                        'INV_ACT');
            L_s_error :=TRUE;

         end if;

---for update/delete if season_id not exists
         if rec.s_action IN (action_mod,action_del)
            and rec.PK_SEASONS_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_s_table,
                        rec.s_row_seq,
                        'SEASON_ID',
                        'INV_SEASON');
            L_s_error :=TRUE;
         end if;
----for new/mod if season_desc is null
         if rec.s_action IN (action_new,action_mod)
            and NOT(  rec.SEASON_DESC  is NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_s_table,
                        rec.s_row_seq,
                        'SEASON_DESC',
                        'MUST_ENTER_SEASON_DESC');
            L_s_error :=TRUE;
         end if;
----for new/mod if start_date/ end_date is invalid
         if rec.s_action IN (action_new,action_mod) then

            if rec.start_date is NULL then
                   WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_s_table,
                           rec.s_row_seq,
                           'START_DATE',
                           'ENTER_START_DATE');
               L_s_error :=TRUE;
            elsif rec.start_date>rec.old_start_date
               or rec.start_date > rec.end_date then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_s_table,
                           rec.s_row_seq,
                           'START_DATE',
                           'INV_DATE');
               L_s_error :=TRUE;
            end if;

            if rec.end_date is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_s_table,
                           rec.s_row_seq,
                           'END_DATE',
                           'ENTER_END_DATE');
               L_s_error :=TRUE;
            elsif rec.end_date < rec.old_end_date
               or rec.end_date < rec.start_date then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_s_table,
                           rec.s_row_seq,
                           'END_DATE',
                           'INV_DATE');
               L_s_error :=TRUE;
            end if;
         end if;
         if PROCESS_VAL_SEASONS(O_error_message,
                                L_s_error,
                                rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_s_table,
                        rec.s_row_seq,
                        NULL,
                        O_error_message);
            L_s_error :=TRUE;
         end if;

      end if; -- if s_rank = 1 and rec.season_id is NOT NULL

--- for PHASES
--HEAD-DETAIL
      if rec.p_rid is NOT NULL then
         if rec.p_action is NULL
            or rec.p_action NOT IN (action_new,action_mod,action_del) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_p_table,
                        rec.p_row_seq,
                        'ACTION',
                        'INV_ACT');
            L_p_error :=TRUE;
         end if;

----for update/delete if phase not exists
         if rec.p_action IN (action_mod,action_del)
            and rec.PK_PHASES_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_p_table,
                        rec.p_row_seq,
                        'PHASE_ID',
                        'PH_NOT_EXIST');
            L_p_error :=TRUE;
         end if;

----for new, if season_id doesnt exists
         if rec.p_action= action_new
            and (rec.s_action is NULL
                 or rec.s_action <> action_new)
            and rec.phs_ssn_fk_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_p_table,
                        rec.p_row_seq,
                        'SEASON_ID',
                        'INV_SEASON');
            L_p_error :=TRUE;
         end if;

---for phase-desc is null
         if rec.p_action IN (action_new,action_mod)
            and NOT(rec.PHASE_DESC is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_p_table,
                        rec.p_row_seq,
                        'PHASE_DESC',
                        'ENTER_PHASE_DESC');
             L_p_error :=TRUE;
         end if;

         if rec.p_action in (action_new,action_mod)
            and rec.p_start_date is NULL then
            WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_p_table,
                           rec.p_row_seq,
                           'START_DATE',
                           'ENTER_START_DATE');
            L_p_error :=TRUE;
         end if;
         if rec.p_action in (action_new,action_mod)
            and rec.p_end_date is NULL then
            WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_p_table,
                           rec.p_row_seq,
                           'END_DATE',
                           'ENTER_END_DATE');
            L_p_error :=TRUE;
         end if;

         if PROCESS_VAL_PHASES(O_error_message,
                               L_p_error,
                               rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_p_table,
                        rec.p_row_seq,
                        NULL,
                        O_error_message);
            L_p_error :=TRUE;
         end if;

         if L_s_error
            and rec.p_action is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_p_table,
                        rec.p_row_seq,
                        'SEASON_ID',
                        'ERROR_IN_HEAD_REC');
            L_p_error :=TRUE;
         end if;
      end if;

----processing
      if NOT L_s_error then

         L_seasons_temp_rec.season_id                       := rec.season_id;

         L_seasons_temp_rec.season_desc                     := rec.season_desc;
         L_seasons_temp_rec.start_date                      := rec.start_date;
         L_seasons_temp_rec.end_date                        := rec.end_date;
         L_seasons_temp_rec.filter_merch_id                 := rec.filter_merch_id;
         L_seasons_temp_rec.filter_org_id                   := rec.filter_org_id;

         if LP_module_merch_level NOT IN ('C','S') then
            L_seasons_temp_rec.filter_merch_id_class        := NULL;
            L_seasons_temp_rec.filter_merch_id_subclass     := NULL;
         else
            L_seasons_temp_rec.filter_merch_id_class        := rec.filter_merch_id_class;
            L_seasons_temp_rec.filter_merch_id_subclass     := rec.filter_merch_id_subclass;
         end if;

         if rec.s_rank = 1
            and rec.season_id is NOT NULL then

            savepoint season_process;

            if rec.s_action = action_new then
               if SEASON_SQL.NEXT_SEASON(O_error_message,
                                         L_season_id) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_s_table,
                              rec.s_row_seq,
                              'SEASON_ID',
                              O_error_message);
                  L_s_process_error :=TRUE;

               end if;

               L_seasons_temp_rec.season_id := L_season_id;
               
               update svc_seasons_tl
                  set season_id = L_season_id
                where season_id = rec.season_id
                  and action = action_new
                  and process_id = I_process_id
                  and chunk_id = I_chunk_id;

               if NOT L_s_process_error then
                  if EXEC_SEASONS_INS(O_error_message,
                                      L_seasons_temp_rec)=FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_s_table,
                                 rec.s_row_seq,
                                 NULL,
                                 O_error_message);
                     L_s_process_error :=TRUE;
                  end if;
               end if;
            end if;

            if rec.s_action = action_mod then
               if EXEC_SEASONS_UPD( O_error_message,
                                    L_seasons_temp_rec)=FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_s_table,
                              rec.s_row_seq,
                              NULL,
                              O_error_message);
                  L_s_process_error :=TRUE;
               end if;
            end if;

            if rec.s_action = action_del then
               if EXEC_SEASONS_DEL( O_error_message,
                                    L_seasons_temp_rec)=FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_s_table,
                              rec.s_row_seq,
                              NULL,
                              O_error_message);
                  L_s_process_error :=TRUE;
               end if;
            end if;
         end if; -- if rec.s_rank =1

         if NOT L_s_process_error then

            if NOT L_p_error
               and rec.p_action IN (action_new,action_mod,action_del)then
               if rec.s_action is NULL
                  or rec.s_action <> action_del then
                  L_phases_temp_rec.season_id   := rec.p_season_id;
                  L_phases_temp_rec.phase_desc  := rec.phase_desc;
                  L_phases_temp_rec.phase_id    := rec.phase_id;

                  if rec.p_action IN (action_new,action_mod) then
                     if rec.s_action = action_mod
                        or rec.s_action is NULL then
                        open C_SEASON_PERIOD(rec.p_season_id);
                        fetch C_SEASON_PERIOD into rec.start_date,rec.end_date;
                        close C_SEASON_PERIOD;
                     end if;

                     if rec.p_action =action_new
                        or(rec.p_action = action_mod
                        and rec.pk_phases_rid is NOT NULL) then

                        if rec.p_start_date is NOT NULL
                           and(rec.p_start_date < rec.start_date
                           or rec.p_start_date > rec.end_date) then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_chunk_id,
                                       L_p_table,
                                       rec.p_row_seq,
                                       'START_DATE',
                                       'INV_DATE');
                           L_p_error := TRUE;
                        end if;

                        if rec.p_end_date is NOT NULL
                           and (rec.p_end_date > rec.end_date
                           or rec.p_end_date < rec.start_date) then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_chunk_id,
                                       L_p_table,
                                       rec.p_row_seq,
                                       'END_DATE',
                                       'INV_DATE');
                           L_p_error := TRUE;
                        end if;
                     end if;

                     L_phases_temp_rec.start_date := rec.p_start_date;
                     L_phases_temp_rec.end_date   := rec.p_end_date;
                  end if;

                  if NOT L_p_error then
                     if rec.p_action = action_new then
                        if rec.s_action = action_new then
                           L_phases_temp_rec.season_id := L_season_id;
                        end if;
                        open c_phase(L_phases_temp_rec.season_id);
                        fetch c_phase into L_phase_id;
                        close c_phase;
                           
                        L_phases_temp_rec.phase_id  := L_phase_id;

                        update svc_phases_tl
                           set season_id = NVL(L_season_id, rec.p_season_id),
                               phase_id = L_phase_id
                         where season_id = rec.p_season_id
                           and phase_id = rec.phase_id
                           and action = action_new
                           and process_id = I_process_id
                           and chunk_id = I_chunk_id;

                        if EXEC_PHASES_INS(O_error_message,
                                           L_phases_temp_rec)=FALSE then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_chunk_id,
                                       L_p_table,
                                       rec.p_row_seq,
                                       NULL,
                                       O_error_message);
                           L_p_process_error :=TRUE;
                        end if;
                     end if;

                     if rec.p_action = action_mod then
                        if EXEC_PHASES_UPD(O_error_message,
                                           L_phases_temp_rec)=FALSE then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_chunk_id,
                                       L_p_table,
                                       rec.p_row_seq,
                                       NULL,
                                       O_error_message);
                           L_p_process_error :=TRUE;
                        end if;
                     end if;

                     if rec.p_action = action_del then
                        if EXEC_PHASES_DEL(O_error_message,
                                           L_phases_temp_rec)=FALSE then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_chunk_id,
                                       L_p_table,
                                       rec.p_row_seq,
                                       NULL,
                                       O_error_message);
                           L_p_process_error :=TRUE;
                        end if;
                     end if;
                     if NOT L_p_process_error then
                        L_row_seq_tab.extend();
                        L_row_seq_tab(c)  := rec.p_row_seq;
                        c:=c+1;
                     end if;
                  end if; ---inner NOT L_p_error
               else

                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_p_table,
                              rec.p_row_seq,
                              'SEASON_ID',
                              'SEA_REC_DEL');
                  L_p_process_error :=TRUE;
               end if;  --NOT s_action <> action_del
            end if; -- NOT L_p_error and valid p_action

            if rec.season_id <> nvl(rec.next_season_id,9999) then

               if NOT L_s_process_error
                  and rec.s_action = action_new then
                  L_count:=NULL;
                  open C_PHASE_COUNT(L_season_id);
                  fetch C_PHASE_COUNT into L_count;
                  close C_PHASE_COUNT;

                  if L_count IS NULL then
                     L_phases_temp_rec.phase_id              := '1';
                     L_phases_temp_rec.season_id             := L_season_id;
                     L_phases_temp_rec.phase_desc            := rec.season_desc;
                     L_phases_temp_rec.start_date            := rec.start_date;
                     L_phases_temp_rec.end_date              := rec.end_date;

                     if EXEC_PHASES_INS(O_error_message,
                                        L_phases_temp_rec)=FALSE then

                        WRITE_ERROR(I_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                    I_chunk_id,
                                    L_p_table,
                                    rec.s_row_seq,
                                    NULL,
                                    O_error_message);
                        L_s_process_error :=TRUE;
                        ROLLBACK to season_process;
                     end if;
                  end if;
               end if;
               if (rec.s_action = action_mod or rec.s_action is NULL)
                  and rec.p_action=action_del then

                  L_ph_count:=NULL;
                  open  C_PH_COUNT (rec.season_id);
                  fetch C_PH_COUNT into L_ph_count;
                  close C_PH_COUNT;
                  --HEAD-DETAIL
                  if L_ph_count < 1 then
                     L_rowseq_count := L_row_seq_tab.count();
                     if L_rowseq_count > 0 then
                        for i in 1..L_rowseq_count
                        LOOP
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                       I_chunk_id,
                                       L_p_table,
                                       L_row_seq_tab(i),
                                       'PHASE_ID',
                                       'CANT_DEL_LAST_PH');
                           L_p_process_error :=TRUE;
                        END LOOP;
                        ROLLBACK TO season_process;
                        if rec.s_action is NOT NULL then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                       I_chunk_id,
                                       L_s_table,
                                       rec.s_row_seq,
                                       'SEASON_ID',
                                       'DETAIL_PROC_ER');
                        end if;
                     end if;
                  end if;
               end if;    --rec.s_action in (action_new,action_mod) or rec.s_action is NULL then
            end if;
         elsif rec.p_action is NOT NULL then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_p_table,
                                 rec.p_row_seq,
                                 'SEASON_ID',
                                 'ERROR_IN_HEAD_REC');

         end if;
    end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then

      if C_PH_COUNT%ISOPEN then
         close C_PH_COUNT;
      end if;
      if C_PHASE_COUNT%ISOPEN then
         close C_PHASE_COUNT;
      end if;
      if C_PHASE%ISOPEN then
         close C_PHASE;
      end if;
      if C_SEASON_PERIOD%ISOPEN then
         close C_SEASON_PERIOD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      return FALSE;

END PROCESS_SEASONS;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SEASONS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_seasons_TL.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_seasons_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_SEASONS.PROCESS_SEASONS_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SEASONS_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SEASONS';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SEASONS_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_seasons_TL_temp_rec  SEASONS_TL%ROWTYPE;
   L_seasons_tl_upd_rst   ROW_SEQ_TAB;
   L_seasons_tl_del_rst   ROW_SEQ_TAB;

   cursor C_SVC_SEASONS_TL(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
      select pk_seasons_TL.rowid  as pk_seasons_TL_rid,
             fk_seasons.rowid     as fk_seasons_rid,
             fk_lang.rowid        as fk_lang_rid,
             st.lang,
             st.season_id,
             st.season_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_seasons_TL  st,
             seasons         fk_seasons,
             seasons_TL      pk_seasons_TL,
             lang            fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.season_id   =  fk_seasons.season_id (+)
         and st.lang        =  pk_seasons_TL.lang (+)
         and st.season_id   =  pk_seasons_TL.season_id (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_SEASONS_TL_TAB is TABLE OF C_SVC_SEASONS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_seasons_tl_tab        SVC_SEASONS_TL_TAB;

   L_seasons_TL_ins_tab         seasons_tl_tab         := NEW seasons_tl_tab();
   L_seasons_TL_upd_tab         seasons_tl_tab         := NEW seasons_tl_tab();
   L_seasons_TL_del_tab         seasons_tl_tab         := NEW seasons_tl_tab();

BEGIN
   if C_SVC_SEASONS_TL%ISOPEN then
      close C_SVC_SEASONS_TL;
   end if;

   open C_SVC_SEASONS_TL(I_process_id,
                         I_chunk_id);
   LOOP
      fetch C_SVC_SEASONS_TL bulk collect into L_svc_seasons_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_seasons_tl_tab.COUNT > 0 then
         FOR i in L_svc_seasons_tl_tab.FIRST..L_svc_seasons_tl_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_seasons_tl_tab(i).action is NULL
               or L_svc_seasons_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_seasons_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_seasons_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_seasons_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_seasons_tl_tab(i).action = action_new
               and L_svc_seasons_tl_tab(i).pk_seasons_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_seasons_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_seasons_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_seasons_tl_tab(i).lang is NOT NULL
               and L_svc_seasons_tl_tab(i).season_id is NOT NULL
               and L_svc_seasons_tl_tab(i).pk_seasons_TL_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_seasons_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_seasons_tl_tab(i).action = action_new
               and L_svc_seasons_tl_tab(i).season_id is NOT NULL
               and L_svc_seasons_tl_tab(i).fk_seasons_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_seasons_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_seasons_tl_tab(i).action = action_new
               and L_svc_seasons_tl_tab(i).lang is NOT NULL
               and L_svc_seasons_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_seasons_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_seasons_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_seasons_tl_tab(i).season_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_seasons_tl_tab(i).row_seq,
                              'SEASON_DESC',
                              'MUST_ENTER_SEASON_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_seasons_tl_tab(i).season_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_seasons_tl_tab(i).row_seq,
                           'SEASON_ID',
                           'SEASON_NULL');
               L_error :=TRUE;
            end if;

            if L_svc_seasons_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_seasons_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_seasons_TL_temp_rec.lang := L_svc_seasons_tl_tab(i).lang;
               L_seasons_TL_temp_rec.season_id := L_svc_seasons_tl_tab(i).season_id;
               L_seasons_TL_temp_rec.season_desc := L_svc_seasons_tl_tab(i).season_desc;
               L_seasons_TL_temp_rec.create_datetime := SYSDATE;
               L_seasons_TL_temp_rec.create_id := LP_user;
               L_seasons_TL_temp_rec.last_update_datetime := SYSDATE;
               L_seasons_TL_temp_rec.last_update_id := LP_user;

               if L_svc_seasons_tl_tab(i).action = action_new then
                  L_seasons_TL_ins_tab.extend;
                  L_seasons_TL_ins_tab(L_seasons_TL_ins_tab.count()) := L_seasons_TL_temp_rec;
               end if;

               if L_svc_seasons_tl_tab(i).action = action_mod then
                  L_seasons_TL_upd_tab.extend;
                  L_seasons_TL_upd_tab(L_seasons_TL_upd_tab.count()) := L_seasons_TL_temp_rec;
                  L_seasons_tl_upd_rst(L_seasons_TL_upd_tab.count()) := L_svc_seasons_tl_tab(i).row_seq;
               end if;

               if L_svc_seasons_tl_tab(i).action = action_del then
                  L_seasons_TL_del_tab.extend;
                  L_seasons_TL_del_tab(L_seasons_TL_del_tab.count()) := L_seasons_TL_temp_rec;
                  L_seasons_tl_del_rst(L_seasons_TL_del_tab.count()) := L_svc_seasons_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_SEASONS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_SEASONS_TL;

   if EXEC_SEASONS_TL_INS(O_error_message,
                          L_seasons_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_SEASONS_TL_UPD(O_error_message,
                          L_seasons_TL_upd_tab,
                          L_seasons_tl_upd_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_SEASONS_TL_DEL(O_error_message,
                          L_seasons_TL_del_tab,
                          L_seasons_tl_del_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SEASONS_TL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PHASES_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_PHASES_TL.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_PHASES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_PHASES.PROCESS_PHASES_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'PHASES';
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'PHASES_TL';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_PHASES_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_phases_tl_temp_rec   PHASES_TL%ROWTYPE;
   L_phases_tl_upd_rst    ROW_SEQ_TAB;
   L_phases_tl_del_rst    ROW_SEQ_TAB;

   cursor C_SVC_PHASES_TL(I_process_id NUMBER,
                          I_chunk_id NUMBER) is
      select pk_phases_tl.rowid  as pk_phases_tl_rid,
             fk_phases.rowid     as fk_phases_rid,
             fk_lang.rowid       as fk_lang_rid,
             st.lang,
             st.season_id,
             st.phase_id,
             st.phase_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_phases_tl   st,
             phases          fk_phases,
             phases_tl       pk_phases_tl,
             lang            fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.season_id   =  fk_phases.season_id (+)
         and st.phase_id    =  fk_phases.phase_id (+)
         and st.lang        =  pk_phases_tl.lang (+)
         and st.season_id   =  pk_phases_tl.season_id (+)
         and st.phase_id    =  pk_phases_tl.phase_id (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_PHASES_TL_TAB is TABLE OF C_SVC_PHASES_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_phases_tl_tab        SVC_PHASES_TL_TAB;

   L_phases_tl_ins_tab         phases_tl_tab         := NEW phases_tl_tab();
   L_phases_tl_upd_tab         phases_tl_tab         := NEW phases_tl_tab();
   L_phases_tl_del_tab         phases_tl_tab         := NEW phases_tl_tab();

BEGIN
   if C_SVC_PHASES_TL%ISOPEN then
      close C_SVC_PHASES_TL;
   end if;

   open C_SVC_PHASES_TL(I_process_id,
                        I_chunk_id);
   LOOP
      fetch C_SVC_PHASES_TL bulk collect into L_svc_phases_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_phases_tl_tab.COUNT > 0 then
         FOR i in L_svc_phases_tl_tab.FIRST..L_svc_phases_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_phases_tl_tab(i).action is NULL
               or L_svc_phases_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_phases_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_phases_tl_tab(i).lang = LP_primary_lang and L_svc_phases_tl_tab(i).action = action_new then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_phases_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG');
               L_error :=TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_phases_tl_tab(i).action = action_new
               and L_svc_phases_tl_tab(i).pk_phases_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_phases_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_phases_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_phases_tl_tab(i).lang is NOT NULL
               and L_svc_phases_tl_tab(i).season_id is NOT NULL
               and L_svc_phases_tl_tab(i).phase_id is NOT NULL
               and L_svc_phases_tl_tab(i).pk_phases_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_phases_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_phases_tl_tab(i).action = action_new
               and L_svc_phases_tl_tab(i).season_id is NOT NULL
               and L_svc_phases_tl_tab(i).phase_id is NOT NULL
               and L_svc_phases_tl_tab(i).fk_phases_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_phases_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_phases_tl_tab(i).action = action_new
               and L_svc_phases_tl_tab(i).lang is NOT NULL
               and L_svc_phases_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_phases_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_phases_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_phases_tl_tab(i).phase_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_phases_tl_tab(i).row_seq,
                              'PHASE_DESC',
                              'ENTER_PHASE_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_phases_tl_tab(i).phase_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_phases_tl_tab(i).row_seq,
                           'PHASE_ID',
                           'PHASE_NULL');
               L_error :=TRUE;
            end if;

            if L_svc_phases_tl_tab(i).season_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_phases_tl_tab(i).row_seq,
                           'SEASON_ID',
                           'SEASON_NULL');
               L_error :=TRUE;
            end if;

            if L_svc_phases_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_phases_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_phases_tl_temp_rec.lang := L_svc_phases_tl_tab(i).lang;
               L_phases_tl_temp_rec.season_id := L_svc_phases_tl_tab(i).season_id;
               L_phases_tl_temp_rec.phase_id := L_svc_phases_tl_tab(i).phase_id;
               L_phases_tl_temp_rec.phase_desc := L_svc_phases_tl_tab(i).phase_desc;
               L_phases_tl_temp_rec.create_datetime := SYSDATE;
               L_phases_tl_temp_rec.create_id := LP_user;
               L_phases_tl_temp_rec.last_update_datetime := SYSDATE;
               L_phases_tl_temp_rec.last_update_id := LP_user;

               if L_svc_phases_tl_tab(i).action = action_new then
                  L_phases_tl_ins_tab.extend;
                  L_phases_tl_ins_tab(L_phases_tl_ins_tab.count()) := L_phases_tl_temp_rec;
               end if;

               if L_svc_phases_tl_tab(i).action = action_mod then
                  L_phases_tl_upd_tab.extend;
                  L_phases_tl_upd_tab(L_phases_tl_upd_tab.count()) := L_phases_tl_temp_rec;
                  L_phases_tl_upd_rst(L_phases_tl_upd_tab.count()) := L_svc_phases_tl_tab(i).row_seq;
               end if;

               if L_svc_phases_tl_tab(i).action = action_del then
                  L_phases_tl_del_tab.extend;
                  L_phases_tl_del_tab(L_phases_tl_del_tab.count()) := L_phases_tl_temp_rec;
                  L_phases_tl_del_rst(L_phases_tl_del_tab.count()) := L_svc_phases_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_PHASES_TL%NOTFOUND;
   END LOOP;
   close C_SVC_PHASES_TL;

   if EXEC_PHASES_TL_INS(O_error_message,
                         L_phases_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_PHASES_TL_UPD(O_error_message,
                         L_phases_tl_upd_tab,
                         L_phases_tl_upd_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_PHASES_TL_DEL(O_error_message,
                         L_phases_tl_del_tab,
                         L_phases_tl_del_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_PHASES_TL;
---------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
     from svc_phases_tl
    where process_id = I_process_id;
    
   delete
     from svc_seasons_tl
    where process_id = I_process_id;

   delete
     from svc_phases
    where process_id = I_process_id;
    
   delete
     from svc_seasons
    where process_id = I_process_id;
END;
---------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT NUMBER,
                  I_process_id    IN     NUMBER,
                  I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    :='CORESVC_SEASONS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_SEASONS(O_error_message,
                      I_process_id,
                      I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_SEASONS_TL(O_error_message,
                         I_process_id,
                         I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_PHASES_TL(O_error_message,
                        I_process_id,
                        I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_SEASONS;
/
