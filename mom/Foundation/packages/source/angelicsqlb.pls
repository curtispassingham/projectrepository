CREATE OR REPLACE PACKAGE BODY ANGELIC_PKG AS
-- Function and procedure implementations
FUNCTION ANG_GET_CHAIN_NAME(Is_chain_name IN chain.chain%TYPE) 
  RETURN VARCHAR2 IS
    L_chain_name chain.chain_name%TYPE;
    cursor C_GET_CHAIN_NAME is
         Select translate(lower(trim(chain_name)),'1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ ~!@#$%^&*()_+}{":?><`-=]['''';/.,','1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ') 
         from chain
         where chain=Is_chain_name;
 BEGIN
     open C_GET_CHAIN_NAME;
     fetch C_GET_CHAIN_NAME into L_chain_name;
    RETURN L_chain_name;
 END;
END ANGELIC_PKG;
/