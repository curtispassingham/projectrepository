CREATE OR REPLACE PACKAGE BODY ITEMLIST_DELETE_SQL AS
----------------------------------------------------------------
   RECORD_LOCKED   EXCEPTION;
   LP_table        VARCHAR2(50);
----------------------------------------------------------------
-- Name:    SKULIST_DELETE
-- Purpose: Deletes all styles or SKUs found given the specified criteria
--          and the number of styles or SKUs in the SKU list.
----------------------------------------------------------------
FUNCTION SKULIST_DELETE(I_itemlist           IN     SKULIST_HEAD.SKULIST%TYPE,
                        I_pack_ind           IN     ITEM_MASTER.PACK_IND%TYPE,
                        I_item               IN     ITEM_MASTER.ITEM%TYPE,
                        I_item_parent        IN     SKULIST_CRITERIA.ITEM_PARENT%TYPE,
                        I_item_grandparent   IN     SKULIST_CRITERIA.ITEM_GRANDPARENT%TYPE,
                        I_dept               IN     SKULIST_CRITERIA.DEPT%TYPE,
                        I_class              IN     SKULIST_CRITERIA.CLASS%TYPE,
                        I_subclass           IN     SKULIST_CRITERIA.SUBCLASS%TYPE,
                        I_supplier           IN     SKULIST_CRITERIA.SUPPLIER%TYPE,
                        I_diff_1             IN     SKULIST_CRITERIA.DIFF_1%TYPE,
                        I_diff_2             IN     SKULIST_CRITERIA.DIFF_2%TYPE,
                        I_diff_3             IN     SKULIST_CRITERIA.DIFF_3%TYPE,
                        I_diff_4             IN     SKULIST_CRITERIA.DIFF_4%TYPE,
                        I_uda_id             IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                        I_uda_value          IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                        I_uda_max_date       IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                        I_uda_min_date       IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                        I_season_id          IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                        I_phase_id           IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                        I_count_ind          IN     VARCHAR2,
                        I_no_add_ind         IN     VARCHAR2,
                        I_item_level         IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                        O_no_items           IN OUT NUMBER,
                        O_error_message      IN OUT VARCHAR2)
   RETURN BOOLEAN is

   L_program          VARCHAR2(64) := 'ITEMLIST_DELETE_SQL.SKULIST_DELETE';
   L_item             ITEM_MASTER.ITEM%TYPE;
   L_dummy            VARCHAR2(1);
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   L_select           VARCHAR2(200);
   L_lock             VARCHAR2(200);
   L_from_clause      VARCHAR2(200);
   L_main_where       VARCHAR2(200);
   L_where_clause     VARCHAR2(3000);
   L_statement        VARCHAR2(3400);
   L_statement2       VARCHAR2(3400);
   TYPE ORD_CURSOR is REF CURSOR;
   C_ITEM             ORD_CURSOR;
   L_DISPLAY_TYPE     UDA.DISPLAY_TYPE%TYPE;
   
   cursor C_CHECK_DISPLAY_TYPE is
         SELECT DISPLAY_TYPE
         FROM   uda
         WHERE  uda_id = I_uda_id;
BEGIN
   L_lock := 'select item ' ||
               'from skulist_detail ';
   L_main_where := 'where skulist = '|| I_itemlist ||
                    ' and item in (select ima.item ';
   L_from_clause := 'from item_master ima';
   L_where_clause := ' where ima.item_level <= ima.tran_level ';

   if I_item_parent is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '((ima.item = ''' || I_item_parent || ''') or ' ||
                        '(ima.item_parent = '''||I_item_parent||''')) ';
   end if;

   if I_item_grandparent is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '((ima.item = '''||I_item_grandparent||''') or ' ||
                        '(ima.item_parent = '''||I_item_grandparent||''') or ' ||
                        '(ima.item_grandparent = '''||I_item_grandparent||''')) ';
   end if;

   if I_item is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = '''||I_item||'''';
   end if;

   if I_dept is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.dept = '||I_dept;
   end if;

   if I_class is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.class = '||I_class;
   end if;

   if I_subclass is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.subclass = '||I_subclass;
   end if;

   if I_diff_1 is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_1   = ''' ||I_diff_1|| ''' or ' ||
                        '(ima.diff_2   = ''' ||I_diff_1|| ''') or ' ||
                        '(ima.diff_3   = ''' ||I_diff_1|| ''') or ' ||
                        '(ima.diff_4   = ''' ||I_diff_1|| '''))';

      if I_diff_2 is NOT NULL then
         L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_2   = ''' ||I_diff_2|| ''' or ' ||
                        '(ima.diff_3   = ''' ||I_diff_2|| ''') or ' ||
                        '(ima.diff_4   = ''' ||I_diff_2|| ''') or ' ||
                        '(ima.diff_1   = ''' ||I_diff_2|| '''))';

         if I_diff_3 is NOT NULL then
            L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_3   = ''' ||I_diff_3|| ''' or ' ||
                        '(ima.diff_4   = ''' ||I_diff_3|| ''') or ' ||
                        '(ima.diff_1   = ''' ||I_diff_3|| ''') or ' ||
                        '(ima.diff_2   = ''' ||I_diff_3|| '''))';

            if I_diff_4 is NOT NULL then
               L_where_clause := L_where_clause || ' and ' ||
                        '(ima.diff_4   = ''' ||I_diff_4|| ''' or ' ||
                        '(ima.diff_1   = ''' ||I_diff_4|| ''') or ' ||
                        '(ima.diff_2   = ''' ||I_diff_4|| ''') or ' ||
                        '(ima.diff_3   = ''' ||I_diff_4|| '''))';
            end if;
         end if;
      end if;
   end if;  

   if I_pack_ind is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.pack_ind = '''||I_pack_ind||'''';
   end if;

   if I_item_level is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item_level = '||I_item_level;
   end if;

   if I_supplier is NOT NULL then
      L_from_clause := L_from_clause ||
                       ', item_supplier isu';
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = isu.item and ' ||
                        'isu.supplier = '||I_supplier;
   end if;

   if I_season_id is NOT NULL then
      L_from_clause := L_from_clause ||
                       ', item_seasons ise';
      L_where_clause := L_where_clause || ' and ' ||
                        'ima.item = ise.item and ' ||
                        'ise.season_id = '||I_season_id||' and ' ||
                        'ise.phase_id = nvl('''||I_phase_id||''', ise.phase_id)';
   end if;

   if I_uda_id is NOT NULL then
   
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_DISPLAY_TYPE', 'UDA', NULL); 
      open C_CHECK_DISPLAY_TYPE;
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_DISPLAY_TYPE', 'UDA', NULL);
      fetch C_CHECK_DISPLAY_TYPE into L_DISPLAY_TYPE ;
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_DISPLAY_TYPE', 'UDA', NULL);
      close C_CHECK_DISPLAY_TYPE;
      
      if (L_DISPLAY_TYPE='LV') then
         L_from_clause := L_from_clause ||
                         ', uda_item_lov uil';

         L_where_clause := L_where_clause || ' and ' ||
                          '(ima.item = uil.item and ' ||
                          'uil.uda_id = '||I_uda_id||') ';
      else
	     L_from_clause := L_from_clause ||
                         ', uda_item_date ud';
         L_where_clause := L_where_clause || ' and ' ||
                          '(ima.item = ud.item and ' ||
                          'ud.uda_id = '||I_uda_id||') ';
      end if; 
    
   end if;

   if I_uda_value is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'uil.uda_value = '||I_uda_value;
   end if;

   if I_uda_min_date is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ud.uda_date >= '''||I_uda_min_date||'''';
   end if;

   if I_uda_max_date is NOT NULL then
      L_where_clause := L_where_clause || ' and ' ||
                        'ud.uda_date <= '''||I_uda_max_date||'''';
   end if;

   if I_no_add_ind = 'N' then
      L_statement := L_lock ||
                     L_main_where ||
                     L_from_clause ||
                     L_where_clause ||
                     ') for update nowait';

      EXECUTE IMMEDIATE L_statement;
      open C_ITEM for L_statement;

    loop
      fetch C_ITEM into L_item;
      if C_ITEM%FOUND then
         if SIT_SQL.DELETE_ITEM(O_error_message,
                                I_itemlist,
                                L_item) = FALSE then
            return FALSE;
         end if;
      else
         EXIT;
      end if;
    end loop;
      close C_ITEM;

      L_statement2 := 'delete from skulist_detail ' ||
                       L_main_where ||
                       L_from_clause ||
                       L_where_clause ||')';

      EXECUTE IMMEDIATE L_statement2;

      delete from skulist_dept_class_subclass sk1
       where sk1.skulist = I_itemlist
         and (sk1.skulist, sk1.dept, sk1.class, sk1.subclass) not in
                (select sk2.skulist, im2.dept, im2.class, im2.subclass
                   from item_master im2,
                        skulist_detail sk2
                  where sk2.skulist = I_itemlist
                    and sk2.item = im2.item);
   end if;
   ---
   if I_count_ind = 'Y' then
      if ITEMLIST_ATTRIB_SQL.GET_ITEM_COUNT(O_error_message,
                                     I_itemlist,
                                  O_no_items) = FALSE then
         RETURN FALSE;
      end if;
   end if;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('SKULIST_DETAIL_REC_LOCK',
                                            to_char(I_itemlist),
                                            NULL,
                                            NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
      RETURN FALSE;
END SKULIST_DELETE;
---------------------------------------------------------------------------------------------
FUNCTION DEL_SKULIST(O_error_message   IN OUT      VARCHAR2,
                     I_skulist         IN          SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN is

BEGIN

   LP_table:= 'STAKE_SCHEDULE';

   SQL_LIB.SET_MARK('DELETE',NULL,'STAKE_SCHEDULE','SKULIST: '|| I_skulist);
   delete from stake_schedule
      where stake_schedule.skulist = I_skulist;

   LP_table:= 'LOC_CLSF_DETAIL';

   SQL_LIB.SET_MARK('DELETE',NULL,'LOC_CLSF_DETAIL','SKULIST: '|| I_skulist);
   delete from loc_clsf_detail
      where loc_clsf_detail.skulist = I_skulist;

   LP_table:= 'SKULIST_CRITERIA';

   SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_CRITERIA','SKULIST: '|| I_skulist);
   delete from skulist_criteria
      where skulist_criteria.skulist = I_skulist;

   LP_table:= 'SKULIST_DETAIL';

   SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_DETAIL','SKULIST: '|| I_skulist);
   delete from skulist_detail
      where skulist_detail.skulist = I_skulist;

   LP_table:= 'SKULIST_DEPT_CLASS_SUBCLASS';

   SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_DEPT_CLASS_SUBCLASS','SKULIST: '|| I_skulist);
   delete from skulist_dept_class_subclass
    where skulist_dept_class_subclass.skulist = I_skulist;

   LP_table:= 'SKULIST_DEPT';

   SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_DEPT','SKULIST: '|| I_skulist);
   delete from skulist_dept
      where skulist_dept.skulist = I_skulist;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                SQLERRM,
                                'DEL_SKULIST',
                                to_char(SQLCODE));
      RETURN FALSE;

END DEL_SKULIST;
-----------------------------------------------------------------------
FUNCTION LOCK_SKULIST (O_error_message  IN OUT VARCHAR2,
                       I_skulist        skulist_head.skulist%TYPE)
   RETURN BOOLEAN is

   cursor C_LOCK_STAKE_SCHEDULE is
      select 'x'
        from stake_schedule
       where skulist = I_skulist
         for update nowait;

   cursor C_LOCK_LOC_CLSF_DETAIL is
      select 'x'
        from loc_clsf_detail
       where skulist = I_skulist
         for update nowait;

   cursor C_LOCK_SKULIST_CRITERIA is
      select 'x'
        from skulist_criteria
       where skulist = I_skulist
         for update nowait;

   cursor C_LOCK_SKULIST_DETAIL is
      select 'x'
        from skulist_detail
       where skulist = I_skulist
         for update nowait;
BEGIN
   LP_table := 'STAKE_SCHEDULE';
   open C_LOCK_STAKE_SCHEDULE;
   close C_LOCK_STAKE_SCHEDULE;

   LP_table := 'LOC_CLSF_DETAIL';
   open C_LOCK_LOC_CLSF_DETAIL;
   close C_LOCK_LOC_CLSF_DETAIL;

   LP_table := 'SKULIST_CRITERIA';
   open C_LOCK_SKULIST_CRITERIA;
   close C_LOCK_SKULIST_CRITERIA;

   LP_table := 'SKULIST_DETAIL';
   open C_LOCK_SKULIST_DETAIL;
   close C_LOCK_SKULIST_DETAIL;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('ITEMDELETE_REC_LOC',
                                            LP_table,
                                            I_skulist);
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                SQLERRM,
                                'LOCK_SKULIST',
                                to_char(SQLCODE));
      RETURN FALSE;
END LOCK_SKULIST;
--------------------------------------------------------------
END ITEMLIST_DELETE_SQL;
/


