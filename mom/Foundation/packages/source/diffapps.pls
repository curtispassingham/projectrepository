
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFF_APPLY_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------
TYPE item_temp_rectype IS RECORD
(
 item_number_type   ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
 auto_create        VARCHAR2(1),
 item_parent        ITEM_MASTER.ITEM_PARENT%TYPE,
 parent_desc        ITEM_MASTER.ITEM_DESC%TYPE,
 parent_diff1       ITEM_MASTER.DIFF_1%TYPE,
 parent_diff2       ITEM_MASTER.DIFF_2%TYPE,
 parent_diff3       ITEM_MASTER.DIFF_3%TYPE,
 parent_diff4       ITEM_MASTER.DIFF_4%TYPE);  

TYPE diff_range_rectype IS RECORD
(
diff_range1_group1  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range1_group2  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range2_group1  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range2_group2  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
multi_range1        DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
multi_range2        DIFF_RANGE_HEAD.DIFF_RANGE%TYPE
);

TYPE diff_mixed_rectype IS RECORD
(
diff_range_group1   DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range_group2   DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range_group3   DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
multi_range         DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
diff1_used          VARCHAR2(1),
diff2_used          VARCHAR2(1),
diff3_used          VARCHAR2(1),
diff4_used          VARCHAR2(1)
);

-------------------------------------------------------------------------------------------------
--    Name: POP_TEMP_DIFF
-- Purpose: Populates TEMP_DIFFx tables with all applicable differential values.
-------------------------------------------------------------------------------------------------
FUNCTION POP_TEMP_DIFF(O_error_message     IN OUT  VARCHAR2,
                       O_item_number_type  IN OUT  ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                       I_item_parent       IN      ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_diff1_id          IN      DIFF_IDS.DIFF_ID%TYPE,
                       I_diff1_group_ind   IN      V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                       I_diff2_id          IN      DIFF_IDS.DIFF_ID%TYPE,
                       I_diff2_group_ind   IN      V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                       I_diff3_id          IN      DIFF_IDS.DIFF_ID%TYPE,
                       I_diff3_group_ind   IN      V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                       I_diff4_id          IN      DIFF_IDS.DIFF_ID%TYPE,
                       I_diff4_group_ind   IN      V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE) 
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: TOGGLE_SELECTED
-- Purpose: Updates selected_ind column based on the value of I_selected for all records on 
--          TEMP_DIFFx.
-------------------------------------------------------------------------------------------------
FUNCTION TOGGLE_SELECTED(O_error_message  IN OUT  VARCHAR2,
                         I_selected       IN      BOOLEAN,
                         I_diff_no        IN      VARCHAR2)
RETURN BOOLEAN ;
-------------------------------------------------------------------------------------------------
--    Name: POP_ITEM_TEMP
-- Purpose: Create potential new child records from the cartesian product of the 
--          'APPLIED' temp_diffx records.
-------------------------------------------------------------------------------------------------
FUNCTION POP_ITEM_TEMP(O_error_message     IN OUT  VARCHAR2,
                       O_item_temp_created IN OUT  BOOLEAN,
                       O_dups_created      IN OUT  BOOLEAN,
                       I_item_temp_rec     item_temp_rectype)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: CLEAR_TEMP_TABLE
-- Purpose: Delete all records from specified temp table.
-------------------------------------------------------------------------------------------------
FUNCTION CLEAR_TEMP_TABLE(O_error_message  IN OUT  VARCHAR2,
                          I_table_name     IN      VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
--    Name: CHECK_ITEM_TEMP
-- Purpose: Check item_temp table to see if any diffs have been applied.
-------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_TEMP(O_error_message  IN OUT  VARCHAR2,
                         O_diffs_applied  IN OUT  VARCHAR2)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
--    Name: VALIDATE_ITEM_TEMP
-- Purpose: Validate records on item_temp table before creating and commiting children.
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_TEMP(O_error_message  IN OUT  VARCHAR2,
                            O_valid          IN OUT  BOOLEAN)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: ITEM_TEMP_EXISTS
-- Purpose: Determine if item is on item_temp table before applying item to temp table.
-------------------------------------------------------------------------------------------------
FUNCTION TEMP_ITEM_EXISTS(O_error_message  IN OUT  VARCHAR2,
                          O_exists         IN OUT  BOOLEAN,
                          I_item           IN      ITEM_MASTER.ITEM%TYPE,
                          I_diff1          IN      ITEM_MASTER.DIFF_1%TYPE,
                          I_diff2          IN      ITEM_MASTER.DIFF_2%TYPE,
                          I_diff3          IN      ITEM_MASTER.DIFF_3%TYPE,
                          I_diff4          IN      ITEM_MASTER.DIFF_4%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------------------------
--    Name: POP_RANGE_ITEM_TEMP
-- Purpose: Create potential new child records from the cartesian product of the 
--          2 Ranges that have 2 or more diff_ids defined.
-------------------------------------------------------------------------------------------------
FUNCTION POP_RANGE_ITEM_TEMP(O_error_message     IN OUT  VARCHAR2,
                             O_item_temp_created IN OUT  BOOLEAN,
                             O_dups_created      IN OUT  BOOLEAN,
                             I_item_temp_rec     item_temp_rectype,
                             I_info_rec          diff_range_rectype)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: POP_MIXED_ITEM_TEMP
-- Purpose: Create potential new child records from the cartesian product of  
--          1 Range with 2 or more diff_ids defined and 0 or more diff_ids from the 
--          temp_diffx tables.
-------------------------------------------------------------------------------------------------
FUNCTION POP_MIXED_ITEM_TEMP(O_error_message     IN OUT  VARCHAR2,
                             O_item_temp_created IN OUT  BOOLEAN,
                             O_dups_created      IN OUT  BOOLEAN,
                             I_item_temp_rec     item_temp_rectype,
                             I_info_rec          diff_mixed_rectype)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: UPDATE_TEMP_DIFF_RANGE
-- Purpose: Update a diff_tempx table to select the diff_ids in the diff_range_detail table
--          for the input diff_range identifier.
-------------------------------------------------------------------------------------------------
FUNCTION UPDATE_TEMP_DIFF_RANGE (O_error_message  IN OUT  VARCHAR2,
                                 I_diff_range     IN      DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
                                 I_table_no       IN      NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: ITEM_NUMBER_TYPE
-- Purpose: Returns the item_number_type if existing children exist.
-------------------------------------------------------------------------------------------------
FUNCTION ITEM_NUMBER_TYPE(O_error_message     IN OUT  VARCHAR2,
                          O_item_number_type  IN OUT  ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                          I_item_parent       IN      ITEM_MASTER.ITEM_PARENT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: DELETE_SELECTED_ITEM_TEMP
-- Purpose: Delete records from the item_temp table based on filter criteria passed
--          into the function.
-------------------------------------------------------------------------------------------------
FUNCTION DELETE_SELECTED_ITEM_TEMP(O_error_message     IN OUT  VARCHAR2,
                                   I_item_parent       IN      ITEM_MASTER.ITEM_PARENT%TYPE,
                                   I_diff_1            IN      ITEM_TEMP.DIFF_1%TYPE,
                                   I_diff_2            IN      ITEM_TEMP.DIFF_2%TYPE,
                                   I_diff_3            IN      ITEM_TEMP.DIFF_3%TYPE,
                                   I_diff_4            IN      ITEM_TEMP.DIFF_4%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: POP_ITEM_TEMP
-- Purpose: Overloaded function with single-value parameters instead of a record type
--          parameter to handle function calls in ADF.
-------------------------------------------------------------------------------------------------
FUNCTION POP_ITEM_TEMP(O_error_message      IN OUT   VARCHAR2,
                       O_item_temp_created  IN OUT   BOOLEAN,
                       O_dups_created       IN OUT   BOOLEAN,
                       I_item_number_type   IN       ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                       I_auto_create        IN       VARCHAR2,
                       I_item_parent        IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_parent_desc        IN       ITEM_MASTER.ITEM_DESC%TYPE,
                       I_parent_diff1       IN       ITEM_MASTER.DIFF_1%TYPE,
                       I_parent_diff2       IN       ITEM_MASTER.DIFF_2%TYPE,
                       I_parent_diff3       IN       ITEM_MASTER.DIFF_3%TYPE,
                       I_parent_diff4       IN       ITEM_MASTER.DIFF_4%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: POP_MIXED_ITEM_TEMP
-- Purpose: Overloaded function with single-value parameters instead of record type
--          parameters to handle function calls in ADF.
-------------------------------------------------------------------------------------------------
FUNCTION POP_MIXED_ITEM_TEMP(O_error_message       IN OUT   VARCHAR2,
                             O_item_temp_created   IN OUT   BOOLEAN,
                             O_dups_created        IN OUT   BOOLEAN,
                             I_item_number_type    IN       ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                             I_auto_create         IN       VARCHAR2,
                             I_item_parent         IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                             I_parent_desc         IN       ITEM_MASTER.ITEM_DESC%TYPE,
                             I_parent_diff1        IN       ITEM_MASTER.DIFF_1%TYPE,
                             I_parent_diff2        IN       ITEM_MASTER.DIFF_2%TYPE,
                             I_parent_diff3        IN       ITEM_MASTER.DIFF_3%TYPE,
                             I_parent_diff4        IN       ITEM_MASTER.DIFF_4%TYPE,
                             I_diff_range_group1   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                             I_diff_range_group2   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                             I_diff_range_group3   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                             I_multi_range         IN       DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
                             I_diff1_used          IN       VARCHAR2,
                             I_diff2_used          IN       VARCHAR2,
                             I_diff3_used          IN       VARCHAR2,
                             I_diff4_used          IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
--    Name: POP_RANGE_ITEM_TEMP
-- Purpose: Overloaded function with single-value parameters instead of record type
--          parameters to handle function calls in ADF.
-------------------------------------------------------------------------------------------------
FUNCTION POP_RANGE_ITEM_TEMP(O_error_message        IN OUT   VARCHAR2,
                             O_item_temp_created    IN OUT   BOOLEAN,
                             O_dups_created         IN OUT   BOOLEAN,
                             I_item_number_type     IN       ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                             I_auto_create          IN       VARCHAR2,
                             I_item_parent          IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                             I_parent_desc          IN       ITEM_MASTER.ITEM_DESC%TYPE,
                             I_parent_diff1         IN       ITEM_MASTER.DIFF_1%TYPE,
                             I_parent_diff2         IN       ITEM_MASTER.DIFF_2%TYPE,
                             I_parent_diff3         IN       ITEM_MASTER.DIFF_3%TYPE,
                             I_parent_diff4         IN       ITEM_MASTER.DIFF_4%TYPE,
                             I_diff_range1_group1   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                             I_diff_range1_group2   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                             I_diff_range2_group1   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                             I_diff_range2_group2   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                             I_multi_range1         IN       DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
                             I_multi_range2         IN       DIFF_RANGE_HEAD.DIFF_RANGE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
END DIFF_APPLY_SQL;
/


