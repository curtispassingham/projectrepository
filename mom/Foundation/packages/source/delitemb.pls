create or replace
PACKAGE BODY DELETE_ITEM_RECORDS_SQL AS
----------------------------------------------------------------
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

LP_table        VARCHAR2(50);
----------------------------------------------------------------------
FUNCTION DEL_ITEM(error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_key_value     IN       ITEM_MASTER.ITEM%TYPE,
                  i_cancel_item   IN       BOOLEAN) RETURN BOOLEAN IS

   L_class                    ITEM_MASTER.CLASS%TYPE;
   L_class_name               CLASS.CLASS_NAME%TYPE;
   L_default_waste_pct        ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;
   L_dept                     ITEM_MASTER.DEPT%TYPE;
   L_dept_name                DEPS.DEPT_NAME%TYPE;
   L_elc_ind                  SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_grandparent_desc         ITEM_MASTER.ITEM_DESC%TYPE;
   L_import_ind               SYSTEM_OPTIONS.IMPORT_IND%TYPE;
   L_item                     ITEM_EXP_DETAIL.ITEM%TYPE;
   L_item_desc                ITEM_MASTER.ITEM_DESC%TYPE;
   L_item_grandparent         ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_item_level               ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_item_parent              ITEM_MASTER.ITEM_PARENT%TYPE;
   L_orderable_ind            ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_ind                 ITEM_MASTER.PACK_IND%TYPE;
   L_pack_tmpl_id             PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE;
   L_pack_type                ITEM_MASTER.PACK_TYPE%TYPE;
   L_parent_desc              ITEM_MASTER.ITEM_DESC%TYPE;
   L_sellable_ind             ITEM_MASTER.SELLABLE_IND%TYPE;
   L_selling_unit_retail_prim ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_prim         ITEM_LOC.SELLING_UOM%TYPE;
   L_short_desc               ITEM_MASTER.SHORT_DESC%TYPE;
   L_simple_pack_ind          ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
   L_status                   ITEM_MASTER.STATUS%TYPE;
   L_std_unit_cost_prim       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_std_unit_retail_prim     ITEM_LOC.UNIT_RETAIL%TYPE;
   L_std_uom_prim             ITEM_MASTER.STANDARD_UOM%TYPE;
   L_subclass                 ITEM_MASTER.SUBCLASS%TYPE;
   L_sub_name                 SUBCLASS.SUB_NAME%TYPE;
   L_tran_level               ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_vdate                    PERIOD.VDATE%TYPE := DATES_SQL.GET_VDATE;
   L_waste_pct                ITEM_MASTER.WASTE_PCT%TYPE;
   L_waste_type               ITEM_MASTER.WASTE_TYPE%TYPE;
   L_pack_tmpl_del_ind        VARCHAR2(1) := 'X';
   L_item_exists              BOOLEAN;
   L_repl_attr_id             REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE;
   L_config_type              POS_CONFIG_ITEMS.POS_CONFIG_TYPE%TYPE;
   L_config_id                POS_CONFIG_ITEMS.POS_CONFIG_ID%TYPE;
   L_num_items                NUMBER(7);
   L_dummy                    VARCHAR(1);
   L_tax_info_tbl             OBJ_TAX_INFO_TBL   := OBJ_TAX_INFO_TBL ();
   L_tax_info_rec             OBJ_TAX_INFO_REC   := OBJ_TAX_INFO_REC ();
   L_repl_attr_id_cnt         NUMBER(7);
   L_cnt_ref                  NUMBER(10);

   TYPE TYP_ROWID              is TABLE of ROWID;
   TBL_ROWID                   TYP_ROWID;
   TBL_CFAROWID                TYP_ROWID;

   cursor C_GET_ITEM is
      select item
        from item_master
       where item_parent = I_key_value
          or item_grandparent = I_key_value;

   cursor C_GET_TRAN_LEVEL_ITEMS is
      select item
        from item_master
       where (item_parent = I_key_value
          or item_grandparent = I_key_value)
         and item_level = tran_level;

   cursor C_GET_PACK_TMPL_ID is
    select i.pack_tmpl_id
      from packitem i,
           pack_tmpl_head h
     where i.pack_no = I_key_value
       and i.pack_tmpl_id = h.pack_tmpl_id
       and h.fash_prepack_ind = 'Y';

   cursor C_LOCK_SKULIST_CRITERIA is
      select sk.rowid
        from skulist_criteria sk,
             (select item
                from gtt_dlyprg_item) im
       where sk.item = im.item
          or sk.item_parent = i_key_value
          or sk.item_grandparent = i_key_value
       order by sk.rowid
         for update of sk.item nowait;

   cursor C_LOCK_SUB_ITEMS_DTL is
      select si.rowid
        from sub_items_detail si,
             (select item
                from gtt_dlyprg_item) im
       where si.item = im.item
          or si.sub_item = im.item
       order by rowid
         for update of si.item nowait;

   cursor C_LOCK_SUB_ITEMS_HEAD is
      select si.rowid
        from sub_items_head si,
             (select item
                from gtt_dlyprg_item ) im
       where si.item = im.item
       order by si.rowid
         for update of si.item nowait;

   cursor C_LOCK_SKULIST_DETAIL is
      select sd.rowid
        from skulist_detail sd,
             (select item
                from gtt_dlyprg_item ) im
       where sd.item = im.item
       order by sd.rowid
         for update of sd.item nowait;

   cursor C_LOCK_INV_STATUS_QTY is
      select inv.rowid
        from inv_status_qty inv,
             (select item
                from gtt_dlyprg_item ) im
       where inv.item = im.item
       order by inv.rowid
         for update of inv.item nowait;

   cursor C_LOCK_REPL_ATTR_UPD_EXCLUDE is
      select repl.rowid
        from repl_attr_update_exclude repl,
             (select item
                from gtt_dlyprg_item ) im
       where repl.item = im.item
       order by repl.rowid
         for update of repl.item nowait;

   cursor C_COUNT_REPL_ATTR_ID is
      select COUNT(rowid)
        from repl_attr_update_item
       where repl_attr_id in (select ri.repl_attr_id
                                from repl_attr_update_item ri,
                                     (select item
                                        from gtt_dlyprg_item ) im
                               where ri.item = im.item);

   cursor C_LOCK_REPL_ATTR_UPDATE_LOC is
      select repl.rowid
        from repl_attr_update_loc repl
       where repl.repl_attr_id in (select ri.repl_attr_id
                                     from repl_attr_update_item ri,
                                          (select item
                                             from gtt_dlyprg_item ) im
                                    where ri.item = im.item)
       order by repl.rowid
         for update of repl.repl_attr_id nowait;

   cursor C_LOCK_REPL_ATTR_UPDATE_HEAD is
      select repl.rowid
        from repl_attr_update_head repl
       where repl.repl_attr_id in (select repl_attr_id
                                     from repl_attr_update_item ri,
                                          (select item
                                             from gtt_dlyprg_item ) im
                                    where ri.item = im.item)
        order by repl.rowid
          for update of repl.repl_attr_id nowait;

   cursor C_LOCK_MASTER_REPL_ATTR is
      select rpl.rowid
        from master_repl_attr rpl,
             (select item
                from gtt_dlyprg_item ) im
       where rpl.item = im.item
       order by rpl.rowid
         for update of rpl.item nowait;

   cursor C_LOCK_REPL_ATTR is
      select rpl.rowid
        from repl_attr_update_item rpl,
             (select item
                from gtt_dlyprg_item ) im
       where rpl.item = im.item
       order by rpl.rowid
         for update of rpl.item nowait;

   cursor C_LOCK_REPL_DAY is
      select rpl.rowid
        from repl_day rpl,
             (select item
                from gtt_dlyprg_item ) im
       where rpl.item = im.item
       order by rpl.rowid
         for update of rpl.item nowait;

   cursor C_LOCK_REPL_ITEM_LOC is
      select rpl.rowid
        from repl_item_loc rpl,
             (select item
                from gtt_dlyprg_item ) im
       where rpl.item = im.item
       order by rpl.rowid
         for update of rpl.item nowait;

   cursor C_LOCK_REPL_ITEM_LOC_UPDATES is
      select rpl.rowid
        from repl_item_loc_updates rpl,
             (select item
                from gtt_dlyprg_item ) im
       where rpl.item = im.item
       order by rpl.rowid
         for update of rpl.item nowait;

  cursor C_LOCK_COST_SS_DTL_LOC is
      select cost.rowid
        from cost_susp_sup_detail_loc cost,
             (select item
                from gtt_dlyprg_item ) im
       where cost.item = im.item
       order by cost.rowid
         for update of cost.item nowait;

   cursor C_LOCK_COST_SS_DTL is
      select cost.rowid
        from cost_susp_sup_detail cost,
             (select item
                from gtt_dlyprg_item ) im
       where cost.item = im.item
       order by cost.rowid
         for update of cost.item nowait;

   cursor C_LOCK_ITEM_EXP_DETAIL is
      select cost.rowid
        from item_exp_detail cost,
             (select item
                from gtt_dlyprg_item ) im
       where cost.item = im.item
       order by cost.rowid
         for update of cost.item nowait;

   cursor C_LOCK_ITEM_EXP_HEAD is
      select cost.rowid
        from item_exp_head cost,
             (select item
                from gtt_dlyprg_item ) im
       where cost.item = im.item
       order by cost.rowid
         for update of cost.item nowait;

   cursor C_LOCK_ITEM_HTS_ASSESS is
      select hts.rowid
        from item_hts_assess hts,
             (select item
                from gtt_dlyprg_item ) im
       where hts.item = im.item
       order by hts.rowid
         for update of hts.item nowait;

   cursor C_LOCK_ITEM_HTS is
      select hts.rowid
        from item_hts hts,
             (select item
                from gtt_dlyprg_item ) im
       where hts.item = im.item
       order by hts.rowid
         for update of hts.item nowait;

   cursor C_LOCK_REQ_DOC is
      select r.rowid
        from req_doc r,
             (select item
                from gtt_dlyprg_item) im
       where r.key_value_2 = im.item
         and module = 'IT'
       order by r.rowid
         for update nowait;

   cursor C_LOCK_ITEM_IMPORT_ATTR is
      select imp.rowid
        from item_import_attr imp,
             (select item
                from gtt_dlyprg_item ) im
       where imp.item = im.item
       order by imp.rowid
         for update of imp.item nowait;

    cursor C_LOCK_TIMELINE is
       select t.rowid
         from timeline t,
              (select item
                 from gtt_dlyprg_item ) im
        where t.timeline_type = 'IT'
          and t.key_value_1 = im.item
           or t.key_value_2 = im.item
        order by rowid
          for update nowait;

   cursor C_LOCK_COND_TARIFF_TREATMENT is
      select ct.rowid
        from cond_tariff_treatment ct,
             (select item
                from gtt_dlyprg_item ) im
       where ct.item = im.item
       order by ct.rowid
         for update of ct.item nowait;

   cursor C_LOCK_ITEM_IMAGE_TL is
      select img.rowid
        from item_image_tl img,
             (select item
                from gtt_dlyprg_item ) im
       where img.item = im.item
       order by img.rowid
         for update of img.item nowait;
         
   cursor C_LOCK_ITEM_IMAGE is
      select img.rowid
        from item_image img,
             (select item
                from gtt_dlyprg_item ) im
       where img.item = im.item
       order by img.rowid
         for update of img.item nowait;

   cursor C_LOCK_ITEM_SUPP_UOM is
      select isu.rowid
        from item_supp_uom isu,
             (select item
                from gtt_dlyprg_item ) im
       where isu.item = im.item
       order by isu.rowid
         for update of isu.item nowait;

   cursor C_LOCK_DEAL_ITEMLOC_ITEM is
      select dii.rowid
        from deal_itemloc_item dii,
             (select item
                from gtt_dlyprg_item ) im
       where dii.item = im.item
       order by dii.rowid
         for update of dii.item nowait;

   cursor C_LOCK_FUTURE_COST is
      select fc.rowid
        from (select item
                from gtt_dlyprg_item) im,
             future_cost fc
       where fc.item = im.item
       order by fc.rowid
         for update of fc.item nowait;

   cursor C_LOCK_DEAL_PARENT_DIFF is
      select dipd.rowid
        from deal_itemloc_parent_diff dipd,
             (select item
                from gtt_dlyprg_item ) im
       where dipd.item_parent = im.item
       order by dipd.rowid
         for update of dipd.item_parent nowait;

   cursor C_LOCK_DEAL_DETAIL is
      select d.rowid
        from deal_detail d,
             (select item
                from gtt_dlyprg_item) im
       where d.qty_thresh_buy_item = im.item
          or d.qty_thresh_get_item = im.item
       order by d.rowid
         for update nowait;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_CE is
      select isc.rowid
        from item_supp_country_cfa_ext isc,
             (select item
                from gtt_dlyprg_item) im
       where isc.item = im.item
       order by isc.rowid
         for update of isc.item nowait;

   cursor C_LOCK_ITEM_SUPP_COUNTRY is
      select sup.rowid
        from item_supp_country sup,
             (select item
                from gtt_dlyprg_item ) im
       where sup.item = im.item
       order by sup.rowid
         for update of sup.item nowait;

   cursor C_LOCK_ITEM_SUPP_MANU_COUNTRY is
      select ismc.rowid
        from item_supp_manu_country ismc,
             (select item
                from gtt_dlyprg_item ) im
       where ismc.item = im.item
       order by ismc.rowid
         for update of ismc.item nowait;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_DIM is
      select dim.rowid
        from item_supp_country_dim dim,
             (select item
                from gtt_dlyprg_item ) im
       where dim.item = im.item
       order by dim.rowid
         for update of dim.item nowait;

   cursor C_LOCK_RECLASS_ITEM is
      select reclass.rowid
        from reclass_item  reclass,
             (select item
                from gtt_dlyprg_item ) im
       where reclass.item = im.item
       order by reclass.rowid
         for update of reclass.item nowait;

    cursor C_LOCK_SUP_AVAIL is
      select sup.rowid
        from sup_avail sup,
             (select item
                from gtt_dlyprg_item ) im
       where sup.item = im.item
       order by sup.rowid
         for update of sup.item nowait;

   cursor C_LOCK_ITEM_LOC_CFA_EXT is
      select ilc.rowid
        from item_loc_cfa_ext ilc
       where exists (select 'X'
                       from item_loc il
                      where (   il.item = I_key_value
                             or il.item_parent = I_key_value
                             or il.item_grandparent = I_key_value)
                        and  ilc.item = il.item
                        and  ilc.loc  = il.loc )
      for update nowait;

   cursor C_LOCK_ITEM_LOC is
      select rowid
        from item_loc
       where item = I_key_value
          or item_parent = I_key_value
          or item_grandparent = I_key_value
         for update nowait;

   cursor C_LOCK_ITEM_LOC_SOH is
      select rowid
        from item_loc_soh
       where item = I_key_value
          or item_parent = I_key_value
          or item_grandparent = I_key_value
         for update nowait;

   cursor C_LOCK_ITEM_SUPPLIER_CFA_EXT is
      select isc.rowid
        from item_supplier_cfa_ext isc,
            (select isp.item, isp.supplier
               from item_supplier isp
              where exists (select 'X'
                             from item_master im
                            where (   im.item             = I_key_value
                                   or im.item_parent      = I_key_value
                                   or im.item_grandparent = I_key_value)
                              and isp.item = im.item)) ip
         where isc.supplier = ip.supplier
           and isc.item     = ip.item
      for update of isc.item nowait;

    cursor C_LOCK_ITEM_SUPPLIER_TL is
       select /*+ ordered */ isp.rowid
         from gtt_dlyprg_item i,
              item_supplier_tl isp
        where isp.item = i.item
        order by isp.rowid
          for update of isp.item nowait;
          
    cursor C_LOCK_ITEM_SUPPLIER is
       select /*+ ordered */ isp.rowid
         from gtt_dlyprg_item i,
              item_supplier isp
        where isp.item = i.item
        order by isp.rowid
          for update of isp.item nowait;

   cursor C_LOCK_PACK_TMPL_DETAIL is
      select 'x'
        from pack_tmpl_detail
       where pack_tmpl_id = L_pack_tmpl_id
         for update nowait;

   cursor C_LOCK_SUPS_PACK_TMPL_DESC_TL is
      select 'x'
        from sups_pack_tmpl_desc_tl
       where pack_tmpl_id = L_pack_tmpl_id
         for update nowait;

   cursor C_LOCK_SUPS_PACK_TMPL_DESC is
      select 'x'
        from sups_pack_tmpl_desc
       where pack_tmpl_id = L_pack_tmpl_id
         for update nowait;

   cursor C_LOCK_PACK_TMPL_HEAD is
      select 'x'
        from pack_tmpl_head
       where pack_tmpl_id = L_pack_tmpl_id
         for update nowait;

   cursor C_LOCK_UDA_ITEM_LOV is
      select uda.rowid
        from uda_item_lov uda,
             (select item
                from gtt_dlyprg_item ) im
       where uda.item = im.item
       order by uda.rowid
         for update of uda.item nowait;

   cursor C_LOCK_UDA_ITEM_DATE is
      select uda.rowid
        from uda_item_date uda,
             (select item
                from gtt_dlyprg_item ) im
       where uda.item = im.item
       order by uda.rowid
         for update of uda.item nowait;

   cursor C_LOCK_UDA_ITEM_FF_TL is
      select udatl.rowid
        from uda_item_ff_tl udatl,
             (select item
                from gtt_dlyprg_item ) im
       where udatl.item = im.item
       order by udatl.rowid
         for update of udatl.item nowait;
         
   cursor C_LOCK_UDA_ITEM_FF is
      select uda.rowid
        from uda_item_ff uda,
             (select item
                from gtt_dlyprg_item ) im
       where uda.item = im.item
       order by uda.rowid
         for update of uda.item nowait;

   cursor C_LOCK_ITEM_SEASONS is
      select its.rowid
        from item_seasons its,
             (select item
                from gtt_dlyprg_item ) im
       where its.item = im.item
       order by its.rowid
         for update of its.item nowait;

   cursor C_LOCK_ITEM_TICKET is
      select it.rowid
        from item_ticket it,
             (select item
                from gtt_dlyprg_item ) im
       where it.item = im.item
       order by it.rowid
         for update of it.item nowait;

  cursor C_LOCK_REPL_RESULTS is
      select rpl.rowid
        from repl_results rpl,
             (select item
                from gtt_dlyprg_item ) im
       where rpl.item = im.item
       order by rpl.rowid
         for update of rpl.item nowait;

   cursor C_LOCK_DAILY_PURGE is
      select p.rowid
        from daily_purge p,
             (select item
                from gtt_dlyprg_item) im
       where p.key_value = im.item
         and p.table_name = 'ITEM_MASTER'
         for update nowait;

   cursor C_LOCK_DAILY_PURGE_II is
      select p.rowid
        from daily_purge p,
             (select item
                from gtt_dlyprg_item) im
       where p.key_value = im.item
         and p.table_name = 'ITEM_MASTER'
         for update nowait;

   cursor C_LOCK_COMP_SHOP_LIST is
      select c1.rowid
        from comp_shop_list c1,
             (select item
                from gtt_dlyprg_item) im
       where c1.ref_item = im.item
          or c1.item = im.item
         for update nowait;

   cursor C_LOCK_COMP_SHOP_LIST_REF_ITEM is
      select 'x'
        from comp_shop_list
       where ref_item = i_key_value
         for update nowait;

   cursor C_LOCK_ITEM_APPROVAL_ERROR is
      select ia.rowid
        from item_approval_error ia,
             (select item
                from gtt_dlyprg_item ) im
       where ia.item = im.item
       order by ia.rowid
         for update of ia.item nowait;

   cursor C_LOCK_TICKET_REQUEST is
      select tr.rowid
        from ticket_request tr,
             (select item
                from gtt_dlyprg_item ) im
       where tr.item = im.item
       order by tr.rowid
         for update of tr.item nowait;

   cursor C_LOCK_SIT_EXPLODE is
      select se.rowid
        from sit_explode se,
             (select item
                from gtt_dlyprg_item ) im
       where se.item = im.item
       order by se.rowid
         for update of se.item nowait;

   cursor C_LOCK_SIT_CONFLICT is
      select sc.rowid
        from sit_conflict sc,
             (select item
                from gtt_dlyprg_item ) im
       where sc.item = im.item
       order by sc.rowid
         for update of sc.item nowait;

   cursor C_LOCK_SOURCE_DLVRY_SCHED_EXC is
      select sd.rowid
        from source_dlvry_sched_exc sd,
             (select item
                from gtt_dlyprg_item ) im
       where sd.item = im.item
       order by sd.rowid
         for update of sd.item nowait;

   cursor C_LOCK_PRICE_HIST is
      select ph.rowid
        from price_hist ph,
             (select item
                from gtt_dlyprg_item ) im
       where ph.item = im.item
       order by ph.rowid
         for update of ph.item nowait;

   cursor C_LOCK_ITEM_LOC_TRAITS is
      select il.rowid
        from item_loc_traits il,
             (select item
                from gtt_dlyprg_item ) im
       where il.item = im.item
       order by il.rowid
         for update of il.item nowait;

   cursor C_LOCK_EDI_DAILY_SALES is
      select edi.rowid
        from edi_daily_sales edi,
             (select item
                from gtt_dlyprg_item ) im
       where edi.item = im.item
       order by edi.rowid
         for update of edi.item nowait;

   cursor C_LOCK_PACKITEM_BREAKOUT is
      select rowid
        from packitem_breakout
       where pack_no in (select item
                           from gtt_dlyprg_item)
         for update nowait;

    cursor C_LOCK_PACKITEM is
      select rowid
        from packitem
       where pack_no in (select item
                           from gtt_dlyprg_item)
         for update nowait;

   cursor C_LOCK_ITM_SUP_CTRY_BRACK_COST is
      select isc.rowid
        from item_supp_country_bracket_cost isc,
             (select item
                from gtt_dlyprg_item ) im
       where isc.item = im.item
       order by isc.rowid
         for update of isc.item nowait;

   cursor C_LOCK_ITM_SUPP_CTRY_LOC_CE is
      select isc.rowid
        from item_supp_country_loc_cfa_ext isc,
             (select isp.item, isp.supplier,
                     isp.origin_country_id, isp.loc
                from item_supp_country_loc isp
               where exists (select 'X'
                               from item_master im
                              where (   im.item             = I_key_value
                                     or im.item_parent      = I_key_value
                                     or im.item_grandparent = I_key_value)
                                and isp.item = im.item)) ip
       where isc.supplier          = ip.supplier
         and isc.item              = ip.item
         and isc.origin_country_id = ip.origin_country_id
         and isc.loc               = ip.loc
         for update of isc.item nowait;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
      select /*+ ordered index(iscl) */
             iscl.rowid
        from (select item
                from gtt_dlyprg_item) i,
             item_supp_country_loc iscl
       where iscl.item = i.item
       order by iscl.rowid
         for update of iscl.item nowait;

   cursor C_LOCK_POS_MERCH_CRITERIA is
      select pos.rowid
        from pos_merch_criteria pos,
             (select item
                from gtt_dlyprg_item ) im
       where pos.item = im.item
       order by pos.rowid
         for update of pos.item nowait;

   cursor C_LOCK_ITEM_CHRG_DETAIL is
      select itc.rowid
        from item_chrg_detail itc,
             (select item
                from gtt_dlyprg_item ) im
       where itc.item = im.item
       order by itc.rowid
         for update of itc.item nowait;

   cursor C_LOCK_ITEM_CHRG_HEAD is
      select itc.rowid
        from item_chrg_head itc,
             (select item
                from gtt_dlyprg_item ) im
       where itc.item = im.item
       order by itc.rowid
         for update of itc.item nowait;

   cursor C_LOCK_ITEM_MFQUEUE is
      select mf.rowid
        from item_mfqueue mf,
             (select item
                from gtt_dlyprg_item ) im
       where mf.item = im.item
       order by mf.rowid
         for update of mf.item nowait;

   cursor C_LOCK_ITEM_PUB_INFO is
      select ipi.rowid
        from item_pub_info ipi,
             (select item
                from gtt_dlyprg_item ) im
       where ipi.item = im.item
         and ipi.published = 'N'
       order by ipi.rowid
         for update of ipi.item nowait;

   cursor C_LOCK_ITEMLOC_MFQUEUE is
      select mf.rowid
        from itemloc_mfqueue mf,
             (select item
                from gtt_dlyprg_item ) im
       where mf.item = im.item
       order by mf.rowid
         for update of mf.item nowait;

   cursor C_LOCK_ITEM_MASTER_TL is
      select rowid
        from item_master_tl
       where item in (select item
                        from item_master 
                       where item = I_key_value
                          or item_parent = I_key_value
                          or item_grandparent = I_key_value)
         for update nowait;
         
   cursor C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where item = I_key_value
          or item_parent = I_key_value
          or item_grandparent = I_key_value
         for update nowait;

   cursor C_LOCK_ITEM_MASTER_CFA_EXT is
      select 'x'
        from item_master_cfa_ext
       where item in( select item
                        from item_master 
                       where item = I_key_value
                          or item_parent = I_key_value
                          or item_grandparent = I_key_value)
         for update nowait;

   cursor C_CHECK_SIMPLE_PACK is
      select pk.pack_no
        from packitem pk,
             (select item
                from gtt_dlyprg_item ) im
       where pk.item = im.item;

   cursor C_LOCK_ITEM_XFORM_HEAD_TL IS
      select ixtl.rowid
        from item_xform_head_tl ixtl
       where ixtl.item_xform_head_id in (select ix.item_xform_head_id
                                           from item_xform_head ix,
                                                (select item
                                                   from gtt_dlyprg_item ) im
                                          where ix.head_item = im.item)
       order by ixtl.rowid
         for update of ixtl.item_xform_head_id nowait;
         
   cursor C_LOCK_ITEM_XFORM_HEAD IS
      select ix.rowid
        from item_xform_head ix,
             (select item
                from gtt_dlyprg_item ) im
       where ix.head_item = im.item
       order by ix.rowid
         for update of ix.head_item nowait;


   cursor C_LOCK_ITEM_XFORM_DETAIL IS
      select ix.rowid
        from item_xform_detail ix,
             (select item
                from gtt_dlyprg_item ) im
       where ix.detail_item = im.item
       order by ix.rowid
         for update of ix.detail_item nowait;


   cursor C_LOCK_DEAL_ITEM_LOC_EXPLODE IS
      select de.rowid
        from deal_item_loc_explode de,
             (select item
                from gtt_dlyprg_item ) im
       where de.item = im.item
       order by de.rowid
         for update of de.item nowait;

   cursor C_GET_REPL_ATTR_ID IS
      select NVL(repl_attr_id,0)
        from repl_attr_update_item repl,
             (select item
                from gtt_dlyprg_item ) im
       where repl.item = I_key_value
         and repl.item = im.item;

   cursor C_LOCK_POS_CONFIG_ITEMS is
      select 'x'
        from pos_config_items
       where item
          in (select item
                from item_master
               where ((item_parent = i_key_value
                      and item_parent is not null)
                  or (item_grandparent = i_key_value
                      and item_grandparent is not null)
                  or item_master.item = i_key_value))
         for update nowait;

   cursor C_LOCK_POS_STORE is
      select 'x'
        from pos_store
       where pos_config_id = L_config_id
         and pos_config_type = L_config_type
         for update nowait;

   cursor C_LOCK_POS_PROD_REST_HEAD is
      select 'x'
        from pos_prod_rest_head
       where pos_prod_rest_id = L_config_id
         for update nowait;

   cursor C_LOCK_POS_COUPON_HEAD is
      select 'x'
        from pos_coupon_head
       where coupon_id = L_config_id
         for update nowait;

   cursor C_GET_CONFIG is
      select distinct pos_config_type,
             pos_config_id
        from pos_config_items
       where item
          in (select item
                from item_master
               where ((item_parent = i_key_value
                      and item_parent is not null)
                  or (item_grandparent = i_key_value
                      and item_grandparent is not null)
                  or item_master.item = i_key_value));

   cursor C_GET_TOTAL_ITEMS is
      select count(item)
        from pos_config_items
       where pos_config_id = L_config_id
         and pos_config_type = L_config_type
       group by pos_config_type,
                pos_config_id;

   cursor C_LOCK_REPL_ITEM_LOC_SUPP_DIST is
      select *
        from repl_item_loc_supp_dist
       where item
          in (select item
                from item_master
               where (   item_parent = i_key_value
                      or item_grandparent = i_key_value
                      or item_master.item = i_key_value))
         for update nowait;

   cursor C_LOCK_ITEM_COUNTRY_L10N_EXT is
      select icle.rowid
        from item_country_l10n_ext icle,
             (select item
                from gtt_dlyprg_item ) im
       where icle.item = im.item
       order by icle.rowid
         for update of icle.item nowait;

   cursor C_LOCK_ITEM_COUNTRY is
      select ic.rowid
        from item_country ic,
             (select item
                from gtt_dlyprg_item ) im
       where ic.item = im.item
       order by ic.rowid
         for update of ic.item nowait;

   cursor C_LOCK_ITEM_COST_DETAIL is
      select icd.rowid
        from item_cost_detail icd,
             (select item
                from gtt_dlyprg_item ) im
       where icd.item = im.item
       order by icd.rowid
         for update of icd.item nowait;

   cursor C_LOCK_ITEM_COST_HEAD is
      select ich.rowid
        from item_cost_head ich,
             (select item
                from gtt_dlyprg_item ) im
       where ich.item = im.item
       order by ich.rowid
         for update of ich.item nowait;

   cursor C_GET_PRICE_HIST is
      select 'x'
        from price_hist
       where item = I_key_value
         and tran_type != 0
         and tran_type != 99 ;

   cursor C_CNT_COMP_SHOP_LIST_REF_ITEM is
      select count(1)
        from comp_shop_list csl1,
             comp_shop_list csl
       where csl.ref_item = I_key_value
         and csl.ref_item <> csl1.ref_item
         and csl.item=csl1.item
         and csl.shopper=csl1.shopper
         and csl.shop_date=csl1.shop_date
         and csl.comp_store=csl1.comp_store;

   cursor C_LOCK_RELATED_ITEM_HEAD_TL is
      select rihtl.rowid
        from related_item_head_tl rihtl
       where rihtl.relationship_id in (select rih.relationship_id
                                         from related_item_head rih,
                                              (select item
                                                 from gtt_dlyprg_item ) im
                                        where rih.item = im.item)
       order by rihtl.rowid
         for update of rihtl.relationship_id nowait;
         
   cursor C_LOCK_RELATED_ITEM_HEAD is
      select rih.rowid
        from related_item_head rih,
             (select item
                from gtt_dlyprg_item ) im
       where rih.item = im.item
       order by rih.rowid
         for update of rih.item nowait;

   cursor C_LOCK_RELATED_ITEM_DETAIL_II is
     select rid.rowid
       from related_item_detail rid,
            (select rih.relationship_id
               from related_item_head rih,
                    (select item
                       from gtt_dlyprg_item ) im
              where rih.item = im.item ) rel_id
       where rid.relationship_id = rel_id.relationship_id
       order by rid.rowid
         for update of rid.relationship_id nowait;

   cursor C_LOCK_RELATED_ITEM_DETAIL is
      select rid.rowid
        from related_item_detail rid,
             (select item
                from gtt_dlyprg_item ) im
       where rid.related_item = im.item
       order by rid.rowid
         for update of rid.relationship_id nowait;
         
   cursor C_LOCK_RMS_OI_CMPLTD is
      select rid.rowid
        from rms_oi_data_stwrd_cmpltd_items rid,
             (select item
                from gtt_dlyprg_item ) im
       where rid.item = im.item
       order by rid.rowid
         for update of rid.item nowait;         

BEGIN
   ---

   if ITEM_VALIDATE_SQL.EXIST(error_message,
                              L_item_exists,
                              I_key_value) = FALSE then
      return FALSE;
   end if;
   ---

   if L_item_exists = TRUE then

      delete from gtt_dlyprg_item;

      insert into gtt_dlyprg_item(item)
         select distinct item
           from item_master
          where (item_parent = I_key_value
                 or item_grandparent = I_key_value
                 or item = I_key_value);

      FOR rec in C_CHECK_SIMPLE_PACK LOOP
         if DEL_ITEM(error_message,
                     rec.pack_no,
                     i_cancel_item) = FALSE then
            return FALSE;
         end if;
      END LOOP;

      if SYSTEM_OPTIONS_SQL.GET_IMPORT_ELC_IND(error_message,
                                               L_import_ind,
                                               L_elc_ind) = FALSE then
         return FALSE;
      end if;
      ---

      if ITEM_ATTRIB_SQL.GET_INFO(error_message,
                                  L_item_desc,                       /* item desc */
                                  L_item_level,                      /* item level */
                                  L_tran_level,                      /* tran level */
                                  L_status,                          /* status */
                                  L_pack_ind,                        /* pack ind */
                                  L_dept,                            /* dept */
                                  L_dept_name,                       /* dept name */
                                  L_class,                           /* class */
                                  L_class_name,                      /* class name */
                                  L_subclass,                        /* subclass */
                                  L_sub_name,                        /* subclass name */
                                  L_sellable_ind,                    /* sellable ind */
                                  L_orderable_ind,                   /* orderable ind */
                                  L_pack_type,                       /* pack type */
                                  L_simple_pack_ind,                 /* simple pack ind */
                                  L_waste_type,                      /* waste type */
                                  L_item_parent,                     /* item parent */
                                  L_item_grandparent,                /* item grandparent */
                                  L_short_desc,                      /* short desc */
                                  L_waste_pct,                       /* waste pct */
                                  L_default_waste_pct,               /* default waste pct */
                                  I_key_value) = FALSE then
         RETURN FALSE;
      end if;

      if L_pack_ind = 'Y' then

         SQL_LIB.SET_MARK('open',
                          'C_GET_PACK_TMPL_ID',
                          'packitem, pack_tmpl_head',
                          'PACK_NO: '||I_key_value);

         open C_GET_PACK_TMPL_ID;

         SQL_LIB.SET_MARK('fetch',
                          'C_GET_PACK_TMPL_ID',
                          'packitem, pack_tmpl_head',
                          'PACK_NO: '||I_key_value);

         fetch C_GET_PACK_TMPL_ID into L_pack_tmpl_id;

         if C_GET_PACK_TMPL_ID%FOUND then
            L_pack_tmpl_del_ind := 'Y';
         end if;

         SQL_LIB.SET_MARK('close',
                          'C_GET_PACK_TMPL_ID',
                          'packitem, pack_tmpl_head',
                          'PACK_NO: '||I_key_value);

         close C_GET_PACK_TMPL_ID;

      end if;  /* end if L_pack_ind = 'Y' */

      if I_cancel_item = FALSE then
         if L_status = 'A' then /* only want to do inserts for items in approved status */
            if L_item_level < L_tran_level then
               if NOT(L_pack_ind = 'Y' AND L_sellable_ind = 'N') then
               
                  if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(error_message,
                                                          L_std_unit_cost_prim,
                                                          L_std_unit_retail_prim,
                                                          L_std_uom_prim,
                                                          L_selling_unit_retail_prim,
                                                          L_selling_uom_prim,
                                                          I_key_value) = FALSE then
                     return FALSE;
                  end if;
               
                  SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST',
                                   'ITEM: '||I_key_value);
               
                  insert into price_hist(tran_type,
                                         reason,
                                         item,
                                         loc,
                                         unit_cost,
                                         unit_retail,
                                         selling_unit_retail,
                                         selling_uom,
                                         action_date)
                                  values(99,                         /* tran type */
                                         0,                          /* reason */
                                         I_key_value,                /* item */
                                         0,                          /* loc */
                                         L_std_unit_cost_prim,       /* unit cost */
                                         L_std_unit_retail_prim,     /* unit retail */
                                         L_selling_unit_retail_prim, /* selling unit retail */
                                         L_selling_uom_prim,         /* selling uom */
                                         TRUNC(to_date(to_char((L_vdate),'DDMMYY')||
                                         to_char(sysdate,'HH24MISS'),'DDMMYYHH24MISS')));
               
                  SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST',
                                   'ITEM: '||I_key_value);
               
                  -- Item/Location record
                  insert into price_hist(tran_type,
                                         reason,
                                         item,
                                         loc,
                                         loc_type,
                                         unit_cost,
                                         unit_retail,
                                         selling_unit_retail,
                                         selling_uom,
                                         action_date)
                                 (select 99,
                                         0,
                                         I_key_value,
                                         il.loc,
                                         il.loc_type,
                                         iscl.unit_cost,
                                         il.unit_retail,
                                         il.selling_unit_retail,
                                         il.selling_uom,
                                         TRUNC(to_date(to_char(L_vdate,'DDMMYY')||to_char(sysdate,'HH24MISS'),'DDMMYYHH24MISS'))
                                    from item_loc il,
                                         item_supp_country_loc iscl
                                   where il.item = I_key_value
                                     and il.item = iscl.item
                                     and il.loc  = iscl.loc);
               
               end if; /* end if NOT(L_pack_ind = 'Y' AND L_sellable_ind = 'N') */
               for c_rec in C_GET_TRAN_LEVEL_ITEMS LOOP
                  if NOT(L_pack_ind = 'Y' AND L_sellable_ind = 'N') then
               
                     if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(error_message,
                                                             L_std_unit_cost_prim,
                                                             L_std_unit_retail_prim,
                                                             L_std_uom_prim,
                                                             L_selling_unit_retail_prim,
                                                             L_selling_uom_prim,
                                                             c_rec.item) = FALSE then
                        return FALSE;
                     end if;
               
                     SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST',
                                      'ITEM: '||c_rec.item);
               
                     insert into price_hist(tran_type,
                                            reason,
                                            item,
                                            loc,
                                            unit_cost,
                                            unit_retail,
                                            selling_unit_retail,
                                            selling_uom,
                                            action_date)
                                     values(99,                         /* tran type */
                                            0,                          /* reason */
                                            c_rec.item,                 /* item */
                                            0,                          /* loc */
                                            L_std_unit_cost_prim,       /* unit cost */
                                            L_std_unit_retail_prim,     /* unit retail */
                                            L_selling_unit_retail_prim, /* selling unit retail */
                                            L_selling_uom_prim,         /* selling uom */
                                            TRUNC(to_date(to_char((L_vdate),'DDMMYY')||
                                            to_char(sysdate,'HH24MISS'),'DDMMYYHH24MISS')));
               
                     SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST',
                                      'ITEM: '||c_rec.item);
               
                     -- Item/Location record
                     insert into price_hist(tran_type,
                                            reason,
                                            item,
                                            loc,
                                            loc_type,
                                            unit_cost,
                                            unit_retail,
                                            selling_unit_retail,
                                            selling_uom,
                                            action_date)
                                           (select 99,
                                            0,
                                            c_rec.item,
                                            il.loc,
                                            il.loc_type,
                                            ils.unit_cost,
                                            il.unit_retail,
                                            il.selling_unit_retail,
                                            il.selling_uom,
                                            TRUNC(to_date(to_char(L_vdate,'DDMMYY')||to_char(sysdate,'HH24MISS'),'DDMMYYHH24MISS'))
                                       from item_loc il,
                                            item_loc_soh ils
                                      where il.item = c_rec.item
                                        and il.item = ils.item
                                        and il.loc  = ils.loc);
               
                  end if; /* end if NOT(L_pack_ind = 'Y' AND L_sellable_ind = 'N') */
               END LOOP; /*End of the FOR Loop */
            elsif L_item_level = L_tran_level then
               if NOT(L_pack_ind = 'Y' AND L_sellable_ind = 'N') then
                  if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(error_message,
                                                          L_std_unit_cost_prim,
                                                          L_std_unit_retail_prim,
                                                          L_std_uom_prim,
                                                          L_selling_unit_retail_prim,
                                                          L_selling_uom_prim,
                                                          I_key_value) = FALSE then
                     return FALSE;
                  end if;
            
                  SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST',
                                   'ITEM: '||I_key_value);
            
                  insert into price_hist(tran_type,
                                         item,
                                         loc,
                                         unit_cost,
                                         unit_retail,
                                         selling_unit_retail,
                                         selling_uom,
                                         action_date)
                                   values(99,                         /* tran type */
                                          I_key_value,                /* item */
                                          0,                          /* loc */
                                          L_std_unit_cost_prim,       /* unit cost */
                                          L_std_unit_retail_prim,     /* unit retail */
                                          L_selling_unit_retail_prim, /* selling unit retail */
                                          L_selling_uom_prim,         /* selling uom */
                                          TRUNC(to_date(to_char((L_vdate),'DDMMYY')||
                                          to_char(sysdate,'HH24MISS'),'DDMMYYHH24MISS')));
            
                  SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST',
                                   'ITEM: '||I_key_value);
            
                  -- Item/Location record
                  insert into price_hist(tran_type,
                                         reason,
                                         item,
                                         loc,
                                         loc_type,
                                         unit_cost,
                                         unit_retail,
                                         selling_unit_retail,
                                         selling_uom,
                                         action_date)
                  (select 99,
                          0,
                          I_key_value,
                          il.loc,
                          il.loc_type,
                          ils.unit_cost,
                          il.unit_retail,
                          il.selling_unit_retail,
                          il.selling_uom,
                          TRUNC(to_date(to_char(L_vdate,'DDMMYY')||to_char(sysdate,'HH24MISS'),'DDMMYYHH24MISS'))
                     from item_loc il,
                          item_loc_soh ils
                    where il.item = I_key_value
                      and il.item = ils.item
                      and il.loc  = ils.loc);
               end if; /* end if NOT(L_pack_ind = 'Y' AND L_sellable_ind = 'N') */
            end if; /* end if L_item_level > L_tran_level */
         end if; /* end if L_status = 'A' */
      end if; /* end if I_cancel_item = FALSE */
      ---

      if i_cancel_item = TRUE then

         LP_table := 'PRICE_HIST';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_PRICE_HIST;
         fetch C_LOCK_PRICE_HIST bulk collect into TBL_ROWID;
         close C_LOCK_PRICE_HIST;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'PRICE_HIST', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from price_hist
                where rowid = TBL_ROWID(i);
         end if;
          ---

      end if; /* end if i_cancel_item = TRUE */

      /* price hist deletion when no prich changes happened */
      open C_GET_PRICE_HIST;
      fetch C_GET_PRICE_HIST into L_dummy;
      if C_GET_PRICE_HIST%NOTFOUND then

         LP_table := 'PRICE_HIST';
         TBL_ROWID := TYP_ROWID();

         open C_LOCK_PRICE_HIST;
         fetch C_LOCK_PRICE_HIST bulk collect into TBL_ROWID;
         close C_LOCK_PRICE_HIST;
         if TBL_ROWID is not NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'PRICE_HIST', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from price_hist
                where rowid = TBL_ROWID(i);
         end if;
         ---

         if PM_RETAIL_API_SQL.DELETE_RPM_ITEM_ZONE_PRICE(error_message,
                                                         I_key_value) = FALSE then
            return FALSE;
         end if;

      end if;
      /* end price hist deletion */

      ---
      if L_pack_ind = 'N' then

         LP_table := 'SUB_ITEMS_DETAIL';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_SUB_ITEMS_DTL;
         fetch C_LOCK_SUB_ITEMS_DTL bulk collect into TBL_ROWID;
         close C_LOCK_SUB_ITEMS_DTL;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'SUB_ITEMS_DETAIL', 'ITEM, SUB_ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from sub_items_detail
             where rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table := 'SUB_ITEMS_HEAD';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_SUB_ITEMS_HEAD;
         fetch C_LOCK_SUB_ITEMS_HEAD bulk collect into TBL_ROWID;
         close C_LOCK_SUB_ITEMS_HEAD;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'SUB_ITEMS_HEAD', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from sub_items_head
             where rowid = TBL_ROWID(i);
         end if;
      end if;
      ---
      LP_table := 'ITEMLOC_MFQUEUE';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEMLOC_MFQUEUE;
      fetch C_LOCK_ITEMLOC_MFQUEUE bulk collect into TBL_ROWID;
      close C_LOCK_ITEMLOC_MFQUEUE;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEMLOC_MFQUEUE', 'ITEM: '||i_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from itemloc_mfqueue
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'ITEM_LOC_TRAITS';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_LOC_TRAITS;
      fetch C_LOCK_ITEM_LOC_TRAITS bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_LOC_TRAITS;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_LOC_TRAITS', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
         delete from item_loc_traits
          where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'ITEM_LOC_SOH';
       TBL_ROWID := TYP_ROWID();

      open C_LOCK_ITEM_LOC_SOH;
      fetch C_LOCK_ITEM_LOC_SOH bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_LOC_SOH;

      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_LOC_SOH', 'Item: '||i_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from item_loc_soh
             where rowid = TBL_ROWID(i);
      end if;

      ---
      LP_table     := 'ITEM_LOC_CFA_EXT';
      TBL_CFAROWID := TYP_ROWID();

      open C_LOCK_ITEM_LOC_CFA_EXT;
      fetch C_LOCK_ITEM_LOC_CFA_EXT bulk collect into TBL_CFAROWID;
      close C_LOCK_ITEM_LOC_CFA_EXT;

      if TBL_CFAROWID is not NULL and TBL_CFAROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_LOC_CFA_EXT', 'Item: '||I_key_value);
         FORALL i in TBL_CFAROWID.first..TBL_CFAROWID.last
            delete from item_loc_cfa_ext
             where rowid = TBL_CFAROWID(i);
      end if;
      ---
      LP_table := 'ITEM_LOC';
      TBL_ROWID := TYP_ROWID();

      open C_LOCK_ITEM_LOC;
      fetch C_LOCK_ITEM_LOC bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_LOC;

      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_LOC', 'Item: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from item_loc
             where rowid = TBL_ROWID(i);
      end if;

      ---

      LP_table := 'SOURCE_DLVRY_SCHED_EXC';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_SOURCE_DLVRY_SCHED_EXC;
      fetch C_LOCK_SOURCE_DLVRY_SCHED_EXC bulk collect into TBL_ROWID;
      close C_LOCK_SOURCE_DLVRY_SCHED_EXC;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'SOURCE_DLVRY_SCHED_EXC', 'Item: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from source_dlvry_sched_exc
             where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'SKULIST_CRITERIA';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_SKULIST_CRITERIA;
      fetch C_LOCK_SKULIST_CRITERIA bulk collect into TBL_ROWID;
      close C_LOCK_SKULIST_CRITERIA;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_CRITERIA','ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from skulist_criteria
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'SKULIST_DETAIL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_SKULIST_DETAIL;
      fetch C_LOCK_SKULIST_DETAIL bulk collect into TBL_ROWID;
      close C_LOCK_SKULIST_DETAIL;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_DETAIL', 'ITEM:  '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from skulist_detail
            where rowid = TBL_ROWID(i);
      end if;
      ---
      L_tax_info_tbl.EXTEND();
      L_tax_info_rec.item                  := I_key_value;
      L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;

      FOR c_rec in C_GET_ITEM LOOP
         L_tax_info_tbl.EXTEND();
         L_tax_info_rec.item                  := c_rec.item;
         L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
      END LOOP;

      if TAX_SQL.DELETE_TAX_ITEM(error_message,
                                 L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
      ---
      LP_table := 'INV_STATUS_QTY';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_INV_STATUS_QTY;
      fetch C_LOCK_INV_STATUS_QTY bulk collect into TBL_ROWID;
      close C_LOCK_INV_STATUS_QTY;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'INV_STATUS_QTY', 'ITEM:  '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from inv_status_qty
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'REQ_DOC';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_REQ_DOC;
      fetch C_LOCK_REQ_DOC bulk collect into TBL_ROWID;
      close C_LOCK_REQ_DOC;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'REQ_DOC', 'KEY VALUE 1: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
         delete from req_doc
          where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'TIMELINE';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_TIMELINE;
      fetch C_LOCK_TIMELINE bulk collect into TBL_ROWID;
      close C_LOCK_TIMELINE;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'TIMELINE', 'KEY VALUE 1: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from timeline
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'ITEM_IMAGE_TL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_IMAGE_TL;
      fetch C_LOCK_ITEM_IMAGE_TL bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_IMAGE_TL;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_IMAGE_TL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_image_tl
            where rowid = TBL_ROWID(i);
      end if;
      ---
      
      LP_table := 'ITEM_IMAGE';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_IMAGE;
      fetch C_LOCK_ITEM_IMAGE bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_IMAGE;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_IMAGE', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_image
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'ITEM_SUPP_UOM';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_SUPP_UOM;
      fetch C_LOCK_ITEM_SUPP_UOM bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_SUPP_UOM;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_UOM', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_supp_uom
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'DEAL_ITEMLOC_ITEM';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_DEAL_ITEMLOC_ITEM;
      fetch C_LOCK_DEAL_ITEMLOC_ITEM bulk collect into TBL_ROWID;
      close C_LOCK_DEAL_ITEMLOC_ITEM;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_DEAL_ITEMLOC_ITEM', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from deal_itemloc_item
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'FUTURE_COST';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_FUTURE_COST;
      fetch C_LOCK_FUTURE_COST bulk collect into TBL_ROWID;
      close C_LOCK_FUTURE_COST;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'FUTURE_COST', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from future_cost
             where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'DEAL_ITEMLOC_PARENT_DIFF';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_DEAL_PARENT_DIFF;
      fetch C_LOCK_DEAL_PARENT_DIFF bulk collect into TBL_ROWID;
      close C_LOCK_DEAL_PARENT_DIFF;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_DEAL_PARENT_DIFF', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from deal_itemloc_parent_diff
             where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'DEAL_DETAIL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_DEAL_DETAIL;
      fetch C_LOCK_DEAL_DETAIL bulk collect into TBL_ROWID;
      close C_LOCK_DEAL_DETAIL;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'DEAL_DETAIL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
         delete from deal_detail
          where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'RECLASS_ITEM';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_RECLASS_ITEM;
      fetch C_LOCK_RECLASS_ITEM bulk collect into TBL_ROWID;
      close C_LOCK_RECLASS_ITEM;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'RECLASS_ITEM', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from reclass_item
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'UDA_ITEM_LOV';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_UDA_ITEM_LOV;
      fetch C_LOCK_UDA_ITEM_LOV bulk collect into TBL_ROWID;
      close C_LOCK_UDA_ITEM_LOV;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'UDA_ITEM_LOV', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from uda_item_lov
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'UDA_ITEM_DATE';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_UDA_ITEM_DATE;
      fetch C_LOCK_UDA_ITEM_DATE bulk collect into TBL_ROWID;
      close C_LOCK_UDA_ITEM_DATE;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'UDA_ITEM_DATE', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from uda_item_date
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'UDA_ITEM_FF_TL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_UDA_ITEM_FF_TL;
      fetch C_LOCK_UDA_ITEM_FF_TL bulk collect into TBL_ROWID;
      close C_LOCK_UDA_ITEM_FF_TL;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'UDA_ITEM_FF_TL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from uda_item_ff_tl
            where rowid = TBL_ROWID(i);
      end if;
      ---
      
      LP_table := 'UDA_ITEM_FF';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_UDA_ITEM_FF;
      fetch C_LOCK_UDA_ITEM_FF bulk collect into TBL_ROWID;
      close C_LOCK_UDA_ITEM_FF;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'UDA_ITEM_FF', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from uda_item_ff
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'ITEM_SEASONS';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_SEASONS;
      fetch C_LOCK_ITEM_SEASONS bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_SEASONS;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SEASONS', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_seasons
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'ITEM_TICKET';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_TICKET;
      fetch C_LOCK_ITEM_TICKET bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_TICKET;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_TICKET', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_ticket
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'DAILY_PURGE';
      TBL_rowid := TYP_rowid();
      open C_LOCK_DAILY_PURGE;
      fetch C_LOCK_DAILY_PURGE bulk collect into TBL_rowid;
      close C_LOCK_DAILY_PURGE;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'DAILY_PURGE', 'KEY_VALUE: '||I_key_value);
         FORALL i in TBL_rowid.first..TBL_rowid.last
           delete from daily_purge
            where rowid = TBL_rowid(i);
      end if;
      ---

      LP_table := 'COMP_SHOP_LIST';
      if L_item_level > L_tran_level then
         open C_LOCK_COMP_SHOP_LIST_REF_ITEM;
         close C_LOCK_COMP_SHOP_LIST_REF_ITEM;
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'COMP_SHOP_LIST',
                          'REF_ITEM: '||i_key_value);
         open C_CNT_COMP_SHOP_LIST_REF_ITEM;
         fetch C_CNT_COMP_SHOP_LIST_REF_ITEM into L_cnt_ref;
         close C_CNT_COMP_SHOP_LIST_REF_ITEM;
         if L_cnt_ref = 0 then
            update comp_shop_list
               set ref_item = NULL
             where ref_item = I_key_value;
         else
            delete from comp_shop_list
             where ref_item = I_key_value;
         end if;

      else
         TBL_rowid := TYP_rowid();
         open C_LOCK_COMP_SHOP_LIST;
         fetch C_LOCK_COMP_SHOP_LIST bulk collect into TBL_rowid;
         close C_LOCK_COMP_SHOP_LIST;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'COMP_SHOP_LIST', 'ITEM: '||I_key_value);
            FORALL i in TBL_rowid.first..TBL_rowid.last
              delete from comp_shop_list
               where rowid = TBL_rowid(i);
         end if;
      end if;
      ---

      LP_table := 'ITEM_APPROVAL_ERROR';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_APPROVAL_ERROR;
      fetch C_LOCK_ITEM_APPROVAL_ERROR bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_APPROVAL_ERROR;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_APPROVAL_ERROR', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_approval_error
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'TICKET_REQUEST';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_TICKET_REQUEST;
      fetch C_LOCK_TICKET_REQUEST bulk collect into TBL_ROWID;
      close C_LOCK_TICKET_REQUEST;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'TICKET_REQUEST', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from ticket_request
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'SIT_CONFLICT';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_SIT_CONFLICT;
      fetch C_LOCK_SIT_CONFLICT bulk collect into TBL_ROWID;
      close C_LOCK_SIT_CONFLICT;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'SIT_CONFLICT', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from sit_conflict
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'SIT_EXPLODE';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_SIT_EXPLODE;
      fetch C_LOCK_SIT_EXPLODE bulk collect into TBL_ROWID;
      close C_LOCK_SIT_EXPLODE;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'SIT_EXPLODE', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from sit_explode
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'POS_MERCH_CRITERIA';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_POS_MERCH_CRITERIA;
      fetch C_LOCK_POS_MERCH_CRITERIA bulk collect into TBL_ROWID;
      close C_LOCK_POS_MERCH_CRITERIA;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'POS_MERCH_CRITERIA', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from pos_merch_criteria
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'POS_CONFIG_ITEMS';
      open C_LOCK_POS_CONFIG_ITEMS;
      fetch C_LOCK_POS_CONFIG_ITEMS into L_dummy;
      if C_LOCK_POS_CONFIG_ITEMS%FOUND then
         SQL_LIB.SET_MARK('UPDATE',NULL,'POS_CONFIG_ITEMS', 'ITEM: '||i_key_value);
         update pos_config_items pci
            set status = 'D'
          where item
             in (select item
                   from item_master
                  where ((item_parent = i_key_value
                         and item_parent is not null)
                     or (item_grandparent = i_key_value
                         and item_grandparent is not null)
                     or item_master.item = i_key_value));

         for c1 in C_GET_CONFIG loop
            L_config_type := c1.pos_config_type;
            L_config_id   := c1.pos_config_id;
            L_num_items   := 0;
            open C_GET_TOTAL_ITEMS;
            fetch C_GET_TOTAL_ITEMS into L_num_items;
            if L_num_items = 1 then
               open C_LOCK_POS_STORE;
               close C_LOCK_POS_STORE;
               SQL_LIB.SET_MARK('UPDATE',NULL,'POS_STORE', 'POS_CONFIG_ID: '||L_config_id);
               update pos_store
                  set status = 'D'
                where pos_config_id = L_config_id
                  and pos_config_type = L_config_type;

               if (L_config_type = 'PRES') then
                  open C_LOCK_POS_PROD_REST_HEAD;
                  close C_LOCK_POS_PROD_REST_HEAD;
                  SQL_LIB.SET_MARK('UPDATE',NULL,'POS_PROD_REST_HEAD', 'POS_PROD_REST_ID: '||L_config_id);
                  update pos_prod_rest_head
                     set pos_config_status = 'D',
                         extract_req_ind = 'Y'
                   where pos_prod_rest_id = L_config_id;
               end if;
               if (L_config_type = 'COUP') then
                  open C_LOCK_POS_COUPON_HEAD;
                  close C_LOCK_POS_COUPON_HEAD;
                  SQL_LIB.SET_MARK('UPDATE',NULL,'POS_COUPON_HEAD', 'COUPON_ID: '||L_config_id);
                  update pos_coupon_head
                     set pos_config_status = 'D',
                         extract_req_ind = 'Y'
                   where coupon_id = L_config_id;
               end if;
            end if;
            close C_GET_TOTAL_ITEMS;
         end loop;
      end if;
      close C_LOCK_POS_CONFIG_ITEMS;
      ---

      LP_table := 'ITEM_CHRG_DETAIL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_CHRG_DETAIL;
      fetch C_LOCK_ITEM_CHRG_DETAIL bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_CHRG_DETAIL;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_CHRG_DETAIL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_chrg_detail
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'ITEM_CHRG_HEAD';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_CHRG_HEAD;
      fetch C_LOCK_ITEM_CHRG_HEAD bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_CHRG_HEAD;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_CHRG_HEAD', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_chrg_head
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'EDI_DAILY_SALES';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_EDI_DAILY_SALES;
      fetch C_LOCK_EDI_DAILY_SALES bulk collect into TBL_ROWID;
      close C_LOCK_EDI_DAILY_SALES;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE', NULL, 'EDI_DAILY_SALES', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from edi_daily_sales
            where rowid = TBL_ROWID(i);
      end if;
      ---

      if L_pack_ind = 'N' then

      ---

         LP_table := 'REPL_ATTR_UPDATE_EXCLUDE';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_REPL_ATTR_UPD_EXCLUDE;
         fetch C_LOCK_REPL_ATTR_UPD_EXCLUDE bulk collect into TBL_ROWID;
         close C_LOCK_REPL_ATTR_UPD_EXCLUDE;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'REPL_ATTR_UPD_EXCLUDE', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from repl_attr_update_exclude
               where rowid = TBL_ROWID(i);
         end if;
         ---
         open C_COUNT_REPL_ATTR_ID;
         fetch C_COUNT_REPL_ATTR_ID into L_repl_attr_id_cnt;
         close C_COUNT_REPL_ATTR_ID;
         ---
         LP_table := 'REPL_ATTR_UPDATE_LOC';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_REPL_ATTR_UPDATE_LOC;
         fetch C_LOCK_REPL_ATTR_UPDATE_LOC bulk collect into TBL_ROWID;
         close C_LOCK_REPL_ATTR_UPDATE_LOC;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 and L_repl_attr_id_cnt >= 1 then
            SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_REPL_ATTR_UPDATE_LOC', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from repl_attr_update_loc
               where rowid = TBL_ROWID(i);

         end if;
         ---
         LP_table := 'REPL_ATTR_UPDATE_HEAD';
         TBL_ROWID := TYP_ROWID();

         open C_LOCK_REPL_ATTR_UPDATE_HEAD;
         fetch C_LOCK_REPL_ATTR_UPDATE_HEAD bulk collect into TBL_ROWID;
         close C_LOCK_REPL_ATTR_UPDATE_HEAD;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 and L_repl_attr_id_cnt >= 1 then
            SQL_LIB.SET_MARK('DELETE',NULL,'REPL_ATTR_UPDATE_HEAD', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from repl_attr_update_head
               where rowid = TBL_ROWID(i);
         end if;

         ---
             open  C_GET_REPL_ATTR_ID;
                     fetch C_GET_REPL_ATTR_ID into L_repl_attr_id;
                     close C_GET_REPL_ATTR_ID;

         LP_table := 'REPL_ATTR_UPDATE_ITEM';
         TBL_ROWID := TYP_ROWID();

         open C_LOCK_REPL_ATTR;
         fetch C_LOCK_REPL_ATTR bulk collect into TBL_ROWID;
         close C_LOCK_REPL_ATTR;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'REPL_ATTR_UPDATE_ITEM', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from repl_attr_update_item
               where rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table := 'MASTER_REPL_ATTR';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_MASTER_REPL_ATTR;
         fetch C_LOCK_MASTER_REPL_ATTR bulk collect into TBL_ROWID;
         close C_LOCK_MASTER_REPL_ATTR;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'MASTER_REPL_ATTR', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from master_repl_attr
               where rowid = TBL_ROWID(i);
         end if;
        ---

         LP_table := 'REPL_DAY';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_REPL_DAY;
         fetch C_LOCK_REPL_DAY bulk collect into TBL_ROWID;
         close C_LOCK_REPL_DAY;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'REPL_DAY', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from repl_day
               where rowid = TBL_ROWID(i);
         end if;
         ---
         LP_table := 'REPL_ITEM_LOC_UPDATES';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_REPL_ITEM_LOC_UPDATES;
         fetch C_LOCK_REPL_ITEM_LOC_UPDATES bulk collect into TBL_ROWID;
         close C_LOCK_REPL_ITEM_LOC_UPDATES;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'REPL_ITEM_LOC_UPDATES', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
             delete from repl_item_loc_updates
              where rowid = TBL_ROWID(i);
         end if;
         ---

         if I_cancel_item = FALSE then

            SQL_LIB.SET_MARK('INSERT',NULL,'REPL_ITEM_LOC_UPDATES',
                             'ITEM: '||I_key_value);

            insert into repl_item_loc_updates(item,
                                              location,
                                              loc_type,
                                              change_type)
                   select l.item,
                          l.location,
                          l.loc_type,
                          'RILD'
                     from repl_item_loc l,
                          gtt_dlyprg_item t
                    where l.item = t.item;
      ---
         end if;

         LP_table := 'REPL_ITEM_LOC';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_REPL_ITEM_LOC;
         fetch C_LOCK_REPL_ITEM_LOC bulk collect into TBL_ROWID;
         close C_LOCK_REPL_ITEM_LOC;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'REPL_ITEM_LOC', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from repl_item_loc
               where rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table := 'SUP_AVAIL';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_SUP_AVAIL;
         fetch C_LOCK_SUP_AVAIL bulk collect into TBL_ROWID;
         close C_LOCK_SUP_AVAIL;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'SUP_AVAIL', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from sup_avail
               where rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table := 'REPL_RESULTS';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_REPL_RESULTS;
         fetch C_LOCK_REPL_RESULTS bulk collect into TBL_ROWID;
         close C_LOCK_REPL_RESULTS;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'REPL_RESULTS', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from repl_results
               where rowid = TBL_ROWID(i);
         end if;
         ---

      end if; /* end if L_pack_ind = 'N' */

      if L_pack_ind = 'Y' then

         LP_table := 'PACKITEM_BREAKOUT';
         TBL_ROWID := TYP_ROWID();

         open C_LOCK_PACKITEM_BREAKOUT;
         fetch C_LOCK_PACKITEM_BREAKOUT bulk collect into TBL_ROWID;
         close C_LOCK_PACKITEM_BREAKOUT;

         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'PACKITEM_BREAKOUT', 'ITEM: '||i_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                  delete from packitem_breakout
                   where rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table := 'PACKITEM';
         TBL_ROWID := TYP_ROWID();

         open C_LOCK_PACKITEM;
         fetch C_LOCK_PACKITEM bulk collect into TBL_ROWID;
         close C_LOCK_PACKITEM;

         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE', NULL, 'PACKITEM', 'ITEM: '||i_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                  delete from packitem
                   where rowid = TBL_ROWID(i);
         end if;

         ---

         if L_orderable_ind = 'Y' then

            LP_table := 'COST_SUSP_SUP_DETAIL_LOC';
            TBL_ROWID := TYP_ROWID();
            open C_LOCK_COST_SS_DTL_LOC;
            fetch C_LOCK_COST_SS_DTL_LOC bulk collect into TBL_ROWID;
            close C_LOCK_COST_SS_DTL_LOC;
            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'COST_SUSP_SUP_DETAIL_LOC', 'ITEM: '||I_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from cost_susp_sup_detail_loc
                where rowid = TBL_ROWID(i);
            end if;
            ---

            LP_table := 'COST_SUSP_SUP_DETAIL';
            TBL_ROWID := TYP_ROWID();
            open C_LOCK_COST_SS_DTL;
            fetch C_LOCK_COST_SS_DTL bulk collect into TBL_ROWID;
            close C_LOCK_COST_SS_DTL;
            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'COST_SUSP_SUP_DETAIL', 'ITEM: '||I_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from cost_susp_sup_detail
                where rowid = TBL_ROWID(i);
            end if;
            ---
            LP_table := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
            TBL_ROWID := TYP_ROWID();
            open C_LOCK_ITM_SUP_CTRY_BRACK_COST;
            fetch C_LOCK_ITM_SUP_CTRY_BRACK_COST bulk collect into TBL_ROWID;
            close C_LOCK_ITM_SUP_CTRY_BRACK_COST;
            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_BRACKET_COST', 'ITEM: '||I_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                 delete from item_supp_country_bracket_cost
                  where rowid = TBL_ROWID(i);
            end if;
            ---
            if L_elc_ind = 'Y' then

               LP_table  := 'ITEM_EXP_DETAIL';
               TBL_ROWID := TYP_ROWID();
               open C_LOCK_ITEM_EXP_DETAIL;
               fetch C_LOCK_ITEM_EXP_DETAIL bulk collect into TBL_ROWID;
               close C_LOCK_ITEM_EXP_DETAIL;
               if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
                  SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_EXP_DETAIL', 'ITEM: '||I_key_value);
                  FORALL i in TBL_ROWID.first..TBL_ROWID.last
                    delete from item_exp_detail
                     where rowid = TBL_ROWID(i);
               end if;
               ---

               LP_table  := 'ITEM_EXP_HEAD';
               TBL_ROWID := TYP_ROWID();
               open C_LOCK_ITEM_EXP_HEAD;
               fetch C_LOCK_ITEM_EXP_HEAD bulk collect into TBL_ROWID;
               close C_LOCK_ITEM_EXP_HEAD;
               if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
                  SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_EXP_HEAD', 'ITEM: '||I_key_value);
                  FORALL i in TBL_ROWID.first..TBL_ROWID.last
                    delete from item_exp_head
                     where rowid = TBL_ROWID(i);
               end if;
               ---

               LP_table  := 'ITEM_HTS_ASSESS';
               TBL_ROWID := TYP_ROWID();
               open C_LOCK_ITEM_HTS_ASSESS;
               fetch C_LOCK_ITEM_HTS_ASSESS bulk collect into TBL_ROWID;
               close C_LOCK_ITEM_HTS_ASSESS;
               if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
                  SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_HTS_ASSESS', 'ITEM: '||I_key_value);
                  FORALL i in TBL_ROWID.first..TBL_ROWID.last
                    delete from item_hts_assess
                     where rowid = TBL_ROWID(i);
               end if;
               ---

               LP_table  := 'ITEM_HTS';
               TBL_ROWID := TYP_ROWID();
               open C_LOCK_ITEM_HTS;
               fetch C_LOCK_ITEM_HTS bulk collect into TBL_ROWID;
               close C_LOCK_ITEM_HTS;
               if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
                  SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_HTS', 'ITEM: '||I_key_value);
                  FORALL i in TBL_ROWID.first..TBL_ROWID.last
                    delete from item_hts
                     where rowid = TBL_ROWID(i);
               end if;
               ---

               LP_table  := 'COND_TARIFF_TREATMENT';
               TBL_ROWID := TYP_ROWID();
               open C_LOCK_COND_TARIFF_TREATMENT;
               fetch C_LOCK_COND_TARIFF_TREATMENT bulk collect into TBL_ROWID;
               close C_LOCK_COND_TARIFF_TREATMENT;
               if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
                  SQL_LIB.SET_MARK('DELETE',NULL,'COND_TARIFF_TREATMENT', 'ITEM: '||I_key_value);
                  FORALL i in TBL_ROWID.first..TBL_ROWID.last
                    delete from cond_tariff_treatment
                     where rowid = TBL_ROWID(i);
               end if;
               ---
            end if; /* end if L_elc_ind = 'Y' */
            ---
            LP_table := 'ITEM_SUPP_COUNTRY_LOC_CFA_EXT';
            TBL_CFAROWID:= TYP_ROWID();

            open C_LOCK_ITM_SUPP_CTRY_LOC_CE;
            fetch C_LOCK_ITM_SUPP_CTRY_LOC_CE bulk collect into TBL_CFAROWID;
            close C_LOCK_ITM_SUPP_CTRY_LOC_CE;

            if TBL_CFAROWID is not NULL and TBL_CFAROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_LOC_CFA_EXT', 'ITEM: '||i_key_value);
               FORALL i in TBL_CFAROWID.first..TBL_CFAROWID.last
                  delete from item_supp_country_loc_cfa_ext
                    where rowid = TBL_CFAROWID(i);
            end if;
            ---
            LP_table := 'ITEM_SUPP_COUNTRY_LOC';
            TBL_ROWID := TYP_ROWID();

            open C_LOCK_ITEM_SUPP_COUNTRY_LOC;
            fetch C_LOCK_ITEM_SUPP_COUNTRY_LOC bulk collect into TBL_ROWID;
            close C_LOCK_ITEM_SUPP_COUNTRY_LOC;

            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_LOC', 'ITEM: '||i_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                  delete from item_supp_country_loc
                   where rowid = TBL_ROWID(i);
            end if;

            ---
            LP_table := 'ITEM_SUPP_COUNTRY_DIM';
            TBL_ROWID := TYP_ROWID();
            open C_LOCK_ITEM_SUPP_COUNTRY_DIM;
            fetch C_LOCK_ITEM_SUPP_COUNTRY_DIM bulk collect into TBL_ROWID;
            close C_LOCK_ITEM_SUPP_COUNTRY_DIM;
            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_DIM', 'ITEM: '||I_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                delete from item_supp_country_dim
                   where  rowid = TBL_ROWID(i);
            end if;
            ---

            LP_table := 'ITEM_SUPP_MANU_COUNTRY';
            TBL_ROWID := TYP_ROWID();
            open C_LOCK_ITEM_SUPP_MANU_COUNTRY;
            fetch C_LOCK_ITEM_SUPP_MANU_COUNTRY bulk collect into TBL_ROWID;
            close C_LOCK_ITEM_SUPP_MANU_COUNTRY;
            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_ITEM_SUPP_MANU_COUNTRY', 'ITEM: '||I_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                delete from item_supp_manu_country
                   where  rowid = TBL_ROWID(i);
            end if;
            ---
            LP_table := 'ITEM_COST_DETAIL';
            TBL_ROWID := TYP_ROWID();
            open C_LOCK_ITEM_COST_DETAIL;
            fetch C_LOCK_ITEM_COST_DETAIL bulk collect into TBL_ROWID;
            close C_LOCK_ITEM_COST_DETAIL;
            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_ITEM_COST_DETAIL', 'ITEM: '||i_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                  delete from item_cost_detail
                   where rowid = TBL_ROWID(i);
            end if;
            ---
            LP_table := 'ITEM_COST_HEAD';
            TBL_ROWID := TYP_ROWID();
            open C_LOCK_ITEM_COST_HEAD;
            fetch C_LOCK_ITEM_COST_HEAD bulk collect into TBL_ROWID;
            close C_LOCK_ITEM_COST_HEAD;
            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_ITEM_COST_HEAD', 'ITEM: '||i_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                  delete from item_cost_head
                   where rowid = TBL_ROWID(i);
            end if;
            ---
            LP_table := 'ITEM_SUPP_COUNTRY_CFA_EXT';
            TBL_CFAROWID:= TYP_ROWID();

            open C_LOCK_ITEM_SUPP_COUNTRY_CE;
            fetch C_LOCK_ITEM_SUPP_COUNTRY_CE bulk collect into TBL_CFAROWID;
            close C_LOCK_ITEM_SUPP_COUNTRY_CE;

            if TBL_CFAROWID is not NULL and TBL_CFAROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_CFA_EXT', 'ITEM: '||i_key_value);
               FORALL i in TBL_CFAROWID.first..TBL_CFAROWID.last
                  delete from item_supp_country_cfa_ext
                    where rowid = TBL_CFAROWID(i);
            end if;
            ---
            LP_table := 'ITEM_SUPP_COUNTRY';
            TBL_ROWID := TYP_ROWID();
            open C_LOCK_ITEM_SUPP_COUNTRY;
            fetch C_LOCK_ITEM_SUPP_COUNTRY bulk collect into TBL_ROWID;
            close C_LOCK_ITEM_SUPP_COUNTRY;
            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY', 'ITEM: '||I_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                 delete from item_supp_country
                  where rowid = TBL_ROWID(i);
            end if;
            ---
            LP_table := 'ITEM_SUPPLIER_CFA_EXT';
            TBL_CFAROWID:= TYP_ROWID();

            open C_LOCK_ITEM_SUPPLIER_CFA_EXT;
            fetch C_LOCK_ITEM_SUPPLIER_CFA_EXT bulk collect into TBL_CFAROWID;
            close C_LOCK_ITEM_SUPPLIER_CFA_EXT;

            if TBL_CFAROWID is not NULL and TBL_CFAROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER_CFA_EXT', 'ITEM: '||i_key_value);
               FORALL i in TBL_CFAROWID.first..TBL_CFAROWID.last
                  delete from item_supplier_cfa_ext
                    where rowid = TBL_CFAROWID(i);
            end if;
            ---
            
            LP_table := 'ITEM_SUPPLIER_TL';
            TBL_ROWID := TYP_ROWID();

            open C_LOCK_ITEM_SUPPLIER_TL;
            fetch C_LOCK_ITEM_SUPPLIER_TL bulk collect into TBL_ROWID;
            close C_LOCK_ITEM_SUPPLIER_TL;

            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER_TL', 'ITEM: '||i_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                  delete from item_supplier_tl
                   where rowid = TBL_ROWID(i);
            end if;
            ---
            
            LP_table := 'ITEM_SUPPLIER';
            TBL_ROWID := TYP_ROWID();

            open C_LOCK_ITEM_SUPPLIER;
            fetch C_LOCK_ITEM_SUPPLIER bulk collect into TBL_ROWID;
            close C_LOCK_ITEM_SUPPLIER;

            if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
               SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER', 'ITEM: '||i_key_value);
               FORALL i in TBL_ROWID.first..TBL_ROWID.last
                  delete from item_supplier
                   where rowid = TBL_ROWID(i);
            end if;
            ---

            ---
         end if; /* end if L_orderable_ind = 'Y' */

         if L_pack_tmpl_del_ind = 'Y' then

            LP_table := 'PACK_TMPL_DETAIL';
            open C_LOCK_PACK_TMPL_DETAIL;
            close C_LOCK_PACK_TMPL_DETAIL;
            SQL_LIB.SET_MARK('DELETE', NULL, 'PACK_TMPL_DETAIL', 'PACK TMPL ID: '||L_pack_tmpl_id);
            delete from pack_tmpl_detail
             where pack_tmpl_id = L_pack_tmpl_id;
            ---

            LP_table := 'SUPS_PACK_TMPL_DESC_TL';
            open C_LOCK_SUPS_PACK_TMPL_DESC_TL;
            close C_LOCK_SUPS_PACK_TMPL_DESC_TL;
            SQL_LIB.SET_MARK('DELETE', NULL, 'SUPS_PACK_TMPL_DESC_TL', 'PACK TMPL ID: '||L_pack_tmpl_id);
            delete from sups_pack_tmpl_desc_tl
             where pack_tmpl_id = L_pack_tmpl_id;
            ---
            
            LP_table := 'SUPS_PACK_TMPL_DESC';
            open C_LOCK_SUPS_PACK_TMPL_DESC;
            close C_LOCK_SUPS_PACK_TMPL_DESC;
            SQL_LIB.SET_MARK('DELETE', NULL, 'SUPS_PACK_TMPL_DESC', 'PACK TMPL ID: '||L_pack_tmpl_id);
            delete from sups_pack_tmpl_desc
             where pack_tmpl_id = L_pack_tmpl_id;
            ---
            LP_table := 'PACK_TMPL_HEAD';
            open C_LOCK_PACK_TMPL_HEAD;
            close C_LOCK_PACK_TMPL_HEAD;
            SQL_LIB.SET_MARK('DELETE', NULL, 'PACK_TMPL_HEAD', 'PACK TMPL ID: '||L_pack_tmpl_id);
            delete from pack_tmpl_head
             where pack_tmpl_id = L_pack_tmpl_id;
            ---
         end if;

      elsif L_pack_ind = 'N' then

         LP_table  := 'ITEM_EXP_DETAIL';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_EXP_DETAIL;
         fetch C_LOCK_ITEM_EXP_DETAIL bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_EXP_DETAIL;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_EXP_DETAIL', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from item_exp_detail
               where rowid = TBL_ROWID(i);
         end if;
         ---
         LP_table  := 'ITEM_EXP_HEAD';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_EXP_HEAD;
         fetch C_LOCK_ITEM_EXP_HEAD bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_EXP_HEAD;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_EXP_HEAD', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from item_exp_head
               where rowid = TBL_ROWID(i);
         end if;
         ---
         ---

         LP_table  := 'ITEM_HTS_ASSESS';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_HTS_ASSESS;
         fetch C_LOCK_ITEM_HTS_ASSESS bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_HTS_ASSESS;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_HTS_ASSESS', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from item_hts_assess
               where rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table  := 'ITEM_HTS';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_HTS;
         fetch C_LOCK_ITEM_HTS bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_HTS;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_HTS', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from item_hts
               where rowid = TBL_ROWID(i);
         end if;
         ---
         LP_table  := 'COND_TARIFF_TREATMENT';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_COND_TARIFF_TREATMENT;
         fetch C_LOCK_COND_TARIFF_TREATMENT bulk collect into TBL_ROWID;
         close C_LOCK_COND_TARIFF_TREATMENT;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'COND_TARIFF_TREATMENT', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from cond_tariff_treatment
               where rowid = TBL_ROWID(i);
         end if;
         ---

      end if; /* end if L_pack_ind = 'Y' */

      if L_pack_ind = 'N' then

         ---
         LP_table := 'COST_SUSP_SUP_DETAIL_LOC';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_COST_SS_DTL_LOC;
         fetch C_LOCK_COST_SS_DTL_LOC bulk collect into TBL_ROWID;
         close C_LOCK_COST_SS_DTL_LOC;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'COST_SUSP_SUP_DETAIL_LOC', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from cost_susp_sup_detail_loc
             where rowid = TBL_ROWID(i);
         end if;
         ---
         LP_table := 'COST_SUSP_SUP_DETAIL';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_COST_SS_DTL;
         fetch C_LOCK_COST_SS_DTL bulk collect into TBL_ROWID;
         close C_LOCK_COST_SS_DTL;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'COST_SUSP_SUP_DETAIL', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from cost_susp_sup_detail
             where rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITM_SUP_CTRY_BRACK_COST;
         fetch C_LOCK_ITM_SUP_CTRY_BRACK_COST bulk collect into TBL_ROWID;
         close C_LOCK_ITM_SUP_CTRY_BRACK_COST;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_BRACKET_COST', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from item_supp_country_bracket_cost
               where rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table := 'REPL_ITEM_LOC_SUPP_DIST';
         open C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
         close C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
         SQL_LIB.SET_MARK('DELETE',NULL,'REPL_ITEM_LOC_SUPP_DIST', 'ITEM: '||I_key_value);
         delete from REPL_ITEM_LOC_SUPP_DIST
          where item
             in (select item
                   from item_master
                   where (   item_parent = I_key_value
                          or item_grandparent = I_key_value
                          or item_master.item = I_key_value));

         ---
         LP_table := 'ITEM_SUPP_COUNTRY_LOC_CFA_EXT';
         TBL_CFAROWID:= TYP_ROWID();

         open C_LOCK_ITM_SUPP_CTRY_LOC_CE;
         fetch C_LOCK_ITM_SUPP_CTRY_LOC_CE bulk collect into TBL_CFAROWID;
         close C_LOCK_ITM_SUPP_CTRY_LOC_CE;

         if TBL_CFAROWID is not NULL and TBL_CFAROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_LOC_CFA_EXT', 'ITEM: '||I_key_value);
            FORALL i in TBL_CFAROWID.first..TBL_CFAROWID.last
               delete from item_supp_country_loc_cfa_ext
                 where rowid = TBL_CFAROWID(i);
         end if;
         ---
         LP_table := 'ITEM_SUPP_COUNTRY_LOC';
         TBL_ROWID := TYP_ROWID();

         open C_LOCK_ITEM_SUPP_COUNTRY_LOC;
         fetch C_LOCK_ITEM_SUPP_COUNTRY_LOC bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_SUPP_COUNTRY_LOC;

         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_LOC', 'ITEM: '||i_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from item_supp_country_loc
                where rowid = TBL_ROWID(i);
         end if;

         ---

         LP_table := 'ITEM_SUPP_COUNTRY_DIM';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_SUPP_COUNTRY_DIM;
         fetch C_LOCK_ITEM_SUPP_COUNTRY_DIM bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_SUPP_COUNTRY_DIM;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_DIM', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
             delete from item_supp_country_dim
                where  rowid = TBL_ROWID(i);
         end if;
         ---

         LP_table := 'ITEM_SUPP_MANU_COUNTRY';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_SUPP_MANU_COUNTRY;
         fetch C_LOCK_ITEM_SUPP_MANU_COUNTRY bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_SUPP_MANU_COUNTRY;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_ITEM_SUPP_MANU_COUNTRY', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
             delete from item_supp_manu_country
                where  rowid = TBL_ROWID(i);
         end if;
         ---
         LP_table := 'ITEM_COST_DETAIL';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_COST_DETAIL;
         fetch C_LOCK_ITEM_COST_DETAIL bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_COST_DETAIL;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_ITEM_COST_DETAIL', 'ITEM: '||i_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from item_cost_detail
                where rowid = TBL_ROWID(i);
         end if;
         ---
         LP_table := 'ITEM_COST_HEAD';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_COST_HEAD;
         fetch C_LOCK_ITEM_COST_HEAD bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_COST_HEAD;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_ITEM_COST_HEAD', 'ITEM: '||i_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from item_cost_head
                where rowid = TBL_ROWID(i);
         end if;
         ---
         LP_table := 'ITEM_SUPP_COUNTRY_CFA_EXT';
         TBL_CFAROWID:= TYP_ROWID();

         open C_LOCK_ITEM_SUPP_COUNTRY_CE;
         fetch C_LOCK_ITEM_SUPP_COUNTRY_CE bulk collect into TBL_CFAROWID;
         close C_LOCK_ITEM_SUPP_COUNTRY_CE;

         if TBL_CFAROWID is not NULL and TBL_CFAROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_CFA_EXT', 'ITEM: '||i_key_value);
            FORALL i in TBL_CFAROWID.first..TBL_CFAROWID.last
               delete from item_supp_country_cfa_ext
                 where rowid = TBL_CFAROWID(i);
         end if;
         ---
         LP_table := 'ITEM_SUPP_COUNTRY';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_SUPP_COUNTRY;
         fetch C_LOCK_ITEM_SUPP_COUNTRY bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_SUPP_COUNTRY;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from item_supp_country
               where rowid = TBL_ROWID(i);
         end if;
         ---
         LP_table := 'ITEM_SUPPLIER_CFA_EXT';
         TBL_CFAROWID:= TYP_ROWID();

         open C_LOCK_ITEM_SUPPLIER_CFA_EXT;
         fetch C_LOCK_ITEM_SUPPLIER_CFA_EXT bulk collect into TBL_CFAROWID;
         close C_LOCK_ITEM_SUPPLIER_CFA_EXT;

         if TBL_CFAROWID is not NULL and TBL_CFAROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER_CFA_EXT', 'ITEM: '||i_key_value);
            FORALL i in TBL_CFAROWID.first..TBL_CFAROWID.last
               delete from item_supplier_cfa_ext
                 where rowid = TBL_CFAROWID(i);
         end if;
         ---

         LP_table := 'ITEM_SUPPLIER_TL';
         TBL_ROWID := TYP_ROWID();

          open C_LOCK_ITEM_SUPPLIER_TL;
         fetch C_LOCK_ITEM_SUPPLIER_TL bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_SUPPLIER_TL;

         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER_TL', 'ITEM: '||i_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from item_supplier_tl
                where rowid = TBL_ROWID(i);
         end if;

         ---
         
         LP_table := 'ITEM_SUPPLIER';
         TBL_ROWID := TYP_ROWID();

          open C_LOCK_ITEM_SUPPLIER;
         fetch C_LOCK_ITEM_SUPPLIER bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_SUPPLIER;

         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER', 'ITEM: '||i_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
               delete from item_supplier
                where rowid = TBL_ROWID(i);
         end if;

      end if; /* end if L_pack_ind = 'N' */

         ---

      if L_import_ind = 'Y' then

         LP_table := 'ITEM_IMPORT_ATTR';
         TBL_ROWID := TYP_ROWID();
         open C_LOCK_ITEM_IMPORT_ATTR;
         fetch C_LOCK_ITEM_IMPORT_ATTR bulk collect into TBL_ROWID;
         close C_LOCK_ITEM_IMPORT_ATTR;
         if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
            SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_IMPORT_ATTR', 'ITEM: '||I_key_value);
            FORALL i in TBL_ROWID.first..TBL_ROWID.last
              delete from item_import_attr
               where rowid = TBL_ROWID(i);
         end if;
      end if; /* end L_import_ind */

         ---
      LP_table := 'ITEM_MFQUEUE';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_MFQUEUE;
      fetch C_LOCK_ITEM_MFQUEUE bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_MFQUEUE;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_MFQUEUE', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_mfqueue
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'ITEM_PUB_INFO';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_PUB_INFO;
      fetch C_LOCK_ITEM_PUB_INFO bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_PUB_INFO;
      if TBL_ROWID is not NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_PUB_INFO', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
            delete from item_pub_info
             where rowid = TBL_ROWID(i);
      end if;

      ---
      LP_table := 'ITEM_SUPPLIER_CFA_EXT';
      TBL_CFAROWID:= TYP_ROWID();

      open C_LOCK_ITEM_SUPPLIER_CFA_EXT;
      fetch C_LOCK_ITEM_SUPPLIER_CFA_EXT bulk collect into TBL_CFAROWID;
      close C_LOCK_ITEM_SUPPLIER_CFA_EXT;

      if TBL_CFAROWID is not NULL and TBL_CFAROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER_CFA_EXT', 'ITEM: '||i_key_value);
         FORALL i in TBL_CFAROWID.first..TBL_CFAROWID.last
            delete from item_supplier_cfa_ext
              where rowid = TBL_CFAROWID(i);
      end if;
      ---

      LP_table := 'ITEM_SUPPLIER_TL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_SUPPLIER_TL;
      fetch C_LOCK_ITEM_SUPPLIER_TL bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_SUPPLIER_TL;

      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER_TL', 'ITEM: '||i_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_supplier_tl
            where rowid = TBL_ROWID(i);
      end if;
      ---
      
      LP_table := 'ITEM_SUPPLIER';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_SUPPLIER;
      fetch C_LOCK_ITEM_SUPPLIER bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_SUPPLIER;

      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPPLIER', 'ITEM: '||i_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_supplier
            where rowid = TBL_ROWID(i);
      end if;
      ---

      LP_table := 'DEAL_ITEM_LOC_EXPLODE';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_DEAL_ITEM_LOC_EXPLODE;
      fetch C_LOCK_DEAL_ITEM_LOC_EXPLODE bulk collect into TBL_ROWID;
      close C_LOCK_DEAL_ITEM_LOC_EXPLODE;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'DEAL_ITEM_LOC_EXPLODE', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from deal_item_loc_explode
            where rowid = TBL_ROWID(i);
      end if;

      LP_table := 'ITEM_XFORM_DETAIL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_XFORM_DETAIL;
      fetch C_LOCK_ITEM_XFORM_DETAIL bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_XFORM_DETAIL;

      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_XFORM_DETAIL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_xform_detail
            where rowid = TBL_ROWID(i);
      end if;
      ---
      
      LP_table := 'ITEM_XFORM_HEAD_TL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_XFORM_HEAD_TL;
      fetch C_LOCK_ITEM_XFORM_HEAD_TL bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_XFORM_HEAD_TL;
      
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_XFORM_HEAD_TL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_xform_head_tl
            where rowid = TBL_ROWID(i);
      end if;
      ---


      LP_table := 'ITEM_XFORM_HEAD';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_XFORM_HEAD;
      fetch C_LOCK_ITEM_XFORM_HEAD bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_XFORM_HEAD;


      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_XFORM_HEAD', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_xform_head
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'ITEM_COUNTRY_L10N_EXT';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_COUNTRY_L10N_EXT;
      fetch C_LOCK_ITEM_COUNTRY_L10N_EXT bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_COUNTRY_L10N_EXT;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_ITEM_COUNTRY_L10N_EXT', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_country_l10n_ext
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'ITEM_COUNTRY';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_COUNTRY;
      fetch C_LOCK_ITEM_COUNTRY bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_COUNTRY;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'C_LOCK_ITEM_COUNTRY', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_country
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'ITEM_MASTER_CFA_EXT';
      open C_LOCK_ITEM_MASTER_CFA_EXT;
      close C_LOCK_ITEM_MASTER_CFA_EXT;
      SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_MASTER_CFA_EXT', 'ITEM: '||I_key_value);
      delete from item_master_cfa_ext
       where item in( select item
                        from item_master 
                       where item = I_key_value
                          or item_parent = I_key_value
                          or item_grandparent = I_key_value) ;
       ---
      LP_table := 'RELATED_ITEM_DETAIL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_RELATED_ITEM_DETAIL_II;
      fetch C_LOCK_RELATED_ITEM_DETAIL_II bulk collect into TBL_ROWID;
      close C_LOCK_RELATED_ITEM_DETAIL_II;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'RELATED_ITEM_DETAIL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from related_item_detail
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'RELATED_ITEM_HEAD_TL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_RELATED_ITEM_HEAD_TL;
      fetch C_LOCK_RELATED_ITEM_HEAD_TL bulk collect into TBL_ROWID;
      close C_LOCK_RELATED_ITEM_HEAD_TL;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'RELATED_ITEM_HEAD_TL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from related_item_head_tl
            where rowid = TBL_ROWID(i);
      end if;
      ---      
      LP_table := 'RELATED_ITEM_HEAD';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_RELATED_ITEM_HEAD;
      fetch C_LOCK_RELATED_ITEM_HEAD bulk collect into TBL_ROWID;
      close C_LOCK_RELATED_ITEM_HEAD;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'RELATED_ITEM_HEAD', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from related_item_head
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'RELATED_ITEM_DETAIL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_RELATED_ITEM_DETAIL;
      fetch C_LOCK_RELATED_ITEM_DETAIL bulk collect into TBL_ROWID;
      close C_LOCK_RELATED_ITEM_DETAIL;
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'RELATED_ITEM_DETAIL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from related_item_detail
            where rowid = TBL_ROWID(i);
      end if;
      ---
      LP_table := 'RMS_OI_DATA_STWRD_CMPLTD_ITEMS';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_RMS_OI_CMPLTD;
      fetch C_LOCK_RMS_OI_CMPLTD bulk collect into TBL_ROWID;
      close C_LOCK_RMS_OI_CMPLTD;
      
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'RMS_OI_DATA_STWRD_CMPLTD_ITEMS', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from rms_oi_data_stwrd_cmpltd_items
            where rowid = TBL_ROWID(i);
      end if;      
      ---
      LP_table := 'ITEM_MASTER_TL';
      TBL_ROWID := TYP_ROWID();
      open C_LOCK_ITEM_MASTER_TL;
      fetch C_LOCK_ITEM_MASTER_TL bulk collect into TBL_ROWID;
      close C_LOCK_ITEM_MASTER_TL;
      
      if TBL_ROWID is NOT NULL and TBL_ROWID.count > 0 then
         SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_MASTER_TL', 'ITEM: '||I_key_value);
         FORALL i in TBL_ROWID.first..TBL_ROWID.last
           delete from item_master_tl
            where rowid = TBL_ROWID(i);
      end if;
     ---      
      LP_table := 'ITEM_MASTER';
      open C_LOCK_ITEM_MASTER;
      close C_LOCK_ITEM_MASTER;
      SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_MASTER', 'ITEM: '||I_key_value);
      delete from item_master
       where item_grandparent = I_key_value;
      delete from item_master
       where item_parent = I_key_value;
      delete from item_master
       where item = I_key_value;
      ---

   else

      LP_table := 'DAILY_PURGE';
      open C_LOCK_DAILY_PURGE_II;
      close C_LOCK_DAILY_PURGE_II;
      SQL_LIB.SET_MARK('DELETE',NULL,'DAILY_PURGE', 'KEY_VALUE: '||I_key_value);
      delete from daily_purge
       where key_value = I_key_value
         and table_name = 'ITEM_MASTER';

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      error_message := sql_lib.create_msg('DELRECS_REC_LOC',
                                          LP_table,
                                          I_key_value);
      return FALSE;

   when OTHERS then
      error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                          'DEL_ITEM', SQLCODE);
      return FALSE;
END DEL_ITEM;
------------------------------------------------------------------
END DELETE_ITEM_RECORDS_SQL;
/