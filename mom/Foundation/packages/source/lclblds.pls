
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LOCLIST_BUILD_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------------------------
-- Function:  LOCK_DETAILS
-- Purpose:   To lock all child records on the LOC_LIST_DETAIL table
--            based on the location list number.
------------------------------------------------------------------------------
FUNCTION LOCK_DETAILS(O_error_message IN OUT VARCHAR2,
                      I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  DELETE_DETAILS
-- Purpose:   To delete all records on the LOC_LIST_DETAIL table
--            based on the location list number.
------------------------------------------------------------------------------
FUNCTION DELETE_DETAILS(O_error_message IN OUT VARCHAR2,
                        I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  LOCK_CRITERIA
-- Purpose:   To lock all records on the LOC_LIST_CRITERIA table 
--            based on the location list number.
------------------------------------------------------------------------------
FUNCTION LOCK_CRITERIA(O_error_message IN OUT VARCHAR2,
                       I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  DELETE_CRITERIA
-- Purpose:   To delete all records on the LOC_LIST_CRITERIA table 
--            based on the location list number.
------------------------------------------------------------------------------
FUNCTION DELETE_CRITERIA(O_error_message IN OUT VARCHAR2,
                         I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  DELETE_CRITERIA_TEMP
-- Purpose:   To lock and delete all records on the LOC_LIST_CRITERIA_TEMP  
--            table based on the location list number.
------------------------------------------------------------------------------
FUNCTION DELETE_CRITERIA_TEMP(O_error_message IN OUT VARCHAR2,
                              I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  REBUILD_LIST
-- Purpose:   Based on the location list number and records on the LOC_LIST_
--            CRITERIA table, rebuild stores and warehouses for the location 
--            list.
------------------------------------------------------------------------------
FUNCTION REBUILD_LIST(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_criteria_exist IN OUT VARCHAR2,
                      I_loc_list       IN     LOC_LIST_HEAD.LOC_LIST%TYPE,
                      I_org_level      IN     VARCHAR2 DEFAULT Null,
                      I_org_id         IN     store.store%type DEFAULT Null)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  TEST_ST_CRITERIA_TEMP
-- Purpose:   Based on the location list number and records on the LOC_LIST_
--            CRITERIA_TEMP table, build and test the SQL query statement for 
--            generating all stores on the location list.
------------------------------------------------------------------------------
FUNCTION TEST_ST_CRITERIA_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_query_stmt    IN OUT VARCHAR2,
                               O_valid         IN OUT BOOLEAN,
                               I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  TEST_WH_CRITERIA_TEMP
-- Purpose:   Based on the location list number and records on the LOC_LIST_
--            CRITERIA_TEMP table, build and test the SQL query statement for 
--            generating all warehouses on the location list.
------------------------------------------------------------------------------
FUNCTION TEST_WH_CRITERIA_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_query_stmt    IN OUT VARCHAR2,
                               O_valid         IN OUT BOOLEAN,
                               I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  POPULATE_CRITERIA_TEMP
-- Purpose:   Copy all records from the LOC_LIST_CRITERIA table to the 
--            LOC_LIST_CRITERIA_TEMP table for the location list . 
------------------------------------------------------------------------------
FUNCTION POPULATE_CRITERIA_TEMP(O_error_message IN OUT VARCHAR2,
                                I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  POPULATE_CRITERIA
-- Purpose:   Copy all records from the LOC_LIST_CRITERIA_TEMP table to the 
--            LOC_LIST_CRITERIA table for the location list . 
------------------------------------------------------------------------------
FUNCTION POPULATE_CRITERIA(O_error_message IN OUT VARCHAR2,
                           I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  COPY_CRITERIA
-- Purpose:   Copy LOC_LIST_CRITERIA records from an existing location list to 
--            a new location list. Called from the Location List Header form
--            when creating a location list based on an existing one.
------------------------------------------------------------------------------
FUNCTION COPY_CRITERIA(O_error_message        IN OUT VARCHAR2,
                       I_loc_list_existing    IN     LOC_LIST_HEAD.LOC_LIST%TYPE,
                       I_loc_list_new         IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  ADD_CRITERIA
-- Purpose:   This function is called from the Location List Detail form.  It
--            updates the LOC_LIST_CRITERIA table to reflect the stores and 
--            warehouses added or deleted through the Location List Detail form.
--            After the LOC_LIST_CRITERIA table is updated, remove all LOC_LIST_
--            DETAIL records marked as 'D' and clear the action_type field of 
--            all records marked as 'A'.
------------------------------------------------------------------------------
FUNCTION ADD_CRITERIA(O_error_message IN OUT VARCHAR2,
                      I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function: DELETE_STAKE_SCHEDULE
-- Purpose:  This function deletes records from the stake_schedule table for the specified --           location_list
------------------------------------------------------------------------------
FUNCTION DELETE_STAKE_SCHEDULE(O_error_message IN OUT VARCHAR2,
                               I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function: LOCK_STAKE_SCHEDULE
-- Purpose:  This function locks the stake_schedule table for the location list passed in
------------------------------------------------------------------------------
FUNCTION LOCK_STAKE_SCHEDULE(O_error_message IN OUT VARCHAR2,
                             I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function: TEST_CRITERIA_CHANGE
-- Purpose:  This function checks weather location list criteria has changed for
--           a passed in location list whose static indicator is on.
------------------------------------------------------------------------------
FUNCTION TEST_CRITERIA_CHANGE(O_error_message IN OUT VARCHAR2,
                              O_valid         OUT    BOOLEAN,
                              I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;
END LOCLIST_BUILD_SQL;     
/



