CREATE OR REPLACE PACKAGE RETAIL_CTX_PKG AUTHID CURRENT_USER AS
----------------------------------------------------------------
-- Name:    SET_APP_CTX
-- Purpose: This procedure will be called from ReSA for data security
--          purpose. This Will set the context for the application user .
----------------------------------------------------------------
  PROCEDURE SET_APP_CTX(p_attribute_name VARCHAR2, p_attribute_value VARCHAR2) ;
END;
/