
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XSTORE_SQL AS

---------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
---------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_STORE
   -- Purpose      :  This method will be responsible for calling STORE_SQL to add the store
   --                 record.
---------------------------------------------------------------------------------------------
FUNCTION CREATE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_STORE_TRAIT
   -- Purpose      :  This function will be responsible for calling the
   --                 LOC_TRAITS_SQL.INSERT_STORE_TRAIT function.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_STORE_TRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_WALKTHROUGH_STORE
   -- Purpose      :  This function will be responsible for calling the
   --                 STORE_SQL.INSERT_WALKTHROUGH_STORE function.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_WALKTHROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store_rec       IN       STORE_SQL.WALK_THROUGH_STORE_TBL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_STORE
   -- Purpose      :  This function will be responsible for calling STORE_SQL to update the
   --                 store record.
---------------------------------------------------------------------------------------------
FUNCTION MODIFY_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_STORE
   -- Purpose      :  This function will be responsible for calling STORE_SQL to delete the
   --                 store record.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_STORE_TRAIT
   -- Purpose      :  This function will be responsible for calling the
   --                 LOC_TRAITS_SQL.DELETE_STORE_TRAIT function.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_STORE_TRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_WALKTHROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
FUNCTION INSERT_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       ADDRESS_SQL.TYPE_ADDRESS_TBL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       ADDRESS_SQL.TYPE_ADDRESS_TBL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       ADDRESS_SQL.TYPE_ADDRESS_TBL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
---------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_store_rec       IN       STORE_SQL.STORE_REC,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XSTORE.LP_cre_type then  

      if not CREATE_STORE(O_error_message,
                          I_store_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_mod_type then
      if not MODIFY_STORE(O_error_message,
                          I_store_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_del_type then
      if not DELETE_STORE(O_error_message,
                          I_store_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_loctrt_cre_type then
      if not INSERT_STORE_TRAIT(O_error_message,
                                I_store_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_loctrt_del_type then
      if not DELETE_STORE_TRAIT(O_error_message,
                                I_store_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_wt_cre_type then
      if not INSERT_WALKTHROUGH_STORE(O_error_message,
                                      I_store_rec.walk_through_tbl) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_wt_del_type then
      if not DELETE_WALKTHROUGH_STORE(O_error_message,
                                      I_store_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_addr_cre_type then
      if not INSERT_ADDR(O_error_message,
                         I_store_rec.address_tbl) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_addr_mod_type then
      if not UPDATE_ADDR(O_error_message,
                         I_store_rec.address_tbl) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XSTORE.LP_addr_del_type then
      if not DELETE_ADDR(O_error_message,
                         I_store_rec.address_tbl) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;


END PERSIST;
---------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION BODY
---------------------------------------------------------------------------------------------
FUNCTION CREATE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.CREATE_STORE';
   L_address_tbl  ADDRESS_SQL.TYPE_ADDRESS_TBL := I_store_rec.address_tbl;

BEGIN

   if not STORE_SQL.INSERT_STORE(O_error_message,
                                 I_store_rec) then
      return FALSE;
   end if;

   if not INSERT_ADDR(O_error_message,
                      L_address_tbl) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_STORE;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_STORE_TRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.INSERT_STORE_TRAIT';

BEGIN

   if not LOC_TRAITS_SQL.INSERT_STORE_TRAIT(O_error_message,
                                            I_store_rec.loc_trait_tbl,
                                            I_store_rec.store_row.store) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_STORE_TRAIT;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_WALKTHROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store_rec       IN       STORE_SQL.WALK_THROUGH_STORE_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.INSERT_WALKTHROUGH_STORE';

BEGIN

   if not STORE_SQL.INSERT_WALK_THROUGH_STORE(O_error_message,
                                              I_store_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_WALKTHROUGH_STORE;
---------------------------------------------------------------------------------------------
FUNCTION MODIFY_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.MODIFY_STORE';
   L_address_tbl  ADDRESS_SQL.TYPE_ADDRESS_TBL := I_store_rec.address_tbl;

BEGIN

   if not STORE_SQL.UPDATE_STORE(O_error_message,
                                 I_store_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_STORE;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.DELETE_STORE';

BEGIN

   if not STORE_SQL.DELETE_STORE(O_error_message,
                                 I_store_rec) then
      return FALSE;
   end if;
      
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_STORE;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_STORE_TRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.DELETE_STORE_TRAIT';

BEGIN

   if I_store_rec.loc_trait_tbl.count > 0 then

      if not LOC_TRAITS_SQL.DELETE_STORE_TRAIT(O_error_message,
                                               I_store_rec.loc_trait_tbl,
                                               I_store_rec.store_row.store) then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_STORE_TRAIT;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_WALKTHROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.DELETE_WALKTHROUGH_STORE';

BEGIN

   if I_store_rec.walk_through_tbl.count > 0 then

      if not STORE_SQL.DELETE_WALK_THROUGH_STORE(O_error_message,
                                                 I_store_rec) then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_WALKTHROUGH_STORE;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       ADDRESS_SQL.TYPE_ADDRESS_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.INSERT_ADDR';

BEGIN

   if ADDRESS_SQL.INSERT_ADDR(O_error_message,
                              I_address_tbl) = FALSE then
      return FALSE;
   end if;
        
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ADDR;
---------------------------------------------------------------------------------------------

FUNCTION UPDATE_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       ADDRESS_SQL.TYPE_ADDRESS_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.UPDATE_ADDR';
   L_seq_no    ADDR.SEQ_NO%TYPE;
   
   cursor C_GET_SEQ_NO(L_external_ref_id   IN   ADDR.EXTERNAL_REF_ID%TYPE) is
      select seq_no
        from addr
       where external_ref_id = L_external_ref_id;

BEGIN

   FOR i in I_address_tbl.first..I_address_tbl.last LOOP
      if I_address_tbl(i).primary_addr_ind = 'Y' then
         open C_GET_SEQ_NO(I_address_tbl(i).external_ref_id);
         fetch C_GET_SEQ_NO into L_seq_no;
         close C_GET_SEQ_NO;
         if ADDRESS_SQL.UPDATE_PRIM_ADD_IND(O_error_message,
                                            I_address_tbl(i).module,
                                            I_address_tbl(i).key_value_1,
                                            I_address_tbl(i).key_value_2,
                                            I_address_tbl(i).addr_type,
                                            L_seq_no) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   if ADDRESS_SQL.UPDATE_ADDR(O_error_message,
                              I_address_tbl) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ADDR;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       ADDRESS_SQL.TYPE_ADDRESS_TBL)
   RETURN BOOLEAN IS

   L_program                 VARCHAR2(50) := 'RMSSUB_XSTORE_SQL.DELETE_ADDR';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_module                  ADDR.MODULE%TYPE;
   L_key_value_1             ADDR.KEY_VALUE_1%TYPE;
   L_key_value_2             ADDR.KEY_VALUE_2%TYPE;
   L_addr_type               ADDR.ADDR_TYPE%TYPE;
   L_seq_no                  ADDR.SEQ_NO%TYPE;
   L_primary_addr_ind        ADDR.PRIMARY_ADDR_IND%TYPE;
   L_mandatory_ind           ADD_TYPE_MODULE.MANDATORY_IND%TYPE;
   
   cursor C_GET_ADDR_INFO(L_external_ref_id   IN   ADDR.EXTERNAL_REF_ID%TYPE) is
      select atm.mandatory_ind ,
             ad.key_value_1,
             ad.key_value_2,
             ad.addr_type,
             ad.seq_no,
             ad.primary_addr_ind,
             ad.module
        from add_type_module atm,
             addr ad
       where ad.addr_type = atm.address_type
         and ad.module = atm.module
         and ad.external_ref_id = L_external_ref_id;

BEGIN

   if I_address_tbl.count > 0 then
      FOR i in I_address_tbl.first..I_address_tbl.last LOOP
         open C_GET_ADDR_INFO(I_address_tbl(i).external_ref_id);
         fetch C_GET_ADDR_INFO into L_mandatory_ind,
                                    L_key_value_1,
                                    L_key_value_2,
                                    L_addr_type,
                                    L_seq_no,
                                    L_primary_addr_ind,
                                    L_module;
         close C_GET_ADDR_INFO;
        
         if ADDRESS_SQL.DELETE_ADDRESSES(O_error_message,
                                         L_key_value_1,
                                         L_key_value_2,
                                         L_addr_type,
                                         L_seq_no,
                                         L_primary_addr_ind,
                                         L_module,
                                         L_mandatory_ind) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_ADDR_INFO%ISOPEN then
         close C_GET_ADDR_INFO;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ADDR;
---------------------------------------------------------------------------------------------
END RMSSUB_XSTORE_SQL;
/
