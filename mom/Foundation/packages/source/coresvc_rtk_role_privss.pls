CREATE OR REPLACE PACKAGE CORESVC_RTK_ROLE_PRIVS AUTHID CURRENT_USER AS
   template_key                  CONSTANT VARCHAR2(255):= 'RTK_ROLE_PRIVS_DATA';
   template_category             CODE_DETAIL.CODE%TYPE := 'RMSSEC';
   
   action_mod                    VARCHAR2(25)          := 'MOD';
   action_new                    VARCHAR2(25)          := 'NEW';
   action_del                    VARCHAR2(25)          := 'DEL';
   
   RTK_ROLE_PRIVS_sheet          VARCHAR2(255)         := 'RTK_ROLE_PRIVS';
   
   RTK_ROLE_PRIVS$Action         NUMBER                :=1;
   RTK_ROLE_PRIVS$ROLE           NUMBER                :=2;
   RTK_ROLE_PRIVS$ORD_APPR_AMT   NUMBER                :=3;
         
   sheet_name_trans              S9T_PKG.trans_map_typ;
   action_column                 VARCHAR2(255)         := 'ACTION';
   
   TYPE RTK_ROLE_PRIVS_rec_tab IS TABLE OF RTK_ROLE_PRIVS%ROWTYPE;
-----------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
-----------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
-----------------------------------------------------------------------------   
END CORESVC_RTK_ROLE_PRIVS;
/
