CREATE OR REPLACE PACKAGE BODY STORE_SQL AS

---------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
---------------------------------------------------------------------------------------------
   -- Function Name: LOCK_WALK_THROUGH_STORE
   -- Purpose      : This function will lock the WALK_THROUGH_STORE table for update or delete.
---------------------------------------------------------------------------------------------
FUNCTION LOCK_WALK_THROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_store_id        IN       STORE.STORE%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
---------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------
FUNCTION INSERT_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'STORE_SQL.INSERT_STORE';
   L_rms_async_id STORE_ADD.RMS_ASYNC_ID%TYPE;

BEGIN

   SQL_LIB.SET_MARK('INSERT', NULL, 'STORE_ADD', 'store: '||I_store_rec.store_row.store);
   L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
   insert into STORE_ADD(store,
                         store_name,
                         store_name10,
                         store_name3,
                         store_name_secondary,
                         store_class,
                         store_mgr_name,
                         store_open_date,
                         store_close_date,
                         acquired_date,
                         remodel_date,
                         fax_number,
                         phone_number,
                         email,
                         total_square_ft,
                         selling_square_ft,
                         linear_distance,
                         vat_region,
                         vat_include_ind,
                         stockholding_ind,
                         channel_id,
                         store_format,
                         mall_name,
                         district,
                         transfer_zone,
                         default_wh,
                         stop_order_days,
                         start_order_days,
                         currency_code,
                         lang,
                         copy_repl_ind,
                         like_store,
                         price_store,
                         cost_location,
                         tran_no_generated,
                         integrated_pos_ind,
                         copy_activity_ind,
                         copy_dlvry_ind,
                         duns_number,
                         duns_loc,
                         sister_store,
                         tsf_entity_id,
                         auto_rcv,
                         store_type,
                         wf_customer_id,
                         timezone_name,
                         org_unit_id,
                         customer_order_loc_ind,
                         copy_clearance_ind,
                         rms_async_id)
                  values(I_store_rec.store_row.store,
                         I_store_rec.store_row.store_name,
                         I_store_rec.store_row.store_name10,
                         I_store_rec.store_row.store_name3,
                         I_store_rec.store_row.store_name_secondary,                         
                         I_store_rec.store_row.store_class,
                         I_store_rec.store_row.store_mgr_name,
                         I_store_rec.store_row.store_open_date,
                         I_store_rec.store_row.store_close_date,
                         I_store_rec.store_row.acquired_date,
                         I_store_rec.store_row.remodel_date,
                         I_store_rec.store_row.fax_number,
                         I_store_rec.store_row.phone_number,
                         I_store_rec.store_row.email,
                         I_store_rec.store_row.total_square_ft,
                         I_store_rec.store_row.selling_square_ft,
                         I_store_rec.store_row.linear_distance,
                         I_store_rec.store_row.vat_region,
                         I_store_rec.store_row.vat_include_ind,
                         I_store_rec.store_row.stockholding_ind,
                         I_store_rec.store_row.channel_id,
                         I_store_rec.store_row.store_format,
                         I_store_rec.store_row.mall_name,
                         I_store_rec.store_row.district,
                         I_store_rec.store_row.transfer_zone,
                         I_store_rec.store_row.default_wh,
                         I_store_rec.store_row.stop_order_days,
                         I_store_rec.store_row.start_order_days,
                         I_store_rec.store_row.currency_code,
                         I_store_rec.store_row.lang,
                         I_store_rec.store_row.copy_repl_ind,
                         I_store_rec.store_row.like_store,
                         I_store_rec.store_row.price_store,
                         I_store_rec.store_row.cost_location,
                         I_store_rec.store_row.tran_no_generated,
                         I_store_rec.store_row.integrated_pos_ind,
                         I_store_rec.store_row.copy_activity_ind,
                         I_store_rec.store_row.copy_dlvry_ind,
                         I_store_rec.store_row.duns_number,
                         I_store_rec.store_row.duns_loc,
                         I_store_rec.store_row.sister_store,
                         I_store_rec.store_row.tsf_entity_id,
                         NVL(I_store_rec.store_row.auto_rcv,'D'),
                         I_store_rec.store_row.store_type,
                         I_store_rec.store_row.wf_customer_id,
                         I_store_rec.store_row.timezone_name,
                         I_store_rec.store_row.org_unit_id,
                         I_store_rec.store_row.customer_order_loc_ind,
                         I_store_rec.store_row.copy_clearance_ind,
                         L_rms_async_id);
                         
	  IF RMS_ASYNC_PROCESS_SQL.ENQUEUE_STORE_ADD(O_error_message,L_rms_async_id)=FALSE THEN
	    RETURN false;
	  END IF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_STORE;
-------------------------------------------------------------------------------
FUNCTION UPDATE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'STORE_SQL.UPDATE_STORE';

BEGIN

   if not LOCK_STORE(O_error_message,
                     I_store_rec.store_row.store) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'STORE', 'store: '||I_store_rec.store_row.store);
   update store
      set store_name              = I_store_rec.store_row.store_name,
          store_name10            = I_store_rec.store_row.store_name10,
          store_name3             = I_store_rec.store_row.store_name3,
          store_name_secondary    = I_store_rec.store_row.store_name_secondary,          


          store_class             = I_store_rec.store_row.store_class,
          store_mgr_name          = I_store_rec.store_row.store_mgr_name,
          store_open_date         = I_store_rec.store_row.store_open_date,
          store_close_date        = I_store_rec.store_row.store_close_date,
          acquired_date           = I_store_rec.store_row.acquired_date,
          remodel_date            = I_store_rec.store_row.remodel_date,
          fax_number              = I_store_rec.store_row.fax_number,
          phone_number            = I_store_rec.store_row.phone_number,
          email                   = I_store_rec.store_row.email,
          total_square_ft         = I_store_rec.store_row.total_square_ft,
          selling_square_ft       = I_store_rec.store_row.selling_square_ft,
          linear_distance         = I_store_rec.store_row.linear_distance,
          vat_region              = I_store_rec.store_row.vat_region,
          vat_include_ind         = I_store_rec.store_row.vat_include_ind,
          channel_id              = I_store_rec.store_row.channel_id,
          store_format            = I_store_rec.store_row.store_format,
          mall_name               = I_store_rec.store_row.mall_name,
          district                = I_store_rec.store_row.district,


          transfer_zone           = I_store_rec.store_row.transfer_zone,
          default_wh              = I_store_rec.store_row.default_wh,
          stop_order_days         = I_store_rec.store_row.stop_order_days,
          start_order_days        = I_store_rec.store_row.start_order_days,


          lang                    = I_store_rec.store_row.lang,
          tran_no_generated       = I_store_rec.store_row.tran_no_generated,
          integrated_pos_ind      = I_store_rec.store_row.integrated_pos_ind,
          duns_number             = I_store_rec.store_row.duns_number,
          duns_loc                = I_store_rec.store_row.duns_loc,
          sister_store            = I_store_rec.store_row.sister_store,
          tsf_entity_id           = I_store_rec.store_row.tsf_entity_id,
          timezone_name           = I_store_rec.store_row.timezone_name,
          wf_customer_id          = I_store_rec.store_row.wf_customer_id,
          customer_order_loc_ind  = I_store_rec.store_row.customer_order_loc_ind
    where store                   = I_store_rec.store_row.store;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   if not ORGANIZATION_SQL.LOCK_STORE_HIERARCHY(O_error_message,
                                                ORGANIZATION_SQL.LP_store,
                                                I_store_rec.store_row.store) then
      return FALSE;
   end if;
   ---
   update store_hierarchy
      set district = I_store_rec.store_row.district,
          region   = (select region
                        from district
                       where district = I_store_rec.store_row.district),
          area     = (select region.area
                        from region,
                             district
                       where region.region = district.region
                         and district.district = I_store_rec.store_row.district),
          chain    = (select area.chain
                        from area,
                             region,
                             district
                       where area.area = region.area
                         and region.region = district.region
                         and district.district = I_store_rec.store_row.district)
    where store = I_store_rec.store_row.store
      and district != I_store_rec.store_row.district;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_STORE;
-------------------------------------------------------------------------------
FUNCTION DELETE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'STORE_SQL.DELETE_STORE';
   L_table          VARCHAR2(5)  := 'STORE';
   L_delete_type    VARCHAR2(1)  := 'D';
   L_delete_order   NUMBER       := 1;

BEGIN

   if not DAILY_PURGE_SQL.INSERT_RECORD (O_error_message,
                                         TO_CHAR(I_store_rec.store_row.store),
                                         L_table,
                                         L_delete_type,
                                         L_delete_order) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_STORE;
-------------------------------------------------------------------------------
FUNCTION INSERT_WALK_THROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_store_rec       IN       STORE_SQL.WALK_THROUGH_STORE_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'STORE_SQL.INSERT_WALK_THROUGH_STORE';
   L_exists       BOOLEAN;

BEGIN

   FOR i in I_store_rec.first..I_store_rec.last LOOP
      SQL_LIB.SET_MARK('INSERT', NULL, 'WALK_THROUGH_STORE', 'store: '||I_store_rec(i).store);
      insert into WALK_THROUGH_STORE(store,
                                     walk_through_store)
                              values(I_store_rec(i).store,
                                     I_store_rec(i).walk_through_store);
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_WALK_THROUGH_STORE;
-------------------------------------------------------------------------------
FUNCTION DELETE_WALK_THROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_store_rec       IN       STORE_SQL.STORE_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'STORE_SQL.DELETE_WALK_THROUGH_STORE';

BEGIN

   if I_store_rec.walk_through_tbl.count > 0 then
      FOR i in I_store_rec.walk_through_tbl.first..I_store_rec.walk_through_tbl.last LOOP

         if not LOCK_WALK_THROUGH_STORE(O_error_message,
                                        I_store_rec.store_row.store) then
            return FALSE;
         end if;

         SQL_LIB.SET_MARK('DELETE', NULL, 'WALK_THROUGH_STORE', 'store: '||TO_CHAR(I_store_rec.store_row.store));
         delete from WALK_THROUGH_STORE
          where store = I_store_rec.store_row.store
            and walk_through_store = I_store_rec.walk_through_tbl(i).walk_through_store;
         ---
         if SQL%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('NO_WALK_THROUGH_DEL');
            return FALSE;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_WALK_THROUGH_STORE;
---------------------------------------------------------------------------------------------
FUNCTION LOCK_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_store_id        IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'STORE_SQL.LOCK_STORE';
   L_table        VARCHAR2(10)  := 'STORE';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_STORE IS
      select 'x'
        from STORE
       where store = I_store_id
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_STORE', 'STORE', 'store: '||I_store_id);
   open C_LOCK_STORE;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_STORE', 'STORE', 'store: '||I_store_id);
   close C_LOCK_STORE;

   ---
   return TRUE;
EXCEPTION

   when RECORD_LOCKED then
      if C_LOCK_STORE%ISOPEN then
         close C_LOCK_STORE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'store '||I_store_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_LOCK_STORE%ISOPEN then
         close C_LOCK_STORE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_STORE;
---------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
FUNCTION LOCK_WALK_THROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_store_id        IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'STORE_SQL.LOCK_WALK_THROUGH_STORE';
   L_table        VARCHAR2(20)  := 'WALK_THROUGH_STORE';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_WALK_THROUGH_STORE IS
      select 'x'
        from WALK_THROUGH_STORE
       where store = I_store_id
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_WALK_THROUGH_STORE', 'STORE', 'store: '||I_store_id);
   open C_LOCK_WALK_THROUGH_STORE;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_WALK_THROUGH_STORE', 'STORE', 'store: '||I_store_id);
   close C_LOCK_WALK_THROUGH_STORE;

   ---
   return TRUE;
EXCEPTION

   when RECORD_LOCKED then
      if C_LOCK_WALK_THROUGH_STORE%ISOPEN then
         close C_LOCK_WALK_THROUGH_STORE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'store '||I_store_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_LOCK_WALK_THROUGH_STORE%ISOPEN then
         close C_LOCK_WALK_THROUGH_STORE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_WALK_THROUGH_STORE;
---------------------------------------------------------------------------------------------
FUNCTION TIMEZONE_NAME_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists         IN OUT BOOLEAN,
                              I_timezone_name  IN     STORE.TIMEZONE_NAME%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(50) := 'STORE_SQL.TIMEZONE_NAME_EXISTS';
   L_ind                VARCHAR2(1)  := 'N';

   cursor C_TIMEZONE_NAME_EXIST is
      select 'Y'
        from v_timezone_names
       where timezone_name = I_timezone_name;
BEGIN
   O_exists := FALSE;
   
   open C_TIMEZONE_NAME_EXIST;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_TIMEZONE_NAME_EXIST',
                    'V_TIMEZONE_NAMES',
                    'Time Zone Name');
                    
   fetch C_TIMEZONE_NAME_EXIST INTO L_ind;
   
   SQL_LIB.SET_MARK('FETCH',
                       'C_TIMEZONE_NAME_EXIST',
                       'V_TIMEZONE_NAMES',
                       'Time Zone Name');
   
   if L_ind = 'Y' then
      O_exists := TRUE;
   end if;
   
   
   close C_TIMEZONE_NAME_EXIST;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_TIMEZONE_NAME_EXIST',
                    'V_TIMEZONE_NAMES',
                    'Time Zone Name');
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_TIMEZONE_NAME_EXIST%ISOPEN then
         close C_TIMEZONE_NAME_EXIST;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;         
END TIMEZONE_NAME_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION DEL_STORE_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_store_id        IN       STORE.STORE%TYPE)
   RETURN BOOLEAN IS
   
   L_program       VARCHAR2(50) := 'SUPPLIER_SETUP_SQL.DEL_SUPS_ATTRIB';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_STORE_ADD_L10N_EXT is
      select 'x'
        from store_add_l10n_ext
       where store = I_store_id
         for update nowait;

   cursor C_LOCK_STORE_L10N_EXT is
      select 'x'
        from store_l10n_ext
       where store = I_store_id
         for update nowait;

BEGIN

   L_table := 'STORE_ADD_L10N_EXT';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_STORE_ADD_L10N_EXT',
                    L_table,
                    'store: '||I_store_id);

   open  C_LOCK_STORE_ADD_L10N_EXT;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_STORE_ADD_L10N_EXT',
                    L_table,
                    'store: '||I_store_id);

   close C_LOCK_STORE_ADD_L10N_EXT;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'store: '||I_store_id);

   delete from store_add_l10n_ext
         where store = I_store_id;
   ---
   L_table := 'STORE_L10N_EXT';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_STORE_L10N_EXT',
                    L_table,
                    'store: '||I_store_id);

   open  C_LOCK_STORE_L10N_EXT;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_STORE_L10N_EXT',
                    L_table,
                    'store: '||I_store_id);

   close C_LOCK_STORE_L10N_EXT;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'store: '||I_store_id);

   delete from store_l10n_ext
         where store = I_store_id;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                            'Store: ' || to_char(I_store_id),
                                             NULL);
   return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;

END DEL_STORE_ATTRIB;
---------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_STORE_ADD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_store_rec       IN       STORE_SQL.STORE_REC,
                                 I_mode            IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program      VARCHAR2(50) := 'STORE_SQL.INSERT_UPDATE_STORE_ADD';
BEGIN
   if I_mode = 'INSERT' then
      insert into store_add (store,
                             store_name,
                             store_name_secondary,
                             store_name10,
                             store_name3,
                             store_class,
                             store_mgr_name,
                             store_open_date,
                             store_close_date,
                             acquired_date,
                             remodel_date,
                             fax_number,
                             phone_number,
                             email,
                             total_square_ft,
                             selling_square_ft,
                             linear_distance,
                             vat_region,
                             vat_include_ind,
                             store_format,
                             mall_name,
                             district,
                             transfer_zone,
                             default_wh,
                             stop_order_days,
                             start_order_days,
                             currency_code,
                             lang,
                             copy_repl_ind,
                             like_store,
                             price_store,
                             cost_location,
                             tran_no_generated,
                             integrated_pos_ind,
                             copy_dlvry_ind,
                             copy_activity_ind,
                             channel_id,
                             stockholding_ind,
                             duns_number,
                             duns_loc,
                             sister_store,
                             tsf_entity_id,
                             org_unit_id,
                             auto_rcv,
                             remerch_ind,
                             store_type,
                             wf_customer_id,
                             timezone_name,
                             customer_order_loc_ind,
                             copy_clearance_ind,
                             rms_async_id)
                      values(I_store_rec.store_row.store,
                             I_store_rec.store_row.store_name,
                             I_store_rec.store_row.store_name_secondary,
                             I_store_rec.store_row.store_name10,
                             I_store_rec.store_row.store_name3,
                             I_store_rec.store_row.store_class,
                             I_store_rec.store_row.store_mgr_name,
                             I_store_rec.store_row.store_open_date,
                             I_store_rec.store_row.store_close_date,
                             I_store_rec.store_row.acquired_date,
                             I_store_rec.store_row.remodel_date,
                             I_store_rec.store_row.fax_number,
                             I_store_rec.store_row.phone_number,
                             I_store_rec.store_row.email,
                             I_store_rec.store_row.total_square_ft,
                             I_store_rec.store_row.selling_square_ft,
                             I_store_rec.store_row.linear_distance,
                             I_store_rec.store_row.vat_region,
                             I_store_rec.store_row.vat_include_ind,
                             I_store_rec.store_row.store_format,
                             I_store_rec.store_row.mall_name,
                             I_store_rec.store_row.district,
                             I_store_rec.store_row.transfer_zone,
                             I_store_rec.store_row.default_wh,
                             I_store_rec.store_row.stop_order_days,
                             I_store_rec.store_row.start_order_days,
                             I_store_rec.store_row.currency_code,
                             I_store_rec.store_row.lang,
                             I_store_rec.store_row.copy_repl_ind,
                             I_store_rec.store_row.like_store,
                             I_store_rec.store_row.price_store,
                             I_store_rec.store_row.cost_location,
                             I_store_rec.store_row.tran_no_generated,
                             I_store_rec.store_row.integrated_pos_ind,
                             I_store_rec.store_row.copy_dlvry_ind,
                             I_store_rec.store_row.copy_activity_ind,
                             I_store_rec.store_row.channel_id,
                             I_store_rec.store_row.stockholding_ind,
                             I_store_rec.store_row.duns_number,
                             I_store_rec.store_row.duns_loc,
                             I_store_rec.store_row.sister_store,
                             I_store_rec.store_row.tsf_entity_id,
                             I_store_rec.store_row.org_unit_id,
                             I_store_rec.store_row.auto_rcv,
                             I_store_rec.store_row.remerch_ind,
                             I_store_rec.store_row.store_type,
                             I_store_rec.store_row.wf_customer_id,
                             I_store_rec.store_row.timezone_name,
                             I_store_rec.store_row.customer_order_loc_ind,
                             I_store_rec.store_row.copy_clearance_ind,
                             I_store_rec.store_row.rms_async_id);
   else
      update store_add 
         set store_name                =   I_store_rec.store_row.store_name,
             store_name_secondary      =   I_store_rec.store_row.store_name_secondary,
             store_name10              =   I_store_rec.store_row.store_name10,
             store_name3               =   I_store_rec.store_row.store_name3,
             store_class               =   I_store_rec.store_row.store_class,
             store_mgr_name            =   I_store_rec.store_row.store_mgr_name,
             store_open_date           =   I_store_rec.store_row.store_open_date,
             store_close_date          =   I_store_rec.store_row.store_close_date,
             acquired_date             =   I_store_rec.store_row.acquired_date,
             remodel_date              =   I_store_rec.store_row.remodel_date,
             fax_number                =   I_store_rec.store_row.fax_number,
             phone_number              =   I_store_rec.store_row.phone_number,
             email                     =   I_store_rec.store_row.email,
             total_square_ft           =   I_store_rec.store_row.total_square_ft,
             selling_square_ft         =   I_store_rec.store_row.selling_square_ft,
             linear_distance           =   I_store_rec.store_row.linear_distance,
             vat_region                =   I_store_rec.store_row.vat_region,
             vat_include_ind           =   I_store_rec.store_row.vat_include_ind,
             store_format              =   I_store_rec.store_row.store_format,
             mall_name                 =   I_store_rec.store_row.mall_name,
             district                  =   I_store_rec.store_row.district,
             transfer_zone             =   I_store_rec.store_row.transfer_zone,
             default_wh                =   I_store_rec.store_row.default_wh,
             stop_order_days           =   I_store_rec.store_row.stop_order_days,
             start_order_days          =   I_store_rec.store_row.start_order_days,
             currency_code             =   I_store_rec.store_row.currency_code,
             lang                      =   I_store_rec.store_row.lang,
             copy_repl_ind             =   I_store_rec.store_row.copy_repl_ind,
             like_store                =   I_store_rec.store_row.like_store,
             price_store               =   I_store_rec.store_row.price_store,
             cost_location             =   I_store_rec.store_row.cost_location,
             tran_no_generated         =   I_store_rec.store_row.tran_no_generated,
             integrated_pos_ind        =   I_store_rec.store_row.integrated_pos_ind,
             copy_dlvry_ind            =   I_store_rec.store_row.copy_dlvry_ind,
             copy_activity_ind         =   I_store_rec.store_row.copy_activity_ind,
             channel_id                =   I_store_rec.store_row.channel_id,
             stockholding_ind          =   I_store_rec.store_row.stockholding_ind,
             duns_number               =   I_store_rec.store_row.duns_number,
             duns_loc                  =   I_store_rec.store_row.duns_loc,
             sister_store              =   I_store_rec.store_row.sister_store,
             tsf_entity_id             =   I_store_rec.store_row.tsf_entity_id,
             org_unit_id               =   I_store_rec.store_row.org_unit_id,
             auto_rcv                  =   I_store_rec.store_row.auto_rcv,
             remerch_ind               =   I_store_rec.store_row.remerch_ind,
             store_type                =   I_store_rec.store_row.store_type,
             wf_customer_id            =   I_store_rec.store_row.wf_customer_id,
             timezone_name             =   I_store_rec.store_row.timezone_name,
             customer_order_loc_ind    =   I_store_rec.store_row.customer_order_loc_ind,
             copy_clearance_ind	       =   I_store_rec.store_row.copy_clearance_ind
       where store =   I_store_rec.store_row.store;
   end if;
   return true;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_UPDATE_STORE_ADD;
------------------------------------------------------------------------------------------
FUNCTION CANCEL_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_id       IN       STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)   := 'STORE_SQL.CANCEL_STORE';
   L_store           STORE.STORE%TYPE;
   L_table           VARCHAR2(30);
   
   RECORD_LOCKED   EXCEPTION;
   


   cursor C_LOCK_ADDR_TL is
      select 'x'
        from addr_tl
       where addr_key in (select addr_key
                            from addr
                           where key_value_1 = TO_CHAR(I_store_id)
                             and module      = 'ST')
         for update nowait;
  
   cursor C_LOCK_ADDR is
      select 'x'
        from addr
       where key_value_1 = TO_CHAR(I_store_id)
         and module      = 'ST'
         for update nowait;
  
   cursor C_LOCK_WALK_THROUGH_STORE is
      select 'x'
        from walk_through_store
       where store = L_store
         for update nowait;
      
   cursor C_LOCK_WF_COST_RELATIONSHIP is
      select 'x'
        from wf_cost_relationship
       where location = L_store
         for update nowait;   
      
   cursor C_LOCK_STORE_ADD_TL is
      select 'x'
        from store_add_tl
       where store = L_store
         for update nowait;
      
   cursor C_LOCK_STORE_ADD is
      select 'x'
        from store_add
       where store = L_store
         for update nowait;
      
       
BEGIN

    ---
   L_store := to_number(I_store_id);
   L_table := 'WALK_THROUGH_STORE';

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_WALK_THROUGH_STORE',
                    'WALK_THROUGH_STORE',
                    'STORE: '||L_store);
   open C_LOCK_WALK_THROUGH_STORE;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_WALK_THROUGH_STORE',
                    'WALK_THROUGH_STORE',
                    'STORE: '||L_store);
   close C_LOCK_WALK_THROUGH_STORE;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'WALK_THROUGH_STORE',
                    'STORE: '||I_store_id);


   delete from walk_through_store
    where store = L_store;
   --- 
   L_table := 'ADDR_TL';

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ADDR_TL',
                    'ADDR_TL',
                    'STORE: '||I_store_id);
   open C_LOCK_ADDR_TL;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ADDR_TL',
                    'ADDR_TL',
                    'STORE: '||I_store_id);
   close C_LOCK_ADDR_TL;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ADDR_TL',
                    'STORE: '||I_store_id);
   delete from addr_tl
         where addr_key in (select addr_key
                              from addr
                             where key_value_1 = TO_CHAR(I_store_id)
                               and module      = 'ST');
   --- 
   L_table := 'ADDR';

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ADDR',
                    'ADDR',
                    'STORE: '||I_store_id);
   open C_LOCK_ADDR;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ADDR',
                    'ADDR',
                    'STORE: '||I_store_id);
   close C_LOCK_ADDR;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ADDR',
                    'STORE: '||I_store_id);

   delete from addr
         where key_value_1 = TO_CHAR(I_store_id)
           and module      = 'ST';
   ---
   L_table := 'WF_COST_RELATIONSHIP';

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_WF_COST_RELATIONSHIP',
                    'WF_COST_RELATIONSHIP',
                    'STORE: '||L_store);
   open C_LOCK_WF_COST_RELATIONSHIP;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_WF_COST_RELATIONSHIP',
                    'WF_COST_RELATIONSHIP',
                    'STORE: '||L_store);
   close C_LOCK_WF_COST_RELATIONSHIP;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'WF_COST_RELATIONSHIP',
                    'STORE: '||I_store_id);
   delete from wf_cost_relationship
    where location = L_store;
   ---
   L_table := 'STORE_ADD_TL';

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_STORE_ADD_TL',
                    'STORE_ADD_TL',
                    'STORE: '||L_store);
   open C_LOCK_STORE_ADD_TL;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_STORE_ADD_TL',
                    'STORE_ADD_TL',
                    'STORE: '||L_store);
   close C_LOCK_STORE_ADD_TL;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'STORE_ADD_TL',
                    'STORE: '||I_store_id);
    
   delete from store_add_tl
    where store = L_store;

   ---
   L_table := 'STORE_ADD';

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_STORE_ADD',
                    'STORE_ADD',
                    'STORE: '||L_store);
   open C_LOCK_STORE_ADD;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_STORE',
                    'STORE_ADD',
                    'STORE: '||L_store);
   close C_LOCK_STORE_ADD;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'STORE_ADD',
                    'STORE: '||I_store_id);
   delete from store_add
    where store = L_store;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ADDR%ISOPEN then
         close C_LOCK_ADDR;
      end if;
      if C_LOCK_ADDR_TL%ISOPEN then
         close C_LOCK_ADDR_TL;
      end if;
      if C_LOCK_WALK_THROUGH_STORE%ISOPEN then
         close C_LOCK_WALK_THROUGH_STORE;
      end if;   
      if C_LOCK_WF_COST_RELATIONSHIP%ISOPEN then
         close C_LOCK_WF_COST_RELATIONSHIP;
      end if;   
      if C_LOCK_STORE_ADD%ISOPEN then
         close C_LOCK_STORE_ADD;
      end if;   
      if C_LOCK_STORE_ADD_TL%ISOPEN then
         close C_LOCK_STORE_ADD_TL;
      end if;   
 
      O_error_message := sql_lib.create_msg('DELRECS_REC_LOC',
                                            L_table,
                                            I_store_id);
      return FALSE;

   when OTHERS then
      if C_LOCK_ADDR%ISOPEN then
         close C_LOCK_ADDR;
      end if;   
      if C_LOCK_ADDR_TL%ISOPEN then
         close C_LOCK_ADDR_TL;
      end if;
      if C_LOCK_WALK_THROUGH_STORE%ISOPEN then
         close C_LOCK_WALK_THROUGH_STORE;
      end if;   
      if C_LOCK_WF_COST_RELATIONSHIP%ISOPEN then
         close C_LOCK_WF_COST_RELATIONSHIP;
      end if;   
      if C_LOCK_STORE_ADD%ISOPEN then
         close C_LOCK_STORE_ADD;
      end if;
      if C_LOCK_STORE_ADD_TL%ISOPEN then
         close C_LOCK_STORE_ADD_TL;
      end if;
    
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);

      return FALSE;
END CANCEL_STORE;              
---------------------------------------------------------------------------------------------- 


END STORE_SQL;
/
