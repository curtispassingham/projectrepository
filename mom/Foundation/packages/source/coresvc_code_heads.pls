CREATE OR REPLACE PACKAGE CORESVC_CODE_HEAD AUTHID CURRENT_USER AS

   template_key               CONSTANT VARCHAR2(255) := 'CODE_HEAD_DATA';
   template_category          CONSTANT VARCHAR2(255) := 'RMSADM';
   action_new                 VARCHAR2(25)           := 'NEW';
   action_mod                 VARCHAR2(25)           := 'MOD';
   action_del                 VARCHAR2(25)           := 'DEL';
   CODE_HEAD_sheet            VARCHAR2(255)          := 'CODE_HEAD';
   CODE_HEAD$ACTION           NUMBER                 :=1;
   CODE_HEAD$CODE_TYPE_DESC   NUMBER                 :=3;
   CODE_HEAD$CODE_TYPE        NUMBER                 :=2;
   CODE_DETAIL_sheet          VARCHAR2(255)          := 'CODE_DETAIL';
   CODE_DETAIL$ACTION         NUMBER                 :=1;
   CODE_DETAIL$CODE_TYPE      NUMBER                 :=2;
   CODE_DETAIL$CODE           NUMBER                 :=3;
   CODE_DETAIL$CODE_DESC      NUMBER                 :=4;
   CODE_DETAIL$REQUIRED_IND   NUMBER                 :=5;
   CODE_DETAIL$CODE_SEQ       NUMBER                 :=6;
   CODE_DETAIL_TL_sheet       VARCHAR2(255)          := 'CODE_DETAIL_TL';
   CODE_DETAIL_TL$ACTION      NUMBER                 :=1;
   CODE_DETAIL_TL$LANG        NUMBER                 :=2;
   CODE_DETAIL_TL$CODE_TYPE   NUMBER                 :=3;
   CODE_DETAIL_TL$CODE        NUMBER                 :=4;
   CODE_DETAIL_TL$CODE_DESC   NUMBER                 :=5;

   Type CODE_HEAD_rec_tab IS TABLE OF CODE_HEAD%ROWTYPE;
   Type CODE_DETAIL_rec_tab IS TABLE OF CODE_DETAIL%ROWTYPE;
   sheet_name_trans           S9T_PKG.trans_map_typ;
   action_column              VARCHAR2(255)          := 'ACTION';
--------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_CODE_HEAD;
/
