
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFF_RANGE_SQL AUTHID CURRENT_USER AS

   TYPE header_info
      is RECORD(group1_desc   DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                group2_desc   DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                group3_desc   DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                type1         DIFF_TYPE.DIFF_TYPE%TYPE,
                type1_desc    DIFF_TYPE.DIFF_TYPE_DESC%TYPE,
                type2         DIFF_TYPE.DIFF_TYPE%TYPE,
                type2_desc    DIFF_TYPE.DIFF_TYPE_DESC%TYPE,
                type3         DIFF_TYPE.DIFF_TYPE%TYPE,
                type3_desc    DIFF_TYPE.DIFF_TYPE_DESC%TYPE);     

-------------------------------------------------------------------------------------------
FUNCTION EXIST(O_error_message    IN OUT VARCHAR2,
               O_exist            IN OUT BOOLEAN,
               I_diff_range       IN     DIFF_RANGE_HEAD.DIFF_RANGE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION LIKE_RANGE(O_error_message    IN OUT VARCHAR2,
                    I_new_range        IN     DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
                    I_new_range_desc   IN     DIFF_RANGE_HEAD.DIFF_RANGE_DESC%TYPE,
                    I_like_range       IN     DIFF_RANGE_HEAD.DIFF_RANGE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION DELETE_RANGE(O_error_message    IN OUT VARCHAR2,
                      I_diff_range       IN     DIFF_RANGE_HEAD.DIFF_RANGE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE(O_error_message IN OUT VARCHAR2,
                         O_duplicate     IN OUT BOOLEAN,
                         I_diff_1        IN     DIFF_RANGE_DETAIL.DIFF_1%TYPE,
                         I_diff_2        IN     DIFF_RANGE_DETAIL.DIFF_2%TYPE,
                         I_diff_3        IN     DIFF_RANGE_DETAIL.DIFF_3%TYPE,
                         I_diff_range    IN     DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE,
                         I_rowid         IN     ROWID)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_header_rec    IN OUT DIFF_RANGE_SQL.HEADER_INFO,
                         I_diff_group1   IN     DIFF_RANGE_HEAD.DIFF_GROUP_1%TYPE,
                         I_diff_group2   IN     DIFF_RANGE_HEAD.DIFF_GROUP_2%TYPE,
                         I_diff_group3   IN     DIFF_RANGE_HEAD.DIFF_GROUP_3%TYPE) 

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_seq_no        IN OUT DIFF_RANGE_DETAIL.SEQ_NO%TYPE,
                         I_diff_range    IN     DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION GET_GROUPS_AND_DESC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        IN OUT BOOLEAN,
                             O_range_desc    IN OUT DIFF_RANGE_HEAD.DIFF_RANGE_DESC%TYPE,
                             O_group_1       IN OUT DIFF_RANGE_HEAD.DIFF_GROUP_1%TYPE,
                             O_group_2       IN OUT DIFF_RANGE_HEAD.DIFF_GROUP_2%TYPE,
                             O_group_3       IN OUT DIFF_RANGE_HEAD.DIFF_GROUP_3%TYPE,
                             I_diff_range    IN     DIFF_RANGE_HEAD.DIFF_RANGE%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------
FUNCTION GET_GROUPS_AND_DESC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        IN OUT BOOLEAN,
                             O_range_desc    IN OUT DIFF_RANGE_HEAD.DIFF_RANGE_DESC%TYPE,
                             O_range_type    IN OUT DIFF_RANGE_HEAD.DIFF_RANGE_TYPE%TYPE,
                             O_group_1       IN OUT DIFF_RANGE_HEAD.DIFF_GROUP_1%TYPE,
                             O_group_2       IN OUT DIFF_RANGE_HEAD.DIFF_GROUP_2%TYPE,
                             O_group_3       IN OUT DIFF_RANGE_HEAD.DIFF_GROUP_3%TYPE,
                             I_diff_range    IN     DIFF_RANGE_HEAD.DIFF_RANGE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END DIFF_RANGE_SQL;
/

