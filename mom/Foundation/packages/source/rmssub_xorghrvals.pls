
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XORGHR_VALIDATE AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
   LP_old_parent_id    NUMBER;
----------------------------------------------------------------------------
--PUBLIC FUNCTIONS
----------------------------------------------------------------------------
---------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                       O_org_hier_rec    OUT    NOCOPY ORGANIZATION_SQL.ORG_HIER_REC,
                       I_message         IN            "RIB_XOrgHrDesc_REC",
                       I_message_type    IN            VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                       O_org_hier_rec    OUT    NOCOPY ORGANIZATION_SQL.ORG_HIER_REC,
                       I_message         IN            "RIB_XOrgHrRef_REC",
                       I_message_type    IN            VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XORGHR_VALIDATE;
/
