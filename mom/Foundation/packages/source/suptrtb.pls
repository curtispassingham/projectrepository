
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SUP_TRAITS_SQL AS
----------------------------------------------------------------------------------------------

FUNCTION TRAIT_EXISTS(O_error_message IN OUT VARCHAR2,
                      O_trait_exists  IN OUT BOOLEAN,
                      I_sup_trait     IN     sup_traits.sup_trait%TYPE)
                      RETURN BOOLEAN IS

   cursor C_FIND_TRAIT is
      select 'Y'
        from sup_traits
       where sup_trait = I_sup_trait;

   L_find_trait   VARCHAR2(1)   := NULL;

BEGIN
   if I_sup_trait is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_sup_trait', 'SUP_TRAITS_SQL.TRAIT_EXISTS', NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_FIND_TRAIT',
                    'sup_traits',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   open C_FIND_TRAIT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_FIND_TRAIT',
                    'sup_traits',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   fetch C_FIND_TRAIT into L_find_trait;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_FIND_TRAIT',
                    'sup_traits',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   close C_FIND_TRAIT;

   if L_find_trait = 'Y' then
      O_trait_exists := TRUE;
   else
      O_trait_exists := FALSE;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_TRAITS_SQL.TRAIT_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;

END TRAIT_EXISTS;
----------------------------------------------------------------------------------------------
FUNCTION SUP_TRAIT_DESC(O_error_message IN OUT VARCHAR2,
                        I_sup_trait     IN     sup_traits.sup_trait%TYPE,
                        O_trait_desc    IN OUT VARCHAR2)
                        RETURN BOOLEAN IS

   cursor C_TRAIT_DESC is
      select description
        from v_sup_traits_tl
       where sup_trait = I_sup_trait;

BEGIN
   if I_sup_trait is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRAIT', NULL, NULL, NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_TRAIT_DESC',
                    'v_sup_traits_tl',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   open C_TRAIT_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_TRAIT_DESC',
                    'v_sup_traits_tl',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   fetch C_TRAIT_DESC into O_trait_desc;

   if C_TRAIT_DESC%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRAIT',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_TRAIT_DESC',
                       'v_sup_traits_tl',
                       'SUP_TRAIT:  '||to_char(I_sup_trait));
      close C_TRAIT_DESC;
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_TRAIT_DESC',
                    'v_sup_traits_tl',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   close C_TRAIT_DESC;


   RETURN TRUE;
  
 EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_TRAITS_SQL.SUP_TRAIT_DESC',
                                            to_char(SQLCODE));
      RETURN FALSE;

END SUP_TRAIT_DESC;
----------------------------------------------------------------------------------------------
FUNCTION MASTER_SUPPLIER(O_error_message  IN OUT VARCHAR2,
                         I_sup_trait      IN     sup_traits.sup_trait%TYPE,
                         O_master_sup_ind IN OUT sup_traits.master_sup_ind%TYPE,
                         O_master_sup     IN OUT sup_traits.master_sup%TYPE)
                         RETURN BOOLEAN IS

   cursor C_MASTER_SUP_INFO is
      select master_sup_ind,
             master_sup
        from sup_traits
       where sup_trait = I_sup_trait;

BEGIN
   if I_sup_trait is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_sup_trait',
                                             'SUP_TRAITS_SQL.MASTER_SUPPLIER',
                                             NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_MASTER_SUP_INFO',
                    'sup_traits',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   open C_MASTER_SUP_INFO;

   SQL_LIB.SET_MARK('FETCH',
                    'C_MASTER_SUP_INFO',
                    'sup_traits',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   fetch C_MASTER_SUP_INFO into O_master_sup_ind,
                                O_master_sup;

   if C_MASTER_SUP_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRAIT',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_MASTER_SUP_INFO',
                       'sup_traits',
                       'SUP_TRAIT:  '||to_char(I_sup_trait));
      close C_MASTER_SUP_INFO;
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_MASTER_SUP_INFO',
                    'sup_traits',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   close C_MASTER_SUP_INFO;

   RETURN TRUE;
  
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_TRAITS_SQL.MASTER_SUPPLIER',
                                            to_char(SQLCODE));
      RETURN FALSE;

END MASTER_SUPPLIER;
----------------------------------------------------------------------------------------------   
FUNCTION RELATION_EXISTS(O_error_message   IN OUT VARCHAR2,
                         O_relation_exists IN OUT BOOLEAN,
                         O_master_exists   IN OUT BOOLEAN,
                         I_supplier        IN     sup_traits_matrix.supplier%TYPE,
                         I_sup_trait       IN     sup_traits.sup_trait%TYPE,
                         I_master_sup_ind  IN     sup_traits.master_sup_ind%TYPE)
                         RETURN BOOLEAN IS

   L_find_relation   VARCHAR2(1)   := NULL;
   L_find_master     VARCHAR2(1)   := NULL;

   cursor C_RELATION_EXISTS is
      select 'Y'
        from sup_traits_matrix
       where supplier = I_supplier
         and sup_trait = I_sup_trait;

   cursor C_MASTER_EXISTS is
      select 'Y'
        from sup_traits,
             sup_traits_matrix
       where sup_traits.master_sup_ind = 'Y'
         and sup_traits.sup_trait = sup_traits_matrix.sup_trait
         and sup_traits_matrix.supplier = I_supplier;

BEGIN
   if I_sup_trait is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TRAIT', NULL, NULL, NULL);
      RETURN FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER', NULL, NULL, NULL);
      RETURN FALSE;
   end if;

   if I_master_sup_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MASTER_SUP_IND', NULL, NULL, NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_RELATION_EXISTS',
                    'sup_traits_matrix',
                    'SUP_TRAIT:  '||to_char(I_sup_trait)||
                    ', SUPPLIER:  '||to_char(I_supplier));
   open C_RELATION_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_RELATION_EXISTS',
                    'sup_traits_matrix',
                    'SUP_TRAIT:  '||to_char(I_sup_trait)||
                    ', SUPPLIER:  '||to_char(I_supplier));
   fetch C_RELATION_EXISTS into L_find_relation;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_RELATION_EXISTS',
                    'sup_traits_matrix',
                    'SUP_TRAIT:  '||to_char(I_sup_trait)||
                    ', SUPPLIER:  '||to_char(I_supplier));

   close C_RELATION_EXISTS;

   if L_find_relation = 'Y' then
      O_relation_exists := TRUE;
   else
      O_relation_exists := FALSE;
   end if;

   if O_relation_exists = FALSE and I_master_sup_ind = 'Y' then  
      SQL_LIB.SET_MARK('OPEN',
                       'C_MASTER_EXISTS',
                       'sup_traits_matrix, sup_traits',
                       'SUPPLIER:  '||to_char(I_supplier));
      open C_MASTER_EXISTS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_MASTER_EXISTS',
                       'sup_traits_matrix, sup_traits',
                       'SUPPLIER:  '||to_char(I_supplier));
      fetch C_MASTER_EXISTS into L_find_master;
 
      SQL_LIB.SET_MARK('CLOSE',
                       'C_MASTER_EXISTS',
                       'sup_traits_matrix, sup_traits',
                       'SUP_TRAIT:  '||to_char(I_sup_trait)||
                       'SUPPLIER:  '||to_char(I_supplier));

      close C_MASTER_EXISTS;

      if L_find_master = 'Y' then
         O_master_exists := TRUE;
      else
         O_master_exists := FALSE;
      end if;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_TRAITS_SQL.RELATION_EXISTS',
                                            to_char(SQLCODE));
      RETURN FALSE;

END RELATION_EXISTS;
----------------------------------------------------------------------------------------------
FUNCTION ROWS_EXIST(O_error_message   IN OUT VARCHAR2,
                    O_rows_exist      IN OUT BOOLEAN,
                    I_supplier        IN     sup_traits_matrix.supplier%TYPE,
                    I_sup_trait       IN     sup_traits.sup_trait%TYPE)
                    RETURN BOOLEAN IS

   L_find_rows   VARCHAR2(255)   := NULL;

   cursor C_MATRIX_ROWS is
      select 'Y'
        from sup_traits_matrix
       where sup_trait = I_sup_trait;

   cursor C_LIST_ROWS is
      select 'Y'
        from sup_traits_matrix
       where supplier = I_supplier;

BEGIN
   if I_supplier is NULL and I_sup_trait is not NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_MATRIX_ROWS',
                       'sup_traits_matrix',
                       'SUP_TRAIT:  '||to_char(I_sup_trait));
      open C_MATRIX_ROWS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_MATRIX_ROWS',
                       'sup_traits_matrix',
                       'SUP_TRAIT:  '||to_char(I_sup_trait));
      fetch C_MATRIX_ROWS into L_find_rows;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_MATRIX_ROWS',
                       'sup_traits_matrix',
                       'SUP_TRAIT:  '||to_char(I_sup_trait));
      close C_MATRIX_ROWS;

      if L_find_rows = 'Y' then
         O_rows_exist := TRUE;
      else
         O_rows_exist := FALSE;
      end if;   

   elsif I_supplier is not NULL and I_sup_trait is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_LIST_ROWS',
                       'sup_traits_matrix',
                       'SUPPLIER:  '||to_char(I_supplier));
      open C_LIST_ROWS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_LIST_ROWS',
                       'sup_traits_matrix',
                       'SUPPLIER:  '||to_char(I_supplier));
      fetch C_LIST_ROWS into L_find_rows;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LIST_ROWS',
                       'sup_traits_matrix',
                       'SUPPLIER:  '||to_char(I_supplier));
      close C_LIST_ROWS;

      if L_find_rows = 'Y' then
         O_rows_exist := TRUE;
      else
         O_rows_exist := FALSE;
      end if;

   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL, NULL, NULL);
      RETURN FALSE;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_TRAITS_SQL.ROWS_EXIST',
                                            to_char(SQLCODE));
      RETURN FALSE;

END ROWS_EXIST;
----------------------------------------------------------------------------------------------                       
FUNCTION DELETE_TRAIT(O_error_message   IN OUT VARCHAR2,
                      I_sup_trait       IN     sup_traits.sup_trait%TYPE)
                      RETURN BOOLEAN IS

   L_supplier     sup_traits_matrix.supplier%TYPE;
   L_exception_id NUMBER(2);
   L_rowid        VARCHAR2(18);
   L_dummy        VARCHAR2(1);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_SUP_MATRIX is
      select supplier, 
             rowid
        from sup_traits_matrix
       where sup_trait = I_sup_trait;

   cursor C_MATRIX_LOCK is
      select 'x'
        from sup_traits_matrix
       where rowid = L_rowid 
         for update nowait;

   cursor C_LOCK_SUP_TRAITS_TL is
      select 'x'
        from sup_traits_tl
       where sup_trait = I_sup_trait 
         for update nowait;

   cursor C_LOCK_SUP_TRAITS is
      select rowid
        from sup_traits
       where sup_trait = I_sup_trait 
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'sup_traits_matrix',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));
   L_exception_id := 1;
   for c_rec in  C_GET_SUP_MATRIX
   LOOP
      L_supplier  := c_rec.supplier;
      L_rowid     := c_rec.rowid;
      open C_MATRIX_LOCK;
      fetch C_MATRIX_LOCK into L_dummy;
      close C_MATRIX_LOCK;
   END LOOP;  

   delete
     from sup_traits_matrix
    where sup_trait = I_sup_trait;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'sup_traits_tl',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));

   L_exception_id := 2;
   
   open C_LOCK_SUP_TRAITS_TL;
   close C_LOCK_SUP_TRAITS_TL;

   delete
     from sup_traits_tl
    where sup_trait = I_sup_trait;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'sup_traits',
                    'SUP_TRAIT:  '||to_char(I_sup_trait));

   open C_LOCK_SUP_TRAITS;
   fetch C_LOCK_SUP_TRAITS into L_rowid;
   close C_LOCK_SUP_TRAITS;

   delete
     from sup_traits
    where rowid = L_rowid;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then      
      if L_exception_id = 1 then
         O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED', 
                                               'SUP_TRAITS_MATRIX', 
                                               to_char(I_sup_trait), 
                                               to_char(L_supplier));
      elsif L_exception_id = 2 then
         O_error_message := SQL_LIB.CREATE_MSG('REC_LOCK_SUP_TRT', to_char(I_sup_trait));
      end if;
      RETURN FALSE;

   when OTHERS then      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUP_TRAITS_SQL.DELETE_TRAIT',
                                            to_char(SQLCODE));
      RETURN FALSE;

END DELETE_TRAIT;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MASTER_SUP(O_error_message   IN OUT VARCHAR2,
                             O_exists          IN OUT BOOLEAN,
                             I_sup_trait       IN     SUP_TRAITS.SUP_TRAIT%TYPE)
                             RETURN BOOLEAN IS

   L_program     VARCHAR2(50)    := 'SUP_TRAITS_SQL.VALIDATE_MASTER_SUP';
   L_exists      VARCHAR2(255)   := 'N';

   cursor C_SUPP_EXISTS is
     select distinct 'Y'
       from sup_traits_matrix st
      where st.sup_trait = I_sup_trait
        and st.supplier in (select st2.supplier
                           from sup_traits_matrix st2,
                                sup_traits tr
                          where st2.sup_trait != I_sup_trait
                            and st2.sup_trait = tr.sup_trait
                            and tr.master_sup_ind = 'Y');
BEGIN
   if I_sup_trait is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_sup_trait',
                                             L_program,
                                             NULL);
      RETURN FALSE;
   end if;
   ---
   -- Check to see if any of the suppliers associated with the inputted trait 
   --  already exist for another master supplier trait 
   SQL_LIB.SET_MARK('OPEN', 'C_SUPP_EXISTS', 'sup_traits_matrix', 'SUP_TRAIT:  '||to_char(I_sup_trait));
   open C_SUPP_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_SUPP_EXISTS', 'sup_traits_matrix', 'SUP_TRAIT:  '||to_char(I_sup_trait));
   fetch C_SUPP_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_EXISTS', 'sup_traits_matrix', 'SUP_TRAIT:  '||to_char(I_sup_trait));
   close C_SUPP_EXISTS;

   if L_exists = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;

END VALIDATE_MASTER_SUP;
----------------------------------------------------------------------------------------------
END SUP_TRAITS_SQL;
/
