
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRRCLS_VALIDATE AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_pend_merch_hier_rec   OUT      PEND_MERCH_HIER%ROWTYPE,
                       I_message               IN       "RIB_XMrchHrRclsDesc_REC",
                       I_message_type          IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_pend_merch_hier_rec   OUT      PEND_MERCH_HIER%ROWTYPE,
                       I_message               IN       "RIB_XMrchHrRclsRef_REC")
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRRCLS_VALIDATE;
/
