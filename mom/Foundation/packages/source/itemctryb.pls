CREATE OR REPLACE PACKAGE BODY ITEM_COUNTRY_SQL AS
---------------------------------------------------------
FUNCTION CHECK_DUP_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists        OUT    BOOLEAN,
                                I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                                I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program              VARCHAR2(50) := 'ITEM_COUNTRY_SQL.CHECK_DUP_ITEM_COUNTRY';
   L_item_country_exists  VARCHAR2(1)  := NULL;
   
   cursor C_CHECK_ITEM_COUNTRY is
      select 'x'
        from item_country
       where country_id = I_country_id
         and item = I_item;

BEGIN

   if I_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_country_id',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   
   open C_CHECK_ITEM_COUNTRY;
   fetch C_CHECK_ITEM_COUNTRY into L_item_country_exists;
   if (C_CHECK_ITEM_COUNTRY%NOTFOUND) then
      O_exists := FALSE;
   else 
      O_exists := TRUE;
   end if;
   close C_CHECK_ITEM_COUNTRY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DUP_ITEM_COUNTRY;
---------------------------------------------------------
FUNCTION DELETE_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program            VARCHAR2(50) := 'ITEM_COUNTRY_SQL.DELETE_ITEM_COUNTRY';

   O_desc               ITEM_MASTER.ITEM_DESC%TYPE;
   O_status             ITEM_MASTER.STATUS%TYPE;
   O_item_level         ITEM_MASTER.ITEM_LEVEL%TYPE;
   O_tran_level         ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_exists             VARCHAR2(1)  := NULL;
   L_loc_exists         VARCHAR2(1)  := NULL;
   L_location           ITEM_LOC.LOC%TYPE;
   L_loc_type           ITEM_LOC.LOC_TYPE%TYPE;
   L_module             ADDR.MODULE%TYPE;
   L_key_value_1        ADDR.KEY_VALUE_1%TYPE;
   L_key_value_2        ADDR.KEY_VALUE_2%TYPE;
   L_add_1              ADDR.ADD_1%TYPE;
   L_add_2              ADDR.ADD_2%TYPE;
   L_add_3              ADDR.ADD_3%TYPE;
   L_city               ADDR.CITY%TYPE;
   L_state              ADDR.STATE%TYPE;
   L_country_id         ADDR.COUNTRY_ID%TYPE;
   L_post               ADDR.POST%TYPE;
   L_store_type         STORE.STORE_TYPE%TYPE;

   cursor C_GET_PRIM_DLVY_CTRY is
      select 'X'
        from item_cost_head
       where item                = I_item
         and delivery_country_id = I_country_id
         and prim_dlvy_ctry_ind  = 'Y';

   cursor C_GET_CTRY_LOC is
      select loc,loc_type
        from item_loc
       where item = I_item;

   cursor C_GET_STORE_TYPE(stores NUMBER) is
      select store_type
       from  store
      where  store = stores;

BEGIN
   if I_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_country_id',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_PRIM_DLVY_CTRY','ITEM_COST_HEAD','ITEM: '||I_item);
   OPEN C_GET_PRIM_DLVY_CTRY;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIM_DLVY_CTRY','ITEM_COST_HEAD','ITEM: '||I_item);
   FETCH C_GET_PRIM_DLVY_CTRY into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIM_DLVY_CTRY','ITEM_COST_HEAD','ITEM: '||I_item);
   CLOSE C_GET_PRIM_DLVY_CTRY;
   
    FOR item_loc_rec in C_GET_CTRY_LOC LOOP

        L_location := item_loc_rec.loc;
        L_loc_type := item_loc_rec.loc_type;

        if L_loc_type = 'W' then
           L_module := 'WH';
           L_key_value_1 := L_location;
        elsif L_loc_type = 'S' then        
           open C_GET_STORE_TYPE(L_location);
           fetch C_GET_STORE_TYPE into L_store_type;
           close C_GET_STORE_TYPE;

           if L_store_type in ('F') then
              L_module := 'WFST';
           elsif L_store_type = 'C' then
              L_module := 'ST';
           end if;

           L_key_value_1 := L_location;
        elsif L_loc_type = 'E' then
           L_module := 'PTNR';
           L_key_value_1 := L_loc_type;
           L_key_value_2 := L_location;
        end if;

        if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                     L_add_1,
                                     L_add_2,
                                     L_add_3,
                                     L_city,
                                     L_state,
                                     L_country_id,
                                     L_post,
                                     L_module,
                                     L_key_value_1,
                                     L_key_value_2) = FALSE then
           return FALSE;
        end if;

        if I_country_id =  L_country_id then
           L_loc_exists := 'X';
           EXIT;
        end if;
    END LOOP;

   if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                               O_desc,
                               O_status,
                               O_item_level,
                               O_tran_level,
                               I_item) = FALSE then
      return FALSE;
   end if;
   
   if L_exists is NOT NULL then
       O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DELETE_ITEM_CTRY');
       return FALSE;
   elsif L_loc_exists is NOT NULL then
       O_error_message := SQL_LIB.CREATE_MSG('CTRY_ITEM_LOC_CONST');
       return FALSE;
   else
      delete
        from item_country_l10n_ext
       where country_id = I_country_id
         and item = I_item;

      delete
        from item_cost_detail
       where delivery_country_id = I_country_id
         and item = I_item;

      delete
        from item_cost_head
       where delivery_country_id = I_country_id
         and item = I_item;

      delete
        from item_country
       where country_id = I_country_id
         and item = I_item;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ITEM_COUNTRY;
---------------------------------------------------------
FUNCTION CHECK_COUNTRY_ATTRIB_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists        OUT    BOOLEAN,
                                    I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                                    I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'ITEM_COUNTRY_SQL.CHECK_COUNTRY_ATTRIB_EXIST';
   L_icl10next_exists   VARCHAR2(1)  := NULL;

   cursor C_CHECK_ITEM_COUNTRY_L10N_EXT is
      select 'x'
        from item_country_l10n_ext
       where country_id = I_country_id
         and item = I_item;

BEGIN

   if I_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_country_id',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   open C_CHECK_ITEM_COUNTRY_L10N_EXT;
   fetch C_CHECK_ITEM_COUNTRY_L10N_EXT into L_icl10next_exists;
   if C_CHECK_ITEM_COUNTRY_L10N_EXT%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_ITEM_COUNTRY_L10N_EXT;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_COUNTRY_ATTRIB_EXIST;
---------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists        IN OUT BOOLEAN,
                                  I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program              VARCHAR2(50) := 'ITEM_COUNTRY_SQL.CHECK_ITEM_COUNTRY_EXIST';
   L_item_country_exists  VARCHAR2(1)  := NULL;

   cursor C_CHECK_ITEM_COUNTRY is
      select 'x'
        from item_country
       where item = I_item;

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   open C_CHECK_ITEM_COUNTRY;
   fetch C_CHECK_ITEM_COUNTRY into L_item_country_exists;

   if C_CHECK_ITEM_COUNTRY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_ITEM_COUNTRY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ITEM_COUNTRY_EXIST;
---------------------------------------------------------
FUNCTION CHECK_COUNTRY_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists        IN OUT BOOLEAN,
                              I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE)

RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'ITEM_COUNTRY_SQL.CHECK_COUNTRY_EXISTS';
   L_country_exists  VARCHAR2(1)  := NULL;

   cursor C_CHECK_COUNTRY is
      select 'x'
        from item_country
       where country_id = I_country_id;

BEGIN

   if I_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_country_id',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   open C_CHECK_COUNTRY;
   fetch C_CHECK_COUNTRY into L_country_exists;
   if C_CHECK_COUNTRY%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_COUNTRY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
            return FALSE;

END CHECK_COUNTRY_EXISTS;
---------------------------------------------------------
FUNCTION COPY_DOWN_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program              VARCHAR2(50) := 'ITEM_COUNTRY_SQL.COPY_DOWN_COUNTRY';

   L_children             ITEM_MASTER%ROWTYPE;
   L_child_item           ITEM_COUNTRY.ITEM%TYPE;
   L_parent_country       ITEM_COUNTRY.COUNTRY_ID%TYPE;
   L_child_ic_exists      VARCHAR2(1);

   O_desc                 ITEM_MASTER.ITEM_DESC%TYPE;
   O_status               ITEM_MASTER.STATUS%TYPE;
   O_item_level           ITEM_MASTER.ITEM_LEVEL%TYPE;
   O_tran_level           ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_country_attrib_rec   COUNTRY_ATTRIB%ROWTYPE;
   L_exists_ind           VARCHAR2(1)   := NULL;
   L_localized_ctry_added BOOLEAN       := FALSE;

   cursor C_GET_ITEM_CHILDREN is
   select *
     from item_master
    where (item_parent = I_item or
           item_grandparent = I_item)
      and item_level >= tran_level;

   cursor C_CHECK_ITEM_COUNTRY is
      select *
        from item_country
       where item = I_item;

   cursor C_GET_ITEM_COUNTRY is
      select 'x'
        from item_country
       where item = L_child_item
         and country_id = L_parent_country;

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   FOR item_country_rec IN C_CHECK_ITEM_COUNTRY LOOP 
       FOR item_child_rec IN C_GET_ITEM_CHILDREN LOOP
           if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                       O_desc,
                                       O_status,
                                       O_item_level,
                                       O_tran_level,
                                       item_child_rec.item) = FALSE then
              return FALSE;
           end if;

           L_child_item := item_child_rec.item;
           L_parent_country := item_country_rec.country_id;

           open C_GET_ITEM_COUNTRY;
           fetch C_GET_ITEM_COUNTRY into L_child_ic_exists;
           if C_GET_ITEM_COUNTRY%NOTFOUND then
           insert into item_country (item,
                                     country_id) 
                             values (item_child_rec.item,
                                    item_country_rec.country_id);
                                    
            if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                       L_country_attrib_rec,
                                                       NULL,
                                                       item_country_rec.country_id)= FALSE then 
               return FALSE;
            end if;
            
            if L_country_attrib_rec.localized_ind = 'Y' then
               L_localized_ctry_added := TRUE;
            end if;   
            
           end if;
           close C_GET_ITEM_COUNTRY;

      END LOOP;
      
      if L_localized_ctry_added then
         if L10N_SQL.CALL_EXEC_FUNC_FND(O_error_message,
                                        L_exists_ind,
                                        'COPY_DOWN_ATTRIB',
                                        item_country_rec.country_id,
                                        NULL,
                                        NULL,
                                        NULL,
                                        I_item)= FALSE then
            return FALSE;
         end if;                              
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END COPY_DOWN_COUNTRY;
---------------------------------------------------------
FUNCTION DEFAULT_LOC_REQ(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE)

RETURN BOOLEAN IS
   L_program              VARCHAR2(50) := 'ITEM_COUNTRY_SQL.DEFAULT_LOC_REQ';

   L_default_loc          COUNTRY_ATTRIB.DEFAULT_LOC%TYPE;
   L_exists               VARCHAR2(1)  := NULL;

   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;

   cursor C_DEFAULT_LOC is
      select default_loc
        from country_attrib
       where country_id = I_country_id;

BEGIN

   if I_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_country_id',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   open C_DEFAULT_LOC;
   fetch C_DEFAULT_LOC into L_default_loc;
   close C_DEFAULT_LOC;

   if L_default_loc is NOT NULL then
      L_exists := 'Y';
    else
      L_exists := 'N';
   end if;

   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;

   if L_system_options_rec.default_tax_type in ('SVAT','GTAX') and L_exists = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('DEFAULT_LOC_REQ');
      return FALSE;
   else 
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DEFAULT_LOC_REQ;
---------------------------------------------------------
FUNCTION CREATE_DEFAULT_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program              VARCHAR2(50) := 'ITEM_COUNTRY_SQL.CREATE_DEFAULT_ITEM_COUNTRY';
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;

   if DEFAULT_LOC_REQ(O_error_message,
                      L_system_options_rec.base_country_id) = FALSE then
      return FALSE;
   end if;

   if INSERT_ITEM_COUNTRY(O_error_message,
                          L_system_options_rec.base_country_id,
                          I_item) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_DEFAULT_ITEM_COUNTRY;
---------------------------------------------------------
FUNCTION INSERT_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program              VARCHAR2(50) := 'ITEM_COUNTRY_SQL.INSERT_ITEM_COUNTRY';
   O_exists               BOOLEAN;

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_country_id',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if CHECK_DUP_ITEM_COUNTRY(O_error_message,
                             O_exists,
                             I_country_id,
                             I_item) = FALSE then
      return FALSE;
   end if;

   if O_exists = FALSE then
      insert into item_country(item,
                               country_id)
                        values(I_item,
                               I_country_id);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ITEM_COUNTRY;
---------------------------------------------------------
FUNCTION GET_ITEM_COUNTRY_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_country_id    OUT    COUNTRY.COUNTRY_ID%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'ITEM_COUNTRY_SQL.GET_ITEM_COUNTRY_ID';

   cursor C_GET_COUNTRY_ID is
      select country_id
        from item_country
       where item = I_item
         and rownum = 1;

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_COUNTRY_ID',
                    'item_country',
                    'item: '||I_item);
   open C_GET_COUNTRY_ID;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_COUNTRY_ID',
                    'item_country',
                    'item: '||I_item);
   fetch C_GET_COUNTRY_ID into O_country_id;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_COUNTRY_ID',
                    'item_country',
                    'item: '||I_item);
   
   close C_GET_COUNTRY_ID;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
            return FALSE;

END GET_ITEM_COUNTRY_ID;
---------------------------------------------------------
FUNCTION CHECK_ALL_COUNTRY_ATTRIB_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_exists        OUT    BOOLEAN,
                                        I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'ITEM_COUNTRY_SQL.CHECK_ALL_COUNTRY_ATTRIB_EXIST';
   L_localized_ind      COUNTRY_ATTRIB.COUNTRY_ID%TYPE  := NULL;
   TYPE V_country_id IS TABLE OF ITEM_COUNTRY.COUNTRY_ID%TYPE;
   L_country_id    V_country_id;


   cursor C_CHECK_LOCALIZED is
      select ic.country_id
        from item_country ic,
             country_attrib ca
       where ic.country_id     = ca.country_id
         and ic.item           = I_item
         and ca.localized_ind = 'Y';

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   O_exists := TRUE;
   
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_LOCALIZED', 'COUNTRY_ATTRIB', I_item);
   open C_CHECK_LOCALIZED;

   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_LOCALIZED', 'COUNTRY_ATTRIB', I_item);
   fetch C_CHECK_LOCALIZED BULK COLLECT INTO L_country_id;

   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_LOCALIZED', 'COUNTRY_ATTRIB', I_item);
   close C_CHECK_LOCALIZED;

   FOR i IN 1..L_country_id.count LOOP
      if ITEM_COUNTRY_SQL.CHECK_COUNTRY_ATTRIB_EXIST(O_error_message,
                                                     O_exists,
                                                     L_country_id(i),
                                                     I_item) = FALSE then
         return FALSE;
      end if;
   
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ALL_COUNTRY_ATTRIB_EXIST;
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE)

RETURN BOOLEAN IS
   L_program              VARCHAR2(50) := 'ITEM_COUNTRY_SQL.VALIDATE_ITEM_COUNTRY';
   L_item_country         COUNTRY.COUNTRY_ID%TYPE;
   L_country_attrib_rec   COUNTRY_ATTRIB%ROWTYPE;  
   L_exists               BOOLEAN := TRUE;
   L_key_tbl              L10N_ENTITY_SQL.TBL_KEY_VAL_TBL;
   L_base_table           EXT_ENTITY.BASE_RMS_TABLE%TYPE := 'ITEM_COUNTRY';
   L_key_name_1           VARCHAR2(250) := 'ITEM';
   L_key_name_2           VARCHAR2(250) := 'COUNTRY_ID';
   
   cursor C_GET_ITEM_COUNTRY is
      select country_id
        from item_country
       where item = I_item;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   for c_rec in C_GET_ITEM_COUNTRY LOOP
     L_item_country := c_rec.country_id;
     
     if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                L_country_attrib_rec,
                                                NULL,
                                                L_item_country)= FALSE then
        return FALSE;                                            
     end if;
     
     if L_country_attrib_rec.localized_ind = 'Y' and I_pack_ind ='N' then
        if ITEM_COUNTRY_SQL.CHECK_COUNTRY_ATTRIB_EXIST(O_error_message,
                                                       L_exists,
                                                       L_item_country,
                                                       I_item) = FALSE then
           return FALSE;
        end if;                                               
     end if;
     
     if NOT L_exists then
        O_error_message := SQL_LIB.CREATE_MSG('NO_COUNTRY_ATTRIB_LOC',L_item_country);
        return FALSE;
     end if;
     
     if I_pack_ind ='N' then
        if L10N_ENTITY_SQL.BUILD_L10N_EXT_KEY(O_error_message,
                                              L_key_tbl,
                                              I_base_table=>L_base_table,
                                              I_key_name_1=>L_key_name_1,
                                              I_key_val_1 =>I_item,
                                              I_key_name_2=>L_key_name_2,
                                              I_key_val_2 =>L_item_country) = FALSE then
           return FALSE;
        end if;
     
        if L10N_ENTITY_SQL.CHECK_REQ_EXT(O_error_message,
                                         L_base_table,
                                         L_item_country,
                                         L_key_tbl) = FALSE then
            return FALSE;
         end if;
      end if;   
   end LOOP;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ITEM_COUNTRY;
---------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_DLVRY_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                   I_supplier      IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                   I_origin_cntry  IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                                   I_country_id    IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(50)                   := 'ITEM_COUNTRY_SQL.INSERT_ITEM_DLVRY_COUNTRY';
   L_item_dlvry_country_exists VARCHAR2(1)                    :=NULL  ;
   L_prim_dlvry_country        COUNTRY.COUNTRY_ID%TYPE        :=NULL  ;
   L_item_country              COUNTRY.COUNTRY_ID%TYPE        :=NULL;
   L_exist                     BOOLEAN;
   L_default_tax_type          SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

   cursor C_GET_DLVRY_COUNTRY is
      select distinct delivery_country_id
        from item_cost_head
       where item               = I_item
         and prim_dlvy_ctry_ind ='Y';
   
   cursor C_GET_PRIM_DLVRY_COUNTRY is
      select delivery_country_id
        from item_cost_head
       where item               = I_item
         and prim_dlvy_ctry_ind ='Y'
         and supplier           = I_supplier
         and origin_country_id  = I_origin_cntry;
        
   cursor C_GET_ITEM_COUNTRY is
      select 'X'
        from item_country
       where country_id         = I_country_id
         and item               = I_item;
           
BEGIN

   if NOT SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                                  L_default_tax_type) then
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   if I_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_country_id',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
  
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   
   if I_origin_cntry is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_origin_cntry',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   
   If L_default_tax_type = 'GTAX' then
	   
	   if ITEM_COST_SQL.CHECK_DUP_ITEM_COST(O_error_message,
											L_exist,
											I_item,
											I_supplier,
											I_origin_cntry,
											I_country_id)=FALSE then 
		  return FALSE;
	   end if;
			 
	   if L_exist then
		  return TRUE;
	   else
		  open  C_GET_PRIM_DLVRY_COUNTRY;
		  fetch C_GET_PRIM_DLVRY_COUNTRY into L_prim_dlvry_country;
		  close C_GET_PRIM_DLVRY_COUNTRY;
		  
		  open  C_GET_ITEM_COUNTRY;
		  fetch C_GET_ITEM_COUNTRY into L_item_country;
		  close C_GET_ITEM_COUNTRY;
			 
		  if L_prim_dlvry_country IS NOT NULL then
			 if L_item_country IS NULL then
				insert into item_country (item,
										  country_id)
								  values (I_item,
										  I_country_id);
			 end if;
			 insert into item_cost_head (item,
										 supplier,
										 origin_country_id,
										 delivery_country_id,
										 prim_dlvy_ctry_ind,
										 nic_static_ind,
										 base_cost,
										 negotiated_item_cost,
										 extended_base_cost,
										 inclusive_cost)
										(select item,
												supplier,
												origin_country_id,
												I_country_id,
												'N',
												nic_static_ind,
												base_cost,
												negotiated_item_cost,
												extended_base_cost,
												inclusive_cost
										   from item_cost_head
										  where item                = I_item
											and delivery_country_id = L_prim_dlvry_country
											and supplier            = I_supplier
											and origin_country_id   = I_origin_cntry);                                    
			 insert into item_cost_detail (item,
										   supplier,
										   origin_country_id,
										   delivery_country_id,
										   cond_type,
										   cond_value,
										   applied_on,
										   comp_rate,
										   calculation_basis,
										   recoverable_amount,
										   modified_taxable_base)
										  (select item,
										   supplier,
										   origin_country_id,
										   I_country_id,
										   cond_type,
										   cond_value,
										   applied_on,
										   comp_rate,
										   calculation_basis,
										   recoverable_amount,
										   modified_taxable_base
									  from item_cost_detail
									 where item                = I_item
									   and delivery_country_id = L_prim_dlvry_country
									   and supplier            = I_supplier
									   and origin_country_id   = I_origin_cntry);
			  
		  end if; -- L_prim_dlvry_country IS NOT NULL 
	   end if; -- L_exist = FALSE
	   
	   return TRUE;
   else -- For Tax Type SALES or SVAT
       return TRUE;
   end if;    
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ITEM_DLVRY_COUNTRY;
---------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_DLVRY_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                   I_supplier      IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                                   I_origin_cntry  IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(50) := 'ITEM_COUNTRY_SQL.INSERT_ITEM_DLVRY_COUNTRY';
   L_country_id                ADDR.COUNTRY_ID%TYPE;
   L_default_tax_type          SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;
   
   cursor C_GET_ITEM_LOC is
      select iscl.loc,
             iscl.loc_type,
             mva.country_id
        from item_supp_country_loc iscl,
             mv_loc_prim_addr mva
       where iscl.item              = I_item
         and iscl.supplier          = I_supplier
         and iscl.origin_country_id = I_origin_cntry
         and mva.loc                = iscl.loc;
       
BEGIN
   if NOT SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                                  L_default_tax_type) then
      return FALSE;
   end if;

   if I_item is NULL then
	 O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
										  'I_item',
										  L_program,
										  NULL);
	  return FALSE;
   end if;
  
   if I_supplier is NULL then
	  O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
										   'I_supplier',
										   L_program,
										   NULL);
	  return FALSE;
   end if;

   if I_origin_cntry is NULL then
	  O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
										   'I_origin_cntry',
										   L_program,
										   NULL);
	  return FALSE;
   end if;
   
   If L_default_tax_type = 'GTAX' then
	   FOR item_loc_rec in C_GET_ITEM_LOC LOOP
		  L_country_id := item_loc_rec.country_id;
		  if (nvl(GV_country_id,-1) !=  L_country_id) then 
			 if ITEM_COUNTRY_SQL.INSERT_ITEM_DLVRY_COUNTRY(O_error_message,
														   I_item,
														   I_supplier,
														   I_origin_cntry,
														   L_country_id) = FALSE then
				return FALSE;
			 end if;
			 GV_country_id := item_loc_rec.country_id;                       
		  end if;       
	   END LOOP;
		
	   return TRUE;
   else -- For Tax Type SALES or SVAT
       return TRUE;
   end if;  
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ITEM_DLVRY_COUNTRY;
---------------------------------------------------------

FUNCTION INSERT_ITEM_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_locs            IN       LOC_TBL)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'ITEM_COUNTRY_SQL.INSERT_ITEM_COUNTRY';
   
BEGIN
   if I_item is NULL then
     O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_item',
                                          L_program,
                                          NULL);
      return FALSE;
   end if;

   if I_locs is NULL or I_locs.COUNT <= 0 then
     O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_locs',
                                          L_program,
                                          NULL);
      return FALSE;
   end if;
   
   -- Add an item-country relationship for each unique delivery country
   FORALL i in I_locs.first .. I_locs.last
      insert into item_country (item,
                                country_id)
                        select I_item,
                               country_id
                          from mv_loc_prim_addr mva
                         where mva.loc = I_locs(i)
                           and not exists (select 'x'
                                             from item_country ic
                                            where ic.item = I_item
                                              and ic.country_id = mva.country_id
                                              and rownum = 1);
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
            return FALSE;
END INSERT_ITEM_COUNTRY;
---------------------------------------------------------------------------------------
FUNCTION CHECK_LOCALIZED_CTRY_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists          IN OUT   BOOLEAN,
                                     I_item            IN       ITEM_MASTER.ITEM%TYPE)




RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ITEM_COUNTRY_SQL.CHECK_LOCALIZED_CTRY_ATTRIB';

   cursor C_CHECK_LOCALIZED_CTRY is
      select ic.country_id,
             ca.localized_ind
        from item_country ic,
             country_attrib ca






       where ic.item = I_item
         and ic.country_id = ca.country_id;

   TYPE localized_ctry_TYPE is TABLE of C_CHECK_LOCALIZED_CTRY%ROWTYPE INDEX BY BINARY_INTEGER;
   L_localized_ctry_tbl   localized_ctry_TYPE;








BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_item',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   O_exists := FALSE;
   L_localized_ctry_tbl.DELETE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_LOCALIZED_CTRY',
                    'ITEM_COUNTRY, COUNTRY_ATTRIB',
                    'Item: ' || I_item);




   open C_CHECK_LOCALIZED_CTRY;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_LOCALIZED_CTRY',
                    'ITEM_COUNTRY, COUNTRY_ATTRIB',
                    'Item: ' || I_item);
   fetch C_CHECK_LOCALIZED_CTRY BULK COLLECT INTO L_localized_ctry_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_LOCALIZED_CTRY',
                    'ITEM_COUNTRY, COUNTRY_ATTRIB',
                    'Item: ' || I_item);

   close C_CHECK_LOCALIZED_CTRY;

   FOR i in 1..L_localized_ctry_tbl.COUNT LOOP
      if L_localized_ctry_tbl(i).localized_ind = 'Y' then


         if ITEM_COUNTRY_SQL.CHECK_COUNTRY_ATTRIB_EXIST(O_error_message,
                                                        O_exists,
                                                        L_localized_ctry_tbl(i).country_id,
                                                        I_item) = FALSE then


            return FALSE;
         end if;

         if O_exists = TRUE then
            exit;
         end if;

      else
         O_exists := FALSE;
      end if;

   END LOOP;

   return TRUE;


EXCEPTION
   when OTHERS then



      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));



      return FALSE;















END CHECK_LOCALIZED_CTRY_ATTRIB;
---------------------------------------------------------------------------------------

FUNCTION DELETE_CHILD_ITEM_COUNTRY(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_parent_country           IN     ITEM_COUNTRY.COUNTRY_ID%TYPE,
                                   I_item                     IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'ITEM_COUNTRY_SQL.DELETE_CHILD_ITEM_COUNTRY';
   L_exists          VARCHAR2(1);
   L_child_item      ITEM_MASTER.ITEM%TYPE;
  
   cursor C_GET_CHILDREN is 
      select item 
        from item_master 
       where (item_parent = I_item or item_grandparent = I_item)
         and item_level >= tran_level;
   
   cursor C_CHECK_ITEM_COUNTRY is
      select 'x' 
      from item_country 
         where country_id = I_parent_country
           and item = L_child_item;

BEGIN
   FOR child in C_GET_CHILDREN LOOP
      
      L_child_item := child.item;
      
      open C_CHECK_ITEM_COUNTRY;
      fetch C_CHECK_ITEM_COUNTRY into L_exists;
      close C_CHECK_ITEM_COUNTRY;
      
      if L_exists is NOT NULL then
         if ITEM_COUNTRY_SQL.DELETE_ITEM_COUNTRY(O_error_message,
                                                 I_parent_country,
                                                 L_child_item) = FALSE then
            return FALSE;
         end if;
      end if;
     
   END LOOP;

   return TRUE;
 
EXCEPTION
   when OTHERS then
      if C_CHECK_ITEM_COUNTRY%ISOPEN then
         close C_CHECK_ITEM_COUNTRY;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;     
END DELETE_CHILD_ITEM_COUNTRY;                   
--------------------------------------------------------------------------
FUNCTION CHECK_ITEM_LOC_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists        OUT    BOOLEAN,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE,
                               I_supplier      IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                               I_origin_cntry  IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE)                              
RETURN BOOLEAN IS                      
   
   L_PROGRAM      VARCHAR2(50) := 'ITEM_COUNTRY_SQL.CHECK_ITEM_LOC_EXISTS';
   L_loc_exists   VARCHAR2(1);
   L_cnt          NUMBER := 0;
   
BEGIN

   select count(*)
     into L_cnt 
     from item_supp_country_loc iscl
     where iscl.item                    = I_item
       and iscl.supplier                = I_supplier
       and iscl.origin_country_id       = I_origin_cntry;

   if L_cnt > 0 then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
  
   return TRUE; 

EXCEPTION  
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
    return FALSE;  
END CHECK_ITEM_LOC_EXISTS;
--------------------------------------------------------------------------------------
END ITEM_COUNTRY_SQL;
/   