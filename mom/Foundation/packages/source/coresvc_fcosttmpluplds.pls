CREATE OR REPLACE PACKAGE CORESVC_WF_COST_TMPL_UPLD_SQL AUTHID CURRENT_USER AS

GP_costtmplupld_fhead_row   SVC_WF_COST_TMPL_UPLD_FHEAD%ROWTYPE;
GP_costtmplupld_thead_row   SVC_WF_COST_TMPL_UPLD_THEAD%ROWTYPE;
GP_wf_cost_tmpl_head_row    WF_COST_BUILDUP_TMPL_HEAD%ROWTYPE;


-- Parameter tables
FHEAD   CONSTANT  VARCHAR2(30) := 'SVC_WF_COST_TMPL_UPLD_FHEAD';
THEAD   CONSTANT  VARCHAR2(30) := 'SVC_WF_COST_TMPL_UPLD_THEAD';
TDETL   CONSTANT  VARCHAR2(30) := 'SVC_WF_COST_TMPL_UPLD_TDETL';
TTAIL   CONSTANT  VARCHAR2(30) := 'SVC_WF_COST_TMPL_UPLD_TTAIL';
FTAIL   CONSTANT  VARCHAR2(30) := 'SVC_WF_COST_TMPL_UPLD_FTAIL';

-- Process/chunk status
NEW_RECORD   CONSTANT  VARCHAR2(1) := 'N';
ERROR        CONSTANT  VARCHAR2(1) := 'E';
REJECTED     CONSTANT  VARCHAR2(1) := 'R';
PROCESSED    CONSTANT  VARCHAR2(1) := 'P';

-- THEAD message types

TMPL_HEAD_ADD   CONSTANT  VARCHAR2(30) := 'costtmpadd';
TMPL_HEAD_MOD   CONSTANT  VARCHAR2(30) := 'costtmpmod';
TMPL_HEAD_DEL   CONSTANT  VARCHAR2(30) := 'costtmpdel';

-- TDETL message types

TMPL_RELN_ADD   CONSTANT  VARCHAR2(30) := 'costtmpreladd';
TMPL_RELN_MOD   CONSTANT  VARCHAR2(30) := 'costtmprelmod';
TMPL_RELN_DEL   CONSTANT  VARCHAR2(30) := 'costtmpreldel';

-------------------------------------------------------------------------------------------------
-- Function: INITIALIZE_PROCESS_STATUS
-- Description: Inserts the initial entry for the Cost Template upload process.
--              This is an asynchronous function.
-------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_PROCESS_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE,
                                   I_reference_id    IN     SVC_WF_COST_TMPL_UPLD_STATUS.REFERENCE_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: VALIDATE_PARAMETER_TABLES
-- Description: Validates information in the parameter tables after loading prior to main process
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PARAMETER_TABLES(O_error_message   IN OUT VARCHAR2,
                                   I_process_id      IN     SVC_WF_COST_TMPL_UPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------
-- Function: CHUNK_COST_TMPL_UPLD
-- Description: Groups the data in the SVC_WF_COST_TMPL_UPLD_THEAD into chunks
-------------------------------------------------------------------------------------------------
FUNCTION CHUNK_COST_TMPL_UPLD(O_error_message    IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id       IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: INITIALIZE_CHUNK_STATUS
-- Description: Inserts the status tracking entries after a successful chunking.
--              This is an asynchronous function.
-------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_CHUNK_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: PROCESS_COST_TMPL_UPLD
-- Description: Main process function for the cost template upload process
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COST_TMPL_UPLD(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                                I_chunk_id        IN     SVC_WF_COST_TMPL_UPLD_THEAD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: PROCESS_TRANSACTION
-- Description: Function to process one transaction
-------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TRANSACTION(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                             I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: WRITE_FILE_PROCESS_STATUS
-- Description: Function to update the status of records after processing one transaction
-------------------------------------------------------------------------------------------------
FUNCTION WRITE_FILE_PROCESS_STATUS(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: PROCESS_FUTURE_COST
-- Description: Function to invoke the FUTURE COST ENGINE.
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_FUTURE_COST(O_error_message   IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN     SVC_WF_COST_TMPL_UPLD_THEAD.PROCESS_ID%TYPE,
                             I_seq_no          IN     SVC_WF_COST_TMPL_UPLD_THEAD.SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: PURGE_RECORDS
-- Description: Function to purge the records in the staging table.
--------------------------------------------------------------------------------------------------
FUNCTION PURGE_RECORDS(O_error_message IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN  BOOLEAN;
-------------------------------------------------------------------------------------------------
END CORESVC_WF_COST_TMPL_UPLD_SQL;
/
