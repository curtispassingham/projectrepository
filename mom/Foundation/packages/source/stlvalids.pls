SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STORE_LIST_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------
-- Name: GROUPS
-- Description:  This functions calls the validation for
--               a location group type depending on the
--               passed in group ind
-- Called Functions: STORES
--                   STORE_CLASS
--                   DISTRICTS
--                   REGIONS
--                   PROMO
--                   TZONE
--                   VAL_LOC_TRAITS
--                   ZONE_GROUP
--  Created by:  Darcie Miller
--  Date:        12-AUG-96
---------------------------------------------------
   FUNCTION GROUPS(IO_message    IN OUT VARCHAR2,
                   I_group_ind   IN     VARCHAR2,
                   I_value       IN     VARCHAR2,
                   IO_value_desc IN OUT VARCHAR2,
                   IO_exist_ind  IN OUT BOOLEAN)
            return BOOLEAN;
-------------------------------------------------
-- Name: STORES
-- Description:  This functions validates the existence
--               of a passed in store
-- Called Functions: none
--  Created by:  Darcie Miller
--  Date:        12-AUG-96
---------------------------------------------------
   FUNCTION STORES(IO_message      IN OUT VARCHAR2,
                  I_store          IN     VARCHAR2,
                  IO_store_name    IN OUT VARCHAR2,
                  IO_exist_ind     IN OUT BOOLEAN)
            return BOOLEAN;
-------------------------------------------------
-------------------------------------------------
-- Name: AREA
-- Description:  This functions validates the existence
--               of a passed in store
-- Called Functions: none
--  Created by:  Medini Gaddikeri
--  Date:        12-AUG-96
---------------------------------------------------
   FUNCTION AREA(IO_message      IN OUT VARCHAR2,
                  I_area          IN     VARCHAR2,
                  IO_area_name    IN OUT VARCHAR2,
                  IO_exist_ind     IN OUT BOOLEAN)
            return BOOLEAN;
-------------------------------------------------

-- Name: STORE_CLASS
-- Description:  This functions validates the existence of
--               a passed in store class
-- Called Functions: none
--  Created by:  Darcie Miller
--  Date:        12-AUG-96
---------------------------------------------------
   FUNCTION STORE_CLASS(IO_message          IN OUT VARCHAR2,
                        I_store_class       IN     VARCHAR2,
                        IO_store_class_desc IN OUT VARCHAR2,
                        IO_exist_ind        IN OUT BOOLEAN)
            return BOOLEAN;
-------------------------------------------------
-- Name: DISTRICTS
-- Description:  This functions validates the existence of
--               a passed in district
-- Called Functions: none
--  Created by:  Darcie Miller
--  Date:        12-AUG-96
---------------------------------------------------

   FUNCTION DISTRICTS(IO_message      IN OUT VARCHAR2,
                     I_district      IN     VARCHAR2,
                     IO_description  IN OUT VARCHAR2,
                     IO_exist_ind    IN OUT BOOLEAN)
            return BOOLEAN;
-------------------------------------------------
-- Name: REGIONS
-- Description:  This functions validates the existence of
--               a passed in region
-- Called Functions: none
--  Created by:  Darcie Miller
--  Date:        12-AUG-96
---------------------------------------------------

   FUNCTION REGIONS(IO_message      IN OUT VARCHAR2,
                   I_region         IN     VARCHAR2,
                   IO_description   IN OUT VARCHAR2,
                   IO_exist_ind     IN OUT BOOLEAN)
            return BOOLEAN;


-------------------------------------------------
-- Name: TZONE
-- Description:  This functions validates the existence of
--               a passed in transfer zone
-- Called Functions: none
--  Created by:  Darcie Miller
--  Date:        12-AUG-96
---------------------------------------------------

   FUNCTION TZONE(IO_message       IN OUT VARCHAR2,
                  I_tzone          IN     VARCHAR2,
                  IO_description   IN OUT VARCHAR2,
                  IO_exist_ind     IN OUT BOOLEAN)
            return BOOLEAN;
-------------------------------------------------
-- Name: VAL_LOC_TRAITS
-- Description:  This functions validates the existence of
--               a passed in location trait
-- Called Functions: none
--  Created by:  Darcie Miller
--  Date:        12-AUG-96
---------------------------------------------------

   FUNCTION VAL_LOC_TRAITS(IO_message       IN OUT VARCHAR2,
                           I_trait          IN     VARCHAR2,
                           IO_description   IN OUT VARCHAR2,
                           IO_exist_ind     IN OUT BOOLEAN)
            return BOOLEAN;
-------------------------------------------------
FUNCTION LOCATIONS_EXIST(O_message     IN OUT VARCHAR2,
                         O_exists      IN OUT BOOLEAN,
                         I_group_ind   IN     VARCHAR2,
                         I_value       IN     NUMBER,
                         I_item        IN     ITEM_MASTER.ITEM%TYPE)
            return BOOLEAN;
END;
/


