
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE FUNCTION HASH_ITEM(IO_error_message       IN OUT VARCHAR2,
                                     IO_item_hash_code      IN OUT NUMBER,
                                     I_item                 IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN AUTHID CURRENT_USER IS
BEGIN
   IO_item_hash_code := 0;

   for i in 1..LENGTH(I_item) loop
      IO_item_hash_code := IO_item_hash_code + ASCII(SUBSTR(I_item, i, 1));
   end loop;

   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                  	         'HASH_ITEM',
                                  	          to_char(SQLCODE));
      return FALSE;
END;
/
