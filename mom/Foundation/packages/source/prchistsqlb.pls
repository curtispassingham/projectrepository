CREATE OR REPLACE PACKAGE BODY PRICE_HIST_SQL AS
--------------------------------------------------------------------------------
FUNCTION BULK_INSERT_PRICE_HIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_new_price_attr    IN       NEW_PRICE_REC,
                                I_record_type       IN       VARCHAR2,
                                I_reason            IN       PRICE_HIST.REASON%TYPE,
                                I_loc_type          IN       PRICE_HIST.LOC_TYPE%TYPE)

   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'PRICE_HIST_SQL.BULK_INSERT_PRICE_HIST';
   L_table           VARCHAR2(20) := 'PRICE_HIST';
   L_action_date     DATE         := GET_VDATE + 1;

   L_item            PRICE_HIST.ITEM%TYPE := NULL;
   L_loc             PRICE_HIST.LOC%TYPE := NULL;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_locked, -54);

   cursor C_LOCK_PRICE_HIST is
      select ph.item,
             ph.loc
        from price_hist ph,
             api_price_hist_temp aph
       where ph.item = aph.item
         and ph.loc = aph.loc
         and ph.tran_type = I_new_price_attr.tran_type
         and ph.action_date = L_action_date
         for update of ph.item nowait;
         
   cursor C_LOCK_PRICE_HIST_SEQ is
      select ph.item,
             ph.loc
        from price_hist ph,
             api_price_hist aph
       where ph.item = aph.item
         and ph.loc = aph.loc
         and aph.seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no
         and ph.tran_type = I_new_price_attr.tran_type
         and ph.action_date = L_action_date
         for update of ph.item nowait;

BEGIN
   if RMSSUB_XCOSTCHG.LP_api_seq_no is NOT NULL then

      open C_LOCK_PRICE_HIST_SEQ;
      fetch C_LOCK_PRICE_HIST_SEQ into L_item,
                                       L_loc;
      close C_LOCK_PRICE_HIST_SEQ;

      if I_record_type = 'C' then
         MERGE INTO price_hist ph
            USING (select *
                     from  api_price_hist
                    where  seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no) aph
            ON    (aph.item = ph.item AND
                   aph.loc = ph.loc AND
                   ph.action_date = L_action_date AND
                   ph.tran_type = I_new_price_attr.tran_type)
            WHEN MATCHED THEN
            update
               set ph.reason              = I_reason,
                   ph.loc_type            = aph.loc_type,
                   ph.unit_cost           = aph.unit_cost,
                   ph.unit_retail         = aph.unit_retail,
                   ph.selling_unit_retail = aph.unit_retail
            WHEN NOT MATCHED THEN
            insert (tran_type,
                    reason,
                    item,
                    loc,
                    loc_type,
                    unit_cost,
                    unit_retail,
                    action_date)
            values (I_new_price_attr.tran_type,
                    I_reason,
                    aph.item,
                    aph.loc,
                    aph.loc_type,
                    aph.unit_cost,
                    aph.unit_retail,
                    L_action_date);
      elsif I_record_type = 'R' then
         MERGE INTO price_hist ph
            USING (select *
                     from  api_price_hist
                    where  seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no) aph
            ON    (aph.item = ph.item AND
                   aph.loc = ph.loc AND
                   ph.action_date = L_action_date AND
                   ph.tran_type = I_new_price_attr.tran_type)
            WHEN MATCHED THEN
            update
               set ph.reason              = I_reason,
                   ph.unit_cost           = aph.unit_cost,
                   ph.unit_retail         = I_new_price_attr.standard_unit_retail,
                   ph.selling_unit_retail = I_new_price_attr.selling_unit_retail,
                   ph.selling_uom         = I_new_price_attr.selling_uom,
                   ph.multi_units         = I_new_price_attr.multi_units,
                   ph.multi_unit_retail   = I_new_price_attr.multi_unit_retail,
                   ph.multi_selling_uom   = I_new_price_attr.multi_selling_uom,
                   ph.post_date           = L_action_date
            WHEN NOT MATCHED THEN
            insert (tran_type,
                    reason,
                    item,
                    loc,
                    loc_type,
                    unit_cost,
                    unit_retail,
                    selling_unit_retail,
                    selling_uom,
                    action_date,
                    multi_units,
                    multi_unit_retail,
                    multi_selling_uom,
                    post_date)
            values (I_new_price_attr.tran_type,
                    I_reason,
                    aph.item,
                    aph.loc,
                    aph.loc_type,
                    aph.unit_cost,
                    I_new_price_attr.standard_unit_retail,
                    I_new_price_attr.selling_unit_retail,
                    I_new_price_attr.selling_uom,
                    L_action_date,
                    I_new_price_attr.multi_units,
                    I_new_price_attr.multi_unit_retail,
                    I_new_price_attr.multi_selling_uom,
                    NULL);
      end if;
   else
      ---
      open C_LOCK_PRICE_HIST;
      fetch C_LOCK_PRICE_HIST into L_item,
                                   L_loc;
      close C_LOCK_PRICE_HIST;
      ---

      if I_record_type = 'C' then
         MERGE INTO price_hist ph
            USING (select *
                     from  api_price_hist_temp) aph
            ON    (aph.item = ph.item AND
                   aph.loc = ph.loc AND
                   ph.action_date = L_action_date AND
                   ph.tran_type = I_new_price_attr.tran_type)
            WHEN MATCHED THEN
            update
               set ph.reason              = I_reason,
                   ph.loc_type            = aph.loc_type,
                   ph.unit_cost           = aph.unit_cost,
                   ph.unit_retail         = aph.unit_retail,
                   ph.selling_unit_retail = aph.unit_retail
            WHEN NOT MATCHED THEN
            insert (tran_type,
                    reason,
                    item,
                    loc,
                    loc_type,
                    unit_cost,
                    unit_retail,
                    action_date)
            values (I_new_price_attr.tran_type,
                    I_reason,
                    aph.item,
                    aph.loc,
                    aph.loc_type,
                    aph.unit_cost,
                    aph.unit_retail,
                    L_action_date);
      elsif I_record_type = 'R' then
         MERGE INTO price_hist ph
            USING (select *
                     from  api_price_hist_temp) aph
            ON    (aph.item = ph.item AND
                   aph.loc = ph.loc AND
                   ph.action_date = L_action_date AND
                   ph.tran_type = I_new_price_attr.tran_type)
            WHEN MATCHED THEN
            update
               set ph.reason              = I_reason,
                   ph.unit_cost           = aph.unit_cost,
                   ph.unit_retail         = I_new_price_attr.standard_unit_retail,
                   ph.selling_unit_retail = I_new_price_attr.selling_unit_retail,
                   ph.selling_uom         = I_new_price_attr.selling_uom,
                   ph.multi_units         = I_new_price_attr.multi_units,
                   ph.multi_unit_retail   = I_new_price_attr.multi_unit_retail,
                   ph.multi_selling_uom   = I_new_price_attr.multi_selling_uom,
                   ph.post_date           = L_action_date
            WHEN NOT MATCHED THEN
            insert (tran_type,
                    reason,
                    item,
                    loc,
                    loc_type,
                    unit_cost,
                    unit_retail,
                    selling_unit_retail,
                    selling_uom,
                    action_date,
                    multi_units,
                    multi_unit_retail,
                    multi_selling_uom,
                    post_date)
            values (I_new_price_attr.tran_type,
                    I_reason,
                    aph.item,
                    aph.loc,
                    aph.loc_type,
                    aph.unit_cost,
                    I_new_price_attr.standard_unit_retail,
                    I_new_price_attr.selling_unit_retail,
                    I_new_price_attr.selling_uom,
                    L_action_date,
                    I_new_price_attr.multi_units,
                    I_new_price_attr.multi_unit_retail,
                    I_new_price_attr.multi_selling_uom,
                    NULL);
      end if;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'item '|| L_item,
                                            'loc ' || L_loc);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END BULK_INSERT_PRICE_HIST;

--------------------------------------------------------------------------------
-- Function name: INSERT_PRICE_HIST
--
--------------------------------------------------------------------------------
FUNCTION INSERT_PRICE_HIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item_rec        IN     RMSSUB_XITEM.ITEM_API_REC,
                           I_unit_cost       IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)

   RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'PRICE_HIST_SQL.INSERT_PRICE_HIST';
BEGIN

   insert into price_hist(tran_type,
                          reason,
                          event,
                          item,
                          loc,
                          loc_type,
                          unit_cost,
                          unit_retail,
                          selling_unit_retail,
                          selling_uom,
                          action_date,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom)
                  values (0, --tran_type
                          0, --reason
                          NULL, --event
                          I_item_rec.item_master_def.item,
                          0, --loc
                          NULL, --loc_type
                          I_unit_cost,
                          I_item_rec.izp_tbl(1).unit_retail,
                          I_item_rec.izp_tbl(1).selling_unit_retail,
                          I_item_rec.izp_tbl(1).selling_uom,
                          I_item_rec.item_master_def.create_datetime, --action_date
                          NULL, --multi_units
                          NULL, --multi_unit_retail
                          NULL  --multi_selling_uom
                          );
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_PRICE_HIST;
--------------------------------------------------------------------------------
END PRICE_HIST_SQL;
/
