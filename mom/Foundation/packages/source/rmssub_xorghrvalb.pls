
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XORGHR_VALIDATE AS
-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQUIRED_FIELDS
-- Purpose      : This function will check all required fields for the create and modify messages.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message           IN       "RIB_XOrgHrDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQUIRED_FIELDS
-- Purpose      : This function will check all required fields for the delete messages.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message           IN       "RIB_XOrgHrRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_LEVEL
-- Purpose      : This function will check the message or the value of hier_level.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LEVEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message           IN       "RIB_XOrgHrDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_LEVEL
-- Purpose      : This function will check the message or the value of hier_level.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LEVEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message           IN       "RIB_XOrgHrRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: ORGHIER_EXISTS
-- Purpose      : This function will check existence by calling
--                ORGANIZATION_VALIDATE_SQL.EXIST.
-------------------------------------------------------------------------------------------------------
FUNCTION ORGHIER_EXISTS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists            OUT      BOOLEAN,
                        I_hier_level        IN       VARCHAR2,
                        I_hier_value        IN       NUMBER,
                        I_message_type      IN       VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_CURRENT_PARENT
-- Purpose      : Gets the current parent id of the area, region, or district
--                by calling ORGANIZATION_ATTRIB_SQL.GET_PARENT
-------------------------------------------------------------------------------------------------------
FUNCTION GET_CURRENT_PARENT(O_error_message  IN OUT VARCHAR2,
                            O_parent_id      OUT    NUMBER,
                            I_hier_level     IN     VARCHAR2,
                            I_hier_value     IN     NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_LOC_TRAITS
-- Purpose      : This function will populate LOC_TRAITS_TBL
--                the values from the create or modify RIB messages.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_LOC_TRAITS(O_error_message  IN OUT         VARCHAR2,
                             O_loc_traits_tbl OUT    NOCOPY  ORGANIZATION_SQL.LOC_TRAIT_TBL,
                             I_loctrt_TBL     IN             "RIB_XOrgHrLocTrt_TBL")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_RECORD
-- Purpose      : This function will populate all the fields in the ORGANIZATION_SQL.ORG_HIER_REC with
--                the values from the create or modify RIB messages.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                         O_org_hier_rec    OUT    NOCOPY ORGANIZATION_SQL.ORG_HIER_REC,
                         I_loc_traits_tbl  IN            ORGANIZATION_SQL.LOC_TRAIT_TBL,
                         I_message         IN            "RIB_XOrgHrDesc_REC")
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_RECORD
-- Purpose      : This function will populate all the fields in the ORGANIZATION_SQL.ORG_HIER_REC with
--                the values from the delete RIB message.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_org_hier_rec    OUT     ORGANIZATION_SQL.ORG_HIER_REC,
                         I_loc_traits_tbl  IN      ORGANIZATION_SQL.LOC_TRAIT_TBL,
                         I_message         IN      "RIB_XOrgHrRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_LOC_TRAITS
-- Purpose      : This function will check if there are any associated loc traits within a CHAIN
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_TRAITS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN    "RIB_XOrgHrDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_LOC_TRAITS
-- Purpose      : This function will check if there are any associated loc traits within a CHAIN
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_TRAITS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN    "RIB_XOrgHrRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                       O_org_hier_rec  OUT    NOCOPY ORGANIZATION_SQL.ORG_HIER_REC,
                       I_message       IN            "RIB_XOrgHrDesc_REC",
                       I_message_type  IN            VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)  := 'RMSSUB_XORGHR_VALIDATE.CHECK_MESSAGE';
   L_exists          BOOLEAN       := FALSE;
   L_parent_id       NUMBER;
   L_loc_traits_tbl  ORGANIZATION_SQL.LOC_TRAIT_TBL;

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_LEVEL(O_error_message,
                      I_message) then
      return FALSE;
   end if;

   if not CHECK_LOC_TRAITS(O_error_message,
                           I_message) then
      return FALSE;
   end if;

   if LOWER(I_message_type) = RMSSUB_XORGHR.LP_mod_type then
      if not ORGHIER_EXISTS(O_error_message,
                            L_exists,
                            I_message.hier_level,
                            I_message.hier_value) then
         return FALSE;
      end if;
      if not L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE',
                                               'hier_value',
                                               I_message.hier_value,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if LOWER(I_message_type) = RMSSUB_XORGHR.LP_mod_type and
      I_message.hier_level in (ORGANIZATION_SQL.LP_region,
                               ORGANIZATION_SQL.LP_district) then
      if not GET_CURRENT_PARENT(O_error_message,
                                LP_old_parent_id,
                                I_message.hier_level,
                                I_message.hier_value) then
         return FALSE;
      end if;
   end if;

   if not POPULATE_LOC_TRAITS(O_error_message,
                              L_loc_traits_tbl,
                              I_message.xorghrloctrt_tbl) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_org_hier_rec,
                          L_loc_traits_tbl,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                       O_org_hier_rec  OUT   NOCOPY  ORGANIZATION_SQL.ORG_HIER_REC,
                       I_message       IN            "RIB_XOrgHrRef_REC",
                       I_message_type  IN            VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.CHECK_MESSAGE';
   L_exists           BOOLEAN      := FALSE;
   L_loc_traits_tbl   ORGANIZATION_SQL.LOC_TRAIT_TBL;

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_LEVEL(O_error_message,
                      I_message) then
      return FALSE;
   end if;

   if not CHECK_LOC_TRAITS(O_error_message,
                           I_message) then
      return FALSE;
   end if;

   if not ORGHIER_EXISTS(O_error_message,
                         L_exists,
                         I_message.hier_level,
                         I_message.hier_value,
                         I_message_type) then
      return FALSE;
   end if;

   if I_message_type IS NOT NULL AND LOWER(I_message_type)= RMSSUB_XORGHR.LP_del_type  then
      if not L_exists AND O_error_message = 'INV_FIELD_VALUE'  then

         O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE',
                                               'hier_value',
                                                I_message.hier_value,
                                                NULL);
        return FALSE;

      end if;
   elsif not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE',
                                            'hier_value',
                                            I_message.hier_value,
                                            NULL);
      return FALSE;
   end if;

   if L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('REG_REC_EXISTS');
      return FALSE;
   end if;

   if not POPULATE_LOC_TRAITS(O_error_message,
                              L_loc_traits_tbl,
                              I_message.xorghrloctrt_tbl) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_org_hier_rec,
                          L_loc_traits_tbl,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;


-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message           IN       "RIB_XOrgHrDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.hier_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Org level');
      return FALSE;
   end if;
   ---
   if I_message.hier_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Org value');
      return FALSE;
   end if;
   ---
   if I_message.hier_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Org description');
      return FALSE;
   end if;
   ---
   if I_message.hier_level != ORGANIZATION_SQL.LP_chain and
      I_message.parent_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL','Org parent id');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message           IN       "RIB_XOrgHrRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.hier_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', 'No Org level specified ', NULL, NULL);
      return FALSE;
   end if;
   ---
   if I_message.hier_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', 'No Org value specified', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                         O_org_hier_rec    OUT    NOCOPY ORGANIZATION_SQL.ORG_HIER_REC,
                         I_loc_traits_tbl  IN            ORGANIZATION_SQL.LOC_TRAIT_TBL,
                         I_message         IN            "RIB_XOrgHrDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.POPULATE_RECORD';

BEGIN

   O_org_hier_rec.hier_value    := I_message.hier_value;
   O_org_hier_rec.hier_level    := I_message.hier_level;
   O_org_hier_rec.hier_desc     := I_message.hier_desc;
   O_org_hier_rec.mgr_name      := I_message.mgr_name;
   O_org_hier_rec.currency_code := I_message.currency_code;
   O_org_hier_rec.parent_id     := I_message.parent_id;
   O_org_hier_rec.old_parent_id := LP_old_parent_id;

   if I_loc_traits_tbl is NOT NULL and I_loc_traits_tbl.count > 0 then
      O_org_hier_rec.loc_trait_ids := I_loc_traits_tbl;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                         O_org_hier_rec    OUT    NOCOPY ORGANIZATION_SQL.ORG_HIER_REC,
                         I_loc_traits_tbl  IN            ORGANIZATION_SQL.LOC_TRAIT_TBL,
                         I_message         IN            "RIB_XOrgHrRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.POPULATE_RECORD';

BEGIN

   O_org_hier_rec.hier_value   := I_message.hier_value;
   O_org_hier_rec.hier_level   := I_message.hier_level;

   if I_loc_traits_tbl is not NULL then
      O_org_hier_rec.loc_trait_ids := I_loc_traits_tbl;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LEVEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message           IN       "RIB_XOrgHrDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.CHECK_LEVEL';

BEGIN
   if I_message.hier_level not in (ORGANIZATION_SQL.LP_chain,
                                   ORGANIZATION_SQL.LP_area,
                                   ORGANIZATION_SQL.LP_region,
                                   ORGANIZATION_SQL.LP_district) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_LEVEL');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_LEVEL;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LEVEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message           IN       "RIB_XOrgHrRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.CHECK_LEVEL';

BEGIN
   if I_message.hier_level not in (ORGANIZATION_SQL.LP_chain,
                                   ORGANIZATION_SQL.LP_area,
                                   ORGANIZATION_SQL.LP_region,
                                   ORGANIZATION_SQL.LP_district) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_LEVEL');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_LEVEL;
-------------------------------------------------------------------------------------------------------
FUNCTION ORGHIER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists          OUT      BOOLEAN,
                        I_hier_level      IN       VARCHAR2,
                        I_hier_value      IN       NUMBER,
                        I_message_type    IN       VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.ORGHIER_EXISTS';
   L_type         NUMBER;

BEGIN

   if I_hier_level = ORGANIZATION_SQL.LP_chain then
      L_type := 10;
   elsif I_hier_level = ORGANIZATION_SQL.LP_area then
      L_type := 20;
   elsif I_hier_level = ORGANIZATION_SQL.LP_region then
      L_type := 30;
   elsif I_hier_level = ORGANIZATION_SQL.LP_district then
      L_type := 40;
   end if;

   if I_message_type IS NOT NULL AND LOWER(I_message_type)= RMSSUB_XORGHR.LP_del_type  then

      if not ORGANIZATION_VALIDATE_SQL.EXIST(O_error_message,
                                            L_type,
                                            I_hier_value,
                                            O_exists,
                                            I_message_type) then
        return FALSE;
      end if;
   else

      if not ORGANIZATION_VALIDATE_SQL.EXIST(O_error_message,
                                            L_type,
                                            I_hier_value,
                                            O_exists) then
        return FALSE;
     end if;
  end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ORGHIER_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_CURRENT_PARENT(O_error_message  IN OUT VARCHAR2,
                            O_parent_id      OUT    NUMBER,
                            I_hier_level     IN     VARCHAR2,
                            I_hier_value     IN     NUMBER)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.GET_CURRENT_PARENT';

BEGIN

   if not ORGANIZATION_ATTRIB_SQL.GET_PARENT(O_error_message,
                                             O_parent_id,
                                             I_hier_level,
                                             I_hier_value) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_CURRENT_PARENT;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_LOC_TRAITS(O_error_message  IN OUT         VARCHAR2,
                             O_loc_traits_tbl OUT    NOCOPY  ORGANIZATION_SQL.LOC_TRAIT_TBL,
                             I_loctrt_TBL     IN             "RIB_XOrgHrLocTrt_TBL")
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.POPULATE_LOC_TRAITS';

BEGIN

   O_loc_traits_tbl := ORGANIZATION_SQL.LOC_TRAIT_TBL();
   if I_loctrt_TBL is not NULL and
      I_loctrt_TBL.count > 0 then
      FOR i in I_loctrt_TBL.first..I_loctrt_TBL.last LOOP
         O_loc_traits_tbl.EXTEND();
         O_loc_traits_tbl(i).loc_trait_id := I_loctrt_TBL(i).hier_trait_id;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_LOC_TRAITS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_TRAITS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN     "RIB_XOrgHrDesc_REC")
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.CHECK_LOC_TRAITS';

BEGIN

   if I_message.hier_level = ORGANIZATION_SQL.LP_chain and
      (I_message.xorghrloctrt_tbl is NOT NULL AND
       I_message.xorghrloctrt_tbl.count > 0 ) then
         O_error_message := SQL_LIB.CREATE_MSG('LOC_TRAITS_CHAIN', NULL, NULL, NULL);
         return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_LOC_TRAITS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_TRAITS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN     "RIB_XOrgHrRef_REC")
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XORGHR_VALIDATE.CHECK_LOC_TRAITS';

BEGIN

   if I_message.hier_level = ORGANIZATION_SQL.LP_chain and
      (I_message.xorghrloctrt_tbl is NOT NULL AND
       I_message.xorghrloctrt_tbl.count > 0 ) then
         O_error_message := SQL_LIB.CREATE_MSG('LOC_TRAITS_CHAIN', NULL, NULL, NULL);
         return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_LOC_TRAITS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XORGHR_VALIDATE;
/
