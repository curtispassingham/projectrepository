
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ITEMLIST_ATTRIB_SQL AS

-----------------------------------------------------------------------------------
FUNCTION GET_NAME (O_error_message   IN OUT   VARCHAR2,
                   I_itemlist        IN       NUMBER,
                   O_list_name       IN OUT   VARCHAR2)

   RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'ITEMLIST_ATTRIB_SQL.GET_NAME';
   ---
   cursor C_SKULIST is
      select skulist_desc
        from v_skulist_head_tl
       where skulist = I_itemlist;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_SKULIST',
                    'V_SKULIST_HEAD_TL',
                    to_char(I_itemlist));
   open C_SKULIST;
   SQL_LIB.SET_MARK('FETCH',
                    'C_SKULIST',
                    'V_SKULIST_HEAD_TL',
                    to_char(I_itemlist));
   fetch C_SKULIST into O_list_name;
   if C_SKULIST%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LIST',
                                            NULL,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SKULIST',
                       'V_SKULIST_HEAD_TL',
                       to_char(I_itemlist));
      close C_SKULIST;
      return FALSE;
   end if;
   close C_SKULIST;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NAME;
--------------------------------------------------------------------
FUNCTION GET_ITEM_COUNT(O_error_message    IN OUT    VARCHAR2,
                        I_itemlist         IN        SKULIST_HEAD.SKULIST%TYPE,
                        O_count            IN OUT    NUMBER)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'ITEMLIST_ATTRIB_SQL.GET_ITEM_COUNT';
   ---
   cursor C_COUNT is
      select count(*)
        from skulist_detail
       where skulist = I_itemlist;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT',
                    'SKULIST_DETAIL',
                    to_char(I_itemlist));
   open C_COUNT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT',
                    'SKULIST_DETAIL',
                    to_char(I_itemlist));
   fetch C_COUNT into O_count;
   if C_COUNT%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_COUNT',
                       'SKULIST_DETAIL',
                       to_char(I_itemlist));
      close C_COUNT;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SKU_LIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_COUNT',
                       'SKULIST_DETAIL',
                       to_char(I_itemlist));
      close C_COUNT;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEM_COUNT;
---------------------------------------------------------------------
FUNCTION GET_DEPOSIT_ITEM_COUNT(O_error_message   IN OUT   VARCHAR2,
                                I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE,
                                I_deposit_type    IN       ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE,
                                O_count           IN OUT   NUMBER)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'ITEMLIST_ATTRIB_SQL.GET_DEPOSIT_ITEM_COUNT';
   ---
   cursor C_COUNT is
      select count( im.item )
        from item_master    im,
             skulist_detail sd
       where im.deposit_item_type = I_deposit_type
         and im.item              = sd.item
         and sd.skulist           = I_itemlist;

BEGIN
   O_count := 0;

   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT',
                    'SKULIST_DETAIL, ITEM_MASTER',
                    'skulist = ' || to_char(I_itemlist) || ', deposit_item_type = ' || I_deposit_type );
   open C_COUNT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT',
                    'SKULIST_DETAIL, ITEM_MASTER',
                    'skulist = ' || to_char(I_itemlist) || ', deposit_item_type = ' || I_deposit_type );
   fetch C_COUNT into O_count;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT',
                    'SKULIST_DETAIL, ITEM_MASTER',
                    'skulist = ' || to_char(I_itemlist) || ', deposit_item_type = ' || I_deposit_type );
   close C_COUNT;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEPOSIT_ITEM_COUNT;
---------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_skulist_desc            IN OUT   SKULIST_HEAD.SKULIST_DESC%TYPE,
                         O_create_date             IN OUT   SKULIST_HEAD.CREATE_DATE%TYPE,
                         O_last_rebuild_date       IN OUT   SKULIST_HEAD.LAST_REBUILD_DATE%TYPE,
                         O_create_id               IN OUT   SKULIST_HEAD.CREATE_ID%TYPE,
                         O_static_ind              IN OUT   SKULIST_HEAD.STATIC_IND%TYPE,
                         O_comment_desc            IN OUT   SKULIST_HEAD.COMMENT_DESC%TYPE,
                         O_filter_org_id           IN OUT   SKULIST_HEAD.FILTER_ORG_ID%TYPE,
                         I_itemlist                IN       SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'ITEMLIST_ATTRIB_SQL.GET_HEADER_INFO';

   ---
   cursor C_HEADER is
      select stl.skulist_desc,
             s.create_date,
             s.last_rebuild_date,
             s.create_id,
             s.static_ind,
             s.comment_desc,
             s.filter_org_id
        from skulist_head s,
             v_skulist_head_tl stl
       where s.skulist = I_itemlist
         and stl.skulist=s.skulist;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_HEADER',
                    'SKULIST_HEAD',
                    to_char(I_itemlist));
   open C_HEADER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_HEADER',
                    'SKULIST_HEAD',
                    to_char(I_itemlist));
   fetch C_HEADER into O_skulist_desc,
                       O_create_date,
                       O_last_rebuild_date,
                       O_create_id,
                       O_static_ind,
                       O_comment_desc,
                       O_filter_org_id;
   if C_HEADER%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_HEADER',
                       'SKULIST_HEAD',
                       to_char(I_itemlist));
      close C_HEADER;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SKU_LIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_HEADER',
                    'SKULIST_HEAD',
                    to_char(I_itemlist));
   close C_HEADER;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_HEADER_INFO;
--------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_skulist_desc            IN OUT   SKULIST_HEAD.SKULIST_DESC%TYPE,
                         O_create_date             IN OUT   SKULIST_HEAD.CREATE_DATE%TYPE,
                         O_last_rebuild_date       IN OUT   SKULIST_HEAD.LAST_REBUILD_DATE%TYPE,
                         O_create_id               IN OUT   SKULIST_HEAD.CREATE_ID%TYPE,
                         O_static_ind              IN OUT   SKULIST_HEAD.STATIC_IND%TYPE,
                         O_comment_desc            IN OUT   SKULIST_HEAD.COMMENT_DESC%TYPE,
                         I_itemlist                IN       SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN IS

   L_dummy_filter_id   SKULIST_HEAD.FILTER_ORG_ID%TYPE; -- arbitrary typecast used

BEGIN

   if not GET_HEADER_INFO(O_error_message,
                         O_skulist_desc,
                         O_create_date,
                         O_last_rebuild_date,
                         O_create_id,
                         O_static_ind,
                         O_comment_desc,
                         L_dummy_filter_id,
                         I_itemlist) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ITEMLIST_ATRIB_SQL.GET_HEADER_INFO',
                                             to_char(SQLCODE));
      return FALSE;
END GET_HEADER_INFO;
----------------------------------------------------------------------------------
FUNCTION ITEM_ON_CONSIGNMENT_EXISTS(O_error_message   IN OUT   VARCHAR2,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)    := 'ITEMLIST_ATTRIB_SQL.ITEM_ON_CONSIGNMENT_EXISTS';
   L_table      VARCHAR2(64)    := 'SKULIST_DETAIL, ITEM_SUPPLIER';
   L_exists     VARCHAR2(1);
   ---
   cursor C_EXISTS is
      select /*+ INDEX(s) */
             'Y'
        from skulist_detail l, item_supplier s
       where s.item = l.item
         and l.skulist = I_itemlist
         and s.consignment_rate IS NOT NULL;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    L_table,
                    to_char(I_itemlist));
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    L_table,
                    to_char(I_itemlist));
   fetch C_EXISTS into L_exists;
   if C_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    L_table,
                    to_char(I_itemlist));
   close C_EXISTS;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ITEM_ON_CONSIGNMENT_EXISTS;
--------------------------------------------------------------------
FUNCTION PACK_ITEM_EXISTS(O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          I_itemlist        IN       SKULIST_HEAD.SKULIST%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)    := 'ITEMLIST_ATTRIB_SQL.PACK_ITEM_EXISTS';
   L_table      VARCHAR2(64)    := 'SKULIST_DETAIL';
   L_exists     VARCHAR2(1);
   ---
   cursor C_EXISTS is
      select 'Y'
        from skulist_detail
       where skulist = I_itemlist
         and pack_ind = 'Y';
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    L_table,
                    to_char(I_itemlist));
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    L_table,
                    to_char(I_itemlist));
   fetch C_EXISTS into L_exists;
   if C_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    L_table,
                    to_char(I_itemlist));
   close C_EXISTS;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PACK_ITEM_EXISTS;

--------------------------------------------------------------------
FUNCTION ITEM_ON_CRITERIA(O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          O_action_type     IN OUT   SKULIST_CRITERIA.ACTION_TYPE%TYPE,
                          I_skulist         IN       SKULIST_CRITERIA.SKULIST%TYPE,
                          I_item            IN       SKULIST_CRITERIA.ITEM%TYPE)
return BOOLEAN is

   L_program      VARCHAR2(64) := 'ITEMLIST_ATTRIB_SQL.ITEM_ON_CRITERIA';
   L_action_type  SKULIST_CRITERIA.ACTION_TYPE%TYPE;

   cursor C_ITEM_EXISTS is
      select action_type
        from skulist_criteria
       where item = I_item
         and skulist = I_skulist;
BEGIN
   open C_ITEM_EXISTS;
   fetch C_ITEM_EXISTS into L_action_type;
   O_action_type := L_action_type;
   if C_ITEM_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   close C_ITEM_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END ITEM_ON_CRITERIA;
--------------------------------------------------------------------
FUNCTION CRITERIA_EXISTS(O_error_message   IN OUT   VARCHAR2,
                         O_exists          IN OUT   BOOLEAN,
                         I_skulist         IN       SKULIST_CRITERIA.SKULIST%TYPE)
   RETURN BOOLEAN IS

   L_dummy        VARCHAR2(1);
   L_program      VARCHAR2(64) := 'ITEMLIST_ATTRIB_SQL.CRITERIA_EXISTS';

   cursor C_CRIT_EXISTS is
      select 'X'
        from skulist_criteria
       where skulist = I_skulist;
BEGIN
   open C_CRIT_EXISTS;
   fetch C_CRIT_EXISTS into L_dummy;
   if C_CRIT_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   close C_CRIT_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;
END CRITERIA_EXISTS;
--------------------------------------------------------------------
FUNCTION SKULIST_DEPT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_dept_exists     IN OUT   BOOLEAN,
                             I_itemlist        IN       SKULIST_DEPT.SKULIST%TYPE,
                             I_dept            IN       SKULIST_DEPT.DEPT%TYPE,
                             I_class           IN       SKULIST_DEPT.CLASS%TYPE DEFAULT NULL,
                             I_subclass        IN       SKULIST_DEPT.SUBCLASS%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)   := 'ITEMLIST_ADD_SQL.SKULIST_DEPT_EXISTS';
   L_count           NUMBER(1)      := 0;

BEGIN

   select count(1)
     into L_count
     from skulist_dept
    where skulist = I_itemlist
      and dept = I_dept
      and class = nvl(I_class,class)
      and subclass = nvl(I_subclass,subclass)
      and rownum = 1;

   if L_count > 0 then
      O_dept_exists := TRUE;
   else
      O_dept_exists := FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SKULIST_DEPT_EXISTS;
----------------------------------------------------------
FUNCTION ITEM_IN_SKULIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item            IN       SKULIST_DETAIL.ITEM%TYPE,
                         I_dept            IN       SKULIST_DEPT.DEPT%TYPE,
                         I_class           IN       SKULIST_DEPT.CLASS%TYPE,
                         I_subclass        IN       SKULIST_DEPT.SUBCLASS%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60)              := 'ITEMLIST_ATTRIB_SQL.ITEM_IN_SKULIST';
   L_skulist   SKULIST_HEAD.SKULIST%TYPE := NULL;

   cursor C_ITEM_IN_SKULIST is
      select skulist
        from skulist_detail
       where item = I_item;

BEGIN
   -- Check if required input parameter is null
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_IN_SKULIST',
                    'SKULIST_DETAIL',
                    'SKULIST: '||I_item);

   FOR rec IN C_ITEM_IN_SKULIST LOOP
      if rec.skulist is not NULL then
         if UPDATE_ITEM_LIST(O_error_message,
                             rec.skulist,
                             I_dept,
                             I_class,
                             I_subclass) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ITEM_IN_SKULIST;
----------------------------------------------------------
FUNCTION UPDATE_ITEM_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_skulist         IN       SKULIST_HEAD.SKULIST%TYPE,
                          I_dept            IN       DEPS.DEPT%TYPE,
                          I_class           IN       SKULIST_DEPT.CLASS%TYPE,
                          I_subclass        IN       SKULIST_DEPT.SUBCLASS%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'ITEMLIST_ATTRIB_SQL.UPDATE_ITEM_LIST';
   L_exist   VARCHAR2(1)  := NULL;

   cursor C_UPDATE_ITEMLIST is
      select 'x'
        from skulist_dept
       where skulist = I_skulist
         and dept = I_dept
         and class = I_class
         and subclass = I_subclass;

BEGIN
   -- Verify if required input parameters are null
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_skulist is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_skulist',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_subclass',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_UPDATE_ITEMLIST',
                    'SKULIST_DEPT',
                    'SKULIST: '||to_char(I_skulist) ||
                    ', DEPT: ' ||to_char(I_dept) ||
                    ', CLASS: '||to_char(I_class) ||
                    ', SUBCLASS: '||to_char(I_subclass));
   open C_UPDATE_ITEMLIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_UPDATE_ITEMLIST',
                    'SKULIST_DEPT',
                    'SKULIST: '||to_char(I_skulist) ||
                    ', DEPT: ' ||to_char(I_dept) ||
                    ', CLASS: '||to_char(I_class) ||
                    ', SUBCLASS: '||to_char(I_subclass));

   fetch C_UPDATE_ITEMLIST into L_exist;

   if C_UPDATE_ITEMLIST%NOTFOUND then
      -- The dept does not exist, insert a new record for it
      SQL_LIB.SET_MARK('INSERT',
                       'NULL',
                       'SKULIST_DEPT',
                       'SKULIST: '||to_char(I_skulist) ||
                       ', DEPT: ' ||to_char(I_dept) ||
                       ', CLASS: '||to_char(I_class) ||
                       ', SUBCLASS: '||to_char(I_subclass));

      insert into skulist_dept(skulist,
                               dept,
                               class,
                               subclass)
                        values(I_skulist,
                               I_dept,
                               I_class,
                               I_subclass);
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                       'C_UPDATE_ITEMLIST',
                       'SKULIST_DEPT',
                       'SKULIST: '||to_char(I_skulist) ||
                       ', DEPT: ' ||to_char(I_dept) ||
                       ', CLASS: '||to_char(I_class) ||
                       ', SUBCLASS: '||to_char(I_subclass));
   close C_UPDATE_ITEMLIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END UPDATE_ITEM_LIST;
--------------------------------------------------------------------
FUNCTION GET_INVENTORY_ITEM_COUNT(O_error_message    IN OUT    VARCHAR2,
                                  I_itemlist         IN        SKULIST_HEAD.SKULIST%TYPE,
                                  O_count            IN OUT    NUMBER)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'ITEMLIST_ATTRIB_SQL.GET_INVENTORY_ITEM_COUNT';
   ---
   cursor C_COUNT is
      select count(*)
        from skulist_detail sd,
             item_master im
       where sd.skulist       = I_itemlist
         and sd.item          = im.item
         and im.inventory_ind = 'Y';

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT',
                    'SKULIST_DETAIL',
                    to_char(I_itemlist));
   open C_COUNT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT',
                    'SKULIST_DETAIL',
                    to_char(I_itemlist));
   fetch C_COUNT into O_count;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT',
                    'SKULIST_DETAIL',
                    to_char(I_itemlist));
   close C_COUNT;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_INVENTORY_ITEM_COUNT;
-------------------------------------------------------------------------------
FUNCTION NON_VENDOR_PACKS_EXIST(
    O_error_message IN OUT VARCHAR2,
    O_exists OUT BOOLEAN,
    I_itemlist IN SKULIST_HEAD.SKULIST%TYPE)
  RETURN BOOLEAN
IS
  CURSOR C_PACK_ITEM
  IS
    SELECT 'Y'
    FROM skulist_detail s,
      item_master im
    WHERE s.item       = im.item
    AND s.skulist      = I_itemlist
    AND s.pack_ind     = 'Y'
    AND (im.pack_type != 'V'
    OR im.pack_type   IS NULL);
  L_exists VARCHAR2(1):='N';
  L_program  VARCHAR2(64) := 'ITEMLIST_ATTRIB_SQL.NON_VENDOR_PACKS_EXIST';
BEGIN
  OPEN C_PACK_ITEM;
  FETCH C_PACK_ITEM INTO L_exists;
  CLOSE C_PACK_ITEM;
  IF L_exists = 'Y' THEN
    O_exists := true;
  ELSE
    O_exists := false;
  END IF;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END NON_VENDOR_PACKS_EXIST;
----------------------------------------------------------------------------------------------
FUNCTION DELETE_SKULIST_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_skulist         IN       SKULIST_HEAD_TL.SKULIST%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'ITEMLIST_ATTRIB_SQL.DELETE_SKULIST_HEAD_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SKULIST_HEAD_TL is
      select 'x'
        from skulist_head_tl
       where skulist = I_skulist
         for update nowait;
BEGIN

   if I_skulist is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_skulist',
                                             L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'SKULIST_HEAD_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_SKULIST_HEAD_TL',
                    L_table,
                    'I_skulist '||I_skulist);
   open C_LOCK_SKULIST_HEAD_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_SKULIST_HEAD_TL',
                    L_table,
                    'I_skulist '||I_skulist);
   close C_LOCK_SKULIST_HEAD_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SKULIST_HEAD_TL',
                    'I_skulist '||I_skulist);
                    
   delete from skulist_head_tl
    where skulist = I_skulist;
    
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_SKULIST_HEAD_TL;
--------------------------------------------------------------------------------------------------------
END ITEMLIST_ATTRIB_SQL;
/


