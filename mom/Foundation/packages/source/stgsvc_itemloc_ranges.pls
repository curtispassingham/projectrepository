CREATE OR REPLACE PACKAGE STGSVC_ITEM_LOC_RANGING_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------------
-- Function Name: INSERT_SVC_ITEM_LOC_RANGING
-- Purpose      : This function inserts data into SVC_ITEM_LOC_RANGING table for async item-loc 
--                processing.
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_LOC_RANGING(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_rms_async_id            IN OUT RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                     I_item                    IN     SVC_ITEM_LOC_RANGING.ITEM%TYPE,
                                     I_item_parent             IN     SVC_ITEM_LOC_RANGING.ITEM_PARENT%TYPE,
                                     I_item_grandparent        IN     SVC_ITEM_LOC_RANGING.ITEM_GRANDPARENT%TYPE,
                                     I_locs_tbl                IN     LOC_TBL,
                                     I_loc_group_type          IN     SVC_ITEM_LOC_RANGING.LOC_GROUP_TYPE%TYPE,
                                     I_loc_group_value         IN     SVC_ITEM_LOC_RANGING.LOC_GROUP_VALUE%TYPE,
                                     I_currency_code           IN     SVC_ITEM_LOC_RANGING.CURRENCY_CODE%TYPE,
                                     I_short_desc              IN     SVC_ITEM_LOC_RANGING.SHORT_DESC%TYPE,
                                     I_dept                    IN     SVC_ITEM_LOC_RANGING.DEPT%TYPE,
                                     I_class                   IN     SVC_ITEM_LOC_RANGING.CLASS%TYPE,
                                     I_subclass                IN     SVC_ITEM_LOC_RANGING.SUBCLASS%TYPE,
                                     I_item_level              IN     SVC_ITEM_LOC_RANGING.ITEM_LEVEL%TYPE,
                                     I_tran_level              IN     SVC_ITEM_LOC_RANGING.TRAN_LEVEL%TYPE,
                                     I_sellable_ind            IN     SVC_ITEM_LOC_RANGING.SELLABLE_IND%TYPE,
                                     I_orderable_ind           IN     SVC_ITEM_LOC_RANGING.ORDERABLE_IND%TYPE,
                                     I_pack_ind                IN     SVC_ITEM_LOC_RANGING.PACK_IND%TYPE,
                                     I_pack_type               IN     SVC_ITEM_LOC_RANGING.PACK_TYPE%TYPE,
                                     I_item_status             IN     SVC_ITEM_LOC_RANGING.ITEM_STATUS%TYPE,
                                     I_waste_type              IN     SVC_ITEM_LOC_RANGING.WASTE_TYPE%TYPE,
                                     I_item_loc_status         IN     SVC_ITEM_LOC_RANGING.ITEM_LOC_STATUS%TYPE,
                                     I_local_item_desc         IN     SVC_ITEM_LOC_RANGING.LOCAL_ITEM_DESC%TYPE,
                                     I_local_short_desc        IN     SVC_ITEM_LOC_RANGING.LOCAL_SHORT_DESC%TYPE,
                                     I_ranged_ind              IN     SVC_ITEM_LOC_RANGING.RANGED_IND%TYPE,
                                     I_taxable_ind             IN     SVC_ITEM_LOC_RANGING.TAXABLE_IND%TYPE,
                                     I_store_price_ind         IN     SVC_ITEM_LOC_RANGING.STORE_PRICE_IND%TYPE,
                                     I_primary_supp            IN     SVC_ITEM_LOC_RANGING.PRIMARY_SUPP%TYPE,
                                     I_primary_cntry           IN     SVC_ITEM_LOC_RANGING.PRIMARY_CNTRY%TYPE,
                                     I_primary_variant         IN     SVC_ITEM_LOC_RANGING.PRIMARY_VARIANT%TYPE,
                                     I_primary_cost_pack       IN     SVC_ITEM_LOC_RANGING.PRIMARY_COST_PACK%TYPE,
                                     I_source_method           IN     SVC_ITEM_LOC_RANGING.SOURCE_METHOD%TYPE,
                                     I_source_wh               IN     SVC_ITEM_LOC_RANGING.SOURCE_WH%TYPE,
                                     I_store_ord_mult          IN     SVC_ITEM_LOC_RANGING.STORE_ORD_MULT%TYPE,
                                     I_receive_as_type         IN     SVC_ITEM_LOC_RANGING.RECEIVE_AS_TYPE%TYPE,
                                     I_inbound_handling_days   IN     SVC_ITEM_LOC_RANGING.INBOUND_HANDLING_DAYS%TYPE,
                                     I_daily_waste_pct         IN     SVC_ITEM_LOC_RANGING.DAILY_WASTE_PCT%TYPE,
                                     I_ti                      IN     SVC_ITEM_LOC_RANGING.TI%TYPE,
                                     I_hi                      IN     SVC_ITEM_LOC_RANGING.HI%TYPE,
                                     I_unit_cost_loc           IN     SVC_ITEM_LOC_RANGING.UNIT_COST_LOC%TYPE,
                                     I_unit_retail_loc         IN     SVC_ITEM_LOC_RANGING.UNIT_RETAIL_LOC%TYPE,
                                     I_selling_unit_retail_loc IN     SVC_ITEM_LOC_RANGING.SELLING_UNIT_RETAIL_LOC%TYPE,
                                     I_selling_uom             IN     SVC_ITEM_LOC_RANGING.SELLING_UOM%TYPE,
                                     I_multi_units             IN     SVC_ITEM_LOC_RANGING.MULTI_UNITS%TYPE,
                                     I_multi_unit_retail       IN     SVC_ITEM_LOC_RANGING.MULTI_UNIT_RETAIL%TYPE,
                                     I_multi_selling_uom       IN     SVC_ITEM_LOC_RANGING.MULTI_SELLING_UOM%TYPE,
                                     I_meas_of_each            IN     SVC_ITEM_LOC_RANGING.MEAS_OF_EACH%TYPE,
                                     I_meas_of_price           IN     SVC_ITEM_LOC_RANGING.MEAS_OF_PRICE%TYPE,
                                     I_uom_of_price            IN     SVC_ITEM_LOC_RANGING.UOM_OF_PRICE%TYPE,
                                     I_ext_uin_ind             IN     SVC_ITEM_LOC_RANGING.EXT_UIN_IND%TYPE,
                                     I_uin_type                IN     SVC_ITEM_LOC_RANGING.UIN_TYPE%TYPE,
                                     I_uin_label               IN     SVC_ITEM_LOC_RANGING.UIN_LABEL%TYPE,
                                     I_default_to_children     IN     SVC_ITEM_LOC_RANGING.DEFAULT_TO_CHILDREN%TYPE,
                                     I_capture_time            IN     SVC_ITEM_LOC_RANGING.CAPTURE_TIME%TYPE,
                                     I_costing_loc             IN     SVC_ITEM_LOC_RANGING.COSTING_LOC%TYPE DEFAULT NULL,
                                     I_costing_loc_type        IN     SVC_ITEM_LOC_RANGING.COSTING_LOC_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: INSERT_SVC_ITEM_LOC_RANGING
-- Purpose      : This function inserts data into SVC_ITEM_LOC_RANGING table for async new-item-loc 
--                processing.
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_LOC_RANGING(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_rms_async_id  IN OUT RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                     I_input         IN     NEW_ITEM_LOC_SQL.NIL_INPUT_TBL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: INSERT_SVC_ITEM_LOC_RANGING_WR
-- Purpose      : This function is a wrapper for INSERT_SVC_ITEM_LOC_RANGING to be called from 
--                ADF using DB object Type.
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_LOC_RANGING_WR(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_rms_async_id  IN OUT RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                        I_input         IN     WRP_NIL_INPUT_TBL)
RETURN NUMBER;
-----------------------------------------------------------------------------------------------
END STGSVC_ITEM_LOC_RANGING_SQL;
/
