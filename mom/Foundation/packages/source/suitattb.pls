
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SUPP_ITEM_ATTRIB_SQL AS
-----------------------------------------------------------------------
FUNCTION GET_VPN ( O_error_message IN OUT  VARCHAR2,
                   I_item          IN      item_supplier.item%TYPE,
                   I_supplier      IN      item_supplier.supplier%TYPE,
                   O_vpn           IN OUT  item_supplier.vpn%TYPE)
            RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'SUPP_ITEM_ATTRIB_SQL.GET_VPN';

   cursor C_VPN is
      select vpn
        from item_supplier
       where item = I_item
         and supplier = I_supplier;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_VPN','item_supplier',NULL);
   open C_VPN;
   SQL_LIB.SET_MARK('FETCH','C_VPN','item_supplier',NULL);
   fetch C_VPN into O_vpn;
   if C_VPN%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_VPN','item_supplier',NULL);
      close C_VPN;
      O_error_message := sql_lib.create_msg('INV_SKU_SUP',
                                            null,null,null);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_VPN','item_supplier',NULL);
   close C_VPN;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_VPN;
-------------------------------------------------------------------
FUNCTION GET_PRIMARY_SUPP( O_error_message  IN OUT  VARCHAR2,
                           O_exists         IN OUT  BOOLEAN,
                           O_supplier       IN OUT  ITEM_SUPPLIER.SUPPLIER%TYPE,
                           I_item           IN      ITEM_SUPPLIER.ITEM%TYPE)
RETURN BOOLEAN is

L_program   VARCHAR2(64) := 'SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP';


cursor C_GET_SUPPLIER is
  select supplier
    from item_supplier
   where item = I_item
     and primary_supp_ind = 'Y';

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_SUPPLIER','item_supplier',NULL);
   open C_GET_SUPPLIER;
   SQL_LIB.SET_MARK('FETCH','C_GET_SUPPLIER','item_supplier',NULL);
   fetch C_GET_SUPPLIER into O_supplier;
   if C_GET_SUPPLIER%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_SUPPLIER','item_supplier',NULL);
   close C_GET_SUPPLIER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIMARY_SUPP;
-----------------------------------------------------------------------------
FUNCTION GET_PRIMARY_COUNTRY(O_error_message  IN OUT  VARCHAR2,
                             O_origin_country IN OUT  ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_item           IN      ITEM_SUPP_COUNTRY.ITEM%TYPE,
                             I_supplier       IN      ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   cursor C_ITEM_SUPP_COUNTRY is
      select origin_country_id
        from item_supp_country
       where item                = I_item
         and supplier            = I_supplier
         and primary_country_ind = 'Y';
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY',
                      'Item: ' || I_item || ' supplier: ' || to_char(I_supplier));
   open C_ITEM_SUPP_COUNTRY;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY',
                      'Item: ' || I_item || ' supplier: ' || to_char(I_supplier));
   fetch C_ITEM_SUPP_COUNTRY into O_origin_country;
   if C_ITEM_SUPP_COUNTRY%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY',
                       'Item: ' || I_item || ' I_supplier: ' || to_char(I_supplier));
      close C_ITEM_SUPP_COUNTRY;
      O_error_message := sql_lib.create_msg('INV_SKU_SUP',
                                            null,null,null);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY',
                      'Item: ' || I_item || ' I_supplier: ' || to_char(I_supplier));
   close C_ITEM_SUPP_COUNTRY;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_COUNTRY',
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIMARY_COUNTRY;
-----------------------------------------------------------------------------------------
FUNCTION GET_PACK_SIZES(O_error_message      IN OUT VARCHAR2,
                        O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        O_pallet_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_case_desc          IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_inner_desc         IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN is
   L_dummy1   ITEM_SUPPLIER.PALLET_NAME%TYPE;
   L_dummy2   ITEM_SUPPLIER.CASE_NAME%TYPE;
   L_dummy3   ITEM_SUPPLIER.INNER_NAME%TYPE;
   L_dummy4   ITEM_SUPP_COUNTRY.TI%TYPE;
   L_dummy5   ITEM_SUPP_COUNTRY.HI%TYPE;

BEGIN

   if GET_PACK_SIZES(O_error_message,
                     L_dummy4,
                     L_dummy5,
                     O_supp_pack_size,
                     O_inner_pack_size,
                     O_pallet_desc,
                     O_case_desc,
                     O_inner_desc,
                     L_dummy1,
                     L_dummy2,
                     L_dummy3,
                     I_item,
                     I_supplier,
                     I_origin_country_id) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES',
                                            to_char(SQLCODE));
      return FALSE;
END GET_PACK_SIZES;
-------------------------------------------------------------------------------------------
FUNCTION GET_PACK_SIZES(O_error_message      IN OUT VARCHAR2,
                        O_ti                 IN OUT ITEM_SUPP_COUNTRY.TI%TYPE,
                        O_hi                 IN OUT ITEM_SUPP_COUNTRY.HI%TYPE,
                        O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        O_pallet_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_case_desc          IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_inner_desc         IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN is
   L_dummy1   ITEM_SUPPLIER.PALLET_NAME%TYPE;
   L_dummy2   ITEM_SUPPLIER.CASE_NAME%TYPE;
   L_dummy3   ITEM_SUPPLIER.INNER_NAME%TYPE;

BEGIN

   if GET_PACK_SIZES(O_error_message,
                     O_ti,
                     O_hi,
                     O_supp_pack_size,
                     O_inner_pack_size,
                     O_pallet_desc,
                     O_case_desc,
                     O_inner_desc,
                     L_dummy1,
                     L_dummy2,
                     L_dummy3,
                     I_item,
                     I_supplier,
                     I_origin_country_id) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES',
                                            to_char(SQLCODE));
      return FALSE;
END GET_PACK_SIZES;
-------------------------------------------------------------------------------------------
FUNCTION GET_PACK_SIZES(O_error_message      IN OUT VARCHAR2,
                        O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        O_pallet_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_case_desc          IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_inner_desc         IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_pallet_name        IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                        O_case_name          IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                        O_inner_name         IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN is
   L_dummy1   ITEM_SUPP_COUNTRY.TI%TYPE;
   L_dummy2   ITEM_SUPP_COUNTRY.HI%TYPE;

BEGIN

   if GET_PACK_SIZES(O_error_message,
                     L_dummy1,
                     L_dummy2,
                     O_supp_pack_size,
                     O_inner_pack_size,
                     O_pallet_desc,
                     O_case_desc,
                     O_inner_desc,
                     O_pallet_name,
                     O_case_name,
                     O_inner_name,
                     I_item,
                     I_supplier,
                     I_origin_country_id) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES',
                                            to_char(SQLCODE));
      return FALSE;
END GET_PACK_SIZES;
-------------------------------------------------------------------------------------------
FUNCTION GET_PACK_SIZES(O_error_message      IN OUT VARCHAR2,
                        O_ti                 IN OUT ITEM_SUPP_COUNTRY.TI%TYPE,
                        O_hi                 IN OUT ITEM_SUPP_COUNTRY.HI%TYPE,
                        O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                        O_inner_pack_size    IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                        O_pallet_desc        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_case_desc          IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_inner_desc         IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                        O_pallet_name        IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                        O_case_name          IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                        O_inner_name         IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   L_pack_type                  ITEM_MASTER.PACK_TYPE%TYPE;
   L_pack_ind                   ITEM_MASTER.PACK_IND%TYPE;
   L_orderable_ind              ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_sellable_ind               ITEM_MASTER.SELLABLE_IND%TYPE;

   L_default_standard_uom       SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE;
   L_default_dimension_uom      SYSTEM_OPTIONS.DEFAULT_DIMENSION_UOM%TYPE;
   L_default_weight_uom         SYSTEM_OPTIONS.DEFAULT_WEIGHT_UOM%TYPE;
   L_default_packing_method     SYSTEM_OPTIONS.DEFAULT_PACKING_METHOD%TYPE;

   cursor C_GET_PACK_SIZES is
      select isc.ti,
             isc.hi,
             isc.supp_pack_size,
             isc.inner_pack_size,
             isp.pallet_name,
             isp.case_name,
             isp.inner_name
        from item_supp_country isc, item_supplier isp
       where isc.item = I_item
         and isc.supplier = isp.supplier
         and isc.item = isp.item
         and ((isc.supplier  = I_supplier
               and I_supplier is NOT NULL)
                or (isc.primary_supp_ind = 'Y'
                    and  I_supplier is NULL))
         and ((isc.origin_country_id  = I_origin_country_id
               and I_origin_country_id is NOT NULL)
                or (isc.primary_country_ind = 'Y'
                    and  I_origin_country_id is NULL));

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_PACK_SIZES','ITEM_SUPP_COUNTRY',NULL);
   open C_GET_PACK_SIZES;
   SQL_LIB.SET_MARK('FETCH','C_GET_PACK_SIZES','ITEM_SUPP_COUNTRY',NULL);
   fetch C_GET_PACK_SIZES into O_ti,
                               O_hi,
                               O_supp_pack_size,
                               O_inner_pack_size,
                               O_pallet_name,
                               O_case_name,
                               O_inner_name;
   if C_GET_PACK_SIZES%NOTFOUND then
      if not ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                           L_pack_ind,
                                           L_sellable_ind,
                                           L_orderable_ind,
                                           L_pack_type,
                                           I_item) then
         return FALSE;
      end if;
      if L_orderable_ind = 'N' then
         -- For non orderable item, there is no ITEM_SUPPLIER or ITEM_SUPP_COUNTRY record, so default
         -- value will be used for the pack size.
         O_ti              := 1;
         O_hi              := 1;
         O_supp_pack_size  := 1;
         O_inner_pack_size := 1;
         if SYSTEM_OPTIONS_SQL.GET_DEFAULT_UOM_AND_NAMES (O_error_message,
                                                          L_default_standard_uom,
                                                          L_default_dimension_uom,
                                                          L_default_weight_uom,
                                                          L_default_packing_method,
                                                          O_pallet_name,
                                                          O_case_name,
                                                          O_inner_name) = FALSE then
            return FALSE;
         end if;
      else
         SQL_LIB.SET_MARK('CLOSE','C_GET_PACK_SIZES','ITEM_SUPP_COUNTRY',NULL);
         close C_GET_PACK_SIZES;
         O_error_message := sql_lib.create_msg('INV_ITEM/SUPPLIER/ORIGIN',
                                                NULL,NULL,NULL);
         return FALSE;
      end if;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PACK_SIZES','ITEM_SUPP_COUNTRY',NULL);
   close C_GET_PACK_SIZES;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'PALN',
                                 O_pallet_name,
                                 O_pallet_desc) = FALSE then
      return FALSE;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'INRN',
                                 O_inner_name,
                                 O_inner_desc) = FALSE then
      return FALSE;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'CASN',
                                 O_case_name,
                                 O_case_desc) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES',
                                            to_char(SQLCODE));
      return FALSE;
END GET_PACK_SIZES;
-----------------------------------------------------------------------------------------
FUNCTION GET_CARTON_INFO(O_error_message     IN OUT VARCHAR2,
                         O_length               OUT ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                         O_height               OUT ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                         O_width                OUT ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE,
                         O_dim_uom              OUT ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                         O_weight               OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                         O_weight_uom           OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                         O_net_weight           OUT ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE,
                         O_liquid_volume        OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
                         O_volume_uom           OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE,
                         O_stat_cube            OUT ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE,
                         O_tare_wgt             OUT ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE,
                         O_tare_ind             OUT ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                         I_origin_country    IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                         I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
RETURN BOOLEAN IS

L_primary_country_ind   ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE;

   cursor C_GET_CARTON_INFO is
      select iscd.length,
             iscd.height,
             iscd.width,
             iscd.lwh_uom,
             iscd.weight,
             iscd.weight_uom,
             iscd.net_weight,
             iscd.liquid_volume,
             iscd.liquid_volume_uom,
             iscd.stat_cube,
             iscd.tare_weight,
             iscd.tare_type
        from item_supp_country_dim iscd, item_supp_country isc
       where iscd.item                = I_item
         and iscd.supplier            = I_supplier
         and iscd.item               = isc.item
         and iscd.supplier           = isc.supplier
         and iscd.origin_country     = isc.origin_country_id
         and ((isc.origin_country_id  = I_origin_country
               and I_origin_country is NOT NULL)
                or (isc.primary_country_ind = 'Y'
                    and I_origin_country is NULL));

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_CARTON_INFO','ITEM_SUPP_COUNTRY',NULL);
   open C_GET_CARTON_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_CARTON_INFO','ITEM_SUPP_COUNTRY',NULL);
   fetch C_GET_CARTON_INFO into O_length,
                                O_height,
                                O_width,
                                O_dim_uom,
                                O_weight,
                                O_weight_uom,
                                O_net_weight,
                                O_liquid_volume,
                                O_volume_uom,
                                O_stat_cube,
                                O_tare_wgt,
                                O_tare_ind;
   if C_GET_CARTON_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_CARTON_INFO','ITEM_SUPP_COUNTRY',NULL);
      close C_GET_CARTON_INFO;
      O_error_message := sql_lib.create_msg('INV_ITEM/SUPPLIER/ORIGIN',
                                             NULL,NULL,NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_CARTON_INFO','ITEM_SUPP_COUNTRY',NULL);
   close C_GET_CARTON_INFO;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_CARTON_INFO',
                                            to_char(SQLCODE));
      return FALSE;
END GET_CARTON_INFO;
-----------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_SUPP_COUNTRY(O_error_message  IN OUT VARCHAR2,
                                  O_supplier       IN OUT ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                  O_origin_country IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                  I_item           IN     ITEM_SUPP_COUNTRY.ITEM%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_PRIMARY_SUPP_COUNTRY is
      select supplier, origin_country_id
        from item_supp_country
       where item = I_item
         and primary_supp_ind = 'Y'
         and primary_country_ind = 'Y';

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);
   open C_GET_PRIMARY_SUPP_COUNTRY;
   SQL_LIB.SET_MARK('FETCH','C_GET_PRIMARY_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);
   fetch C_GET_PRIMARY_SUPP_COUNTRY into O_supplier,
                                         O_origin_country;
   if C_GET_PRIMARY_SUPP_COUNTRY%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_PRIMARY_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);
      close C_GET_PRIMARY_SUPP_COUNTRY;
      O_error_message := sql_lib.create_msg('NO_SUPP_COOG',
                                             I_item,NULL,NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_PRIMARY_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);
   close C_GET_PRIMARY_SUPP_COUNTRY;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP_COUNTRY',
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIMARY_SUPP_COUNTRY;
-----------------------------------------------------------------------------------------
FUNCTION GET_ATTRIB_FOR_ORDSKU(O_error_message     IN OUT VARCHAR2,
                               O_unit_cost         IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                               O_supp_pack_size    IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                               O_origin_country_id IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                               I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                               I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'SUPP_ITEM_ATTRIB_SQL.GET_ATTRIB_FOR_ORDSKU';

   cursor C_GET_ATTRIB is
      select origin_country_id,
             unit_cost,
             supp_pack_size
        from item_supp_country
       where item                = I_item
         and supplier            = I_supplier
         and primary_country_ind = 'Y';
BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_ATTRIB','ITEM_SUPP_COUNTRY','Item: '|| I_item || ' Supplier: ' || to_char(I_supplier));
   open C_GET_ATTRIB;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_ATTRIB','ITEM_SUPP_COUNTRY','Item: '|| I_item || ' Supplier: ' || to_char(I_supplier));
   fetch C_GET_ATTRIB into O_origin_country_id,
                           O_unit_cost,
                           O_supp_pack_size;
   if C_GET_ATTRIB%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_ATTRIB','ITEM_SUPP_COUNTRY','Item: '|| I_item || ' Supplier: ' || to_char(I_supplier));
      close C_GET_ATTRIB;
      O_error_message := SQL_LIB.CREATE_MSG('NO_SUPP_COOG',
                                             I_item,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_ATTRIB','ITEM_SUPP_COUNTRY','Item: '|| I_item || ' Supplier: ' || to_char(I_supplier));
   close C_GET_ATTRIB;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ATTRIB_FOR_ORDSKU;
-----------------------------------------------------------------------------------------
FUNCTION GET_SUPP_PACK_SIZE(O_error_message      IN OUT VARCHAR2,
                            O_supp_pack_size     IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                            I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                            I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                            I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   L_pack_type                  ITEM_MASTER.PACK_TYPE%TYPE;
   L_pack_ind                   ITEM_MASTER.PACK_IND%TYPE;
   L_orderable_ind              ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_sellable_ind               ITEM_MASTER.SELLABLE_IND%TYPE;

   cursor C_GET_PACK_SIZES is
      select isc.supp_pack_size
        from item_supp_country isc
       where isc.item = I_item
        and ((isc.supplier  = I_supplier
               and I_supplier is NOT NULL)
                or (isc.primary_supp_ind = 'Y'
                    and  I_supplier is NULL))
         and ((isc.origin_country_id  = I_origin_country_id
               and I_origin_country_id is NOT NULL)
                or (isc.primary_country_ind = 'Y'
                    and  I_origin_country_id is NULL));

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_PACK_SIZES','ITEM_SUPP_COUNTRY',NULL);
   open C_GET_PACK_SIZES;

   SQL_LIB.SET_MARK('FETCH','C_GET_PACK_SIZES','ITEM_SUPP_COUNTRY',NULL);
   fetch C_GET_PACK_SIZES into O_supp_pack_size;

   if C_GET_PACK_SIZES%NOTFOUND then
      if not ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                           L_pack_ind,
                                           L_sellable_ind,
                                           L_orderable_ind,
                                           L_pack_type,
                                           I_item) then
            return FALSE;
      end if;
      if L_orderable_ind = 'N' then
         O_supp_pack_size:=1;
      else
      SQL_LIB.SET_MARK('CLOSE','C_GET_PACK_SIZES','ITEM_SUPP_COUNTRY',NULL);
      close C_GET_PACK_SIZES;
      O_error_message := sql_lib.create_msg('INV_ITEM/SUPPLIER/ORIGIN',
                                             NULL,NULL,NULL);
      return FALSE;
      end if;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_GET_PACK_SIZES','ITEM_SUPP_COUNTRY',NULL);
   close C_GET_PACK_SIZES;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_PACK_SIZES',
                                            to_char(SQLCODE));
      return FALSE;
END GET_SUPP_PACK_SIZE;
-----------------------------------------------------------------------------------------
FUNCTION GET_LEAD_TIME(O_error_message     IN OUT  VARCHAR2,
                       O_lead_time         IN OUT  ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                       I_item              IN      ITEM_SUPP_COUNTRY.ITEM%TYPE,
                       I_supplier          IN      ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                       I_origin_country_id IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

cursor C_GET_LEAD_TIME is
   select NVL(isc.lead_time, 0)
     from item_supp_country isc
    where isc.item = I_item
      and isc.supplier = I_supplier
      and isc.origin_country_id  = I_origin_country_id ;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY',NULL);
   open C_GET_LEAD_TIME;

   SQL_LIB.SET_MARK('FETCH','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY',NULL);
   fetch C_GET_LEAD_TIME into O_lead_time;

   if C_GET_LEAD_TIME%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY',NULL);
      close C_GET_LEAD_TIME;
      O_error_message := sql_lib.create_msg('ITEM/SUPP/ORIG',
                                             NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY',NULL);
   close C_GET_LEAD_TIME;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_LEAD_TIME',
                                            to_char(SQLCODE));
      return FALSE;
END GET_LEAD_TIME;
-----------------------------------------------------------------------------------------
FUNCTION GET_INFO ( O_error_message         IN OUT VARCHAR2,
                    O_relationship_exists   IN OUT BOOLEAN,
                    O_primary_supp_ind      IN OUT ITEM_SUPPLIER.PRIMARY_SUPP_IND%TYPE,
                    O_vpn                   IN OUT ITEM_SUPPLIER.VPN%TYPE,
                    O_supp_label            IN OUT ITEM_SUPPLIER.SUPP_LABEL%TYPE,
                    O_consignment_rate      IN OUT ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
                    O_supp_diff_1           IN OUT ITEM_SUPPLIER.SUPP_DIFF_1%TYPE,
                    O_supp_diff_2           IN OUT ITEM_SUPPLIER.SUPP_DIFF_2%TYPE,
                    O_supp_diff_3           IN OUT ITEM_SUPPLIER.SUPP_DIFF_3%TYPE,
                    O_supp_diff_4           IN OUT ITEM_SUPPLIER.SUPP_DIFF_4%TYPE,
                    O_pallet_name           IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                    O_case_name             IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                    O_inner_name            IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                    O_supp_discontinue_date IN OUT ITEM_SUPPLIER.SUPP_DISCONTINUE_DATE%TYPE,
                    O_concession_rate       IN OUT ITEM_SUPPLIER.CONCESSION_RATE%TYPE,
                    I_item                  IN     ITEM_SUPPLIER.ITEM%TYPE,
                    I_supplier              IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   return BOOLEAN is
   ---
   L_program VARCHAR2(64) := 'SUPP_ITEM_ATTRIB_SQL.GET_INFO';
   ---
   cursor C_GET_INFO is
      select primary_supp_ind,
             vpn,
             supp_label,
             consignment_rate,
             supp_diff_1,
             supp_diff_2,
             supp_diff_3,
             supp_diff_4,
             pallet_name,
             case_name,
             inner_name,
             supp_discontinue_date,
             concession_rate
        from item_supplier
       where item     = I_item
         and supplier = I_supplier;
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_SUPPLIER',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   O_primary_supp_ind := NULL;
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_INFO',
                    'ITEM_SUPPLIER',
                    'ITEM: '||I_item||', SUPPLIER: '||I_supplier);
   open C_GET_INFO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_INFO',
                    'ITEM_SUPPLIER',
                    'ITEM: '||I_item||', SUPPLIER: '||I_supplier);
   fetch C_GET_INFO into O_primary_supp_ind,
                         O_vpn,
                         O_supp_label,
                         O_consignment_rate,
                         O_supp_diff_1,
                         O_supp_diff_2,
                         O_supp_diff_3,
                         O_supp_diff_4,
                         O_pallet_name,
                         O_case_name,
                         O_inner_name,
                         O_supp_discontinue_date,
                         O_concession_rate;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_INFO',
                    'ITEM_SUPPLIER',
                    'ITEM: '||I_item||', SUPPLIER: '||I_supplier);
   close C_GET_INFO;
   O_relationship_exists := TRUE;
   ---
   if O_primary_supp_ind is NULL then
      O_relationship_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_SUP_NOT_EXIST',
                                            I_supplier,
                                            I_item,
                                            NULL);
      return TRUE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_INFO;
----------------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_CASE_SIZE(O_error_message     IN OUT VARCHAR2,
                               O_primary_case_size IN OUT ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE,
                               I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                               I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN is

   L_item   ITEM_SUPPLIER.ITEM%TYPE;

   cursor C_GET_PRIMARY_CASE_SIZE is
      select item,
             primary_case_size
        from item_supplier
       where item     = I_item
         and supplier = I_supplier;
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_SUPPLIER',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   open C_GET_PRIMARY_CASE_SIZE;

   fetch C_GET_PRIMARY_CASE_SIZE into L_item,
                                      O_primary_case_size;

   close C_GET_PRIMARY_CASE_SIZE;

   -- Raise error only when record is found, but primary_case_size is NULL
   if L_item is NOT NULL and O_primary_case_size is NULL then

      O_error_message := SQL_LIB.CREATE_MSG('NO_PR_CASE_SIZE',
                                            NULL,NULL,NULL);
      return FALSE;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_CASE_SIZE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIMARY_CASE_SIZE;
-----------------------------------------------------------------------------------------
--Function Name: GET_SUPP_SOB
--Purpose      : Get supplier related to the input Item that has the same org unit as
--               the Input Location. Primary Supplier having higher priority.
------------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_SOB (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_supplier          IN OUT ITEM_LOC.PRIMARY_SUPP%TYPE,
                       I_item              IN     ITEM_LOC.ITEM%TYPE,
                       I_location          IN     ITEM_LOC.LOC%TYPE,
                       I_loc_type          IN     ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_SUPP_SOB is
      select supplier
        from item_supplier isu,
             partner_org_unit pou,
             (select org_unit_id
                from store
               where I_loc_type = 'S'
                 and store = I_location
           union all
              select org_unit_id
                from wh
               where I_loc_type = 'W'
                 and wh = I_location
           union all
              select org_unit_id
                from partner
               where I_loc_type = 'E'
                 and partner_id = I_location) loc_org_unit
       where isu.item = I_item
         and pou.partner = isu.supplier
         and pou.partner_type in ('S','U')
         and pou.org_unit_id = loc_org_unit.org_unit_id
    order by isu.primary_supp_ind desc;

   L_program   VARCHAR2(64) := 'SUPP_ITEM_ATTRIB_SQL.GET_SUPP_SOB';

BEGIN
   open C_GET_SUPP_SOB;

   fetch C_GET_SUPP_SOB into O_supplier;

   if C_GET_SUPP_SOB%NOTFOUND then
      O_supplier := NULL;
   end if;

   close C_GET_SUPP_SOB;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SUPP_SOB;

-----------------------------------------------------------------------------------------
--Function Name: CHECK_ITEM_SUPP_PARENT_EXIST
--Purpose      : Checks if any one of the Supplier Sites related to the input Supplier is associated
--               with the item passed as input
------------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_SUPP_PARENT_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists            IN OUT BOOLEAN,
                                      I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                                      I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                      I_parent_ind        IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   cursor C_CHECK_ITEM_SUPP_PARENT_EXIST is
   select 'Y'
     from item_supplier isu,
          sups sup
    where isu.item = I_item
      and isu.supplier = sup.supplier
      and sup.supplier_parent = I_supplier;

   cursor C_CHK_PARNT_ITM_SUP_PARNT_EXST is
   select 'Y'
     from item_supp_country isc,
          item_master im,
          sups s2
    where im.item = isc.item
      and (im.item = I_item
           or im.item_parent = I_item
           or im.item_grandparent = I_item)
      and im.item_level <= im.tran_level
      and s2.supplier_parent = I_supplier
      and isc.supplier = s2.supplier
    group by isc.supplier, isc.origin_country_id
   having count(1) = (select count(1)
                        from item_master im2
                       where (im2.item = I_item
                              or im2.item_parent = I_item
                              or im2.item_grandparent = I_item)
                         and im2.item_level <= im2.tran_level);


   L_item_exists        VARCHAR2(1);
   L_program            VARCHAR2(64) := 'SUPP_ITEM_ATTRIB_SQL.CHECK_ITEM_SUPP_PARENT_EXIST';

BEGIN
   O_exists := TRUE;

   if I_parent_ind = 'N' then
      open C_CHECK_ITEM_SUPP_PARENT_EXIST;

      fetch C_CHECK_ITEM_SUPP_PARENT_EXIST into L_item_exists;

      if C_CHECK_ITEM_SUPP_PARENT_EXIST%NOTFOUND then
         O_exists := FALSE;
      end if;

      close C_CHECK_ITEM_SUPP_PARENT_EXIST;
   else
      open C_CHK_PARNT_ITM_SUP_PARNT_EXST;

      fetch C_CHK_PARNT_ITM_SUP_PARNT_EXST into L_item_exists;

      if C_CHK_PARNT_ITM_SUP_PARNT_EXST%NOTFOUND then
         O_exists := FALSE;
      end if;

      close C_CHK_PARNT_ITM_SUP_PARNT_EXST;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ITEM_SUPP_PARENT_EXIST;
-----------------------------------------------------------------------------------------
--Function Name: CHECK_VPN_SUPP_EXIST
--Purpose      : Checks if an item record exists in Item_supplier table for the input supplier
--               and VPN
------------------------------------------------------------------------------------------------
FUNCTION CHECK_VPN_SUPP_EXIST (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists            IN OUT BOOLEAN,
                               I_vpn               IN     ITEM_SUPPLIER.VPN%TYPE,
                               I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   cursor C_CHECK_VPN_SUPP_EXIST is
   select 'Y'
     from item_supplier isu,
          sups sup
    where isu.vpn = I_vpn
      and isu.supplier = sup.supplier
      and (sup.supplier_parent = I_supplier
       or  sup.supplier = I_supplier);

   L_vpn_supp_exists    VARCHAR2(1);
   L_program            VARCHAR2(64) := 'SUPP_ITEM_ATTRIB_SQL.CHECK_VPN_SUPP_EXIST';

BEGIN
   O_exists := TRUE;

   open C_CHECK_VPN_SUPP_EXIST;

   fetch C_CHECK_VPN_SUPP_EXIST into L_vpn_supp_exists;

   if C_CHECK_VPN_SUPP_EXIST%NOTFOUND then
      O_exists := FALSE;
   end if;

   close C_CHECK_VPN_SUPP_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_VPN_SUPP_EXIST;
-----------------------------------------------------------------------------------------
--Function Name: ITEMSUPP_FILTER_LIST
--Purpose      : Checks if any of the supplier associated to the item doesn't exist in V_SUPS
--               i.e : User doesn't have access to the supplier
------------------------------------------------------------------------------------------------
FUNCTION ITEMSUPP_FILTER_LIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_diff_ind        IN OUT VARCHAR2,
                              I_item            IN     ITEM_SUPPLIER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_DIFFERENCE is
   select 'Y'
     from item_supplier isu
    where supplier not in (select supplier
                           from v_sups);

   L_program            VARCHAR2(64) := 'SUPP_ITEM_ATTRIB_SQL.ITEMSUPP_FILTER_LIST';

BEGIN
   open C_GET_DIFFERENCE;

   fetch C_GET_DIFFERENCE into O_diff_ind;

   if C_GET_DIFFERENCE%NOTFOUND then
      O_diff_ind := 'N';
   end if;

   close C_GET_DIFFERENCE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ITEMSUPP_FILTER_LIST;
---------------------------------------------------------------------------------------------------------
END SUPP_ITEM_ATTRIB_SQL;
/
