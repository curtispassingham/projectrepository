
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE CLASS_ATTRIB_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------
-- Name:    GET_NAME
-- Purpose: Looks up a CLASS name from CLASS.
-- Created By: Chad Whipple, 01-AUG-96.
--------------------------------------------------------------------
   FUNCTION GET_NAME (O_error_message  IN OUT VARCHAR2,
                      I_dept           IN     NUMBER,
                      I_class          IN     NUMBER,
                      O_class_name     IN OUT VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Name:    GET_CLASS_VAT_IND
-- Purpose: fetches the class vat indicator from the class table
--------------------------------------------------------------------
   FUNCTION GET_CLASS_VAT_IND (O_error_message  IN OUT VARCHAR2,
                               O_class_vat_ind  IN OUT CLASS.CLASS_VAT_IND%TYPE,
                               I_dept           IN     DEPS.DEPT%TYPE,
                               I_class          IN     CLASS.CLASS%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Name:    ITEMS_EXIST_IN_CLASS
-- Purpose: determines if items are associated with the given class
--------------------------------------------------------------------
   FUNCTION ITEMS_EXIST_IN_CLASS (O_error_message  IN OUT VARCHAR2,
                                  O_exist          IN OUT BOOLEAN,
                                  I_dept           IN     DEPS.DEPT%TYPE,
                                  I_class          IN     CLASS.CLASS%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Name:    GET_CLASS_VAT_IND
-- Purpose: fetches the class vat indicator from the class table
--------------------------------------------------------------------
   FUNCTION GET_CLASS_VAT_IND (O_error_message  IN OUT VARCHAR2,
                               O_class_vat_ind  IN OUT CLASS.CLASS_VAT_IND%TYPE,
                               I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
END CLASS_ATTRIB_SQL;
/


