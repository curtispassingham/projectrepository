create or replace PACKAGE CORESVC_PO_INDUCT_CFG AUTHID CURRENT_USER AS
   template_key           CONSTANT VARCHAR2(255):='PO_INDUCT_CONFIG_DATA';
   action_new             VARCHAR2(25)          := 'NEW';
   action_mod             VARCHAR2(25)          := 'MOD';
   action_del             VARCHAR2(25)          := 'DEL';
   SVC_ORD_PM_CFG_sheet                                 VARCHAR2(255)         := 'SVC_ORDER_PARAMETER_CONFIG';
   SVC_ORD_PM_CFG_$Action                               NUMBER                :=1;
   SVC_ORD_PM_CFG_$APPLY_DEALS                          NUMBER                :=8;
   SVC_ORD_PM_CFG_$APPLY_SCALING                        NUMBER                :=7;
   SVC_ORD_PM_CFG_$SKIP_OPEN_SPMT                       NUMBER                :=13;
   SVC_ORD_PM_CFG_$CANCEL_ALLOC                         NUMBER                :=12;
   SVC_ORD_PM_CFG_$RECALC_RPL                           NUMBER                :=11;
   SVC_ORD_PM_CFG_$OTB_OVERRIDE                         NUMBER                :=10;
   SVC_ORD_PM_CFG_$MAX_ORD_EXP_DY                       NUMBER                :=6;
   SVC_ORD_PM_CFG_$MAX_ORD_NO_QTY                       NUMBER                :=5;
   SVC_ORD_PM_CFG_$WAIT_BTWN_THRD                       NUMBER                :=4;
   SVC_ORD_PM_CFG_$MAX_THREADS                          NUMBER                :=3;
   SVC_ORD_PM_CFG_$MAX_CHUNK_SIZE                       NUMBER                :=2;
   SVC_ORD_PM_CFG_$APPLY_BRACKETS                       NUMBER                :=9;
   SVC_ORD_PM_CFG_$OVRD_ML_CT_SRC                       NUMBER                :=14;

   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE := 'RMSADM';
   TYPE SVC_ORDER_PM_CFG_rec_tab IS TABLE OF SVC_ORDER_PARAMETER_CONFIG%ROWTYPE;

   PO_INDUCT_CONFIG_sheet             VARCHAR2(255)         := 'PO_INDUCT_CONFIG';
   PO_INDUCT_CONFIG$Action                        NUMBER                :=1;
   PO_INDUCT_CONFIG$MXPOFR_DNLD                   NUMBER                :=2;
   PO_INDUCT_CONFIG$MXPOFRSN_DNLD                 NUMBER                :=3;
   PO_INDUCT_CONFIG$MXFLSZFR_UPLD                 NUMBER                :=4;
   PO_INDUCT_CONFIG$MXFLSZFRSC_UL                 NUMBER                :=5;

   TYPE PO_INDUCT_CONFIG_rec_tab IS TABLE OF PO_INDUCT_CONFIG%ROWTYPE;
---------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER
                        )
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER
                   )
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
END CORESVC_PO_INDUCT_CFG;
/