
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_LOC_HIST_SQL AUTHID CURRENT_USER IS
------------------------------------------------------------------------------------
--  Function Name:  EOW_SALES_ISSUES
--  Purpose      :  This new function will take a specified item, location, 
--                  location type, date range, and sales type and use the information 
--                  to populate up to 54 end of week sales_issues variables 
--  Called By    :  Store Sales / Warehouse Issues form (stslwhis.fmb)
--  Input Values :  I_item, I_loc, I_loc_type, I_from_date and I_back_to_date
--  Return Value :  O_eow_sales_issues1...54 and O_sum_sales_issues 
--  Created      :  30-JAN-2001 by Shaam A. Khan

-------------------------------------------------------------------------------------
 FUNCTION EOW_SALES_ISSUES
         (O_error_message       IN OUT VARCHAR2,
          O_eow_sales_issues1   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues2   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues3   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues4   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues5   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues6   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues7   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues8   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues9   IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues10  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues11  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues12  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues13  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues14  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues15  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues16  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues17  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues18  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues19  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues20  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues21  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues22  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues23  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues24  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues25  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues26  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues27  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues28  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues29  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues30  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues31  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues32  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues33  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues34  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues35  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues36  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues37  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues38  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues39  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues40  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues41  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues42  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues43  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues44  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues45  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues46  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues47  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues48  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues49  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues50  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues51  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues52  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues53  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_eow_sales_issues54  IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_sum_sales_issues    IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          I_item                IN     ITEM_LOC_HIST.ITEM%TYPE,
          I_loc                 IN     ITEM_LOC_HIST.LOC%TYPE,
          I_loc_type            IN     ITEM_LOC_HIST.LOC_TYPE%TYPE,
          I_from_date           IN     ITEM_LOC_HIST.EOW_DATE%TYPE,
          I_back_to_date        IN     ITEM_LOC_HIST.EOW_DATE%TYPE,
          I_sales_type          IN     ITEM_LOC_HIST.SALES_TYPE%TYPE
		  )
   RETURN BOOLEAN;
--------------------------------------------------------------------------
--  Function Name :  WTD_HTD_SALES_ISSUES
--  Purpose       :  This new function will take a specified item and optional location 
--                   to populate the week-to-date and half-to-date sales and issues information
--  Called By     :  Item Location Inventory form (itlocinv.fmb)
--  Input Values  :  I_item, I_loc (Optional)
--  Return Value  :  O_wtd_sales_issues, O_htd_sales_issues 
--  Created       :  30-JAN-2001 by Shaam A. Khan

--------------------------------------------------------------------------
  FUNCTION WTD_HTD_SALES_ISSUES
         (O_error_message       IN OUT VARCHAR2,
          O_wtd_sales_issues    IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_htd_sales_issues    IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          I_item                IN     ITEM_LOC_HIST.ITEM%TYPE,
          I_loc                 IN     ITEM_LOC_HIST.LOC%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------
   FUNCTION WTD_HTD_SALES_ISSUES_CORP
         (O_error_message IN OUT VARCHAR2,
          O_wtd_issues    IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_htd_issues    IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_wtd_sales     IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          O_htd_sales     IN OUT ITEM_LOC_HIST.SALES_ISSUES%TYPE,
          I_item          IN     ITEM_LOC_HIST.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------------------
END ITEM_LOC_HIST_SQL;
/
