
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PM_PROMO_SQL AUTHID CURRENT_USER IS

--------------------------------------------------------------------------------
-- This is a stub package that should be replaced once the price management
-- apis are developed.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PUBLIC RECORD TYPES
--------------------------------------------------------------------------------

TYPE PROMO_REC IS RECORD
(
   promo_id           RPM_PROMO.PROMO_ID%TYPE,
   promo_desc         RPM_PROMO.NAME%TYPE,  -- map to NAME instead of DESCRIPTION on RPM_PROMO
   --
   promo_event_id     RPM_PROMO_EVENT.PROMO_EVENT_ID%TYPE,
   promo_event_desc   RPM_PROMO_EVENT.DESCRIPTION%TYPE
);


--------------------------------------------------------------------------------
-- PUBLIC VARIABLES
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION GET_PROMO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_promotion     IN     ORDHEAD.PROMOTION%TYPE,
                   O_promo_rec        OUT PM_PROMO_SQL.PROMO_REC)
RETURN BOOLEAN;


END PM_PROMO_SQL;
/
