CREATE OR REPLACE PACKAGE ORGANIZATION_SQL AUTHID CURRENT_USER AS 
---------------------------------------------------------------------
TYPE LOC_TRAIT_REC   IS RECORD(loc_trait_id     LOC_TRAITS.LOC_TRAIT%TYPE);
TYPE LOC_TRAIT_TBL   IS TABLE OF LOC_TRAIT_REC;
TYPE ORG_HIER_REC    IS RECORD(hier_level       VARCHAR2(30),
                               hier_value       STORE.STORE%TYPE,
                               hier_desc        STORE.STORE_NAME%TYPE,
                               mgr_name         STORE.STORE_MGR_NAME%TYPE,
                               currency_code    CURRENCIES.CURRENCY_CODE%TYPE,
                               parent_id        STORE.STORE%TYPE,
                               old_parent_id    STORE.STORE%TYPE,
                               loc_trait_ids    LOC_TRAIT_TBL);
---------------------------------------------------------------------
LP_chain      CONSTANT   VARCHAR2(2)  := 'CH';
LP_area       CONSTANT   VARCHAR2(2)  := 'AR';  
LP_region     CONSTANT   VARCHAR2(2)  := 'RE';
LP_district   CONSTANT   VARCHAR2(2)  := 'DI'; 
LP_store      CONSTANT   VARCHAR2(2)  := 'S';   
LP_wh         CONSTANT   VARCHAR2(2)  := 'W';        
---------------------------------------------------------------------------------------------
-- Function Name : ORG_HIER_SUBQUERY
-- Purpose       : This function returns a query string that retrieves store values from
--                 organization hierarchy tables depending on the level of hierarchy passed
--                 to the function.  Optionally, a list of values for the hierarchy table, i.e.
--                 CHAIN, AREA, etc.) can be passed in as a LOC_TBL type.  The function will
--                 determine if 1 or more values is in the table and adjust the query to use
--                 this information as a condition for limiting the hierarchy table.
--                 Optionally, the query string can contain a join to another table if that 
--                 table name is passed into the funtion.  The query string can be used as a 
--                 subquery or executed as an independent query.
--                 Values for I_hier_level are the constants for organizational hierarchy 
--                 levels declared in this package spec.
---------------------------------------------------------------------------------------------
FUNCTION ORG_HIER_SUBQUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_subquery        OUT    VARCHAR2,
                           I_bind_no         IN     VARCHAR2,
                           I_hier_level      IN     VARCHAR2,
                           I_hier_tbl        IN     LOC_TBL,
                           I_main_query_tab  IN     VARCHAR2,
                           I_bind_no_sup     IN     VARCHAR2   DEFAULT NULL)
RETURN  BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : ORG_HIER_SUBQUERY
-- Purpose       : This function is overloaded to optionally include in the query
--                 the following conditions: country_id, currency_code and exception stores
--                 These conditions are required for the pricing subscription API packages.
---------------------------------------------------------------------------------------------
FUNCTION ORG_HIER_SUBQUERY(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_subquery           OUT    VARCHAR2,
                           I_bind_no            IN     VARCHAR2,
                           I_hier_level         IN     VARCHAR2,
                           I_hier_tbl           IN     LOC_TBL,
                           I_main_query_tab     IN     VARCHAR2,
                           I_currency_bind_no   IN     VARCHAR2,
                           I_country_bind_no    IN     VARCHAR2,
                           I_store_list_bind_no IN     VARCHAR2)
RETURN  BOOLEAN;
-------------------------------------------------------------------------------
   -- Function    : INSERT_CHAIN
   -- Purpose     : Takes in a chain table record and inserts all of
   --               its contents into the CHAIN table.
---------------------------------------------------------------------
FUNCTION INSERT_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : UPDATE_CHAIN
   -- Purpose     : Takes in a chain table record and updates 
   --               the CHAIN table.
---------------------------------------------------------------------
FUNCTION UPDATE_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_CHAIN
   -- Purpose     : Takes in a chain table record and deletes
   --               from the CHAIN table.
---------------------------------------------------------------------
FUNCTION DELETE_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
---------------------------------------------------------------------
   -- Function    : INSERT_AREA
   -- Purpose     : Takes in an area table record and inserts all of
   --               its contents into the AREA table.
---------------------------------------------------------------------
FUNCTION INSERT_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : UPDATE_AREA
   -- Purpose     : Takes in an area table record and updates 
   --               the AREA table.
---------------------------------------------------------------------
FUNCTION UPDATE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_AREA
   -- Purpose     : Takes in an area table record and deletes 
   --               from the AREA table.
---------------------------------------------------------------------
FUNCTION DELETE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
---------------------------------------------------------------------
   -- Function    : INSERT_REGION
   -- Purpose     : Takes in a region table record and inserts all of
   --               its contents into the REGION table.
---------------------------------------------------------------------
FUNCTION INSERT_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : UPDATE_REGION
   -- Purpose     : Takes in a region table record and updates 
   --               the REGION table.
---------------------------------------------------------------------

FUNCTION UPDATE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_REGION
   -- Purpose     : Takes in a region table record and deletes 
   --               from the REGION table.
---------------------------------------------------------------------

FUNCTION DELETE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
---------------------------------------------------------------------
   -- Function    : INSERT_DISTRICT
   -- Purpose     : Takes in a district table record and inserts all of
   --               its contents into the DISTRICT table.
---------------------------------------------------------------------
FUNCTION INSERT_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : UPDATE_DISTRICT
   -- Purpose     : Takes in a district table record and updates 
   --               the DISTRICT table.
---------------------------------------------------------------------
FUNCTION UPDATE_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_DISTRICT
   -- Purpose     : Takes in a district table record and deletes 
   --               from the DISTRICT table.
---------------------------------------------------------------------
FUNCTION DELETE_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION VALIDATE_IDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists             OUT   BOOLEAN,
                      I_hier_level      IN       VARCHAR2,
                      I_hier_tbl        IN       LOC_TBL)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: LOCK_STORE_HIERARCHY
-- Purpose      : This function will lock the STORE_HIERARCHY table
--                for all the stores under the hierarchy passed in
---------------------------------------------------------------------
FUNCTION LOCK_STORE_HIERARCHY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hier_level    IN     VARCHAR2,
                       I_hier_value    IN     NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: WH_COUNTRY_SUBQUERY
-- Purpose      : This function will add the country id to a warehouse
--                dynamic query.  It will create a subquery that references 
--                the ADDR and ADD_TYPE_MODULE tables.
---------------------------------------------------------------------

FUNCTION WH_COUNTRY_SUBQUERY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_subquery           IN OUT VARCHAR2,
                             I_country_bind_no     IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : CHECK_STORE_ADD_EXISTS
   -- Purpose     : This function will check for store records in the
   --               STORE_ADD table which are associated to a district.
---------------------------------------------------------------------
FUNCTION CHECK_STORE_ADD_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_district        IN       DISTRICT.DISTRICT%TYPE,
                                O_exists          IN OUT   BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_AREA_TL
   -- Purpose     : This function will deletes the area from area_tl table.
---------------------------------------------------------------------
FUNCTION DELETE_AREA_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_area            IN       AREA_TL.AREA%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------   
   -- Function    : DELETE_CHAIN_TL
   -- Purpose     : This function will deletes the chain from chain_tl table.
---------------------------------------------------------------------
FUNCTION DELETE_CHAIN_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_chain           IN       CHAIN_TL.CHAIN%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------   
   -- Function    : DELETE_DISTRICT_TL
   -- Purpose     : This function deletes district from district_tl table.
---------------------------------------------------------------------
FUNCTION DELETE_DISTRICT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_district        IN       DISTRICT_TL.DISTRICT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------   
   -- Function    : DELETE_REGION_TL
   -- Purpose     : This function deletes region from region_tl table.
---------------------------------------------------------------------
FUNCTION DELETE_REGION_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_region          IN       REGION_TL.REGION%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------   

END ORGANIZATION_SQL;
/