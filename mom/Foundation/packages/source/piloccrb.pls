
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY PACKITEM_LOC_CREATE_SQL AS
-------------------------------------------------------------------------------
FUNCTION CHECK_SKU_STATUS(O_error_message  IN OUT VARCHAR2,
                          O_inactive_ind   IN OUT BOOLEAN,
                          O_del_disc_ind   IN OUT BOOLEAN,
                          I_pack_no        IN     ITEM_MASTER.ITEM%TYPE,
                          I_loc            IN     ITEM_LOC.LOC%TYPE)
   return BOOLEAN is
   ---
   L_program        VARCHAR2(60):= 'PACKITEM_LOC_CREATE_SQL.CHECK_SKU_STATUS';
   L_fetch          VARCHAR2(1);
   ---
   cursor C_INACTIVE_STATUS is
      select 'x'
        from item_loc il, v_packsku_qty pq
       where il.item    = pq.item
         and pq.pack_no = I_pack_no
         and il.loc     = I_loc
         and il.status  = 'I';
   ---
   cursor C_DEL_DISC_STATUS is
      select 'x'
        from item_loc il, v_packsku_qty pq
       where il.item    = pq.item
         and pq.pack_no = I_pack_no
         and il.loc     = I_loc
         and il.status  in ('C', 'D');
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_PACK_NO', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_LOC', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_INACTIVE_STATUS',
                    'ITEM_LOC, V_PACKSKU_QTY',
                    'PACK_NO: '||I_pack_no||' LOC: '||to_char(I_loc));
   open C_INACTIVE_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_INACTIVE_STATUS',
                    'ITEM_LOC, V_PACKSKU_QTY',
                    'PACK_NO: '||I_pack_no||' LOC: '||to_char(I_loc));
   fetch C_INACTIVE_STATUS into L_fetch;
   if C_INACTIVE_STATUS%FOUND then
      O_inactive_ind := TRUE;
   else
      O_inactive_ind := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_INACTIVE_STATUS',
                    'ITEM_LOC, V_PACKSKU_QTY',
                    'PACK_NO: '||I_pack_no||' LOC: '||to_char(I_loc));
   close C_INACTIVE_STATUS;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_DEL_DISC_STATUS',
                    'ITEM_LOC, V_PACKSKU_QTY',
                    'PACK_NO: '||I_pack_no||' LOC: '||to_char(I_loc));
   open C_DEL_DISC_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_DEL_DISC_STATUS',
                    'ITEM_LOC, V_PACKSKU_QTY',
                    'PACK_NO: '||I_pack_no||' LOC: '||to_char(I_loc));
   fetch C_DEL_DISC_STATUS into L_fetch;
   if C_DEL_DISC_STATUS%FOUND then
      O_del_disc_ind := TRUE;
   else
      O_del_disc_ind := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_DEL_DISC_STATUS',
                    'ITEM_LOC, V_PACKSKU_QTY',
                    'PACK_NO: '||I_pack_no||' LOC: '||to_char(I_loc));
   close C_DEL_DISC_STATUS;
   ---
   return TRUE;      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_SKU_STATUS;   
-------------------------------------------------------------------------------
FUNCTION UPDATE_SKU_STATUS(O_error_message  IN OUT VARCHAR2,
                           I_pack_no        IN     ITEM_MASTER.ITEM%TYPE,
                           I_loc            IN     ITEM_LOC.LOC%TYPE,
                           I_status         IN     ITEM_LOC.STATUS%TYPE)
   return BOOLEAN as
   ---
   L_program       VARCHAR2(60)  := 'PACKITEM_LOC_CREATE_SQL.UPDATE_SKU_STATUS';
   L_vdate         DATE          := GET_VDATE;
   L_loc_type      ITEM_LOC.LOC_TYPE%TYPE;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_ITEM_LOC is
      select 'x'
        from item_loc il,
             item_master im,
             packitem pi
       where il.item     = pi.item
         and il.item     = im.item
         and im.pack_ind = 'N'
         and il.loc      = I_loc         
         and pi.pack_no  = I_pack_no
         and il.status  != I_status
         for update of il.status nowait;
   ---
   cursor C_GET_ITEMS is
      select il.item
        from item_loc il,
             item_master im,
             packitem pi
       where il.item     = im.item
         and il.item     = pi.item
         and im.pack_ind = 'N'
         and il.loc      = I_loc
         and pi.pack_no  = I_pack_no
         and il.status  != I_status;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_PACK_NO', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_LOC', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_status is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_STATUS', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    'PACK_NO: '||I_pack_no||' LOC: '||to_char(I_loc)||' STATUS: '||I_status);
   open C_LOCK_ITEM_LOC;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    'PACK_NO: '||I_pack_no||' LOC: '||to_char(I_loc)||' STATUS: '||I_status);
   close C_LOCK_ITEM_LOC;
   ---
   if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                   L_loc_type,
                                   I_loc) = FALSE then
      return FALSE;
   end if;
   ---
   update item_loc il
      set il.status               = I_status,
          il.last_update_id       = get_user,
          il.last_update_datetime = sysdate
    where il.loc     = I_loc
      and il.status != I_status
      and exists( select 'x'
                    from packitem pi
                   where pi.pack_no = I_pack_no
                     and pi.item    = il.item )
      and exists( select 'x'
                    from item_master im
                   where im.item     = il.item
                     and im.pack_ind = 'N' );
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
     O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                      'ITEM_LOC',
                                      to_char(I_loc),
                                      I_pack_no);
      return FALSE;
   when OTHERS then 
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
		      	 SQLERRM,
				 L_program,
			       to_char(SQLCODE));
      return FALSE;  
END UPDATE_SKU_STATUS;
-------------------------------------------------------------------------------
END;
/


