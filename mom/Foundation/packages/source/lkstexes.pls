
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LIKE_STORE_EXECUTE_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------
FUNCTION COPY_STORE_ITEMS(I_like_store	IN		store.store%TYPE,
                          I_new_store	IN		store.store%TYPE,
	                    O_error_message	IN OUT	RTK_ERRORS.RTK_TEXT%TYPE,
                          O_found		IN OUT	BOOLEAN)
	RETURN BOOLEAN;

end LIKE_STORE_EXECUTE_SQL;
/


