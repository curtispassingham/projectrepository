
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE EDIUK_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------
-- FUNCTION: SUPPLIER_EXISTS
-- Function      : This function validates that a record exists on the item_supplier
--                 table for the specified item and supplier. If a vpn or ref_item is 
--                 passed in then the item is retrieved.
-- Called by     : EDIUKSAV.PC
-------------------------------------------------------------------
FUNCTION SUPPLIER_EXISTS(O_error_message IN OUT VARCHAR2,
                         I_supplier      IN     SUPS.SUPPLIER%TYPE,
                         I_item_type     IN     VARCHAR2,
                         I_item_id       IN     VARCHAR2,
                         O_exist         IN OUT BOOLEAN)
RETURN BOOLEAN;

------------------------------------------------------------------
-- FUNCTION: SUP_AVAIL_INSERTS
-- Function: This function updates/inserts a record on the sup_avail table.
------------------------------------------------------------------
FUNCTION SUP_AVAIL_INSERTS(I_supplier      IN     SUPS.SUPPLIER%TYPE,
                           I_avail_qty     IN     NUMBER,
                           O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------
END EDIUK_SQL;
/
