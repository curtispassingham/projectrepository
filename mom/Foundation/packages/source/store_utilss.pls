CREATE OR REPLACE PACKAGE STORE_UTILS AUTHID CURRENT_USER AS 

----------------------------------------------------------------
-- Function Name: check_country
-- Purpose	: Calls COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB internally
----------------------------------------------------------------
FUNCTION check_country(p_store IN NUMBER, 
					   p_store_type IN VARCHAR2) 
	RETURN VARCHAR2;

----------------------------------------------------------------
-- Function Name: CHECK_L10N
-- Purpose	: Calls L10N_ENTITY_SQL.BUILD_L10N_EXT_KEY internally
----------------------------------------------------------------	
FUNCTION CHECK_L10N(p_store      IN NUMBER,
					p_store_type IN VARCHAR2,
					p_pm_mode    IN VARCHAR2)
    RETURN VARCHAR2;

----------------------------------------------------------------
-- Function Name: check_pricing_zone_id
-- Purpose	: Calls PM_RETAIL_API_SQL.GET_PRICING_ZONE_ID internally
----------------------------------------------------------------	
FUNCTION check_pricing_zone_id(p_store      IN NUMBER,
							   p_price_zone IN ITEM_LOC.LOC%TYPE)
	RETURN VARCHAR2;
	
----------------------------------------------------------------
-- Function Name: get_exists_pricing_zone_id
-- Purpose	: Calls PM_RETAIL_API_WRP.GET_PRICING_ZONE_ID internally
----------------------------------------------------------------  
FUNCTION get_exists_pricing_zone_id(o_error_message IN OUT varchar2,
									o_exists        IN OUT number,
									p_price_zone    IN ITEM_LOC.LOC%TYPE)
	RETURN NUMBER;

----------------------------------------------------------------
-- Function Name: validate_district
-- Purpose	: validation for district
----------------------------------------------------------------  
FUNCTION validate_district(o_error_message IN OUT varchar2,
						   i_district      IN number,
						   i_store         IN number)
	RETURN NUMBER;

----------------------------------------------------------------
-- Function Name: cancel_store
-- Purpose	: undo changes to the store or remove new store data
----------------------------------------------------------------  
FUNCTION cancel_store(i_store in number)
	RETURN VARCHAR2;

----------------------------------------------------------------
-- Procedure Name: set_savepoint_pregc
-- Purpose	: set savepoint pregc in db
----------------------------------------------------------------  
procedure set_savepoint_pregc;

----------------------------------------------------------------
-- Procedure Name: rollback_pregc
-- Purpose	: rollback to savepoint pregc in db
----------------------------------------------------------------  
procedure rollback_pregc;

END STORE_UTILS;
/
