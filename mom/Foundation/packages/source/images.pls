
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE IMAGE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
--- FUNCTION: DEFAULT_DOWN
--- PURPOSE:  This function associates item images to all children down
---           to the transaction level.
-----------------------------------------------------------------------------
FUNCTION DEFAULT_DOWN (O_error_message   IN OUT   VARCHAR2,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Function Name : CHECK_DUP_PRIORITY
-- Purpose       : This function checks if the priority entered by the user is
--                 already present in item_image table.
-----------------------------------------------------------------------------
FUNCTION CHECK_DUP_PRIORITY (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists             IN OUT   BOOLEAN,
                             I_item               IN       ITEM_IMAGE.ITEM%TYPE,
                             I_image_name         IN       ITEM_IMAGE.IMAGE_NAME%TYPE,
                             I_display_priority   IN       ITEM_IMAGE.DISPLAY_PRIORITY%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Function Name : CHECK_IMAGE_NAME
-- Purpose       : This function checks if the image name entered by the user
--                 is already present in item_image table.
-----------------------------------------------------------------------------
FUNCTION CHECK_IMAGE_NAME (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_item            IN       ITEM_IMAGE.ITEM%TYPE,
                           I_image_name      IN       ITEM_IMAGE.IMAGE_NAME%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Function Name : GET_PRIMARY_IMAGE
-- Purpose       : This function retrieves the primary image record present
--                 if exists and associated to an item from item_image table.
-----------------------------------------------------------------------------
FUNCTION GET_PRIMARY_IMAGE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            O_image_name      IN OUT   ITEM_IMAGE.IMAGE_NAME%TYPE,
                            I_item            IN       ITEM_IMAGE.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Function Name : DELETE_ITEM_IMAGE_TL
-- Purpose       : This function deletes  from ITEM_IMAGE_TL table.
-----------------------------------------------------------------------------
FUNCTION DELETE_ITEM_IMAGE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN  ITEM_IMAGE_TL.ITEM%TYPE,
                              I_image_name      IN   ITEM_IMAGE_TL.IMAGE_NAME%TYPE )
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
END;
/
