CREATE OR REPLACE PACKAGE BODY RMSMFM_SUPPLIER AS

LP_system_options_rec   SYSTEM_OPTIONS%ROWTYPE := NULL;

            /*** API State Variables ***/
PROCEDURE GETNXT(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 O_message_type OUT VARCHAR2,
                 O_supplier     OUT sups.supplier%TYPE,
                 O_addr_seq_no  OUT addr.seq_no%TYPE,
                 O_addr_type    OUT addr.addr_type%TYPE,
                 O_message      OUT CLOB);

PROCEDURE_ERROR exception;

FUNCTION CREATE_PREVIOUS(O_status    OUT VARCHAR2,
                         O_text      OUT VARCHAR2,
                         I_queue_rec IN supplier_mfqueue%ROWTYPE)
RETURN BOOLEAN;

PROCEDURE CLEAN_QUEUE(O_status    OUT VARCHAR2,
                      O_text      OUT VARCHAR2,
                      I_queue_rec IN OUT supplier_mfqueue%ROWTYPE);

FUNCTION CAN_CREATE(O_status    OUT VARCHAR2,
                    O_text      OUT VARCHAR2,
                    I_queue_rec IN supplier_mfqueue%ROWTYPE)
RETURN BOOLEAN;

PROCEDURE MAKE_CREATE(O_status    OUT VARCHAR2,
                      O_text      OUT VARCHAR2,
                      O_msg       OUT CLOB,
                      I_queue_rec IN supplier_mfqueue%ROWTYPE);

PROCEDURE MAKE_CREATE_POU(O_status OUT VARCHAR2,
                          O_text   OUT VARCHAR2,
                          O_msg       OUT CLOB,
                          I_queue_rec IN  supplier_mfqueue%ROWTYPE);

PROCEDURE DELETE_QUEUE_REC(O_status    OUT VARCHAR2,
                           O_text      OUT VARCHAR2,
                           I_seq_no IN supplier_mfqueue.seq_no%TYPE);

PROCEDURE REPLACE_QUEUE_SUP(O_status    OUT VARCHAR2,
                            O_text      OUT VARCHAR2,
                            I_rec  in out supplier_mfqueue%rowtype);

PROCEDURE REPLACE_QUEUE_ADR(O_status    OUT VARCHAR2,
                            O_text      OUT VARCHAR2,
                            I_rec  in out supplier_mfqueue%ROWTYPE);

PROCEDURE CHECK_STATUS(I_status_code IN VARCHAR2);

           /*** Public Program Bodies***/

--------------------------------------------------------------------------------
PROCEDURE ADDTOQ(I_message_type   IN  VARCHAR2,
                 I_supplier       IN  sups.supplier%TYPE,
                 I_addr_seq_no    IN  addr.seq_no%TYPE,
                 I_addr_type      IN  addr.addr_type%TYPE,
                 I_ret_allow_ind  IN  VARCHAR2,
                 I_org_unit_id    IN  VARCHAR2,
                 I_message        IN  CLOB,
                 O_status         OUT VARCHAR2,
                 O_text           OUT VARCHAR2)
AS

BEGIN
   -- write the message out.
   insert into supplier_mfqueue(seq_no,
                                pub_status,
                                message_type,
                                supplier,
                                addr_seq_no,
                                addr_type,
                                ret_allow_ind,
                                org_unit_id,
                                message)
   (select supplier_mfsequence.nextval,
           'U',
           I_message_type,
           I_supplier,
           I_addr_seq_no,
           I_addr_type,
           I_ret_allow_ind,
           I_org_unit_id,
           I_message
      from dual);
   ---
   O_status := API_CODES.SUCCESS;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                               'RMSMFM_SUPPLIER.ADDTOQ');

END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code   OUT VARCHAR2,
                 O_error_msg     OUT VARCHAR2,
                 O_message_type  OUT VARCHAR2,
                 O_message       OUT CLOB,
                 O_supplier     OUT sups.supplier%TYPE,
                 O_addr_seq_no  OUT addr.seq_no%TYPE,
                 O_addr_type    OUT addr.addr_type%TYPE)
AS

BEGIN
   GETNXT(O_status_code,
          O_error_msg,
          O_message_type,
          O_supplier,
          O_addr_seq_no,
          O_addr_type,
          O_message);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                               'RMSMFM_SUPPLIER.GETNXT');
END GETNXT;

           /*** Private Program Bodies***/
-------------------------------------------------------------------------------------
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 O_message_type OUT VARCHAR2,
                 O_supplier     OUT sups.supplier%TYPE,
                 O_addr_seq_no  OUT addr.seq_no%TYPE,
                 O_addr_type    OUT addr.addr_type%TYPE,
                 O_message      OUT CLOB)
AS

   L_queue_rec supplier_mfqueue%ROWTYPE;
   ---
   cursor C_QUEUE is
      select *
        from supplier_mfqueue
       where pub_status = 'U'
       order by seq_no;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_text,
                                            LP_system_options_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   open C_QUEUE;
   ---
   LOOP
      fetch C_QUEUE into L_queue_rec;
      ---
      if C_QUEUE%NOTFOUND then
         O_status := API_CODES.NO_MSG;
         exit;
      end if;
      ---
      if L_queue_rec.message_type != VEND_ADD then
         if CREATE_PREVIOUS(O_status,
                            O_text,
                            L_queue_rec) then
            CHECK_STATUS(O_status);
            ---
            CLEAN_QUEUE(O_status,
                        O_text,
                        L_queue_rec);
            CHECK_STATUS(O_status);
            ---
            if CAN_CREATE(O_status,
                          O_text,
                          L_queue_rec) then
               CHECK_STATUS(O_status);
               ---
               MAKE_CREATE(O_status,
                           O_text,
                           O_message,
                           L_queue_rec);
               CHECK_STATUS(O_status);
               ---
               O_message_type := VEND_ADD;
               O_supplier     := L_queue_rec.supplier;
               O_status       := API_CODES.NEW_MSG;
               exit;
            end if;
         else
            DELETE_QUEUE_REC(O_status,
                             O_text,
                             L_queue_rec.seq_no);
            CHECK_STATUS(O_status);
            ---
            -- If the message if Org Unit related and is not part of a VendorCre,
            -- then attach the message to a VendorDesc payload.
            if L_queue_rec.message_type = OU_CRE or
               L_queue_rec.message_type = OU_DEL then
               MAKE_CREATE_POU(O_status,
                               O_text,
                               O_message,
                               L_queue_rec);
               CHECK_STATUS(O_status);
               ---
               O_message_type := L_queue_rec.message_type;
               O_supplier     := L_queue_rec.supplier;
               O_status       := API_CODES.NEW_MSG;
               exit;
            end if;

            O_message := L_queue_rec.message;
            O_message_type := L_queue_rec.message_type;
            O_supplier := L_queue_rec.supplier;
            O_status := API_CODES.NEW_MSG;
            ---
            exit;
         end if;
      else
         UPDATE supplier_mfqueue
         set pub_status = 'N'
         where seq_no = L_queue_rec.seq_no;
      end if;
   END LOOP;
   ---
   close C_QUEUE;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                               'RMSMFM_SUPPLIER.GETNXT');
END GETNXT;
-------------------------------------------------------------------------------------
FUNCTION CREATE_PREVIOUS(O_status    OUT VARCHAR2,
                         O_text      OUT VARCHAR2,
                         I_queue_rec IN supplier_mfqueue%ROWTYPE)
   RETURN BOOLEAN IS

   cursor C_CREATE_PREV is
      select 'x'
        from supplier_mfqueue
       where message_type = VEND_ADD
         and supplier = I_queue_rec.supplier
         and seq_no <= I_queue_rec.seq_no
         and pub_status = 'N';

   L_create_found BOOLEAN;
   L_dummy        VARCHAR2(1);

BEGIN
   open C_CREATE_PREV;
   fetch C_CREATE_PREV into L_dummy;
   ---
   L_create_found := C_CREATE_PREV%FOUND;
   ---
   close C_CREATE_PREV;
   ---
   return L_create_found;

EXCEPTION
   when others then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SUPPLIER.CREATE_PREVIOUS');
      RETURN TRUE;

END CREATE_PREVIOUS;
-------------------------------------------------------------------------------------
PROCEDURE CLEAN_QUEUE(O_status OUT VARCHAR2,
                      O_text   OUT VARCHAR2,
                      I_queue_rec IN OUT supplier_mfqueue%ROWTYPE) IS
   cre_root  xmldom.DOMElement;
   test      NUMBER(1) := 0;

BEGIN

   if I_queue_rec.message_type = VEND_DEL then
      delete from supplier_mfqueue
       where supplier = I_queue_rec.supplier
         and seq_no <= I_queue_rec.seq_no;

   elsif I_queue_rec.message_type = ADDR_DEL then
      delete from supplier_mfqueue
       where supplier = I_queue_rec.supplier
         and addr_type = I_queue_rec.addr_type
         and addr_seq_no = I_queue_rec.addr_seq_no
         and seq_no <= I_queue_rec.seq_no;

   elsif I_queue_rec.message_type = OU_DEL then
      delete from supplier_mfqueue
       where supplier = I_queue_rec.supplier
         and lower(message_type) like 'vendorou%'
         and org_unit_id = I_queue_rec.org_unit_id
         and seq_no <= I_queue_rec.seq_no;

   elsif I_queue_rec.message_type = VEND_UPD then
      REPLACE_QUEUE_SUP(O_status,
                        O_text,
                        I_queue_rec);
      CHECK_STATUS(O_status);
      ---
      DELETE_QUEUE_REC(O_status,
                       O_text,
                       I_queue_rec.seq_no);
      CHECK_STATUS(O_status);

   elsif I_queue_rec.message_type = ADDR_UPD then
      ---
      REPLACE_QUEUE_ADR(O_status,
                        O_text,
                        I_queue_rec);
      CHECK_STATUS(O_status);
      ---
      DELETE_QUEUE_REC(O_status,
                       O_text,
                       I_queue_rec.seq_no);
      CHECK_STATUS(O_status);

   elsif I_queue_rec.message_type = ADDR_CRE then
       update supplier_mfqueue
       set pub_status = 'N'
       where seq_no = I_queue_rec.seq_no;

   elsif I_queue_rec.message_type = OU_CRE then
       update supplier_mfqueue
       set pub_status = 'N'
       where seq_no = I_queue_rec.seq_no;
   end if;

EXCEPTION
   when others then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SUPPLIER.CLEAN_QUEUE');
END CLEAN_QUEUE;
-------------------------------------------------------------------------------------
FUNCTION CAN_CREATE(O_status OUT VARCHAR2,
                    O_text   OUT VARCHAR2,
                    I_queue_rec IN supplier_mfqueue%ROWTYPE)
   RETURN BOOLEAN IS
   L_addr_type  addr.addr_type%TYPE;
   L_can_create    BOOLEAN := FALSE;
   type_03_found   BOOLEAN := FALSE;
   type_04_found   BOOLEAN := FALSE;
   type_05_found   BOOLEAN := FALSE;
   met_03_need     BOOLEAN := FALSE;
   met_04_need     BOOLEAN := FALSE;
   met_05_need     BOOLEAN := FALSE;
   pou_need        BOOLEAN := FALSE;
   L_dummy         VARCHAR(1);

   cursor C_GET_ADDR_TYPE_CUR is
      select addr_type
        from supplier_mfqueue
       where supplier = I_queue_rec.supplier
         and message_type = ADDR_CRE
         and seq_no <= I_queue_rec.seq_no;

   cursor C_GET_POU_CUR is
      select 'x'
        from supplier_mfqueue
       where supplier = I_queue_rec.supplier
         and message_type = OU_CRE
         and seq_no <= I_queue_rec.seq_no;


BEGIN

   FOR addr_type_rec IN C_GET_ADDR_TYPE_CUR
   LOOP
      if addr_type_rec.addr_type = '03' then
         type_03_found := TRUE;
      end if;
      ---
      if addr_type_rec.addr_type = '04' then
         type_04_found := TRUE;
      end if;
      ---
      if addr_type_rec.addr_type = '05' then
         type_05_found := TRUE;
      end if;
   END LOOP;
   -- If returns are allowed for the supplier, then a returns address is required.
   if I_queue_rec.ret_allow_ind = 'Y' then
      -- Need a type 3
      if type_03_found then
         met_03_need := TRUE;
      end if;
   else
      met_03_need := true;
   end if;
   ---
   met_05_need := TRUE;   -- Formerly used when invc_match_ind existed (now removed)
   ---
   if type_04_found then
      met_04_need := TRUE;
   end if;
   ---
   -- If org units are in use then a partner_org_unit record is required.
   if LP_system_options_rec.org_unit_ind = 'Y' then
      open C_GET_POU_CUR;
      fetch C_GET_POU_CUR into L_dummy;
      ---
      pou_need := C_GET_POU_CUR%FOUND;
      close C_GET_POU_CUR;
   else
      pou_need := TRUE;
   end if;
   ---
   if met_03_need then
      if met_04_need then
         if met_05_need then
            if pou_need then
              L_can_create := TRUE;
            end if;
         end if;
      end if;
   end if;
   ---
   return L_can_create;

EXCEPTION
   when others then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SUPPLIER.CAN_CREATE');
      RETURN TRUE;
END CAN_CREATE;
------------------------------------------------------------------------------------------\
PROCEDURE MAKE_CREATE(O_status OUT VARCHAR2,
                      O_text   OUT VARCHAR2,
                      O_msg       OUT CLOB,
                      I_queue_rec IN  supplier_mfqueue%ROWTYPE) IS

   cre_root   xmldom.DOMElement;
   hdr_root   xmldom.DOMElement;
   upd_root   xmldom.DOMElement;
   hdr_elem   xmldom.DOMElement;
   addr_elem  xmldom.DOMElement;
   pou_elem   xmldom.DOMElement;
   L_sup_rec  supplier_mfqueue%ROWTYPE;

   cursor C_GET_SUP_MESSAGE is
      select *
        from supplier_mfqueue
       where message_type = VEND_ADD
         and supplier = I_queue_rec.supplier
         and seq_no < I_queue_rec.seq_no;

   cursor C_GET_ADR_MESSAGES is
      select message
        from supplier_mfqueue
       where lower(message_type) like ('vendoraddr%')
         and supplier = I_queue_rec.supplier
         and seq_no <= I_queue_rec.seq_no
         order by seq_no asc;

   cursor C_GET_POU_MESSAGES is
      select message
        from supplier_mfqueue
       where lower(message_type) like ('vendorou%')
         and supplier = I_queue_rec.supplier
         and seq_no <= I_queue_rec.seq_no
         order by seq_no asc;

BEGIN

   open C_GET_SUP_MESSAGE;
   fetch C_GET_SUP_MESSAGE into L_sup_rec;
   close C_GET_SUP_MESSAGE;
   ---

   if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                     O_text,
                                     cre_root,
                                     VEND_DESC_MSG) then
      return;
   end if;

   -- Read the Vendor Header Root into a DOM
   hdr_root := API_LIBRARY.readRoot(L_sup_rec.message,
                                    VEND_HDR_DESC_MSG);

   -- Create the VendorHdrDesc subnode on the root
   hdr_elem := rib_xml.addElement(cre_root,
                                  VEND_HDR_DESC_MSG);
   ---
   rib_xml.addElementContents(hdr_elem,
                              hdr_root);
   ---
   FOR message_rec IN C_GET_ADR_MESSAGES LOOP
      upd_root := API_LIBRARY.readRoot(message_rec.message,
                                       VEND_ADDR_DESC_MSG);
      ---
      addr_elem := rib_xml.addElement(cre_root,
                                      VEND_ADDR_DESC_MSG);
      ---
      rib_xml.addElementContents(addr_elem,
                                 upd_root);
   END LOOP;
   ---
   FOR message_rec IN C_GET_POU_MESSAGES LOOP
      upd_root := API_LIBRARY.readRoot(message_rec.message,
                                       VEND_OU_DESC_MSG);
      ---
      pou_elem := rib_xml.addElement(cre_root,
                                     VEND_OU_DESC_MSG);
      ---
      rib_xml.addElementContents(pou_elem,
                                 upd_root);
   END LOOP;
   ---
   if API_LIBRARY.WRITE_DOCUMENT(O_status,
                                 O_text,
                                 O_msg,
                                 cre_root) = FALSE then
      return;
   end if;
   ---
   delete from supplier_mfqueue
    where supplier = I_queue_rec.supplier
      and seq_no <= I_queue_rec.seq_no;

EXCEPTION
   when others then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SUPPLIER.MAKE_CREATE');

END MAKE_CREATE;
------------------------------------------------------------------------------------------\
PROCEDURE MAKE_CREATE_POU(O_status OUT VARCHAR2,
                          O_text   OUT VARCHAR2,
                          O_msg       OUT CLOB,
                          I_queue_rec IN  supplier_mfqueue%ROWTYPE) IS

   cre_root     xmldom.DOMElement;
   hdr_root     xmldom.DOMElement;
   upd_root     xmldom.DOMElement;
   hdr_elem     xmldom.DOMElement;
   pou_elem     xmldom.DOMElement;
   L_sup_rec    sups%ROWTYPE;
   L_msg        CLOB;

   cursor C_GET_SUP_INFO is
      select *
        from sups
       where supplier=I_queue_rec.supplier;

BEGIN

   open C_GET_SUP_INFO;
   fetch C_GET_SUP_INFO into L_sup_rec;
   close C_GET_SUP_INFO;

   if I_queue_rec.message_type = OU_CRE then
      -- create a supplier xml message
      supplier_xml.build_supplier(L_sup_rec,
                                 'C',     -- event
                                 L_msg,   -- output xml
                                 O_status,
                                 O_text);

      ---
     if O_status = API_CODES.UNHANDLED_ERROR then
        raise PROGRAM_ERROR;
     end if;
     ---
     -- create the supplier CLOB containing only the supplier number
     if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                       O_text,
                                       cre_root,
                                       VEND_DESC_MSG) then
        return;
     end if;

     -- Read the Vendor Header Root into a DOM
     hdr_root := API_LIBRARY.readRoot(L_msg,
                                      VEND_HDR_DESC_MSG);

     -- Create the VendorHdrDesc subnode on the root
     hdr_elem := rib_xml.addElement(cre_root,
                                    VEND_HDR_DESC_MSG);
     ---
     rib_xml.addElementContents(hdr_elem,
                                hdr_root);
     ---
     -- Add the org unit node
     upd_root := API_LIBRARY.readRoot(I_queue_rec.message,
                                      VEND_OU_DESC_MSG);
     ---
     pou_elem := rib_xml.addElement(cre_root,
                                    VEND_OU_DESC_MSG);
     ---
     rib_xml.addElementContents(pou_elem,
                                upd_root);
     ---
     if API_LIBRARY.WRITE_DOCUMENT(O_status,
                                   O_text,
                                   O_msg,
                                   cre_root) = FALSE then
        return;
     end if;

   else
      -- create a supplier xml message
      supplier_xml.build_supplier(L_sup_rec,
                                  'D',     -- event
                                  L_msg,   -- output xml
                                  O_status,
                                  O_text);

      ---
      if O_status = API_CODES.UNHANDLED_ERROR then
         raise PROGRAM_ERROR;
      end if;
      ---
      -- create the supplier CLOB containing only the supplier number
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        cre_root,
                                        VEND_REF_MSG) then
         return;
      end if;
      -- Read the Vendor Header Root into a DOM
      hdr_root := API_LIBRARY.readRoot(L_msg,
                                       VEND_REF_MSG);

      ---
      rib_xml.addElementContents(cre_root,
                                 hdr_root);
      ---

      -- Add the org unit node
      upd_root := API_LIBRARY.readRoot(I_queue_rec.message,
                                       VEND_OU_REF_MSG);
      ---
      pou_elem := rib_xml.addElement(cre_root,
                                     VEND_OU_REF_MSG);
      ---
      rib_xml.addElementContents(pou_elem,
                                 upd_root);
      ---
      if API_LIBRARY.WRITE_DOCUMENT(O_status,
                                    O_text,
                                    O_msg,
                                    cre_root) = FALSE then
         return;
      end if;
   end if;

EXCEPTION
   when others then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SUPPLIER.MAKE_CREATE_POU');

END MAKE_CREATE_POU;
------------------------------------------------------------------------------------------\
PROCEDURE DELETE_QUEUE_REC(O_status OUT VARCHAR2,
                           O_text   OUT VARCHAR2,
                           I_seq_no IN supplier_mfqueue.seq_no%TYPE) is

BEGIN
   delete from supplier_mfqueue
    where seq_no = I_seq_no;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SUPPLIER.DELETE_QUEUE_REC');
END DELETE_QUEUE_REC;
------------------------------------------------------------------------------------------\
-- replace the message data in a sup queue record with a new message.
PROCEDURE REPLACE_QUEUE_SUP(O_status    OUT VARCHAR2,
                            O_text      OUT VARCHAR2,
                            I_rec  in out supplier_mfqueue%rowtype)
IS
   PROGRAM_ERROR   EXCEPTION;
   ---
   cursor locker is
   select 1 from supplier_mfqueue
    where pub_status = 'N'
      and seq_no < I_rec.seq_no
      and message_type = VEND_ADD
      and supplier = I_rec.supplier
      for update nowait;

BEGIN

   -- in addition to all the normal reasons you need to do row locking, Oracle
   -- won't let you modify the contents of a LOB unless you have it locked.
   open locker;
   close locker;
   ---
   update supplier_mfqueue
      set message = I_rec.message
    where pub_status = 'N'
      and seq_no < I_rec.seq_no
      and message_type = VEND_ADD
      and supplier = I_rec.supplier;

EXCEPTION
   when others then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SUPPLIER.REPLACE_QUEUE_SUP');
END REPLACE_QUEUE_SUP;
------------------------------------------------------------------------------------------\
-- replace the message data in a adr queue record with a new message.
PROCEDURE REPLACE_QUEUE_ADR(O_status    OUT VARCHAR2,
                            O_text      OUT VARCHAR2,
                            I_rec  in out supplier_mfqueue%rowtype)
IS
   PROGRAM_ERROR   EXCEPTION;
   ---
   cursor locker is
   select 1 from supplier_mfqueue
    where seq_no < I_rec.seq_no
      and message_type = ADDR_CRE
      and addr_type = I_rec.addr_type
      and addr_seq_no = I_rec.addr_seq_no
      and supplier = I_rec.supplier
      for update nowait;

BEGIN
   -- in addition to all the normal reasons you need to do row locking, Oracle
   -- won't let you modify the contents of a LOB unless you have it locked.
   open locker;
   close locker;
   ---
   update supplier_mfqueue
      set message = I_rec.message
    where pub_status = 'N'
      and seq_no < I_rec.seq_no
      and message_type = ADDR_CRE
      and addr_type = I_rec.addr_type
      and addr_seq_no = I_rec.addr_seq_no
      and supplier = I_rec.supplier;

EXCEPTION
   when others then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SUPPLIER.REPLACE_QUEUE_ADR');
END REPLACE_QUEUE_ADR;
-------------------------------------------------------------------------------------
PROCEDURE CHECK_STATUS(I_status_code IN VARCHAR2)
IS

   -- CHECK_STATUS raises an exception if the status code is set to 'E'rror.
   -- This should be called immediately after calling a procedure
   -- that sets the status code.  Any procedure that calls CHECK_STATUS
   -- must have its own exception handling section.

   PROGRAM_ERROR   EXCEPTION;

BEGIN

   if I_status_code = API_CODES.UNHANDLED_ERROR then
      raise PROGRAM_ERROR;
   end if;

END CHECK_STATUS;
--------------------------------------------------------------------------------
END RMSMFM_SUPPLIER;
/
