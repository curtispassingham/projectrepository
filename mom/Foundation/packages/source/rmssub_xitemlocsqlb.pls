CREATE OR REPLACE PACKAGE BODY RMSSUB_XITEMLOC_SQL AS

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------

TYPE ITEM_TABLE_TYPE IS TABLE OF ITEM_MASTER.ITEM%TYPE
INDEX BY BINARY_INTEGER;
TYPE ITEM_STATUS_TABLE_TYPE IS TABLE OF ITEM_MASTER.STATUS%TYPE
INDEX BY BINARY_INTEGER;
TYPE ITEM_LEVEL_TABLE_TYPE IS TABLE OF ITEM_MASTER.ITEM_LEVEL%TYPE
INDEX BY BINARY_INTEGER;
TYPE TRAN_LEVEL_TABLE_TYPE IS TABLE OF ITEM_MASTER.TRAN_LEVEL%TYPE
INDEX BY BINARY_INTEGER;
TYPE LOC_TABLE_TYPE IS TABLE OF ITEM_LOC.LOC%TYPE
INDEX BY BINARY_INTEGER;
TYPE LOC_TYPE_TABLE_TYPE IS TABLE OF ITEM_LOC.LOC_TYPE%TYPE
INDEX BY BINARY_INTEGER;
TYPE PRIMARY_SUPP_TABLE_TYPE IS TABLE OF ITEM_LOC.PRIMARY_SUPP%TYPE
INDEX BY BINARY_INTEGER;
TYPE PRIMARY_CNTRY_TABLE_TYPE IS TABLE OF ITEM_LOC.PRIMARY_CNTRY%TYPE
INDEX BY BINARY_INTEGER;
TYPE STATUS_TABLE_TYPE IS TABLE OF ITEM_LOC.STATUS%TYPE
INDEX BY BINARY_INTEGER;
TYPE LOCAL_ITEM_DESC_TABLE_TYPE IS TABLE OF ITEM_LOC.LOCAL_ITEM_DESC%TYPE
INDEX BY BINARY_INTEGER;
TYPE LOCAL_SHORT_DESC_TABLE_TYPE IS TABLE OF ITEM_LOC.LOCAL_SHORT_DESC%TYPE
INDEX BY BINARY_INTEGER;
TYPE PRIMARY_VARIANT_TABLE_TYPE IS TABLE OF ITEM_LOC.PRIMARY_VARIANT%TYPE
INDEX BY BINARY_INTEGER;
TYPE UNIT_RETAIL_TABLE_TYPE IS TABLE OF ITEM_LOC.UNIT_RETAIL%TYPE
INDEX BY BINARY_INTEGER;
TYPE TI_TABLE_TYPE IS TABLE OF ITEM_LOC.TI%TYPE
INDEX BY BINARY_INTEGER;
TYPE HI_TABLE_TYPE IS TABLE OF ITEM_LOC.HI%TYPE
INDEX BY BINARY_INTEGER;
TYPE STORE_ORD_MULT_TABLE_TYPE IS TABLE OF ITEM_LOC.STORE_ORD_MULT%TYPE
INDEX BY BINARY_INTEGER;
TYPE DAILY_WASTE_PCT_TABLE_TYPE IS TABLE OF ITEM_LOC.DAILY_WASTE_PCT%TYPE
INDEX BY BINARY_INTEGER;
TYPE TAXABLE_IND_TABLE_TYPE IS TABLE OF ITEM_LOC.TAXABLE_IND%TYPE
INDEX BY BINARY_INTEGER;
TYPE MEAS_OF_EACH_TABLE_TYPE IS TABLE OF ITEM_LOC.MEAS_OF_EACH%TYPE
INDEX BY BINARY_INTEGER;
TYPE MEAS_OF_PRICE_TABLE_TYPE IS TABLE OF ITEM_LOC.MEAS_OF_PRICE%TYPE
INDEX BY BINARY_INTEGER;
TYPE UOM_OF_PRICE_TABLE_TYPE IS TABLE OF ITEM_LOC.UOM_OF_PRICE%TYPE
INDEX BY BINARY_INTEGER;
TYPE SELLING_UNIT_RETAIL_TABLE_TYPE IS TABLE OF
ITEM_LOC.SELLING_UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
TYPE SELLING_UOM_TABLE_TYPE IS TABLE OF ITEM_LOC.SELLING_UOM%TYPE
INDEX BY BINARY_INTEGER;
TYPE PRIMARY_COST_PACK_TABLE_TYPE IS TABLE OF ITEM_LOC.PRIMARY_COST_PACK%TYPE
INDEX BY BINARY_INTEGER;
TYPE PROCESS_CHILDREN_TABLE_TYPE IS TABLE OF VARCHAR2(1)
INDEX BY BINARY_INTEGER;
TYPE RECEIVE_AS_TYPE_TABLE_TYPE IS TABLE OF ITEM_LOC.RECEIVE_AS_TYPE%TYPE
INDEX BY BINARY_INTEGER;
TYPE RANGED_IND_TABLE_TYPE IS TABLE OF ITEM_LOC.RANGED_IND%TYPE
INDEX BY BINARY_INTEGER;
TYPE SOURCE_METHOD_TABLE_TYPE IS TABLE OF ITEM_LOC.SOURCE_METHOD%TYPE
INDEX BY BINARY_INTEGER;
TYPE SOURCE_WH_TABLE_TYPE IS TABLE OF ITEM_LOC.SOURCE_WH%TYPE
INDEX BY BINARY_INTEGER;

TYPE UIN_TYPE_TABLE_TYPE IS TABLE OF ITEM_LOC.UIN_TYPE%TYPE
INDEX BY BINARY_INTEGER;
TYPE UIN_LABEL_TABLE_TYPE IS TABLE OF ITEM_lOC.UIN_LABEL%TYPE
INDEX BY BINARY_INTEGER;
TYPE CAPTURE_TIME_TABLE_TYPE IS TABLE OF ITEM_LOC.CAPTURE_TIME%TYPE
INDEX BY BINARY_INTEGER;
TYPE EXT_UIN_IND_TABLE_TYPE IS TABLE OF ITEM_LOC.EXT_UIN_IND%TYPE
INDEX BY BINARY_INTEGER;

--------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
--------------------------------------------------------------------------------

FUNCTION UPDATE_ITEM_LOC(O_error_message      IN OUT   VARCHAR2,
                         I_item               IN       ITEM_MASTER.ITEM%TYPE,
                         I_hier_level         IN       VARCHAR2,
                         I_hier_value         IN       NUMBER,
                         I_primary_supp       IN       ITEM_LOC.PRIMARY_SUPP%TYPE,
                         I_primary_cntry      IN       ITEM_LOC.PRIMARY_CNTRY%TYPE,
                         I_local_item_desc    IN       ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                         I_status             IN       ITEM_LOC.STATUS%TYPE,
                         I_store_ord_mult     IN       ITEM_LOC.STORE_ORD_MULT%TYPE,
                         I_receive_as_type    IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_taxable_ind        IN       ITEM_LOC.TAXABLE_IND%TYPE,
                         I_local_short_desc   IN       ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                         I_daily_waste_pct    IN       ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                         I_source_method      IN       ITEM_LOC.SOURCE_METHOD%TYPE,
                         I_source_wh          IN       ITEM_LOC.SOURCE_WH%TYPE,
                         I_ti                 IN       ITEM_LOC.TI%TYPE,
                         I_hi                 IN       ITEM_LOC.HI%TYPE,
                         I_uin_type           IN       ITEM_LOC.UIN_TYPE%TYPE  DEFAULT NULL,
                         I_uin_label          IN       ITEM_lOC.UIN_LABEL%TYPE  DEFAULT NULL,
                         I_capture_time       IN       ITEM_LOC.CAPTURE_TIME%TYPE  DEFAULT NULL,
                         I_ext_uin_ind        IN       ITEM_LOC.EXT_UIN_IND%TYPE  DEFAULT NULL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------

FUNCTION PERSIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message       IN     "RIB_XItemLocDesc_REC",
                 I_message_type  IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XITEMLOC_SQL.PERSIST';
   L_input           NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
   L_event_run_type         COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;
      
   cursor C_GET_RUN_TYPE is
      select event_run_type 
        from cost_event_run_type_config
       where event_type='NIL';
   L_source_method   ITEM_LOC.SOURCE_METHOD%TYPE;

   cursor C_GET_INPUT IS
      select I_message.item,
             NULL,         --item_parent,
             NULL,         --item_grandparent,
             NULL,         --short_desc,
             NULL,         --dept,
             NULL,         --class,
             NULL,         --subclass,
             NULL,         --item_level,
             NULL,         --tran_level,
             NULL,         --item_status,
             NULL,         --waste_type,
             NULL,         --sellable_ind,
             NULL,         --orderable_ind,
             NULL,         --pack_ind,
             NULL,         --pack_type,
             NULL,         --item_desc,
             NULL,         --diff_1,
             NULL,         --diff_2,
             NULL,         --diff_3,
             NULL,         --diff_4,
             CASE
                WHEN I_message.hier_level IN ('S','W','E','I')
                THEN MXILD.hier_value
                ELSE NULL
             END loc,
             CASE
                WHEN I_message.hier_level IN ('S','W','E','I')
                THEN I_message.hier_level
                ELSE NULL
             END loc_type,
             MXILD.daily_waste_pct,
             NULL,         --unit_cost_loc,
             NULL,         --unit_retail_loc,
             NULL,         --selling_retail_loc,
             NULL,         --selling_uom,
             NULL,                   --multi_units,
             NULL,                   --multi_unit_retail,
             NULL,                   --multi_selling_uom,
             NULL,         --item_loc_status,
             MXILD.taxable_ind,
             MXILD.ti,
             MXILD.hi,
             MXILD.store_ord_mult,
             NULL,         --meas_of_each,
             NULL,         --meas_of_price,
             NULL,         --uom_of_price,
             NULL,         --primary_variant,
             MXILD.primary_supp,
             MXILD.primary_cntry,
             MXILD.local_item_desc,
             MXILD.local_short_desc,
             NULL,         --primary_cost_pack,
             MXILD.receive_as_type,
             NULL,         --store_price_ind,
             MXILD.uin_type,
             MXILD.uin_label,
             MXILD.capture_time,
             MXILD.ext_uin_ind,
             CASE 
                WHEN I_message.hier_level = 'W'
                THEN MXILD.source_method
                ELSE MXILD.source_method
             END source_method,
             MXILD.source_wh,
             NULL,         --inbound_handling_days,
             NULL,         --currency_code,
             NULL,         --like_store,
             'N',          --default_to_children_ind,
             NULL,         --class_vat_ind,
             CASE
                WHEN I_message.hier_level NOT IN ('S','W','E','I')
                THEN I_message.hier_level
                ELSE NULL
             END hier_level,
             CASE WHEN I_message.hier_level NOT IN ('S','W','E','I','C') THEN
                MXILD.hier_value
             ELSE NULL END hier_num_value,
             CASE
                WHEN I_message.hier_level= 'C'
                THEN MXILD.hier_value
                ELSE NULL
             END hier_char_value,
             NULL,
             NULL,
             'Y',  --ranged_ind
             NULL,
             NULL
        from TABLE(CAST (I_message.XItemLocDtl_TBL AS "RIB_XItemLocDtl_TBL")) MXILD;

BEGIN
   if I_message_type = RMSSUB_XITEMLOC.ITEM_LOC_ADD then
      --
      FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE:=NULL;
      --
      open C_GET_INPUT;
      fetch C_GET_INPUT BULK COLLECT into L_input;
      close C_GET_INPUT;
      --
      --
      open C_GET_RUN_TYPE;
      fetch C_GET_RUN_TYPE into L_event_run_type;
      close C_GET_RUN_TYPE;
      
      if L_event_run_type = FUTURE_COST_EVENT_SQL.SYNC_COST_EVENT_RUN_TYPE then
         FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE := FUTURE_COST_EVENT_SQL.ASYNC_COST_EVENT_RUN_TYPE;
      end if;
      --
      if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                       L_input) = FALSE then
         return FALSE;
      end if;
      ---
      FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE:=NULL;
      ---
   elsif I_message_type = RMSSUB_XITEMLOC.ITEM_LOC_UPD then
      for i in 1 .. I_message.XItemLocDtl_TBL.last loop
         if I_message.hier_level = 'W' then
            L_source_method := I_message.XItemLocDtl_TBL(i).source_method;
         else
            L_source_method := NVL(I_message.XItemLocDtl_TBL(i).source_method,'S');
         end if;

         if UPDATE_ITEM_LOC(O_error_message,
                          I_message.item,
                          I_message.hier_level,
                          I_message.XItemLocDtl_TBL(i).hier_value,
                          I_message.XItemLocDtl_TBL(i).primary_supp,
                          I_message.XItemLocDtl_TBL(i).primary_cntry,
                          I_message.XItemLocDtl_TBL(i).local_item_desc,
                          I_message.XItemLocDtl_TBL(i).status,
                          I_message.XItemLocDtl_TBL(i).store_ord_mult,
                          I_message.XItemLocDtl_TBL(i).receive_as_type,
                          I_message.XItemLocDtl_TBL(i).taxable_ind,
                          I_message.XItemLocDtl_TBL(i).local_short_desc,
                          I_message.XItemLocDtl_TBL(i).daily_waste_pct,
                          L_source_method,
                          I_message.XItemLocDtl_TBL(i).source_wh,
                          I_message.XItemLocDtl_TBL(i).ti,
                          I_message.XItemLocDtl_TBL(i).hi,
                          I_message.XItemLocDtl_TBL(i).uin_type,
                          I_message.XItemLocDtl_TBL(i).uin_label,
                          I_message.XItemLocDtl_TBL(i).capture_time,
                          I_message.XItemLocDtl_TBL(i).ext_uin_ind) = FALSE then
            return FALSE;
         end if;
      end loop;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE',
      I_message_type, null, null);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
--------------------------------------------------------------------------------

FUNCTION UPDATE_ITEM_LOC(O_error_message      IN OUT   VARCHAR2,
                         I_item               IN       ITEM_MASTER.ITEM%TYPE,
                         I_hier_level         IN       VARCHAR2,
                         I_hier_value         IN       NUMBER,
                         I_primary_supp       IN       ITEM_LOC.PRIMARY_SUPP%TYPE,
                         I_primary_cntry      IN       ITEM_LOC.PRIMARY_CNTRY%TYPE,
                         I_local_item_desc    IN       ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                         I_status             IN       ITEM_LOC.STATUS%TYPE,
                         I_store_ord_mult     IN       ITEM_LOC.STORE_ORD_MULT%TYPE,
                         I_receive_as_type    IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_taxable_ind        IN       ITEM_LOC.TAXABLE_IND%TYPE,
                         I_local_short_desc   IN       ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                         I_daily_waste_pct    IN       ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                         I_source_method      IN       ITEM_LOC.SOURCE_METHOD%TYPE,
                         I_source_wh          IN       ITEM_LOC.SOURCE_WH%TYPE,
                         I_ti                 IN       ITEM_LOC.TI%TYPE,
                         I_hi                 IN       ITEM_LOC.HI%TYPE,
                         I_uin_type           IN       ITEM_LOC.UIN_TYPE%TYPE DEFAULT NULL,
                         I_uin_label          IN       ITEM_LOC.UIN_LABEL%TYPE DEFAULT NULL,
                         I_capture_time       IN       ITEM_LOC.CAPTURE_TIME%TYPE DEFAULT NULL,
                         I_ext_uin_ind        IN       ITEM_LOC.EXT_UIN_IND%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

L_program               VARCHAR2(50) := 'RMSSUB_XITEMLOC_SQL.UPDATE_ITEM_LOC';
--
L_locs                  LOC_TBL := LOC_TBL();
--
L_item                  ITEM_TABLE_TYPE;
L_item_status           ITEM_STATUS_TABLE_TYPE;
L_item_level            ITEM_LEVEL_TABLE_TYPE;
L_tran_level            TRAN_LEVEL_TABLE_TYPE;
L_loc                   LOC_TABLE_TYPE;
L_loc_type              LOC_TYPE_TABLE_TYPE;
L_primary_supp          PRIMARY_SUPP_TABLE_TYPE;
L_primary_cntry         PRIMARY_CNTRY_TABLE_TYPE;
L_status                STATUS_TABLE_TYPE;
L_local_item_desc       LOCAL_ITEM_DESC_TABLE_TYPE;
L_local_short_desc      LOCAL_SHORT_DESC_TABLE_TYPE;
L_primary_variant       PRIMARY_VARIANT_TABLE_TYPE;
L_unit_retail           UNIT_RETAIL_TABLE_TYPE;
L_ti                    TI_TABLE_TYPE;
L_hi                    HI_TABLE_TYPE;
L_store_ord_mult        STORE_ORD_MULT_TABLE_TYPE;
L_daily_waste_pct       DAILY_WASTE_PCT_TABLE_TYPE;
L_taxable_ind           TAXABLE_IND_TABLE_TYPE;
L_meas_of_each          MEAS_OF_EACH_TABLE_TYPE;
L_meas_of_price         MEAS_OF_PRICE_TABLE_TYPE;
L_uom_of_price          UOM_OF_PRICE_TABLE_TYPE;
L_selling_unit_retail   SELLING_UNIT_RETAIL_TABLE_TYPE;
L_selling_uom           SELLING_UOM_TABLE_TYPE;
L_primary_cost_pack     PRIMARY_COST_PACK_TABLE_TYPE;
L_process_children      PROCESS_CHILDREN_TABLE_TYPE;
L_receive_as_type       RECEIVE_AS_TYPE_TABLE_TYPE;
L_ranged_ind            RANGED_IND_TABLE_TYPE;
L_source_method         SOURCE_METHOD_TABLE_TYPE;
L_source_wh             SOURCE_WH_TABLE_TYPE;
L_uin_type              UIN_TYPE_TABLE_TYPE;
L_uin_label             UIN_LABEL_TABLE_TYPE;
L_capture_time          CAPTURE_TIME_TABLE_TYPE;
L_ext_uin_ind           EXT_UIN_IND_TABLE_TYPE;


cursor C_ORG_HIER is
  select s.store
    from chain c,
         area a,
         region r,
         district d,
         store s
   where s.district = d.district
     and d.district = decode(I_hier_level, 'DI', I_hier_value, d.district)
     and d.region = r.region
     and r.region = decode(I_hier_level, 'RE', I_hier_value, r.region)
     and r.area = a.area
     and a.area = decode(I_hier_level, 'AR', I_hier_value, a.area)
     and a.chain = c.chain
     and c.chain = decode(I_hier_level, 'CH', I_hier_value, c.chain)
order by store;

cursor C_ITEM_LOC is
select im.item,
       im.status,
       im.item_level,
       im.tran_level,
       il.loc,
       il.loc_type,
       il.primary_supp,
       il.primary_cntry,
       il.status,
       il.local_item_desc,
       il.local_short_desc,
       il.primary_variant,
       il.unit_retail,
       il.ti,
       il.hi,
       il.store_ord_mult,
       il.daily_waste_pct,
       il.taxable_ind,
       il.meas_of_each,
       il.meas_of_price,
       il.uom_of_price,
       il.selling_unit_retail,
       il.selling_uom,
       il.primary_cost_pack,
       'N',
       il.receive_as_type,
       il.ranged_ind,
       il.source_method,
       il.source_wh,
       il.uin_type,
       il.uin_label,
       il.capture_time,
       il.ext_uin_ind
  from item_loc il,
       item_master im,
       TABLE (CAST (L_locs AS LOC_TBL)) l
 where im.item = il.item
   and il.item = I_item
   and il.loc = value(l);

BEGIN

   if I_hier_level in ('S', 'W') then
      L_locs.extend();
      L_locs(1) := I_hier_value;
   else
      open C_ORG_HIER;
      fetch C_ORG_HIER bulk collect into L_locs;
      close C_ORG_HIER;

      if L_locs.count = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_VALUE', I_hier_value);
         return FALSE;
      end if;
   end if;

   open C_ITEM_LOC;
   fetch C_ITEM_LOC bulk collect into L_item,
                                      L_item_status,
                                      L_item_level,
                                      L_tran_level,
                                      L_loc,
                                      L_loc_type,
                                      L_primary_supp,
                                      L_primary_cntry,
                                      L_status,
                                      L_local_item_desc,
                                      L_local_short_desc,
                                      L_primary_variant,
                                      L_unit_retail,
                                      L_ti,
                                      L_hi,
                                      L_store_ord_mult,
                                      L_daily_waste_pct,
                                      L_taxable_ind,
                                      L_meas_of_each,
                                      L_meas_of_price,
                                      L_uom_of_price,
                                      L_selling_unit_retail,
                                      L_selling_uom,
                                      L_primary_cost_pack,
                                      L_process_children,
                                      L_receive_as_type,
                                      L_ranged_ind,
                                      L_source_method,
                                      L_source_wh,
                                      L_uin_type,
                                      L_uin_label,
                                      L_capture_time,
                                      L_ext_uin_ind;
   close C_ITEM_LOC;
   --
   if L_item.count=0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_LOC_REL',
                                            I_item,
                                            L_loc(1),
                                            null);
      return FALSE;
   end if;
   --

   for i in L_item.first .. L_item.last loop

      if I_source_method IN ('S', 'W') and
         NVL(I_source_method, '999') != NVL(L_source_method(i), '999') then
         L_source_method(i) := I_source_method;
         L_source_wh(i)     := I_source_wh;
      elsif NVL(I_source_wh, -999) != NVL(L_source_wh(i), -999) then
         L_source_method(i) := I_source_method;
         L_source_wh (i)    := I_source_wh;
      end if;

      L_local_short_desc(i) := I_local_short_desc;
      L_daily_waste_pct(i)  := I_daily_waste_pct;

      if L_status(i) != I_status then
         if ITEM_LOC_SQL.STATUS_CHANGE_VALID(O_error_message,
                                             L_item(i),
                                             L_loc(i),
                                             L_loc_type(i),
                                             L_status(i),
                                             I_status) = FALSE then
            return FALSE;
         end if;
      end if;

      if ITEM_LOC_SQL.UPDATE_ITEM_LOC(O_error_message,
                                   L_item(i),
                                   L_item_status(i),
                                   L_item_level(i),
                                   L_tran_level(i),
                                   L_loc(i),
                                   L_loc_type(i),
                                   nvl(I_primary_supp,L_primary_supp(i)),
                                   nvl(I_primary_cntry,L_primary_cntry(i)),
                                   I_status,
                                   nvl(I_local_item_desc, L_local_item_desc(i)),
                                   L_local_short_desc(i),
                                   L_primary_variant(i),
                                   L_unit_retail(i),
                                   nvl(I_ti,L_ti(i)),
                                   nvl(I_hi,L_hi(i)),
                                   I_store_ord_mult,
                                   L_daily_waste_pct(i),
                                   nvl(I_taxable_ind, L_taxable_ind(i)),
                                   L_meas_of_each(i),
                                   L_meas_of_price(i),
                                   L_uom_of_price(i),
                                   L_selling_unit_retail(i),
                                   L_selling_uom(i),
                                   L_primary_cost_pack(i),
                                   L_process_children(i),
                                   nvl(I_receive_as_type, L_receive_as_type(i)),
                                   L_ranged_ind(i), -- ranged_ind
                                   null,
                                   null,
                                   L_source_method(i),
                                   L_source_wh(i),
                                   nvl(I_uin_type, L_uin_type(i)),
                                   nvl(I_uin_label, L_uin_label(i)),
                                   nvl(I_capture_time, L_capture_time(i)),
                                   nvl(I_ext_uin_ind, L_ext_uin_ind(i))) = FALSE then
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ITEM_LOC;
--------------------------------------------------------------------------------

END RMSSUB_XITEMLOC_SQL;
/

