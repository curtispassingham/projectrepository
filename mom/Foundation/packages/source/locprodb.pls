CREATE OR REPLACE PACKAGE BODY LOC_PROD_SECURITY_SQL AS
-----------------------------------------------------------------------
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
LP_table        VARCHAR2(50);
----------------------------------------------------------------------
FUNCTION NEW_USER(O_error_message IN OUT VARCHAR2,
                  O_exist         IN OUT BOOLEAN,
                  I_user_id       IN     all_users.username%TYPE)
return BOOLEAN IS
   L_exist              VARCHAR2(1) := 'N';
   cursor C_NEW_USER is
      select 'Y'
        from all_users
       where username = I_user_id;
BEGIN
   ---
   sql_lib.set_mark('OPEN','C_NEW_USER','ALL_USERS','USER_ID'||I_user_id);
   open C_NEW_USER;
   SQL_LIB.SET_MARK('FECTH','C_NEW_USER','ALL_USERS','USER_ID'||I_user_id);
   fetch C_NEW_USER into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_NEW_USER','ALL_USERS','USER_ID'||I_user_id);
   close C_NEW_USER;
   ---
   if L_exist = 'Y' then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.NEW_USER',
                                            to_char(SQLCODE));
   RETURN FALSE;
END NEW_USER;
-----------------------------------------------------------------------
FUNCTION NEXT_GROUP_NUMBER(O_error_message IN OUT VARCHAR2,
                           O_group_id      IN OUT sec_group.group_id%TYPE)
return BOOLEAN IS
   L_group_id           sec_group.group_id%TYPE;
   L_first_time         VARCHAR2(1) := 'Y';
   L_initial             sec_group.group_id%TYPE;
   L_exist              VARCHAR2(1) := 'N';

   cursor C_GET_NEXT_GROUP_ID is
      select sec_group_sequence.NEXTVAL 
        from dual;

   cursor C_GROUP_NUMBER is
      select 'Y'
        from sec_group
       where group_id = L_group_id;
BEGIN
   
   LOOP
      ---Retrieve Sequence Number
      SQL_LIB.SET_MARK('OPEN', 'C_GET_NEXT_GROUP_ID', 'DUAL', NULL);
      open C_GET_NEXT_GROUP_ID;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_NEXT_GROUP_ID', 'DUAL', NULL);
      fetch C_GET_NEXT_GROUP_ID into L_group_id;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_NEXT_GROUP_ID', 'DUAL', NULL);
      close C_GET_NEXT_GROUP_ID;

      ---set termination condition for loop
      if L_first_time = 'Y' then
         L_initial := L_group_id - 1;
         L_first_time := 'N';
      elsif L_group_id = L_initial then
         O_error_message := SQL_LIB.CREATE_MSG('NO_MORE_GRP_ID_SEQUENCE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      
      ---Check to be sure this Group ID does not already exist   
      SQL_LIB.SET_MARK('OPEN','C_GROUP_NUMBER','SEC_GROUP','GROUP_ID'||L_group_id);
      open C_GROUP_NUMBER;
      SQL_LIB.SET_MARK('FETCH','C_GROUP_NUMBER','SEC_GROUP','GROUP_ID'||L_group_id);
      fetch C_GROUP_NUMBER into L_exist;
      ---
      if (C_GROUP_NUMBER%notfound) then
         SQL_LIB.SET_MARK('CLOSE','C_GROUP_NUMBER','SEC_GROUP','GROUP_ID'||L_group_id);
         close C_GROUP_NUMBER;
         ---bind sequence number with group id variable
         O_group_id := L_group_id;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GROUP_NUMBER','SEC_GROUP','GROUP_ID'||L_group_id);
      close C_GROUP_NUMBER;
      ---
   END LOOP;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.NEXT_GROUP_NUMBER',
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_GROUP_NUMBER;
---------------------------------------------------------------------------------------
FUNCTION GET_GROUP_NAME(O_error_message IN OUT VARCHAR2,
                        O_group_name    IN OUT sec_group.group_name%TYPE,
                        I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN IS
   cursor C_GROUP_NAME is
      select group_name
        from v_sec_group_tl
       where group_id = I_group_id;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_GROUP_NAME','V_SEC_GROUP_TL','GROUP_ID'||I_group_id);
   open C_GROUP_NAME;
   SQL_LIB.SET_MARK('FETCH','C_GROUP_NAME','V_SEC_GROUP_TL','GROUP_ID'||I_group_id);
   fetch C_GROUP_NAME into O_group_name;
   ---
   if C_GROUP_NAME%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_GROUP_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE','C_GROUP_NAME','V_SEC_GROUP_TL','GROUP_ID'||I_group_id);
      close C_GROUP_NAME;

      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GROUP_NAME','V_SEC_GROUP_TL','GROUP_ID'||I_group_id);
   close C_GROUP_NAME;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.GET_GROUP_NAME',
                                            to_char(SQLCODE));
      return FALSE;
END GET_GROUP_NAME;
---------------------------------------------------------------------------------
FUNCTION LOCK_GRP_SEC(O_error_message IN OUT VARCHAR2,
                      I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN IS
   cursor C_SEC_USER_GROUP is
      select user_seq
        from sec_user_group
       where group_id = I_group_id
         for update nowait;
         
   cursor C_SEC_GROUP_LOC_MATRIX is
      select group_id
        from sec_group_loc_matrix
       where group_id = I_group_id
         for update nowait;
   
BEGIN
   ---
   LP_table := 'sec_user_group';
   SQL_LIB.SET_MARK('OPEN','C_SEC_USER_GROUP','SEC_USER_GROUP','GROUP_ID'||I_group_id);
   open C_SEC_USER_GROUP;
   SQL_LIB.SET_MARK('CLOSE','C_SEC_USER_GROUP','SEC_USER_GROUP','GROUP_ID'||I_group_id);
   close C_SEC_USER_GROUP;
   ---
   LP_table := 'sec_group_loc_matrix';
   SQL_LIB.SET_MARK('OPEN','C_SEC_GROUP_LOC_MATRIX','SEC_GROUP_LOC_MATRIX','GROUP_ID'||I_group_id);
   open C_SEC_GROUP_LOC_MATRIX;
   SQL_LIB.SET_MARK('CLOSE','C_SEC_GROUP_LOC_MATRIX','SEC_GROUP_LOC_MATRIX','GROUP_ID'||I_group_id);
   close C_SEC_GROUP_LOC_MATRIX;
   ---
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            LP_table,
                                            I_group_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.LOCK_GRP_SEC',
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_GRP_SEC;
------------------------------------------------------------------------------------
FUNCTION DELETE_GRP_SEC(O_error_message IN OUT VARCHAR2,
                        I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN IS
BEGIN
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'sec_user_group','GROUP_ID'||I_group_id);
   delete from sec_user_group
      where group_id = I_group_id;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'sec_group_loc_matrix','GROUP_ID'||I_group_id);
   delete from sec_group_loc_matrix
      where group_id = I_group_id;
   ---   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.DELETE_GRP_SEC',
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_GRP_SEC;
-------------------------------------------------------------------------------------
FUNCTION LOCK_USR_SEC(O_error_message IN OUT VARCHAR2,
                      I_user_id       IN     VARCHAR2)
return BOOLEAN IS
   cursor C_SEC_USER is
      select 'x'
        from sec_user su,
             sec_user_group sug
       where su.database_user_id = I_user_id
         and su.user_seq         = sug.user_seq
         for update nowait;   

BEGIN
   ---
   LP_table := 'sec_user, sec_user_group';
   SQL_LIB.SET_MARK('OPEN','C_SEC_USER','SEC_USER, SEC_USER_GROUP','DATABASE_USER_ID'||I_user_id);
   open C_SEC_USER;
   SQL_LIB.SET_MARK('CLOSE','C_SEC_USER','SEC_USER, SEC_USER_GROUP','DATABASE_USER_ID'||I_user_id);
   close C_SEC_USER;
   ---   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            LP_table,
                                            I_user_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.LOCK_USR_SEC',
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_USR_SEC;
---------------------------------------------------------------------------------
FUNCTION DELETE_USR_SEC(O_error_message IN OUT VARCHAR2,
                        I_user_id       IN     VARCHAR2)
return BOOLEAN IS
BEGIN
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'sec_user_group','USER_ID '||I_user_id);
   delete from sec_user_group sug
         where exists (select 'x'
                         from sec_user su
                        where su.database_user_id = I_user_id
                          and su.user_seq         = sug.user_seq);
                          
      ---
   SQL_LIB.SET_MARK('DELETE',NULL,'sec_user','DATABASE_USER_ID '||I_user_id);
   delete from sec_user
      where database_user_id = I_user_id;
      
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.DELETE_USR_SEC',
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_USR_SEC;
-----------------------------------------------------------------------------------
FUNCTION LOC_CHECK_UNIQUE(O_error_message    IN OUT VARCHAR2,
                          O_exist            IN OUT BOOLEAN,
                          I_group_id         IN     sec_group_loc_matrix.group_id%TYPE,
                          I_column_code      IN     sec_group_loc_matrix.column_code%TYPE,
                          I_region           IN     sec_group_loc_matrix.region%TYPE,
                          I_district         IN     sec_group_loc_matrix.district%TYPE,
                          I_store            IN     sec_group_loc_matrix.store%TYPE,
                          I_wh               IN     sec_group_loc_matrix.wh%TYPE)
return BOOLEAN IS
   L_exist                     VARCHAR2(1) := 'N';
   cursor C_GRP_LOC_MATRIX is
      select 'Y'
        from sec_group_loc_matrix
       where group_id = I_group_id
         and column_code = I_column_code
         and nvl(region,-1) = nvl(I_region,-1)
         and nvl(district,-1) = nvl(I_district,-1)
         and nvl(store,-1) = nvl(I_store,-1)
         and nvl(wh,-1) = nvl(I_wh,-1);
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_GRP_LOC_MATRIX','SEC_GROUP_LOC_MATRIX',NULL);
   open C_GRP_LOC_MATRIX;
   SQL_LIB.SET_MARK('FETCH','C_GRP_LOC_MATRIX','SEC_GROUP_LOC_MATRIX',NULL);
   fetch C_GRP_LOC_MATRIX into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_GRP_LOC_MATRIX','SEC_GROUP_LOC_MATRIX',NULL);
   close C_GRP_LOC_MATRIX;
   ---
   if L_exist = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('REC_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.LOC_CHECK_UNIQUE',
                                            to_char(SQLCODE));
      return FALSE;
END LOC_CHECK_UNIQUE;
------------------------------------------------------------------------------------
FUNCTION EXPAND_LOC(O_error_message    IN OUT VARCHAR2,
                    I_group_id         IN     sec_group_loc_matrix.group_id%TYPE,
                    I_region           IN     sec_group_loc_matrix.region%TYPE,
                    I_district         IN     sec_group_loc_matrix.district%TYPE,
                    I_store            IN     sec_group_loc_matrix.store%TYPE,
                    I_wh               IN     sec_group_loc_matrix.wh%TYPE,
                    I_select_ind       IN     sec_group_loc_matrix.select_ind%TYPE,
                    I_update_ind       IN     sec_group_loc_matrix.update_ind%TYPE)
return BOOLEAN IS
   L_code                      code_detail.code%TYPE;
   L_exist                     BOOLEAN;
   cursor C_CODE is
      select code
        from code_detail
       where code_type = 'LSEC'
         and code != 'LALL';
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_CODE','CODE_DETAIL','CODE_TYPE'||'LSEC');
   open C_CODE;
   loop
      SQL_LIB.SET_MARK('FETCH','C_CODE','CODE_DETAIL','CODE_TYPE'||'LSEC');
      fetch C_CODE into L_code;
      exit when C_CODE%NOTFOUND;
      ---
      if not LOC_CHECK_UNIQUE(O_error_message,
                              L_exist,
                              I_group_id,
                              L_code,
                              I_region,
                              I_district,
                              I_store,
                              I_wh) then

         return FALSE;

     end if;
      ---
      if L_exist then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXPAND_LOC',
                                               NULL,
                                               NULL,
                                               NULL);

         return FALSE;
      else
         ---
         if L_code in ('LPRM','LSTR','LSTDY','LSTACH') then
            if I_wh is NULL then
               insert into sec_group_loc_matrix(column_code,
                                             group_id,
                                             region,
                                             district,
                                             store,
                                             wh,
                                             select_ind,
                                             update_ind)
                values(L_code,
                       I_group_id,
                       I_region,
                       I_district,
                       I_store,
                       NULL,
                       I_select_ind,
                       I_update_ind);
            end if;
         elsif L_code = 'LALCFR' then
            if I_wh is not NULL then
               insert into sec_group_loc_matrix(column_code,
                                             group_id,
                                             region,
                                             district,
                                             store,
                                             wh,
                                             select_ind,
                                             update_ind)
                values(L_code,
                       I_group_id,
                       NULL,
                       NULL,
                       NULL,
                       I_wh,
                       I_select_ind,
                       I_update_ind);
            end if;
         else
            insert into sec_group_loc_matrix(column_code,
                                             group_id,
                                             region,
                                             district,
                                             store,
                                             wh,
                                             select_ind,
                                             update_ind)
                values(L_code,
                       I_group_id,
                       I_region,
                       I_district,
                       I_store,
                       I_wh,
                       I_select_ind,
                       I_update_ind);
         end if;
         ---
      end if;
      ---
   end loop;
   SQL_LIB.SET_MARK('CLOSE','C_CODE','CODE_DETAIL','CODE_TYPE'||'LSEC');
   close C_CODE;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.EXPAND_LOC',
                                            to_char(SQLCODE));
      return FALSE;
END EXPAND_LOC;
--------------------------------------------------------------------------------------
FUNCTION USRGRP_CHECK_UNIQUE(O_error_message   IN OUT VARCHAR2,
                             O_exist           IN OUT BOOLEAN,
                             I_group_id        IN     sec_user_group.group_id%TYPE,
                             I_user_id         IN     sec_user.database_user_id%TYPE)
RETURN BOOLEAN IS

   L_exist             VARCHAR2(1) := 'N';

   cursor C_user_group is
      select 'Y'
        from sec_user_group sug,
             sec_user su
       where group_id = I_group_id
         and sug.user_seq = su.user_seq
         and su.database_user_id = I_user_id;

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_USER_GROUP','SEC_USER_GROUP, SEC_USER','GROUP_ID'||I_group_id||'database_user_id'||I_user_id);
   open C_USER_GROUP;
   SQL_LIB.SET_MARK('FETCH','C_USER_GROUP','SEC_USER_GROUP, SEC_USER','GROUP_ID'||I_group_id||'database_user_id'||I_user_id);
   fetch C_USER_GROUP into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_USER_GROUP','SEC_USER_GROUP, SEC_USER','GROUP_ID'||I_group_id||'database_user_id'||I_user_id);
   close C_USER_GROUP;
   ---
   if L_exist = 'Y' then

      O_error_message := SQL_LIB.CREATE_MSG('REC_EXIST',
                                            NULL,
                                            NULL,
                                            NULL);
      O_exist := TRUE;

   else
      O_exist := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.USRGRP_CHECK_UNIQUE',
                                            to_char(SQLCODE));
      return FALSE;

END USRGRP_CHECK_UNIQUE;
-------------------------------------------------------------------------------------------
FUNCTION GET_ROLE(O_error_message   IN OUT VARCHAR2,
                  O_role               OUT SEC_GROUP.ROLE%TYPE,
                  I_group_id        IN     SEC_GROUP.GROUP_ID%TYPE) 
   RETURN BOOLEAN IS
   cursor C_GET_ROLE is
      select role
        from sec_group
       where group_id = I_group_id;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_ROLE','SEC_GROUP','GROUP_ID'||I_group_id);
   open C_GET_ROLE;
   SQL_LIB.SET_MARK('FETCH','C_GET_ROLE','SEC_GROUP','GROUP_ID'||I_group_id);
   fetch C_GET_ROLE into O_role;
   ---
   if C_GET_ROLE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_GROUP_ID',
                                            NULL,
                                            NULL,
                                            NULL);

      SQL_LIB.SET_MARK('CLOSE','C_GET_ROLE','SEC_GROUP','GROUP_ID'||I_group_id);
      close C_GET_ROLE;

      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_ROLE','SEC_GROUP','GROUP_ID'||I_group_id);
   close C_GET_ROLE;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.GET_ROLE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ROLE;
------------------------------------------------------------------------------
FUNCTION INSERT_SEC_USER(O_error_message        IN OUT VARCHAR2,
                         I_database_user_id     IN     SEC_USER.DATABASE_USER_ID%TYPE,
                         I_application_user_id	IN     SEC_USER.APPLICATION_USER_ID%TYPE DEFAULT NULL) 
RETURN BOOLEAN is

   L_user_seq      SEC_USER.USER_SEQ%TYPE;
   
   cursor C_GET_NEXT_USER_SEQ is
      select sec_user_sequence.NEXTVAL 
        from dual;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_NEXT_USER_SEQ', 'DUAL', NULL);
   open C_GET_NEXT_USER_SEQ;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_NEXT_USER_SEQ', 'DUAL', NULL);
   fetch C_GET_NEXT_USER_SEQ into L_user_seq;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_NEXT_USER_SEQ', 'DUAL', NULL);
   close C_GET_NEXT_USER_SEQ;
   
   insert into SEC_USER(user_seq,
                        database_user_id,
                        application_user_id,
					    rms_user_ind,
					    resa_user_ind,
					    reim_user_ind,
					    allocation_user_ind)
                 VALUES(L_user_seq,
                        I_database_user_id, 
                        I_application_user_id,
						'Y',
						'N',
						'N',
						'N');
   return TRUE;                     


   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_PROD_SECURITY_SQL.GET_ROLE',
                                            to_char(SQLCODE));
     
END INSERT_SEC_USER;
----------------------------------------------------------------------------
END;
/


