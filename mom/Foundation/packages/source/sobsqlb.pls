



CREATE OR REPLACE PACKAGE BODY SET_OF_BOOKS_SQL AS
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------
--- Function Name:  GET_DESC
--- Purpose:        This Function will get the description for the Set of Books ID
---                 passed as input from FIF_GL_SETUP table
------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_set_of_books_desc  IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE,
                  I_set_of_books_id    IN       FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE)
   return BOOLEAN IS

   cursor C_GET_DESC is
   select set_of_books_desc
     from fif_gl_setup
    where set_of_books_id = I_set_of_books_id;

   L_program VARCHAR2(64) := 'SET_OF_BOOKS_SQL.GET_DESC';

BEGIN
   open C_GET_DESC;

   fetch C_GET_DESC into O_set_of_books_desc;

   if C_GET_DESC%NOTFOUND then
      close C_GET_DESC;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SET_OF_BOOKS_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_DESC;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_DESC;

------------------------------------------------------------------------------
--- Function Name:  GET_SOB_LOC
--- Purpose:        This Function will get the Set of Books ID and its description
---                 for the Location passed as input from FIF_GL_SETUP, STORE
---                 WH, PARTNER and TSF_ENTITY_ORG_UNIT_SOB tables
------------------------------------------------------------------------------
FUNCTION GET_SOB_LOC(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_set_of_books_desc  IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE,
                     O_set_of_books_id    IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE,
                     I_location           IN       ITEM_LOC.LOC%TYPE,
                     I_location_type      IN       ITEM_LOC.LOC_TYPE%TYPE)
   return BOOLEAN  IS

   cursor C_GET_SOB is
   select set_of_books_id,
          set_of_books_desc
     from mv_loc_sob
    where location = I_location
      and location_type = I_location_type;

   L_program VARCHAR2(64) := 'SET_OF_BOOKS_SQL.GET_SOB_LOC';

BEGIN
   open C_GET_SOB;

   fetch C_GET_SOB into O_set_of_books_id,
                        O_set_of_books_desc;

   if C_GET_SOB%NOTFOUND then
      close C_GET_SOB;
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_SOB;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_SOB_LOC;

------------------------------------------------------------------------------
--- Function Name:  CHECK_SUPP_SINGLE_LOC
--- Purpose:        This Function will return True in O_exists if the Location/Location Type
---                 and the Supplier passed as input are attached to the Same Org Unit
------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_SINGLE_LOC(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists             IN OUT   BOOLEAN,
                               I_supplier           IN       SUPS.SUPPLIER%TYPE,
                               I_location           IN       ITEM_LOC.LOC%TYPE,
                               I_location_type      IN       ITEM_LOC.LOC_TYPE%TYPE)
   return BOOLEAN  IS

   cursor C_CHECK_ORG_UNIT_S is
   select 'Y'
     from store s,
          partner_org_unit pou,
          sups sup
    where s.store = I_location
      and (sup.supplier = I_supplier
       or sup.supplier_parent = I_supplier)      
      and pou.partner = sup.supplier
      and pou.partner_type in ('S','U')
      and pou.org_unit_id = s.org_unit_id;

   cursor C_CHECK_ORG_UNIT_W is
   select 'Y'
     from wh wh,
          partner_org_unit pou,
          sups sup
    where wh.wh = I_location
      and (sup.supplier = I_supplier
       or sup.supplier_parent = I_supplier)
      and pou.partner = sup.supplier
      and pou.partner_type in ('S','U')
      and pou.org_unit_id = wh.org_unit_id;

   cursor C_CHECK_ORG_UNIT_E is
   select 'Y'
     from partner par,
          partner_org_unit pou
    where par.partner_id = I_location
      and pou.partner = I_supplier
      and pou.partner_type in ('S','U')
      and pou.org_unit_id = par.org_unit_id
      and par.partner_type = 'E';

   L_program               VARCHAR2(64) := 'SET_OF_BOOKS_SQL.CHECK_SUPP_SINGLE_LOC';
   L_stockholding_store    BOOLEAN := FALSE;
   L_store_name            STORE.STORE_NAME%TYPE;
   L_store_type            STORE.STORE_TYPE%TYPE;
   L_fetch_cursor_value    VARCHAR2(1) := 'N';   

BEGIN
   O_exists := TRUE;

   if I_location_type='S' then
      -- If not company store/Franchise stockholding store skip the validation
      if NOT FILTER_LOV_VALIDATE_SQL.STOCKHOLDING_STORE(O_error_message,
                                                        L_stockholding_store,
                                                        L_store_name,
                                                        L_store_type,
                                                        I_location) then
         return FALSE;
      end if;
      if (L_store_type = 'C') or (L_store_type = 'F' and L_stockholding_store = TRUE) then
         open C_CHECK_ORG_UNIT_S;
         fetch C_CHECK_ORG_UNIT_S into L_fetch_cursor_value;
         close C_CHECK_ORG_UNIT_S;
      else
         L_fetch_cursor_value := 'Y';
      end if;
   elsif I_location_type='W' then
      open C_CHECK_ORG_UNIT_W;
      fetch C_CHECK_ORG_UNIT_W into L_fetch_cursor_value;
      close C_CHECK_ORG_UNIT_W;
   elsif I_location_type='E' then
      open C_CHECK_ORG_UNIT_E;
      fetch C_CHECK_ORG_UNIT_E into L_fetch_cursor_value;
      close C_CHECK_ORG_UNIT_E;
   end if;

   if L_fetch_cursor_value <> 'Y' then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('LOC_SUPP_NO_SAME_ORG_UNIT',
                                             I_location,
                                             I_supplier,
                                             NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END CHECK_SUPP_SINGLE_LOC;

------------------------------------------------------------------------------
--- Function Name:  CHECK_SUPP_MASS_LOC
--- Purpose:        This Function will return true in O_exists if Location/Location Type
---                 related to the item don't have a primary supplier associated
------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_MASS_LOC (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists             IN OUT   BOOLEAN,
                              I_item               IN       ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN IS

  cursor C_CHECK_SUPP is
   select 'Y'
     from item_loc il
    where il.item = I_item
      and primary_supp is NULL
      and status <> 'D';

   L_program                  VARCHAR2(64) := 'SET_OF_BOOKS_SQL.CHECK_SUPP_MASS_LOC';
   L_fetch_cursor_value       VARCHAR2(1);

BEGIN
   O_exists := TRUE;

   open C_CHECK_SUPP;

   fetch C_CHECK_SUPP into L_fetch_cursor_value;

   if C_CHECK_SUPP%NOTFOUND then
      O_exists := FALSE;
   end if;

   close C_CHECK_SUPP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END CHECK_SUPP_MASS_LOC;

------------------------------------------------------------------------------
--- Function Name:  GET_TSF_ENTITY
--- Purpose:        This Function will return the TSF_ENTITY_ID for the Org_unit_id
---                 and the Set_of_books_id passed as input.
------------------------------------------------------------------------------
FUNCTION GET_TSF_ENTITY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_tsf_entity_id   IN OUT   TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_org_unit_id     IN       ORG_UNIT.ORG_UNIT_ID%TYPE,
                        I_set_of_books_id IN       FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE)
   return BOOLEAN IS

   cursor C_GET_TSF_ENTITY is
   select tsf_entity_id
     from tsf_entity_org_unit_sob
    where org_unit_id = NVL(I_org_unit_id,org_unit_id)
      and set_of_books_id = NVL(I_set_of_books_id,set_of_books_id);

   L_program         VARCHAR2(64) := 'SET_OF_BOOKS_SQL.GET_TSF_ENTITY';

BEGIN

   if I_org_unit_id is NULL and I_set_of_books_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_EITHER',
                                             'I_org_unit_id',
                                             'I_set_of_books_id',
                                              L_program);
      return FALSE;
   end if;
   
   O_exists := TRUE;

   open C_GET_TSF_ENTITY;

   fetch C_GET_TSF_ENTITY into O_tsf_entity_id;

   if C_GET_TSF_ENTITY%NOTFOUND then
      close C_GET_TSF_ENTITY;
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_TSF_ENTITY_FOUND',
                                            NULL,
                                            NULL,
                                            NULL);
      
      return FALSE;
   end if;

   close C_GET_TSF_ENTITY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_TSF_ENTITY;

------------------------------------------------------------------------------
--- Function Name:  GET_SOB_CURRENCY
--- Purpose:        This Function will return the Currency Code and Set_of_books_id 
---                 from FIF_GL_SETUP table for the Org_unit_id passed as input.
------------------------------------------------------------------------------
FUNCTION GET_SOB_CURRENCY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_currency_code    IN OUT   FIF_GL_SETUP.CURRENCY_CODE%TYPE,
                          O_set_of_books_id  IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE,
                          I_org_unit_id      IN       ORG_UNIT.ORG_UNIT_ID%TYPE)
   return BOOLEAN IS

   cursor C_GET_CURRENCY_CODE is
   select fgs.currency_code,
          fgs.set_of_books_id
     from fif_gl_setup fgs,
          tsf_entity_org_unit_sob teous
    where fgs.set_of_books_id = teous.set_of_books_id
      and teous.org_unit_id = I_org_unit_id;

   L_program VARCHAR2(64) := 'SET_OF_BOOKS_SQL.GET_SOB_CURRENCY';

BEGIN
   open C_GET_CURRENCY_CODE;

   fetch C_GET_CURRENCY_CODE into O_currency_code,
                                  O_set_of_books_id;

   if C_GET_CURRENCY_CODE%NOTFOUND then
      close C_GET_CURRENCY_CODE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORGUNIT',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_CURRENCY_CODE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_SOB_CURRENCY;

------------------------------------------------------------------------------
--- Function Name:  CHECK_ORG_EXISTS
--- Purpose:        This Function will return true if the input TSF_ENTITY_ID
---                 is associated to at-least one Org_unit_id in TSF_ENTITY_ORG_UNIT_SOB table
------------------------------------------------------------------------------
FUNCTION CHECK_ORG_EXISTS(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists        IN OUT   BOOLEAN,
                          I_tsf_entity_id IN       TSF_ENTITY.TSF_ENTITY_ID%TYPE)
   return BOOLEAN IS


   cursor C_GET_ORG_UNIT is
      select 'Y'
        from tsf_entity_org_unit_sob
       where tsf_entity_id = I_tsf_entity_id;

   L_program                     VARCHAR2(64) := 'SET_OF_BOOKS_SQL.CHECK_ORG_EXISTS';
   L_fetch_cursor_value          VARCHAR2(1);

BEGIN
   O_exists := TRUE;

   open C_GET_ORG_UNIT;

   fetch C_GET_ORG_UNIT into L_fetch_cursor_value;

   if C_GET_ORG_UNIT%NOTFOUND then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_ORG_UNIT_TSF_ENTITY',
                                            I_tsf_entity_id,
                                            NULL,
                                            NULL);
   end if;

   close C_GET_ORG_UNIT;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END CHECK_ORG_EXISTS;

------------------------------------------------------------------------------
--- Function Name:  CHECK_SUPP_ORG_UNIT_EXISTS
--- Purpose:        This Function will return true if the input Supplier
---                 is associated to the input Org Unit in partner_org_unit table
---                 When input Org Unit is NULL, it returns TRUE if supplier is
---                 associated to any Org Unit
------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_ORG_UNIT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                    I_org_unit_id     IN       ORG_UNIT.ORG_UNIT_ID%TYPE DEFAULT NULL)
   return BOOLEAN IS

   cursor C_GET_ORG_UNIT is
   select  'Y'
     from  partner_org_unit pou,
           sups sup
    where  (sup.supplier = I_supplier 
       or  sup.supplier_parent = I_supplier)
      and  partner = sup.supplier
      and  partner_type in ('S','U')
      and  org_unit_id = NVL(I_org_unit_id,org_unit_id);

   L_program                      VARCHAR2(64) := 'SET_OF_BOOKS_SQL.CHECK_SUPP_ORG_UNIT_EXISTS';
   L_fetch_cursor_value           VARCHAR2(1);

BEGIN
   O_exists := TRUE;

   open C_GET_ORG_UNIT;

   fetch C_GET_ORG_UNIT into L_fetch_cursor_value;

   if C_GET_ORG_UNIT%NOTFOUND then
      O_exists := FALSE;
      if I_org_unit_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ORG_UNIT_FOR_SUPP',
                                               I_supplier,
                                               NULL,
                                               NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('ORG_UNIT_NOT_RELATE_SUPP',
                                               I_org_unit_id,
                                               I_supplier,
                                               NULL);
      end if;
   end if;

   close C_GET_ORG_UNIT;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END CHECK_SUPP_ORG_UNIT_EXISTS;

------------------------------------------------------------------------------
--- Function Name:  CHECK_ALL_LOCS_SINGLE_SOB
--- Purpose:        This Function will return true if all the locations in
---                 table LOCATION_DIST_TEMP belongs to the same Set of Books ID
------------------------------------------------------------------------------
FUNCTION CHECK_ALL_LOCS_SINGLE_SOB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists          IN OUT   BOOLEAN,
                                   I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN IS

   cursor C_CHECK_SOB is
   select MIN(set_of_books_id),MAX(set_of_books_id)
     from mv_loc_sob vls,
          location_dist_temp ldt
    where vls.location=ldt.location
      and vls.location_type = ldt.loc_type
      and ldt.order_no = NVL(I_order_no,ldt.order_no)
      and (ldt.contract_no = I_contract_no OR I_contract_no IS NULL);

   L_program               VARCHAR2(64) := 'SET_OF_BOOKS_SQL.CHECK_ALL_LOCS_SINGLE_SOB';
   L_min_set_of_books_id   FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE;
   L_max_set_of_books_id   FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE;

BEGIN
   O_exists := TRUE;

   if I_order_no is NULL and I_contract_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_EITHER',
                                             'I_order_no',
                                             'I_contract_no',
                                              L_program);
      return FALSE;
   end if;

   open C_CHECK_SOB;

   fetch C_CHECK_SOB into L_min_set_of_books_id,
                          L_max_set_of_books_id;

   close C_CHECK_SOB;

   if L_min_set_of_books_id<>L_max_set_of_books_id then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_SINGLE_SOB',
                                             NULL,
                                             NULL,
                                             NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END CHECK_ALL_LOCS_SINGLE_SOB;
------------------------------------------------------------------------------
--- Function Name:  REFRESH_MV_LOC_SOB
--- Purpose:        This Function will perform a complete refresh of the materialized view
---                 MV_LOC_SOB
------------------------------------------------------------------------------
FUNCTION REFRESH_MV_LOC_SOB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   return BOOLEAN IS

   L_program               VARCHAR2(64) := 'SET_OF_BOOKS_SQL.REFRESH_MV_LOC_SOB';

BEGIN
   DBMS_MVIEW.REFRESH('mv_loc_sob','c');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END REFRESH_MV_LOC_SOB;
------------------------------------------------------------------------------
--- Function Name:  GET_SINGLE_SOB_ID
--- Purpose:        This Function will return Set of Books ID and Set of books Desc 
---                 from FIF_GL_SETUP
------------------------------------------------------------------------------
FUNCTION GET_SINGLE_SOB_ID(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_set_of_books_desc  IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE,
                           O_Set_of_books_id    IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE)
   return BOOLEAN IS

   cursor C_GET_SET_OF_BOOKS is
   select set_of_books_desc,
          set_of_books_id
     from fif_gl_setup;

   L_program VARCHAR2(64) := 'SET_OF_BOOKS_SQL.GET_SINGLE_SOB_ID';

BEGIN
   open C_GET_SET_OF_BOOKS;

   fetch C_GET_SET_OF_BOOKS into O_set_of_books_desc,
                                 O_Set_of_books_id;

   if C_GET_SET_OF_BOOKS%NOTFOUND then
      close C_GET_SET_OF_BOOKS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_SET_OF_BOOKS_FOUND',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_SET_OF_BOOKS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_SINGLE_SOB_ID;

------------------------------------------------------------------------------
--- Function Name:  GET_CURRENCY_CODE
--- Purpose:        This Function will return the Currency Code 
---                 from FIF_GL_SETUP table for the Set_of_books_id passed as input.
------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_currency_code    IN OUT   FIF_GL_SETUP.CURRENCY_CODE%TYPE,
                           I_set_of_books_id  IN       FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE)
   return BOOLEAN IS

   cursor C_GET_CURRENCY_CODE is
   select fgs.currency_code
     from fif_gl_setup fgs
    where fgs.set_of_books_id = I_set_of_books_id;

   L_program VARCHAR2(64) := 'SET_OF_BOOKS_SQL.GET_CURRENCY_CODE';

BEGIN
   open C_GET_CURRENCY_CODE;

   fetch C_GET_CURRENCY_CODE into O_currency_code;

   if C_GET_CURRENCY_CODE%NOTFOUND then
      close C_GET_CURRENCY_CODE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SET_OF_BOOKS_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_CURRENCY_CODE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_CURRENCY_CODE;
----------------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_SUPP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_supplier           IN OUT   SUPS.SUPPLIER%TYPE,
                          I_location           IN       ITEM_LOC.LOC%TYPE,
                          I_location_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_item               IN       ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN IS

   L_program               VARCHAR2(64) := 'SET_OF_BOOKS_SQL.GET_PRIMARY_SUPP';
   L_company_store         BOOLEAN := FALSE;
   L_store_name            STORE.STORE_NAME%TYPE;

   cursor C_GET_SUPP_S is
     select supplier
       from item_supplier isp,
            partner_org_unit pou,
            store str
      where isp.item        = I_item
        and pou.partner     = isp.supplier
        and pou.partner_type in ('S','U')
        and str.store       = I_location
        and pou.org_unit_id = str.org_unit_id
      order by primary_supp_ind desc;

   cursor C_GET_SUPP_W is
     select supplier
       from item_supplier isp,
            partner_org_unit pou,
            wh wah
      where isp.item        = I_item
        and pou.partner     = isp.supplier
        and pou.partner_type in ('S','U')
        and wah.wh          = I_location
        and pou.org_unit_id = wah.org_unit_id
      order by primary_supp_ind desc;

   cursor C_GET_SUPP_E is
     select supplier
       from item_supplier isp,
            partner_org_unit pou,
            partner prt
      where isp.item        = I_item
        and pou.partner     = isp.supplier
        and pou.partner_type in ('S','U')
        and prt.partner_id  = TO_CHAR(I_location)
        and pou.org_unit_id = prt.org_unit_id
      order by primary_supp_ind desc;

BEGIN
   if I_location_type = 'S' then
      if FILTER_LOV_VALIDATE_SQL.COMPANY_STORE(O_error_message,
                                               L_company_store,
                                               L_store_name,
                                               I_location) = FALSE then
         return FALSE;
      end if;
      if L_company_store = FALSE then
         O_supplier := NULL;
      else
         -- Store
        open  C_GET_SUPP_S;
        fetch C_GET_SUPP_S into O_supplier;
        close C_GET_SUPP_S;
     end if;
   -- WH
   elsif I_location_type = 'W' then
      open  C_GET_SUPP_W;
      fetch C_GET_SUPP_W into O_supplier;
      close C_GET_SUPP_W;
   -- Finisher
   elsif I_location_type = 'E' then
      open  C_GET_SUPP_E;
      fetch C_GET_SUPP_E into O_supplier;
      close C_GET_SUPP_E;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_PRIMARY_SUPP;
------------------------------------------------------------------------------
--- Function Name:  GET_TSF_ENTITY
--- Purpose:        This Function will return the TSF_ENTITY_ID for the Org_unit_id
---                 passed as input.
------------------------------------------------------------------------------
FUNCTION GET_TSF_ENTITY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_tsf_entity_id   IN OUT   TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_org_unit_id     IN       ORG_UNIT.ORG_UNIT_ID%TYPE)
   return BOOLEAN IS

   cursor C_GET_TSF_ENTITY is
      select tsf_entity_id
        from tsf_entity_org_unit_sob
       where org_unit_id = I_org_unit_id;
    
   L_program         VARCHAR2(64) := 'SET_OF_BOOKS_SQL.GET_TSF_ENTITY';

BEGIN

   if I_org_unit_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Org Unit ID',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   O_exists := TRUE;

   open C_GET_TSF_ENTITY;

   fetch C_GET_TSF_ENTITY into O_tsf_entity_id;

   if O_tsf_entity_id is NULL then
      O_exists := FALSE;
   end if;

   close C_GET_TSF_ENTITY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_TSF_ENTITY;

------------------------------------------------------------------------------
END SET_OF_BOOKS_SQL;
/
