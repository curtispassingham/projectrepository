
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY MERCH_DEFAULT_SQL AS
--------------------------------------------------------------------------
FUNCTION GET_SEQ_NO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_seq_no          IN OUT   MERCH_HIER_DEFAULT.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)    := 'MERCH_DEFAULT_SQL.CHECK_DUP_MERCH_DEFAULTS';
   L_first_time               VARCHAR2(3) := 'YES';
   L_wrap_seq_no              MERCH_HIER_DEFAULT.SEQ_NO%TYPE;
   L_exists                   VARCHAR2(1) := NULL;

   cursor C_SEQ_EXISTS is
      select 'x'
        from merch_hier_default
       where seq_no  = O_seq_no;

   cursor C_SELECT_NEXTVAL is
      select merch_defaults_seq_no_sequence.NEXTVAL
        from dual;

BEGIN
   LOOP
      SQL_LIB.SET_MARK('OPEN',
                       'C_SELECT_NEXTVAL',
                       'DUAL',
                       NULL);
      open C_SELECT_NEXTVAL;

      SQL_LIB.SET_MARK('FETCH',
                       'C_SELECT_NEXTVAL',
                       'DUAL',
                       NULL);
      fetch C_SELECT_NEXTVAL into O_seq_no;
      if L_first_time = 'YES' then
         L_wrap_seq_no   := O_seq_no;
         L_first_time    := 'NO';
      elsif O_seq_no = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL', 
                                               NULL, 
                                               NULL, 
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE',
                          'C_SELECT_NEXTVAL',
                          'DUAL',
                          NULL);
         close C_SELECT_NEXTVAL;
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_SELECT_NEXTVAL',
                       'DUAL',
                       NULL);
      close C_SELECT_NEXTVAL;

      SQL_LIB.SET_MARK('OPEN',
                       'C_SEQ_EXISTS',
                       'MERCH_HIER_DEFAULT',
                       'seq_no: '||TO_CHAR(O_seq_no));
      open C_SEQ_EXISTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_SEQ_EXISTS',
                       'MERCH_HIER_DEFAULT',
                       'seq_no: '||TO_CHAR(O_seq_no));
      fetch C_SEQ_EXISTS into L_exists;
      if C_SEQ_EXISTS%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_SEQ_EXISTS',
                          'MERCH_HIER_DEFAULT',
                          'seq_no: '||TO_CHAR(O_seq_no));
         close C_SEQ_EXISTS;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SEQ_EXISTS',
                       'MERCH_HIER_DEFAULT',
                       'seq_no: '||TO_CHAR(O_seq_no));
      close C_SEQ_EXISTS;
   END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END GET_SEQ_NO;
--------------------------------------------------------------------------
FUNCTION CHECK_DUP_MERCH_DEFAULTS (O_error_message     IN OUT  VARCHAR2,
                           O_dup_exist            IN OUT  BOOLEAN,
                           I_dialog        IN     MERCH_HIER_DEFAULT.DIALOG%TYPE,
                           I_info          IN     MERCH_HIER_DEFAULT.INFO%TYPE,
                           I_dept          IN     MERCH_HIER_DEFAULT.DEPT%TYPE,
                           I_class         IN     MERCH_HIER_DEFAULT.CLASS%TYPE,
                           I_subclass      IN     MERCH_HIER_DEFAULT.SUBCLASS%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(64)    := 'MERCH_DEFAULT_SQL.CHECK_DUP_MERCH_DEFAULTS';
   L_exists    VARCHAR2(1);

      cursor C_DUP_EXIST is
         select 'Y'
           from merch_hier_default
          where dialog   = I_dialog
            and info     = I_info
            and dept     = I_dept
            and class    = I_class
            and subclass = I_subclass;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_DUP_EXIST', 'MERCH_HIER_DEFAULT', NULL);
   open C_DUP_EXIST;
   SQL_LIB.SET_MARK('FETCH', 'C_DUP_EXIST', 'MERCH_HIER_DEFAULT', NULL);
   fetch C_DUP_EXIST into L_exists;
   if C_DUP_EXIST%FOUND then
      O_dup_exist := TRUE;
   else
      O_dup_exist := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_DUP_EXIST', 'MERCH_HIER_DEFAULT', NULL);
   close C_DUP_EXIST;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END CHECK_DUP_MERCH_DEFAULTS;
---------------------------------------------------------------------------------
FUNCTION EXPAND_DOWN (O_error_message IN OUT VARCHAR2,
                      I_info          IN     MERCH_HIER_DEFAULT.INFO%TYPE,
                      I_dept          IN     DEPS.DEPT%TYPE,
                      I_class         IN     CLASS.CLASS%TYPE,
                      I_available_ind IN     MERCH_HIER_DEFAULT.AVAILABLE_IND%TYPE,
                      I_required_ind  IN     MERCH_HIER_DEFAULT.REQUIRED_IND%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(64)    := 'MERCH_DEFAULT_SQL.EXPAND_DOWN';
   L_seq_no     MERCH_HIER_DEFAULT.SEQ_NO%TYPE;

   cursor C_EXPAND_SUBCLASS is
    select subclass
      from subclass   sc
     where sc.dept  = I_dept
           and  sc.class = I_class
           and not exists(select mhd.subclass
                            from merch_hier_default mhd
                           where mhd.dept  = I_dept
                            and  mhd.class = I_class
                            and  sc.subclass = mhd.subclass
                            and  mhd.info = I_info);

   cursor C_EXPAND_CLASS is
    select class,subclass
      from subclass   sc
     where sc.dept  = I_dept
           and not exists(select mhd.subclass
                            from merch_hier_default mhd
                           where mhd.dept  = I_dept
                            and  mhd.class = sc.class
                            and  sc.subclass = mhd.subclass
                            and  mhd.info = I_info);
BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT',NULL,NULL,NULL);
      return FALSE;
   end if;

   if I_class is not NULL then
      FOR C_rec in C_EXPAND_SUBCLASS LOOP
         if GET_SEQ_NO(O_error_message,
                       L_seq_no) = FALSE then
            return FALSE;
         end if;
           insert into merch_hier_default(seq_no,
                                          dialog,
                                          info,
                                          dept,
                                          class,
                                          subclass,
                                          available_ind,
                                          required_ind,
                                          create_datetime,
                                          last_update_id,
                                          last_update_date)
                                   values(L_seq_no,
                                          'ITEM',
                                          I_info,
                                          I_dept,
                                          I_class,
                                          C_rec.subclass,
                                          I_available_ind,
                                          I_required_ind,
                                          sysdate,
                                          user,
                                          sysdate);
      END LOOP;
   else
      FOR C_rec in C_EXPAND_CLASS LOOP
         if GET_SEQ_NO(O_error_message,
                       L_seq_no) = FALSE then
            return FALSE;
         end if;
            insert into merch_hier_default(seq_no,
                                           dialog,
                                           info,
                                           dept,
                                           class,
                                           subclass,
                                           available_ind,
                                           required_ind,
                                           create_datetime,
                                           last_update_id,
                                           last_update_date)
                                    values(L_seq_no,
                                           'ITEM',
                                           I_info,
                                           I_dept,
                                           C_rec.class,
                                           C_rec.subclass,
                                           I_available_ind,
                                           I_required_ind,
                                           sysdate,
                                           user,
                                           sysdate);
      END LOOP;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END EXPAND_DOWN;
---------------------------------------------------------------------------------
FUNCTION GET_REQ_INDS(O_error_message           IN OUT VARCHAR2,
                      O_loc_req_ind             IN OUT VARCHAR2,
                      O_season_req_ind          IN OUT VARCHAR2,
                      O_impattrib_req_ind       IN OUT VARCHAR2,
                      O_docs_req_ind            IN OUT VARCHAR2,
                      O_hts_req_ind             IN OUT VARCHAR2,
                      O_tariff_req_ind          IN OUT VARCHAR2,
                      O_exp_req_ind             IN OUT VARCHAR2,
                      O_timeline_req_ind        IN OUT VARCHAR2,
                      O_tickets_req_ind         IN OUT VARCHAR2,
                      O_image_req_ind           IN OUT VARCHAR2,
                      O_sub_tr_items_req_ind    IN OUT VARCHAR2,
                      O_dimension_req_ind       IN OUT VARCHAR2,
                      O_diffs_req_ind           IN OUT VARCHAR2,
                      O_mfg_rec_req_ind         IN OUT VARCHAR2,
                      O_pack_sz_req_ind         IN OUT VARCHAR2,
                      O_retail_lb_req_ind       IN OUT VARCHAR2,
                      O_handling_req_ind        IN OUT VARCHAR2,
                      O_handl_temp_req_ind      IN OUT VARCHAR2,
                      O_wastage_req_ind         IN OUT VARCHAR2,
                      O_comments_req_ind        IN OUT VARCHAR2,
                      I_dept                    IN     DEPS.DEPT%TYPE,
                      I_class                   IN     CLASS.CLASS%TYPE,
                      I_subclass                IN     SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

    L_program           VARCHAR2(64)    := 'MERCH_DEFAULT_SQL.GET_REQ_INDS';
    L_ind               VARCHAR2(1);
    L_req_string        VARCHAR2(64);
    L_length            Number(4) := 1;

   cursor C_CODES is
      select c.code
        from code_detail c
       where c.code_type = 'MHDI'
       order by c.code_seq;

   cursor C_HIER (L_code varchar2) is
      select m.required_ind
        from merch_hier_default m
       where m.info     = L_code
         and m.dept     = I_dept
         and m.class    = I_class
         and m.subclass = I_subclass;

BEGIN

   if (I_dept is NULL
      or I_class is NULL
      or I_subclass is NULL) then
        O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   for code_rec in C_CODES loop
      SQL_LIB.SET_MARK('OPEN', 'C_HIER','MERCH_HIER_DEFAULT', NULL);
      open C_HIER(code_rec.code);
      SQL_LIB.SET_MARK('FETCH', 'C_HIER','MERCH_HIER_DEFAULT', NULL);
      fetch C_HIER into L_ind;
      if C_HIER%NOTFOUND then
         L_ind := 'N';
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_HIER','MERCH_HIER_DEFAULT', NULL);
      close C_HIER;
      ---
      L_req_string := L_req_string || L_ind;
      ---
   end loop;
   ---
   O_loc_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_season_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_impattrib_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_docs_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_hts_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_tariff_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_exp_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_timeline_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_tickets_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_image_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_sub_tr_items_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_dimension_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_diffs_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_mfg_rec_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_pack_sz_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_retail_lb_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_handling_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_handl_temp_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_wastage_req_ind := SUBSTR(L_req_string, L_length,1);
   L_length := L_length + 1;
   O_comments_req_ind := SUBSTR(L_req_string, L_length,1);
return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END GET_REQ_INDS;
---------------------------------------------------------------------------
FUNCTION DEFAULTS_EXIST(O_error_message  IN OUT  VARCHAR2,
                        O_exists         IN OUT  BOOLEAN,
                        I_dept           IN      DEPS.DEPT%TYPE,
                        I_class          IN      CLASS.CLASS%TYPE,
                        I_subclass       IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MERCH_DEFAULT_SQL.DEFAULTS_EXIST';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_HIER is
      select 'Y'
        from merch_hier_default
       where dept = I_dept
         and class = nvl(I_class, class)
         and subclass = nvl(I_subclass, subclass);

BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_HIER','MERCH_HIER_DEFAULT',NULL);
   open C_CHECK_HIER;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_HIER','MERCH_HIER_DEFAULT',NULL);
   fetch C_CHECK_HIER into L_dummy;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_HIER','MERCH_HIER_DEFAULT',NULL);
   close C_CHECK_HIER;
   ---
   if L_dummy = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END DEFAULTS_EXIST;
---------------------------------------------------------------------------
FUNCTION COPY_DEFAULTS(O_error_message  IN OUT  VARCHAR2,
                       I_from_dept      IN      DEPS.DEPT%TYPE,
                       I_from_class     IN      CLASS.CLASS%TYPE,
                       I_from_subclass  IN      SUBCLASS.SUBCLASS%TYPE,
                       I_to_dept        IN      DEPS.DEPT%TYPE,
                       I_to_class       IN      CLASS.CLASS%TYPE,
                       I_to_subclass    IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MERCH_DEFAULT_SQL.COPY_DEFAULTS';

BEGIN
   if I_from_dept is NULL
     or I_from_class is NULL
     or I_from_subclass is NULL
     or I_to_dept is NULL
     or I_to_class is NULL
     or I_to_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'MERCH_HIER_DEFAULT',
                    'Dept: '||to_char(I_from_dept)||' '||
                    'Class: '||to_char(I_from_class)||' '||
                    'Subclass: '||to_char(I_from_subclass));
   delete from merch_hier_default
    where dept = I_to_dept
      and class = I_to_class
      and subclass = I_to_subclass;
   ---
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'MERCH_HIER_DEFAULT',
                    'Dept: '||to_char(I_from_dept)||' '||
                    'Class: '||to_char(I_from_class)||' '||
                    'Subclass: '||to_char(I_from_subclass));
   insert into merch_hier_default(seq_no,
                                  dialog,
                                  info,
                                  dept,
                                  class,
                                  subclass,
                                  available_ind,
                                  required_ind,
                                  create_datetime,
                                  last_update_id,
                                  last_update_date)
                           select merch_defaults_seq_no_sequence.NEXTVAL,
                                  'ITEM',
                                  info,
                                  I_to_dept,
                                  I_to_class,
                                  I_to_subclass,
                                  available_ind,
                                  required_ind,
                                  SYSDATE,
                                  USER,
                                  SYSDATE
                             from merch_hier_default
                            where dept = I_from_dept
                              and class = I_from_class
                              and subclass = I_from_subclass;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END COPY_DEFAULTS;
---------------------------------------------------------------------------
END;
/
