CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRDEPT_SQL AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_DEPARTMENT
   -- Purpose      :  Inserts record on the DEPS table.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DEPARTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_dept_rec        IN       MERCH_SQL.DEPT_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_DEPARTMENT
   -- Purpose      :  Updates record on the DEPS table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DEPARTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_dept_rec        IN       MERCH_SQL.DEPT_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_DEPARTMENT
   -- Purpose      :  Deletes records on the DEPS table
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DEPARTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_dept            IN       DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_VAT
   -- Purpose      :  Inserts records on the VAT_DEPS table
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_VAT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_dept_detail_tbl   IN       MERCH_SQL.VAT_DEPT_DETAIL_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_VAT
   -- Purpose      :  Updates records on the VAT_DEPS table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_VAT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_dept_detail_tbl   IN       MERCH_SQL.VAT_DEPT_DETAIL_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_dept_rec        IN       MERCH_SQL.DEPT_REC,
                 I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDEPT_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XMRCHHRDEPT.LP_cre_type then
      if not CREATE_DEPARTMENT(O_error_message,
                               I_dept_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRDEPT.LP_mod_type then
      if not MODIFY_DEPARTMENT(O_error_message,
                               I_dept_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRDEPT.LP_del_type then
      if not DELETE_DEPARTMENT(O_error_message,
                               I_dept_rec.dept_row.dept) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRDEPT.LP_dtl_cre_type then
      if not CREATE_VAT(O_error_message,
                        I_dept_rec.vat_detail) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRDEPT.LP_dtl_mod_type then
      if not MODIFY_VAT(O_error_message,
                        I_dept_rec.vat_detail) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DEPARTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_dept_rec        IN       MERCH_SQL.DEPT_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDEPT_SQL.CREATE_DEPARTMENT';
   L_type_code    VARCHAR2(1)  := 'D';

BEGIN

   if not MERCH_SQL.INSERT_DEPT(O_error_message,
                                I_dept_rec) then
      return FALSE;
   end if;

   if not STKLEDGR_SQL.STOCK_LEDGER_INSERT(L_type_code,
                                           I_dept_rec.dept_row.dept,
                                           NULL,
                                           NULL,
                                           NULL,
                                           O_error_message) then
      return FALSE;
   end if;

   if I_dept_rec.vat_detail IS NOT NULL and I_dept_rec.vat_detail.count > 0 then
      if not CREATE_VAT(O_error_message,
                        I_dept_rec.vat_detail) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_DEPARTMENT;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DEPARTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_dept_rec        IN       MERCH_SQL.DEPT_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDEPT_SQL.MODIFY_DEPARTMENT';

BEGIN

   if not MERCH_SQL.MODIFY_DEPT(O_error_message,
                                I_dept_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_DEPARTMENT;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DEPARTMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_dept            IN       DEPS.DEPT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDEPT_SQL.DELETE_DEPARTMENT';

BEGIN

   if not MERCH_SQL.DELETE_DEPT(O_error_message,
                                I_dept) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DEPARTMENT;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_VAT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_dept_detail_tbl   IN       MERCH_SQL.VAT_DEPT_DETAIL_TBL)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XMRCHHRDEPT_SQL.CREATE_VAT';
   L_active_date          DATE := GET_VDATE;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_tax_info_tbl         OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   L_tax_info_rec         OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();
   
   L_run_report           BOOLEAN;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if L_system_options_row.default_tax_type = 'SVAT' then
      FOR i in I_dept_detail_tbl.first..I_dept_detail_tbl.last LOOP
         L_tax_info_rec := OBJ_TAX_INFO_REC();
         L_tax_info_tbl.EXTEND;
         L_tax_info_rec.merch_hier_value      := I_dept_detail_tbl(i).dept;
         L_tax_info_rec.from_tax_region       := I_dept_detail_tbl(i).vat_region;
         L_tax_info_rec.tax_code              := I_dept_detail_tbl(i).vat_code;
         L_tax_info_rec.cost_retail_ind       := I_dept_detail_tbl(i).vat_type;
         L_tax_info_rec.reverse_vat_ind       := I_dept_detail_tbl(i).reverse_vat_ind;
         
         L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
      END LOOP;

      if TAX_SQL.INSERT_UPDATE_TAX_DEPS(O_error_message,
                                        L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;

      L_tax_info_rec := OBJ_TAX_INFO_REC();
      L_tax_info_tbl.DELETE;
      L_tax_info_tbl.EXTEND;
      L_tax_info_rec.merch_hier_value      := I_dept_detail_tbl(1).dept;
      L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;

      if TAX_SQL.TAX_DEPT_REGION_EXISTS(O_error_message,
                                        L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;

      L_tax_info_tbl.DELETE;

      FOR i in I_dept_detail_tbl.first..I_dept_detail_tbl.last LOOP
         L_tax_info_rec                            := OBJ_TAX_INFO_REC();
         
         L_tax_info_rec.merch_hier_value           := I_dept_detail_tbl(i).dept;
         L_tax_info_rec.merch_hier_level           := 4;
         L_tax_info_rec.from_tax_region            := I_dept_detail_tbl(i).vat_region;
         L_tax_info_rec.tax_code                   := I_dept_detail_tbl(i).vat_code;
         L_tax_info_rec.cost_retail_ind            := I_dept_detail_tbl(i).vat_type;
         L_tax_info_rec.active_date                := L_active_date;
         
         L_tax_info_tbl.extend;
         L_tax_info_tbl(L_tax_info_tbl.COUNT)      := L_tax_info_rec;
         
      END LOOP;
      
      -- Explode dept to all skus
      if TAX_SQL.GET_TAX_INFO(O_error_message,
                              L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;

      if TAX_SQL.INSERT_UPDATE_TAX_SKU(O_error_message,
                                       L_run_report,
                                       L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_VAT;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_VAT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_dept_detail_tbl   IN       MERCH_SQL.VAT_DEPT_DETAIL_TBL)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XMRCHHRDEPT_SQL.MODIFY_VAT';
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_tax_info_tbl         OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   L_tax_info_rec         OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();
   L_exists               VARCHAR2(1);
   L_dept                 VAT_DEPS.DEPT%TYPE;
   L_tax_region           VAT_DEPS.VAT_REGION%TYPE;
   L_tax_type             VAT_DEPS.VAT_TYPE%TYPE;
   L_tax_code             VAT_DEPS.VAT_CODE%TYPE;
   L_reverse_vat_ind      VAT_DEPS.REVERSE_VAT_IND%TYPE;

   cursor C_CHECK_UPDATE IS
      select 'x'
        from vat_deps vd
       where vd.dept = L_dept
         and vd.vat_region = L_tax_region
         and vd.vat_type = L_tax_type;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if L_system_options_row.default_tax_type ='SVAT' then
      FOR i in I_dept_detail_tbl.first..I_dept_detail_tbl.last LOOP
         L_exists          := NULL;
         L_dept            := I_dept_detail_tbl(i).dept;
         L_tax_code        := I_dept_detail_tbl(i).vat_code;
         L_tax_region      := I_dept_detail_tbl(i).vat_region;
         L_tax_type        := I_dept_detail_tbl(i).vat_type;
         L_reverse_vat_ind := I_dept_detail_tbl(i).reverse_vat_ind;

         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_UPDATE',
                          'VAT_DEPS',
                          NULL);
         open C_CHECK_UPDATE;

         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_UPDATE',
                          'VAT_DEPS',
                          NULL);
         fetch C_CHECK_UPDATE into L_exists;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_UPDATE',
                          'VAT_DEPS',
                          NULL);
         close C_CHECK_UPDATE;

         if L_exists is NOT NULL then
            L_tax_info_rec                       := OBJ_TAX_INFO_REC();
            L_tax_info_tbl.DELETE;
            L_tax_info_tbl.EXTEND;
            L_tax_info_rec.merch_hier_value      := L_dept;
            L_tax_info_rec.tax_code              := L_tax_code;
            L_tax_info_rec.from_tax_region       := L_tax_region;
            L_tax_info_rec.cost_retail_ind       := L_tax_type;
            L_tax_info_rec.reverse_vat_ind       := L_reverse_vat_ind;
            L_tax_info_tbl(L_tax_info_tbl.count) := L_tax_info_rec;

            if TAX_SQL.INSERT_UPDATE_TAX_DEPS(O_error_message,
                                              L_tax_info_tbl) = FALSE then
               return FALSE;
            end if;
         else
            O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_VAT;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRDEPT_SQL;
/