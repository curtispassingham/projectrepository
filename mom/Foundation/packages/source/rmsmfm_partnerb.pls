CREATE OR REPLACE PACKAGE BODY RMSMFM_PARTNER AS

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------

TYPE ROWID_TBL IS TABLE OF ROWID INDEX BY BINARY_INTEGER;
--
LP_error_status VARCHAR2(1) := null;


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK(O_error_msg          OUT VARCHAR2,
                        O_queue_locked       OUT BOOLEAN,
                        I_partner_key_rec IN     PARTNER_KEY_REC)
RETURN BOOLEAN;

FUNCTION PROCESS_QUEUE_RECORD(O_error_msg          OUT VARCHAR2,
                              O_message         IN OUT NOCOPY RIB_OBJECT,
                              O_routing_info    IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id         OUT RIB_BUSOBJID_TBL,
                              O_message_type    IN OUT VARCHAR2,
                              I_partner_key_rec IN     PARTNER_KEY_REC,
                              I_hdr_published   IN     PARTNER_PUB_INFO.PUBLISHED%TYPE,
                              I_rowid           IN     ROWID)
RETURN BOOLEAN;

FUNCTION DELETE_QUEUE_REC(O_error_msg    OUT VARCHAR2,
                          I_rowid     IN     ROWID)
RETURN BOOLEAN;

FUNCTION MAKE_CREATE(O_error_msg          OUT VARCHAR2,
                     O_message         IN OUT NOCOPY RIB_OBJECT,
                     O_routing_info    IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                     I_partner_key_rec IN     PARTNER_KEY_REC,
                     I_rowid           IN     ROWID)
RETURN BOOLEAN;

FUNCTION BUILD_HEADER_OBJECT(O_error_msg           IN OUT VARCHAR2,
                             O_rib_partnerdesc_rec IN OUT NOCOPY "RIB_PartnerDesc_REC",
                             O_routing_info        IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             I_partner_key_rec     IN     PARTNER_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_HEADER_OBJECT(O_error_msg          IN OUT VARCHAR2,
                             O_rib_partnerref_rec IN OUT NOCOPY "RIB_PartnerRef_REC",
                             O_routing_info       IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             I_partner_key_rec    IN     PARTNER_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg              IN OUT VARCHAR2,
                              O_rib_ptnraddrdtl_tbl    IN OUT NOCOPY "RIB_AddrDesc_TBL",
                              O_rib_ptnroudtl_tbl      IN OUT NOCOPY "RIB_PartnerOUDesc_TBL",
                              O_partner_mfqueue_rowid  IN OUT NOCOPY ROWID_TBL,
                              O_partner_mfqueue_size   IN OUT BINARY_INTEGER,
                              O_ptnr_addrdtl_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_ptnr_addrdtl_size      IN OUT BINARY_INTEGER,
                              O_delete_row_ind         IN OUT VARCHAR2,
                              I_message_type           IN     PARTNER_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_partner_key_rec        IN     PARTNER_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_SINGLE_DETAIL(O_error_msg             IN OUT VARCHAR2,
                             O_rib_ptnraddrdtl_tbl   IN OUT NOCOPY "RIB_AddrDesc_TBL",
                             O_rib_ptnraddrdtl_rec   IN OUT NOCOPY "RIB_AddrDesc_REC",
                             I_state_name            IN     STATE.DESCRIPTION%TYPE,
                             I_country_name          IN     COUNTRY.COUNTRY_DESC%TYPE,
                             I_addr_key              IN     ADDR.ADDR_KEY%TYPE,
                             I_addr_type             IN     ADDR.ADDR_TYPE%TYPE,
                             I_primary_ind           IN     ADD_TYPE_MODULE.PRIMARY_IND%TYPE,
                             I_primary_addr_ind      IN     ADDR.PRIMARY_ADDR_IND%TYPE,
                             I_add_1                 IN     ADDR.ADD_1%TYPE,
                             I_add_2                 IN     ADDR.ADD_2%TYPE,
                             I_add_3                 IN     ADDR.ADD_3%TYPE,
                             I_city                  IN     ADDR.CITY%TYPE,
                             I_state                 IN     ADDR.STATE%TYPE,
                             I_country_id            IN     ADDR.COUNTRY_ID%TYPE,
                             I_post                  IN     ADDR.POST%TYPE,
                             I_jurs_code             IN     ADDR.JURISDICTION_CODE%TYPE,
                             I_contact_name          IN     ADDR.CONTACT_NAME%TYPE,
                             I_contact_phone         IN     ADDR.CONTACT_PHONE%TYPE,
                             I_contact_telex         IN     ADDR.CONTACT_TELEX%TYPE,
                             I_contact_fax           IN     ADDR.CONTACT_FAX%TYPE,
                             I_contact_email         IN     ADDR.CONTACT_EMAIL%TYPE,
                             I_oracle_vendor_site_id IN     ADDR.ORACLE_VENDOR_SITE_ID%TYPE,
                             I_county                IN     ADDR.COUNTY%TYPE)
RETURN BOOLEAN;

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg              OUT VARCHAR2,
                                     O_rib_partnerdesc_rec IN OUT NOCOPY "RIB_PartnerDesc_REC",
                                     I_message_type        IN     PARTNER_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_partner_key_rec     IN     PARTNER_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg          IN OUT VARCHAR2,
                                     O_rib_partnerref_rec IN OUT NOCOPY "RIB_PartnerRef_REC",
                                     I_partner_key_rec    IN     PARTNER_KEY_REC)
RETURN BOOLEAN;

PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT VARCHAR2,
                        O_error_msg     IN OUT VARCHAR2,
                        O_message          OUT RIB_OBJECT,
                        O_message_type  IN OUT VARCHAR2,
                        O_bus_obj_id    IN OUT RIB_BUSOBJID_TBL,
                        O_routing_info  IN OUT RIB_ROUTINGINFO_TBL,
                        I_seq_no        IN     PARTNER_MFQUEUE.SEQ_NO%TYPE,
                        I_function_keys IN     PARTNER_KEY_REC);


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg          OUT VARCHAR2,
                I_message_type    IN     VARCHAR2,
                I_partner_key_rec IN     PARTNER_KEY_REC,
                I_publish_ind     IN     ADDR.PUBLISH_IND%TYPE)
RETURN BOOLEAN IS

L_partner_id   PARTNER.PARTNER_ID%TYPE := null;

cursor C_PARTNER_PUB_INFO is
select ppi.partner_id
  from partner_pub_info ppi
 where ppi.partner_type = I_partner_key_rec.partner_type
   and ppi.partner_id = I_partner_key_rec.partner_id
   for update;

BEGIN

   if I_message_type != HDR_ADD then
      open C_PARTNER_PUB_INFO;
      fetch C_PARTNER_PUB_INFO into L_partner_id;
      close C_PARTNER_PUB_INFO;

      if L_partner_id is null then
         O_error_msg := SQL_LIB.CREATE_MSG('PUB_INFO_NOT_FOUND',
                                           'partner_pub_info',
                                           I_partner_key_rec.partner_type || I_partner_key_rec.partner_id,
                                           null);
         return FALSE;
      end if;

      -- delete any prior messages on the queue that will be
      -- redundant once we add our new message to the queue

      if I_message_type = DTL_UPD then

         delete from partner_mfqueue
          where partner_type = I_partner_key_rec.partner_type
            and partner_id = I_partner_key_rec.partner_id
            and addr_key = I_partner_key_rec.addr_key
            and message_type = DTL_UPD;

      elsif I_message_type = DTL_DEL then

          delete from partner_mfqueue
           where partner_type = I_partner_key_rec.partner_type
             and partner_id = I_partner_key_rec.partner_id
             and addr_key = I_partner_key_rec.addr_key;

      elsif I_message_type = HDR_UPD then

         delete from partner_mfqueue
          where partner_type = I_partner_key_rec.partner_type
            and partner_id = I_partner_key_rec.partner_id
            and message_type = HDR_UPD;

      elsif I_message_type = HDR_DEL then

         delete from partner_mfqueue
          where partner_type = I_partner_key_rec.partner_type
            and partner_id = I_partner_key_rec.partner_id;

      end if;

   elsif I_message_type = HDR_ADD then

      open C_PARTNER_PUB_INFO;
      fetch C_PARTNER_PUB_INFO into L_partner_id;
      close C_PARTNER_PUB_INFO;

      if L_partner_id is null then
         insert into partner_pub_info(partner_type,
                                      partner_id,
                                      published)
                               values(I_partner_key_rec.partner_type,
                                      I_partner_key_rec.partner_id,
                                      'N');
      end if;
   end if;

   --

   if I_publish_ind = 'Y' or I_message_type != DTL_DEL then

      insert into partner_mfqueue(seq_no,
                                  partner_type,
                                  partner_id,
                                  addr_key,
                                  message_type,
                                  family,
                                  custom_message_type,
                                  pub_status,
                                  transaction_number,
                                  transaction_time_stamp)
                           values(partner_mfsequence.nextval,
                                  I_partner_key_rec.partner_type,
                                  I_partner_key_rec.partner_id,
                                  I_partner_key_rec.addr_key,
                                  I_message_type,
                                  RMSMFM_PARTNER.FAMILY,
                                  null,
                                  'U',
                                  null,
                                  sysdate);

   end if;

   LP_error_status := API_CODES.SUCCESS;
   return TRUE;

EXCEPTION

   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(LP_error_status,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_PARTNER.ADDTOQ');
      return FALSE;

END ADDTOQ;
--------------------------------------------------------------------------------

PROCEDURE GETNXT(O_status_code     OUT VARCHAR2,
                 O_error_msg       OUT VARCHAR2,
                 O_message_type    OUT VARCHAR2,
                 O_message         OUT RIB_OBJECT,
                 O_bus_obj_id      OUT RIB_BUSOBJID_TBL,
                 O_routing_info    OUT RIB_ROUTINGINFO_TBL,
                 I_num_threads  IN     NUMBER DEFAULT 1,
                 I_thread_val   IN     NUMBER DEFAULT 1) IS

L_mfqueue_row       PARTNER_MFQUEUE%ROWTYPE;
L_partner_key_rec   PARTNER_KEY_REC := null;
L_prtnr_published   PARTNER_PUB_INFO.PUBLISHED%TYPE := null;
L_exists            VARCHAR2(1) := null;
L_hosp              VARCHAR2(1) := 'N';
L_queue_locked      BOOLEAN := FALSE;
L_rowid             ROWID := null;

cursor C_QUEUE is
  select seq_no,
         partner_type,
         partner_id,
         addr_key,
         message_type,
         pub_status,
         rowid
    from partner_mfqueue
   where pub_status = 'U'
order by seq_no;

cursor C_HOSP is
select 'Y'
  from partner_mfqueue
 where partner_type = L_partner_key_rec.partner_type
   and partner_id = L_partner_key_rec.partner_id
   and pub_status = API_CODES.HOSPITAL;

cursor C_GET_PUBLISHED is
select ppi.published
  from partner_pub_info ppi
 where ppi.partner_type = L_partner_key_rec.partner_type
   and ppi.partner_id = L_partner_key_rec.partner_id;

cursor C_ADDR_EXISTS is
  select 'X'
    from addr a,
         add_type_module atm,
         partner_mfqueue pq
   where a.module = atm.module
     and a.addr_type = atm.address_type
     and a.module = 'PTNR'
     and atm.primary_ind = 'Y'
     and a.key_value_1 = pq.partner_type
     and a.key_value_2 = pq.partner_id
     and a.addr_key = pq.addr_key(+)
     and a.key_value_1 = L_partner_key_rec.partner_type
     and a.key_value_2 = L_partner_key_rec.partner_id;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   loop

      O_message := null;

      open C_QUEUE;
      fetch C_QUEUE into L_mfqueue_row.seq_no,
                         L_mfqueue_row.partner_type,
                         L_mfqueue_row.partner_id,
                         L_mfqueue_row.addr_key,
                         L_mfqueue_row.message_type,
                         L_mfqueue_row.pub_status,
                         L_rowid;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_QUEUE;

      O_message_type := L_mfqueue_row.message_type;
      L_partner_key_rec.partner_type := L_mfqueue_row.partner_type;
      L_partner_key_rec.partner_id := L_mfqueue_row.partner_id;
      L_partner_key_rec.addr_key := L_mfqueue_row.addr_key;

      if O_message_type = HDR_ADD then
         -- make sure address record exist before publishing a partnercre message
         open C_ADDR_EXISTS;
         fetch C_ADDR_EXISTS into L_exists;
         close C_ADDR_EXISTS;
      end if;

      if O_message_type = HDR_ADD AND
         L_exists is NULL then
         -- do not publish a partner without addresses
         update partner_mfqueue 
            set pub_status = 'H' 
          where partner_type = L_partner_key_rec.partner_type
            and partner_id = L_partner_key_rec.partner_id;       
         exit;
      end if;

      if LOCK_THE_BLOCK( O_error_msg,
                         L_queue_locked,
                         L_partner_key_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      
      if O_message_type = DTL_ADD THEN
         delete from partner_mfqueue 
               where partner_type = L_partner_key_rec.partner_type
                 and partner_id = L_partner_key_rec.partner_id  
                 and addr_key is null 
                 and pub_status = API_CODES.HOSPITAL;
      end if;     

      if L_queue_locked = FALSE then

         open  C_HOSP;
         fetch C_HOSP into L_hosp;
         close C_HOSP;

         if L_hosp = 'Y' then
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                              null,
                                              null,
                                              null);
            raise PROGRAM_ERROR;
         end if;

         open  C_GET_PUBLISHED;
         fetch C_GET_PUBLISHED into L_prtnr_published;
         close C_GET_PUBLISHED;

         if PROCESS_QUEUE_RECORD(O_error_msg,
                                 O_message,
                                 O_routing_info,
                                 O_bus_obj_id,
                                 O_message_type,
                                 L_partner_key_rec,
                                 L_prtnr_published,
                                 L_rowid) = FALSE then

            raise PROGRAM_ERROR;
         end if;

         if L_mfqueue_row.message_type = HDR_DEL and
         O_message is null then
            null;  -- skip the record if not sending a HDR_DEL message
         else
            exit;  -- Break out of loop after processing record
         end if;
     end if;

   end loop;

   if O_message is null then
      O_status_code := API_CODES.NO_MSG;

   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_partner_key_rec.partner_id);
   end if;

EXCEPTION

   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_mfqueue_row.seq_no,
                    L_partner_key_rec);

END GETNXT;
--------------------------------------------------------------------------------

PROCEDURE PUB_RETRY(O_status_code     OUT VARCHAR2,
                    O_error_msg       OUT VARCHAR2,
                    O_message_type IN OUT VARCHAR2,
                    O_message         OUT RIB_OBJECT,
                    O_bus_obj_id   IN OUT RIB_BUSOBJID_TBL,
                    O_routing_info IN OUT RIB_ROUTINGINFO_TBL,
                    I_ref_object   IN     RIB_OBJECT) IS

L_seq_no            PARTNER_MFQUEUE.SEQ_NO%TYPE := null;
L_prtnr_published   PARTNER_PUB_INFO.PUBLISHED%TYPE := null;
L_rowid             ROWID := null;
L_partner_key_rec   PARTNER_KEY_REC;
L_mfqueue_row       PARTNER_MFQUEUE%ROWTYPE;
L_queue_locked      BOOLEAN := FALSE;

cursor C_RETRY_QUEUE is
select q.partner_type,
       q.partner_id,
       q.addr_key,
       ppi.published,
       q.message_type,
       q.pub_status,
       q.rowid
  from partner_mfqueue q,
       partner_pub_info ppi
 where q.seq_no = L_seq_no
   and q.partner_type = ppi.partner_type
   and q.partner_id = ppi.partner_id;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   -- get info from routing info
   -- assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;
   O_message := null;

   -- get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_mfqueue_row.partner_type,
                            L_mfqueue_row.partner_id,
                            L_mfqueue_row.addr_key,
                            L_prtnr_published,
                            L_mfqueue_row.message_type,
                            L_mfqueue_row.pub_status,
                            L_rowid;
   close C_RETRY_QUEUE;

   if L_mfqueue_row.partner_id is null then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   L_partner_key_rec.partner_type := L_mfqueue_row.partner_type;
   L_partner_key_rec.partner_id := L_mfqueue_row.partner_id;
   L_partner_key_rec.addr_key := L_mfqueue_row.addr_key;

   if LOCK_THE_BLOCK(O_error_msg,
                     L_queue_locked,
                     L_partner_key_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then
      if PROCESS_QUEUE_RECORD(O_error_msg,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              L_mfqueue_row.message_type,
                              L_partner_key_rec,
                              L_prtnr_published,
                              L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      O_message_type := L_mfqueue_row.message_type;

      if O_message is null then
         O_status_code := API_CODES.NO_MSG;
      else
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_partner_key_rec.partner_id);
      end if;
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION

   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_seq_no,
                    L_partner_key_rec);

END PUB_RETRY;


--------------------------------------------------------------------------------
-- PRIVATE PROCEDURES
--------------------------------------------------------------------------------

FUNCTION PROCESS_QUEUE_RECORD(O_error_msg          OUT VARCHAR2,
                              O_message         IN OUT NOCOPY RIB_OBJECT,
                              O_routing_info    IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id         OUT RIB_BUSOBJID_TBL,
                              O_message_type    IN OUT VARCHAR2,
                              I_partner_key_rec IN     PARTNER_KEY_REC,
                              I_hdr_published   IN     PARTNER_PUB_INFO.PUBLISHED%TYPE,
                              I_rowid           IN     ROWID)
RETURN BOOLEAN IS

L_rib_partnerdesc_rec   "RIB_PartnerDesc_REC" := null;
L_rib_partnerref_rec    "RIB_PartnerRef_REC" := null;
L_partner_id            PARTNER.PARTNER_ID%TYPE := null;

cursor C_PARTNER_PUB_INFO is
select ppi.partner_id
  from partner_pub_info ppi
 where ppi.partner_type = I_partner_key_rec.partner_type
   and ppi.partner_id = I_partner_key_rec.partner_id
   for update;

BEGIN

   if O_message_type = HDR_DEL then
      if I_hdr_published = 'Y' then
         if BUILD_HEADER_OBJECT(O_error_msg,
                                L_rib_partnerref_rec,
                                O_routing_info,
                                I_partner_key_rec) = FALSE then
            return FALSE;
         end if;
         O_message := L_rib_partnerref_rec;
      end if;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_rowid) = FALSE then
         return FALSE;
      end if;

      open C_PARTNER_PUB_INFO;
      fetch C_PARTNER_PUB_INFO into L_partner_id;
      close C_PARTNER_PUB_INFO;

      delete from partner_pub_info ppi
       where not exists (select 'x'
                           from partner_mfqueue pm
                          where pm.partner_id = ppi.partner_id
                            and pm.partner_type = ppi.partner_type)
         and ppi.partner_id = I_partner_key_rec.partner_id;

   elsif I_hdr_published in ('N','I') then
      if I_hdr_published = 'N' then
         O_message_type := HDR_ADD;
      else
         O_message_type := DTL_ADD;
      end if;

      if MAKE_CREATE(O_error_msg,
                     O_message,
                     O_routing_info,
                     I_partner_key_rec,
                     I_rowid) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type in (HDR_UPD, DTL_ADD, DTL_UPD) then
      if BUILD_HEADER_OBJECT(O_error_msg,
                             L_rib_partnerdesc_rec,
                             O_routing_info,
                             I_partner_key_rec) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg,
                                     L_rib_partnerdesc_rec,
                                     O_message_type,
                                     I_partner_key_rec) = FALSE then
         return FALSE;
      end if;

      if O_message_type = HDR_UPD then
         delete from partner_mfqueue
          where rowid = I_rowid;
      end if;

      O_message := L_rib_partnerdesc_rec;

   elsif O_message_type = DTL_DEL then
      if BUILD_HEADER_OBJECT(O_error_msg,
                             L_rib_partnerref_rec,
                             O_routing_info,
                             I_partner_key_rec) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_DELETE_OBJECTS(O_error_msg,
                                     L_rib_partnerref_rec,
                                     I_partner_key_rec) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_partnerref_rec;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.PROCESS_QUEUE_RECORD',
                                        to_char(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_RECORD;
--------------------------------------------------------------------------------

FUNCTION MAKE_CREATE(O_error_msg          OUT VARCHAR2,
                     O_message         IN OUT NOCOPY RIB_OBJECT,
                     O_routing_info    IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                     I_partner_key_rec IN     PARTNER_KEY_REC,
                     I_rowid           IN     ROWID)
RETURN BOOLEAN IS

L_rib_partnerdesc_rec     "RIB_PartnerDesc_REC" := null;
L_rib_ptnraddrdtl_tbl     "RIB_AddrDesc_TBL" := null;
L_rib_ptnroudtl_tbl       "RIB_PartnerOUDesc_TBL" := null;
L_partner_mfqueue_rowid   ROWID_TBL;
L_partner_mfqueue_size    BINARY_INTEGER := 0;
L_addr_rowid              ROWID_TBL;
L_addr_size               BINARY_INTEGER := 0;
L_delete_rowid_ind        VARCHAR2(1) := 'Y';

BEGIN

   if BUILD_HEADER_OBJECT(O_error_msg,
                          L_rib_partnerdesc_rec,
                          O_routing_info,
                          I_partner_key_rec) = FALSE then
      return FALSE;
   end if;

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           L_rib_ptnraddrdtl_tbl,
                           L_rib_ptnroudtl_tbl,
                           L_partner_mfqueue_rowid,
                           L_partner_mfqueue_size,
                           L_addr_rowid,
                           L_addr_size,
                           L_delete_rowid_ind,
                           null,
                           I_partner_key_rec) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_delete_rowid_ind = 'Y' then
      L_partner_mfqueue_size := L_partner_mfqueue_size +1;
      L_partner_mfqueue_rowid(L_partner_mfqueue_size) := I_rowid;
      L_rib_partnerdesc_rec.addrdesc_tbl := null;
   end if;

   update partner_pub_info
      set published = decode(L_delete_rowid_ind, 'Y', 'Y', 'I')
    where partner_type = I_partner_key_rec.partner_type
      and partner_id = I_partner_key_rec.partner_id;

   L_rib_partnerdesc_rec.addrdesc_tbl := L_rib_ptnraddrdtl_tbl;
   L_rib_partnerdesc_rec.partneroudesc_tbl := L_rib_ptnroudtl_tbl;

   if L_partner_mfqueue_size > 0 then
      forall i in 1 .. L_partner_mfqueue_size
      delete from partner_mfqueue
       where rowid = L_partner_mfqueue_rowid(i);
   end if;

   if L_addr_size > 0 then
      forall i in 1 .. L_addr_size
      update addr
         set publish_ind = 'Y'
       where rowid = L_addr_rowid(i);
   end if;

   O_message := L_rib_partnerdesc_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.MAKE_CREATE',
                                        to_char(SQLCODE));
      return FALSE;

END MAKE_CREATE;
--------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT(O_error_msg           IN OUT VARCHAR2,
                             O_rib_partnerdesc_rec IN OUT NOCOPY "RIB_PartnerDesc_REC",
                             O_routing_info        IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             I_partner_key_rec     IN     PARTNER_KEY_REC)
RETURN BOOLEAN IS

L_partner_row       PARTNER%ROWTYPE;
L_rib_routing_rec   RIB_ROUTINGINFO_REC := null;

cursor C_PARTNER is
select *
  from partner p
 where p.partner_type = I_partner_key_rec.partner_type
   and p.partner_id = I_partner_key_rec.partner_id;

BEGIN

   open C_PARTNER;
   fetch C_PARTNER into L_partner_row;
   close C_PARTNER;

   if L_partner_row.partner_id is null then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_PARTNER_PUB',
                                        I_partner_key_rec.partner_type || I_partner_key_rec.partner_id,
                                        null,
                                        null);
      return FALSE;
   end if;

   O_rib_partnerdesc_rec := "RIB_PartnerDesc_REC"(0,
                                                L_partner_row.partner_type,
                                                 L_partner_row.partner_id,
                                                 L_partner_row.partner_desc,
                                                 L_partner_row.currency_code,
                                                 L_partner_row.lang,
                                                 L_partner_row.status,
                                                 L_partner_row.contact_name,
                                                 L_partner_row.contact_phone,
                                                 L_partner_row.contact_fax,
                                                 L_partner_row.contact_telex,
                                                 L_partner_row.contact_email,
                                                 L_partner_row.mfg_id,
                                                 L_partner_row.principle_country_id,
                                                 L_partner_row.line_of_credit,
                                                 L_partner_row.outstand_credit,
                                                 L_partner_row.open_credit,
                                                 L_partner_row.ytd_credit,
                                                 L_partner_row.ytd_drawdowns,
                                                 L_partner_row.tax_id,
                                                 L_partner_row.terms,
                                                 L_partner_row.service_perf_req_ind,
                                                 L_partner_row.invc_pay_loc,
                                                 L_partner_row.invc_receive_loc,
                                                 L_partner_row.import_country_id,
                                                 L_partner_row.primary_ia_ind,
                                                 null,
                                                 null);

   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('Partner_type',
                                            L_partner_row.partner_type,
                                            'Partner_id',
                                            L_partner_row.partner_id,
                                            null,
                                            null);
   O_routing_info.extend;
   O_routing_info(O_routing_info.count) := L_rib_routing_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.BUILD_HEADER_OBJECT',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;
--------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT(O_error_msg             IN OUT VARCHAR2,
                             O_rib_partnerref_rec    IN OUT NOCOPY "RIB_PartnerRef_REC",
                             O_routing_info          IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             I_partner_key_rec       IN     PARTNER_KEY_REC)
RETURN BOOLEAN IS

L_rib_routing_rec   RIB_ROUTINGINFO_REC := null;

BEGIN

   O_rib_partnerref_rec := "RIB_PartnerRef_REC"(0,
                                              I_partner_key_rec.partner_type,
                                              I_partner_key_rec.partner_id,
                                              null);

   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('Partner_type',
                                            I_partner_key_rec.partner_type,
                                            'Partner_id',
                                            I_partner_key_rec.partner_id,
                                            null,
                                            null);

   O_routing_info.extend;
   O_routing_info(O_routing_info.count) := L_rib_routing_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.BUILD_HEADER_OBJECT',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg              IN OUT VARCHAR2,
                              O_rib_ptnraddrdtl_tbl    IN OUT NOCOPY "RIB_AddrDesc_TBL",
                              O_rib_ptnroudtl_tbl      IN OUT NOCOPY "RIB_PartnerOUDesc_TBL",
                              O_partner_mfqueue_rowid  IN OUT NOCOPY ROWID_TBL,
                              O_partner_mfqueue_size   IN OUT BINARY_INTEGER,
                              O_ptnr_addrdtl_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_ptnr_addrdtl_size      IN OUT BINARY_INTEGER,
                              O_delete_row_ind         IN OUT VARCHAR2,
                              I_message_type           IN     PARTNER_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_partner_key_rec        IN     PARTNER_KEY_REC)
RETURN BOOLEAN IS

L_rib_partnerdtl_rec    "RIB_AddrDesc_REC" := null;
L_rib_ptnraddrdtl_tbl   "RIB_AddrDesc_TBL" := null;
L_rib_ptnroudtl_rec     "RIB_PartnerOUDesc_REC" := null;
L_rib_ptnroudtl_tbl     "RIB_PartnerOUDesc_TBL" := null;
L_records_found         BOOLEAN := FALSE;
L_max_details           RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 12;
L_num_threads           RIB_SETTINGS.NUM_THREADS%TYPE := null;
L_min_time_lag          RIB_SETTINGS.MINUTES_TIME_LAG%TYPE := null;
L_status_code           VARCHAR2(1) := null;
L_details_processed     RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 0;

cursor C_ADDR_MC is
  select s.description,
         c.country_desc,
         a.addr_key,
         a.addr_type,
         atm.primary_ind,
         a.primary_addr_ind,
         a.add_1,
         a.add_2,
         a.add_3,
         a.city,
         a.state,
         a.country_id,
         a.post,
         a.jurisdiction_code,
         a.contact_name,
         a.contact_phone,
         a.contact_telex,
         a.contact_fax,
         a.contact_email,
         a.oracle_vendor_site_id,
         a.county,
         a.rowid addr_rowid,
         pq.rowid pq_rowid
    from addr a,
         add_type_module atm,
         partner_mfqueue pq,
         state s,
         country c
   where a.module = atm.module
     and a.addr_type = atm.address_type
     and a.module = 'PTNR'
     and a.publish_ind = 'N'
     and a.state = s.state (+)
     and a.country_id = c.country_id
     and a.country_id =s.country_id (+)
     and a.key_value_1 = pq.partner_type (+)
     and a.key_value_2 = pq.partner_id (+)
     and a.addr_key = pq.addr_key (+)
     and a.key_value_1 = I_partner_key_rec.partner_type
     and a.key_value_2 = I_partner_key_rec.partner_id
     and (a.jurisdiction_code is NULL
          or exists (select 'x'
                       from country_tax_jurisdiction ctj
                      where ctj.jurisdiction_code = a.jurisdiction_code
                        and ctj.state = a.state
                        and ctj.country_id = a.country_id))
order by atm.primary_ind desc,
         a.primary_addr_ind desc
     for update of a.publish_ind nowait;

CURSOR C_ADDR IS
select s.description,
       c.country_desc,
       a.addr_key,
       a.addr_type,
       atm.primary_ind,
       a.primary_addr_ind,
       a.add_1,
       a.add_2,
       a.add_3,
       a.city,
       a.state,
       a.country_id,
       a.post,
       a.jurisdiction_code,
       a.contact_name,
       a.contact_phone,
       a.contact_telex,
       a.contact_fax,
       a.contact_email,
       a.oracle_vendor_site_id,
       a.county,
       a.rowid addr_rowid,
       pq.rowid pq_rowid
  from addr a,
       add_type_module atm,
       partner_mfqueue pq,
       state s,
       country c
 where a.module = atm.module
   and a.addr_type = atm.address_type
   and a.module = 'PTNR'
   and a.state = s.state (+)
   and a.country_id = c.country_id
   and a.country_id =s.country_id (+)
   and a.addr_key = pq.addr_key (+)
   and a.addr_key = I_partner_key_rec.addr_key
   and pq.message_type (+) = I_message_type
   and (a.jurisdiction_code is NULL
        or exists (select 'x'
                     from country_tax_jurisdiction ctj
                    where ctj.jurisdiction_code = a.jurisdiction_code
                      and ctj.state = a.state
                      and ctj.country_id = a.country_id))
   for update of a.publish_ind nowait;

CURSOR C_ADDR_HDR_UPD IS
select s.description,
       c.country_desc,
       a.addr_key,
       a.addr_type,
       atm.primary_ind,
       a.primary_addr_ind,
       a.add_1,
       a.add_2,
       a.add_3,
       a.city,
       a.state,
       a.country_id,
       a.post,
       a.jurisdiction_code,
       a.contact_name,
       a.contact_phone,
       a.contact_telex,
       a.contact_fax,
       a.contact_email,
       a.oracle_vendor_site_id,
       a.county,
       a.rowid addr_rowid
  from addr a,
       add_type_module atm,
       state s,
       country c
 where a.module = atm.module
   and a.addr_type = atm.address_type
   and a.module = 'PTNR'
   and a.key_value_1 = I_partner_key_rec.partner_type
   and a.key_value_2 = I_partner_key_rec.partner_id
   and atm.primary_ind = 'Y'
   and a.primary_addr_ind = 'Y'
   and a.state = s.state (+)
   and a.country_id = c.country_id
   and a.country_id =s.country_id (+)
   and (a.jurisdiction_code is NULL
        or exists (select 'x'
                     from country_tax_jurisdiction ctj
                    where ctj.jurisdiction_code = a.jurisdiction_code
                      and ctj.state = a.state
                      and ctj.country_id = a.country_id));

CURSOR C_ORG_UNIT_ID IS
select org_unit_id
  from partner
 where partner_type = I_partner_key_rec.partner_type
   and partner_id = I_partner_key_rec.partner_id;

BEGIN

   if O_rib_ptnraddrdtl_tbl is null then
      L_rib_ptnraddrdtl_tbl := "RIB_AddrDesc_TBL"();
   else
      L_rib_ptnraddrdtl_tbl := O_rib_ptnraddrdtl_tbl;
   end if;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_PARTNER.FAMILY);

   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   if I_message_type is null then
      for rec in C_ADDR_MC loop
         L_records_found := TRUE;
         if L_details_processed >= L_max_details then
            O_delete_row_ind := 'N';
            exit;
         end if;

         if BUILD_SINGLE_DETAIL(O_error_msg,
                                L_rib_ptnraddrdtl_tbl,
                                L_rib_partnerdtl_rec,
                                rec.description,
                                rec.country_desc,
                                rec.addr_key,
                                rec.addr_type,
                                rec.primary_ind,
                                rec.primary_addr_ind,
                                rec.add_1,
                                rec.add_2,
                                rec.add_3,
                                rec.city,
                                rec.state,
                                rec.country_id,
                                rec.post,
                                rec.jurisdiction_code,
                                rec.contact_name,
                                rec.contact_phone,
                                rec.contact_telex,
                                rec.contact_fax,
                                rec.contact_email,
                                rec.oracle_vendor_site_id,
                                rec.county) = FALSE then
            return FALSE;
         end if;

         O_ptnr_addrdtl_size := O_ptnr_addrdtl_size + 1;
         O_ptnr_addrdtl_rowid_tbl(O_ptnr_addrdtl_size) := rec.addr_rowid;

         if rec.pq_rowid is not null then
            O_partner_mfqueue_size := O_partner_mfqueue_size + 1;
            O_partner_mfqueue_rowid(O_partner_mfqueue_size) := rec.pq_rowid;
         end if;

         L_details_processed := L_details_processed + 1;
      end loop;
   elsif I_message_type = HDR_UPD then
      for rec in C_ADDR_HDR_UPD loop
         L_records_found := TRUE;

         if BUILD_SINGLE_DETAIL(O_error_msg,
                                L_rib_ptnraddrdtl_tbl,
                                L_rib_partnerdtl_rec,
                                rec.description,
                                rec.country_desc,
                                rec.addr_key,
                                rec.addr_type,
                                rec.primary_ind,
                                rec.primary_addr_ind,
                                rec.add_1,
                                rec.add_2,
                                rec.add_3,
                                rec.city,
                                rec.state,
                                rec.country_id,
                                rec.post,
                                rec.jurisdiction_code,
                                rec.contact_name,
                                rec.contact_phone,
                                rec.contact_telex,
                                rec.contact_fax,
                                rec.contact_email,
                                rec.oracle_vendor_site_id,
                                rec.county) = FALSE then
            return FALSE;
         end if;

         O_ptnr_addrdtl_size := O_ptnr_addrdtl_size + 1;
         O_ptnr_addrdtl_rowid_tbl(O_ptnr_addrdtl_size) := rec.addr_rowid;

         L_details_processed := L_details_processed + 1;
         exit;
      end loop;
   else
      for rec in C_ADDR loop
         L_records_found := TRUE;
         if L_details_processed >= L_max_details then
            O_delete_row_ind := 'N';
            exit;
         end if;

         if BUILD_SINGLE_DETAIL(O_error_msg,
                                L_rib_ptnraddrdtl_tbl,
                                L_rib_partnerdtl_rec,
                                rec.description,
                                rec.country_desc,
                                rec.addr_key,
                                rec.addr_type,
                                rec.primary_ind,
                                rec.primary_addr_ind,
                                rec.add_1,
                                rec.add_2,
                                rec.add_3,
                                rec.city,
                                rec.state,
                                rec.country_id,
                                rec.post,
                                rec.jurisdiction_code,
                                rec.contact_name,
                                rec.contact_phone,
                                rec.contact_telex,
                                rec.contact_fax,
                                rec.contact_email,
                                rec.oracle_vendor_site_id,
                                rec.county) = FALSE then
            return FALSE;
         end if;

         O_ptnr_addrdtl_size := O_ptnr_addrdtl_size + 1;
         O_ptnr_addrdtl_rowid_tbl(O_ptnr_addrdtl_size) := rec.addr_rowid;

         if rec.pq_rowid is not null then
            O_partner_mfqueue_size := O_partner_mfqueue_size + 1;
            O_partner_mfqueue_rowid(O_partner_mfqueue_size) := rec.pq_rowid;
         end if;

         L_details_processed := L_details_processed + 1;
      end loop;

      -- if no data found in cursor, raise error
      if not L_records_found then
         O_error_msg := SQL_LIB.CREATE_MSG('NO_PARTNER_PUB',
                                           I_partner_key_rec.partner_type,
                                           I_partner_key_rec.partner_id,
                                           null);
         return FALSE;
      end if;
   end if;

   if L_rib_ptnraddrdtl_tbl.count > 0 then
      O_rib_ptnraddrdtl_tbl := L_rib_ptnraddrdtl_tbl;
   else
      O_rib_ptnraddrdtl_tbl := null;
   end if;

   if O_rib_ptnroudtl_tbl is null then
      L_rib_ptnroudtl_tbl := "RIB_PartnerOUDesc_TBL"();
   else
      L_rib_ptnroudtl_tbl := O_rib_ptnroudtl_tbl;
   end if;

   for rec in C_ORG_UNIT_ID loop
      L_rib_ptnroudtl_rec := "RIB_PartnerOUDesc_REC"(0,
                                                     rec.org_unit_id);

      L_rib_ptnroudtl_tbl.extend;
      L_rib_ptnroudtl_tbl(L_rib_ptnroudtl_tbl.count) := L_rib_ptnroudtl_rec;

   end loop;

   O_rib_ptnroudtl_tbl := L_rib_ptnroudtl_tbl;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.BUILD_DETAIL_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_OBJECTS;
--------------------------------------------------------------------------------

FUNCTION BUILD_SINGLE_DETAIL(O_error_msg             IN OUT VARCHAR2,
                             O_rib_ptnraddrdtl_tbl   IN OUT NOCOPY "RIB_AddrDesc_TBL",
                             O_rib_ptnraddrdtl_rec   IN OUT NOCOPY "RIB_AddrDesc_REC",
                             I_state_name            IN     STATE.DESCRIPTION%TYPE,
                             I_country_name          IN     COUNTRY.COUNTRY_DESC%TYPE,
                             I_addr_key              IN     ADDR.ADDR_KEY%TYPE,
                             I_addr_type             IN     ADDR.ADDR_TYPE%TYPE,
                             I_primary_ind           IN     ADD_TYPE_MODULE.PRIMARY_IND%TYPE,
                             I_primary_addr_ind      IN     ADDR.PRIMARY_ADDR_IND%TYPE,
                             I_add_1                 IN     ADDR.ADD_1%TYPE,
                             I_add_2                 IN     ADDR.ADD_2%TYPE,
                             I_add_3                 IN     ADDR.ADD_3%TYPE,
                             I_city                  IN     ADDR.CITY%TYPE,
                             I_state                 IN     ADDR.STATE%TYPE,
                             I_country_id            IN     ADDR.COUNTRY_ID%TYPE,
                             I_post                  IN     ADDR.POST%TYPE,
                             I_jurs_code             IN     ADDR.JURISDICTION_CODE%TYPE,
                             I_contact_name          IN     ADDR.CONTACT_NAME%TYPE,
                             I_contact_phone         IN     ADDR.CONTACT_PHONE%TYPE,
                             I_contact_telex         IN     ADDR.CONTACT_TELEX%TYPE,
                             I_contact_fax           IN     ADDR.CONTACT_FAX%TYPE,
                             I_contact_email         IN     ADDR.CONTACT_EMAIL%TYPE,
                             I_oracle_vendor_site_id IN     ADDR.ORACLE_VENDOR_SITE_ID%TYPE,
                             I_county                IN     ADDR.COUNTY%TYPE)
RETURN BOOLEAN IS

BEGIN

   O_rib_ptnraddrdtl_rec := "RIB_AddrDesc_REC"(0,
                                               NULL,
                                               I_state_name,
                                               I_country_name,
                                               I_addr_key,
                                               I_addr_type,
                                               I_primary_ind,
                                               I_primary_addr_ind,
                                               I_add_1,
                                               I_add_2,
                                               I_add_3,
                                               I_city,
                                               I_state,
                                               I_country_id,
                                               I_post,
                                               I_contact_name,
                                               I_contact_phone,
                                               I_contact_telex,
                                               I_contact_fax,
                                               I_contact_email,
                                               I_oracle_vendor_site_id,
                                               I_county,
                                               I_jurs_code);

   O_rib_ptnraddrdtl_tbl.extend;
   O_rib_ptnraddrdtl_tbl(O_rib_ptnraddrdtl_tbl.count) := O_rib_ptnraddrdtl_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.BUILD_SINGLE_DETAIL',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_SINGLE_DETAIL;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg              OUT VARCHAR2,
                                     O_rib_partnerdesc_rec IN OUT NOCOPY "RIB_PartnerDesc_REC",
                                     I_message_type        IN     PARTNER_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_partner_key_rec     IN     PARTNER_KEY_REC)
RETURN BOOLEAN IS

L_partner_mfqueue_rowid   ROWID_TBL;
L_partner_mfqueue_size    BINARY_INTEGER := 0;
L_addr_rowid              ROWID_TBL;
L_addr_size               BINARY_INTEGER := 0;
L_delete_row_ind          VARCHAR2(1) := 'Y';

BEGIN

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           O_rib_partnerdesc_rec.AddrDesc_tbl,
                           O_rib_partnerdesc_rec.PartnerOUDesc_TBL,
                           L_partner_mfqueue_rowid,
                           L_partner_mfqueue_size,
                           L_addr_rowid,
                           L_addr_size,
                           L_delete_row_ind,
                           I_message_type,
                           I_partner_key_rec) = FALSE then
      return FALSE;
   end if;

   if L_addr_size > 0 then
      forall i in 1 .. L_addr_size
      update addr
         set publish_ind = 'Y'
       where rowid = L_addr_rowid(i);
   end if;

   if L_partner_mfqueue_size > 0 then
      forall i in 1 .. L_partner_mfqueue_size
      delete from partner_mfqueue
       where rowid = L_partner_mfqueue_rowid(i);
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.BUILD_DETAIL_CHANGE_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_CHANGE_OBJECTS;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg          IN OUT VARCHAR2,
                                     O_rib_partnerref_rec IN OUT NOCOPY "RIB_PartnerRef_REC",
                                     I_partner_key_rec    IN     PARTNER_KEY_REC)
RETURN BOOLEAN IS

L_partner_mfqueue_rowid   ROWID_TBL;
L_partner_mfqueue_size    BINARY_INTEGER := 0;
L_rib_ptnraddrref_tbl     "RIB_AddrRef_TBL" := null;
L_rib_ptnraddrref_rec     "RIB_AddrRef_REC" := null;
--
L_max_details             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 12;
L_num_threads             RIB_SETTINGS.NUM_THREADS%TYPE := null;
L_min_time_lag            RIB_SETTINGS.MINUTES_TIME_LAG%TYPE := null;
L_status_code             VARCHAR2(1) := null;
L_details_processed       RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 0;

cursor C_ADDR_DELETE is
select pq.addr_key,
       pq.rowid pq_rowid
  from partner_mfqueue pq
 where pq.partner_type = I_partner_key_rec.partner_type
   and pq.partner_id = I_partner_key_rec.partner_id
   and pq.message_type = DTL_DEL
   and rownum <= L_max_details;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_PARTNER.FAMILY);

   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   L_rib_ptnraddrref_tbl := "RIB_AddrRef_TBL"();

   for rec in C_ADDR_DELETE loop
      if rec.pq_rowid is not null then
         L_rib_ptnraddrref_rec := "RIB_AddrRef_REC"(0, rec.addr_key);
         L_rib_ptnraddrref_tbl.extend;
         L_rib_ptnraddrref_tbl(L_rib_ptnraddrref_tbl.count) := L_rib_ptnraddrref_rec;

         L_partner_mfqueue_size := L_partner_mfqueue_size + 1;
         L_partner_mfqueue_rowid(L_partner_mfqueue_size) := rec.pq_rowid;
      end if;
   end loop;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_partner_mfqueue_size > 0 then
      forall i in 1 .. L_partner_mfqueue_size
      delete from partner_mfqueue
       where rowid = L_partner_mfqueue_rowid(i);
   end if;

   if L_rib_ptnraddrref_tbl.count > 0 then
      O_rib_partnerref_rec.addrref_tbl := L_rib_ptnraddrref_tbl;
   else
      O_rib_partnerref_rec.addrref_tbl := null;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.BUILD_DETAIL_DELETE_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_DELETE_OBJECTS;
--------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK(O_error_msg          OUT VARCHAR2,
                        O_queue_locked       OUT BOOLEAN,
                        I_partner_key_rec IN     PARTNER_KEY_REC)
RETURN BOOLEAN IS

L_table         VARCHAR2(30) := 'partner_mfqueue';
L_key1          VARCHAR2(100) := I_partner_key_rec.partner_type;
L_key2          VARCHAR2(100) := I_partner_key_rec.partner_id;
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

cursor C_LOCK_QUEUE is
select 'x'
  from partner_mfqueue
 where partner_type = L_key1
   and partner_id = L_key2
   for update nowait;

BEGIN

   O_queue_locked := FALSE;

   open C_LOCK_QUEUE;
   close C_LOCK_QUEUE;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_queue_locked := TRUE;
      return TRUE;

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.LOCK_THE_BLOCK',
                                        to_char(SQLCODE));
      return FALSE;

END LOCK_THE_BLOCK;
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT VARCHAR2,
                        O_error_msg     IN OUT VARCHAR2,
                        O_message          OUT RIB_OBJECT,
                        O_message_type  IN OUT VARCHAR2,
                        O_bus_obj_id    IN OUT RIB_BUSOBJID_TBL,
                        O_routing_info  IN OUT RIB_ROUTINGINFO_TBL,
                        I_seq_no        IN     PARTNER_MFQUEUE.SEQ_NO%TYPE,
                        I_function_keys IN     PARTNER_KEY_REC) IS

L_rib_partnerdtlref_rec   "RIB_AddrRef_REC";
L_rib_partnerdtlref_tbl   "RIB_AddrRef_TBL";
L_rib_partnerref_rec      "RIB_PartnerRef_REC";
L_error_type              VARCHAR2(5) := null;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then
      O_bus_obj_id := RIB_BUSOBJID_TBL(I_function_keys.partner_type,
                                       I_function_keys.partner_id);
      O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                I_seq_no,
                                                                null,
                                                                null,
                                                                null,
                                                                null));
      L_rib_partnerdtlref_rec := "RIB_AddrRef_REC"(0,
                                                 I_function_keys.addr_key);
      L_rib_partnerdtlref_tbl := "RIB_AddrRef_TBL"(L_rib_partnerdtlref_rec);
      L_rib_partnerref_rec := "RIB_PartnerRef_REC"(0,
                                                 I_function_keys.partner_type,
                                                 I_function_keys.partner_id,
                                                 L_rib_partnerdtlref_tbl);
      O_message := L_rib_partnerref_rec;

      update partner_mfqueue
         set pub_status = LP_error_status
       where seq_no = I_seq_no;
   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type, O_error_msg);

EXCEPTION

   when OTHERS then
      O_status_code := API_CODES.UNHANDLED_ERROR;
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.HANDLE_ERRORS',
                                        to_char(SQLCODE));

END HANDLE_ERRORS;
--------------------------------------------------------------------------------

FUNCTION DELETE_QUEUE_REC(O_error_msg    OUT VARCHAR2,
                          I_rowid     IN     ROWID)
RETURN BOOLEAN IS

BEGIN

   delete from partner_mfqueue
    where rowid = I_rowid;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_PARTNER.DELETE_QUEUE_REC',
                                        to_char(SQLCODE));
      return FALSE;

END DELETE_QUEUE_REC;
--------------------------------------------------------------------------------

END RMSMFM_PARTNER;
/
