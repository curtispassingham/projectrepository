create or replace PACKAGE CORESVC_STATE AUTHID CURRENT_USER AS
   template_key               CONSTANT VARCHAR2(255):='STATE_DATA';
   action_new                 VARCHAR2(25)          := 'NEW';
   action_mod                 VARCHAR2(25)          := 'MOD';
   action_del                 VARCHAR2(25)          := 'DEL';
   STATE_TL_sheet             VARCHAR2(255)         := 'STATE_TL';
   STATE_TL$Action            NUMBER                :=1;
   STATE_TL$DESCRIPTION       NUMBER                :=5;
   STATE_TL$COUNTRY_ID        NUMBER                :=4;
   STATE_TL$STATE             NUMBER                :=3;
   STATE_TL$LANG              NUMBER                :=2;
   sheet_name_trans           S9T_PKG.trans_map_typ;
   action_column              VARCHAR2(255)         := 'ACTION';
   template_category          CODE_DETAIL.CODE%TYPE := 'RMSADM';
   TYPE STATE_TL_rec_tab IS TABLE OF STATE_TL%ROWTYPE;
   STATE_sheet                VARCHAR2(255)         := 'STATE';
   STATE$Action               NUMBER                :=1;
   STATE$COUNTRY_ID           NUMBER                :=4;
   STATE$DESCRIPTION          NUMBER                :=3;
   STATE$STATE                NUMBER                :=2;
   TYPE STATE_rec_tab IS TABLE OF STATE%ROWTYPE;

   --------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   --------------------------------------------------------------
   FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                         O_error_COUNT     OUT      NUMBER,
                         I_file_id         IN       s9t_folder.file_id%TYPE,
                         I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
   ---------------------------------------------------------------------
END CORESVC_STATE;
/