CREATE OR REPLACE PACKAGE CURRENCY_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
-- Name:       GET_NAME
-- Purpose:    Gets currency description.
-------------------------------------------------------------------------------------------
FUNCTION GET_NAME (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
                   O_currency_desc   IN OUT   CURRENCIES.CURRENCY_DESC%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------
-- Name:       GET_RATE
-- Purpose:    This function retrieves exchanges rates, dependent
--             upon the exchange type entered.
-------------------------------------------------------------------------------------------
FUNCTION GET_RATE (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exchange_rate    IN OUT   CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                   I_currency_code    IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                   I_exchange_type    IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                   I_effective_date   IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------
-- Name:       GET_FORMAT
-- Purpose:    Gets currency formats.
-------------------------------------------------------------------------------------------
FUNCTION GET_FORMAT (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_currency_code       IN       CURRENCIES.CURRENCY_CODE%TYPE,
                     O_currency_rtl_fmt    IN OUT   CURRENCIES.CURRENCY_RTL_FMT%TYPE,
                     O_currency_rtl_dec    IN OUT   CURRENCIES.CURRENCY_RTL_DEC%TYPE,
                     O_currency_cost_fmt   IN OUT   CURRENCIES.CURRENCY_COST_FMT%TYPE,
                     O_currency_cost_dec   IN OUT   CURRENCIES.CURRENCY_COST_DEC%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------
-- Name:          CONVERT_BY_LOCATION
-- Purpose:       Convert a value from any location's currency to another
--                location's currency, based on location information(location
--                number, location type,etc.).  If NULL is passed in these
--                location fields, primary currency is then assumed.  If the
--                location type is 'O'rder, a call is made to retrieve the
--                currency code and exchange rate for the order; these are both
--                used in the CONVERT call.  Otherwise, just the currency code
--                is retrieved for the location, and then used in the CONVERT
--                function.
-------------------------------------------------------------------------------------------
FUNCTION CONVERT_BY_LOCATION (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_location_in         IN       VARCHAR2,
                              I_location_type_in    IN       VARCHAR2,
                              I_zone_group_id_in    IN       COST_ZONE.ZONE_GROUP_ID%TYPE,
                              I_location_out        IN       VARCHAR2,
                              I_location_type_out   IN       VARCHAR2,
                              I_zone_group_id_out   IN       COST_ZONE.ZONE_GROUP_ID%TYPE,
                              I_currency_value      IN       NUMBER,
                              O_currency_value      IN OUT   NUMBER,
                              I_cost_retail_ind     IN       VARCHAR2,
                              I_effective_date      IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                              I_exchange_type       IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                              I_calling_program            IN      VARCHAR2  DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:    CONVERT
-- Purpose: Converts a value from one currency to another.
--          If NULL is passed in for I_currency or I_currency_out
--          the function will assume the primary currency.
--          For I_cost_retail_ind, pass 'C' if passing a cost,
--          'R' if passing a retail that does not need to be converted
--          to a 'smart' retail or 'P' for a retail that needs to be
--          'smart' by passing the value through calc_adjust.
--          If NULL is passed in for I_effective_date the function
--          will use today's date, otherwise pass in the date of
--          the currency rate to be used in the conversion.
--          If NULL is passed in for I_exchange_type,
--          the default on system_options will be used. If the
--          system_options.consolidation_ind = 'Y', use the
--          consolidated rate'C', otherwise use 'O' for the
--          operational rate.  If NULL is passed in for I_exchange_rate_in
--          or I_exchange_rate_out, then the exchange rate between
--          the in/out currency and the primary currency will be
--          retrieved and used.
-------------------------------------------------------------------------------------------
FUNCTION CONVERT (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_currency_value      IN       NUMBER,
                  I_currency            IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  I_currency_out        IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  O_currency_value      IN OUT   NUMBER,
                  I_cost_retail_ind     IN       VARCHAR2,
                  I_effective_date      IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                  I_exchange_type       IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                  I_in_exchange_rate    IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_out_exchange_rate   IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:     CONVERT
-- Purpose:  This is the old version of the CONVERT function, and is in existence
--           to compensate for all current calls to the function, but do not have
--           values to pass to the new input parameters.
-------------------------------------------------------------------------------------------
FUNCTION CONVERT (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_currency_value    IN       NUMBER,
                  I_currency          IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  I_currency_out      IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  O_currency_value    IN OUT   NUMBER,
                  I_cost_retail_ind   IN       VARCHAR2,
                  I_effective_date    IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                  I_exchange_type     IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:    ROUND_CURRENCY
-- Purpose: Rounds the currency value to the correct number of
--          decimal places based on retail_ind.
-----------------------------------------------------------------
FUNCTION ROUND_CURRENCY (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_currency_value    IN OUT   NUMBER,
                         I_currency_out      IN       VARCHAR2,
                         I_cost_retail_ind   IN       VARCHAR2)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------
-- Name:    GET_CURR_LOC
-- Purpose: To retrieve the specific currency_code for the
--          location.
--          Valid I_location_type values are 'W'(Warehouse), 'S'(Store),
--          'Z'(price zone), 'V'(supplier), 'O'(ordering), 'C', (Contracts),
--          'L'(landed cost zone), (E) External Finisher, (I) Internal Finisher, and (D) Deals.
--          Also, given the location, and the
--          location_type, this function will retrieve the currency_code
--          from the Partner table.
-------------------------------------------------------------------------------------------
FUNCTION GET_CURR_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_location        IN       VARCHAR2,
                       I_location_type   IN       ITEM_LOC.LOC_TYPE%TYPE,
                       I_zone_group_id   IN       COST_ZONE.ZONE_GROUP_ID%TYPE,
                       O_currency_code   IN OUT   CURRENCIES.CURRENCY_CODE%TYPE)
    RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:    EXIST
-- Purpose: Checks to see if the currency exists on the currencies table.
-------------------------------------------------------------------------------------------
FUNCTION EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
                O_exists          IN OUT   BOOLEAN)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name: CHECK_EMU_COUNTRIES
-- Purpose: Checks to see if I_currency is participating in the EURO currency program.
--          If so O_participating_ind is returned TRUE else it is returned FALSE.
-------------------------------------------------------------------------------------------
FUNCTION CHECK_EMU_COUNTRIES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_participating_ind   IN OUT   BOOLEAN,
                             I_currency            IN       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------    
FUNCTION CHECK_EMU_COUNTRIES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_participating_ind   IN OUT   BOOLEAN,
                             I_currency            IN       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                             I_effective_date      IN       DATE)
   RETURN BOOLEAN;    
-------------------------------------------------------------------------------------------
FUNCTION CONVERT_VALUE(I_cost_retail_ind   IN   VARCHAR2,
                       I_currency_out      IN   CURRENCIES.CURRENCY_CODE%TYPE,
                       I_currency_in       IN   CURRENCIES.CURRENCY_CODE%TYPE,
                       I_currency_value    IN   NUMBER)
   RETURN NUMBER;
-------------------------------------------------------------------------------------------
-- Name:     CONVERT
-- Purpose:  This CONVERT function is used to convert from one currncy to another
--           and the final output should be rounded to 4 decimal places or no rounding
--           should happen
-------------------------------------------------------------------------------------------
FUNCTION CONVERT (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_currency_value      IN       NUMBER,
                  I_currency            IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  I_currency_out        IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                  O_currency_value      IN OUT   NUMBER,
                  I_cost_retail_ind     IN       VARCHAR2,
                  I_effective_date      IN       CURRENCY_RATES.EFFECTIVE_DATE%TYPE,
                  I_exchange_type       IN       CURRENCY_RATES.EXCHANGE_TYPE%TYPE,
                  I_in_exchange_rate    IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_out_exchange_rate   IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_rounding_ind        IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END CURRENCY_SQL;
/
