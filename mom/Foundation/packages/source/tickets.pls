CREATE OR REPLACE PACKAGE TICKET_SQL AUTHID CURRENT_USER AS

TYPE ticket_TBL    is table of TICKET_REQUEST%ROWTYPE        INDEX BY BINARY_INTEGER;


----------------------------------------------------------------------------- 
FUNCTION TICKET_TYPE_EXISTS(O_ERROR_MESSAGE IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_EXISTS        IN OUT BOOLEAN,
                            I_TICKET_TYPE   IN     TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------- 
FUNCTION TICKET_ITEM_EXISTS(O_ERROR_MESSAGE IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_EXISTS        IN OUT BOOLEAN,
                            I_TICKET_TYPE   IN     TICKET_TYPE_DETAIL.TICKET_TYPE_ID%TYPE,
                            I_TICKET_ITEM   IN     TICKET_TYPE_DETAIL.TICKET_ITEM_ID%TYPE,
                            I_UDA           IN     TICKET_TYPE_DETAIL.UDA_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------- 
-- Name:       GET_NAME
-- Purpose:    This function retrieves the ticket description for the passed in
--             ticket type.
-- Created by: Ann Zawistoski, 08-OCT-97
-----------------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_ticket_desc   IN OUT TICKET_TYPE_HEAD.TICKET_TYPE_DESC%TYPE,
                  O_exists        IN OUT BOOLEAN,
                  I_ticket_type   IN     TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------- 
-- Name:       INSERT_ITEM_TICKET
-- Purpose:    This function inserts the given information into the item_ticket table.
--             I_item_type may be 'ITEM_LIST', 'SKU', or 'STYLE'.  
-- Created by: Ann Zawistoski, 08-OCT-97
-----------------------------------------------------------------------------
FUNCTION INSERT_ITEM_TICKET(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_item_type       IN  VARCHAR2,
                  I_item_no         IN  ITEM_TICKET.ITEM%TYPE,
                  I_ticket_type_id  IN  TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE,
                  I_po_print_type   IN  ITEM_TICKET.PO_PRINT_TYPE%TYPE, 
                  I_print_on_pc_ind IN  ITEM_TICKET.PRINT_ON_PC_IND%TYPE,
                  I_ticket_over_pct IN  ITEM_TICKET.TICKET_OVER_PCT%TYPE)
RETURN BOOLEAN; 
-----------------------------------------------------------------------------
-- Name:      REQUEST
-- Purpose:   Inserts a ticket request record.
-- Note:      The I_location input parameter cannot be %typed b/c it must
--            be flexible enough to hold a store class value (character).
------------------------------------------------------------------------------
FUNCTION REQUEST(O_ERROR_MESSAGE      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_LOC_TYPE           IN       CODE_DETAIL.CODE%TYPE,
                 I_LOCATION           IN       VARCHAR2,
                 I_ITEM_TYPE          IN       VARCHAR2,
                 I_ITEM               IN       ITEM_TICKET.ITEM%TYPE,
                 I_TICKET_TYPE        IN       TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE,
                 I_QUANTITY           IN       TICKET_REQUEST.QTY%TYPE,
                 I_COUNTRY            IN       TICKET_REQUEST.COUNTRY_OF_ORIGIN%TYPE,
                 I_PRINT_ONLINE_IND   IN       TICKET_REQUEST.PRINT_ONLINE_IND%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION APPROVE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                 I_print_online_ind  IN TICKET_REQUEST.PRINT_ONLINE_IND%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       COPY_TICKET
-- Purpose:    This function copies all ticket types defined for a sku on 
--             item_ticket table to another sku.
-- Created by: Kate Brady, 02-OCT-97
-----------------------------------------------------------------------------
   FUNCTION COPY_TICKET(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_copy_from_item  IN     ITEM_TICKET.ITEM%TYPE,
                       I_copy_to_item    IN     ITEM_TICKET.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       RECEIVE
-- Purpose:    This function takes a SKU/store combination, where tickets are
--             to be requested on receipt, and inserts records accordingly
--             into TICKET_REQUEST.
-- Created by: Kate Brady, 03-OCT-97
-----------------------------------------------------------------------------
   FUNCTION RECEIVE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_order_no         IN     ORDHEAD.ORDER_NO%TYPE,
                    I_item             IN     ITEM_TICKET.ITEM%TYPE,
                    I_store            IN     STORE.STORE%TYPE,
                    I_receipt_qty      IN     ORDLOC.QTY_RECEIVED%TYPE,
                    I_print_online_ind IN     TICKET_REQUEST.PRINT_ONLINE_IND%TYPE)
   RETURN BOOLEAN;


-----------------------------------------------------------------------------
--Function Name: GET_ITEM_TICKET_INFO
--Purpose:       To return all information from the item_ticket table for a given item
--               if the item.  If the item has only one ticket_type_id associated 
--               with it, the function will return the appropriate output values.  If
--               the item has multiple ticket_type_ids and no input ticket_type_id is 
--               specified, the function will return either 'N' or 'M' (for no or
--               multiple ticket_types_ids).  If the item has multiple ticket_types
--               and an input ticket_type is specified, the function will return the 
--               appropriate values (if defined).
-----------------------------------------------------------------------------------
FUNCTION GET_ITEM_TICKET_INFO
             (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
              O_no_or_multi_ind IN OUT VARCHAR2,
              O_ticket_type_id  IN OUT ITEM_TICKET.TICKET_TYPE_ID%TYPE,
              O_po_print_type   IN OUT ITEM_TICKET.PO_PRINT_TYPE%TYPE,
              O_print_on_pc_ind IN OUT ITEM_TICKET.PRINT_ON_PC_IND%TYPE,
              O_print_over_pct  IN OUT ITEM_TICKET.TICKET_OVER_PCT%TYPE,
              I_ticket_type_id  IN     ITEM_TICKET.TICKET_TYPE_ID%TYPE,
              I_item            IN     ITEM_TICKET.ITEM%TYPE)
              RETURN BOOLEAN;
------------------------------------------------------------------------------------
--Function Name:  INSERT_TICKET_REQUEST
--Purpose:        To insert a record into the ticket_request table.  The function
--                will not execute if I_item, I_ticket_type or I_location is NULL.
--                This function assumes valid data.
------------------------------------------------------------------------------------
FUNCTION INSERT_TICKET_REQUEST 
      (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
       I_item              IN     TICKET_REQUEST.ITEM%TYPE,
       I_ticket_type_id    IN     TICKET_REQUEST.TICKET_TYPE_ID%TYPE,
       I_qty               IN     TICKET_REQUEST.QTY%TYPE,
       I_loc_type          IN     TICKET_REQUEST.LOC_TYPE%TYPE,
       I_location          IN     TICKET_REQUEST.LOCATION%TYPE,
       I_unit_retail       IN     TICKET_REQUEST.UNIT_RETAIL%TYPE,
       I_multi_units       IN     TICKET_REQUEST.MULTI_UNITS%TYPE,
       I_multi_unit_retail IN     TICKET_REQUEST.MULTI_UNIT_RETAIL%TYPE, 
       I_country_of_origin IN     TICKET_REQUEST.COUNTRY_OF_ORIGIN%TYPE,
       I_order_no          IN     TICKET_REQUEST.ORDER_NO%TYPE,
       I_print_online_ind  IN     TICKET_REQUEST.PRINT_ONLINE_IND%TYPE,
       I_ticket_over_pct   IN     ITEM_TICKET.TICKET_OVER_PCT%TYPE)
 RETURN BOOLEAN;
-----------------------------------------------------------------------------
--- Name:       INSERT_TICKET_TO_CHILDREN 
-- Purpose:    This function gives the user ability to now default down ticket 
--             information to the existing children/grandchildren of an item.
--             It does not apply this information below the transaction level.
-- Created by: Roger Nallapraju, 02-Feb-01;
------------------------------------------------------------------------------
FUNCTION INSERT_TICKET_TO_CHILDREN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                   I_item_type     IN     VARCHAR2,
                                   I_item_level    IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                                   I_tran_level    IN     ITEM_MASTER.TRAN_LEVEL%TYPE) 
 RETURN BOOLEAN;
------------------------------------------------------------------------------                  
--- Name:       TICKET_TYPE_EXISTS
-- Purpose:    This function checks item_ticket to see if a ticket type has 
--             already been associated with an item.
-- Created by: Michael Johnson, 25-Jul-01;
------------------------------------------------------------------------------
FUNCTION TICKET_TYPE_ITEM_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists        IN OUT BOOLEAN,
                                 I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                 I_ticket_type   IN     ITEM_TICKET.TICKET_TYPE_ID%TYPE) 
 RETURN BOOLEAN;
------------------------------------------------------------------------------ 
--- Name:       NEW_PRICE_CHANGE
-- Purpose:    This function inserts records into TICKET_REQUEST table for new 
--             price changes. .
-- Created by: Thanh Huynh, 17-Aug-04;
------------------------------------------------------------------------------
FUNCTION NEW_PRICE_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_ticket_tbl    IN     TICKET_SQL.TICKET_TBL)
 RETURN BOOLEAN;
------------------------------------------------------------------------------ 
--- Name:       UPDATE_PRICE_CHANGE
-- Purpose:    This function updates records into TICKET_REQUEST table for  
--             price changes. .
-- Created by: Thanh Huynh, 17-Aug-04;
------------------------------------------------------------------------------
FUNCTION UPDATE_PRICE_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ticket_tbl    IN     TICKET_SQL.TICKET_TBL)
 RETURN BOOLEAN;
------------------------------------------------------------------------------ 
--- Name:       DELETE_PRICE_CHANGE
-- Purpose:    This function deletes records from TICKET_REQUEST table for  
--             price changes. .
-- Created by: Thanh Huynh, 17-Aug-04;
------------------------------------------------------------------------------
FUNCTION DELETE_PRICE_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_ticket_tbl    IN     TICKET_SQL.TICKET_TBL)
 RETURN BOOLEAN;
------------------------------------------------------------------------------ 
FUNCTION LOCK_TICKET(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_item            IN     TICKET_REQUEST.ITEM%TYPE,
                     I_ticket_type_id  IN     TICKET_REQUEST.TICKET_TYPE_ID%TYPE,
                     I_location        IN     TICKET_REQUEST.LOCATION%TYPE,
                     I_price_change_id IN     TICKET_REQUEST.PRICE_CHANGE_ID%TYPE)
 RETURN BOOLEAN;
------------------------------------------------------------------------------ 
--- Name:      VALIDATE_ITEM_TICKET
-- Purpose:    This function will check if an ticket type is associated with
--             an item.
------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_TICKET(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists           IN OUT   BOOLEAN,
                              I_ticket_type_id   IN       ITEM_TICKET.TICKET_TYPE_ID%TYPE,
                              I_item             IN       ITEM_TICKET.ITEM%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------ 

END TICKET_SQL;
/
