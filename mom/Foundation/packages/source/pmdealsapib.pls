CREATE OR REPLACE PACKAGE BODY PM_DEALS_API_SQL IS
--------------------------------------------------------------------------------
-- This internal function will be used to find the diff number (1-4) because RPM 
-- stores diffs as a collection and does not recognize the order.  RPM will pass
-- a merch level code of 8 and an item parent/diff. RMS will have to determine
-- the diff number and correct merch level code 
FUNCTION ORDER_DIFFS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_merch_level_code  IN OUT CODE_DETAIL.CODE%TYPE,
                     I_item_parent       IN     ITEM_MASTER.ITEM%TYPE,
                     I_diff_id           IN     ITEM_MASTER.DIFF_1%TYPE)
 RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'PM_DEALS_API_SQL.ORDER_DIFFS';
   L_merch_level_code  CODE_DETAIL.CODE%TYPE;

   cursor C_DIFF_ORDER is
      select decode(I_diff_id, diff_1, 8,diff_2, 9, diff_3, 10, diff_4, 11, -9) diff_order
        from item_master
       where item_parent = I_item_parent
         and (diff_1 = I_diff_id
             or diff_2 = I_diff_id
             or diff_3 = I_diff_id
             or diff_4 = I_diff_id)
         and rownum = 1; -- this is because more than one child may exist with the specified diff id

BEGIN
   
   if I_item_parent is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item_parent',
                                            L_program,
                                            NULL);  
      return FALSE;
   end if;

   if I_diff_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_diff_id',
                                            L_program,
                                            NULL); 
      return FALSE;
   end if;
   
   ---------------------------------
   -- Merch_level values:
   -- DIML 8      Item Parent - Diff 1
   -- DIML 9      Item Parent - Diff 2
   -- DIML 10     Item Parent - Diff 3
   -- DIML 11     Item Parent - Diff 4
   ---------------------------------
   open C_DIFF_ORDER;
   fetch C_DIFF_ORDER into L_merch_level_code;
   close C_DIFF_ORDER;

   if L_merch_level_code = -9 or L_merch_level_code is NULL then 
      -- item/diff combination does not exist
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_DIFF_COMBO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   O_merch_level_code := L_merch_level_code;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ORDER_DIFFS;
---------------------------------------------------------------------------------
PROCEDURE CREATE_DEAL(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_return_code     IN OUT  BOOLEAN,
                      O_deal_ids_table  IN OUT  DEAL_IDS_TBL,
                      I_deal_head_rec   IN OUT  DEAL_HEAD_REC) AS 

   L_program             VARCHAR2(61) := 'PM_DEALS_API_SQL.CREATE_DEAL';

   R_deal_ids_rec        DEAL_IDS_REC;
   L_deal_id             DEAL_HEAD.DEAL_ID%TYPE;
   L_next_invoice_date   DEAL_HEAD.EST_NEXT_INVOICE_DATE%TYPE;
   L_sysdate             DATE := SYSDATE;
   L_currency_code       SUPS.CURRENCY_CODE%TYPE;  --RPM does not store currency code, so it needs to be fetched.
   L_index               BINARY_INTEGER;
   L_sups_rec            SUPS%ROWTYPE;

   cursor C_GET_PARTNER_INFO is
      select currency_code
        from partner
       where partner_id = I_deal_head_rec.partner_id
         and partner_type = I_deal_head_rec.partner_type;
BEGIN
   if I_deal_head_rec.active_date is NULL then
      O_return_code := FALSE;
      return;
   end if;

   -- Initialize the deal ids table
   if O_deal_ids_table is NULL then
      O_deal_ids_table :=DEAL_IDS_TBL();
   else
      O_deal_ids_table.DELETE;
   end if;

   if DEAL_ATTRIB_SQL.GET_NEXT_DEAL_ID(O_error_message,
                                       L_deal_id) = FALSE then
      O_return_code := FALSE;
      return;
   end if;

   if DEAL_FINANCE_SQL.CALC_INITIAL_INVOICE_DATE(O_error_message,
                                                 L_next_invoice_date,
                                                 I_deal_head_rec.active_date,
                                                 I_deal_head_rec.close_date,
                                                 I_deal_head_rec.bill_back_period) = FALSE then
      O_return_code := FALSE;
      return;
   end if;

   if I_deal_head_rec.partner_type = 'S' then

      if SUPP_ATTRIB_SQL.GET_SUPS(O_error_message,
                                  L_sups_rec,
                                  I_deal_head_rec.supplier) = FALSE then
         O_return_code := FALSE;
         return;
      end if;
      --- Supplier Site check
      if L_sups_rec.supplier_parent is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SUPP_SITE_NOT_ALLOW',
                                               NULL,
                                               NULL,
                                               NULL);
         O_return_code := FALSE;
         return;
      end if;
      ---
      L_currency_code := L_sups_rec.currency_code;
   else
      open C_GET_PARTNER_INFO;
      fetch C_GET_PARTNER_INFO into L_currency_code;
      close C_GET_PARTNER_INFO;
      if L_currency_code is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ACTIVE_PARTNER',
                                               I_deal_head_rec.partner_id,
                                               I_deal_head_rec.partner_type,
                                               NULL);
         O_return_code := FALSE;
         return;
      end if;
   end if;

   insert into DEAL_HEAD(DEAL_ID,
                         PARTNER_TYPE,
                         PARTNER_ID,
                         SUPPLIER,
                         TYPE,
                         STATUS,
                         CURRENCY_CODE,
                         ACTIVE_DATE,
                         CLOSE_DATE,
                         CREATE_DATETIME,
                         CREATE_ID,
                         APPROVAL_DATE,
                         APPROVAL_ID,
                         RECALC_APPROVED_ORDERS,
                         LAST_UPDATE_ID,
                         LAST_UPDATE_DATETIME,
                         BILLING_TYPE,
                         BILL_BACK_PERIOD,
                         REBATE_PURCH_SALES_IND,
                         REBATE_IND,
                         GROWTH_REBATE_IND,
                         BILLING_PARTNER_TYPE,
                         BILLING_PARTNER_ID,
                         BILLING_SUPPLIER_ID,
                         DEAL_REPORTING_LEVEL,
                         BILL_BACK_METHOD,
                         DEAL_INCOME_CALCULATION,
                         INVOICE_PROCESSING_LOGIC,
                         STOCK_LEDGER_IND,
                         INCLUDE_VAT_IND,
                         SECURITY_IND,
                         EST_NEXT_INVOICE_DATE,
                         RPM_DEAL_IND)
                 values (L_deal_id,
                        I_deal_head_rec.partner_type,
                        decode(I_deal_head_rec.partner_type,'S',NULL,I_deal_head_rec.partner_id),
                        decode(I_deal_head_rec.partner_type,'S',I_deal_head_rec.supplier,NULL),
                        'P',
                        'A',
                        L_currency_code,
                        I_deal_head_rec.active_date,
                        I_deal_head_rec.close_date,
                        L_sysdate,
                        I_deal_head_rec.user_id,
                        L_sysdate,
                        I_deal_head_rec.user_id,
                        'N',
                        I_deal_head_rec.user_id,
                        L_sysdate,
                        'VFP',
                        I_deal_head_rec.bill_back_period,
                        'S',
                        'Y',
                        'N',
                        I_deal_head_rec.partner_type,
                        decode(I_deal_head_rec.partner_type,'S',NULL,I_deal_head_rec.partner_id),
                        decode(I_deal_head_rec.partner_type,'S',I_deal_head_rec.supplier,NULL),
                        I_deal_head_rec.deal_reporting_level,
                        I_deal_head_rec.bill_back_method,
                        'A',
                        I_deal_head_rec.invoice_processing_logic,
                        I_deal_head_rec.stock_ledger_ind,
                        I_deal_head_rec.include_vat_ind,
                        'N',
                        L_next_invoice_date,
                        'Y');

   if I_deal_head_rec.deal_detail_table.COUNT > 0 then
      -- when creating a new deal, the deal_detail_table.deal_id value will be NULL, update it to the new L_deal_id
      -- value to be used when inserting deal components
      I_deal_head_rec.deal_detail_table(1).deal_id := L_deal_id;

      NEW_DEAL_COMP(O_error_message,
                    O_return_code,
                    O_deal_ids_table,
                    I_deal_head_rec.deal_detail_table);
      if O_return_code = FALSE then
         return;
      end if;
   end if;

   O_return_code := TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_return_code := FALSE;
END CREATE_DEAL;
--------------------------------------------------------------------------------
PROCEDURE NEW_DEAL_COMP(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_return_code        IN OUT  BOOLEAN,
                        O_deal_ids_table     IN OUT  DEAL_IDS_TBL,
                        I_deal_detail_table  IN      DEAL_DETAIL_TBL) IS

   L_program          VARCHAR2(61) := 'PM_DEALS_API_SQL.NEW_DEAL_COMP';

   R_deal_ids_rec       DEAL_IDS_REC;
   L_deal_id            DEAL_DETAIL.DEAL_ID%TYPE;
   L_deal_detail_id     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE;
   L_active_date        DEAL_HEAD.ACTIVE_DATE%TYPE;
   L_close_date         DEAL_HEAD.CLOSE_DATE%TYPE;
   L_deal_reporting_level DEAL_HEAD.DEAL_REPORTING_LEVEL%TYPE;
   L_bbd_add_rep_days   DEAL_HEAD.BBD_ADD_REP_DAYS%TYPE;
   
                        
   L_sysdate            DATE         := SYSDATE;
   L_index              BINARY_INTEGER;
   L_index2             BINARY_INTEGER;      
   L_rowid              ROWID;
   cursor C_GET_DEAL_HEAD_INFO is
   select active_date,
          close_date,
          deal_reporting_level,
          bbd_add_rep_days
     from deal_head
    where deal_id = L_deal_id;

   cursor C_LOCK_DEAL_COMP_PROM(L_i1 BINARY_INTEGER,L_i2 BINARY_INTEGER) is
   select rowid 
     from deal_comp_prom
          where deal_id          = I_deal_detail_table(L_i1).deal_id
            and deal_detail_id   = I_deal_detail_table(L_i1).deal_detail_id
            and (promotion_id    = I_deal_detail_table(L_i1).deal_comp_prom_table (L_i2).promotion_id
                or promotion_id is NULL)
            and (promo_comp_id   = I_deal_detail_table(L_i1).deal_comp_prom_table (L_i2).promo_comp_id
                or promo_comp_id is NULL);
BEGIN
   -- Initialize the deal ids table. this needs to be done because NEW_DEAL_COMP 
   -- can be called independantly of CREATE_DEAL
   if O_deal_ids_table is NULL then
      O_deal_ids_table :=DEAL_IDS_TBL();
   end if;

   for L_index in 1 .. I_deal_detail_table.count loop
      if I_deal_detail_table(L_index).deal_detail_id is NULL then

         if L_index = 1 then -- first time in loop, set the local deal_id variable
            L_deal_id := I_deal_detail_table(L_index).deal_id;

            open C_GET_DEAL_HEAD_INFO;
            fetch C_GET_DEAL_HEAD_INFO into L_active_date,
                                            L_close_date,
                                            L_deal_reporting_level,
                                            L_bbd_add_rep_days;
            close C_GET_DEAL_HEAD_INFO;
         end if;

         if DEAL_ATTRIB_SQL.GET_NEXT_DEAL_DETAIL_ID(O_error_message,
                                                    L_deal_detail_id,
                                                    L_deal_id) = FALSE then
            O_return_code := FALSE;
            return;
         end if;

         insert into DEAL_DETAIL(DEAL_ID,
                                 DEAL_DETAIL_ID,
                                 APPLICATION_ORDER,
                                 DEAL_COMP_TYPE,
                                 COLLECT_START_DATE,
                                 COLLECT_END_DATE,
                                 PRICE_COST_APPL_IND,
                                 DEAL_CLASS,
                                 QTY_THRESH_RECUR_IND,
                                 TRAN_DISCOUNT_IND,
                                 CREATE_DATETIME,
                                 LAST_UPDATE_ID,
                                 LAST_UPDATE_DATETIME,
                                 VFP_DEFAULT_CONTRIB_PCT,
                                 TOTAL_BUDGET_FIXED_IND,
                                 TOTAL_ACTUAL_FIXED_IND)
                         values (L_deal_id,
                                 L_deal_detail_id,
                                 L_deal_detail_id,
                                 'VFP',
                                 I_deal_detail_table(L_index).collect_start_date,
                                 I_deal_detail_table(L_index).collect_end_date,
                                 'N',
                                 'CS',
                                 'N',
                                 'N',
                                 L_sysdate,
                                 I_deal_detail_table(L_index).user_id,
                                 L_sysdate,
                                 I_deal_detail_table(L_index).vfp_default_contrib_pct,
                                 'N',
                                 'N');

        if DEAL_ACTUAL_FORECAST_SQL.CREATE_TEMPLATE(O_error_message,
                                                     L_deal_id,
                                                     L_active_date,
                                                     L_close_date,
                                                     L_deal_detail_id,
                                                     L_deal_reporting_level,
                                                     L_bbd_add_rep_days) = FALSE then
            O_return_code := FALSE;
            return;
        end if;

         if I_deal_detail_table(L_index).deal_comp_prom_table.COUNT > 0 then

            for L_index2 in 1 .. I_deal_detail_table(L_index).deal_comp_prom_table.COUNT loop
                              insert into DEAL_COMP_PROM(DEAL_ID,
                                          DEAL_DETAIL_ID,
                                          PROMOTION_ID,
                                          PROMO_COMP_ID,
                                          CONTRIBUTION_PCT)
                                  values (L_deal_id,
                                          L_deal_detail_id,
                                          I_deal_detail_table(L_index).deal_comp_prom_table(L_index2).promotion_id, 
                                          I_deal_detail_table(L_index).deal_comp_prom_table(L_index2).promo_comp_id,
                                          I_deal_detail_table(L_index).deal_comp_prom_table(L_index2).contribution_pct);
            end loop;
         end if;
              
         -- Populate the output table
         R_deal_ids_rec := DEAL_IDS_REC(L_deal_id, L_deal_detail_id);
            
         -- Add record to deal ids table
         O_deal_ids_table.EXTEND;
         O_deal_ids_table(O_deal_ids_table.COUNT) := R_deal_ids_rec;

      else -- deal_detail_id is not NULL

         if I_deal_detail_table(L_index).deal_comp_prom_table.COUNT > 0 then

            for L_index2 in 1 .. I_deal_detail_table(L_index).deal_comp_prom_table.COUNT loop

               open C_LOCK_DEAL_COMP_PROM(L_index,L_index2);
               fetch C_LOCK_DEAL_COMP_PROM into L_rowid;

               update deal_comp_prom
                  set contribution_pct = I_deal_detail_table(L_index).deal_comp_prom_table(L_index2).contribution_pct,
                      promotion_id     = I_deal_detail_table(L_index).deal_comp_prom_table(L_index2).promotion_id,
                      promo_comp_id    = I_deal_detail_table(L_index).deal_comp_prom_table(L_index2).promo_comp_id
                where rowid = L_rowid;
                close C_LOCK_DEAL_COMP_PROM;
                
                  if SQL%NOTFOUND then
                     O_return_code := FALSE;
                     O_error_message := SQL_LIB.CREATE_MSG('INV_DEAL_COMP_VFP',
                                                           I_deal_detail_table(L_index).deal_id
                                                           ||'/'||I_deal_detail_table(L_index).deal_detail_id
                                                           ||'/'||I_deal_detail_table(L_index).deal_comp_prom_table (L_index2).promotion_id
                                                           ||'/'||I_deal_detail_table(L_index).deal_comp_prom_table (L_index2).promo_comp_id,
                                                           L_program,
                                                           NULL);
                     return;
                  end if;                
            end loop;
         end if;
      end if;   -- deal_detail_id is NULL
   end loop;  -- L_index

   O_return_code := TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_return_code := FALSE;
END NEW_DEAL_COMP;
--------------------------------------------------------------------------------
END PM_DEALS_API_SQL;
/