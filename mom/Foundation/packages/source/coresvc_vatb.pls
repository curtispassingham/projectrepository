CREATE OR REPLACE PACKAGE BODY CORESVC_VAT AS

   cursor C_SVC_VAT_CODES(I_process_id   NUMBER,
                          I_chunk_id     NUMBER) is
      select t.pk_vat_code_rates_rid,
             t.vcr_vcd_fk_rid,
             t.stvc_rid,
             t.stvc_process_id,
             t.stvc_chunk_id,
             t.stvc_action,
             t.stvc_row_seq,
             UPPER(t.stvc_vat_code)   as stvc_vat_code,
             t.stvc_vat_code_desc,
             t.stvc_incl_nic_ind,
             LEAD(t.stvc_vat_code,1,0) OVER (ORDER BY t.stvc_vat_code) as next_vat_code,
             t.stvcr_rid,
             t.stvcr_process_id,
             t.stvcr_chunk_id,
             t.stvcr_action,
             t.stvcr_row_seq,
             UPPER(t.stvcr_vat_code)  as stvcr_vat_code,
             t.stvcr_vat_rate,
             t.stvcr_active_date,
             t.stvcr_active_date_s9t,
             t.base_vc_vat_code,
             t.base_vc_vat_code_desc,
             t.base_vc_incl_nic_ind,
             t.base_vcr_vat_code,
             t.base_vcr_active_date,
             t.base_vcr_vat_rate,
             ROW_NUMBER() OVER (PARTITION BY t.stvc_vat_code ORDER BY t.stvc_vat_code) as vc_rank
        from (select pk_vat_code_rates.rowid       as pk_vat_code_rates_rid,
                     vcr_vcd_fk.rowid              as vcr_vcd_fk_rid,
                     stvc.rowid                    as stvc_rid,
                     stvc.action                   as stvc_action,
                     stvc.row_seq                  as stvc_row_seq,
                     UPPER(stvc.vat_code)          as stvc_vat_code,
                     stvc.vat_code_desc            as stvc_vat_code_desc,
                     stvc.incl_nic_ind             as stvc_incl_nic_ind,
                     stvc.process_id               as stvc_process_id,
                     stvc.chunk_id                 as stvc_chunk_id,
                     stvcr.action                  as stvcr_action,
                     stvcr.row_seq                 as stvcr_row_seq,
                     stvcr.rowid                   as stvcr_rid,
                     UPPER(stvcr.vat_code)         as stvcr_vat_code,
                     stvcr.active_date             as stvcr_active_date,
                     stvcr.active_date_s9t         as stvcr_active_date_s9t,
                     stvcr.vat_rate                as stvcr_vat_rate,
                     stvcr.process_id              as stvcr_process_id,
                     stvcr.chunk_id                as stvcr_chunk_id,
                     vcr_vcd_fk.vat_code           as base_vc_vat_code,
                     vcr_vcd_fk.vat_code_desc      as base_vc_vat_code_desc,
                     vcr_vcd_fk.incl_nic_ind       as base_vc_incl_nic_ind,
                     pk_vat_code_rates.vat_code    as base_vcr_vat_code,
                     pk_vat_code_rates.active_date as base_vcr_active_date,
                     pk_vat_code_rates.vat_rate    as base_vcr_vat_rate
                from svc_vat_codes      stvc,
                     svc_vat_code_rates stvcr,
                     vat_codes          vcr_vcd_fk,
                     vat_code_rates     pk_vat_code_rates
               where stvc.process_id          = I_process_id
                 and stvc.chunk_id            = I_chunk_id
                 and stvc.process_id          = stvcr.process_id(+)
                 and stvc.chunk_id            = stvcr.chunk_id(+)
                 and UPPER(stvc.vat_code)     = stvcr.vat_code(+)
                 and UPPER(stvc.vat_code)     = vcr_vcd_fk.vat_code (+)
                 and UPPER(stvcr.vat_code)    = pk_vat_code_rates.vat_code (+)
                 and stvcr.active_date        = pk_vat_code_rates.active_date (+)
      union all
      select pk_vat_code_rates.rowid                as pk_vat_code_rates_rid,
             vcr_vcd_fk.rowid                       as vcr_vcd_fk_rid,
             stvc.rowid                             as stvc_rid,
             stvc.action                            as stvc_action,
             stvc.row_seq                           as stvc_row_seq,
             UPPER(NVL(stvc.vat_code,vcr_vcd_fk.vat_code)) as stvc_vat_code,
             stvc.vat_code_desc                     as stvc_vat_code_desc,
             stvc.incl_nic_ind                      as stvc_incl_nic_ind,
             stvc.process_id                        as stvc_process_id,
             stvc.chunk_id                          as stvc_chunk_id,
             stvcr.action                           as stvcr_action,
             stvcr.row_seq                          as stvcr_row_seq,
             stvcr.rowid                            as stvcr_rid,
             UPPER(stvcr.vat_code)                  as stvcr_vat_code,
             stvcr.active_date                      as stvcr_active_date,
             stvcr.active_date_s9t                  as stvcr_active_date_s9t,
             stvcr.vat_rate                         as stvcr_vat_rate,
             stvcr.process_id                       as stvcr_process_id,
             stvcr.chunk_id                         as stvcr_chunk_id,
             vcr_vcd_fk.vat_code                    as base_vc_vat_code,
             vcr_vcd_fk.vat_code_desc               as base_vc_vat_code_desc,
             vcr_vcd_fk.incl_nic_ind                as base_vc_incl_nic_ind,
             pk_vat_code_rates.vat_code             as base_vcr_vat_code,
             pk_vat_code_rates.active_date          as base_vcr_active_date,
             pk_vat_code_rates.vat_rate             as base_vcr_vat_rate
        from svc_vat_code_rates stvcr,
             svc_vat_codes      stvc,
             vat_codes          vcr_vcd_fk,
             vat_code_rates     pk_vat_code_rates
       where stvcr.process_id         = I_process_id
         and stvcr.chunk_id           = I_chunk_id
         and stvcr.process_id         = stvc.process_id(+)
         and stvcr.chunk_id           = stvc.chunk_id(+)
         and UPPER(stvcr.vat_code)    = stvc.vat_code(+)
         and UPPER(stvcr.vat_code)    = vcr_vcd_fk.vat_code (+)
         and UPPER(stvcr.vat_code)    = pk_vat_code_rates.vat_code(+)
         and stvcr.active_date        = pk_vat_code_rates.active_date(+)
         and stvc.vat_code is NULL ) t;

   CURSOR C_SVC_VAT_REG(I_process_id   NUMBER,
                        I_chunk_id     NUMBER ) IS
      select pk_vat_region.rowid                 as pk_vat_region_rid,
             pk_vat_region.reverse_vat_threshold as base_reverse_vat_threshold,
             pk_vat_region.vat_region            as base_vat_region,
             pk_vat_region.vat_region_name       as base_vat_region_name,
             pk_vat_region.vat_region_type       as base_vat_region_type,
             Pk_Vat_Region.Acquisition_Vat_Ind   As Base_Vat_Acquisition_Vat_Ind,
             LEAD(st.vat_region_type, 1, 0) OVER (ORDER BY st.vat_region_type desc) AS next_vat_region_type,
             st.vat_region,
             ABS(st.reverse_vat_threshold)       as reverse_vat_threshold,
             st.vat_region_name,
             st.vat_region_type                  as vat_region_type,
             st.acquisition_vat_ind,
             st.vat_calc_type                    as vat_calc_type,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             Upper(St.Action)                    as Action,
             St.Process$status,
             row_number() over (partition BY st.vat_region_type order by st.vat_region_type desc) AS vatreg_rank,
             cd_vat_calc_type.rowid              as cd_vat_calc_type_rid
        from svc_vat_region  st,
             vat_region      pk_vat_region,
             code_detail     cd_vat_calc_type
       where st.process_id = I_process_id
         And St.Chunk_Id   = I_chunk_id
         and st.vat_region = pk_vat_region.vat_region (+)
         and st.vat_calc_type = cd_vat_calc_type.code (+)
         and cd_vat_calc_type.code_type (+) = 'VRCT';
         

   LP_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   TYPE errors_tab_typ     IS TABLE OF svc_admin_upld_er%ROWTYPE;
   LP_errors_tab           errors_tab_typ;
   LP_warning_tab          errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab       s9t_errors_tab_typ;

   Type VAT_CODES_TAB IS TABLE OF VAT_CODES_TL%ROWTYPE;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   
   Type VAT_REGION_TAB IS TABLE OF VAT_REGION_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   
   LP_primary_lang    LANG.LANG%TYPE;
-------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
-------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet     IN   VARCHAR2,
                           I_row_seq   IN   NUMBER,
                           I_col       IN   VARCHAR2,
                           I_sqlcode   IN   NUMBER,
                           I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES( I_file_id   NUMBER ) IS
   L_sheets               s9t_pkg.names_map_typ;
   VAT_CODES_cols         s9t_pkg.names_map_typ;
   VAT_CODE_RATES_cols    s9t_pkg.names_map_typ;
   VAT_CODES_CFA_EXT_cols s9t_pkg.names_map_typ;
   VAT_CODES_TL_cols      s9t_pkg.names_map_typ;
   VAT_REG_cols           s9t_pkg.names_map_typ;
   VAT_REG_TL_cols        s9t_pkg.names_map_typ;
FUNCTION get_col_indx(I_names   IN s9t_pkg.names_map_typ,
                      I_col_key IN VARCHAR2)
RETURN NUMBER IS
BEGIN
   IF I_names.exists(I_col_key) THEN
     RETURN I_names(I_col_key);
   ELSE
     RETURN NULL;
   END IF;
END get_col_indx;

BEGIN
   L_sheets                       := s9t_pkg.get_sheet_names(I_file_id);
   --VAT_CODES
   VAT_CODES_cols                 := s9t_pkg.get_col_names(I_file_id,VAT_CODES_sheet);
   VAT_CODES$Action               := VAT_CODES_cols('ACTION');
   VAT_CODES$VAT_CODE             := VAT_CODES_cols('VAT_CODE');
   VAT_CODES$VAT_CODE_DESC        := VAT_CODES_cols('VAT_CODE_DESC');
   VAT_CODES$INCL_NIC_IND         := VAT_CODES_cols('INCL_NIC_IND');
   --VAT_CODES_TL
   VAT_CODES_TL_cols              := s9t_pkg.get_col_names(I_file_id,VAT_CODES_TL_sheet);
   VAT_CODES_TL$Action            := VAT_CODES_TL_cols('ACTION');
   VAT_CODES_TL$LANG              := VAT_CODES_TL_cols('LANG');
   VAT_CODES_TL$VAT_CODE          := VAT_CODES_TL_cols('VAT_CODE');
   VAT_CODES_TL$VAT_CODE_DESC     := VAT_CODES_TL_cols('VAT_CODE_DESC');
 --VAT_CODES_CFA_EXT
   VAT_CODES_CFA_EXT_cols         := s9t_pkg.get_col_names(I_file_id,VAT_CODES_CFA_EXT_sheet);
   VAT_CODES_CFA_EXT$Action       := get_col_indx(VAT_CODES_CFA_EXT_cols,'ACTION');
   VAT_CODES_CFA_EXT$VAT_CODE     := get_col_indx(VAT_CODES_CFA_EXT_cols,'VAT_CODE');
   VAT_CODES_CFA_EXT$GROUP_ID     := get_col_indx(VAT_CODES_CFA_EXT_cols,'GROUP_ID');
   VAT_CODES_CFA_EXT$VARCHAR2_1   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_1');
   VAT_CODES_CFA_EXT$VARCHAR2_2   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_2');
   VAT_CODES_CFA_EXT$VARCHAR2_3   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_3');
   VAT_CODES_CFA_EXT$VARCHAR2_4   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_4');
   VAT_CODES_CFA_EXT$VARCHAR2_5   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_5');
   VAT_CODES_CFA_EXT$VARCHAR2_6   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_6');
   VAT_CODES_CFA_EXT$VARCHAR2_7   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_7');
   VAT_CODES_CFA_EXT$VARCHAR2_8   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_8');
   VAT_CODES_CFA_EXT$VARCHAR2_9   := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_9');
   VAT_CODES_CFA_EXT$VARCHAR2_10  := get_col_indx(VAT_CODES_CFA_EXT_cols,'VARCHAR2_10');
   VAT_CODES_CFA_EXT$NUMBER_11    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_11');
   VAT_CODES_CFA_EXT$NUMBER_12    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_12');
   VAT_CODES_CFA_EXT$NUMBER_13    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_13');
   VAT_CODES_CFA_EXT$NUMBER_14    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_14');
   VAT_CODES_CFA_EXT$NUMBER_15    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_15');
   VAT_CODES_CFA_EXT$NUMBER_16    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_16');
   VAT_CODES_CFA_EXT$NUMBER_17    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_17');
   VAT_CODES_CFA_EXT$NUMBER_18    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_18');
   VAT_CODES_CFA_EXT$NUMBER_19    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_19');
   VAT_CODES_CFA_EXT$NUMBER_20    := get_col_indx(VAT_CODES_CFA_EXT_cols,'NUMBER_20');
   VAT_CODES_CFA_EXT$DATE_21      := get_col_indx(VAT_CODES_CFA_EXT_cols,'DATE_21');
   VAT_CODES_CFA_EXT$DATE_22      := get_col_indx(VAT_CODES_CFA_EXT_cols,'DATE_22');
   VAT_CODES_CFA_EXT$DATE_23      := get_col_indx(VAT_CODES_CFA_EXT_cols,'DATE_23');
   VAT_CODES_CFA_EXT$DATE_24      := get_col_indx(VAT_CODES_CFA_EXT_cols,'DATE_24');
   VAT_CODES_CFA_EXT$DATE_25      := get_col_indx(VAT_CODES_CFA_EXT_cols,'DATE_25');
   
  --VAT_CODE_RATES
   VAT_CODE_RATES_cols            := s9t_pkg.get_col_names(I_file_id,VAT_CODE_RATES_sheet);
   VAT_CODE_RATES$Action          := VAT_CODE_RATES_cols('ACTION');
   VAT_CODE_RATES$VAT_CODE        := VAT_CODE_RATES_cols('VAT_CODE');
   VAT_CODE_RATES$ACTIVE_DATE     := VAT_CODE_RATES_cols('ACTIVE_DATE');
   VAT_CODE_RATES$ACTIVE_DATE_S9T := VAT_CODE_RATES_cols('ACTIVE_DATE_S9T');
   VAT_CODE_RATES$VAT_RATE        := VAT_CODE_RATES_cols('VAT_RATE');
   --VAT_REGIONS
   VAT_REG_cols                  := s9t_pkg.get_col_names(I_file_id,VAT_REG_sheet);
   VAT_REG$Action                := VAT_REG_cols('ACTION');
   VAT_REG$VAT_REGION            := VAT_REG_cols('VAT_REGION');
   VAT_REG$VAT_REGION_NAME       := VAT_REG_cols('VAT_REGION_NAME');
   VAT_REG$VAT_REGION_TYPE       := VAT_REG_cols('VAT_REGION_TYPE');
   VAT_REG$ACQUISITION_VAT_IND   := VAT_REG_cols('ACQUISITION_VAT_IND');
   VAT_REG$REVERSE_VAT_THRESHOLD := VAT_REG_cols('REVERSE_VAT_THRESHOLD');
   VAT_REG$VAT_CALC_TYPE         := VAT_REG_cols('VAT_CALC_TYPE');
   --VAT_REGIONS_TL
   VAT_REG_TL_cols               := s9t_pkg.get_col_names(I_file_id,VAT_REG_TL_sheet);
   VAT_REG_TL$Action             := VAT_REG_TL_cols('ACTION');
   VAT_REG_TL$LANG               := VAT_REG_TL_cols('LANG');
   VAT_REG_TL$VAT_REGION         := VAT_REG_TL_cols('VAT_REGION');
   VAT_REG_TL$VAT_REGION_NAME    := VAT_REG_TL_cols('VAT_REGION_NAME');
   
END POPULATE_NAMES;
-------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_VAT_CODES ( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = vat_codes_sheet )
   select s9t_row(s9t_cells(CORESVC_VAT.action_mod,
                            vat_code,
                            vat_code_desc,
                            incl_nic_ind ))
     from vat_codes;
END POPULATE_VAT_CODES;
-------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_VAT_CODES_TL ( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = VAT_CODES_TL_SHEET )
   select s9t_row(s9t_cells(CORESVC_VAT.action_mod,
                            lang,
                            vat_code,
                            vat_code_desc))
     from vat_codes_tl;
END POPULATE_VAT_CODES_TL;
-------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_VAT_CODE_RATES( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into table
     (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id    = I_file_id
         and ss.sheet_name = vat_code_rates_sheet )
   select s9t_row(s9t_cells(CORESVC_VAT.action_mod,
                            vat_code,
                            active_date,
                            active_date,
                            vat_rate ))
     from vat_code_rates;
END POPULATE_VAT_CODE_RATES;
-------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_VAT_REG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = VAT_REG_sheet )
   select s9t_row(s9t_cells(CORESVC_VAT.action_mod,
                            vat_region,
                            vat_region_name,
                            vat_region_type,
                            acquisition_vat_ind,
                            reverse_vat_threshold,
                            vat_calc_type))
     from vat_region ;
END POPULATE_VAT_REG;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_VAT_REG_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = VAT_REG_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_VAT.action_mod,
                            lang,
                            vat_region,
                            vat_region_name))
     from vat_region_tl ;
END POPULATE_VAT_REG_TL;
------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file       s9t_file;
   L_file_name  s9t_folder.file_name%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(VAT_CODES_sheet);
   L_file.sheets(L_file.get_sheet_index(VAT_CODES_sheet)).column_headers := S9T_CELLS('ACTION',
                                                                                      'VAT_CODE',
                                                                                      'VAT_CODE_DESC',
                                                                                      'INCL_NIC_IND');
   L_file.add_sheet(VAT_CODE_RATES_sheet);
   L_file.sheets(L_file.get_sheet_index(VAT_CODE_RATES_sheet)).column_headers := S9T_CELLS('ACTION',
                                                                                           'VAT_CODE',
                                                                                           'ACTIVE_DATE',
                                                                                           'ACTIVE_DATE_S9T',
                                                                                           'VAT_RATE');

   L_file.add_sheet(VAT_CODES_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(VAT_CODES_TL_sheet)).column_headers := S9T_CELLS('ACTION',
                                                                                         'LANG',
                                                                                         'VAT_CODE',
                                                                                         'VAT_CODE_DESC');
   L_file.add_sheet(VAT_REG_sheet);                                                                                         
   L_file.sheets(L_file.get_sheet_index(VAT_REG_sheet)).column_headers := s9t_cells('ACTION',
                                                                                    'VAT_REGION',
                                                                                    'VAT_REGION_NAME',
                                                                                    'VAT_REGION_TYPE',
                                                                                    'ACQUISITION_VAT_IND',
                                                                                    'REVERSE_VAT_THRESHOLD',
                                                                                    'VAT_CALC_TYPE'  );

   L_file.add_sheet(VAT_REG_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(VAT_REG_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                       'LANG',
                                                                                       'VAT_REGION',
                                                                                       'VAT_REGION_NAME');
                                                                                         
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
-------------------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_VAT.CREATE_S9T';
   L_file    s9t_file;
BEGIN

   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_VAT_CODES(O_file_id);
      POPULATE_VAT_CODE_RATES(O_file_id);
      POPULATE_VAT_CODES_TL(O_file_id);
      POPULATE_VAT_REG(O_file_id);
      POPULATE_VAT_REG_TL(O_file_id);
      COMMIT;
   end if;
   --VAT_CODES_EXT
   if CORESVC_CFLEX.ADD_CFLEX_SHEETS(O_error_message,
                                     O_file_id,
                                     template_key,
                                     I_template_only_ind) = FALSE   then
      return FALSE;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_VAT_CODES ( I_file_id      IN   s9t_folder.file_id%TYPE,
                                  I_process_id   IN   SVC_VAT_CODES.process_id%TYPE ) IS

   TYPE svc_VAT_CODES_col_typ IS TABLE OF SVC_VAT_CODES%ROWTYPE;

   L_temp_rec         SVC_VAT_CODES%ROWTYPE;
   L_default_rec      SVC_VAT_CODES%ROWTYPE;
   L_process_id       SVC_VAT_CODES.process_id%type;
   svc_vat_codes_col  svc_VAT_CODES_col_typ  :=  NEW svc_vat_codes_col_typ();
   L_error            BOOLEAN                :=  FALSE;

   cursor C_MANDATORY_IND is
      select vat_code_mi,
             vat_code_desc_mi,
             incl_nic_ind_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = CORESVC_VAT.template_key
                 and wksht_key    ='VAT_CODES')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('VAT_CODE'      as VAT_CODE,
                                                                'VAT_CODE_DESC' as VAT_CODE_DESC,
                                                                'INCL_NIC_IND'  as INCL_NIC_IND,
                                                                 NULL           as dummy));
   L_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_VAT_CODES';
   L_pk_columns    VARCHAR2(255)  := 'VAT Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   -- Get default values.
   FOR rec IN
   (select vat_code_dv,
           vat_code_desc_dv,
           incl_nic_ind_dv,
    null as dummy
  from (select column_key,
               default_value
          from s9t_tmpl_cols_def
         where template_key  = CORESVC_VAT.template_key
           and wksht_key     = 'VAT_CODES')
         PIVOT (MAX(default_value) as dv FOR (column_key) IN ('VAT_CODE'      as VAT_CODE,
                                                              'VAT_CODE_DESC' as VAT_CODE_DESC,
                                                              'INCL_NIC_IND'  as INCL_NIC_IND,
                                                               NULL           as dummy))) LOOP

   BEGIN
    L_default_rec.VAT_CODE := rec.VAT_CODE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_CODES',
                          NULL,
                         'VAT_CODE',
                         'INV_DEFAULT',
                          SQLERRM);
   END;


   BEGIN
    L_default_rec.VAT_CODE_DESC := rec.VAT_CODE_DESC_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_CODES',
                          NULL,
                         'VAT_CODE_DESC',
                         'INV_DEFAULT',
                          SQLERRM);
   END;


   BEGIN
   L_default_rec.INCL_NIC_IND := rec.INCL_NIC_IND_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_CODES',
                          NULL,
                         'INCL_NIC_IND',
                         'INV_DEFAULT',
                          SQLERRM);
   END;
   END LOOP;

 --Get mandatory indicators
   open c_mandatory_ind;
   fetch c_mandatory_ind
   into L_mi_rec;
   close c_mandatory_ind;
   FOR rec IN
    (select r.get_cell(vat_codes$action)              as action,
            UPPER(r.get_cell(vat_codes$vat_code))     as vat_code,
            r.get_cell(vat_codes$vat_code_desc)       as vat_code_desc,
            r.get_cell(vat_codes$incl_nic_ind)        as incl_nic_ind,
            r.get_row_seq()                           as row_seq
       from s9t_folder sf,
            TABLE(sf.s9t_file_obj.sheets) ss,
            TABLE(ss.s9t_rows) r
      where sf.file_id  = i_file_id
        and ss.sheet_name = sheet_name_trans(vat_codes_sheet))  LOOP

      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;

   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          VAT_CODES_sheet,
                          rec.row_seq,
                          action_column,
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.VAT_CODE := rec.VAT_CODE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          VAT_CODES_sheet,
                          rec.row_seq,
                         'VAT_CODE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.VAT_CODE_DESC := rec.VAT_CODE_DESC;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          VAT_CODES_sheet,
                          rec.row_seq,
                         'VAT_CODE_DESC',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.INCL_NIC_IND := rec.INCL_NIC_IND;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          VAT_CODES_sheet,
                          rec.row_seq,
                         'INCL_NIC_IND',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   end;

    if rec.action = CORESVC_VAT.action_new then
       L_temp_rec.VAT_CODE      := NVL(L_temp_rec.VAT_CODE,
                                       L_default_rec.VAT_CODE);
       L_temp_rec.VAT_CODE_DESC := NVL(L_temp_rec.VAT_CODE_DESC,
                                       L_default_rec.VAT_CODE_DESC);
       L_temp_rec.INCL_NIC_IND  := NVL(L_temp_rec.INCL_NIC_IND,
                                       L_default_rec.INCL_NIC_IND);
    end if;

    if NOT ( L_temp_rec.VAT_CODE is NOT NULL
             and 1 = 1 ) then
             WRITE_S9T_ERROR( I_file_id,
                              VAT_CODES_sheet,
                              rec.row_seq,
                              NULL,
                              NULL,
                              SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
             L_error := TRUE;
   end if;

   if NOT L_error then
          svc_VAT_CODES_col.EXTEND();
          svc_VAT_CODES_col(svc_VAT_CODES_col.COUNT()):=L_temp_rec;
   end if;

  END LOOP;

   BEGIN
   forall i IN 1..svc_VAT_CODES_col.COUNT SAVE EXCEPTIONS
   merge INTO SVC_VAT_CODES st
   using (select (case
                     when L_mi_rec.vat_code_mi        = 'N'
                      and svc_vat_codes_col(i).action = CORESVC_VAT.action_mod
                      and s1.vat_code       is NULL then
                          mt.vat_code
                     else s1.vat_code
                     end) as vat_code,
                 (case
                     when L_mi_rec.vat_code_desc_mi   = 'N'
                      and svc_vat_codes_col(i).action = CORESVC_VAT.action_mod
                      and s1.vat_code_desc  is NULL then
                          mt.vat_code_desc
                      else s1.vat_code_desc
                      end) as vat_code_desc,
                 (case
                     when L_mi_rec.incl_nic_ind_mi    = 'N'
                      and svc_vat_codes_col(i).action = CORESVC_VAT.action_mod
                      and s1.incl_nic_ind   is NULL then
                          mt.incl_nic_ind
                     else s1.incl_nic_ind
                     end) as incl_nic_ind,
                     null as dummy
            from (select svc_vat_codes_col(i).vat_code      as vat_code,
                         svc_vat_codes_col(i).vat_code_desc as vat_code_desc,
                         svc_vat_codes_col(i).incl_nic_ind  as incl_nic_ind,
                                                       null as dummy
                    from dual) s1, VAT_CODES mt
                   where mt.VAT_CODE (+) = s1.VAT_CODE
                         and           1 = 1) sq
      ON ( st.VAT_CODE = sq.VAT_CODE and
           svc_VAT_CODES_col(i).action in (CORESVC_VAT.action_mod,CORESVC_VAT.action_del))

      when matched then
          update
               set PROCESS_ID        = svc_VAT_CODES_col(i).PROCESS_ID ,
                   CHUNK_ID          = svc_VAT_CODES_col(i).CHUNK_ID ,
                   ROW_SEQ           = svc_VAT_CODES_col(i).ROW_SEQ ,
                   action            = svc_VAT_CODES_col(i).ACTION,
                   PROCESS$STATUS    = svc_VAT_CODES_col(i).PROCESS$STATUS ,
                   VAT_CODE_DESC     = sq.VAT_CODE_DESC ,
                   INCL_NIC_IND      = sq.INCL_NIC_IND ,
                   CREATE_ID         = svc_VAT_CODES_col(i).CREATE_ID ,
                   CREATE_DATETIME   = svc_VAT_CODES_col(i).CREATE_DATETIME ,
                   LAST_UPD_ID       = svc_VAT_CODES_col(i).LAST_UPD_ID ,
                   LAST_UPD_DATETIME = svc_VAT_CODES_col(i).LAST_UPD_DATETIME
      when NOT matched then
         insert( process_id ,
                 chunk_id ,
                 row_seq ,
                 action ,
                 process$status ,
                 vat_code ,
                 vat_code_desc ,
                 incl_nic_ind ,
                 create_id ,
                 create_datetime ,
                 last_upd_id ,
                 last_upd_datetime)
         values( svc_vat_codes_col(i).process_id ,
                 svc_vat_codes_col(i).chunk_id ,
                 svc_vat_codes_col(i).row_seq ,
                 svc_vat_codes_col(i).action ,
                 svc_vat_codes_col(i).process$status ,
                 sq.vat_code ,
                 sq.vat_code_desc ,
                 sq.incl_nic_ind ,
                 svc_vat_codes_col(i).create_id ,
                 svc_vat_codes_col(i).create_datetime ,
                 svc_vat_codes_col(i).last_upd_id ,
                 svc_vat_codes_col(i).last_upd_datetime);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          VAT_CODES_sheet,
                          svc_VAT_CODES_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;

   END;
END PROCESS_S9T_VAT_CODES;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_VAT_CODES_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_VAT_CODES_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_VAT_CODES_TL_COL_TYP IS TABLE OF SVC_VAT_CODES_TL%ROWTYPE;
   L_temp_rec             SVC_VAT_CODES_TL%ROWTYPE;
   SVC_VAT_CODES_TL_col   SVC_VAT_CODES_TL_COL_TYP := NEW SVC_VAT_CODES_TL_COL_TYP();
   L_process_id           SVC_VAT_CODES_TL.PROCESS_ID%TYPE;
   L_error                BOOLEAN := FALSE;
   L_default_rec          SVC_VAT_CODES_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select vat_code_desc_mi,
             vat_code_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'VAT_CODES_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('VAT_CODE_DESC' as vat_code_desc,
                                       'VAT_CODE'      as vat_code,
                                       'LANG'          as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_VAT_CODES_TL';
   L_pk_columns    VARCHAR2(255)  := 'VAT Code, LANG';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select vat_code_desc_dv,
                      vat_code_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'VAT_CODES_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('VAT_CODE_DESC' as vat_code_desc,
                                                'VAT_CODE'      as vat_code,
                                                'LANG'          as lang)))
   LOOP
      BEGIN
         L_default_rec.vat_code_desc := rec.vat_code_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_CODES_TL_SHEET ,
                            NULL,
                           'VAT_CODE_DESC' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.vat_code := rec.vat_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           VAT_CODES_TL_SHEET ,
                            NULL,
                           'VAT_CODE' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_CODES_TL_SHEET ,
                            NULL,
                           'LANG' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(vat_codes_tl$action))       as action,
                      r.get_cell(vat_codes_tl$vat_code_desc)       as vat_code_desc,
                      UPPER(r.get_cell(vat_codes_tl$vat_code))     as vat_code,
                      r.get_cell(vat_codes_tl$lang)                as lang,
                      r.get_row_seq()                              as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(VAT_CODES_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            vat_codes_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.vat_code_desc := rec.vat_code_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_CODES_TL_SHEET,
                            rec.row_seq,
                            'VAT_CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.vat_code := rec.vat_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_CODES_TL_SHEET,
                            rec.row_seq,
                            'VAT_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_CODES_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_VAT.action_new then
         L_temp_rec.vat_code_desc := NVL( L_temp_rec.vat_code_desc,L_default_rec.vat_code_desc);
         L_temp_rec.vat_code      := NVL( L_temp_rec.vat_code,L_default_rec.vat_code);
         L_temp_rec.lang          := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.vat_code is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         VAT_CODES_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_VAT_CODES_TL_col.extend();
         SVC_VAT_CODES_TL_col(SVC_VAT_CODES_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_VAT_CODES_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_VAT_CODES_TL st
      using(select
                  (case
                   when l_mi_rec.vat_code_desc_mi = 'N'
                    and SVC_VAT_CODES_TL_col(i).action = CORESVC_VAT.action_mod
                    and s1.vat_code_desc IS NULL then
                        mt.vat_code_desc
                   else s1.vat_code_desc
                   end) as vat_code_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_VAT_CODES_TL_col(i).action = CORESVC_VAT.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.vat_code_mi = 'N'
                    and SVC_VAT_CODES_TL_col(i).action = CORESVC_VAT.action_mod
                    and s1.vat_code IS NULL then
                        mt.vat_code
                   else s1.vat_code
                   end) as vat_code
              from (select SVC_VAT_CODES_TL_col(i).vat_code_desc as vat_code_desc,
                           SVC_VAT_CODES_TL_col(i).vat_code        as vat_code,
                           SVC_VAT_CODES_TL_col(i).lang              as lang
                      from dual) s1,
                   vat_codes_TL mt
             where mt.vat_code (+) = s1.vat_code
               and mt.lang (+)       = s1.lang) sq
                on (st.vat_code = sq.vat_code and
                    st.lang = sq.lang and
                    SVC_VAT_CODES_TL_col(i).ACTION IN (CORESVC_VAT.action_mod,CORESVC_VAT.action_del))
      when matched then
      update
         set process_id        = SVC_VAT_CODES_TL_col(i).process_id ,
             chunk_id          = SVC_VAT_CODES_TL_col(i).chunk_id ,
             row_seq           = SVC_VAT_CODES_TL_col(i).row_seq ,
             action            = SVC_VAT_CODES_TL_col(i).action ,
             process$status    = SVC_VAT_CODES_TL_col(i).process$status ,
             vat_code_desc = sq.vat_code_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             vat_code_desc ,
             vat_code ,
             lang)
      values(SVC_VAT_CODES_TL_col(i).process_id ,
             SVC_VAT_CODES_TL_col(i).chunk_id ,
             SVC_VAT_CODES_TL_col(i).row_seq ,
             SVC_VAT_CODES_TL_col(i).action ,
             SVC_VAT_CODES_TL_col(i).process$status ,
             sq.vat_code_desc ,
             sq.vat_code ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            VAT_CODES_TL_SHEET,
                            SVC_VAT_CODES_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_VAT_CODES_TL;
-------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_VAT_CODE_RATES( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id   IN   SVC_VAT_CODE_RATES.process_id%TYPE ) IS
   TYPE svc_VAT_CODE_RATES_col_typ is TABLE OF SVC_VAT_CODE_RATES%ROWTYPE;
   L_temp_rec             SVC_VAT_CODE_RATES%ROWTYPE;
   svc_vat_code_rates_col svc_VAT_CODE_RATES_col_typ := NEW svc_VAT_CODE_RATES_col_typ();
   L_process_id           SVC_VAT_CODE_RATES.process_id%TYPE;
   L_error                BOOLEAN                    := FALSE;
   L_default_rec          SVC_VAT_CODE_RATES%ROWTYPE;
  cursor C_MANDATORY_IND IS
     select vat_code_mi,
            active_date_mi,
            vat_rate_mi,
            1 as dummy
       from (select column_key,
                    mandatory
               from s9t_tmpl_cols_def
              where template_key  = CORESVC_VAT.template_key
                and wksht_key     = 'VAT_CODE_RATES')
              PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('VAT_CODE'     as VAT_CODE,
                                                               'ACTIVE_DATE'  as ACTIVE_DATE,
                                                               'VAT_RATE'     as VAT_RATE,
                                                                NULL          as dummy));
   L_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_VAT_CODE_RATES';
   L_pk_columns    VARCHAR2(255)  := 'VAT Code,Active Date';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   -- Get default values.
   FOR rec IN
   (select vat_code_dv,
           active_date_dv,
           vat_rate_dv,
           null as dummy
      from (select column_key,
                   default_value
      from s9t_tmpl_cols_def
     where template_key  = CORESVC_VAT.template_key
       and wksht_key     = 'VAT_CODE_RATES')
     PIVOT (MAX(default_value) as dv FOR (column_key) IN ('VAT_CODE'     as VAT_CODE,
                                                          'ACTIVE_DATE'  as ACTIVE_DATE,
                                                          'VAT_RATE'     as VAT_RATE,
                                                           NULL          as dummy)))
LOOP

BEGIN
   L_default_rec.VAT_CODE := rec.VAT_CODE_dv;
EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                      'VAT_CODE_RATES',
                       NULL,
                      'VAT_CODE',
                      'INV_DEFAULT',
                       SQLERRM);
END;

BEGIN
   L_default_rec.ACTIVE_DATE := rec.ACTIVE_DATE_dv;
EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                      'VAT_CODE_RATES',
                       NULL,
                      'ACTIVE_DATE',
                      'INV_DEFAULT',
                       SQLERRM);
END;

BEGIN
   L_default_rec.VAT_RATE := rec.VAT_RATE_dv;
EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                      'VAT_CODE_RATES',
                       NULL,
                      'VAT_RATE',
                      'INV_DEFAULT',
                       SQLERRM);
END;
END LOOP;

   --Get mandatory indicators
   open c_mandatory_ind;
   fetch c_mandatory_ind
   into l_mi_rec;
   close c_mandatory_ind;
   FOR rec IN
   (select r.get_cell(vat_code_rates$action)                as action,
           UPPER(r.get_cell(vat_code_rates$vat_code))       as vat_code,
           r.get_cell(vat_code_rates$active_date)           as active_date,
           r.get_cell(vat_code_rates$active_date_s9t)       as active_date_s9t,
           r.get_cell(vat_code_rates$vat_rate)              as vat_rate,
           r.get_row_seq()                                  as row_seq
      from s9t_folder sf,
           TABLE(sf.s9t_file_obj.sheets) ss,
           TABLE(ss.s9t_rows)            r
     where sf.file_id    = I_file_id
       and ss.sheet_name = sheet_name_trans(vat_code_rates_sheet)) LOOP

     L_temp_rec := NULL;
     L_temp_rec.process_id        := I_process_id;
     L_temp_rec.chunk_id          := 1;
     L_temp_rec.row_seq           := rec.row_seq;
     L_temp_rec.process$status    := 'N';
     L_temp_rec.create_id         := GET_USER;
     L_temp_rec.last_upd_id       := GET_USER;
     L_temp_rec.create_datetime   := SYSDATE;
     L_temp_rec.last_upd_datetime := SYSDATE;
     L_error                      := FALSE;

   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          VAT_CODE_RATES_sheet,
                          rec.row_seq,
                          action_column,
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.VAT_CODE := rec.VAT_CODE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          VAT_CODE_RATES_sheet,
                          rec.row_seq,
                         'VAT_CODE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.ACTIVE_DATE := rec.ACTIVE_DATE;
   EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                       VAT_CODE_RATES_sheet,
                       rec.row_seq,
                      'ACTIVE_DATE',
                       SQLCODE,
                       SQLERRM);
      L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.ACTIVE_DATE_S9T := rec.ACTIVE_DATE_S9T;
   EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                       VAT_CODE_RATES_sheet,
                       rec.row_seq,
                      'ACTIVE_DATE_S9T',
                       SQLCODE,
                       SQLERRM);
      L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.VAT_RATE := rec.VAT_RATE;
   EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                       VAT_CODE_RATES_sheet,
                       rec.row_seq,
                      'VAT_RATE',
                       SQLCODE,
                       SQLERRM);
      L_error := TRUE;
   END;

   if rec.action = CORESVC_VAT.action_new then
      L_temp_rec.VAT_CODE    := NVL( L_temp_rec.VAT_CODE,
                                     L_default_rec.VAT_CODE);
      L_temp_rec.ACTIVE_DATE := NVL( L_temp_rec.ACTIVE_DATE,
                                     L_default_rec.ACTIVE_DATE);
      L_temp_rec.VAT_RATE    := NVL( L_temp_rec.VAT_RATE,
                                     L_default_rec.VAT_RATE);
   end if;

   if NOT ( L_temp_rec.ACTIVE_DATE is NOT NULL or
            L_temp_rec.VAT_CODE    is NOT NULL ) then
            WRITE_S9T_ERROR( I_file_id,
                             VAT_CODE_RATES_sheet,
                             rec.row_seq,
                             NULL,
                             NULL,
                             SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
            L_error := TRUE;
   end if;

   if NOT L_error then
      svc_VAT_CODE_RATES_col.EXTEND();
      svc_VAT_CODE_RATES_col(svc_VAT_CODE_RATES_col.COUNT()):=L_temp_rec;
   end if;

   END LOOP;

   BEGIN
   forall i IN 1..svc_VAT_CODE_RATES_col.count SAVE EXCEPTIONS
   merge INTO SVC_VAT_CODE_RATES st
   using(SELECT(case
                   when l_mi_rec.vat_code_mi    = 'N'
                    and svc_vat_code_rates_col(i).action = CORESVC_VAT.action_mod
                    and s1.vat_code    is NULL then
                        mt.vat_code
                   else s1.vat_code
                   end) as vat_code,
               (case
                   when l_mi_rec.active_date_mi = 'N'
                    and svc_vat_code_rates_col(i).action = CORESVC_VAT.action_mod
                    and s1.active_date is NULL then
                        mt.active_date
                   else s1.active_date
                   end) as active_date,
               (case
                   when l_mi_rec.vat_rate_mi    = 'N'
                    and svc_vat_code_rates_col(i).action = CORESVC_VAT.action_mod
                    and s1.vat_rate    is NULL then
                        mt.vat_rate
                   else s1.vat_rate
                    end) as vat_rate,
                    null as dummy
           from (select svc_VAT_CODE_RATES_col(i).VAT_CODE     as VAT_CODE,
                        svc_VAT_CODE_RATES_col(i).ACTIVE_DATE  as ACTIVE_DATE,
                        svc_VAT_CODE_RATES_col(i).VAT_RATE     as VAT_RATE,
                                                         null  as dummy
                   from dual) s1, VAT_CODE_RATES mt
                  WHERE mt.ACTIVE_DATE (+)  = s1.ACTIVE_DATE and
                        mt.VAT_CODE (+)     = s1.VAT_CODE    and
                                          1 = 1) sq
      ON ( st.ACTIVE_DATE = sq.ACTIVE_DATE and
           st.VAT_CODE    = sq.VAT_CODE    and
           svc_VAT_CODE_RATES_col(i).action in (CORESVC_VAT.action_mod,CORESVC_VAT.action_del))

      when matched then
         update
            set PROCESS_ID        = svc_VAT_CODE_RATES_col(i).PROCESS_ID ,
                CHUNK_ID          = svc_VAT_CODE_RATES_col(i).CHUNK_ID ,
                ROW_SEQ           = svc_VAT_CODE_RATES_col(i).ROW_SEQ ,
                ACTION            = svc_VAT_CODE_RATES_col(i).ACTION,
                PROCESS$STATUS    = svc_VAT_CODE_RATES_col(i).PROCESS$STATUS ,
                VAT_RATE          = sq.VAT_RATE ,
                CREATE_ID         = svc_VAT_CODE_RATES_col(i).CREATE_ID ,
                CREATE_DATETIME   = svc_VAT_CODE_RATES_col(i).CREATE_DATETIME ,
                LAST_UPD_ID       = svc_VAT_CODE_RATES_col(i).LAST_UPD_ID ,
                LAST_UPD_DATETIME = svc_VAT_CODE_RATES_col(i).LAST_UPD_DATETIME
      when not matched then
         insert( PROCESS_ID ,
                 CHUNK_ID ,
                 ROW_SEQ ,
                 ACTION ,
                 PROCESS$STATUS ,
                 VAT_CODE ,
                 ACTIVE_DATE ,
                 ACTIVE_DATE_S9T,
                 VAT_RATE ,
                 CREATE_ID ,
                 CREATE_DATETIME ,
                 LAST_UPD_ID ,
                 LAST_UPD_DATETIME)
         values( svc_VAT_CODE_RATES_col(i).PROCESS_ID ,
                 svc_VAT_CODE_RATES_col(i).CHUNK_ID ,
                 svc_VAT_CODE_RATES_col(i).ROW_SEQ ,
                 svc_VAT_CODE_RATES_col(i).ACTION ,
                 svc_VAT_CODE_RATES_col(i).PROCESS$STATUS ,
                 sq.VAT_CODE ,
                 sq.ACTIVE_DATE ,
                 svc_VAT_CODE_RATES_col(i).ACTIVE_DATE_S9T ,
                 sq.VAT_RATE ,
                 svc_VAT_CODE_RATES_col(i).CREATE_ID ,
                 svc_VAT_CODE_RATES_col(i).CREATE_DATETIME ,
                 svc_VAT_CODE_RATES_col(i).LAST_UPD_ID ,
                 svc_VAT_CODE_RATES_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          VAT_CODE_RATES_sheet,
                          svc_VAT_CODE_RATES_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
  END;
END PROCESS_S9T_VAT_CODE_RATES;
------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_VAT_REG( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id   IN   SVC_VAT_REGION.process_id%TYPE) IS
   TYPE svc_VAT_REG_col_typ IS TABLE OF SVC_VAT_REGION%ROWTYPE;
   L_temp_rec        SVC_VAT_REGION%ROWTYPE;
   L_default_rec     SVC_VAT_REGION%ROWTYPE;
   L_process_id      SVC_VAT_REGION.process_id%TYPE;
   L_error           BOOLEAN             := FALSE;
   svc_vat_reg_col   svc_vat_reg_col_typ := NEW svc_vat_reg_col_typ();

   cursor C_MANDATORY_IND is
      select REVERSE_VAT_THRESHOLD_mi,
             VAT_REGION_mi,
             VAT_REGION_NAME_mi,
             VAT_REGION_TYPE_mi,
             ACQUISITION_VAT_IND_mi,
             VAT_CALC_TYPE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_VAT.template_key
                 and wksht_key      = 'VAT_REGION')
               PIVOT (MAX(mandatory) AS mi
                      FOR (column_key) IN ('REVERSE_VAT_THRESHOLD' as reverse_vat_threshold,
                                           'VAT_REGION'            as vat_region,
                                           'VAT_REGION_NAME'       as vat_region_name,
                                           'VAT_REGION_TYPE'       as vat_region_type,
                                           'ACQUISITION_VAT_IND'   as acquisition_vat_ind,
                                           'VAT_CALC_TYPE'         as vat_calc_type,
                                            null                   as dummy));
   l_mi_rec        c_mandatory_ind%rowtype;
   dml_errors      exception;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_VAT_REGION';
   L_pk_columns    VARCHAR2(255)  := 'VAT Region';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select REVERSE_VAT_THRESHOLD_dv,
                      vat_region_dv,
                      vat_region_name_dv,
                      vat_region_type_dv,
                      acquisition_vat_ind_dv,
                      vat_calc_type_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_VAT.template_key
                          and wksht_key     = 'VAT_REGION')
                        PIVOT (MAX(default_value) as dv
                               FOR (column_key) in ('REVERSE_VAT_THRESHOLD' as reverse_vat_threshold,
                                                    'VAT_REGION'            as vat_region,
                                                    'VAT_REGION_NAME'       as vat_region_name,
                                                    'VAT_REGION_TYPE'       as vat_region_type,
                                                    'ACQUISITION_VAT_IND'   as acquisition_vat_ind,
                                                    'VAT_CALC_TYPE'         as vat_calc_type,
                                                     null                   as dummy)))
   LOOP
   BEGIN
      L_default_rec.REVERSE_VAT_THRESHOLD := rec.REVERSE_VAT_THRESHOLD_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_REGION',
                          NULL,
                         'REVERSE_VAT_THRESHOLD',
                         'INV_DEFAULT',
                          SQLERRM);
   END;
   BEGIN
      L_default_rec.VAT_REGION := rec.VAT_REGION_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_REGION',
                          NULL,
                         'VAT_REGION',
                         'INV_DEFAULT',
                          SQLERRM);
   END;
   BEGIN
      L_default_rec.VAT_REGION_NAME := rec.VAT_REGION_NAME_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_REGION',
                          NULL,
                         'VAT_REGION_NAME',
                         'INV_DEFAULT',
                          SQLERRM);
   END;
   BEGIN
      L_default_rec.VAT_REGION_TYPE := rec.VAT_REGION_TYPE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_REGION',
                          NULL,
                         'VAT_REGION_TYPE',
                         'INV_DEFAULT',
                          SQLERRM);
   END;
   BEGIN
      L_default_rec.ACQUISITION_VAT_IND := rec.ACQUISITION_VAT_IND_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_REGION',
                          NULL,
                         'ACQUISITION_VAT_IND',
                         'INV_DEFAULT',
                          SQLERRM);
   END;
   BEGIN
      L_default_rec.VAT_CALC_TYPE := rec.VAT_CALC_TYPE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'VAT_REGION',
                          NULL,
                         'VAT_CALC_TYPE',
                         'INV_DEFAULT',
                          SQLERRM);
   END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
   (select r.get_cell(VAT_REG$ACTION)                as action,
           r.get_cell(VAT_REG$REVERSE_VAT_THRESHOLD) as reverse_vat_threshold,
           r.get_cell(VAT_REG$VAT_REGION)            as vat_region,
           r.get_cell(VAT_REG$VAT_REGION_NAME)       as vat_region_name,
           r.get_cell(VAT_REG$VAT_REGION_TYPE)       as vat_region_type,
           r.get_cell(VAT_REG$ACQUISITION_VAT_IND)   as acquisition_vat_ind,
           r.get_cell(VAT_REG$VAT_CALC_TYPE)         as vat_calc_type,
           r.get_row_seq()                           as row_seq
      from s9t_folder sf,
           TABLE(sf.s9t_file_obj.sheets) ss,
           TABLE(ss.s9t_rows) r
     where sf.file_id  = i_file_id
       and ss.sheet_name = sheet_name_trans(vat_reg_sheet))

     LOOP
       L_temp_rec                   := NULL;
     L_temp_rec.process_id        := I_process_id;
     L_temp_rec.chunk_id          := 1;
     L_temp_rec.row_seq           := rec.row_seq;
     L_temp_rec.process$status    := 'N';
     L_temp_rec.create_id         := GET_USER;
     L_temp_rec.last_upd_id       := GET_USER;
     L_temp_rec.create_datetime   := SYSDATE;
     L_temp_rec.last_upd_datetime := SYSDATE;
     L_error := FALSE;

     BEGIN
        L_temp_rec.Action := rec.Action;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR( I_file_id,
                            VAT_REG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.REVERSE_VAT_THRESHOLD := rec.REVERSE_VAT_THRESHOLD;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR( I_file_id,
                            VAT_REG_sheet,
                            rec.row_seq,
                           'REVERSE_VAT_THRESHOLD',
                            SQLCODE,
                            SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.VAT_REGION := rec.VAT_REGION;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR( I_file_id,
                            VAT_REG_sheet,
                            rec.row_seq,
                           'VAT_REGION',
                            SQLCODE,
                            SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.VAT_REGION_NAME := rec.VAT_REGION_NAME;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR( I_file_id,
                            VAT_REG_sheet,
                            rec.row_seq,
                           'VAT_REGION_NAME',
                            SQLCODE,
                            SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.VAT_REGION_TYPE := rec.VAT_REGION_TYPE;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR( I_file_id,
                            VAT_REG_sheet,
                            rec.row_seq,
                           'VAT_REGION_TYPE',
                            SQLCODE,
                            SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.ACQUISITION_VAT_IND := rec.ACQUISITION_VAT_IND;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR( I_file_id,
                            VAT_REG_sheet,
                            rec.row_seq,
                           'ACQUISITION_VAT_IND',
                            SQLCODE,
                            SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.VAT_CALC_TYPE := rec.VAT_CALC_TYPE;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR( I_file_id,
                            VAT_REG_sheet,
                            rec.row_seq,
                           'VAT_CALC_TYPE',
                            SQLCODE,
                            SQLERRM);
           L_error := TRUE;
     END;

     if rec.action = CORESVC_VAT.action_new then
        L_temp_rec.REVERSE_VAT_THRESHOLD := NVL( L_temp_rec.REVERSE_VAT_THRESHOLD,
                                                 L_default_rec.REVERSE_VAT_THRESHOLD);
        L_temp_rec.VAT_REGION            := NVL( L_temp_rec.VAT_REGION,
                                                 L_default_rec.VAT_REGION);
        L_temp_rec.VAT_REGION_NAME       := NVL( L_temp_rec.VAT_REGION_NAME,
                                                 L_default_rec.VAT_REGION_NAME);
        L_temp_rec.VAT_REGION_TYPE       := NVL( L_temp_rec.VAT_REGION_TYPE,
                                                 L_default_rec.VAT_REGION_TYPE);
        L_temp_rec.ACQUISITION_VAT_IND   := NVL( L_temp_rec.ACQUISITION_VAT_IND,
                                                 L_default_rec.ACQUISITION_VAT_IND);
        L_temp_rec.VAT_CALC_TYPE         := NVL( L_temp_rec.VAT_CALC_TYPE,
                                                 L_default_rec.VAT_CALC_TYPE);
     end if;

    if NOT (L_temp_rec.VAT_REGION is NOT NULL) then
            WRITE_S9T_ERROR( I_file_id,
                             VAT_REG_sheet,
                             rec.row_seq,
                             NULL,
                             NULL,
                             SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
            L_error := TRUE;
    end if;

    if NOT L_error then
           svc_VAT_REG_col.extend();
           svc_VAT_REG_col(svc_VAT_REG_col.COUNT()):=L_temp_rec;
    end if;

  END LOOP;

  BEGIN
  forall i IN 1..svc_VAT_REG_col.COUNT SAVE EXCEPTIONS
  merge into SVC_VAT_REGION st
  using
      (select (case
                  when l_mi_rec.reverse_vat_threshold_mi = 'N' and
                       svc_vat_reg_col(i).action         = CORESVC_VAT.action_mod and
                       s1.reverse_vat_threshold             is null then
                       mt.reverse_vat_threshold
                  else s1.reverse_vat_threshold
                  end) as reverse_vat_threshold,
              (case
                  when l_mi_rec.vat_region_mi    = 'N' and
                       svc_vat_reg_col(i).action = CORESVC_VAT.action_mod and
                       s1.vat_region             is null then
                       mt.vat_region
                  else s1.vat_region
                  end) as vat_region,
              (case
                  when l_mi_rec.vat_region_name_mi = 'N' and
                       svc_vat_reg_col(i).action   = CORESVC_VAT.action_mod and
                       s1.vat_region_name             is null then
                       mt.vat_region_name
                  else s1.vat_region_name
                  end) as vat_region_name,
              (case
                  when l_mi_rec.vat_region_type_mi = 'N' and
                       svc_vat_reg_col(i).action   = CORESVC_VAT.action_mod and
                       s1.vat_region_type             is null then
                       mt.vat_region_type
                  else s1.vat_region_type
                  end) as vat_region_type,
              (case
                  when l_mi_rec.acquisition_vat_ind_mi = 'N' and
                       svc_vat_reg_col(i).action       = CORESVC_VAT.action_mod and
                       s1.acquisition_vat_ind             is null then
                       mt.acquisition_vat_ind
                  else s1.acquisition_vat_ind
                  end) as acquisition_vat_ind,
              (case
                  when l_mi_rec.vat_calc_type_mi = 'N' and
                       svc_vat_reg_col(i).action       = CORESVC_VAT.action_mod and
                       s1.vat_calc_type                   is null then
                       mt.vat_calc_type
                  else s1.vat_calc_type
                  end) as vat_calc_type,
                  null as dummy
         from (select svc_vat_reg_col(i).reverse_vat_threshold as reverse_vat_threshold,
                      svc_vat_reg_col(i).vat_region            as vat_region,
                      svc_vat_reg_col(i).vat_region_name       as vat_region_name,
                      svc_vat_reg_col(i).vat_region_type       as vat_region_type,
                      svc_vat_reg_col(i).acquisition_vat_ind   as acquisition_vat_ind,
                      svc_vat_reg_col(i).vat_calc_type         as vat_calc_type,
                      null                                     as dummy
                 from dual) s1,vat_region mt
                where mt.vat_region (+) = s1.vat_region) sq
      ON ( st.VAT_REGION   =   sq.VAT_REGION  and
           svc_vat_reg_col(i).action in (CORESVC_VAT.action_mod,CORESVC_VAT.action_del))

      when matched then
         update set process_id            = svc_vat_reg_col(i).PROCESS_ID ,
                    chunk_id              = svc_vat_reg_col(i).CHUNK_ID ,
                    row_seq               = svc_vat_reg_col(i).ROW_SEQ ,
                    action                = svc_vat_reg_col(i).ACTION,
                    process$status        = svc_vat_reg_col(i).PROCESS$STATUS ,
                    vat_region_name       = sq.VAT_REGION_NAME ,
                    acquisition_vat_ind   = sq.ACQUISITION_VAT_IND ,
                    vat_region_type       = sq.VAT_REGION_TYPE ,
                    reverse_vat_threshold = sq.REVERSE_VAT_THRESHOLD ,
                    vat_calc_type         = sq.VAT_CALC_TYPE ,
                    create_id             = svc_vat_reg_col(i).CREATE_ID ,
                    create_datetime       = svc_vat_reg_col(i).CREATE_DATETIME ,
                    last_upd_id           = svc_vat_reg_col(i).LAST_UPD_ID ,
                    last_upd_datetime     = svc_vat_reg_col(i).LAST_UPD_DATETIME
      when not matched then
         insert( process_id ,
                 chunk_id ,
                 row_seq ,
                 action ,
                 process$status ,
                 reverse_vat_threshold ,
                 vat_region ,
                 vat_region_name ,
                 vat_region_type ,
                 acquisition_vat_ind ,
                 vat_calc_type,
                 create_id ,
                 create_datetime ,
                 last_upd_id ,
                 last_upd_datetime)
         values( svc_vat_reg_col(i).process_id ,
                 svc_vat_reg_col(i).chunk_id ,
                 svc_vat_reg_col(i).row_seq ,
                 svc_vat_reg_col(i).action ,
                 svc_vat_reg_col(i).process$status ,
                 sq.reverse_vat_threshold ,
                 sq.vat_region ,
                 sq.vat_region_name ,
                 sq.vat_region_type ,
                 sq.acquisition_vat_ind ,
                 sq.vat_calc_type,
                 svc_vat_reg_col(i).create_id ,
                 svc_vat_reg_col(i).create_datetime ,
                 svc_vat_reg_col(i).last_upd_id ,
                 svc_vat_reg_col(i).last_upd_datetime);
  EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          VAT_REG_sheet,
                          svc_VAT_REG_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
   END;
END process_s9t_VAT_REG;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_VAT_REG_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_VAT_REGION_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_VAT_REGION_TL_COL_TYP IS TABLE OF SVC_VAT_REGION_TL%ROWTYPE;
   L_temp_rec              SVC_VAT_REGION_TL%ROWTYPE;
   svc_vat_region_tL_col   SVC_VAT_REGION_TL_COL_TYP := NEW SVC_VAT_REGION_TL_COL_TYP();
   L_process_id            SVC_VAT_REGION_TL.PROCESS_ID%TYPE;
   L_error                 BOOLEAN := FALSE;
   L_default_rec           SVC_VAT_REGION_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select vat_region_name_mi,
             vat_region_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'VAT_REGION_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('VAT_REGION_NAME' as vat_region_name,
                                       'VAT_REGION'      as vat_region,
                                       'LANG'            as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_VAT_REGION_TL';
   L_pk_columns    VARCHAR2(255)  := 'VAT Region, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select vat_region_name_dv,
                      vat_region_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'VAT_REGION_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('VAT_REGION_NAME' as vat_region_name,
                                                'VAT_REGION'      as vat_region,
                                                'LANG'            as lang)))
   LOOP
      BEGIN
         L_default_rec.vat_region_name := rec.vat_region_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_REG_TL_SHEET ,
                            NULL,
                           'VAT_REGION_NAME' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.vat_region := rec.vat_region_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           VAT_REG_TL_SHEET ,
                            NULL,
                           'VAT_REGION' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_REG_TL_SHEET ,
                            NULL,
                           'LANG' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(vat_reg_tl$action)            as action,
                      r.get_cell(vat_reg_tl$vat_region_name)   as vat_region_name,
                      r.get_cell(vat_reg_tl$vat_region)        as vat_region,
                      r.get_cell(vat_reg_tl$lang)              as lang,
                      r.get_row_seq()                              as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(VAT_REG_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_REG_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.vat_region_name := rec.vat_region_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_REG_TL_SHEET,
                            rec.row_seq,
                            'VAT_REGION_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.vat_region := rec.vat_region;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_REG_TL_SHEET,
                            rec.row_seq,
                            'VAT_REGION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            VAT_REG_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_VAT.action_new then
         L_temp_rec.vat_region_name := NVL( L_temp_rec.vat_region_name,L_default_rec.vat_region_name);
         L_temp_rec.vat_region      := NVL( L_temp_rec.vat_region,L_default_rec.vat_region);
         L_temp_rec.lang            := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.vat_region is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         VAT_REG_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_vat_region_tL_col.extend();
         svc_vat_region_tL_col(svc_vat_region_tL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_vat_region_tL_col.COUNT SAVE EXCEPTIONS
      merge into svc_vat_region_tL st
      using(select
                  (case
                   when l_mi_rec.vat_region_name_mi = 'N'
                    and svc_vat_region_tL_col(i).action = CORESVC_VAT.action_mod
                    and s1.vat_region_name IS NULL then
                        mt.vat_region_name
                   else s1.vat_region_name
                   end) as vat_region_name,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_vat_region_tL_col(i).action = CORESVC_VAT.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.vat_region_mi = 'N'
                    and svc_vat_region_tL_col(i).action = CORESVC_VAT.action_mod
                    and s1.vat_region IS NULL then
                        mt.vat_region
                   else s1.vat_region
                   end) as vat_region
              from (select svc_vat_region_tL_col(i).vat_region_name as vat_region_name,
                           svc_vat_region_tL_col(i).vat_region        as vat_region,
                           svc_vat_region_tL_col(i).lang              as lang
                      from dual) s1,
                   VAT_REGION_TL mt
             where mt.vat_region (+) = s1.vat_region
               and mt.lang (+)       = s1.lang) sq
                on (st.vat_region = sq.vat_region and
                    st.lang = sq.lang and
                    svc_vat_region_tL_col(i).ACTION IN (CORESVC_VAT.action_mod,CORESVC_VAT.action_del))
      when matched then
      update
         set process_id        = svc_vat_region_tL_col(i).process_id ,
             chunk_id          = svc_vat_region_tL_col(i).chunk_id ,
             row_seq           = svc_vat_region_tL_col(i).row_seq ,
             action            = svc_vat_region_tL_col(i).action ,
             process$status    = svc_vat_region_tL_col(i).process$status ,
             vat_region_name = sq.vat_region_name
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             vat_region_name ,
             vat_region ,
             lang)
      values(svc_vat_region_tL_col(i).process_id ,
             svc_vat_region_tL_col(i).chunk_id ,
             svc_vat_region_tL_col(i).row_seq ,
             svc_vat_region_tL_col(i).action ,
             svc_vat_region_tL_col(i).process$status ,
             sq.vat_region_name ,
             sq.vat_region ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            VAT_REG_TL_SHEET,
                            svc_vat_region_tL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_VAT_REG_TL;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_VAT.PROCESS_S9T';
   L_file            s9t_file;
   L_sheets          s9t_pkg.names_map_typ;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;

   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR      EXCEPTION;
   PRAGMA        EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   commit;--to ensure that the record in s9t_folder is commited
   s9t_pkg.ods2obj(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE   then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = FALSE then
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'S9T_INVALID_TEMPLATE');
   else

      POPULATE_NAMES(I_file_id);

      sheet_name_trans := s9t_pkg.sheet_trans(L_file.template_key,
                                              L_file.user_lang);

      PROCESS_S9T_VAT_CODES(I_file_id,
                            I_process_id);

      PROCESS_S9T_VAT_CODE_RATES(I_file_id,
                                 I_process_id);

      PROCESS_S9T_VAT_CODES_TL(I_file_id,
                               I_process_id);
                               
      PROCESS_S9T_VAT_REG(I_file_id,
                          I_process_id);

      PROCESS_S9T_VAT_REG_TL(I_file_id,
                             I_process_id);                               
   end if;

   if CORESVC_CFLEX.PROCESS_S9T_SHEETS_ADMINAPI(O_error_message,
                                                I_file_id,
                                                I_process_id,
                                                template_key) = FALSE   then
      return FALSE;
   end if;

   O_error_count     := LP_s9t_errors_tab.COUNT();

   forall i IN 1..O_error_count
   insert into s9t_errors
        values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   -- Update SVC_PROCESS_TRACKER
   if O_error_count     = 0  then
      L_process_status :='PS';
   else
      L_process_status :='PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   commit;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);
         update svc_process_tracker
            set status  = 'PE',
                file_id = I_file_id
          where process_id = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
-------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODES_INS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_vat_codes_temp_rec   IN       VAT_CODES%ROWTYPE )
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)   :='CORESVC_VAT.EXEC_VAT_CODES_INS';
BEGIN
   insert into VAT_CODES
        values I_vat_codes_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_VAT_CODES_INS;
------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODES_UPD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_vat_codes_temp_rec   IN       VAT_CODES%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_VAT.EXEC_VAT_CODES_UPD';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='VAT_CODES';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_VAT_CODES_LOCK is
      select 'X'
        from VAT_CODES
       where VAT_CODE = I_vat_codes_temp_rec.vat_code
       for update nowait;
BEGIN
   open C_VAT_CODES_LOCK;
   close C_VAT_CODES_LOCK;

   update VAT_CODES
      set row      = I_vat_codes_temp_rec
    where VAT_CODE = I_vat_codes_temp_rec.vat_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_vat_codes_temp_rec.vat_code,
                                             NULL);
      return FALSE;

   when OTHERS then
      if C_VAT_CODES_LOCK%ISOPEN then
         close C_VAT_CODES_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      return FALSE;
END EXEC_VAT_CODES_UPD;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CFA_STG_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id           IN       NUMBER,
                              I_chunk_id             IN       NUMBER,
                              I_vat_codes_temp_rec   IN       VAT_CODES%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_VAT.EXEC_VAT_CFA_STG_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CFA_EXT';
   Record_Locked  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_RECS_DELETE is
     select sc.process_id ,
            sc.chunk_id ,
            sc.row_seq ,
            sc.action ,
            sc.base_rms_table ,
            sc.group_set_view_name ,
            sc.keys_col ,
            sc.attrs_col ,
            gs.group_set_id,
            gs.default_func,
            sf.template_key,
            sc.rowid as rid
       from svc_cfa_ext          sc,
            cfa_attrib_group_set gs,
            svc_process_tracker  sp,
            s9t_folder           sf
      where sc.process_id          = I_process_id
        and chunk_id               = I_chunk_id
        and sp.process_id          = I_process_id
        and sp.file_id             = sf.file_id (+)
        and sc.group_set_view_name = gs.group_set_view_name;

BEGIN
   open C_RECS_DELETE;
   close C_RECS_DELETE;

   FOR rec IN C_RECS_DELETE
   LOOP
      if key_val_pairs_pkg.get_attr(rec.KEYS_COL,'VAT_CODE') = I_vat_codes_temp_rec.vat_code   then
         delete from svc_cfa_ext
            where row_seq = rec.row_seq;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED   then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_vat_codes_temp_rec.vat_code,
                                             NULL);
      return FALSE;

   when OTHERS   then
      if C_RECS_DELETE%ISOPEN then
         close C_RECS_DELETE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_VAT_CFA_STG_DEL;
-----------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODES_DEL_PRE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id           IN       NUMBER,
                                I_chunk_id             IN       NUMBER,
                                I_vat_codes_temp_rec   IN       VAT_CODES%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_VAT.EXEC_VAT_CODE_DEL_PRE';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='VAT_CODE_RATES';
   L_dummy        vat_codes.vat_code%TYPE;
   Record_Locked  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_LOCK_RATE is
      select vat_code
        from VAT_CODE_RATES
       where vat_code = I_vat_codes_temp_rec.vat_code
   for update nowait;

BEGIN
   open C_LOCK_RATE;
   close C_LOCK_RATE;

   delete from VAT_CODE_RATES
      where vat_code = I_vat_codes_temp_rec.vat_code;

   if EXEC_VAT_CFA_STG_DEL(O_error_message,
                           I_process_id,
                           I_chunk_id,
                           I_vat_codes_temp_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED   then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_vat_codes_temp_rec.vat_code,
                                             NULL);
      return FALSE;

   when OTHERS   then
      if C_LOCK_RATE%ISOPEN then
         close C_LOCK_RATE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      return FALSE;
END EXEC_VAT_CODES_DEL_PRE;
------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODES_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id           IN       NUMBER,
                            I_chunk_id             IN       NUMBER,
                            I_vat_codes_temp_rec   IN       VAT_CODES%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_VAT.EXEC_VAT_CODES_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_CODES';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_VAT_CODES_TL_LOCK is
      select 'X'
        from vat_codes_tl
       where vat_code = I_vat_codes_temp_rec.vat_code
       for update nowait;

   cursor C_VAT_CODES_LOCK is
      select 'X'
        from VAT_CODES
       where VAT_CODE = I_vat_codes_temp_rec.vat_code
       for update nowait;
BEGIN
   open C_VAT_CODES_TL_LOCK;
   close C_VAT_CODES_TL_LOCK;

   open C_VAT_CODES_LOCK;
   close C_VAT_CODES_LOCK;

   if EXEC_VAT_CODES_DEL_PRE(O_error_message,
                             I_process_id,
                             I_chunk_id,
                             I_vat_codes_temp_rec) = FALSE   then
      return FALSE;
   else
      delete
        from vat_codes_tl
       where vat_code = I_vat_codes_temp_rec.vat_code;

      delete
        from VAT_CODES
       where VAT_CODE = I_vat_codes_temp_rec.vat_code;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_vat_codes_temp_rec.vat_code,
                                             NULL);
      return FALSE;

   when OTHERS then
      if C_VAT_CODES_TL_LOCK%ISOPEN then
         close C_VAT_CODES_TL_LOCK;
      end if;
      if C_VAT_CODES_LOCK%ISOPEN then
         close C_VAT_CODES_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      return FALSE;
END EXEC_VAT_CODES_DEL;
-----------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODE_RATES_INS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_vat_code_rates_temp_rec IN     VAT_CODE_RATES%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_VAT.EXEC_VAT_CODE_RATES_INS';

BEGIN
   insert into VAT_CODE_RATES
        values I_vat_code_rates_temp_rec;

   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_VAT_CODE_RATES_INS;
------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODE_RATES_UPD(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_vat_code_rates_temp_rec   IN       VAT_CODE_RATES%ROWTYPE,
                                 I_active_date               IN       DATE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_VAT.EXEC_VAT_CODE_RATES_UPD';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='VAT_CODE_RATES';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_VAT_CODE_RATES_LOCK is
      select 'X'
        from VAT_CODE_RATES
       where ACTIVE_DATE = I_active_date
         and VAT_CODE    = I_vat_code_rates_temp_rec.vat_code
       for update nowait;
BEGIN
   open C_VAT_CODE_RATES_LOCK;
   close C_VAT_CODE_RATES_LOCK;

   update VAT_CODE_RATES
      set row         = I_vat_code_rates_temp_rec
    where ACTIVE_DATE = I_active_date
      and VAT_CODE    = I_vat_code_rates_temp_rec.vat_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_vat_code_rates_temp_rec.vat_code,
                                             NULL);
      return FALSE;

   when OTHERS then
      if C_VAT_CODE_RATES_LOCK%ISOPEN then
         close C_VAT_CODE_RATES_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_VAT_CODE_RATES_UPD;
------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODE_RATES_DEL_PRE(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_vat_code_rates_temp_rec   IN       VAT_CODE_RATES%ROWTYPE,
                                     I_rec                       IN       C_SVC_VAT_CODES%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                  :='CORESVC_VAT.EXEC_VAT_CODE_RATES_DEL_PRE';
   L_orig_date    DATE;
   L_orig_rate    VAT_CODE_RATES.VAT_RATE%TYPE;
   L_today        DATE                          := GET_VDATE;
   L_tax_code_rec OBJ_TAX_INFO_REC              := OBJ_TAX_INFO_REC();
   L_tax_code_tbl OBJ_TAX_INFO_TBL              := OBJ_TAX_INFO_TBL();

BEGIN
   if I_vat_code_rates_temp_rec.vat_rate       is NOT NULL
      or I_vat_code_rates_temp_rec.active_date is NOT NULL
      or I_vat_code_rates_temp_rec.create_id   is NOT NULL   then

      -- Getting database values for original rate and original active_date.
      L_orig_rate := I_rec.base_vcr_vat_rate;
      L_orig_date := I_rec.base_vcr_active_date;

      L_tax_code_tbl.EXTEND();
      L_tax_code_rec.tax_code              := I_VAT_CODE_RATES_temp_rec.vat_code;
      L_tax_code_rec.tax_rate              := L_orig_rate;
      L_tax_code_rec.active_date           := L_orig_date;
      L_tax_code_tbl(L_tax_code_tbl.COUNT) := L_tax_code_rec;

      if TAX_SQL.DELETE_ITEM_DEPS_TAXRATE(O_error_message,
                                          L_tax_code_tbl) = FALSE   then
         return FALSE;
      end if;

      if L_orig_date <= L_today   then
         O_error_message := 'CANNOT_DEL_VAT_RATE_ACT';
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_VAT_CODE_RATES_DEL_PRE;
------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODE_RATES_DEL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_vat_code_rates_temp_rec IN     VAT_CODE_RATES%ROWTYPE,
                                 I_rec                     IN     C_SVC_VAT_CODES%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_VAT.EXEC_VAT_CODE_RATES_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_CODE_RATES';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_VAT_CODE_RATES_LOCK is
      select 'X'
        from VAT_CODE_RATES
       where ACTIVE_DATE = I_vat_code_rates_temp_rec.active_date
         and VAT_CODE    = I_vat_code_rates_temp_rec.vat_code
       for update nowait;
BEGIN

   if EXEC_VAT_CODE_RATES_DEL_PRE(O_error_message,
                                  I_vat_code_rates_temp_rec,
                                  I_rec) = FALSE   then
      return FALSE;
   end if;

   open C_VAT_CODE_RATES_LOCK;
   close C_VAT_CODE_RATES_LOCK;

   delete from VAT_CODE_RATES
    where ACTIVE_DATE = I_vat_code_rates_temp_rec.active_date
      and VAT_CODE    = I_vat_code_rates_temp_rec.vat_code;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_vat_code_rates_temp_rec.vat_code,
                                             I_vat_code_rates_temp_rec.active_date);
      return FALSE;

   when OTHERS then
      if C_VAT_CODE_RATES_LOCK%ISOPEN then
         close C_VAT_CODE_RATES_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_VAT_CODE_RATES_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODES_TL_INS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_vat_codes_tl_ins_tab    IN       VAT_CODES_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_VAT.EXEC_VAT_CODES_TL_INS';

BEGIN
   if I_vat_codes_tl_ins_tab is NOT NULL and I_vat_codes_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_vat_codes_tl_ins_tab.COUNT()
         insert into vat_codes_tl
              values I_vat_codes_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_VAT_CODES_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODES_TL_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_vat_codes_tl_upd_tab   IN       VAT_CODES_TAB,
                               I_vat_codes_tl_upd_rst   IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_VAT_CODES_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_VAT_CODES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_VAT.EXEC_VAT_CODES_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_CODES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_VAT_CODES_TL_UPD(I_vat_codes  VAT_CODES_TL.vat_code%TYPE,
                                  I_lang       VAT_CODES_TL.LANG%TYPE) is
      select 'x'
        from vat_codes_tl
       where vat_code = I_vat_codes
         and lang = I_lang
         for update nowait;

BEGIN
   if I_vat_codes_tl_upd_tab is NOT NULL and I_vat_codes_tl_upd_tab.count > 0 then
      for i in I_vat_codes_tl_upd_tab.FIRST..I_vat_codes_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_vat_codes_tl_upd_tab(i).lang);
            L_key_val2 := 'VAT Code: '||to_char(I_vat_codes_tl_upd_tab(i).vat_code);
            open C_LOCK_VAT_CODES_TL_UPD(I_vat_codes_tl_upd_tab(i).vat_code,
                                         I_vat_codes_tl_upd_tab(i).lang);
            close C_LOCK_VAT_CODES_TL_UPD;
            
            update vat_codes_tl
               set vat_code_desc = I_vat_codes_tl_upd_tab(i).vat_code_desc,
                   last_update_id = I_vat_codes_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_vat_codes_tl_upd_tab(i).last_update_datetime
             where lang = I_vat_codes_tl_upd_tab(i).lang
               and vat_code = I_vat_codes_tl_upd_tab(i).vat_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_VAT_CODES_TL',
                           I_vat_codes_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_VAT_CODES_TL_UPD%ISOPEN then
         close C_LOCK_VAT_CODES_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_VAT_CODES_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_CODES_TL_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_vat_codes_tl_del_tab   IN       VAT_CODES_TAB,
                               I_vat_codes_tl_DEL_rst   IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_VAT_CODES_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_VAT_CODES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_VAT.EXEC_VAT_CODES_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_CODES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_VAT_CODES_TL_DEL(I_vat_code  VAT_CODES_TL.VAT_CODE%TYPE,
                                  I_lang      VAT_CODES_TL.LANG%TYPE) is
      select 'x'
        from vat_codes_tl
       where vat_code = I_vat_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_vat_codes_tl_del_tab is NOT NULL and I_vat_codes_tl_del_tab.count > 0 then
      for i in I_vat_codes_tl_del_tab.FIRST..I_vat_codes_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_vat_codes_tl_del_tab(i).lang);
            L_key_val2 := 'VAT Code: '||to_char(I_vat_codes_tl_del_tab(i).vat_code);
            open C_LOCK_VAT_CODES_TL_DEL(I_vat_codes_tl_del_tab(i).vat_code,
                                         I_vat_codes_tl_del_tab(i).lang);
            close C_LOCK_VAT_CODES_TL_DEL;
            
            delete vat_codes_tl
             where lang = I_vat_codes_tl_del_tab(i).lang
               and vat_code = I_vat_codes_tl_del_tab(i).vat_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_VAT_CODES_TL',
                           I_vat_codes_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_VAT_CODES_TL_DEL%ISOPEN then
         close C_LOCK_VAT_CODES_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_VAT_CODES_TL_DEL;
-------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_VAT_CODES(O_error           IN OUT    BOOLEAN,
                               O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                               I_rec             IN OUT    C_SVC_VAT_CODES%ROWTYPE )
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_VAT.PROCESS_VAL_VAT_CODES';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_VAT_CODES';
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   if LP_system_options_row.default_tax_type in ('SVAT','SALES')
      and I_rec.stvc_incl_nic_ind is NOT NULL
      and I_rec.stvc_incl_nic_ind = 'Y'   then
      WRITE_ERROR( I_rec.stvc_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.stvc_chunk_id,
                   L_table,
                   I_rec.stvc_row_seq,
                  'INCL_NIC_IND',
                  'IND_NOT_Y');
      O_error := TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_VAT_CODES;
------------------------------------------------------------------------------------------------------------------------------------
FUNCTION ACTIVE_DATE(O_error           IN OUT   BOOLEAN,
                     O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rec             IN       C_SVC_VAT_CODES%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      :='CORESVC_VAT.ACTIVE_DATE';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_VAT_CODE_RATES';
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE          := NULL;
   L_vdate         DATE                              := GET_VDATE;
   L_tax_code_rec  OBJ_TAX_INFO_REC                  := OBJ_TAX_INFO_REC();
   L_tax_code_tbl  OBJ_TAX_INFO_TBL                  := OBJ_TAX_INFO_TBL();
BEGIN
   if I_rec.stvcr_action = action_new
      and I_rec.stvcr_active_date is NOT NULL
      and I_rec.stvcr_active_date < L_vdate   then
      WRITE_ERROR( I_rec.stvcr_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.stvcr_chunk_id,
                   L_table,
                   I_rec.stvcr_row_seq,
                  'ACTIVE_DATE',
                  'ACT_DATE_NOT_BEF_TODAY');
      O_error := TRUE;
   elsif I_rec.stvcr_action = action_mod
         and I_rec.stvcr_active_date > L_vdate
         and I_rec.stvcr_active_date_s9t <> I_rec.stvcr_active_date
         and I_rec.stvcr_active_date_s9t is NOT NULL   then
         if  I_rec.stvcr_active_date_s9t < L_vdate   then
             WRITE_ERROR( I_rec.stvcr_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_rec.stvcr_chunk_id,
                          L_table,
                          I_rec.stvcr_row_seq,
                         'ACTIVE_DATE_S9T',
                         'ACT_DATE_NOT_BEF_TODAY');
             O_error := TRUE;
         end if;
   end if;

   if I_rec.stvcr_action = action_mod
      and I_rec.stvcr_active_date_s9t is NOT NULL
      and I_rec.stvcr_active_date_s9t <> I_rec.stvcr_active_date   then

      L_tax_code_tbl.EXTEND();
      L_tax_code_rec.tax_code              := I_rec.stvcr_vat_code;
      L_tax_code_rec.active_date           := I_rec.stvcr_active_date_s9t;
      L_tax_code_tbl(L_tax_code_tbl.COUNT) := L_tax_code_rec;

      if TAX_SQL.GET_TAX_CODE(L_error_message,
                              L_tax_code_tbl) = FALSE   then
         WRITE_ERROR( I_rec.stvcr_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.stvcr_chunk_id,
                      L_table,
                      I_rec.stvcr_row_seq,
                      NULL,
                      L_error_message);
         O_error := TRUE;
      end if;

      if L_tax_code_tbl.COUNT > 0
         and L_tax_code_tbl(L_tax_code_tbl.COUNT).active_date is NOT NULL   then
         WRITE_ERROR( I_rec.stvcr_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.stvcr_chunk_id,
                      L_table,
                      I_rec.stvcr_row_seq,
                     'VAT_RATE',
                     'VAT_CODE_DATE_EXIST');
         O_error := TRUE;
      end if;
   end if;

   --Active VAT rates are not editable
   if I_rec.stvcr_action = action_mod
      and I_rec.stvcr_active_date is NOT NULL
      and I_rec.stvcr_vat_rate is NOT NULL   then
      if I_rec.stvcr_active_date <= L_vdate   then                  -- this is an active_vat_rate record so cannot be modified
         if I_rec.stvcr_active_date <> I_rec.stvcr_active_date_s9t  -- checking for modifications.
            or I_rec.stvcr_vat_rate <> I_rec.base_vcr_vat_rate   then
            WRITE_ERROR( I_rec.stvcr_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.stvcr_chunk_id,
                         L_table,
                         I_rec.stvcr_row_seq,
                        'Active Date, Rate(%)',
                        'CANT_UPD_ACT_VAT_RATES');
            O_error := TRUE;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END ACTIVE_DATE;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_VAT_CODE_RATES(O_error           IN OUT   BOOLEAN,
                                    O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rec             IN       C_SVC_VAT_CODES%ROWTYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                      :='CORESVC_VAT.PROCESS_VAL_VAT_CODE_RATES';
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_VAT_CODE_RATES';
   L_vdate    DATE                              := GET_VDATE;
BEGIN

   if I_rec.stvcr_vat_rate is NOT NULL
      and I_rec.stvcr_vat_rate < 0   then
      WRITE_ERROR( I_rec.stvcr_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.stvcr_chunk_id,
                   L_table,
                   I_rec.stvcr_row_seq,
                  'VAT_RATE',
                  'POSITIVE_VALUE');
      O_error := TRUE;
   end if;

   if I_rec.stvcr_action = action_mod
      and I_rec.stvcr_active_date > L_vdate then
         if I_rec.stvcr_active_date_s9t is NULL then
            WRITE_ERROR( I_rec.stvcr_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.stvcr_chunk_id,
                         L_table,
                         I_rec.stvcr_row_seq,
                        'ACTIVE_DATE_S9T',
                        'MUST_ENTER_FIELD');
            O_error := TRUE;
         end if;
   end if;

   if I_rec.stvcr_active_date  is NOT NULL
      and I_rec.stvcr_vat_rate is NOT NULL   then
      if ACTIVE_DATE(O_error,
                     O_error_message,
                     I_rec) = FALSE   then
         O_error := TRUE;
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_VAT_CODE_RATES;
------------------------------------------------------------------------------------------------------

FUNCTION PROCESS_VAT_CODES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_VAT_CODES.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_VAT_CODES.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64)                      :='CORESVC_VAT.PROCESS_VAT_CODES';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_VAT_CODES';
   L_rates_table             VARCHAR2(255)                     :='SVC_VAT_CODE_RATES';
   L_tax_code_rec            OBJ_TAX_INFO_REC                  := OBJ_TAX_INFO_REC();
   L_tax_code_tbl            OBJ_TAX_INFO_TBL                  := OBJ_TAX_INFO_TBL();
   L_vcr_process_error       BOOLEAN                           := FALSE;
   L_vc_process_error        BOOLEAN                           := FALSE;
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE          := NULL;
   L_vc_error                BOOLEAN                           := FALSE;
   L_vcr_error               BOOLEAN                           := FALSE;
   L_count_rate              NUMBER;
   L_vat_codes_temp_rec      VAT_CODES%ROWTYPE;
   L_vat_code_rates_temp_rec VAT_CODE_RATES%ROWTYPE;
   L_rowseq_count            NUMBER;
   c                         NUMBER      := 1;


   TYPE L_row_seq_tab_type IS TABLE OF SVC_VAT_CODES.ROW_SEQ%TYPE;
   L_row_seq_tab                       L_row_seq_tab_type;

   --Cursor for checking if atleast one vat rate exist for the vat_code.
   cursor C_RATES_ATLEAST(L_rates_vat  vat_codes.vat_code%type) is
      select count(vat_code_rates.active_date) into L_count_rate
        from vat_code_rates
       where vat_code = L_rates_vat;

BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   L_error_message := NULL;
---Retieving System Options.
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            LP_system_options_row) = FALSE   then
      return FALSE;
   end if;

   FOR rec IN C_SVC_VAT_CODES(I_process_id,I_chunk_id)
   LOOP

      if rec.vc_rank = 1 then
         L_vc_error               := FALSE;
         L_vc_process_error       := FALSE;
         L_row_seq_tab.DELETE;
         c:=1;
      end if;

         --For the first record with vat_code rank 1 and the record is non empty.
      if rec.vc_rank = 1
         and rec.stvc_rid is NOT NULL   then

         if (rec.stvc_action is NULL and rec.stvc_rid is NOT NULL)
            or rec.stvc_action NOT IN (action_new,action_mod,action_del)  then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.stvc_row_seq,
                         'ACTION',
                         'INV_ACT');
            L_vc_error :=TRUE;
         end if;

         if rec.stvc_action = action_new
            and rec.vcr_vcd_fk_rid is NOT NULL  then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.stvc_row_seq,
                        'VAT_CODE',
                        'VAT_CODE_EXIST');
            L_vc_error :=TRUE;
         end if;

         if rec.stvc_action IN (action_mod,action_del)
            and rec.vcr_vcd_fk_rid is NULL
            and rec.stvc_vat_code  is NOT NULL  then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.stvc_row_seq,
                        'VAT_CODE',
                        'NO_RECORD');
            L_vc_error :=TRUE;
         end if;

         if NOT( rec.stvc_vat_code is NOT NULL )  then
                 WRITE_ERROR( I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.stvc_row_seq,
                             'VAT_CODE',
                             'ENT_VAT_CODE');
                 L_vc_error :=TRUE;
         end if;

         if rec.stvc_action IN (action_new,action_mod) then
            if rec.stvc_incl_nic_ind is NULL
               or rec.stvc_incl_nic_ind NOT IN ( 'Y','N' )   then
                WRITE_ERROR( I_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_chunk_id,
                             L_table,
                             rec.stvc_row_seq,
                            'INCL_NIC_IND',
                            'INV_Y_N_IND');
                L_vc_error :=TRUE;
            end if;

            if rec.stvc_vat_code_desc is NULL   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.stvc_row_seq,
                           'VAT_CODE_DESC',
                           'CODE_DESC_REQ');
               L_vc_error :=TRUE;
            end if;

            if PROCESS_VAL_VAT_CODES(L_vc_error,
                                     L_error_message,
                                     rec) = FALSE   then
               L_vc_error := TRUE;
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.stvc_row_seq,
                            NULL,
                            L_error_message);
            end if;
         end if;

         if rec.stvc_action = action_del
            and rec.vcr_vcd_fk_rid is NOT NULL
            and rec.stvc_vat_code  is NOT NULL   then
            L_tax_code_tbl.EXTEND();
            L_tax_code_rec.tax_code              := rec.stvc_vat_code;
            L_tax_code_tbl(L_tax_code_tbl.COUNT) := L_tax_code_rec;

            if TAX_SQL.DELETE_ITEM_DEPS_TAXCODE(L_error_message,
                                                L_tax_code_tbl) = FALSE then
               WRITE_ERROR(rec.stvc_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           rec.stvc_chunk_id,
                           L_table,
                           rec.stvc_row_seq,
                           NULL,
                           L_error_message);
               L_vc_error := TRUE;
            elsif TAX_SQL.DEL_VAT_CODES_EXT(L_error_message,
                                            rec.stvc_vat_code) = FALSE then
                  WRITE_ERROR(rec.stvc_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              rec.stvc_chunk_id,
                              L_table,
                              rec.stvc_row_seq,
                              NULL,
                              L_error_message);
                  L_vc_error := TRUE;
            end if;
         end if;
      end if;
      if rec.vc_rank = 1 then
         SAVEPOINT RATE_SCREEN;
      end if;
---------VAT_CODES PROCESSING---------------------------------------------------------------------------------------------------------
      if NOT L_vc_error and rec.vc_rank = 1 then

         L_vat_codes_temp_rec.vat_code      := rec.stvc_vat_code;
         L_vat_codes_temp_rec.vat_code_desc := rec.stvc_vat_code_desc;
         L_vat_codes_temp_rec.incl_nic_ind  := rec.stvc_incl_nic_ind;

         if rec.stvc_action = action_new   then

            if EXEC_VAT_CODES_INS(L_error_message,
                                  L_vat_codes_temp_rec) = FALSE   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.stvc_row_seq,
                            NULL,
                            L_error_message);
               L_vc_process_error := TRUE;
            end if;
         end if;

         if rec.stvc_action = action_mod  then

            if EXEC_VAT_CODES_UPD(L_error_message,
                                  L_vat_codes_temp_rec) = FALSE   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.stvc_row_seq,
                            NULL,
                            L_error_message);
               L_vc_process_error := TRUE;
            end if;
         end if;

         if rec.stvc_action = action_del  then

            if EXEC_VAT_CODES_DEL(L_error_message,
                                  rec.stvc_process_id,    --new
                                  rec.stvc_chunk_id,      --new
                                  L_vat_codes_temp_rec ) = FALSE   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.stvc_row_seq,
                            NULL,
                            L_error_message);
               L_vc_process_error := TRUE;
            end if;
         end if;
      end if;

      -- Log error against corresponding VAT Rate record if VAT Code record has errors
      if L_vc_error or L_vc_process_error then

         if rec.stvcr_rid is NOT NULL and rec.stvcr_row_seq is not NULL then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_rates_table,
                         rec.stvcr_row_seq,
                         'VAT_CODE',
                         'ERROR_IN_HEAD_REC');
            L_vc_process_error := TRUE;
         end if;
      end if;



      --VALIDATIONS and PROCESSING
      --For VAT_CODE_RATES only if tax_type is SVAT
      --Sheet 2 will be [validated] irrespective of the validation / processing errors in sheet1 (when action is New and Mod)
      if LP_system_options_row.default_tax_type = 'SVAT'   then

         if rec.stvcr_rid is NOT NULL
            and NVL(rec.stvc_action,'-1') <> action_del then -- if action is del for vat_codes no need to validate and process the VAT_RATES.

            L_vcr_error         := FALSE;
            L_vcr_process_error := FALSE;

            if (rec.stvcr_action is NULL and rec.stvcr_rid is NOT NULL)
               or rec.stvcr_action NOT IN (action_new,action_mod,action_del)  then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_rates_table,
                            rec.stvcr_row_seq,
                           'ACTION',
                           'INV_ACT');
               L_vcr_error :=TRUE;
            end if;

            if rec.stvcr_action = action_new
               and rec.PK_VAT_CODE_RATES_rid is NOT NULL  then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_rates_table,
                            rec.stvcr_row_seq,
                           'VAT Code,Active Date',
                           'DUP_RECORD');
               L_vcr_error :=TRUE;
            end if;

            if rec.stvcr_action IN (action_del,action_mod)
               and rec.PK_VAT_CODE_RATES_rid is NULL
               and rec.stvcr_vat_code    is NOT NULL
               and rec.stvcr_active_date is NOT NULL   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_rates_table,
                            rec.stvcr_row_seq,
                           'VAT Code,Active Date',
                           'NO_RECORD');
               L_vcr_error := TRUE;
            end if;

            if rec.stvcr_action = action_new
               and rec.VCR_VCD_FK_rid is NULL
               and rec.stvcr_vat_code is NOT NULL   then
               if  rec.stvc_rid is NULL  then
                   WRITE_ERROR( I_process_id,
                                SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                I_chunk_id,
                                L_rates_table,
                                rec.stvcr_row_seq,
                               'VAT_CODE',
                               'INV_VAT_CODE');
                   L_vcr_error :=TRUE;
               end if;
            end if;

            if NOT( rec.stvcr_vat_code is NOT NULL )   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_rates_table,
                            rec.stvcr_row_seq,
                           'VAT_CODE',
                           'ENT_VAT_CODE');
               L_vcr_error :=TRUE;
            end if;

            if NOT( rec.stvcr_active_date  is NOT NULL ) then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_rates_table,
                            rec.stvcr_row_seq,
                           'ACTIVE_DATE',
                           'ENT_ACT_DATE');
               L_vcr_error :=TRUE;
            end if;

            if rec.stvcr_action in (action_new,action_mod)  then
               if NOT( rec.stvcr_vat_rate  is NOT NULL ) then
                  WRITE_ERROR( I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_rates_table,
                               rec.stvcr_row_seq,
                              'VAT_RATE',
                              'ENT_VAT_RATE');
                  L_vcr_error :=TRUE;
               end if;

               if PROCESS_VAL_VAT_CODE_RATES(L_vcr_error,
                                             L_error_message,
                                             rec) = FALSE   then
                  L_vcr_error := TRUE;
                  WRITE_ERROR( I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_rates_table,
                               rec.stvcr_row_seq,
                               NULL,
                               L_error_message);
               end if;
            end if;

            --VAT_CODE_RATES PROCESSING
            if NOT L_vcr_error and NOT L_vc_process_error and NOT L_vc_error then

               L_vat_code_rates_temp_rec.VAT_CODE     := rec.stvcr_vat_code;
               L_vat_code_rates_temp_rec.ACTIVE_DATE  := rec.stvcr_active_date;
               L_vat_code_rates_temp_rec.VAT_RATE     := rec.stvcr_vat_rate;
               L_vat_code_rates_temp_rec.CREATE_ID    := GET_USER;
               L_vat_code_rates_temp_rec.CREATE_DATE  := SYSDATE;

               if rec.stvcr_action = action_new   then

                  if EXEC_VAT_CODE_RATES_INS(L_error_message,
                                             L_vat_code_rates_temp_rec) = FALSE   then
                     WRITE_ERROR( I_process_id,
                                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                  I_chunk_id,
                                  L_rates_table,
                                  rec.stvcr_row_seq,
                                  NULL,
                                  L_error_message);
                     L_vcr_process_error := TRUE;
                  end if;
               end if;

               if rec.stvcr_action = action_mod   then
                  L_vat_code_rates_temp_rec.active_date := rec.stvcr_active_date_s9t;

                  if EXEC_VAT_CODE_RATES_UPD(L_error_message,
                                             L_vat_code_rates_temp_rec,
                                             rec.base_vcr_active_date) = FALSE   then
                     WRITE_ERROR( I_process_id,
                                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                  I_chunk_id,
                                  L_rates_table,
                                  rec.stvcr_row_seq,
                                  NULL,
                                  L_error_message);
                     L_vcr_process_error := TRUE;
                  end if;
               end if;

               if rec.stvcr_action = action_del   then

                  if EXEC_VAT_CODE_RATES_DEL(L_error_message,
                                             L_vat_code_rates_temp_rec,
                                             rec) = FALSE then
                     if L_error_message = 'CANNOT_DEL_VAT_RATE_ACT' then
                        WRITE_ERROR( I_process_id,
                                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                     I_chunk_id,
                                     L_rates_table,
                                     rec.stvcr_row_seq,
                                     'Active Date, Rate(%)',
                                     L_error_message);
                     else
                        if INSTR(L_error_message,'CANNOT_DEL_VAT_RATE')!= 0 and INSTR(L_error_message,'CANNOT_DEL_VAT_RATE_ACT') = 0 then
                           WRITE_ERROR( I_process_id,
                                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                        I_chunk_id,
                                        L_rates_table,
                                        rec.stvcr_row_seq,
                                        'VAT_RATE',
                                        L_error_message);
                        else
                           WRITE_ERROR( I_process_id,
                                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                        I_chunk_id,
                                        L_rates_table,
                                        rec.stvcr_row_seq,
                                        NULL,
                                        L_error_message);
                        end if;
                     end if;
                  L_vcr_process_error := TRUE;
                  end if;
               end if;

               if NOT L_vcr_process_error then
                  L_row_seq_tab.extend();
                  L_row_seq_tab(c)  := rec.stvcr_row_seq;
                  c:=c+1;
               end if;
            end if;
         else
            if rec.stvcr_action not in (action_del)
               and rec.stvcr_rid is NOT NULL   then

               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_rates_table,
                            rec.stvcr_row_seq,
                            NULL,
                            'VAT_CODE_REC_DEL');
               L_vcr_process_error := TRUE;
            end if;
         end if; --rec.stvcr_rid is NOT NULL and NVL(rec.stvc_action,'-1') <> action_del


         -- Rollback condition check.
         -- Atleast one rate must be present for a vat_code after insertion in VAT_CODES and VAT_CODE_RATES or after deletion from VAT_CODE_RATES.

         if rec.stvc_vat_code <> TO_CHAR(rec.next_vat_code)
            and L_vc_process_error = FALSE
            and L_vc_error = FALSE   then

            open  C_RATES_ATLEAST (rec.stvc_vat_code);
            fetch C_RATES_ATLEAST into L_count_rate;
            close C_RATES_ATLEAST;

            if L_count_rate < 1   then
               --Insertion Case
               if rec.stvc_action = action_new   then
                  ROLLBACK TO RATE_SCREEN;

                  WRITE_ERROR( I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               'VAT_CODES',
                               rec.stvc_row_seq,
                               'VAT_CODE',
                               'ATLEAST_ONE_RATE_REQUIRED');

               end if;
               --Deletion Case: The stvc_action is MOD or Sheet1 is EMPTY.
               if rec.stvc_action = action_mod
                  or rec.stvc_rid is NULL   then
                  ROLLBACK TO RATE_SCREEN;
                  L_rowseq_count := L_row_seq_tab.count();

                  if L_rowseq_count > 0 then
                     for i in 1..L_rowseq_count
                     LOOP
                        WRITE_ERROR( I_process_id,
                                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                     I_chunk_id,
                                    'VAT_CODE_RATES',
                                     L_row_seq_tab(i),
                                    'VAT Code,Active Date',
                                    'ATLEAST_ONE_RATE_REQUIRED');
                     END LOOP;
                  end if;
               end if;
               if rec.stvc_action = action_new and rec.stvcr_rid is NOT NULL then

                  WRITE_ERROR( I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               'VAT_CODES',
                               rec.stvc_row_seq,
                               'VAT_CODE',
                               'DETAIL_PROC_ER');
               end if;
            end if;
         end if; --rec.stvc_vat_code <> TO_CHAR(rec.next_vat_code)
      end if;--SVAT if_block.
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAT_CODES;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAT_CODES_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_VAT_CODES_TL.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_VAT_CODES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64) := 'CORESVC_VAT.PROCESS_VAT_CODES_TL';
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_CODES_TL';
   L_base_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_CODES';
   L_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_VAT_CODES_TL';
   L_error                    BOOLEAN := FALSE;
   L_process_error            BOOLEAN := FALSE;
   L_vat_codes_tl_temp_rec    VAT_CODES_TL%ROWTYPE;
   L_vat_codes_tl_upd_rst     ROW_SEQ_TAB;
   L_vat_codes_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_VAT_CODES_TL(I_process_id NUMBER,
                             I_chunk_id NUMBER) is
      select pk_vat_codes_tl.rowid  as pk_vat_codes_tl_rid,
             fk_vat_codes.rowid     as fk_vat_codes_rid,
             fk_lang.rowid          as fk_lang_rid,
             st.lang,
             UPPER(st.vat_code)     as vat_code,
             st.vat_code_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_vat_codes_tl  st,
             vat_codes         fk_vat_codes,
             vat_codes_tl      pk_vat_codes_tl,
             lang              fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and UPPER(st.vat_code)    =  UPPER(fk_vat_codes.vat_code (+))
         and st.lang        =  pk_vat_codes_tl.lang (+)
         and UPPER(st.vat_code)    =  UPPER(pk_vat_codes_tl.vat_code (+))
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_VAT_CODES_TL is TABLE OF C_SVC_VAT_CODES_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_vat_codes_tab        SVC_VAT_CODES_TL;

   L_vat_codes_TL_ins_tab         VAT_CODES_TAB         := NEW VAT_CODES_TAB();
   L_vat_codes_TL_upd_tab         VAT_CODES_TAB         := NEW VAT_CODES_TAB();
   L_vat_codes_TL_del_tab         VAT_CODES_TAB         := NEW VAT_CODES_TAB();

BEGIN
   if C_SVC_VAT_CODES_TL%ISOPEN then
      close C_SVC_VAT_CODES_TL;
   end if;

   open C_SVC_VAT_CODES_TL(I_process_id,
                           I_chunk_id);
   LOOP
      fetch C_SVC_VAT_CODES_TL bulk collect into L_svc_vat_codes_tab limit LP_bulk_fetch_limit;
      if L_svc_vat_codes_tab.COUNT > 0 then
         FOR i in L_svc_vat_codes_tab.FIRST..L_svc_vat_codes_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_vat_codes_tab(i).action is NULL
               or L_svc_vat_codes_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_codes_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_vat_codes_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_codes_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_vat_codes_tab(i).action = action_new
               and L_svc_vat_codes_tab(i).pk_vat_codes_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_codes_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_vat_codes_tab(i).action IN (action_mod, action_del)
               and L_svc_vat_codes_tab(i).lang is NOT NULL
               and L_svc_vat_codes_tab(i).vat_code is NOT NULL
               and L_svc_vat_codes_tab(i).pk_vat_codes_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_vat_codes_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_vat_codes_tab(i).action = action_new
               and L_svc_vat_codes_tab(i).vat_code is NOT NULL
               and L_svc_vat_codes_tab(i).fk_vat_codes_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_vat_codes_tab(i).row_seq,
                            'VAT_CODE',
                            'INV_VAT_CODE');
               L_error :=TRUE;
            end if;

            if L_svc_vat_codes_tab(i).action = action_new
               and L_svc_vat_codes_tab(i).lang is NOT NULL
               and L_svc_vat_codes_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_codes_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_vat_codes_tab(i).action in (action_new, action_mod) then
               if L_svc_vat_codes_tab(i).vat_code_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_vat_codes_tab(i).row_seq,
                              'VAT_CODE_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_vat_codes_tab(i).vat_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_codes_tab(i).row_seq,
                           'VAT_CODE',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_vat_codes_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_codes_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_vat_codes_tl_temp_rec.lang := L_svc_vat_codes_tab(i).lang;
               L_vat_codes_tl_temp_rec.vat_code := L_svc_vat_codes_tab(i).vat_code;
               L_vat_codes_tl_temp_rec.vat_code_desc := L_svc_vat_codes_tab(i).vat_code_desc;
               L_vat_codes_tl_temp_rec.create_datetime := SYSDATE;
               L_vat_codes_tl_temp_rec.create_id := GET_USER;
               L_vat_codes_tl_temp_rec.last_update_datetime := SYSDATE;
               L_vat_codes_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_vat_codes_tab(i).action = action_new then
                  L_vat_codes_TL_ins_tab.extend;
                  L_vat_codes_TL_ins_tab(L_vat_codes_TL_ins_tab.count()) := L_vat_codes_tl_temp_rec;
               end if;

               if L_svc_vat_codes_tab(i).action = action_mod then
                  L_vat_codes_TL_upd_tab.extend;
                  L_vat_codes_TL_upd_tab(L_vat_codes_TL_upd_tab.count()) := L_vat_codes_tl_temp_rec;
                  L_vat_codes_tl_upd_rst(L_vat_codes_TL_upd_tab.count()) := L_svc_vat_codes_tab(i).row_seq;
               end if;

               if L_svc_vat_codes_tab(i).action = action_del then
                  L_vat_codes_TL_del_tab.extend;
                  L_vat_codes_TL_del_tab(L_vat_codes_TL_del_tab.count()) := L_vat_codes_tl_temp_rec;
                  L_vat_codes_tl_del_rst(L_vat_codes_TL_del_tab.count()) := L_svc_vat_codes_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_VAT_CODES_TL%NOTFOUND;
   END LOOP;
   close C_SVC_VAT_CODES_TL;

   if EXEC_VAT_CODES_TL_INS(O_error_message,
                            L_vat_codes_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_VAT_CODES_TL_UPD(O_error_message,
                            L_vat_codes_TL_upd_tab,
                            L_vat_codes_tl_upd_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_VAT_CODES_TL_DEL(O_error_message,
                            L_vat_codes_TL_del_tab,
                            L_vat_codes_tl_del_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAT_CODES_TL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_REG_INS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_vat_reg_temp_rec   IN       VAT_REGION%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_VAT.EXEC_VAT_REG_INS';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_VAT_REGION';

BEGIN
   insert into vat_region
        values I_vat_reg_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_VAT_REG_INS;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_REG_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_vat_reg_temp_rec       IN       VAT_REGION%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_VAT.EXEC_VAT_REG_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_REGION';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_VAT_REGION_LOCK is
      select 'X'
        from VAT_REGION
       where vat_region = I_vat_reg_temp_rec.vat_region
       for update nowait;
BEGIN

   open C_VAT_REGION_LOCK;
   close C_VAT_REGION_LOCK;

   update VAT_REGION
      set row        = I_vat_reg_temp_rec
    where vat_region = I_vat_reg_temp_rec.vat_region;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_vat_reg_temp_rec.vat_region,
                                             NULL);
      return FALSE;

   when OTHERS then
      if C_VAT_REGION_LOCK%ISOPEN then
         close C_VAT_REGION_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      return FALSE;
END EXEC_VAT_REG_UPD;
----------------------------------------------------------------------------------------------------
FUNCTION CHECK_DEL_VAT_REG(O_warning_flag    IN OUT   BOOLEAN,
                           O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_rec             IN       C_SVC_VAT_REG%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      :='CORESVC_VAT.CHECK_DEL_VAT_REG';
   L_var           VARCHAR2(1)                       := NULL;
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_VAT_REGION';

   cursor C_STORE is
      select 'x'
        from store
       where store.vat_region = I_rec.vat_region;

   cursor C_DEPS is
      select 'x'
        from vat_deps
       where vat_deps.vat_region = I_rec.vat_region;

   cursor C_WAREHOUSE is
      select 'x'
        from wh
       where wh.vat_region = I_rec.vat_region;

   cursor C_SKU is
      select 'x'
        from vat_item
       where vat_region = I_rec.vat_region;

   cursor C_SYSTEM_OPTIONS is
      select 'x'
        from system_options
       where default_vat_region = I_rec.vat_region;

BEGIN
   O_warning_flag := FALSE;

   open C_STORE;
   fetch C_STORE into L_var;
   if C_STORE%FOUND then
      --WarningMessageTypeI: Stops Processing
      WRITE_ERROR( I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                  'VAT_REGION',
                  'CANNOT_DEL_VAT_REGION_ST');
      O_warning_flag := TRUE;
   end if;
   close C_STORE;

   open C_DEPS;
   fetch C_DEPS into L_var;
   if C_DEPS%FOUND then
      WRITE_ERROR( I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                  'VAT_REGION',
                  'CANNOT_DEL_VAT_R_DEPT');
      O_warning_flag  := TRUE;
   end if;
   close C_DEPS;

   open C_WAREHOUSE;
   fetch C_WAREHOUSE into L_var;
   if C_WAREHOUSE%FOUND then
      WRITE_ERROR( I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                  'VAT_REGION',
                  'CANNOT_DEL_VAT_R_WH');
      O_warning_flag  := TRUE;
   end if;
   close C_WAREHOUSE;

   open C_SKU;
   fetch C_SKU into L_var;
   if C_SKU%FOUND then
      WRITE_ERROR( I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                  'VAT_REGION',
                  'CANNOT_DEL_VAT_R_SKU');
      O_warning_flag  := TRUE;
   end if;
   close C_SKU;

   open C_SYSTEM_OPTIONS;
   fetch C_SYSTEM_OPTIONS into L_var;
   if C_SYSTEM_OPTIONS%FOUND then
      WRITE_ERROR( I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                  'VAT_REGION',
                  'CANNOT_DEL_DEF_VAT_REG');
      O_warning_flag  := TRUE;
   end if;
   close C_SYSTEM_OPTIONS;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_STORE%ISOPEN then
         close C_STORE;
      end if;
      if C_DEPS%ISOPEN then
         close C_DEPS;
      end if;
      if C_WAREHOUSE%ISOPEN then
         close C_WAREHOUSE;
      end if;
      if C_SKU%ISOPEN then
         close C_SKU;
      end if;
      if C_SYSTEM_OPTIONS%ISOPEN then
         close C_SYSTEM_OPTIONS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_DEL_VAT_REG;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_REG_DEL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_vat_reg_temp_rec  IN      VAT_REGION%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_VAT.EXEC_VAT_REG_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_REGION';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_VAT_REGION_TL_LOCK is
      select 'X'
        from vat_region_tl
       where vat_region = I_vat_reg_temp_rec.vat_region
       for update nowait;

   cursor C_VAT_REGION_LOCK is
      select 'X'
        from VAT_REGION
       where vat_region = I_vat_reg_temp_rec.vat_region
       for update nowait;
BEGIN

   open C_VAT_REGION_TL_LOCK;
   close C_VAT_REGION_TL_LOCK;

   delete
     from vat_region_tl
    where vat_region =i_vat_reg_temp_rec.vat_region;

   open C_VAT_REGION_LOCK;
   close C_VAT_REGION_LOCK;

   delete
     from vat_region
    where vat_region =i_vat_reg_temp_rec.vat_region;



   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_vat_reg_temp_rec.vat_region,
                                             NULL);
      return FALSE;

   when OTHERS then
      if C_VAT_REGION_TL_LOCK%ISOPEN then
         close C_VAT_REGION_TL_LOCK;
      end if;
      if C_VAT_REGION_LOCK%ISOPEN then
         close C_VAT_REGION_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_VAT_REG_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_REG_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_vat_region_tl_ins_tab    IN       VAT_REGION_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_VAT.EXEC_VAT_REG_TL_INS';

BEGIN
   if I_vat_region_tl_ins_tab is NOT NULL and I_vat_region_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_vat_region_tl_ins_tab.COUNT()
         insert into vat_region_tl
              values I_vat_region_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_VAT_REG_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_REG_TL_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_vat_region_tl_upd_tab  IN       VAT_REGION_TAB,
                             I_vat_region_tl_upd_rst  IN       ROW_SEQ_TAB,
                             I_process_id             IN       SVC_VAT_REGION_TL.PROCESS_ID%TYPE,
                             I_chunk_id               IN       SVC_VAT_REGION_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_VAT.EXEC_VAT_REG_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_REGION_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_VAT_REGION_TL_UPD(I_vat_region  VAT_REGION_TL.VAT_REGION%TYPE,
                                   I_lang        VAT_REGION_TL.LANG%TYPE) is
      select 'x'
        from vat_region_tl
       where vat_region = I_vat_region
         and lang = I_lang
         for update nowait;

BEGIN
   if I_vat_region_tl_upd_tab is NOT NULL and I_vat_region_tl_upd_tab.count > 0 then
      for i in I_vat_region_tl_upd_tab.FIRST..I_vat_region_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_vat_region_tl_upd_tab(i).lang);
            L_key_val2 := 'VAT Region: '||to_char(I_vat_region_tl_upd_tab(i).vat_region);
            open C_LOCK_VAT_REGION_TL_UPD(I_vat_region_tl_upd_tab(i).vat_region,
                                          I_vat_region_tl_upd_tab(i).lang);
            close C_LOCK_VAT_REGION_TL_UPD;
            
            update vat_region_tl
               set vat_region_name = I_vat_region_tl_upd_tab(i).vat_region_name,
                   last_update_id = I_vat_region_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_vat_region_tl_upd_tab(i).last_update_datetime
             where lang = I_vat_region_tl_upd_tab(i).lang
               and vat_region = I_vat_region_tl_upd_tab(i).vat_region;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_VAT_REGION_TL',
                           I_vat_region_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_VAT_REGION_TL_UPD%ISOPEN then
         close C_LOCK_VAT_REGION_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_VAT_REG_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_VAT_REG_TL_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_vat_region_tl_del_tab  IN       VAT_REGION_TAB,
                             I_vat_region_tl_del_rst  IN       ROW_SEQ_TAB,
                             I_process_id             IN       SVC_VAT_REGION_TL.PROCESS_ID%TYPE,
                             I_chunk_id               IN       SVC_VAT_REGION_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_VAT.EXEC_VAT_REG_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_REGION_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_VAT_REGION_TL_DEL(I_vat_region  VAT_REGION_TL.VAT_REGION%TYPE,
                                   I_lang        VAT_REGION_TL.LANG%TYPE) is
      select 'x'
        from vat_region_tl
       where vat_region = I_vat_region
         and lang = I_lang
         for update nowait;

BEGIN
   if I_vat_region_tl_del_tab is NOT NULL and I_vat_region_tl_del_tab.count > 0 then
      for i in I_vat_region_tl_del_tab.FIRST..I_vat_region_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_vat_region_tl_del_tab(i).lang);
            L_key_val2 := 'Vat Region: '||to_char(I_vat_region_tl_del_tab(i).vat_region);
            open C_LOCK_VAT_REGION_TL_DEL(I_vat_region_tl_del_tab(i).vat_region,
                                          I_vat_region_tl_del_tab(i).lang);
            close C_LOCK_VAT_REGION_TL_DEL;
            
            delete vat_region_tl
             where lang = I_vat_region_tl_del_tab(i).lang
               and vat_region = I_vat_region_tl_del_tab(i).vat_region;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_VAT_REGION_TL',
                           I_vat_region_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_VAT_REGION_TL_DEL%ISOPEN then
         close C_LOCK_VAT_REGION_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_VAT_REG_TL_DEL;
---------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_VAT_REG(O_error           IN OUT   BOOLEAN,
                             O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rec             IN       C_SVC_VAT_REG%ROWTYPE )
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_VAT.PROCESS_VAL_VAT_REG';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_VAT_REGION';
BEGIN

   if I_rec.action = action_new
      and I_rec.vat_region <= 0   then
      WRITE_ERROR( I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                   'VAT_REGION',
                   'GREATER_0');
      O_error :=TRUE;
   end if;

   --Predelete Check
   if I_rec.action = action_del   then
      if CHECK_DEL_VAT_REG(O_error,
                           O_error_message,
                           I_rec ) = FALSE   then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_VAL_VAT_REG;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAT_REG(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id    IN     SVC_VAT_REGION.PROCESS_ID%TYPE,
                         I_chunk_id      IN     SVC_VAT_REGION.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      :='CORESVC_VAT.PROCESS_VAT_REG';
   L_error              BOOLEAN;
   L_count              NUMBER;
   L_warning_flag       BOOLEAN                           := FALSE;
   L_process_error      BOOLEAN                           := FALSE;
   L_vat_reg_temp_rec   VAT_REGION%ROWTYPE;
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_VAT_REGION';
   L_warning_count      NUMBER;
   L_rowseq_count       NUMBER;
   c                    NUMBER                            := 1;
   TYPE L_row_seq_tab_type IS TABLE OF SVC_COMP_STORE_LINK.ROW_SEQ%TYPE;
   L_row_seq_tab        L_row_seq_tab_type := NEW L_row_seq_tab_type();
   

   cursor C_EU is
      select count(vat_region)
        from vat_region
       where vat_region_type = 'E';

   cursor C_SVC_EU is
      select row_seq
        from svc_vat_region
       where vat_region_type = 'E'
         and process_id = I_process_id;

BEGIN
   LP_warning_tab := NEW errors_tab_typ();
   FOR rec IN c_svc_VAT_REG(I_process_id,
                            I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;
      
      if rec.vatreg_rank = 1 then
         SAVEPOINT VATREG;
         L_row_seq_tab.DELETE;
         
         c:=1;
      end if;
      
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del)   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_VAT_REGION_rid is NOT NULL   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'VAT_REGION',
                    'VAT_REGION_EXIST');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.VAT_REGION IS NOT NULL
         and rec.PK_VAT_REGION_rid is NULL   then
            WRITE_ERROR( rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         rec.chunk_id,
                         L_table,
                         rec.row_seq,
                        'VAT_REGION',
                        'NO_RECORD');
            L_error :=TRUE;
      end if;

      if rec.action = action_new
         and(rec.VAT_REGION is NULL )   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'VAT_REGION',
                     'ENTER_NUM');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_new)   then

         if(rec.action = action_new
            or (rec.action = action_mod
            and NVL(rec.base_vat_region_name,-2) <> NVL(rec.vat_region_name,-1)) ) then
            if NOT rec.VAT_REGION_NAME IS NOT NULL then
                   WRITE_ERROR( I_process_id,
                                SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                I_chunk_id,
                                L_table,
                                rec.row_seq,
                               'VAT_REGION_NAME',
                               'ENTER_DESC');
                   L_error :=TRUE;
            end if;
         end if;

         if(rec.action = action_new
            or (rec.action = action_mod
            and NVL(rec.base_vat_region_type,-2) <> NVL(rec.vat_region_type,-1)) )   then
            if rec.VAT_REGION_TYPE IS NULL
               or rec.VAT_REGION_TYPE NOT IN  ( 'E', 'M', 'N' )   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'VAT_REGION_TYPE',
                           'INV_VAT_REG_E_M_N');
               L_error :=TRUE;
            end if;
         end if;

         if(rec.action = action_new
            or (rec.action = action_mod
            and NVL(rec.base_vat_region_type,-2) <> NVL(rec.vat_region_type,-1)) )   then
            if rec.acquisition_vat_ind IS NULL
               or rec.acquisition_vat_ind NOT IN ( 'Y','N' )   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'ACQUISITION_VAT_IND',
                           'INV_Y_N_IND');
               L_error :=TRUE;
            end if;
         end if;

      end if;

      -- Validate VAT Calc Type
      if rec.action in (action_new, action_mod) then
         if rec.vat_calc_type is NULL then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                         'VAT_CALC_TYPE',
                         'ENT_VAT_CALC_TYPE');
            L_error :=TRUE;
         end if;
         if rec.vat_calc_type is NOT NULL and rec.cd_vat_calc_type_rid is NULL then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                         'VAT_CALC_TYPE',
                         'INV_VAT_CALC_TYPE');
            L_error :=TRUE;
         end if;
         if rec.vat_calc_type = 'E' and rec.acquisition_vat_ind is NOT NULL and rec.acquisition_vat_ind != 'N'  then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                         'VAT Calc Type, Acquisition VAT',
                         'ACQ_VAT_IND_N_FOR_EXEMPT');
            L_error :=TRUE;
         end if;

         if rec.vat_calc_type = 'E' and rec.reverse_vat_threshold is NOT NULL  then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                         'VAT Calc Type, Reverse VAT Threshold',
                         'VAT_THRSHOLD_NULL_EXEMPT');
            L_error :=TRUE;
         end if;
      end if;

      -- Call to the validation function
      if(PROCESS_VAL_VAT_REG( L_error,
                              O_error_message,
                              rec ) = FALSE )   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_vat_reg_temp_rec.acquisition_vat_ind   := rec.acquisition_vat_ind;
         L_vat_reg_temp_rec.vat_region_type       := rec.vat_region_type;
         L_vat_reg_temp_rec.vat_region_name       := rec.vat_region_name;
         L_vat_reg_temp_rec.vat_region            := rec.vat_region;
         L_vat_reg_temp_rec.reverse_vat_threshold := rec.reverse_vat_threshold;
         L_vat_reg_temp_rec.vat_calc_type         := rec.vat_calc_type;

         if rec.action = action_new then
            if EXEC_VAT_REG_INS(O_error_message,
                                L_vat_reg_temp_rec ) = FALSE   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error :=TRUE;
            else
               LP_warning_tab.EXTEND();
               LP_warning_tab(LP_warning_tab.COUNT()).process_id  := rec.process_id;
               LP_warning_tab(LP_warning_tab.COUNT()).error_seq   := SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL;
               LP_warning_tab(LP_warning_tab.COUNT()).chunk_id    := rec.chunk_id;
               LP_warning_tab(LP_warning_tab.COUNT()).table_name  := L_table;
               LP_warning_tab(LP_warning_tab.COUNT()).row_seq     := rec.row_seq;
               LP_warning_tab(LP_warning_tab.COUNT()).column_name := 'VAT_REGION';
               LP_warning_tab(LP_warning_tab.COUNT()).error_msg   := 'VAT_REG_DEPT';
               LP_warning_tab(LP_warning_tab.COUNT()).error_type  := 'W';
            end if;
         end if;

         if rec.action = action_mod then
            
            if EXEC_VAT_REG_UPD(O_error_message,
                                L_vat_reg_temp_rec )=FALSE   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_VAT_REG_DEL(O_error_message,
                                L_vat_reg_temp_rec ) = FALSE   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if NOT L_process_error then
            L_row_seq_tab.extend();
            L_row_seq_tab(c)  := rec.row_seq;
            c:=c+1;
         end if;
      end if;

      if rec.vat_region_type <> rec.next_vat_region_type then

         L_count := 0;
         open C_EU;
         fetch C_EU into L_count;
         close C_EU;
         
         
         if L_count > 1 then
            
            L_rowseq_count := L_row_seq_tab.count();
            if L_rowseq_count > 0 then
              for i in 1..L_rowseq_count
               LOOP
                  WRITE_ERROR( I_process_id,
                                SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                I_chunk_id,
                                L_table,
                                L_row_seq_tab(i),
                               'VAT_REGION_TYPE',
                               'ONE_EU_REGION');
               END LOOP;
            end if;
            ROLLBACK TO SAVEPOINT VATREG;
            L_process_error := TRUE;
            LP_warning_tab.DELETE();
         elsif  LP_warning_tab.COUNT() > 0 then
                -- Insert warning messages for all committed new VAT regions
            
            L_warning_count := LP_warning_tab.COUNT();
            forall i IN 1..L_warning_count
               insert into svc_admin_upld_er
                    values LP_warning_tab(i);
            LP_warning_tab.DELETE();
         end if;
         
      end if;
      
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
   
   if C_EU%ISOPEN then
      close C_EU;
   end if;
   if C_SVC_EU%ISOPEN then
      close C_SVC_EU;
   end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAT_REG;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAT_REGION_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_VAT_REGION_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_VAT_REGION_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64) := 'CORESVC_VAT.PROCESS_VAT_REGION_TL';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_REGION_TL';
   L_base_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'VAT_REGION';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_VAT_REGION_TL';
   L_error                     BOOLEAN := FALSE;
   L_process_error             BOOLEAN := FALSE;
   L_vat_region_tl_temp_rec    VAT_REGION_TL%ROWTYPE;
   L_vat_region_tl_upd_rst     ROW_SEQ_TAB;
   L_vat_region_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_VAT_REGION_TL(I_process_id NUMBER,
                              I_chunk_id NUMBER) is
      select pk_vat_region_tl.rowid  as pk_vat_region_tl_rid,
             fk_vat_region.rowid     as fk_vat_region_rid,
             fk_lang.rowid           as fk_lang_rid,
             st.lang,
             st.vat_region,
             st.vat_region_name,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_vat_region_tl  st,
             vat_region         fk_vat_region,
             vat_region_tl      pk_vat_region_tl,
             lang               fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.vat_region  =  fk_vat_region.vat_region (+)
         and st.lang        =  pk_vat_region_tl.lang (+)
         and st.vat_region  =  pk_vat_region_tl.vat_region (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_VAT_REGION_TL is TABLE OF C_SVC_VAT_REGION_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_vat_region_tab        SVC_VAT_REGION_TL;

   L_VAT_REGION_TL_ins_tab         VAT_REGION_TAB         := NEW VAT_REGION_TAB();
   L_VAT_REGION_TL_upd_tab         VAT_REGION_TAB         := NEW VAT_REGION_TAB();
   L_VAT_REGION_TL_del_tab         VAT_REGION_TAB         := NEW VAT_REGION_TAB();

BEGIN
   if C_SVC_VAT_REGION_TL%ISOPEN then
      close C_SVC_VAT_REGION_TL;
   end if;

   open C_SVC_VAT_REGION_TL(I_process_id,
                            I_chunk_id);
   LOOP
      fetch C_SVC_VAT_REGION_TL bulk collect into L_svc_vat_region_tab limit LP_bulk_fetch_limit;
      if L_svc_vat_region_tab.COUNT > 0 then
         FOR i in L_svc_vat_region_tab.FIRST..L_svc_vat_region_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_vat_region_tab(i).action is NULL
               or L_svc_vat_region_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_region_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_vat_region_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_region_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_vat_region_tab(i).action = action_new
               and L_svc_vat_region_tab(i).pk_vat_region_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_region_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_vat_region_tab(i).action IN (action_mod, action_del)
               and L_svc_vat_region_tab(i).lang is NOT NULL
               and L_svc_vat_region_tab(i).vat_region is NOT NULL
               and L_svc_vat_region_tab(i).pk_vat_region_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_vat_region_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_vat_region_tab(i).action = action_new
               and L_svc_vat_region_tab(i).vat_region is NOT NULL
               and L_svc_vat_region_tab(i).fk_vat_region_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_vat_region_tab(i).row_seq,
                            NULL,
                           'INV_VAT_REGION');
               L_error :=TRUE;
            end if;

            if L_svc_vat_region_tab(i).action = action_new
               and L_svc_vat_region_tab(i).lang is NOT NULL
               and L_svc_vat_region_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_region_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_vat_region_tab(i).action in (action_new, action_mod) then
               if L_svc_vat_region_tab(i).vat_region_name is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_vat_region_tab(i).row_seq,
                              'VAT_REGION_NAME',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_vat_region_tab(i).vat_region is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_region_tab(i).row_seq,
                           'VAT_REGION',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_vat_region_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_vat_region_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_vat_region_tl_temp_rec.lang := L_svc_vat_region_tab(i).lang;
               L_vat_region_tl_temp_rec.vat_region := L_svc_vat_region_tab(i).vat_region;
               L_vat_region_tl_temp_rec.vat_region_name := L_svc_vat_region_tab(i).vat_region_name;
               L_vat_region_tl_temp_rec.create_datetime := SYSDATE;
               L_vat_region_tl_temp_rec.create_id := GET_USER;
               L_vat_region_tl_temp_rec.last_update_datetime := SYSDATE;
               L_vat_region_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_vat_region_tab(i).action = action_new then
                  L_VAT_REGION_TL_ins_tab.extend;
                  L_VAT_REGION_TL_ins_tab(L_VAT_REGION_TL_ins_tab.count()) := L_vat_region_tl_temp_rec;
               end if;

               if L_svc_vat_region_tab(i).action = action_mod then
                  L_VAT_REGION_TL_upd_tab.extend;
                  L_VAT_REGION_TL_upd_tab(L_VAT_REGION_TL_upd_tab.count()) := L_vat_region_tl_temp_rec;
                  L_vat_region_tl_upd_rst(L_VAT_REGION_TL_upd_tab.count()) := L_svc_vat_region_tab(i).row_seq;
               end if;

               if L_svc_vat_region_tab(i).action = action_del then
                  L_VAT_REGION_TL_del_tab.extend;
                  L_VAT_REGION_TL_del_tab(L_VAT_REGION_TL_del_tab.count()) := L_vat_region_tl_temp_rec;
                  L_vat_region_tl_del_rst(L_VAT_REGION_TL_del_tab.count()) := L_svc_vat_region_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_VAT_REGION_TL%NOTFOUND;
   END LOOP;
   close C_SVC_VAT_REGION_TL;

   if EXEC_VAT_REG_TL_INS(O_error_message,
                          L_VAT_REGION_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_VAT_REG_TL_UPD(O_error_message,
                          L_VAT_REGION_TL_upd_tab,
                          L_vat_region_tl_upd_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_VAT_REG_TL_DEL(O_error_message,
                          L_VAT_REGION_TL_del_tab,
                          L_vat_region_tl_del_rst,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAT_REGION_TL;
-----------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from SVC_VAT_CODES_TL
      where process_id = I_process_id;

   delete from svc_vat_code_rates
      where process_id = I_process_id;

   delete from svc_vat_codes
      where process_id = I_process_id;

   delete from svc_vat_region_tl
      where process_id = I_process_id;

   delete from svc_vat_region
      where process_id = I_process_id;      
END;
--------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    := 'CORESVC_VAT.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_tab          CORESVC_CFLEX.TYP_ERR_TAB;
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   
   if PROCESS_VAT_CODES(O_error_message,
                        I_process_id,
                        I_chunk_id) = FALSE   then
      return FALSE;
   end if;
   
   if PROCESS_VAT_CODES_TL(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE   then
      return FALSE;
   end if;
   
   if PROCESS_VAT_REG(O_error_message,
                      I_process_id,
                      I_chunk_id)=FALSE   then
      return FALSE;
   end if;

   
   if PROCESS_VAT_REGION_TL(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE   then
      return FALSE;
   end if;
   
   if CORESVC_CFLEX.PROCESS_CFA(O_error_message,
                                L_err_tab,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   FOR i in 1..L_err_tab.count
   LOOP
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_err_tab(i).view_name,
                  L_err_tab(i).row_seq,
                  L_err_tab(i).attrib,
                  L_err_tab(i).err_msg);
   END LOOP;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
            action_date = SYSDATE
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-------------------------------------------------------------------------------------------------------------------------------------
END CORESVC_VAT;
/