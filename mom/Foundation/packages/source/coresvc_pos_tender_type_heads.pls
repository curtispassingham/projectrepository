-- File Name : CORESVC_POS_TENDER_TYPE_HEAD_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_POS_TENDER_TYPE_HEAD AUTHID CURRENT_USER AS
   template_key          CONSTANT VARCHAR2(255) :='POS_TENDER_TYPE_HEAD_DATA';
   action_new                     VARCHAR2(25)  :='NEW';
   action_mod                     VARCHAR2(25)  :='MOD';
   action_del                     VARCHAR2(25)  :='DEL';
   POS_TT_HEAD_sheet              VARCHAR2(255) :='POS_TENDER_TYPE_HEAD';
   POS_TT_HEAD$Action             NUMBER        := 1;
   POS_TT_HEAD$TENDER_TYPE_ID     NUMBER        := 2;
   POS_TT_HEAD$TENDER_TYPE_DESC   NUMBER        := 3;
   POS_TT_HEAD$TENDER_TYPE_GROUP  NUMBER        := 4;
   POS_TT_HEAD$EFFECTIVE_DATE     NUMBER        := 5;
   
   POS_TT_HEAD_TL_sheet           VARCHAR2(255) :='POS_TENDER_TYPE_HEAD_TL';
   POS_TT_HEAD_TL$Action          NUMBER        := 1;
   POS_TT_HEAD_TL$LANG            NUMBER        := 2;
   POS_TT_HEAD_TL$TTYPE_ID        NUMBER        := 3;
   POS_TT_HEAD_TL$TTYPE_DESC      NUMBER        := 4;

   sheet_name_trans               S9T_PKG.trans_map_typ;
   action_column                  VARCHAR2(255)         := 'ACTION';
   template_category              CODE_DETAIL.CODE%TYPE := 'RSATT';
   TYPE POS_TT_HEAD_rec_tab IS TABLE OF POS_TENDER_TYPE_HEAD%ROWTYPE;
------------------------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT s9t_folder.file_id%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     s9t_folder.file_id%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
------------------------------------------------------------------------------------------------------
END CORESVC_POS_TENDER_TYPE_HEAD;
/
