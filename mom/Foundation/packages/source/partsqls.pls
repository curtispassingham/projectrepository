CREATE OR REPLACE PACKAGE PARTNER_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------
-- Function Name: GET_DESC
-- Purpose:       This function will validate that the Partner exists
--                and retrieve any given Partner's description and
--                return it to the calling module.
---------------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message   IN OUT   VARCHAR2,
                  O_partner_desc    IN OUT   PARTNER.PARTNER_DESC%TYPE,
                  I_partner_id      IN       PARTNER.PARTNER_ID%TYPE,
                  I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: GET_INFO
-- Purpose:       This function retrieves information from the Partner
--                table, given a Partner ID and Partner Type, and
--                returns it to the calling form.
---------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message          IN OUT   VARCHAR2,
                  O_partner_desc           IN OUT   PARTNER.PARTNER_DESC%TYPE,
                  O_currency_code          IN OUT   PARTNER.CURRENCY_CODE%TYPE,
                  O_status                 IN OUT   PARTNER.STATUS%TYPE,
                  O_principle_country_id   IN OUT   PARTNER.PRINCIPLE_COUNTRY_ID%TYPE,
                  O_mfg_id                 IN OUT   PARTNER.MFG_ID%TYPE,
                  O_lang                   IN OUT   PARTNER.LANG%TYPE,
                  I_partner_id             IN       PARTNER.PARTNER_id%TYPE,
                  I_partner_type           IN       PARTNER.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: CHECK_DELETE_PARTNER
-- Purpose:       This function checks the lc_head and ordhead tables
--                to see if a partner is being referenced in any other module.
--                If no references exist, the associated exp_profile_head,
--                exp_profile_detail, req_doc, and addr records are deleted.
--                Also checks price_susp_head table to see if a partner
--                being deleted is on a price change generated for a
--                vendor funded markdown.
---------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_PARTNER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE,
                              I_partner_id      IN       PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: DEL_PARTNER_CHILD_REC
-- Purpose:       This function will delete address (addr), documents (req_doc),
--                and expenses (exp_prof_head and exp_prof_detail) for a given partner
--                and partner_type.
---------------------------------------------------------------------------------------
FUNCTION DEL_PARTNER_CHILD_REC(O_error_message   IN OUT   VARCHAR2,
                               I_partner_type    IN       partner.partner_type%TYPE,
                               I_partner_id      IN       partner.partner_id%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------
-- Function Name: VALID_ACTIVE
-- Purpose:       This function determines if an active partner exists for
--                the passed in partner ID and parnter type.
---------------------------------------------------------------------------------------
FUNCTION VALID_ACTIVE(O_error_message   IN OUT   VARCHAR2,
                      O_valid           IN OUT   BOOLEAN,
                      I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE,
                      I_partner_id      IN       PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: GET_BANK_CREDIT
-- Purpose:       This function retrieves Credit information for a given bank.
---------------------------------------------------------------------------------------
FUNCTION GET_BANK_CREDIT(O_error_message          IN OUT   VARCHAR2,
                         O_line_of_credit         IN OUT   PARTNER.LINE_OF_CREDIT%TYPE,
                         O_outstand_credit        IN OUT   PARTNER.OUTSTAND_CREDIT%TYPE,
                         O_open_credit            IN OUT   PARTNER.OPEN_CREDIT%TYPE,
                         O_line_of_credit_prim    IN OUT   PARTNER.LINE_OF_CREDIT%TYPE,
                         O_outstand_credit_prim   IN OUT   PARTNER.OUTSTAND_CREDIT%TYPE,
                         O_open_credit_prim       IN OUT   PARTNER.OPEN_CREDIT%TYPE,
                         I_bank                   IN       PARTNER.PARTNER_ID%TYPE,
                         I_convert                IN       BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: BANK_STORE_EXISTS
-- Purpose:       This function will check for the existence of a particular
--                bank/store relationship on the sa_bank_store table
---------------------------------------------------------------------------------------
FUNCTION BANK_STORE_EXISTS(O_error_message   IN OUT   VARCHAR2,
                           O_exists          IN OUT   BOOLEAN,
                           I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE,
                           I_partner_id      IN       PARTNER.PARTNER_ID%TYPE,
                           I_store           IN       STORE.STORE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: STORES_EXIST
-- Purpose:       Checks for the existence of stores associated
--                with the given bank.
---------------------------------------------------------------------------------------
FUNCTION STORES_EXIST(O_error_message   IN OUT   VARCHAR2,
                      O_exists          IN OUT   BOOLEAN,
                      I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE,
                      I_partner_id      IN       PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: GET_PART_PRIMARY_ADDR
-- Purpose:       This function will take as input the partner type and
--                id, and will return the partner's primary address
--                (of the selected address type) through the addr_key value.
---------------------------------------------------------------------------------------
FUNCTION GET_PART_PRIMARY_ADDR(O_error_message   IN OUT   VARCHAR2,
                               O_addr_key        IN OUT   ADDR.ADDR_KEY%TYPE,
                               I_partner_id      IN       PARTNER.PARTNER_ID%TYPE,
                               I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE,
                               I_addr_type       IN       ADDR.ADDR_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: GET_PRIM_IA
-- Purpose:       This function will retrieve the primary import authority partner
--                for a given country ID.
---------------------------------------------------------------------------------------
FUNCTION GET_PRIM_IA(O_error_message   IN OUT   VARCHAR2,
                     O_partner_type    IN OUT   PARTNER.PARTNER_TYPE%TYPE,
                     O_partner_id      IN OUT   PARTNER.PARTNER_ID%TYPE,
                     I_country_id      IN       CE_HEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: UPDATE_PRIM_IA
-- Purpose:       This function will validate that a given partner is the only
--                primary import authority for a given country and if not it
--                will change other partners primary import authority indicators
--                to 'N' thereby making the given partner the primary import
--                authority for the given country.
---------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIM_IA(O_error_message   IN OUT   VARCHAR2,
                        I_country_id      IN       CE_HEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: GET_TERMS
-- Purpose:       This function will take as input the partner type and
--                id, and will return the partner's TERMS.
---------------------------------------------------------------------------------------
FUNCTION GET_TERMS(O_error_message   IN OUT   VARCHAR2,
                   O_terms           IN OUT   PARTNER.TERMS%TYPE,
                   I_partner_id      IN       PARTNER.PARTNER_ID%TYPE,
                   I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------

-- Function Name: NEXT_PARTNER
-- Purpose:       This function will return the next available partner id from
--                the sequence
---------------------------------------------------------------------------------------
FUNCTION NEXT_PARTNER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_partner_id      IN OUT   PARTNER.PARTNER_ID%TYPE,
                      I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: STATE_EXISTS
-- Purpose:       This function will check if the partner id exists on the STATE table
---------------------------------------------------------------------------------------
FUNCTION STATE_EXISTS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists                IN OUT   BOOLEAN,
                      O_dup_exists            IN OUT   VARCHAR2,
                      O_principal_country_id  IN OUT   PARTNER.PRINCIPLE_COUNTRY_ID%TYPE,
                      I_partner_id            IN       PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: PARTNER_EXISTS
-- Purpose:       This function will check if the partner id and partner type
--                combination exists on the PARTNER table
---------------------------------------------------------------------------------------
FUNCTION PARTNER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_partner_id      IN       PARTNER.PARTNER_ID%TYPE,
                        I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: DEL_PARTNER_ATTRIB
-- Purpose:       This function will delete from the PARTNER_L10N_EXT for  a given
--                partner_id and partner_type
---------------------------------------------------------------------------------------
FUNCTION DEL_PARTNER_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_partner_id      IN       PARTNER_L10N_EXT.PARTNER_ID%TYPE,
                            I_partner_type    IN       PARTNER_L10N_EXT.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function Name: DELETE_PARTNER_TL
-- Purpose:       This function will delete record from the PARTNER_TL table for a given
--                partner_id and partner_type
---------------------------------------------------------------------------------------
FUNCTION DELETE_PARTNER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_partner_type    IN       PARTNER_TL.PARTNER_TYPE%TYPE,
                           I_partner_id	     IN       PARTNER_TL.PARTNER_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_partner_id        IN       PARTNER.PARTNER_ID%TYPE,
                    I_partner_type      IN       PARTNER.PARTNER_TYPE%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END PARTNER_SQL;
/
