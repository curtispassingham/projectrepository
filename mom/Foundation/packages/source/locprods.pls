CREATE OR REPLACE PACKAGE LOC_PROD_SECURITY_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
-- Name:    NEW_USER
-- Purpose: This function takes a user_id as input, then return a value 
--          of TRUE back to the calling module if the user_id exists on
--          the all_users table, otherwise, it will return a value of 
--          FALSE back to the calling module if the user_id does not 
--          exist on the all_users table.  
-- Created By: Jeff Fang 06-01-99
FUNCTION NEW_USER(O_error_message IN OUT VARCHAR2,
                  O_exist         IN OUT BOOLEAN,
                  I_user_id       IN     all_users.username%TYPE) 
return BOOLEAN;
-----------------------------------------------------------------------------
-- Name:    NEXT_GROUP_NUMBER
-- Purpose: Select the sec_group_sequence.NEXTVAL into a local variable and 
--          perform a check to ensure it does not already exist. If it does 
--          not exist then return the value of the group back to the calling 
--          module. 
-- Created By: Jeff Fang 06-01-99
FUNCTION NEXT_GROUP_NUMBER(O_error_message IN OUT VARCHAR2,
                           O_group_id      IN OUT sec_group.group_id%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------
-- Name:    GET_GROUP_NAME
-- Purpose: This function will be used to return the group name back to 
--          the calling module. If a record is found the function should 
--          return TRUE and pass the record group name to the calling module.
--          If no record is found the function will return FALSE and an error
--          message will be returned stating the group_id is invalid.
-- Created By: Jeff Fang 06-01-99
FUNCTION GET_GROUP_NAME(O_error_message IN OUT VARCHAR2,
                        O_group_name    IN OUT sec_group.group_name%TYPE,
                        I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------
-- Name:    LOCK_GRP_SEC
-- Purpose: This function will be used to lock records so they can be deleted.
--          It will lock all records for the specified group off the 
--          sec_user_group, sec_group_loc_matrix, sec_group_loc_matrix tables.
-- Created By: Jeff Fang 06-01-99
FUNCTION LOCK_GRP_SEC(O_error_message IN OUT VARCHAR2,
                      I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------------
-- Name:    DELETE_GRP_SEC
-- Purpose: This function will be used to delete group security details. It 
--          will delete all the records for the specified group off the
--          sec_user_group table. Then depending on the security type of the
--          group, it will then delete records from one of the group security
--          matrix table.
-- Created By: Jeff Fang 06-01-99
FUNCTION DELETE_GRP_SEC(O_error_message IN OUT VARCHAR2,
                        I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN;
------------------------------------------------------------------------------
-- Name:    LOCK_USR_SEC
-- Purpose: This function will be used to lock records so they can be deleted.
--          It will lock all records for the specified user off the 
--          sec_user_group table.
-- Created By: Jeff Fang 06-01-99
FUNCTION LOCK_USR_SEC(O_error_message IN OUT VARCHAR2,
                      I_user_id       IN     VARCHAR2)
return BOOLEAN;
------------------------------------------------------------------------------
-- Name:    DELETE_USR_SEC
-- Purpose: This function will be used to delete user security details. It 
--          will delete all records for the specified user off the 
--          sec_user_group table.
-- Created By: Jeff Fang 06-01-99
FUNCTION DELETE_USR_SEC(O_error_message IN OUT VARCHAR2,
                        I_user_id      IN      VARCHAR2)
return BOOLEAN;
------------------------------------------------------------------------------ 
-- Name:    LOC_CHECK_UNIQUE
-- Purpose: This function will take the following parameters as input: group_id,
--          column_code, region, district, store, wh. It will then check that a 
--          record with the same unique key does not exist on the sec_group_loc_matrix 
--          table. If the record does exist then the function should return a 
--          value of TRUE in the exist field and should return an error message 
--          informing the user that this record already exists. If the record doesn't
--          exist the function should simply return a value of FALSE in the exist field.
-- Created By: Jeff Fang 06-02-99
FUNCTION LOC_CHECK_UNIQUE(O_error_message    IN OUT VARCHAR2,
                          O_exist            IN OUT BOOLEAN,
                          I_group_id         IN  sec_group_loc_matrix.group_id%TYPE,
                          I_column_code      IN  sec_group_loc_matrix.column_code%TYPE,
                          I_region           IN  sec_group_loc_matrix.region%TYPE,
                          I_district         IN  sec_group_loc_matrix.district%TYPE,
                          I_store            IN  sec_group_loc_matrix.store%TYPE,
                          I_wh               IN  sec_group_loc_matrix.wh%TYPE)
RETURN BOOLEAN; 
--------------------------------------------------------------------------------------
-- Name:    EXPAND_LOC
-- Purpose: This function will take the following parameters as input: group_id, region,
--          district, store, wh. It must then create record on the sec_group_loc_matrix 
--          table for every functional area to which location security can be applied. 
--          If the input wh field has been populated then it should be nulled out before
--          applying the promotional security record. 
-- Created By: Jeff Fang 06-03-99 
FUNCTION EXPAND_LOC(O_error_message    IN OUT VARCHAR2,
                    I_group_id         IN     sec_group_loc_matrix.group_id%TYPE,
                    I_region           IN     sec_group_loc_matrix.region%TYPE,
                    I_district         IN     sec_group_loc_matrix.district%TYPE,
                    I_store            IN     sec_group_loc_matrix.store%TYPE,
                    I_wh               IN     sec_group_loc_matrix.wh%TYPE,
                    I_select_ind       IN     sec_group_loc_matrix.select_ind%TYPE,
                    I_update_ind       IN     sec_group_loc_matrix.update_ind%TYPE)

RETURN BOOLEAN; 
--------------------------------------------------------------------------------------------
-- Name:    USRGRP_CHECK_UNIQUE
-- Purpose: This function will take the following parameters as input: group_id and user_id. It 
--          will then check that a record with the same primary key does not exist on the 
--          sec_user_group table. If the record does exist then the function should return a value
--          of TRUE in the exist field and should return an error message informing the user that 
--          this record already exists. If the record doesn't exist the function should simply 
--          return a value of FALSE in the exist field.
-- Created By: Jeff Fang 06-10-99
FUNCTION USRGRP_CHECK_UNIQUE(O_error_message   IN OUT VARCHAR2,
                             O_exist           IN OUT BOOLEAN,
                             I_group_id        IN     sec_user_group.group_id%TYPE,
                             I_user_id         IN     sec_user.database_user_id%TYPE) 
RETURN BOOLEAN; 
-------------------------------------------------------------------------------------------------
-- Name:    GET_ROLE
-- Purpose: This function will be used to return the role back to 
--          the calling module. If a record is found the function should 
--          return TRUE and pass the role to the calling module.
--          If no record is found the function will return FALSE and an error
--          message will be returned stating the group_id is invalid.
FUNCTION GET_ROLE(O_error_message   IN OUT VARCHAR2,
                  O_role               OUT SEC_GROUP.ROLE%TYPE,
                  I_group_id        IN     SEC_GROUP.GROUP_ID%TYPE) 
RETURN BOOLEAN; 
-------------------------------------------------------------------------------------------------
-- Name:    INSERT_SEC_USER
-- Purpose: This function will be used to add user in SEC_USER table.
FUNCTION INSERT_SEC_USER(O_error_message        IN OUT VARCHAR2,
                         I_database_user_id     IN     SEC_USER.DATABASE_USER_ID%TYPE,
                         I_application_user_id	IN     SEC_USER.APPLICATION_USER_ID%TYPE DEFAULT NULL) 
RETURN BOOLEAN; 
---------------------------------------------------------------------------
END;
/
