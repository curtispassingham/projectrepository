
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XDIFFID_VALIDATE AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diffid_rec      OUT    NOCOPY   DIFF_IDS%ROWTYPE,
                       I_message         IN              "RIB_XDiffIDDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diffid_rec      OUT    NOCOPY   DIFF_IDS%ROWTYPE,
                       I_message         IN              "RIB_XDiffIDRef_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XDIFFID_VALIDATE;
/
