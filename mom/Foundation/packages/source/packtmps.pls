CREATE OR REPLACE PACKAGE PACK_TEMPLATE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
   TYPE header_info is RECORD
      ( group1_desc   DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
        group2_desc   DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
        group3_desc   DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
        group4_desc   DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
        type1         DIFF_TYPE.DIFF_TYPE%TYPE,
        type1_desc    DIFF_TYPE.DIFF_TYPE_DESC%TYPE,
        type2         DIFF_TYPE.DIFF_TYPE%TYPE,
        type2_desc    DIFF_TYPE.DIFF_TYPE_DESC%TYPE,
        type3         DIFF_TYPE.DIFF_TYPE%TYPE,
        type3_desc    DIFF_TYPE.DIFF_TYPE_DESC%TYPE,
        type4         DIFF_TYPE.DIFF_TYPE%TYPE,
        type4_desc    DIFF_TYPE.DIFF_TYPE_DESC%TYPE);     

   TYPE input_rectype IS RECORD
      ( parent_diff1       ITEM_MASTER.DIFF_1%TYPE,
        parent_diff2       ITEM_MASTER.DIFF_2%TYPE,
        parent_diff3       ITEM_MASTER.DIFF_3%TYPE,
        parent_diff4       ITEM_MASTER.DIFF_4%TYPE,
        pack_tmpl_id       TEMP_PACK_TMPL.PACK_TMPL_ID%TYPE,
        diff1              TEMP_PACK_TMPL.DIFF1%TYPE,
        diff1_seq          TEMP_PACK_TMPL.DIFF1_SEQ%TYPE,
        diff2              TEMP_PACK_TMPL.DIFF2%TYPE,
        diff2_seq          TEMP_PACK_TMPL.DIFF2_SEQ%TYPE,
        diff3              TEMP_PACK_TMPL.DIFF3%TYPE,
        diff3_seq          TEMP_PACK_TMPL.DIFF3_SEQ%TYPE,
        diff4              TEMP_PACK_TMPL.DIFF4%TYPE,
        diff4_seq          TEMP_PACK_TMPL.DIFF4_SEQ%TYPE,
        diff_range         DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
        diff_range_group1  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
        diff_range_group2  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
        diff_range_group3  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
        qty                TEMP_PACK_TMPL.QTY%TYPE,
        row_id             ROWID,
        apply_to           VARCHAR2(3),
        insert_or_update   VARCHAR2(6));

---------------------------------------------------------------------------------------------
--Function Name : NEXT_PACK_TEMPLATE_ID
--Purpose       : This package will include a procedure to fetch the next
--                pack template id.
--Calls         :  
--Created By    : Sara Isbell
--                    30-AUG-97
---------------------------------------------------------------------------------------------
   PROCEDURE NEXT_PACK_TEMPLATE_ID (O_error_message    OUT VARCHAR2,
                                    IO_pack_tmpl_id IN OUT PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                                    O_return_code      OUT VARCHAR2);
---------------------------------------------------------------------------------------------
-- Function Name: GET_DESC
-- Purpose      : Get pack_tmpl_desc from the pack_tmpl_head table for a given pack_tmpl_id 
---------------------------------------------------------------------------------------------
   FUNCTION GET_DESC(O_error_message   IN OUT VARCHAR2,
                     O_pack_tmpl_desc  IN OUT PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE,
                     I_pack_tmpl_id    IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
      RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: POP_TEMP_PACK_TMPL
-- Purpose      : Copy records from the pack_tmpl_detail table to the temp_pack_tmpl table
--                for a given pack_tmpl_id.
-- Calls        : Nothing
---------------------------------------------------------------------------------------------
   FUNCTION POP_TEMP_PACK_TMPL(O_error_message     IN OUT VARCHAR2,
                               I_pack_tmpl_id      IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
      RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Overloaded version: This function was overloaded so that it could be used within the 
--                     packtmpl.fmb form for the like pack template functionality and so
--                     that the fashpack.fmb form did not need modifications.
---------------------------------------------------------------------------------------------
   FUNCTION POP_TEMP_PACK_TMPL(O_error_message     IN OUT VARCHAR2,
                               I_pack_tmpl_id      IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                               I_like_pack_tmpl_id IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
      RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_PACK_TEMPLATE
-- Purpose      : Delete records from pack_tmpl_detail and pack_tmpl_head tables for
--                a given pack_tmpl_id.
-- Calls        : Nothing
---------------------------------------------------------------------------------------------
   FUNCTION DELETE_PACK_TEMPLATE(O_error_message IN OUT VARCHAR2,
                                 I_pack_tmpl_id  IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                                 I_head_and_tail IN     BOOLEAN)
      RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: DELETE_TEMP_PACK_TMPL
-- Purpose      : Delete record from temp_pack_tmpl table for a given ID, and diffs 
--                If ID, diffs are not given, then delete all 
--                records.  If only ID is given, then delete all records with that ID.
-- Calls        : Nothing
---------------------------------------------------------------------------------------------
   FUNCTION DELETE_TEMP_PACK_TMPL(O_error_message IN OUT VARCHAR2,
                                  I_pack_tmpl_id  IN     TEMP_PACK_TMPL.PACK_TMPL_ID%TYPE,
                                  I_diff_1        IN     TEMP_PACK_TMPL.DIFF1%TYPE,
                                  I_diff_2        IN     TEMP_PACK_TMPL.DIFF2%TYPE,
                                  I_diff_3        IN     TEMP_PACK_TMPL.DIFF3%TYPE,
                                  I_diff_4        IN     TEMP_PACK_TMPL.DIFF4%TYPE)
      RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_HEAD
-- Purpose      : Insert a record into the pack_tmpl_head table for a given pack_tmpl_id and
--                pack_tmpl_desc.
-- Calls        : Nothing
---------------------------------------------------------------------------------------------
   FUNCTION INSERT_HEAD(O_error_message      IN OUT   VARCHAR2,
                        I_pack_tmpl_id       IN       PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                        I_pack_tmpl_desc     IN       PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE,
                        I_pack_type          IN       PACK_TMPL_HEAD.PACK_TYPE%TYPE,
                        I_diff_group_1       IN       PACK_TMPL_HEAD.DIFF_GROUP_1%TYPE,
                        I_diff_group_2       IN       PACK_TMPL_HEAD.DIFF_GROUP_2%TYPE,
                        I_diff_group_3       IN       PACK_TMPL_HEAD.DIFF_GROUP_3%TYPE,
                        I_diff_group_4       IN       PACK_TMPL_HEAD.DIFF_GROUP_4%TYPE,
                        I_fash_prepack_ind   IN       PACK_TMPL_HEAD.FASH_PREPACK_IND%TYPE,
                        I_receive_as_type    IN       PACK_TMPL_HEAD.REC_AS_TYPE%TYPE)
      RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_UPDATE_PACK_TEMPLATE_DETAIL
-- Purpose      : Update new pack_tmpl_detail's qty for a given pack_tmpl_id, diff_1,
--                diff_2, diff_3, diff_4, and qty.
-- Calls        : Nothing
---------------------------------------------------------------------------------------------
   FUNCTION INSERT_UPDATE_PACK_TMPL_DETAIL(O_error_message IN OUT VARCHAR2,
                                           I_pack_tmpl_id  IN     PACK_TMPL_DETAIL.PACK_TMPL_ID%TYPE,
                                           I_diff_1        IN     PACK_TMPL_DETAIL.DIFF_1%TYPE,
                                           I_diff_2        IN     PACK_TMPL_DETAIL.DIFF_2%TYPE,
                                           I_diff_3        IN     PACK_TMPL_DETAIL.DIFF_3%TYPE,
                                           I_diff_4        IN     PACK_TMPL_DETAIL.DIFF_4%TYPE,
                                           I_qty           IN     PACK_TMPL_DETAIL.QTY%TYPE)
      RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_FASH_PREPACK_IND
-- Purpose      : Retrieves the fash_prepack_ind from pack_tmpl_head.
--------------------------------------------------------------------------------
   FUNCTION GET_FASH_PREPACK_IND(O_error_message IN OUT VARCHAR2,
                                 O_exists        IN OUT BOOLEAN,
                                 I_pack_tmpl_id  IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
      RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Function Name: DELETE_PACK_TMPL_DETAIL
-- Purpose      : Deletes records from pack_tmpl_detail.
--------------------------------------------------------------------------
   FUNCTION DELETE_PACK_TMPL_DETAIL(O_error_message  IN OUT VARCHAR2,
                                    I_pack_tmpl_id   IN     PACK_TMPL_DETAIL.PACK_TMPL_ID%TYPE,
                                    I_diff_1         IN     PACK_TMPL_DETAIL.DIFF_1%TYPE,
                                    I_diff_2         IN     PACK_TMPL_DETAIL.DIFF_2%TYPE,
                                    I_diff_3         IN     PACK_TMPL_DETAIL.DIFF_3%TYPE,
                                    I_diff_4         IN     PACK_TMPL_DETAIL.DIFF_4%TYPE)

      RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Function Name: BUILD_FILTER_WHERE
-- Purpose      : Builds where clause based on the records on the fashpack_filter_temp table.
---------------------------------------------------------------------------   
FUNCTION BUILD_FILTER_WHERE (O_error_message   IN OUT VARCHAR2,
                                O_where_clause    IN OUT FILTER_TEMP.WHERE_CLAUSE%TYPE,
                                I_clear_filter    IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Function Name: BUILD_SUP_COST
-- Purpose      : This function calculates the cost of a prepack by summing the costs
--                of the components.
-- Created By   : Steph Vogel 02/17/00
---------------------------------------------------------------------------   
   FUNCTION BUILD_SUP_COST (O_error_message     IN OUT VARCHAR2,
                            O_sup_cost          IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                            I_pack_template_id  IN     TEMP_PACK_TMPL.PACK_TMPL_ID%TYPE,
                            I_supplier          IN     SUPS.SUPPLIER%TYPE,
                            I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                            I_item_parent       IN     ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Function Name: CHECK_ALL_RECORDS
-- Purpose      : Check if all the records on the temp_pack_tmpl table have all
--                the required data.
---------------------------------------------------------------------------  
   FUNCTION CHECK_ALL_RECORDS (O_error_message IN OUT VARCHAR2,
                               O_valid         IN OUT BOOLEAN,
                               I_num_of_diffs  IN     NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Function Name: DELETE_PACK_ITEM_EXPENSES
-- Purpose      : This function gets the component fashion sku for the fashion
--          prepack, then calls ORDER_SETUP_SQL.DELETE_COMPS to delete
--          individual items from the expenses and assessments tables. 
---------------------------------------------------------------------------  
FUNCTION DELETE_PACK_ITEM_EXPENSES(O_error_message IN OUT VARCHAR2,
                                   I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                                   I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                                   I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                                   I_diff_1        IN     ITEM_MASTER.DIFF_1%TYPE,
                                   I_diff_2        IN     ITEM_MASTER.DIFF_2%TYPE,
                                   I_diff_3        IN     ITEM_MASTER.DIFF_3%TYPE,
                                   I_diff_4        IN     ITEM_MASTER.DIFF_4%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------
-- Function Name: UPDATE_PACK_ITEM_EXPENSES
-- Purpose      : This function gets the component fashion sku for the fashion
--          prepack, then calculates the expenses and assessmensts for
--          the individual sku.
---------------------------------------------------------------------------  
FUNCTION UPDATE_PACK_ITEM_EXPENSES(O_error_message IN OUT VARCHAR2,
                                   I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                                   I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                                   I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                                   I_diff_1        IN     ITEM_MASTER.DIFF_1%TYPE,
                                   I_diff_2        IN     ITEM_MASTER.DIFF_2%TYPE,
                                   I_diff_3        IN     ITEM_MASTER.DIFF_3%TYPE,
                                   I_diff_4        IN     ITEM_MASTER.DIFF_4%TYPE)
RETURN BOOLEAN;




-------------------------------------------------------------------------------------------
FUNCTION EXIST(O_error_message    IN OUT VARCHAR2,
               O_exist            IN OUT BOOLEAN,
               I_pack_tmpl_id     IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION LIKE_TMPL(O_error_message        IN OUT VARCHAR2,
                    I_new_pack_tmpl_id     IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                    I_new_pack_tmpl_desc   IN     PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE,
                    I_like_tmpl            IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE(O_error_message IN OUT VARCHAR2,
                         O_duplicate     IN OUT BOOLEAN,
                         I_diff_1        IN     PACK_TMPL_DETAIL.DIFF_1%TYPE,
                         I_diff_2        IN     PACK_TMPL_DETAIL.DIFF_2%TYPE,
                         I_diff_3        IN     PACK_TMPL_DETAIL.DIFF_3%TYPE,
                         I_diff_4        IN     PACK_TMPL_DETAIL.DIFF_4%TYPE,
                         I_pack_tmpl_id  IN     PACK_TMPL_DETAIL.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_header_rec    IN OUT PACK_TEMPLATE_SQL.HEADER_INFO,
                         I_diff_group1   IN     PACK_TMPL_HEAD.DIFF_GROUP_1%TYPE,
                         I_diff_group2   IN     PACK_TMPL_HEAD.DIFF_GROUP_2%TYPE,
                         I_diff_group3   IN     PACK_TMPL_HEAD.DIFF_GROUP_3%TYPE,
                         I_diff_group4   IN     PACK_TMPL_HEAD.DIFF_GROUP_4%TYPE)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_seq_no        IN OUT PACK_TMPL_DETAIL.SEQ_NO%TYPE,
                         I_pack_tmpl_id  IN     PACK_TMPL_DETAIL.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION GET_GROUPS_AND_DESC(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT  BOOLEAN, 
                             O_pack_tmpl_desc  IN OUT  PACK_TMPL_HEAD.PACK_TMPL_DESC%TYPE,
                             O_group_1         IN OUT  PACK_TMPL_HEAD.DIFF_GROUP_1%TYPE,
                             O_group_2         IN OUT  PACK_TMPL_HEAD.DIFF_GROUP_2%TYPE,
                             O_group_3         IN OUT  PACK_TMPL_HEAD.DIFF_GROUP_3%TYPE,
                             O_group_4         IN OUT  PACK_TMPL_HEAD.DIFF_GROUP_4%TYPE,
                             I_pack_tmpl_id    IN      PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------          
--- APPLY_RANGE : This function will accept a record (defined above) and a where clause.  
---               It will insert records into temp_pack_tmpl based on the diff groups in the
---               range and the items selected in the where clause.
--------------------------------------------------------------------------------------------          
FUNCTION APPLY_RANGE(O_error_message  IN OUT  VARCHAR2,   
                     I_where_clause   IN      VARCHAR2,                                                         
                     I_rec            IN      INPUT_RECTYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------                                                       
--- APPLY_DIFF : This function applies diffs, qty, and seq_no.  It may apply to ONE or ALL records.
--------------------------------------------------------------------------------------------                                                       
FUNCTION APPLY_DIFF(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    I_where_clause   IN      VARCHAR2,
                    I_rec            IN      INPUT_RECTYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------                                                       
--- CHECK_OVERLAP : This function will determine if any of the diffs passed in would
---                 overwrite an existing diff on temp_pack_tmpl when applying to all records.
--------------------------------------------------------------------------------------------                                                       
FUNCTION CHECK_OVERLAP(O_error_message      IN OUT  VARCHAR2,
                       O_overlap            IN OUT  BOOLEAN,
                       I_pack_tmpl_id_used  IN      VARCHAR2,
                       I_diff1_used         IN      VARCHAR2,
                       I_diff2_used         IN      VARCHAR2,
                       I_diff3_used         IN      VARCHAR2,
                       I_diff4_used         IN      VARCHAR2,
                       I_where_clause       IN      VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------                                                       
--- CHECK_DUPS : This function searches for duplicate records on temp_pack_tmpl.  To find
---              duplicate records we group by the pack_tmpl_id and the diffs, then count
---              these records.  If there are more than 1 of any record then we have a duplicate.
--------------------------------------------------------------------------------------------                                                       
FUNCTION CHECK_DUPS(O_error_message  IN OUT  VARCHAR2,
                    O_dups_exist     IN OUT  BOOLEAN,
                    I_diff_count     IN      NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------                                                       
--- COUNT_RECS : This function counts all records for a pack_tmpl_id.
--------------------------------------------------------------------------------------------                                                       
FUNCTION COUNT_RECS(O_error_message  IN OUT  VARCHAR2,
                   O_rec_count      IN OUT  NUMBER,
                    I_pack_tmpl_id   IN OUT  TEMP_PACK_TMPL.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------                                                       

--------------------------------------------------------------------------------------------                                                       
--- PACK_TMPL_QTY : This function provides the the quantity for a pack_no
--------------------------------------------------------------------------------------------
FUNCTION PACK_TMPL_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,                                  
                       O_pack_tmpl_qty   IN OUT   PACK_TMPL_DETAIL.QTY%TYPE,                                 
                       I_pack_no         IN       PACKITEM.PACK_NO%TYPE)                                     
RETURN BOOLEAN;    
--------------------------------------------------------------------------------------------
-- Function : APPLY_RANGE_WRP
-- Purpose  : This is a wrapper function for APPLY_RANGE that passes a database
--            type object instead of a PL/SQL type as an input parameter to be called from Java wrappers.
--------------------------------------------------------------------------------------------------------
FUNCTION APPLY_RANGE_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_where_clause    IN       VARCHAR2,
                         I_rec             IN       WRP_RANGE_REC)
RETURN INTEGER;
----------------------------------------------------------------------------------------------------------   
-- Function : APPLY_DIFF_WRP
-- Purpose  : This is a wrapper function for APPLY_DIFF that passes a database
--            type object instead of a PL/SQL type as an input parameter to be called from Java wrappers.
--------------------------------------------------------------------------------------------------------
FUNCTION APPLY_DIFF_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_where_clause    IN       VARCHAR2,
                        I_rec             IN       WRP_DIFF_REC)
RETURN INTEGER;
----------------------------------------------------------------------------------------------------------
-- Function : DELETE_INVALID_DIFFS
-- Purpose  : Some of the diff combinations selected do not exist for item parent in the system. The additional 
--            items associated with the item parent/diff combinations will be removed from the pre-pack.
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_INVALID_DIFFS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_PACK_TMPL_HEAD_TL
-- Purpose:       This function deletes records from the pack_tmpl_head_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_PACK_TMPL_HEAD_TL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_pack_tmpl_id    IN       PACK_TMPL_HEAD_TL.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END PACK_TEMPLATE_SQL;
/
