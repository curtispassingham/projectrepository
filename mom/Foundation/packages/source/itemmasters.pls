CREATE OR REPLACE PACKAGE ITEM_MASTER_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------
-- Function:    ITEM_IMAGE
-- Purpose:     Checks to determine if any item image records exist for the item
-------------------------------------------------------------------
FUNCTION ITEM_IMAGE (O_error_message  IN OUT    VARCHAR2,
                     O_exist          IN OUT    BOOLEAN,
                     I_ITEM           IN        ITEM_MASTER.ITEM%TYPE) 
         RETURN BOOLEAN;

------------------------------------------------------------------- 
-- Function:    DEFAULT_CHILD_COST_ZONE
-- Purpose:     Defaults a new cost zone group to all of an item's children.
-------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_COST_ZONE(O_error_message IN OUT VARCHAR2,
                                 I_new_cost_zone IN     ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE,
                                 I_item          IN     ITEM_MASTER.ITEM%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------- 
-- Function: UPDATE_PRIMARY_REF_ITEM_IND
-- Purpose:  Updates all sub-transaction level items primary_ref_item_ind to 'N'o for a passed in item
-------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_REF_ITEM_IND(O_error_message    IN OUT VARCHAR2,
                                     I_item_parent      IN     ITEM_MASTER.ITEM%TYPE,
                                     I_item             IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function:   DEFAULT_CHILD_GROC_ATTRIB
-- Purpose: Defaults grocery attributes to all of an item's children.
-----------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_GROC_ATTRIB(O_error_message            IN OUT VARCHAR2,
                                   I_item                     IN ITEM_MASTER.ITEM%TYPE,
                                   I_package_size             IN ITEM_MASTER.PACKAGE_SIZE%TYPE,
                                   I_package_uom              IN ITEM_MASTER.PACKAGE_UOM%TYPE,
                                   I_retail_label_type        IN ITEM_MASTER.RETAIL_LABEL_TYPE%TYPE,
                                   I_retail_label_value       IN ITEM_MASTER.RETAIL_LABEL_VALUE%TYPE,
                                   I_handling_sensitivity     IN ITEM_MASTER.HANDLING_SENSITIVITY%TYPE,
                                   I_handling_temp            IN ITEM_MASTER.HANDLING_TEMP%TYPE,
                                   I_waste_type               IN ITEM_MASTER.WASTE_TYPE%TYPE,
                                   I_default_waste_pct        IN ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                                   I_waste_pct                IN ITEM_MASTER.WASTE_PCT%TYPE,
                                   I_container_item           IN ITEM_MASTER.CONTAINER_ITEM%TYPE,
                                   I_deposit_in_price_per_uom IN ITEM_MASTER.DEPOSIT_IN_PRICE_PER_UOM%TYPE,
                                   I_sale_type                IN ITEM_MASTER.SALE_TYPE%TYPE,
                                   I_order_type               IN ITEM_MASTER.ORDER_TYPE%TYPE,
                                   I_perishable_ind           IN ITEM_MASTER.PERISHABLE_IND%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function:   DEFAULT_CHILD_RCOM_ATTRIB
-- Purpose: Defaults RCOM attributes to all of an item's children.
-----------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_RCOM_ATTRIB(O_error_message          IN OUT VARCHAR2,
                                   I_item                   IN     ITEM_MASTER.ITEM%TYPE,
                                   I_item_service_level     IN     ITEM_MASTER.ITEM_SERVICE_LEVEL%TYPE,
                           I_product_classification IN     ITEM_MASTER.PRODUCT_CLASSIFICATION%TYPE,
                           I_brand_name             IN     ITEM_MASTER.BRAND_NAME%TYPE,
                                   I_gift_wrap_ind          IN     ITEM_MASTER.GIFT_WRAP_IND%TYPE,
                                   I_ship_alone_ind         IN     ITEM_MASTER.SHIP_ALONE_IND%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function:   UPDATE_CHECK_UDA_IND
-- Purpose: Sets the item_master.check_uda_ind = Y.
--              Called when users click OK on the itemuda form for the
--              first time.
-----------------------------------------------------------------------
FUNCTION UPDATE_CHECK_UDA_IND(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item               IN     ITEM_MASTER.ITEM%TYPE,
                              I_check_uda_ind      IN     ITEM_MASTER.CHECK_UDA_IND%TYPE DEFAULT 'Y')
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function:    UPDATE_ITEM_MASTER
-- Purpose:     Updates a single row on the item_master. Used for external item API.
--              Updates item_desc, short_item_desc, store_ord_mult, comments, forecast_ind from
--              input parameter of item_master row%type.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_MASTER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec      IN ITEM_MASTER%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: LOCK_ITEM_MASTER
-- Purpose      : This function locks an item_master row for a single item.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_MASTER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function:    DEFAULT_CHILD_STORE_ORD_MULT
-- Purpose:     Updates the store order multiple of the child item based on
--              the new store order multiple of the parent item. Used by the itemmaster form.
--
-----------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_STORE_ORD_MULT (O_error_message     IN     OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                       I_store_ord_mult    IN     ITEM_MASTER.STORE_ORD_MULT%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------- 
-- Function:    DEFAULT_CHILD_FORECAST_IND
-- Purpose:     Defaults the forecastable indicator to all its children.
-----------------------------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_FORECAST_IND(O_error_message   IN OUT   VARCHAR2,
                                    I_forecast_ind    IN       ITEM_MASTER.FORECAST_IND%TYPE,
                                    I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END ITEM_MASTER_SQL;
/
