create or replace PACKAGE BODY CORESVC_COST_COMPONENT AS
   cursor C_SVC_ELC_COMP(I_process_id   NUMBER,
                         I_chunk_id     NUMBER) is
      select pk_elc_comp.rowid        as pk_elc_comp_rid,
             st.rowid                 as st_rid,
             elc_cvb_fk.rowid         as elc_cvb_fk_rid,
             elc_cnt_fk.rowid         as elc_cnt_fk_rid,
             elc_cur_fk.rowid         as elc_cur_fk_rid,
             cd_ctyp.rowid            as cd_rid,
             UPPER(st.action)         as action,
             UPPER(st.comp_id)        as comp_id,
             st.comp_desc,
             st.comp_type,
             pk_elc_comp.comp_type    as old_comp_type,
             st.assess_type,
             UPPER(st.import_country_id) as import_country_id,
             st.expense_type,
             pk_elc_comp.expense_type as old_expense_type,
             st.up_chrg_type,
             pk_elc_comp.up_chrg_type as old_up_chrg_type,
             st.up_chrg_group,
             UPPER(st.cvb_code)       as cvb_code,
             st.calc_basis,
             pk_elc_comp.calc_basis   as old_calc_basis,
             st.cost_basis,
             pk_elc_comp.cost_basis   as old_cost_basis,
             st.exp_category,
             st.comp_rate,
             st.comp_level,
             st.display_order,
             st.always_default_ind,
             UPPER(st.comp_currency)  as comp_currency,
             st.per_count,
             UPPER(st.per_count_uom)  as per_count_uom,
             st.nom_flag_1,
             st.nom_flag_2,
             st.nom_flag_3,
             st.nom_flag_4,
             st.nom_flag_5,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             st.process$status
        from svc_elc_comp st,
             elc_comp     pk_elc_comp,
             cvb_head     elc_cvb_fk,
             country      elc_cnt_fk,
             currencies   elc_cur_fk,
             code_detail  cd_ctyp
       where st.process_id                = I_process_id
         and st.chunk_id                  = I_chunk_id
         and UPPER(st.comp_id)            = pk_elc_comp.comp_id (+)
         --and st.comp_type                 = pk_elc_comp.comp_type(+)
         and st.comp_type                 = cd_ctyp.code (+)
         and cd_ctyp.code_type (+)        = 'CTYP'
         and UPPER(st.import_country_id)  = elc_cnt_fk.country_id (+)
         and UPPER(st.cvb_code)           = elc_cvb_fk.cvb_code (+)
         and UPPER(st.comp_currency)      = elc_cur_fk.currency_code (+);

   cursor C_SVC_COST_COMP_UPD_STG(I_process_id   NUMBER,
                                  I_chunk_id     NUMBER) is
      select
             st.rowid                   as st_rid,
             elc_fk.rowid               as elc_rid, 
             cd_ctyp.rowid              as cd_rid, 
             cd_expt.rowid              as cd_ex_rid, 
             pk_cost_comp.rowid         as pk_ccus_rid,
             pk_cost_comp.dept          as dept_pk,
             pk_cost_comp.from_loc      as from_loc_pk,
             pk_cost_comp.to_loc        as to_loc_pk,
             pk_cost_comp.exp_prof_key  as exp_prof_key_pk,
             st.effective_date,
             st.effective_date_upd,
             pk_cost_comp.defaulting_level,
             st.ptnr_default_ind,
             st.supp_default_ind,
             st.cntry_default_ind,
             UPPER(st.new_per_count_uom)  as new_per_count_uom,
             st.new_per_count,
             UPPER(st.new_comp_currency)  as new_comp_currency,
             st.new_comp_rate,
             st.expense_type,
             st.comp_type,
             st.comp_id,
             st.dept_default_ind,
             st.tsf_alloc_default_ind,
             st.order_default_ind,
             st.item_default_ind,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)           as action,
             st.process$status
        from svc_cost_comp_upd_stg st,
             cost_comp_upd_stg     pk_cost_comp, 
             elc_comp              elc_fk,  
             code_detail           cd_ctyp,
             code_detail           cd_expt,
             dual
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.comp_id            = pk_cost_comp.comp_id (+)
         and st.effective_date     = pk_cost_comp.effective_date (+)
         and st.comp_id            = elc_fk.comp_id (+)
         and st.comp_type          = cd_ctyp.code (+)
         and cd_ctyp.code_type (+) = 'CTYP'
         and st.expense_type       = cd_expt.code (+)
         and cd_expt.code_type (+) = 'EXPT';

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab           errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab       s9t_errors_tab_typ;

   Type ELC_COMP_TL_TAB IS TABLE OF ELC_COMP_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id  IN  S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet    IN  VARCHAR2,
                          I_row_seq  IN  NUMBER,
                          I_col      IN  VARCHAR2,
                          I_sqlcode  IN  NUMBER,
                          I_sqlerrm  IN  VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets         S9T_PKG.NAMES_MAP_TYP;
   ELC_COMP_cols    S9T_PKG.NAMES_MAP_TYP;
   ELC_COMP_TL_cols S9T_PKG.NAMES_MAP_TYP;
   ELC_COMP_CFA_EXT_cols  S9T_PKG.NAMES_MAP_TYP;
   COST_COMP_UPD_STG_cols S9T_PKG.NAMES_MAP_TYP;

FUNCTION get_col_indx(I_names   IN s9t_pkg.names_map_typ,
                      I_col_key IN VARCHAR2)
RETURN NUMBER IS
BEGIN
   IF I_names.exists(I_col_key) THEN
     RETURN I_names(I_col_key);
   ELSE
     RETURN NULL;
   END IF;
END get_col_indx;

BEGIN
   L_sheets                       := S9T_PKG.GET_SHEET_NAMES(I_file_id);
                                  
   --ELC_COMP                     
   ELC_COMP_cols                  := S9T_PKG.GET_COL_NAMES(I_file_id,ELC_COMP_sheet);
   ELC_COMP$Action                := ELC_COMP_cols('ACTION');
   ELC_COMP$COMP_ID               := ELC_COMP_cols('COMP_ID');
   ELC_COMP$COMP_DESC             := ELC_COMP_cols('COMP_DESC');
   ELC_COMP$COMP_TYPE             := ELC_COMP_cols('COMP_TYPE');
   ELC_COMP$ASSESS_TYPE           := ELC_COMP_cols('ASSESS_TYPE');
   ELC_COMP$EXPENSE_TYPE          := ELC_COMP_cols('EXPENSE_TYPE');
   ELC_COMP$UP_CHRG_TYPE          := ELC_COMP_cols('UP_CHRG_TYPE');
   ELC_COMP$UP_CHRG_GROUP         := ELC_COMP_cols('UP_CHRG_GROUP');
   ELC_COMP$CVB_CODE              := ELC_COMP_cols('CVB_CODE');
   ELC_COMP$CALC_BASIS            := ELC_COMP_cols('CALC_BASIS');
   ELC_COMP$COST_BASIS            := ELC_COMP_cols('COST_BASIS');
   ELC_COMP$EXP_CATEGORY          := ELC_COMP_cols('EXP_CATEGORY');
   ELC_COMP$COMP_RATE             := ELC_COMP_cols('COMP_RATE');
   ELC_COMP$COMP_LEVEL            := ELC_COMP_cols('COMP_LEVEL');
   ELC_COMP$DISPLAY_ORDER         := ELC_COMP_cols('DISPLAY_ORDER');
   ELC_COMP$ALWAYS_DEFAULT_IND    := ELC_COMP_cols('ALWAYS_DEFAULT_IND');
   ELC_COMP$COMP_CURRENCY         := ELC_COMP_cols('COMP_CURRENCY');
   ELC_COMP$PER_COUNT             := ELC_COMP_cols('PER_COUNT');
   ELC_COMP$PER_COUNT_UOM         := ELC_COMP_cols('PER_COUNT_UOM');
   ELC_COMP$NOM_FLAG_1            := ELC_COMP_cols('NOM_FLAG_1');
   ELC_COMP$NOM_FLAG_2            := ELC_COMP_cols('NOM_FLAG_2');
   ELC_COMP$NOM_FLAG_3            := ELC_COMP_cols('NOM_FLAG_3');
   ELC_COMP$NOM_FLAG_4            := ELC_COMP_cols('NOM_FLAG_4');
   ELC_COMP$NOM_FLAG_5            := ELC_COMP_cols('NOM_FLAG_5');
                                  
   -- ELC_COMP_TL                 
   ELC_COMP_TL_cols               := S9T_PKG.GET_COL_NAMES(I_file_id,ELC_COMP_TL_sheet);
   ELC_COMP_TL$Action             := ELC_COMP_TL_cols('ACTION');
   ELC_COMP_TL$LANG               := ELC_COMP_TL_cols('LANG');
   ELC_COMP_TL$COMP_ID            := ELC_COMP_TL_cols('COMP_ID');
   ELC_COMP_TL$COMP_DESC          := ELC_COMP_TL_cols('COMP_DESC');
   
   --ELC_COMP_CFA_EXT
   ELC_COMP_CFA_EXT_cols          := s9t_pkg.get_col_names(I_file_id,ELC_COMP_CFA_EXT_sheet);
   ELC_COMP_CFA_EXT$Action        := get_col_indx(ELC_COMP_CFA_EXT_cols,'ACTION');
   ELC_COMP_CFA_EXT$COMP_ID       := get_col_indx(ELC_COMP_CFA_EXT_cols,'COMP_ID');
   ELC_COMP_CFA_EXT$GROUP_ID      := get_col_indx(ELC_COMP_CFA_EXT_cols,'GROUP_ID');
   ELC_COMP_CFA_EXT$VARCHAR2_1    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_1');
   ELC_COMP_CFA_EXT$VARCHAR2_2    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_2');
   ELC_COMP_CFA_EXT$VARCHAR2_3    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_3');
   ELC_COMP_CFA_EXT$VARCHAR2_4    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_4');
   ELC_COMP_CFA_EXT$VARCHAR2_5    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_5');
   ELC_COMP_CFA_EXT$VARCHAR2_6    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_6');
   ELC_COMP_CFA_EXT$VARCHAR2_7    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_7');
   ELC_COMP_CFA_EXT$VARCHAR2_8    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_8');
   ELC_COMP_CFA_EXT$VARCHAR2_9    := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_9');
   ELC_COMP_CFA_EXT$VARCHAR2_10   := get_col_indx(ELC_COMP_CFA_EXT_cols,'VARCHAR2_10');
   ELC_COMP_CFA_EXT$NUMBER_11     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_11');
   ELC_COMP_CFA_EXT$NUMBER_12     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_12');
   ELC_COMP_CFA_EXT$NUMBER_13     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_13');
   ELC_COMP_CFA_EXT$NUMBER_14     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_14');
   ELC_COMP_CFA_EXT$NUMBER_15     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_15');
   ELC_COMP_CFA_EXT$NUMBER_16     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_16');
   ELC_COMP_CFA_EXT$NUMBER_17     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_17');
   ELC_COMP_CFA_EXT$NUMBER_18     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_18');
   ELC_COMP_CFA_EXT$NUMBER_19     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_19');
   ELC_COMP_CFA_EXT$NUMBER_20     := get_col_indx(ELC_COMP_CFA_EXT_cols,'NUMBER_20');
   ELC_COMP_CFA_EXT$DATE_21       := get_col_indx(ELC_COMP_CFA_EXT_cols,'DATE_21');
   ELC_COMP_CFA_EXT$DATE_22       := get_col_indx(ELC_COMP_CFA_EXT_cols,'DATE_22');
   ELC_COMP_CFA_EXT$DATE_23       := get_col_indx(ELC_COMP_CFA_EXT_cols,'DATE_23');
   ELC_COMP_CFA_EXT$DATE_24       := get_col_indx(ELC_COMP_CFA_EXT_cols,'DATE_24');
   ELC_COMP_CFA_EXT$DATE_25       := get_col_indx(ELC_COMP_CFA_EXT_cols,'DATE_25');
   
   --COST_COMP_UPD_STG
   COST_COMP_UPD_STG_cols         := s9t_pkg.get_col_names(I_file_id,COST_COMP_UPD_STG_sheet);
   CCUS$Action                    := COST_COMP_UPD_STG_cols('ACTION');
   CCUS$EFFECTIVE_DATE            := COST_COMP_UPD_STG_cols('EFFECTIVE_DATE');
   CCUS$EFFECTIVE_DATE_UPD        := COST_COMP_UPD_STG_cols('EFFECTIVE_DATE_UPD');   
   CCUS$PTNR_DEFAULT_IND          := COST_COMP_UPD_STG_cols('PTNR_DEFAULT_IND');
   CCUS$SUPP_DEFAULT_IND          := COST_COMP_UPD_STG_cols('SUPP_DEFAULT_IND');
   CCUS$CNTRY_DEFAULT_IND         := COST_COMP_UPD_STG_cols('CNTRY_DEFAULT_IND');
   CCUS$NEW_PER_COUNT_UOM         := COST_COMP_UPD_STG_cols('NEW_PER_COUNT_UOM');
   CCUS$NEW_PER_COUNT             := COST_COMP_UPD_STG_cols('NEW_PER_COUNT');
   CCUS$NEW_COMP_CURRENCY         := COST_COMP_UPD_STG_cols('NEW_COMP_CURRENCY');
   CCUS$NEW_COMP_RATE             := COST_COMP_UPD_STG_cols('NEW_COMP_RATE');
   CCUS$EXPENSE_TYPE              := COST_COMP_UPD_STG_cols('EXPENSE_TYPE');
   CCUS$COMP_TYPE                 := COST_COMP_UPD_STG_cols('COMP_TYPE');
   CCUS$COMP_ID                   := COST_COMP_UPD_STG_cols('COMP_ID');
   CCUS$DEPT_DEFAULT_IND          := COST_COMP_UPD_STG_cols('DEPT_DEFAULT_IND');
   CCUS$TSF_ALLOC_DEFAULT_IND     := COST_COMP_UPD_STG_cols('TSF_ALLOC_DEFAULT_IND');
   CCUS$ORDER_DEFAULT_IND         := COST_COMP_UPD_STG_cols('ORDER_DEFAULT_IND');
   CCUS$ITEM_DEFAULT_IND          := COST_COMP_UPD_STG_cols('ITEM_DEFAULT_IND');
   
END POPULATE_NAMES;

-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ELC_COMP( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = ELC_COMP_sheet )
          select s9t_row(s9t_cells(CORESVC_COST_COMPONENT.action_mod ,
                                   comp_id,
                                   comp_desc,
                                   comp_type,
                                   assess_type,
                                   import_country_id,
                                   expense_type,
                                   up_chrg_type,
                                   up_chrg_group,
                                   cvb_code,
                                   calc_basis,
                                   cost_basis,
                                   exp_category,
                                   comp_rate,
                                   comp_level,
                                   display_order,
                                   always_default_ind,
                                   comp_currency,
                                   per_count,
                                   per_count_uom,
                                   nom_flag_1,
                                   nom_flag_2,
                                   nom_flag_3,
                                   nom_flag_4,
                                   nom_flag_5))
            from ELC_COMP;
END POPULATE_ELC_COMP;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_ELC_COMP_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = ELC_COMP_TL_sheet )
          select s9t_row(s9t_cells(CORESVC_COST_COMPONENT.action_mod,
                                   lang,
                                   comp_id,
                                   comp_desc))
            from ELC_COMP_TL;
END POPULATE_ELC_COMP_TL;
----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COST_COMP_UPD_STG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE( select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = COST_COMP_UPD_STG_sheet )
      select s9t_row(s9t_cells(CORESVC_COST_COMPONENT.action_mod,
                               comp_id,
                               comp_type,
                               expense_type,
                               new_comp_rate,
                               new_per_count,
                               new_per_count_uom,
                               new_comp_currency,
                               effective_date,
                               effective_date,
                               item_default_ind,
                               order_default_ind,
                               cntry_default_ind,
                               supp_default_ind,
                               ptnr_default_ind,
                               dept_default_ind,
                               tsf_alloc_default_ind))
         from cost_comp_upd_stg 
        where defaulting_level = 'E';
END POPULATE_COST_COMP_UPD_STG;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file      S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(ELC_COMP_sheet);
   L_file.sheets(l_file.get_sheet_index(ELC_COMP_sheet)).column_headers := s9t_cells('ACTION',
                                                                                     'COMP_ID',
                                                                                     'COMP_DESC',
                                                                                     'COMP_TYPE',
                                                                                     'ASSESS_TYPE',
                                                                                     'IMPORT_COUNTRY_ID',
                                                                                     'EXPENSE_TYPE',
                                                                                     'UP_CHRG_TYPE',
                                                                                     'UP_CHRG_GROUP',
                                                                                     'CVB_CODE',
                                                                                     'CALC_BASIS',
                                                                                     'COST_BASIS',
                                                                                     'EXP_CATEGORY',
                                                                                     'COMP_RATE',
                                                                                     'COMP_LEVEL',
                                                                                     'DISPLAY_ORDER',
                                                                                     'ALWAYS_DEFAULT_IND',
                                                                                     'COMP_CURRENCY',
                                                                                     'PER_COUNT',
                                                                                     'PER_COUNT_UOM',
                                                                                     'NOM_FLAG_1',
                                                                                     'NOM_FLAG_2',
                                                                                     'NOM_FLAG_3',
                                                                                     'NOM_FLAG_4',
                                                                                     'NOM_FLAG_5');

   L_file.add_sheet(ELC_COMP_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(ELC_COMP_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                        'LANG',
                                                                                        'COMP_ID',
                                                                                        'COMP_DESC');
   L_file.add_sheet(COST_COMP_UPD_STG_sheet);
   L_file.sheets(l_file.get_sheet_index(COST_COMP_UPD_STG_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                               'COMP_ID', 
                                                                                               'COMP_TYPE',
                                                                                               'EXPENSE_TYPE',
                                                                                               'NEW_COMP_RATE',
                                                                                               'NEW_PER_COUNT',
                                                                                               'NEW_PER_COUNT_UOM',
                                                                                               'NEW_COMP_CURRENCY',
                                                                                               'EFFECTIVE_DATE',
                                                                                               'EFFECTIVE_DATE_UPD',
                                                                                               'ITEM_DEFAULT_IND',
                                                                                               'ORDER_DEFAULT_IND',
                                                                                               'CNTRY_DEFAULT_IND',
                                                                                               'SUPP_DEFAULT_IND',
                                                                                               'PTNR_DEFAULT_IND',
                                                                                               'DEPT_DEFAULT_IND',
                                                                                               'TSF_ALLOC_DEFAULT_IND');
   S9T_PKG.SAVE_OBJ(L_FILE);
END INIT_S9T;
------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_COST_COMPONENT.CREATE_S9T';
   L_file    S9T_FILE;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_ELC_COMP(O_file_id);
      POPULATE_ELC_COMP_TL(O_file_id);
      POPULATE_COST_COMP_UPD_STG(O_file_id);
      COMMIT;
   end if;
   if CORESVC_CFLEX.ADD_CFLEX_SHEETS(O_error_message,
                                     O_file_id,
                                     template_key,
                                     I_template_only_ind) = FALSE   then
      return FALSE;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ELC_COMP(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id IN SVC_ELC_COMP.PROCESS_ID%TYPE) IS

   TYPE svc_elc_comp_col_typ IS TABLE OF SVC_ELC_COMP%ROWTYPE;
   L_temp_rec        SVC_ELC_COMP%ROWTYPE;
   svc_elc_comp_col  svc_elc_comp_col_typ           := NEW svc_elc_comp_col_typ();
   L_process_id      SVC_ELC_COMP.PROCESS_ID%TYPE;
   L_error           BOOLEAN                        := FALSE;
   L_default_rec     SVC_ELC_COMP%ROWTYPE;
   cursor C_MANDATORY_IND is
      select comp_id_mi,
             comp_desc_mi,
             comp_type_mi,
             assess_type_mi,
             import_country_id_mi,
             expense_type_mi,
             up_chrg_type_mi,
             up_chrg_group_mi,
             cvb_code_mi,
             calc_basis_mi,
             cost_basis_mi,
             exp_category_mi,
             comp_rate_mi,
             comp_level_mi,
             display_order_mi,
             always_default_ind_mi,
             comp_currency_mi,
             per_count_mi,
             per_count_uom_mi,
             nom_flag_1_mi,
             nom_flag_2_mi,
             nom_flag_3_mi,
             nom_flag_4_mi,
             nom_flag_5_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key    = template_key
                 and wksht_key       = 'ELC_COMP')
               PIVOT(MAX(mandatory) as mi FOR (column_key)IN( 'COMP_ID'            as comp_id,
                                                              'COMP_DESC'          as comp_desc,
                                                              'COMP_TYPE'          as comp_type,
                                                              'ASSESS_TYPE'        as assess_type,
                                                              'IMPORT_COUNTRY_ID'  as import_country_id,
                                                              'EXPENSE_TYPE'       as expense_type,
                                                              'UP_CHRG_TYPE'       as up_chrg_type,
                                                              'UP_CHRG_GROUP'      as up_chrg_group,
                                                              'CVB_CODE'           as cvb_code,
                                                              'CALC_BASIS'         as calc_basis,
                                                              'COST_BASIS'         as cost_basis,
                                                              'EXP_CATEGORY'       as exp_category,
                                                              'COMP_RATE'          as comp_rate,
                                                              'COMP_LEVEL'         as comp_level,
                                                              'DISPLAY_ORDER'      as display_order,
                                                              'ALWAYS_DEFAULT_IND' as always_default_ind,
                                                              'COMP_CURRENCY'      as comp_currency,
                                                              'PER_COUNT'          as per_count,
                                                              'PER_COUNT_UOM'      as per_count_uom,
                                                              'NOM_FLAG_1'         as nom_flag_1,
                                                              'NOM_FLAG_2'         as nom_flag_2,
                                                              'NOM_FLAG_3'         as nom_flag_3,
                                                              'NOM_FLAG_4'         as nom_flag_4,
                                                              'NOM_FLAG_5'         as nom_flag_5,
                                                               NULL                as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_ELC_COMP';
   L_pk_columns    VARCHAR2(255)  := 'Component';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select comp_id_dv,
                      comp_desc_dv,
                      comp_type_dv,
                      assess_type_dv,
                      import_country_id_dv,
                      expense_type_dv,
                      up_chrg_type_dv,
                      up_chrg_group_dv,
                      cvb_code_dv,
                      calc_basis_dv,
                      cost_basis_dv,
                      exp_category_dv,
                      comp_rate_dv,
                      comp_level_dv,
                      display_order_dv,
                      always_default_ind_dv,
                      comp_currency_dv,
                      per_count_dv,
                      per_count_uom_dv,
                      nom_flag_1_dv,
                      nom_flag_2_dv,
                      nom_flag_3_dv,
                      nom_flag_4_dv,
                      nom_flag_5_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key   = template_key
                          and wksht_key      = 'ELC_COMP' )
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ( 'COMP_ID'            as comp_id,
                                                                              'COMP_DESC'          as comp_desc,
                                                                              'COMP_TYPE'          as comp_type,
                                                                              'ASSESS_TYPE'        as assess_type,
                                                                              'IMPORT_COUNTRY_ID'  as import_country_id,
                                                                              'EXPENSE_TYPE'       as expense_type,
                                                                              'UP_CHRG_TYPE'       as up_chrg_type,
                                                                              'UP_CHRG_GROUP'      as up_chrg_group,
                                                                              'CVB_CODE'           as cvb_code,
                                                                              'CALC_BASIS'         as calc_basis,
                                                                              'COST_BASIS'         as cost_basis,
                                                                              'EXP_CATEGORY'       as exp_category,
                                                                              'COMP_RATE'          as comp_rate,
                                                                              'COMP_LEVEL'         as comp_level,
                                                                              'DISPLAY_ORDER'      as display_order,
                                                                              'ALWAYS_DEFAULT_IND' as always_default_ind,
                                                                              'COMP_CURRENCY'      as comp_currency,
                                                                              'PER_COUNT'          as per_count,
                                                                              'PER_COUNT_UOM'      as per_count_uom,
                                                                              'NOM_FLAG_1'         as nom_flag_1,
                                                                              'NOM_FLAG_2'         as nom_flag_2,
                                                                              'NOM_FLAG_3'         as nom_flag_3,
                                                                              'NOM_FLAG_4'         as nom_flag_4,
                                                                              'NOM_FLAG_5'         as nom_flag_5,
                                                                               NULL                as dummy)))
   LOOP
      BEGIN
         L_default_rec.comp_id := rec.comp_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'COMP_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.nom_flag_5 := rec.nom_flag_5_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'NOM_FLAG_5 ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.nom_flag_4 := rec.nom_flag_4_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'NOM_FLAG_4 ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.nom_flag_3 := rec.nom_flag_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'NOM_FLAG_3 ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.nom_flag_2 := rec.nom_flag_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'NOM_FLAG_2 ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.nom_flag_1 := rec.nom_flag_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'NOM_FLAG_1 ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.per_count_uom := rec.per_count_uom_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'PER_COUNT_UOM ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.per_count := rec.per_count_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'PER_COUNT ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_currency := rec.comp_currency_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'COMP_CURRENCY ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.always_default_ind := rec.always_default_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'ALWAYS_DEFAULT_IND ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.display_order := rec.display_order_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'DISPLAY_ORDER ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_level := rec.comp_level_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'COMP_LEVEL ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_rate := rec.comp_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'COMP_RATE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.exp_category := rec.exp_category_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'EXP_CATEGORY ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.cost_basis := rec.cost_basis_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'COST_BASIS ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.calc_basis := rec.calc_basis_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'CALC_BASIS ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.cvb_code := rec.cvb_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'CVB_CODE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.up_chrg_group := rec.up_chrg_group_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'UP_CHRG_GROUP ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.up_chrg_type := rec.up_chrg_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'UP_CHRG_TYPE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.expense_type := rec.expense_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'EXPENSE_TYPE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'IMPORT_COUNTRY_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.assess_type := rec.assess_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'ASSESS_TYPE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_type := rec.comp_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'COMP_TYPE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_desc := rec.comp_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'ELC_COMP ' ,
                             NULL,
                            'COMP_DESC ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
   (select UPPER(r.get_cell(elc_comp$action))             as action,
           UPPER(r.get_cell(elc_comp$comp_id))            as comp_id,
           r.get_cell(elc_comp$comp_desc)                 as comp_desc,
           r.get_cell(elc_comp$comp_type)                 as comp_type,
           r.get_cell(elc_comp$assess_type)               as assess_type,
           UPPER(r.get_cell(elc_comp$import_country_id))  as import_country_id,
           r.get_cell(elc_comp$expense_type)              as expense_type,
           r.get_cell(elc_comp$up_chrg_type)              as up_chrg_type,
           r.get_cell(elc_comp$up_chrg_group)             as up_chrg_group,
           UPPER(r.get_cell(elc_comp$cvb_code))           as cvb_code,
           r.get_cell(elc_comp$calc_basis)                as calc_basis,
           r.get_cell(elc_comp$cost_basis)                as cost_basis,
           r.get_cell(elc_comp$exp_category)              as exp_category,
           r.get_cell(elc_comp$comp_rate)                 as comp_rate,
           r.get_cell(elc_comp$comp_level)                as comp_level,
           r.get_cell(elc_comp$display_order)             as display_order,
           r.get_cell(elc_comp$always_default_ind)        as always_default_ind,
           UPPER(r.get_cell(elc_comp$comp_currency))      as comp_currency,
           r.get_cell(elc_comp$per_count)                 as per_count,
           UPPER(r.get_cell(elc_comp$per_count_uom))      as per_count_uom,
           r.get_cell(elc_comp$nom_flag_1)                as nom_flag_1,
           r.get_cell(elc_comp$nom_flag_2)                as nom_flag_2,
           r.get_cell(elc_comp$nom_flag_3)                as nom_flag_3,
           r.get_cell(elc_comp$nom_flag_4)                as nom_flag_4,
           r.get_cell(elc_comp$nom_flag_5)                as nom_flag_5,
           r.get_row_seq()                                as row_seq
      from s9t_folder sf,
           TABLE(sf.s9t_file_obj.sheets) ss,
           TABLE(ss.s9t_rows) r
     where sf.file_id     = I_file_id
       and ss.sheet_name  = GET_SHEET_NAME_TRANS(ELC_COMP_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_id := rec.comp_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'COMP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_5 := rec.nom_flag_5;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'NOM_FLAG_5',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_4 := rec.nom_flag_4;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'NOM_FLAG_4',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_3 := rec.nom_flag_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'NOM_FLAG_3',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_2 := rec.nom_flag_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'NOM_FLAG_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.nom_flag_1 := rec.nom_flag_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'NOM_FLAG_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.per_count_uom := rec.per_count_uom;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'PER_COUNT_UOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.per_count := rec.per_count;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'PER_COUNT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_currency := rec.comp_currency;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'COMP_CURRENCY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.always_default_ind := rec.always_default_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'ALWAYS_DEFAULT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.display_order := rec.display_order;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'DISPLAY_ORDER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_level := rec.comp_level;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'COMP_LEVEL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_rate := rec.comp_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'COMP_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.exp_category := rec.exp_category;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'EXP_CATEGORY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cost_basis := rec.cost_basis;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'COST_BASIS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.calc_basis := rec.calc_basis;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'CALC_BASIS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cvb_code := rec.cvb_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'CVB_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.up_chrg_group := rec.up_chrg_group;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'UP_CHRG_GROUP',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.up_chrg_type := rec.up_chrg_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'UP_CHRG_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.expense_type := rec.expense_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'EXPENSE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.assess_type := rec.assess_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'ASSESS_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_type := rec.comp_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'COMP_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_desc := rec.comp_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ELC_COMP_sheet,
                            rec.row_seq,
                            'COMP_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_COST_COMPONENT.action_new then
         L_temp_rec.comp_id             := NVL( L_temp_rec.comp_id     ,
                                                L_default_rec.comp_id);
         L_temp_rec.comp_desc           := NVL( L_temp_rec.comp_desc   ,
                                                L_default_rec.comp_desc);
         L_temp_rec.comp_type           := NVL( L_temp_rec.comp_type   ,
                                                L_default_rec.comp_type);
         L_temp_rec.assess_type         := NVL( L_temp_rec.assess_type ,
                                                L_default_rec.assess_type);
         L_temp_rec.import_country_id   := NVL( L_temp_rec.import_country_id,
                                                L_default_rec.import_country_id);
         L_temp_rec.expense_type        := NVL( L_temp_rec.expense_type ,
                                                L_default_rec.expense_type);
         L_temp_rec.up_chrg_type        := NVL( L_temp_rec.up_chrg_type ,
                                                L_default_rec.up_chrg_type);
         L_temp_rec.up_chrg_group       := NVL( L_temp_rec.up_chrg_group,
                                                L_default_rec.up_chrg_group);
         L_temp_rec.cvb_code            := NVL( L_temp_rec.cvb_code     ,
                                                L_default_rec.cvb_code);
         L_temp_rec.calc_basis          := NVL( L_temp_rec.calc_basis   ,
                                                L_default_rec.calc_basis);
         L_temp_rec.cost_basis          := NVL( L_temp_rec.cost_basis   ,
                                                L_default_rec.cost_basis);
         L_temp_rec.exp_category        := NVL( L_temp_rec.exp_category ,
                                                L_default_rec.exp_category);
         L_temp_rec.comp_rate           := NVL( L_temp_rec.comp_rate    ,
                                                L_default_rec.comp_rate);
         L_temp_rec.comp_level          := NVL( L_temp_rec.comp_level   ,
                                                L_default_rec.comp_level);
         L_temp_rec.display_order       := NVL( L_temp_rec.display_order,
                                                L_default_rec.display_order);
         L_temp_rec.always_default_ind  := NVL( L_temp_rec.always_default_ind,
                                                L_default_rec.always_default_ind);
         L_temp_rec.comp_currency       := NVL( L_temp_rec.comp_currency,
                                                L_default_rec.comp_currency);
         L_temp_rec.per_count           := NVL( L_temp_rec.per_count    ,
                                                L_default_rec.per_count);
         L_temp_rec.per_count_uom       := NVL( L_temp_rec.per_count_uom,
                                                L_default_rec.per_count_uom);
         L_temp_rec.nom_flag_1          := NVL( L_temp_rec.nom_flag_1   ,
                                                L_default_rec.nom_flag_1);
         L_temp_rec.nom_flag_2          := NVL( L_temp_rec.nom_flag_2   ,
                                                L_default_rec.nom_flag_2);
         L_temp_rec.nom_flag_3          := NVL( L_temp_rec.nom_flag_3   ,
                                                L_default_rec.nom_flag_3);
         L_temp_rec.nom_flag_4          := NVL( L_temp_rec.nom_flag_4   ,
                                                L_default_rec.nom_flag_4);
         L_temp_rec.nom_flag_5          := NVL( L_temp_rec.nom_flag_5   ,
                                                L_default_rec.nom_flag_5);
      end if;
      if NOT (L_temp_rec.comp_id is NOT NULL
              and 1 = 1)then
         WRITE_S9T_ERROR(I_file_id,
                         ELC_COMP_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
       svc_elc_comp_col.extend();
       svc_elc_comp_col(svc_elc_comp_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_elc_comp_col.COUNT SAVE EXCEPTIONS
      merge into svc_elc_comp st
      using(select(case
                   when L_mi_rec.comp_id_mi             = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.comp_id IS NULL
                   then mt.comp_id
                   else s1.comp_id
                   end) as comp_id,
                  (case
                   when L_mi_rec.comp_desc_mi           = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.comp_desc is null
                   then mt.comp_desc
                   else s1.comp_desc
                   end) as comp_desc,
                  (case
                   when L_mi_rec.comp_type_mi           = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.comp_type is null
                   then mt.comp_type
                   else s1.comp_type
                   end) as comp_type,
                  (case
                   when L_mi_rec.assess_type_mi         = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.assess_type is null
                   then mt.assess_type
                   else s1.assess_type
                   end) as assess_type,
                  (case
                   when L_mi_rec.import_country_id_mi   = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.import_country_id is null
                   then mt.import_country_id
                   else s1.import_country_id
                   end) as import_country_id,
                  (case
                   when L_mi_rec.expense_type_mi        = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.expense_type is null
                   then mt.expense_type
                   else s1.expense_type
                   end) as expense_type,
                  (case
                   when L_mi_rec.up_chrg_type_mi        = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.up_chrg_type is null
                   then mt.up_chrg_type
                   else s1.up_chrg_type
                   end) as up_chrg_type,
                  (case
                   when L_mi_rec.up_chrg_group_mi       = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.up_chrg_group is null
                   then mt.up_chrg_group
                   else s1.up_chrg_group
                   end) as up_chrg_group,
                  (case
                   when L_mi_rec.cvb_code_mi            = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.cvb_code is null
                   then mt.cvb_code
                   else s1.cvb_code
                   end) as cvb_code,
                  (case
                   when L_mi_rec.calc_basis_mi          = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.calc_basis is null
                   then mt.calc_basis
                   else s1.calc_basis
                   end) as calc_basis,
                  (case
                   when L_mi_rec.cost_basis_mi          = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.cost_basis is null
                   then mt.cost_basis
                   else s1.cost_basis
                   end) as cost_basis,
                  (case
                   when L_mi_rec.exp_category_mi        = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.exp_category is null
                   then mt.exp_category
                   else s1.exp_category
                   end) as exp_category,
                  (case
                   when L_mi_rec.comp_rate_mi           = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.comp_rate is null
                   then mt.comp_rate
                   else s1.comp_rate
                   end) as comp_rate,
                  (case
                   when L_mi_rec.comp_level_mi          = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.comp_level is null
                   then mt.comp_level
                   else s1.comp_level
                   end) as comp_level,
                  (case
                   when L_mi_rec.display_order_mi       = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.display_order is null
                   then mt.display_order
                   else s1.display_order
                   end) as display_order,
                  (case
                   when L_mi_rec.always_default_ind_mi  = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.always_default_ind IS NULL
                   then mt.always_default_ind
                   else s1.always_default_ind
                   end) as always_default_ind,
                  (case
                   when L_mi_rec.comp_currency_mi       = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.comp_currency IS NULL
                   then mt.comp_currency
                   else s1.comp_currency
                   end) as comp_currency,
                  (case
                   when L_mi_rec.per_count_mi           = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.per_count IS NULL
                   then mt.per_count
                   else s1.per_count
                   end) as per_count,
                  (case
                   when L_mi_rec.per_count_uom_mi       = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.per_count_uom IS NULL
                   then mt.per_count_uom
                   else s1.per_count_uom
                   end) as per_count_uom,
                  (case
                   when L_mi_rec.nom_flag_1_mi          = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.nom_flag_1 IS NULL
                   then mt.nom_flag_1
                   else s1.nom_flag_1
                   end) as nom_flag_1,
                  (case
                   when L_mi_rec.nom_flag_2_mi          = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.nom_flag_2 IS NULL
                   then mt.nom_flag_2
                   else s1.nom_flag_2
                   end) as nom_flag_2,
                  (case
                   when L_mi_rec.nom_flag_3_mi          = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.nom_flag_3 IS NULL
                   then mt.nom_flag_3
                   else s1.nom_flag_3
                   end) as nom_flag_3,
                  (case
                   when L_mi_rec.nom_flag_4_mi          = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.nom_flag_4 IS NULL
                   then mt.nom_flag_4
                   else s1.nom_flag_4
                   end) as nom_flag_4,
                  (case
                   when L_mi_rec.nom_flag_5_mi          = 'N'
                    and svc_elc_comp_col(i).action      = CORESVC_COST_COMPONENT.action_mod
                    and s1.nom_flag_5 IS NULL
                   then mt.nom_flag_5
                   else s1.nom_flag_5
                   end) as nom_flag_5,
                   null as dummy
              from(select svc_elc_comp_col(i).comp_id            as comp_id,
                          svc_elc_comp_col(i).comp_desc          as comp_desc,
                          svc_elc_comp_col(i).comp_type          as comp_type,
                          svc_elc_comp_col(i).assess_type        as assess_type,
                          svc_elc_comp_col(i).import_country_id  as import_country_id,
                          svc_elc_comp_col(i).expense_type       as expense_type,
                          svc_elc_comp_col(i).up_chrg_type       as up_chrg_type,
                          svc_elc_comp_col(i).up_chrg_group      as up_chrg_group,
                          svc_elc_comp_col(i).cvb_code           as cvb_code,
                          svc_elc_comp_col(i).calc_basis         as calc_basis,
                          svc_elc_comp_col(i).cost_basis         as cost_basis,
                          svc_elc_comp_col(i).exp_category       as exp_category,
                          svc_elc_comp_col(i).comp_rate          as comp_rate,
                          svc_elc_comp_col(i).comp_level         as comp_level,
                          svc_elc_comp_col(i).display_order      as display_order,
                          svc_elc_comp_col(i).always_default_ind as always_default_ind,
                          svc_elc_comp_col(i).comp_currency      as comp_currency,
                          svc_elc_comp_col(i).per_count          as per_count,
                          svc_elc_comp_col(i).per_count_uom      as per_count_uom,
                          svc_elc_comp_col(i).nom_flag_1         as nom_flag_1,
                          svc_elc_comp_col(i).nom_flag_2         as nom_flag_2,
                          svc_elc_comp_col(i).nom_flag_3         as nom_flag_3,
                          svc_elc_comp_col(i).nom_flag_4         as nom_flag_4,
                          svc_elc_comp_col(i).nom_flag_5         as nom_flag_5,
                          null as dummy
                     from dual )s1,
                                elc_comp mt
                    where mt.comp_id (+)                = s1.comp_id
                      and 1 = 1 )sq on (st.comp_id      = sq.comp_id
                                         and svc_elc_comp_col(i).action IN (CORESVC_COST_COMPONENT.action_mod,
                                                                            CORESVC_COST_COMPONENT.action_del)
                                        )
      when matched then
         update
            set process_id                = svc_elc_comp_col(i).process_id ,
                chunk_id                  = svc_elc_comp_col(i).chunk_id ,
                row_seq                   = svc_elc_comp_col(i).row_seq ,
                action                    = svc_elc_comp_col(i).action ,
                process$status            = svc_elc_comp_col(i).process$status ,
                comp_desc                 = sq.comp_desc ,
                comp_type                 = sq.comp_type ,
                assess_type               = sq.assess_type ,
                import_country_id         = sq.import_country_id ,
                expense_type              = sq.expense_type ,
                up_chrg_type              = sq.up_chrg_type ,
                up_chrg_group             = sq.up_chrg_group ,
                cvb_code                  = sq.cvb_code ,
                calc_basis                = sq.calc_basis ,
                cost_basis                = sq.cost_basis ,
                exp_category              = sq.exp_category ,
                comp_rate                 = sq.comp_rate ,
                comp_level                = sq.comp_level ,
                display_order             = sq.display_order ,
                always_default_ind        = sq.always_default_ind ,
                comp_currency             = sq.comp_currency ,
                per_count                 = sq.per_count ,
                per_count_uom             = sq.per_count_uom ,
                nom_flag_1                = sq.nom_flag_1 ,
                nom_flag_2                = sq.nom_flag_2 ,
                nom_flag_3                = sq.nom_flag_3 ,
                nom_flag_4                = sq.nom_flag_4 ,
                nom_flag_5                = sq.nom_flag_5 ,
                create_id                 = svc_elc_comp_col(i).create_id ,
                create_datetime           = svc_elc_comp_col(i).create_datetime ,
                last_upd_id               = svc_elc_comp_col(i).last_upd_id ,
                last_upd_datetime         = svc_elc_comp_col(i).last_upd_datetime
      when NOT matched then
         insert(process_id,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                comp_id ,
                comp_desc ,
                comp_type ,
                assess_type ,
                import_country_id ,
                expense_type ,
                up_chrg_type ,
                up_chrg_group ,
                cvb_code ,
                calc_basis ,
                cost_basis ,
                exp_category ,
                comp_rate ,
                comp_level ,
                display_order ,
                always_default_ind ,
                comp_currency ,
                per_count ,
                per_count_uom ,
                nom_flag_1 ,
                nom_flag_2 ,
                nom_flag_3 ,
                nom_flag_4 ,
                nom_flag_5 ,
                create_id ,
                create_datetime ,
                last_upd_id ,
                last_upd_datetime)
         values(svc_elc_comp_col(i).process_id ,
                svc_elc_comp_col(i).chunk_id ,
                svc_elc_comp_col(i).row_seq ,
                svc_elc_comp_col(i).action ,
                svc_elc_comp_col(i).process$status ,
                sq.comp_id ,
                sq.comp_desc ,
                sq.comp_type ,
                sq.assess_type ,
                sq.import_country_id ,
                sq.expense_type ,
                sq.up_chrg_type ,
                sq.up_chrg_group ,
                sq.cvb_code ,
                sq.calc_basis ,
                sq.cost_basis ,
                sq.exp_category ,
                sq.comp_rate ,
                sq.comp_level ,
                sq.display_order ,
                sq.always_default_ind ,
                sq.comp_currency ,
                sq.per_count ,
                sq.per_count_uom ,
                sq.nom_flag_1 ,
                sq.nom_flag_2 ,
                sq.nom_flag_3 ,
                sq.nom_flag_4 ,
                sq.nom_flag_5 ,
                svc_elc_comp_col(i).create_id ,
                svc_elc_comp_col(i).create_datetime ,
                svc_elc_comp_col(i).last_upd_id ,
                svc_elc_comp_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             ELC_COMP_sheet,
                             svc_elc_comp_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_ELC_COMP;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_ELC_COMP_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_ELC_COMP_TL.PROCESS_ID%TYPE) IS

   TYPE svc_elc_comp_tl_col_TYP IS TABLE OF SVC_ELC_COMP_TL%ROWTYPE;
   L_temp_rec            SVC_ELC_COMP_TL%ROWTYPE;
   svc_elc_comp_tl_col   svc_elc_comp_tl_col_TYP := NEW svc_elc_comp_tl_col_TYP();
   L_process_id          SVC_ELC_COMP_TL.PROCESS_ID%TYPE;
   L_error               BOOLEAN := FALSE;
   L_default_rec         SVC_ELC_COMP_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select comp_desc_mi,
             comp_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'ELC_COMP_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('COMP_DESC' as comp_desc,
                                       'COMP_ID'   as comp_id,
                                       'LANG'      as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_ELC_COMP_TL';
   L_pk_columns    VARCHAR2(255)  := 'Component, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select comp_desc_dv,
                      comp_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'ELC_COMP_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('COMP_DESC' as comp_desc,
                                                'COMP_ID'   as comp_id,
                                                'LANG'      as lang)))
   LOOP
      BEGIN
         L_default_rec.comp_desc := rec.comp_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            elc_comp_TL_sheet ,
                            NULL,
                           'COMP_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.comp_id := rec.comp_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           elc_comp_TL_sheet ,
                            NULL,
                           'COMP_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            elc_comp_TL_sheet ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(elc_comp_TL$ACTION))       as action,
                      r.get_cell(elc_comp_TL$comp_desc)           as comp_desc,
                      UPPER(r.get_cell(elc_comp_TL$comp_id))      as comp_id,
                      r.get_cell(elc_comp_TL$LANG)                as lang,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(elc_comp_TL_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            elc_comp_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_desc := rec.comp_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            elc_comp_TL_sheet,
                            rec.row_seq,
                            'COMP_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comp_id := rec.comp_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            elc_comp_TL_sheet,
                            rec.row_seq,
                            'COMP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            elc_comp_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_COST_COMPONENT.action_new then
         L_temp_rec.comp_desc := NVL( L_temp_rec.comp_desc,L_default_rec.comp_desc);
         L_temp_rec.comp_id   := NVL( L_temp_rec.comp_id,L_default_rec.comp_id);
         L_temp_rec.lang      := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.comp_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         ELC_COMP_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_elc_comp_tl_col.extend();
         svc_elc_comp_tl_col(svc_elc_comp_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_elc_comp_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_ELC_COMP_TL st
      using(select
                  (case
                   when l_mi_rec.comp_desc_mi = 'N'
                    and svc_elc_comp_tl_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.comp_desc IS NULL then
                        mt.comp_desc
                   else s1.comp_desc
                   end) as comp_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_elc_comp_tl_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.comp_id_mi = 'N'
                    and svc_elc_comp_tl_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.comp_id IS NULL then
                        mt.comp_id
                   else s1.comp_id
                   end) as comp_id
              from (select svc_elc_comp_tl_col(i).comp_desc as comp_desc,
                           svc_elc_comp_tl_col(i).comp_id   as comp_id,
                           svc_elc_comp_tl_col(i).lang      as lang
                      from dual) s1,
                   elc_comp_tl mt
             where mt.comp_id (+) = s1.comp_id
               and mt.lang (+)    = s1.lang) sq
                on (st.comp_id = sq.comp_id and
                    st.lang    = sq.lang and
                    svc_elc_comp_tl_col(i).ACTION IN (CORESVC_COST_COMPONENT.action_mod,CORESVC_COST_COMPONENT.action_del))
      when matched then
      update
         set process_id        = svc_elc_comp_tl_col(i).process_id ,
             chunk_id          = svc_elc_comp_tl_col(i).chunk_id ,
             row_seq           = svc_elc_comp_tl_col(i).row_seq ,
             action            = svc_elc_comp_tl_col(i).action ,
             process$status    = svc_elc_comp_tl_col(i).process$status ,
             comp_desc         = sq.comp_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             comp_desc ,
             comp_id ,
             lang)
      values(svc_elc_comp_tl_col(i).process_id ,
             svc_elc_comp_tl_col(i).chunk_id ,
             svc_elc_comp_tl_col(i).row_seq ,
             svc_elc_comp_tl_col(i).action ,
             svc_elc_comp_tl_col(i).process$status ,
             sq.comp_desc ,
             sq.comp_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            ELC_COMP_TL_SHEET,
                            svc_elc_comp_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_ELC_COMP_TL;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COST_COMP_UPD_STG( I_file_id      IN   s9t_folder.file_id%TYPE,
                                         I_process_id   IN   SVC_COST_COMP_UPD_STG.process_id%TYPE) IS
   TYPE svc_COST_COMP_UPD_STG_col_typ IS TABLE OF SVC_COST_COMP_UPD_STG%ROWTYPE;
   L_temp_rec                  SVC_COST_COMP_UPD_STG%ROWTYPE;
   svc_COST_COMP_UPD_STG_col   svc_COST_COMP_UPD_STG_col_typ := NEW svc_COST_COMP_UPD_STG_col_typ();
   L_process_id                SVC_COST_COMP_UPD_STG.process_id%TYPE;
   L_error                     BOOLEAN := FALSE;
   L_default_rec               SVC_COST_COMP_UPD_STG%ROWTYPE;
   cursor C_MANDATORY_IND is
      select EFFECTIVE_DATE_mi,
             PTNR_DEFAULT_IND_mi,
             SUPP_DEFAULT_IND_mi,
             CNTRY_DEFAULT_IND_mi,
             NEW_PER_COUNT_UOM_mi,
             NEW_PER_COUNT_mi,
             NEW_COMP_CURRENCY_mi,
             NEW_COMP_RATE_mi,
             EXPENSE_TYPE_mi,
             COMP_TYPE_mi,
             COMP_ID_mi,
             DEPT_DEFAULT_IND_mi,
             TSF_ALLOC_DEFAULT_IND_mi,
             ORDER_DEFAULT_IND_mi,
             ITEM_DEFAULT_IND_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'COST_COMP_UPD_STG') 
               PIVOT (MAX(mandatory) as mi
                 FOR (column_key) IN ('EFFECTIVE_DATE'        as EFFECTIVE_DATE,
                                      'PTNR_DEFAULT_IND'      as PTNR_DEFAULT_IND,
                                      'SUPP_DEFAULT_IND'      as SUPP_DEFAULT_IND,
                                      'CNTRY_DEFAULT_IND'     as CNTRY_DEFAULT_IND,
                                      'NEW_PER_COUNT_UOM'     as NEW_PER_COUNT_UOM,
                                      'NEW_PER_COUNT'         as NEW_PER_COUNT,
                                      'NEW_COMP_CURRENCY'     as NEW_COMP_CURRENCY,
                                      'NEW_COMP_RATE'         as NEW_COMP_RATE,
                                      'EXPENSE_TYPE'          as EXPENSE_TYPE,
                                      'COMP_TYPE'             as COMP_TYPE,
                                      'COMP_ID'               as COMP_ID,
                                      'DEPT_DEFAULT_IND'      as DEPT_DEFAULT_IND,
                                      'TSF_ALLOC_DEFAULT_IND' as TSF_ALLOC_DEFAULT_IND,
                                      'ORDER_DEFAULT_IND'     as ORDER_DEFAULT_IND,
                                      'ITEM_DEFAULT_IND'      as ITEM_DEFAULT_IND,
                                       null                   as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COST_COMP_UPD_STG';
   L_pk_columns    VARCHAR2(255)  := 'Component,Effective Date';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;   
BEGIN
  -- Get default values.
   FOR rec IN (select EFFECTIVE_DATE_dv,
                      PTNR_DEFAULT_IND_dv,
                      SUPP_DEFAULT_IND_dv,
                      CNTRY_DEFAULT_IND_dv,
                      NEW_PER_COUNT_UOM_dv,
                      NEW_PER_COUNT_dv,
                      NEW_COMP_CURRENCY_dv,
                      NEW_COMP_RATE_dv,
                      EXPENSE_TYPE_dv,
                      COMP_TYPE_dv,
                      COMP_ID_dv,
                      DEPT_DEFAULT_IND_dv,
                      TSF_ALLOC_DEFAULT_IND_dv,
                      ORDER_DEFAULT_IND_dv,
                      ITEM_DEFAULT_IND_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'COST_COMP_UPD_STG') 
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ( 'EFFECTIVE_DATE'        as EFFECTIVE_DATE,
                                                 'PTNR_DEFAULT_IND'      as PTNR_DEFAULT_IND,
                                                 'SUPP_DEFAULT_IND'      as SUPP_DEFAULT_IND,
                                                 'CNTRY_DEFAULT_IND'     as CNTRY_DEFAULT_IND,
                                                 'NEW_PER_COUNT_UOM'     as NEW_PER_COUNT_UOM,
                                                 'NEW_PER_COUNT'         as NEW_PER_COUNT,
                                                 'NEW_COMP_CURRENCY'     as NEW_COMP_CURRENCY,
                                                 'NEW_COMP_RATE'         as NEW_COMP_RATE,
                                                 'EXPENSE_TYPE'          as EXPENSE_TYPE,
                                                 'COMP_TYPE'             as COMP_TYPE,
                                                 'COMP_ID'               as COMP_ID,
                                                 'DEPT_DEFAULT_IND'      as DEPT_DEFAULT_IND,
                                                 'TSF_ALLOC_DEFAULT_IND' as TSF_ALLOC_DEFAULT_IND,
                                                 'ORDER_DEFAULT_IND'     as ORDER_DEFAULT_IND,
                                                 'ITEM_DEFAULT_IND'      as ITEM_DEFAULT_IND,
                                                  NULL                   as dummy)))
   LOOP
      BEGIN
         L_default_rec.EFFECTIVE_DATE := rec.EFFECTIVE_DATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'EFFECTIVE_DATE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.PTNR_DEFAULT_IND := rec.PTNR_DEFAULT_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'PTNR_DEFAULT_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SUPP_DEFAULT_IND := rec.SUPP_DEFAULT_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'SUPP_DEFAULT_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CNTRY_DEFAULT_IND := rec.CNTRY_DEFAULT_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'CNTRY_DEFAULT_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.NEW_PER_COUNT_UOM := rec.NEW_PER_COUNT_UOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'NEW_PER_COUNT_UOM ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.NEW_PER_COUNT := rec.NEW_PER_COUNT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'NEW_PER_COUNT ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.NEW_COMP_CURRENCY := rec.NEW_COMP_CURRENCY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'NEW_COMP_CURRENCY ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.NEW_COMP_RATE := rec.NEW_COMP_RATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'NEW_COMP_RATE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.EXPENSE_TYPE := rec.EXPENSE_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'EXPENSE_TYPE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COMP_TYPE := rec.COMP_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'COMP_TYPE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COMP_ID := rec.COMP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'COMP_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DEPT_DEFAULT_IND := rec.DEPT_DEFAULT_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'DEPT_DEFAULT_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TSF_ALLOC_DEFAULT_IND := rec.TSF_ALLOC_DEFAULT_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'TSF_ALLOC_DEFAULT_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ORDER_DEFAULT_IND := rec.ORDER_DEFAULT_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'ORDER_DEFAULT_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ITEM_DEFAULT_IND := rec.ITEM_DEFAULT_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COST_COMP_UPD_STG ' ,
                            NULL,
                           'ITEM_DEFAULT_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(CCUS$Action)                        as Action,
          r.get_cell(CCUS$EFFECTIVE_DATE)                as EFFECTIVE_DATE,
          r.get_cell(CCUS$EFFECTIVE_DATE_UPD)            as EFFECTIVE_DATE_UPD,
          r.get_cell(CCUS$PTNR_DEFAULT_IND)              as PTNR_DEFAULT_IND,
          r.get_cell(CCUS$SUPP_DEFAULT_IND)              as SUPP_DEFAULT_IND,
          r.get_cell(CCUS$CNTRY_DEFAULT_IND)             as CNTRY_DEFAULT_IND,
          UPPER(r.get_cell(CCUS$NEW_PER_COUNT_UOM))      as NEW_PER_COUNT_UOM,
          r.get_cell(CCUS$NEW_PER_COUNT)                 as NEW_PER_COUNT,
          UPPER(r.get_cell(CCUS$NEW_COMP_CURRENCY))      as NEW_COMP_CURRENCY,
          r.get_cell(CCUS$NEW_COMP_RATE)                 as NEW_COMP_RATE,
          r.get_cell(CCUS$EXPENSE_TYPE)                  as EXPENSE_TYPE,
          r.get_cell(CCUS$COMP_TYPE)                     as COMP_TYPE,
          r.get_cell(CCUS$COMP_ID)                       as COMP_ID,
          r.get_cell(CCUS$DEPT_DEFAULT_IND)              as DEPT_DEFAULT_IND,
          r.get_cell(CCUS$TSF_ALLOC_DEFAULT_IND)         as TSF_ALLOC_DEFAULT_IND,
          r.get_cell(CCUS$ORDER_DEFAULT_IND)             as ORDER_DEFAULT_IND,
          r.get_cell(CCUS$ITEM_DEFAULT_IND)              as ITEM_DEFAULT_IND,
          r.get_row_seq()                                as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
    where sf.file_id  = I_file_id
      and ss.sheet_name = sheet_name_trans(COST_COMP_UPD_STG_sheet))
   LOOP
      L_temp_rec                   := NULL;      
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.EFFECTIVE_DATE := rec.EFFECTIVE_DATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'EFFECTIVE_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;      
      BEGIN
         L_temp_rec.EFFECTIVE_DATE_UPD := rec.EFFECTIVE_DATE_UPD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'EFFECTIVE_DATE_UPD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;    
      BEGIN
         L_temp_rec.PTNR_DEFAULT_IND := rec.PTNR_DEFAULT_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'PTNR_DEFAULT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SUPP_DEFAULT_IND := rec.SUPP_DEFAULT_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'SUPP_DEFAULT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CNTRY_DEFAULT_IND := rec.CNTRY_DEFAULT_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'CNTRY_DEFAULT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NEW_PER_COUNT_UOM := rec.NEW_PER_COUNT_UOM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'NEW_PER_COUNT_UOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NEW_PER_COUNT := rec.NEW_PER_COUNT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'NEW_PER_COUNT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NEW_COMP_CURRENCY := rec.NEW_COMP_CURRENCY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'NEW_COMP_CURRENCY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NEW_COMP_RATE := rec.NEW_COMP_RATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'NEW_COMP_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.EXPENSE_TYPE := rec.EXPENSE_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'EXPENSE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COMP_TYPE := rec.COMP_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'COMP_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COMP_ID := rec.COMP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'COMP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEPT_DEFAULT_IND := rec.DEPT_DEFAULT_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'DEPT_DEFAULT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TSF_ALLOC_DEFAULT_IND := rec.TSF_ALLOC_DEFAULT_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'TSF_ALLOC_DEFAULT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ORDER_DEFAULT_IND := rec.ORDER_DEFAULT_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'ORDER_DEFAULT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ITEM_DEFAULT_IND := rec.ITEM_DEFAULT_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COST_COMP_UPD_STG_sheet,
                            rec.row_seq,
                            'ITEM_DEFAULT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_COST_COMPONENT.action_new then
         L_temp_rec.EFFECTIVE_DATE        := NVL( L_temp_rec.EFFECTIVE_DATE,L_default_rec.EFFECTIVE_DATE);
         L_temp_rec.PTNR_DEFAULT_IND      := NVL( L_temp_rec.PTNR_DEFAULT_IND,L_default_rec.PTNR_DEFAULT_IND);
         L_temp_rec.SUPP_DEFAULT_IND      := NVL( L_temp_rec.SUPP_DEFAULT_IND,L_default_rec.SUPP_DEFAULT_IND);
         L_temp_rec.CNTRY_DEFAULT_IND     := NVL( L_temp_rec.CNTRY_DEFAULT_IND,L_default_rec.CNTRY_DEFAULT_IND);
         L_temp_rec.NEW_PER_COUNT_UOM     := NVL( L_temp_rec.NEW_PER_COUNT_UOM,L_default_rec.NEW_PER_COUNT_UOM);
         L_temp_rec.NEW_PER_COUNT         := NVL( L_temp_rec.NEW_PER_COUNT,L_default_rec.NEW_PER_COUNT);
         L_temp_rec.NEW_COMP_CURRENCY     := NVL( L_temp_rec.NEW_COMP_CURRENCY,L_default_rec.NEW_COMP_CURRENCY);
         L_temp_rec.NEW_COMP_RATE         := NVL( L_temp_rec.NEW_COMP_RATE,L_default_rec.NEW_COMP_RATE);
         L_temp_rec.EXPENSE_TYPE          := NVL( L_temp_rec.EXPENSE_TYPE,L_default_rec.EXPENSE_TYPE);
         L_temp_rec.COMP_TYPE             := NVL( L_temp_rec.COMP_TYPE,L_default_rec.COMP_TYPE);
         L_temp_rec.COMP_ID               := NVL( L_temp_rec.COMP_ID,L_default_rec.COMP_ID);
         L_temp_rec.DEPT_DEFAULT_IND      := NVL( L_temp_rec.DEPT_DEFAULT_IND,L_default_rec.DEPT_DEFAULT_IND);
         L_temp_rec.TSF_ALLOC_DEFAULT_IND := NVL( L_temp_rec.TSF_ALLOC_DEFAULT_IND,L_default_rec.TSF_ALLOC_DEFAULT_IND);
         L_temp_rec.ORDER_DEFAULT_IND     := NVL( L_temp_rec.ORDER_DEFAULT_IND,L_default_rec.ORDER_DEFAULT_IND);
         L_temp_rec.ITEM_DEFAULT_IND      := NVL( L_temp_rec.ITEM_DEFAULT_IND,L_default_rec.ITEM_DEFAULT_IND);
      end if;
      if not (L_temp_rec.COMP_ID is NOT NULL and
              L_temp_rec.EFFECTIVE_DATE is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         COST_COMP_UPD_STG_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_COST_COMP_UPD_STG_col.extend();
         svc_COST_COMP_UPD_STG_col(svc_COST_COMP_UPD_STG_col.COUNT()):=l_temp_rec;
      end if;
      
   END LOOP;
   BEGIN
      forall i IN 1..svc_COST_COMP_UPD_STG_col.COUNT SAVE EXCEPTIONS
      merge into SVC_COST_COMP_UPD_STG st
      using(select
                  (case
                   when l_mi_rec.EFFECTIVE_DATE_mi = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.EFFECTIVE_DATE IS NULL
                   then mt.EFFECTIVE_DATE
                   else s1.EFFECTIVE_DATE
                   end) as EFFECTIVE_DATE,
                  (case
                   when l_mi_rec.PTNR_DEFAULT_IND_mi = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.PTNR_DEFAULT_IND IS NULL
                   then mt.PTNR_DEFAULT_IND
                   else s1.PTNR_DEFAULT_IND
                   end) as PTNR_DEFAULT_IND,
                  (case
                   when l_mi_rec.SUPP_DEFAULT_IND_mi = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.SUPP_DEFAULT_IND IS NULL
                   then mt.SUPP_DEFAULT_IND
                   else s1.SUPP_DEFAULT_IND
                   end) as SUPP_DEFAULT_IND,
                  (case
                   when l_mi_rec.CNTRY_DEFAULT_IND_mi = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.CNTRY_DEFAULT_IND IS NULL
                   then mt.CNTRY_DEFAULT_IND
                   else s1.CNTRY_DEFAULT_IND
                   end) as CNTRY_DEFAULT_IND,
                  (case
                   when l_mi_rec.NEW_PER_COUNT_UOM_mi = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.NEW_PER_COUNT_UOM IS NULL
                   then mt.NEW_PER_COUNT_UOM
                   else s1.NEW_PER_COUNT_UOM
                   end) as NEW_PER_COUNT_UOM,
                  (case
                   when l_mi_rec.NEW_PER_COUNT_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.NEW_PER_COUNT IS NULL
                   then mt.NEW_PER_COUNT
                   else s1.NEW_PER_COUNT
                   end) as NEW_PER_COUNT,
                  (case
                   when l_mi_rec.NEW_COMP_CURRENCY_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.NEW_COMP_CURRENCY IS NULL
                   then mt.NEW_COMP_CURRENCY
                   else s1.NEW_COMP_CURRENCY
                   end) as NEW_COMP_CURRENCY,
                  (case
                   when l_mi_rec.NEW_COMP_RATE_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.NEW_COMP_RATE IS NULL
                   then mt.NEW_COMP_RATE
                   else s1.NEW_COMP_RATE
                   end) as NEW_COMP_RATE,
                  (case
                   when l_mi_rec.EXPENSE_TYPE_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.EXPENSE_TYPE IS NULL
                   then mt.EXPENSE_TYPE
                   else s1.EXPENSE_TYPE
                   end) as EXPENSE_TYPE,
                  (case
                   when l_mi_rec.COMP_TYPE_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.COMP_TYPE IS NULL
                   then mt.COMP_TYPE
                   else s1.COMP_TYPE
                   end) as COMP_TYPE,
                  (case
                   when l_mi_rec.COMP_ID_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.COMP_ID IS NULL
                   then mt.COMP_ID
                   else s1.COMP_ID
                   end) as COMP_ID,
                  (case
                   when l_mi_rec.DEPT_DEFAULT_IND_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.DEPT_DEFAULT_IND IS NULL
                   then mt.DEPT_DEFAULT_IND
                   else s1.DEPT_DEFAULT_IND
                   end) as DEPT_DEFAULT_IND,
                  (case
                   when l_mi_rec.TSF_ALLOC_DEFAULT_IND_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.TSF_ALLOC_DEFAULT_IND IS NULL
                   then mt.TSF_ALLOC_DEFAULT_IND
                   else s1.TSF_ALLOC_DEFAULT_IND
                   end) as TSF_ALLOC_DEFAULT_IND,
                  (case
                   when l_mi_rec.ORDER_DEFAULT_IND_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.ORDER_DEFAULT_IND IS NULL
                   then mt.ORDER_DEFAULT_IND
                   else s1.ORDER_DEFAULT_IND
                   end) as ORDER_DEFAULT_IND,
                  (case
                   when l_mi_rec.ITEM_DEFAULT_IND_mi    = 'N'
                    and svc_COST_COMP_UPD_STG_col(i).action = CORESVC_COST_COMPONENT.action_mod
                    and s1.ITEM_DEFAULT_IND IS NULL
                   then mt.ITEM_DEFAULT_IND
                   else s1.ITEM_DEFAULT_IND
                   end) as ITEM_DEFAULT_IND,
                  null as dummy
              from (select
                          svc_COST_COMP_UPD_STG_col(i).EFFECTIVE_DATE        as EFFECTIVE_DATE,
                          svc_COST_COMP_UPD_STG_col(i).PTNR_DEFAULT_IND      as PTNR_DEFAULT_IND,
                          svc_COST_COMP_UPD_STG_col(i).SUPP_DEFAULT_IND      as SUPP_DEFAULT_IND,
                          svc_COST_COMP_UPD_STG_col(i).CNTRY_DEFAULT_IND     as CNTRY_DEFAULT_IND,
                          svc_COST_COMP_UPD_STG_col(i).NEW_PER_COUNT_UOM     as NEW_PER_COUNT_UOM,
                          svc_COST_COMP_UPD_STG_col(i).NEW_PER_COUNT         as NEW_PER_COUNT,
                          svc_COST_COMP_UPD_STG_col(i).NEW_COMP_CURRENCY     as NEW_COMP_CURRENCY,
                          svc_COST_COMP_UPD_STG_col(i).NEW_COMP_RATE         as NEW_COMP_RATE,
                          svc_COST_COMP_UPD_STG_col(i).EXPENSE_TYPE          as EXPENSE_TYPE,
                          svc_COST_COMP_UPD_STG_col(i).COMP_TYPE             as COMP_TYPE,
                          svc_COST_COMP_UPD_STG_col(i).COMP_ID               as COMP_ID,
                          svc_COST_COMP_UPD_STG_col(i).DEPT_DEFAULT_IND      as DEPT_DEFAULT_IND,
                          svc_COST_COMP_UPD_STG_col(i).TSF_ALLOC_DEFAULT_IND as TSF_ALLOC_DEFAULT_IND,
                          svc_COST_COMP_UPD_STG_col(i).ORDER_DEFAULT_IND     as ORDER_DEFAULT_IND,
                          svc_COST_COMP_UPD_STG_col(i).ITEM_DEFAULT_IND      as ITEM_DEFAULT_IND,
                          null as dummy
                      from dual ) s1,
                    COST_COMP_UPD_STG mt
              where mt.COMP_ID (+)        = s1.COMP_ID
                and mt.EFFECTIVE_DATE (+) = s1.EFFECTIVE_DATE
                and 1 = 1 )sq
                on (st.COMP_ID         = sq.COMP_ID and
                    st.EFFECTIVE_DATE  = sq.EFFECTIVE_DATE and
                    svc_COST_COMP_UPD_STG_col(i).ACTION IN (CORESVC_COST_COMPONENT.action_mod,CORESVC_COST_COMPONENT.action_del))
      when matched then
      update
         set process_id            = svc_COST_COMP_UPD_STG_col(i).process_id ,
             chunk_id              = svc_COST_COMP_UPD_STG_col(i).chunk_id ,
             row_seq               = svc_COST_COMP_UPD_STG_col(i).row_seq ,
             action                = svc_COST_COMP_UPD_STG_col(i).action ,
             process$status        = svc_COST_COMP_UPD_STG_col(i).process$status ,
             effective_date_upd    = svc_COST_COMP_UPD_STG_col(i).effective_date_upd ,
             item_default_ind      = sq.item_default_ind ,
             supp_default_ind      = sq.supp_default_ind ,
             order_default_ind     = sq.order_default_ind ,
             dept_default_ind      = sq.dept_default_ind ,
             ptnr_default_ind      = sq.ptnr_default_ind ,
             new_comp_rate         = sq.new_comp_rate ,
             comp_type             = sq.comp_type ,
             new_per_count         = sq.new_per_count ,
             expense_type          = sq.expense_type ,
             tsf_alloc_default_ind = sq.tsf_alloc_default_ind ,
             cntry_default_ind     = sq.cntry_default_ind ,
             new_comp_currency     = sq.new_comp_currency ,
             new_per_count_uom     = sq.new_per_count_uom ,
             create_id             = svc_COST_COMP_UPD_STG_col(i).create_id ,
             create_datetime       = svc_COST_COMP_UPD_STG_col(i).create_datetime ,
             last_upd_id           = svc_COST_COMP_UPD_STG_col(i).last_upd_id ,
             last_upd_datetime     = svc_COST_COMP_UPD_STG_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             effective_date,
             effective_date_upd,
             ptnr_default_ind ,
             supp_default_ind ,
             cntry_default_ind ,
             new_per_count_uom ,
             new_per_count ,
             new_comp_currency ,
             new_comp_rate ,
             expense_type ,
             comp_type ,
             comp_id ,
             dept_default_ind ,
             tsf_alloc_default_ind ,
             order_default_ind ,
             item_default_ind ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_COST_COMP_UPD_STG_col(i).process_id ,
             svc_COST_COMP_UPD_STG_col(i).chunk_id ,
             svc_COST_COMP_UPD_STG_col(i).row_seq ,
             svc_COST_COMP_UPD_STG_col(i).action ,
             svc_COST_COMP_UPD_STG_col(i).process$status ,
             sq.effective_date,
             svc_COST_COMP_UPD_STG_col(i).effective_date_upd,
             sq.ptnr_default_ind ,
             sq.supp_default_ind ,
             sq.cntry_default_ind ,
             sq.new_per_count_uom ,
             sq.new_per_count ,
             sq.new_comp_currency ,
             sq.new_comp_rate ,
             sq.expense_type ,
             sq.comp_type ,
             sq.comp_id ,
             sq.dept_default_ind ,
             sq.tsf_alloc_default_ind ,
             sq.order_default_ind ,
             sq.item_default_ind ,
             svc_COST_COMP_UPD_STG_col(i).create_id ,
             svc_COST_COMP_UPD_STG_col(i).create_datetime ,
             svc_COST_COMP_UPD_STG_col(i).last_upd_id ,
             svc_COST_COMP_UPD_STG_col(i).last_upd_datetime );
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          COST_COMP_UPD_STG_sheet,
                          svc_COST_COMP_UPD_STG_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);                            
      END LOOP;
END;
END PROCESS_S9T_COST_COMP_UPD_STG;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count   IN OUT NUMBER,
                      I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64):='CORESVC_COST_COMPONENT.PROCESS_S9T';
   L_file            S9T_FILE;
   L_sheets          S9T_PKG.NAMES_MAP_TYP;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR      EXCEPTION;
   PRAGMA        EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_ELC_COMP(I_file_id,I_process_id);
      PROCESS_S9T_ELC_COMP_TL(I_file_id,I_process_id);
      PROCESS_S9T_COST_COMP_UPD_STG(I_file_id,I_process_id);
   end if;
   ---
   if CORESVC_CFLEX.PROCESS_S9T_SHEETS_ADMINAPI(O_error_message,
                                                I_file_id,
                                                I_process_id,
                                                template_key) = FALSE   then
      return FALSE;
   end if;
   ---
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab        := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status      := 'PS';
   else
      L_process_status      := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message   := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                               NULL,
                                               NULL,
                                               NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.COUNT();
      forall i IN 1..O_error_count
         insert into s9t_errors
            values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count

      insert into s9t_errors
      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status   = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;

      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_ELC_COMP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error          IN OUT  BOOLEAN,
                              I_rec            IN OUT  C_SVC_ELC_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      := 'CORESVC_COST_COMPONENT.PROCESS_ELC_COMP_VAL';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_ELC_COMP';
   L_exists            BOOLEAN                           := TRUE;
   L_import_country_id ELC_COMP.IMPORT_COUNTRY_ID%TYPE;
   L_comp_currency     ELC_COMP.COMP_CURRENCY%TYPE;
   L_code_exists    VARCHAR(1);
   L_uchg_exists    VARCHAR2(1) := 'N';

   cursor C_DETAIL_EXISTS is
      select 'x'
        from code_detail
       where code_detail.code_type = 'EXPC'
         and code_detail.code = I_rec.exp_category;

   cursor C_UCHG_EXISTS is
      select 'Y'
        from code_detail
       where code_detail.code_type = 'UCHG'
         and code_detail.code = I_rec.up_chrg_group;
BEGIN
   L_import_Country_id := I_rec.import_country_id;

   if I_rec.action IN (action_new, action_mod) then
      if I_rec.comp_type='A' then
         if I_rec.assess_type is not NULL then
            if ELC_SQL.ASSESS_TYPE_EXISTS(O_error_message,
                                          L_exists,
                                          I_rec.assess_type) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           NULL,
                           O_error_message);
               O_error :=TRUE;
            end if;
            if L_exists = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'ASSESS_TYPE',
                           'ASSESS_NOT_EXIST');
               O_error :=TRUE;
            end if;
         end if;

         if I_rec.import_country_id is NULL then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'IMPORT_COUNTRY_ID',
                        'ENTER_IMPORT_COUNTRY');
            O_error :=TRUE;
         end if;
         --foreign key constraint
         if I_rec.import_country_id is NOT NULL
            and I_rec.elc_cnt_fk_rid is NULL then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'IMPORT_COUNTRY_ID',
                        'INV_IMPORT_COUNTRY');
            O_error :=TRUE;
         end if;

        -- if the given CVB has an assessment attached to it in RMS table cvb_detail, then
        -- need to evaluate the import country  when trying to insert/update cvb_code
         if I_rec.comp_id is not NULL
            and I_rec.cvb_code is NOT NULL
            and I_rec.calc_basis ='V' then
            if ELC_SQL.VALIDATE_ASSESS_CVB(O_error_message,
                                           L_exists,
                                           L_import_country_id,
                                           I_rec.cvb_code,
                                           I_rec.comp_id) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           NULL,
                           O_error_message);
               O_error :=TRUE;
            end if;
            if L_exists = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'Computation Value Base,Import Country',
                           'CVB_SAME_IMPORT');
               O_error :=TRUE;
            end if;
         end if;

         --validations for comp_currency
         L_exists := FALSE;
         if I_rec.import_country_id is NOT NULL then
         -- currencies need to match for same import country  for comp_type = 'A'
            if ELC_SQL.GET_ASSESS_DEFAULT_CURRENCY(O_error_message,
                                                   L_exists,
                                                   L_comp_currency,
                                                   I_rec.import_country_id,
                                                   I_rec.comp_id) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           NULL,
                           O_error_message);
               O_error :=TRUE;
            end if;

            if L_exists = TRUE then
               if(I_rec.comp_currency is NOT NULL
                  and I_rec.comp_currency <> L_comp_currency) then
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              I_rec.row_seq,
                              'COMP_CURRENCY',
                              'SAME_CUR_IMP_CNTRY');
                  O_error :=TRUE;
               else
                  I_rec.comp_currency := L_comp_currency;
               end if;
            else
               if I_rec.comp_currency is NULL then
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              I_rec.row_seq,
                              'COMP_CURRENCY',
                              'CURRENCY_CODE_REQ');
                  O_error :=TRUE;
               end if;
            end if;
         end if;
         if I_rec.comp_currency is NULL then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'COMP_CURRENCY',
                        'CURRENCY_CODE_REQ');
            O_error :=TRUE;
         end if;
      elsif I_rec.comp_type='E' then
         if I_rec.action = action_new then
            if I_rec.expense_type is NULL
               or I_rec.EXPENSE_TYPE NOT IN( 'Z','C' )then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'EXPENSE_TYPE',
                           'INV_EXP_TYPE');
               O_error :=TRUE;
            end if;
         else
            if NVL(I_rec.expense_type ,-1) <> NVL(I_rec.old_expense_type,-1) then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'EXPENSE_TYPE',
                           'CANT_MOD_EXPENSE_TYPE');
               O_error :=TRUE;
            end if;
         end if;

   open  C_DETAIL_EXISTS;
   fetch C_DETAIL_EXISTS into L_code_exists;
   close C_DETAIL_EXISTS;
 
         if I_rec.exp_category is NULL
            or L_code_exists is NULL then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'EXP_CATEGORY',
                        'INV_EXP_CATEGORY');
            O_error :=TRUE;
         end if;
      elsif I_rec.comp_type = 'U' then
         if I_rec.action = action_new then
            if I_rec.up_chrg_type is NULL
               or I_rec.UP_CHRG_TYPE NOT IN ('E','P' )then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'UP_CHRG_TYPE',
                           'INV_UP_CHRG_TYPE');
               O_error :=TRUE;
            end if;
         else
            if NVL(I_rec.up_chrg_type,-1) <> NVL(I_rec.old_up_chrg_type,-1) then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'UP_CHRG_TYPE',
                           'CANT_MOD_UP_CHRG_TYPE');
               O_error :=TRUE;
            end if;
         end if;
   open  C_UCHG_EXISTS;
   fetch C_UCHG_EXISTS into L_uchg_exists;
   close C_UCHG_EXISTS;

         if I_rec.up_chrg_group is NULL or L_uchg_exists ='N' then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'UP_CHRG_GROUP',
                        'INV_UP_CHRG_GROUP');
            O_error :=TRUE;
         end if;
         ---for comp_type 'U' display_order must be greater than zero
         if I_rec.display_order is not NULL
            and I_rec.comp_type = 'U' then
            if I_rec.display_order = 0 then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'DISPLAY_ORDER',
                           'UP_DISP_GRTR_ZERO');
               O_error :=TRUE;
            end if;
         end if; --display order
      end if; ----validations based on comp_type

      if I_rec.calc_basis = 'S' then
         if (I_rec.per_count < 1 )then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'PER_COUNT',
                        'PER_COUNT_RANGE');
            O_error :=TRUE;
         end if;

         L_exists := TRUE;
         -----for validating per_count_uom
         if I_rec.per_count_uom is not NULL then
            if UOM_SQL.UOM_EXISTS(O_error_message,
                                  L_exists,
                                  I_rec.per_count_uom,
                                  NULL) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'PER_COUNT_UOM',
                           O_error_message);
               O_error :=TRUE;
            end if;
            if L_exists = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'PER_COUNT_UOM',
                           'UOM_NOT_EXIST');
               O_error :=TRUE;
            end if;
         end if;------- per count UOM
      end if;--- calc_basis = 'S'
      if (I_rec.display_order < 0) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DISPLAY_ORDER',
                     'DISP_ORD_RANGE');
         O_error :=TRUE;
      end if;
      if I_rec.comp_type IN ('A','E')
         and I_rec.comp_level is NOT NULL
         and (I_rec.comp_level < 1) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_LEVEL',
                     'COMP_LEVEL_RANGE');
         O_error :=TRUE;
      end if;
   end if;

---for mod-- cannot modify
   if I_rec.action = action_mod then

      -- for update, cannot modify comp_type
      if NVL(I_rec.comp_type,-1) <> NVL(I_rec.old_comp_type,-1) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_TYPE',
                     'CANT_MOD_COMP_TYPE');
         O_error :=TRUE;
      end if;

      ----for update, cannot modify calc_basis
      if NVL(I_rec.calc_basis ,-1) <> NVL(I_rec.old_calc_basis,-1) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CALC_BASIS',
                     'CANT_MOD_CALC_BASIS');
         O_error :=TRUE;
      end if;
   end if;

   L_exists := FALSE;
   ---validations for delete, cant delete if comp_id exists in dependent tables.
   if I_rec.action = action_del
      and I_rec.comp_id is NOT NULL
      and I_rec.pk_elc_comp_rid is not NULL
      and I_rec.comp_type IN ('A','E','U') then
      if ELC_SQL.CHECK_DELETE_COMP(O_error_message,
                                   L_exists,
                                   I_rec.comp_id,
                                   I_rec.comp_type) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_exists = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_ID',
                     'CANNOT_DELETE_COMP');
         O_error :=TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_ELC_COMP;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_ELC_COMP_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_elc_comp_temp_rec   IN       ELC_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_ELC_COMP_INS';
BEGIN
   insert into ELC_COMP
      values I_elc_comp_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ELC_COMP_INS;
---------------------------------------------------------------------------------------------------
FUNCTION EXEC_ELC_COMP_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_elc_comp_temp_rec   IN       ELC_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_ELC_COMP_UPD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   cursor C_ELC_COMP is
      select 'x'
        from ELC_COMP
       where elc_comp.comp_id  = I_elc_comp_temp_rec.comp_id;
BEGIN
   open C_ELC_COMP;
   close C_ELC_COMP;

   update ELC_COMP
      set row     = I_elc_comp_temp_rec
    where comp_id = I_elc_comp_temp_rec.comp_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'SVC_ELC_COMP',
                                             I_elc_comp_temp_rec.comp_id,
                                             NULL);
      close C_ELC_COMP;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if c_elc_comp%ISOPEN   then
         close C_ELC_COMP;
      end if;
      return FALSE;
END EXEC_ELC_COMP_UPD;
----------------------------------------------------------------------------------------------------------
FUNCTION EXEC_ELC_CFA_STG_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id           IN       NUMBER,
                              I_chunk_id             IN       NUMBER,
                              I_elc_comp_temp_rec    IN     ELC_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_COST_COMPONENT.EXEC_ELC_CFA_STG_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CFA_EXT';
   Record_Locked  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_RECS_DELETE is
     select sc.process_id ,
            sc.chunk_id ,
            sc.row_seq ,
            sc.action ,
            sc.base_rms_table ,
            sc.group_set_view_name ,
            sc.keys_col ,
            sc.attrs_col ,
            gs.group_set_id,
            gs.default_func,
            sf.template_key,
            sc.rowid as rid
       from svc_cfa_ext          sc,
            cfa_attrib_group_set gs,
            svc_process_tracker  sp,
            s9t_folder           sf
      where sc.process_id          = I_process_id
        and chunk_id               = I_chunk_id
        and sp.process_id          = I_process_id
        and sp.file_id             = sf.file_id (+)
        and sc.group_set_view_name = gs.group_set_view_name;

BEGIN
   open C_RECS_DELETE;
   close C_RECS_DELETE;

   FOR rec IN C_RECS_DELETE
   LOOP
      if key_val_pairs_pkg.get_attr(rec.KEYS_COL,'ELC_COMP') = I_elc_comp_temp_rec.comp_id   then
         delete from svc_cfa_ext
            where row_seq = rec.row_seq;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED   then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_elc_comp_temp_rec.comp_id,
                                            NULL);
      return FALSE;

   when OTHERS   then
      if C_RECS_DELETE%ISOPEN then
         close C_RECS_DELETE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ELC_CFA_STG_DEL;
---------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_ELC_COMP_PRE_DEL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id        IN       NUMBER,
                               I_chunk_id          IN       NUMBER,
                               I_elc_comp_temp_rec IN     ELC_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_ELC_COMP_PRE_DEL';
   L_exists      BOOLEAN;
   L_comp_type   ELC_COMP.COMP_TYPE%TYPE;

   cursor C_COMP_TYPE is
      select COMP_TYPE
        from ELC_COMP
       where COMP_ID = I_elc_comp_temp_rec.comp_id;

BEGIN

      open C_COMP_TYPE;
      fetch C_COMP_TYPE into L_comp_type;
      close C_COMP_TYPE;

      if ELC_SQL.CHECK_DELETE_COMP(O_error_message,
                                   L_exists,
                                   I_elc_comp_temp_rec.comp_id,
                                   L_comp_type) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = TRUE then
         return FALSE;
      else
        if COST_COMP_UPD_SQL.DELETE_COST_COMP(O_error_message,
                                              'E',
                                              I_elc_comp_temp_rec.comp_id,
                                              L_comp_type,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              I_elc_comp_temp_rec.import_country_id,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL) = FALSE then
           return FALSE;
        end if;
  
        if COST_COMP_UPD_SQL.DELETE_COST_COMP_EXT(O_error_message,
                                                  I_elc_comp_temp_rec.comp_id) = FALSE then
           return FALSE;
        end if;        
      end if;
      if EXEC_ELC_CFA_STG_DEL(O_error_message,
                              I_process_id,
                              I_chunk_id,
                              I_elc_comp_temp_rec) = FALSE then
        return FALSE;
      end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_ELC_COMP_PRE_DEL;
----------------------------------------------------------------------------------------------------------
FUNCTION EXEC_ELC_COMP_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id           IN       NUMBER,
                           I_chunk_id             IN       NUMBER,
                           I_elc_comp_temp_rec   IN       ELC_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_ELC_COMP_DEL';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   cursor C_ELC_COMP is
      select 'x'
        from elc_comp
       where elc_comp.comp_id  = I_elc_comp_temp_rec.comp_id;

   cursor C_ELC_COMP_TL is
      select 'x'
        from elc_comp_tl
       where comp_id  = I_elc_comp_temp_rec.comp_id;
BEGIN
   open C_ELC_COMP_TL;
   close C_ELC_COMP_TL;

   open C_ELC_COMP;
   close C_ELC_COMP;

   if EXEC_ELC_COMP_PRE_DEL(O_error_message,
                            I_process_id,
                            I_chunk_id,
                            I_elc_comp_temp_rec) = FALSE then
      return FALSE;
   else
      ---
      delete 
        from elc_comp_tl 
       where comp_id = I_elc_comp_temp_rec.comp_id;
      
      delete
        from elc_comp
       where comp_id = I_elc_comp_temp_rec.comp_id;
      ---
       
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            'SVC_ELC_COMP',
                                             I_elc_comp_temp_rec.comp_id,
                                             NULL);
      close C_ELC_COMP;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_ELC_COMP%ISOPEN   then
         close C_ELC_COMP;
      end if;
      return FALSE;
END EXEC_ELC_COMP_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_ELC_COMP_TL_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_elc_comp_tl_ins_tab    IN       ELC_COMP_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_ELC_COMP_TL_INS';

BEGIN
   if I_elc_comp_tl_ins_tab is NOT NULL and I_elc_comp_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_elc_comp_tl_ins_tab.COUNT()
         insert into elc_comp_tl
              values I_elc_comp_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_ELC_COMP_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_ELC_COMP_TL_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_elc_comp_tl_upd_tab   IN       ELC_COMP_TL_TAB,
                              I_elc_comp_tl_upd_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_ELC_COMP_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_ELC_COMP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_ELC_COMP_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'ELC_COMP_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_ELC_COMP_TL_UPD(I_comp_id   ELC_COMP_TL.COMP_ID%TYPE,
                                 I_lang      ELC_COMP_TL.LANG%TYPE) is
      select 'x'
        from elc_comp_tl
       where comp_id = I_comp_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_elc_comp_tl_upd_tab is NOT NULL and I_elc_comp_tl_upd_tab.count > 0 then
      for i in I_elc_comp_tl_upd_tab.FIRST..I_elc_comp_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_elc_comp_tl_upd_tab(i).lang);
            L_key_val2 := 'Comp ID: '||to_char(I_elc_comp_tl_upd_tab(i).comp_id);
            open C_LOCK_ELC_COMP_TL_UPD(I_elc_comp_tl_upd_tab(i).comp_id,
                                     I_elc_comp_tl_upd_tab(i).lang);
            close C_LOCK_ELC_COMP_TL_UPD;
            
            update elc_comp_tl
               set comp_desc = I_elc_comp_tl_upd_tab(i).comp_desc,
                   last_update_id = I_elc_comp_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_elc_comp_tl_upd_tab(i).last_update_datetime
             where lang = I_elc_comp_tl_upd_tab(i).lang
               and comp_id = I_elc_comp_tl_upd_tab(i).comp_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_ELC_COMP_TL',
                           I_elc_comp_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_ELC_COMP_TL_UPD%ISOPEN then
         close C_LOCK_ELC_COMP_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_ELC_COMP_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_ELC_COMP_TL_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_elc_comp_tl_del_tab   IN       ELC_COMP_TL_TAB,
                              I_elc_comp_tl_del_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_ELC_COMP_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_ELC_COMP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_ELC_COMP_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'ELC_COMP_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_ELC_COMP_TL_DEL(I_comp_id  ELC_COMP_TL.COMP_ID%TYPE,
                                 I_lang     ELC_COMP_TL.LANG%TYPE) is
      select 'x'
        from elc_comp_tl
       where comp_id =  I_comp_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_elc_comp_tl_del_tab is NOT NULL and I_elc_comp_tl_del_tab.count > 0 then
      for i in I_elc_comp_tl_del_tab.FIRST..I_elc_comp_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_elc_comp_tl_del_tab(i).lang);
            L_key_val2 := 'Comp ID: '||to_char(I_elc_comp_tl_del_tab(i).comp_id);
            open C_LOCK_ELC_COMP_TL_DEL(I_elc_comp_tl_del_tab(i).comp_id,
                                     I_elc_comp_tl_del_tab(i).lang);
            close C_LOCK_ELC_COMP_TL_DEL;
           
            delete elc_comp_tl
             where lang = I_elc_comp_tl_del_tab(i).lang
               and comp_id = I_elc_comp_tl_del_tab(i).comp_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_elc_comp_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_ELC_COMP_TL_DEL%ISOPEN then
         close C_LOCK_ELC_COMP_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_ELC_COMP_TL_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ELC_COMP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_ELC_COMP_TL.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_ELC_COMP_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_COST_COMPONENT.PROCESS_ELC_COMP_TL';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_ELC_COMP_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'ELC_COMP_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_elc_comp_tl_temp_rec    ELC_COMP_TL%ROWTYPE;
   L_elc_comp_tl_upd_rst     ROW_SEQ_TAB;
   L_elc_comp_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_ELC_COMP_TL(I_process_id NUMBER,
                            I_chunk_id   NUMBER) is
      select pk_elc_comp_tl.rowid  as pk_elc_comp_tl_rid,
             fk_elc_comp.rowid     as fk_elc_comp_rid,
             fk_lang.rowid         as fk_lang_rid,
             st.lang,
             st.comp_id,
             st.comp_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_elc_comp_tl  st,
             elc_comp         fk_elc_comp,
             elc_comp_tl      pk_elc_comp_tl,
             lang             fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and UPPER(st.comp_id) =  UPPER(fk_elc_comp.comp_id (+))
         and st.lang        =  pk_elc_comp_tl.lang (+)
         and UPPER(st.comp_id) =  UPPER(pk_elc_comp_tl.comp_id(+))
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_ELC_COMP_TL is TABLE OF C_SVC_ELC_COMP_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_elc_comp_tl_tab        SVC_ELC_COMP_TL;

   L_elc_comp_tl_ins_tab         elc_comp_tl_tab         := NEW elc_comp_tl_tab();
   L_elc_comp_tl_upd_tab         elc_comp_tl_tab         := NEW elc_comp_tl_tab();
   L_elc_comp_tl_del_tab         elc_comp_tl_tab         := NEW elc_comp_tl_tab();

BEGIN
   if C_SVC_ELC_COMP_TL%ISOPEN then
      close C_SVC_ELC_COMP_TL;
   end if;

   open C_SVC_ELC_COMP_TL(I_process_id,
                          I_chunk_id);
   LOOP
      fetch C_SVC_ELC_COMP_TL bulk collect into L_svc_elc_comp_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_elc_comp_tl_tab.COUNT > 0 then
         FOR i in L_svc_elc_comp_tl_tab.FIRST..L_svc_elc_comp_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_elc_comp_tl_tab(i).action is NULL
               or L_svc_elc_comp_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_elc_comp_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_elc_comp_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_elc_comp_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_elc_comp_tl_tab(i).action = action_new
               and L_svc_elc_comp_tl_tab(i).pk_elc_comp_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_elc_comp_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_elc_comp_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_elc_comp_tl_tab(i).comp_id is NOT NULL
               and L_svc_elc_comp_tl_tab(i).lang is NOT NULL
               and L_svc_elc_comp_tl_tab(i).pk_elc_comp_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_elc_comp_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_elc_comp_tl_tab(i).action = action_new
               and L_svc_elc_comp_tl_tab(i).comp_id is NOT NULL
               and L_svc_elc_comp_tl_tab(i).fk_elc_comp_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_elc_comp_tl_tab(i).row_seq,
                            'COMP_ID',
                            'COMP_ID_NOT_EXIST');
               L_error :=TRUE;
            end if;

            if L_svc_elc_comp_tl_tab(i).action = action_new
               and L_svc_elc_comp_tl_tab(i).lang is NOT NULL
               and L_svc_elc_comp_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_elc_comp_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_elc_comp_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_elc_comp_tl_tab(i).comp_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_elc_comp_tl_tab(i).row_seq,
                              'COMP_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_elc_comp_tl_tab(i).comp_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_elc_comp_tl_tab(i).row_seq,
                           'COMP_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_elc_comp_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_elc_comp_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_elc_comp_tl_temp_rec.lang := L_svc_elc_comp_tl_tab(i).lang;
               L_elc_comp_tl_temp_rec.comp_id := L_svc_elc_comp_tl_tab(i).comp_id;
               L_elc_comp_tl_temp_rec.comp_desc := L_svc_elc_comp_tl_tab(i).comp_desc;
               L_elc_comp_tl_temp_rec.create_datetime := SYSDATE;
               L_elc_comp_tl_temp_rec.create_id := GET_USER;
               L_elc_comp_tl_temp_rec.last_update_datetime := SYSDATE;
               L_elc_comp_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_elc_comp_tl_tab(i).action = action_new then
                  L_elc_comp_tl_ins_tab.extend;
                  L_elc_comp_tl_ins_tab(L_elc_comp_tl_ins_tab.count()) := L_elc_comp_tl_temp_rec;
               end if;

               if L_svc_elc_comp_tl_tab(i).action = action_mod then
                  L_elc_comp_tl_upd_tab.extend;
                  L_elc_comp_tl_upd_tab(L_elc_comp_tl_upd_tab.count()) := L_elc_comp_tl_temp_rec;
                  L_elc_comp_tl_upd_rst(L_elc_comp_tl_upd_tab.count()) := L_svc_elc_comp_tl_tab(i).row_seq;
               end if;

               if L_svc_elc_comp_tl_tab(i).action = action_del then
                  L_elc_comp_tl_del_tab.extend;
                  L_elc_comp_tl_del_tab(L_elc_comp_tl_del_tab.count()) := L_elc_comp_tl_temp_rec;
                  L_elc_comp_tl_del_rst(L_elc_comp_tl_del_tab.count()) := L_svc_elc_comp_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_ELC_COMP_TL%NOTFOUND;
   END LOOP;
   close C_SVC_ELC_COMP_TL;

   if EXEC_ELC_COMP_TL_INS(O_error_message,
                           L_elc_comp_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_ELC_COMP_TL_UPD(O_error_message,
                           L_elc_comp_tl_upd_tab,
                           L_elc_comp_tl_upd_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_ELC_COMP_TL_DEL(O_error_message,
                           L_elc_comp_tl_del_tab,
                           L_elc_comp_tl_del_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_ELC_COMP_TL;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_ELC_COMP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id     IN      SVC_ELC_COMP.PROCESS_ID%TYPE,
                          I_chunk_id       IN      SVC_ELC_COMP.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program             VARCHAR2(64)                       :='CORESVC_COST_COMPONENT.PROCESS_ELC_COMP';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  :='SVC_ELC_COMP';
   L_process_error       BOOLEAN;
   L_error               BOOLEAN;
   L_elc_comp_temp_rec   ELC_COMP%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_ELC_COMP(I_process_id,
                             I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      --for invalid action
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      --for duplicate record
      if rec.action = action_new
         and rec.pk_elc_comp_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMP_ID',
                     'COMP_EXISTS');
         L_error :=TRUE;
      end if;

      --for insert/delete if comp_type is invalid
      if rec.action IN (action_new,action_mod) then
         if rec.comp_type IS NULL
            or rec.cd_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMP_TYPE',
                        'INV_COMP_TYPE');
            L_error :=TRUE;
         end if;
      end if;

      --for update/delete if record doesnt exists
      if rec.action IN (action_mod,action_del)
         and rec.pk_elc_comp_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMP_ID',
                     'COMP_NOT_EXIST');
         L_error :=TRUE;
      end if;

      --for validations on action new,mod
      if rec.action IN (action_new,action_mod) then


         --for insert/update if comp_desc is null
         if rec.comp_desc IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMP_DESC',
                        'COMP_DESC_REQ');
            L_error :=TRUE;
         end if;

         --for validations of comp_type 'A' and 'E'
         if rec.comp_type IN ('A','E') then
            ---for insert/update if cvb_code is NULL
            ---validations for cost basis and cvb_code
            ---for comp_type 'E' any of the CVB_CODE or COST_BASIS must be NOT NULL
            if rec.comp_type = 'E'
               and rec.calc_basis = 'V'
               and rec.cvb_code is NULL then
               if rec.cost_basis is NULL then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'Computation Value Base,Order Cost Basis',
                              'ENTER_COST_CVB');
                  L_error :=TRUE;
               end if;
               if rec.cost_basis is NOT NULL
                  and rec.cost_basis NOT IN ( 'S','O' )then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              'COST_BASIS',
                              'INV_COST_BASIS');
                  L_error :=TRUE;
               end if;
            end if;

            --if cvb_code doesnt exists, foreign key constraint
            if rec.calc_basis = 'V'
               and rec.cvb_code is NOT NULL
               and rec.elc_cvb_fk_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'CVB_CODE',
                           'CVB_NOT_EXIST');
                 L_error :=TRUE;
            elsif rec.calc_basis = 'S'
                  and rec.cvb_code is NOT NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'CVB_CODE',
                           'CVB_NOT_NEED');
                 L_error :=TRUE;
            end if;

            if rec.action =action_mod
               and rec.comp_level is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COMP_LEVEL',
                           'COMP_LEVEL_REQ');
               L_error :=TRUE;
            end if;
         end if;-----------comp_type 'A' & 'E'

         if rec.always_default_ind NOT IN ('Y','N')
            or rec.always_default_ind is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ALWAYS_DEFAULT_IND',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_1 NOT IN('N','+','-' )
            or rec.nom_flag_1 is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_1',
                        'INV_NOM_FLAG_VAL');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_2 NOT IN('N','+','-' )
            or rec.nom_flag_2 is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_2',
                        'INV_NOM_FLAG_VAL');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_3 NOT IN( 'N','+','-' )
            or rec.nom_flag_3 is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_3',
                        'INV_NOM_FLAG_VAL');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_4 NOT IN('N','+','-' )
            or rec.nom_flag_4 is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_4',
                        'INV_NOM_FLAG_VAL');
            L_error :=TRUE;
         end if;

         if rec.nom_flag_5 NOT IN ('N','+','-' )
            or rec.nom_flag_5 is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NOM_FLAG_5',
                        'INV_NOM_FLAG_VAL');
            L_error :=TRUE;
         end if;


         ---for insert/update if comp_currency is NULL
         if rec.comp_type IN ('E','U') then
            if rec.comp_currency IS NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'COMP_CURRENCY',
                           'CURRENCY_CODE_REQ');
               L_error :=TRUE;
            end if;
         end if;

         --for insert/update if comp_currency doesnot exists in the CURRENCIES table
         --(foreign key constraint)
         if rec.comp_currency is NOT NULL
            and rec.elc_cur_fk_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMP_CURRENCY',
                        'INV_CURRENCY');
            L_error :=TRUE;
         end if;

         ---for insert/update if comp_rate is NULL
         if rec.comp_rate IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMP_RATE',
                        'NO_COMP_RATE');
            L_error :=TRUE;
         end if;
         ---for insert/update if comp_rate is negative value
         if rec.comp_rate < 0 then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMP_RATE',
                        'COMP_RATE_RANGE');
            L_error :=TRUE;
         end if;
      end if;

      ---for insert if calc_basis is NULL
      if rec.action = action_new
         and rec.calc_basis IS NULL
         or rec.calc_basis NOT IN ('V','S')then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CALC_BASIS',
                     'INV_CALC_BASIS');
         L_error :=TRUE;
      end if;

      if rec.action = action_mod
         and rec.display_order IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DISPLAY_ORDER',
                     'DISPLAY_ORDER_REQ');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new, action_mod) then
         if NOT((rec.calc_basis = 'V' and rec.per_count is NULL and rec.per_count_uom is NULL)
                or
               (rec.calc_basis = 'S' and rec.per_count is NOT NULL and rec.per_count_uom is NOT NULL and rec.cost_basis is NULL)) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'Calculation Basis, Component Per Count, Component Per Count UOM, Order Cost Basis',
                        'CHK_ELC_COMP_CALC_BASIS');
            L_error :=TRUE;
         end if;

         if NOT (rec.cvb_code is NOT NULL and rec.cost_basis is NULL
                 or rec.cvb_code is NULL and rec.cost_basis is NOT NULL
                 or rec.cvb_code is NULL and rec.cost_basis is NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'Computation Value Base, Order Cost Basis',
                        'CHK_ELC_COMP_CVB_CODE');
            L_error :=TRUE;
         end if;
         if rec.comp_type = 'E'
            and rec.calc_basis = 'S'
            and (rec.cost_basis is NOT NULL
             or rec.cvb_code is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'Order Cost Basis, Computation Value Base',
                        'CHK_ELC_COMP_ORDER_COST');
            L_error :=TRUE;
         end if;

      end if;


      --for other validations
      if PROCESS_VAL_ELC_COMP(O_error_message,
                              L_error,
                              rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      SAVEPOINT process;
      if NOT L_error then
         L_elc_comp_temp_rec.comp_id                 := rec.comp_id;
         L_elc_comp_temp_rec.comp_desc               := rec.comp_desc;
         L_elc_comp_temp_rec.comp_type               := rec.comp_type;
         L_elc_comp_temp_rec.assess_type             := rec.assess_type;
         L_elc_comp_temp_rec.import_country_id       := rec.import_country_id;
         L_elc_comp_temp_rec.expense_type            := rec.expense_type;
         L_elc_comp_temp_rec.up_chrg_type            := rec.up_chrg_type;
         L_elc_comp_temp_rec.up_chrg_group           := rec.up_chrg_group;
         L_elc_comp_temp_rec.cvb_code                := rec.cvb_code;
         L_elc_comp_temp_rec.calc_basis              := rec.calc_basis;
         L_elc_comp_temp_rec.cost_basis              := rec.cost_basis;
         L_elc_comp_temp_rec.exp_category            := rec.exp_category;
         L_elc_comp_temp_rec.comp_rate               := rec.comp_rate;
         L_elc_comp_temp_rec.comp_level              := rec.comp_level;
         L_elc_comp_temp_rec.display_order           := rec.display_order;
         L_elc_comp_temp_rec.always_default_ind      := rec.always_default_ind;
         L_elc_comp_temp_rec.comp_currency           := rec.comp_currency;
         L_elc_comp_temp_rec.per_count               := rec.per_count;
         L_elc_comp_temp_rec.per_count_uom           := rec.per_count_uom;
         L_elc_comp_temp_rec.nom_flag_1              := rec.nom_flag_1;
         L_elc_comp_temp_rec.nom_flag_2              := rec.nom_flag_2;
         L_elc_comp_temp_rec.nom_flag_3              := rec.nom_flag_3;
         L_elc_comp_temp_rec.nom_flag_4              := rec.nom_flag_4;
         L_elc_comp_temp_rec.nom_flag_5              := rec.nom_flag_5;
         L_elc_comp_temp_rec.sys_generated_ind       := 'N';

         if rec.comp_type ='A' then
            L_elc_comp_temp_rec.expense_type         := NULL;
            L_elc_comp_temp_rec.up_chrg_type         := NULL;
            L_elc_comp_temp_rec.up_chrg_group        := NULL;
            L_elc_comp_temp_rec.exp_category         := NULL;
            L_elc_comp_temp_rec.cost_basis           := NULL;


         elsif rec.comp_type ='E' then
            L_elc_comp_temp_rec.assess_type          := NULL;
            L_elc_comp_temp_rec.import_country_id    := NULL;
            L_elc_comp_temp_rec.up_chrg_type         := NULL;
            L_elc_comp_temp_rec.up_chrg_group        := NULL;


         elsif rec.comp_type = 'U' then
            L_elc_comp_temp_rec.cvb_code             := NULL;
            L_elc_comp_temp_rec.comp_level           := '1';
            L_elc_comp_temp_rec.NOM_FLAG_1           := 'N';
            L_elc_comp_temp_rec.NOM_FLAG_2           := 'N';
            L_elc_comp_temp_rec.NOM_FLAG_3           := 'N';
            L_elc_comp_temp_rec.NOM_FLAG_4           := 'N';
            L_elc_comp_temp_rec.NOM_FLAG_5           := 'N';
            L_elc_comp_temp_rec.ALWAYS_DEFAULT_IND   := 'N';
            L_elc_comp_temp_rec.assess_type          := NULL;
            L_elc_comp_temp_rec.import_country_id    := NULL;
            L_elc_comp_temp_rec.expense_type         := NULL;
            L_elc_comp_temp_rec.exp_category         := NULL;
            L_elc_comp_temp_rec.cost_basis           := NULL;
         end if;

         if rec.action = action_new then
            if rec.COMP_LEVEL  is NULL then
               L_elc_comp_temp_rec.COMP_LEVEL        := '1';
            end if;
            if rec.DISPLAY_ORDER is NULL then
               L_elc_comp_temp_rec.DISPLAY_ORDER     := '1';
            end if;

            if EXEC_ELC_COMP_INS(O_error_message,
                                 L_elc_comp_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_ELC_COMP_UPD(O_error_message,
                                 L_elc_comp_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_ELC_COMP_DEL(O_error_message,
                                 rec.process_id,    
                                 rec.chunk_id,
                                 L_elc_comp_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
               ROLLBACk to process;
            end if;
         end if;

         if NOT L_process_error --adding future cost component
            and rec.action IN (action_new,action_mod) then
            if ELC_SQL.CREATE_FC_FOR_COSTTMPL(O_error_message,
                                              L_elc_comp_temp_rec.comp_id) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
               ROLLBACK to process;
            end if;
         end if;
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_ELC_COMP;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_COST_COMP_UPD_STG_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_error           IN OUT   BOOLEAN,  
                                       L_comp_rate       IN OUT   elc_comp.comp_rate%TYPE,
                                       L_per_count       IN OUT   elc_comp.per_count%TYPE,
                                       L_per_count_uom   IN OUT   elc_comp.per_count_uom%TYPE,    
                                       L_calc_basis      IN OUT   elc_comp.calc_basis%TYPE,
                                       L_comp_currency   IN OUT   elc_comp.comp_currency%TYPE,    
                                       I_rec             IN       C_SVC_COST_COMP_UPD_STG%ROWTYPE,
                                       L_date            IN       DATE )
        
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'CORESVC_COST_COMPONENT.PROCESS_COST_COMP_UPD_STG_VAL';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_COMP_UPD_STG';  
   L_sys_generated_ind   elc_comp.sys_generated_ind%TYPE;
   L_comp_type           elc_comp.comp_type%TYPE;
   L_up_chrg_group       elc_comp.up_chrg_group%TYPE; 
   L_exists              BOOLEAN;
   L_exists_uom          BOOLEAN;  
   L_duplicate           BOOLEAN;

   cursor C_ELC is
      select sys_generated_ind, comp_type, up_chrg_group, calc_basis, comp_rate, comp_currency, per_count, per_count_uom      
        from elc_comp
       where comp_id = I_rec.comp_id; 
        
BEGIN
   open  C_ELC;
   fetch C_ELC into L_sys_generated_ind, L_comp_type, L_up_chrg_group, L_calc_basis,L_comp_rate, L_comp_currency, L_per_count, L_per_count_uom ;
   close C_ELC;
   
   if I_rec.comp_type is NOT NULL 
      and I_rec.cd_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'COMP_TYPE',
                  'INV_COMP_TYPE');
      O_error :=TRUE;
   end if;
   
   if I_rec.expense_type is NOT NULL 
      and I_rec.cd_ex_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'EXPENSE_TYPE',
                  'INV_EXP_TYPE');
      O_error :=TRUE;
   end if;
   
   if I_rec.new_comp_currency is not NULL then
      if CURRENCY_SQL.EXIST(O_error_message,
                            I_rec.new_comp_currency,
                            L_exists) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_exists = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'NEW_COMP_CURRENCY',
                     'INV_CURRENCY');
         O_error :=TRUE;      
      end if;
   end if;
   
   if I_rec.new_per_count_uom is NOT NULL then
      if UOM_SQL.UOM_EXISTS(O_error_message,
                            L_exists_uom,
                            I_rec.new_per_count_uom,
                            NULL) and L_exists_uom = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'NEW_PER_COUNT_UOM',
                     O_error_message);
         O_error :=TRUE;      
      end if;
   end if;   
   
   if L_calc_basis != 'S' 
      and (I_rec.new_per_count is NOT NULL 
      or I_rec.new_per_count_uom is NOT NULL) then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'Per Count,Per Count UOM',
                  'NO_COUNT');
      O_error :=TRUE;     
   end if;
   
   if I_rec.action = action_new then
      if I_rec.elc_rid is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_ID',
                     'INV_COMP_ID');
         O_error :=TRUE;
      end if;  
      
      if I_rec.effective_date < L_date then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'EFFECTIVE_DATE',
                     'EDATE_AFTER_TODAY');
         O_error :=TRUE;         
      end if;
    
      if L_sys_generated_ind != 'N' 
         or (L_comp_type = 'U' and L_up_chrg_group = 'W' ) then 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_ID',
                     'NO_ACCESS_COMP');
         O_error :=TRUE;  
      else
         if L_sys_generated_ind = 'N' 
            and L_up_chrg_group = 'W' then 
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'COMP_ID',
                        'NO_ACCESS_COMP');
            O_error :=TRUE;  
         end if;
      end if;   
   end if; -- I_rec.action = action_new
     
   if I_rec.action = action_mod then
      if I_rec.effective_date_upd < L_date then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'EFFECTIVE_DATE_UPD',
                     'EDATE_AFTER_TODAY');
         O_error :=TRUE;         
      end if;
      if I_rec.effective_date_upd <> I_rec.effective_date then      --Check only if date has been modified
         if COMP_RT_UPD_SQL.IS_UNIQUE(O_error_message,
                                      L_duplicate,
                                      'E',
                                      I_rec.effective_date_upd,
                                      I_rec.comp_id,
                                      I_rec.dept_pk,
                                      I_rec.from_loc_pk,
                                      I_rec.to_loc_pk,
                                      I_rec.exp_prof_key_pk) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        NULL,
                        O_error_message);
            O_error :=TRUE;           
         end if;
         if L_duplicate = TRUE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'EFFECTIVE_DATE',
                        'DUP_EFFECTIVE_DATE');
            O_error :=TRUE;         
         end if;
      end if;  --I_rec.effective_date_upd <> I_rec.effective_date then    
   end if; --I_rec.action = action_mod
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COST_COMP_UPD_STG_VAL;
--------------------------------------------------------------------------------
FUNCTION EXEC_CCUS_POST_INS( O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_comp_upd_stg_temp_rec   IN       COST_COMP_UPD_STG%ROWTYPE,
                              I_rec                          IN       C_SVC_COST_COMP_UPD_STG%ROWTYPE,
                              L_comp_currency                IN       elc_comp.comp_currency%TYPE,    
                              L_per_count                    IN       elc_comp.per_count%TYPE,
                              L_per_count_uom                IN       elc_comp.per_count_uom%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_CCUS_POST_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_COMP_UPD_STG';
BEGIN
   if COMP_RT_UPD_SQL.UPDATE_OLD_VALUES(O_error_message,
                                        I_rec.new_comp_rate,
                                        L_comp_currency,
                                        L_per_count,
                                        L_per_count_uom,
                                        'E',
                                        I_rec.comp_id,
                                        I_rec.dept_pk,
                                        I_rec.from_loc_pk,
                                        I_rec.to_loc_pk,
                                        I_rec.exp_prof_key_pk) = FALSE then
      return FALSE;
   end if;
  
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CCUS_POST_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_COST_COMP_UPD_STG_INS(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_comp_upd_stg_temp_rec   IN       COST_COMP_UPD_STG%ROWTYPE,
                                    I_rec                          IN       C_SVC_COST_COMP_UPD_STG%ROWTYPE,
                                    L_comp_currency                IN       elc_comp.comp_currency%TYPE,    
                                    L_per_count                    IN       elc_comp.per_count%TYPE,
                                    L_per_count_uom                IN       elc_comp.per_count_uom%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_COST_COMP_UPD_STG_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_COMP_UPD_STG';
   
BEGIN
   SAVEPOINT updold;

   insert into cost_comp_upd_stg
      values I_cost_comp_upd_stg_temp_rec;   
   if EXEC_CCUS_POST_INS (O_error_message,
                         I_cost_comp_upd_stg_temp_rec,
                         I_rec,
                         L_comp_currency,
                         L_per_count,
                         L_per_count_uom) = FALSE then
      ROLLBACK TO updold;                                     
      return FALSE;
      end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COST_COMP_UPD_STG_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_COST_COMP_UPD_STG_UPD(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_comp_upd_stg_temp_rec   IN       COST_COMP_UPD_STG%ROWTYPE,
                                    I_rec                          IN       C_SVC_COST_COMP_UPD_STG%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_COST_COMP_UPD_STG_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COST_COMP_UPD_STG';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_CCUS_UPD is
      select 'x'
        from cost_comp_upd_stg
       where comp_id = I_cost_comp_upd_stg_temp_rec.comp_id
         and effective_date = I_rec.effective_date   
         for update nowait;
BEGIN
   open  C_LOCK_CCUS_UPD;
   close C_LOCK_CCUS_UPD;
   
   update cost_comp_upd_stg
      set new_comp_rate = I_cost_comp_upd_stg_temp_rec.new_comp_rate,
          new_per_count = I_cost_comp_upd_stg_temp_rec.new_per_count,
          new_per_count_uom = I_cost_comp_upd_stg_temp_rec.new_per_count_uom,
          new_comp_currency = I_cost_comp_upd_stg_temp_rec.new_comp_currency,
          effective_date = I_cost_comp_upd_stg_temp_rec.effective_date,
          cntry_default_ind = I_cost_comp_upd_stg_temp_rec.cntry_default_ind,    
          supp_default_ind = I_cost_comp_upd_stg_temp_rec.supp_default_ind,
          ptnr_default_ind = I_cost_comp_upd_stg_temp_rec.ptnr_default_ind,
          item_default_ind = I_cost_comp_upd_stg_temp_rec.item_default_ind,
          order_default_ind =  I_cost_comp_upd_stg_temp_rec.order_default_ind,
          tsf_alloc_default_ind = I_cost_comp_upd_stg_temp_rec.tsf_alloc_default_ind,
          dept_default_ind = I_cost_comp_upd_stg_temp_rec.dept_default_ind
    where comp_id = I_cost_comp_upd_stg_temp_rec.comp_id
      and effective_date = I_rec.effective_date;
   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_cost_comp_upd_stg_temp_rec.comp_id,
                                             I_cost_comp_upd_stg_temp_rec.effective_date);
      return FALSE;
   when OTHERS then
      if C_LOCK_CCUS_UPD%ISOPEN then
         close C_LOCK_CCUS_UPD;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COST_COMP_UPD_STG_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_COST_COMP_UPD_STG_DEL(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_comp_upd_stg_temp_rec   IN       COST_COMP_UPD_STG%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COST_COMPONENT.EXEC_COST_COMP_UPD_STG_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COST_COMP_UPD_STG';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_CCUS_DEL is
      select 'x'
        from cost_comp_upd_stg
       where comp_id = I_cost_comp_upd_stg_temp_rec.comp_id
         and effective_date = I_cost_comp_upd_stg_temp_rec.effective_date   
         for update nowait;
BEGIN
   open  C_LOCK_CCUS_DEL;
   close C_LOCK_CCUS_DEL;
   
   delete
     from cost_comp_upd_stg
    where comp_id = I_cost_comp_upd_stg_temp_rec.comp_id
      and effective_date = I_cost_comp_upd_stg_temp_rec.effective_date;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_cost_comp_upd_stg_temp_rec.comp_id,
                                             I_cost_comp_upd_stg_temp_rec.effective_date);
      return FALSE;
   when OTHERS then
      if C_LOCK_CCUS_DEL%ISOPEN then
         close C_LOCK_CCUS_DEL;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COST_COMP_UPD_STG_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_COST_COMP_UPD_STG( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_process_id      IN       SVC_COST_COMP_UPD_STG.PROCESS_ID%TYPE,
                                    I_chunk_id        IN       SVC_COST_COMP_UPD_STG.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                      VARCHAR2(64) := 'CORESVC_COST_COMPONENT.PROCESS_COST_COMP_UPD_STG';
   L_table                        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COST_COMP_UPD_STG';
   L_error                        BOOLEAN;
   L_process_error                BOOLEAN := FALSE;
   L_cost_comp_upd_stg_temp_rec   cost_comp_upd_stg%ROWTYPE;
   L_seq_no                       cost_comp_upd_stg.seq_no%TYPE;
   L_calc_basis                   elc_comp.calc_basis%TYPE;
   L_exists                       BOOLEAN;
   L_comp_currency                elc_comp.comp_currency%TYPE;   
   L_per_count                    elc_comp.per_count%TYPE;
   L_per_count_uom                elc_comp.per_count_uom%TYPE; 
   L_comp_rate_elc                elc_comp.comp_rate%TYPE;
   L_comp_currency_elc            elc_comp.comp_currency%TYPE;   
   L_per_count_elc                elc_comp.per_count%TYPE;
   L_per_count_uom_elc            elc_comp.per_count_uom%TYPE;    
   L_comp_rate                    cost_comp_upd_stg.new_comp_rate%TYPE;
   L_mod                          BOOLEAN := TRUE;  
   L_date                         DATE ;
   L_set                          BOOLEAN;
   L_effective_date               DATE;               

   cursor C_ISFIRST (I_comp_id cost_comp_upd_stg.comp_id%TYPE) is
      select min (effective_date) 
        from cost_comp_upd_stg
        where comp_id = I_comp_id;
   
BEGIN
   L_date := GET_VDATE +1;
   FOR rec IN C_SVC_COST_COMP_UPD_STG(I_process_id,
                                      I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      L_set := FALSE;
      
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      
      if rec.action = action_new 
        and rec.pk_ccus_rid is NOT NULL then
        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.nextval,
                    I_chunk_id,
                    L_table,
                    rec.row_seq,
                    'Component,Effective Date',
                    'DUP_RECORD');
         L_error := TRUE;
      end if;
      
      if rec.action = action_mod 
         and rec.effective_date < L_date then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'NO_MOD_EDATE');
         L_error := TRUE;
         L_mod := FALSE;
      end if;   
          
      if rec.action IN (action_mod,action_del) then
         if rec.pk_ccus_rid IS NULL then
            WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.nextval,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'Component,Effective Date',
                       'NO_RECORD');
            L_error := TRUE;
         end if;
         if rec.defaulting_level != 'E' then
            WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.nextval,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'COMP_ID',
                       'NO_MOD_DEL');
            L_error := TRUE;
         end if;         
      end if;
      
      if rec.action IN (action_new, action_del)
         and rec.effective_date <> rec.effective_date_upd then
         WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.nextval,
                    I_chunk_id,
                    L_table,
                    rec.row_seq,
                    'Effective Date,New Effective Date',
                    'EDATE_SAME');
         L_error := TRUE;
      end if;   
      
      if rec.action = action_new 
         or (rec.action = action_mod and L_mod) then
         
         if rec.comp_type IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMP_TYPE',
                        'MUST_ENTER_TYPE');
            L_error := TRUE;
         end if; 
         
         if rec.item_default_ind is NULL 
            or rec.item_default_ind NOT IN ('Y','N')then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ITEM_DEFAULT_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
         
         if rec.order_default_ind is NULL 
            or rec.order_default_ind NOT IN ('Y','N')then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ORDER_DEFAULT_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;

         end if;
         
         if rec.supp_default_ind is NULL 
            or rec.supp_default_ind NOT IN ('Y','N') then

            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'SUPP_DEFAULT_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
         
         if rec.ptnr_default_ind is NULL 
            or rec.ptnr_default_ind NOT IN ('Y','N') then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'PTNR_DEFAULT_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;

         if rec.dept_default_ind is NULL 
            or rec.dept_default_ind NOT IN ('Y','N') then

            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DEPT_DEFAULT_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
         
         if rec.tsf_alloc_default_ind is NULL 
            or rec.tsf_alloc_default_ind NOT IN ('Y','N') then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TSF_ALLOC_DEFAULT_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
         
         if rec.cntry_default_ind is NULL 
            or rec.cntry_default_ind NOT IN ('Y','N') then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CNTRY_DEFAULT_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;

         end if;

         
         if PROCESS_COST_COMP_UPD_STG_VAL(O_error_message,
                                          L_error,
                                          L_comp_rate_elc,                                           
                                          L_per_count,
                                          L_per_count_uom,
                                          L_calc_basis,
                                          L_comp_currency,
                                          rec,
                                          L_date) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error := TRUE;
         end if;
      end if; -- rec.action IN (action_new, action_mod)
      
      if NOT L_error and 
         rec.action = action_new then
         if COMP_RT_UPD_SQL.GET_NEXT_SEQUENCE(O_error_message,
                                              L_seq_no) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error := TRUE;
         end if;
      end if;

      if NOT L_error 
         and rec.action IN (action_new, action_mod) then
         if rec.new_comp_rate is NULL 
            or rec.new_comp_currency is NULL 
            or (L_calc_basis != 'V' 
            and (rec.new_per_count is NULL 
            or rec.new_per_count_uom is NULL )) then
            L_comp_rate := rec.new_comp_rate;
            L_comp_currency_elc := L_comp_currency;
            L_per_count_elc := L_per_count;
            L_per_count_uom_elc := L_per_count_uom;
            
            open C_ISFIRST (rec.comp_id);
            fetch C_ISFIRST into L_effective_date;
            close C_ISFIRST;
            
            if rec.action = action_new then

               if (L_effective_date is NULL) or (L_effective_date > rec.effective_date) then
                  L_set := TRUE;         
                  rec.new_comp_rate     := NVL(rec.new_comp_rate, L_comp_rate_elc);
                  rec.new_comp_currency := NVL(rec.new_comp_currency, L_comp_currency_elc);
                  if L_calc_basis != 'V' then
                     rec.new_per_count     := NVL(rec.new_per_count, L_per_count_elc);
                     rec.new_per_count_uom := NVL(rec.new_per_count_uom, L_per_count_uom_elc);
                  end if;
               else
               
                  if COMP_RT_UPD_SQL.GET_DEFAULT_VALUES(O_error_message,
                                                        L_exists,
                                                        L_comp_rate,
                                                        L_comp_currency,
                                                        L_per_count,
                                                        L_per_count_uom,
                                                        'E',
                                                        rec.comp_id,
                                                        rec.dept_pk,
                                                        rec.from_loc_pk,
                                                        rec.to_loc_pk,
                                                        rec.exp_prof_key_pk,
                                                        rec.effective_date) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 rec.row_seq,
                                 NULL,
                                 O_error_message);
                     L_error := TRUE;  
                  end if;
               end if;    --  (L_effective_date is NULL) or (L_effective_date > rec.effective_date) 
            else      -- if rec.action = action_mod

               if (L_effective_date is NULL) or (L_effective_date >= rec.effective_date_upd) then
                  L_set := TRUE;  
                  rec.new_comp_rate     := NVL(rec.new_comp_rate, L_comp_rate_elc);
                  rec.new_comp_currency := NVL(rec.new_comp_currency, L_comp_currency_elc);
                  if L_calc_basis != 'V' then
                     rec.new_per_count     := NVL(rec.new_per_count, L_per_count_elc);
                     rec.new_per_count_uom := NVL(rec.new_per_count_uom, L_per_count_uom_elc);
                  end if;                  
               else   
                  if COMP_RT_UPD_SQL.GET_DEFAULT_VALUES(O_error_message,
                                                        L_exists,
                                                        L_comp_rate,
                                                        L_comp_currency,
                                                        L_per_count,
                                                        L_per_count_uom,
                                                        'E',
                                                        rec.comp_id,
                                                        rec.dept_pk,
                                                        rec.from_loc_pk,
                                                        rec.to_loc_pk,
                                                        rec.exp_prof_key_pk,
                                                        rec.effective_date_upd) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 rec.row_seq,
                                 NULL,
                                 O_error_message);
                     L_error := TRUE;  
                     end if;  
               end if;  --(L_effective_date is NULL) or (L_effective_date > rec.effective_date_upd)
            end if;
            if L_error = FALSE then
               if L_set = FALSE then
                  if L_exists = FALSE then 
                     rec.new_comp_rate     := NVL(rec.new_comp_rate, L_comp_rate_elc);
                     rec.new_comp_currency := NVL(rec.new_comp_currency, L_comp_currency_elc);
                     if L_calc_basis != 'V' then
                        rec.new_per_count     := NVL(rec.new_per_count, L_per_count_elc);
                        rec.new_per_count_uom := NVL(rec.new_per_count_uom, L_per_count_uom_elc);
                     end if;
                  else
                     rec.new_comp_rate     := NVL(rec.new_comp_rate, L_comp_rate);
                     rec.new_comp_currency := NVL(rec.new_comp_currency, L_comp_currency);
                     if L_calc_basis != 'V' then
                        rec.new_per_count     := NVL(rec.new_per_count, L_per_count);
                        rec.new_per_count_uom := NVL(rec.new_per_count_uom, L_per_count_uom);
                     end if;
                  end if;
               end if; --if L_set = FALSE then
            end if;
         end if;
      end if;   
      
      if NOT L_error then
         if rec.action = action_new then
            L_cost_comp_upd_stg_temp_rec.SEQ_NO             := L_seq_no;
            L_cost_comp_upd_stg_temp_rec.old_comp_rate      := rec.new_comp_rate;            
         end if;
         if rec.action = action_mod 
            and rec.effective_date_upd is NOT NULL then
            L_cost_comp_upd_stg_temp_rec.effective_date     := rec.effective_date_upd;    
         else   
            L_cost_comp_upd_stg_temp_rec.effective_date     := rec.effective_date;                  
         end if;
         L_cost_comp_upd_stg_temp_rec.DEFAULTING_LEVEL      := 'E';
         L_cost_comp_upd_stg_temp_rec.comp_id               := rec.comp_id;
         L_cost_comp_upd_stg_temp_rec.comp_type             := rec.comp_type;
         L_cost_comp_upd_stg_temp_rec.expense_type          := rec.expense_type;
         L_cost_comp_upd_stg_temp_rec.new_comp_rate         := rec.new_comp_rate;
         L_cost_comp_upd_stg_temp_rec.new_comp_currency     := rec.new_comp_currency;
         L_cost_comp_upd_stg_temp_rec.new_per_count         := rec.new_per_count;
         L_cost_comp_upd_stg_temp_rec.new_per_count_uom     := rec.new_per_count_uom;
            
         if rec.comp_type = 'A' then          
            L_cost_comp_upd_stg_temp_rec.item_default_ind      := rec.item_default_ind;
            L_cost_comp_upd_stg_temp_rec.order_default_ind     := rec.order_default_ind;
            L_cost_comp_upd_stg_temp_rec.tsf_alloc_default_ind := 'N';
            L_cost_comp_upd_stg_temp_rec.dept_default_ind      := 'N';
            L_cost_comp_upd_stg_temp_rec.cntry_default_ind     := 'N';
            L_cost_comp_upd_stg_temp_rec.supp_default_ind      := 'N';
            L_cost_comp_upd_stg_temp_rec.ptnr_default_ind      := 'N';
         end if;
         if rec.comp_type = 'E' then          
            L_cost_comp_upd_stg_temp_rec.item_default_ind      := rec.item_default_ind;
            L_cost_comp_upd_stg_temp_rec.order_default_ind     := rec.order_default_ind;
            L_cost_comp_upd_stg_temp_rec.tsf_alloc_default_ind := 'N';
            L_cost_comp_upd_stg_temp_rec.DEPT_DEFAULT_IND      := 'N';
            L_cost_comp_upd_stg_temp_rec.supp_default_ind      := rec.supp_default_ind;
            L_cost_comp_upd_stg_temp_rec.ptnr_default_ind      := rec.ptnr_default_ind;
            if rec.expense_type <> 'Z' then
               L_cost_comp_upd_stg_temp_rec.cntry_default_ind  := rec.cntry_default_ind;
            else
               L_cost_comp_upd_stg_temp_rec.cntry_default_ind  := 'N';
            end if;
         end if;
         if rec.comp_type = 'U' then          
            L_cost_comp_upd_stg_temp_rec.ITEM_DEFAULT_IND      := rec.item_default_ind;
            L_cost_comp_upd_stg_temp_rec.order_default_ind     := 'N';
            L_cost_comp_upd_stg_temp_rec.tsf_alloc_default_ind := rec.tsf_alloc_default_ind;
            L_cost_comp_upd_stg_temp_rec.dept_default_ind      := rec.dept_default_ind;         
            L_cost_comp_upd_stg_temp_rec.cntry_default_ind     := 'N';
            L_cost_comp_upd_stg_temp_rec.supp_default_ind      := 'N';
            L_cost_comp_upd_stg_temp_rec.ptnr_default_ind      := 'N';
         end if;
         if rec.action = action_new then
            if EXEC_COST_COMP_UPD_STG_INS(O_error_message,
                                          L_cost_comp_upd_stg_temp_rec,
                                          rec,
                                          L_comp_currency,
                                          L_per_count,
                                          L_per_count_uom) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_COST_COMP_UPD_STG_UPD( O_error_message,
                                           L_cost_comp_upd_stg_temp_rec,
                                           rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_COST_COMP_UPD_STG_DEL(O_error_message,
                                          L_cost_comp_upd_stg_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COST_COMP_UPD_STG;
----------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
     from svc_elc_comp_tl
    where process_id = I_process_id;

   delete
     from svc_elc_comp
    where process_id = I_process_id;

   delete from svc_cost_comp_upd_stg 
      where process_id = I_process_id;
END;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT NUMBER,
                  I_process_id    IN     NUMBER,
                  I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                    :='CORESVC_COST_COMPONENT.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_err_tab          CORESVC_CFLEX.TYP_ERR_TAB;

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_ELC_COMP(O_error_message,
                       I_process_id,
                       I_chunk_id)=FALSE   then
      return FALSE;
   end if;

   if PROCESS_ELC_COMP_TL(O_error_message,
                          I_process_id,
                          I_chunk_id)=FALSE   then
      return FALSE;
   end if;
   
   if CORESVC_CFLEX.PROCESS_CFA(O_error_message,
                                L_err_tab,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   if PROCESS_COST_COMP_UPD_STG(O_error_message,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   FOR i in 1..L_err_tab.count
   LOOP
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_err_tab(i).view_name,
                  L_err_tab(i).row_seq,
                  L_err_tab(i).attrib,
                  L_err_tab(i).err_msg);
   END LOOP;
   
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
         action_date = SYSDATE
   where process_id = I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------------
END CORESVC_COST_COMPONENT;
/