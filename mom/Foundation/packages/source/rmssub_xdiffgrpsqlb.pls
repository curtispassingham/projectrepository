
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XDIFFGRP_SQL AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_DIFF_GROUP
   -- Purpose      :  Inserts records on the diff_group tables
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DIFF_GROUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diffgrp_head_rec   IN       DIFF_GROUP_SQL.DIFF_GROUP_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_DIFF_GROUP_DETAIL
   -- Purpose      :  Inserts records on the diff_group_detail table
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DIFF_GROUP_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_diffgrp_detail_tbl   IN       DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_DIFF_GROUP
   -- Purpose      :  Updates a record on the diff_group_head table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIFF_GROUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diffgrp_head_rec   IN       DIFF_GROUP_HEAD%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_DIFF_GROUP_DETAIL
   -- Purpose      :  Updates records on the diff_group_detail table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIFF_GROUP_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_diffgrp_detail_tbl   IN       DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_DIFF_GROUP
   -- Purpose      :  Deletes records on the diff_group tables
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIFF_GROUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diffgrp_head_rec   IN       DIFF_GROUP_HEAD%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_DIFF_GROUP_DETAIL
   -- Purpose      :  Deletes records on the diff_group tables
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIFF_GROUP_DETAIL(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_diffgrp_detail_tbl   IN     DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_diffgrp_rec     IN       DIFF_GROUP_SQL.DIFF_GROUP_REC,
                 I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XDIFFGRP.LP_cre_type then
      if not CREATE_DIFF_GROUP(O_error_message,
                               I_diffgrp_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XDIFFGRP.LP_dtl_cre_type then
      if not CREATE_DIFF_GROUP_DETAIL(O_error_message,
                                      I_diffgrp_rec.diff_group_details) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XDIFFGRP.LP_mod_type then
      if not MODIFY_DIFF_GROUP(O_error_message,
                               I_diffgrp_rec.diff_group_head_row) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XDIFFGRP.LP_dtl_mod_type then
      if not MODIFY_DIFF_GROUP_DETAIL(O_error_message,
                                      I_diffgrp_rec.diff_group_details) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XDIFFGRP.LP_del_type then
      if not DELETE_DIFF_GROUP(O_error_message,
                               I_diffgrp_rec.diff_group_head_row) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XDIFFGRP.LP_dtl_del_type then
      if not DELETE_DIFF_GROUP_DETAIL(O_error_message,
                                      I_diffgrp_rec.diff_group_details) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DIFF_GROUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diffgrp_head_rec   IN       DIFF_GROUP_SQL.DIFF_GROUP_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_SQL.CREATE_DIFF_GROUP';

BEGIN

   if not DIFF_GROUP_SQL.INSERT_DIFF_GROUP(O_error_message,
                                           I_diffgrp_head_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_DIFF_GROUP;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_DIFF_GROUP_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_diffgrp_detail_tbl   IN       DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_SQL.CREATE_DIFF_GROUP_DETAIL';

BEGIN

   if not DIFF_GROUP_SQL.INSERT_DETAIL(O_error_message,
                                       I_diffgrp_detail_tbl) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_DIFF_GROUP_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIFF_GROUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diffgrp_head_rec   IN       DIFF_GROUP_HEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_SQL.MODIFY_DIFF_GROUP';

BEGIN

   if not DIFF_GROUP_SQL.UPDATE_HEAD(O_error_message,
                                     I_diffgrp_head_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_DIFF_GROUP;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIFF_GROUP_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_diffgrp_detail_tbl   IN       DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_SQL.MODIFY_DIFF_GROUP_DETAIL';

BEGIN

   if not DIFF_GROUP_SQL.UPDATE_DETAIL(O_error_message,
                                       I_diffgrp_detail_tbl) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_DIFF_GROUP_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIFF_GROUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diffgrp_head_rec   IN       DIFF_GROUP_HEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_SQL.DELETE_DIFF_GROUP';

BEGIN

   -- delete from diff_group_detail table
   if not DIFF_GROUP_SQL.DELETE_DIFF_GROUP_DETAILS(O_error_message,
                                                   I_diffgrp_head_rec.diff_group_id) then
      return FALSE;
   end if;
   ---
   -- delete from diff_group_head table
   if not DIFF_GROUP_SQL.DELETE_GRP_NO_DETAILS(O_error_message,
                                               I_diffgrp_head_rec.diff_group_id) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DIFF_GROUP;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIFF_GROUP_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_diffgrp_detail_tbl   IN       DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_SQL.DELETE_DIFF_GROUP_DETAIL';
   L_exists       VARCHAR2(1)  := NULL;

   cursor C_CHK_DETAIL_EXIST is
      select 'x'
        from diff_group_detail
       where diff_group_id = I_diffgrp_detail_tbl(1).diff_group_id;

BEGIN

   if not DIFF_GROUP_SQL.DELETE_DETAIL(O_error_message,
                                       I_diffgrp_detail_tbl) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_CHK_DETAIL_EXIST', 'DIFF_GROUP_DETAIL', 'diff_group_id: '||I_diffgrp_detail_tbl(1).diff_group_id);
   open C_CHK_DETAIL_EXIST;

   SQL_LIB.SET_MARK('FETCH', 'C_CHK_DETAIL_EXIST', 'DIFF_GROUP_DETAIL', 'diff_group_id: '||I_diffgrp_detail_tbl(1).diff_group_id);
   fetch C_CHK_DETAIL_EXIST into L_exists;

   SQL_LIB.SET_MARK('CLOSE', 'C_CHK_DETAIL_EXIST', 'DIFF_GROUP_DETAIL', 'diff_group_id: '||I_diffgrp_detail_tbl(1).diff_group_id);
   close C_CHK_DETAIL_EXIST;

   if L_exists is NULL then
      if not DIFF_GROUP_SQL.DELETE_GRP_NO_DETAILS(O_error_message,
                                                  I_diffgrp_detail_tbl(1).diff_group_id) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DIFF_GROUP_DETAIL;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XDIFFGRP_SQL;
/