/******************************************************************************
* Service Name     : PricingCostService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/PricingCostService/v1
* Description      : Pricing Cost web service
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE PRICINGCOSTSERVICEPROVIDERIMPL AUTHID CURRENT_USER AS
 

/******************************************************************************
 *
 * Operation       : lookupPrcCostColCriVo
 * Description     : 
      Lookup the Pricing Cost from the future cost table for the input query object.
    
 * 
 * Input           : "RIB_PrcCostColCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PrcCostColCriVo/v1
 * Description     : 
         PrcCostColQry object is a collection of item supplier location objects to fetch the pricing cost for.
      
 * 
 * Output          : "RIB_PrcCostColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/PrcCostColDesc/v1
 * Description     : 
         PrcCostColDesc object is a collection of pricing cost from the future cost table for the inputs provided.
      
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE lookupPrcCostColCriVo(I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                                I_businessObject          IN  "RIB_PrcCostColCriVo_REC",
                                O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                                O_businessObject          OUT "RIB_PrcCostColDesc_REC"
                               );
/******************************************************************************/



END PricingCostServiceProviderImpl;
/
 