CREATE OR REPLACE PACKAGE POS_UPDATE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
--Function: DELETE_POS_STORE
--Purpose:  This function has been created to delete store records from
--          the POS_STORE table.
--          This function is being called from posstore.fmb
----------------------------------------------------------------------------
FUNCTION DELETE_POS_STORE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_pos_config_id     IN       POS_STORE.POS_CONFIG_ID%TYPE,
                          I_pos_config_type   IN       POS_STORE.POS_CONFIG_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------
--Function: INSERT_POS_STORE
--Purpose:  This function has been created to insert store records to
--          the POS_STORE table.
----------------------------------------------------------------------------
FUNCTION INSERT_POS_STORE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_pos_config_id     IN       POS_STORE.POS_CONFIG_ID%TYPE,
                          I_pos_config_type   IN       POS_STORE.POS_CONFIG_TYPE%TYPE,
                          I_location          IN       NUMBER,
                          I_loc_type          IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------
END;
/
