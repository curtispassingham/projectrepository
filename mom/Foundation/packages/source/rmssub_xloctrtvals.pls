
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XLOCTRT_VALIDATE AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_loctrait_rec    OUT    NOCOPY   LOC_TRAITS%ROWTYPE,
                       I_message         IN              "RIB_XLocTrtDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_loctrait_rec    OUT    NOCOPY   LOC_TRAITS%ROWTYPE,
                       I_message         IN              "RIB_XLocTrtRef_REC")
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XLOCTRT_VALIDATE;
/
