create or replace PACKAGE CORESVC_DEAL_COMP_TYPE AUTHID CURRENT_USER AS
   template_key                           CONSTANT VARCHAR2(255)         :='DEAL_COMP_TYPE_DATA';
   action_new                                      VARCHAR2(25)          := 'NEW';
   action_mod                                      VARCHAR2(25)          := 'MOD';
   action_del                                      VARCHAR2(25)          := 'DEL';
   DEAL_COMP_TYPE_TL_sheet                         VARCHAR2(255)         := 'DEAL_COMP_TYPE_TL';
   DEAL_COMP_TYPE_TL$Action                        NUMBER                :=1;
   DEAL_COMP_TYPE_TL$REVIEWED_IND                  NUMBER                :=6;
   DEAL_COMP_TYPE_TL$ORIG_LANG_IN                  NUMBER                :=5;
   DEAL_COMP_TYPE_TL$DL_CP_TP_DSC                  NUMBER                :=4;
   DEAL_COMP_TYPE_TL$LANG                          NUMBER                :=3;
   DEAL_COMP_TYPE_TL$DL_CMP_TP                     NUMBER                :=2;
   sheet_name_trans                                S9T_PKG.trans_map_typ;
   action_column                                   VARCHAR2(255)         := 'ACTION';
   template_category                               CODE_DETAIL.CODE%TYPE := 'RMSPCO';
   TYPE DEAL_COMP_TYPE_TL_rec_tab IS               TABLE OF DEAL_COMP_TYPE_TL%ROWTYPE;
   DEAL_COMP_TYPE_sheet                            VARCHAR2(255)         := 'DEAL_COMP_TYPE';
   DEAL_COMP_TYPE$Action                           NUMBER                :=1;
   DEAL_COMP_TYPE$DEAL_COMP_TYPE                   NUMBER                :=2;
   DEAL_COMP_TYPE$DL_COMP_TP_DESC                  NUMBER                :=3;

   TYPE DEAL_COMP_TYPE_rec_tab IS TABLE OF DEAL_COMP_TYPE%ROWTYPE;

  --------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   ---------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;

   END CORESVC_DEAL_COMP_TYPE;
   /