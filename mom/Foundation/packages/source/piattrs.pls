CREATE OR REPLACE PACKAGE PACKITEM_ATTRIB_SQL AUTHID CURRENT_USER AS

TYPE comp_item_TBL is table of V_PACKSKU_QTY.ITEM%TYPE;
TYPE comp_qty_TBL  is table of V_PACKSKU_QTY.QTY%TYPE;
TYPE comp_cost_TBL is table of ITEM_LOC_SOH.UNIT_COST%TYPE;

--------------------------------------------------------------------------------
-- Name:     GET_ORDER_COMP_COST
-- Purpose:  Overloaded to allow location to be passed in.
--           This function finds the cost for a pack component item based on the
--           overall cost of the pack for the pack/component item/supp/country/location
--           combination.
--------------------------------------------------------------------------------
FUNCTION GET_ORDER_COMP_COST(O_error_message     IN OUT VARCHAR2,
                             I_pack_no           IN     PACKITEM.PACK_NO%TYPE,
                             I_order_cost        IN     NUMBER,
                             I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                             I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                             I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_location          IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                             O_order_cost        IN OUT NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:     GET_ORDER_COMP_COST
-- Purpose:  Finds the cost for a pack component item based on the
--           overall cost of the pack for the pack/component item/supp/country.
--------------------------------------------------------------------------------
FUNCTION GET_ORDER_COMP_COST(O_error_message     IN OUT VARCHAR2,
                             I_pack_no           IN     PACKITEM.PACK_NO%TYPE,
                             I_order_cost        IN     NUMBER,
                             I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                             I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                             I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             O_order_cost        IN OUT NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name:  NEXT_PREV_PACKITEM
-- Purpose      :  Retrieves the first, next, or previous component items
--                 of a pack.
--------------------------------------------------------------------------------
FUNCTION NEXT_PREV_PACKITEM(O_error_message  IN OUT VARCHAR2,
                            O_item           IN OUT ITEM_MASTER.ITEM%TYPE,
                            I_pack_no        IN     PACKITEM.PACK_NO%TYPE,
                            I_old_item       IN     ITEM_MASTER.ITEM%TYPE,
                            I_which_item     IN     VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:     GET_PARENT_TMPL_ID
-- Purpose:  Returns the item_parent and pack_tmpl_id for the pack_no passed into function.
--           All component items will have the same item_parent and pack_tmpl_id for the
--           given pack.
--------------------------------------------------------------------------------
FUNCTION GET_PARENT_TMPL_ID(O_error_message IN OUT VARCHAR2,
                            O_item_parent   IN OUT PACKITEM.ITEM_PARENT%TYPE,
                            O_pack_tmpl_id  IN OUT PACKITEM.PACK_TMPL_ID%TYPE,
                            I_pack_no       IN     PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name:  GET_ITEM_AND_QTY
-- Purpose:        To retrieve one component item and quantity of a pack. This
--                 function is used for simple packs where there is only one item.
--------------------------------------------------------------------------------
FUNCTION GET_ITEM_AND_QTY (O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           O_item          IN OUT PACKITEM.ITEM%TYPE,
                           O_qty           IN OUT PACKITEM.PACK_QTY%TYPE,
                           I_pack_no       IN     PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name:  GET_NUMBER_OF_ITEMS
-- Purpose:        To retrieve the number of component items attached to
--                 a pack.
--------------------------------------------------------------------------------
FUNCTION GET_NUMBER_OF_ITEMS (O_error_message IN OUT VARCHAR2,
                              O_no_of_items   IN OUT NUMBER,
                              I_pack_no       IN     PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name:  GET_COMP_COSTS
-- Purpose:        To retrieve a list of component items, their quantities,
--                 and their costs.
--------------------------------------------------------------------------------
FUNCTION GET_COMP_COSTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_comp_items      IN OUT   NOCOPY COMP_ITEM_TBL,
                        O_comp_qtys       IN OUT   NOCOPY COMP_QTY_TBL,
                        O_comp_costs      IN OUT   NOCOPY COMP_COST_TBL,
                        I_pack_no         IN       PACKITEM.PACK_NO%TYPE,
                        I_loc             IN       ITEM_LOC_SOH.LOC%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name:  GET_COMP_QTYS
-- Purpose:        To retrieve a list of component items and their quantities.
--------------------------------------------------------------------------------
FUNCTION GET_COMP_QTYS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_comp_items      IN OUT   NOCOPY COMP_ITEM_TBL,
                       O_comp_qtys       IN OUT   NOCOPY COMP_QTY_TBL,
                       I_pack_no         IN       PACKITEM.PACK_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END PACKITEM_ATTRIB_SQL;
/


