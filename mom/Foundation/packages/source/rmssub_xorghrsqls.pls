
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XORGHR_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_hier_level      IN       VARCHAR2,
                 I_message_type    IN       VARCHAR2,
                 I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XORGHR_SQL;
/