CREATE OR REPLACE PACKAGE ITEM_SUPP_COUNTRY_DIM_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name: GET_ROW
--Purpose      : This function retrieves item dimensions information required
--               in the calculation of Cost Per UOM.
-------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists                  IN OUT   BOOLEAN,
                 O_item_supp_country_dim   IN OUT   ITEM_SUPP_COUNTRY_DIM%ROWTYPE,
                 I_item                    IN       ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                 I_supplier                IN       ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                 I_origin_country          IN       ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                 I_dim_object              IN       ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
--Function Name: GET_ROW
--Purpose      : This iis an overloaded function retrieves item dimensions information required
--               in the calculation of Cost Per UOM.The parametes will be individual parameter instead
--               of %ROWTYPE.
-------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists                  IN OUT   BOOLEAN,
                 O_presentation_method     IN OUT   ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD%TYPE,
                 O_length                  IN OUT   ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                 O_width		   IN OUT   ITEM_SUPP_COUNTRY_DIM.WIDTH	%TYPE,
                 O_height                  IN OUT   ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                 O_lwh_uom                 IN OUT   ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                 O_weight                  IN OUT   ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                 O_net_weight              IN OUT   ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE,
                 O_weight_uom              IN OUT   ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                 O_liquid_volume           IN OUT   ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
                 O_liquid_volume_uom       IN OUT   ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE,
                 O_stat_cube               IN OUT   ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE,
                 O_tare_weight             IN OUT   ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE,
                 O_tare_type               IN OUT   ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE,
                 I_item                    IN       ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                 I_supplier                IN       ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                 I_origin_country          IN       ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                 I_dim_object              IN       ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function Name: GET_NOMINAL_WEIGHT
-- Purpose      : This function returns the nominal weight for an item
--                in the item's cost unit of measure. The nominal weight 
--                is defined as the net weight (in weight_uom) on 
--                ITEM_SUPP_COUNTRY_DIM table for the item's primary
--                supplier and primary country. It will be converted
--                to cost uom. 
--------------------------------------------------------------------
FUNCTION GET_NOMINAL_WEIGHT(O_error_message IN OUT VARCHAR2,
                            O_weight_cuom   IN OUT item_supp_country_dim.net_weight%TYPE,
                            I_item          IN     item_master.item%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function Name: GET_NOMINAL_WEIGHT_UOM
-- Purpose      : This function returns the nominal weight and weight uom 
--                for an item. ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT is for case ,
--                the returned nominal weight is for eaches.
--------------------------------------------------------------------
FUNCTION GET_NOMINAL_WEIGHT_UOM(O_error_message   IN OUT   VARCHAR2,
                                O_weight          IN OUT   ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE,
                                O_weight_uom      IN OUT   ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                                I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END ITEM_SUPP_COUNTRY_DIM_SQL;
/
