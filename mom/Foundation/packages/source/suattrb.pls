CREATE OR REPLACE PACKAGE BODY SUPP_ATTRIB_SQL AS
--------------------------------------------------------------------
FUNCTION GET_SUPP_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_supplier        IN       NUMBER,
                       O_supp_desc       IN OUT   VARCHAR2) RETURN BOOLEAN is

   cursor C_supp_desc is
      select sup_name from v_sups_tl where supplier = I_supplier;

BEGIN
   open C_supp_desc;
   fetch C_supp_desc
      into O_supp_desc;
   if C_supp_desc%NOTFOUND then
      close C_supp_desc;
      O_error_message := sql_lib.create_msg('INV_SUP_NUM',
                                            null,
                                            null,
                                            null);
      RETURN FALSE;
   end if;
   close C_supp_desc;
  
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ATTRIB_SQL.GET_SUPP_DESC',
                                            To_Char(SQLCODE));
      RETURN FALSE;
END GET_SUPP_DESC;

---------------------------------------------------------------------
FUNCTION GET_PAYMENT_DESC(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_currency           IN       VARCHAR2,
                          I_terms              IN       VARCHAR2,
                          I_frght_terms        IN       VARCHAR2,
                          O_currency_desc      IN OUT   VARCHAR2,
                          O_terms_code         IN OUT   VARCHAR2,
                          O_terms_desc         IN OUT   VARCHAR2,
                          O_frght_terms_desc   IN OUT   VARCHAR2) RETURN BOOLEAN is

   cursor C_currency is
      select currency_desc
        from v_currencies_tl
       where currency_code = I_currency;

   cursor C_terms is
      select terms_code,
             terms_desc
        from v_terms_head_tl
       where terms = I_terms;

   cursor C_frght_terms is
      select term_desc
        from v_freight_terms_tl
       where freight_terms = I_frght_terms;

BEGIN
   open C_CURRENCY;
   fetch C_CURRENCY
      into O_currency_desc;
   if C_CURRENCY%NOTFOUND then
      close C_CURRENCY;
      O_error_message := sql_lib.create_msg('INV_CURRENCY',
                                            null,
                                            null,
                                            null);
      RETURN FALSE;
   end if;
   close C_CURRENCY;
  
   open C_TERMS;
   fetch C_TERMS
      into O_terms_code, O_terms_desc;
   if C_TERMS%NOTFOUND then
      close C_TERMS;
      O_error_message := sql_lib.create_msg('INV_TERMS',
                                            null,
                                            null,
                                            null);
      RETURN FALSE;
   end if;
   close C_TERMS;

   open C_FRGHT_TERMS;
   fetch C_FRGHT_TERMS
      into O_frght_terms_desc;
   if C_FRGHT_TERMS%NOTFOUND then
      close C_FRGHT_TERMS;
      O_error_message := sql_lib.create_msg('INV_TERMS',
                                            null,
                                            null,
                                            null);
      RETURN FALSE;
   end if;
   close C_FRGHT_TERMS;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ATTRIB_SQL.GET_TERMS_INFO',
                                            To_Char(SQLCODE));
      RETURN FALSE;
END GET_PAYMENT_DESC;
---------------------------------------------------------------------
FUNCTION GET_STATUS(I_supplier        IN       NUMBER,
                    O_status          IN OUT   VARCHAR2,
                    O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE) RETURN BOOLEAN IS

   cursor C_status is
      select sup_status from sups where supplier = I_supplier;

BEGIN
   open C_status;
   fetch C_status
      into O_status;
   if C_status%NOTFOUND then
      close C_status;
      O_error_message := sql_lib.create_msg('INV_SUP_NUM',
                                            null,
                                            null,
                                            null);
      RETURN FALSE;
   end if;
   close C_status;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ATTRIB_SQL.GET_STATUS',
                                            To_Char(SQLCODE));
   RETURN FALSE;
END GET_STATUS;
-----------------------------------------------------------------------
FUNCTION GET_RET_AUTH_REQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_supplier        IN       NUMBER,
                          O_ret_auth_req    IN OUT   VARCHAR2) RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'SUPP_ITEM_ATTRIB_SQL.GET_RET_AUTH_REQ';

   cursor C_GET_REQ is
      select ret_auth_req from sups where supplier = I_supplier;

BEGIN
   open C_GET_REQ;
   fetch C_GET_REQ
      into O_ret_auth_req;
   if C_GET_REQ%NOTFOUND then
      close C_GET_REQ;
      O_error_message := sql_lib.create_msg('INV_SUP_NUM',
                                            null,
                                            null,
                                            null);
      return FALSE;
   end if;
   close C_GET_REQ;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_RET_AUTH_REQ;
--------------------------------------------------------------------
FUNCTION GET_RET_ALLOW_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_supplier        IN       NUMBER,
                           O_ret_allow_ind   IN OUT   VARCHAR2) RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'SUPP_ATTRIB_SQL.GET_RET_ALLOW_IND';

   cursor C_GET_ALLOW is
      select ret_allow_ind from sups where supplier = I_supplier;

BEGIN
   open C_GET_ALLOW;
   fetch C_GET_ALLOW
      into O_ret_allow_ind;
   if C_GET_ALLOW%NOTFOUND then
      close C_GET_ALLOW;
      O_error_message := sql_lib.create_msg('INV_SUP_NUM',
                                            null,
                                            null,
                                            null);
      return FALSE;
   end if;
   close C_GET_ALLOW;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_RET_ALLOW_IND;
--------------------------------------------------------------------
FUNCTION GET_MIN_RET_AMT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_supplier        IN       NUMBER,
                         O_min_ret_amt     IN OUT   NUMBER) RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'SUPP_ATTRIB_SQL.GET_MIN_RET_AMT';

   cursor C_GET_AMT is
      select ret_min_dol_amt from sups where supplier = I_supplier;

BEGIN
   open C_GET_AMT;
   fetch C_GET_AMT
      into O_min_ret_amt;
   if C_GET_AMT%NOTFOUND then
      close C_GET_AMT;
      O_error_message := sql_lib.create_msg('INV_SUP_NUM',
                                            null,
                                            null,
                                            null);
      return FALSE;
   end if;
   close C_GET_AMT;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_MIN_RET_AMT;
--------------------------------------------------------------------
FUNCTION COURIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_supplier        IN       NUMBER,
                 O_desc            IN OUT   VARCHAR2) RETURN BOOLEAN IS

   cursor C_ATTRIB is
      select ret_courier from sups where supplier = I_supplier;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_ATTRIB', 'SUPS', to_char(I_supplier));
   open C_ATTRIB;

   SQL_LIB.SET_MARK('FETCH', 'C_ATTRIB', 'SUPS', to_char(I_supplier));
   fetch C_ATTRIB
      into O_desc;
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_SUP_NUM',
                                            NULL,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ATTRIB', 'SUPS', to_char(I_supplier));
      close C_ATTRIB;
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_ATTRIB', 'SUPS', to_char(I_supplier));
   close C_ATTRIB;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ATTRIB_SQL.COURIER',
                                            to_char(SQLCODE));
      RETURN FALSE;
END COURIER;
--------------------------------------------------------------------
FUNCTION ORDER_SUP_DETAILS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_currency_code    IN OUT   SUPS.CURRENCY_CODE%TYPE,
                           O_terms            IN OUT   SUPS.TERMS%TYPE,
                           O_freight_terms    IN OUT   SUPS.FREIGHT_TERMS%TYPE,
                           O_sup_status       IN OUT   SUPS.SUP_STATUS%TYPE,
                           O_qc_ind           IN OUT   SUPS.QC_IND%TYPE,
                           O_edi_po_ind       IN OUT   SUPS.EDI_PO_IND%TYPE,
                           O_pre_mark_ind     IN OUT   SUPS.PRE_MARK_IND%TYPE,
                           O_ship_method      IN OUT   SUPS.SHIP_METHOD%TYPE,
                           O_payment_method   IN OUT   SUPS.PAYMENT_METHOD%TYPE,
                           I_supplier         IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS

   L_function VARCHAR2(50) := 'SUPP_ATTRIB_SQL.ORDER_SUP_DETAILS';

   cursor C_SUP_DETAILS is
      select currency_code,
             terms,
             freight_terms,
             sup_status,
             qc_ind,
             edi_po_ind,
             pre_mark_ind,
             ship_method,
             payment_method
        from sups
       where supplier = I_supplier;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUP_DETAILS',
                    'SUPS',
                    'supplier: ' || TO_CHAR(I_supplier));
   open C_SUP_DETAILS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_SUP_DETAILS',
                    'SUPS',
                    'supplier: ' || TO_CHAR(I_supplier));
   fetch C_SUP_DETAILS
      into O_currency_code, O_terms, O_freight_terms, O_sup_status, O_qc_ind, O_edi_po_ind, O_pre_mark_ind, O_ship_method, O_payment_method;
   if C_SUP_DETAILS%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUP_NUM',
                                            NULL,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SUP_DETAILS',
                       'SUPS',
                       'supplier: ' || to_char(I_supplier));
      close C_SUP_DETAILS;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUP_DETAILS',
                    'SUPS',
                    'supplier: ' || to_char(I_supplier));
   close C_SUP_DETAILS;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END ORDER_SUP_DETAILS;
-------------------------------------------------------------------------
FUNCTION NEXT_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_addr_key        IN OUT   ADDR.ADDR_KEY%TYPE) RETURN BOOLEAN IS
   L_addr_key    ADDR.ADDR_KEY%TYPE := NULL;
   L_exists      VARCHAR2(1) := NULL;
   L_first_time  VARCHAR2(1) := 'Y';
   L_wrap_number ADDR.ADDR_KEY%TYPE := NULL;
   L_program     VARCHAR2(50) := 'SUPP_ATTRIB_SQL.NEXT_ADDR';

   cursor C_GET_NEXT_KEY is
      select addr_sequence.NEXTVAL from dual;

   cursor C_CHECK_KEY is
      select 'x' from addr where addr_key = L_addr_key;
BEGIN
   LOOP
      --- Retrieve sequence number
      SQL_LIB.SET_MARK('OPEN', 'C_GET_NEXT_KEY', 'DUAL', NULL);
      open C_GET_NEXT_KEY;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_NEXT_KEY', 'DUAL', NULL);
      fetch C_GET_NEXT_KEY
         into L_addr_key;

      if C_GET_NEXT_KEY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_NEXT_KEY', 'DUAL', NULL);
         close C_GET_NEXT_KEY;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_GET_NEXT_KEY', 'DUAL', NULL);
      close C_GET_NEXT_KEY;

      if (L_first_time = 'Y') then
         L_wrap_number := L_addr_key;
         L_first_time  := 'N';
      elsif (L_addr_key = L_wrap_number) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SQL_NO_AVAIL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      --- Check address key existence
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_KEY',
                       'ADDR',
                       'address key: ' || to_char(L_addr_key));
      open C_CHECK_KEY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_KEY',
                       'ADDR',
                       'address key: ' || to_char(L_addr_key));
      fetch C_CHECK_KEY
         into L_exists;

      if C_CHECK_KEY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_KEY',
                          'ADDR',
                          'address key: ' || to_char(L_addr_key));
         close C_CHECK_KEY;
         O_addr_key := L_addr_key;
         EXIT;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_KEY',
                       'ADDR',
                       'address key: ' || to_char(L_addr_key));
      close C_CHECK_KEY;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END NEXT_ADDR;
-------------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_currency_code   IN OUT   SUPS.CURRENCY_CODE%TYPE,
                           I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS
   ---
   L_program VARCHAR2(64) := 'SUPP_ATTRIB_SQL.GET_CURRENCY_CODE';
   ---
   cursor C_SUPS is
      select currency_code from sups where supplier = I_supplier;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUPS',
                    'SUPS',
                    'Supplier: ' || to_char(I_supplier));
   open C_SUPS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUPS',
                    'SUPS',
                    'Supplier: ' || to_char(I_supplier));
   fetch C_SUPS
      into O_currency_code;
   if C_SUPS%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SUPS',
                       'SUPS',
                       'Supplier: ' || to_char(I_supplier));
      close C_SUPS;
      O_error_message := sql_lib.create_msg('INV_SUP_NUM',
                                            null,
                                            null,
                                            null);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUPS',
                    'SUPS',
                    'Supplier: ' || to_char(I_supplier));
   close C_SUPS;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CURRENCY_CODE;
---------------------------------------------------------------------------------------
FUNCTION ORDER_IMPORT_DETAILS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_agent            IN OUT   SUP_IMPORT_ATTR.AGENT%TYPE,
                              O_lading_port      IN OUT   SUP_IMPORT_ATTR.LADING_PORT%TYPE,
                              O_discharge_port   IN OUT   SUP_IMPORT_ATTR.DISCHARGE_PORT%TYPE,

                              O_factory          IN OUT   SUP_IMPORT_ATTR.FACTORY%TYPE,
                              O_partner_type_1   IN OUT   SUP_IMPORT_ATTR.PARTNER_TYPE_1%TYPE,
                              O_partner_1        IN OUT   SUP_IMPORT_ATTR.PARTNER_1%TYPE,
                              O_partner_type_2   IN OUT   SUP_IMPORT_ATTR.PARTNER_TYPE_2%TYPE,
                              O_partner_2        IN OUT   SUP_IMPORT_ATTR.PARTNER_2%TYPE,
                              O_partner_type_3   IN OUT   SUP_IMPORT_ATTR.PARTNER_TYPE_3%TYPE,
                              O_partner_3        IN OUT   SUP_IMPORT_ATTR.PARTNER_2%TYPE,

                              I_supplier         IN       SUP_IMPORT_ATTR.SUPPLIER%TYPE) RETURN BOOLEAN IS

   L_function VARCHAR2(50) := 'SUPP_ATTRIB_SQL.ORDER_IMPORT_DETAILS';

   cursor C_DETAILS is

      /* Old cursor is being modified to select extra partners from sup_import_attr table */


      select agent,
             lading_port,
             discharge_port,
             factory,
             partner_type_1,
             partner_1,
             partner_type_2,
             partner_2,
             partner_type_3,
             partner_3
        from sup_import_attr
       where supplier   = I_supplier
         and rownum     = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_DETAILS',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   open C_DETAILS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_DETAILS',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   fetch C_DETAILS


    into O_agent,
         O_lading_port,
         O_discharge_port,
         O_factory,
         O_partner_type_1,
         O_partner_1,
         O_partner_type_2,
         O_partner_2,
         O_partner_type_3,
         O_partner_3;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DETAILS',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   close C_DETAILS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END ORDER_IMPORT_DETAILS;
----------------------------------------------------------------------------------------------------
FUNCTION DBT_MEMO_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_dbt_memo_code   IN OUT   sups.dbt_memo_code%TYPE,
                       I_supplier        IN       sups.supplier%TYPE) RETURN BOOLEAN IS

   cursor C_get_dbt_memo_code is
      select dbt_memo_code from sups where supplier = I_supplier;

BEGIN

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DBT_MEMO_CODE',
                    'SUPS',
                    'Supplier: ' || to_char(I_supplier));

   open C_get_dbt_memo_code;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DBT_MEMO_CODE',
                    'SUPS',
                    'Supplier: ' || to_char(I_supplier));

   fetch C_get_dbt_memo_code
      into O_dbt_memo_code;

   if C_get_dbt_memo_code%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_DBT_MEMO_CODE',
                       'SUPS',
                       'Supplier: ' || to_char(I_supplier));
      close C_get_dbt_memo_code;
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER', NULL, NULL, NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DBT_MEMO_CODE',
                    'SUPS',
                    'Supplier: ' || to_char(I_supplier));

   close C_get_dbt_memo_code;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ATTRIB_SQL.DBT_MEMO_CODE',
                                            to_char(SQLCODE));
      return FALSE;
END DBT_MEMO_CODE;
-------------------------------------------------------------------------------------
FUNCTION VALID_BENEFICIARY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_supplier        IN       SUP_IMPORT_ATTR.SUPPLIER%TYPE) RETURN BOOLEAN IS
   L_program VARCHAR2(50) := 'SUPP_ATTRIB_SQL.VALID_BENEFICIARY';
   L_exists  VARCHAR2(1);

   cursor C_BENEFICIARY_IND is
      select 'x'
        from sup_import_attr
       where supplier = I_supplier and beneficiary_ind = 'Y';

BEGIN
   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_BENEFICIARY_IND',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   open C_BENEFICIARY_IND;
   SQL_LIB.SET_MARK('FETCH',
                    'C_BENEFICIARY_IND',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   fetch C_BENEFICIARY_IND
      into L_exists;

   if C_BENEFICIARY_IND%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_BENEFICIARY',
                                            NULL,
                                            NULL,
                                            NULL);
      O_exists        := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_BENEFICIARY_IND',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   close C_BENEFICIARY_IND;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_BENEFICIARY;

-------------------------------------------------------------------------------------
FUNCTION BENEFICIARY_DETAILS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_with_recourse_ind   IN OUT   SUP_IMPORT_ATTR.WITH_RECOURSE_IND%TYPE,
                             O_variance_pct        IN OUT   SUP_IMPORT_ATTR.VARIANCE_PCT%TYPE,
                             O_neg_days            IN OUT   SUP_IMPORT_ATTR.LC_NEG_DAYS%TYPE,
                             O_place_of_expiry     IN OUT   SUP_IMPORT_ATTR.PLACE_OF_EXPIRY%TYPE,
                             O_drafts_at           IN OUT   SUP_IMPORT_ATTR.DRAFTS_AT%TYPE,
                             O_present_terms       IN OUT   SUP_IMPORT_ATTR.PRESENTATION_TERMS%TYPE,
                             O_issuing_bank        IN OUT   SUP_IMPORT_ATTR.ISSUING_BANK%TYPE,
                             O_advising_bank       IN OUT   SUP_IMPORT_ATTR.ADVISING_BANK%TYPE,
                             I_supplier            IN       SUP_IMPORT_ATTR.SUPPLIER%TYPE) RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'SUPP_ATTRIB_SQL.BENEFICIARY_DETAILS';

   cursor C_SUP_IMPORT_ATTR is
      select with_recourse_ind,
             variance_pct,
             lc_neg_days,
             place_of_expiry,
             drafts_at,
             presentation_terms,
             issuing_bank,
             advising_bank
        from sup_import_attr
       where supplier = I_supplier;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUP_IMPORT_ATTR',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   open C_SUP_IMPORT_ATTR;
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUP_IMPORT_ATTR',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   fetch C_SUP_IMPORT_ATTR
      into O_with_recourse_ind, O_variance_pct, O_neg_days, O_place_of_expiry, O_drafts_at, O_present_terms, O_issuing_bank, O_advising_bank;

   if C_SUP_IMPORT_ATTR%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SUP_IMPORT_ATTR',
                       'SUP_IMPORT_ATTR',
                       'Supplier: ' || to_char(I_supplier));
      close C_SUP_IMPORT_ATTR;
      O_error_message := SQL_LIB.CREATE_MSG('NO_SUP_ATTR',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUP_IMPORT_ATTR',
                    'SUP_IMPORT_ATTR',
                    'Supplier: ' || to_char(I_supplier));
   close C_SUP_IMPORT_ATTR;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BENEFICIARY_DETAILS;
--------------------------------------------------------------------------------
FUNCTION NEXT_INV_MGMT_SEQ_NO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_sup_dept_seq_no   IN OUT   NUMBER) RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'SUPP_ATTRIB_SQL.NEXT_INV_MGMT_SEQ_NO';
   L_exists               VARCHAR2(1) := NULL;
   L_first_time           BOOLEAN := TRUE;
   L_wrap_sequence_number SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE := NULL;
   ---
   cursor C_NEXTVAL is
      select inv_mgmt_sequence.NEXTVAL from dual;
   ---
   cursor C_SEQ_EXISTS is
      select 'x'
        from sup_inv_mgmt
       where sup_dept_seq_no = O_sup_dept_seq_no;
   ---
BEGIN
   ---
   LOOP
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_NEXTVAL', 'inv_mgmt_sequence', NULL);
      open C_NEXTVAL;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_NEXTVAL', 'inv_mgmt_sequence', NULL);
      fetch C_NEXTVAL
         into O_sup_dept_seq_no;

      --- handling if sequence runs out of numbers, notify user to contact DBA
      if L_first_time then
         L_wrap_sequence_number := O_sup_dept_seq_no;
         L_first_time           := FALSE;
      elsif (O_sup_dept_seq_no = L_wrap_sequence_number) THEN
         O_error_message := SQL_LIB.CREATE_MSG('INV_MGMT_SEQ_MAXED',
                                               NULL,
                                               NULL,
                                               NULL);
         close C_NEXTVAL;
         return FALSE;
      end if;
      ---
      /* does newly fetched sequence number exist on the SUP_INV_MGMT table? */
      SQL_LIB.SET_MARK('OPEN', 'C_SEQ_EXISTS', 'sup_inv_mgmt', NULL);
      open C_SEQ_EXISTS;
      SQL_LIB.SET_MARK('FETCH', 'C_SEQ_EXISTS', 'sup_inv_mgmt', NULL);
      fetch C_SEQ_EXISTS
         into L_exists;
      ---
      if (C_SEQ_EXISTS%NOTFOUND) then
         SQL_LIB.SET_MARK('CLOSE', 'C_SEQ_EXISTS', 'sup_inv_mgmt', NULL);
         close C_SEQ_EXISTS;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_NEXTVAL',
                          'inv_mgmt_sequence',
                          NULL);
         close C_NEXTVAL;
         ---
         EXIT;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SEQ_EXISTS',
                       'inv_mgmt_sequence',
                       NULL);
      close C_SEQ_EXISTS;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_NEXTVAL', 'sup_inv_mgmt', NULL);
      close C_NEXTVAL;
      ---
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END NEXT_INV_MGMT_SEQ_NO;
---------------------------------------------------------------------------------------
FUNCTION ADDR_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists          IN OUT   BOOLEAN,
                     I_key_value_1     IN       VARCHAR2,
                     I_key_value_2     IN       VARCHAR2) RETURN BOOLEAN IS

   L_exists  VARCHAR2(1);
   L_program VARCHAR2(50) := 'SUPP_ATTRIB_SQL.ADDR_EXISTS';

   cursor C_ADDR_EXISTS is
      select 'x'
        from addr
       where key_value_1 = I_key_value_1 and
             ((I_key_value_2 is null and key_value_2 is null) or
             I_key_value_2 = key_value_2);

BEGIN

   open C_ADDR_EXISTS;
   fetch C_ADDR_EXISTS
      into L_exists;
   if C_ADDR_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_ADDR_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ADDR_EXISTS;
---------------------------------------------------------------------------------------
FUNCTION GET_SUP_PRIMARY_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_addr_key        IN OUT   ADDR.ADDR_KEY%TYPE,
                              I_supplier        IN       SUPS.SUPPLIER%TYPE,
                              I_addr_type       IN       ADDR.ADDR_TYPE%TYPE) RETURN BOOLEAN IS

   cursor C_GET_SUP_PRIM_ADDR is
      select addr_key
        from addr
       where key_value_1 = to_char(I_supplier) and module = 'SUPP' and
             primary_addr_ind = 'Y' and addr_type = I_addr_type;

BEGIN

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SUP_PRIM_ADDR ',
                    'ADDR',
                    'Supplier: ' || to_char(I_supplier) || 'Addr type: ' ||
                    (I_addr_type));
   open C_GET_SUP_PRIM_ADDR;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SUP_PRIM_ADDR ',
                    'ADDR',
                    'Supplier: ' || to_char(I_supplier) || 'Addr type: ' ||
                    (I_addr_type));
   fetch C_GET_SUP_PRIM_ADDR
      into O_addr_key;
   ---
   if C_GET_SUP_PRIM_ADDR%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SUP_PRIM_ADDR ',
                       'ADDR',
                       'Supplier: ' || to_char(I_supplier) ||
                       'Addr type: ' || (I_addr_type));
      close C_GET_SUP_PRIM_ADDR;
      O_error_message := SQL_LIB.CREATE_MSG('ERROR_ADDR_KEY',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SUP_PRIM_ADDR ',
                    'ADDR',
                    'Supplier: ' || to_char(I_supplier) || 'Addr type: ' ||
                    (I_addr_type));
   close C_GET_SUP_PRIM_ADDR;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ATTRIB_SQL.GET_SUP_PRIMARY_ADDR',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_SUP_PRIMARY_ADDR;
---------------------------------------------------------------------------
FUNCTION GET_SUP_PRIMARY_ADDR_SEQ_NO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_addr_seq_no     IN OUT   ADDR.SEQ_NO%TYPE,
                                     I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                     I_addr_type       IN       ADDR.ADDR_TYPE%TYPE) RETURN BOOLEAN IS

   cursor C_GET_SUP_PRIM_ADDR_SEQ_NO is
      select seq_no
        from addr
       where key_value_1 = to_char(I_supplier)
         and module = 'SUPP'
         and primary_addr_ind = 'Y'
         and addr_type = I_addr_type;

BEGIN

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_SUP_PRIM_ADDR_SEQ_NO;
   fetch C_GET_SUP_PRIM_ADDR_SEQ_NO
      into O_addr_seq_no;
   ---
   if C_GET_SUP_PRIM_ADDR_SEQ_NO%NOTFOUND then
      close C_GET_SUP_PRIM_ADDR_SEQ_NO;
      O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   close C_GET_SUP_PRIM_ADDR_SEQ_NO;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ATTRIB_SQL.GET_SUP_PRIMARY_ADDR_SEQ_NO',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_SUP_PRIMARY_ADDR_SEQ_NO;
---------------------------------------------------------------------------
FUNCTION GET_LEAD_TIME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_lead_time       IN OUT   SUPS.DEFAULT_ITEM_LEAD_TIME%TYPE,
                       I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS

   cursor C_LEAD_TIME is
      select default_item_lead_time
        from sups
       where supplier = I_supplier;

BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   open C_LEAD_TIME;
   fetch C_LEAD_TIME
      into O_lead_time;
   close C_LEAD_TIME;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ATTRIB_SQL.GET_LEAD_TIME',
                                            to_char(SQLCODE));
      return FALSE;
END GET_LEAD_TIME;
-------------------------------------------------------------------------------------
FUNCTION GET_BRACKET_COSTING_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_bracket_ind     IN OUT   SUPS.BRACKET_COSTING_IND%TYPE,
                                 I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'SUPP_ATTRIB_SQL.GET_BRACKET_COSTING_IND';

   cursor C_GET_BRACKET_IND is
      select bracket_costing_ind from sups where supplier = I_supplier;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_BRACKET_IND',
                    'sups',
                    'supplier: ' || I_supplier);
   open C_GET_BRACKET_IND;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_BRACKET_IND',
                    'sups',
                    'supplier: ' || I_supplier);
   fetch C_GET_BRACKET_IND
      into O_bracket_ind;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_BRACKET_IND',
                    'sups',
                    'supplier: ' || I_supplier);
   close C_GET_BRACKET_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_BRACKET_COSTING_IND;
--------------------------------------------------------------------------
FUNCTION NEXT_SUPPLIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_supplier_id     IN OUT   SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS

   L_program VARCHAR2( 64 ) := 'SUPP_ATTRIB_SQL.NEXT_SUPPLIER';

   L_supplier_id        NUMBER := 0;
   L_prev_supplier_id   SUPS.SUPPLIER%TYPE := 0;
   L_supplier_exists    VARCHAR2(1):= 'N';
   L_first_fetch_ind    VARCHAR2(1) := 'Y';

   cursor C_CHECK_SUPPLIER is
      select 'Y'
        from sups
       where supplier = L_supplier_id
         and rownum = 1;

   cursor C_NEXT_SEQUENCE is
      select supplier_seq.NEXTVAL
        from dual;

BEGIN
   LOOP
     L_supplier_exists := 'N';   
     SQL_LIB.SET_MARK( 'OPEN','C_next_sequence','supplier',NULL );
     open C_NEXT_SEQUENCE;
     SQL_LIB.SET_MARK( 'FETCH','C_next_sequence','supplier',NULL );
     fetch C_NEXT_SEQUENCE into L_supplier_id;
     SQL_LIB.SET_MARK( 'CLOSE','C_next_sequence','supplier', NULL );
     close C_NEXT_SEQUENCE;
     if L_first_fetch_ind = 'Y' then
        L_prev_supplier_id := L_supplier_id;
        L_first_fetch_ind := 'N';
     elsif L_supplier_id = L_prev_supplier_id then
        O_error_message := SQL_LIB.CREATE_MSG( 'NO_SEQ_NO_AVAIL',
                                               NULL,
                                               NULL,
                                               NULL );
        return FALSE;
     end if;
     SQL_LIB.SET_MARK( 'OPEN', 'C_CHECK_SUPPLIER', 'sups', NULL );
     open C_CHECK_SUPPLIER;
     SQL_LIB.SET_MARK( 'FETCH','C_CHECK_SUPPLIER','sups', NULL );
     fetch C_CHECK_SUPPLIER into L_supplier_exists;
     SQL_LIB.SET_MARK( 'CLOSE','C_CHECK_SUPPLIER','sups', NULL );
     close C_CHECK_SUPPLIER;

     if L_supplier_exists = 'N' then
        O_supplier_id := L_supplier_id;
        return TRUE;
     end if;
   END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char( SQLCODE ) );
      return FALSE;
END NEXT_SUPPLIER;

--------------------------------------------------------------------------
FUNCTION GET_SUPS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_sups_record     IN OUT   SUPS%ROWTYPE,
                   I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS

   L_program varchar2(60) := 'SUPP_ATTRIB_SQL.GET_SUPS';

   cursor C_SUPS is
      select *
        from sups
       where supplier = I_supplier;

BEGIN

   if I_supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_supplier,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_SUPS',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   open C_SUPS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_SUPS',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   fetch C_SUPS into O_sups_record;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUPS',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   close C_SUPS;

   if O_sups_record.supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INV_SUPPLIER',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SUPS;
--------------------------------------------------------------------------------------------
-- Function: CHECK_SUPPLIER_SITES
-- Purpose : This function will return True if the input supplier is a supplier site.
--------------------------------------------------------------------------------------------

FUNCTION CHECK_SUPPLIER_SITES (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_supplier        IN       SUPS.SUPPLIER%TYPE) return BOOLEAN IS

   cursor C_CHECK_SUPPLIER_SITE is
   select 'Y'
     from sups
    where supplier = I_supplier
      and supplier_parent is NOT NULL;

   L_program               VARCHAR2(64) := 'SUPP_ATTRIB_SQL.CHECK_SUPPLIER_SITES';
   L_fetch_cursor_value    VARCHAR2(1);

BEGIN
   O_exists := TRUE;

   open C_CHECK_SUPPLIER_SITE;

   fetch C_CHECK_SUPPLIER_SITE into L_fetch_cursor_value;

   if C_CHECK_SUPPLIER_SITE%NOTFOUND then
      O_exists := FALSE;
      O_error_message := sql_lib.create_msg('INV_SUPPLIER_SITE',
                                            null,
                                            null,
                                            null);
   end if;

   close C_CHECK_SUPPLIER_SITE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END CHECK_SUPPLIER_SITES;

--------------------------------------------------------------------------------------------
-- Function: GET_SUPP_PARENT_DESC
-- Purpose : This function will return the Supplier_parent and its description
--           for the input supplier site.
--------------------------------------------------------------------------------------------

FUNCTION GET_SUPP_PARENT_DESC (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_supplier_parent_name   IN OUT   SUPS.SUP_NAME%TYPE,
                               O_supplier_parent        IN OUT   SUPS.SUPPLIER_PARENT%TYPE,
                               I_supplier               IN       SUPS.SUPPLIER%TYPE) return BOOLEAN IS

   L_program   VARCHAR2(64) := 'SUPP_ATTRIB_SQL.GET_SUPP_PARENT_DESC';
   L_supp_rec  SUPS%ROWTYPE;

BEGIN
   if SUPP_ATTRIB_SQL.GET_SUPS(O_error_message,
                               L_supp_rec,
                               I_supplier) = FALSE then
      return FALSE;
   end if;

   if L_supp_rec.supplier_parent is NULL then
      O_error_message := sql_lib.create_msg('INV_SUPPLIER_SITE',
                                            null,
                                            null,
                                            null);
      return FALSE;
   end if;

   if SUPP_ATTRIB_SQL.GET_SUPP_DESC(O_error_message,
                                    L_supp_rec.supplier_parent,
                                    O_supplier_parent_name) = FALSE then
      return FALSE;
   end if;

   O_supplier_parent := L_supp_rec.supplier_parent;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END GET_SUPP_PARENT_DESC;

--------------------------------------------------------------------------------------------
-- Function: CHECK_ORG_UNIT_OPEN_PO_EXIST
-- Purpose : This function will return true if a PO exists for the Supplier passed as input
--           that is not closed for a location associated to the Org Unit Id passed as input
--------------------------------------------------------------------------------------------

FUNCTION CHECK_ORG_UNIT_OPEN_PO_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists          IN OUT   BOOLEAN,
                                       I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                       I_org_unit_id     IN       ORG_UNIT.ORG_UNIT_ID%TYPE) return BOOLEAN IS

   cursor C_CHECK_OPEN_PO is
   select 'Y'
     from  ordhead ohe,
           ordloc olo,
           (select store location,
                   'S' location_type
              from store
             where store_type  in ('C','F')
               and org_unit_id = I_org_unit_id
            union all
            select wh location,
                   'W' location_type
              from wh
             where org_unit_id = I_org_unit_id) loc
    where ohe.supplier = I_supplier
      and ohe.orig_approval_date is not NULL
      and olo.order_no = ohe.order_no
      and olo.location = loc.location
      and olo.loc_type = loc.location_type
      and olo.qty_ordered > NVL(olo.qty_received,0);

   L_program               VARCHAR2(64) := 'SUPP_ATTRIB_SQL.CHECK_ORG_UNIT_OPEN_PO_EXIST';
   L_fetch_cursor_value    VARCHAR2(1);

BEGIN
   O_exists := TRUE;

   open C_CHECK_OPEN_PO;

   fetch C_CHECK_OPEN_PO into L_fetch_cursor_value;

   if C_CHECK_OPEN_PO%NOTFOUND then
      O_exists := FALSE;
   end if;

   close C_CHECK_OPEN_PO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END CHECK_ORG_UNIT_OPEN_PO_EXIST;

--------------------------------------------------------------------------------------------
-- Function: CHECK_SUPPLIER_SITE_EXIST
-- Purpose : This function will return true if a supplier site is associated with the
--           Supplier passed as input with status and DSD ind same as the values passed
--           as input
--------------------------------------------------------------------------------------------

FUNCTION CHECK_SUPPLIER_SITE_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                    I_status          IN       SUPS.SUP_STATUS%TYPE   DEFAULT NULL,
                                    I_dsd_ind         IN       SUPS.DSD_IND%TYPE      DEFAULT NULL) return BOOLEAN IS

   cursor C_CHECK_SUPPLIER_SITE_EXISTS is
   select 'Y'
     from sups
    where supplier_parent = I_supplier
      and sup_status      = NVL(I_status,sup_status)
      and dsd_ind         = NVL(I_dsd_ind,dsd_ind);

   L_program              VARCHAR2(64) := 'SUPP_ATTRIB_SQL.CHECK_SUPPLIER_SITE_EXIST';
   L_fetch_cursor_value   VARCHAR2(1);

BEGIN
   O_exists := TRUE;

   open C_CHECK_SUPPLIER_SITE_EXISTS;

   fetch C_CHECK_SUPPLIER_SITE_EXISTS into L_fetch_cursor_value;

   if C_CHECK_SUPPLIER_SITE_EXISTS%NOTFOUND then
      O_exists := FALSE;
      O_error_message := sql_lib.create_msg('NO_SUPPLIER_SITE_EXIST',
                                            I_supplier,
                                            null,
                                            null);
   end if;

   close C_CHECK_SUPPLIER_SITE_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;

END CHECK_SUPPLIER_SITE_EXIST;
--------------------------------------------------------------------------------------------
FUNCTION GET_IMPORT_ID (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_import_id       IN OUT   SUPS_IMP_EXP.IMPORT_ID%TYPE,
                        O_import_type     IN OUT   SUPS_IMP_EXP.IMPORT_TYPE%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS

   cursor C_IMPORT_ID is
    select import_id,
           import_type
      from sups_imp_exp
     where supplier = I_supplier
       and default_ind = 'Y';

   L_program              VARCHAR2(50) := 'SUPP_ATTRIB_SQL.GET_IMPORT_ID';

BEGIN

   O_exists := FALSE;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER',
                                            I_supplier,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_IMPORT_ID',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   open C_IMPORT_ID;
   SQL_LIB.SET_MARK('FETCH',
                    'C_IMPORT_ID',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   fetch C_IMPORT_ID into O_import_id,
                          O_import_type;

      if O_import_id is NULL then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_IMPORT_ID',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   close C_IMPORT_ID;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_IMPORT_ID;
--------------------------------------------------------------------------------------------
FUNCTION GET_ROUTING_LOC (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_routing_loc_id   IN OUT   SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE,
                          O_exists           IN OUT   BOOLEAN,
                          I_supplier         IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS

   cursor C_ROUTING_LOC is
   select routing_loc_id
     from sups_routing_loc
    where supplier = I_supplier
      and default_ind = 'Y';

   L_program              VARCHAR2(50) := 'SUPP_ATTRIB_SQL.GET_ROUTING_LOC';

BEGIN

   O_exists := FALSE;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER',
                                            I_supplier,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_ROUTING_LOC',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   open C_ROUTING_LOC;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ROUTING_LOC',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   fetch C_ROUTING_LOC into O_routing_loc_id;

      if O_routing_loc_id is NULL then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ROUTING_LOC',
                    NULL,
                    'SUPPLIER: '||I_supplier);
   close C_ROUTING_LOC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END GET_ROUTING_LOC;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_SUP_IMPORT_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists        IN OUT   BOOLEAN,
                                   I_supplier      IN       SUPS.SUPPLIER%TYPE,
                                   I_importer      IN       SUPS_IMP_EXP.IMPORT_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_IMPORT is
      select 'x'
        from sups_imp_exp s
       where s.supplier = I_supplier
         and (s.import_id = I_importer
              or I_importer is NULL);

   L_program   VARCHAR2(64) := 'SUPP_ATTRIB_SQL.CHECK_SUP_IMPORT_EXIST';
   L_value     VARCHAR2(1);

BEGIN
   O_exists := FALSE;

   open C_IMPORT;
   SQL_LIB.SET_MARK('OPEN',
                    'C_IMPORT',
                    'SUPS_IMP_EXP',
                    'SUPPLIER : ' || I_supplier);

   fetch C_IMPORT into L_value;
   SQL_LIB.SET_MARK('FETCH',
                    'C_IMPORT',
                    'SUPS_IMP_EXP',
                    'SUPPLIER : ' || I_supplier);

   if L_value is NOT NULL then
      O_exists := TRUE;
   end if;

   close C_IMPORT;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_IMPORT',
                    'SUPS_IMP_EXP',
                    'SUPPLIER : ' || I_supplier);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_SUP_IMPORT_EXIST;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_RLOC_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_supplier        IN       SUPS.SUPPLIER%TYPE,
                           I_routing_loc     IN       SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE)
RETURN BOOLEAN IS

   cursor C_ROUTING_LOC is
      select 'x'
        from sups_routing_loc s
       where s.supplier = I_supplier
         and s.routing_loc_id = I_routing_loc;

   L_program   VARCHAR2(64) := 'SUPP_ATTRIB_SQL.CHECK_RLOC_EXIST';
   L_value     VARCHAR2(1);

BEGIN
   O_exists := FALSE;

   open C_ROUTING_LOC;
   SQL_LIB.SET_MARK('OPEN',
                    'C_ROUTING_LOC',
                    'SUPS_ROUTING_LOC',
                    'SUPPLIER : ' || I_supplier);

   fetch C_ROUTING_LOC into L_value;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ROUTING_LOC',
                    'SUPS_ROUTING_LOC',
                    'SUPPLIER : ' || I_supplier);

   if L_value is NOT NULL then
      O_exists := TRUE;
   end if;

   close C_ROUTING_LOC;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ROUTING_LOC',
                    'SUPS_ROUTING_LOC',
                    'SUPPLIER : ' || I_supplier);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_RLOC_EXIST;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_ENT_IN_USE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   VARCHAR2,
                          I_import_id       IN       SUPS_IMP_EXP.IMPORT_ID%TYPE,
                          I_supplier        IN       SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'SUPP_ATTRIB_SQL.CHECK_ENT_IN_USE';

   cursor C_IE_ON_ORDER is
      select 'x'
        from ordhead
       where import_id = I_import_id
         and supplier = I_supplier
         and rownum = 1;

BEGIN

   open C_IE_ON_ORDER;
   fetch C_IE_ON_ORDER into O_exists;
   close C_IE_ON_ORDER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ENT_IN_USE;
--------------------------------------------------------------------------------------------
FUNCTION CHECK_RL_IN_USE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT   VARCHAR2,
                         I_routing_id      IN       SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE,
                         I_supplier        IN       SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'SUPP_ATTRIB_SQL.CHECK_RL_IN_USE';

   cursor C_RL_ON_ORDER is
      select 'x'
        from ordhead
       where routing_loc_id = I_routing_id
         and supplier = I_supplier
         and rownum = 1;

BEGIN

   open C_RL_ON_ORDER;
   fetch C_RL_ON_ORDER into O_exists;
   close C_RL_ON_ORDER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_RL_IN_USE;
--------------------------------------------------------------------------------------------
-- Function: UPDATE_SUPPLIER_SITE
-- Purpose : This function will update the status of supplier sites under a supplier
--           based on the status of the supplier.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_SUPPLIER_SITE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_supplier        IN       SUPS.SUPPLIER%TYPE,
                              I_status          IN       SUPS.SUP_STATUS%TYPE)
RETURN BOOLEAN IS

   cursor C_CHECK_ANY_SUP_SITE_EXISTS is
      select 'Y'
        from sups
       where supplier_parent = I_supplier;

      cursor C_CHECK_FINANCIAL_AP is
      select financial_ap
         from product_config_options;

   L_program              VARCHAR2(64) := 'SUPP_ATTRIB_SQL.UPDATE_SUPPLIER_SITE';
   L_fetch_cursor_value   VARCHAR2(1);
   L_financial_ap  VARCHAR2(1);

BEGIN
   open C_CHECK_ANY_SUP_SITE_EXISTS;
   open C_CHECK_FINANCIAL_AP;

   fetch C_CHECK_ANY_SUP_SITE_EXISTS into L_fetch_cursor_value;
   fetch C_CHECK_FINANCIAL_AP into L_financial_ap;

   if C_CHECK_ANY_SUP_SITE_EXISTS%FOUND then
      update sups
         set status_upd_by_rms = DECODE(L_financial_ap, NULL, NULL,DECODE(I_status, 'I', DECODE(sup_status, 'A','Y'),NULL)),
         sup_status = I_status
       where supplier_parent = I_supplier;
   end if;

   close C_CHECK_ANY_SUP_SITE_EXISTS;
   close C_CHECK_FINANCIAL_AP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END UPDATE_SUPPLIER_SITE;
--------------------------------------------------------------------------------------------
-- Function: UPDATE_STATUS_UPD_BY_RMS
-- Purpose : This function will update the status_upd_by_rms flag of supplier / supplier site 
--           when updated from RMS.
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS_UPD_BY_RMS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_supplier         IN       SUPS.SUPPLIER%TYPE,
                                  I_status           IN       SUPS.SUP_STATUS%TYPE,
                                  I_old_status       IN       SUPS.SUP_STATUS%TYPE)
   return BOOLEAN IS
   
   L_program              VARCHAR2(64) := 'SUPP_ATTRIB_SQL.UPDATE_STATUS_UPD_BY_RMS';

BEGIN

   update sups
      set status_upd_by_rms = DECODE(I_status, 'I', DECODE(I_old_status, 'A','Y'), NULL)
    where supplier = I_supplier;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            To_Char(SQLCODE));
      return FALSE;
END UPDATE_STATUS_UPD_BY_RMS;
--------------------------------------------------------------------------------------------
END SUPP_ATTRIB_SQL;
/
