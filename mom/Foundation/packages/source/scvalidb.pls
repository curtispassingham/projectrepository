
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SUBCLASS_VALIDATE_SQL AS

FUNCTION EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
               I_dept          IN     NUMBER,
               I_class         IN     NUMBER,
               I_subclass      IN     NUMBER,
               O_exist         IN OUT BOOLEAN)
         return BOOLEAN is

   L_program    VARCHAR2(64) := 'SUBCLASS_VALIDATE_SQL.EXIST';
   L_subclass   VARCHAR2(1);

-- This function determines if the entered subclass exists

   cursor C_SUBCLASS is
      select 'x'
        from subclass
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_SUBCLASS','SUBCLASS','Subclass: '||to_char
   (I_subclass)||' Class: '||to_char(I_class)||' Dept: '||to_char(I_dept));
   open C_SUBCLASS;
   SQL_LIB.SET_MARK('FETCH','C_SUBCLASS','SUBCLASS','Subclass: '||to_char
   (I_subclass)||' Class: '||to_char(I_class)||' Dept: '||to_char(I_dept));
   fetch C_SUBCLASS into L_subclass;
   if C_SUBCLASS%NOTFOUND then
      O_exist := FALSE;
      O_error_message := sql_lib.create_msg('INV_SUB_4_CLS_DPT',
                                             I_subclass,
                                             I_class,
                                             I_dept);
   else
      O_exist := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_SUBCLASS','SUBCLASS','Subclass: '||to_char
   (I_subclass)||' Class: '||to_char(I_class)||' Dept: '||to_char(I_dept));
   close C_SUBCLASS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   RETURN FALSE;
END EXIST;
-------------------------------------------------------------------
FUNCTION CLASS_SUBCLASS_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_dept          IN     DEPS.DEPT%TYPE,
                              I_class         IN     CLASS.CLASS%TYPE,
                              O_exist         IN OUT BOOLEAN)
RETURN BOOLEAN IS
   L_subclass   VARCHAR2(1)  := NULL;
   L_program    VARCHAR2(60) := 'SUBCLASS_VALIDATE_SQL.CLASS_SUBCLASS_EXIST';

   CURSOR C_CLASS_SUBCLASS_EXIST IS
      SELECT 'x'
        FROM subclass
       WHERE dept = I_dept
         AND class = I_class;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_CLASS_SUBCLASS_EXIST',
                    'SUBCLASS',
                    ' Class: '||to_char(I_class)||' Dept: '||to_char(I_dept));
   open C_CLASS_SUBCLASS_EXIST;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CLASS_SUBCLASS_EXIST',
                    'SUBCLASS',
                    ' Class: '||to_char(I_class)||' Dept: '||to_char(I_dept));
   fetch C_CLASS_SUBCLASS_EXIST into L_subclass;
   if L_subclass is NULL then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CLASS_SUBCLASS_EXIST',
                    'SUBCLASS',
                    ' Class: '||to_char(I_class)||' Dept: '||to_char(I_dept));
   close C_CLASS_SUBCLASS_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      RETURN FALSE;
END CLASS_SUBCLASS_EXIST;
-------------------------------------------------------------------
END SUBCLASS_VALIDATE_SQL;
/
