CREATE OR REPLACE PACKAGE BODY RMSMFM_STORE AS

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------

TYPE ROWID_TBL IS TABLE OF ROWID INDEX BY BINARY_INTEGER;
--
LP_error_status   VARCHAR2(1):= null;


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK(O_error_msg        OUT VARCHAR2,
                        O_queue_locked     OUT BOOLEAN,
                        I_store_key_rec IN     STORE_KEY_REC)
RETURN BOOLEAN;

FUNCTION PROCESS_QUEUE_RECORD(O_error_msg        OUT VARCHAR2,
                              O_message       IN OUT NOCOPY RIB_OBJECT,
                              O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id       OUT RIB_BUSOBJID_TBL,
                              O_message_type  IN OUT STORE_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_store_key_rec IN     STORE_KEY_REC,
                              I_hdr_published IN     STORE_PUB_INFO.PUBLISHED%TYPE,
                              I_rowid         IN     ROWID)
RETURN BOOLEAN;

FUNCTION DELETE_QUEUE_REC(O_error_msg    OUT VARCHAR2,
                          I_rowid     IN     ROWID)
RETURN BOOLEAN;

FUNCTION MAKE_CREATE(O_error_msg        OUT VARCHAR2,
                     O_message       IN OUT NOCOPY RIB_OBJECT,
                     O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                     I_store_key_rec IN     STORE_KEY_REC,
                     I_rowid         IN     ROWID)
RETURN BOOLEAN;

FUNCTION BUILD_HEADER_OBJECT(O_error_msg            OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_routing_info      IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_rib_storedesc_rec    OUT NOCOPY "RIB_StoreDesc_REC",
                             I_store_key_rec     IN     STORE_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_HEADER_OBJECT(O_error_msg           OUT VARCHAR2,
                             O_routing_info     IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_rib_storeref_rec    OUT NOCOPY "RIB_StoreRef_REC",
                             I_store_key_rec    IN     STORE_KEY_REC)
RETURN BOOLEAN;

PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT VARCHAR2,
                        O_error_msg     IN OUT VARCHAR2,
                        O_message       IN OUT NOCOPY RIB_OBJECT,
                        O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                        O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                        O_message_type  IN OUT STORE_MFQUEUE.MESSAGE_TYPE%TYPE,
                        I_seq_no        IN     STORE_MFQUEUE.SEQ_NO%TYPE,
                        I_store_key_rec IN     STORE_KEY_REC);

FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg               IN OUT VARCHAR2,
                              O_rib_storeaddrdtl_tbl    IN OUT NOCOPY "RIB_AddrDesc_TBL",
                              O_store_mfqueue_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_store_mfqueue_size      IN OUT BINARY_INTEGER,
                              O_store_addrdtl_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_store_addrdtl_size      IN OUT BINARY_INTEGER,
                              O_delete_row_ind          IN OUT VARCHAR2,
                              I_message_type            IN     STORE_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_store_key_rec           IN     STORE_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_SINGLE_DETAIL(O_error_msg             IN OUT VARCHAR2,
                             O_rib_storeaddrdtl_tbl  IN OUT NOCOPY "RIB_AddrDesc_TBL",
                             O_rib_storeaddrdtl_rec  IN OUT NOCOPY "RIB_AddrDesc_REC",
                             I_state_name            IN     STATE.DESCRIPTION%TYPE,
                             I_country_name          IN     COUNTRY.COUNTRY_DESC%TYPE,
                             I_addr_key              IN     ADDR.ADDR_KEY%TYPE,
                             I_addr_type             IN     ADDR.ADDR_TYPE%TYPE,
                             I_primary_ind           IN     ADD_TYPE_MODULE.PRIMARY_IND%TYPE,
                             I_primary_addr_ind      IN     ADDR.PRIMARY_ADDR_IND%TYPE,
                             I_add_1                 IN     ADDR.ADD_1%TYPE,
                             I_add_2                 IN     ADDR.ADD_2%TYPE,
                             I_add_3                 IN     ADDR.ADD_3%TYPE,
                             I_city                  IN     ADDR.CITY%TYPE,
                             I_state                 IN     ADDR.STATE%TYPE,
                             I_country_id            IN     ADDR.COUNTRY_ID%TYPE,
                             I_post                  IN     ADDR.POST%TYPE,
                             I_jurs_code             IN     ADDR.JURISDICTION_CODE%TYPE,
                             I_contact_name          IN     ADDR.CONTACT_NAME%TYPE,
                             I_contact_phone         IN     ADDR.CONTACT_PHONE%TYPE,
                             I_contact_telex         IN     ADDR.CONTACT_TELEX%TYPE,
                             I_contact_fax           IN     ADDR.CONTACT_FAX%TYPE,
                             I_contact_email         IN     ADDR.CONTACT_EMAIL%TYPE,
                             I_oracle_vendor_site_id IN     ADDR.ORACLE_VENDOR_SITE_ID%TYPE,
                             I_county                IN     ADDR.COUNTY%TYPE)
RETURN BOOLEAN;

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg            OUT VARCHAR2,
                                     O_rib_storedesc_rec IN OUT NOCOPY "RIB_StoreDesc_REC",
                                     I_message_type      IN     STORE_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_store_key_rec     IN     STORE_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg           OUT VARCHAR2,
                                     O_rib_storeref_rec IN OUT NOCOPY "RIB_StoreRef_REC",
                                     I_store_key_rec    IN     STORE_KEY_REC)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg           OUT VARCHAR2,
                I_message_type     IN     VARCHAR2,
                I_store_key_rec    IN     STORE_KEY_REC,
                I_addr_publish_ind IN     ADDR.PUBLISH_IND%TYPE)
RETURN BOOLEAN IS

L_store              STORE.STORE%TYPE := null;
L_initial_pub        VARCHAR2(1) := null;
--
L_pricing_loc        STORE_ADD.PRICE_STORE%TYPE := null;

cursor C_STORE is
select s.store
  from store s
 where s.store = I_store_key_rec.store;

cursor C_STORE_PUB_INFO is
select spi.store,
       spi.published
  from store_pub_info spi
 where spi.store = I_store_key_rec.store;

cursor C_STORE_ADD is
select sa.price_store
  from store_add sa
 where sa.store = I_store_key_rec.store;

BEGIN

   -- for store detail messages, do nothing until the
   -- storeadd batch program has added the store

   if I_message_type in (DTL_ADD, DTL_UPD, DTL_DEL) then
      L_store := null;

      open C_STORE;
      fetch C_STORE into L_store;
      close C_STORE;

      if L_store is null then
         return TRUE;
      end if;
   end if;

   --

   if I_message_type != HDR_ADD then
      -- get store pub info

      L_store := null;
      L_initial_pub := null;

      open C_STORE_PUB_INFO;
      fetch C_STORE_PUB_INFO into L_store,
                                  L_initial_pub;
      close C_STORE_PUB_INFO;

      if L_store is null then
         O_error_msg := SQL_LIB.CREATE_MSG('PUB_INFO_NOT_FOUND',
                                           'store_pub_info',
                                           I_store_key_rec.store,
                                           null);
         return FALSE;
      end if;

      -- delete any prior messages on the queue that will be
      -- redundant once we add our new message to the queue

      if I_message_type = DTL_UPD then
         delete from store_mfqueue
          where store = I_store_key_rec.store
            and addr_key = I_store_key_rec.addr_key
            and message_type = DTL_UPD;
      elsif I_message_type = DTL_DEL then
         delete from store_mfqueue
          where store = I_store_key_rec.store
            and addr_key = I_store_key_rec.addr_key;
      elsif I_message_type = HDR_UPD then
         delete from store_mfqueue
          where store = I_store_key_rec.store
            and message_type = HDR_UPD;
      elsif I_message_type = HDR_DEL then
         delete from store_mfqueue
          where store = I_store_key_rec.store;
         if L_initial_pub = 'N' then 
            delete from store_pub_info
            where store = I_store_key_rec.store;
         end if;    
      end if;

   elsif I_message_type = HDR_ADD then

      open C_STORE_ADD;
      fetch C_STORE_ADD into L_pricing_loc;
      close C_STORE_ADD;

      insert into store_pub_info(store,
                                 published,
                                 store_type,
                                 pricing_loc,
                                 stockholding_ind)
                          values(I_store_key_rec.store,
                                 'N',
                                 I_store_key_rec.store_type,
                                 L_pricing_loc,
                                 I_store_key_rec.stockholding_ind);
   end if;

   --

   if (I_addr_publish_ind = 'Y' or I_message_type!= DTL_DEL) and NOT(NVL(L_initial_pub,'N') = 'N' and I_message_type =  HDR_DEL) then
      insert into store_mfqueue(seq_no,
                                store,
                                addr_key,
                                message_type,
                                family,
                                custom_message_type,
                                pub_status,
                                transaction_number,
                                transaction_time_stamp)
                         values(store_mfsequence.nextval,
                                I_store_key_rec.store,
                                I_store_key_rec.addr_key,
                                I_message_type,
                                RMSMFM_STORE.FAMILY,
                                null,
                                'U',
                                I_store_key_rec.store,
                                SYSDATE);
   end if;

   LP_error_status := API_CODES.SUCCESS;
   return TRUE;

EXCEPTION

   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(LP_error_status,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_STORE.ADDTOQ');
   return FALSE;                    

END ADDTOQ;
--------------------------------------------------------------------------------

PROCEDURE GETNXT(O_status_code     OUT VARCHAR2,
                 O_error_msg       OUT VARCHAR2,
                 O_message_type    OUT VARCHAR2,
                 O_message         OUT RIB_OBJECT,
                 O_bus_obj_id      OUT RIB_BUSOBJID_TBL,
                 O_routing_info    OUT RIB_ROUTINGINFO_TBL,
                 I_num_threads     IN     NUMBER DEFAULT 1,
                 I_thread_val      IN     NUMBER DEFAULT 1) IS

L_store_queue_rec   STORE_MFQUEUE%ROWTYPE;
L_store             STORE.STORE%TYPE := null;
L_store_key_rec     RMSMFM_STORE.STORE_KEY_REC := null;
L_pub_status        STORE_MFQUEUE.PUB_STATUS%TYPE := null;
L_rowid             ROWID := null;
L_hdr_published     STORE_PUB_INFO.PUBLISHED%TYPE := null;

L_store_type        VARCHAR2(1);
L_hosp              VARCHAR2(1) := 'N';
L_keep_queue        BOOLEAN := FALSE;
L_queue_locked      BOOLEAN := FALSE;

cursor C_QUEUE is
  select seq_no,
         store,
         addr_key,
         message_type,
         pub_status,
         rowid
    from store_mfqueue
   where pub_status = 'U'
order by seq_no;

cursor C_HOSP is
select 'Y'
 from store_mfqueue
where store = L_store
  and pub_status = API_CODES.HOSPITAL;

cursor C_HDR_PUB is
select spi.published,
       spi.store_type,
       spi.pricing_loc,
       s.currency_code,
       spi.stockholding_ind
  from store_pub_info spi,
       store s
 where s.store (+) = spi.pricing_loc
   and spi.store = L_store;



   
BEGIN  
   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   loop
      O_message := null;
      open C_QUEUE;
      fetch C_QUEUE into L_store_queue_rec.seq_no,
                         L_store_queue_rec.store,
                         L_store_queue_rec.addr_key,
                         L_store_queue_rec.message_type,
                         L_store_queue_rec.pub_status,
                         L_rowid;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_QUEUE;
      

      
      O_message_type := L_store_queue_rec.message_type;
      L_store := L_store_queue_rec.store;
      L_store_key_rec.store := L_store_queue_rec.store;

      
      L_store_key_rec.addr_key := L_store_queue_rec.addr_key;

      if LOCK_THE_BLOCK(O_error_msg,
                        L_queue_locked,
                        L_store_key_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_queue_locked = FALSE then
         open  C_HOSP;
         fetch C_HOSP into L_hosp;
         close C_HOSP;

         if L_hosp = 'Y' then
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP', null, null, null);
            raise PROGRAM_ERROR;
         end if;

         open  C_HDR_PUB;
         fetch C_HDR_PUB into L_hdr_published,
                              L_store_key_rec.store_type,
                              L_store_key_rec.pricing_loc,
                              L_store_key_rec.pricing_loc_curr,
                              L_store_key_rec.stockholding_ind;
         close C_HDR_PUB;

         if PROCESS_QUEUE_RECORD(O_error_msg,
                                 O_message,
                                 O_routing_info,
                                 O_bus_obj_id,
                                 O_message_type,
                                 L_store_key_rec,
                                 L_hdr_published,
                                 L_rowid) = FALSE then
            raise PROGRAM_ERROR;
         end if;

         if L_store_queue_rec.message_type = HDR_DEL and
         O_message is null then
            null;  -- skip the record if not sending a HDR_DEL message
         else
            exit;  -- Break out of loop after processing record
         end if;
      end if;
   end loop;

   if O_message IS null then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_store);
   end if;

EXCEPTION

   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    O_message_type,
                    L_store_queue_rec.seq_no,
                    L_store_key_rec);

END GETNXT;
--------------------------------------------------------------------------------

PROCEDURE PUB_RETRY(O_status_code     OUT VARCHAR2,
                    O_error_msg       OUT VARCHAR2,
                    O_message_type IN OUT VARCHAR2,
                    O_message         OUT RIB_OBJECT,
                    O_bus_obj_id   IN OUT RIB_BUSOBJID_TBL,
                    O_routing_info IN OUT RIB_ROUTINGINFO_TBL,
                    I_ref_object   IN     RIB_OBJECT) IS

L_seq_no            STORE_MFQUEUE.SEQ_NO%TYPE := null;
L_hdr_published     STORE_PUB_INFO.PUBLISHED%TYPE := null;
L_rowid             ROWID := null;
L_store_key_rec     STORE_KEY_REC := null;
L_store_queue_rec   STORE_MFQUEUE%ROWTYPE;
L_keep_queue        BOOLEAN := FALSE;
L_queue_locked      BOOLEAN := FALSE;

cursor C_RETRY_QUEUE is
select sq.seq_no,
       sq.store,
       spi.published hdr_published,
       spi.store_type,
       sq.addr_key,
       sq.message_type,
       sq.pub_status,
       spi.pricing_loc,
       s.currency_code,
       sq.rowid,
       spi.stockholding_ind
  from store_mfqueue sq,
       store_pub_info spi,
       store s
 where s.store (+) = spi.pricing_loc
   and sq.seq_no = L_seq_no
   and sq.store = spi.store;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   -- get info from routing info
   -- assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;
   O_message := null;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_store_queue_rec.seq_no,
                            L_store_queue_rec.store,
                            L_hdr_published,
                            L_store_key_rec.store_type,
                            L_store_queue_rec.addr_key,
                            L_store_queue_rec.message_type,
                            L_store_queue_rec.pub_status,
                            L_store_key_rec.pricing_loc,
                            L_store_key_rec.pricing_loc_curr,
                            L_rowid,
                            L_store_key_rec.stockholding_ind;
   close C_RETRY_QUEUE;

   if L_store_queue_rec.store is null then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;
   L_store_key_rec.store := L_store_queue_rec.store;
   L_store_key_rec.addr_key := L_store_queue_rec.addr_key;

   if LOCK_THE_BLOCK(O_error_msg,
                     L_queue_locked,
                     L_store_key_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then
      if PROCESS_QUEUE_RECORD(O_error_msg,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              L_store_queue_rec.message_type,
                              L_store_key_rec,
                              L_hdr_published,
                              L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      O_message_type := L_store_queue_rec.message_type;

      if O_message IS null then
         O_status_code := API_CODES.NO_MSG;
      else
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_store_queue_rec.store);
      end if;
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION

   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_store_queue_rec.message_type,
                    L_store_queue_rec.seq_no,
                    L_store_key_rec);

END PUB_RETRY;


--------------------------------------------------------------------------------
-- PRIVATE PROCEDURES
--------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK(O_error_msg        OUT VARCHAR2,
                        O_queue_locked     OUT BOOLEAN,
                        I_store_key_rec IN     STORE_KEY_REC)
RETURN BOOLEAN IS

L_table         VARCHAR2(30) := 'STORE_MFQUEUE';
L_key1          VARCHAR2(100) := I_store_key_rec.store;
L_key2          VARCHAR2(100) := I_store_key_rec.addr_key;
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

cursor C_LOCK_QUEUE_KEY1 is
select 'x'
  from store_mfqueue
 where store = L_key1
   for update nowait;

cursor C_LOCK_QUEUE_KEY2 is
select 'x'
  from store_mfqueue
 where store = L_key1
   and addr_key = L_key2
   for update nowait;

BEGIN

   O_queue_locked := FALSE;

   if L_key2 is null then
      open C_LOCK_QUEUE_KEY1;
      close C_LOCK_QUEUE_KEY1;
   else
      open C_LOCK_QUEUE_KEY2;
      close C_LOCK_QUEUE_KEY2;
   end if;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_queue_locked := TRUE;
      return TRUE;

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_STORE.LOCK_THE_BLOCK',
                                        to_char(SQLCODE));
      return FALSE;

END LOCK_THE_BLOCK;
--------------------------------------------------------------------------------

FUNCTION PROCESS_QUEUE_RECORD(O_error_msg        OUT VARCHAR2,
                              O_message       IN OUT NOCOPY RIB_OBJECT,
                              O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id       OUT RIB_BUSOBJID_TBL,
                              O_message_type  IN OUT STORE_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_store_key_rec IN     STORE_KEY_REC,
                              I_hdr_published IN     STORE_PUB_INFO.PUBLISHED%TYPE,
                              I_rowid         IN     ROWID)
RETURN BOOLEAN IS

L_rib_storedesc_rec   "RIB_StoreDesc_REC" := null;
L_rib_storeref_rec    "RIB_StoreRef_REC" := null;

BEGIN

   if O_message_type = HDR_DEL then       -- STORERef
      if I_hdr_published = 'Y' then
         if BUILD_HEADER_OBJECT(O_error_msg,
                                O_routing_info,
                                L_rib_storeref_rec,
                                I_store_key_rec) = FALSE then
            return FALSE;
         end if;
         O_message := L_rib_storeref_rec;
      end if;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_rowid) = FALSE then
         return FALSE;
      end if;

      delete from store_pub_info
       where store = I_store_key_rec.store;

   elsif I_hdr_published in ('N', 'I') then
      if I_hdr_published = 'N' then
         O_message_type := HDR_ADD;
      else
         O_message_type := DTL_ADD;
      end if;

      if MAKE_CREATE(O_error_msg,
                     O_message,
                     O_routing_info,
                     I_store_key_rec,
                     I_rowid) = FALSE then
         return FALSE;
      end if;
   elsif O_message_type in(HDR_UPD,DTL_ADD,DTL_UPD) then
      if BUILD_HEADER_OBJECT(O_error_msg,
                             O_routing_info,
                             L_rib_storedesc_rec,
                             I_store_key_rec) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg,
                                     L_rib_storedesc_rec,
                                     O_message_type,
                                     I_store_key_rec) = FALSE then
         return FALSE;
      end if;

      if O_message_type = HDR_UPD then
         if DELETE_QUEUE_REC(O_error_msg,I_rowid) = FALSE then
            return FALSE;
         end if;
      end if;

      O_message := L_rib_storedesc_rec;

   elsif O_message_type = DTL_DEL then
      if BUILD_HEADER_OBJECT(O_error_msg,
                             O_routing_info,
                             L_rib_storeref_rec,
                             I_store_key_rec) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_DELETE_OBJECTS(O_error_msg,
                                     L_rib_storeref_rec,
                                     I_store_key_rec) = FALSE then
         return FALSE;
      end if;
      O_message := L_rib_storeref_rec;
   else
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      if O_error_msg is null then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'RMSMFM_STORE.PROCESS_QUEUE_RECORD',
                                           to_char(SQLCODE));
      end if;
      return FALSE;

END PROCESS_QUEUE_RECORD;
--------------------------------------------------------------------------------

FUNCTION DELETE_QUEUE_REC(O_error_msg    OUT VARCHAR2,
                          I_rowid     IN     ROWID)
RETURN BOOLEAN IS

BEGIN

   delete from store_mfqueue
    where rowid = I_rowid;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_STORE.DELETE_QUEUE_REC',
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_QUEUE_REC;
--------------------------------------------------------------------------------

FUNCTION MAKE_CREATE(O_error_msg        OUT VARCHAR2,
                     O_message       IN OUT NOCOPY RIB_OBJECT,
                     O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                     I_store_key_rec IN     STORE_KEY_REC,
                     I_rowid         IN     ROWID)
RETURN BOOLEAN IS

L_rib_storedesc_rec         "RIB_StoreDesc_REC" := null;
L_rib_storeaddrdtl_tbl      "RIB_AddrDesc_TBL" := null;
L_store_mfqueue_rowid_tbl   ROWID_TBL;
L_store_mfqueue_size        BINARY_INTEGER := 0;
L_store_addrdtl_rowid_tbl   ROWID_TBL;
L_store_addrdtl_size        BINARY_INTEGER := 0;
L_delete_rowid_ind          VARCHAR2(1) := 'Y';

BEGIN

   if BUILD_HEADER_OBJECT(O_error_msg,
                          O_routing_info,
                          L_rib_storedesc_rec,
                          I_store_key_rec) = FALSE then
      return FALSE;
   end if;

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           L_rib_storeaddrdtl_tbl,
                           L_store_mfqueue_rowid_tbl,
                           L_store_mfqueue_size,
                           L_store_addrdtl_rowid_tbl,
                           L_store_addrdtl_size,
                           L_delete_rowid_ind,
                           null,
                           I_store_key_rec) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   -- Update store_pub_info;
   update store_pub_info
      set published = decode(L_delete_rowid_ind,'Y', 'Y', 'I')
    where store = I_store_key_rec.store;

   -- Add the detail to the header;
   L_rib_storedesc_rec.AddrDesc_TBL := L_rib_storeaddrdtl_tbl;

   if L_delete_rowid_ind = 'Y' then
      -- Delete from store_mfqueue where rowid = I_rowid;
      if L_store_mfqueue_size > 0 then
         for i in 1..L_store_mfqueue_size loop
            if DELETE_QUEUE_REC(O_error_msg,
                                L_store_mfqueue_rowid_tbl(i)) = FALSE then
               return FALSE;
            end if;
         end loop;
      end if;
   end if;

   -- Update store_addr_dtl_info;
   if L_store_addrdtl_size > 0 then
      forall i in 1 .. L_store_addrdtl_size
      update addr
         set publish_ind = 'Y'
       where rowid = L_store_addrdtl_rowid_tbl(i);
   end if;

   O_message := L_rib_storedesc_rec;
   return TRUE;

EXCEPTION

   when OTHERS then
      if O_error_msg is null then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_STORE.MAKE_CREATE',
                                            to_char(SQLCODE));
      end if;
      return FALSE;

END MAKE_CREATE;
--------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT(O_error_msg            OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_routing_info      IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_rib_storedesc_rec    OUT NOCOPY "RIB_StoreDesc_REC",
                             I_store_key_rec     IN     STORE_KEY_REC)
RETURN BOOLEAN IS

L_rib_routing_rec   RIB_ROUTINGINFO_REC := null;
L_store_type        VARCHAR2(1) := null;

cursor C_STORE is
select s.store,
       s.store_type,
       s.org_unit_id,
       s.store_name,
       s.store_name10,
       s.store_name3,
       s.store_class,
       s.store_mgr_name,
       s.store_open_date,
       s.store_close_date,
       s.acquired_date,
       s.remodel_date,
       s.fax_number,
       s.phone_number,
       s.email,
       s.total_square_ft,
       s.selling_square_ft,
       s.linear_distance,
       s.stockholding_ind,
       s.channel_id,
       s.store_format,
       s.mall_name,
       s.district,
       d.district_name,
       s.transfer_zone,
       tz.description,
       s.default_wh,
       s.stop_order_days,
       s.start_order_days,
       s.currency_code,
       s.lang,
       s.integrated_pos_ind,
       s.orig_currency_code,
       s.duns_number,
       s.duns_loc,
       s.timezone_name
  from store s,
       district d,
       tsfzone tz
 where s.store = I_store_key_rec.store
   and s.district = d.district
   and s.transfer_zone = tz.transfer_zone (+);

cursor C_WH is 
select physical_wh 
  from store s,wh w 
 where s.store = I_store_key_rec.store
   and w.wh(+) = s.default_wh;

L_store_rec   C_STORE%ROWTYPE := null;
L_wh_rec      NUMBER(10);

BEGIN

   OPEN C_STORE;
   FETCH C_STORE into L_store_rec;

   if C_STORE%NOTFOUND then
      CLOSE C_STORE;
      O_error_msg := SQL_LIB.CREATE_MSG('NO_STORE_HEADER_PUB', I_store_key_rec.store, null, null);
      raise PROGRAM_ERROR;
   end if;
   CLOSE C_STORE;
   
   OPEN C_WH;
   FETCH C_WH into L_wh_rec;
   CLOSE C_WH;

   O_rib_storedesc_rec := "RIB_StoreDesc_REC"(0,
                                            L_store_rec.store,
                                            L_store_rec.store_type,
                                            L_store_rec.store_name,
                                            L_store_rec.store_name10,
                                            L_store_rec.store_name3,
                                            null,                   ---STORE_ADD1  - deprecated but needed for backward compatibility
                                            null,                   ---STORE_ADD2  - deprecated but needed for backward compatibility
                                            null,                   ---STORE_CITY  - deprecated but needed for backward compatibility
                                            null,                   ---countY      - deprecated but needed for backward compatibility
                                            null,                   ---STATE       - deprecated but needed for backward compatibility
                                            null,                   ---countRY_ID  - deprecated but needed for backward compatibility
                                            null,                   ---STORE_PCODE - deprecated but needed for backward compatibility
                                            L_store_rec.store_class,
                                            L_store_rec.store_mgr_name,
                                            L_store_rec.store_open_date,
                                            L_store_rec.store_close_date,
                                            L_store_rec.acquired_date,
                                            L_store_rec.remodel_date,
                                            L_store_rec.fax_number,
                                            L_store_rec.phone_number,
                                            L_store_rec.email,
                                            L_store_rec.total_square_ft,
                                            L_store_rec.selling_square_ft,
                                            L_store_rec.linear_distance,
                                            L_store_rec.stockholding_ind,
                                            L_store_rec.channel_id,
                                            L_store_rec.store_format,
                                            L_store_rec.mall_name,
                                            L_store_rec.district,
                                            L_store_rec.district_name,
                                            null,                   ---PROMO_ZONE - deprecated but needed for backward compatibility
                                            null,                   ---PROMO_DESC - deprecated but needed for backward compatibility
                                            L_store_rec.transfer_zone,
                                            L_store_rec.description,
                                            L_wh_rec,               ---Phyiscal Warehouse
                                            L_store_rec.stop_order_days,
                                            L_store_rec.start_order_days,
                                            L_store_rec.currency_code,
                                            L_store_rec.lang,
                                            L_store_rec.integrated_pos_ind,
                                            L_store_rec.orig_currency_code,
                                            L_store_rec.duns_number,
                                            L_store_rec.duns_loc,
                                            null,
                                            I_store_key_rec.pricing_loc,
                                            I_store_key_rec.pricing_loc_curr,
                                            L_store_rec.org_unit_id,
                                            L_store_rec.timezone_name);

   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('dest_id',
                                            I_store_key_rec.store,
                                            null,
                                            null,
                                            null,
                                            null);
   O_routing_info.EXTend;
   O_routing_info(O_routing_info.count) := L_rib_routing_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      if O_error_msg is null then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_STORE.BUILD_HEADER_OBJECT',
                                            to_char(SQLCODE));
      end if;
      return FALSE;

END BUILD_HEADER_OBJECT;
--------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT(O_error_msg           OUT VARCHAR2,
                             O_routing_info     IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_rib_storeref_rec    OUT NOCOPY "RIB_StoreRef_REC",
                             I_store_key_rec    IN     STORE_KEY_REC)
RETURN BOOLEAN IS

L_rib_routing_rec   RIB_ROUTINGINFO_REC := null;
L_store_type        VARCHAR2(1) := null;
 
BEGIN
 
   O_rib_storeref_rec := "RIB_StoreRef_REC"(0,
                                          I_store_key_rec.store,
                                          I_store_key_rec.store_type,
                                          I_store_key_rec.stockholding_ind,
                                          null);                        -- Address detail Rec

   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('dest_id',
                                            I_store_key_rec.store,
                                            null,
                                            null,
                                            null,
                                            null);
   O_routing_info.EXTend;
   O_routing_info(O_routing_info.count) := L_rib_routing_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      if O_error_msg is null then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_STORE.BUILD_HEADER_OBJECT',
                                            to_char(SQLCODE));
      end if;
      return FALSE;

END BUILD_HEADER_OBJECT;
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code   IN OUT VARCHAR2,
                        O_error_msg     IN OUT VARCHAR2,
                        O_message       IN OUT NOCOPY RIB_OBJECT,
                        O_bus_obj_id    IN OUT NOCOPY RIB_BUSOBJID_TBL,
                        O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                        O_message_type  IN OUT STORE_MFQUEUE.MESSAGE_TYPE%TYPE,
                        I_seq_no        IN     STORE_MFQUEUE.SEQ_NO%TYPE,
                        I_store_key_rec IN     STORE_KEY_REC) IS

L_rib_storedtlref_rec   "RIB_AddrRef_REC";
L_rib_storedtlref_tbl   "RIB_AddrRef_TBL";
L_rib_storeref_rec      "RIB_StoreRef_REC";

L_error_type            VARCHAR2(5) := null;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then
      O_bus_obj_id := RIB_BUSOBJID_TBL(I_store_key_rec.store);
      O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                I_seq_no,
                                                                null, 
                                                                null,
                                                                null,
                                                                null));
      L_rib_storedtlref_rec := "RIB_AddrRef_REC"(0,I_store_key_rec.addr_key);
      L_rib_storedtlref_tbl := "RIB_AddrRef_TBL"(L_rib_storedtlref_rec);
      L_rib_storeref_rec := "RIB_StoreRef_REC"(0,
                                               I_store_key_rec.store,
                                               NULL,
                                               NULL,
                                               L_rib_storedtlref_tbl);
      O_message := L_rib_storeref_rec;

      update store_mfqueue
         set pub_status = LP_error_status
       where seq_no = I_seq_no;

   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type, O_error_msg);

EXCEPTION

   when OTHERS then
      O_status_code := API_CODES.UNHANDLED_ERROR;
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_STORE.HANDLE_ERRORS',
                                        to_char(SQLCODE));

END HANDLE_ERRORS;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg               IN OUT VARCHAR2,
                              O_rib_storeaddrdtl_tbl    IN OUT NOCOPY "RIB_AddrDesc_TBL",
                              O_store_mfqueue_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_store_mfqueue_size      IN OUT BINARY_INTEGER,
                              O_store_addrdtl_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_store_addrdtl_size      IN OUT BINARY_INTEGER,
                              O_delete_row_ind          IN OUT VARCHAR2,
                              I_message_type            IN     STORE_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_store_key_rec           IN     STORE_KEY_REC)
RETURN BOOLEAN IS

L_rib_storeaddrdtl_tbl    "RIB_AddrDesc_TBL" := null;
L_rib_storeaddrdtl_rec    "RIB_AddrDesc_REC" := null;
L_records_found           BOOLEAN := FALSE;
L_max_details             RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE :=12;
L_num_threads             RIB_SETTINGS.NUM_THREADS%TYPE := null;
L_min_time_lag            RIB_SETTINGS.MINUTES_TIME_LAG%TYPE := null;
L_status_code             VARCHAR2(1) := null;
L_details_processed       RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE :=0;
L_primary_addr_type_ind   ADD_TYPE_MODULE.PRIMARY_IND%TYPE := 'N';
L_primary_addr_ind        ADDR.PRIMARY_ADDR_IND%TYPE := 'N';

RECORD_LOCKED             EXCEPTION;
PRAGMA                    EXCEPTION_INIT(RECORD_LOCKED, -54);

cursor C_ADDR_MC IS
  select s.description,
         c.country_desc,
         a.addr_key,
         a.addr_type,
         atm.primary_ind,
         a.primary_addr_ind,
         a.add_1,
         a.add_2,
         a.add_3,
         a.city,
         a.state,
         a.country_id,
         a.post,
         a.jurisdiction_code,
         decode(atm.primary_ind,'Y',decode(a.primary_addr_ind,'Y',nvl(a.contact_name,st.store_mgr_name),a.contact_name),a.contact_name) contact_name,
         decode(atm.primary_ind,'Y',decode(a.primary_addr_ind,'Y',nvl(a.contact_phone,st.phone_number),a.contact_phone),a.contact_phone) contact_phone,
         a.contact_telex,
         a.contact_fax,
         decode(atm.primary_ind,'Y',decode(a.primary_addr_ind,'Y',nvl(a.contact_email,st.email),a.contact_email),a.contact_email) contact_email,
         a.oracle_vendor_site_id,
         a.county,
         a.rowid addr_rowid,
         sq.rowid sq_rowid
    from addr a,
         add_type_module atm,
         store_mfqueue sq,
         state s,
         country c,
         store st
   where a.module = atm.module
     and a.addr_type = atm.address_type
     and a.module in ('ST', 'WFST')
     and a.publish_ind = 'N'
     and a.state = s.state(+)
     and a.country_id = c.country_id
     and a.country_id = s.country_id(+)
     and a.key_value_1 = sq.store(+)
     and a.key_value_1 = TO_CHAR(I_store_key_rec.store)
     and st.store = a.key_value_1
     and (a.jurisdiction_code is NULL
          or exists (select 'x' 
                       from country_tax_jurisdiction ctj
                      where ctj.jurisdiction_code = a.jurisdiction_code
                        and ctj.state = a.state
                        and ctj.country_id = a.country_id))
order by atm.primary_ind desc,
         a.primary_addr_ind desc
     for update of a.publish_ind nowait;

cursor C_ADDR is
select s.description,
       c.country_desc,
       a.addr_key,
       a.addr_type,
       atm.primary_ind,
       a.primary_addr_ind,
       a.add_1,
       a.add_2,
       a.add_3,
       a.city,
       a.state,
       a.country_id,
       a.post,
       a.jurisdiction_code,
       decode(atm.primary_ind,'Y',decode(a.primary_addr_ind,'Y',nvl(a.contact_name,st.store_mgr_name),a.contact_name),a.contact_name)  contact_name,
       decode(atm.primary_ind,'Y',decode(a.primary_addr_ind,'Y',nvl(a.contact_phone,st.phone_number),a.contact_phone),a.contact_phone) contact_phone,
       a.contact_telex,
       a.contact_fax,
       decode(atm.primary_ind,'Y',decode(a.primary_addr_ind,'Y',nvl(a.contact_email,st.email),a.contact_email),a.contact_email) contact_email,
       a.oracle_vendor_site_id,
       a.county,
       a.rowid addr_rowid,
       sq.rowid sq_rowid
  from addr a,
       add_type_module atm,
       store_mfqueue sq,
       state s,
       country c,
       store st
 where a.module = atm.module
   and a.addr_type = atm.address_type
   and a.module in ('ST', 'WFST')
   and a.state = s.state(+)
   and a.country_id = c.country_id
   and a.country_id = s.country_id(+)
   and a.addr_key = sq.addr_key(+)
   and a.addr_key = TO_CHAR(I_store_key_rec.addr_key)
   and st.store = a.key_value_1
   and sq.message_type(+) = I_message_type
   and (a.jurisdiction_code is NULL
        or exists (select 'x' 
                     from country_tax_jurisdiction ctj
                    where ctj.jurisdiction_code = a.jurisdiction_code
                      and ctj.state = a.state
                      and ctj.country_id = a.country_id))

   for update of a.publish_ind nowait;

cursor C_ADDR_HDR_UPD is
select s.description,
       c.country_desc,
       a.addr_key,
       a.addr_type,
       atm.primary_ind,
       a.primary_addr_ind,
       a.add_1,
       a.add_2,
       a.add_3,
       a.city,
       a.state,
       a.country_id,
       a.post,
       a.jurisdiction_code,
       nvl(a.contact_name,st.store_mgr_name) contact_name,
       nvl(a.contact_phone,st.phone_number) contact_phone,
       a.contact_telex,
       a.contact_fax,
       nvl(a.contact_email,st.email) contact_email,
       a.oracle_vendor_site_id,
       a.county,
       a.rowid addr_rowid
  from addr a,
       add_type_module atm,
       state s,
       country c,
       store st
 where a.module = atm.module
   and a.addr_type = atm.address_type
   and a.module in ('ST', 'WFST')
   and a.key_value_1 = TO_CHAR(I_store_key_rec.store)
   and st.store = a.key_value_1
   and atm.primary_ind = 'Y'
   and a.primary_addr_ind = 'Y'
   and a.state = s.state(+)
   and a.country_id = c.country_id
   and a.country_id =s.country_id(+)
   and (a.jurisdiction_code is NULL
        or exists (select 'x' 
                     from country_tax_jurisdiction ctj
                    where ctj.jurisdiction_code = a.jurisdiction_code
                      and ctj.state = a.state
                      and ctj.country_id = a.country_id));

BEGIN

   if O_rib_storeaddrdtl_tbl is null then
      L_rib_storeaddrdtl_tbl := "RIB_AddrDesc_TBL"();
   else
      L_rib_storeaddrdtl_tbl := O_rib_storeaddrdtl_tbl;
   end if;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_STORE.FAMILY);
   if L_status_code IN (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   if I_message_type is null then
      for rec in C_ADDR_MC loop
         L_records_found := TRUE;
         if L_details_processed >= L_max_details then
            O_delete_row_ind := 'N';
            exit;
         end if;

         if BUILD_SINGLE_DETAIL (O_error_msg,
                                 L_rib_storeaddrdtl_tbl,
                                 L_rib_storeaddrdtl_rec,
                                 rec.description,
                                 rec.country_desc,
                                 rec.addr_key,
                                 rec.addr_type,
                                 rec.primary_ind,
                                 rec.primary_addr_ind,
                                 rec.add_1,
                                 rec.add_2,
                                 rec.add_3,
                                 rec.city,
                                 rec.state,
                                 rec.country_id,
                                 rec.post,
                                 rec.jurisdiction_code,
                                 rec.contact_name,
                                 rec.contact_phone,
                                 rec.contact_telex,
                                 rec.contact_fax,
                                 rec.contact_email,
                                 rec.oracle_vendor_site_id,
                                 rec.county) = FALSE then
            return FALSE;
         end if;
         O_store_addrdtl_size := O_store_addrdtl_size + 1;
         O_store_addrdtl_rowid_tbl(O_store_addrdtl_size) := rec.addr_rowid;

         if rec.sq_rowid is not null then
            O_store_mfqueue_size := O_store_mfqueue_size + 1;
            O_store_mfqueue_rowid_tbl(O_store_mfqueue_size) := rec.sq_rowid;
         end if;

         L_details_processed := L_details_processed + 1;
      end loop;
   elsif I_message_type = HDR_UPD then
      for rec in C_ADDR_HDR_UPD loop
         L_records_found := TRUE;

         if BUILD_SINGLE_DETAIL (O_error_msg,
                                 L_rib_storeaddrdtl_tbl,
                                 L_rib_storeaddrdtl_rec,
                                 rec.description,
                                 rec.country_desc,
                                 rec.addr_key,
                                 rec.addr_type,
                                 rec.primary_ind,
                                 rec.primary_addr_ind,
                                 rec.add_1,
                                 rec.add_2,
                                 rec.add_3,
                                 rec.city,
                                 rec.state,
                                 rec.country_id,
                                 rec.post,
                                 rec.jurisdiction_code,
                                 rec.contact_name,
                                 rec.contact_phone,
                                 rec.contact_telex,
                                 rec.contact_fax,
                                 rec.contact_email,
                                 rec.oracle_vendor_site_id,
                                 rec.county) = FALSE then
            return FALSE;
         end if;
         O_store_addrdtl_size := O_store_addrdtl_size + 1;
         O_store_addrdtl_rowid_tbl(O_store_addrdtl_size) := rec.addr_rowid;

         L_details_processed := L_details_processed + 1;
         exit;  -- exit after getting primary_addr_type and primary_addr which is first addr record.
      end loop;
   else
      for rec in C_ADDR loop
         L_records_found := TRUE;
         if L_details_processed >= L_max_details then
            O_delete_row_ind := 'N';
            exit;
         end if;

         if BUILD_SINGLE_DETAIL (O_error_msg,
                                 L_rib_storeaddrdtl_tbl,
                                 L_rib_storeaddrdtl_rec,
                                 rec.description,
                                 rec.country_desc,
                                 rec.addr_key,
                                 rec.addr_type,
                                 rec.primary_ind,
                                 rec.primary_addr_ind,
                                 rec.add_1,
                                 rec.add_2,
                                 rec.add_3,
                                 rec.city,
                                 rec.state,
                                 rec.country_id,
                                 rec.post,
                                 rec.jurisdiction_code,
                                 rec.contact_name,
                                 rec.contact_phone,
                                 rec.contact_telex,
                                 rec.contact_fax,
                                 rec.contact_email,
                                 rec.oracle_vendor_site_id,
                                 rec.county) = FALSE then
            return FALSE;
         end if;
         O_store_addrdtl_size := O_store_addrdtl_size + 1;
         O_store_addrdtl_rowid_tbl(O_store_addrdtl_size) := rec.addr_rowid;

         if rec.sq_rowid is not null then
            O_store_mfqueue_size := O_store_mfqueue_size + 1;
            O_store_mfqueue_rowid_tbl(O_store_mfqueue_size) := rec.sq_rowid;
         end if;

         L_details_processed := L_details_processed + 1;
      end loop;
   end if;

   -- if no data found in cursor, raise error
   if not L_records_found then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_ADDR_DETAIL_PUB', I_store_key_rec.store, null, null);
      return FALSE;
   end if;

   if L_rib_storeaddrdtl_tbl.count > 0 then
      O_rib_storeaddrdtl_tbl := L_rib_storeaddrdtl_tbl;
   else
      O_rib_storeaddrdtl_tbl := null;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_STORE.BUILD_DETAIL_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_OBJECTS;
--------------------------------------------------------------------------------

FUNCTION BUILD_SINGLE_DETAIL(O_error_msg             IN OUT VARCHAR2,
                             O_rib_storeaddrdtl_tbl  IN OUT NOCOPY "RIB_AddrDesc_TBL",
                             O_rib_storeaddrdtl_rec  IN OUT NOCOPY "RIB_AddrDesc_REC",
                             I_state_name            IN     STATE.DESCRIPTION%TYPE,
                             I_country_name          IN     COUNTRY.COUNTRY_DESC%TYPE,
                             I_addr_key              IN     ADDR.ADDR_KEY%TYPE,
                             I_addr_type             IN     ADDR.ADDR_TYPE%TYPE,
                             I_primary_ind           IN     ADD_TYPE_MODULE.PRIMARY_IND%TYPE,
                             I_primary_addr_ind      IN     ADDR.PRIMARY_ADDR_IND%TYPE,
                             I_add_1                 IN     ADDR.ADD_1%TYPE,
                             I_add_2                 IN     ADDR.ADD_2%TYPE,
                             I_add_3                 IN     ADDR.ADD_3%TYPE,
                             I_city                  IN     ADDR.CITY%TYPE,
                             I_state                 IN     ADDR.STATE%TYPE,
                             I_country_id            IN     ADDR.COUNTRY_ID%TYPE,
                             I_post                  IN     ADDR.POST%TYPE,
                             I_jurs_code             IN     ADDR.JURISDICTION_CODE%TYPE,
                             I_contact_name          IN     ADDR.CONTACT_NAME%TYPE,
                             I_contact_phone         IN     ADDR.CONTACT_PHONE%TYPE,
                             I_contact_telex         IN     ADDR.CONTACT_TELEX%TYPE,
                             I_contact_fax           IN     ADDR.CONTACT_FAX%TYPE,
                             I_contact_email         IN     ADDR.CONTACT_EMAIL%TYPE,
                             I_oracle_vendor_site_id IN     ADDR.ORACLE_VENDOR_SITE_ID%TYPE,
                             I_county                IN     ADDR.COUNTY%TYPE)
RETURN BOOLEAN IS

BEGIN

   O_rib_storeaddrdtl_rec:= "RIB_AddrDesc_REC"(0,
                                              NULL,
                                              I_state_name,
                                              I_country_name,
                                              I_addr_key,
                                              I_addr_type,
                                              I_primary_ind,
                                              I_primary_addr_ind,
                                              I_add_1,
                                              I_add_2,
                                              I_add_3,
                                              I_city,
                                              I_state,
                                              I_country_id,
                                              I_post,
                                              I_contact_name,
                                              I_contact_phone,
                                              I_contact_telex,
                                              I_contact_fax,
                                              I_contact_email,
                                              I_oracle_vendor_site_id,
                                              I_county,
                                              I_jurs_code);

   O_rib_storeaddrdtl_tbl.EXTend;
   O_rib_storeaddrdtl_tbl(O_rib_storeaddrdtl_tbl.count) := O_rib_storeaddrdtl_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_STORE.BUILD_SINGLE_DETAIL',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_SINGLE_DETAIL;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg            OUT VARCHAR2,
                                     O_rib_storedesc_rec IN OUT NOCOPY "RIB_StoreDesc_REC",
                                     I_message_type      IN     STORE_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_store_key_rec     IN     STORE_KEY_REC)
RETURN BOOLEAN IS

L_store_mfqueue_rowid_tbl   ROWID_TBL;
L_store_mfqueue_size        BINARY_INTEGER := 0;
L_store_addrdtl_rowid_tbl   ROWID_TBL;
L_store_addrdtl_size        BINARY_INTEGER := 0;
L_delete_row_ind            VARCHAR2(1) := 'Y';

BEGIN
   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           O_rib_storedesc_rec.AddrDesc_tbl,
                           L_store_mfqueue_rowid_tbl,
                           L_store_mfqueue_size,
                           L_store_addrdtl_rowid_tbl,
                           L_store_addrdtl_size,
                           L_delete_row_ind,
                           I_message_type,
                           I_store_key_rec) = FALSE then
      return FALSE;
   end if;
   if L_store_addrdtl_size > 0 then
      forall i in 1..L_store_addrdtl_size
         update addr
         set publish_ind = 'Y'
         where rowid = L_store_addrdtl_rowid_tbl(i);
   end if;

   if L_store_mfqueue_size > 0 then
      for i in 1..L_store_mfqueue_size loop
         if DELETE_QUEUE_REC(O_error_msg,L_store_mfqueue_rowid_tbl(i)) = FALSE then
            return FALSE;
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_STORE.BUILD_DETAIL_CHANGE_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_CHANGE_OBJECTS;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg           OUT VARCHAR2,
                                     O_rib_storeref_rec IN OUT NOCOPY "RIB_StoreRef_REC",
                                     I_store_key_rec    IN     STORE_KEY_REC)
RETURN BOOLEAN IS

L_storeaddr_rowid_tbl      ROWID_TBL;
L_storeaddr_size           BINARY_INTEGER := 0;
L_store_mfqueue_rowid_tbl  ROWID_TBL;
L_store_mfqueue_size       BINARY_INTEGER := 0;
L_rib_storeaddrref_rec     "RIB_AddrRef_REC" := null;
L_rib_storeaddrref_tbl     "RIB_AddrRef_TBL" := null;
L_rib_storeref_rec         "RIB_StoreRef_REC" := null;
L_max_details              rib_settings.max_details_to_publish%TYPE := 12;
L_num_threads              rib_settings.num_threads%TYPE := null;
L_min_time_lag             rib_settings.minutes_time_lag%TYPE := null;
L_status_code              VARCHAR2(1) := null;
L_details_processed        rib_settings.max_details_to_publish%TYPE := 0;

cursor C_ADDR_DELETE is
select sq.addr_key,
       sq.rowid sq_rowid
  from store_mfqueue sq
 where sq.store = I_store_key_rec.store
   and sq.message_type = DTL_DEL
   and rownum <= L_max_details;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_STORE.FAMILY);
   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   L_rib_storeaddrref_tbl := "RIB_AddrRef_TBL"();
   for rec in C_ADDR_DELETE loop
      if rec.sq_rowid IS NOT null then
         L_rib_storeaddrref_rec := "RIB_AddrRef_REC"(0,rec.addr_key);
         L_rib_storeaddrref_tbl.EXTend;
         L_rib_storeaddrref_tbl(L_rib_storeaddrref_tbl.count) := L_rib_storeaddrref_rec;
      end if;

      L_store_mfqueue_size := L_store_mfqueue_size + 1;
      L_store_mfqueue_rowid_tbl(L_store_mfqueue_size) := rec.sq_rowid;

   end loop;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_store_mfqueue_size > 0 then
      for i in 1..L_store_mfqueue_size loop
         if DELETE_QUEUE_REC(O_error_msg,L_store_mfqueue_rowid_tbl(i)) = FALSE then
            return FALSE;
         end if;
      end loop;
   end if;

   if L_rib_storeaddrref_tbl.count > 0 then
      O_rib_storeref_rec.addrref_tbl := L_rib_storeaddrref_tbl;
   else
      O_rib_storeref_rec := null;
   end if;
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_STORE.BUILD_DETAIL_DELETE_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_DELETE_OBJECTS;
--------------------------------------------------------------------------------

END RMSMFM_STORE;
/
