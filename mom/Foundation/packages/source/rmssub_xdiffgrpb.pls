
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XDIFFGRP AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHANGE_CASE
   -- Purpose      : This private function should convert specific char fields from the rib message to 
   --                uppercase and the message type to lowercase.
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XDiffGrpDesc_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHANGE_CASE
   -- Purpose      : This private function should convert specific char fields from the rib message to 
   --                uppercase and the message type to lowercase.
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XDiffGrpRef_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_program               VARCHAR2(50) := 'RMSSUB_XDIFFGRP.CONSUME';
   L_ref_message           "RIB_XDiffGrpRef_REC";
   L_message               "RIB_XDiffGrpDesc_REC";
   L_diffgrp_rec           DIFF_GROUP_SQL.DIFF_GROUP_REC;
   L_message_type          VARCHAR2(15) := I_message_type;
  
BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   if LOWER(I_message_type) in (LP_cre_type, LP_dtl_cre_type, LP_mod_type, LP_dtl_mod_type) then
      L_message := treat(I_MESSAGE AS "RIB_XDiffGrpDesc_REC");

      if L_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      if CHANGE_CASE(O_error_message,
                     L_message,
                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Validate Message Contents
      if NOT RMSSUB_XDIFFGRP_VALIDATE.CHECK_MESSAGE(O_error_message,
                                                    L_diffgrp_rec,
                                                    L_message,
                                                    L_message_type) then
         raise PROGRAM_ERROR;
      end if;
      ---

      -- INSERT/UPDATE table
      if RMSSUB_XDIFFGRP_SQL.PERSIST(O_error_message,
                                     L_diffgrp_rec,
                                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   elsif LOWER(I_message_type) in (LP_del_type, LP_dtl_del_type) then
      L_ref_message := treat(I_MESSAGE AS "RIB_XDiffGrpRef_REC");

      if CHANGE_CASE(O_error_message,
                     L_ref_message,
                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if L_ref_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      -- Validate Message Contents
      if RMSSUB_XDIFFGRP_VALIDATE.CHECK_MESSAGE(O_error_message,
                                                L_diffgrp_rec,
                                                L_ref_message,
                                                L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- DELETE from table
      if RMSSUB_XDIFFGRP_SQL.PERSIST(O_error_message,
                                     L_diffgrp_rec,
                                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then

       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XDiffGrpDesc_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XDIFFGRP.CHANGE_CASE';

BEGIN

   IO_message_type              := LOWER(IO_message_type);
   IO_message.diff_group_id     := UPPER(IO_message.diff_group_id);
   IO_message.diff_group_type   := UPPER(IO_message.diff_group_type);

   if IO_message.xdiffgrpdtl_tbl is NOT NULL AND IO_message.xdiffgrpdtl_tbl.COUNT > 0 then
      FOR i in IO_message.xdiffgrpdtl_tbl.first..IO_message.xdiffgrpdtl_tbl.last LOOP
         IO_message.xdiffgrpdtl_tbl(i).diff_id := UPPER(IO_message.xdiffgrpdtl_tbl(i).diff_id);
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHANGE_CASE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XDiffGrpRef_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XDIFFGRP.CHANGE_CASE';

BEGIN

   IO_message_type             := LOWER(IO_message_type);
   IO_message.diff_group_id    := UPPER(IO_message.diff_group_id);

   if IO_message.xdiffgrpdtlref_tbl is NOT NULL AND IO_message.xdiffgrpdtlref_tbl.COUNT > 0 then
      FOR i in IO_message.xdiffgrpdtlref_tbl.first..IO_message.xdiffgrpdtlref_tbl.last LOOP
         IO_message.xdiffgrpdtlref_tbl(i).diff_id := UPPER(IO_message.xdiffgrpdtlref_tbl(i).diff_id);
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHANGE_CASE;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE BODY
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2) IS

   L_program VARCHAR2(50) := 'RMSSUB_XDIFFGRP.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);
EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XDIFFGRP;
/
