
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEMLIST_DELETE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
-- Name:    SKULIST_DELETE
-- Purpose: Deletes all styles or SKUs found given the specified criteria
--          and the number of styles or SKUs in the SKU list.
----------------------------------------------------------------
FUNCTION SKULIST_DELETE(I_itemlist           IN     SKULIST_HEAD.SKULIST%TYPE,
                        I_pack_ind           IN     ITEM_MASTER.PACK_IND%TYPE,
                        I_item               IN     ITEM_MASTER.ITEM%TYPE,
                        I_item_parent        IN     SKULIST_CRITERIA.ITEM_PARENT%TYPE,
                        I_item_grandparent   IN     SKULIST_CRITERIA.ITEM_GRANDPARENT%TYPE,
                        I_dept               IN     SKULIST_CRITERIA.DEPT%TYPE,
                        I_class              IN     SKULIST_CRITERIA.CLASS%TYPE,
                        I_subclass           IN     SKULIST_CRITERIA.SUBCLASS%TYPE,
                        I_supplier           IN     SKULIST_CRITERIA.SUPPLIER%TYPE,
                        I_diff_1             IN     SKULIST_CRITERIA.DIFF_1%TYPE,
                        I_diff_2             IN     SKULIST_CRITERIA.DIFF_2%TYPE,
                        I_diff_3             IN     SKULIST_CRITERIA.DIFF_3%TYPE,
                        I_diff_4             IN     SKULIST_CRITERIA.DIFF_4%TYPE,
                        I_uda_id             IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                        I_uda_value          IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                        I_uda_max_date       IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                        I_uda_min_date       IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                        I_season_id          IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                        I_phase_id           IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                        I_count_ind          IN     VARCHAR2,
                        I_no_add_ind         IN     VARCHAR2,
                        I_item_level         IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                        O_no_items           IN OUT NUMBER,
                        O_error_message      IN OUT VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name:  DEL_SKULIST
-- Purpose :            This function will delete an Item list from STAKE_SCHEDULE for
--                            a unique schedule id.
-- Created by :        Bill Zacher   8/27/99
-----------------------------------------------------------------------
FUNCTION DEL_SKULIST (O_error_message  IN OUT   VARCHAR2,
                      I_skulist        IN       skulist_head.skulist%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name:  LOCK_SKULIST
-- Purpose: 	   To lock LOC_CLSF_DETAIL, SKLIST_CRITERIA, AND SKULIST_DETIAL
--                 tables.
----------------------------------------------------------------
FUNCTION LOCK_SKULIST(O_error_message  IN OUT VARCHAR2,
                      I_skulist        IN     skulist_head.skulist%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
END;
/


