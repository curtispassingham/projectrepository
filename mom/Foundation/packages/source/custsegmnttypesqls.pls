CREATE OR REPLACE PACKAGE CUST_SEGMENT_TYPE_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------------------
--- Procedure Name: CHECK_CUST_SEGMENT_TYPE_EXISTS
--- Purpose:        This function will check if the input Customer Segment Type is present in the
---                 CUSTOMER_SEGMENT_TYPES table.If present it will return the description.
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CUST_SEGMENT_TYPE_EXISTS (O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_exists                       IN OUT   BOOLEAN,
                                         O_customer_segment_type_desc   IN OUT   CUSTOMER_SEGMENT_TYPES.CUSTOMER_SEGMENT_TYPE_DESC%TYPE,
                                         I_customer_segment_type        IN       CUSTOMER_SEGMENT_TYPES.CUSTOMER_SEGMENT_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: CUST_SEGMENT_TYPE_EXCEP_EXISTS
--- Purpose:        This function will check if the input Customer Segment Type has any exceptions
---                 that prevent the segment type from being deleted.
------------------------------------------------------------------------------------------------------------
FUNCTION CUST_SEGMENT_TYPE_EXCEP_EXISTS (O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_exists                       IN OUT   BOOLEAN,
                                         I_customer_segment_type        IN       CUSTOMER_SEGMENT_TYPES.CUSTOMER_SEGMENT_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
END CUST_SEGMENT_TYPE_SQL;
/
