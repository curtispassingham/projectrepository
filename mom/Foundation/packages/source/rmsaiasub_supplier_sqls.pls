CREATE OR REPLACE PACKAGE RMSAIASUB_SUPPLIER_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------
--- Function Name : PERSIST
--- Purpose       : Inserts records into SUPPLIER,SUPPLIER SITE,PARTNER_ORG_UNIT and ADDR tables
---                 by calling appropriate PRIVATE functions in the same package.
-------------------------------------------------------------------------------------------
FUNCTION PERSIST (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_supplier_record   IN OUT   SUPP_REC)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END RMSAIASUB_SUPPLIER_SQL;
/
