CREATE OR REPLACE PACKAGE BODY ITEM_INDUCT_ERRVAL AS
------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_EXISTS(O_error_message  IN OUT   VARCHAR2,
                           IO_desc          IN OUT   VARCHAR2,
                           IO_exists        IN OUT   BOOLEAN,
                           I_process_id     IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                           I_item           IN       V_IIND_ERRORS_LIST.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.CHECK_ITEM_EXISTS';
   L_item      V_IIND_ERRORS_LIST.ITEM%TYPE;
   L_desc      V_IIND_ERRORS_LIST.ITEM_DESC_TRANS%TYPE;

   cursor C_GET_ITEM is
     select item,
            item_desc_trans
       from v_iind_errors_list
      where process_id = I_process_id
        and item       = I_item;

BEGIN
   open C_GET_ITEM;
   fetch C_GET_ITEM into L_item,
                      L_desc;
   if C_GET_ITEM%NOTFOUND then
      IO_desc := NULL;
      IO_exists := FALSE;
   else
      IO_desc := L_desc;
      IO_exists := TRUE;
   end if;
   close C_GET_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_ITEM%ISOPEN then
         close C_GET_ITEM;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_ITEM_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER_EXISTS(O_error_message           IN OUT   VARCHAR2,
                               IO_sup_name               IN OUT   VARCHAR2,
                               IO_exists                 IN OUT   BOOLEAN,
                               I_process_id              IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                               I_supplier                IN       V_SUPS.SUPPLIER%TYPE,
                               I_supplier_site           IN       V_SUPS.SUPPLIER%TYPE,
                               I_GV_supplier_sites_ind   IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.CHECK_SUPPLIER_EXISTS';
   L_sup_name   V_SUPS.SUP_NAME%TYPE;
   L_supplier   V_SUPS.SUPPLIER%TYPE;

   cursor C_GET_SUPPLIER is
      select DISTINCT s2.sup_name AS sup_name,
             s2.supplier
        from v_iind_errors_list v,
             v_sups s1,
             v_sups s2
       where I_GV_supplier_sites_ind  = 'Y'
         and v.supplier          = s1.supplier
         and s1.supplier_parent  = s2.supplier
         and (I_supplier_site IS NULL or
              I_supplier_site = v.supplier)
         and process_id  = I_process_id
         and s2.supplier = I_supplier
       union all
      select DISTINCT sup_name_trans AS sup_name,
             supplier
        from v_iind_errors_list v
       where I_GV_supplier_sites_ind = 'N'
         and v.process_id          = I_process_id
         and v.supplier            = I_supplier;

BEGIN
   open C_GET_SUPPLIER;
   fetch C_GET_SUPPLIER into L_sup_name,
                             L_supplier;
   if C_GET_SUPPLIER%NOTFOUND then
      IO_sup_name := NULL;
      IO_exists := FALSE;
   else
      IO_sup_name := L_sup_name;
      IO_exists := TRUE;
   end if;
   close C_GET_SUPPLIER;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_SUPPLIER%ISOPEN then
         close C_GET_SUPPLIER;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_SUPPLIER_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER_SITE_EXISTS(O_error_message       IN OUT   VARCHAR2,
                                    IO_sup_name           IN OUT   VARCHAR2,
                                    IO_sup_parent         IN OUT   VARCHAR2,
                                    IO_sup_parent_name    IN OUT   VARCHAR2,
                                    IO_sup_exists         IN OUT   BOOLEAN,
                                    IO_sup_site_exists    IN OUT   BOOLEAN,
                                    I_process_id          IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                                    I_supplier            IN       V_IIND_ERRORS_LIST.SUPPLIER%TYPE,
                                    I_supplier_site       IN       V_IIND_ERRORS_LIST.SUPPLIER%TYPE,
                                    I_supplier_provided   IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.CHECK_SUPPLIER_SITE_EXISTS';
   L_sup_name   V_SUPS.SUP_NAME%TYPE;
   L_supplier   V_SUPS.SUPPLIER%TYPE;

   cursor C_CHECK is
     select DISTINCT v.supplier
       from v_iind_errors_list v
      where process_id = I_process_id
        and v.supplier = I_supplier_site;

   cursor C_GET_SUP_DETAILS_P is
     select DISTINCT v.sup_name_trans AS sup_name,
            v.supplier,
            s2.supplier AS supplier_parent,
            s2.sup_name AS sup_parent_name
       from v_iind_errors_list v,
            v_sups s1,
            v_sups s2
      where v.process_id         = I_process_id
        and v.supplier           = s1.supplier
        and I_supplier           = s1.supplier_parent
        and s2.supplier          = s1.supplier_parent
        and v.supplier           = I_supplier_site;

   cursor C_GET_SUP_DETAILS is
     select DISTINCT v.sup_name_trans AS sup_name,
            v.supplier,
            s.supplier AS supplier_parent,
            s.sup_name AS sup_parent_name
       from v_iind_errors_list v,
            v_sups s
      where process_id        = I_process_id
        and v.supplier        = I_supplier_site
        and v.supplier_parent = s.supplier(+);

BEGIN
   if I_supplier_provided = 'Y' then
      open C_GET_SUP_DETAILS_P;
      fetch C_GET_SUP_DETAILS_P into IO_sup_name,
                                     L_supplier,
                                     IO_sup_parent,
                                     IO_sup_parent_name;
      if C_GET_SUP_DETAILS_P%NOTFOUND then
         IO_sup_site_exists := FALSE;
      else
         IO_sup_site_exists := TRUE;
      end if;
      close C_GET_SUP_DETAILS_P;
   else
      open C_CHECK;
      fetch C_CHECK into L_supplier;
      if C_CHECK%NOTFOUND then
         IO_sup_exists := FALSE;
         return TRUE;
      else
         IO_sup_exists := TRUE;
         open C_GET_SUP_DETAILS;
         fetch C_GET_SUP_DETAILS into IO_sup_name,
                                      L_supplier,
                                      IO_sup_parent,
                                      IO_sup_parent_name;
         if C_GET_SUP_DETAILS%NOTFOUND then
            IO_sup_site_exists := FALSE;
         else
            IO_sup_site_exists := TRUE;
         end if;
         close C_GET_SUP_DETAILS;
      end if;
      close C_CHECK;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_SUP_DETAILS_P%ISOPEN then
         close C_GET_SUP_DETAILS_P;
      end if;

      if C_CHECK%ISOPEN then
         close C_CHECK;
      end if;

      if C_GET_SUP_DETAILS%ISOPEN then
         close C_GET_SUP_DETAILS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_SUPPLIER_SITE_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_COUNTRY_EXISTS(O_error_message   IN OUT   VARCHAR2,
                              IO_country_desc   IN OUT   VARCHAR2,
                              IO_exists         IN OUT   BOOLEAN,
                              I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                              I_country_id      IN       V_IIND_ERRORS_LIST.COUNTRY_ID%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.CHECK_COUNTRY_EXISTS';
   L_country_id     V_IIND_ERRORS_LIST.COUNTRY_ID%TYPE;
   L_country_desc   V_IIND_ERRORS_LIST.COUNTRY_DESC_TRANS%TYPE;

   cursor C_CHECK_COUNTRY is
      select country_desc_trans AS country_desc,
             country_id
        from v_iind_errors_list
       where process_id = I_process_id
         and country_id   = I_country_id;

BEGIN
   open C_CHECK_COUNTRY;
   fetch C_CHECK_COUNTRY into IO_country_desc,
                              L_country_id;
   if C_CHECK_COUNTRY%NOTFOUND then
      IO_exists := FALSE;
   else
      IO_exists := TRUE;
   end if;
   close C_CHECK_COUNTRY;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_COUNTRY%ISOPEN then
         close C_CHECK_COUNTRY;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_COUNTRY_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_TABLE_DESC_EXISTS(O_error_message   IN OUT   VARCHAR2,
                                 IO_exists         IN OUT   BOOLEAN,
                                 I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                                 I_wksht           IN       V_IIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.CHECK_TABLE_DESC_EXISTS';
   L_table_desc     V_IIND_ERRORS_LIST.TABLE_DESC%TYPE;

   cursor C_CHECK_TABLE_DESC is
      select table_desc
        from v_iind_errors_list
       where process_id = I_process_id
         and table_desc   = I_wksht;

BEGIN
   open C_CHECK_TABLE_DESC;
   fetch C_CHECK_TABLE_DESC into L_table_desc;
   if C_CHECK_TABLE_DESC%NOTFOUND then
      IO_exists := FALSE;
   else
      IO_exists := TRUE;
   end if;
   close C_CHECK_TABLE_DESC;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_TABLE_DESC%ISOPEN then
         close C_CHECK_TABLE_DESC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_TABLE_DESC_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ROW_EXISTS(O_error_message   IN OUT   VARCHAR2,
                          IO_exists         IN OUT   BOOLEAN,
                          I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                          I_row             IN       V_IIND_ERRORS_LIST.ROW_SEQ%TYPE,
                          I_wksht           IN       V_IIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.CHECK_ROW_EXISTS';
   L_row_seq        V_IIND_ERRORS_LIST.ROW_SEQ%TYPE;

   cursor C_CHECK_ROW is
      select row_seq
        from v_iind_errors_list
       where UPPER(table_desc) = UPPER(I_wksht)
         and process_id = I_process_id
         and row_seq = I_row;

BEGIN
   open C_CHECK_ROW;
   fetch C_CHECK_ROW into L_row_seq;
   if C_CHECK_ROW%NOTFOUND then
      IO_exists := FALSE;
   else
      IO_exists := TRUE;
   end if;
   close C_CHECK_ROW;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_ROW%ISOPEN then
         close C_CHECK_ROW;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_ROW_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_COLUMN_EXISTS(O_error_message   IN OUT   VARCHAR2,
                             IO_exists         IN OUT   BOOLEAN,
                             I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                             I_column          IN       V_IIND_ERRORS_LIST.COLUMN_DESC%TYPE,
                             I_wksht           IN       V_IIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.CHECK_COLUMN_EXISTS';
   L_column_desc    V_IIND_ERRORS_LIST.COLUMN_DESC%TYPE;

   cursor C_CHECK_COLUMN is
      select column_desc
        from v_iind_errors_list
       where UPPER(table_desc) = I_wksht
         and process_id   = I_process_id
         and column_desc  = I_column;

BEGIN
   open C_CHECK_COLUMN;
   fetch C_CHECK_COLUMN into L_column_desc;
   if C_CHECK_COLUMN%NOTFOUND then
      IO_exists := FALSE;
   else
      IO_exists := TRUE;
   end if;
   close C_CHECK_COLUMN;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_COLUMN%ISOPEN then
         close C_CHECK_COLUMN;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_COLUMN_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ERR_MSG_EXISTS(O_error_message   IN OUT   VARCHAR2,
                              IO_exists         IN OUT   BOOLEAN,
                              I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                              I_error           IN       V_IIND_ERRORS_LIST.ERROR_MSG%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.CHECK_ERR_MSG_EXISTS';
   L_error_msg      V_IIND_ERRORS_LIST.ERROR_MSG%TYPE;

   cursor C_CHECK_ERROR_MSG is
      select DISTINCT error_msg
        from v_iind_errors_list
       where process_id = I_process_id
         and error_msg  = I_error;

BEGIN
   open C_CHECK_ERROR_MSG;
   fetch C_CHECK_ERROR_MSG into L_error_msg;
   if C_CHECK_ERROR_MSG%NOTFOUND then
      IO_exists := FALSE;
   else
      IO_exists := TRUE;
   end if;
   close C_CHECK_ERROR_MSG;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_ERROR_MSG%ISOPEN then
         close C_CHECK_ERROR_MSG;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END CHECK_ERR_MSG_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION GET_PROCESS_INFO(O_error_message    IN OUT   VARCHAR2,
                          IO_process_desc    IN OUT   V_SVC_PRC_TRACKER.PROCESS_DESC%TYPE,
                          IO_template_name   IN OUT   V_SVC_PRC_TRACKER.TEMPLATE_NAME%TYPE,
                          IO_action_date     IN OUT   V_SVC_PRC_TRACKER.ACTION_DATE%TYPE,
                          I_process_id       IN       V_SVC_PRC_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(50) := 'ITEM_INDUCT_ERRVAL.GET_PROCESS_INFO';

   cursor C_HEAD is
     select process_desc,
            template_name,
            action_date
       from v_svc_prc_tracker
      where process_id = I_process_id;

BEGIN
   open C_HEAD;
   fetch C_HEAD into IO_process_desc,
                     IO_template_name,
                     IO_action_date;
   close C_HEAD;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_HEAD%ISOPEN then
         close C_HEAD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_PROCESS_INFO;
------------------------------------------------------------------------------------------
END ITEM_INDUCT_ERRVAL;
/
