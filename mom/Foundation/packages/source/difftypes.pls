
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFF_TYPE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
--    Name: DIFF_TYPE_EXISTS
-- Purpose: Determine if the input diff type already exists.  
--          Returns TRUE or FALSE.
----------------------------------------------------------------
FUNCTION DIFF_TYPE_EXISTS(O_error_message  IN OUT  VARCHAR2,
                          O_exists         IN OUT  BOOLEAN,
                          I_diff_type      IN      DIFF_TYPE.DIFF_TYPE%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------
--    Name: CHECK_ASSOCIATION
-- Purpose: Determine if the input diff type is associated with
--          any diff groups or diff ids.
----------------------------------------------------------------
FUNCTION CHECK_ASSOCIATION(O_error_message  IN OUT  VARCHAR2,
                           O_assoc_exists   IN OUT  BOOLEAN,
                           I_diff_type      IN      DIFF_TYPE.DIFF_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
--    Name: GET_DESC
-- Purpose: This function gets a translated description for a passed in diff type.
----------------------------------------------------------------
FUNCTION GET_DESC(O_error_message     IN OUT    VARCHAR2,
                  O_diff_type_desc    IN OUT    DIFF_TYPE.DIFF_TYPE_DESC%TYPE,
                  I_diff_type         IN        DIFF_TYPE.DIFF_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
END DIFF_TYPE_SQL;
/
