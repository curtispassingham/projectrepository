CREATE OR REPLACE PACKAGE BODY CUST_SEGMENT_SQL AS
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CUST_SEGMENT_ID_EXISTS (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists                  IN OUT   BOOLEAN,
                                       O_customer_segment_desc   IN OUT   CUSTOMER_SEGMENTS.CUSTOMER_SEGMENT_DESC%TYPE,
                                       I_customer_segment_id     IN       CUSTOMER_SEGMENTS.CUSTOMER_SEGMENT_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'CUST_SEGMENT_SQL.CHECK_CUST_SEGMENT_ID_EXISTS';
   
   cursor C_GET_DESCRIPTION is
      select customer_segment_desc
        from v_customer_segments_tl
       where customer_segment_id = I_customer_segment_id;

BEGIN
   O_exists := TRUE;
   O_customer_segment_desc := NULL;
   ---
   if I_customer_segment_id IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_customer_segment_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_DESCRIPTION',
                    'CUSTOMER_SEGMENTS',
                    I_customer_segment_id);
   open C_GET_DESCRIPTION;
   --
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_DESCRIPTION',
                    'CUSTOMER_SEGMENTS',
                    I_customer_segment_id);
   fetch C_GET_DESCRIPTION into O_customer_segment_desc;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_DESCRIPTION',
                    'CUSTOMER_SEGMENTS',
                    I_customer_segment_id);
   close C_GET_DESCRIPTION;
   ---
   if O_customer_segment_desc is NULL then
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CUST_SEGMENT_ID_EXISTS;
------------------------------------------------------------------------------------------------------------
FUNCTION CUST_SEGMENT_ID_EXCEP_EXISTS (O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists                     IN OUT   BOOLEAN,
                                       I_customer_segment_id        IN       CUSTOMER_SEGMENTS.CUSTOMER_SEGMENT_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'CUST_SEGMENT_SQL.CUST_SEGMENT_ID_EXCEP_EXISTS';
   L_cust_seg_active  NUMBER(1)    := 0;
   
BEGIN
   O_exists := FALSE;
   ---
   if I_customer_segment_id IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_customer_segment_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if MERCH_API_SQL.CHECK_CUST_SEGMENT_ACTIVE(O_error_message,
                                              L_cust_seg_active,
                                              I_customer_segment_id) != 1 then
      return FALSE;
   end if;
   ---
   if L_cust_seg_active = 1 then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CUST_SEGMENT_ID_EXCEP_EXISTS;
------------------------------------------------------------------------------------------------------------
END CUST_SEGMENT_SQL;
/
