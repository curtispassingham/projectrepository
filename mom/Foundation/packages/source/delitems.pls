
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DELETE_ITEM_RECORDS_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------
-- Name:      DEL_ITEM
-- Purpose:   This function will handle checks for the deletion
--            of any item.  
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_ITEM(error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  i_key_value   IN     ITEM_MASTER.ITEM%TYPE,
                  i_cancel_item IN     BOOLEAN) RETURN BOOLEAN;
------------------------------------------------------------------
END;
/


