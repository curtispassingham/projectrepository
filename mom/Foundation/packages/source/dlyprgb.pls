
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY DAILY_PURGE_SQL AS
--------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTS (O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_key_value     IN     DAILY_PURGE.KEY_VALUE%TYPE,
                       I_table_name    IN     DAILY_PURGE.TABLE_NAME%TYPE)
   RETURN BOOLEAN IS

   L_dummy    VARCHAR2(1)  := NULL;

   cursor C_EXISTS is
      select 'x'
        from daily_purge
       where key_value = I_key_value
         and table_name = I_table_name;

BEGIN

   open C_EXISTS;
   fetch C_EXISTS into L_dummy;
   close C_EXISTS;

   if L_dummy = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DAILY_PURGE_SQL.CHECK_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_EXISTS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_RECORD (O_error_message  IN OUT VARCHAR2,
                        I_key_value      IN     DAILY_PURGE.KEY_VALUE%TYPE,
                        I_table_name     IN     DAILY_PURGE.TABLE_NAME%TYPE,
                        I_delete_type    IN     DAILY_PURGE.DELETE_TYPE%TYPE,
                        I_delete_order   IN     DAILY_PURGE.DELETE_ORDER%TYPE)
   RETURN BOOLEAN IS
   
   L_dept              DEPS.DEPT%TYPE;
   L_class             CLASS.CLASS%TYPE;

BEGIN

   if I_key_value is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_key_value',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if I_table_name is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_table_name',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if I_delete_type is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_delete_type',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;

   if I_delete_order is NULL then
      O_error_message:= sql_lib.create_msg('INVALID_PARM',
                                           'I_delete_order',
                                           'NULL',
                                           'NOT NULL');
      return FALSE;
   end if;
   
   if I_table_name = 'DEPS' then
      L_dept := to_number(I_key_value);

      insert into daily_purge(key_value,
                              table_name,
                              delete_type,
                              delete_order)
                       values(I_key_value,
                              'DEPS',
                              'D',
                              '3');
      
      insert into daily_purge (key_value,
                               table_name,
                               delete_type,
                               delete_order)
                        select key_v,
                               'CLASS',
                               'D',
                               '2'
                          from (select substr(to_char(s.dept, '0999'), 2, 4) || ';' || substr(to_char(s.class, '0999'), 2, 4) key_v
                                  from class s
                                 where dept = L_dept) table_temp
                         where table_temp.key_v not in (select key_value 
                                                          from daily_purge 
                                                         where table_name = 'CLASS');
      
      insert into daily_purge (key_value,
                               table_name,
                               delete_type,
                               delete_order)
                        select key_v,
                               'SUBCLASS',
                               'D',
                               '1'
                          from (select substr(to_char(s.dept, '0999'), 2, 4) || ';' || substr(to_char(s.class, '0999'), 2, 4) || ';' || substr(to_char(s.subclass, '0999'), 2, 4) key_v
                                  from subclass s
                                 where dept = L_dept) table_temp
                         where table_temp.key_v not in (select key_value 
                                                          from daily_purge 
                                                         where table_name = 'SUBCLASS');
      
   elsif I_table_name = 'CLASS' then
      L_dept  := to_number(substr(I_key_value,1,4));
      L_class := to_number(substr(I_key_value,6,4));

      insert into daily_purge(key_value,
                              table_name,
                              delete_type,
                              delete_order)
                       values(I_key_value,
                              'CLASS',
                              'D',
                              '2');

      insert into daily_purge (key_value,
                               table_name,
                               delete_type,
                               delete_order)
                        select key_v,
                               'SUBCLASS',
                               'D',
                               '1'
                          from (select substr(to_char(s.dept, '0999'), 2, 4) || ';' || substr(to_char(s.class, '0999'), 2, 4) || ';' || substr(to_char(s.subclass, '0999'), 2, 4) key_v
                                  from subclass s
                                 where dept  = L_dept
                                   and class = L_class) table_temp
                         where table_temp.key_v not in (select key_value 
                                                         from daily_purge 
                                                        where table_name = 'SUBCLASS');
      
   else
      insert into daily_purge(key_value,
                              table_name,
                              delete_type,
                              delete_order)
                       values(I_key_value,
                              I_table_name,
                              I_delete_type,
                              I_delete_order);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DAILY_PURGE_SQL.INSERT_RECORD',
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_RECORD;
----------------------------------------------------------------------------------------
FUNCTION ITEM_PARENT_GRANDPARENT_EXIST(O_error_message IN OUT VARCHAR2,
                                       O_exists        IN OUT BOOLEAN,
                                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                                       I_table_name    IN     DAILY_PURGE.TABLE_NAME%TYPE)
   RETURN BOOLEAN IS

   L_dummy    VARCHAR2(1)  := 'N';

   cursor C_EXISTS is
      select *
        from (select 'Y'
                from daily_purge dp
               where I_item = dp.key_value
                 and table_name = I_table_name
                 and rownum = 1
               UNION
              select 'Y'
                from daily_purge dp,
                     item_master im
               where ((I_item = im.item and im.item_parent = dp.key_value)
                  or (I_item = im.item and im.item_grandparent = dp.key_value))
                 and table_name = I_table_name
                 and rownum = 1)
       where rownum = 1;

BEGIN

   open C_EXISTS;
   fetch C_EXISTS into L_dummy;
   close C_EXISTS;

   if L_dummy = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST',
                                            to_char(SQLCODE));
      return FALSE;
END ITEM_PARENT_GRANDPARENT_EXIST;
----------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_STATUS(O_error_message  IN OUT VARCHAR2,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE)                        
     RETURN BOOLEAN IS
     RECORD_LOCKED   EXCEPTION;
     PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
     LP_table        VARCHAR2(50);
 cursor C_LOCK_ITEM_MASTER is
    select 'x'
      from item_master
     where item = I_item
        or item_parent = I_item
        or item_grandparent = I_item
       for update nowait;
 BEGIN
      LP_table := 'ITEM_MASTER';
      open C_LOCK_ITEM_MASTER;
      close C_LOCK_ITEM_MASTER;
      SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_MASTER', 'ITEM: '||i_item);

      update item_master
         set status = 'W'
       where item_grandparent = I_item;
      update item_master
      set status = 'W'
       where item_parent = I_item;
      update item_master
      set status = 'W'
       where item = I_item;
      return TRUE;
 EXCEPTION

    when RECORD_LOCKED then
       O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             LP_table,
                                             I_item,
                                             NULL);
       return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DAILY_PURGE_SQL.ITEM_STATUS',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ITEM_STATUS;
----------------------------------------------------------------------------------------
FUNCTION CONTENTS_ITEM_EXISTS(O_error_message  IN OUT VARCHAR2,
                              O_exists         IN OUT BOOLEAN,
                              I_item           IN     ITEM_MASTER.ITEM%TYPE)                        
     RETURN BOOLEAN IS
 
 L_dummy   VARCHAR2(1);
     
 cursor C_CHECK_CONTENTS_ITEM is
    select 'x'
      from item_master im
     where im.container_item = I_item
       and im.item not in(select key_value 
                            from daily_purge 
                           where key_value = im.item
                             and table_name = 'ITEM_MASTER')
       and rownum = 1;
 BEGIN
      
      open   C_CHECK_CONTENTS_ITEM;
      fetch  C_CHECK_CONTENTS_ITEM into L_dummy;
      close  C_CHECK_CONTENTS_ITEM;
      
      if L_dummy is not null then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      
      return TRUE;
 EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DAILY_PURGE_SQL.CONTENTS_ITEM_EXISTS',
                                            to_char(SQLCODE));
      return FALSE;
END CONTENTS_ITEM_EXISTS;
-----------------------------------------------------------------------------------------------
FUNCTION REF_ITEM_COUNT(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_rec_count      IN OUT   NUMBER,
                        I_item           IN       ITEM_MASTER.ITEM%TYPE,
                        I_table_name     IN       DAILY_PURGE.TABLE_NAME%TYPE)
   RETURN BOOLEAN IS

   L_rec_count    NUMBER  := 0;
   L_program      VARCHAR2(64) := 'DAILY_PURGE_SQL.REF_ITEM_COUNT';

   cursor C_COUNT is
      select count(*)
        from item_master 
       where item_parent = I_item 
         and item_level  > tran_level
         and item not in ( select key_value
                             from daily_purge dp,item_master im
                            where dp.key_value   = im.item 
                              and im.item_parent = I_item 
                              and dp.table_name  = I_table_name);
         
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_table_name',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT',
                    'item_master',
                    NULL); 
   open C_COUNT;
      
   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT',
                    'item_master',
                    NULL);
   fetch C_COUNT into L_rec_count;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT',
                    'item_master',
                    NULL);
   close C_COUNT;

   O_rec_count := L_rec_count;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DAILY_PURGE_SQL.REF_ITEM_COUNT',to_char(SQLCODE));
      return FALSE;
END REF_ITEM_COUNT;
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_PRIMARY_REF_ITEM_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                    I_table_name      IN       DAILY_PURGE.TABLE_NAME%TYPE)
   RETURN BOOLEAN IS

   L_dummy    VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'DAILY_PURGE_SQL.CHECK_PRIMARY_REF_ITEM_IND';

   cursor C_EXISTS is
      select 'Y'
        from item_master 
       where item_parent = I_item
         and primary_ref_item_ind = 'Y'
         and item in ( select dp.key_value
                             from daily_purge dp,item_master im
                        where dp.key_value   = im.item 
                          and im.item_parent = I_item
                          and dp.table_name  = I_table_name)
         and rownum = 1;
 
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_table_name',
                                            L_program,
                                            NULL);
      return FALSE;
   end if; 

   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'item_master',
                    NULL);
   open C_EXISTS;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'item_master',
                    NULL);
   fetch C_EXISTS into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    'item_master',
                    NULL);
   close C_EXISTS;

   if L_dummy = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DAILY_PURGE_SQL.CHECK_PRIMARY_REF_ITEM_IND',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_PRIMARY_REF_ITEM_IND;
----------------------------------------------------------------------------------------
FUNCTION ALL_RECORDS_DELETED(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        IN OUT BOOLEAN,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE,
                             I_table_name    IN     DAILY_PURGE.TABLE_NAME%TYPE)
   RETURN BOOLEAN IS

   L_dummy    VARCHAR2(1)  := 'N';
   L_program  VARCHAR2(64) := 'DAILY_PURGE_SQL.ALL_RECORDS_DELETED';

   cursor C_EXISTS is
      select 'Y' 
        from item_master 
       where item_parent = I_item
         and item not in (select dp.key_value
                            from daily_purge dp,
                                 item_master im
                           where dp.key_value   = im.item 
                             and im.item_parent = I_item
                             and dp.table_name  = I_table_name)
         and rownum = 1;

BEGIN   
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_table_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_table_name',
                                            L_program,
                                            NULL);
      return FALSE;
   end if; 

   SQL_LIB.SET_MARK('OPEN',
                    'C_EXISTS',
                    'item_master',
                    NULL);
   open C_EXISTS;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_EXISTS',
                    'item_master',
                    NULL);   
   fetch C_EXISTS into L_dummy;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_EXISTS',
                    'item_master',
                    NULL);   
   close C_EXISTS;

   if L_dummy = 'Y' then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DAILY_PURGE_SQL.ALL_RECORDS_DELETED',
                                            to_char(SQLCODE));
      return FALSE;
END ALL_RECORDS_DELETED;
----------------------------------------------------------------------------------------
END DAILY_PURGE_SQL;
/
