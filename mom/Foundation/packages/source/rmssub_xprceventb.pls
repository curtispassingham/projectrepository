CREATE OR REPLACE PACKAGE BODY RMSSUB_XPRCEVENT AS

 LP_process_id       SVC_PRICING_EVENT_HEAD.PROCESS_ID%TYPE := NULL;
 LP_wh               CONSTANT   VARCHAR2(2) :='WH';
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: CAPITALIZE_FIELDS
   -- Purpose      : This private function should convert all char fields from the message to uppercase
-------------------------------------------------------------------------------------------------------
FUNCTION CAPITALIZE_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_message        IN OUT NOCOPY   "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: EVENT_EXISTS
   -- Purpose      : This function is used to check if the event already exists 
-------------------------------------------------------------------------------------------------------     
FUNCTION EVENT_EXISTS (O_error_message  IN OUT VARCHAR2,
                       O_exist          IN OUT BOOLEAN,
                       I_message               "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN;   
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: PERSIST
   -- Purpose      : This function is used to persist the the data in the staging table and process
   --                 the same depending on the effective date
-------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message   IN       "RIB_XPrcEventDesc_REC")
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
   -- Function Name: PERSIST  overloaded
   -- Purpose      : This function is used to delete the record from staging table
-------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message   IN       "RIB_XPrcEventRef_REC")
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-------------------------------------------------------------------------------------------------------
FUNCTION EVENT_EXISTS (O_error_message  IN OUT VARCHAR2,
                       O_exist          IN OUT BOOLEAN,
                       I_message        "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN IS   
   L_exists       VARCHAR2(1);
   L_program     VARCHAR2(64) := 'RMSSUB_XPRCEVENT.EVENT_EXISTS';

   cursor C_SVC_PRICING_EVENT_HEAD is
      select 'x'
        from svc_pricing_event_head svc 
       where svc.event_id = I_message.event_id
         and svc.event_type = I_message.event_type
         and rownum = 1;
         
    cursor C_INVALID_EVENT is
      select 'x'
        from svc_pricing_event_head svc 
       where svc.item = I_message.item
         and svc.hier_level = I_message.hier_level
         and svc.effective_date = TO_DATE(I_message.effective_date, 'DD-MON-YY')          
         and ((I_message.event_type = 'PROMS' and 
              svc.event_type       =  'PROME')
         or  (I_message.event_type = 'PROME' and 
              svc.event_type       =  'PROMS')  
         or  (I_message.event_type = 'CLRS' and   
                   svc.event_type       =  'CLRE')  
         or  (I_message.event_type = 'CLRE' and   
                   svc.event_type       =  'CLRS'));
         
BEGIN
   O_exist := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_SVC_PRICING_EVENT_HEAD','SVC_PRICING_EVENT_HEAD',' Event: '|| I_message.event_id ||' Event_type: '||I_message.event_type);
   open C_SVC_PRICING_EVENT_HEAD;
   SQL_LIB.SET_MARK('FETCH','C_SVC_PRICING_EVENT_HEAD','SVC_PRICING_EVENT_HEAD',' Event: '|| I_message.event_id ||' Event_type: '||I_message.event_type);
   fetch C_SVC_PRICING_EVENT_HEAD into L_exists;
  
   if C_SVC_PRICING_EVENT_HEAD%FOUND then
      O_exist := TRUE;    
         SQL_LIB.SET_MARK('CLOSE','C_SVC_PRICING_EVENT_HEAD','SVC_PRICING_EVENT_HEAD', ' Event: '|| I_message.event_id ||' Event_type: '||I_message.event_type);
         close C_SVC_PRICING_EVENT_HEAD;   
      return TRUE;   
   end if;
   
   SQL_LIB.SET_MARK('CLOSE','C_SVC_PRICING_EVENT_HEAD','SVC_PRICING_EVENT_HEAD', ' Event: '|| I_message.event_id ||' Event_type: '||I_message.event_type);
   close C_SVC_PRICING_EVENT_HEAD;

   -- If event type is promotiom / Clearance make sure start and end do not exist on the same day.   
   if I_message.event_type in ('CLRS', 'CLRE','PROMS', 'PROME')  then
   
      SQL_LIB.SET_MARK('OPEN','C_INVALID_EVENT','SVC_PRICING_EVENT_HEAD',' Event: '|| I_message.event_id ||' Event_type: '||I_message.event_type);
      open C_INVALID_EVENT;
      SQL_LIB.SET_MARK('FETCH','C_INVALID_EVENT','SVC_PRICING_EVENT_HEAD',' Event: '|| I_message.event_id ||' Event_type: '||I_message.event_type);
      fetch C_INVALID_EVENT into L_exists;
  
      if C_INVALID_EVENT%FOUND then 
         O_exist := TRUE;    
            SQL_LIB.SET_MARK('CLOSE','C_INVALID_EVENT','SVC_PRICING_EVENT_HEAD', ' Event: '|| I_message.event_id ||' Event_type: '||I_message.event_type);
            close C_INVALID_EVENT;   
         return TRUE;   
      end if;
   
      SQL_LIB.SET_MARK('CLOSE','C_INVALID_EVENT','SVC_PRICING_EVENT_HEAD', ' Event: '|| I_message.event_id ||' Event_type: '||I_message.event_type);
      close C_INVALID_EVENT; 
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EVENT_EXISTS;
---------------------------------------------------------------------------------------------------------------------------------- 
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message         IN       "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'RMSSUB_XPRCEVENT.PERSIST'; 
   L_vdate       DATE := get_vdate();    
   
   Cursor C_PRICE_SEQUENCE is
     select  price_sequence.nextval
       from  dual;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_PRICE_SEQUENCE', 'PRICE_SEQUENCE', NULL);
   open C_PRICE_SEQUENCE;

   SQL_LIB.SET_MARK('FETCH', 'C_PRICE_SEQUENCE', 'PRICE_SEQUENCE', NULL);
   fetch C_PRICE_SEQUENCE into LP_process_id;  

   SQL_LIB.SET_MARK('CLOSE', 'C_PRICE_SEQUENCE', 'PRICE_SEQUENCE', NULL);
   close C_PRICE_SEQUENCE;

   insert
      into svc_pricing_event_head
        ( process_id,
          process_status,
          event_type,
          event_id,
          item_level,
          item,
          diff_id,
          hier_level,
          effective_date,
          currency_code,
          selling_unit_retail,
          selling_uom,
          multi_units,
          multi_unit_retail,
          multi_selling_uom,
          promo_selling_retail,
          promo_selling_uom  ) 
      select  LP_process_id,
              'N' ,
              I_message.event_type,
              I_message.event_id,
              I_message.ITEM_LEVEL,           
              I_message.ITEM,
              I_message.diff_id,
              I_message.hier_level,
              TRUNC(I_message.effective_date),
              I_message.currency_code,
              I_message.selling_unit_retail,
              I_message.selling_uom,
              I_message.multi_units,
              I_message.multi_unit_retail,
              I_message.multi_selling_uom, 
              I_message.promo_unit_retail,
              I_message.promo_selling_uom
         from item_master im
         where im.item = I_message.item;   
      
   if I_message.XPrcEventDtl_TBL is NOT NULL and
      I_message.hier_level is not NULL and  I_message.XPrcEventDtl_TBL.COUNT > 0 then
      FOR i in I_message.XPrcEventDtl_TBL.first..I_message.XPrcEventDtl_TBL.last loop   
          if I_message.hier_level = LP_wh then  
              insert into svc_pricing_event_locs
              (process_id,
               hier_value )  
              select LP_process_id,
                     wh
                from WH
               where physical_wh = I_message.XPrcEventDtl_TBL(i).hier_value
                     or ( wh = I_message.XPrcEventDtl_TBL(i).hier_value
                        and wh <> physical_wh);   
           else 
               insert into 
                   svc_pricing_event_locs
                   (process_id,
                    hier_value ) values
                   (LP_process_id,
                   I_message.XPrcEventDtl_TBL(i).hier_value );  
          end if;    
      end loop; 
   end if;
   
   if L_vdate = TO_DATE(I_message.effective_date, 'DD-MON-YY') then  
   
      if CORESVC_XPRICE_SQL.EXPLODE_DETAILS(O_error_message,
                                            LP_process_id) = FALSE then
      return FALSE;   
      end if;

      if CORESVC_XPRICE_SQL.PROCESS_DETAILS(O_error_message,
                                            LP_process_id) = FALSE then    
                                                               
      return FALSE;
      
      update svc_pricing_event_head sph
        set process_status = (select spt.process_status 
                                from svc_pricing_event_temp spt
                               where spt.process_id = LP_process_id)
      where sph.process_id = LP_process_id;
      
      end if;
   end if;    
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST;
---------------------------------------------------------------------------------------------------------------------------------- 
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message         IN       "RIB_XPrcEventRef_REC")
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'RMSSUB_XPRCEVENT.PERSIST';    
   
   L_exists Varchar2(1);
   
   cursor C_LOCK_SVC_PRC_EVENT_HEAD is 
     select 'X' 
       from svc_pricing_event_head sph
      where sph.event_id = I_message.event_id
        and sph.event_type = I_message.event_type
        and sph.process_status = 'N' 
        for update nowait;     
BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_SVC_PRC_EVENT_HEAD', 'SVC_PRICING_EVENT_HEAD', NULL);
   open C_LOCK_SVC_PRC_EVENT_HEAD;

   SQL_LIB.SET_MARK('FETCH', 'C_LOCK_SVC_PRC_EVENT_HEAD', 'SVC_PRICING_EVENT_HEAD', NULL);
   fetch C_LOCK_SVC_PRC_EVENT_HEAD into L_exists;  
   
   if C_LOCK_SVC_PRC_EVENT_HEAD%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SVC_PRC_EVENT_HEAD','SVC_PRICING_EVENT_HEAD',NULL);
      close C_LOCK_SVC_PRC_EVENT_HEAD;
      O_error_message := SQL_LIB.CREATE_MSG('EVENT_ALREADY_PROCESSED',
                                             to_char(I_message.event_id),
                                             I_message.event_type,
                                             null);
       return FALSE;
   end if;   

   delete from  SVC_PRICING_EVENT_HEAD sph
     where sph.event_id   = I_message.event_id
       and sph.event_type = I_message.event_type 
       and sph.process_status = 'N';
       
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_SVC_PRC_EVENT_HEAD', 'SVC_PRICING_EVENT_HEAD', NULL);
   close C_LOCK_SVC_PRC_EVENT_HEAD;
      
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PERSIST;
---------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CAPITALIZE_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_message        IN OUT NOCOPY   "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XPRCEVENT.CAPITALIZE_FIELD';

BEGIN
   
   IO_message.diff_id           := UPPER(IO_message.diff_id);
   IO_message.selling_uom       := UPPER(IO_message.selling_uom);
   IO_message.multi_selling_uom := UPPER(IO_message.multi_selling_uom);
   IO_message.promo_selling_uom := UPPER(IO_message.promo_selling_uom);
   IO_message.currency_code     := UPPER(IO_message.currency_code);
   IO_message.hier_level        := UPPER(IO_message.hier_level);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CAPITALIZE_FIELDS;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE BODY
-------------------------------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2) IS

    L_program VARCHAR2(50) := 'RMSSUB_XPRCEVENT.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;  
---------------------------------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_program        VARCHAR2(25) := 'RMSSUB_XPRCEVENT.CONSUME'; 
   L_message_type   VARCHAR2(30) := LOWER(I_message_type);  
   L_message        "RIB_XPrcEventDesc_REC";
   L_ref_message    "RIB_XPrcEventRef_REC";
   L_event_exists   BOOLEAN;

BEGIN

  O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
  if API_LIBRARY.INIT(O_error_message) = FALSE then
     raise PROGRAM_ERROR;
  end if;

  if I_message is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
     raise PROGRAM_ERROR;
  end if; 

  if LOWER(L_message_type) in (LP_prc_chg_cre, LP_prc_chg_mod)  then
     
     L_message := treat(I_MESSAGE AS "RIB_XPrcEventDesc_REC");
            
     if L_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
     end if;   
      
     if CAPITALIZE_FIELDS(O_error_message,
                          L_message) = FALSE then
         raise PROGRAM_ERROR;
     end if;
      
      --- Check if duplicate event exists
     if RMSSUB_XPRCEVENT.EVENT_EXISTS(O_error_message,
                                      L_event_exists,
                                      L_message) = FALSE then                            
         raise PROGRAM_ERROR;
     end if;
       
     if L_event_exists = TRUE then
        O_error_message := SQL_LIB.CREATE_MSG('EVENT_EXISTS', NULL, NULL, NULL); 
        raise PROGRAM_ERROR;
     end if;
      
      -- Validate Message Contents
     if RMSSUB_XPRCEVENT_VALIDATE.CHECK_MESSAGE(O_error_message,
                                                L_message) = FALSE then
        raise PROGRAM_ERROR;
     end if;
     
      -- Insert / UPDATE table
     if PERSIST(O_error_message,
                L_message) = FALSE then
        raise PROGRAM_ERROR;
     end if;
   
  elsif LOWER(L_message_type) in  (LP_prc_chg_del)  then     
     L_ref_message := treat(I_MESSAGE AS "RIB_XPrcEventRef_REC");  
     
     if RMSSUB_XPRCEVENT.PERSIST(O_error_message,
                                 L_ref_message) = FALSE then                            
       raise PROGRAM_ERROR;
    end if;
  else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'));
      raise PROGRAM_ERROR;
  end if;
   
   return;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;
---------------------------------------------------------------------------------------------------------------------------------- 
END RMSSUB_XPRCEVENT;
/