CREATE OR REPLACE PACKAGE CORESVC_BUYER_MERCHANT AUTHID CURRENT_USER AS
   template_key  CONSTANT VARCHAR2(255)         := 'BUYER_MERCHANT_DATA';
   action_new             VARCHAR2(25)          := 'NEW';
   action_mod             VARCHAR2(25)          := 'MOD';
   action_del             VARCHAR2(25)          := 'DEL';
   BUYER_sheet            VARCHAR2(255)         := 'BUYER';
   BUYER$ACTION           NUMBER                :=1;
   BUYER$BUYER_FAX        NUMBER                :=5;
   BUYER$BUYER_PHONE      NUMBER                :=4;
   BUYER$BUYER_NAME       NUMBER                :=3;
   BUYER$BUYER            NUMBER                :=2;
   merchant_sheet         VARCHAR2(255)         := 'MERCHANT';
   merchant$action        NUMBER                :=1;
   merchant$merch_fax     NUMBER                :=5;
   merchant$merch_phone   NUMBER                :=4;
   merchant$merch_name    NUMBER                :=3;
   merchant$merch         NUMBER                :=2;
   TYPE buyer_rec_tab IS TABLE OF BUYER%ROWTYPE;
   TYPE MERCHANT_rec_tab IS TABLE OF MERCHANT%rowtype;
   sheet_name_trans S9T_PKG.TRANS_MAP_TYP;
   action_column          VARCHAR2(255)         := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE      := 'RMSFND';
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   FUNCTION PROCESS_S9T(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count       IN OUT  NUMBER,
                        I_file_id           IN      S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id        IN      NUMBER
                        )
   RETURN BOOLEAN;
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
END CORESVC_BUYER_MERCHANT;
/