
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SUPPORG_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------------------
TYPE partner_org_unit_rec IS RECORD(org_unit_id       PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE,
                                    org_unit_desc     ORG_UNIT.DESCRIPTION%TYPE,
                                    primary_pay_site  PARTNER_ORG_UNIT.PRIMARY_PAY_SITE%TYPE,
                                    error_message     RTK_ERRORS.RTK_TEXT%TYPE,
                                    return_code       VARCHAR2(5));

TYPE partner_org_unit_tbl IS TABLE OF partner_org_unit_rec INDEX BY BINARY_INTEGER;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE
--- Purpose:        This procedure will query PARTNER_ORG_UNIT table and populate the above declared 
---                 table of records that will be used as the 'base table' in the Supplier Org Unit form.
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                           I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                           I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: DELETE_PROCEDURE
--- Purpose:        This procedure will delete the corresponding records from PARTNER_ORG_UNIT table 
---                 for the inputs I_PARTNER, I_PARTNER_TYPE  and the org_unit_id in the IN OUT table rec.
------------------------------------------------------------------------------------------------------------
PROCEDURE DELETE_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                            I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                            I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: INSERT_PROCEDURE
--- Purpose:        This procedure will insert data in the PARTNER_ORG_UNIT table
---                 from the Supplier Org Unit form.
------------------------------------------------------------------------------------------------------------
PROCEDURE INSERT_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                            I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                            I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: LOCK_PROCEDURE
--- Purpose:        This procedure will lock the queried records in PARTNER_ORG_UNIT table
------------------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                          I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                          I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: UPDATE_PROCEDURE
--- Purpose:        This procedure will update the record in the PARTNER_ORG_UNIT table
---                 from the Supplier Org Unit form.
------------------------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                            I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                            I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE);
------------------------------------------------------------------------------------------------------------
--- Procedure Name: CHECK_PRIM_PAYSITE_EXIST
--- Purpose:        This function will check if a primary pay-site already is associated to the 
---                 Supplier-Org Unit combination. If yes sets O_exists to True else to False
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PRIM_PAYSITE_EXIST (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists                IN OUT   BOOLEAN,
                                   I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                                   I_org_unit_id           IN       PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: UPDATE_PRIM_PAYSITE_N
--- Purpose:        This function will set the primary pay-site to "N" for all the supplier sites related to the
---                 Parent supplier (for the i/p site) - Org Unit combination. 
------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIM_PAYSITE_N (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                                I_org_unit_id           IN       PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: CHECK_PRIM_PAYSITE_EXIST
--- Purpose:        This function will check if a primary pay-site already is associated to the 
---                 Supplier-Org Unit combination. If yes sets O_exists to True else to False
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PPSITE_REMADD_EXIST (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists                IN OUT   BOOLEAN,
                                    IO_org_unit_id          IN OUT   PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE,
                                    I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------
END SUPPORG_SQL;
/
