
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_PRICECHANGE AS

--------------------------------------------------------------------------------
--                             PRIVATE PROTOTYPES                             --
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                        IO_error_message IN OUT VARCHAR2,
                        I_cause          IN     VARCHAR2,
                        I_program        IN     VARCHAR2);


--------------------------------------------------------------------------------
--                            PUBLIC PROCEDURES                               --
--------------------------------------------------------------------------------

PROCEDURE CONSUME(O_status_code   IN OUT VARCHAR2,
                  O_error_message IN OUT VARCHAR2,
                  I_message       IN     OBJ_PRICEEVENT_IL_TBL) IS

L_program   VARCHAR2(100) := 'RMSSUB_PRICECHANGE.CONSUME';

BEGIN

   O_status_code := API_CODES.SUCCESS;

   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   --

   if I_message is null then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', null, null, null);
      raise PROGRAM_ERROR;
   end if;

   --

   if RMSSUB_PRICECHANGE_UPDATE.BUILD_PRICE_CHANGE(O_error_message,
                                                      I_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   --

   if RMSSUB_PRICECHANGE_UPDATE.PERSIST(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   --

   if RMSSUB_PRICECHANGE_UPDATE.PROCESS_VFM(O_error_message,
                                               I_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   --

   return;

EXCEPTION

   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);

END CONSUME;


----------------------------------------------

PROCEDURE PROCESS_STAGED_DEALS (O_status_code   IN OUT VARCHAR2, 
                                O_ERROR_MESSAGE IN OUT VARCHAR2) IS

BEGIN

   PROCESS_STAGED_DEALS(O_status_code, O_ERROR_MESSAGE, new OBJ_PRICE_CHANGE_TBL()); 
END;

----------------------------------------------

PROCEDURE PROCESS_STAGED_DEALS (O_status_code   IN OUT VARCHAR2, 
                                O_ERROR_MESSAGE IN OUT VARCHAR2,
                                I_EVENTS_TBL    IN OBJ_PRICE_CHANGE_TBL) IS
L_program   VARCHAR2(100) := 'RMSSUB_PRICECHANGE.PROCESS_STAGED_DEALS';
BEGIN
   O_status_code := API_CODES.SUCCESS;

   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if RMSSUB_PRICECHANGE_UPDATE.PROCESS_STAGED_DEALS(O_error_message, I_events_tbl) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION

   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

   HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END PROCESS_STAGED_DEALS;


--------------------------------------------------------------------------------
--                            PRIVATE PROCEDURES                              --
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                        IO_error_message IN OUT VARCHAR2,
                        I_cause          IN     VARCHAR2,
                        I_program        IN     VARCHAR2) IS

L_program   VARCHAR2(100) := 'RMSSUB_PRICECHANGE.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION

   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;

--------------------------------------------------------------------------------
END RMSSUB_PRICECHANGE;
/
