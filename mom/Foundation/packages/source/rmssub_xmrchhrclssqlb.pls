
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRCLS_SQL AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CREATE_CLASS
   -- Purpose      : This function will call MERCH_SQL.INSERT_CLASS to create a class record.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: MODIFY_CLASS
   -- Purpose      : This function will call MERCH_SQL.MODIFY_CLASS to update a class record.
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: DELETE_CLASS
   -- Purpose      : This function will call MERCH_SQL.DELETE_CLASS to add a class record to
   --                the daily_purge_table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_class_rec       IN       CLASS%ROWTYPE,
                 I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCLS_SQL.PERSIST';

BEGIN
   if I_message_type = RMSSUB_XMRCHHRCLS.LP_cre_type then
      if not CREATE_CLASS(O_error_message,
                          I_class_rec)then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRCLS.LP_mod_type then
      if not MODIFY_CLASS(O_error_message,
                          I_class_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRCLS.LP_del_type then
      if not DELETE_CLASS(O_error_message,
                          I_class_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-----------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCLS_SQL.CREATE_CLASS';
   L_type_code    VARCHAR2(1)  := 'C';

BEGIN
   if not MERCH_SQL.INSERT_CLASS(O_error_message,
                                 I_class_rec) then
      return FALSE;
   end if;
   if not STKLEDGR_SQL.STOCK_LEDGER_INSERT(L_type_code,
                                           I_class_rec.dept,
                                           I_class_rec.class,
                                           NULL,
                                           NULL,
                                           O_error_message)then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_CLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCLS_SQL.MODIFY_CLASS';

BEGIN

   if not MERCH_SQL.MODIFY_CLASS(O_error_message,
                                 I_class_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_CLASS;
--------------------------------------------------------------------------------------------------------
FUNCTION DELETE_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCLS_SQL.DELETE_CLASS';

BEGIN

   if not MERCH_SQL.DELETE_CLASS(O_error_message,
                                 I_class_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_CLASS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRCLS_SQL;
/
