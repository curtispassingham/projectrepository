
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BANNER_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name:   GET_BANNER_NAME
--Purpose      :   Retrieves the name of the banner from the banner table..
--------------------------------------------------------------------------------
FUNCTION GET_BANNER_NAME    (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_banner_name          IN OUT BANNER.BANNER_NAME%TYPE,
                              I_banner_id            IN     BANNER.BANNER_ID%TYPE)         
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   BANNER_ID_EXIST
--Purpose      :   This function checks if the banner id passed in exists on
--                 the banner table.                 
--------------------------------------------------------------------------------
FUNCTION BANNER_ID_EXIST     (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist                 IN OUT BOOLEAN,
                              I_banner_id            IN     BANNER.BANNER_ID%TYPE)      
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   DELETE_BANNER
--Purpose      :   This function will check to see if the selected banner is associated to any 
--                 channels.  If the banner is attached to a channel then O_exist will 
--                 set to true. If the banner is not attached to any channel then the O_exist will be
--                 set to false and the banner will be deleted. 
--------------------------------------------------------------------------------
FUNCTION DELETE_BANNER        (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist                 IN OUT BOOLEAN,
                               I_banner_id             IN     BANNER.BANNER_ID%TYPE)         
   RETURN BOOLEAN;
--------------------------------------------------------------------------------

END BANNER_SQL;
/