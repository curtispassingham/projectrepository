
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE WH_DEPT_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Function Name: GET_NEXT_SEQ_NO
-- Purpose:       Gets the next available sequence number for a given wh to be
--                inserted into the WH_DEPT table. The seq_no column will be 
--                combined with the wh column to be the primary key for the table.
-- Created:       02-28-01
-------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ_NO(O_error_message   IN OUT   VARCHAR2,
                         O_seq_no          IN OUT   WH_DEPT.SEQ_NO%TYPE,
                         I_wh              IN       WH_DEPT.WH%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: WH_DEPT_EXISTS
-- Purpose:       This function will be called from whdept.fmb and will be
--                used to determine if a specific warehouse/department 
--                combination already exists on the WH_DEPT table.
-- Created:       02-28-01
-------------------------------------------------------------------------------
FUNCTION WH_DEPT_EXISTS(O_error_message   IN OUT   VARCHAR2,
                        O_exists          IN OUT   BOOLEAN,
                        I_wh              IN       WH_DEPT.WH%TYPE,
                        I_dept            IN       WH_DEPT.DEPT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: WH_EXISTS
-- Purpose:       This function will be called from whdept.fmb and will be
--                used to determine if records exist for the passed in warehouse.
--                If I_check_null_dept_ind is sent as 'Y', the function will 
--                determine if a wh-only record exists for the warehouse, i.e,
--                a record containing the warehouse and a NULL dept column.
-- Created:       02-28-01
-------------------------------------------------------------------------------
FUNCTION WH_EXISTS(O_error_message         IN OUT   VARCHAR2,
                   O_exists                IN OUT   BOOLEAN,
                   I_wh                    IN       WH_DEPT.WH%TYPE,
                   I_check_null_dept_ind   IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END WH_DEPT_SQL;
/
