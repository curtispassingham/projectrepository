
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY IMAGE_SQL AS
--------------------------------------------------------------------------------
FUNCTION DEFAULT_DOWN(O_error_message   IN OUT   VARCHAR2,
                      I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'IMAGE_SQL.DEFAULT_DOWN';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_IMAGE_TL is
      select 'x'
        from item_image_tl
       where item in (select im.item
                        from item_master im
                       where (im.item_parent = I_item
                          or im.item_grandparent = I_item)
                         and im.item_level <= im.tran_level)
         for update nowait;

   cursor C_LOCK_ITEM_IMAGE is
      select 'x'
        from item_image
       where item in (select im.item
                        from item_master im
                       where (im.item_parent = I_item
                              or im.item_grandparent = I_item)
                         and im.item_level <= im.tran_level)
         for update nowait;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);

      return FALSE;
   end if;
   ---
   L_table := 'ITEM_IMAGE_TL';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_IMAGE_TL',
                    'ITEM_IMAGE_TL',
                    NULL);
   open C_LOCK_ITEM_IMAGE_TL;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_IMAGE_TL',
                    'ITEM_IMAGE_TL',
                    NULL);
   close C_LOCK_ITEM_IMAGE_TL;
   ---
   L_table := 'ITEM_IMAGE';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_IMAGE',
                    'ITEM_IMAGE',
                    NULL);
   open C_LOCK_ITEM_IMAGE;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_IMAGE',
                    'ITEM_IMAGE',
                    NULL);
   close C_LOCK_ITEM_IMAGE;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ITEM_IMAGE_TL',
                    NULL);
   delete from item_image_tl
    where item in (select im.item
                     from item_master im
                    where (im.item_parent = I_item
                       or im.item_grandparent = I_item)
                      and im.item_level <= im.tran_level);
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ITEM_IMAGE',
                    NULL);
   delete from item_image
    where item in (select im.item
                     from item_master im
                    where (im.item_parent = I_item
                           or im.item_grandparent = I_item)
                      and im.item_level <= im.tran_level);
   ---
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'ITEM_IMAGE',
                    NULL);
   insert into item_image(item,
                          image_name,
                          image_addr,
                          image_desc,
                          create_datetime,
                          last_update_datetime,
                          last_update_id,
                          create_id,
                          image_type,
                          primary_ind,
                          display_priority)
                   select im.item,
                          i.image_name,
                          i.image_addr,
                          i.image_desc,
                          SYSDATE,
                          SYSDATE,
                          USER,
                          USER,
                          i.image_type,
                          i.primary_ind,
                          i.display_priority
                     from item_image i,
                          item_master im
                    where i.item = I_item
                      and (im.item_parent = I_item
                           or im.item_grandparent = I_item)
                      and im.item_level <= im.tran_level;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

END DEFAULT_DOWN;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_PRIORITY (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists             IN OUT   BOOLEAN,
                             I_item               IN       ITEM_IMAGE.ITEM%TYPE,
                             I_image_name         IN       ITEM_IMAGE.IMAGE_NAME%TYPE,
                             I_display_priority   IN       ITEM_IMAGE.DISPLAY_PRIORITY%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'IMAGE_SQL.CHECK_DUP_PRIORITY';
   L_priority_exist   VARCHAR2(1)  := NULL;

   cursor C_PRIORITY_EXISTS is
      select 'x'
        from item_image
       where item = I_item
         and image_name <> I_image_name
         and display_priority = I_display_priority;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_item,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_image_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_image_name,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_PRIORITY_EXISTS',
                    'ITEM_IMAGE',
                    NULL);
   open C_PRIORITY_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PRIORITY_EXISTS',
                    'ITEM_IMAGE',
                    NULL);
   fetch C_PRIORITY_EXISTS into L_priority_exist;

   if L_priority_exist is NOT NULL then
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PRIORITY_EXISTS',
                    'ITEM_IMAGE',
                    NULL);
   close C_PRIORITY_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_DUP_PRIORITY;
---------------------------------------------------------------------------------------------
FUNCTION CHECK_IMAGE_NAME (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_item            IN       ITEM_IMAGE.ITEM%TYPE,
                           I_image_name      IN       ITEM_IMAGE.IMAGE_NAME%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'IMAGE_SQL.CHECK_IMAGE_NAME';
   L_filename_exist   VARCHAR2(1)  := NULL;

   cursor C_FILENAME_EXISTS is
      select 'x'
        from item_image
       where item = I_item
         and image_name = I_image_name;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_item,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_image_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_image_name,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_FILENAME_EXISTS',
                    'ITEM_IMAGE',
                    NULL);
   open C_FILENAME_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_FILENAME_EXISTS',
                    'ITEM_IMAGE',
                    NULL);
   fetch C_FILENAME_EXISTS into L_filename_exist;

   if L_filename_exist is NOT NULL then
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_FILENAME_EXISTS',
                    'ITEM_IMAGE',
                    NULL);
   close C_FILENAME_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_IMAGE_NAME;
---------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_IMAGE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            O_image_name      IN OUT   ITEM_IMAGE.IMAGE_NAME%TYPE,
                            I_item            IN       ITEM_IMAGE.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60)               := 'IMAGE_SQL.GET_PRIMARY_IMAGE';
   L_prim_image   ITEM_IMAGE.IMAGE_NAME%TYPE := NULL;

   cursor C_GET_PRIMARY_IMAGE is
      select image_name
        from item_image
       where item = I_item
         and primary_ind = 'Y';

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_item,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_exists := FALSE;
   O_image_name := NULL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PRIMARY_IMAGE',
                    'ITEM_IMAGE',
                    'Item: '||TO_CHAR(I_item));
   open C_GET_PRIMARY_IMAGE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PRIMARY_IMAGE',
                    'ITEM_IMAGE',
                    'Item: '||TO_CHAR(I_item));
   fetch C_GET_PRIMARY_IMAGE into L_prim_image;

   if L_prim_image is NOT NULL then
      O_exists := TRUE;
      O_image_name := L_prim_image;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PRIMARY_IMAGE',
                    'ITEM_IMAGE',
                    'Item: '||TO_CHAR(I_item));
   close C_GET_PRIMARY_IMAGE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_PRIMARY_IMAGE;
---------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_IMAGE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN  ITEM_IMAGE_TL.ITEM%TYPE,
                              I_image_name      IN   ITEM_IMAGE_TL.IMAGE_NAME%TYPE )
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'IMAGE_SQL.DELETE_ITEM_IMAGE_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_IMAGE_TL is
      select 'x'
        from item_image_tl
       where item = I_item and image_name = I_image_name
         for update nowait;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_image_name is NULL  then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_image_name',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_IMAGE_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_IMAGE_TL',
                    L_table,
                    'I_item '||I_item||' I_image_name '||I_image_name);
   open C_LOCK_ITEM_IMAGE_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_IMAGE_TL',
                    L_table,
                    'I_item '||I_item||' I_image_name '||I_image_name);
   close C_LOCK_ITEM_IMAGE_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ITEM_IMAGE_TL',
                    'I_item '||I_item||' I_image_name '||I_image_name);
                    
   delete from item_image_tl
    where item = I_item 
    and image_name = I_image_name;
    
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ITEM_IMAGE_TL;
---------------------------------------------------------------------------------------------
END IMAGE_SQL;
/
