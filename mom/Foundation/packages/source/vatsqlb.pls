CREATE OR REPLACE PACKAGE BODY VAT_SQL AS

------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
------------------------------------------------------------------------------------
   -- Function Name: LOCK_VAT_DEPS
   -- Purpose      : This function will lock the VAT_DEPS table for update or delete.
------------------------------------------------------------------------------------
FUNCTION LOCK_VAT_DEPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_vat_region      IN       VAT_DEPS.VAT_REGION%TYPE,
                       I_dept            IN       VAT_DEPS.DEPT%TYPE,
                       I_vat_type        IN       VAT_DEPS.VAT_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: LOCK_VAT_ITEM
-- Purpose      : This function will lock the VAT_ITEM table for update or delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_VAT_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists        IN OUT BOOLEAN,
                       I_item          IN     VAT_ITEM.ITEM%TYPE,
                       I_vat_region    IN     VAT_ITEM.VAT_REGION%TYPE,
                       I_vat_type      IN     VAT_ITEM.VAT_TYPE%TYPE,
                       I_active_date   IN     DATE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
------------------------------------------------------------------------------------
   LP_prev_vat_code                        VAT_CODE_RATES.VAT_CODE%TYPE      := NULL;
   LP_prev_active_date                     VAT_CODE_RATES.ACTIVE_DATE%TYPE   := NULL;
   LP_prev_vat_rate                        VAT_CODE_RATES.VAT_RATE%TYPE      := NULL;
   LP_prev_store_vat_region                STORE.VAT_REGION%TYPE             := NULL;
   LP_prev_store_vat_region_store          STORE.STORE%TYPE                  := NULL;
   LP_prev_wh_vat_region                   WH.VAT_REGION%TYPE                := NULL;
   LP_prev_wh_vat_region_wh                WH.WH%TYPE                        := NULL;
   LP_prev_ext_fin_vat_region              PARTNER.VAT_REGION%TYPE           := NULL;
   LP_prev_ext_fin_vat_region_fin          COST_ZONE_GROUP_LOC.LOCATION%TYPE := NULL;

------------------------------------------------------------------------------------
FUNCTION GET_VAT_RATE(O_error_message IN OUT  VARCHAR2,
                      IO_vat_region   IN OUT  VAT_REGION.VAT_REGION%TYPE,
                      IO_vat_code     IN OUT  VAT_CODE_RATES.VAT_CODE%TYPE,
                      O_vat_rate      IN OUT  VAT_CODE_RATES.VAT_RATE%TYPE,
                      I_item          IN      ITEM_MASTER.ITEM%TYPE,
                      I_dept          IN      DEPS.DEPT%TYPE,
                      I_loc_type      IN      COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                      I_location      IN      COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                      I_active_date   IN      VAT_CODE_RATES.ACTIVE_DATE%TYPE,
                      I_vat_type      IN      VAT_item.VAT_TYPE%TYPE,
                      I_store_ind     IN      VARCHAR2)


   RETURN BOOLEAN IS

   L_program                 VARCHAR2(64)                        :=  'VAT_SQL.GET_VAT_RATE';
   L_active_date             VAT_CODE_RATES.ACTIVE_DATE%TYPE     :=   NVL(I_active_date, GET_VDATE);
   L_vat_code                VAT_CODE_RATES.VAT_CODE%TYPE        :=   IO_vat_code;
   L_vat_region              VAT_REGION.VAT_REGION%TYPE          :=   IO_vat_region;
   L_dept                    DEPS.DEPT%TYPE                      :=   I_dept;
   L_pack_ind                ITEM_MASTER.PACK_IND%TYPE;
   L_packitem                ITEM_MASTER.ITEM%TYPE;
   L_pack_retail_in_vat      ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_pack_retail_ex_vat      ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_vat_rate                VAT_ITEM.VAT_RATE%TYPE;
   L_packitem_unit_retail    ITEM_LOC.UNIT_RETAIL%TYPE;
   L_packitem_unit_cost      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_pack_cost_in_vat        ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := 0;
   L_pack_cost_ex_vat        ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := 0;
   --L_pack_type             ITEM_MASTER.PACK_TYPE%TYPE;
   L_sellable_ind            ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind           ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type               ITEM_MASTER.PACK_TYPE%TYPE;
   -- added for pricing.get_detail function
   L_standard_uom_loc        ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_loc ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_loc         ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_loc         ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_loc   ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_loc   ITEM_LOC.MULTI_SELLING_UOM%TYPE;


   cursor C_STORE_VAT_REGION is
      select vat_region
        from store
       where store = I_location;

   cursor C_WH_VAT_REGION is
      select vat_region
        from wh
       where wh = I_location;

   cursor C_EXTERNAL_FINISHER_VAT_REGION is
      select vat_region
        from partner
       where partner_id = TO_CHAR(I_location)
         and partner_type = 'E';

   cursor C_VAT_SKU (item_param item_master.item%TYPE) is
      select vat_code, vat_rate
        from vat_item
       where item = item_param
         and vat_region  = L_vat_region
         and vat_type in (I_vat_type, 'B')
         and active_date <= L_active_date
      order by active_date desc;  -- Much faster than then the correlated subquery.

   cursor C_VAT_RATE is
      select vat_rate
        from vat_code_rates
       where vat_code = L_vat_code
         and active_date <= L_active_date
      order by active_date desc;  -- Much faster than then the correlated subquery.

   cursor C_VAT_DEPS is
       select vd.vat_code, vcr1.vat_rate
         from vat_deps vd, vat_code_rates vcr1
        where dept = L_dept
          and vd.vat_region = L_vat_region
          and vcr1.vat_code = vd.vat_code
          and vd.vat_type in (I_vat_type, 'B')
          and vcr1.active_date <= L_active_date
      order by vcr1.active_date desc;  -- Much faster than then the correlated subquery.

     cursor C_COST is
        select unit_cost
          from item_supp_country
         where item_supp_country.item = L_packitem
           and item_supp_country.primary_country_ind = 'Y'
           and item_supp_country.primary_supp_ind = 'Y';

     cursor C_PACKSKU is
        select item, qty
         from v_packsku_qty
        where pack_no = I_item;

BEGIN
   if IO_vat_region is NULL then
      if I_loc_type = 'S' then
         if I_location = LP_prev_store_vat_region_store then
            L_vat_region := LP_prev_store_vat_region;
         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_STORE_VAT_REGION',
                             'store',
                             NULL);
            open  C_STORE_VAT_REGION;
            SQL_LIB.SET_MARK('FETCH',
                             'C_STORE_VAT_REGION',
                             'store',
                             NULL);
            fetch C_STORE_VAT_REGION into L_vat_region;
            if C_STORE_VAT_REGION%NOTFOUND then
               -- This location doesn't have associated vat region.
               O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                     L_program,
                                                     NULL,
                                                     NULL);
               SQL_LIB.SET_MARK('CLOSE',
                                'C_STORE_VAT_REGION',
                                'store',
                                NULL);
               close C_STORE_VAT_REGION;
               RETURN FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_STORE_VAT_REGION',
                             'store',
                             NULL);
            close C_STORE_VAT_REGION;
            LP_prev_store_vat_region_store := I_location;
            LP_prev_store_vat_region       := L_vat_region;
         end if;
      elsif I_loc_type = 'W' then
         if I_location = LP_prev_wh_vat_region_wh then
            L_vat_region := LP_prev_wh_vat_region;
         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_wh_vat_region',
                             'wh',
                             NULL);
            open  C_WH_VAT_REGION;
            SQL_LIB.SET_MARK('FETCH',
                             'C_wh_vat_region',
                             'wh',
                             NULL);
            fetch C_WH_VAT_REGION into L_vat_region;
            if C_WH_VAT_REGION%NOTFOUND then
               -- This location doesn't have associated vat region.
               O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                      L_program,
                                                      NULL,
                                                      NULL);

               SQL_LIB.SET_MARK('CLOSE',
                                'C_WH_VAT_REGION',
                                'wh',
                                NULL);
               close C_WH_VAT_REGION;
               RETURN FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_wh_vat_region',
                             'wh',
                             NULL);
            close C_WH_VAT_REGION;
            LP_prev_wh_vat_region_wh := I_location;
            LP_prev_wh_vat_region := L_vat_region;
         end if;
      elsif I_loc_type = 'E' then
         if I_location = LP_prev_ext_fin_vat_region_fin then
            L_vat_region := LP_prev_ext_fin_vat_region;
         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_EXTERNAL_FINISHER_VAT_REGION',
                             'external finisher',
                             NULL);
            open  C_EXTERNAL_FINISHER_VAT_REGION;
            SQL_LIB.SET_MARK('FETCH',
                             'C_EXTERNAL_FINISHER_VAT_REGION',
                             'external finisher',
                             NULL);
            fetch C_EXTERNAL_FINISHER_VAT_REGION into L_vat_region;
            if C_EXTERNAL_FINISHER_VAT_REGION%NOTFOUND then
               -- This location doesn't have associated vat region.
               O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                      L_program,
                                                      NULL,
                                                      NULL);
               SQL_LIB.SET_MARK('CLOSE',
                                'C_EXTERNAL_FINISHER_VAT_REGION',
                                'external finisher',
                                NULL);
               close C_EXTERNAL_FINISHER_VAT_REGION;
               RETURN FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXTERNAL_FINISHER_VAT_REGION',
                             'external finisher',
                             NULL);
            close C_EXTERNAL_FINISHER_VAT_REGION;
            LP_prev_ext_fin_vat_region_fin   := I_location;
            LP_prev_ext_fin_vat_region       := L_vat_region;
         end if;
      end if;
      if L_vat_region is NULL and
         IO_vat_code is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
   end if;
   if IO_vat_code is NOT NULL then
      if L_vat_code = LP_prev_vat_code and
         L_active_date = LP_prev_active_date then
         O_vat_rate := LP_prev_vat_rate;
      else
         SQL_LIB.SET_MARK('OPEN',
                          'C_VAT_RATE',
                          'vat_code_rates',
                          NULL);
         open C_VAT_RATE;
         SQL_LIB.SET_MARK('FETCH',
                          'C_VAT_RATE',
                          'vat_code_rates',
                          NULL);
         fetch C_VAT_RATE into O_vat_rate;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_VAT_RATE',
                          'vat_code_rates',
                          NULL);
         close C_VAT_RATE;
         LP_prev_vat_code := L_vat_code;
         LP_prev_active_date := L_active_date;
         LP_prev_vat_rate := O_vat_rate;
      end if;
   else
      if I_vat_type not in ('R', 'C') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      if I_item is not NULL then
        if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                         L_pack_ind,
                                         L_sellable_ind,
                                         L_orderable_ind,
                                         L_pack_type,
                                         I_item) = FALSE then
            return FALSE;
         end if;

         if L_pack_ind = 'Y' and (L_pack_type = 'B' or L_pack_type is NULL) then
            if I_vat_type = 'R' then
               for rec in C_PACKSKU LOOP
                  L_packitem := rec.item;
                  if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                                   L_packitem_unit_retail,
                                                   L_standard_uom_loc,
                                                   L_selling_unit_retail_loc,
                                                   L_selling_uom_loc,
                                                   L_multi_units_loc,
                                                   L_multi_unit_retail_loc,
                                                   L_multi_selling_uom_loc,
                                                   L_packitem,
                                                   I_loc_type,
                                                   I_location) = FALSE then
                     return FALSE;
                  end if;
                  L_pack_retail_in_vat := L_pack_retail_in_vat
                                          + L_packitem_unit_retail * rec.qty;
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_vat_item',
                                   'vat_item',
                                   NULL);
                  open  C_VAT_SKU(L_packitem);
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_vat_item',
                                   'vat_item',
                                   NULL);
                  fetch C_VAT_SKU into L_vat_code,
                                       L_vat_rate;
                  if C_VAT_SKU%NOTFOUND then
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_vat_item',
                                      'vat_item',
                                      NULL);
                     close C_VAT_SKU;
                     if I_store_ind = 'Y' then
                     -- i.e. buyer does not intend to carry mdse of this dept
                     -- at this store or vat region this store belongs to.
                        O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                              L_packitem,
                                                              To_char(L_vat_region),
                                                              NULL);
                        return FALSE;
                     else
                        L_vat_rate := 0;
                     end if;
                  else
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_vat_item',
                                      'vat_item',
                                      NULL);
                     close C_VAT_SKU;
                  end if;
                  L_pack_retail_ex_vat := L_pack_retail_ex_vat
                           + rec.qty * L_packitem_unit_retail /(1 + L_vat_rate /100);
               END LOOP;
               If L_pack_retail_ex_vat > 0 then
                  O_vat_rate :=
                    ((L_pack_retail_in_vat / L_pack_retail_ex_vat) - 1) * 100;
               else
                  O_vat_rate := 0;
               end if;
              else
               for rec in C_PACKSKU LOOP
                  L_packitem := rec.item;
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_cost',
                                   'item_supp_country',
                                   NULL);
                  open C_COST;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_cost',
                                   'item_supp_country',
                                   NULL);
                  fetch C_COST into L_packitem_unit_cost;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_cost',
                                   'item_supp_country',
                                   NULL);
                  close C_COST;
                  ---
                  L_pack_cost_ex_vat := L_pack_cost_ex_vat
                              + L_packitem_unit_cost * rec.qty;
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_vat_item',
                                   'vat_item',
                                   NULL);
                  open  C_VAT_SKU(L_packitem);
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_vat_item',
                                   'vat_item',
                                   NULL);
                  fetch C_VAT_SKU into L_vat_code,
                                       L_vat_rate;
                  if C_VAT_SKU%NOTFOUND then
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_vat_item',
                                      'vat_item',
                                      NULL);
                     close C_VAT_SKU;
                     if I_store_ind = 'Y' then
                     -- i.e. buyer does not intend to carry mdse of this dept
                     -- at this store or vat region this store belongs to.
                        O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                              L_packitem,
                                                              To_char(L_vat_region),
                                                              NULL);
                        return FALSE;
                     else
                        L_vat_rate := 0;
                     end if;
                  else
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_vat_item',
                                      'vat_item',
                                      NULL);
                     close C_VAT_SKU;
                  end if;
                  L_pack_cost_in_vat := L_pack_cost_in_vat
                                        + rec.qty * L_packitem_unit_cost * (1 + L_vat_rate /100);
               END LOOP;
               If L_pack_cost_ex_vat > 0 then
                  O_vat_rate :=
                    ((L_pack_cost_in_vat / L_pack_cost_ex_vat) - 1) * 100;
               else
                  O_vat_rate := 0;
               end if;
            end if;
         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_vat_item',
                             'vat_item',
                             NULL);
            open  C_VAT_SKU(I_item);
            SQL_LIB.SET_MARK('FETCH',
                             'C_vat_item',
                             'vat_item',
                             NULL);
            fetch C_VAT_SKU into L_vat_code,
                                 O_vat_rate;
            if C_VAT_SKU%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_vat_item',
                                'vat_item',
                                NULL);
               close C_VAT_SKU;
               if I_store_ind = 'Y' then
               -- i.e. buyer does not intend to carry mdse of this dept
               -- at this store or vat region this store belongs to.
                  O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                        I_item,
                                                        To_char(L_vat_region),
                                                        NULL);
                  return FALSE;
               else
                  -- this is when this function is called for a price zone
                  -- or for a item, where decision to carry mdse at a store
                  -- is not being made. Therefore we just return vat rate as 0
                  -- if vat rate is not found
                  O_vat_rate := 0;
                  IO_vat_region := L_vat_region;
                  IO_vat_code   := NULL;
                  return TRUE;
               end if;
            end if;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_vat_item',
                             'vat_item',
                             NULL);
            close C_VAT_SKU;
         end if;
      else
         if (L_dept is not NULL and L_vat_region is not NULL) then
            SQL_LIB.SET_MARK('OPEN',
                             'C_vat_deps',
                             'vat_deps',
                             NULL);
            open  C_VAT_DEPS;
            SQL_LIB.SET_MARK('FETCH',
                             'C_vat_dept',
                             'vat_deps',
                             NULL);
            fetch C_VAT_DEPS into L_vat_code, O_vat_rate;
            if C_VAT_DEPS%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_vat_deps',
                                'vat_deps',
                                NULL);
               close C_VAT_DEPS;
               if I_store_ind = 'Y' then
               -- i.e. buyer does not intend to carry mdse of this dept
               -- at this store or vat region this store belongs to.
                  O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_DEPT',
                                                        To_char(L_dept),
                                                        To_char(L_vat_region),
                                                        NULL);
                  return FALSE;
               else
                  O_vat_rate    := 0;
                  IO_vat_region := L_vat_region;
                  IO_vat_code   := NULL;
                  return TRUE;
               end if;
            end if;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_vat_deps',
                             'vat_deps',
                             NULL);
            close C_VAT_DEPS;
         else
            O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            RETURN FALSE;
         end if;
      end if;
   end if;
   IO_vat_region := L_vat_region;
   IO_vat_code   := L_vat_code;
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   RETURN FALSE;

END GET_VAT_RATE;
----------------------------------------------------------------------------------
FUNCTION GET_VAT_RATE(O_error_message IN OUT  VARCHAR2,
                      IO_vat_region   IN OUT  vat_region.vat_region%TYPE,
                      IO_vat_code     IN OUT  vat_code_rates.vat_code%TYPE,
                      O_vat_rate      IN OUT  vat_code_rates.vat_rate%TYPE,
                      I_item          IN      item_master.item%TYPE,
                      I_dept          IN      deps.dept%TYPE,
                      I_loc_type      IN      cost_zone_group_loc.loc_type%TYPE,
                      I_location      IN      cost_zone_group_loc.location%TYPE,
                      I_active_date   IN      vat_code_rates.active_date%TYPE,
                      I_vat_type      IN      vat_item.vat_type%TYPE)
    RETURN BOOLEAN IS
BEGIN

   if GET_VAT_RATE(O_error_message,
                   IO_vat_region,
                   IO_vat_code,
                   O_vat_rate,
                   I_item,
                   I_dept,
                   I_loc_type,
                   I_location,
                   I_active_date,
                   I_vat_type,
                   NULL) = FALSE then
      return FALSE;
   end if;
   return TRUE;
END GET_VAT_RATE;
----------------------------------------------------------------------------------
FUNCTION VAT_REGION_DESC (O_error_message IN OUT VARCHAR2,
                          I_vat_region    IN     VAT_ITEM.VAT_REGION%TYPE,
                          O_vat_desc      IN OUT VARCHAR2)

RETURN BOOLEAN IS

   cursor C_ATTRIB is
   select vat_region_name
     from v_vat_region_tl
    where vat_region = I_vat_region;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_ATTRIB', 'V_VAT_REGION_TL', null);
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH' , 'C_ATTRIB', 'V_VAT_REGION_TL', null);
   fetch C_ATTRIB into O_vat_desc;
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_VAT_REGION',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'V_VAT_REGION_TL', null);
      close C_ATTRIB;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'V_VAT_REGION_TL', null);
   close C_ATTRIB;


   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        'VAT_SQL.VAT_REGION_DESC',
                        to_char(SQLCODE));
   RETURN FALSE;

END VAT_REGION_DESC;
-----------------------------------------------------------------------
FUNCTION VAT_CODE_DESC (O_error_message    IN OUT VARCHAR2,
                        I_vat_code         IN     vat_codes.vat_code%TYPE,
                        O_vat_desc         IN OUT VARCHAR2)

RETURN BOOLEAN IS

   cursor C_ATTRIB is
   select vat_code_desc
     from v_vat_codes_tl
    where vat_code = I_vat_code;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_ATTRIB', 'V_VAT_CODES_TL', I_vat_code);
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH' , 'C_ATTRIB', 'V_VAT_CODES_TL', I_vat_code);
   fetch C_ATTRIB into O_vat_desc;
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_VAT_CODE',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'V_VAT_CODES_TL', I_vat_code);
      close C_ATTRIB;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'V_VAT_CODES_TL', null);
   close C_ATTRIB;
   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        'VAT_SQL.VAT_CODE_DESC',
                        to_char(SQLCODE));
   RETURN FALSE;

END VAT_CODE_DESC;
------------------------------------------------------------------------------------------------
FUNCTION VAT_SKU_EXIST(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_item          IN     item_master.item%TYPE,
                       I_vat_region    IN     vat_item.vat_region%TYPE,
                       I_active_date   IN     vat_item.active_date%TYPE,
                       I_vat_type      IN     vat_item.vat_type%TYPE ) return BOOLEAN is

   L_dummy  VARCHAR2(1) := NULL;

   cursor C_vat_item is
      select 'x'
        from vat_item
       where item = I_item
         and vat_region = I_vat_region
         and active_date = I_active_date
         and (vat_type = decode(I_vat_type, 'B', vat_type, I_vat_type) or
              vat_type = 'B')
         and rownum = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_vat_item', 'vat_item', I_item);
   open C_vat_item;
   SQL_LIB.SET_MARK('FETCH', 'C_vat_item', 'vat_item', I_item);
   fetch C_vat_item into L_dummy;
   SQL_LIB.SET_MARK('CLOSE', 'C_vat_item', 'vat_item', I_item);
   close C_vat_item;

   if L_dummy = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
         'VAT_SQL.VAT_SKU_EXIST', TO_CHAR(SQLCODE));
      return FALSE;
END VAT_SKU_EXIST;
------------------------------------------------------------------------------------------------
FUNCTION VAT_DEPT_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exists        IN OUT BOOLEAN,
                        I_dept          IN     vat_deps.dept%TYPE,
                        I_vat_region    IN     vat_deps.vat_region%TYPE,
                        I_vat_type      IN     vat_deps.vat_type%TYPE ) return BOOLEAN is

   L_dummy  VARCHAR2(1) := NULL;

   cursor C_vat_dept is
      select 'x'
        from vat_deps
       where dept = I_dept
         and vat_region = I_vat_region
         and (vat_type = decode(I_vat_type, 'B', vat_type, I_vat_type)
          or vat_type = 'B');

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_vat_dept', 'vat_deps', I_dept);
   open C_vat_dept;
   SQL_LIB.SET_MARK('FETCH', 'C_vat_dept', 'vat_deps', I_dept);
   fetch C_vat_dept into L_dummy;
   SQL_LIB.SET_MARK('CLOSE', 'C_vat_dept', 'vat_deps', I_dept);
   close C_vat_dept;

   if L_dummy = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
         'VAT_SQL.VAT_DEPT_EXIST', TO_CHAR(SQLCODE));
      return FALSE;
END VAT_DEPT_EXIST;
------------------------------------------------------------------------------------------------
FUNCTION VAT_CODE_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exists        IN OUT BOOLEAN,
                        I_vat_code      IN     vat_codes.vat_code%TYPE ) return BOOLEAN is

   L_dummy  VARCHAR2(1) := NULL;

   cursor C_vat_code is
      select 'x'
        from vat_codes
       where vat_code = I_vat_code;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_vat_code', 'vat_codes', I_vat_code);
   open C_vat_code;
   SQL_LIB.SET_MARK('FETCH', 'C_vat_code', 'vat_codes', I_vat_code);
   fetch C_vat_code into L_dummy;
   SQL_LIB.SET_MARK('CLOSE', 'C_vat_code', 'vat_codes', I_vat_code);
   close C_vat_code;

   if L_dummy = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'VAT_SQL.VAT_CODE_EXIST',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VAT_CODE_EXIST;
------------------------------------------------------------------------------------------------
FUNCTION DATE_CODE_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exists         IN OUT BOOLEAN,
                        I_active_date    IN     vat_code_rates.active_date%TYPE,
                        I_vat_code       IN     vat_codes.vat_code%TYPE ) return BOOLEAN is

   L_dummy  VARCHAR2(1) := NULL;

   cursor C_date_code is
      select 'x'
        from vat_code_rates
       where vat_code = I_vat_code
         and active_date = I_active_date;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_date_code',
                    'vat_code_rates',
                    I_vat_code);
   open C_date_code;
   SQL_LIB.SET_MARK('FETCH',
                    'C_date_code',
                    'vat_code_rates',
                    I_vat_code);
   fetch C_date_code into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_date_code',
                    'vat_code_rates',
                    I_vat_code);
   close C_date_code;

   if L_dummy = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'VAT_SQL.DATE_CODE_EXIST',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DATE_CODE_EXIST;
-----------------------------------------------------------------------------------------------
FUNCTION VAT_RATE_EXIST(O_error_message IN OUT VARCHAR2,
                        I_vat_code      IN     VAT_CODES.VAT_CODE%TYPE )
return BOOLEAN IS

L_program_name   VARCHAR2(60)  := 'VAT_SQL.VATE_RATE_EXIST';
L_dummy         VARCHAR2(1);

CURSOR C_VAT_RATE is
   select 'Y'
     from vat_code_rates
    where vat_code = I_vat_code;

BEGIN
 if I_vat_code is NULL then
    O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                          L_program_name,
                                          NULL,
                                          NULL);
    return FALSE;
 end if;

 SQL_LIB.SET_MARK('OPEN' ,
                  'C_VAT_RATE',
                  'VAT_CODE_RATES',
                  I_vat_code);
 open C_VAT_RATE;
 SQL_LIB.SET_MARK('FETCH' ,
                  'C_VAT_RATE',
                  'VAT_CODE_RATES',
                  I_vat_code);
 fetch C_VAT_RATE into L_dummy;
 if C_VAT_RATE%NOTFOUND then
    O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_REQUIRED',
                                          I_vat_code,
                                          NULL,
                                          NULL);
    SQL_LIB.SET_MARK('CLOSE' ,
                     'C_VAT_RATE',
                     'VAT_CODE_RATES',
                     I_vat_code);
    close C_VAT_RATE;
    return FALSE;
 end if;
 SQL_LIB.SET_MARK('CLOSE' ,
                  'C_VAT_RATE',
                  'VAT_CODE_RATES',
                  I_vat_code);
 close C_VAT_RATE;

 return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VAT_RATE_EXIST;
-----------------------------------------------------------------------------------------------
FUNCTION VERIFY_VAT_TYPE(O_error_message IN OUT VARCHAR2,
                         I_dept          IN     VAT_DEPS.DEPT%TYPE,
                         I_vat_region    IN     VAT_DEPS.VAT_REGION%TYPE)
return BOOLEAN is
L_program_name    VARCHAR2(60)  := 'VAT_SQL.VERIFY_VAT_TYPE';
L_vat_type        VAT_DEPS.VAT_TYPE%TYPE;
L_dummy          VARCHAR2(1);

cursor C_DEFAULT_REGION is
      select 'Y'
        from vat_deps
       where dept = I_dept
         and vat_region = I_vat_region
         and vat_type in ('B', 'R');

cursor C_INVC_DEFAULT_REGION is
   select 'Y'
     from vat_deps vd1
    where vd1.dept = I_dept
      and vd1.vat_region = I_vat_region
      and (vd1.vat_type = 'B'
           or (vd1.vat_type = 'R'
                   and exists(select 'x'
                                from vat_deps vd2
                               where vd2.vat_type = 'C'
                                 and vd1.dept = vd2.dept
                                 and vd1.vat_region = vd2.vat_region)));

cursor C_GET_VAT_TYPE is
   select vat_type
     from vat_deps
    where dept = I_dept
      and vat_region = I_vat_region;

BEGIN

   if I_dept is NULL or I_vat_region is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT', L_program_name, NULL, NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN' ,
                    'C_DEFAULT_REGION',
                    'VAT_DEPS',
                    I_dept);
   open C_DEFAULT_REGION;
   SQL_LIB.SET_MARK('FETCH' ,
                    'C_DEFAULT_REGION',
                    'VAT_DEPS',
                    I_dept);
   fetch C_DEFAULT_REGION into L_dummy;

   if C_DEFAULT_REGION%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INVC_MATCH_OFF_VAT_TYPE',
                                            I_vat_region,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE' ,
                       'C_DEFAULT_REGION',
                       'VAT_DEPS',
                       I_dept);
      close C_DEFAULT_REGION;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE' ,
                    'C_DEFAULT_REGION',
                    'VAT_DEPS',
                    I_dept);
   close C_DEFAULT_REGION;

    return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VERIFY_VAT_TYPE;
-------------------------------------------------------------------------------

--new Function added ---
FUNCTION GET_MIN_CODE_ACTIVE_DATE(O_error_message     IN OUT VARCHAR2,
                                  O_min_date          IN OUT VAT_CODE_RATES.ACTIVE_DATE%TYPE,
                                  I_vat_code          IN     VAT_CODE_RATES.VAT_CODE%TYPE)

   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)  := 'VAT_SQL.GET_MIN_CODE_ACTIVE_DATE';

   cursor C_GET_MIN_ACTIVE_DATE is
      select min(active_date)
        from vat_code_rates
       where vat_code =I_vat_code;

BEGIN

   O_min_date := NULL;

   SQL_LIB.SET_MARK('OPEN','C_GET_MIN_ACTIVE_DATE','vat_code_rates',I_vat_code);
   open C_GET_MIN_ACTIVE_DATE;

   SQL_LIB.SET_MARK('FETCH','C_GET_MIN_ACTIVE_DATE','vat_code_rates',I_vat_code);
   fetch C_GET_MIN_ACTIVE_DATE into O_min_date;

   SQL_LIB.SET_MARK('CLOSE','C_GET_MIN_ACTIVE_DATE','vat_code_rates',I_vat_code);
   close C_GET_MIN_ACTIVE_DATE;

   if O_min_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_VAT_CODE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_MIN_CODE_ACTIVE_DATE;
--------------------------------------------------------------------------------------------
FUNCTION VAT_EXISTS_LOC(O_error_message   IN OUT VARCHAR2,
                        O_exists          IN OUT BOOLEAN,
                        I_item            IN     item_master.item%type,
                        I_location        IN     store.store%type,
                        I_location_type   IN     VARCHAR2,
                        I_date            IN     DATE)
    RETURN BOOLEAN is

    L_dummy VARCHAR2(1) :=NULL;
    L_program  VARCHAR2(64)  :='VAT_SQL.VAT_EXISTS_LOC';
    I_loc   store.store%type;
    L_date  date;


    cursor C_VAT_ITEM_ST is
       select 'x'
    from vat_item v, store s
       where v.item = I_item
       and s.store = I_loc
       and s.vat_region = v.vat_region
       and v.active_date <= L_date;

    cursor C_VAT_ITEM_WH is
       select 'x'
    from vat_item v, wh w
       where v.item = I_item
       and w.wh = I_loc
       and w.vat_region = v.vat_region
       and v.active_date <= L_date;

BEGIN
      if I_location_type = 'S' then
        SQL_LIB.SET_MARK('OPEN','C_VAT_ITEM_ST','vat_item,store',I_item);
        open C_VAT_ITEM_ST;
        SQL_LIB.SET_MARK('FETCH','C_VAT_ITEM_ST','vat_item,store',I_item);
        fetch C_VAT_ITEM_ST into L_dummy;
        SQL_LIB.SET_MARK('CLOSE','C_VAT_ITEM_ST','vat_item,store',I_item);
        CLOSE C_VAT_ITEM_ST;

           if L_dummy = 'x' then
              O_exists := TRUE;
           else
              O_exists := FALSE;
           end if;

           return TRUE;

       end if;

      if I_location_type = 'W' then
        SQL_LIB.SET_MARK('OPEN','C_VAT_ITEM_WH','vat_item,store',I_item);
        open C_VAT_ITEM_WH;
        SQL_LIB.SET_MARK('FETCH','C_VAT_ITEM_WH','vat_item,store',I_item);
        fetch C_VAT_ITEM_WH into L_dummy;
        SQL_LIB.SET_MARK('CLOSE','C_VAT_ITEM_WH','vat_item,store',I_item);
        CLOSE C_VAT_ITEM_WH;

        if L_dummy = 'x' then
           O_exists := TRUE;
        else
           O_exists := FALSE;
        end if;
        return TRUE;
      end if;

  EXCEPTION

  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VAT_EXISTS_LOC;
-------------------------------------------------------------------------------
FUNCTION VERIFY_VAT_REGION(O_error_message IN OUT VARCHAR2,
                           I_dept          IN     VAT_DEPS.DEPT%TYPE)
return BOOLEAN is
   L_program_name    VARCHAR2(60)  := 'VAT_SQL.VERIFY_VAT_REGION';
   L_region_missing  VARCHAR2(1)   := 'N';
   ---
   cursor C_VAT_REGION is
      select 'Y'
        from vat_region
       where exists (select vat_region from vat_region
                     minus
                     select vat_region from vat_deps
                     where dept = I_dept
                       and vat_type in ('B', 'R'));
   ---
BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program_name, NULL, NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN' , 'C_VAT_REGION', 'VAT_DEPS', I_dept);
   open C_VAT_REGION;
   SQL_LIB.SET_MARK('FETCH' , 'C_VAT_REGION', 'VAT_DEPS', I_dept);
   fetch C_VAT_REGION into L_region_missing;
   close C_VAT_REGION;
   ---
   if L_region_missing = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('VAT_DEPS_MISSING_REGION',
                                            I_dept,NULL,NULL);
      return FALSE;
   else
      return TRUE;
   end if;
EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program_name, TO_CHAR(SQLCODE));
      return FALSE;
END VERIFY_VAT_REGION;
-------------------------------------------------------------------------------
FUNCTION VATRATE_ITEM_DEPS_DELETION(O_error_message IN OUT VARCHAR2,
                      I_vat_rate      IN     VAT_ITEM.VAT_RATE%TYPE,
                      I_vat_code      IN     VAT_DEPS.VAT_CODE%TYPE,
                      I_active_date   IN     VAT_ITEM.ACTIVE_DATE%TYPE)
return BOOLEAN is

L_program    VARCHAR2(60) := 'VAT_SQL.VATRATE_ITEM_DEPS_DELETION';
L_dummy        VARCHAR2(1)  := 'Y';


   cursor C_CHECK is
      select 'x'
        from vat_item
       where vat_item.vat_rate    = I_vat_rate
         and vat_item.vat_code    = I_vat_code
         and vat_item.active_date >= I_active_date
         and rownum = 1;


BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK',
                    'vat_item',
                    NULL);
   open C_CHECK;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK',
                    'vat_item',
                    NULL);
   fetch C_CHECK into L_dummy;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK',
                    'vat_item',
                    NULL);
   close C_CHECK;
   ---
   if L_dummy = 'x' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_VAT_RATE',
                                            NULL,NULL,NULL);
      return FALSE;
   else
      return TRUE;
   end if;
   ---

   return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      return FALSE;
END VATRATE_ITEM_DEPS_DELETION;
-------------------------------------------------------------------------------
FUNCTION VATCODE_ITEM_DEPS_DELETION(O_error_message       IN OUT VARCHAR2,
                      I_vat_item_code      IN     VAT_ITEM.VAT_CODE%TYPE,
                      I_vat_deps_code      IN     VAT_DEPS.VAT_CODE%TYPE)
return BOOLEAN is

L_program    VARCHAR2(60) := 'VAT_SQL.VATCODE_ITEM_DEPS_DELETION';
L_dummy        VARCHAR2(1)  := 'Y';

   cursor C_INVC_MERCH_VAT_VALIDATE is
      select 'x'
        from invc_merch_vat invc
       where invc.vat_code = I_vat_item_code
         and rownum = 1
   UNION ALL
      select 'x'
        from iif_merch_vat iif
       where iif.vat_code = I_vat_item_code
         and rownum = 1
   UNION ALL
      select 'x'
        from invc_non_merch inm
       where inm.vat_code = I_vat_item_code
         and rownum = 1;


   cursor C_VERIFY is
      select 'x'
        from vat_deps
       where vat_deps.vat_code = I_vat_deps_code
         and rownum = 1
   UNION ALL
      select 'x'
        from vat_item
       where vat_item.vat_code = I_vat_item_code
         and rownum = 1;


BEGIN

   open C_VERIFY;
   fetch C_VERIFY into L_dummy;
   close C_VERIFY;
   ---
   if L_dummy = 'x' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_VAT_CODE', NULL,NULL,NULL);
      return FALSE;
   end if;
   open C_INVC_MERCH_VAT_VALIDATE;
   fetch C_INVC_MERCH_VAT_VALIDATE into L_dummy;
   close C_INVC_MERCH_VAT_VALIDATE;
   ---
   if L_dummy = 'x' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_VAT_INVC', NULL,NULL,NULL);
      return FALSE;
   else
      return TRUE;
   end if;
   ---

return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, TO_CHAR(SQLCODE));
      return FALSE;
END VATCODE_ITEM_DEPS_DELETION;
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_VAT_DEPT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_dept_detail_tbl   IN       MERCH_SQL.VAT_DEPT_DETAIL_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'VAT_SQL.UPDATE_VAT_DEPT';

BEGIN

   FOR i in I_dept_detail_tbl.first..I_dept_detail_tbl.last LOOP
      if not LOCK_VAT_DEPS(O_error_message,
                           I_dept_detail_tbl(i).vat_region,
                           I_dept_detail_tbl(i).dept,
                           I_dept_detail_tbl(i).vat_type) then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'VAT_DEPS',
                       'vat_region: ' || I_dept_detail_tbl(i).vat_region);
      update vat_deps
         set vat_code   = I_dept_detail_tbl(i).vat_code
       where vat_region = I_dept_detail_tbl(i).vat_region
         and dept       = I_dept_detail_tbl(i).dept
         and vat_type   = I_dept_detail_tbl(i).vat_type;
      ---
      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      ---
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_VAT_DEPT;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_VAT_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item          IN     VAT_ITEM.ITEM%TYPE,
                         I_vat_region    IN     VAT_ITEM.VAT_REGION%TYPE,
                         I_type          IN     VAT_ITEM.VAT_TYPE%TYPE,
                         I_active_date   IN     DATE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'VAT_SQL.DELETE_VAT_ITEM';
   L_exists       BOOLEAN;

BEGIN
   if not LOCK_VAT_ITEM(O_error_message,
                        L_exists,
                        I_item,
                        I_vat_region,
                        I_type,
                        I_active_date) then
      return FALSE;
   end if;

   if L_exists then

      SQL_LIB.SET_MARK('DELETE', NULL, 'VAT_ITEM','item: '||I_item||
                                               ' vat region: '||I_vat_region||
                                               ' vat type: '||I_type);
      delete from VAT_ITEM
       where item = I_item
         and vat_region = I_vat_region
         and vat_type = I_type
         and active_date = I_active_date;
      ---
      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      ---
   else --- don't return false in the case if create item vat may not exist for delete
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS',
                                            NULL,
                                            NULL,
                                            NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_VAT_ITEM;
-----------------------------------------------------------------------------------------------
FUNCTION GET_VAT_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_vat_code        IN OUT   VAT_CODE_RATES.VAT_CODE%TYPE,
                      I_vat_rate        IN       VAT_CODE_RATES.VAT_RATE%TYPE,
                      I_vat_date        IN       VAT_CODE_RATES.ACTIVE_DATE%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_VAT_CODE is
      select vat_code
        from vat_code_rates
       where vat_rate     = I_vat_rate
         and active_date <= I_vat_date;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_VAT_CODE',
                    'VAT_CODE_RATES',
                    I_vat_rate);
   open C_GET_VAT_CODE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_VAT_CODE',
                    'VAT_CODE_RATES',
                    I_vat_rate);
   fetch C_GET_VAT_CODE into O_vat_code;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_VAT_CODE',
                    'VAT_CODE_RATES',
                    I_vat_rate);
   close C_GET_VAT_CODE;

   if O_vat_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'VAT_SQL.GET_VAT_CODE',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_VAT_CODE;
-----------------------------------------------------------------------------------------
FUNCTION GET_ZERO_RATE_VAT_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_vat_code        IN OUT   VAT_CODE_RATES.VAT_CODE%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_ZERO_RATE_VAT_CODE is
      select v.vat_code
        from vat_code_rates v
       where vat_rate = 0
         and active_date <= (select vdate
                               from period)
         and NOT EXISTS (select 'X'
                           from vat_code_rates v2
                          where v2.vat_code = v.vat_code
                            and v2.active_date > v.active_date
                            and rownum = 1);

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ZERO_RATE_VAT_CODE',
                    'VAT_CODE_RATES',
                    0);
   open C_GET_ZERO_RATE_VAT_CODE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ZERO_RATE_VAT_CODE',
                    'VAT_CODE_RATES',
                    0);
   fetch C_GET_ZERO_RATE_VAT_CODE into O_vat_code;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ZERO_RATE_VAT_CODE',
                    'VAT_CODE_RATES',
                    0);
   close C_GET_ZERO_RATE_VAT_CODE;

   if O_vat_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ZERO_VAT_RATE_NOTFOUND',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'VAT_SQL.GET_ZERO_RATE_VAT_CODE',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ZERO_RATE_VAT_CODE;
--------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC_VAT_BULK(O_error_message    OUT VARCHAR2,
                          IO_ild_vat_tbl  IN OUT OBJ_ITEM_LOCATION_DATE_VAT_TBL)
RETURN BOOLEAN IS

L_program   VARCHAR2(100) := 'VAT_SQL.GET_ITEM_LOC_VAT_BULK';

cursor C_VAT is
   select OBJ_ITEM_LOCATION_DATE_VAT_REC(ild.item,
                                         ild.location,
                                         ild.action_date,
                                         --
                                         nvl(r.vat_rate, ild.vat_rate),
                                         ild.vat_value,
                                         nvl(r.class_vat_ind, ild.class_vat_ind))
     from TABLE(CAST(IO_ild_vat_tbl AS OBJ_ITEM_LOCATION_DATE_VAT_TBL)) ild,
          (   select distinct im.item,
                     s.store location,
                     ild1.action_date,
                     --
                     first_value(vi.vat_rate) over
                     (partition by im.item, s.store order by vi.active_date desc) vat_rate,
                     --
                     0,
                     c.class_vat_ind
                from TABLE(CAST(IO_ild_vat_tbl AS OBJ_ITEM_LOCATION_DATE_VAT_TBL)) ild1,
                     store s,
                     vat_item vi,
                     item_master im,
                     class c
               where c.class = im.class
                 and c.dept = im.dept
                 --
                 and im.item = vi.item
                 --
                 and vi.vat_type in ('R', 'B')
                 and vi.vat_region = s.vat_region
                 and vi.active_date <= ild1.action_date
                 and vi.item = ild1.item
                 --
                 and s.store = ild1.location
           union all
              select distinct im.item,
                     w.wh location,
                     ild2.action_date,
                     --
                     first_value(vi.vat_rate) over
                     (partition by im.item, w.wh order by vi.active_date desc) vat_rate,
                     --
                     0,
                     c.class_vat_ind
                from TABLE(CAST(IO_ild_vat_tbl AS OBJ_ITEM_LOCATION_DATE_VAT_TBL)) ild2,
                     wh w,
                     vat_item vi,
                     item_master im,
                     class c
               where c.class = im.class
                 and c.dept = im.dept
                 --
                 and im.item = vi.item
                 --
                 and vi.vat_type in ('R', 'B')
                 and vi.vat_region = w.vat_region
                 and vi.active_date <= ild2.action_date
                 and vi.item = ild2.item
                 --
                 and w.wh = ild2.location) r
    where ild.item = r.item (+)
      and ild.location = r.location (+)
      and ild.action_date = r.action_date (+);

BEGIN

   open C_VAT;
   fetch C_VAT bulk collect into IO_ild_vat_tbl;
   close C_VAT;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ITEM_LOC_VAT_BULK;
-----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION BODY
-----------------------------------------------------------------------------------------------
FUNCTION LOCK_VAT_DEPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_vat_region      IN       VAT_DEPS.VAT_REGION%TYPE,
                       I_dept            IN       VAT_DEPS.DEPT%TYPE,
                       I_vat_type        IN       VAT_DEPS.VAT_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'VAT_SQL.LOCK_VAT_DEPS';
   L_table        VARCHAR2(10)  := 'VAT_DEPS';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_VAT_DEPS IS
      select 'x'
        from vat_deps
       where vat_region = I_vat_region
         and dept       = I_dept
         and vat_type   = I_vat_type
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_VAT_DEPS',
                    'VAT_DEPS',
                    'vat_region: '||I_vat_region);
   open C_LOCK_VAT_DEPS;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_VAT_DEPS',
                    'VAT_DEPS',
                    'vat_region: '||I_vat_region);
   close C_LOCK_VAT_DEPS;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'vat_region ' || I_vat_region||
                                            'dept ' || I_dept);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_VAT_DEPS;
---------------------------------------------------------------------
FUNCTION LOCK_VAT_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists        IN OUT BOOLEAN,
                       I_item          IN     VAT_ITEM.ITEM%TYPE,
                       I_vat_region    IN     VAT_ITEM.VAT_REGION%TYPE,
                       I_vat_type      IN     VAT_ITEM.VAT_TYPE%TYPE,
                       I_active_date   IN     DATE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'VAT_SQL.LOCK_VAT_ITEM';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   L_dummy        VARCHAR2(1);

   CURSOR C_LOCK_VAT_ITEM IS
      select 'x'
        from vat_item
       where item = I_item
         and vat_region = I_vat_region
         and vat_type = I_vat_type
         and active_date = I_active_date
         and rownum = 1
         for update nowait;
BEGIN
   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_VAT_ITEM',
                    'VAT_ITEM',
                    'item: '||I_item||' vat region: '||I_vat_region||' vat type: '||I_vat_type);
   open C_LOCK_VAT_ITEM;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCK_VAT_ITEM',
                    'VAT_ITEM',
                    'item: '||I_item||' vat region: '||I_vat_region||' vat type: '||I_vat_type);
   fetch C_LOCK_VAT_ITEM into L_dummy;
   if C_LOCK_VAT_ITEM%FOUND then
      O_exists := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_VAT_ITEM',
                    'VAT_ITEM',
                    'item: '||I_item||' vat region: '||I_vat_region||' vat type: '||I_vat_type);
   close C_LOCK_VAT_ITEM;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            NULL,
                                            'VAT_ITEM',
                                            'vat_region '|| I_vat_region||' vat type: '||I_vat_type);

      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_VAT_ITEM;
-----------------------------------------------------------------------------------------
END;
/