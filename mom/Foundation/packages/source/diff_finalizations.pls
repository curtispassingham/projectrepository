CREATE OR REPLACE PACKAGE DIFF_FINALIZATION AUTHID CURRENT_USER
AS
  --------------------------------------------------------------------------------
  FUNCTION VALIDATE_INPUTS(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
      O_parent_item   IN OUT ITEM_MASTER.ITEM%TYPE ,
      O_VPN           IN OUT ITEM_SUPPLIER.VPN%TYPE,
      I_ITEM          IN     ITEM_MASTER.ITEM%TYPE )
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION INITIALIZE(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
      I_ITEM          IN ITEM_MASTER.ITEM%TYPE ,
      I_parent_item   IN ITEM_MASTER.ITEM%TYPE ,
      I_VPN           IN item_supplier.vpn%type )
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION MAP_DIFFS(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
      I_DIFF_SEQ      IN NUMBER)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION POPULATE_DIFF_1(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION POPULATE_DIFF_2(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION POPULATE_DIFF_3(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION POPULATE_DIFF_4(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION FINALIZE(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION VALIDATE_DIFF_GROUP(
      O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      O_DIFF_GROUP_DESC IN OUT DIFF_IDS.DIFF_DESC%TYPE,
      I_DIFF            IN     DIFF_IDS.DIFF_ID%TYPE)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION VALIDATE_DIFF_ID(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      O_DIFF_DESC     IN OUT DIFF_IDS.DIFF_DESC%TYPE,
      I_DIFF_SEQ      IN     NUMBER,
      I_DIFF_GROUP    IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
      I_DIFF_ID       IN     DIFF_IDS.DIFF_ID%TYPE )
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION UPDATE_DIFF_GROUP(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_DIFF_GROUP    IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
    I_DIFF_DESC     IN     DIFF_IDS.DIFF_DESC%TYPE, 
    I_DIFF_SEQ      IN     NUMBER)
  RETURN BOOLEAN;
  --------------------------------------------------------------------------------
END DIFF_FINALIZATION;
/