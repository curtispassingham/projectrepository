
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XLOCTRT_VALIDATE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the create and modify messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XLocTrtDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will populate all the fields in the LOC_TRAITS%ROWTYPE
   --                with the values from the create and modify RIB messages.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_loctrait_rec    OUT    NOCOPY   LOC_TRAITS%ROWTYPE,
                         I_message         IN              "RIB_XLocTrtDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will populate all the fields in the LOC_TRAITS%ROWTYPE
   --                with the values from the delete RIB message.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_loctrait_rec    OUT    NOCOPY   LOC_TRAITS%ROWTYPE,
                         I_message         IN              "RIB_XLocTrtRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCTRAIT_EXISTS
   -- Purpose      : This function will check loc_trait existence by calling LOC_TRAITS_SQL.LOC_TRAIT_EXISTS.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCTRAIT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loc_trait       IN       LOC_TRAITS.LOC_TRAIT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_loctrait_rec    OUT    NOCOPY   LOC_TRAITS%ROWTYPE,
                       I_message         IN              "RIB_XLocTrtDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'RMSSUB_XLOCTRT_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XLOCTRT.LP_mod_type then
      if not LOCTRAIT_EXISTS(O_error_message,
                             I_message.hier_trait_id) then
         return FALSE;
      end if;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_loctrait_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_loctrait_rec    OUT    NOCOPY   LOC_TRAITS%ROWTYPE,
                       I_message         IN              "RIB_XLocTrtRef_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XLOCTRT_VALIDATE.CHECK_MESSAGE';

BEGIN

   if I_message.hier_trait_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'hier_trait_id', NULL, NULL);
      return FALSE;
   end if;

   if not LOCTRAIT_EXISTS(O_error_message,
                          I_message.hier_trait_id) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_loctrait_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XLocTrtDesc_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XLOCTRT_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.hier_trait_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'hier_trait_id', NULL, NULL);
      return FALSE;
   end if;

   if I_message.trait_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'trait_desc', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_loctrait_rec    OUT    NOCOPY   LOC_TRAITS%ROWTYPE,
                         I_message         IN              "RIB_XLocTrtDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XLOCTRT_VALIDATE.POPULATE_RECORD';

BEGIN

   O_loctrait_rec.loc_trait   := I_message.hier_trait_id;
   O_loctrait_rec.description := I_message.trait_desc;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_loctrait_rec    OUT    NOCOPY   LOC_TRAITS%ROWTYPE,
                         I_message         IN              "RIB_XLocTrtRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XLOCTRT_VALIDATE.POPULATE_RECORD';

BEGIN

   O_loctrait_rec.loc_trait := I_message.hier_trait_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCTRAIT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loc_trait       IN       LOC_TRAITS.LOC_TRAIT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XLOCTRT_VALIDATE.LOCTRAIT_EXISTS';
   L_exists       BOOLEAN      := FALSE;

BEGIN

   if LOC_TRAITS_SQL.LOC_TRAIT_EXISTS(O_error_message,
                                      L_exists,
                                      I_loc_trait) = FALSE then
      return FALSE;
   end if;

   if L_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TRAIT', I_loc_trait);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCTRAIT_EXISTS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XLOCTRT_VALIDATE;
/
