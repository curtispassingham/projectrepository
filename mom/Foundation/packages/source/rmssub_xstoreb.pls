
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XSTORE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: CAPITALIZE_FIELDS
   -- Purpose      : This private function should convert specific char fields to uppercase.
-------------------------------------------------------------------------------------------------------
FUNCTION CAPITALIZE_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_message        IN OUT NOCOPY   "RIB_XStoreDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_program               VARCHAR2(25) := 'RMSSUB_XSTORE.CONSUME';
   L_store_rec             STORE_SQL.STORE_REC;
   L_ref_message           "RIB_XStoreRef_REC";
   L_message               "RIB_XStoreDesc_REC";
   L_message_type          VARCHAR2(15) := LOWER(I_message_type);

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   if L_message_type in (LP_cre_type, LP_mod_type, LP_loctrt_cre_type, LP_wt_cre_type, LP_addr_cre_type, LP_addr_mod_type) then
      L_message := treat(I_MESSAGE AS "RIB_XStoreDesc_REC");
      ---
      if L_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;
      
      if CAPITALIZE_FIELDS(O_error_message,
                           L_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Validate Message Contents
      if RMSSUB_XSTORE_VALIDATE.CHECK_MESSAGE(O_error_message,
                                              L_store_rec,
                                              L_message,
                                              L_message_type) = FALSE then

         raise PROGRAM_ERROR;
      end if;

      -- INSERT/UPDATE table
      if RMSSUB_XSTORE_SQL.PERSIST(O_error_message,
                                   L_store_rec,
                                   L_message_type) = FALSE then

         raise PROGRAM_ERROR;
      end if;
   elsif L_message_type in (LP_del_type, LP_loctrt_del_type, LP_wt_del_type, LP_addr_del_type) then
      L_ref_message := treat(I_MESSAGE AS "RIB_XStoreRef_REC");
      ---
      if L_ref_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
         raise PROGRAM_ERROR;
      end if;
 
      -- Validate Message Contents
      if RMSSUB_XSTORE_VALIDATE.CHECK_MESSAGE(O_error_message,
                                              L_store_rec,
                                              L_ref_message,
                                              L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- INSERT/UPDATE table
      if RMSSUB_XSTORE_SQL.PERSIST(O_error_message,
                                   L_store_rec,
                                   L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(L_message_type, 'NULL'));
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CAPITALIZE_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_message        IN OUT NOCOPY   "RIB_XStoreDesc_REC")

   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'RMSSUB_XSTORE.CAPITALIZE_FIELDS';

BEGIN

   IO_message.store_class    := UPPER(IO_message.store_class);
   IO_message.currency_code  := UPPER(IO_message.currency_code);


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CAPITALIZE_FIELDS;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE BODY
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2) IS

    L_program VARCHAR2(50) := 'RMSSUB_XSTORE.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;

-------------------------------------------------------------------------------------------------------
END RMSSUB_XSTORE;
/
