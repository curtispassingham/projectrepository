
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XITEMRCLS AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
--PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_cre_type           VARCHAR2(15) := 'xitemrclscre';
   LP_dtl_cre_type       VARCHAR2(15) := 'xitemrclsdtlcre';
   LP_del_type           VARCHAR2(15) := 'xitemrclsdel';
   LP_dtl_del_type       VARCHAR2(15) := 'xitemrclsdtldel';
----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code   IN OUT   VARCHAR2,
                  O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN       RIB_OBJECT,
                  I_message_type  IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_XITEMRCLS;
/
