
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE MERCHANT_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--Function Name: GET_MERCH_NAME
--Purpose:       This function will retrieve the merchant name of a given merchant.
--Calls:         None
--Created:       26-JUNE-2003, by Rochelle Paz
--------------------------------------------------------------------
FUNCTION GET_MERCH_NAME(O_error_message   IN OUT   VARCHAR2,
                        O_merch_name      IN OUT   MERCHANT.MERCH_NAME%TYPE,
                        I_merchant        IN       MERCHANT.MERCH%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------   
END MERCHANT_SQL;
/
