-- File Name : CORESVC_COMPHEAD_SPEC.pls
CREATE OR REPLACE PACKAGE CORESVC_COMPHEAD AUTHID CURRENT_USER AS

   template_category     CONSTANT VARCHAR2(255) :='RMSFND';
   template_key          CONSTANT VARCHAR2(255) :='COMPHEAD_DATA';
   action_new                     VARCHAR2(25)  :='NEW';
   action_mod                     VARCHAR2(25)  :='MOD';
   action_del                     VARCHAR2(25)  :='DEL';
   COMPHEAD_sheet                 VARCHAR2(255) :='COMPHEAD';
   COMPHEAD$Action                NUMBER        :=1;
   COMPHEAD$COMPANY               NUMBER        :=2;
   COMPHEAD$CO_NAME               NUMBER        :=3;
   COMPHEAD$CO_ADD1               NUMBER        :=4;
   COMPHEAD$CO_ADD2               NUMBER        :=5;
   COMPHEAD$CO_ADD3               NUMBER        :=6;
   COMPHEAD$CO_CITY               NUMBER        :=7;
   COMPHEAD$CO_STATE              NUMBER        :=8;
   COMPHEAD$CO_COUNTRY            NUMBER        :=9;
   COMPHEAD$CO_POST               NUMBER        :=10;
   COMPHEAD$CO_NAME_SECONDARY     NUMBER        :=11;
   COMPHEAD$CO_JURISDICTION_CODE  NUMBER        :=12;
   
   COMPHEAD_TL_sheet                 VARCHAR2(255) :='COMPHEAD_TL';
   COMPHEAD_TL$Action                NUMBER        :=1;
   COMPHEAD_TL$LANG                  NUMBER        :=2;
   COMPHEAD_TL$COMPANY               NUMBER        :=3;
   COMPHEAD_TL$CO_NAME               NUMBER        :=4;
   COMPHEAD_TL$CO_ADD1               NUMBER        :=5;
   COMPHEAD_TL$CO_ADD2               NUMBER        :=6;
   COMPHEAD_TL$CO_ADD3               NUMBER        :=7;
   COMPHEAD_TL$CO_CITY               NUMBER        :=8;
   COMPHEAD_TL$CO_NAME_SECONDARY     NUMBER        :=9;

   action_column                  VARCHAR2(255) :='ACTION';

   TYPE COMPHEAD_rec_tab is TABLE OF COMPHEAD%ROWTYPE;

   sheet_name_trans S9T_PKG.trans_map_typ;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN;
------------------------------------------------------------------------------- 
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count   IN OUT NUMBER,
                     I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count   IN OUT NUMBER,
                 I_process_id    IN     NUMBER,
                 I_chunk_id      IN     NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2;
 -------------------------------------------------------------------------------
END CORESVC_COMPHEAD;
/
