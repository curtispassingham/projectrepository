CREATE OR REPLACE PACKAGE BODY POS_CONFIG_SQL IS
----------------------------------------------------------------------------
FUNCTION CHECK_ITEM (O_error_message    IN OUT VARCHAR2,
                      I_item            IN     ITEM_MASTER.ITEM%TYPE,
                      I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                      I_class           IN     ITEM_MASTER.CLASS%TYPE,
                      I_subclass        IN     ITEM_MASTER.SUBCLASS%TYPE,
                      I_store           IN     STORE.STORE%TYPE,
                      I_action          IN     VARCHAR2)
 RETURN BOOLEAN IS


   L_program       VARCHAR2(50)                                  := 'POS_CONFIG_SQL.CHECK_ITEM';
   L_config_id     POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE         := NULL;
   L_config_type   POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE       := NULL;
   L_valid_store   VARCHAR2(1)                                   :='N';
   L_test_items    VARCHAR2(1)                                   := NULL;
   L_group         GROUPS.GROUP_NO%TYPE                          := NULL;
   L_dept          ITEM_MASTER.DEPT%TYPE                         := NULL;
   L_class         ITEM_MASTER.CLASS%TYPE                        := NULL;
   L_subclass      ITEM_MASTER.SUBCLASS%TYPE                     := NULL;
   L_exists        VARCHAR2(1)                                   := NULL;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(50);
   L_field         VARCHAR2(50);


   cursor C_ALL_CONFIGS is
      select distinct pos_config_type,
                      pos_config_id
                 from pos_merch_criteria;

   cursor C_GET_GROUP is
      select group_no
        from deps
       where dept = L_dept;

   cursor C_CHECK_DUPS is
      select 'x'
        from pos_config_items
       where item            = I_item
         and pos_config_id   = L_config_id
         and pos_config_type = L_config_type
         and (   store       = I_store
              or I_store       IS NULL);

   cursor C_CHECK_ITEM is
      select 'x'
        from pos_merch_criteria pm,
             pos_store ps
       where group_no = L_group
         and pm.pos_config_type = L_config_type
         and pm.pos_config_id   = L_config_id
         and pm.pos_config_type = ps.pos_config_type
         and pm.pos_config_id   = ps.pos_config_id
         and (dept IS NULL
              or (dept = L_dept
                  and class IS NULL)
              or (dept = L_dept
                  and class = L_class
                  and subclass IS NULL)
              or (dept = L_dept
                  and class = L_class
                  and subclass = L_subclass
                  and item IS NULL)
              or (item = I_item
                  and exclude_ind = 'N'))
          and (ps.store = I_store and I_store is not NULL
               or I_store is NULL)
          and not exists (select 'x'
                            from pos_merch_criteria
                           where item = I_item
                             and pos_config_id   = L_config_id
                             and pos_config_type = L_config_type
                             and exclude_ind     = 'Y');

   cursor C_LOCK_COUPON is
      select 'x'
        from pos_coupon_head
       where coupon_id = L_config_id
         for update nowait;

   cursor C_LOCK_PROD_REST is
      select 'x'
        from pos_prod_rest_head
       where pos_prod_rest_id = L_config_id
         for update nowait;

BEGIN

   -- hierarchy info may/may not be supplied in deletion case.
   if I_dept IS NULL then
      if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                        I_item,
                                        L_dept,
                                        L_class,
                                        L_subclass) = FALSE then
         RETURN FALSE;
      end if;
   else
      L_dept     := I_dept;
      L_class    := I_class;
      L_subclass := I_subclass;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_GROUP','DEPS','DEPT : ' || L_dept);
   open C_GET_GROUP;

   SQL_LIB.SET_MARK('FETCH','C_GET_GROUP','DEPS','DEPT : ' || L_dept);
   fetch C_GET_GROUP into L_group;

   SQL_LIB.SET_MARK('CLOSE','C_GET_GROUP','DEPS','DEPT : ' || L_dept);
   close C_GET_GROUP;

   FOR r_config_value IN C_ALL_CONFIGS LOOP
      L_config_type := r_config_value.pos_config_type;
      L_config_id   := r_config_value.pos_config_id;
      L_test_items  := NULL;
      L_exists      := NULL;
      ---
      --- Check to see if an item exists with the same config type, id and store
      SQL_LIB.SET_MARK('OPEN','C_CHECK_DUPS', 'POS_CONFIG_ITEMS', 'CONFIG_ID : ' || L_config_id);
      open C_CHECK_DUPS;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_DUPS','POS_CONFIG_ITEMS', 'CONFIG_ID : ' || L_config_id);
      fetch C_CHECK_DUPS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_DUPS','POS_CONFIG_ITEMS', 'CONFIG_ID : ' || L_config_id);
      close C_CHECK_DUPS;
      ---
      --- Verify that the item meets criteria in POS_MERCH_CRITERIA
      SQL_LIB.SET_MARK('OPEN','C_CHECK_ITEM','POS_MERCH_CRITERIA','CONFIG_ID : ' || L_config_id);
      open c_check_item;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_ITEM','POS_MERCH_CRITERIA','CONFIG_ID : ' || L_config_id);
      fetch c_check_item into L_test_items;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_ITEM','POS_MERCH_CRITERIA','CONFIG_ID : ' || L_config_id);
      close c_check_item;
      ---
      if L_test_items IS NOT NULL then
         if I_store IS NULL then
            SQL_LIB.SET_MARK('INSERT',NULL,'POS_CONFIG_ITEMS','POS_CONFIG_ID : ' || L_config_id);

            --- Insert all items corresponding with current information
            insert into pos_config_items (pos_config_type,
                                          pos_config_id,
                                          item,
                                          status,
                                          store)
                                  select  L_config_type,
                                          L_config_id,
                                          I_item,
                                          I_action,
                                          ps.store
                                    from  pos_store ps
                                    where pos_config_type = L_config_type
                                      and pos_config_id   = L_config_id;

            --- Delete items with an opposite status (eliminate duplicates)
            SQL_LIB.SET_MARK('DELETE', NULL, 'POS_CONFIG_ITEMS', 'POS_CONFIG_ID : ' || L_config_id);
            delete from pos_config_items
             where status <> I_action
               and pos_config_type = L_config_type
               and pos_config_id   = L_config_id
               and item            = I_item;
         else
            if L_exists IS NULL then
               SQL_LIB.SET_MARK('INSERT',NULL,'POS_CONFIG_ITEMS','POS_CONFIG_ID : ' || L_config_id);
               insert into pos_config_items (pos_config_type,
                                             pos_config_id,
                                             item,
                                             status,
                                             store)
                                     values (L_config_type,
                                             L_config_id,
                                             I_item,
                                             I_action,
                                             I_store);
            else
               SQL_LIB.SET_MARK('UPDATE',NULL,'POS_CONFIG_ITEMS', 'POS_CONFIG_ID : ' || L_config_id);
               update pos_config_items
                  set status = I_action
                where pos_config_type = L_config_type
                  and pos_config_id   = L_config_id
                  and store           = I_store
                  and item            = I_item;
            end if;
         end if;
         ---
         if L_config_type = 'COUP' then
            L_table  := 'POS_COUPON_HEAD';
            L_field  := to_char(L_config_id);
            ---
            SQL_LIB.SET_MARK('OPEN','C_LOCK_COUPON','POS_COUPON_HEAD','COUPON_ID : ' || L_config_id);
            open C_LOCK_COUPON;
            SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUPON','POS_COUPON_HEAD','COUPON_ID : ' || L_config_id);
            close C_LOCK_COUPON;
            ---
            SQL_LIB.SET_MARK('UPDATE',NULL,'POS_COUPON_HEAD','COUPON_ID : ' || L_config_id);
            update pos_coupon_head
               set extract_req_ind = 'Y'
             where coupon_id = L_config_id;

         elsif L_config_type = 'PRES' then
            L_table  := 'POS_PROD_REST_HEAD';
            L_field  := to_char(L_config_id);
            ---
            SQL_LIB.SET_MARK('OPEN','C_LOCK_PROD_REST','POS_PROD_REST_HEAD','PROD_REST_ID : ' || L_config_id);
            open C_LOCK_PROD_REST;
            SQL_LIB.SET_MARK('CLOSE','C_LOCK_PROD_REST','POS_PROD_REST_HEAD','PROD_REST_ID : ' || L_config_id);
            close C_LOCK_PROD_REST;
            ---
            SQL_LIB.SET_MARK('UPDATE',NULL,'POS_PROD_REST_HEAD', 'POS_PROD_REST_ID: '|| L_config_id);
            update pos_prod_rest_head
               set extract_req_ind = 'Y'
             where pos_prod_rest_id = L_config_id;
         end if;
      end if;
   END LOOP;
   ---
   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG ('DELRECS_REC_LOC',
                                              L_table,
                                              L_field,
                                              NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));

   return FALSE;
END CHECK_ITEM;
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM (O_error_message   IN OUT VARCHAR2,
                        I_group           IN     DEPS.GROUP_NO%TYPE,
                        I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                        I_class           IN     ITEM_MASTER.CLASS%TYPE,
                        I_subclass        IN     ITEM_MASTER.SUBCLASS%TYPE,
                        I_item            IN     ITEM_MASTER.ITEM%TYPE,
                        O_exists          IN OUT VARCHAR2)
 RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'POS_CONFIG_SQL.VALIDATE_ITEM';

   cursor C_CHECK_EXISTS is
      select 'Y'
        from item_master im,
             deps d
       where d.group_no = NVL(I_group, group_no)
         and d.dept = im.dept
         and im.status = 'A'
         and im.dept = nvl(I_dept, im.dept)
         and im.class = nvl(I_class, im.class)
         and im.subclass = nvl(I_subclass, im.subclass)
         and im.item = nvl(I_item, im.item)
         and rownum = 1;

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_check_exists','item_master/deps',NULL);
   open C_CHECK_EXISTS;

   SQL_LIB.SET_MARK('FETCH','C_check_exists','item_master/deps',NULL);
   fetch C_CHECK_EXISTS into O_exists;

   if C_CHECK_EXISTS%NOTFOUND then
      O_exists := 'N';
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_check_exists','item_master/deps',NULL);
   close C_CHECK_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              NULL);

   return FALSE;
END VALIDATE_ITEM;
-------------------------------------------------------------------------------------
FUNCTION TOTAL_ITEMS (O_error_message     IN OUT VARCHAR2,
                      I_pos_config_id     IN     POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                      I_pos_config_type   IN     POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE,
                      O_total_items       IN OUT NUMBER)
 RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'POS_CONFIG_SQL.TOTAL_ITEMS';

   cursor C_TOTAL_ITEMS is
      select count(distinct im.item)
        from item_master im,
             deps d,
             pos_merch_criteria pm
       where pm.pos_config_id = I_pos_config_id
         and pm.pos_config_type = I_pos_config_type
         and nvl(pm.status, '*') != 'D'
         and im.status = 'A'
         and im.sellable_ind = 'Y'
         and im.item_level = im.tran_level
         and (d.group_no = pm.group_no
              and d.dept = im.dept
              and ((pm.dept is NULL)
               or (im.dept = pm.dept
                   and pm.class is NULL)
               or (pm.dept = im.dept
                   and pm.class = im.class
                   and pm.subclass is NULL)
               or (pm.dept = im.dept
                   and pm.class = im.class
                   and pm.subclass = im.subclass
                   and pm.item is NULL)
               or (pm.dept = im.dept
                   and pm.class = im.class
                   and pm.subclass = im.subclass
                   and pm.item = im.item
                   and pm.exclude_ind = 'N'))
         and not exists (select 'x'
                           from pos_merch_criteria pmc
                          where pmc.pos_config_id = I_pos_config_id
                            and pmc.pos_config_type = I_pos_config_type
                            and nvl(pmc.status, '*') != 'D'
                            and item = im.item
                            and pmc.exclude_ind = 'Y'));

BEGIN

   ---
   SQL_LIB.SET_MARK('OPEN','C_total_items','item_master/deps/pos_merch_criteria',NULL);
   open C_TOTAL_ITEMS;

   SQL_LIB.SET_MARK('FETCH','C_total_items','item_master/deps/pos_merch_criteria',NULL);
   fetch C_TOTAL_ITEMS into O_total_items;

   SQL_LIB.SET_MARK('CLOSE','C_total_items','item_master/deps/pos_merch_criteria',NULL);
   close C_TOTAL_ITEMS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              NULL);

   return FALSE;
END TOTAL_ITEMS;

-------------------------------------------------------------------------------------
FUNCTION POSMCRIT_FILTER_LIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_diff              IN OUT   VARCHAR2,
                              I_total_items       IN       NUMBER,
                              I_pos_config_id     IN       POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                              I_pos_config_type   IN       POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'POS_CONFIG_SQL.POSMCRIT_FILTER_LIST';
   L_null_parameter_name   VARCHAR2(30);
   L_posmcrit              NUMBER(6);
   L_vposmcrit             NUMBER(6);
   NULL_PARAMETER          EXCEPTION;
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_COUNT_V_POSMCRIT IS
      select count(distinct im.item)
        from v_item_master im,
             deps d,
             v_pos_merch_criteria vpm
       where vpm.pos_config_id = I_pos_config_id
         and vpm.pos_config_type = I_pos_config_type
         and vpm.status != 'D'
         and im.status = 'A'
         and im.sellable_ind = 'Y'
         and (d.group_no = vpm.group_no
              and d.dept = im.dept
              and ((vpm.dept is NULL)
               or (im.dept = vpm.dept
                   and vpm.class is NULL)
               or (vpm.dept = im.dept
                   and vpm.class = im.class
                   and vpm.subclass is NULL)
               or (vpm.dept = im.dept
                   and vpm.class = im.class
                   and vpm.subclass = im.subclass
                   and vpm.item is NULL)
               or (vpm.dept = im.dept
                   and vpm.class = im.class
                   and vpm.subclass = im.subclass
                   and vpm.item = im.item
                   and vpm.exclude_ind = 'N'))
         and not exists (select 'x'
                           from v_pos_merch_criteria vpmc
                          where vpmc.pos_config_id = I_pos_config_id
                            and vpmc.pos_config_type = I_pos_config_type
                            and vpmc.status != 'D'
                            and item = im.item
                            and vpmc.exclude_ind = 'Y'));

BEGIN

   if I_total_items is NULL then
      L_null_parameter_name := 'total_items';
      raise NULL_PARAMETER;
   end if;

   if I_pos_config_id is NULL then
      L_null_parameter_name := 'pos_config_id';
      raise NULL_PARAMETER;
   end if;

   if I_pos_config_type is NULL then
      L_null_parameter_name := 'pos_config_type';
      raise NULL_PARAMETER;
   end if;

   --

   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT_V_POSMCRIT',
                    'V_POS_MERCH_CRITERIA',
                    'POS_CONFIG_ID: '|| TO_CHAR(I_pos_config_id)||
                    'POS_CONFIG_TYPE: '|| I_pos_config_type);
   open C_COUNT_V_POSMCRIT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT_V_POSMCRIT',
                    'V_POS_MERCH_CRITERIA',
                    'POS_CONFIG_ID: '|| TO_CHAR(I_pos_config_id)||
                    'POS_CONFIG_TYPE: '|| I_pos_config_type);
   fetch C_COUNT_V_POSMCRIT into L_vposmcrit;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT_V_POSMCRIT',
                    'V_POS_MERCH_CRITERIA',
                    'POS_CONFIG_ID: '|| TO_CHAR(I_pos_config_id)||
                    'POS_CONFIG_TYPE: '|| I_pos_config_type);
   close C_COUNT_V_POSMCRIT;

   if NVL(I_total_items,0) != NVL(L_vposmcrit,0) then
     O_diff := 'Y';
   else
     O_diff := 'N';
   end if;

   return TRUE;

EXCEPTION

   when NULL_PARAMETER then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_null_parameter_name,
                                            L_program,
                                            NULL);
      return FALSE;

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'POS_MERCH_CRITERIA',
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POSMCRIT_FILTER_LIST;

-------------------------------------------------------------------------------------
FUNCTION POS_STORE_FILTER_LIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_diff              IN OUT   VARCHAR2,
                               I_count_pos_store   IN       NUMBER,
                               I_pos_config_type   IN       POS_STORE.POS_CONFIG_TYPE%TYPE,
                               I_pos_config_id     IN       POS_STORE.POS_CONFIG_ID%TYPE)
   RETURN BOOLEAN IS

    cursor C_COUNT_V_POS_STORE is
      select count(store)
        from v_pos_store
       where pos_config_type = I_pos_config_type
             and pos_config_id = I_pos_config_id
             and (status is NULL or status != 'D');

   L_program       VARCHAR2(64)   := 'POS_CONFIG_SQL.POS_STORE_FILTER_LIST';
   L_v_pos_store   NUMBER(6)      := -987436;
BEGIN
   if I_count_pos_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_count_pos_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_pos_config_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_pos_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT_V_POS_STORE',
                    'V_pos_store',
                    NULL);
   open C_COUNT_V_POS_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT_V_POS_STORE',
                    'V_pos_store',
                    NULL);
   fetch C_COUNT_V_POS_STORE into L_v_pos_store;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT_V_POS_STORE',
                    'V_pos_store',
                    NULL);
   close C_COUNT_V_POS_STORE;

   if nvl(L_v_pos_store,0) != nvl(I_count_pos_store,0) then
      O_diff := 'Y';
   else
      O_diff := 'N';
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_VALIDATE_SQL.ORDLOC_FILTER_LIST',
                                            to_char(SQLCODE));
   return FALSE;
END POS_STORE_FILTER_LIST;

-------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_CRITERION (O_error_message   IN OUT VARCHAR2,
                              I_pos_config_id   IN     POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                              I_pos_config_type IN     POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE,
                              I_group           IN     DEPS.GROUP_NO%TYPE,
                              I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                              I_class           IN     ITEM_MASTER.CLASS%TYPE,
                              I_subclass        IN     ITEM_MASTER.SUBCLASS%TYPE,
                              I_item            IN     ITEM_MASTER.ITEM%TYPE,
                              I_exclude_ind     IN     POS_MERCH_CRITERIA.EXCLUDE_IND%TYPE,
                              O_exists          IN OUT VARCHAR2)
 RETURN BOOLEAN IS


   L_program       VARCHAR2(50) := 'POS_CONFIG_SQL.CHECK_DUP_CRITERION';

   cursor C_CHECK_EXISTS is
      select 'Y'
        from pos_merch_criteria
       where pos_config_id     = I_pos_config_id
         and pos_config_type   = I_pos_config_type
         and group_no          = I_group
         and nvl(dept, -1)     = nvl(I_dept, -1)
         and nvl(class, -1)    = nvl(I_class, -1)
         and nvl(subclass, -1) = nvl(I_subclass, -1)
         and nvl(item, 'x')    = nvl(I_item, 'x')
         and exclude_ind       = I_exclude_ind
         and status           != 'D'
         and rownum = 1;

BEGIN

   if I_pos_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_pos_config_id',I_pos_config_id,L_program);
      return FALSE;
   end if;
   ---
   if I_pos_config_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_pos_config_type',I_pos_config_type,L_program);
      return FALSE;
   end if;
   ---
   if I_exclude_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_exclude_ind',I_exclude_ind,L_program);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','POS_MERCH_CRITERIA',NULL);
   open C_CHECK_EXISTS;

   SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','POS_MERCH_CRITERIA',NULL);
   fetch C_CHECK_EXISTS into O_exists;

   if C_CHECK_EXISTS%notfound then
      O_exists := 'N';
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','POS_MERCH_CRITERIA',NULL);
   close C_CHECK_EXISTS;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              NULL);

   return FALSE;
END CHECK_DUP_CRITERION;

-------------------------------------------------------------------------------------
FUNCTION TOTAL_STORES (O_error_message   IN OUT VARCHAR2,
                       O_store_count     IN OUT NUMBER,
                       I_pos_config_id   IN     POS_STORE.POS_CONFIG_ID%TYPE,
                       I_pos_config_type IN     POS_STORE.POS_CONFIG_TYPE%TYPE)
RETURN BOOLEAN IS


   L_program       VARCHAR2(50) := 'POS_CONFIG_SQL.TOTAL_STORES';

   cursor C_COUNT_STORES is
      select COUNT(store)
        from pos_store
       where pos_config_id = I_pos_config_id
         and pos_config_type = I_pos_config_type
         and status is NOT NULL;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT_STORES',
                    'POS_STORE',
                    'POS_CONFIG_ID : '|| I_pos_config_id);
   open C_COUNT_STORES;
   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT_STORES',
                    'POS_STORE',
                    'POS_CONFIG_ID : '|| I_pos_config_id);
   fetch C_COUNT_STORES into O_store_count;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT_STORES',
                    'POS_STORE',
                    'POS_CONFIG_ID : '|| I_pos_config_id);
   close C_COUNT_STORES;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              NULL);

   return FALSE;

END TOTAL_STORES;

-------------------------------------------------------------------------------------
FUNCTION DELETE_MERCH_CRITERIA(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_pos_config_type     IN      POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE,
                               I_pos_config_id       IN      POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                               I_seq_no              IN      POS_MERCH_CRITERIA.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)  := 'POS_CONFIG_SQL.DELETE_MERCH_CRITERIA';
   L_table              VARCHAR2(20)  := 'POS_MERCH_CRITERIA';
   L_rowid              ROWID;   
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   L_extracted          VARCHAR2(1);

   cursor C_LOCK_POS_MERCH_CRITERIA is
      select 'x'
        from pos_merch_criteria pmc
       where pmc.pos_config_type = I_pos_config_type
         and pmc.pos_config_id = I_pos_config_id
         and pmc.seq_no = NVL(I_seq_no, pmc.seq_no)
         for update nowait;

   cursor C_CHECK_EXTRACTED is
      select extracted_ind
        from pos_prod_rest_head
       where pos_prod_rest_id = I_pos_config_id;

BEGIN

   --- Check required input parameters
   if I_pos_config_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_pos_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open  C_CHECK_EXTRACTED;
   fetch C_CHECK_EXTRACTED INTO L_extracted;
   close C_CHECK_EXTRACTED;
   ---
   if L_extracted = 'Y' then
      open C_LOCK_POS_MERCH_CRITERIA;
      close C_LOCK_POS_MERCH_CRITERIA;
      ---
      update pos_merch_criteria pmc
         set pmc.status = 'D'
       where pmc.pos_config_type = I_pos_config_type
         and pmc.pos_config_id = I_pos_config_id
         and pmc.seq_no = NVL(I_seq_no, pmc.seq_no);
   else
      delete pos_merch_criteria pmc
       where pmc.pos_config_type = I_pos_config_type
         and pmc.pos_config_id = I_pos_config_id
         and pmc.seq_no = NVL(I_seq_no, pmc.seq_no);
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_pos_config_id,
                                            I_seq_no);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_MERCH_CRITERIA;
-----------------------------------------------------------------------------

FUNCTION DELETE_ITEM (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_pos_id                  IN       POS_COUPON_HEAD.COUPON_ID%TYPE,
                      I_pos_config_type         IN       POS_STORE.POS_CONFIG_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(61)    := 'POS_CONFIG_SQL.DELETE_ITEM';
   L_table            VARCHAR2(20);

BEGIN
   IF I_pos_config_type IN ('COUP', 'PRES') THEN
     L_table := 'POS_CONFIG_ITEMS';
      DELETE
        FROM POS_CONFIG_ITEMS
       WHERE POS_CONFIG_ID = I_pos_id
         AND POS_CONFIG_TYPE = I_pos_config_type;
   END IF;

   L_table := 'POS_STORE';
   DELETE
     FROM POS_STORE
    WHERE POS_CONFIG_ID = I_pos_id
      AND POS_CONFIG_TYPE = I_pos_config_type;

   IF I_pos_config_type IN ('COUP', 'PRES') THEN
     L_table := 'POS_MERCH_CRITERIA';
      DELETE
        FROM POS_MERCH_CRITERIA
       WHERE POS_CONFIG_ID = I_pos_id
         AND POS_CONFIG_TYPE = I_pos_config_type;
   END IF;

   IF I_pos_config_type = 'COUP' THEN
     L_table := 'POS_COUPON_HEAD';
      DELETE
        FROM POS_COUPON_HEAD_TL
       WHERE COUPON_ID = I_pos_id;
   END IF;

   IF I_pos_config_type = 'PRES' THEN
     L_table := 'POS_DAY_TIME_DATE';
      DELETE
        FROM POS_DAY_TIME_DATE
       WHERE POS_PROD_REST_ID = I_pos_id;

     L_table := 'POS_PROD_REST_HEAD';
      DELETE
        FROM POS_PROD_REST_HEAD
       WHERE POS_PROD_REST_ID = I_pos_id;
   END IF;

   IF I_pos_config_type = 'TTYP' THEN
     L_table := 'POS_TENDER_TYPE_HEAD';
      DELETE
        FROM POS_TENDER_TYPE_HEAD
       WHERE TENDER_TYPE_ID = I_pos_id;
   END IF;

   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_ITEM;
-----------------------------------------------------------------------------

FUNCTION DELETE_MERCH_CRITERIA_NEW(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_pos_config_type     IN      POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE,
                                   I_pos_config_id       IN      POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                                   I_seq_no              IN      POS_MERCH_CRITERIA.SEQ_NO%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61)  := 'POS_CONFIG_SQL.DELETE_MERCH_CRITERIA_NEW';
   L_table              VARCHAR2(20)  := 'POS_MERCH_CRITERIA';
   L_rowid              ROWID;   
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

BEGIN

   --- Check required input parameters
   if I_pos_config_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_pos_config_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pos_config_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   
   delete from pos_merch_criteria pmc
    where pos_config_type = I_pos_config_type
      and pos_config_id = I_pos_config_id
      and seq_no = NVL(I_seq_no, pmc.seq_no);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_pos_config_id,
                                            I_seq_no);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_MERCH_CRITERIA_NEW;
-----------------------------------------------------------------------------------------------

FUNCTION UPDATE_MERCH_CRITERIA(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item     IN ITEM_MASTER.ITEM%TYPE,
                               I_dept     IN ITEM_MASTER.DEPT%TYPE,
                               I_class    IN ITEM_MASTER.CLASS%TYPE,
                               I_subclass IN ITEM_MASTER.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)             := 'POS_CONFIG_SQL.UPDATE_MERCH_CRITERIA';
   L_table         VARCHAR2(20)             := 'POS_MERCH_CRITERIA';
   L_group         GROUPS.GROUP_NO%TYPE     := NULL;

   
   L_rowid              ROWID;
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

  cursor C_GET_GROUP is
     select group_no
       from deps
      where dept = I_dept;

   cursor C_LOCK_POS_MERCH_CRITERIA is
      select 'x'
        from pos_merch_criteria 
       where item = I_item
         for update nowait;
BEGIN

  if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            'POS_CONFIG_SQL.UPDATE_MERCH_CRITERIA',
                                            'NULL');
      return FALSE;
   end if;
   
  if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            'POS_CONFIG_SQL.UPDATE_MERCH_CRITERIA',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            'POS_CONFIG_SQL.UPDATE_MERCH_CRITERIA',
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_subclass',
                                            'POS_CONFIG_SQL.UPDATE_MERCH_CRITERIA',
                                            'NULL');
      return FALSE;
   end if;
   ---
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_GROUP','DEPS','DEPT : ' || I_dept);
   open C_GET_GROUP;

   SQL_LIB.SET_MARK('FETCH','C_GET_GROUP','DEPS','DEPT : ' || I_dept);
   fetch C_GET_GROUP into L_group;

   SQL_LIB.SET_MARK('CLOSE','C_GET_GROUP','DEPS','DEPT : ' || I_dept);
   close C_GET_GROUP;
   ---
   if L_group is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'L_group',
                                            'POS_CONFIG_SQL.UPDATE_MERCH_CRITERIA',
                                            'NULL');
      return FALSE;
   end if;
   ---
   open C_LOCK_POS_MERCH_CRITERIA;
   close C_LOCK_POS_MERCH_CRITERIA;
   SQL_LIB.SET_MARK('UPDATE',NULL,'POS_MERCH_CRITERIA','I_item : ' || I_item);
   
   update pos_merch_criteria
      set group_no = L_group,
          dept     = I_dept,
          class    = I_class,
          subclass = I_subclass
     where item    = I_item;

return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_item,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_MERCH_CRITERIA;
-----------------------------------------------------------------------------
FUNCTION DELETE_POS_COUPON_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_coupon_id           IN       POS_COUPON_HEAD_TL.COUPON_ID%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'POS_CONFIG_SQL.DELETE_POS_COUPON_HEAD_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_POS_COUPON_HEAD_TL is
      select 'x'
        from pos_coupon_head_tl
       where coupon_id = I_coupon_id
         for update nowait;
BEGIN

   if I_coupon_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_coupon_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'POS_COUPON_HEAD_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_POS_COUPON_HEAD_TL',
                    L_table,
                    'I_coupon_id '||I_coupon_id);
   open C_LOCK_POS_COUPON_HEAD_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_POS_COUPON_HEAD_TL',
                    L_table,
                    'I_coupon_id '||I_coupon_id);
   close C_LOCK_POS_COUPON_HEAD_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'I_coupon_id '||I_coupon_id);
   delete from pos_coupon_head_tl
    where coupon_id = I_coupon_id;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_POS_COUPON_HEAD_TL;
----------------------------------------------------------------------------------------------------------
END POS_CONFIG_SQL;
/