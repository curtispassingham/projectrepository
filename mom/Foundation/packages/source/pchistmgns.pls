CREATE OR REPLACE PACKAGE PCHISTMGN_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------
--Function Name: GET_INFO
--Purpose      : This function will be responsible for taking search criteria
--               provided by the price history and margin form, getting all
--               the information and populating it in gtt_price_hist_mgn table
-------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message          IN OUT VARCHAR2,
                  I_from_date              IN     PRICE_HIST.ACTION_DATE%TYPE,
                  I_to_date                IN     PRICE_HIST.ACTION_DATE%TYPE,
                  I_grp_type               IN     VARCHAR2,
                  I_loc_type               IN     PRICE_HIST.LOC_TYPE%TYPE,
                  I_loc                    IN     PRICE_HIST.LOC%TYPE,
                  I_tran_type              IN     CODE_DETAIL.CODE%TYPE,
                  I_item_type              IN     VARCHAR2,
                  I_item                   IN     PRICE_HIST.ITEM%TYPE,
                  I_item_desc              IN     ITEM_MASTER.ITEM_DESC%TYPE,
                  I_prim_lang              IN     LANG.LANG%TYPE,
                  I_user_lang              IN     LANG.LANG%TYPE,
                  I_prim_currency          IN     CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM(O_error_message          IN OUT VARCHAR2,
                       O_exists                 IN OUT BOOLEAN,
                       I_item                   IN     PRICE_HIST.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message          IN OUT VARCHAR2,
                           O_exists                 IN OUT BOOLEAN,
                           I_loc                    IN     PRICE_HIST.LOC%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
FUNCTION CALC_MARGIN(I_item            IN   ITEM_MASTER.ITEM%TYPE,
                     I_dept            IN   ITEM_MASTER.DEPT%TYPE,
                     I_class           IN   ITEM_MASTER.CLASS%TYPE,
                     I_subclass        IN   ITEM_MASTER.SUBCLASS%TYPE,
                     I_pack_ind        IN   ITEM_MASTER.PACK_IND%TYPE,                       
                     I_class_vat_ind   IN   CLASS.CLASS_VAT_IND%TYPE,
                     I_location        IN   ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                     I_loc_type        IN   FUTURE_COST.LOC_TYPE%TYPE,
                     I_action_date     IN   PRICE_HIST.ACTION_DATE%TYPE,
                     I_pricing_cost    IN   FUTURE_COST.PRICING_COST%TYPE,
                     I_unit_retail     IN   PRICE_HIST.UNIT_RETAIL%TYPE)
   RETURN NUMBER;
---------------------------------------------------------------------------------
END PCHISTMGN_SQL;
/