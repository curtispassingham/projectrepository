CREATE OR REPLACE PACKAGE CORESVC_BANNER_CHANNELS AUTHID CURRENT_USER AS
--------------------------------------------------------------------
   template_key         CONSTANT VARCHAR2(255) := 'BANNER_CHANNELS_DATA';
   template_category    CONSTANT VARCHAR2(255) := 'RMSFND';
   action_new           VARCHAR2(25)           := 'NEW';
   action_mod           VARCHAR2(25)           := 'MOD';
   action_del           VARCHAR2(25)           := 'DEL';
   BANNER_sheet         VARCHAR2(255)          := 'BANNER';
   BANNER$Action        NUMBER                 := 1;
   BANNER$BANNER_NAME   NUMBER                 := 3;
   BANNER$BANNER_ID     NUMBER                 := 2;

   BANNER_TL_sheet         VARCHAR2(255)          := 'BANNER_TL';
   BANNER_TL$Action        NUMBER                 := 1;
   BANNER_TL$LANG          NUMBER                 := 2;
   BANNER_TL$BANNER_ID     NUMBER                 := 3;
   BANNER_TL$BANNER_NAME   NUMBER                 := 4;

   CHANNELS_sheet         VARCHAR2(255)         := 'CHANNELS';
   CHANNELS$Action        NUMBER                :=1;
   CHANNELS$BANNER_ID     NUMBER                :=5;
   CHANNELS$CHANNEL_TYPE  NUMBER                :=4;
   CHANNELS$CHANNEL_NAME  NUMBER                :=3;
   CHANNELS$CHANNEL_ID    NUMBER                :=2;
   
   CHANNELS_TL_sheet         VARCHAR2(255)         := 'CHANNELS_TL';
   CHANNELS_TL$Action        NUMBER                :=1;
   CHANNELS_TL$LANG          NUMBER                :=2;
   CHANNELS_TL$CHANNEL_ID    NUMBER                :=3;
   CHANNELS_TL$CHANNEL_NAME  NUMBER                :=4;

   sheet_name_trans   S9T_PKG.TRANS_MAP_TYP;
   action_column      VARCHAR2(255) := 'ACTION';
--------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message       IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT    S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN        CHAR   DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------   
END CORESVC_BANNER_CHANNELS;
/