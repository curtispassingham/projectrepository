
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DATES_SQL AUTHID CURRENT_USER AS
   FUNCTION GET_SYSAVAIL(O_error_message IN OUT VARCHAR2,
                         O_sysavail      IN OUT period.sysavail%TYPE)
      RETURN BOOLEAN;

   /* this function is overloaded. this version will not include any */
   /* passed in parameters to allow it to be called within sql statements */
   /* the other version has the approved method of error handling and */
   /* it is preferred.  this version will be phased out. */
   FUNCTION GET_VDATE 
      RETURN DATE;
   PRAGMA RESTRICT_REFERENCES(GET_VDATE,WNDS);

   /* overloaded function. see comments above. */
   FUNCTION GET_VDATE(O_error_message IN OUT VARCHAR2,
                      O_vdate         IN OUT period.vdate%TYPE)
      RETURN BOOLEAN;

   FUNCTION GET_START_454_HALF(O_error_message  IN OUT VARCHAR2,
                               O_start_454_half IN OUT period.start_454_half%TYPE)
      RETURN BOOLEAN;

   FUNCTION GET_END_454_HALF(O_error_message  IN OUT VARCHAR2,
                             O_end_454_half   IN OUT period.end_454_half%TYPE)
      RETURN BOOLEAN;

   FUNCTION GET_START_454_MONTH(O_error_message   IN OUT VARCHAR2,
                                O_start_454_month IN OUT period.start_454_month%TYPE)
      RETURN BOOLEAN;

   FUNCTION GET_MID_454_MONTH(O_error_message  IN OUT VARCHAR2,
                              O_mid_454_month  IN OUT period.mid_454_month%TYPE)
      RETURN BOOLEAN;

   FUNCTION GET_END_454_MONTH(O_error_message  IN OUT VARCHAR2,
                              O_end_454_month  IN OUT period.end_454_month%TYPE)
      RETURN BOOLEAN;

   FUNCTION GET_HALF_NO(O_error_message IN OUT VARCHAR2,
                        O_half_no       IN OUT period.half_no%TYPE)
      RETURN BOOLEAN;
   FUNCTION GET_NEXT_HALF_NO(O_error_message IN OUT VARCHAR2,
                             O_next_half_no  IN OUT period.next_half_no%TYPE)
      RETURN BOOLEAN;

   /* overloaded function...if no input date is passed in, then the function */
   /* will return the 454 day of the current vdate */
   FUNCTION GET_454_DAY(O_error_message IN OUT VARCHAR2,
                             O_curr_454_day  IN OUT period.curr_454_day%TYPE)
      RETURN BOOLEAN;

   /* overloaded function...if the action date is included as an input parameter */
   /* then the function will return the 454 day for the date that is passed in */
   FUNCTION GET_454_DAY(O_error_message  IN OUT VARCHAR2,
                        O_454_day        IN OUT period.curr_454_day%TYPE,
                        I_action_date    IN     date)
      RETURN BOOLEAN;
   FUNCTION GET_454_WEEK(O_error_message IN OUT VARCHAR2,
                         O_curr_454_week IN OUT period.curr_454_week%TYPE)
      RETURN BOOLEAN;

   FUNCTION GET_454_MONTH(O_error_message  IN OUT VARCHAR2,
                          O_curr_454_month IN OUT period.curr_454_month%TYPE)
      RETURN BOOLEAN;
   FUNCTION GET_454_YEAR(O_error_message IN OUT VARCHAR2,
                         O_curr_454_year IN OUT period.curr_454_year%TYPE)
      RETURN BOOLEAN;
   FUNCTION GET_454_MONTH_IN_HALF(O_error_message          IN OUT VARCHAR2,
                                  O_curr_454_month_in_half IN OUT period.curr_454_month_in_half%TYPE)
      RETURN BOOLEAN;
   FUNCTION GET_454_WEEK_IN_HALF(O_error_message         IN OUT VARCHAR2,
                                 O_curr_454_week_in_half IN OUT period.curr_454_week_in_half%TYPE)
      RETURN BOOLEAN;
   /* overloaded function....will return the eow date for the current vdate */
   /* if no input date is passed as a parameter */
   FUNCTION GET_EOW_DATE(O_error_message IN OUT VARCHAR2,
                         O_eow_date      IN OUT date)
      RETURN BOOLEAN;

   /* overloaded function....if an input date paraemter is passed */
   /* function will return the valid end of week date */
   /* for the action date input parameter */
   FUNCTION GET_EOW_DATE(O_error_message IN OUT VARCHAR2,
                         O_eow_date      IN OUT date,
                         I_action_date   IN date)
      RETURN BOOLEAN;

   FUNCTION RESET_GLOBALS(O_error_message IN OUT VARCHAR2)
      RETURN BOOLEAN;
END DATES_SQL;
/


