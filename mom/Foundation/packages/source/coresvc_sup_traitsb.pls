CREATE OR REPLACE PACKAGE BODY CORESVC_SUP_TRAITS AS
   cursor C_SVC_SUP_TRAITS(I_process_id   NUMBER,
                           I_chunk_id     NUMBER) is
      select pk_sup_traits.rowid  as pk_sup_traits_rid,
             st.rowid             as st_rid,
             st.master_sup,
             st.master_sup_ind,
             st.description,
             st.sup_trait,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)     as action,
             st.process$status
        from svc_sup_traits st,
             sup_traits     pk_sup_traits
       where st.process_id       = I_process_id
         and st.chunk_id         = I_chunk_id
         and st.sup_trait        = pk_sup_traits.sup_trait (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type SUP_TRAITS_TL_TAB IS TABLE OF SUP_TRAITS_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if SHEET_NAME_TRANS.EXISTS(I_sheet_name) then
      return SHEET_NAME_TRANS(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS

BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;
END WRITE_S9T_ERROR;

------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets             S9T_PKG.NAMES_MAP_TYP;
   sup_traits_cols      S9T_PKG.NAMES_MAP_TYP;
   sup_traits_tl_cols   S9T_PKG.NAMES_MAP_TYP;

BEGIN
   L_sheets                     := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   sup_traits_cols              := S9T_PKG.GET_COL_NAMES(I_file_id,sup_traits_sheet);
   sup_traits$action            := sup_traits_cols('ACTION');
   sup_traits$sup_trait         := sup_traits_cols('SUP_TRAIT');
   sup_traits$description       := sup_traits_cols('DESCRIPTION');
   sup_traits$master_sup_ind    := sup_traits_cols('MASTER_SUP_IND');
   sup_traits$master_sup        := sup_traits_cols('MASTER_SUP');

   sup_traits_tl_cols              := S9T_PKG.GET_COL_NAMES(I_file_id,sup_traits_tl_sheet);
   sup_traits_tl$action            := sup_traits_tl_cols('ACTION');
   sup_traits_tl$lang              := sup_traits_tl_cols('LANG');
   sup_traits_tl$sup_trait         := sup_traits_tl_cols('SUP_TRAIT');
   sup_traits_tl$description       := sup_traits_tl_cols('DESCRIPTION');
END POPULATE_NAMES;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SUP_TRAITS(I_file_id   IN   NUMBER) IS

BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = sup_traits_sheet)
               select s9t_row(s9t_cells(CORESVC_SUP_TRAITS.action_mod ,
                                        sup_trait,
                                        description,
                                        master_sup_ind,
                                        master_sup
                                       ))
                 from sup_traits;
END POPULATE_SUP_TRAITS;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SUP_TRAITS_TL(I_file_id   IN   NUMBER) IS

BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = SUP_TRAITS_TL_SHEET)
               select s9t_row(s9t_cells(CORESVC_SUP_TRAITS.action_mod ,
                                        lang,
                                        sup_trait,
                                        description))
                 from sup_traits_tl;
END POPULATE_SUP_TRAITS_TL;
---------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file       S9T_FILE;
   L_file_name  S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang := GET_USER_LANG;

   L_file.add_sheet(sup_traits_sheet);
   L_file.sheets(l_file.get_sheet_index(sup_traits_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'SUP_TRAIT',
                                                                                        'DESCRIPTION',
                                                                                        'MASTER_SUP_IND',
                                                                                        'MASTER_SUP'
                                                                                       );
   S9T_PKG.SAVE_OBJ(L_file);

   L_file.add_sheet(SUP_TRAITS_TL_SHEET);
   L_file.sheets(l_file.get_sheet_index(SUP_TRAITS_TL_SHEET)).column_headers := s9t_cells( 'ACTION',
                                                                                           'LANG',
                                                                                           'SUP_TRAIT',
                                                                                           'DESCRIPTION' );
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;

---------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind   IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file      S9T_FILE;
   L_program   VARCHAR2(64) := 'CORESVC_SUP_TRAITS.CREATE_S9T';

BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_SUP_TRAITS(O_file_id);
      POPULATE_SUP_TRAITS_TL(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;

---------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SUP_TRAITS(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_SUP_TRAITS.PROCESS_ID%TYPE) IS

   TYPE svc_sup_traits_col_typ IS TABLE OF SVC_SUP_TRAITS%ROWTYPE;
   L_temp_rec           SVC_SUP_TRAITS%ROWTYPE;
   svc_sup_traits_col   svc_sup_traits_col_typ             := NEW svc_sup_traits_col_typ();
   L_process_id         SVC_SUP_TRAITS.PROCESS_ID%TYPE;
   L_error              BOOLEAN                            := FALSE;
   L_default_rec        SVC_SUP_TRAITS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select sup_trait_mi,
             description_mi,
             master_sup_ind_mi,
             master_sup_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'SUP_TRAITS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('SUP_TRAIT'      as sup_trait,
                                            'DESCRIPTION'    as description,
                                            'MASTER_SUP_IND' as master_sup_ind,
                                            'MASTER_SUP'     as master_sup,
                                             NULL as dummy)
                      );
   L_mi_rec    C_MANDATORY_IND%ROWTYPE;
   dml_errors  EXCEPTION;
   PRAGMA      exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_SUP_TRAITS';
   L_pk_columns    VARCHAR2(255)  := 'Supplier Trait';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select sup_trait_dv,
                      description_dv,
                      master_sup_ind_dv,
                      master_sup_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'SUP_TRAITS'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'SUP_TRAIT'      as sup_trait,
                                                      'DESCRIPTION'    as description,
                                                      'MASTER_SUP_IND' as master_sup_ind,
                                                      'MASTER_SUP'     as master_sup,
                                                       NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.master_sup := rec.master_sup_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_TRAITS ',
                            NULL,
                           'MASTER_SUP ',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.master_sup_ind := rec.master_sup_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_TRAITS ',
                            NULL,
                           'MASTER_SUP_IND ',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.description := rec.description_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_TRAITS ',
                            NULL,
                           'DESCRIPTION ',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.sup_trait := rec.sup_trait_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_TRAITS ',
                            NULL,
                           'SUP_TRAIT ',
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(sup_traits$action)              as action,
          r.get_cell(sup_traits$sup_trait)           as sup_trait,
          r.get_cell(sup_traits$description)         as description,
          r.get_cell(sup_traits$master_sup_ind)      as master_sup_ind,
          r.get_cell(sup_traits$master_sup)          as master_sup,
          r.get_row_seq()                            as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id    = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(sup_traits_sheet)
  )
   LOOP

      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            sup_traits_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.master_sup := rec.master_sup;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            sup_traits_sheet,
                            rec.row_seq,
                            'MASTER_SUP',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.master_sup_ind := rec.master_sup_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            sup_traits_sheet,
                            rec.row_seq,
                            'MASTER_SUP_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.description := rec.description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            sup_traits_sheet,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.sup_trait := rec.sup_trait;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            sup_traits_sheet,
                            rec.row_seq,
                            'SUP_TRAIT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SUP_TRAITS.action_new then
         L_temp_rec.master_sup     := NVL(L_temp_rec.master_sup,L_default_rec.master_sup);
         L_temp_rec.master_sup_ind := NVL(L_temp_rec.master_sup_ind,L_default_rec.master_sup_ind);
         L_temp_rec.description    := NVL(L_temp_rec.description,L_default_rec.description);
         L_temp_rec.sup_trait      := NVL(L_temp_rec.sup_trait,L_default_rec.sup_trait);
      end if;
      if NOT (L_temp_rec.sup_trait is NOT NULL
              and 1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                          sup_traits_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_sup_traits_col.extend();
         svc_sup_traits_col(svc_sup_traits_col.COUNT()):=L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      FORALL i IN 1..svc_sup_traits_col.COUNT SAVE EXCEPTIONS
         merge into SVC_SUP_TRAITS st
         using(select(case
                      when L_mi_rec.sup_trait_mi = 'N'
                       and svc_sup_traits_col(i).action = CORESVC_SUP_TRAITS.action_mod
                       and s1.sup_trait IS NULL
                      then mt.sup_trait
                      else s1.sup_trait
                      end) as sup_trait,
                     (case
                      when l_mi_rec.description_mi = 'N'
                       and svc_sup_traits_col(i).action = CORESVC_SUP_TRAITS.action_mod
                       and s1.description IS NULL
                      then mt.description
                      else s1.description
                      end) as description,
                     (case
                      when l_mi_rec.master_sup_ind_mi = 'N'
                       and svc_sup_traits_col(i).action = CORESVC_SUP_TRAITS.action_mod
                       and s1.master_sup_ind IS NULL
                      then mt.master_sup_ind
                      else s1.master_sup_ind
                      end) as master_sup_ind,
                     (case
                      when l_mi_rec.master_sup_mi = 'N'
                       and svc_sup_traits_col(i).action = CORESVC_SUP_TRAITS.action_mod
                       and s1.master_sup IS NULL
                      then mt.master_sup
                      else s1.master_sup
                       end) as master_sup,
                     NULL as dummy
                 from (select svc_sup_traits_col(i).sup_trait      as sup_trait,
                              svc_sup_traits_col(i).description    as description,
                              svc_sup_traits_col(i).master_sup_ind as master_sup_ind,
                              svc_sup_traits_col(i).master_sup     as master_sup,
                              NULL as dummy
                         from dual ) s1,
                      sup_traits mt
                where mt.sup_trait (+) = s1.sup_trait
                  and 1 = 1 )sq
                   on (st.sup_trait = sq.sup_trait
                       and svc_sup_traits_col(i).action IN (CORESVC_SUP_TRAITS.action_mod,
                                                            CORESVC_SUP_TRAITS.action_del)
                       )
         when matched then
            update
               set process_id         = svc_sup_traits_col(i).process_id ,
                   chunk_id           = svc_sup_traits_col(i).chunk_id ,
                   row_seq            = svc_sup_traits_col(i).row_seq ,
                   action             = svc_sup_traits_col(i).action ,
                   process$status     = svc_sup_traits_col(i).process$status ,
                   description        = sq.description ,
                   master_sup_ind     = sq.master_sup_ind ,
                   master_sup         = sq.master_sup ,
                   create_id          = svc_sup_traits_col(i).create_id ,
                   create_datetime    = svc_sup_traits_col(i).create_datetime ,
                   last_upd_id        = svc_sup_traits_col(i).last_upd_id ,
                   last_upd_datetime  = svc_sup_traits_col(i).last_upd_datetime
         when NOT matched then
            insert(process_id,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   sup_trait ,
                   description ,
                   master_sup_ind ,
                   master_sup ,
                   create_id ,
                   create_datetime ,
                   last_upd_id ,
                   last_upd_datetime)
            values(svc_sup_traits_col(i).process_id ,
                   svc_sup_traits_col(i).chunk_id ,
                   svc_sup_traits_col(i).row_seq ,
                   svc_sup_traits_col(i).action ,
                   svc_sup_traits_col(i).process$status ,
                   sq.sup_trait ,
                   sq.description ,
                   sq.master_sup_ind ,
                   sq.master_sup ,
                   svc_sup_traits_col(i).create_id ,
                   svc_sup_traits_col(i).create_datetime ,
                   svc_sup_traits_col(i).last_upd_id ,
                   svc_sup_traits_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            sup_traits_sheet,
                            svc_sup_traits_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SUP_TRAITS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SUP_TRAITS_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_SUP_TRAITS_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_SUP_TRAITS_TL_COL_TYP IS TABLE OF SVC_SUP_TRAITS_TL%ROWTYPE;
   L_temp_rec              SVC_SUP_TRAITS_TL%ROWTYPE;
   svc_sup_traits_Tl_col   SVC_SUP_TRAITS_TL_COL_TYP := NEW SVC_SUP_TRAITS_TL_COL_TYP();
   L_process_id            SVC_SUP_TRAITS_TL.PROCESS_ID%TYPE;
   L_error                 BOOLEAN := FALSE;
   L_default_rec           SVC_SUP_TRAITS_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select description_mi,
             sup_trait_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'SUP_TRAITS_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('DESCRIPTION' as description,
                                       'SUP_TRAIT'   as sup_trait,
                                       'LANG'        as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_SUP_TRAITS_TL';
   L_pk_columns    VARCHAR2(255)  := 'Supplier Trait, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select description_dv,
                      sup_trait_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'SUP_TRAITS_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('DESCRIPTION' as description,
                                                'SUP_TRAIT'   as sup_trait,
                                                'LANG'        as lang)))
   LOOP
      BEGIN
         L_default_rec.description := rec.description_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_TRAITS_TL_SHEET ,
                            NULL,
                           'DESCRIPTION' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.sup_trait := rec.sup_trait_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           SUP_TRAITS_TL_SHEET ,
                            NULL,
                           'SUP_TRAIT' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_TRAITS_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(sup_traits_tl$action))  as action,
                      r.get_cell(sup_traits_tl$description)    as description,
                      r.get_cell(sup_traits_tl$sup_trait)      as sup_trait,
                      r.get_cell(sup_traits_tl$lang)           as lang,
                      r.get_row_seq()                          as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(SUP_TRAITS_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            sup_traits_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.description := rec.description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_TRAITS_TL_SHEET,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.sup_trait := rec.sup_trait;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_TRAITS_TL_SHEET,
                            rec.row_seq,
                            'SUP_TRAIT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_TRAITS_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SUP_TRAITS.action_new then
         L_temp_rec.description := NVL( L_temp_rec.description,L_default_rec.description);
         L_temp_rec.sup_trait   := NVL( L_temp_rec.sup_trait,L_default_rec.sup_trait);
         L_temp_rec.lang        := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.sup_trait is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         SUP_TRAITS_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_SUP_TRAITS_TL_col.extend();
         SVC_SUP_TRAITS_TL_col(SVC_SUP_TRAITS_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_SUP_TRAITS_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SUP_TRAITS_TL st
      using(select
                  (case
                   when l_mi_rec.description_mi = 'N'
                    and SVC_SUP_TRAITS_TL_col(i).action = CORESVC_SUP_TRAITS.action_mod
                    and s1.description IS NULL then
                        mt.description
                   else s1.description
                   end) as description,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_SUP_TRAITS_TL_col(i).action = CORESVC_SUP_TRAITS.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.sup_trait_mi = 'N'
                    and SVC_SUP_TRAITS_TL_col(i).action = CORESVC_SUP_TRAITS.action_mod
                    and s1.sup_trait IS NULL then
                        mt.sup_trait
                   else s1.sup_trait
                   end) as sup_trait
              from (select SVC_SUP_TRAITS_TL_col(i).description as description,
                           SVC_SUP_TRAITS_TL_col(i).sup_trait        as sup_trait,
                           SVC_SUP_TRAITS_TL_col(i).lang              as lang
                      from dual) s1,
                   sup_traits_TL mt
             where mt.sup_trait (+) = s1.sup_trait
               and mt.lang (+)       = s1.lang) sq
                on (st.sup_trait = sq.sup_trait and
                    st.lang = sq.lang and
                    SVC_SUP_TRAITS_TL_col(i).ACTION IN (CORESVC_SUP_TRAITS.action_mod,CORESVC_SUP_TRAITS.action_del))
      when matched then
      update
         set process_id        = SVC_SUP_TRAITS_TL_col(i).process_id ,
             chunk_id          = SVC_SUP_TRAITS_TL_col(i).chunk_id ,
             row_seq           = SVC_SUP_TRAITS_TL_col(i).row_seq ,
             action            = SVC_SUP_TRAITS_TL_col(i).action ,
             process$status    = SVC_SUP_TRAITS_TL_col(i).process$status ,
             description = sq.description
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             description ,
             sup_trait ,
             lang)
      values(SVC_SUP_TRAITS_TL_col(i).process_id ,
             SVC_SUP_TRAITS_TL_col(i).chunk_id ,
             SVC_SUP_TRAITS_TL_col(i).row_seq ,
             SVC_SUP_TRAITS_TL_col(i).action ,
             SVC_SUP_TRAITS_TL_col(i).process$status ,
             sq.description ,
             sq.sup_trait ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            SUP_TRAITS_TL_SHEET,
                            SVC_SUP_TRAITS_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SUP_TRAITS_TL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_SUP_TRAITS.PROCESS_S9T';
   L_file            S9T_FILE;
   L_sheets          S9T_PKG.NAMES_MAP_TYP;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	     EXCEPTION;
   PRAGMA	     EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_SUP_TRAITS(I_file_id,I_process_id);
      PROCESS_S9T_SUP_TRAITS_TL(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();

   FORALL i in 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   COMMIT;
   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      FORALL i in 1..O_error_count
         insert into s9t_errors
              values lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;

      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_SUP_TRAITS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error           IN OUT   BOOLEAN,
                                I_rec             IN       C_SVC_SUP_TRAITS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_SUP_TRAITS.PROCESS_VAL_SUP_TRAITS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_TRAITS';
   L_exists    BOOLEAN                           := FALSE;

BEGIN
   if I_rec.action in (action_new,action_mod) then
      if I_rec.master_sup_ind = 'Y'
         and I_rec.master_sup is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'MASTER_SUP',
                     'ENTER_MASTER_SUP');
         O_error := TRUE;
      end if;

      if I_rec.master_sup_ind ='Y' then
         if SUP_TRAITS_SQL.VALIDATE_MASTER_SUP(O_error_message,
                                               L_exists,
                                               I_rec.sup_trait) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        NULL,
                        O_error_message);
            O_error := TRUE;
         elsif L_exists = TRUE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'MASTER_SUP_IND',
                        'NO_CHG_MASTER_SUP_IND');
            O_error := TRUE;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_SUP_TRAITS;

----------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_TRAITS_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sup_traits_temp_rec   IN       SUP_TRAITS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_SUP_TRAITS.EXEC_SUP_TRAITS_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_TRAITS';

BEGIN
   insert into sup_traits
        values I_sup_traits_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SUP_TRAITS_INS;

-------------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_TRAITS_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sup_traits_temp_rec   IN       SUP_TRAITS%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_SUP_TRAITS.EXEC_SUP_TRAITS_UPD';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_TRAITS';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor c_sup_traits is
      select 'x'
        from sup_traits
       where sup_trait = I_sup_traits_temp_rec.sup_trait
         for update nowait;

BEGIN
   open c_sup_traits;
   close c_sup_traits;
   update sup_traits
      set row = I_sup_traits_temp_rec
    where sup_trait = I_sup_traits_temp_rec.sup_trait;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sup_traits_temp_rec.sup_trait,
                                             NULL);
      return FALSE;

   when OTHERS then
      if c_sup_traits%ISOPEN then
         close C_SUP_TRAITS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SUP_TRAITS_UPD;
--------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_TRAITS_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sup_traits_temp_rec   IN       SUP_TRAITS%ROWTYPE,
                             I_rec                   IN       C_SVC_SUP_TRAITS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_SUP_TRAITS.EXEC_SUP_TRAITS_DEL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_TRAITS';

BEGIN

   if SUP_TRAITS_SQL.DELETE_TRAIT(O_error_message,
                                  I_sup_traits_temp_rec.sup_trait) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      return FALSE;
   end if;

   return TRUE;
EXCEPTION

   when OTHERS then

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SUP_TRAITS_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_TRAITS_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sup_traits_tl_ins_tab    IN       SUP_TRAITS_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_SUP_TRAITS.EXEC_SUP_TRAITS_TL_INS';

BEGIN
   if I_sup_traits_tl_ins_tab is NOT NULL and I_sup_traits_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_sup_traits_tl_ins_tab.COUNT()
         insert into sup_traits_tl
              values I_sup_traits_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_sup_traits_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_TRAITS_TL_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sup_traits_tl_upd_tab   IN       SUP_TRAITS_TL_TAB,
                                I_sup_traits_tl_upd_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_SUP_TRAITS_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_SUP_TRAITS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SUP_TRAITS.EXEC_SUP_TRAITS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SUP_TRAITS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_SUP_TRAITS_TL_UPD(I_sup_traits  SUP_TRAITS_TL.sup_trait%TYPE,
                                   I_lang        SUP_TRAITS_TL.LANG%TYPE) is
      select 'x'
        from sup_traits_tl
       where sup_trait = I_sup_traits
         and lang = I_lang
         for update nowait;

BEGIN
   if I_sup_traits_tl_upd_tab is NOT NULL and I_sup_traits_tl_upd_tab.count > 0 then
      for i in I_sup_traits_tl_upd_tab.FIRST..I_sup_traits_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_sup_traits_tl_upd_tab(i).lang);
            L_key_val2 := 'Supplier Trait: '||to_char(I_sup_traits_tl_upd_tab(i).sup_trait);
            open C_LOCK_SUP_TRAITS_TL_UPD(I_sup_traits_tl_upd_tab(i).sup_trait,
                                          I_sup_traits_tl_upd_tab(i).lang);
            close C_LOCK_SUP_TRAITS_TL_UPD;
            
            update sup_traits_tl
               set description = I_sup_traits_tl_upd_tab(i).description,
                   last_update_id = I_sup_traits_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_sup_traits_tl_upd_tab(i).last_update_datetime
             where lang = I_sup_traits_tl_upd_tab(i).lang
               and sup_trait = I_sup_traits_tl_upd_tab(i).sup_trait;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SUP_TRAITS_TL',
                           I_sup_traits_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_SUP_TRAITS_TL_UPD%ISOPEN then
         close C_LOCK_SUP_TRAITS_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_SUP_TRAITS_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_TRAITS_TL_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sup_traits_tl_del_tab   IN       SUP_TRAITS_TL_TAB,
                                I_sup_traits_tl_del_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_SUP_TRAITS_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_SUP_TRAITS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_SUP_TRAITS.EXEC_SUP_TRAITS_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SUP_TRAITS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_SUP_TRAITS_TL_DEL(I_sup_trait   SUP_TRAITS_TL.SUP_TRAIT%TYPE,
                                   I_lang        SUP_TRAITS_TL.LANG%TYPE) is
      select 'x'
        from sup_traits_TL
       where sup_trait = I_sup_trait
         and lang = I_lang
         for update nowait;

BEGIN
   if I_sup_traits_tl_del_tab is NOT NULL and I_sup_traits_tl_del_tab.count > 0 then
      for i in I_sup_traits_tl_del_tab.FIRST..I_sup_traits_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_sup_traits_tl_del_tab(i).lang);
            L_key_val2 := 'Supplier Trait: '||to_char(I_sup_traits_tl_del_tab(i).sup_trait);
            open C_LOCK_SUP_TRAITS_TL_DEL(I_sup_traits_tl_del_tab(i).sup_trait,
                                          I_sup_traits_tl_del_tab(i).lang);
            close C_LOCK_SUP_TRAITS_TL_DEL;
            
            delete sup_traits_tl
             where lang = I_sup_traits_tl_del_tab(i).lang
               and sup_trait = I_sup_traits_tl_del_tab(i).sup_trait;
               
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_SUP_TRAITS_TL',
                           I_sup_traits_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_SUP_TRAITS_TL_DEL%ISOPEN then
         close C_LOCK_SUP_TRAITS_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_SUP_TRAITS_TL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_SUP_TRAITS(O_error_message   IN OUT   RTK_ERRORS.RTK_KEY%TYPE,
                            I_process_id      IN       SVC_SUP_TRAITS.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_SUP_TRAITS.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                      := 'CORESVC_SUP_TRAITS.PROCESS_SUP_TRAITS';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_TRAITS';
   L_process_error          BOOLEAN;
   L_error                  BOOLEAN;
   L_sup_traits_temp_rec    SUP_TRAITS%ROWTYPE;

BEGIN
   FOR rec in C_SVC_SUP_TRAITS(I_process_id,
                               I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      if rec.action is NULL
         or rec.action NOT in (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.pk_sup_traits_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SUP_TRAIT',
                     'DUP_SUP_TRAIT');
         L_error := TRUE;
      end if;

      if rec.action in (action_mod,action_del)
         and rec.sup_trait is NOT NULL
         and rec.pk_sup_traits_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SUP_TRAIT',
                     'SUP_TRAIT_NOT_EXIST');
         L_error := TRUE;
      end if;

      if rec.action in (action_new,action_mod)
         and rec.description is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DESCRIPTION',
                        'ENTER_SUP_TRAIT_DESC');
            L_error := TRUE;
      end if;

      if rec.action IN (action_new,action_mod)
         and rec.sup_trait is NOT NULL
         and rec.sup_trait < 1 then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SUP_TRAIT',
                     'GREATER_0');
         L_error :=TRUE;
      end if;

      if rec.action in (action_new,action_mod) then
         if rec.master_sup_ind NOT in ( 'Y','N' ) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'MASTER_SUP_IND',
                           'INV_Y_N_IND');
               L_error := TRUE;
         end if;
      end if;

      if rec.action in (action_new,action_mod) then
         if rec.master_sup_ind is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'MASTER_SUP_IND',
                           'MUST_ENTER_VALUE');
               L_error := TRUE;
         end if;
      end if;

      if rec.action in (action_new,action_mod) then
         if PROCESS_VAL_SUP_TRAITS(O_error_message,
                                   L_error,
                                   rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error := TRUE;
         end if;
      end if;

      if NOT L_error then
         L_sup_traits_temp_rec.sup_trait           := rec.sup_trait;
         L_sup_traits_temp_rec.description         := rec.description;
         L_sup_traits_temp_rec.master_sup_ind      := rec.master_sup_ind;
         L_sup_traits_temp_rec.master_sup          := rec.master_sup;

         if rec.master_sup_ind is NULL then
            L_sup_traits_temp_rec.master_sup_ind   := 'N';
         end if;

         if L_sup_traits_temp_rec.master_sup_ind = 'N' then
            L_sup_traits_temp_rec.master_sup       := NULL;
         end if;

         if rec.action = action_new then
            if EXEC_SUP_TRAITS_INS(O_error_message,
                                   L_sup_traits_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_SUP_TRAITS_UPD(O_error_message,
                                   L_sup_traits_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_SUP_TRAITS_DEL(O_error_message,
                                   L_sup_traits_temp_rec,
                                   rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;

   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SUP_TRAITS;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SUP_TRAITS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_SUP_TRAITS_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_SUP_TRAITS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64) := 'CORESVC_SUP_TRAITS.PROCESS_SUP_TRAITS_TL';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SUP_TRAITS_TL';
   L_base_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SUP_TRAITS';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_TRAITS_TL';
   L_error                     BOOLEAN := FALSE;
   L_process_error             BOOLEAN := FALSE;
   L_sup_traits_TL_temp_rec    SUP_TRAITS_TL%ROWTYPE;
   L_sup_traits_tl_upd_rst     ROW_SEQ_TAB;
   L_sup_traits_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_SUP_TRAITS_TL(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
      select pk_sup_traits_tl.rowid  as pk_sup_traits_tl_rid,
             fk_sup_traits.rowid     as fk_sup_traits_rid,
             fk_lang.rowid           as fk_lang_rid,
             st.lang,
             st.sup_trait,
             st.description,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_sup_traits_tl  st,
             sup_traits         fk_sup_traits,
             sup_traits_tl      pk_sup_traits_tl,
             lang               fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.sup_trait   =  fk_sup_traits.sup_trait (+)
         and st.lang        =  pk_sup_traits_TL.lang (+)
         and st.sup_trait   =  pk_sup_traits_TL.sup_trait (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_SUP_TRAITS_TL is TABLE OF C_SVC_SUP_TRAITS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_sup_traits_tab        SVC_SUP_TRAITS_TL;

   L_sup_traits_TL_ins_tab         sup_traits_tl_tab         := NEW sup_traits_tl_tab();
   L_sup_traits_TL_upd_tab         sup_traits_tl_tab         := NEW sup_traits_tl_tab();
   L_sup_traits_TL_del_tab         sup_traits_tl_tab         := NEW sup_traits_tl_tab();

BEGIN
   if C_SVC_SUP_TRAITS_TL%ISOPEN then
      close C_SVC_SUP_TRAITS_TL;
   end if;

   open C_SVC_SUP_TRAITS_TL(I_process_id,
                            I_chunk_id);
   LOOP
      fetch C_SVC_SUP_TRAITS_TL bulk collect into L_svc_sup_traits_tab limit LP_bulk_fetch_limit;
      if L_svc_sup_traits_tab.COUNT > 0 then
         FOR i in L_svc_sup_traits_tab.FIRST..L_svc_sup_traits_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_sup_traits_tab(i).action is NULL
               or L_svc_sup_traits_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sup_traits_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

             --check for primary_lang
            if L_svc_sup_traits_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sup_traits_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_sup_traits_tab(i).action = action_new
               and L_svc_sup_traits_tab(i).pk_sup_traits_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sup_traits_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_sup_traits_tab(i).action IN (action_mod, action_del)
               and L_svc_sup_traits_tab(i).lang is NOT NULL
               and L_svc_sup_traits_tab(i).sup_trait is NOT NULL
               and L_svc_sup_traits_tab(i).pk_sup_traits_TL_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_sup_traits_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_sup_traits_tab(i).action = action_new
               and L_svc_sup_traits_tab(i).sup_trait is NOT NULL
               and L_svc_sup_traits_tab(i).fk_sup_traits_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_sup_traits_tab(i).row_seq,
                            'SUP_TRAIT',
                            'SUP_TRAIT_NOT_EXIST');
               L_error :=TRUE;
            end if;

            if L_svc_sup_traits_tab(i).action = action_new
               and L_svc_sup_traits_tab(i).lang is NOT NULL
               and L_svc_sup_traits_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sup_traits_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_sup_traits_tab(i).action in (action_new, action_mod) then
               if L_svc_sup_traits_tab(i).description is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_sup_traits_tab(i).row_seq,
                              'DESCRIPTION',
                              'ENTER_SUP_TRAIT_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_sup_traits_tab(i).sup_trait is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sup_traits_tab(i).row_seq,
                           'SUP_TRAIT',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_sup_traits_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sup_traits_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_sup_traits_TL_temp_rec.lang := L_svc_sup_traits_tab(i).lang;
               L_sup_traits_TL_temp_rec.sup_trait := L_svc_sup_traits_tab(i).sup_trait;
               L_sup_traits_TL_temp_rec.description := L_svc_sup_traits_tab(i).description;
               L_sup_traits_TL_temp_rec.create_datetime := SYSDATE;
               L_sup_traits_TL_temp_rec.create_id := GET_USER;
               L_sup_traits_TL_temp_rec.last_update_datetime := SYSDATE;
               L_sup_traits_TL_temp_rec.last_update_id := GET_USER;

               if L_svc_sup_traits_tab(i).action = action_new then
                  L_sup_traits_TL_ins_tab.extend;
                  L_sup_traits_TL_ins_tab(L_sup_traits_TL_ins_tab.count()) := L_sup_traits_TL_temp_rec;
               end if;

               if L_svc_sup_traits_tab(i).action = action_mod then
                  L_sup_traits_TL_upd_tab.extend;
                  L_sup_traits_TL_upd_tab(L_sup_traits_TL_upd_tab.count()) := L_sup_traits_TL_temp_rec;
                  L_sup_traits_TL_upd_rst(L_sup_traits_TL_upd_tab.count()) := L_svc_sup_traits_tab(i).row_seq;
               end if;

               if L_svc_sup_traits_tab(i).action = action_del then
                  L_sup_traits_TL_del_tab.extend;
                  L_sup_traits_TL_del_tab(L_sup_traits_TL_del_tab.count()) := L_sup_traits_TL_temp_rec;
                  L_sup_traits_TL_del_rst(L_sup_traits_TL_del_tab.count()) := L_svc_sup_traits_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_SUP_TRAITS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_SUP_TRAITS_TL;

   if EXEC_SUP_TRAITS_TL_INS(O_error_message,
                             L_sup_traits_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_SUP_TRAITS_TL_UPD(O_error_message,
                             L_sup_traits_TL_upd_tab,
                             L_sup_traits_TL_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_SUP_TRAITS_TL_DEL(O_error_message,
                             L_sup_traits_TL_del_tab,
                             L_sup_traits_TL_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SUP_TRAITS_TL;
-----------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_SUP_TRAITS.PROCESS_ID%TYPE) IS

BEGIN
   delete
     from svc_sup_traits_tl
    where process_id= I_process_id;

   delete
     from svc_sup_traits
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;

----------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64):='CORESVC_SUP_TRAITS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_SUP_TRAITS(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_SUP_TRAITS_TL(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;

EXCEPTION
   when OTHERS then
   CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------
END CORESVC_SUP_TRAITS;
/