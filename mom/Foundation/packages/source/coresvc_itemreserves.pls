CREATE OR REPLACE PACKAGE CORESVC_ITEM_RESERVE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
FUNCTION PURGE_ITEM_RESERVE_TABLE(O_error_message   IN OUT   NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION RESERVE_ITEM_NUMBER(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_businessObject            IN OUT   "RIB_ItemNumColDesc_REC",
                             I_serviceOperationContext   IN       "RIB_ServiceOpContext_REC",
                             I_businessObject            IN       "RIB_ItemNumCriVo_REC")
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists             OUT   BOOLEAN,
                           I_item            IN       SVC_ITEM_RESERVATION.ITEM%TYPE,
                           I_item_num_type   IN       SVC_ITEM_RESERVATION.ITEM_NUMBER_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END CORESVC_ITEM_RESERVE_SQL;
/
