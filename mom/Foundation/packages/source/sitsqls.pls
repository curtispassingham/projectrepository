
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SIT_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------------
-- Function: SIT_EXISTS
-- Purpose:  This function takes an itemlist or loc_list as input
--           and checks if it is on the sit_head table.
--           Returns true when the list is found, false otherwise 
--           
-----------------------------------------------------------------------------------------------
FUNCTION SIT_EXISTS(O_error_message IN OUT  VARCHAR2,
                    O_exists        IN OUT  BOOLEAN,
                    I_itemlist      IN      SKULIST_HEAD.SKULIST%TYPE,
                    I_loc_list      IN      LOC_LIST_HEAD.LOC_LIST%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: DELETE_SIT
-- Purpose:  This function takes an itemlist, and/or loc_list, or itemloc_link_id 
--           as input and deletes the associated records from sit_head, 
--           sit_detail, sit_explode, and sit_conflict.
-----------------------------------------------------------------------------------------------
FUNCTION DELETE_SIT(O_error_message   IN OUT  VARCHAR2,
                    I_itemloc_link_id IN      SIT_HEAD.ITEMLOC_LINK_ID%TYPE,
                    I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                    I_loc_list        IN      LOC_LIST_HEAD.LOC_LIST%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: INSERT_ITEM
-- Purpose:  This function takes an item and itemlist as input and 
--           inserts a new record on sit_explode for every location 
--           that will be linked to the new item. 
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM(O_error_message   IN OUT  VARCHAR2,
                     I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                     I_item            IN      SKULIST_DETAIL.ITEM%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: DELETE_ITEM
-- Purpose:  This function takes an item and itemlist as input and updates
--           the sit_explode table.  Any record associated with the passed in 
--           itemlist/item will have its action updated to 'D'.
-----------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM(O_error_message   IN OUT  VARCHAR2,
                     I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                     I_item            IN      SKULIST_DETAIL.ITEM%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: EXPLODE_RECORDS
-- Purpose:  This function creates new records on the sit_explode table for the
--           product of items in the passed in itemlist and locations from the 
--           passed in loc_list for a newly created itemloc_link_id. The default 
--           value for the update_ind is 'Y' when null is passed in. 
-----------------------------------------------------------------------------------------------
FUNCTION EXPLODE_RECORDS(O_error_message   IN OUT  VARCHAR2,
                         I_itemloc_link_id IN      SIT_HEAD.ITEMLOC_LINK_ID%TYPE,
                         I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                         I_loc_list        IN      LOC_LIST_HEAD.LOC_LIST%TYPE,
                         I_update_ind      IN      SIT_EXPLODE.UPDATE_IND%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: VALIDATE_NEW_LINK
-- Purpose:  This function will scan the sit_explode table to find the possilbe creation
--           of duplicate item/loc records in a newly created link.  Any duplicates that are
--           found are recorded on the sit_conflict table and the function outputs 'Y',
--           otherwise 'N'.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_NEW_LINK(O_error_message    IN OUT  VARCHAR2,
                           O_dup_flag         IN OUT  VARCHAR2,
                           I_itemloc_link_id  IN      SIT_HEAD.ITEMLOC_LINK_ID%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: DETAIL_EXISTS
-- Purpose:  This function scans the sit_detail table to determine if 
--           details exist for the input itemloc_link_id.  If details are found
--           the fucntion outputs TRUE otherwise FALSE.
-----------------------------------------------------------------------------------------------
FUNCTION DETAIL_EXISTS(O_error_message    IN OUT  VARCHAR2,
                       O_exists           IN OUT  BOOLEAN,
                       I_itemloc_link_id  IN      SIT_DETAIL.ITEMLOC_LINK_ID%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: COPY_SIT_CONFLICT
-- Purpose:  This function is to copy all the sit_conflict records--associated with either the input 
--           itemlist or location list--to the sit_conflict_temp table.  This is done so that records 
--           can be deleted from sit_explode without child record constraint errors while rebuilding
--           an item list or location list.  REBUILD_SIT_CONFLICT should be called after 
--           the list is rebuilt.  
-----------------------------------------------------------------------------------------------
FUNCTION COPY_SIT_CONFLICT(O_error_message   IN OUT VARCHAR2,
                           I_itemlist        IN     SIT_HEAD.SKULIST%TYPE,
                           I_loc_list        IN     SIT_HEAD.LOC_LIST%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: REBUILD_SIT_CONFLICT
-- Purpose:  This function is to copy all the sit_conflict_temp records--associated with either the
--           input itemlist or loc_list--to the sit_conflict table.  This function should be called 
--           after COPY_SIT_CONFLICT, and after the necessary processing of the parent records 
--           has taken place.
-----------------------------------------------------------------------------------------------
FUNCTION REBUILD_SIT_CONFLICT(O_error_message    IN OUT VARCHAR2,
                              I_itemlist         IN     SIT_HEAD.SKULIST%TYPE,
                              I_loc_list         IN     SIT_HEAD.LOC_LIST%TYPE) 
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function: SITMAIN_PRE_PROCESS
-- Purpose:  This function is called by prepost batch which checks for items in item_loc for which
--           entries are not present in item_country and item_cost_head. If not found, it inserts
--           the corresponding entries in the respective tables.
-----------------------------------------------------------------------------------------------
FUNCTION SITMAIN_PRE_PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END SIT_SQL;
/
