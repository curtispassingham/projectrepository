CREATE OR REPLACE PACKAGE ITEM_LOC_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC(O_error_message       IN OUT VARCHAR2,
                      O_item_parent         IN OUT item_loc.item_parent%TYPE,
                      O_item_grandparent    IN OUT item_loc.item_grandparent%TYPE,
                      O_loc_type            IN OUT item_loc.loc_type%TYPE,
                      O_unit_retail         IN OUT item_loc.unit_retail%TYPE,
                      O_selling_unit_retail IN OUT item_loc.selling_unit_retail%TYPE,
                      O_selling_uom         IN OUT item_loc.selling_uom%TYPE,
                      O_clear_ind           IN OUT item_loc.clear_ind%TYPE,
                      O_taxable_ind         IN OUT item_loc.taxable_ind%TYPE,
                      O_local_item_desc     IN OUT item_loc.local_item_desc%TYPE,
                      O_local_short_desc    IN OUT item_loc.local_short_desc%TYPE,
                      O_ti                  IN OUT item_loc.ti%TYPE,
                      O_hi                  IN OUT item_loc.hi%TYPE,
                      O_store_ord_mult      IN OUT item_loc.store_ord_mult%TYPE,
                      O_status              IN OUT item_loc.status%TYPE,
                      O_status_update_date  IN OUT item_loc.status_update_date%TYPE,
                      O_daily_waste_pct     IN OUT item_loc.daily_waste_pct%TYPE,
                      O_meas_of_each        IN OUT item_loc.meas_of_each%TYPE,
                      O_meas_of_price       IN OUT item_loc.meas_of_price%TYPE,
                      O_uom_of_price        IN OUT item_loc.uom_of_price%TYPE,
                      O_primary_variant     IN OUT item_loc.primary_variant%TYPE,
                      O_primary_supp        IN OUT item_loc.primary_supp%TYPE,
                      O_primary_cntry       IN OUT item_loc.primary_cntry%TYPE,
                      O_primary_cost_pack   IN OUT item_loc.primary_cost_pack%TYPE,
                      I_item                IN     item_loc.item%TYPE,
                      I_loc                 IN     item_loc.loc%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC(O_error_message       IN OUT VARCHAR2,
                      O_item_loc            IN OUT ITEM_LOC%ROWTYPE,
                      I_item                IN     ITEM_LOC.ITEM%TYPE,
                      I_loc                 IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION ALL_LOCS_EXIST(O_error_message  IN OUT VARCHAR2,
                        O_all_locs_exist IN OUT BOOLEAN,
                        I_item           IN     item_master.item%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC(O_error_message          IN OUT VARCHAR2,
                         I_item                   IN     ITEM_MASTER.ITEM%TYPE,
                         I_item_status            IN     ITEM_MASTER.STATUS%TYPE,
                         I_item_level             IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                         I_tran_level             IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                         I_loc                    IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type               IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_primary_supp           IN     ITEM_LOC.PRIMARY_SUPP%TYPE,
                         I_primary_cntry          IN     ITEM_LOC.PRIMARY_CNTRY%TYPE,
                         I_status                 IN     ITEM_LOC.STATUS%TYPE,
                         I_local_item_desc        IN     ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                         I_local_short_desc       IN     ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                         I_primary_variant        IN     ITEM_LOC.PRIMARY_VARIANT%TYPE,
                         I_unit_retail            IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_ti                     IN     ITEM_LOC.TI%TYPE,
                         I_hi                     IN     ITEM_LOC.HI%TYPE,
                         I_store_ord_mult         IN     ITEM_LOC.STORE_ORD_MULT%TYPE,
                         I_daily_waste_pct        IN     ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                         I_taxable_ind            IN     ITEM_LOC.TAXABLE_IND%TYPE,
                         I_meas_of_each           IN     ITEM_LOC.MEAS_OF_EACH%TYPE,
                         I_meas_of_price          IN     ITEM_LOC.MEAS_OF_PRICE%TYPE,
                         I_uom_of_price           IN     ITEM_LOC.UOM_OF_PRICE%TYPE,
                         I_selling_unit_retail    IN     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                         I_selling_uom            IN     ITEM_LOC.SELLING_UOM%TYPE,
                         I_primary_cost_pack      IN     ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                         I_process_children       IN     VARCHAR2,
                         I_receive_as_type        IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_ranged_ind             IN     ITEM_LOC.RANGED_IND%TYPE,
                         I_inbound_handling_days  IN     ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE,
                         I_store_price_ind        IN     ITEM_LOC.STORE_PRICE_IND%TYPE,
                         I_source_method          IN     ITEM_LOC.SOURCE_METHOD%TYPE,
                         I_source_wh              IN     ITEM_LOC.SOURCE_WH%TYPE,
                         I_multi_units            IN     ITEM_LOC.MULTI_UNITS%TYPE,
                         I_multi_unit_retail      IN     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                         I_multi_selling_uom      IN     ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                         I_uin_type               IN     ITEM_LOC.UIN_TYPE%TYPE,
                         I_uin_label              IN     ITEM_lOC.UIN_LABEL%TYPE,
                         I_capture_time           IN     ITEM_LOC.CAPTURE_TIME%TYPE,
                         I_ext_uin_ind            IN     ITEM_LOC.EXT_UIN_IND%TYPE,
                         I_costing_loc            IN     ITEM_LOC.COSTING_LOC%TYPE DEFAULT NULL,
                         I_costing_loc_type       IN     ITEM_LOC.COSTING_LOC_TYPE%TYPE DEFAULT NULL)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION STATUS_CHANGE_VALID(O_error_message       IN OUT VARCHAR2,
                            I_item                 IN     item_master.item%TYPE,
                            I_loc                  IN     item_loc.loc%TYPE,
                            I_loc_type             IN     item_loc.loc_type%TYPE,
                            I_old_status           IN     item_loc.status%TYPE,
                            I_new_status           IN     item_loc.status%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION LOCS_EXIST_FOR_GROUP(O_error_message IN OUT VARCHAR2,
                              O_locs_exist    IN OUT BOOLEAN,
                              I_group_type    IN     code_detail.code%TYPE,
                              I_group_value   IN     VARCHAR2)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_TRAITS_COMPLETE(O_error_message IN OUT VARCHAR2,
                                   O_complete     IN OUT VARCHAR2,
                                   I_item         IN     item_master.item%TYPE,
                                   I_loc          IN     item_loc.loc%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION ITEM_EXISTS_FOR_ALL_GROUP_LOCS(O_error_message       IN OUT VARCHAR2,
                                        O_item_exists         IN OUT BOOLEAN,
                                        O_item_active_at_locs IN OUT BOOLEAN,
                                        I_item                IN     item_master.item%TYPE,
                                        I_group_type          IN     code_detail.code%TYPE,
                                        I_group_value         IN     VARCHAR2,
                                        I_currency_code       IN     currencies.currency_code%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_PACK_COMP_ITEMLOC_STATUS(O_error_message      IN OUT VARCHAR2,
                                        O_inactive_status    IN OUT BOOLEAN,
                                        O_delete_disc_status IN OUT BOOLEAN,
                                        I_packitem           IN     item_master.item%TYPE,
                                        I_group_type         IN     VARCHAR2,
                                        I_group_value        IN     VARCHAR2,
                                        I_currency_code      IN     currencies.currency_code%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_PACK_COMP_STATUS(O_error_message      IN OUT VARCHAR2,
                                 I_packitem           IN     item_master.item%TYPE,
                                 I_group_type         IN     VARCHAR2,
                                 I_group_value        IN     VARCHAR2,
                                 I_currency_code      IN     currencies.currency_code%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_RECV_AS_TYPE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item            IN     ITEM_LOC.ITEM%TYPE,
                             I_location        IN     ITEM_LOC.LOC%TYPE,
                             I_receive_as_type IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION GET_RECV_AS_TYPE_FOR_VWH   (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists          IN OUT VARCHAR2,
                                     O_receive_as_type IN OUT ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                                     I_location        IN     ITEM_LOC.LOC%TYPE,
                                     I_item            IN     ITEM_LOC.ITEM%TYPE)
   return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION CHILD_LOCS_EXIST (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_child_locs_exist IN OUT BOOLEAN,
                           I_item             IN     ITEM_LOC.ITEM%TYPE)

   return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_COST_PACK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item            IN     ITEM_LOC.ITEM%TYPE,
                                  I_loc         IN     ITEM_LOC.LOC%TYPE DEFAULT NULL)
   return BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION CHECK_PRIMARY_COST_PACK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists   IN OUT VARCHAR2,
                                 I_item     IN     ITEM_LOC.ITEM%TYPE,
                                 I_loc      IN     ITEM_LOC.LOC%TYPE DEFAULT NULL)
   return BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION ITEM_DESC_TO_ITEMLOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN     ITEM_MASTER.ITEM%TYPE,
                              I_item_desc       IN     ITEM_MASTER.ITEM_DESC%TYPE,
                              I_short_desc      IN     ITEM_MASTER.SHORT_DESC%TYPE,
                              I_item_level      IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                              I_tran_level      IN     ITEM_MASTER.TRAN_LEVEL%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION ITEM_LOCATION_EXISTS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_item_location_exists   IN OUT   BOOLEAN,
                              I_item                   IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC(O_error_message          IN OUT VARCHAR2,
                         I_item                   IN     ITEM_MASTER.ITEM%TYPE,
                         I_item_status            IN     ITEM_MASTER.STATUS%TYPE,
                         I_item_level             IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                         I_tran_level             IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                         I_loc                    IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type               IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_primary_supp           IN     ITEM_LOC.PRIMARY_SUPP%TYPE,
                         I_primary_cntry          IN     ITEM_LOC.PRIMARY_CNTRY%TYPE,
                         I_status                 IN     ITEM_LOC.STATUS%TYPE,
                         I_local_item_desc        IN     ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                         I_local_short_desc       IN     ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                         I_primary_variant        IN     ITEM_LOC.PRIMARY_VARIANT%TYPE,
                         I_unit_retail            IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_ti                     IN     ITEM_LOC.TI%TYPE,
                         I_hi                     IN     ITEM_LOC.HI%TYPE,
                         I_store_ord_mult         IN     ITEM_LOC.STORE_ORD_MULT%TYPE,
                         I_daily_waste_pct        IN     ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                         I_taxable_ind            IN     ITEM_LOC.TAXABLE_IND%TYPE,
                         I_meas_of_each           IN     ITEM_LOC.MEAS_OF_EACH%TYPE,
                         I_meas_of_price          IN     ITEM_LOC.MEAS_OF_PRICE%TYPE,
                         I_uom_of_price           IN     ITEM_LOC.UOM_OF_PRICE%TYPE,
                         I_selling_unit_retail    IN     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                         I_selling_uom            IN     ITEM_LOC.SELLING_UOM%TYPE,
                         I_primary_cost_pack      IN     ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                         I_process_children       IN     VARCHAR2,
                         I_receive_as_type        IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_ranged_ind             IN     ITEM_LOC.RANGED_IND%TYPE,
                         I_inbound_handling_days  IN     ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE DEFAULT NULL,
                         I_store_price_ind        IN     ITEM_LOC.STORE_PRICE_IND%TYPE       DEFAULT NULL,
                         I_source_method          IN     ITEM_LOC.SOURCE_METHOD%TYPE         DEFAULT NULL,
                         I_source_wh              IN     ITEM_LOC.SOURCE_WH%TYPE             DEFAULT NULL,
                         I_uin_type               IN     ITEM_LOC.UIN_TYPE%TYPE              DEFAULT NULL,
                         I_uin_label              IN     ITEM_lOC.UIN_LABEL%TYPE             DEFAULT NULL,
                         I_capture_time           IN     ITEM_LOC.CAPTURE_TIME%TYPE          DEFAULT NULL,
                         I_ext_uin_ind            IN     ITEM_LOC.EXT_UIN_IND%TYPE           DEFAULT 'N',
                         I_costing_loc            IN     ITEM_LOC.COSTING_LOC%TYPE           DEFAULT NULL,
                         I_costing_loc_type       IN     ITEM_LOC.COSTING_LOC_TYPE%TYPE      DEFAULT NULL)
return BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid_location   IN OUT   BOOLEAN,
                           I_location         IN       ITEM_LOC.LOC%TYPE,
                           I_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_source_wh        IN       ITEM_LOC.SOURCE_WH%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_WAREHOUSE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist            IN OUT   BOOLEAN,
                            I_item             IN       ITEM_LOC.ITEM%TYPE,
                            I_source_wh        IN       ITEM_LOC.SOURCE_WH%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ST_WH(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exist            IN OUT   BOOLEAN,
                        I_item             IN       ITEM_LOC.ITEM%TYPE,
                        I_location         IN       ITEM_LOC.LOC%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COSTINGLOC_CHANGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_status           IN OUT ITEM_MASTER.STATUS%TYPE,
                                    O_event_type       IN OUT COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE,
                                    O_exists           IN OUT BOOLEAN,
                                    I_item             IN     ITEM_LOC.ITEM%TYPE,
                                    I_location         IN     ITEM_LOC.LOC%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_SERIALIZATION(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT   VARCHAR2,
                                I_item             IN       ITEM_LOC.ITEM%TYPE)

return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_SERIALIZATION(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item             IN       ITEM_LOC.ITEM%TYPE)

return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_item            IN       ITEM_COUNTRY.ITEM%TYPE,
                            I_location        IN       ITEM_LOC.LOC%TYPE,
                            I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_invalid_loc_ind   IN OUT   VARCHAR2,
                            O_valid_locs_tbl    IN OUT   LOC_TBL,
                            I_item              IN       ITEM_LOC.ITEM%TYPE,
                            I_group_type        IN       CODE_DETAIL.CODE%TYPE,
                            I_group_value       IN       VARCHAR2,
                            I_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                            I_currency_code     IN       STORE.CURRENCY_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION DEL_GTAX_ITEM_ROLLUP (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item             IN     ITEM_LOC.ITEM%TYPE)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
---FUNCTION: CHK_MULT_SUPPLIER
---PURPOSE:  This function is to insert item_cost_head and item_cost_detail record
---          for all the supplier and origin country id on base country and primary supplier
---          cost record.
-----------------------------------------------------------------------------------------------
FUNCTION CHK_MULT_SUPPLIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item                  IN     ITEM_COST_HEAD.ITEM%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
---FUNCTION: INSERT_ITEM_COST_LOC
---PURPOSE:  This function is to insert item_cost_head and item_cost_detail record
---          for all the delivery_country_id by taking the base_country_id cost record.
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_COST_LOC(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                        IN     ITEM_COST_HEAD.ITEM%TYPE,
                             I_supplier                    IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                             I_origin_country_id           IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                             I_update_itemcost_child_ind   IN     VARCHAR2 DEFAULT 'N',
                             I_update_loc_ind              IN     VARCHAR2 DEFAULT 'N')

RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: CHECK_ORG_UNIT_ID
-- Purpose      : This function is used to compare the org_unit_id of primary supplier for an item
--                with the locations being added.
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_ORG_UNIT_ID(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_supp_loc_same_ou  IN OUT BOOLEAN,
                           I_locs_tbl          IN LOC_TBL,
                           I_supplier          IN SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: AUTO_RANGE_CONTAINER_ITEM
-- Purpose      : This function is used to copy all the location ranged to content from the
--                Container item. This Function is called from itemmaster from when a container
--                item of existing content item is changed.
-----------------------------------------------------------------------------------------------
FUNCTION AUTO_RANGE_CONTAINER_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_content_item      IN     ITEM_MASTER.ITEM%TYPE,
                                   I_container_item    IN     ITEM_MASTER.ITEM%TYPE)


RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: COST_LOC_EXISTS_FOR_CHILD
-- Purpose      : This function will check if the costing location is not ranged
--                to atleast one child item.
-----------------------------------------------------------------------------------------------
FUNCTION COST_LOC_EXISTS_FOR_CHILD(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists           IN OUT BOOLEAN,
                                   I_item             IN     ITEM_LOC.ITEM%TYPE,
                                   I_cost_loc         IN     ITEM_LOC.COSTING_LOC%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_COSTINGLOC_CHANGE
-- Purpose      : This function will check if the there is a costing location change for the tran
--                level item. This is called from the itemloc form.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COSTINGLOC_CHANGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_status           IN OUT ITEM_MASTER.STATUS%TYPE,
                                    O_event_type       IN OUT COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE,
                                    O_exists           IN OUT BOOLEAN,
                                    I_item             IN     ITEM_LOC.ITEM%TYPE,
                                    I_location         IN     ITEM_LOC.LOC%TYPE,
                                    I_item_level       IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                                    I_tran_level       IN     ITEM_MASTER.TRAN_LEVEL%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: WF_STORE_EXIST
-- Purpose      : Checks if any of the locations are franchise stores.
----------------------------------------------------------------------------------------------
FUNCTION WF_STORE_EXIST(O_error_message   IN OUT   VARCHAR2,
                        O_f_loc_exist     IN OUT   VARCHAR2,
                        O_c_loc_exist     IN OUT   VARCHAR2,
                        I_group_type      IN       CODE_DETAIL.CODE%TYPE,
                        I_group_value     IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_WH_TO_WH
-- Purpose      : Checks if source warehouse belongs to the group value passed in.
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_WH_TO_WH(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_group_type       IN       CODE_DETAIL.CODE_TYPE%TYPE,
                           I_group_value      IN       WH.WH%TYPE,
                           I_source_wh        IN       WH.WH%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END;

/
