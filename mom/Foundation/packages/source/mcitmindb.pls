CREATE OR REPLACE PACKAGE BODY MC_ITEM_IND AS
-------------------------------------------------------------------------------
FUNCTION MC_IND (O_error_message               IN OUT VARCHAR2,
                 O_reject                      IN OUT BOOLEAN,
                 I_item                        IN     ITEM_MASTER.ITEM%TYPE,
                 I_itemlist                    IN     SKULIST_HEAD.SKULIST%TYPE,
                 I_cost_zone_group_id          IN     ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE,
                 I_cost_zone_group_id_update   IN     VARCHAR2,
                 I_merchandise_ind             IN     ITEM_MASTER.MERCHANDISE_IND%TYPE,
                 I_merchandise_ind_update      IN     VARCHAR2,
                 I_forecast_ind                IN     ITEM_MASTER.FORECAST_IND%TYPE,
                 I_forecast_ind_update         IN     VARCHAR2,
                 I_retail_label_type           IN     ITEM_MASTER.RETAIL_LABEL_TYPE%TYPE,
                 I_retail_label_type_update    IN     VARCHAR2,
                 I_retail_label_value          IN     ITEM_MASTER.RETAIL_LABEL_VALUE%TYPE,
                 I_handling_temp               IN     ITEM_MASTER.HANDLING_TEMP%TYPE,
                 I_handling_temp_update        IN     VARCHAR2,
                 I_handling_sensitivity        IN     ITEM_MASTER.HANDLING_SENSITIVITY%TYPE,
                 I_handling_sensitivity_update IN     VARCHAR2,
                 I_catch_weight_ind            IN     ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                 I_catch_weight_ind_update     IN     VARCHAR2,
                 I_waste_type                  IN     ITEM_MASTER.WASTE_TYPE%TYPE,
                 I_waste_type_update           IN     VARCHAR2,
                 I_waste_pct                   IN     ITEM_MASTER.WASTE_PCT%TYPE,
                 I_default_waste_pct           IN     ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                 I_package_size                IN     ITEM_MASTER.PACKAGE_SIZE%TYPE,
                 I_package_size_update         IN     VARCHAR2,
                 I_package_uom                 IN     ITEM_MASTER.PACKAGE_UOM%TYPE,
                 I_perishable_ind              IN     ITEM_MASTER.PERISHABLE_IND%TYPE,
                 I_perishable_ind_update       IN     VARCHAR2,
                 I_comments                    IN     ITEM_MASTER.COMMENTS%TYPE,
                 I_comments_update             IN     VARCHAR2,
                 I_brand_name                  IN     ITEM_MASTER.BRAND_NAME%TYPE,
                 I_brand_update                IN     VARCHAR2, 
                 I_product_classification      IN     ITEM_MASTER.PRODUCT_CLASSIFICATION%TYPE,
                 I_product_class_update        IN     VARCHAR2)
               
RETURN BOOLEAN IS

   L_item           ITEM_MASTER.ITEM%TYPE        := NULL;
   L_pack_ind       SKULIST_DETAIL.PACK_IND%TYPE := NULL;
   L_item_level     ITEM_MASTER.ITEM_LEVEL%TYPE  := NULL;
   L_tran_level     ITEM_MASTER.TRAN_LEVEL%TYPE  := NULL;

   L_reject         BOOLEAN                      := FALSE;
   L_return_reject  BOOLEAN                      := FALSE;

   L_program_name     VARCHAR2(100);
   L_sub_program_name VARCHAR2(100);
   L_table            VARCHAR2(30);

   L_dept_purchase_type        DEPS.PURCHASE_TYPE%TYPE := NULL;
   L_item_type                 VARCHAR2(1) := NULL;
   L_catch_weight_ind_update   VARCHAR2(1) := NULL;

   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);
------------------------------------------------------------------------------
---INTERNAL FUNCTION
------------------------------------------------------------------------------
FUNCTION UPDATE_COST_ZONE(O_error_message IN OUT VARCHAR2,
                          I_item          IN     ITEM_MASTER.ITEM%TYPE,
                          I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN IS
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_orderable_ind ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_sellable_ind  ITEM_MASTER.SELLABLE_IND%TYPE;
   L_pack_type     ITEM_MASTER.PACK_TYPE%TYPE;


   cursor C_ITEMLIST is
      select sd.item,
             sd.item_level,
             sd.tran_level,
             sd.pack_ind,
             im.orderable_ind,
             im.pack_type
        from skulist_detail sd,
             item_master im, 
             item_supp_country isc
       where sd.skulist              = I_itemlist
         and sd.item                 = im.item
         and sd.item                 = isc.item
         and isc.primary_supp_ind    = 'Y'
         and isc.primary_country_ind = 'Y';
------------------------------------------------------------------------------
---INTERNAL FUNCTION FOR UPDATE_COST_ZONE, USED IN PROCESS_ITEM
------------------------------------------------------------------------------
FUNCTION COST_ZONE_VERIFICATION(O_error_message IN OUT VARCHAR2,
                                O_reject        IN OUT BOOLEAN,
                                I_item          IN     ITEM_MASTER.ITEM%TYPE)
return BOOLEAN IS

   L_dummy    VARCHAR2(1);

   cursor C_ORD_ITEM is
      select 'x'
        from ordsku
       where item = I_item;
BEGIN
   L_sub_program_name := '.COST_ZONE_VERIFICATION';

   open C_ORD_ITEM;
   fetch C_ORD_ITEM into L_dummy;
   if C_ORD_ITEM%FOUND then
      if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                I_item,
                                                NULL,
                                                NULL,
                                                'I',
                                                'ITEM_ON_ORDER_ZONE',
                                                user,
                                                NULL,
                                                NULL,
                                                NULL) = FALSE then
         return FALSE;
      end if;
      O_reject := TRUE;
   end if;
   close C_ORD_ITEM;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name||L_sub_program_name,
                                            to_char(SQLCODE));
      RETURN FALSE;
END COST_ZONE_VERIFICATION;
------------------------------------------------------------------------------
---INTERNAL FUNCTION FOR UPDATE_COST_ZONE, USED IN PROCESS_ITEM
------------------------------------------------------------------------------
FUNCTION DELETE_ITEM(O_error_message IN OUT VARCHAR2,
                     I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   cursor C_LOCK_EXP_DETAIL is
      select 'x'
        from item_exp_detail
       where item = I_item
         and item_exp_type = 'Z'
         for update nowait;
   ---
   cursor C_LOCK_EXP_HEAD is
      select 'x'
        from item_exp_head
       where item = I_item
         and item_exp_type = 'Z'
         for update nowait;
   ---
   cursor C_SUPPLIER is
     select supplier
        from item_supplier
      where item= I_item;
BEGIN
   L_sub_program_name := '.DELETE_ITEM';

   L_table := 'ITEM_EXP_DETAIL';
   open C_LOCK_EXP_DETAIL;
   close C_LOCK_EXP_DETAIL;
   delete from item_exp_detail
      where item = I_item
        and item_exp_type = 'Z';
   ---
   L_table := 'ITEM_EXP_HEAD';
   open C_LOCK_EXP_HEAD;
   close C_LOCK_EXP_HEAD;
   delete from item_exp_head
      where item = I_item
        and item_exp_type = 'Z';
   ---
   FOR rec in C_SUPPLIER LOOP
      if ITEM_EXPENSE_SQL.DEFAULT_EXPENSES(O_error_message,
                                           I_item,
                                           rec.supplier,
                                           NULL) = FALSE then
         return FALSE;
      end if;
      if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                'IA',
                                I_item,
                                rec.supplier,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL) = FALSE then
         return FALSE;
      end if;

      if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                'IE',
                                I_item,
                                rec.supplier,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL) = FALSE then
         return FALSE;
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name||L_sub_program_name,
                                            to_char(SQLCODE));
      RETURN FALSE;
END DELETE_ITEM;
------------------------------------------------------------------------------
---INTERNAL FUNCTION FOR UPDATE_COST_ZONE
------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message IN OUT VARCHAR2,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE,
                      I_item_level    IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                      I_tran_level    IN     ITEM_MASTER.TRAN_LEVEL%TYPE)
RETURN BOOLEAN IS
   cursor C_GET_CHILDREN is
      select item
        from item_master
       where (item_parent = L_item
              or item_grandparent = L_item)
         and item_level <= tran_level;

   cursor C_LOCK_ITEM_MASTER is
      select item
        from item_master
       where (item = L_item
              or item_parent = L_item
              or item_grandparent = L_item)
      for update nowait;
BEGIN
   L_sub_program_name := '.PROCESS_ITEM';
   L_item := I_item;

   if I_item_level = 1 then
      if I_item_level < I_tran_level then
         FOR rec in C_GET_CHILDREN LOOP
            if not COST_ZONE_VERIFICATION(O_error_message,
                                          L_reject,
                                          rec.item) then
               return FALSE;
            end if;
            if L_reject = TRUE then
               EXIT;
            end if;
         END LOOP;
      end if;
      if not L_reject then
         if not COST_ZONE_VERIFICATION(O_error_message,
                                       L_reject,
                                       L_item) then
            return FALSE;
         end if;
      end if;

      if not L_reject then
         L_table := 'ITEM_MASTER';
         open  C_LOCK_ITEM_MASTER;
         close C_LOCK_ITEM_MASTER;

         update item_master
            set cost_zone_group_id = I_cost_zone_group_id
          where (item = L_item
                 or item_parent = L_item
                 or item_grandparent = L_item);

         if not DELETE_ITEM(O_error_message,
                            L_item) then
            return FALSE;
         end if;
         if I_item_level < I_tran_level then
            FOR rec in C_GET_CHILDREN LOOP
               if not DELETE_ITEM(O_error_message,
                                  rec.item) then
                  return FALSE;
               end if;
            END LOOP;
         end if;
      else
         O_reject := TRUE;
      end if;
   else
      if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                L_item,
                                                NULL,
                                                NULL,
                                                'I',
                                                'NO_UPDATE_CHILD_COST_ZONE',
                                                user,
                                                NULL,
                                                NULL,
                                                NULL) = FALSE then
         return FALSE;
      end if;
      O_reject := TRUE;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name||L_sub_program_name,
                                            to_char(SQLCODE));
      RETURN FALSE;
END PROCESS_ITEM;
------------------------------------------------------------------------------
--- BEGIN UPDATE_COST_ZONE FUNCTION
------------------------------------------------------------------------------
BEGIN
   L_sub_program_name := '.UPDATE_COST_ZONE';
   L_item := NULL;

   if I_itemlist is not NULL and I_item is NULL then
      FOR rec in C_ITEMLIST LOOP
         L_item          := rec.item;
         L_pack_ind      := rec.pack_ind;
         L_orderable_ind := rec.orderable_ind;
         L_pack_type     := rec.pack_type;
         L_item_level    := rec.item_level;
         L_tran_level    := rec.tran_level;
         L_reject        := FALSE;
         if L_pack_ind = 'N' or
          (L_pack_ind = 'Y' and L_orderable_ind = 'Y' and L_pack_type = 'V') then
            if not PROCESS_ITEM(O_error_message,
                                L_item,
                                L_item_level,
                                L_tran_level) then
               return FALSE;
            end if;
         end if;
      END LOOP;
   else
      if not ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                           L_pack_ind,
                                           L_sellable_ind,
                                           L_orderable_ind,
                                           L_pack_type,
                                           I_item) then
         return FALSE;
      end if;
      ---
      if L_pack_ind = 'N' or
       (L_pack_ind = 'Y' and L_orderable_ind = 'Y' and L_pack_type = 'V') then
         ---
         if not ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                           L_item_level,
                                           L_tran_level,
                                           I_item) then
            return FALSE;
         end if;
         ---
         if not PROCESS_ITEM(O_error_message,
                             I_item,
                             L_item_level,
                             L_tran_level) then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name||L_sub_program_name,
                                            to_char(SQLCODE));
      RETURN FALSE;
END UPDATE_COST_ZONE;
------------------------------------------------------------------------------
---INTERNAL FUNCTION
------------------------------------------------------------------------------
FUNCTION UPDATE_OTHERS(O_error_message IN OUT VARCHAR2,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                       I_itemlist      IN     SKULIST_HEAD.SKULIST%TYPE)
return BOOLEAN IS
   L_dept                  DEPS.DEPT%TYPE         := NULL;
   L_class                 CLASS.CLASS%TYPE       := NULL;
   L_subclass              SUBCLASS.SUBCLASS%TYPE := NULL;

   cursor C_ITEMLIST_AND_CHILDREN is
      select distinct(im.item) item,
             im.item_level,
             im.tran_level,
             im.pack_ind,
             im.dept,
             im.class,
             im.subclass,
             im.orderable_ind,
             im.sellable_ind,
             im.inventory_ind,
             im.deposit_item_type,
             im.status
        from item_master im,
             skulist_detail sd
       where sd.skulist = I_itemlist
         and (im.item = sd.item
              or im.item_parent = sd.item
              or im.item_grandparent = sd.item);

   cursor C_GET_CHILDREN_PARENT is
      select item,
             item_level,
             tran_level,
             pack_ind,
             dept,
             class,
             subclass
        from item_master
       where (item = I_item
              or item_parent = I_item
              or item_grandparent = I_item);

   cursor C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where item = L_item
          or (item_level > tran_level
             and item_parent = L_item)
         for update nowait;

------------------------------------------------------------------------------
--- INTERNAL FUNCTION FOR UPDATE_OTHERS
------------------------------------------------------------------------------
FUNCTION FORECAST_VERIFICATION(O_error_message IN OUT VARCHAR2,
                               O_reject        IN OUT BOOLEAN,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_forecast_repl_method  BOOLEAN                := FALSE;
   L_forecast_sub_items    BOOLEAN                := FALSE;
   L_forecast_main_items   BOOLEAN                := FALSE;
   L_domain_exists         BOOLEAN                := FALSE;

BEGIN
   L_sub_program_name := '.FORECAST_VERIFICATION';

   if I_forecast_ind = 'N' then
       if REPLENISHMENT_SQL.FORECAST_REPL_METHOD(O_error_message,
                                                 I_item,
                                                 L_forecast_repl_method)= FALSE then
         return FALSE;
      end if;
      if L_forecast_repl_method = TRUE then
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_item,
                                                   NULL,
                                                   NULL,
                                                   'I',
                                                   'FORECAST_REPL_METHOD',
                                                   user,
                                                   NULL,
                                                   NULL,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         O_reject := TRUE;
         return TRUE;
      end if;
      ---
      if FORECASTS_SQL.FORECAST_MAIN_ITEMS(O_error_message,
                                           I_item,
                                           L_forecast_main_items)= FALSE then
         return FALSE;
      end if;
      ---
      if L_forecast_main_items = TRUE then
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_item,
                                                   NULL,
                                                   NULL,
                                                   'I',
                                                   'SUB_ITEM_FORECASTS',
                                                   user,
                                                   NULL,
                                                   NULL,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         O_reject := TRUE;
         return TRUE;
      end if;
      ---
      if FORECASTS_SQL.FORECAST_SUB_ITEMS(O_error_message,
                                          I_item,
                                          L_forecast_sub_items) = FALSE then
         return FALSE;
      end if;
      ---
      if L_forecast_sub_items = TRUE then
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_item,
                                                   NULL,
                                                   NULL,
                                                   'I',
                                                   'MAIN_ITEM_FORECASTS',
                                                   user,
                                                   NULL,
                                                   NULL,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         O_reject := TRUE;
         return TRUE;
      end if;
   else  --- I_forecast_ind = 'Y'
      if FORECASTS_SQL.DOMAIN_EXISTS(O_error_message,
                                     L_dept,
                                     L_class,
                                     L_subclass,
                                     L_domain_exists)= FALSE then
         return FALSE;
      end if;
      ---
      if L_domain_exists = FALSE then
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_item,
                                                   NULL,
                                                   NULL,
                                                   'I',
                                                   'NO_DOMAIN_EXISTS',
                                                   user,
                                                   NULL,
                                                   NULL,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         O_reject := TRUE;
         return TRUE;
      end if;
      if L_pack_ind = 'Y' then
         if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                   I_item,
                                                   NULL,
                                                   NULL,
                                                   'I',
                                                   'PACK_FORECAST',
                                                   user,
                                                   NULL,
                                                   NULL,
                                                   NULL) = FALSE then
            return FALSE;
         end if;
         O_reject := TRUE;
         return TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name||L_sub_program_name,
                                            to_char(SQLCODE));
      RETURN FALSE;
END FORECAST_VERIFICATION;
------------------------------------------------------------------------------
--- BEGIN UPDATE_OTHERS FUNCTION
------------------------------------------------------------------------------
BEGIN
   L_sub_program_name  := '.UPDATE_OTHERS';
   L_item              := NULL;

   if I_itemlist is not NULL and I_item is NULL then
      FOR rec in C_ITEMLIST_AND_CHILDREN LOOP
         L_item        := rec.item;
         L_item_level  := rec.item_level;
         L_tran_level  := rec.tran_level;
         L_pack_ind    := rec.pack_ind;
         L_dept        := rec.dept;
         L_class       := rec.class;
         L_subclass    := rec.subclass;
         L_reject      := FALSE;

         if I_forecast_ind_update = 'Y' then
            if not FORECAST_VERIFICATION(O_error_message,
                                         L_reject,
                                         L_item) then
               return FALSE;
            end if;
         end if;

         ---
         if not L_reject then
            --verifying items in itemlist if valid for catch weight.
            if I_catch_weight_ind_update = 'Y' then
               if not DEPT_ATTRIB_SQL.GET_PURCHASE_TYPE(O_error_message,
                                                        L_dept_purchase_type,
                                                        L_dept) then
                  return FALSE;
               end if;
               if not ITEM_ATTRIB_SQL.GET_ITEM_TYPE(O_error_message,
                                                    L_item_type,
                                                    L_item) then
                  return FALSE;
               end if;
               if rec.orderable_ind = 'Y' and
                  rec.inventory_ind = 'Y' and
                  rec.deposit_item_type is NULL and
                  L_dept_purchase_type = 0 and
                  rec.status not in ('S','A') and
                  L_item_type != 'C' then
                     L_catch_weight_ind_update := I_catch_weight_ind_update;
               else
                  L_catch_weight_ind_update := 'N';
               end if;
               if rec.sellable_ind = 'Y' and rec.orderable_ind = 'N' then
                  L_catch_weight_ind_update := 'N';
               end if;
            end if;
            ---
            L_table := 'ITEM_MASTER';
            open  C_LOCK_ITEM_MASTER;
            close C_LOCK_ITEM_MASTER;
            ---
              update item_master
               set merchandise_ind          = DECODE(I_merchandise_ind_update,'Y', I_merchandise_ind , merchandise_ind ),
                   forecast_ind             = DECODE(L_pack_ind, 'N', DECODE(I_forecast_ind_update, 'Y', I_forecast_ind ,forecast_ind), forecast_ind),
                   retail_label_type        = DECODE(I_retail_label_type_update,'Y',I_retail_label_type,  retail_label_type),
                   retail_label_value       = DECODE(I_retail_label_type_update ,'Y',I_retail_label_value ,retail_label_value),
                   handling_temp            = DECODE(I_handling_temp_update ,'Y', I_handling_temp  ,handling_temp),
                   handling_sensitivity     = DECODE(I_handling_sensitivity_update,'Y',I_handling_sensitivity ,handling_sensitivity),
                   catch_weight_ind         = DECODE(L_catch_weight_ind_update,'Y',I_catch_weight_ind, catch_weight_ind),
                   waste_type               = DECODE(I_waste_type_update,'Y', I_waste_type ,waste_type),
                   waste_pct                = DECODE(I_waste_type_update ,'Y', I_waste_pct,waste_pct),
                   default_waste_pct        = DECODE(I_waste_type_update ,'Y', I_default_waste_pct,default_waste_pct),
                   package_size             = DECODE(I_package_size_update, 'Y',I_package_size, package_size),
                   package_uom              = DECODE(I_package_size_update, 'Y',I_package_uom, package_uom),
                   comments                 = DECODE(I_comments_update, 'Y', I_comments,  comments),
                   perishable_ind           = DECODE(I_perishable_ind_update, 'Y', I_perishable_ind, perishable_ind),
                   brand_name               = DECODE(I_brand_update ,'Y', I_brand_name, brand_name),
                   product_classification   = DECODE(I_product_class_update,'Y', I_product_classification, product_classification)
             where item = L_item
                or (item_level > tran_level
                    and item_parent = L_item);

         else
            O_reject := TRUE;
         end if;
      END LOOP;
   else
   
      for rec in C_GET_CHILDREN_PARENT LOOP
         L_item        := rec.item;
         L_item_level  := rec.item_level;
         L_tran_level  := rec.tran_level;
         L_pack_ind    := rec.pack_ind;
         L_dept        := rec.dept;
         L_class       := rec.class;
         L_subclass    := rec.subclass;
         L_reject          := FALSE;

         if I_forecast_ind_update = 'Y' then
            if not FORECAST_VERIFICATION(O_error_message,
                                         L_reject,
                                         L_item) then
               return FALSE;
            end if;
         end if;
         ---
         if not L_reject then
            L_table := 'ITEM_MASTER';
            open  C_LOCK_ITEM_MASTER;
            close C_LOCK_ITEM_MASTER;
            ---
              update item_master
               set merchandise_ind              = DECODE(I_merchandise_ind_update,'Y', I_merchandise_ind , merchandise_ind ),
                   forecast_ind                 = DECODE(L_pack_ind, 'N', DECODE(I_forecast_ind_update, 'Y', I_forecast_ind ,forecast_ind), forecast_ind),
                   retail_label_type            = DECODE(I_retail_label_type_update,'Y',I_retail_label_type,  retail_label_type),
                   retail_label_value           = DECODE(I_retail_label_type_update ,'Y',I_retail_label_value ,retail_label_value),
                   handling_temp                = DECODE(I_handling_temp_update ,'Y', I_handling_temp  ,handling_temp),
                   handling_sensitivity         = DECODE(I_handling_sensitivity_update,'Y',I_handling_sensitivity ,handling_sensitivity),
                   catch_weight_ind             = DECODE(I_catch_weight_ind_update,'Y',I_catch_weight_ind, catch_weight_ind),
                   waste_type                   = DECODE(I_waste_type_update,'Y', I_waste_type ,waste_type),
                   waste_pct                    = DECODE(I_waste_type_update ,'Y', I_waste_pct,waste_pct),
                   default_waste_pct            = DECODE(I_waste_type_update ,'Y', I_default_waste_pct,default_waste_pct),
                   package_size                 = DECODE(I_package_size_update, 'Y',I_package_size, package_size),
                   package_uom                  = DECODE(I_package_size_update, 'Y',I_package_uom, package_uom),
                   comments                     = DECODE(I_comments_update, 'Y', I_comments,  comments),
                   perishable_ind               = DECODE(I_perishable_ind_update, 'Y', I_perishable_ind, perishable_ind),
                   brand_name                   = DECODE(I_brand_update, 'Y', I_brand_name, brand_name),
                   product_classification       = DECODE(I_product_class_update, 'Y', I_product_classification, product_classification)
             where item = L_item
                or (item_level > tran_level
                    and item_parent = L_item);
         else
            O_reject := TRUE;
         end if;
      END LOOP;
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_item,
                                            NULL);
      if C_ITEMLIST_AND_CHILDREN%ISOPEN then
         close C_ITEMLIST_AND_CHILDREN;
      end if;
      
      if C_GET_CHILDREN_PARENT%ISOPEN then
         close C_GET_CHILDREN_PARENT;
      end if;
      
      if C_LOCK_ITEM_MASTER%ISOPEN then
         close C_LOCK_ITEM_MASTER;
      end if;
      
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program_name||L_sub_program_name,
                                            to_char(SQLCODE));
      
      if C_ITEMLIST_AND_CHILDREN%ISOPEN then
         close C_ITEMLIST_AND_CHILDREN;
      end if;
      
      if C_GET_CHILDREN_PARENT%ISOPEN then
         close C_GET_CHILDREN_PARENT;
      end if;
      
      if C_LOCK_ITEM_MASTER%ISOPEN then
         close C_LOCK_ITEM_MASTER;
      end if;
      
      RETURN FALSE;
END UPDATE_OTHERS;

--------------------------------------------------------------------------
---
--- BEGIN MAIN FUNCTION MC_IND
---
--------------------------------------------------------------------------
BEGIN
   L_program_name := 'MC_ITEM_IND.MC_IND';

   if (I_item IS NULL and I_itemlist IS NULL)
    or (I_item IS NOT NULL and I_itemlist IS NOT NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            I_item || ' '||I_itemlist,
                                            'NULL or NOT NULL',
                                            NULL);
      return FALSE;
   end if;

   if I_cost_zone_group_id_update = 'Y' then
      if not UPDATE_COST_ZONE(O_error_message,
                              I_item,
                              I_itemlist) then
         return FALSE;
      end if;
   end if;
   if I_merchandise_ind_update      = 'Y' or
      I_forecast_ind_update         = 'Y' or
      I_retail_label_type_update    = 'Y' or
      I_handling_temp_update        = 'Y' or
      I_handling_sensitivity_update = 'Y' or
      I_catch_weight_ind_update     = 'Y' or
      I_waste_type_update           = 'Y' or
      I_package_size_update         = 'Y' or
      I_comments_update             = 'Y' or
      I_perishable_ind_update       = 'Y' or
      I_brand_update                = 'Y' or 
      I_product_class_update        = 'Y' then
      if not UPDATE_OTHERS(O_error_message,
                           I_item,
                           I_itemlist) then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program_name,
                                             to_char(SQLCODE));
      RETURN FALSE;
END MC_IND;
END MC_ITEM_IND;
/
