
CREATE OR REPLACE PACKAGE BODY SOURCE_DLVRY_SCHED_SQL AS

-----------------------------------------------------------------------
FUNCTION APPLY_SUPLOC_ITEMLIST(O_error_message IN OUT VARCHAR2,
                               O_not_exists    IN OUT BOOLEAN,
                               I_source        IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                               I_source_type   IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                               I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                               I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                               I_day           IN     SOURCE_DLVRY_SCHED_EXC.DAY%TYPE,
                               I_item_list     IN     SKULIST_HEAD.SKULIST%TYPE)
   return BOOLEAN IS

   L_exists             VARCHAR2(1);
   L_sku                item_master.item%TYPE;
   L_loc_exists           BOOLEAN;
   
   cursor C_ITEMLIST_SUPP_EXIST is
      select 'x'
        from skulist_detail skl
       where skl.skulist = I_item_list
         and (    (    I_source_type = 'SUP'
                   and exists (select 'x'
                                 from item_supplier
                                where supplier = I_source
                                  and item     = skl.item))
              or   (   I_source_type = 'W'
                    and exists (select 'x'
                                  from item_loc il,
                                       wh w
                                 where il.item     = skl.item
                                   and il.loc      = w.wh
                                   and il.loc_type = 'W'
                                   and physical_wh = I_source)))
        and exists ( select 'x'
                        from item_loc il2
                       where il2.item = skl.item
                         and (il2.loc = I_location
                          or exists (select wh
                                       from wh
                                      where physical_wh = I_location
                                        and il2.loc     = wh)));
   cursor C_ITEM_LIST is
      select distinct skl.item
        from skulist_detail skl
       where skl.skulist  = I_item_list
         and NOT exists ( select 'x'
                            from source_dlvry_sched_exc sls
                           where sls.item = skl.item )
         and (    (    I_source_type = 'SUP'
                   and exists (select item
                                 from item_supplier
                                where supplier = I_source
                                  and item     = skl.item))
              or  (    I_source_type = 'W'
                   and exists (select 'x'
                                  from item_loc il,
                                       wh w
                                 where il.item     = skl.item
                                   and il.loc      = w.wh
                                   and il.loc_type = 'W'
                                   and physical_wh = I_source)))
         and exists ( select 'x'
                        from item_loc il2
                       where il2.item = skl.item
                         and (il2.loc  = I_location
                          or exists (select wh
                                       from wh
                                      where physical_wh = I_location
                                        and il2.loc     = wh)));
BEGIN
   O_not_exists := FALSE;
   ---
      SQL_LIB.SET_MARK('OPEN','C_ITEMLIST_SUPP_EXIST','ITEM_SUPPLIER, SKULIST_DETAIL',
                       'source: '||to_char(I_source)||', skulist: '||(I_item_list));
      open C_ITEMLIST_SUPP_EXIST;

      SQL_LIB.SET_MARK('FETCH','C_ITEMLIST_SUPP_EXIST','ITEM_SUPPLIER, SKULIST_DETAIL',
                       'source: '||to_char(I_source)||', skulist: '||(I_item_list));
      fetch C_ITEMLIST_SUPP_EXIST into L_exists;

      if C_ITEMLIST_SUPP_EXIST%NOTFOUND then
         O_not_exists := TRUE;
      end if;
           
      SQL_LIB.SET_MARK('CLOSE','C_ITEMLIST_SUPP_EXIST','ITEM_SUPPLIER, SKULIST_DETAIL',
                       'source: '||to_char(I_source)||', skulist: '||(I_item_list));
      close C_ITEMLIST_SUPP_EXIST;

   if O_not_exists = FALSE then
               
      for C_item_list_rec in C_item_list LOOP
         L_sku := C_item_list_rec.item;
           
            insert into source_dlvry_sched_exc(source,
                                               source_type,
                                               location,
                                               loc_type,
                                               day,
                                               item)
                                       values (I_source,
                                               I_source_type,
                                               I_location,
                                               I_loc_type,
                                               I_day,
                                               L_sku);
      end LOOP;
              
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SOURCE_DLVRY_SCHED.APPLY_SUPLOC_ITEMLIST',
                                             to_char(SQLCODE));
    return FALSE;
END APPLY_SUPLOC_ITEMLIST;
----------------------------------------------------------------------
FUNCTION DELETE_SUPLOC_ITEMLIST (O_error_message   IN OUT   VARCHAR2,
                                 I_source          IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                                 I_source_type     IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                                 I_location        IN       SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                                 I_loc_type        IN       SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                                 I_day             IN       SOURCE_DLVRY_SCHED_EXC.DAY%TYPE,
                                 I_item_list       IN       SOURCE_DLVRY_SCHED_EXC.ITEM%TYPE)
   return BOOLEAN IS

   L_table              VARCHAR2(30);
   L_item               VARCHAR2(80);
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SOURCE_DLVRY_SCHED_EXC is
      select 'x'
        from source_dlvry_sched_exc
       where source = I_source
         and source_type = I_source_type
         and location = I_location
         and loc_type = I_loc_type
         and day = I_day
         and exists (select 'x'
                       from skulist_detail
                      where skulist = I_item_list
                        and item = source_dlvry_sched_exc.item)
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', day: '||to_char(I_day)||', skulist: '||(I_item_list));
   open C_LOCK_SOURCE_DLVRY_SCHED_EXC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', day: '||to_char(I_day)||', skulist: '||(I_item_list));
   close C_LOCK_SOURCE_DLVRY_SCHED_EXC;

   L_table := 'SOURCE_DLVRY_SCHED_EXC';
   L_item  := 'source: '||to_char(I_source)||', location: '||to_char(I_location)||
              ', day: '||to_char(I_day)||', skulist: '||(I_item_list);

   SQL_LIB.SET_MARK('DELETE',NULL,'SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', day: '||to_char(I_day)||', skulist: '||(I_item_list));
   delete from SOURCE_DLVRY_SCHED_exc
    where source = I_source
      and source_type = I_source_type
      and location = I_location
      and loc_type = I_loc_type
      and day      = I_day
      and exists (select 'x'
                    from skulist_detail
                   where skulist = I_item_list
                     and item     = source_dlvry_sched_exc.item);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SOURCE_DLVRY_SCHED_EXC%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                          'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                          ', day: '||to_char(I_day)||', skulist: '||(I_item_list));
         close C_LOCK_SOURCE_DLVRY_SCHED_EXC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SOURCE_DLVRY_SCHED.DELETE_SUPLOC_ITEMLIST',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_SUPLOC_ITEMLIST;
-------------------------------------------------------------------------
FUNCTION DELETE_SCHEDULE (O_error_message   IN OUT   VARCHAR2,
                          I_source          IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                          I_source_type     IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                          I_location        IN       SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                          I_loc_type        IN       SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE)
   return BOOLEAN IS

   L_table         VARCHAR2(30);
   L_item          VARCHAR2(60);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SOURCE_DLVRY_SCHED_EXC is
      select 'x'
        from SOURCE_DLVRY_SCHED_exc
       where source = I_source
         and source_type = I_source_type
         and location = I_location
         and loc_type = I_loc_type
         for update nowait;

   cursor C_LOCK_SOURCE_DLVRY_SCHED_DAYS is
      select 'x'
        from SOURCE_DLVRY_SCHED_days
       where source = I_source
         and source_type = I_source_type
         and location = I_location
         and loc_type = I_loc_type
         for update nowait;

   cursor C_LOCK_SOURCE_DLVRY_SCHED is
      select 'x'
        from SOURCE_DLVRY_SCHED
       where source = I_source
         and source_type = I_source_type
         and location = I_location
         and loc_type = I_loc_type
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   open C_LOCK_SOURCE_DLVRY_SCHED_EXC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   close C_LOCK_SOURCE_DLVRY_SCHED_EXC;

   L_table := 'SOURCE_DLVRY_SCHED_EXC';
   L_item  := 'source: '||to_char(I_source)||', location: '||to_char(I_location)||
              ', loc_type: '||I_loc_type;

   SQL_LIB.SET_MARK('DELETE',NULL,'SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   delete from SOURCE_DLVRY_SCHED_exc
         where source = I_source
           and source_type = I_source_type
           and location = I_location
           and loc_type = I_loc_type;


   SQL_LIB.SET_MARK('OPEN','C_LOCK_SOURCE_DLVRY_SCHED_DAYS','SOURCE_DLVRY_SCHED_DAYS',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   open C_LOCK_SOURCE_DLVRY_SCHED_DAYS;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_DAYS','SOURCE_DLVRY_SCHED_DAYS',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   close C_LOCK_SOURCE_DLVRY_SCHED_DAYS;

   L_table := 'SOURCE_DLVRY_SCHED_DAYS';
   L_item  := 'source: '||to_char(I_source)||', location: '||to_char(I_location)||
              ', loc_type: '||I_loc_type;

   SQL_LIB.SET_MARK('DELETE',NULL,'SOURCE_DLVRY_SCHED_DAYS',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   delete from SOURCE_DLVRY_SCHED_days
         where source = I_source
           and source_type = I_source_type
           and location = I_location
           and loc_type = I_loc_type;


   SQL_LIB.SET_MARK('OPEN','C_LOCK_SOURCE_DLVRY_SCHED','SOURCE_DLVRY_SCHED',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   open C_LOCK_SOURCE_DLVRY_SCHED;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED','SOURCE_DLVRY_SCHED',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   close C_LOCK_SOURCE_DLVRY_SCHED;

   L_table := 'SOURCE_DLVRY_SCHED';
   L_item  := 'source: '||to_char(I_source)||', location: '||to_char(I_location)||
              ', loc_type: '||I_loc_type;

   SQL_LIB.SET_MARK('DELETE',NULL,'SOURCE_DLVRY_SCHED',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   delete from SOURCE_DLVRY_SCHED
         where source = I_source
           and source_type = I_source_type
           and location = I_location
           and loc_type = I_loc_type;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SOURCE_DLVRY_SCHED_EXC%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                          'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                          ', loc_type: '||I_loc_type);
         close C_LOCK_SOURCE_DLVRY_SCHED_EXC;
      end if;
      if C_LOCK_SOURCE_DLVRY_SCHED_DAYS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_DAYS','SOURCE_DLVRY_SCHED_DAYS',
                          'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                          ', loc_type: '||I_loc_type);
         close C_LOCK_SOURCE_DLVRY_SCHED_DAYS;
      end if;
      if C_LOCK_SOURCE_DLVRY_SCHED%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED','SOURCE_DLVRY_SCHED',
                          'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                          ', loc_type: '||I_loc_type);
         close C_LOCK_SOURCE_DLVRY_SCHED;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SOURCE_DLVRY_SCHED_SQL.DELETE_SCHEDULE',
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_SCHEDULE;
--------------------------------------------------------------------------------
FUNCTION DELETE_EXC_RECORDS (O_error_message   IN OUT   VARCHAR2,
                             I_source          IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                             I_source_type     IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                             I_location        IN       SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                             I_loc_type        IN       SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                             I_day             IN       SOURCE_DLVRY_SCHED_EXC.DAY%TYPE)
   return BOOLEAN is

   L_table         VARCHAR2(30);
   L_item          VARCHAR2(60);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SOURCE_DLVRY_SCHED_EXC is
     select 'x'
       from SOURCE_DLVRY_SCHED_exc
      where source = I_source
        and source_type = I_source_type
        and location = I_location
        and loc_type = I_loc_type
        and day = I_day
        for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', day: '||to_char(I_day));
   open C_LOCK_SOURCE_DLVRY_SCHED_EXC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', day: '||to_char(I_day));
   close C_LOCK_SOURCE_DLVRY_SCHED_EXC;

   L_table := 'SOURCE_DLVRY_SCHED_EXC';
   L_item  := 'source: '||to_char(I_source)||', location: '||to_char(I_location)||
              ', day: '||to_char(I_day);

   SQL_LIB.SET_MARK('DELETE',NULL,'SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', day: '||to_char(I_day));
   delete from SOURCE_DLVRY_SCHED_EXC
    where source = I_source
      and source_type = I_source_type
      and location = I_location
      and loc_type = I_loc_type
      and day      = I_day;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SOURCE_DLVRY_SCHED_EXC%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                          'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                          ', day: '||to_char(I_day));
         close C_LOCK_SOURCE_DLVRY_SCHED_EXC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SOURCE_DLVRY_SCHED_SQL.DELETE_EXC_RECORDS',
                                             to_char(SQLCODE));
   return FALSE;
END DELETE_EXC_RECORDS;
----------------------------------------------------------------------------------
FUNCTION DELETE_EXC_AND_DAYS_RECORDS (O_error_message   IN OUT   VARCHAR2,
                                      I_source          IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                                      I_source_type     IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                                      I_location        IN       SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                                      I_loc_type        IN       SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE)
   return BOOLEAN is

   L_table         VARCHAR2(30);
   L_item          VARCHAR2(60);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SOURCE_DLVRY_SCHED_EXC is
      select 'x'
        from SOURCE_DLVRY_SCHED_exc
       where source = I_source
         and source_type = I_source_type
         and location = I_location
         and loc_type = I_loc_type
         for update nowait;

   cursor C_LOCK_SOURCE_DLVRY_SCHED_DAYS is
      select 'x'
        from SOURCE_DLVRY_SCHED_days
       where source = I_source
         and source_type = I_source_type
         and location = I_location
         and loc_type = I_loc_type
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   open C_LOCK_SOURCE_DLVRY_SCHED_EXC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   close C_LOCK_SOURCE_DLVRY_SCHED_EXC;

   L_table := 'SOURCE_DLVRY_SCHED_EXC';
   L_item  := 'source: '||to_char(I_source)||', location: '||to_char(I_location)||
              ', loc_type: '||I_loc_type;

   SQL_LIB.SET_MARK('DELETE',NULL,'SOURCE_DLVRY_SCHED_EXC',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   delete from SOURCE_DLVRY_SCHED_EXC
    where source = I_source
      and source_type = I_source_type
      and location = I_location
      and loc_type = I_loc_type;


   SQL_LIB.SET_MARK('OPEN','C_LOCK_SOURCE_DLVRY_SCHED_DAYS','SOURCE_DLVRY_SCHED_DAYS',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   open C_LOCK_SOURCE_DLVRY_SCHED_DAYS;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_DAYS','SOURCE_DLVRY_SCHED_DAYS',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   close C_LOCK_SOURCE_DLVRY_SCHED_DAYS;

   L_table := 'SOURCE_DLVRY_SCHED_DAYS';
   L_item  := 'source: '||to_char(I_source)||', location: '||to_char(I_location)||
              ', loc_type: '||I_loc_type;

   SQL_LIB.SET_MARK('DELETE',NULL,'SOURCE_DLVRY_SCHED_DAYS',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                    ', loc_type: '||I_loc_type);
   delete from SOURCE_DLVRY_SCHED_DAYS
    where source = I_source
      and source_type = I_source_type
      and location = I_location
      and loc_type = I_loc_type;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SOURCE_DLVRY_SCHED_EXC%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_EXC','SOURCE_DLVRY_SCHED_EXC',
                          'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                          ', loc_type: '||I_loc_type);
         close C_LOCK_SOURCE_DLVRY_SCHED_EXC;
      end if;
      if C_LOCK_SOURCE_DLVRY_SCHED_DAYS%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SOURCE_DLVRY_SCHED_DAYS','SOURCE_DLVRY_SCHED_DAYS',
                          'source: '||to_char(I_source)||', location: '||to_char(I_location)||
                          ', loc_type: '||I_loc_type);
         close C_LOCK_SOURCE_DLVRY_SCHED_DAYS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SOURCE_DLVRY_SCHED_SQL.DELETE_EXC_AND_DAYS_RECORDS',
                                            to_char(SQLCODE));
   return FALSE;
END DELETE_EXC_AND_DAYS_RECORDS;
-------------------------------------------------------------------------------------
FUNCTION CHECK_DAY_EXIST (O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          I_source          IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                          I_source_type     IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                          I_location        IN       SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                          I_loc_type        IN       SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                          I_day             IN       SOURCE_DLVRY_SCHED_EXC.DAY%TYPE)
   return BOOLEAN IS
      L_exists      VARCHAR2(1);

      cursor C_DAY_EXIST is
         select 'Y'
           from SOURCE_DLVRY_SCHED_days
          where source = I_source
            and source_type = I_source_type
            and location      = I_location
            and loc_type      = I_loc_type
            and day           = I_day;
 BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_DAY_EXIST','SOURCE_DLVRY_SCHED_DAYS',NULL);
   open C_DAY_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_DAY_EXIST','SOURCE_DLVRY_SCHED_DAYS',NULL);
   fetch C_DAY_EXIST into L_exists;

   if C_DAY_EXIST%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_DAY',NULL,NULL,NULL);
      O_exists        := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_DAY_EXIST','SOURCE_DLVRY_SCHED_DAYS',NULL);
   close C_DAY_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SOURCE_DLVRY_SCHED_SQL.DAY_EXIST',
                                             to_char(SQLCODE));
   return FALSE;
END CHECK_DAY_EXIST;
----------------------------------------------------------------------------
FUNCTION CHECK_EXC_DAY_EXIST (O_error_message   IN OUT   VARCHAR2,
                              O_exists          IN OUT   BOOLEAN,
                              I_source          IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                              I_source_type     IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                              I_location        IN       SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                              I_loc_type        IN       SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                              I_day             IN       SOURCE_DLVRY_SCHED_EXC.DAY%TYPE,
                              I_item            IN       SOURCE_DLVRY_SCHED_EXC.ITEM%TYPE)
   return BOOLEAN IS
      L_exists      VARCHAR2(1);

      cursor C_EXC_DAY_EXIST is
         select 'Y'
           from SOURCE_DLVRY_SCHED_exc
          where source = I_source
            and source_type = I_source_type
            and location      = I_location
            and loc_type      = I_loc_type
            and day           = I_day
            and item          = I_item;
 BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN','C_EXC_DAY_EXIST','SOURCE_DLVRY_SCHED_EXC',NULL);
   open C_EXC_DAY_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_EXC_DAY_EXIST','SOURCE_DLVRY_SCHED_EXC',NULL);
   fetch C_EXC_DAY_EXIST into L_exists;

   if C_EXC_DAY_EXIST%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_DAY',NULL,NULL,NULL);
      O_exists        := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_EXC_DAY_EXIST','SOURCE_DLVRY_SCHED_EXC',NULL);
   close C_EXC_DAY_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SOURCE_DLVRY_SCHED_SQL.EXC_DAY_EXIST',
                                             to_char(SQLCODE));
   return FALSE;
END CHECK_EXC_DAY_EXIST;
--------------------------------------------------------------------------------------

FUNCTION INSERT_DAY_SOURCE(O_error_message  IN OUT  VARCHAR2,
                           I_source         IN      SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                           I_source_type    IN      SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                           I_location       IN      repl_day.location%TYPE,
                           I_loc_type       IN      repl_day.loc_type%TYPE,
                           I_start_date     IN      SOURCE_DLVRY_SCHED.start_date%TYPE,
                           I_start_time     IN      SOURCE_DLVRY_SCHED_days.start_time%TYPE,
                           I_end_time       IN      SOURCE_DLVRY_SCHED_days.end_time%TYPE,
                           I_delivery_cycle IN      SOURCE_DLVRY_SCHED.delivery_cycle%TYPE,
                           I_SUN            IN      VARCHAR2,
                           I_MON            IN      VARCHAR2,
                           I_TUE            IN      VARCHAR2,
                           I_WED            IN      VARCHAR2,
                           I_THU            IN      VARCHAR2,
                           I_FRI            IN      VARCHAR2,
                           I_SAT            IN      VARCHAR2,
                           I_action         IN      VARCHAR2)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'SOURCE_DLVRY_SCHED_SQL.INSERT_DAY_SOURCE';
   L_offset        SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE;
   L_sun           NUMBER(2,0);
   L_mon           NUMBER(2,0);
   L_tue           NUMBER(2,0);
   L_wed           NUMBER(2,0);
   L_thu           NUMBER(2,0);
   L_fri           NUMBER(2,0);
   L_sat           NUMBER(2,0);
   ---
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   ---
   cursor C_LOCK_SOURCE_DLVRY_SCHED_EXC is
      select 'X'
        from source_dlvry_sched_exc
       where source      = I_source
         and source_type = I_source_type
         and location    = I_location
         and loc_type    = I_loc_type
         and ((day = L_sun and I_sun != 'Y') or
              (day = L_mon and I_mon != 'Y') or
              (day = L_tue and I_tue != 'Y') or
              (day = L_wed and I_wed != 'Y') or
              (day = L_thu and I_thu != 'Y') or
              (day = L_fri and I_fri != 'Y') or
              (day = L_sat and I_sat != 'Y') or
              (I_action = 'D'))
         for update nowait;
   ---
   cursor C_LOCK_SOURCE_DLVRY_SCHED_DAYS is
      select 'X'
        from source_dlvry_sched_days
       where source      = I_source
         and source_type = I_source_type
         and location    = I_location
         and loc_type    = I_loc_type
         for update nowait;
   ---
   cursor C_LOCK_SOURCE_DLVRY_SCHED is
      select 'X'
        from source_dlvry_sched
       where source      = I_source
         and source_type = I_source_type
         and location    = I_location
         and loc_type    = I_loc_type
         for update nowait;

BEGIN
   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE then
      return FALSE;
   end if;
   L_sun := L_offset + 1;
   L_mon := L_offset + 2;
   if L_mon > 7 then 
      L_mon := L_mon - 7;
   end if;
   L_tue := L_offset + 3;
   if L_tue > 7 then 
      L_tue := L_tue - 7;
   end if;
   L_wed := L_offset + 4;
   if L_wed > 7 then 
      L_wed := L_wed - 7;
   end if;
   L_thu := L_offset + 5;
   if L_thu > 7 then 
      L_thu := L_thu - 7;
   end if;
   L_fri := L_offset + 6;
   if L_fri > 7 then 
      L_fri := L_fri - 7;
   end if;
   L_sat := L_offset + 7;
   if L_sat > 7 then 
      L_sat := L_sat - 7;
   end if;
   ---
   L_table := 'SOURCE_DLVRY_SCHED_EXC';
   open  C_LOCK_SOURCE_DLVRY_SCHED_EXC;
   close C_LOCK_SOURCE_DLVRY_SCHED_EXC;
   ---
   delete from SOURCE_DLVRY_SCHED_EXC
     where source      = I_source
       and source_type = I_source_type
       and location    = I_location
       and loc_type    = I_loc_type
       and ((day = L_sun and I_sun != 'Y') or
            (day = L_mon and I_mon != 'Y') or
            (day = L_tue and I_tue != 'Y') or
            (day = L_wed and I_wed != 'Y') or
            (day = L_thu and I_thu != 'Y') or
            (day = L_fri and I_fri != 'Y') or
            (day = L_sat and I_sat != 'Y') or
            (I_action = 'D'));
   ---
   L_table := 'SOURCE_DLVRY_SCHED_DAYS';
   open  C_LOCK_SOURCE_DLVRY_SCHED_DAYS;
   close C_LOCK_SOURCE_DLVRY_SCHED_DAYS;
   ---
   delete from SOURCE_DLVRY_SCHED_DAYS
     where source      = I_source
       and source_type = I_source_type
       and location    = I_location
       and loc_type    = I_loc_type;
   ---
   L_table := 'SOURCE_DLVRY_SCHED';
   open  C_LOCK_SOURCE_DLVRY_SCHED;
   close C_LOCK_SOURCE_DLVRY_SCHED;
   ---
   delete from SOURCE_DLVRY_SCHED
     where source = I_source
       and source_type = I_source_type
       and location = I_location
       and loc_type = I_loc_type;
   ---

  if I_action = 'I' then

     insert into SOURCE_DLVRY_SCHED
                   (source,
                    source_type,
                    location,
                    loc_type,
                    start_date,
                    delivery_cycle)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    I_start_date,
                    I_delivery_cycle);


      if I_sun  = 'Y' then
         insert into SOURCE_DLVRY_SCHED_days
                   (source,
                    source_type,
                    location,
                    loc_type,
                    day,
                    start_time,
                    end_time)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    L_sun,
                    I_start_time,
                    I_end_time);
      end if;
      if I_mon = 'Y' then
         insert into SOURCE_DLVRY_SCHED_days
                   (source,
                    source_type,
                    location,
                    loc_type,
                    day,
                    start_time,
                    end_time)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    L_mon,
                    I_start_time,
                    I_end_time);
      end if;
      if I_tue = 'Y' then
         insert into SOURCE_DLVRY_SCHED_days
                   (source,
                    source_type,
                    location,
                    loc_type,
                    day,
                    start_time,
                    end_time)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    L_tue,
                    I_start_time,
                    I_end_time);
      end if;
      if I_wed = 'Y' then
         insert into SOURCE_DLVRY_SCHED_days
                   (source,
                    source_type,
                    location,
                    loc_type,
                    day,
                    start_time,
                    end_time)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    L_wed,
                    I_start_time,
                    I_end_time);
      end if;
      if I_thu = 'Y' then
         insert into SOURCE_DLVRY_SCHED_days
                   (source,
                    source_type,
                    location,
                    loc_type,
                    day,
                    start_time,
                    end_time)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    L_thu,
                    I_start_time,
                    I_end_time);
      end if;
      if I_fri = 'Y' then
         insert into SOURCE_DLVRY_SCHED_days
                   (source,
                    source_type,
                    location,
                    loc_type,
                    day,
                    start_time,
                    end_time)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    L_fri,
                    I_start_time,
                    I_end_time);
      end if;
      if I_sat = 'Y' then
         insert into SOURCE_DLVRY_SCHED_days
                   (source,
                    source_type,
                    location,
                    loc_type,
                    day,
                    start_time,
                    end_time)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    L_sat,
                    I_start_time,
                    I_end_time);
      end if;
   end if;
   ---
   RETURN TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_source),
                                            to_char(I_location));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_DAY_SOURCE;
---------------------------------------------
FUNCTION GET_LOCATIONS(O_error_message    IN OUT   VARCHAR2,
                       I_source           IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                       I_source_type      IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                       I_location         IN       REPL_DAY.LOCATION%TYPE,
                       I_loc_type         IN       REPL_DAY.LOC_TYPE%TYPE,
                       I_start_date       IN       SOURCE_DLVRY_SCHED.START_DATE%TYPE,
                       I_start_time       IN       SOURCE_DLVRY_SCHED_DAYS.START_TIME%TYPE,
                       I_end_time         IN       SOURCE_DLVRY_SCHED_DAYS.END_TIME%TYPE,
                       I_delivery_cycle   IN       SOURCE_DLVRY_SCHED.DELIVERY_CYCLE%TYPE,
                       I_SUN              IN       VARCHAR2,
                       I_MON              IN       VARCHAR2,
                       I_TUE              IN       VARCHAR2,
                       I_WED              IN       VARCHAR2,
                       I_THU              IN       VARCHAR2,
                       I_FRI              IN       VARCHAR2,
                       I_SAT              IN       VARCHAR2,
                       I_action           IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program         VARCHAR2(250) := 'SOURCE_DLVRY_SCHED_SQL.GET_LOCATIONS';
   L_location        STORE.STORE%TYPE :=NULL;
   L_loc_type        ORDLOC.LOC_TYPE%TYPE := NULL;
   L_source_type     SOURCE_DLVRY_SCHED.SOURCE_TYPE%TYPE;

   cursor C_CHAIN is
    select distinct store
      from area a,
           region r,
           district d,
           store s
     where get_primary_lang = get_user_lang
       and s.district = d.district
       and d.region = r.region
       and r.area = a.area
       and a.chain = I_location
       and ((I_source_type = 'SUP' and s.stockholding_ind = 'Y')
            or I_source_type != 'SUP')
    order by 1;

   cursor C_AREA is
    select distinct store
      from region r,
           district d,
           store s
     where get_primary_lang = get_user_lang
       and s.district = d.district
       and d.region = r.region
       and r.area = I_location
       and ((I_source_type = 'SUP' and s.stockholding_ind = 'Y')
            or I_source_type != 'SUP')
    order by 1;

   cursor C_REGION is
    select distinct store
      from district d,
           store s
     where get_primary_lang = get_user_lang
       and s.district = d.district
       and d.region = I_location
       and ((I_source_type = 'SUP' and s.stockholding_ind = 'Y')
            or I_source_type != 'SUP')
    order by 1;

   cursor C_DISTRICT is
    select distinct store
      from district d,
           store s
     where get_primary_lang = get_user_lang
       and s.district = I_location
       and ((I_source_type = 'SUP' and s.stockholding_ind = 'Y')
            or I_source_type != 'SUP')
    order by 1;

   cursor C_LOC_LIST is
    select distinct w.physical_wh location, l.loc_type
      from loc_list_detail l, wh w
     where get_primary_lang = get_user_lang
       and l.loc_list = I_location
       and l.location = w.wh
    UNION ALL
    select distinct s.store location, l.loc_type
      from loc_list_detail l, store s
     where get_primary_lang   = get_user_lang
       and l.loc_list         = I_location
       and l.location         = s.store
       and ((I_source_type = 'SUP' and s.stockholding_ind = 'Y')
            or I_source_type != 'SUP')
    order by 1;

BEGIN

   L_source_type := I_source_type;


   if I_loc_type = 'C' then
      FOR store in C_CHAIN LOOP
         L_location := store.store;

         L_loc_type := 'S';
  
         if SOURCE_DLVRY_SCHED_SQL.INSERT_DAY_SOURCE(O_error_message,
                                                     I_source,
                                                     L_source_type,
                                                     L_location,
                                                     L_loc_type,
                                                     I_start_date,
                                                     I_start_time,
                                                     I_end_time,
                                                     I_delivery_cycle,
                                                     I_SUN,
                                                     I_MON,
                                                     I_TUE,
                                                     I_WED,
                                                     I_THU,
                                                     I_FRI,
                                                     I_SAT,
                                                     I_action)= FALSE then
            return FALSE;
         end if;
      END LOOP;
   elsif I_loc_type = 'A' then
      FOR store in C_AREA LOOP
         L_location := store.store;

         L_loc_type := 'S';
 
         if SOURCE_DLVRY_SCHED_SQL.INSERT_DAY_SOURCE(O_error_message,
                                                     I_source,
                                                     I_source_type,
                                                     L_location,
                                                     L_loc_type,
                                                     I_start_date,
                                                     I_start_time,
                                                     I_end_time,
                                                     I_delivery_cycle,
                                                     I_SUN,
                                                     I_MON,
                                                     I_TUE,
                                                     I_WED,
                                                     I_THU,
                                                     I_FRI,
                                                     I_SAT,
                                                     I_action)= FALSE then

            return FALSE;
         end if;
      END LOOP;

   elsif I_loc_type = 'R' then
      FOR store in C_REGION LOOP
         L_location := store.store;

         L_loc_type := 'S';
         if SOURCE_DLVRY_SCHED_SQL.INSERT_DAY_SOURCE(O_error_message,
                                                     I_source,
                                                     I_source_type,
                                                     L_location,
                                                     L_loc_type,
                                                     I_start_date,
                                                     I_start_time,
                                                     I_end_time,
                                                     I_delivery_cycle,
                                                     I_SUN,
                                                     I_MON,
                                                     I_TUE,
                                                     I_WED,
                                                     I_THU,
                                                     I_FRI,
                                                     I_SAT,
                                                     I_action)= FALSE then

            return FALSE;
         end if;
      END LOOP;
   elsif I_loc_type = 'D' then
      FOR store in C_DISTRICT LOOP
         L_location := store.store;
 
         L_loc_type := 'S';
         if SOURCE_DLVRY_SCHED_SQL.INSERT_DAY_SOURCE(O_error_message,
                                                     I_source,
                                                     I_source_type,
                                                     L_location,
                                                     L_loc_type,
                                                     I_start_date,
                                                     I_start_time,
                                                     I_end_time,
                                                     I_delivery_cycle,
                                                     I_SUN,
                                                     I_MON,
                                                     I_TUE,
                                                     I_WED,
                                                     I_THU,
                                                     I_FRI,
                                                     I_SAT,
                                                     I_action)= FALSE then

            return FALSE;
         end if;
      END LOOP;
   elsif I_loc_type = 'S' then

      L_location := I_location;

      L_loc_type := I_loc_type;
      if SOURCE_DLVRY_SCHED_SQL.INSERT_DAY_SOURCE(O_error_message,
                                                  I_source,
                                                  I_source_type,
                                                  L_location,
                                                  L_loc_type,
                                                  I_start_date,
                                                  I_start_time,
                                                  I_end_time,
                                                  I_delivery_cycle,
                                                  I_SUN,
                                                  I_MON,
                                                  I_TUE,
                                                  I_WED,
                                                  I_THU,
                                                  I_FRI,
                                                  I_SAT,
                                                  I_action)= FALSE then

         return FALSE;
      end if;

   elsif I_loc_type = 'W' then
      L_location := I_location;

      L_loc_type := I_loc_type;
      if SOURCE_DLVRY_SCHED_SQL.INSERT_DAY_SOURCE(O_error_message,
                                                  I_source,
                                                  I_source_type,
                                                  L_location,
                                                  L_loc_type,
                                                  I_start_date,
                                                  I_start_time,
                                                  I_end_time,
                                                  I_delivery_cycle,
                                                  I_SUN,
                                                  I_MON,
                                                  I_TUE,
                                                  I_WED,
                                                  I_THU,
                                                  I_FRI,
                                                  I_SAT,
                                                  I_action)= FALSE then

         return FALSE;
      end if;

   elsif I_loc_type = 'L' then
      FOR store in C_LOC_LIST LOOP
         L_location := store.location;

         L_loc_type := store.loc_type;

         if SOURCE_DLVRY_SCHED_SQL.INSERT_DAY_SOURCE(O_error_message,
                                                     I_source,
                                                     I_source_type,
                                                     L_location,
                                                     L_loc_type,
                                                     I_start_date,
                                                     I_start_time,
                                                     I_end_time,
                                                     I_delivery_cycle,
                                                     I_SUN,
                                                     I_MON,
                                                     I_TUE,
                                                     I_WED,
                                                     I_THU,
                                                     I_FRI,
                                                     I_SAT,
                                                     I_action)= FALSE then

            return FALSE;
         end if;
      END LOOP;
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LOCATIONS;
------------------------------------------------
FUNCTION MANAGE_SOURCE_DLVRY_SCHED(O_error_message    IN OUT   VARCHAR2,
                                   I_source           IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                                   I_source_type      IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                                   I_location         IN       REPL_DAY.LOCATION%TYPE,
                                   I_loc_type         IN       REPL_DAY.LOC_TYPE%TYPE,
                                   I_start_date       IN       SOURCE_DLVRY_SCHED.START_DATE%TYPE,
                                   I_delivery_cycle   IN       SOURCE_DLVRY_SCHED.DELIVERY_CYCLE%TYPE)
RETURN BOOLEAN IS

L_program       VARCHAR2(64) := 'SOURCE_DLVRY_SCHED_SQL.MANAGE_SOURCE_DLVRY_SCHED';


BEGIN



      update SOURCE_DLVRY_SCHED
         set source = I_source,
             source_type = I_source_type,
             location = I_location,
             loc_type = I_loc_type,
             start_date = I_start_date,
             delivery_cycle = I_delivery_cycle
       where source = I_source
         and source_type = I_source_type
         and location = I_location
         and loc_type = I_loc_type;

   if SQL%NOTFOUND then
       insert into SOURCE_DLVRY_SCHED
                   (source,
                    source_type,
                    location,
                    loc_type,
                    start_date,
                    delivery_cycle)
             values(I_source,
                    I_source_type,
                    I_location,
                    I_loc_type,
                    I_start_date,
                    I_delivery_cycle);

   end if;



   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                L_program,to_char(SQLCODE));
      return FALSE;
END MANAGE_SOURCE_DLVRY_SCHED;
---------------------------------------------------------------------------
FUNCTION GET_DELIVERY_CYCLE (O_error_message    IN OUT   VARCHAR2,
                             O_delivery_cycle   IN OUT   SOURCE_DLVRY_SCHED.DELIVERY_CYCLE%TYPE,
                             O_start_date       IN OUT   SOURCE_DLVRY_SCHED.START_DATE%TYPE,
                             I_source           IN       SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                             I_source_type      IN       SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                             I_location         IN       REPL_DAY.LOCATION%TYPE)
RETURN BOOLEAN IS

L_program       VARCHAR2(64) := 'SOURCE_DLVRY_SCHED_SQL.GET_DELIVERY_CYCLE';

cursor C_GET_DELIV_CYLCLE is
   select delivery_cycle,
          start_date
     from source_dlvry_sched
    where source = I_source
      and source_type = I_source_type
      and location = I_location;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_DELIV_CYLCLE','SOURCE_DLVRY_SCHED',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location));
   open C_GET_DELIV_CYLCLE;

   SQL_LIB.SET_MARK('FETCH','C_GET_DELIV_CYLCLE','SOURCE_DLVRY_SCHED',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location));
   fetch C_GET_DELIV_CYLCLE into O_delivery_cycle,
                                 O_start_date;

   SQL_LIB.SET_MARK('CLOSE','C_GET_DELIV_CYLCLE','SOURCE_DLVRY_SCHED',
                    'source: '||to_char(I_source)||', location: '||to_char(I_location));
   close C_GET_DELIV_CYLCLE;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                         L_program,to_char(SQLCODE));
      return FALSE;
END GET_DELIVERY_CYCLE;
--------------------------------------------------------------------
FUNCTION QUERY_DAY_SOURCE(O_error_message   IN OUT   VARCHAR2,
                          I_source_type     IN       SOURCE_DLVRY_SCHED.SOURCE_TYPE%TYPE,
                          I_source          IN       SOURCE_DLVRY_SCHED.SOURCE%TYPE,
                          I_loc_type        IN       SOURCE_DLVRY_SCHED.LOC_TYPE%TYPE,
                          I_location        IN       SOURCE_DLVRY_SCHED.LOCATION%TYPE,
                          O_SUN             IN OUT   VARCHAR2,
                          O_MON             IN OUT   VARCHAR2,
                          O_TUE             IN OUT   VARCHAR2,
                          O_WED             IN OUT   VARCHAR2,
                          O_THU             IN OUT   VARCHAR2,
                          O_FRI             IN OUT   VARCHAR2,
                          O_SAT             IN OUT   VARCHAR2)
 RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'SOURCE_DLVRY_SCHED_SQL.QUERY_DAY_SOURCE';
   L_offset        NUMBER(1);
   L_base_day      NUMBER(1)    := 0;

   cursor C_EXISTS is
      select day
        from source_dlvry_sched_days
       where source_type = I_source_type
         and source = I_source
         and loc_type = I_loc_type
         and location = I_location;

 BEGIN

   if REPLENISHMENT_DAYS_SQL.GET_DAY_OFFSET(O_error_message,
                                            L_offset) = FALSE
   then
      return FALSE;
   end if;

   O_SUN :='N';
   O_MON :='N';
   O_TUE :='N';
   O_WED :='N';
   O_THU :='N';
   O_FRI :='N';
   O_SAT :='N';

   FOR rec in C_EXISTS LOOP
      L_base_day := rec.day-L_offset;
      if L_base_day <= 0
      then
         L_base_day := L_base_day+7;
      end if;

      if L_base_day = 1
         then O_sun := 'Y';
      elsif L_base_day = 2
         then O_MON := 'Y';
      elsif L_base_day = 3
         then O_TUE := 'Y';
      elsif L_base_day = 4
         then O_WED := 'Y';
      elsif L_base_day = 5
         then O_THU := 'Y';
      elsif L_base_day = 6
         then O_FRI := 'Y';
      else
         O_SAT := 'Y';
      end if;

   END LOOP;
    ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                             L_program ,to_char(SQLCODE));
   return FALSE;
END QUERY_DAY_SOURCE;
-----------------------------------------------------------------------------
FUNCTION VALIDATE_SUPLOC_EXC(O_error_message IN OUT VARCHAR2,
                             O_valid         IN OUT BOOLEAN,
                             I_source        IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
                             I_source_type   IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                             I_location      IN     SOURCE_DLVRY_SCHED_EXC.LOCATION%TYPE,
                             I_loc_type      IN     SOURCE_DLVRY_SCHED_EXC.LOC_TYPE%TYPE,
                             I_item_list     IN     SKULIST_HEAD.SKULIST%TYPE,
                             I_item          IN     SOURCE_DLVRY_SCHED_EXC.ITEM%TYPE)
   return BOOLEAN IS
   ---
   L_record_selected_ind  VARCHAR2(1);
   ---
   cursor C_ITEMLIST_VALID is
      select 'x'
        from skulist_detail skl
       where skl.skulist = I_item_list
         and (    (    I_source_type = 'SUP'
                   and exists (select 'x'
                                 from item_supplier
                                where supplier = I_source
                                 and item      = skl.item))
              or  (    I_source_type = 'W'
                   and exists (select 'x'
                                 from item_loc il,
                                      wh w
                                where il.item       = skl.item
                                  and il.loc        = w.wh
                                  and il.loc_type   = 'W'
                                  and w.physical_wh = I_source)))
         and exists (select 'x'
                       from item_loc il2
                      where il2.item = skl.item
                        and (il2.loc  = I_location
                         or exists (select wh
                                      from wh
                                     where physical_wh = I_location
                                       and il2.loc     = wh)));
   ---
 cursor C_ITEM_VALID is
      select 'x' 
        from item_master im
       where im.item = I_item
         and (    (    I_source_type = 'SUP'
                   and exists (select 'x'
                                 from item_supplier
                                where supplier = I_source
                                  and item     = im.item))
               or  (    I_source_type = 'W'
                   and exists (select 'x'
                                 from item_loc il,
                                      wh w
                                where il.item     = im.item
                                  and il.loc      = w.wh
                                  and il.loc_type = 'W'
                                  and physical_wh = I_source)))
         and exists (select 'x'
                       from item_loc il2
                      where il2.item = im.item
                        and (il2.loc  = I_location
                         or exists (select wh
                                      from wh
                                     where physical_wh = I_location
                                       and il2.loc     = wh)));

BEGIN
   if I_source is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_SOURCE', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_source_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_SOURCE_TYPE', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_LOCATION', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_LOC_TYPE', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item_list is NULL and I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 
                                            'I_ITEM or I_ITEM_LIST', 
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item is NOT NULL then --- Adding an individual item.
      SQL_LIB.SET_MARK('OPEN',
                       'C_ITEM_VALID',
                       'ITEM_MASTER, ITEM_SUPPLIER, ITEM_LOC',
                       'Source: '||to_char(I_source)||', Destination: '||to_char(I_location)||', Item: '||I_item);
      open C_ITEM_VALID;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ITEM_VALID',
                       'ITEM_MASTER, ITEM_SUPPLIER, ITEM_LOC',
                       'Source: '||to_char(I_source)||', Destination: '||to_char(I_location)||', Item: '||I_item);
      fetch C_ITEM_VALID into L_record_selected_ind;
      if C_ITEM_VALID%NOTFOUND then
         O_valid := FALSE;
      else
         O_valid := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_VALID',
                       'ITEM_MASTER, ITEM_SUPPLIER, ITEM_LOC',
                       'Source: '||to_char(I_source)||', Destination: '||to_char(I_location)||', Item: '||I_item);
      close C_ITEM_VALID;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_ITEMLIST_VALID',
                       'SKULIST_DETAIL, ITEM_SUPPLIER, ITEM_LOC',
                       'Source: '||to_char(I_source)||', Destination: '||to_char(I_location)||', Item List: '||to_char(I_item_list));
      open C_ITEMLIST_VALID;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ITEMLIST_VALID',
                       'SKULIST_DETAIL, ITEM_SUPPLIER, ITEM_LOC',
                       'Source: '||to_char(I_source)||', Destination: '||to_char(I_location)||', Item List: '||to_char(I_item_list));
      fetch C_ITEMLIST_VALID into L_record_selected_ind;
      if C_ITEMLIST_VALID%NOTFOUND then
         O_valid := FALSE;
      else
         O_valid := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEMLIST_VALID',
                       'SKULIST_DETAIL, ITEM_SUPPLIER, ITEM_LOC',
                       'Source: '||to_char(I_source)||', Destination: '||to_char(I_location)||', Item List: '||to_char(I_item_list));
      close C_ITEMLIST_VALID;
   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SOURCE_DLVRY_SCHED.VALIDATE_SUPLOC_EXC',
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_SUPLOC_EXC;
------------------------------------------------------------------------------------
FUNCTION GET_DELIVERY_DESC  (O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_delivery_slot_desc IN OUT  DELIVERY_SLOT.DELIVERY_SLOT_DESC%TYPE,
                             I_delivery_slot_id   IN      DELIVERY_SLOT.DELIVERY_SLOT_ID%TYPE)
RETURN BOOLEAN IS

L_program              VARCHAR2(64) := 'SOURCE_DLVRY_SCHED_SQL.GET_DELIVERY_DESC';

cursor C_DELIVERY_SLOT is
   select delivery_slot_desc 
     from v_delivery_slot_tl 
    where delivery_slot_id= I_delivery_slot_id;

BEGIN
   open C_DELIVERY_SLOT;

   fetch C_DELIVERY_SLOT into O_delivery_slot_desc;

   if C_DELIVERY_SLOT%NOTFOUND then 
      O_error_message := SQL_LIB.CREATE_MSG('INV_DELV_SLOT_ID',
                                           NULL,
                                           NULL,
                                           NULL);
      close C_DELIVERY_SLOT;
      return FALSE;
   end if; 
   close C_DELIVERY_SLOT;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                         L_program,to_char(SQLCODE));
      return FALSE;
END GET_DELIVERY_DESC;
----------------------------------------------------------------------------------------
END SOURCE_DLVRY_SCHED_SQL;
/

