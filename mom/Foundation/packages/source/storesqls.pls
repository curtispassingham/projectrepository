CREATE OR REPLACE PACKAGE STORE_SQL AUTHID CURRENT_USER AS

TYPE walk_through_store_tbl is TABLE OF WALK_THROUGH_STORE%ROWTYPE;
TYPE store_rec is RECORD(store_row              STORE_ADD%ROWTYPE,
                         original_district_id   DISTRICT.DISTRICT%TYPE,
                         walk_through_tbl       WALK_THROUGH_STORE_TBL,
                         loc_trait_tbl          ORGANIZATION_SQL.LOC_TRAIT_TBL,
                         address_tbl            ADDRESS_SQL.TYPE_ADDRESS_TBL);

-------------------------------------------------------------------------------
-- Name:    INSERT_STORE
-- Purpose: This method will take in a store record and insert all of the record
--          members into their corresponding fields on the STORE_ADD table.
-------------------------------------------------------------------------------
FUNCTION INSERT_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Name:    UPDATE_STORE
-- Purpose: This method will take in a store record, lock the STORE table and
--          update all of the record members for the store id on the passed in
--          record except: Store, Currency, Stockholding Indicator, Like Store,
--          Pricing Store, and Cost Location.
-------------------------------------------------------------------------------
FUNCTION UPDATE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Name:    DELETE_STORE
-- Purpose: This method will call DAILY_PURGE_SQL.INSERT_RECORD, passing the
--          store_id, 'STORE' table name, 'D' for delete type, 1 for delete order.
-------------------------------------------------------------------------------
FUNCTION DELETE_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_store_rec       IN       STORE_SQL.STORE_REC)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Name:    INSERT_WALK_THROUGH_STORE
-- Purpose: This method will take in a store record and insert the walk through
--          stores into the WALK_THROUGH_STORE table.
-------------------------------------------------------------------------------
FUNCTION INSERT_WALK_THROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_store_rec       IN       STORE_SQL.WALK_THROUGH_STORE_TBL)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Name:    DELETE_WALK_THROUGH_STORE
-- Purpose: This method will take in a store record, lock and delete the walk
--          through stores from the  WALK_THROUGH_STORE table.
-------------------------------------------------------------------------------
FUNCTION DELETE_WALK_THROUGH_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_store_rec       IN       STORE_SQL.STORE_REC)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
   -- Function Name: LOCK_STORE
   -- Purpose      : This function will lock the store table for update or delete.
-------------------------------------------------------------------------------
FUNCTION LOCK_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_store_id        IN       STORE.STORE%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Name:    TIMEZONE_NAME_EXISTS
-- Purpose: Checks if Time Zone Name exists in the view V_TimeZone_Names
-------------------------------------------------------------------------------
FUNCTION TIMEZONE_NAME_EXISTS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists         IN OUT BOOLEAN,
                              I_timezone_name  IN     STORE.TIMEZONE_NAME%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION DEL_STORE_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_store_id        IN       STORE.STORE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    INSERT_UPDATE_STORE_ADD
-- Purpose: This function will take in a store record and insert all of the record
--          members into their corresponding fields on the STORE_ADD table.
-------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_STORE_ADD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_store_rec       IN       STORE_SQL.STORE_REC,
                                 I_mode            IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
   -- Function Name : CANCEL_STORE
   -- Purpose       : This function will be called from the store.fmb form in NEW mode. 
   --                 This function deletes the records from multiple tables which had got
   --                 inserted while creating the store.  
                      
FUNCTION CANCEL_STORE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_store_id        IN       STORE.STORE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END STORE_SQL;
/