CREATE OR REPLACE PACKAGE BODY DATA_EXPORT_SQL AS
-------------------------------------------------------------------------------------------------
FUNCTION INS_MERCHHIER_EXPORT_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_action_type     IN       MERCHHIER_EXPORT_STG.ACTION_TYPE%TYPE,
                                  I_division        IN       DIVISION.DIVISION%TYPE,
                                  I_group_no        IN       GROUPS.GROUP_NO%TYPE,
                                  I_dept            IN       DEPS.DEPT%TYPE,
                                  I_class_id        IN       CLASS.CLASS_ID%TYPE,
                                  I_subclass_id     IN       SUBCLASS.SUBCLASS_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'DATA_EXPORT_SQL.INS_MERCHHIER_EXPORT_STG';
   L_action_type     MERCHHIER_EXPORT_STG.ACTION_TYPE%TYPE := NULL;

   cursor C_DIV is
      select action_type
        from merchhier_export_stg
       where division = I_division
         and action_type in (DIV_CRE,DIV_UPD)
         and base_extracted_ind = 'N';

   cursor C_GRP is
      select action_type
        from merchhier_export_stg
       where group_no = I_group_no
         and action_type in (GRP_CRE,GRP_UPD)
         and base_extracted_ind = 'N';

   cursor C_DEPT is
      select action_type
        from merchhier_export_stg
       where dept = I_dept
         and action_type in (DEPT_CRE,DEPT_UPD)
         and base_extracted_ind = 'N';

   cursor C_CLASS is
      select action_type
        from merchhier_export_stg
       where class_id = I_class_id
         and action_type in (CLASS_CRE,CLASS_UPD)
         and base_extracted_ind = 'N';

   cursor C_SUBCLASS is
      select action_type
        from merchhier_export_stg
       where subclass_id = I_subclass_id
         and action_type in (SUBCLASS_CRE,SUBCLASS_UPD)
         and base_extracted_ind = 'N';

BEGIN
   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   -- insert the division record only when there is no mod/cre record present.
   if (I_action_type = DIV_CRE
       or I_action_type = DIV_UPD) then
      merge into merchhier_export_stg mes
           using (select I_division    division,
                         I_action_type action_type,
                         'N'           base_extracted_ind
                    from dual
                 ) inner
              on (    mes.division = inner.division
                  and mes.action_type in (DIV_CRE,DIV_UPD)
                  and mes.base_extracted_ind = 'N'
                 )
            when NOT matched then
               insert (seq_no,
                       division,
                       parent_level,
                       action_type,
                       base_extracted_ind,
                       transaction_datetime)
               values (MERCHHIER_EXPORT_SEQ.nextval,
                       inner.division,
                       'company',
                       inner.action_type,
                       inner.base_extracted_ind,
                       SYSDATE);
   -- Delete the mod/cre division record if present in stg table in case of del message.
   elsif I_action_type = DIV_DEL then
      open C_DIV;
      fetch C_DIV into L_action_type;
      close C_DIV;

      if L_action_type = DIV_CRE then
         delete from merchhier_export_stg
          where division = I_division
            and action_type = DIV_CRE
            and base_extracted_ind = 'N';

      elsif L_action_type = DIV_UPD 
            or L_action_type is NULL then

         if L_action_type = DIV_UPD then
            delete from merchhier_export_stg
             where division = I_division
               and action_type = DIV_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into merchhier_export_stg(seq_no,
                                          division,
                                          action_type,
                                          base_extracted_ind,
                                          transaction_datetime)
                                   values(MERCHHIER_EXPORT_SEQ.nextval,
                                          I_division,
                                          I_action_type,
                                          'N',
                                          SYSDATE);
      end if;

   -- insert the group record only when there is no mod/cre record present.
   elsif (I_action_type = GRP_CRE
         or I_action_type = GRP_UPD) then
      merge into merchhier_export_stg mes
           using (select I_group_no    group_no,
                         I_action_type action_type,
                         'N'           base_extracted_ind
                    from dual
                  ) inner
              on (    mes.group_no = inner.group_no
                  and mes.action_type in (GRP_CRE,GRP_UPD)
                  and mes.base_extracted_ind = 'N'
                  )
            when NOT matched then
               insert(seq_no,
                      group_no,
                      parent_level,
                      action_type,
                      base_extracted_ind,
                      transaction_datetime)
               values(MERCHHIER_EXPORT_SEQ.nextval,
                      inner.group_no,
                      'division',
                      inner.action_type,
                      inner.base_extracted_ind,
                      SYSDATE);
   -- Delete the mod/cre group record if present in stg table in case of del message.
   elsif I_action_type = GRP_DEL then
      open C_GRP;
      fetch C_GRP into L_action_type;
      close C_GRP;

      if L_action_type = GRP_CRE then
         delete from merchhier_export_stg
          where group_no = I_group_no
            and action_type = GRP_CRE
            and base_extracted_ind = 'N';

      elsif L_action_type = GRP_UPD
            or L_action_type is NULL then

         if L_action_type = GRP_UPD then
            delete from merchhier_export_stg
             where group_no = I_group_no
               and action_type = GRP_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into merchhier_export_stg(seq_no,
                                          group_no,
                                          action_type,
                                          base_extracted_ind,
                                          transaction_datetime)
                                   values(MERCHHIER_EXPORT_SEQ.nextval,
                                          I_group_no,
                                          I_action_type,
                                          'N',
                                          SYSDATE);
      end if;

   -- insert the dept record only when there is no mod/cre record present.
   elsif (I_action_type = DEPT_CRE
          or I_action_type = DEPT_UPD) then
      merge into merchhier_export_stg mes
           using (select I_dept        dept,
                         I_action_type action_type,
                         'N'           base_extracted_ind
                    from dual
                 ) inner
              on (    mes.dept        = inner.dept
                  and mes.action_type in (DEPT_CRE,DEPT_UPD)
                  and mes.base_extracted_ind  = 'N'
                 )
            when NOT matched then
               insert(seq_no,
                      dept,
                      parent_level,
                      action_type,
                      base_extracted_ind,
                      transaction_datetime)
               values(MERCHHIER_EXPORT_SEQ.nextval,
                      inner.dept,
                      'group',
                      inner.action_type,
                      inner.base_extracted_ind,
                      SYSDATE);
   -- Delete the mod/cre dept record if present in stg table in case of del message.
   elsif I_action_type = DEPT_DEL then
      open C_DEPT;
      fetch C_DEPT into L_action_type;
      close C_DEPT;

      if L_action_type = DEPT_CRE then
         delete from merchhier_export_stg
          where dept = I_dept
            and action_type = DEPT_CRE
            and base_extracted_ind = 'N';

      elsif L_action_type = DEPT_UPD
            or L_action_type is NULL then
         if L_action_type = DEPT_UPD then
            delete from merchhier_export_stg
             where dept = I_dept
               and action_type = DEPT_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into merchhier_export_stg(seq_no,
                                          dept,
                                          action_type,
                                          base_extracted_ind,
                                          transaction_datetime)
                                   values(MERCHHIER_EXPORT_SEQ.nextval,
                                          I_dept,
                                          I_action_type,
                                          'N',
                                          SYSDATE);
      end if;

   -- insert the class record only when there is no mod/cre record present.
   elsif (I_action_type = CLASS_CRE or I_action_type = CLASS_UPD) then
      merge into merchhier_export_stg mes
           using (select I_class_id    class_id,
                         I_action_type action_type,
                         'N'           base_extracted_ind
                    from dual
                  ) inner
              on (    mes.class_id = inner.class_id
                  and mes.action_type in (CLASS_CRE,CLASS_UPD)
                  and mes.base_extracted_ind = 'N'
                  )
            when NOT matched then
               insert(seq_no,
                      class_id,
                      parent_level,
                      action_type,
                      base_extracted_ind,
                      transaction_datetime)
               values(MERCHHIER_EXPORT_SEQ.nextval,
                      inner.class_id,
                      'department',
                      inner.action_type,
                      inner.base_extracted_ind,
                      SYSDATE);

   -- Delete the mod/cre class record if present in stg table in case of del message.
   elsif I_action_type = CLASS_DEL then
      open C_CLASS;
      fetch C_CLASS into L_action_type;
      close C_CLASS;

      if L_action_type = CLASS_CRE then
         delete from merchhier_export_stg
          where class_id = I_class_id
            and action_type = CLASS_CRE
            and base_extracted_ind = 'N';

      elsif L_action_type = CLASS_UPD 
            or L_action_type is NULL then

         if L_action_type = CLASS_UPD then
            delete from merchhier_export_stg
             where class_id = I_class_id
               and action_type = CLASS_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into merchhier_export_stg(seq_no,
                                          class_id,
                                          action_type,
                                          base_extracted_ind,
                                          transaction_datetime)
                                   values(MERCHHIER_EXPORT_SEQ.nextval,
                                          I_class_id,
                                          I_action_type,
                                          'N',
                                          SYSDATE);
      end if;

   -- insert the subclass record only when there is no mod/cre record present.
   elsif (I_action_type = SUBCLASS_CRE or I_action_type = SUBCLASS_UPD) then
      merge into merchhier_export_stg mes
           using (select I_subclass_id subclass_id,
                         I_action_type action_type,
                         'N'           base_extracted_ind
                    from dual
                 ) inner
              on (    mes.subclass_id   = inner.subclass_id
                  and mes.action_type in (SUBCLASS_CRE,SUBCLASS_UPD)
                  and mes.base_extracted_ind = 'N'
                  )
            when NOT matched then
               insert(seq_no,
                      subclass_id,
                      parent_level,
                      action_type,
                      base_extracted_ind,
                      transaction_datetime)
               values(MERCHHIER_EXPORT_SEQ.nextval,
                      inner.subclass_id,
                      'class',
                      inner.action_type,
                      inner.base_extracted_ind,
                      SYSDATE);
   -- Delete the mod/cre subclass record if present in stg table in case of del message.
   elsif I_action_type = SUBCLASS_DEL then

      open C_SUBCLASS;
      fetch C_SUBCLASS into L_action_type;
      close C_SUBCLASS;

      if L_action_type = SUBCLASS_CRE then
         delete from merchhier_export_stg
          where subclass_id = I_subclass_id
            and action_type = SUBCLASS_CRE
            and base_extracted_ind = 'N';

      elsif L_action_type = SUBCLASS_UPD
            or L_action_type is NULL then

         if L_action_type = SUBCLASS_UPD then
            delete from merchhier_export_stg
             where subclass_id = I_subclass_id
               and action_type = SUBCLASS_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into merchhier_export_stg(seq_no,
                                          subclass_id,
                                          action_type,
                                          base_extracted_ind,
                                          transaction_datetime)
                                   values(MERCHHIER_EXPORT_SEQ.nextval,
                                          I_subclass_id,
                                          I_action_type,
                                          'N',
                                          SYSDATE);
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INS_MERCHHIER_EXPORT_STG;
-------------------------------------------------------------------------------------------------
FUNCTION INS_ORGHIER_EXPORT_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_action_type     IN       ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE,
                                I_comp            IN       COMPHEAD.COMPANY%TYPE,
                                I_chain           IN       CHAIN.CHAIN%TYPE,
                                I_area            IN       AREA.AREA%TYPE,
                                I_region          IN       REGION.REGION%TYPE,
                                I_district        IN       DISTRICT.DISTRICT%TYPE,
                                I_store           IN       STORE.STORE%TYPE,
                                I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'DATA_EXPORT_SQL.INS_ORGHHIER_EXPORT_STG';

   L_action_type   ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE := NULL;

   cursor C_ORG_EXISTS is
      select action_type
        from orghier_export_stg oes
       where (oes.chain = I_chain and
              oes.action_type in (CHAIN_ADD, CHAIN_UPD) and
              oes.base_extracted_ind = 'N')
          or (oes.area = I_area and
              oes.action_type in (AREA_ADD, AREA_UPD) and
              base_extracted_ind = 'N')
          or (oes.region = I_region and
              oes.action_type in (REG_ADD, REG_UPD) and
              base_extracted_ind = 'N')
          or (oes.district = I_district and
              oes.action_type in (DIST_ADD, DIST_UPD) and
              base_extracted_ind = 'N')
          or (oes.store = I_store and
              oes.action_type in (ST_ADD, ST_UPD) and
              base_extracted_ind = 'N')
          or (oes.wh = I_wh and
              oes.action_type in (WH_ADD, WH_UPD) and
              base_extracted_ind = 'N');
BEGIN

   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_action_type = COMP_ADD or
      I_action_type = COMP_UPD then
      merge into orghier_export_stg oes
      using (select I_comp         company,
                    NULL           parent_level,
                    'N'            base_extracted_ind,
                    I_action_type  action_type
               from dual) use_this
      on (oes.company = use_this.company and
          oes.action_type in (COMP_ADD, COMP_UPD) and
          oes.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 company,
                 parent_level,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (ORGHIER_EXPORT_SEQ.NEXTVAL,
                 use_this.company,
                 use_this.parent_level,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);
   end if;

   if I_action_type = CHAIN_ADD or
      I_action_type = CHAIN_UPD then
      merge into orghier_export_stg oes
      using (select I_chain       chain,
                    'company'      parent_level,
                    'N'            base_extracted_ind,
                    I_action_type action_type
               from dual) use_this
      on (oes.chain = use_this.chain and
          oes.action_type in (CHAIN_ADD, CHAIN_UPD) and
          oes.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 chain,
                 parent_level,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (ORGHIER_EXPORT_SEQ.NEXTVAL,
                 use_this.chain,
                 use_this.parent_level,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);
   elsif I_action_type = CHAIN_DEL then
      open C_ORG_EXISTS;
      fetch C_ORG_EXISTS into L_action_type;
      close C_ORG_EXISTS;

      -- there is a 'chaincre' or 'chainmod' that has not been exported.
      -- delete this record and do NOT insert a 'chaindel' record.
      if L_action_type = CHAIN_ADD then
         delete from orghier_export_stg
          where chain = I_chain
            and action_type = CHAIN_ADD
            and base_extracted_ind = 'N';
      elsif L_action_type is NULL or
            L_action_type = CHAIN_UPD then

         if L_action_type = CHAIN_UPD then
            delete from orghier_export_stg
             where chain = I_chain
               and action_type = CHAIN_UPD
               and base_extracted_ind = 'N';
         end if;

         -- insert a 'chaindel' record
         insert into orghier_export_stg(seq_no,
                                        chain,
                                        base_extracted_ind,
                                        action_type,
                                        transaction_datetime)
                                 values(ORGHIER_EXPORT_SEQ.NEXTVAL,
                                        I_chain,
                                        'N',
                                        I_action_type,
                                        SYSDATE);
      end if;
   end if;

   if I_action_type = AREA_ADD or
      I_action_type = AREA_UPD then
      merge into orghier_export_stg oes
      using (select I_area         area,
                    'chain'        parent_level,
                    'N'            base_extracted_ind,
                    I_action_type  action_type
               from dual) use_this
      on (oes.area = use_this.area and
          oes.action_type in (AREA_ADD, AREA_UPD) and
          oes.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 area,
                 parent_level,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (ORGHIER_EXPORT_SEQ.NEXTVAL,
                 use_this.area,
                 use_this.parent_level,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);
   elsif I_action_type = AREA_DEL then
      open C_ORG_EXISTS;
      fetch C_ORG_EXISTS into L_action_type;
      close C_ORG_EXISTS;

      -- there is a 'areacre' or 'areamod' that has not been exported.
      -- delete this record and do NOT insert an 'areadel' record.
      if L_action_type = AREA_ADD then
         delete from orghier_export_stg
          where area = I_area
            and action_type = AREA_ADD
            and base_extracted_ind = 'N';
      elsif L_action_type is NULL or
            L_action_type = AREA_UPD then

         if L_action_type = AREA_UPD then
            delete from orghier_export_stg
             where area = I_area
               and action_type = AREA_UPD
               and base_extracted_ind = 'N';
         end if;

         -- insert an 'areadel' record
         insert into orghier_export_stg(seq_no,
                                        area,
                                        base_extracted_ind,
                                        action_type,
                                        transaction_datetime)
                                 values(ORGHIER_EXPORT_SEQ.NEXTVAL,
                                        I_area,
                                        'N',
                                        I_action_type,
                                        SYSDATE);
      end if;
   end if;

   if I_action_type = REG_ADD or
      I_action_type = REG_UPD then
      merge into orghier_export_stg oes
      using (select I_region       region,
                    'area'         parent_level,
                    'N'            base_extracted_ind,
                    I_action_type  action_type
               from dual) use_this
      on (oes.region = use_this.region and
          oes.action_type in (REG_ADD, REG_UPD) and
          oes.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 region,
                 parent_level,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (ORGHIER_EXPORT_SEQ.NEXTVAL,
                 use_this.region,
                 use_this.parent_level,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);
   elsif I_action_type = REG_DEL then
      open C_ORG_EXISTS;
      fetch C_ORG_EXISTS into L_action_type;
      close C_ORG_EXISTS;

      -- there is a 'regioncre' or 'regionmod' that has not been exported.
      -- delete this record and do NOT insert a 'regiondel' record.
      if L_action_type = REG_ADD then
         delete from orghier_export_stg
          where region = I_region
            and action_type = REG_ADD
            and base_extracted_ind = 'N';
      elsif L_action_type is NULL or
            L_action_type = REG_UPD then

         if L_action_type = REG_UPD then
            delete from orghier_export_stg
             where region = I_region
               and action_type = REG_UPD
               and base_extracted_ind = 'N';
         end if;

         -- insert a 'regiondel' record
         insert into orghier_export_stg(seq_no,
                                        region,
                                        base_extracted_ind,
                                        action_type,
                                        transaction_datetime)
                                 values(ORGHIER_EXPORT_SEQ.NEXTVAL,
                                        I_region,
                                        'N',
                                        I_action_type,
                                        SYSDATE);
      end if;
   end if;

   if I_action_type = DIST_ADD or
      I_action_type = DIST_UPD then
      merge into orghier_export_stg oes
      using (select I_district     district,
                    'region'       parent_level,
                    'N'            base_extracted_ind,
                    I_action_type  action_type
               from dual) use_this
      on (oes.district = use_this.district and
          oes.action_type in (DIST_ADD, DIST_UPD) and
          oes.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 district,
                 parent_level,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (ORGHIER_EXPORT_SEQ.NEXTVAL,
                 use_this.district,
                 use_this.parent_level,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);

      return TRUE;
   elsif I_action_type = DIST_DEL then
      open C_ORG_EXISTS;
      fetch C_ORG_EXISTS into L_action_type;
      close C_ORG_EXISTS;

      -- there is a 'districtcre' or 'districtmod' that has not been exported.
      -- delete this record and do NOT insert a 'districtdel' record.
      if L_action_type = DIST_ADD then
         delete from orghier_export_stg
          where district = I_district
            and action_type = DIST_ADD
            and base_extracted_ind = 'N';
      elsif L_action_type is NULL or
            L_action_type = DIST_UPD then

         if L_action_type = DIST_UPD then
            delete from orghier_export_stg
             where district = I_district
               and action_type = DIST_UPD
               and base_extracted_ind = 'N';
         end if;

         -- insert a 'districtdel' record
         insert into orghier_export_stg(seq_no,
                                        district,
                                        base_extracted_ind,
                                        action_type,
                                        transaction_datetime)
                                 values(ORGHIER_EXPORT_SEQ.NEXTVAL,
                                        I_district,
                                        'N',
                                        I_action_type,
                                        SYSDATE);
      end if;
   end if;

   if I_action_type = ST_ADD or
      I_action_type = ST_UPD then
      merge into orghier_export_stg oes
      using (select I_store        store,
                    'district'     parent_level,
                    'N'            base_extracted_ind,
                    I_action_type  action_type
               from dual) use_this
      on (oes.store = use_this.store and
          oes.action_type in (ST_ADD, ST_UPD) and
          oes.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 store,
                 parent_level,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (ORGHIER_EXPORT_SEQ.NEXTVAL,
                 use_this.store,
                 use_this.parent_level,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);
   elsif I_action_type = ST_DEL then
      open C_ORG_EXISTS;
      fetch C_ORG_EXISTS into L_action_type;
      close C_ORG_EXISTS;

      -- there is a 'storecre' or 'storemod' that has not been exported.
      -- delete this record and do NOT insert a 'storedel' record.
      if L_action_type = ST_ADD then
         delete from orghier_export_stg
          where store = I_store
            and action_type = ST_ADD
            and base_extracted_ind = 'N';
      elsif L_action_type is NULL or
            L_action_type = ST_UPD then

         if L_action_type = ST_UPD then
            delete from orghier_export_stg
             where store = I_store
               and action_type = ST_UPD
               and base_extracted_ind = 'N';
         end if;

         -- insert a 'storedel' record
         insert into orghier_export_stg(seq_no,
                                        store,
                                        base_extracted_ind,
                                        action_type,
                                        transaction_datetime)
                                 values(ORGHIER_EXPORT_SEQ.NEXTVAL,
                                        I_store,
                                        'N',
                                        I_action_type,
                                        SYSDATE);
      end if;
   end if;

   if I_action_type = WH_ADD or
      I_action_type = WH_UPD then
      merge into orghier_export_stg oes
      using (select I_wh           wh,
                    'company'      parent_level,
                    'N'            base_extracted_ind,
                    I_action_type  action_type
               from dual) use_this
      on (oes.wh = use_this.wh and
          oes.action_type in (WH_ADD, WH_UPD) and
          oes.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 wh,
                 parent_level,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (ORGHIER_EXPORT_SEQ.NEXTVAL,
                 use_this.wh,
                 use_this.parent_level,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);

      return TRUE;
   elsif I_action_type = WH_DEL then
      open C_ORG_EXISTS;
      fetch C_ORG_EXISTS into L_action_type;
      close C_ORG_EXISTS;

      -- there is a 'whcre' or 'whmod' that has not been exported.
      -- delete this record and do NOT insert a 'whdel' record.
      if L_action_type = WH_ADD then
         delete from orghier_export_stg
          where wh = I_wh
            and action_type = WH_ADD
            and base_extracted_ind = 'N';
      elsif L_action_type is NULL or
            L_action_type = WH_UPD then

         if L_action_type = WH_UPD then
            delete from orghier_export_stg
             where wh = I_wh
               and action_type = WH_UPD
               and base_extracted_ind = 'N';
         end if;

         -- insert a 'whdel' record
         insert into orghier_export_stg(seq_no,
                                        wh,
                                        base_extracted_ind,
                                        action_type,
                                        transaction_datetime)
                                 values(ORGHIER_EXPORT_SEQ.NEXTVAL,
                                        I_wh,
                                        'N',
                                        I_action_type,
                                        SYSDATE);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INS_ORGHIER_EXPORT_STG;
-----------------------------------------------------------------------------------------------
FUNCTION INS_STORE_EXPORT_STG(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_action_type        IN       STORE_EXPORT_STG.ACTION_TYPE%TYPE,
                              I_store              IN       STORE.STORE%TYPE,
                              I_addr_key           IN       ADDR.ADDR_KEY%TYPE,
                              I_addr_type          IN       ADDR.ADDR_TYPE%TYPE,
                              I_primary_addr_ind   IN       ADDR.PRIMARY_ADDR_IND%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'DATA_EXPORT_SQL.INS_STORE_EXPORT_STG';

   L_action_type   STORE_EXPORT_STG.ACTION_TYPE%TYPE := NULL;

   cursor C_STORE_EXISTS is
      select action_type
        from store_export_stg
       where store = I_store
         and action_type in (ST_ADD, ST_UPD)
         and base_extracted_ind = 'N';

   cursor C_STDTL_EXISTS is
      select action_type
        from store_export_stg
       where store = I_store
         and addr_key = I_addr_key
         and action_type in (STDTL_ADD, STDTL_UPD)
         and base_extracted_ind = 'N';

BEGIN

   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_action_type = ST_ADD or
      I_action_type = ST_UPD then
      merge into store_export_stg ses
      using (select I_store        store,
                    'N'            base_extracted_ind,
                    I_action_type  action_type
               from dual) use_this
      on (ses.store = use_this.store and
          ses.action_type in (ST_ADD, ST_UPD) and
          ses.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 store,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (STORE_EXPORT_SEQ.NEXTVAL,
                 use_this.store,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);

   elsif I_action_type = ST_DEL then
      open C_STORE_EXISTS;
      fetch C_STORE_EXISTS into L_action_type;
      close C_STORE_EXISTS;

      -- there is a 'storecre' or 'storemod' that has not been exported.
      -- delete this record and do NOT insert a 'storedel' record.
      if L_action_type = ST_ADD then
         delete from store_export_stg
          where store = I_store
            and action_type = ST_ADD
            and base_extracted_ind = 'N';
      elsif L_action_type is NULL or
            L_action_type = ST_UPD then

         if L_action_type = ST_UPD then
            delete from store_export_stg
             where store = I_store
               and action_type = ST_UPD
               and base_extracted_ind = 'N';
         end if;

         -- insert a 'storedel' record
         insert into store_export_stg(seq_no,
                                      store,
                                      base_extracted_ind,
                                      action_type,
                                      transaction_datetime)
                               values(STORE_EXPORT_SEQ.NEXTVAL,
                                      I_store,
                                      'N',
                                      I_action_type,
                                      SYSDATE);
      end if;

   end if;

   if I_action_type = STDTL_ADD or
      I_action_type = STDTL_UPD then
      merge into store_export_stg ses
      using (select I_store             store,
                    I_addr_key          addr_key,
                    I_addr_type         addr_type,
                    I_primary_addr_ind  primary_addr_ind,
                    'N'                 base_extracted_ind,
                    I_action_type       action_type
               from dual) use_this
      on (ses.store = use_this.store and
          ses.addr_key = use_this.addr_key and
          ses.action_type in (STDTL_ADD, STDTL_UPD) and
          ses.base_extracted_ind = 'N')
      when NOT matched then
         insert (seq_no,
                 store,
                 addr_key,
                 addr_type,
                 primary_addr_ind,
                 action_type,
                 base_extracted_ind,
                 transaction_datetime)
         values (STORE_EXPORT_SEQ.NEXTVAL,
                 use_this.store,
                 use_this.addr_key,
                 use_this.addr_type,
                 use_this.primary_addr_ind,
                 use_this.action_type,
                 use_this.base_extracted_ind,
                 SYSDATE);

   elsif I_action_type = STDTL_DEL then
      open C_STDTL_EXISTS;
      fetch C_STDTL_EXISTS into L_action_type;
      close C_STDTL_EXISTS;

      -- there is a 'storedtlcre' or 'storedtlmod' that has not been exported.
      -- delete this record and do NOT insert a 'storedtldel' record.
      if L_action_type = STDTL_ADD then
         delete from store_export_stg
          where store = I_store
            and addr_key = I_addr_key
            and action_type = STDTL_ADD
            and base_extracted_ind = 'N';
      elsif L_action_type is NULL or
            L_action_type = STDTL_UPD then

         if L_action_type = STDTL_UPD then
            delete from store_export_stg
             where store = I_store
               and addr_key = I_addr_key
               and action_type = STDTL_UPD
               and base_extracted_ind = 'N';
         end if;

         -- insert a 'storedel' record
         insert into store_export_stg(seq_no,
                                      store,
                                      addr_key,
                                      addr_type,
                                      primary_addr_ind,
                                      base_extracted_ind,
                                      action_type,
                                      transaction_datetime)
                               values(STORE_EXPORT_SEQ.NEXTVAL,
                                      I_store,
                                      I_addr_key,
                                      I_addr_type,
                                      I_primary_addr_ind,
                                      'N',
                                      I_action_type,
                                      SYSDATE);
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INS_STORE_EXPORT_STG;
---------------------------------------------------------------------------------
FUNCTION INS_DIFFS_EXPORT_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_action_type     IN       DIFFS_EXPORT_STG.ACTION_TYPE%TYPE,
                              I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(60)  := 'DATA_EXPORT_SQL.INS_DIFFS_EXPORT_STG';
   L_action_type  DIFFS_EXPORT_STG.ACTION_TYPE%TYPE := NULL;

   cursor C_DIFF_ID is
      select action_type
        from diffs_export_stg
       where diff_id = I_diff_id
         and action_type in (DIFF_CRE,DIFF_UPD)
         and base_extracted_ind = 'N';

BEGIN
   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_diff_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_diff_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if (I_action_type = DIFF_CRE or
       I_action_type = DIFF_UPD) then
      merge into diffs_export_stg des
           using ( select I_diff_id     diff_id,
                          I_action_type action_type,
                          'N'           base_extracted_ind
                     from dual
                 ) inner
              on (    des.diff_id = inner.diff_id
                  and des.action_type in (DIFF_CRE,DIFF_UPD)
                  and des.base_extracted_ind = 'N'
                 )
            when NOT matched then
               insert (seq_no,
                       diff_id,
                       action_type,
                       base_extracted_ind,
                       transaction_datetime)
               values (DIFFS_EXPORT_SEQ.nextval,
                       inner.diff_id,
                       inner.action_type,
                       inner.base_extracted_ind,
                       SYSDATE);
   elsif I_action_type = DIFF_DEL then
      open C_DIFF_ID;
      fetch C_DIFF_ID into L_action_type;
      close C_DIFF_ID;

      -- delete the cre and upd from stg if same diff_id got deleted
      if L_action_type = DIFF_CRE then
         delete from diffs_export_stg
          where diff_id = I_diff_id
            and action_type = DIFF_CRE
            and base_extracted_ind = 'N';

      elsif L_action_type = DIFF_UPD
            or L_action_type is NULL then

         if L_action_type = DIFF_UPD then
            delete from diffs_export_stg
             where diff_id = I_diff_id
               and action_type = DIFF_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into diffs_export_stg(seq_no,
                                      diff_id,
                                      action_type,
                                      base_extracted_ind,
                                      transaction_datetime)
                              values (DIFFS_EXPORT_SEQ.nextval,
                                      I_diff_id,
                                      I_action_type,
                                      'N',
                                      SYSDATE);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INS_DIFFS_EXPORT_STG;
---------------------------------------------------------------------------------
FUNCTION INS_DIFFGRP_EXPORT_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_action_type     IN       DIFFGRP_EXPORT_STG.ACTION_TYPE%TYPE,
                                I_diff_group_id   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                                I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'DATA_EXPORT_SQL.INS_DIFFGRP_EXPORT_STG';
   L_action_type  DIFFGRP_EXPORT_STG.ACTION_TYPE%TYPE := NULL;

   cursor C_DIFFGRPHDR_ID is
      select action_type
        from diffgrp_export_stg
       where diff_group_id = I_diff_group_id
         and action_type in (DIFFGRPHDR_CRE,DIFFGRPHDR_UPD)
         and base_extracted_ind = 'N';

   cursor C_DIFFGRPDTL_ID is
      select action_type
        from diffgrp_export_stg
       where diff_group_id = I_diff_group_id
         and diff_id = I_diff_id
         and action_type in (DIFFGRPDTL_CRE,DIFFGRPDTL_UPD)
         and base_extracted_ind = 'N';

BEGIN
   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'I_action_type',
                                        L_program,
                                        NULL);
      return FALSE;
   end if;
   if (I_action_type = DIFFGRPHDR_CRE 
       or I_action_type = DIFFGRPHDR_UPD) then
      merge into diffgrp_export_stg dges
           using (select I_diff_group_id  diff_group_id,
                         I_action_type    action_type,
                         'N'              base_extracted_ind
                    from dual
                  ) inner
              on (    dges.diff_group_id = inner.diff_group_id
                  and dges.action_type in (DIFFGRPHDR_CRE,DIFFGRPHDR_UPD)
                  and dges.base_extracted_ind = 'N'
                 )
            when NOT matched then
               insert(seq_no,
                      diff_group_id,
                      action_type,
                      base_extracted_ind,
                      transaction_datetime)
               values(DIFFGRP_EXPORT_SEQ.nextval,
                      inner.diff_group_id,
                      inner.action_type,
                      inner.base_extracted_ind,
                      SYSDATE);
   elsif I_action_type = DIFFGRPHDR_DEL then
      open C_DIFFGRPHDR_ID;
      fetch C_DIFFGRPHDR_ID into L_action_type;
      close C_DIFFGRPHDR_ID;

      if L_action_type = DIFFGRPHDR_CRE then
         delete from diffgrp_export_stg
          where diff_group_id = I_diff_group_id  
            and action_type in DIFFGRPHDR_CRE
            and base_extracted_ind = 'N';

      elsif L_action_type = DIFFGRPHDR_UPD
            or L_action_type is NULL then

         if L_action_type = DIFFGRPHDR_UPD then
            delete from diffgrp_export_stg
             where diff_group_id = I_diff_group_id  
               and action_type in DIFFGRPHDR_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into diffgrp_export_stg(seq_no,
                                        diff_group_id,
                                        action_type,
                                        base_extracted_ind,
                                        transaction_datetime)
                                 values(DIFFGRP_EXPORT_SEQ.nextval,
                                        I_diff_group_id,
                                        I_action_type,
                                        'N',
                                        SYSDATE);
      end if;
   elsif (I_action_type = DIFFGRPDTL_CRE 
          or I_action_type = DIFFGRPDTL_UPD) then
      merge into diffgrp_export_stg dges
           using (select I_diff_group_id diff_group_id,
                         I_diff_id       diff_id,
                         I_action_type   action_type,
                         'N'             base_extracted_ind
                   from dual
                  ) inner
              on (    dges.diff_group_id = inner.diff_group_id
                  and dges.diff_id = inner.diff_id
                  and dges.action_type in (DIFFGRPDTL_CRE,DIFFGRPDTL_UPD) 
                  and dges.base_extracted_ind = 'N'
                 )
            when NOT matched then
               insert(seq_no,
                      diff_group_id,
                      diff_id,
                      action_type,
                      base_extracted_ind,
                      transaction_datetime)
              values (DIFFGRP_EXPORT_SEQ.nextval,
                     inner.diff_group_id,
                     inner.diff_id,
                     inner.action_type,
                     inner.base_extracted_ind,
                     SYSDATE);
   elsif I_action_type = DIFFGRPDTL_DEL then
      open C_DIFFGRPDTL_ID;
      fetch C_DIFFGRPDTL_ID into L_action_type;
      close C_DIFFGRPDTL_ID;

      if L_action_type = DIFFGRPDTL_CRE then
         delete from diffgrp_export_stg
          where diff_group_id = I_diff_group_id
            and diff_id = I_diff_id
            and action_type = DIFFGRPDTL_CRE
            and base_extracted_ind = 'N';

      elsif L_action_type = DIFFGRPDTL_UPD
            or L_action_type is NULL then

         if L_action_type = DIFFGRPDTL_UPD then
            delete from diffgrp_export_stg
             where diff_group_id = I_diff_group_id
               and diff_id = I_diff_id
               and action_type = DIFFGRPDTL_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into diffgrp_export_stg(seq_no,
                                        diff_group_id,
                                        diff_id,
                                        action_type,
                                        base_extracted_ind,
                                        transaction_datetime)
                                 values(DIFFGRP_EXPORT_SEQ.nextval,
                                        I_diff_group_id,
                                        I_diff_id,
                                        I_action_type,
                                        'N',
                                        SYSDATE);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;   
END INS_DIFFGRP_EXPORT_STG;
----------------------------------------------------------------------------------
FUNCTION INS_ITEM_EXPORT_STG(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_action_type        IN       ITEM_EXPORT_STG.ACTION_TYPE%TYPE,
                             I_item               IN       ITEM_MASTER.ITEM%TYPE,
                             I_item_parent        IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                             I_item_grandparent   IN       ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                             I_item_level         IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                             I_tran_level         IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                             I_vat_region         IN       VAT_REGION.VAT_REGION%TYPE,
                             I_vat_code           IN       VAT_CODES.VAT_CODE%TYPE,
                             I_vat_type           IN       VAT_ITEM.VAT_TYPE%TYPE,
                             I_merchandise_ind    IN       ITEM_MASTER.MERCHANDISE_IND%TYPE,
                             I_vat_active_date    IN       VAT_ITEM.ACTIVE_DATE%TYPE,
                             I_loc                IN       ITEM_LOC.LOC%TYPE,
                             I_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'DATA_EXPORT_SQL.INS_ITEM_EXPORT_STG';

   L_action_type   ITEM_EXPORT_STG.ACTION_TYPE%TYPE := NULL;
   L_info_exist    VARCHAR2(1) := 'N';
   L_item_loc_parent_exist VARCHAR2(1) := 'N';
 

   cursor C_ITEM is
      select action_type
         from item_export_stg
        where item = I_item
          and action_type = ITEM_UPD
          and base_extracted_ind = 'N';

   cursor C_ITEM_INFO is
      select 'Y'
        from item_export_info
       where item = I_item
         and base_extracted_ind = 'Y'
         and process_id is NOT NULL;

   cursor C_ITLOC is
      select action_type
        from item_export_stg
       where item = I_item
         and loc = I_loc
         and loc_type = I_loc_type
         and action_type in (ITLOC_CRE,ITLOC_UPD)
         and base_extracted_ind = 'N';
   
   cursor C_ITEM_UPC is
      select 'Y'
        from item_export_stg
       where item = I_item_parent
         and action_type = ITLOC_CRE
         and base_extracted_ind = 'Y'
         and process_id is NOT NULL;

   cursor C_VATITM is
      select action_type
        from item_export_stg
       where item = I_item
         and vat_region = I_vat_region
         and vat_code = I_vat_code
         and action_type in (VATITM_CRE,VATITM_UPD)
         and vat_active_date = I_vat_active_date
         and vat_type = I_vat_type
         and base_extracted_ind = 'N';

BEGIN
   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_ITEM_INFO;
   fetch C_ITEM_INFO into L_info_exist;
   close C_ITEM_INFO;
   
    open C_ITEM_UPC;
   fetch C_ITEM_UPC into L_item_loc_parent_exist;
   close C_ITEM_UPC;

   if I_action_type = ITEM_CRE then
      insert into item_export_info(item,
                                   base_extracted_ind,
                                   custom1_extracted_ind,
                                   custom2_extracted_ind,
                                   custom3_extracted_ind,
                                   custom4_extracted_ind,
                                   process_id)
                            values(I_item,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL);
       if L_item_loc_parent_exist ='Y' then                                   
          insert into item_export_stg(seq_no,       
                                     item,
                                     loc,
                                     loc_type,
                                     merchandise_ind,
                                     action_type,
                                     base_extracted_ind,
                                     transaction_datetime)
                              select ITEM_EXPORT_SEQ.nextval,
                                     I_item,
                                     il.loc,
                                     il.loc_type, 
                                     I_merchandise_ind,
                                     'itemupccre',
                                     'N',
                                     SYSDATE                              
                               from  item_loc il
                              where  il.item = I_item_parent 
                                and il.loc_type = 'S'
                                and I_item_level>I_tran_level;           
      end if;
                                    
   elsif I_action_type = ITEM_UPD then      
      if L_info_exist = 'Y' then
         merge into item_export_stg ies
              using (select I_item        item,
                            I_action_type action_type,
                            'N'           base_extracted_ind
                            from dual
                     ) inner
                on (    ies.item = inner.item
                    and ies.action_type = inner.action_type
                    and ies.base_extracted_ind = 'N'
                    )
               when NOT matched then
                  insert(seq_no,
                         item,
                         action_type,
                         base_extracted_ind,
                         transaction_datetime)
                  values(ITEM_EXPORT_SEQ.nextval,
                         inner.item,
                         inner.action_type,
                         inner.base_extracted_ind,
                         SYSDATE);
  
     /*itemhdrmod for location ranged both for parent and child item*/
         merge into item_export_stg ies
              using (select I_item        item,
                            I_action_type action_type,
                            'N'           base_extracted_ind,
                            il.loc        loc,
                            il.loc_type   loc_type
                       from item_loc il
                      where il.item=I_item
                        and il.loc_type = 'S'
                   UNION
                     select I_item        item,
                            I_action_type action_type,
                            'N'           base_extracted_ind,
                            il.loc        loc,
                            il.loc_type  loc_type
                      from item_loc il 
                     where il.item=I_item_parent
                       and I_item_level>I_tran_level
                       and il.loc_type = 'S') inner
                on (    ies.item = inner.item
                    and ies.action_type = inner.action_type
                    and ies.base_extracted_ind = 'N'
                    and ies.loc=inner.loc
                    and ies.loc_type=inner.loc_type
                    )
               when NOT matched then
                  insert(seq_no,
                         item,
                         action_type,
                         base_extracted_ind,
                         loc,
                         loc_type,
                         transaction_datetime)
                  values(ITEM_EXPORT_SEQ.nextval,
                         inner.item,
                         inner.action_type,
                         inner.base_extracted_ind,
                         inner.loc,
                         inner.loc_type,
                         SYSDATE);
      end if;
   elsif I_action_type = ITEM_DEL then
      open C_ITEM;
      fetch C_ITEM into L_action_type;
      close C_ITEM;

      if L_info_exist = 'N' then
         delete from item_export_info
          where item = I_item
            and base_extracted_ind = 'N'
            and process_id is null;
      end if;

      if (L_action_type = ITEM_UPD 
         or L_action_type is NULL) and L_info_exist = 'Y' then

         if L_action_type = ITEM_UPD then
            delete from item_export_stg
             where item = I_item 
               and action_type = ITEM_UPD
               and base_extracted_ind = 'N';
         end if;

         insert into item_export_stg(seq_no,
                                     item,
                                     item_parent,
                                     item_grandparent,
                                     item_level,
                                     tran_level,
                                     action_type,
                                     base_extracted_ind,
                                     transaction_datetime)
                              values(ITEM_EXPORT_SEQ.nextval,
                                     I_item,
                                     I_item_parent,
                                     I_item_grandparent,
                                     I_item_level,
                                     I_tran_level,
                                     I_action_type,
                                     'N',
                                     SYSDATE);
      /*for upc items only*/
      if L_item_loc_parent_exist='Y'  then
         insert into item_export_stg(seq_no,
                                     item,
                                     item_parent,
                                     item_grandparent,
                                     item_level,
                                     tran_level,
                                     action_type,
                                     base_extracted_ind,
                                     transaction_datetime)
                              (select ITEM_EXPORT_SEQ.nextval,
                                      I_item,
                                      I_item_parent,
                                      I_item_grandparent,
                                      I_item_level,
                                      I_tran_level,
                                      'itemupcdel',
                                      'N',
                                      SYSDATE
                                  from dual
                                  where I_item_level>I_tran_level);
      end if;
      
         delete from item_export_info
          where item = I_item;
      end if;
   elsif (I_action_type = VATITM_CRE
          or I_action_type = VATITM_UPD) then
          merge into item_export_stg ies
               using (select I_item            item,
                             I_action_type     action_type,
                             I_vat_region      vat_region,
                             I_vat_code        vat_code,
                             I_vat_type        vat_type,
                             I_vat_active_date vat_active_date,
                             'N'               base_extracted_ind
                             from dual
                     ) inner
                  on (    ies.item = inner.item
                      and ies.vat_region = inner.vat_region
                      and ies.vat_code = inner.vat_code
                      and ies.vat_type = inner.vat_type
                      and ies.vat_active_date = inner.vat_active_date
                      and ies.action_type in (VATITM_CRE,VATITM_UPD)
                      and ies.base_extracted_ind = 'N'
                     )
                when NOT matched then
                   insert(seq_no,
                          item,
                          vat_region,
                          vat_code,
                          vat_type,
                          vat_active_date,
                          action_type,
                          base_extracted_ind,
                          transaction_datetime)
                   values(ITEM_EXPORT_SEQ.nextval,
                          inner.item,
                          inner.vat_region,
                          inner.vat_code,
                          inner.vat_type,
                          inner.vat_active_date,
                          inner.action_type,
                          inner.base_extracted_ind,
                          SYSDATE);
   elsif I_action_type = VATITM_DEL then
      open C_VATITM;
      fetch C_VATITM into L_action_type;
      close C_VATITM;

      if L_action_type = VATITM_CRE then
         delete from item_export_stg
          where item = I_item
            and vat_region = I_vat_region
            and vat_code = I_vat_code
            and vat_active_date = I_vat_active_date
            and vat_type = I_vat_type
            and action_type = VATITM_CRE
            and base_extracted_ind = 'N';

      elsif (L_action_type = VATITM_UPD
            or L_action_type is NULL) and L_info_exist = 'Y' then

            if L_action_type = VATITM_UPD then
               delete from item_export_stg
                where item = I_item
                  and vat_region = I_vat_region
                  and vat_code = I_vat_code
                  and vat_active_date = I_vat_active_date
                  and vat_type = I_vat_type
                  and action_type = VATITM_UPD
                  and base_extracted_ind = 'N';
            end if;

         insert into item_export_stg(seq_no,
                                     item,
                                     vat_region,
                                     vat_code,
                                     vat_type,
                                     vat_active_date,
                                     action_type,
                                     base_extracted_ind,
                                     transaction_datetime)
                              values(ITEM_EXPORT_SEQ.nextval,
                                     I_item,
                                     I_vat_region,
                                     I_vat_code,
                                     I_vat_type,
                                     I_vat_active_date,
                                     I_action_type,
                                     'N',
                                     SYSDATE);
      end if;
   elsif (I_action_type = ITLOC_CRE 
          or I_action_type = ITLOC_UPD) then
          merge into item_export_stg ies
               using (select I_item             item,
                             I_loc              loc,
                             I_loc_type         loc_type,
                             I_action_type      action_type,
                             I_merchandise_ind  merchandise_ind,
                             'N'                base_extracted_ind
                             from dual
                     ) inner
                  on (    ies.item = inner.item
                      and ies.loc = inner.loc
                      and ies.loc_type = inner.loc_type
                      and ies.merchandise_ind = inner.merchandise_ind
                      and ies.action_type in (ITLOC_CRE,ITLOC_UPD)
                      and ies.base_extracted_ind = 'N'
                     )
                when NOT matched then
                   insert(seq_no,
                          item,
                          loc,
                          loc_type,
                          merchandise_ind,
                          action_type,
                          base_extracted_ind,
                          transaction_datetime)
                   values(ITEM_EXPORT_SEQ.nextval,
                          inner.item,
                          inner.loc,
                          inner.loc_type,
                          inner.merchandise_ind,
                          inner.action_type,
                          inner.base_extracted_ind,
                          SYSDATE);

   elsif I_action_type = ITLOC_DEL then
      open C_ITLOC;
      fetch C_ITLOC into L_action_type;
      close C_ITLOC;

      if L_action_type = ITLOC_CRE then
         delete from item_export_stg
          where item = I_item
            and loc = I_loc
            and loc_type = I_loc_type
            and base_extracted_ind = 'N'
            and action_type = ITLOC_CRE;

      elsif (L_action_type = ITLOC_UPD
            or L_action_type is NULL) and L_info_exist = 'Y' then

            if L_action_type = ITLOC_UPD then
               delete from item_export_stg
                where item = I_item
                  and loc = I_loc
                  and loc_type = I_loc_type
                  and base_extracted_ind = 'N'
                  and action_type = ITLOC_UPD;
            end if;

         insert into item_export_stg(seq_no,
                                     item,
                                     loc,
                                     loc_type,
                                     merchandise_ind,
                                     action_type,
                                     base_extracted_ind,
                                     transaction_datetime)
                              values(ITEM_EXPORT_SEQ.nextval,
                                     I_item,
                                     I_loc,
                                     I_loc_type,
                                     I_merchandise_ind,
                                     I_action_type,
                                     'N',
                                     SYSDATE);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;  
END INS_ITEM_EXPORT_STG;
----------------------------------------------------------------------------------
FUNCTION INS_VAT_EXPORT_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_action_type     IN       VAT_EXPORT_STG.ACTION_TYPE%TYPE,
                            I_vat_region      IN       VAT_REGION.VAT_REGION%TYPE,
                            I_vat_code        IN       VAT_CODES.VAT_CODE%TYPE,
                            I_active_date     IN       VAT_CODE_RATES.ACTIVE_DATE%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(65):= 'DATA_EXPORT_SQL.INS_VAT_EXPORT_STG';

   L_vat_region        VAT_REGION.VAT_REGION%TYPE;
   L_vat_code          VAT_CODES.VAT_CODE%TYPE;
   L_active_date       VAT_CODE_RATES.ACTIVE_DATE%TYPE;
   L_action_type       VAT_EXPORT_STG.ACTION_TYPE%TYPE;

   cursor C_VAT_INFO is
      select vr.vat_code,
             vr.active_date
        from vat_codes vc,
             vat_code_rates vr
       where vc.vat_code = vr.vat_code
       order by vr.vat_code,
                vr.active_date;

   cursor C_VAT_REGION is
      select vat_region
        from vat_region
       where vat_calc_type <> 'E';

   cursor C_VAT_CODE_RATES is
      select active_date
        from vat_code_rates
       where vat_code = I_vat_code;

   cursor C_VAT_EXISTS is
      select action_type
        from vat_export_stg
       where vat_region = L_vat_region
         and vat_code = L_vat_code
         and active_date = L_active_date
         and base_extracted_ind = 'N';
BEGIN
   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_action_type = VAT_CRE then
      if I_vat_region is NOT NULL then
         FOR rec in C_VAT_INFO
         LOOP
            insert into vat_export_stg(seq_no,
                                       vat_region,
                                       vat_code,
                                       active_date,
                                       action_type,
                                       base_extracted_ind,
                                       transaction_datetime)
                                values(VAT_EXPORT_SEQ.nextval,
                                       I_vat_region,
                                       rec.vat_code,
                                       rec.active_date,
                                       I_action_type,
                                       'N',
                                       SYSDATE);
         END LOOP;
      elsif I_vat_region is NULL and
            I_vat_code is NOT NULL and
            I_active_date is NOT NULL then
         FOR rec in C_VAT_REGION
         LOOP
            insert into vat_export_stg(seq_no,
                                       vat_region,
                                       vat_code,
                                       active_date,
                                       action_type,
                                       base_extracted_ind,
                                       transaction_datetime)
                                values(VAT_EXPORT_SEQ.nextval,
                                       rec.vat_region,
                                       I_vat_code,
                                       I_active_date,
                                       I_action_type,
                                       'N',
                                       SYSDATE);
         END LOOP;
      end if;
   elsif I_action_type = VAT_UPD then
      if I_vat_region is NOT NULL then
         FOR rec in C_VAT_INFO
         LOOP
            merge into vat_export_stg ves
                using(select I_vat_region    vat_region,
                             rec.vat_code    vat_code,
                             rec.active_date active_date,
                             I_action_type   action_type,
                             'N'             base_extracted_ind
                        from dual
                     ) inner
                  on (    ves.vat_region  = inner.vat_region
                      and ves.vat_code    = inner.vat_code
                      and ves.active_date = inner.active_date
                      and ves.action_type in (VAT_CRE,VAT_UPD)
                      and ves.base_extracted_ind = 'N'
                     )
                when NOT matched then
                   insert(seq_no,
                          vat_region,
                          vat_code,
                          active_date,
                          action_type,
                          base_extracted_ind,
                          transaction_datetime)
                   values(VAT_EXPORT_SEQ.nextval,
                          inner.vat_region,
                          inner.vat_code,
                          inner.active_date,
                          inner.action_type,
                          inner.base_extracted_ind,
                          SYSDATE);
         END LOOP;
      elsif I_vat_region is NULL and
            I_vat_code is NOT NULL and
            I_active_date is NULL then
         FOR rec in C_VAT_REGION
         LOOP
            FOR i in C_VAT_CODE_RATES
            LOOP
               merge into vat_export_stg ves
                   using(select rec.vat_region  vat_region,
                                I_vat_code      vat_code,
                                i.active_date   active_date,
                                I_action_type   action_type,
                                'N'             base_extracted_ind
                           from dual
                         ) inner
                      on (    ves.vat_region  = inner.vat_region
                          and ves.vat_code    = inner.vat_code
                          and ves.active_date = inner.active_date 
                          and ves.action_type in (VAT_CRE,VAT_UPD)
                          and ves.base_Extracted_ind = 'N'
                          )
                   when NOT matched then
                      insert(seq_no,
                             vat_region,
                             vat_code,
                             active_date,
                             action_type,
                             base_extracted_ind,
                             transaction_datetime)
                      values(VAT_EXPORT_SEQ.nextval,
                             inner.vat_region,
                             inner.vat_code,
                             inner.active_date,
                             inner.action_type,
                             'N',
                             SYSDATE);
            END LOOP;
         END LOOP;
      elsif I_vat_region is NULL and
            I_vat_code is NOT NULL and
            I_active_date is NOT NULL then
         FOR rec in C_VAT_REGION
         LOOP
            merge into vat_export_stg ves
                   using(select rec.vat_region  vat_region,
                                I_vat_code      vat_code,
                                I_active_date   active_date,
                                I_action_type   action_type,
                                'N'             base_extracted_ind
                           from dual
                         ) inner
                      on (    ves.vat_region  = inner.vat_region
                          and ves.vat_code    = inner.vat_code
                          and ves.active_date = inner.active_date 
                          and ves.action_type in (VAT_CRE,VAT_UPD)
                          and ves.base_Extracted_ind = 'N'
                          )
                   when NOT matched then
                      insert(seq_no,
                             vat_region,
                             vat_code,
                             active_date,
                             action_type,
                             base_extracted_ind,
                             transaction_datetime)
                      values(VAT_EXPORT_SEQ.nextval,
                             inner.vat_region,
                             inner.vat_code,
                             inner.active_date,
                             inner.action_type,
                             'N',
                             SYSDATE);
         END LOOP;
      end if;
   elsif I_action_type = VAT_DEL then
      if I_vat_region is NOT NULL then
         FOR rec in C_VAT_INFO
         LOOP
            L_vat_region   := I_vat_region;
            L_vat_code     := rec.vat_code;
            L_active_date  := rec.active_date;

            open C_VAT_EXISTS;
            fetch C_VAT_EXISTS into L_action_type;
            close C_VAT_EXISTS;

            if L_action_type is NOT NULL then
               if L_action_type in (VAT_CRE,VAT_UPD) then
                  delete from vat_export_stg
                        where vat_region   = I_vat_region
                          and vat_code     = L_vat_code
                          and active_date  = L_active_date
                          and action_type in (VAT_CRE,VAT_UPD)
                          and base_extracted_ind = 'N';
               end if;
            end if;

            if L_action_type is NULL or
               L_action_type = VAT_UPD then
               insert into vat_export_stg(seq_no,
                                          vat_region,
                                          vat_code,
                                          active_date,
                                          action_type,
                                          base_extracted_ind,
                                          transaction_datetime)
                                   values(VAT_EXPORT_SEQ.nextval,
                                          I_vat_region,
                                          L_vat_code,
                                          L_active_date,
                                          I_action_type,
                                          'N',
                                          SYSDATE);
            end if;
         END LOOP;
      elsif I_vat_region is NULL then
            FOR rec in C_VAT_REGION
            LOOP
               L_vat_region    := rec.vat_region;
               L_vat_code      := I_vat_code;
               L_active_date   := I_active_date;

               if I_vat_code is NOT NULL and
                  I_active_date is NOT NULL then
                  open C_VAT_EXISTS;
                  fetch C_VAT_EXISTS into L_action_type;
                  close C_VAT_EXISTS;

                  if L_action_type is NOT NULL then
                     if L_action_type in (VAT_CRE,VAT_UPD) then
                        delete from vat_export_stg
                              where vat_region   = L_vat_region
                                and vat_code     = L_vat_code
                                and active_date  = L_active_date
                                and action_type in (VAT_CRE,VAT_UPD)
                                and base_extracted_ind = 'N';
                     end if;
                  end if;

                  if L_action_type is NULL or
                     L_action_type = VAT_UPD then
                     insert into vat_export_stg(seq_no,
                                                vat_region,
                                                vat_code,
                                                active_date,
                                                action_type,
                                                base_extracted_ind,
                                                transaction_datetime)
                                         values(VAT_EXPORT_SEQ.nextval,
                                                L_vat_region,
                                                L_vat_code,
                                                L_active_date,
                                                I_action_type,
                                                'N',
                                                SYSDATE);
                  end if;
               end if;
            END LOOP;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INS_VAT_EXPORT_STG;
----------------------------------------------------------------------------------
FUNCTION INS_RELITEM_EXPORT_STG(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_action_type       IN       VAT_EXPORT_STG.ACTION_TYPE%TYPE,
                                I_relationship_id   IN       RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE,
                                I_item              IN       RELATED_ITEM_HEAD.ITEM%TYPE,
                                I_related_item      IN       RELATED_ITEM_DETAIL.RELATED_ITEM%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(65):= 'DATA_EXPORT_SQL.INS_RELITEM_EXPORT_STG';

   L_action_type   RELITEM_EXPORT_STG.ACTION_TYPE%TYPE;

   cursor C_RELITEMHDR_EXISTS is
      select action_type
        from relitem_export_stg
       where relationship_id = I_relationship_id
         and item = I_item
         and base_extracted_ind = 'N';

   cursor C_RELITEMDTL_EXISTS is
      select action_type
        from relitem_export_stg
       where relationship_id = I_relationship_id
         and related_item = I_related_item
         and base_extracted_ind = 'N';

BEGIN
   if I_action_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_relationship_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_relationship_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_action_type = RELITMHDR_CRE
      or I_action_type= RELITMHDR_UPD then
      merge into relitem_export_stg res
           using ( select I_relationship_id      relationship_id,
                          I_item                 item,
                          I_action_type          action_type,
                          'N'                    base_extracted_ind
                     from dual
                  ) inner
               on (    res.relationship_id   = inner.relationship_id
                   and res.item              = inner.item
                   and res.action_type in (RELITMHDR_CRE,RELITMHDR_UPD)
                   and res.base_extracted_ind = inner.base_extracted_ind
                   )
            when NOT matched then
               insert(seq_no,
                      relationship_id,
                      item,
                      action_type,
                      base_extracted_ind,
                      transaction_datetime)
                values(RELITEM_EXPORT_SEQ.nextval,
                       inner.relationship_id,
                       inner.item,
                       inner.action_type,
                       inner.base_extracted_ind,
                       SYSDATE);
   elsif I_action_type = RELITMHDR_DEL then
      open C_RELITEMHDR_EXISTS;
      fetch C_RELITEMHDR_EXISTS into L_action_type;
      close C_RELITEMHDR_EXISTS;

      if L_action_type = RELITMHDR_CRE
         or L_action_type = RELITMHDR_UPD then
         delete from relitem_export_stg
               where relationship_id = I_relationship_id
                 and item = I_item
                 and action_type in (RELITMHDR_CRE,RELITMHDR_UPD)
                 and base_extracted_ind = 'N';
      end if;

      insert into relitem_export_stg(seq_no,
                                     relationship_id,
                                     item,
                                     action_type,
                                     base_extracted_ind,
                                     transaction_datetime)
                              values(RELITEM_EXPORT_SEQ.nextval,
                                     I_relationship_id,
                                     I_item,
                                     I_action_type,
                                     'N',
                                     SYSDATE);
   elsif I_action_type = RELITMDTL_CRE
         or I_action_type = RELITMDTL_UPD then
      merge into relitem_export_stg res
          using ( select I_relationship_id  relationship_id,
                         I_related_item     related_item,
                         I_action_type      action_type,
                         'N'                base_extracted_ind
                     from dual
                 ) inner
             on (    res.relationship_id = inner.relationship_id
                 and res.related_item    = inner.related_item
                 and res.action_type in (RELITMDTL_CRE,RELITMDTL_UPD)
                 and res.base_extracted_ind = inner.base_extracted_ind
                 )
           when NOT matched then
              insert(seq_no,
                     relationship_id,
                     related_item,
                     action_type,
                     base_extracted_ind,
                     transaction_datetime)
              values(RELITEM_EXPORT_SEQ.nextval,
                     inner.relationship_id,
                     inner.related_item,
                     inner.action_type,
                     inner.base_extracted_ind,
                     SYSDATE);
   elsif I_action_type = RELITMDTL_DEL then
      open C_RELITEMDTL_EXISTS;
      fetch C_RELITEMDTL_EXISTS into L_action_type;
      close C_RELITEMDTL_EXISTS;
      if L_action_type = RELITMDTL_CRE
         or L_action_type = RELITMDTL_UPD then
         delete from relitem_export_stg
               where relationship_id = I_relationship_id
                 and related_item = I_related_item
                 and action_type in (RELITMDTL_CRE,RELITMDTL_UPD)
                 and base_extracted_ind = 'N';
      end if;

      insert into relitem_export_stg(seq_no,
                                     relationship_id,
                                     related_item,
                                     action_type,
                                     base_extracted_ind,
                                     transaction_datetime)
                              values(RELITEM_EXPORT_SEQ.nextval,
                                     I_relationship_id,
                                     I_related_item,
                                     I_action_type,
                                     'N',
                                     SYSDATE);
   end if;

   return TRUE;

EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END INS_RELITEM_EXPORT_STG;
----------------------------------------------------------------------------------
FUNCTION PURGE_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(65):= 'DATA_EXPORT_SQL.PURGE_STG';

   L_max_date          DATE;
   L_part_date         DATE;
   L_table_owner       VARCHAR2(20);
   L_table             VARCHAR2(25);
   L_interval_clause   VARCHAR2(500);
   L_interval          VARCHAR2(3);
   L_partition_name    VARCHAR2(30);
   L_high_value        long;
   L_date_diff         VARCHAR2(1);
   L_interval_ind      VARCHAR2(1) := 'N';
   sql_stmt            VARCHAR2(200);
   L_counter           NUMBER(8);

   cursor C_PMAX_DATE is
      select TO_DATE(TO_CHAR(p.vdate-7,'YYYYMMDD'),'YYYYMMDD')
        from period p;

   cursor C_TBL is
      select tbl_name.tbl table_name,
             tbl_owner.owner table_owner
        from (select 'ORGHIER_EXPORT_STG' tbl from dual
              union
              select 'MERCHHIER_EXPORT_STG' tbl from dual
              union
              select 'DIFFS_EXPORT_STG' tbl from dual
              union
              select 'DIFFGRP_EXPORT_STG' tbl from dual
              union
              select 'STORE_EXPORT_STG' tbl from dual
              union
              select 'DATA_EXPORT_HIST' tbl from dual
              union
              select 'ITEM_EXPORT_STG' tbl from dual
              union
              select 'RELITEM_EXPORT_STG' tbl from dual) tbl_name,
             (select TABLE_OWNER owner from SYSTEM_CONFIG_OPTIONS where rownum = 1) tbl_owner;

   cursor C_GET_INTERVAL_INFO(tbl VARCHAR2,ownr VARCHAR2) is
      select interval
        from all_part_tables
       where owner = ownr
         and table_name = tbl;

   cursor C_PARTION_INFO(tbl VARCHAR2,ownr VARCHAR2) is
      select partition_name,
             high_value,
             interval
        from all_tab_partitions
       where table_owner = ownr
         and table_name = tbl
       order by partition_position;

   cursor C_PARTION_COUNT(tbl VARCHAR2,ownr VARCHAR2) is
      select count(1)
        from all_tab_partitions
       where table_owner = ownr
         and table_name = tbl;

   cursor C_DIFF is
      select 'Y'
        from dual
      where L_max_date > L_part_date;
BEGIN
   open C_PMAX_DATE;
   fetch C_PMAX_DATE into L_max_date;
   close C_PMAX_DATE;

   FOR rec in C_TBL
   LOOP
      L_table       := rec.table_name;
      L_table_owner := rec.table_owner;
      open C_GET_INTERVAL_INFO(L_table,L_table_owner);
      fetch C_GET_INTERVAL_INFO into L_interval_clause;
      close C_GET_INTERVAL_INFO;

      if L_interval_clause is NOT NULL then
         L_interval_ind := 'Y';
      end if;
      FOR part in C_PARTION_INFO(L_table,L_table_owner)
      LOOP
         L_date_diff      := 'N';
         L_counter        := 0;
         L_partition_name := part.partition_name;
         L_interval       := part.interval;
         L_high_value     := part.high_value;

         open C_PARTION_COUNT(L_table,L_table_owner);
         fetch C_PARTION_COUNT into L_counter;
         close C_PARTION_COUNT;

         EXECUTE IMMEDIATE 'SELECT ' || L_high_value || ' FROM dual' INTO L_part_date;

         open C_DIFF;
         fetch C_DIFF into L_date_diff;
         close C_DIFF;

         if L_counter > 1 then
           if L_date_diff = 'Y' then
              if (L_interval = 'YES' 
                  or L_interval = 'NO')
                 and L_interval_ind = 'Y' then
                 sql_stmt := 'ALTER TABLE '||L_table_owner||'.'||L_table||' SET INTERVAL ()';
                 EXECUTE IMMEDIATE sql_stmt;
                 sql_stmt := 'ALTER TABLE '||L_table_owner||'.'||L_table||' DROP PARTITION '||L_partition_name||' UPDATE INDEXES';
                 EXECUTE IMMEDIATE sql_stmt;
                 sql_stmt := 'ALTER TABLE '||L_table_owner||'.'||L_table||' SET INTERVAL ('||L_interval_clause||')';
                 EXECUTE IMMEDIATE sql_stmt;
              elsif L_interval_ind = 'N' then
                 sql_stmt := 'ALTER TABLE '||L_table_owner||'.'||L_table||' DROP PARTITION '||L_partition_name||' UPDATE INDEXES';
                 EXECUTE IMMEDIATE sql_stmt;
              end if;
           end if;
         end if;
      END LOOP;
   END LOOP;

   return TRUE;

EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_STG;
----------------------------------------------------------------------------------
END DATA_EXPORT_SQL;
/