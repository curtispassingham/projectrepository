
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_UDA AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
PROCEDURE ADDTOQ(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 I_message_type IN  UDA_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_uda_id       IN  UDA.UDA_ID%TYPE,
                 I_uda_value    IN  UDA_VALUES.UDA_VALUE%TYPE,
                 I_display_type IN  UDA_MFQUEUE.DISPLAY_TYPE%TYPE,
                 I_message      IN  CLOB);
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code  OUT VARCHAR2,
                 O_error_msg    OUT VARCHAR2,
                 O_message_type OUT UDA_MFQUEUE.MESSAGE_TYPE%TYPE,
                 O_message      OUT CLOB,
                 O_uda_id       OUT UDA.UDA_ID%TYPE,
                 O_uda_value    OUT UDA_VALUES.UDA_VALUE%TYPE,
                 O_display_type OUT UDA_MFQUEUE.DISPLAY_TYPE%TYPE);
--------------------------------------------------------------------------------
END RMSMFM_UDA;
/
