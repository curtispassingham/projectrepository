CREATE OR REPLACE PACKAGE ADDRESS_SQL AS

--- Types
TYPE type_address_tbl       is TABLE OF ADDR%ROWTYPE INDEX BY BINARY_INTEGER;
--- Constants
ST        CONSTANT VARCHAR2(12) := 'STATE';
CNTRY     CONSTANT VARCHAR2(12) := 'COUNTRY';
JURIS     CONSTANT VARCHAR2(12) := 'JURISDICTION';


---------------------------------------------------------------------------------
-- Function Name: GET_INFO
-- Purpose:       This function retrieves address information from the
--                ADDR table, given the add_key and returns them to 
--                the calling form.          
----------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message          IN OUT VARCHAR2,
                  O_module                 IN OUT ADDR.MODULE%TYPE,
                  O_key_value_1            IN OUT ADDR.KEY_VALUE_1%TYPE,
                  O_key_value_2            IN OUT ADDR.KEY_VALUE_2%TYPE,
                  O_addr_type              IN OUT ADDR.ADDR_TYPE%TYPE,
                  O_primary_addr_ind       IN OUT ADDR.PRIMARY_ADDR_IND%TYPE,
                  O_add_1                  IN OUT ADDR.ADD_1%TYPE,
                  O_add_2                  IN OUT ADDR.ADD_2%TYPE,
                  O_add_3                  IN OUT ADDR.ADD_3%TYPE,
                  O_city                   IN OUT ADDR.CITY%TYPE,
                  O_state                  IN OUT ADDR.STATE%TYPE,
                  O_country_id             IN OUT ADDR.COUNTRY_ID%TYPE,
                  O_post                   IN OUT ADDR.POST%TYPE,
                  O_jurisdiction_code      IN OUT ADDR.JURISDICTION_CODE%TYPE,
                  O_contact_name           IN OUT ADDR.CONTACT_NAME%TYPE,
                  O_contact_phone          IN OUT ADDR.CONTACT_PHONE%TYPE,
                  O_contact_telex          IN OUT ADDR.CONTACT_TELEX%TYPE,
                  O_contact_fax            IN OUT ADDR.CONTACT_FAX%TYPE,
                  O_contact_email          IN OUT ADDR.CONTACT_EMAIL%TYPE,
                  O_oracle_vendor_site_id  IN OUT ADDR.ORACLE_VENDOR_SITE_ID%TYPE,
                  I_add_key                IN     ADDR.ADDR_KEY%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--FUNCTION Name: VALID_INVC_ADDR
--Purpose:       This funtion will validate an invoice address that associated with 
--               the given supplier number or a partner ID of a given entity
--               Feb-15-2000 by Hien Tran
------------------------------------------------------------------------------------
FUNCTION VALID_INVC_ADDR(O_error_message          IN OUT VARCHAR2,
                          O_exists                IN OUT BOOLEAN,
                          I_supplier              IN     SUPS.SUPPLIER%TYPE,
                          I_partner_id            IN     PARTNER.PARTNER_ID%TYPE,
                          I_partner_type          IN     PARTNER.PARTNER_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--FUNCTION Name: VALID_ORD_ADDR
--Purpose:       This funtion will validate an order address that associated with 
--               the given supplier number of a given entity
--               Nov-6-2000 by RETEK
------------------------------------------------------------------------------------
FUNCTION VALID_ORD_ADDR(O_error_message          IN OUT VARCHAR2,
                        O_exists                 IN OUT BOOLEAN,
                        I_supplier               IN     SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--FUNCTION NAME: PREV_EXISTS
--Purpose:       This function will count the number of instances of a 
--               particular address type.
--               January 17-2002
------------------------------------------------------------------------------------    
FUNCTION PREV_EXISTS(O_error_message       IN OUT VARCHAR2,
                     O_exists              IN OUT BOOLEAN,
                     I_key_value_1         IN     ADDR.KEY_VALUE_1%TYPE,
                     I_key_value_2         IN     ADDR.KEY_VALUE_2%TYPE,
                     I_addr_type           IN     ADDR.ADDR_TYPE%TYPE,
                     I_seq_no              IN     ADDR.SEQ_NO%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--FUNCTION NAME: DELETE_ALLOWED
--Purpose:       This function is called if the address type is 02 (Postal) 
--               or 06 (Remittance). It then checks 2 things. If the address being
--               deleted is the only instance of its type then it can be deleted 
--               even if the primary address indicator is checked. If there are 
--               multiple instances then one of the other instances needs to be set 
--               as the primary before the current one can be deleted.
--
--               January 17-2002 
-------------------------------------------------------------------------------------
FUNCTION DELETE_ALLOWED(O_error_message       IN  OUT VARCHAR2,
                        O_allowed             IN  OUT BOOLEAN,
                        I_key_value_1         IN      ADDR.KEY_VALUE_1%TYPE,
                        I_key_value_2         IN      ADDR.KEY_VALUE_2%TYPE,
                        I_addr_type           IN      ADDR.ADDR_TYPE%TYPE,
                        I_seq_no              IN      ADDR.SEQ_NO%TYPE,
                        I_primary_addr_ind    IN      ADDR.PRIMARY_ADDR_IND%TYPE,
                        I_module              IN      ADDR.MODULE%TYPE,
                        I_mandatory          IN     ADD_TYPE_MODULE.MANDATORY_IND%TYPE
                        )
RETURN BOOLEAN;

-------------------------------------------------------------------------------------
--FUNCTION NAME: UPDATE_PRIM_ADD_IND
--Purpose:       This new function, to be added to the existing ADDRESS_SQL 
--               package, will be required to update the former primary address' 
--               primary address indicator to 'N'.
--               February 6 - 2004
-------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIM_ADD_IND(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_module           IN     ADDR.MODULE%TYPE,
                             I_key_value_1      IN     ADDR.KEY_VALUE_1%TYPE,
                             I_key_value_2      IN     ADDR.KEY_VALUE_2%TYPE,
                             I_addr_type        IN     ADDR.ADDR_TYPE%TYPE,
                             I_seq_no           IN     ADDR.SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--FUNCTION NAME: GET_MAX_SEQ_NO
--Purpose:       This new function, to be added to the existing ADDRESS_SQL 
--               package, will be required to return the max sequence number 
--               that is already being used by the store/warehouse/partner/supplier 
--               for that particular address type.
--               February 6 - 2004
-------------------------------------------------------------------------------------
FUNCTION GET_MAX_SEQ_NO(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_max_seq_no       IN OUT ADDR.SEQ_NO%TYPE,
                        I_module           IN     ADDR.MODULE%TYPE,
                        I_key_value_1      IN     ADDR.KEY_VALUE_1%TYPE,
                        I_key_value_2      IN     ADDR.KEY_VALUE_2%TYPE,
                        I_addr_type        IN     ADDR.ADDR_TYPE%TYPE)
RETURN BOOLEAN;   
-------------------------------------------------------------------------------------
--FUNCTION NAME: GET_ADD_TYPE_MODULE_ROW
--Purpose:       This is a new function that will retrieve the address type and 
--               module from the ADDR table.
-------------------------------------------------------------------------------------
FUNCTION GET_ADD_TYPE_MODULE_ROW(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_add_type_module  IN OUT ADD_TYPE_MODULE%ROWTYPE,
                                 I_module           IN     ADD_TYPE_MODULE.MODULE%TYPE,
                                 I_addr_type        IN     ADD_TYPE_MODULE.ADDRESS_TYPE%TYPE)
RETURN BOOLEAN;   
-------------------------------------------------------------------------------------
--FUNCTION NAME: DELETE_ADDRESSES
--Purpose:       This new function, to be added to the existing ADDRESS_SQL 
--               package, will be required to delete address data from the 
--               ADDR table for each address to be deleted, (as passed in by 
--               the DELETE_ALLOWED function.) 
-------------------------------------------------------------------------------------
FUNCTION DELETE_ADDRESSES(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_key_value_1        IN     ADDR.KEY_VALUE_1%TYPE,
                          I_key_value_2        IN     ADDR.KEY_VALUE_2%TYPE,
                          I_addr_type          IN     ADDR.ADDR_TYPE%TYPE,
                          I_seq_no             IN     ADDR.SEQ_NO%TYPE,
                          I_primary_addr_ind   IN     ADDR.PRIMARY_ADDR_IND%TYPE,
                          I_module             IN     ADDR.MODULE%TYPE,
                          I_mandatory          IN     ADD_TYPE_MODULE.MANDATORY_IND%TYPE
                          )
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--FUNCTION NAME: MAND_ADD_EXISTS
--Purpose:       This new function, to be added to the existing ADDRESS_SQL 
--               package, will be called by the new address maintenance form 
--               when the user attempts to exit after adding one or more addresses.  
--               The function will be required to ensure that at least one address 
--               exists for each mandatory address type for the module in the ADDR 
--               table. If a record does not exist, an error message must be 
--               displayed.
-------------------------------------------------------------------------------------
FUNCTION MAND_ADD_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_address_type      IN OUT ADDR.ADDR_TYPE%TYPE,
                         I_module            IN     ADDR.MODULE%TYPE,
                         I_key_value_1       IN     ADDR.KEY_VALUE_1%TYPE,
                         I_key_value_2       IN     ADDR.KEY_VALUE_2%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------------
--FUNCTION NAME: GET_ADDR_KEY
--Purpose:       This function returns the primary key that identifies the address. 
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ADDR_KEY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_addr_key        OUT      ADDR.ADDR_KEY%TYPE,
                      I_Store           IN       STORE.STORE%TYPE,
                      I_module          IN       ADD_TYPE_MODULE.MODULE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--FUNCTION NAME: GET_ADDR_KEY
--Purpose:       This function returns the address key that identifies the address given the external_ref_id. 
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ADDR_KEY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_addr_key          IN OUT   ADDR.ADDR_KEY%TYPE,
                      I_external_ref_id   IN       ADDR.EXTERNAL_REF_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--FUNCTION NAME: GET_PRIMARY_ADDR_TYPE
--Purpose:       This function returns the primary address type for a particular module
--               from the add_type_module table.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_ADDR_TYPE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_primary_addr_type   OUT      ADD_TYPE_MODULE.ADDRESS_TYPE%TYPE,
                               I_module              IN       ADD_TYPE_MODULE.MODULE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--FUNCTION NAME: INSERT_ADDR
--Purpose:       This function inserts records for stores into the addr table from
--               a table of addresses in the STORE_SQL.STORE_REC. 
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       TYPE_ADDRESS_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--FUNCTION NAME: UPDATE_ADDR
--Purpose:       This function updates the basic address columns ofrecords in the addr table from
--               a table of addresses. 
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_address_tbl     IN       TYPE_ADDRESS_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--FUNCTION NAME: LOCK_ADDR
--Purpose:       This function locks records in the addr table for updating.
-----------------------------------------------------------------------------------------------
FUNCTION LOCK_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_addr_key        IN       ADDR.ADDR_KEY%TYPE)

RETURN BOOLEAN;
-------------------------------------------------------------------------------------   
FUNCTION GET_ADDR_TYPE_DESC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_addr_type_desc   IN OUT   ADD_TYPE_TL.TYPE_DESC%TYPE,
                            I_addr_type        IN       ADD_TYPE.ADDRESS_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------   
--FUNCTION NAME: GET_DEFAULT_ADDR_TYPE
--Purpose:       This function return either the primary address_type or 
--               the mandatory address type for a given module.
-------------------------------------------------------------------------------------   
FUNCTION GET_DEFAULT_ADDR_TYPE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_addr_type_desc   IN OUT   ADD_TYPE_TL.TYPE_DESC%TYPE,
                               O_addr_type        IN OUT   ADD_TYPE.ADDRESS_TYPE%TYPE,
                               I_module           IN       ADD_TYPE_MODULE.MODULE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------   
--FUNCTION NAME: CREATE_ALLOWED
--Purpose:       This function will determine if the user is allowed to create addresses 
--               for the given address type
-------------------------------------------------------------------------------------   
FUNCTION CREATE_UPDATE_ALLOWED(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_allowed          IN OUT   BOOLEAN,
                               I_addr_type        IN       ADD_TYPE.ADDRESS_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------   
FUNCTION GET_SUPP_PRIMARY_ADDR_TYPE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_primary_addr_type    OUT   ADD_TYPE_MODULE.ADDRESS_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--FUNCTION NAME: GET_COUNTRY_ID
--Purpose:       This function will return the country ID for the passed in Supplier 
--               and Address Type
-------------------------------------------------------------------------------------   
FUNCTION GET_COUNTRY_ID (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_country_id        IN OUT   ADDR.COUNTRY_ID%TYPE,
                         I_supplier          IN       SUPS.SUPPLIER %TYPE,
                         I_addr_type         IN       ADDR.ADDR_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_add_1           IN OUT   ADDR.ADD_1%TYPE,
                       O_add_2           IN OUT   ADDR.ADD_2%TYPE,
                       O_add_3           IN OUT   ADDR.ADD_3%TYPE,
                       O_city            IN OUT   ADDR.CITY%TYPE,
                       O_state           IN OUT   ADDR.STATE%TYPE,
                       O_country_id      IN OUT   ADDR.COUNTRY_ID%TYPE,
                       O_post            IN OUT   ADDR.POST%TYPE,
                       I_module          IN       ADDR.MODULE%TYPE,
                       I_key_value_1     IN       ADDR.KEY_VALUE_1%TYPE,
                       I_key_value_2     IN       ADDR.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--FUNCTION NAME: CHECK_ADDR
--Purpose:       This function validates the jurisdiction code/state/country combination
--------------------------------------------------------------------------------------------  
FUNCTION CHECK_ADDR (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_country_id          IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                     IO_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                     IO_state               IN OUT   STATE.STATE%TYPE,
                     IO_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                     I_jurisdiction         IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                     IO_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                     IO_dup_exists          IN OUT   VARCHAR2,
                     I_calling_module       IN       VARCHAR2,
                     I_calling_context      IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--FUNCTION NAME: STATE
--Purpose:       This function used so the constant 'ST' can be used in forms.
--------------------------------------------------------------------------------------------  
FUNCTION STATE_CON
RETURN VARCHAR2;
--------------------------------------------------------------------------------------------
--FUNCTION NAME: COUNTRY
--Purpose:       This function used so the constant 'CNTRY' can be used in forms.
--------------------------------------------------------------------------------------------  
FUNCTION COUNTRY_CON
RETURN VARCHAR2;
--------------------------------------------------------------------------------------------
--FUNCTION NAME: JURIS
--Purpose:       This function used so the constant 'JURIS' can be used in forms.
--------------------------------------------------------------------------------------------  
FUNCTION JURIS_CON
RETURN VARCHAR2;
--------------------------------------------------------------------------------------------
--FUNCTION Name: VALID_ADDR
--Purpose:       This funtion will validate if there exists an address for the values passed in.
--------------------------------------------------------------------------------------------
FUNCTION VALID_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists          IN OUT   BOOLEAN,
                    I_module          IN       ADD_TYPE_MODULE.MODULE%TYPE,
                    I_addr_type       IN       ADD_TYPE.ADDRESS_TYPE%TYPE,
                    I_key_value_1     IN       ADDR.KEY_VALUE_1%TYPE,
                    I_key_value_2     IN       ADDR.KEY_VALUE_2%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--FUNCTION Name: GET_PRIM_ADDR
--Purpose:       This funtion will get the primary address for the entity passed in.
--------------------------------------------------------------------------------------------
FUNCTION GET_PRIM_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_add_1           IN OUT   ADDR.ADD_1%TYPE,
                       O_add_2           IN OUT   ADDR.ADD_2%TYPE,
                       O_add_3           IN OUT   ADDR.ADD_3%TYPE,
                       O_city            IN OUT   ADDR.CITY%TYPE,
                       O_state           IN OUT   ADDR.STATE%TYPE,
                       O_country_id      IN OUT   ADDR.COUNTRY_ID%TYPE,
                       O_post            IN OUT   ADDR.POST%TYPE,
                       O_jurisdiction    IN OUT   ADDR.JURISDICTION_CODE%TYPE,
                       I_module          IN       ADDR.MODULE%TYPE,
                       I_key_value_1     IN       ADDR.KEY_VALUE_1%TYPE,
                       I_key_value_2     IN       ADDR.KEY_VALUE_2%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--FUNCTION Name: REFRESH_MV_LOC_PRIM_ADDR
--Purpose:       This funtion will refresh the materialized view MV_LOC_PRIM_ADDR.
--------------------------------------------------------------------------------------------
FUNCTION REFRESH_MV_LOC_PRIM_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STATE_COUNTRY (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_country_id          IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                                 IO_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                                 IO_state               IN OUT   STATE.STATE%TYPE,
                                 IO_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                                 I_jurisdiction         IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                                 IO_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                 IO_count               IN OUT   NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION VALIDATE_JURISDICTION (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_country_id          IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                         IO_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                         IO_state               IN OUT   STATE.STATE%TYPE,
                         IO_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                         I_jurisdiction         IN       COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                         IO_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                         IO_count               IN OUT   NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
                        
END ADDRESS_SQL;
/