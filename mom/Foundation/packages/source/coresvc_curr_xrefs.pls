-- File Name : CORESVC_CURR_XREF_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_CURR_XREF AUTHID CURRENT_USER AS
   template_key                          CONSTANT VARCHAR2(255)         := 'CURR_XREF_DATA';
   action_new                                     VARCHAR2(25)          := 'NEW';
   action_mod                                     VARCHAR2(25)          := 'MOD';
   action_del                                     VARCHAR2(25)          := 'DEL';
   FIF_CURRENCY_XREF_sheet                        VARCHAR2(255)         := 'CURR_XREF';
   FIF_CURRENCY_XREF$ACTION                       NUMBER                :=1;
   FIF_CURR_XREF$RMS_EXCHNG_TYP                   NUMBER                :=3;
   FIF_CURR_XREF$FIF_EXCHNG_TYP                   NUMBER                :=2;
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255)                                       := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE                              := 'RMSFND';
   TYPE FIF_CURRENCY_XREF_rec_tab IS TABLE OF FIF_CURRENCY_XREF%ROWTYPE;
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
						      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
					     I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
END CORESVC_CURR_XREF;
/
