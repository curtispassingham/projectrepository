CREATE OR REPLACE PACKAGE BODY ITEM_LOC_SQL AS

TYPE locs_table_type IS
   TABLE OF STORE.STORE%TYPE INDEX BY BINARY_INTEGER;

TYPE item_loc_attr_rectype IS RECORD
   (loc_type              ITEM_LOC.LOC_TYPE%TYPE,
    primary_supp          ITEM_LOC.PRIMARY_SUPP%TYPE,
    primary_cntry         ITEM_LOC.PRIMARY_CNTRY%TYPE,
    primary_cost_pack     ITEM_LOC.PRIMARY_COST_PACK%TYPE,
    status                ITEM_LOC.STATUS%TYPE,
    local_item_desc       ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
    local_short_desc      ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
    primary_variant       ITEM_LOC.PRIMARY_VARIANT%TYPE,
    unit_retail           ITEM_LOC.UNIT_RETAIL%TYPE,
    ti                    ITEM_LOC.TI%TYPE,
    hi                    ITEM_LOC.HI%TYPE,
    store_ord_mult        ITEM_LOC.STORE_ORD_MULT%TYPE,
    daily_waste_pct       ITEM_LOC.DAILY_WASTE_PCT%TYPE,
    taxable_ind           ITEM_LOC.TAXABLE_IND%TYPE,
    meas_of_each          ITEM_LOC.MEAS_OF_EACH%TYPE,
    meas_of_price         ITEM_LOC.MEAS_OF_PRICE%TYPE,
    uom_of_price          ITEM_LOC.UOM_OF_PRICE%TYPE,
    selling_unit_retail   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
    selling_uom           ITEM_LOC.SELLING_UOM%TYPE,
    receive_as_type       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
    inbound_handling_days ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE,
    source_method         ITEM_LOC.SOURCE_METHOD%TYPE,
    source_wh             ITEM_LOC.SOURCE_WH%TYPE,
    multi_units           ITEM_LOC.MULTI_UNITS%TYPE,
    multi_unit_retail     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
    multi_selling_uom     ITEM_LOC.MULTI_SELLING_UOM%TYPE,
    uin_type              ITEM_LOC.UIN_TYPE%TYPE,
    uin_label             ITEM_LOC.UIN_LABEL%TYPE,
    capture_time          ITEM_LOC.CAPTURE_TIME%TYPE,
    ext_uin_ind           ITEM_LOC.EXT_UIN_IND%TYPE,
    ranged_ind            ITEM_LOC.RANGED_IND%TYPE,
    costing_loc           ITEM_LOC.COSTING_LOC%TYPE,
    costing_loc_type      ITEM_LOC.COSTING_LOC_TYPE%TYPE
);

-----------------------------------------------------------------
FUNCTION STORE_CLASS_LOCS_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                       O_locs            IN OUT   LOCS_TABLE_TYPE,
                                       I_store_class     IN       STORE.STORE_CLASS%TYPE,
                                       I_currency_code   IN       STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

L_loop_index        NUMBER := 0;

cursor C_GET_LOCS is
   select store
     from v_store
    where store_class   = I_store_class
      and currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.STORE_CLASS_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END STORE_CLASS_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION DISTRICT_LOCS_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                    O_locs            IN OUT   LOCS_TABLE_TYPE,
                                    I_district        IN       STORE.DISTRICT%TYPE,
                                    I_currency_code   IN       STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select store
        from v_store
       where district      = I_district
         and currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.DISTRICT_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END DISTRICT_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION REGION_LOCS_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                  O_locs            IN OUT   LOCS_TABLE_TYPE,
                                  I_region          IN       REGION.REGION%TYPE,
                                  I_currency_code   IN       STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select store
        from v_store
       where region        = I_region
         and currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.REGION_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END REGION_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION AREA_LOCS_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                O_locs            IN OUT   LOCS_TABLE_TYPE,
                                I_area            IN       AREA.AREA%TYPE,
                                I_currency_code   IN       STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

L_loop_index        NUMBER := 0;

cursor C_GET_LOCS is
   select store
     from v_store
    where area     = I_area
      and currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.AREA_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END AREA_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------
FUNCTION TSF_ZONE_LOCS_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                    O_locs            IN OUT   LOCS_TABLE_TYPE,
                                    I_tsf_zone        IN       STORE.TRANSFER_ZONE%TYPE,
                                    I_currency_code   IN       STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select store
        from v_store
       where transfer_zone = I_tsf_zone
         and currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.TSF_ZONE_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END TSF_ZONE_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION LOC_TRAIT_LOCS_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                     O_locs            IN OUT   locs_table_type,
                                     I_loc_trait       IN       loc_traits_matrix.loc_trait%TYPE,
                                     I_currency_code   IN       store.currency_code%TYPE)
return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select l.store
        from loc_traits_matrix l,
             v_store s
       where l.store = s.store
         and l.loc_trait = I_loc_trait
         and s.currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.LOC_TRAIT_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END LOC_TRAIT_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION DEFAULT_WH_LOCS_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                      O_locs            IN OUT   LOCS_TABLE_TYPE,
                                      I_default_wh      IN       STORE.DEFAULT_WH%TYPE,
                                      I_currency_code   IN       STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select store
        from v_store
       where default_wh    = I_default_wh
         and currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.DEFAULT_WH_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END DEFAULT_WH_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION LOC_LIST_ST_LOCS_FOR_CURRENCY(O_error_message   IN   OUT VARCHAR2,
                                       O_locs            IN   OUT LOCS_TABLE_TYPE,
                                       I_loc_list        IN   LOC_LIST_DETAIL.LOC_LIST%TYPE,
                                       I_currency_code   IN   STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select location l
        from loc_list_detail l,
             v_store s
       where l.location = s.store
         and l.loc_list = I_loc_list
         and s.currency_code = nvl(I_currency_code, currency_code)
         and l.loc_type = 'S';

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.LOC_LIST_ST_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END LOC_LIST_ST_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION LOC_LIST_WH_LOCS_FOR_CURRENCY(O_error_message   OUT   VARCHAR2,
                                       O_locs            OUT   LOCS_TABLE_TYPE,
                                       I_loc_list        IN    LOC_LIST_DETAIL.LOC_LIST%TYPE,
                                       I_currency_code   IN    STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select distinct w.wh
        from loc_list_detail l,
             v_wh w
       where (l.location        = w.wh
              or w.physical_wh  = l.location)
         and l.loc_list         = I_loc_list
         and w.currency_code    = nvl(I_currency_code, currency_code)
         and l.loc_type         = 'W'
         and w.stockholding_ind = 'Y';

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;
      fetch C_GET_LOCS into O_locs(L_loop_index);
      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.LOC_LIST_WH_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END LOC_LIST_WH_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION ALL_WAREHOUSES_FOR_CURRENCY(O_error_message   OUT   VARCHAR2,
                                     O_locs            OUT   LOCS_TABLE_TYPE,
                                     I_currency_code   IN    STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select wh
        from v_wh
       where currency_code    = nvl(I_currency_code, currency_code)
         and stockholding_ind = 'Y'
         and NOT EXISTS (select wh
                           from wh_add
                          where wh = v_wh.wh);
BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;
      fetch C_GET_LOCS into O_locs(L_loop_index);
      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.ALL_WAREHOUSES_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ALL_WAREHOUSES_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION ALL_STORES_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                 O_locs            IN OUT   LOCS_TABLE_TYPE,
                                 I_currency_code   IN       STORE.CURRENCY_CODE%TYPE)
return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select store
        from v_store
       where currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;

      fetch C_GET_LOCS into O_locs(L_loop_index);

      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.ALL_STORES_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ALL_STORES_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION ALL_LOCS_FOR_CURRENCY(O_error_message   OUT   VARCHAR2,
                               O_locs            OUT   LOCS_TABLE_TYPE,
                               I_currency_code   IN    STORE.CURRENCY_CODE%TYPE)
   return BOOLEAN IS

   L_loop_index        NUMBER := 0;
   L_sub_loop_index    NUMBER;
   L_exit_greater_loop BOOLEAN := FALSE;

   cursor C_GET_WAREHOUSES is
      select wh
        from v_wh
       where currency_code    = nvl(I_currency_code, currency_code)
         and stockholding_ind = 'Y';

   cursor C_GET_STORES is
      select store
        from v_store
       where currency_code = nvl(I_currency_code, currency_code);

BEGIN
   open C_GET_WAREHOUSES;

   LOOP
      L_loop_index := L_loop_index + 1;
      fetch C_GET_WAREHOUSES into O_locs(L_loop_index);

      if C_GET_WAREHOUSES%NOTFOUND then
         open C_GET_STORES;
         L_sub_loop_index := L_loop_index - 1;

         LOOP
            L_sub_loop_index := L_sub_loop_index + 1;
            fetch C_GET_STORES into O_locs(L_sub_loop_index);

            if C_GET_STORES%NOTFOUND then
               L_exit_greater_loop := TRUE;
               EXIT;
            end if;
         END LOOP;

         close C_GET_STORES;
      end if;

      EXIT WHEN L_exit_greater_loop;
   END LOOP;

   close C_GET_WAREHOUSES;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.ALL_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ALL_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION PHYSICAL_WH_LOCS_FOR_CURRENCY(O_error_message   OUT   VARCHAR2,
                                       O_locs            OUT   LOCS_TABLE_TYPE,
                                       I_ph_wh           IN    WH.PHYSICAL_WH%TYPE,
                                       I_currency_code   IN    STORE.CURRENCY_CODE%TYPE)
   return BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_LOC_SQL.PHYSICAL_WH_LOCS_FOR_CURRENCY';
   L_loop_index   NUMBER       := 0;

   cursor C_GET_LOCS is
      select wh
        from v_wh
       where physical_wh = I_ph_wh
         and currency_code = nvl(I_currency_code, currency_code)
         and stockholding_ind = 'Y';

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_LOCS', 'WH', I_ph_wh);
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;
      fetch C_GET_LOCS into O_locs(L_loop_index);
      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_LOCS', 'WH', I_ph_wh);
   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      RETURN FALSE;
END PHYSICAL_WH_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION ALL_INT_FINISHERS_FOR_CURRENCY(O_error_message   OUT   VARCHAR2,
                                        O_locs            OUT   LOCS_TABLE_TYPE,
                                        I_currency_code   IN    STORE.CURRENCY_CODE%TYPE)
   return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select finisher_id
        from v_internal_finisher
       where currency_code    = nvl(I_currency_code, currency_code);
BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;
      fetch C_GET_LOCS into O_locs(L_loop_index);
      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.ALL_INT_FINISHERS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ALL_INT_FINISHERS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION ALL_EXT_FINISHERS_FOR_CURRENCY(O_error_message   OUT   VARCHAR2,
                                        O_locs            OUT   LOCS_TABLE_TYPE,
                                        I_currency_code   IN    STORE.CURRENCY_CODE%TYPE)
   return BOOLEAN IS

   L_loop_index        NUMBER := 0;

   cursor C_GET_LOCS is
      select finisher_id
        from v_external_finisher
       where currency_code    = nvl(I_currency_code, currency_code);
BEGIN
   open C_GET_LOCS;

   LOOP
      L_loop_index := L_loop_index + 1;
      fetch C_GET_LOCS into O_locs(L_loop_index);
      EXIT WHEN C_GET_LOCS%NOTFOUND;
   END LOOP;

   close C_GET_LOCS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.ALL_EXT_FINISHERS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END ALL_EXT_FINISHERS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GROUP_LOCS_FOR_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                 O_locs            IN OUT   LOCS_TABLE_TYPE,
                                 I_group_type      IN       CODE_DETAIL.CODE%TYPE,
                                 I_group_value     IN       CODE_DETAIL.CODE_DESC%TYPE,
                                 I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE)
return BOOLEAN IS

BEGIN


   if I_group_type in('S', 'W', 'I', 'E') then
      O_locs(1) := I_group_value;
   elsif I_group_type = 'C' then
      if STORE_CLASS_LOCS_FOR_CURRENCY(O_error_message,
                                       O_locs,
                                       I_group_value,
                                       I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'D' then
      if DISTRICT_LOCS_FOR_CURRENCY(O_error_message,
                                    O_locs,
                                    I_group_value,
                                    I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'R' then
      if REGION_LOCS_FOR_CURRENCY(O_error_message,
                                  O_locs,
                                  I_group_value,
                                  I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'A' then
      if AREA_LOCS_FOR_CURRENCY(O_error_message,
                                O_locs,
                                I_group_value,
                                I_currency_code) = FALSE then
         return FALSE;
      end if;


   elsif I_group_type = 'T' then
      if TSF_ZONE_LOCS_FOR_CURRENCY(O_error_message,
                                    O_locs,
                                    I_group_value,
                                    I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'L' then
      if LOC_TRAIT_LOCS_FOR_CURRENCY(O_error_message,
                                     O_locs,
                                     I_group_value,
                                     I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'DW' then
      if DEFAULT_WH_LOCS_FOR_CURRENCY(O_error_message,
                                      O_locs,
                                      I_group_value,
                                      I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'LLS' then
      if LOC_LIST_ST_LOCS_FOR_CURRENCY(O_error_message,
                                       O_locs,
                                       I_group_value,
                                       I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'LLW' then
      if LOC_LIST_WH_LOCS_FOR_CURRENCY(O_error_message,
                                       O_locs,
                                       I_group_value,
                                       I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'AW' then
      if ALL_WAREHOUSES_FOR_CURRENCY(O_error_message,
                                     O_locs,
                                     I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'AS' then
      if ALL_STORES_FOR_CURRENCY(O_error_message,
                                 O_locs,
                                 I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'AL' then
      if ALL_LOCS_FOR_CURRENCY(O_error_message,
                               O_locs,
                               I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'PW' then
      if PHYSICAL_WH_LOCS_FOR_CURRENCY(O_error_message,
                                       O_locs,
                                       I_group_value,
                                       I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'AI' then
      if ALL_INT_FINISHERS_FOR_CURRENCY(O_error_message,
                                        O_locs,
                                        I_currency_code) = FALSE then
         return FALSE;
      end if;
   elsif I_group_type = 'AE' then
      if ALL_EXT_FINISHERS_FOR_CURRENCY(O_error_message,
                                        O_locs,
                                        I_currency_code) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'ITEM_LOC_SQL.GROUP_LOCS_FOR_CURRENCY',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GROUP_LOCS_FOR_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC(O_error_message         IN OUT   VARCHAR2,
                      O_item_parent           IN OUT   ITEM_LOC.ITEM_PARENT%TYPE,
                      O_item_grandparent      IN OUT   ITEM_LOC.ITEM_GRANDPARENT%TYPE,
                      O_loc_type              IN OUT   ITEM_LOC.LOC_TYPE%TYPE,
                      O_unit_retail           IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                      O_selling_unit_retail   IN OUT   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                      O_selling_uom           IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                      O_clear_ind             IN OUT   ITEM_LOC.CLEAR_IND%TYPE,
                      O_taxable_ind           IN OUT   ITEM_LOC.TAXABLE_IND%TYPE,
                      O_local_item_desc       IN OUT   ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                      O_local_short_desc      IN OUT   ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                      O_ti                    IN OUT   ITEM_LOC.TI%TYPE,
                      O_hi                    IN OUT   ITEM_LOC.HI%TYPE,
                      O_store_ord_mult        IN OUT   ITEM_LOC.STORE_ORD_MULT%TYPE,
                      O_status                IN OUT   ITEM_LOC.STATUS%TYPE,
                      O_status_update_date    IN OUT   ITEM_LOC.STATUS_UPDATE_DATE%TYPE,
                      O_daily_waste_pct       IN OUT   ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                      O_meas_of_each          IN OUT   ITEM_LOC.MEAS_OF_EACH%TYPE,
                      O_meas_of_price         IN OUT   ITEM_LOC.MEAS_OF_PRICE%TYPE,
                      O_uom_of_price          IN OUT   ITEM_LOC.UOM_OF_PRICE%TYPE,
                      O_primary_variant       IN OUT   ITEM_LOC.PRIMARY_VARIANT%TYPE,
                      O_primary_supp          IN OUT   ITEM_LOC.PRIMARY_SUPP%TYPE,
                      O_primary_cntry         IN OUT   ITEM_LOC.PRIMARY_CNTRY%TYPE,
                      O_primary_cost_pack     IN OUT   ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                      I_item                  IN       ITEM_LOC.ITEM%TYPE,
                      I_loc                   IN       ITEM_LOC.LOC%TYPE)
return BOOLEAN IS

   L_program VARCHAR2(64) := 'LOC_ITEM_SQL.GET_ITEM_LOC';

   cursor C_GET_ITEM_LOC is
      select item_parent,
             item_grandparent,
             loc_type,
             unit_retail,
             selling_unit_retail,
             selling_uom,
             clear_ind,
             taxable_ind,
             local_item_desc,
             local_short_desc,
             ti,
             hi,
             store_ord_mult,
             status,
             status_update_date,
             daily_waste_pct,
             meas_of_each,
             meas_of_price,
             uom_of_price,
             primary_variant,
             primary_supp,
             primary_cntry,
             primary_cost_pack
        from item_loc
       where item = I_item
         and loc  = I_loc;

BEGIN
   open C_GET_ITEM_LOC;
   fetch C_GET_ITEM_LOC into O_item_parent,
                             O_item_grandparent,
                             O_loc_type,
                             O_unit_retail,
                             O_selling_unit_retail,
                             O_selling_uom,
                             O_clear_ind,
                             O_taxable_ind,
                             O_local_item_desc,
                             O_local_short_desc,
                             O_ti,
                             O_hi,
                             O_store_ord_mult,
                             O_status,
                             O_status_update_date,
                             O_daily_waste_pct,
                             O_meas_of_each,
                             O_meas_of_price,
                             O_uom_of_price,
                             O_primary_variant,
                             O_primary_supp,
                             O_primary_cntry,
                             O_primary_cost_pack;
   close C_GET_ITEM_LOC;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_LOC;
-----------------------------------------------------------------
FUNCTION GET_ITEM_LOC(O_error_message   IN OUT   VARCHAR2,
                      O_item_loc        IN OUT   ITEM_LOC%ROWTYPE,
                      I_item            IN       ITEM_LOC.ITEM%TYPE,
                      I_loc             IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN is

   L_program VARCHAR2(64) := 'LOC_ITEM_SQL.GET_ITEM_LOC';

   cursor C_GET_ITEM_LOC is
      select *
        from item_loc
       where item = I_item
         and loc  = I_loc;

BEGIN
   open C_GET_ITEM_LOC;
   fetch C_GET_ITEM_LOC into O_item_loc;
   close C_GET_ITEM_LOC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_LOC;
-----------------------------------------------------------------
FUNCTION ALL_LOCS_EXIST(O_error_message    IN OUT   VARCHAR2,
                        O_all_locs_exist   IN OUT   BOOLEAN,
                        I_item             IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_dummy VARCHAR2(1);
   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.ALL_LOCS_EXIST';

cursor C_ALL_LOCS_EXIST is
   select 'x'
     from store
    where store not in(select loc
                         from item_loc
                        where item = I_item)
   UNION ALL
   select 'x'
     from wh
    where wh not in(select loc
                      from item_loc
                     where item = I_item);

BEGIN
   open C_ALL_LOCS_EXIST;
   fetch C_ALL_LOCS_EXIST into L_dummy;

   if C_ALL_LOCS_EXIST%NOTFOUND then
      O_all_locs_exist := TRUE;
   else
      O_all_locs_exist := FALSE;
   end if;

   close C_ALL_LOCS_EXIST;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END ALL_LOCS_EXIST;
-----------------------------------------------------------------------------------
--This function will update Primary Supplier,Country for the Franchise locations
--when  Primary supplier is being changed for the associated Costing location.
-----------------------------------------------------------------------------------
FUNCTION UPDATE_TABLE_FRN(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item              IN       ITEM_MASTER.ITEM%TYPE,
                          I_loc               IN       ITEM_LOC.LOC%TYPE,
                          I_primary_supp      IN       ITEM_LOC.PRIMARY_SUPP%TYPE,
                          I_primary_cntry     IN       ITEM_LOC.PRIMARY_CNTRY%TYPE
                          )
RETURN BOOLEAN IS

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   L_program              VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_TABLE_FRN';

   type primary_supp_cntry_ind_tab  IS TABLE OF FUTURE_COST.PRIMARY_SUPP_COUNTRY_IND%TYPE;
   type future_cost_upd_rowid_tab   IS TABLE OF ROWID;
   L_primary_supp_cntry_ind_arr     PRIMARY_SUPP_CNTRY_IND_TAB := PRIMARY_SUPP_CNTRY_IND_TAB();
   L_future_cost_upd_rowid_arr      FUTURE_COST_UPD_ROWID_TAB := FUTURE_COST_UPD_ROWID_TAB();

   cursor C_LOCK_ITEM_LOC is
      select 'x'
        from item_loc
       where item = I_item
         and (loc  = I_loc or
              nvl(source_wh, -1) = I_loc)
         for update nowait;

   cursor C_LOCK_FUTURE_COST (L_primary_supplier   ITEM_LOC.PRIMARY_SUPP%TYPE,
                              L_primary_country    ITEM_LOC.PRIMARY_CNTRY%TYPE) is
      select (case
              when supplier = L_primary_supplier and origin_country_id = L_primary_country then
                   'Y'
              else
                   'N'
              end) upd_primary_supp_cntry_ind,
             rowid upd_rowid
        from future_cost
       where item = I_item
         and location = I_loc
         for update of primary_supp_country_ind nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    I_item);
   open C_LOCK_ITEM_LOC;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    I_item);
   close C_LOCK_ITEM_LOC;

      update item_loc
         set primary_supp         = I_primary_supp,
             primary_cntry        = I_primary_cntry,
             last_update_datetime = sysdate,
             last_update_id       = get_user
       where item = I_item
         and loc = I_loc;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Supplier: '||to_char(I_primary_supp)||
                       ' Country: '|| I_primary_cntry||
                       ' Location: '|| I_Loc);
      open C_LOCK_FUTURE_COST (I_primary_supp,
                               I_primary_cntry);
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Supplier: '||to_char(I_primary_supp)||
                       ' Country: '|| I_primary_cntry||
                       ' Location: '|| I_Loc);
      fetch C_LOCK_FUTURE_COST bulk collect into L_primary_supp_cntry_ind_arr,
                                                 L_future_cost_upd_rowid_arr;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Supplier: '||to_char(I_primary_supp)||
                       ' Country: '|| I_primary_cntry||
                       ' Location: '|| I_Loc);
      close C_LOCK_FUTURE_COST;
      forall i in  L_future_cost_upd_rowid_arr.first..L_future_cost_upd_rowid_arr.last
         update future_cost
            set primary_supp_country_ind = L_primary_supp_cntry_ind_arr(i)
          where rowid = L_future_cost_upd_rowid_arr(i) ;


   return TRUE;
EXCEPTION
   when RECORD_LOCKED then

      if C_LOCK_ITEM_LOC%ISOPEN then
         close C_LOCK_ITEM_LOC;
      end if;
      if C_LOCK_FUTURE_COST%ISOPEN then
         close C_LOCK_FUTURE_COST;
      end if;

      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                              I_item,
                                                              I_loc);
      RETURN FALSE;
   when OTHERS then

      if C_LOCK_ITEM_LOC%ISOPEN then
         close C_LOCK_ITEM_LOC;
      end if;

      if C_LOCK_FUTURE_COST%ISOPEN then
         close C_LOCK_FUTURE_COST;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_TABLE_FRN;
-----------------------------------------------------------------
FUNCTION UPDATE_COST(O_error_message      IN OUT   VARCHAR2,
                     I_item               IN       ITEM_MASTER.ITEM%TYPE,
                     I_loc                IN       ITEM_LOC.LOC%TYPE,
                     I_primary_supp       IN       ITEM_LOC.PRIMARY_SUPP%TYPE,
                     I_primary_cntry      IN       ITEM_LOC.PRIMARY_CNTRY%TYPE,
                     I_process_children   IN       VARCHAR2)
RETURN BOOLEAN IS

   L_dummy   VARCHAR2(1);
   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_COST';
   L_locs_table                locs_table_type;

cursor C_SUPP_CNTRY_CHANGED is
   select 'x'
     from item_loc
    where item = I_item
      and loc  = I_loc
      and ((primary_supp != I_primary_supp) or (primary_cntry != I_primary_cntry));

cursor C_COSTING_LOC is
   select loc
     from item_loc
    where item = I_item
      and costing_loc  = I_loc;

cursor C_GET_ITEM_CHILDREN is
   select item
     from item_master
    where ((item_parent = I_item) or
           (item_grandparent = I_item))
      and tran_level >= item_level;

BEGIN
   open C_SUPP_CNTRY_CHANGED;
   fetch  C_SUPP_CNTRY_CHANGED into L_dummy;

   -- If the supplier/country have been changed, the unit cost will need to be updated.
   if C_SUPP_CNTRY_CHANGED%FOUND then
      if UPDATE_BASE_COST.CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message,
                                                      I_item,
                                                      I_loc,
                                                      I_primary_supp,
                                                      I_primary_cntry,
                                                      I_process_children,
                                                      NULL /* Cost Change Number */ ) = FALSE then
         return FALSE;
      end if;
      open C_COSTING_LOC;
      fetch C_COSTING_LOC BULK COLLECT INTO L_locs_table;
         if L_locs_table.COUNT >0 and L_locs_table is NOT NULL then
            FOR i in L_locs_table.first .. L_locs_table.last LOOP
                if UPDATE_TABLE_FRN(O_error_message,
                                    I_item,
                               L_locs_table(i),
                               I_primary_supp,
                               I_primary_cntry
                               )     = FALSE then
              return FALSE;
                 end if;

                if UPDATE_BASE_COST.CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message,
                                                           I_item,
                                                           L_locs_table(i),
                                                           I_primary_supp,
                                                           I_primary_cntry,
                                                           I_process_children,
                                                           NULL /* Cost Change Number */ ) = FALSE then
              return FALSE;
                end if;
                FOR item_child_rec IN C_GET_ITEM_CHILDREN LOOP
                    if UPDATE_TABLE_FRN(O_error_message,
                              item_child_rec.item,
                            L_locs_table(i),
                            I_primary_supp,
                            I_primary_cntry
                            )     = FALSE then
             return FALSE;
                    end if;
               END LOOP;
            END LOOP;
         end if;
      close C_COSTING_LOC;
   end if;
   close C_SUPP_CNTRY_CHANGED;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_COSTING_LOC%ISOPEN then
         close C_COSTING_LOC;
      end if;
      if C_GET_ITEM_CHILDREN%ISOPEN then
         close C_GET_ITEM_CHILDREN;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));

      return FALSE;

END UPDATE_COST;
-----------------------------------------------------------------
FUNCTION STATUS_UPDATED(O_error_message    IN OUT   VARCHAR2,
                        O_status_updated   IN OUT   BOOLEAN,
                        I_item             IN       ITEM_MASTER.ITEM%TYPE,
                        I_loc              IN       ITEM_LOC.LOC%TYPE,
                        I_new_status       IN       ITEM_LOC.STATUS%TYPE)
RETURN BOOLEAN IS

   L_dummy   VARCHAR2(1);
   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.STATUS_UPDATED';

cursor C_STATUS_CHANGED is
   select 'x'
     from item_loc
    where item = I_item
      and loc = I_loc
      and status != I_new_status;

BEGIN
   open C_STATUS_CHANGED;
   fetch C_STATUS_CHANGED into L_dummy;

   if C_STATUS_CHANGED%FOUND then
      O_status_updated := TRUE;
   end if;

   close C_STATUS_CHANGED;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END STATUS_UPDATED;
-----------------------------------------------------------------
FUNCTION UPDATE_TABLE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item              IN       ITEM_MASTER.ITEM%TYPE,
                      I_loc               IN       ITEM_LOC.LOC%TYPE,
                      I_item_loc_rec      IN       ITEM_LOC_ATTR_RECTYPE,
                      I_status_updated    IN       BOOLEAN,
                      I_store_price_ind   IN       ITEM_LOC.STORE_PRICE_IND%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   L_status_update_date   ITEM_LOC.STATUS_UPDATE_DATE%TYPE;
   L_program              VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_TABLE';
   L_status               ITEM_MASTER.STATUS%TYPE;

   type primary_supp_cntry_ind_tab  IS TABLE OF FUTURE_COST.PRIMARY_SUPP_COUNTRY_IND%TYPE;
   type future_cost_upd_rowid_tab   IS TABLE OF ROWID;
   L_primary_supp_cntry_ind_arr     PRIMARY_SUPP_CNTRY_IND_TAB := PRIMARY_SUPP_CNTRY_IND_TAB();
   L_future_cost_upd_rowid_arr      FUTURE_COST_UPD_ROWID_TAB := FUTURE_COST_UPD_ROWID_TAB();

   cursor C_LOCK_ITEM_LOC is
      select 'x'
        from item_loc
       where item = I_item
         and (loc  = I_loc or
              nvl(source_wh, -1) = I_loc)
         for update nowait;

   cursor C_GET_ITEM_STATUS is
      select status
        from item_master
       where item = I_item;

   cursor C_LOCK_FUTURE_COST (L_primary_supplier   ITEM_LOC.PRIMARY_SUPP%TYPE,
                              L_primary_country    ITEM_LOC.PRIMARY_CNTRY%TYPE) is
      select (case
              when supplier = L_primary_supplier and origin_country_id = L_primary_country then
                   'Y'
              else
                   'N'
              end) upd_primary_supp_cntry_ind,
             rowid upd_rowid
        from future_cost
       where item = I_item
         and location = I_loc
         for update of primary_supp_country_ind nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    I_item);
   open C_LOCK_ITEM_LOC;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    I_item);
   close C_LOCK_ITEM_LOC;

   if I_status_updated then
      L_status_update_date := SYSDATE;
   else
      L_status_update_date := NULL;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ITEM_STATUS',
                    'ITEM_MASTER',
                    I_item);
   open C_GET_ITEM_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ITEM_STATUS',
                    'ITEM_MASTER',
                    I_item);
   fetch C_GET_ITEM_STATUS into L_status;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ITEM_STATUS',
                    'ITEM_MASTER',
                    I_item);
   close C_GET_ITEM_STATUS;

   if L_status <> 'A' then
      update item_loc
         set primary_supp          = I_item_loc_rec.primary_supp,
             primary_cntry         = I_item_loc_rec.primary_cntry,
             status                = I_item_loc_rec.status,
             status_update_date    = nvl(L_status_update_date, status_update_date),
             local_item_desc       = I_item_loc_rec.local_item_desc,
             local_short_desc      = I_item_loc_rec.local_short_desc,
             primary_variant       = I_item_loc_rec.primary_variant,
             unit_retail           = I_item_loc_rec.unit_retail,
             regular_unit_retail   = I_item_loc_rec.unit_retail,
             ti                    = I_item_loc_rec.ti,
             hi                    = I_item_loc_rec.hi,
             store_ord_mult        = I_item_loc_rec.store_ord_mult,
             daily_waste_pct       = I_item_loc_rec.daily_waste_pct,
             taxable_ind           = I_item_loc_rec.taxable_ind,
             meas_of_each          = I_item_loc_rec.meas_of_each,
             meas_of_price         = I_item_loc_rec.meas_of_price,
             uom_of_price          = I_item_loc_rec.uom_of_price,
             selling_unit_retail   = I_item_loc_rec.selling_unit_retail,
             selling_uom           = I_item_loc_rec.selling_uom,
             primary_cost_pack     = I_item_loc_rec.primary_cost_pack,
             receive_as_type       = I_item_loc_rec.receive_as_type,
             source_method         = I_item_loc_rec.source_method,
             source_wh             = I_item_loc_rec.source_wh,
             multi_units           = I_item_loc_rec.multi_units,
             multi_unit_retail     = I_item_loc_rec.multi_unit_retail,
             multi_selling_uom     = I_item_loc_rec.multi_selling_uom,
             last_update_datetime  = sysdate,
             last_update_id        = get_user,
             inbound_handling_days = nvl(I_item_loc_rec.inbound_handling_days,inbound_handling_days),
             store_price_ind       = nvl(I_store_price_ind, store_price_ind),
             uin_type              = I_item_loc_rec.uin_type,
             uin_label             = I_item_loc_rec.uin_label,
             capture_time          = I_item_loc_rec.capture_time,
             ext_uin_ind           = I_item_loc_rec.ext_uin_ind,
             ranged_ind            = I_item_loc_rec.ranged_ind,
             costing_loc           = nvl(I_item_loc_rec.costing_loc,costing_loc),
             costing_loc_type      = nvl(I_item_loc_rec.costing_loc_type,costing_loc_type)
       where item = I_item
         and loc = I_loc;
   else
      update item_loc
         set primary_supp          = I_item_loc_rec.primary_supp,
             primary_cntry         = I_item_loc_rec.primary_cntry,
             status                = I_item_loc_rec.status,
             status_update_date    = nvl(L_status_update_date, status_update_date),
             local_item_desc       = I_item_loc_rec.local_item_desc,
             local_short_desc      = I_item_loc_rec.local_short_desc,
             primary_variant       = I_item_loc_rec.primary_variant,
             ti                    = I_item_loc_rec.ti,
             hi                    = I_item_loc_rec.hi,
             store_ord_mult        = I_item_loc_rec.store_ord_mult,
             daily_waste_pct       = I_item_loc_rec.daily_waste_pct,
             taxable_ind           = I_item_loc_rec.taxable_ind,
             meas_of_each          = I_item_loc_rec.meas_of_each,
             meas_of_price         = I_item_loc_rec.meas_of_price,
             uom_of_price          = I_item_loc_rec.uom_of_price,
             selling_uom           = I_item_loc_rec.selling_uom,
             primary_cost_pack     = I_item_loc_rec.primary_cost_pack,
             receive_as_type       = I_item_loc_rec.receive_as_type,
             source_method         = I_item_loc_rec.source_method,
             source_wh             = I_item_loc_rec.source_wh,
             last_update_datetime  = sysdate,
             last_update_id        = get_user,
             inbound_handling_days = nvl(I_item_loc_rec.inbound_handling_days,inbound_handling_days),
             store_price_ind       = nvl(I_store_price_ind, store_price_ind),
             uin_type              = I_item_loc_rec.uin_type,
             uin_label             = I_item_loc_rec.uin_label,
             capture_time          = I_item_loc_rec.capture_time,
             ext_uin_ind           = I_item_loc_rec.ext_uin_ind,
             ranged_ind            = I_item_loc_rec.ranged_ind,
             costing_loc           = nvl(I_item_loc_rec.costing_loc,costing_loc),
             costing_loc_type      = nvl(I_item_loc_rec.costing_loc_type,costing_loc_type)
       where item = I_item
         and loc = I_loc;

      --
      update item_loc
         set primary_cost_pack  = I_item_loc_rec.primary_cost_pack
       where item = I_item
         and NVL(source_wh, -1) = I_loc;
      --

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Supplier: '||to_char(I_item_loc_rec.primary_supp)||
                       ' Country: '|| I_item_loc_rec.primary_cntry||
                       ' Location: '|| I_Loc);
      open C_LOCK_FUTURE_COST (I_item_loc_rec.primary_supp,
                               I_item_loc_rec.primary_cntry);
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Supplier: '||to_char(I_item_loc_rec.primary_supp)||
                       ' Country: '|| I_item_loc_rec.primary_cntry||
                       ' Location: '|| I_Loc);
      fetch C_LOCK_FUTURE_COST bulk collect into L_primary_supp_cntry_ind_arr,
                                                 L_future_cost_upd_rowid_arr;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_FUTURE_COST',
                       'FUTURE_COST',
                       ' Item: '||I_item||
                       ' Supplier: '||to_char(I_item_loc_rec.primary_supp)||
                       ' Country: '|| I_item_loc_rec.primary_cntry||
                       ' Location: '|| I_Loc);
      close C_LOCK_FUTURE_COST;
      forall i in  L_future_cost_upd_rowid_arr.first..L_future_cost_upd_rowid_arr.last
         update future_cost
            set primary_supp_country_ind = L_primary_supp_cntry_ind_arr(i)
          where rowid = L_future_cost_upd_rowid_arr(i) ;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then

      if C_LOCK_ITEM_LOC%ISOPEN then
         close C_LOCK_ITEM_LOC;
      end if;

      if C_GET_ITEM_STATUS%ISOPEN then
         close C_GET_ITEM_STATUS;
      end if;

      if C_LOCK_FUTURE_COST%ISOPEN then
         close C_LOCK_FUTURE_COST;
      end if;

      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                              I_item,
                                                              I_loc);
      RETURN FALSE;
   when OTHERS then

      if C_LOCK_ITEM_LOC%ISOPEN then
         close C_LOCK_ITEM_LOC;
      end if;

      if C_GET_ITEM_STATUS%ISOPEN then
         close C_GET_ITEM_STATUS;
      end if;

      if C_LOCK_FUTURE_COST%ISOPEN then
         close C_LOCK_FUTURE_COST;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_TABLE;

-----------------------------------------------------------------

FUNCTION UPDATE_ITEM_LOC_FOR_KIDS(O_error_message    IN OUT   VARCHAR2,
                                  I_item             IN       item_master.item%TYPE,
                                  I_loc              IN       item_loc.loc%TYPE,
                                  I_item_loc_rec     IN OUT   item_loc_attr_rectype,
                                  I_status_updated   IN       BOOLEAN)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_ITEM_LOC_FOR_KIDS';
   L_status_update_date ITEM_LOC.STATUS_UPDATE_DATE%TYPE;
   L_found                   VARCHAR2(1) := NULL;

   cursor C_GET_ITEM_CHILDREN is
      select im.item,
             im.status,
             im.item_level,
             im.tran_level
        from item_master im,
             item_loc il
       where ((im.item_parent = I_item)
               or
              (im.item_grandparent = I_item))
         and im.tran_level >= im.item_level
         and il.loc = I_loc
         and im.item = il.item;

   cursor C_CHECK_COST_LOC_RANGED(c_child_item ITEM_MASTER.ITEM%TYPE) is
      select 'X'
        from item_loc
       where item = c_child_item
         and loc = I_item_loc_rec.costing_loc;


BEGIN
   -- The primary variant is not updated for child items.
   I_item_loc_rec.primary_variant := NULL;

   FOR item_child_rec IN C_GET_ITEM_CHILDREN LOOP
      ---
      L_found := NULL;
      if I_item_loc_rec.costing_loc is not NULL then
         SQL_LIB.SET_MARK('OPEN', 'C_CHECK_COST_LOC_RANGED', 'ITEM_LOC', 'Item: ' ||item_child_rec.item||' Loc: '||I_item_loc_rec.costing_loc);
         open C_CHECK_COST_LOC_RANGED(item_child_rec.item);
         SQL_LIB.SET_MARK('FETCH', 'C_CHECK_COST_LOC_RANGED', 'ITEM_LOC', 'Item: ' ||item_child_rec.item||' Loc: '||I_item_loc_rec.costing_loc);
         fetch C_CHECK_COST_LOC_RANGED into L_found ;
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_COST_LOC_RANGED', 'ITEM_LOC', 'Item: ' ||item_child_rec.item||' Loc: '||I_item_loc_rec.costing_loc);
         close C_CHECK_COST_LOC_RANGED;

         if L_found is NOT NULL then
            if UPDATE_TABLE(O_error_message,
                            item_child_rec.item,
                            I_loc,
                            I_item_loc_rec,
                            I_status_updated) = FALSE then
               return FALSE;
            end if;
         end if;
      else
         if UPDATE_TABLE(O_error_message,
                         item_child_rec.item,
                         I_loc,
                         I_item_loc_rec,
                         I_status_updated) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ITEM_LOC_FOR_KIDS;
-----------------------------------------------------------------
FUNCTION UPDATE_STATUS_FOR_KIDS(O_error_message   IN OUT   VARCHAR2,
                                I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                I_loc             IN       ITEM_LOC.LOC%TYPE,
                                I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                                I_status          IN       ITEM_LOC.STATUS%TYPE)
RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_item_status   ITEM_MASTER.STATUS%TYPE;
   L_program       VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_STATUS_FOR_KIDS';
   L_child_item    ITEM_MASTER.ITEM%TYPE;
   L_dummy         VARCHAR2(1);

   cursor C_LOCK_ITEM_LOC(C_item IN item_master.item%TYPE) is
      select 'x'
        from item_loc
       where loc = I_loc
         and item = C_item
         for update of status nowait;

   cursor C_GET_ITEM_CHILDREN is
      select im.item,
             im.item_level,
             im.tran_level,
             im.status
        from item_master im,
             item_loc il
       where im.item = il.item
         and il.loc  = I_loc
         and ((im.item_parent = I_item) or (im.item_grandparent = I_item))
         and im.tran_level >= im.item_level;

BEGIN
   FOR item_child_rec IN C_GET_ITEM_CHILDREN LOOP
      L_child_item := item_child_rec.item;

      open C_LOCK_ITEM_LOC(L_child_item);
      close C_LOCK_ITEM_LOC;

      update item_loc
         set status = I_status,
             status_update_date   = SYSDATE,
             last_update_datetime = SYSDATE,
             last_update_id       = get_user
       where item = item_child_rec.item
         and loc = I_loc;
   END LOOP;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                              L_child_item,
                                                              I_loc);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_STATUS_FOR_KIDS;
-----------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC(O_error_message           IN OUT   VARCHAR2,
                         I_item                    IN       ITEM_MASTER.ITEM%TYPE,
                         I_item_status             IN       ITEM_MASTER.STATUS%TYPE,
                         I_item_level              IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                         I_tran_level              IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                         I_loc                     IN       ITEM_LOC.LOC%TYPE,
                         I_loc_type                IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_primary_supp            IN       ITEM_LOC.PRIMARY_SUPP%TYPE,
                         I_primary_cntry           IN       ITEM_LOC.PRIMARY_CNTRY%TYPE,
                         I_status                  IN       ITEM_LOC.STATUS%TYPE,
                         I_local_item_desc         IN       ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                         I_local_short_desc        IN       ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                         I_primary_variant         IN       ITEM_LOC.PRIMARY_VARIANT%TYPE,
                         I_unit_retail             IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_ti                      IN       ITEM_LOC.TI%TYPE,
                         I_hi                      IN       ITEM_LOC.HI%TYPE,
                         I_store_ord_mult          IN       ITEM_LOC.STORE_ORD_MULT%TYPE,
                         I_daily_waste_pct         IN       ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                         I_taxable_ind             IN       ITEM_LOC.TAXABLE_IND%TYPE,
                         I_meas_of_each            IN       ITEM_LOC.MEAS_OF_EACH%TYPE,
                         I_meas_of_price           IN       ITEM_LOC.MEAS_OF_PRICE%TYPE,
                         I_uom_of_price            IN       ITEM_LOC.UOM_OF_PRICE%TYPE,
                         I_selling_unit_retail     IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                         I_selling_uom             IN       ITEM_LOC.SELLING_UOM%TYPE,
                         I_primary_cost_pack       IN       ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                         I_process_children        IN       VARCHAR2,
                         I_receive_as_type         IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_ranged_ind              IN       ITEM_LOC.RANGED_IND%TYPE,
                         I_inbound_handling_days   IN       ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE,
                         I_store_price_ind         IN       ITEM_LOC.STORE_PRICE_IND%TYPE,
                         I_source_method           IN       ITEM_LOC.SOURCE_METHOD%TYPE ,
                         I_source_wh               IN       ITEM_LOC.SOURCE_WH%TYPE,
                         I_multi_units             IN       ITEM_LOC.MULTI_UNITS%TYPE,
                         I_multi_unit_retail       IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                         I_multi_selling_uom       IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                         I_uin_type                IN       ITEM_LOC.UIN_TYPE%TYPE,
                         I_uin_label               IN       ITEM_lOC.UIN_LABEL%TYPE,
                         I_capture_time            IN       ITEM_LOC.CAPTURE_TIME%TYPE,
                         I_ext_uin_ind             IN       ITEM_LOC.EXT_UIN_IND%TYPE,
                         I_costing_loc             IN       ITEM_LOC.COSTING_LOC%TYPE DEFAULT NULL,
                         I_costing_loc_type        IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_dummy                 VARCHAR2(1);
   L_program               VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_ITEM_LOC';
   L_status_updated        BOOLEAN := FALSE;
   L_item_loc_rec          item_loc_attr_rectype;
   L_source_wh_exists      VARCHAR2(1) := 'N';
   L_def_to_child          BOOLEAN := FALSE;

   cursor C_SOURCE_WH_EXISTS is
      select 'Y'
        from item_loc
       where item = I_item
         and loc = I_source_wh;
BEGIN
   -- Set item loc rec
   L_item_loc_rec.loc_type              := I_loc_type;
   L_item_loc_rec.primary_supp          := I_primary_supp;
   L_item_loc_rec.primary_cntry         := I_primary_cntry;
   L_item_loc_rec.status                := I_status;
   L_item_loc_rec.local_item_desc       := I_local_item_desc;
   L_item_loc_rec.local_short_desc      := I_local_short_desc;
   L_item_loc_rec.primary_variant       := I_primary_variant;
   L_item_loc_rec.unit_retail           := I_unit_retail;
   L_item_loc_rec.ti                    := I_ti;
   L_item_loc_rec.hi                    := I_hi;
   L_item_loc_rec.store_ord_mult        := I_store_ord_mult;
   L_item_loc_rec.daily_waste_pct       := I_daily_waste_pct;
   L_item_loc_rec.taxable_ind           := I_taxable_ind;
   L_item_loc_rec.meas_of_each          := I_meas_of_each;
   L_item_loc_rec.meas_of_price         := I_meas_of_price;
   L_item_loc_rec.uom_of_price          := I_uom_of_price;
   L_item_loc_rec.selling_unit_retail   := I_selling_unit_retail;
   L_item_loc_rec.selling_uom           := I_selling_uom;
   L_item_loc_rec.primary_cost_pack     := I_primary_cost_pack;
   L_item_loc_rec.receive_as_type       := I_receive_as_type;
   L_item_loc_rec.inbound_handling_days := I_inbound_handling_days;
   L_item_loc_rec.multi_units           := I_multi_units;
   L_item_loc_rec.multi_unit_retail     := I_multi_unit_retail;
   L_item_loc_rec.multi_selling_uom     := I_multi_selling_uom;
   L_item_loc_rec.uin_type              := I_uin_type;
   L_item_loc_rec.uin_label             := I_uin_label;
   L_item_loc_rec.capture_time          := I_capture_time;
   L_item_loc_rec.ext_uin_ind           := I_ext_uin_ind;
   L_item_loc_rec.ranged_ind            := I_ranged_ind;
   L_item_loc_rec.costing_loc           := I_costing_loc;
   L_item_loc_rec.costing_loc_type      := I_costing_loc_type;


   if I_loc_type = 'S' and I_source_method is NULL then
      L_item_loc_rec.source_method      := 'S';
   else
      L_item_loc_rec.source_method      := I_source_method;
   end if;

   L_item_loc_rec.source_wh := I_source_wh;

   -- Range the Source WH if not ranged.
   if I_source_method = 'W' then
      open C_SOURCE_WH_EXISTS;
      fetch C_SOURCE_WH_EXISTS into L_source_wh_exists;
      close C_SOURCE_WH_EXISTS;

      if (NVL(L_source_wh_exists,'N') = 'N') then
         if I_process_children = 'Y' then
            L_def_to_child := TRUE;
         else
            L_def_to_child := FALSE;
         end if;
         if NEW_ITEM_LOC(O_error_message,
                         I_item,
                         I_source_wh,
                         NULL, NULL, NULL, NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL, NULL, NULL,
                         NULL, NULL, NULL, NULL,
                         NULL,
                         NULL, NULL, NULL, NULL, NULL, NULL,
                         NULL, NULL, NULL, NULL, NULL, NULL,
                         NULL, NULL, NULL, NULL, NULL, NULL,
                         NULL, NULL, NULL, L_def_to_child, NULL) = FALSE then
            return FALSE;
         end if;
      end if;

   end if;
   --

   -- If the supplier and origin country have been changed, the item/loc's
   -- unit cost may need to be updated.

   if UPDATE_COST(O_error_message,
                  I_item,
                  I_loc,
                  I_primary_supp,
                  I_primary_cntry,
                  I_process_children) = FALSE then
      return FALSE;
   end if;

   -- Determine if the item/loc status will be updated.
   if STATUS_UPDATED(O_error_message,
                     L_status_updated,
                     I_item,
                     I_loc,
                     I_status) = FALSE then
      return FALSE;
   end if;
   ---
   if UPDATE_TABLE(O_error_message,
                   I_item,
                   I_loc,
                   L_item_loc_rec,
                   L_status_updated,
                   I_store_price_ind
                   ) = FALSE then
      return FALSE;
   end if;


   -- Process the children.
   if I_process_children = 'Y' then
      if UPDATE_ITEM_LOC_FOR_KIDS(O_error_message,
                                  I_item,
                                  I_loc,
                                  L_item_loc_rec,
                                  L_status_updated) = FALSE then
         return FALSE;
      end if;

      -- Note: The item/loc rec returned by update_item_loc_for_kids may have
      -- been modified because the parameter is declared as IN OUT.

   -- Status is always updated for children.
   elsif L_status_updated then
      if UPDATE_STATUS_FOR_KIDS(O_error_message,
                                I_item,
                                I_loc,
                                I_loc_type,
                                I_status) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ITEM_LOC;
-----------------------------------------------------------------
FUNCTION ITEM_LOC_ORDERS_EXIST(O_error_message   OUT   VARCHAR2,
                               O_orders_exist    OUT   BOOLEAN,
                               I_item            IN    ITEM_MASTER.ITEM%TYPE,
                               I_loc             IN    ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_dummy              VARCHAR2(1);
   L_program            VARCHAR2(64) := 'ITEM_LOC_SQL.ITEM_LOC_ORDERS_EXIST';

   cursor C_ORDERS_EXIST is
      select 'x'
        from ordloc ol,
             ordhead oh
       where oh.order_no = ol.order_no
         and ol.location = I_loc
         and ol.item     = I_item
         and oh.status in ('W', 'S', 'A');

BEGIN
   open C_ORDERS_EXIST;
   fetch C_ORDERS_EXIST into L_dummy;

   O_orders_exist := C_ORDERS_EXIST%FOUND;

   close C_ORDERS_EXIST;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_LOC_ORDERS_EXIST;
-----------------------------------------------------------------


-----------------------------------------------------------------
FUNCTION ITEM_LOC_TRANSFERS_EXIST(O_error_message     OUT   VARCHAR2,
                                  O_transfers_exist   OUT   BOOLEAN,
                                  I_item              IN    ITEM_MASTER.ITEM%TYPE,
                                  I_loc               IN    ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_dummy              VARCHAR2(1);
   L_program            VARCHAR2(64) := 'ITEM_LOC_SQL.ITEM_LOC_TRANSFERS_EXIST';

   cursor C_TRANSFERS_EXIST is
      select 'Y'
        from tsfhead th,
             tsfdetail td
       where th.status in ('I','A','S','E')
         and th.to_loc = I_loc
         and th.tsf_no = td.tsf_no
         and td.item   = I_item;

BEGIN
   open C_TRANSFERS_EXIST;
   fetch C_TRANSFERS_EXIST into L_dummy;

   O_transfers_exist := C_TRANSFERS_EXIST%FOUND;

   close C_TRANSFERS_EXIST;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_LOC_TRANSFERS_EXIST;
-----------------------------------------------------------------
FUNCTION ITEM_LOC_ALLOC_EXIST(O_error_message     OUT   VARCHAR2,
                              O_alloc_exist       OUT   BOOLEAN,
                              I_item              IN    ITEM_MASTER.ITEM%TYPE,
                              I_loc               IN    ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_dummy              VARCHAR2(1);
   L_program            VARCHAR2(64) := 'ITEM_LOC_SQL.ITEM_LOC_ALLOC_EXIST';

   cursor C_ALLOC_EXIST is
      select 'Y'
        from alloc_header ah,
             alloc_detail ad
       where ah.status in ('W','R','A')
         and ah.item   = I_item
         and ah.alloc_no = ad.alloc_no
         and ad.to_loc = I_loc;


BEGIN
   open C_ALLOC_EXIST;
   fetch C_ALLOC_EXIST into L_dummy;

   O_alloc_exist := C_ALLOC_EXIST%FOUND;

   close C_ALLOC_EXIST;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_LOC_ALLOC_EXIST;
-----------------------------------------------------------------
FUNCTION CHECK_DEPENDENCIES(O_error_message        IN OUT   VARCHAR2,
                            O_dependencies_exist   IN OUT   BOOLEAN,
                            I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                            I_loc                  IN       ITEM_LOC.LOC%TYPE,
                            I_loc_type             IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_exist   BOOLEAN      := FALSE;
   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.CHECK_DEPENDENCIES';

BEGIN
   if ITEM_LOC_ORDERS_EXIST(O_error_message,
                            L_exist,
                            I_item,
                            I_loc) = FALSE then
      return FALSE;
   end if;
   ---
   if L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_ORD_EXIST',
                                            I_item,
                                            to_char(I_loc),
                                            NULL);
      O_dependencies_exist := TRUE;
      return TRUE;
   end if;
   ---

   if ITEM_LOC_TRANSFERS_EXIST(O_error_message,
                               L_exist,
                               I_item,
                               I_loc) = FALSE then
      return FALSE;
   end if;
   ---
   if L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_TSF_EXIST',
                                            I_item,
                                            to_char(I_loc),
                                            NULL);

      O_dependencies_exist := TRUE;
      return TRUE;
   end if;
   ---
   if ITEM_LOC_ALLOC_EXIST(O_error_message,
                           L_exist,
                           I_item,
                           I_loc) = FALSE then
      return FALSE;
   end if;
   ---
   if L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_ALLOC_EXIST',
                                            I_item,
                                            to_char(I_loc),
                                            NULL);

      O_dependencies_exist := TRUE;
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_DEPENDENCIES;
-----------------------------------------------------------------
FUNCTION CHECK_NEW_STATUS(O_error_message   IN OUT   VARCHAR2,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE,
                          I_loc             IN       ITEM_LOC.LOC%TYPE,
                          I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_old_status      IN       ITEM_LOC.STATUS%TYPE,
                          I_new_status      IN       ITEM_LOC.STATUS%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'ITEM_LOC_SQL.CHECK_NEW_STATUS';
   L_store               store.store%TYPE;
   L_wh                  wh.wh%TYPE;
   L_dependencies_exist  BOOLEAN;
   L_item_in_active_pack BOOLEAN;
   L_exists_as_source    BOOLEAN;

BEGIN
   /*
    * If the new status (I_new_status) is 'D' (delete) or 'I' (inactive),
    * then processing needs to check for current dependencies or current
    * pack dependencies.
    */
   if I_new_status in('D', 'I') then
      if CHECK_DEPENDENCIES(O_error_message,
                            L_dependencies_exist,
                            I_item,
                            I_loc,
                            I_loc_type) = FALSE then
         return FALSE;
      end if;

      -- Return the error message returned by check_dependencies.
      if L_dependencies_exist then
         return FALSE;
      end if;

      if ITEMLOC_ATTRIB_SQL.ITEM_IN_ACTIVE_PACK(O_error_message,
                                                I_item,
                                                I_loc,
                                                L_item_in_active_pack) = FALSE then
         return FALSE;
      end if;

      if L_item_in_active_pack then
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_IN_ACTIVE_PACK',
                                               I_item,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   /*
    * If the old status (I_old_status) is 'A' (active), the
    * new status (I_new_status) is 'D' (delete),  'I' (inactive),
    * or 'C' (discontinued), and the location type
    * is 'W' (warehouse), then processing needs to determine whether
    * the warehouse is a source warehouse.
    */
   if (I_old_status = 'A') and
      (I_new_status in('D', 'I', 'C')) and
      (I_loc_type = 'W') then

      if REPLENISHMENT_SQL.WH_EXISTS_AS_SOURCE(O_error_message,
                                               L_exists_as_source,
                                               I_item,
                                               I_loc) = FALSE then
         return FALSE;
      end if;

      if L_exists_as_source then
         O_error_message := SQL_LIB.CREATE_MSG('ITEMLOC_SOURCE_WH',
                                               I_loc,
                                               I_item,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_NEW_STATUS;
-----------------------------------------------------------------
FUNCTION STATUS_CHANGE_VALID(O_error_message   IN OUT   VARCHAR2,
                            I_item             IN       ITEM_MASTER.ITEM%TYPE,
                            I_loc              IN       ITEM_LOC.LOC%TYPE,
                            I_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                            I_old_status       IN       ITEM_LOC.STATUS%TYPE,
                            I_new_status       IN       ITEM_LOC.STATUS%TYPE)
RETURN BOOLEAN IS

L_program             VARCHAR2(64) := 'ITEM_LOC_SQL.STATUS_CHANGE_VALID';

cursor C_GET_ITEM_CHILDREN is
   select item
     from item_master
    where ((item_parent = I_item) or
           (item_grandparent = I_item))
      and tran_level >= item_level;

BEGIN
   if CHECK_NEW_STATUS(O_error_message,
                       I_item,
                       I_loc,
                       I_loc_type,
                       I_old_status,
                       I_new_status) = FALSE then
      return FALSE;
   end if;

   FOR item_rec IN C_GET_ITEM_CHILDREN LOOP
      if CHECK_NEW_STATUS(O_error_message,
                          item_rec.item,
                          I_loc,
                          I_loc_type,
                          I_old_status,
                          I_new_status) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END STATUS_CHANGE_VALID;
-----------------------------------------------------------------
FUNCTION LOCS_EXIST_FOR_GROUP(O_error_message   IN OUT   VARCHAR2,
                              O_locs_exist      IN OUT   BOOLEAN,
                              I_group_type      IN       CODE_DETAIL.CODE%TYPE,
                              I_group_value     IN       VARCHAR2)
RETURN BOOLEAN IS

   L_locs_table locs_table_type;
   L_program    VARCHAR2(64) := 'ITEM_LOC_SQL.LOCS_EXIST_FOR_GROUP';
   L_loc        item_loc.loc%TYPE;
   L_dummy      VARCHAR2(1);


   -- This cursor validates that the user has visibility to the location
   -- by joining it to v_store or v_wh.

   cursor C_LOC_VALID_FOR_USER is
      select 'x'
        from v_store vs
       where vs.store = L_loc
       union
      select 'x'
        from v_wh vw
       where vw.wh = L_loc;

BEGIN

   if GROUP_LOCS_FOR_CURRENCY(O_error_message,
                              L_locs_table,
                              I_group_type,
                              I_group_value,
                              NULL) = FALSE then
      return FALSE;
   end if;

   if L_locs_table.EXISTS(1) = TRUE then
      -- Need to ensure that the user has access to at least one of the
      -- locations
      FOR loop_index IN 1..L_locs_table.COUNT LOOP

         L_loc := L_locs_table(loop_index);
         open C_LOC_VALID_FOR_USER;

         fetch C_LOC_VALID_FOR_USER into L_dummy;
            if C_LOC_VALID_FOR_USER%FOUND then
               O_locs_exist := TRUE;
            else
               O_locs_exist := FALSE;
               O_error_message := SQL_LIB.CREATE_MSG('NO_LOCS_GROUP_USER',
                                                      NULL,
                                                      NULL,
                                                      NULL);
            end if;
         close C_LOC_VALID_FOR_USER;

         if O_locs_exist = TRUE then
            EXIT;
         end if;

      END LOOP;
   else
     O_locs_exist := FALSE;
     O_error_message := SQL_LIB.CREATE_MSG('NO_LOCS_GROUP',
                                            NULL,
                                            NULL,
                                            NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END LOCS_EXIST_FOR_GROUP;
-----------------------------------------------------------------
FUNCTION CHECK_LOC_TRAITS_COMPLETE(O_error_message   IN OUT   VARCHAR2,
                                   O_complete        IN OUT   VARCHAR2,
                                   I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                   I_loc             IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_dummy   VARCHAR2(1);
   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.CHECK_LOC_TRAITS_COMPLETE';

   cursor C_LOC_TRAITS_COMPLETE is
      select 'x'
        from item_loc_traits
       where item = I_item
         and loc  = nvl(I_loc, loc);

BEGIN
   open C_LOC_TRAITS_COMPLETE;
   fetch C_LOC_TRAITS_COMPLETE into L_dummy;

   if C_LOC_TRAITS_COMPLETE%NOTFOUND then
      O_complete := 'N';
   else
      O_complete := 'Y';
   end if;

   close C_LOC_TRAITS_COMPLETE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_LOC_TRAITS_COMPLETE;
-----------------------------------------------------------------
FUNCTION ITEM_EXISTS_FOR_ALL_GROUP_LOCS(O_error_message         IN OUT   VARCHAR2,
                                        O_item_exists           IN OUT   BOOLEAN,
                                        O_item_active_at_locs   IN OUT   BOOLEAN,
                                        I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                        I_group_type            IN       CODE_DETAIL.CODE%TYPE,
                                        I_group_value           IN       VARCHAR2,
                                        I_currency_code         IN       CURRENCIES.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS

   L_locs_table       LOCS_TABLE_TYPE;
   L_program          VARCHAR2(64) := 'ITEM_LOC_SQL.ITEM_EXISTS_FOR_ALL_GROUP_LOCS';
   L_loc              ITEM_LOC.LOC%TYPE;
   L_dummy            VARCHAR2(1);
   L_item_exists_user BOOLEAN;
   L_status           ITEM_LOC.STATUS%TYPE;

   -- This cursor validates that the user has visibility to the location
   -- by joining it to v_store or v_wh

   cursor C_LOC_VALID_FOR_USER is
      select 'x'
        from v_store vs
       where vs.store = L_loc
       union
      select 'x'
        from v_wh vw
       where vw.wh = L_loc;

   cursor C_ALL_LOCS_EXIST is
      select status
        from item_loc
       where item = I_item
         and loc  = L_loc;

BEGIN
   if GROUP_LOCS_FOR_CURRENCY(O_error_message,
                              L_locs_table,
                              I_group_type,
                              I_group_value,
                              I_currency_code) = FALSE then
      return FALSE;
   end if;

   -- If there are no locs in the group, then O_item_exists should be FALSE.
   -- Initially set item to be active at all locations.  If it is not, the
   -- variable will be changed to false.
   O_item_exists         := FALSE;
   O_item_active_at_locs := TRUE;
   FOR loop_index IN 1..L_locs_table.COUNT LOOP
      L_loc := L_locs_table(loop_index);

      -- "Weed-Out" locations that the user does not have access to.
      open C_LOC_VALID_FOR_USER;
      fetch C_LOC_VALID_FOR_USER into L_dummy;
      if C_LOC_VALID_FOR_USER%FOUND then

         close C_LOC_VALID_FOR_USER;

         -- Check if Item Loc association exists
         open C_ALL_LOCS_EXIST;
         fetch C_ALL_LOCS_EXIST into L_status;
         if C_ALL_LOCS_EXIST%NOTFOUND then
            O_item_exists := FALSE;
         else
            O_item_exists := TRUE;
         end if;
         close C_ALL_LOCS_EXIST;

         if L_status != 'A' then
            O_item_active_at_locs := FALSE;
         end if;

      else
         close C_LOC_VALID_FOR_USER;
      end if;

      if O_item_exists = FALSE then
         EXIT;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_EXISTS_FOR_ALL_GROUP_LOCS;
-----------------------------------------------------------------
FUNCTION CHECK_PACK_COMP_ITEMLOC_STATUS(O_error_message        IN OUT   VARCHAR2,
                                        O_inactive_status      IN OUT   BOOLEAN,
                                        O_delete_disc_status   IN OUT   BOOLEAN,
                                        I_packitem             IN       ITEM_MASTER.ITEM%TYPE,
                                        I_group_type           IN       VARCHAR2,
                                        I_group_value          IN       VARCHAR2,
                                        I_currency_code        IN       CURRENCIES.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS

   L_locs_table   locs_table_type;

   L_item         ITEM_LOC.ITEM%TYPE;
   L_loc          ITEM_LOC.LOC%TYPE;
   L_status       ITEM_LOC.STATUS%TYPE;

   L_dummy        VARCHAR2(1);
   L_program      VARCHAR2(64) := 'ITEM_LOC_SQL.CHECK_PACK_COMP_ITEMLOC_STATUS';

   cursor C_GET_ALL_ITEM is
      select im.item
        from item_master im,
             packitem pi
       where im.item    = pi.item
         and pi.pack_no = I_packitem;

   cursor C_GET_STATUS is
         select status
           from item_loc
          where item = L_item
            and loc  = L_loc;

BEGIN
   if GROUP_LOCS_FOR_CURRENCY(O_error_message,
                              L_locs_table,
                              I_group_type,
                              I_group_value,
                              I_currency_code) = FALSE then
      return FALSE;
   end if;

   O_inactive_status    := FALSE;
   O_delete_disc_status := FALSE;

   FOR loop_index IN 1..L_locs_table.COUNT LOOP
     FOR rec in C_GET_ALL_ITEM LOOP
         L_item := rec.item;
         L_loc  := L_locs_table(loop_index);
         ---
         if L_item is not null then
            open C_GET_STATUS;
            fetch C_GET_STATUS into L_status;
            ---
            if L_status = 'I' then
               O_inactive_status := TRUE;
            elsif L_status in ('D','C') then
               O_delete_disc_status := TRUE;
            end if;
            ---
            close C_GET_STATUS;
         end if;
      END LOOP;
      ---
      if O_delete_disc_status and O_inactive_status then
         EXIT;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_PACK_COMP_ITEMLOC_STATUS;
-----------------------------------------------------------------
FUNCTION UPDATE_PACK_COMP_STATUS(O_error_message      IN OUT   VARCHAR2,
                                 I_packitem           IN       ITEM_MASTER.ITEM%TYPE,
                                 I_group_type         IN       VARCHAR2,
                                 I_group_value        IN       VARCHAR2,
                                 I_currency_code      IN       CURRENCIES.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_locs_table locs_table_type;
   L_item        item_master.item%TYPE;
   L_tran_level  item_master.tran_level%TYPE;
   L_item_level  item_master.item_level%TYPE;
   L_status      item_master.status%TYPE;
   L_loc         item_loc.loc%TYPE;
   L_loc_type    item_loc.loc_type%TYPE;
   L_rowid       ROWID;
   L_program    VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_PACK_COMP_STATUS';

   cursor C_GET_ALL_ITEM is
         select im.item,
                im.item_level,
                im.tran_level,
                im.status
           from item_master im,
                packitem pi
          where (im.item = pi.item or im.item_parent = pi.item or im.item_grandparent = pi.item)
            and pi.pack_no = I_packitem
         union
         select item,
                item_level,
                tran_level,
                status
           from item_master
          where item in (select im.item_parent
                           from item_master im,
                                packitem pi
                          where im.item = pi.item
                            and pi.pack_no = I_packitem)
         union
         select item,
                item_level,
                tran_level,
                status
           from item_master
          where item in (select im.item_grandparent
                           from item_master im,
                                packitem pi
                          where im.item = pi.item
                            and pi.pack_no = I_packitem);

      cursor C_LOCK_ITEM_LOC is
         select rowid,
                loc_type
           from item_loc
          where item   = L_item
            and loc    = L_loc
            and status = 'I'
            for update nowait;

BEGIN
   if GROUP_LOCS_FOR_CURRENCY(O_error_message,
                              L_locs_table,
                              I_group_type,
                              I_group_value,
                              I_currency_code) = FALSE then
      return FALSE;
   end if;

   FOR loop_index IN 1..L_locs_table.COUNT LOOP
      FOR rec in C_GET_ALL_ITEM LOOP
         L_item       := rec.item;
         L_tran_level := rec.tran_level;
         L_item_level := rec.item_level;
         L_status     := rec.status;
         L_loc        := L_locs_table(loop_index);

         open C_LOCK_ITEM_LOC;
         fetch C_LOCK_ITEM_LOC into L_rowid, L_loc_type;
         ---
         if C_LOCK_ITEM_LOC%FOUND then
            update item_loc
               set status = 'A',
                   last_update_datetime = SYSDATE,
                   last_update_id       = get_user
             where rowid = L_rowid;
            ---
         end if;

         close C_LOCK_ITEM_LOC;

      END LOOP;

   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                              I_packitem,
                                                              I_group_value);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_PACK_COMP_STATUS;
-----------------------------------------------------------------
FUNCTION UPDATE_RECV_AS_TYPE(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item              IN       ITEM_LOC.ITEM%TYPE,
                             I_location          IN       ITEM_LOC.LOC%TYPE,
                             I_receive_as_type   IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE)
   return BOOLEAN IS

   L_pwh           WH.WH%TYPE;
   L_program       VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_RECV_AS_TYPE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_LOC is
      select 'x'
        from item_loc il
       where item = I_item
         and exists (select wh
                       from wh
                      where physical_wh      = L_pwh
                        and stockholding_ind = 'Y'
                        and il.loc           = wh.wh)
         and loc != I_location
         for UPDATE NOWAIT;

BEGIN
   if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                    L_pwh,
                                    I_location)= FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_LOC','ITEM_LOC','LOCATION: '||to_char(I_location));
   open C_LOCK_ITEM_LOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_LOC','ITEM_LOC','LOCATION: '||to_char(I_location));
   close C_LOCK_ITEM_LOC;
   ---
   update item_loc il
      set receive_as_type = I_receive_as_type,
          last_update_datetime = SYSDATE,
          last_update_id       = get_user
    where item            = I_item
      and exists (select wh
                    from wh
                   where physical_wh      = L_pwh
                     and stockholding_ind = 'Y'
                     and il.loc           = wh.wh)
      and loc != I_location;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                              I_item,
                                                              I_location);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_RECV_AS_TYPE;
------------------------------------------------------------------------------------
FUNCTION GET_RECV_AS_TYPE_FOR_VWH(O_error_message     IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists            IN OUT    VARCHAR2,
                                  O_receive_as_type   IN OUT    ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                                  I_location          IN        ITEM_LOC.LOC%TYPE,
                                  I_item              IN        ITEM_LOC.ITEM%TYPE)
   return BOOLEAN IS

   cursor C_ITMLOC_RELATION(P_pwh WH.WH%TYPE)is
      select receive_as_type
        from item_loc il
       where item = I_item
         and exists (select wh
                       from wh
                      where physical_wh      = P_pwh
                        and stockholding_ind = 'Y'
                        and il.loc           = wh.wh);

   L_pwh       WH.WH%TYPE;
   L_program   VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_RECV_AS_TYPE';
BEGIN
   if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                    L_pwh,
                                    I_location)= FALSE then
      return FALSE;
   end if;
   ---
   FOR C_rec in C_ITMLOC_RELATION(L_pwh) LOOP
      if C_rec.receive_as_type is NOT NULL then
         O_receive_as_type := C_rec.receive_as_type;
         O_exists := 'Y';
         exit;
      else
         O_receive_as_type := NULL;
         O_exists := 'N';
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RECV_AS_TYPE_FOR_VWH;
----------------------------------------------------------------------------------------------------
FUNCTION CHILD_LOCS_EXIST(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_child_locs_exist   IN OUT   BOOLEAN,
                          I_item               IN       ITEM_LOC.ITEM%TYPE)

RETURN BOOLEAN IS

   L_dummy   VARCHAR2(1);
   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.CHILD_LOCS_EXIST';

   cursor C_CHILD_LOCS_EXIST is
      select 'x'
        from item_loc
       where item_parent = I_item
          or item_grandparent = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_CHILD_LOCS_EXIST', 'ITEM_LOC', 'ITEM:  '||I_item);
   open C_CHILD_LOCS_EXIST;
   SQL_LIB.SET_MARK('FETCH', 'C_CHILD_LOCS_EXIST', 'ITEM_LOC','ITEM:  '||I_item);
   fetch C_CHILD_LOCS_EXIST into L_dummy;

   if C_CHILD_LOCS_EXIST%NOTFOUND then
      O_child_locs_exist := FALSE;
   else
      O_child_locs_exist := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_CHILD_LOCS_EXIST', 'ITEM_LOC','ITEM:  '||I_item);
   close C_CHILD_LOCS_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHILD_LOCS_EXIST;
---------------------------------------------------------------------------------
FUNCTION CHECK_PRIMARY_COST_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists          IN OUT   VARCHAR2,
                                 I_item            IN       ITEM_LOC.ITEM%TYPE,
                                 I_loc             IN       ITEM_LOC.LOC%TYPE)
   return BOOLEAN IS
   L_program       VARCHAR2(64) := 'ITEM_LOC_SQL.CHECK_PRIMARY_COST_PACK';
   L_rowid         ROWID;
   cursor C_ITEM_LOC(P_item item_loc.item%TYPE ) is
      select rowid
        from item_loc
       where loc   = I_loc
         and item  = P_item
         and primary_cost_pack is not null;
   cursor C_PACK_ITEM is
      select item
        from packitem
       where pack_no  = I_item;
BEGIN
    FOR c_pack_item_rec in C_PACK_ITEM LOOP
       open C_ITEM_LOC(c_pack_item_rec.item);
       fetch C_ITEM_LOC into L_rowid;
       if L_rowid is not null then
          O_exists := 'Y';
         exit;
       else
         O_exists := 'N';
       end if;
       close C_ITEM_LOC;
    END LOOP;
    return TRUE;
 EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
 END CHECK_PRIMARY_COST_PACK;
 ----------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_COST_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item            IN       ITEM_LOC.ITEM%TYPE,
                                  I_loc             IN       ITEM_LOC.LOC%TYPE DEFAULT NULL)
   return BOOLEAN IS
   L_program       VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_PRIMARY_COST_PACK';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_rowid         ROWID;

   cursor C_LOCK_ITEM_LOC(P_item item_loc.item%TYPE ) is
      select rowid
        from item_loc
       where loc   = I_loc
         and item  = P_item
         and primary_cost_pack is not null
         for update nowait;
   ---
   cursor C_PACK_ITEM is
      select item
        from packitem
       where pack_no  = I_item;
BEGIN
    FOR c_pack_item_rec in C_PACK_ITEM LOOP
       open C_LOCK_ITEM_LOC(c_pack_item_rec.item);
       fetch C_LOCK_ITEM_LOC into L_rowid;
       update item_loc
          set primary_cost_pack = NULL,
          last_update_datetime  = SYSDATE,
          last_update_id        = get_user
        where rowid = L_rowid;
       ---
       close C_LOCK_ITEM_LOC;
    END LOOP;

    return TRUE;
 EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                              I_item,
                                                              I_loc);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
 END UPDATE_PRIMARY_COST_PACK;
------------------------------------------------------------------------------------
FUNCTION ITEM_DESC_TO_ITEMLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_item_desc       IN       ITEM_MASTER.ITEM_DESC%TYPE,
                              I_short_desc      IN       ITEM_MASTER.SHORT_DESC%TYPE,
                              I_item_level      IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                              I_tran_level      IN       ITEM_MASTER.TRAN_LEVEL%TYPE)
RETURN BOOLEAN IS

   L_loc                       ITEM_LOC.LOC%TYPE;
   L_loc_type                  ITEM_LOC.LOC_TYPE%TYPE;
   L_current_loc_item_desc     ITEM_LOC.LOCAL_ITEM_DESC%TYPE;
   L_current_short_desc        ITEM_LOC.LOCAL_SHORT_DESC%TYPE;
   L_taxable_ind               ITEM_LOC.TAXABLE_IND%TYPE;
   L_program                   VARCHAR2(62)   := 'ITEM_LOC_SQL.ITEM_DESC_TO_ITEMLOC';
   RECORD_LOCKED               EXCEPTION;
   L_status                    VARCHAR2(1) := NULL;

  cursor C_ITEM is
      select 'A'
        from item_master
       where item = I_item
         and status = 'A';

  cursor C_LOCK is
      select 'x'
        from item_loc
       where item = I_item
         and loc  = L_loc
         for update nowait;

   cursor C_GET is
      select loc,
             loc_type,
             local_item_desc,
             local_short_desc,
             taxable_ind
        from item_loc
       where item     = I_item;


BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM', 'ITEM_MASTER', 'ITEM: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM', 'ITEM_MASTER', 'ITEM: '||I_item);
   fetch C_ITEM into L_status;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM', 'ITEM_MASTER', 'ITEM: '||I_item);
   close C_ITEM;


   FOR rec in C_GET LOOP
      L_loc                   := rec.loc;
      L_loc_type              := rec.loc_type;
      L_current_loc_item_desc := rec.local_item_desc;
      L_current_short_desc    := rec.local_short_desc;
      L_taxable_ind           := rec.taxable_ind;

      -- If the Long Desc is changed the form changes the Short Desc automatically. Update both.
      if L_current_loc_item_desc != I_item_desc then
         ---
         open  C_LOCK;
         close C_LOCK;
         update item_loc
            set local_item_desc  = I_item_desc,
                local_short_desc = I_short_desc,
                last_update_datetime = SYSDATE,
                last_update_id       = get_user
          where item       = I_item
            and loc        = L_loc;

      -- if the Long Desc is unchanged only Update the Short Desc.
      elsif L_current_short_desc != I_short_desc then
         ---
         open  C_LOCK;
         close C_LOCK;
         update item_loc
            set local_short_desc = I_short_desc,
            last_update_datetime = SYSDATE,
            last_update_id       = get_user
          where item       = I_item
            and loc        = L_loc;
      end if;
   END LOOP;

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'ITEM_LOC',
                                             'I_item',
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ITEM_DESC_TO_ITEMLOC;
-----------------------------------------------------------------------------------
FUNCTION ITEM_LOCATION_EXISTS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_item_location_exists   IN OUT   BOOLEAN,
                              I_item                   IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_item_loc_exists   VARCHAR2(1)  := NULL;
   L_program           VARCHAR2(64) := 'ITEM_LOC_SQL.ITEM_LOCATION_EXISTS';

   cursor C_ITEM_LOCATION_EXISTS is
      select 'x'
        from item_loc
       where item = I_item;

BEGIN

   open C_ITEM_LOCATION_EXISTS;
   fetch C_ITEM_LOCATION_EXISTS into L_item_loc_exists;
   close C_ITEM_LOCATION_EXISTS;

   O_item_location_exists := (L_item_loc_exists is NOT NULL);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ITEM_LOCATION_EXISTS;
-----------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC(O_error_message           IN OUT   VARCHAR2,
                         I_item                    IN       ITEM_MASTER.ITEM%TYPE,
                         I_item_status             IN       ITEM_MASTER.STATUS%TYPE,
                         I_item_level              IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                         I_tran_level              IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                         I_loc                     IN       ITEM_LOC.LOC%TYPE,
                         I_loc_type                IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_primary_supp            IN       ITEM_LOC.PRIMARY_SUPP%TYPE,
                         I_primary_cntry           IN       ITEM_LOC.PRIMARY_CNTRY%TYPE,
                         I_status                  IN       ITEM_LOC.STATUS%TYPE,
                         I_local_item_desc         IN       ITEM_LOC.LOCAL_ITEM_DESC%TYPE,
                         I_local_short_desc        IN       ITEM_LOC.LOCAL_SHORT_DESC%TYPE,
                         I_primary_variant         IN       ITEM_LOC.PRIMARY_VARIANT%TYPE,
                         I_unit_retail             IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_ti                      IN       ITEM_LOC.TI%TYPE,
                         I_hi                      IN       ITEM_LOC.HI%TYPE,
                         I_store_ord_mult          IN       ITEM_LOC.STORE_ORD_MULT%TYPE,
                         I_daily_waste_pct         IN       ITEM_LOC.DAILY_WASTE_PCT%TYPE,
                         I_taxable_ind             IN       ITEM_LOC.TAXABLE_IND%TYPE,
                         I_meas_of_each            IN       ITEM_LOC.MEAS_OF_EACH%TYPE,
                         I_meas_of_price           IN       ITEM_LOC.MEAS_OF_PRICE%TYPE,
                         I_uom_of_price            IN       ITEM_LOC.UOM_OF_PRICE%TYPE,
                         I_selling_unit_retail     IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                         I_selling_uom             IN       ITEM_LOC.SELLING_UOM%TYPE,
                         I_primary_cost_pack       IN       ITEM_LOC.PRIMARY_COST_PACK%TYPE,
                         I_process_children        IN       VARCHAR2,
                         I_receive_as_type         IN       ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_ranged_ind              IN       ITEM_LOC.RANGED_IND%TYPE,
                         I_inbound_handling_days   IN       ITEM_LOC.INBOUND_HANDLING_DAYS%TYPE DEFAULT NULL,
                         I_store_price_ind         IN       ITEM_LOC.STORE_PRICE_IND%TYPE       DEFAULT NULL,
                         I_source_method           IN       ITEM_LOC.SOURCE_METHOD%TYPE         DEFAULT NULL,
                         I_source_wh               IN       ITEM_LOC.SOURCE_WH%TYPE             DEFAULT NULL,
                         I_uin_type                IN       ITEM_LOC.UIN_TYPE%TYPE              DEFAULT NULL,
                         I_uin_label               IN       ITEM_lOC.UIN_LABEL%TYPE             DEFAULT NULL,
                         I_capture_time            IN       ITEM_LOC.CAPTURE_TIME%TYPE          DEFAULT NULL,
                         I_ext_uin_ind             IN       ITEM_LOC.EXT_UIN_IND%TYPE           DEFAULT 'N',
                         I_costing_loc             IN       ITEM_LOC.COSTING_LOC%TYPE           DEFAULT NULL,
                         I_costing_loc_type        IN       ITEM_LOC.COSTING_LOC_TYPE%TYPE      DEFAULT NULL)
RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_ITEM_LOC';

   L_multi_units             ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom       ITEM_LOC.MULTI_SELLING_UOM%TYPE;

BEGIN
   --- Overloaded for call from rmssub_xitemloc API
   ---
   if ITEM_LOC_SQL.UPDATE_ITEM_LOC(O_error_message,
                                   I_item,
                                   I_item_status,
                                   I_item_level,
                                   I_tran_level,
                                   I_loc,
                                   I_loc_type,
                                   I_primary_supp,
                                   I_primary_cntry,
                                   I_status,
                                   I_local_item_desc,
                                   I_local_short_desc,
                                   I_primary_variant,
                                   I_unit_retail,
                                   I_ti,
                                   I_hi,
                                   I_store_ord_mult,
                                   I_daily_waste_pct,
                                   I_taxable_ind,
                                   I_meas_of_each,
                                   I_meas_of_price,
                                   I_uom_of_price,
                                   I_selling_unit_retail,
                                   I_selling_uom,
                                   I_primary_cost_pack,
                                   I_process_children,
                                   I_receive_as_type,
                                   I_ranged_ind,
                                   I_inbound_handling_days,
                                   I_store_price_ind,
                                   I_source_method,
                                   I_source_wh,
                                   L_multi_units,
                                   L_multi_unit_retail,
                                   L_multi_selling_uom,
                                   I_uin_type,
                                   I_uin_label,
                                   I_capture_time,
                                   I_ext_uin_ind,
                                   I_costing_loc,
                                   I_costing_loc_type) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ITEM_LOC;
-----------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid_location   IN OUT   BOOLEAN,
                           I_location         IN       ITEM_LOC.LOC%TYPE,
                           I_loc_type         IN       ITEM_LOC.LOC_TYPE%TYPE,
                           I_source_wh        IN       ITEM_LOC.SOURCE_WH%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'ITEM_LOC_SQL.VALIDATE_LOCATION';
   L_store_type   STORE.STORE_TYPE%TYPE;
   L_exists       VARCHAR2(1)  := NULL;
   L_loc_type     ITEM_LOC.LOC_TYPE%TYPE;

   cursor C_STORE_TYPE is
      select store_type
        from v_store
       where store = I_location;


BEGIN
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   if  I_loc_type in ('W', 'E') or
      (I_loc_type = 'S' and I_source_wh is NOT NULL) then
      O_valid_location := TRUE;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_STORE_TYPE',
                       'STORE',
                       'LOCATION '||to_char(I_location));
      open C_STORE_TYPE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_STORE_TYPE',
                       'STORE',
                       'LOCATION '||to_char(I_location));
      fetch C_STORE_TYPE into L_store_type;

         if L_store_type is NULL then
            SQL_LIB.SET_MARK('CLOSE',
                             'C_STORE_TYPE',
                             'STORE',
                             'LOCATION '||to_char(I_location));
            close C_STORE_TYPE;
            ---
            O_error_message := SQL_LIB.CREATE_MSG('INV_LOC',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         else
            if L_store_type in ('C','F') then
               O_valid_location := TRUE;
            else
               O_valid_location := FALSE;
            end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_STORE_TYPE',
                          'STORE',
                          'LOCATION '||to_char(I_location));
         close C_STORE_TYPE;
         end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_LOCATION;

-----------------------------------------------------------------------------------
--This function will validate the Warehouse being passed in.
-----------------------------------------------------------------------------------

FUNCTION VALIDATE_WAREHOUSE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist            IN OUT   BOOLEAN,
                            I_item             IN       ITEM_LOC.ITEM%TYPE,
                            I_source_wh        IN       ITEM_LOC.SOURCE_WH%TYPE)
RETURN BOOLEAN IS

   L_dummy VARCHAR2(1);
   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.VALIDATE_WAREHOUSE';

   cursor C_WH_EXIST is
      select 'x'
        from item_loc il,wh,v_wh
       where il.item = I_item
         and il.loc = I_source_wh
         and v_wh.wh = il.loc
         and v_wh.stockholding_ind  = 'Y'
         and v_wh.wh = wh.wh
         and wh.finisher_ind = 'N';

   cursor C_store_wh_exist is
     select 'x'
       from item_loc
      where item = I_item
        and loc_type = 'W';

   cursor C_store_default_wh_exist is
      select 'x'
       from V_WH v,
          WH w
        where v.wh = I_source_wh
          and v.wh = w.wh
          and v.stockholding_ind = 'Y'
          and w.finisher_ind = 'N';

BEGIN

   if I_source_wh is NOT NULL then
      open C_WH_EXIST;
      fetch C_WH_EXIST into L_dummy;

      if C_WH_EXIST%FOUND then
         O_exist := TRUE;
      else
        open C_store_default_wh_exist;
        fetch C_store_default_wh_exist into L_dummy;

        if C_store_default_wh_exist%FOUND then
           O_exist := TRUE;
        else
         O_exist := FALSE;
      end if;
     close C_store_default_wh_exist;
      end if;

      close C_WH_EXIST;

      return TRUE;
   else
      open C_store_wh_exist;
      fetch C_store_wh_exist into L_dummy;

      if C_store_wh_exist%FOUND then
         O_exist := TRUE;
      else
         O_exist := FALSE;
      end if;

      close C_store_wh_exist;

      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
     if C_store_default_wh_exist%ISOPEN then
      close C_store_default_wh_exist;
     end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_WAREHOUSE;

-----------------------------------------------------------------------------------
--This function will validate the costing location (Store/Wh) being passed in.
-----------------------------------------------------------------------------------

FUNCTION VALIDATE_ST_WH(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exist            IN OUT BOOLEAN,
                        I_item             IN     ITEM_LOC.ITEM%TYPE,
                        I_location         IN     ITEM_LOC.LOC%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'ITEM_LOC_SQL.VALIDATE_ST_WH';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_ITEM_LOCATION is
      select 'X'
        from item_loc il
       where il.item = I_item
         and il.loc  = I_location
         and il.loc_type in('S','W');

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_ITEM_LOCATION',
                    'ITEM_LOC',
                     'ITEM: '||I_item);
   open C_CHECK_ITEM_LOCATION;

   SQL_LIB.SET_MARK('FETCH',
                     'C_CHECK_ITEM_LOCATION',
                     'ITEM_LOC',
                      'ITEM: '||I_item);
   fetch C_CHECK_ITEM_LOCATION into L_dummy;

   if C_CHECK_ITEM_LOCATION%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_ITEM_LOCATION',
                       'ITEM_LOC',
                        'ITEM: '||I_item);
      close C_CHECK_ITEM_LOCATION;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_LOCATION', NULL, NULL, NULL);
      O_exist    := FALSE;
      return FALSE;
   else
      close C_CHECK_ITEM_LOCATION;
      O_exist    := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_ITEM_LOCATION%ISOPEN then
         close C_CHECK_ITEM_LOCATION;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ST_WH;

-------------------------------------------------------------------------------------
FUNCTION VALIDATE_COSTINGLOC_CHANGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_status           IN OUT ITEM_MASTER.STATUS%TYPE,
                                    O_event_type       IN OUT COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE,
                                    O_exists           IN OUT BOOLEAN,
                                    I_item             IN     ITEM_LOC.ITEM%TYPE,
                                    I_location         IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'ITEM_LOC_SQL.VALIDATE_COSTINGLOC_CHANGE';
   L_valid     VARCHAR2(1);

   cursor C_GET_STATUS is
      select status
        from item_master
       where item= I_item;

   cursor C_CHECK_EVENT_TYPE is
      select event_run_type
        from cost_event_run_type_config
       where event_type='CL';

   cursor C_VALIDATE_COST_EVENT is
      select 'x'
        from cost_event_cl cl,
             period,
             cost_event_result cr
       where cl.item            = I_item
         and cl.franchise_loc   = I_location
         and cl.cost_loc_change_date <= vdate
         and ((cl.cost_event_process_id = cr.cost_event_process_id
         and cr.status != 'C') or(cl.cost_event_process_id not in(select cost_event_process_id from cost_event_result)));

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STATUS',
                    'ITEM_MASTER',
                     I_item);
   open C_GET_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STATUS',
                    'ITEM_MASTER',
                     I_item);
   fetch C_GET_STATUS into O_status;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STATUS',
                    'ITEM_MASTER',
                     I_item);
   close C_GET_STATUS;
   if O_status != 'A' then
      O_exists := TRUE;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_EVENT_TYPE',
                    'COST_EVENT_RUN_TYPE_CONFIG',
                     NULL);

   open C_CHECK_EVENT_TYPE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_EVENT_TYPE',
                    'COST_EVENT_RUN_TYPE_CONFIG',
                     NULL);
   fetch C_CHECK_EVENT_TYPE into O_event_type;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_EVENT_TYPE',
                    'COST_EVENT_RUN_TYPE_CONFIG',
                     NULL);
   close C_CHECK_EVENT_TYPE;

   if O_event_type not in ('BATCH','ASYNC') then
      O_exists := TRUE;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_VALIDATE_COST_EVENT',
                    'COST_EVENT_CL',
                     I_item);
   open C_VALIDATE_COST_EVENT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_VALIDATE_COST_EVENT',
                    'COST_EVENT_CL',
                     I_item);
   fetch C_VALIDATE_COST_EVENT into L_valid;

   if C_VALIDATE_COST_EVENT %NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_VALIDATE_COST_EVENT',
                       'COST_EVENT_CL',
                        I_item);
      close C_VALIDATE_COST_EVENT;
      O_exists := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('COST_LOC_UPDATED', NULL, NULL, NULL);
   else
      O_error_message := SQL_LIB.CREATE_MSG('COST_LOC_PENDING', NULL, NULL, NULL);
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_STATUS%ISOPEN then
         close C_GET_STATUS;
      end if;
      if C_CHECK_EVENT_TYPE%ISOPEN then
         close C_CHECK_EVENT_TYPE;
      end if;
      if C_VALIDATE_COST_EVENT%ISOPEN then
         close C_VALIDATE_COST_EVENT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_COSTINGLOC_CHANGE;

-----------------------------------------------------------------
FUNCTION GET_ITEM_SERIALIZATION(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists           IN OUT   VARCHAR2,
                                I_item             IN       ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.GET_ITEM_SERIALIZATION';

   cursor C_GET_ITEM_SERIALIZATION is
      select 'x'
        from item_loc
       where item = I_item
         and uin_type is NOT NULL;

BEGIN
   O_exists := NULL;
   open  C_GET_ITEM_SERIALIZATION;
   fetch C_GET_ITEM_SERIALIZATION into O_exists;
   close C_GET_ITEM_SERIALIZATION;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_SERIALIZATION;
-----------------------------------------------------------------
FUNCTION UPDATE_ITEM_SERIALIZATION(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item             IN       ITEM_LOC.ITEM%TYPE)

return BOOLEAN  is

   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.UPDATE_ITEM_SERIALIZATION';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_LOC is
      select 'x'
        from item_loc
       where item = I_item
         and uin_type is NOT NULL
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    I_item);
   open C_LOCK_ITEM_LOC;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    I_item);
   close C_LOCK_ITEM_LOC;

   update item_loc
      set uin_type = NULL,
          uin_label = NULL,
          capture_time = NULL,
          ext_uin_ind = 'N'
    where item = I_item
      and loc_type = 'S';

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_LOC',
                                                              I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_ITEM_SERIALIZATION;
-----------------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   BOOLEAN,
                            I_item            IN       ITEM_COUNTRY.ITEM%TYPE,
                            I_location        IN       ITEM_LOC.LOC%TYPE,
                            I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ITEM_LOC_SQL.CHECK_ITEM_COUNTRY';
   L_country_id      ADDR.COUNTRY_ID%TYPE;
   L_localized_ind   COUNTRY_ATTRIB.LOCALIZED_IND%TYPE;
   L_exists          BOOLEAN;

   cursor C_GET_LOCALIZED_IND is
      select ca.country_id,
             ca.localized_ind
        from country_attrib ca,
             mv_loc_prim_addr mva
       where mva.loc = I_location
         and mva.country_id = ca.country_id;
BEGIN

   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_LOCALIZED_IND',
                    'MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB',
                    'LOC:'||TO_CHAR(I_location)||',COUNTRY_ID: '||L_country_id);
   open C_GET_LOCALIZED_IND;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_LOCALIZED_IND',
                    'MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB',
                    'LOC:'||TO_CHAR(I_location)||',COUNTRY_ID: '||L_country_id);
   fetch C_GET_LOCALIZED_IND into L_country_id,L_localized_ind;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_LOCALIZED_IND',
                    'MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB',
                    'LOC:'||TO_CHAR(I_location)||',COUNTRY_ID: '||L_country_id);
   close C_GET_LOCALIZED_IND;

   if L_localized_ind = 'Y' then
      if ITEM_COUNTRY_SQL.CHECK_DUP_ITEM_COUNTRY(O_error_message,
                                                 L_exists,
                                                 L_country_id,
                                                 I_item) = FALSE then
         return FALSE;
      end if;

      if L_exists = TRUE then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_ITEM_COUNTRY;
-----------------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_invalid_loc_ind   IN OUT   VARCHAR2,
                            O_valid_locs_tbl    IN OUT   LOC_TBL,
                            I_item              IN       ITEM_LOC.ITEM%TYPE,
                            I_group_type        IN       CODE_DETAIL.CODE%TYPE,
                            I_group_value       IN       VARCHAR2,
                            I_loc_type          IN       ITEM_LOC.LOC_TYPE%TYPE,
                            I_currency_code     IN       STORE.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'ITEM_LOC_SQL.CHECK_ITEM_COUNTRY';
   L_loc_tbl   locs_table_type;
   L_invalid   NUMBER := 0;
   L_exists    BOOLEAN;
   L_count     NUMBER := 1;

BEGIN

   if GROUP_LOCS_FOR_CURRENCY(O_error_message,
                              L_loc_tbl,
                              I_group_type,
                              I_group_value,
                              I_currency_code) = FALSE then
      return FALSE;
   end if;

   FOR loop_index IN 1..L_loc_tbl.COUNT LOOP
      if ITEM_LOC_SQL.CHECK_ITEM_COUNTRY(O_error_message,
                                         L_exists,
                                         I_item,
                                         L_loc_tbl(loop_index),
                                         I_loc_type) = FALSE then
         return FALSE;
      end if;

      if L_exists = TRUE then
         O_valid_locs_tbl.extend;
         O_valid_locs_tbl(L_count) := L_loc_tbl(loop_index);
         L_count := L_count + 1;
      else
         L_invalid :=  L_invalid + 1;
      end if;
   END LOOP;

   if L_invalid = 0 then
      O_invalid_loc_ind := 'N';
   elsif L_invalid = L_loc_tbl.COUNT then
      O_invalid_loc_ind := 'A';
   elsif L_invalid > 0 and L_invalid <> L_loc_tbl.COUNT then
      O_invalid_loc_ind := 'Y';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_ITEM_COUNTRY;
-----------------------------------------------------------------
FUNCTION DEL_GTAX_ITEM_ROLLUP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item               IN       ITEM_LOC.ITEM%TYPE)

RETURN BOOLEAN IS

   L_default_tax_type         SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;
   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.DEL_GTAX_ITEM_ROLLUP';

   cursor C_DEFAULT_TAX_TYPE is
      select default_tax_type
        from system_options;
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   --Retrieve default_tax_type from system_options
   open C_DEFAULT_TAX_TYPE;
   fetch C_DEFAULT_TAX_TYPE into L_default_tax_type;
   close C_DEFAULT_TAX_TYPE;

   if L_default_tax_type = 'GTAX' then
      delete from gtax_item_rollup
       where item = I_item
         and loc NOT IN (select loc
                           from item_loc
                          where item = I_item);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DEL_GTAX_ITEM_ROLLUP;
---------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_COST_LOC(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                        IN     ITEM_COST_HEAD.ITEM%TYPE,
                             I_supplier                    IN     ITEM_COST_HEAD.SUPPLIER%TYPE,
                             I_origin_country_id           IN     ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                             I_update_itemcost_child_ind   IN     VARCHAR2 DEFAULT 'N',
                             I_update_loc_ind              IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN AS
   L_program    VARCHAR2(50) := 'ITEM_LOC_SQL.INSERT_ITEM_COST_LOC';
   L_exists   VARCHAR2(1);

   TYPE country_id_rec IS TABLE OF VARCHAR2(10) INDEX BY BINARY_INTEGER;
   L_country_tbl country_id_rec;
   L_cost_head_rec      ITEM_COST_HEAD%ROWTYPE;
   L_cost_detail_rec    ITEM_COST_DETAIL%ROWTYPE;
   L_base_country_id    ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_default_tax_type   SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

   cursor C_COST_HEAD_REC is
      select *
         from item_cost_head
         where item = I_item
           and supplier = I_supplier
           and origin_country_id = I_origin_country_id
           and prim_dlvy_ctry_ind ='Y';

   cursor C_COST_DETAIL_REC( I_country_id ITEM_COST_HEAD.ORIGIN_COUNTRY_ID%TYPE) is
      select *
        from item_cost_detail
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id
         and delivery_country_id  = I_country_id;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
      select 'x'
         from item_supp_country_loc iscl
        where iscl.item = I_item
          and iscl.supplier = I_supplier
          and iscl.origin_country_id = I_origin_country_id
          and exists (select 1 
                        from mv_l10n_entity mv 
                       where TO_CHAR(iscl.loc) = mv.entity_id
                         and rownum = 1)
          for update of iscl.base_cost nowait;

   cursor C_LOCK_ITEM_COST_HEAD is
      select 'x'
        from item_cost_head
       where item = I_item
         and supplier =  I_supplier
         and origin_country_id = I_origin_country_id
         and delivery_country_id != L_base_country_id
         for update nowait;

   cursor C_LOCK_ITEM_COST_DETAIL is
      select 'x'
        from item_cost_detail
       where item = I_item
         and supplier =  I_supplier
         and origin_country_id = I_origin_country_id
         and delivery_country_id != L_base_country_id
         for update nowait;

   cursor C_LOCK_ITEM_COST_HEAD_CHILD is
      select 'x'
       from item_cost_head ich,
            item_master im
      where (im.item_parent = I_item or
             im.item_grandparent = I_item)
        and im.item_level <= im.tran_level
        and ich.item = im.item
        and ich.supplier = I_supplier
        and ich.origin_country_id = I_origin_country_id
        and ich.delivery_country_id = L_base_country_id
        for update of ich.negotiated_item_cost nowait;

   cursor C_LOCK_ITEM_COST_DETAIL_CHILD is
      select 'x'
        from item_cost_detail icd,
             item_master im
       where (im.item_parent = I_item or
              im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and icd.item = im.item
         and icd.supplier = I_supplier
         and icd.origin_country_id = I_origin_country_id
         and icd.delivery_country_id = L_base_country_id
         for update of icd.comp_rate nowait;

BEGIN
   if NOT SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                                  L_default_tax_type) then
      return FALSE;
   end if;


   If L_default_tax_type = 'GTAX' then
      if I_update_loc_ind = 'Y' then
         select distinct mv.country_id
             bulk collect into L_country_tbl
        from item_supp_country_loc iscl,
             mv_l10n_entity mv
       where iscl.item = I_item
         and iscl.supplier = I_supplier
         and iscl.origin_country_id = I_origin_country_id
         and mv.entity_id           = TO_CHAR(iscl.loc)
       and (mv.entity = 'LOC' or
         (mv.entity = 'PTNR' and mv.entity_type = 'E'))
         and not exists (select NULL
                           from item_cost_head ich
                          where ich.item = I_item
                            and ich.supplier = I_supplier
                            and ich.origin_country_id = I_origin_country_id
                            and ich.delivery_country_id = mv.country_id);
      if L_country_tbl.COUNT > 0  and L_country_tbl  IS NOT NULL then

          SQL_LIB.SET_MARK('OPEN',
                       'C_COST_HEAD_REC',
                       'ITEM_COST_HEAD',
                       'ITEM:'||I_item);
          open  C_COST_HEAD_REC;

          SQL_LIB.SET_MARK('FETCH',
                       'C_COST_HEAD_REC',
                       'ITEM_COST_HEAD',
                       'ITEM:'||I_item);
          fetch C_COST_HEAD_REC  INTO L_cost_head_rec;

         SQL_LIB.SET_MARK('OPEN',
                          'C_COST_DETAIL_REC',
                          'ITEM_COST_DETAIL',
                          'ITEM:'||I_item);
         open C_COST_DETAIL_REC(L_cost_head_rec.delivery_country_id);

          SQL_LIB.SET_MARK('FETCH',
                       'C_COST_DETAIL_REC',
                       'ITEM_COST_DETAIL',
                       'ITEM:'||I_item);
          fetch C_COST_DETAIL_REC INTO L_cost_detail_rec;

         ---
     if C_COST_HEAD_REC%ROWCOUNT > 0 then
         FORALL i in L_country_tbl.FIRST..L_country_tbl.LAST
            insert into item_cost_head(item,
                                       supplier,
                                       origin_country_id,
                                       delivery_country_id,
                                       prim_dlvy_ctry_ind,
                                       nic_static_ind,
                                       base_cost,
                                       extended_base_cost,
                                       negotiated_item_cost,
                                       inclusive_cost)
                                values(I_item,
                                       I_supplier,
                                       I_origin_country_id,
                                       L_country_tbl(i),
                                       'N',
                                       L_cost_head_rec.nic_static_ind,
                                       L_cost_head_rec.base_cost,
                                       L_cost_head_rec.extended_base_cost,
                                       L_cost_head_rec.negotiated_item_cost,
                                       L_cost_head_rec.base_cost);
     end if;
     if C_COST_DETAIL_REC%ROWCOUNT > 0 then
         FORALL i in L_country_tbl.FIRST..L_country_tbl.LAST
            insert into item_cost_detail(item,
                                         supplier,
                                         origin_country_id,
                                         delivery_country_id,
                                         cond_type,
                                         cond_value,
                                         applied_on,
                                         modified_taxable_base,
                                         comp_rate,
                                         calculation_basis,
                                         recoverable_amount)
                                  values(I_item,
                                         I_supplier,
                                         I_origin_country_id,
                                         L_country_tbl(i),
                                         L_cost_detail_rec.cond_type,
                                         L_cost_detail_rec.cond_value,
                                         L_cost_detail_rec.applied_on,
                                         L_cost_detail_rec.modified_taxable_base,
                                         L_cost_detail_rec.comp_rate,
                                         L_cost_detail_rec.calculation_basis,
                                         L_cost_detail_rec.recoverable_amount);
      end if;
     close C_COST_HEAD_REC;
     close C_COST_DETAIL_REC;
     end if;

        SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC',I_item);

        open C_LOCK_ITEM_SUPP_COUNTRY_LOC;

        SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC',I_item);

        close C_LOCK_ITEM_SUPP_COUNTRY_LOC;

        merge into item_supp_country_loc iscl
        using (select mv.country_id delivery_country_id,
                  iscl.loc loc,
                  ich.negotiated_item_cost negotiated_item_cost,
                  ich.extended_base_cost extended_base_cost,
                  ich.inclusive_cost inclusive_cost,
                  ich.base_cost base_cost
               from item_supp_country_loc iscl,
                  mv_loc_prim_addr mv,
                  item_cost_head ich
              where ich.item = I_item
               and ich.supplier = I_supplier
               and ich.origin_country_id = I_origin_country_id
               and ich.delivery_country_id = mv.country_id
               and mv.loc          = TO_CHAR(iscl.loc)
               and iscl.item = ich.item
               and iscl.supplier =ich.supplier
               and iscl.origin_country_id = ich.origin_country_id
               and iscl.unit_cost = 0) m1
        on (iscl.item = I_item
        and iscl.supplier = I_supplier
        and iscl.origin_country_id = I_origin_country_id
        and m1.loc  = iscl.loc)

        when matched then
          update
            set unit_cost = m1.negotiated_item_cost,
               negotiated_item_cost = m1.negotiated_item_cost,
               extended_base_cost = m1.extended_base_cost,
               inclusive_cost = m1.inclusive_cost,
               base_cost = m1.base_cost;
      end if;

      if SYSTEM_OPTIONS_SQL.GET_BASE_COUNTRY(O_error_message,
                                   L_base_country_id) = FALSE then
        return FALSE;
      end if;

      --sync up ISC and corresponding ICH/ICD for delivery country id other than base delivery country id.

      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_COST_HEAD','ITEM_COST_HEAD',I_item);

      open C_LOCK_ITEM_COST_HEAD;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_COST_HEAD','ITEM_COST_HEAD',I_item);

      close C_LOCK_ITEM_COST_HEAD;

      merge into item_cost_head ich
      using(select ich.negotiated_item_cost negotiated_item_cost,
               ich.extended_base_cost extended_base_cost,
               ich.inclusive_cost inclusive_cost,
               ich.base_cost base_cost
            from item_cost_head ich
           where ich.item = I_item
            and ich.supplier = I_supplier
            and ich.origin_country_id = I_origin_country_id
            and ich.delivery_country_id = L_base_country_id
            and ich.prim_dlvy_ctry_ind = 'Y') m1
      on (ich.item = I_item
      and ich.supplier = I_supplier
      and ich.origin_country_id = I_origin_country_id
      and ich.delivery_country_id != L_base_country_id)
      when matched then
        update
          set negotiated_item_cost = m1.negotiated_item_cost,
             extended_base_cost = m1.extended_base_cost,
             inclusive_cost = m1.base_cost,
             base_cost = m1.base_cost;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_COST_DETAIL','ITEM_COST_DETAIL',I_item);

      open C_LOCK_ITEM_COST_DETAIL;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_COST_DETAIL','ITEM_COST_HEAD',I_item);

      close C_LOCK_ITEM_COST_DETAIL;

      merge into item_cost_detail icd
      using(select icd.cond_value cond_value,
               icd.applied_on applied_on,
               icd.comp_rate  comp_rate,
               icd.calculation_basis calculation_basis,
               icd.recoverable_amount recoverable_amount,
               icd.modified_taxable_base modified_taxable_base
            from item_cost_detail icd
           where icd.item = I_item
            and icd.supplier = I_supplier
            and icd.origin_country_id = I_origin_country_id
            and icd.delivery_country_id = L_base_country_id) m1
      on (icd.item = I_item
      and icd.supplier = I_supplier
      and icd.origin_country_id = I_origin_country_id
      and icd.delivery_country_id != L_base_country_id )
      when matched then
        update
          set cond_value = m1.cond_value,
             applied_on = m1.applied_on,
             comp_rate = m1.comp_rate,
             calculation_basis = m1.calculation_basis,
             recoverable_amount = m1.recoverable_amount,
             modified_taxable_base = m1.modified_taxable_base;

      if I_update_itemcost_child_ind = 'Y' then
        SQL_LIB.SET_MARK('OPEN',
                     'C_LOCK_ITEM_COST_HEAD_CHILD',
                     'ITEM_COST_HEAD',
                     NULL);

        open C_LOCK_ITEM_COST_HEAD_CHILD;
        SQL_LIB.SET_MARK('CLOSE',
                     'C_LOCK_ITEM_COST_HEAD_CHILD',
                     'ITEM_COST_HEAD',
                     NULL);

        close C_LOCK_ITEM_COST_HEAD_CHILD;


        merge into item_cost_head ich
        using (select im.item,
                  ich.negotiated_item_cost negotiated_item_cost,
                  ich.extended_base_cost extended_base_cost,
                  ich.inclusive_cost inclusive_cost,
                  ich.base_cost base_cost
               from item_master im,
                  item_cost_head ich
              where (im.item_parent = I_item or im.item_grandparent = I_item)
                  and im.item_level <= im.tran_level
                  and ich.item = I_item
                  and ich.supplier = I_supplier
                  and ich.origin_country_id = I_origin_country_id
                  and ich.delivery_country_id = L_base_country_id
                  and ich.prim_dlvy_ctry_ind = 'Y') ich2
        on (ich.item = ich2.item
       and  ich.supplier = I_supplier
       and  ich.origin_country_id = I_origin_country_id
       and  ich.delivery_country_id != L_base_country_id)
       when matched then
         update
            set base_cost            = ich2.base_cost,
               extended_base_cost   = ich2.extended_base_cost,
               negotiated_item_cost = ich2.negotiated_item_cost,
               inclusive_cost       = ich2.base_cost;


        SQL_LIB.SET_MARK('OPEN',
                     'C_LOCK_ITEM_COST_DETAIL_CHILD',
                     'ITEM_COST_DETAIL',
                     NULL);

        open C_LOCK_ITEM_COST_DETAIL_CHILD;
        SQL_LIB.SET_MARK('CLOSE',
                     'C_LOCK_ITEM_COST_DETAIL_CHILD',
                     'ITEM_COST_DETAIL',
                     NULL);
        close C_LOCK_ITEM_COST_DETAIL_CHILD;

        merge into item_cost_detail icd
        using(select im.item item,
                  icd.cond_value cond_value,
                  icd.applied_on applied_on,
                  icd.comp_rate  comp_rate,
                  icd.calculation_basis calculation_basis,
                  icd.recoverable_amount recoverable_amount,
                  icd.modified_taxable_base modified_taxable_base
              from item_master im,
                  item_cost_detail icd
             where (im.item_parent = I_item or im.item_grandparent = I_item)
               and im.item_level <= im.tran_level
               and icd.item = I_item
               and icd.supplier = I_supplier
               and icd.origin_country_id = I_origin_country_id
               and icd.delivery_country_id = L_base_country_id) m1
        on (icd.item = m1.item
        and icd.supplier = I_supplier
        and icd.origin_country_id = I_origin_country_id
        and icd.delivery_country_id != L_base_country_id )
        when matched then
          update
            set cond_value = m1.cond_value,
               applied_on = m1.applied_on,
               comp_rate  = m1.comp_rate,
               calculation_basis = m1.calculation_basis,
               recoverable_amount = m1.recoverable_amount,
               modified_taxable_base = m1.modified_taxable_base;
      end if;
      return TRUE;
   else -- for Tax Type SVAT and SALES
       return TRUE;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      if C_COST_HEAD_REC%ISOPEN then
         close C_COST_HEAD_REC;
      end if;
      if C_COST_DETAIL_REC%ISOPEN then
         close C_COST_DETAIL_REC;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ITEM_COST_LOC;
-----------------------------------------------------------------------------
FUNCTION CHK_MULT_SUPPLIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item                  IN     ITEM_COST_HEAD.ITEM%TYPE)
RETURN BOOLEAN AS

  L_program    VARCHAR2(50) := 'ITEM_LOC_SQL.CHK_MULT_SUPPLIER';

  TYPE supplier_typ IS RECORD(supplier          ITEM_SUPPLIER.SUPPLIER%TYPE,
                              origin_country_id ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE);

  TYPE L_supp_rec IS TABLE OF supplier_typ INDEX BY BINARY_INTEGER;

  L_supp_tbl L_supp_rec;

BEGIN
   select isup.supplier,
          isc.origin_country_id
          bulk collect into L_supp_tbl
     from item_supplier isup,
          item_supp_country isc
    where isup.item =isc.item
      and isup.supplier = isc.supplier
      and isup.item = I_item;


   if L_supp_tbl.COUNT >0 and L_supp_tbl is NOT NULL then
      FOR i in L_supp_tbl.FIRST..L_supp_tbl.LAST LOOP
         if ITEM_LOC_SQL.INSERT_ITEM_COST_LOC(O_error_message,
                                              I_item,
                                              L_supp_tbl(i).supplier,
                                              L_supp_tbl(i).origin_country_id,
                                              'N',
                                              'Y') = FALSE then
            return FALSE;
         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_MULT_SUPPLIER;
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_ORG_UNIT_ID(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_supp_loc_same_ou  IN OUT BOOLEAN,
                           I_locs_tbl          IN LOC_TBL,
                           I_supplier          IN SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS
   L_program                       VARCHAR2(64)        := 'ITEM_LOC_SQL.CHECK_ORG_UNIT_ID';
   L_org_unit_id                   PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE;
   L_dummy                         VARCHAR2(1);

   cursor C_CHECK_ORG_UNIT_ID is
      select 'x'
        from TABLE(CAST(I_locs_tbl as LOC_TBL)) l,
             v_loc_ou vlo
       where vlo.location = value(l)
         and not exists (select 'x'
                           from partner_org_unit pou
                          where partner = I_supplier
                            and pou.org_unit_id = vlo.org_unit_id
                            and rownum = 1)
         and rownum = 1;
BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN','C_CHECK_ORG_UNIT_ID','v_loc_ou',I_supplier);
   open C_CHECK_ORG_UNIT_ID;

   SQL_LIB.SET_MARK('FETCH','C_CHECK_ORG_UNIT_ID','v_loc_ou',I_supplier);
   fetch C_CHECK_ORG_UNIT_ID into L_dummy;

   if C_CHECK_ORG_UNIT_ID%NOTFOUND then
      O_supp_loc_same_ou := TRUE;
   else
      O_supp_loc_same_ou := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_ORG_UNIT_ID','v_loc_ou',I_supplier);
   close C_CHECK_ORG_UNIT_ID;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_ORG_UNIT_ID;
-----------------------------------------------------------------------------------------------
FUNCTION AUTO_RANGE_CONTAINER_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_content_item      IN     ITEM_MASTER.ITEM%TYPE,
                                   I_container_item    IN     ITEM_MASTER.ITEM%TYPE)


RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'ITEM_LOC_SQL.AUTO_RANGE_CONTAINER_ITEM';

   L_input_tbl   NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
   L_index       NUMBER              := 0;

   cursor C_GET_ITEMLOC_REC is
      select I_container_item
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,loc
            ,loc_type
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,'A'
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,'N'
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,NULL
            ,'N'
            ,NULL
            ,NULL
        from item_loc il
       where item = I_content_item
         and NOT EXISTS (select 'x'
                           from item_loc il1
                          where il1.item = I_container_item
                            and il1.loc = il.loc);

BEGIN

   open C_GET_ITEMLOC_REC;
   fetch C_GET_ITEMLOC_REC bulk collect into L_input_tbl;
   close C_GET_ITEMLOC_REC;
   ---
   if L_input_tbl is NULL or L_input_tbl.COUNT = 0 then
      return TRUE;
   end if;
   ---
   if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                    L_input_tbl,
                                    'N') = FALSE then
      return FALSE;
   end if;
   ---

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END AUTO_RANGE_CONTAINER_ITEM;
-----------------------------------------------------------------------------------------------
FUNCTION COST_LOC_EXISTS_FOR_CHILD(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists           IN OUT BOOLEAN,
                                   I_item             IN     ITEM_LOC.ITEM%TYPE,
                                   I_cost_loc         IN     ITEM_LOC.COSTING_LOC%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'ITEM_LOC_SQL.COST_LOC_EXISTS_FOR_CHILD';
   L_exists    VARCHAR2(1)    := NULL;

   cursor C_GET_CHILD is
      select  'x'
       from item_master im ,item_loc il
      where im.item = il.item
        and im.item_level <= im.tran_level
        and (im.item_parent = I_item or im.item_grandparent = I_item)
        and not exists(select item
                         from item_loc il2
                        where il2.item = im.item
                          and il2.loc = I_cost_loc)
        and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CHILD',
                    'ITEM_MASTER',
                    I_item);
   open C_GET_CHILD;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CHILD',
                    'ITEM_MASTER',
                    I_item);
   fetch C_GET_CHILD into L_exists;
   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CHILD',
                    'ITEM_MASTER',
                    I_item);
   close C_GET_CHILD;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_CHILD%ISOPEN then
         close C_GET_CHILD;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END COST_LOC_EXISTS_FOR_CHILD;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COSTINGLOC_CHANGE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_status           IN OUT ITEM_MASTER.STATUS%TYPE,
                                    O_event_type       IN OUT COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE,
                                    O_exists           IN OUT BOOLEAN,
                                    I_item             IN     ITEM_LOC.ITEM%TYPE,
                                    I_location         IN     ITEM_LOC.LOC%TYPE,
                                    I_item_level       IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                                    I_tran_level       IN     ITEM_MASTER.TRAN_LEVEL%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'ITEM_LOC_SQL.VALIDATE_COSTINGLOC_CHANGE';
   L_valid     VARCHAR2(1);

   cursor C_GET_STATUS is
      select status
        from item_master
       where item= I_item;

   cursor C_CHECK_EVENT_TYPE is
      select event_run_type
        from cost_event_run_type_config
       where event_type='CL';

   cursor C_VALIDATE_COST_EVENT is
      select 'x'
        from cost_event_cl cl,
             item_master im,
             period,
             cost_event_result cr
       where cl.item            = im.item
         and (
              (im.item = I_item and I_item_level = I_tran_level)
              or
              (im.item_level <= im.tran_level and (im.item_parent = I_item or im.item_grandparent = I_item) and I_item_level < I_tran_level)
              )
         and cl.franchise_loc   = I_location
         and cl.cost_loc_change_date <= vdate
         and ((cl.cost_event_process_id = cr.cost_event_process_id
         and cr.status != 'C') or(cl.cost_event_process_id not in(select cost_event_process_id from cost_event_result)))
         and rownum = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_STATUS',
                    'ITEM_MASTER',
                     I_item);
   open C_GET_STATUS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_STATUS',
                    'ITEM_MASTER',
                     I_item);
   fetch C_GET_STATUS into O_status;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_STATUS',
                    'ITEM_MASTER',
                     I_item);
   close C_GET_STATUS;
   if O_status != 'A' then
      O_exists := TRUE;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_EVENT_TYPE',
                    'COST_EVENT_RUN_TYPE_CONFIG',
                     NULL);

   open C_CHECK_EVENT_TYPE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_EVENT_TYPE',
                    'COST_EVENT_RUN_TYPE_CONFIG',
                     NULL);
   fetch C_CHECK_EVENT_TYPE into O_event_type;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_EVENT_TYPE',
                    'COST_EVENT_RUN_TYPE_CONFIG',
                     NULL);
   close C_CHECK_EVENT_TYPE;

   if O_event_type not in ('BATCH','ASYNC') then
      O_exists := TRUE;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_VALIDATE_COST_EVENT',
                    'COST_EVENT_CL',
                     I_item);
   open C_VALIDATE_COST_EVENT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_VALIDATE_COST_EVENT',
                    'COST_EVENT_CL',
                     I_item);
   fetch C_VALIDATE_COST_EVENT into L_valid;

   if C_VALIDATE_COST_EVENT %NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_VALIDATE_COST_EVENT',
                       'COST_EVENT_CL',
                        I_item);
      close C_VALIDATE_COST_EVENT;
      O_exists := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('COST_LOC_UPDATED', NULL, NULL, NULL);
   else
      O_error_message := SQL_LIB.CREATE_MSG('COST_LOC_PENDING', NULL, NULL, NULL);
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_STATUS%ISOPEN then
         close C_GET_STATUS;
      end if;
      if C_CHECK_EVENT_TYPE%ISOPEN then
         close C_CHECK_EVENT_TYPE;
      end if;
      if C_VALIDATE_COST_EVENT%ISOPEN then
         close C_VALIDATE_COST_EVENT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_COSTINGLOC_CHANGE;
-----------------------------------------------------------------------------------------------
FUNCTION WF_STORE_EXIST(O_error_message   IN OUT   VARCHAR2,
                        O_f_loc_exist     IN OUT   VARCHAR2,
                        O_c_loc_exist     IN OUT   VARCHAR2,
                        I_group_type      IN       CODE_DETAIL.CODE%TYPE,
                        I_group_value     IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'ITEM_LOC_SQL.WF_STORE_EXIST';

   L_store_table   locs_table_type;
   L_loc           STORE.STORE%TYPE;

   /*This cursor validates that the I_group_value has at least one franchise loc.*/
   cursor C_FRANCHISE_EXIST is
      select 'Y'
        from v_store vs
       where vs.store      = L_loc
         and vs.store_type = 'F';

   cursor C_NON_FRANCHISE_EXIST is
      select 'Y'
        from v_store vs
       where vs.store      = L_loc
         and vs.store_type = 'C';

   cursor C_FRANCHISE_STORE is
      select 'Y'
        from v_store vs
       where vs.store_type = 'F'
         and vs.store      = I_group_value;

BEGIN

   O_f_loc_exist := 'N';
   O_c_loc_exist := 'N';

   if I_group_type IN ('AL', 'AS', 'LLS', 'DW', 'L', 'T', 'A', 'R', 'D', 'C') then

      if GROUP_LOCS_FOR_CURRENCY(O_error_message,
                                 L_store_table,
                                 I_group_type,
                                 I_group_value,
                                 NULL) = FALSE then
         return FALSE;
      end if;

      if L_store_table is NOT NULL and
         L_store_table.EXISTS(1) = TRUE then
         --check the group for franchise stores.
         FOR loop_index IN 1..L_store_table.COUNT LOOP

            L_loc := L_store_table(loop_index);
            open C_FRANCHISE_EXIST;

            fetch C_FRANCHISE_EXIST into O_f_loc_exist;
            if C_FRANCHISE_EXIST%FOUND then
               close C_FRANCHISE_EXIST;
               EXIT;
            end if;
            close C_FRANCHISE_EXIST;

         END LOOP;

         --check the group for company stores.
         FOR loop_index IN 1..L_store_table.COUNT LOOP

            L_loc := L_store_table(loop_index);
            open C_NON_FRANCHISE_EXIST;

            fetch C_NON_FRANCHISE_EXIST into O_c_loc_exist;
            if C_NON_FRANCHISE_EXIST%FOUND then
               close C_NON_FRANCHISE_EXIST;
               EXIT;
            end if;
            close C_NON_FRANCHISE_EXIST;

         END LOOP;

      end if;

   elsif I_group_type = 'S' then

      open C_FRANCHISE_STORE;
      fetch C_FRANCHISE_STORE into O_f_loc_exist;
      close C_FRANCHISE_STORE;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END WF_STORE_EXIST;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_WH_TO_WH(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_group_type       IN       CODE_DETAIL.CODE_TYPE%TYPE,
                           I_group_value      IN       WH.WH%TYPE,
                           I_source_wh        IN       WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'ITEM_LOC_SQL.VALIDATE_WH_TO_WH';
   L_exists  VARCHAR2(1);

   cursor C_PWH_EXIST is
      select 'X'
        from v_wh
       where v_wh.physical_wh = I_group_value
         and v_wh.wh          = I_source_wh;

   cursor C_LLW_EXIST is
      select 'X'
        from loc_list_detail
       where loc_list = I_group_value
         and location = I_source_wh;

BEGIN

   if I_group_type = 'W' then
      if I_group_value = I_source_wh then
         O_error_message := SQL_LIB.CREATE_MSG('WH_TO_WH_SAME',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_group_type = 'PW' then
      open C_PWH_EXIST;
      fetch C_PWH_EXIST into L_exists;
      if C_PWH_EXIST%FOUND then
         close C_PWH_EXIST;
         O_error_message := SQL_LIB.CREATE_MSG('WH_TO_PWH_SAME',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      close C_PWH_EXIST;
   elsif I_group_type = 'LLW' then
      open C_LLW_EXIST;
      fetch C_LLW_EXIST into L_exists;
      if C_LLW_EXIST%FOUND then
         close C_LLW_EXIST;
         O_error_message := SQL_LIB.CREATE_MSG('WH_TO_LLWH_SAME',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      close C_LLW_EXIST;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_WH_TO_WH;
-----------------------------------------------------------------------------------
END ITEM_LOC_SQL;
/
