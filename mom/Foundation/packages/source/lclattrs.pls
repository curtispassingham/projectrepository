
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LOCLIST_ATTRIBUTE_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------------------------
-- Function:  GET_NAME
-- Purpose:   To validate the location list that is passed to the function
--            and pass back the location list description to the calling form.
------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message IN OUT VARCHAR2,
                  O_loc_list_desc IN OUT LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                  I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  GET_LOC_COUNT
-- Purpose:   To get the total number of stores and warehouses on the location 
--            list.
------------------------------------------------------------------------------
FUNCTION GET_LOC_COUNT(O_error_message IN OUT VARCHAR2,
                       O_loc_count     IN OUT NUMBER,
                       I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  GET_HEADER_INFO
-- Purpose:   To get the header information
------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message     IN OUT VARCHAR2,
                         O_loc_list_desc     IN OUT LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                         O_create_date       IN OUT LOC_LIST_HEAD.CREATE_DATE%TYPE,
                         O_last_rebuild_date IN OUT LOC_LIST_HEAD.LAST_REBUILD_DATE%TYPE,
                         O_create_id         IN OUT LOC_LIST_HEAD.CREATE_ID%TYPE,
                         O_static_ind        IN OUT LOC_LIST_HEAD.STATIC_IND%TYPE,
                         O_batch_rebuild_ind IN OUT LOC_LIST_HEAD.BATCH_REBUILD_IND%TYPE,
                         O_source            IN OUT LOC_LIST_HEAD.SOURCE%TYPE,
                         O_external_ref_no   IN OUT LOC_LIST_HEAD.EXTERNAL_REF_NO%TYPE,
                         O_comment_desc      IN OUT LOC_LIST_HEAD.COMMENT_DESC%TYPE,
                         O_filter_org_id     IN OUT LOC_LIST_HEAD.FILTER_ORG_ID%TYPE,
                         I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  GET_HEADER_INFO
-- Purpose:   To get the header information
------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message     IN OUT VARCHAR2,
                         O_loc_list_desc     IN OUT LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                         O_create_date       IN OUT LOC_LIST_HEAD.CREATE_DATE%TYPE,
                         O_last_rebuild_date IN OUT LOC_LIST_HEAD.LAST_REBUILD_DATE%TYPE,
                         O_create_id         IN OUT LOC_LIST_HEAD.CREATE_ID%TYPE,
                         O_static_ind        IN OUT LOC_LIST_HEAD.STATIC_IND%TYPE,
                         O_batch_rebuild_ind IN OUT LOC_LIST_HEAD.BATCH_REBUILD_IND%TYPE,
                         O_source            IN OUT LOC_LIST_HEAD.SOURCE%TYPE,
                         O_external_ref_no   IN OUT LOC_LIST_HEAD.EXTERNAL_REF_NO%TYPE,
                         O_comment_desc      IN OUT LOC_LIST_HEAD.COMMENT_DESC%TYPE,
                         I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  GET_SOURCE
-- Purpose:   To get the source field from the LOC_LIST_HEAD table.
------------------------------------------------------------------------------
FUNCTION GET_SOURCE(O_error_message IN OUT VARCHAR2,
                    O_source        IN OUT LOC_LIST_HEAD.SOURCE%TYPE,
                    I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  STORE_EXISTS
-- Purpose:   To check if there is any store on the location list.
------------------------------------------------------------------------------
FUNCTION STORE_EXISTS(O_error_message IN OUT VARCHAR2,
                      O_exist         IN OUT BOOLEAN,
                      I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function:  WH_EXISTS
-- Purpose:   To check if there is any warehouse on the location list.
------------------------------------------------------------------------------
FUNCTION WH_EXISTS(O_error_message IN OUT VARCHAR2,
                   O_exist         IN OUT BOOLEAN,
                   I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Overloaded version: checks for stockholding wh's only
------------------------------------------------------------------------------
FUNCTION WH_EXISTS(O_error_message     IN OUT VARCHAR2,
                   O_exist             IN OUT BOOLEAN,
                   I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE,
                   I_chk_stockhold_ind IN     VARCHAR2)
         RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function:  CRITERIA_EXIST
-- Purpose:   To check if there is any criteria records exist for the location list.
------------------------------------------------------------------------------
FUNCTION CRITERIA_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exist         IN OUT BOOLEAN,
                        I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function:  GET_LAST_REBUILD_DATE
-- Purpose:   fetches the last rebuild date for the given location list.
------------------------------------------------------------------------------
FUNCTION GET_LAST_REBUILD_DATE(O_error_message     IN OUT VARCHAR2,
                               O_last_rebuild_date IN OUT LOC_LIST_HEAD.LAST_REBUILD_DATE%TYPE,
                               I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function Name: DELETE_LOC_LIST_HEAD_TL
-- Purpose:       This function deletes records from the loc_list_head_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_LOC_LIST_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_loc_list        IN       LOC_LIST_HEAD_TL.LOC_LIST%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

END LOCLIST_ATTRIBUTE_SQL;
/
