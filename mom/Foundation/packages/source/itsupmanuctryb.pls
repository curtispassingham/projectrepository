CREATE OR REPLACE PACKAGE BODY ITEM_SUPP_MANU_COUNTRY_SQL AS
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_MANU_CTRY_TO_CHILDREN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_item                IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                      I_supplier            IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                      I_manu_country_id     IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE,
                                      I_primary_country_ind IN     VARCHAR2,
                                      I_replace_ind         IN     VARCHAR2 DEFAULT 'N')
return BOOLEAN is
   L_program                VARCHAR2(64)   := 'ITEM_SUPP_MANU_COUNTRY_SQL.INSERT_MANU_CTRY_TO_CHILDREN';
   L_child_item             ITEM_MASTER.ITEM%TYPE;
   L_bracket_ind            VARCHAR2(1)    := NULL;
   L_inv_mgmt_level         VARCHAR2(6)    := NULL;

   -- Retrieving the child items for which there is no entry in the item_supp_manu_country for that item/supplier/country combination.

   cursor C_GET_CHILD_ITEMS is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and not exists (select 'x'
                           from item_supp_manu_country imc
                          where imc.item = im.item
                            and imc.supplier = I_supplier
                            and imc.manu_country_id = I_manu_country_id);

   -- Retrieving the child items for which there is no entry in the item_supp_manu_country for that item/supplier combination.

   cursor C_GET_CHILD_ITEMS_1 is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and not exists (select 'x'
                            from item_supp_manu_country imc
                           where imc.item = im.item
                             and imc.supplier = I_supplier);

   -- Retrieving the child items for which different manufacturer countries exist but there is no entry for parent's
   -- primary origin country.

   cursor C_GET_CHILD_ITEMS_2 is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and exists (select 'x'
                        from item_supp_manu_country imc
                       where imc.item = im.item
                         and imc.supplier = I_supplier)
       MINUS
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and exists (select 'x'
                        from item_supp_manu_country imc
                       where imc.item = im.item
                         and imc.supplier = I_supplier
                         and imc.manu_country_id = I_manu_country_id);

   -- Retrieving the child items for which there exist an entry in the item_supp_manu_country corresponding to
   -- parent's primary origin country.

   cursor C_GET_CHILD_ITEMS_3 is
       select im.item
         from item_master im
        where (im.item_parent = I_item
               or im.item_grandparent = I_item)
          and im.item_level <= im.tran_level
          and exists (select 'x'
                        from item_supplier its
                       where its.item = im.item
                         and its.supplier = I_supplier)
          and exists (select 'x'
                        from item_supp_manu_country imc
                       where imc.item = im.item
                         and imc.supplier = I_supplier
                         and imc.manu_country_id = I_manu_country_id
                         and imc.Primary_manu_ctry_ind = 'N');

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_manu_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_manu_country_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_primary_country_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_primary_country_ind',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   --
   if I_primary_country_ind = 'N' then
      for c_rec in C_GET_CHILD_ITEMS
      loop
         L_child_item := c_rec.item;
            if INS_MANU_CTRY_IND_TO_CHILDREN(O_error_message,
                                             I_item,
                                             L_child_item,
                                             I_supplier,
                                             I_manu_country_id,
                                             'N',
                                             'N',
                                             'Y') = FALSE then
            return FALSE;
         end if;
      end loop;
      ---
   else
      for c_rec in C_GET_CHILD_ITEMS_1
      loop
         L_child_item := c_rec.item;
         if INS_MANU_CTRY_IND_TO_CHILDREN(O_error_message,
                                          I_item,
                                          L_child_item,
                                          I_supplier,
                                          I_manu_country_id,
                                          'Y',
                                          'N',
                                          'Y') = FALSE then
            return FALSE;
         end if;
      end loop;
      ---
      for c_rec in C_GET_CHILD_ITEMS_2
      loop
         L_child_item := c_rec.item;
         if INS_MANU_CTRY_IND_TO_CHILDREN(O_error_message,
                                          I_item,
                                          L_child_item,
                                          I_supplier,
                                          I_manu_country_id,
                                          I_replace_ind,
                                          I_replace_ind,
                                          'Y') = FALSE then
            return FALSE;
         end if;
      end loop;
         ---
      if I_replace_ind = 'Y' then
         ---
         for c_rec in C_GET_CHILD_ITEMS_3
         loop
            L_child_item := c_rec.item;
            if INS_MANU_CTRY_IND_TO_CHILDREN(O_error_message,
                                              I_item,
                                              L_child_item,
                                              I_supplier,
                                              I_manu_country_id,
                                              'Y',
                                              'Y',
                                              'N') = FALSE then
               return FALSE;
            end if;
         end loop;
         ---
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_MANU_CTRY_TO_CHILDREN;
-------------------------------------------------------------------------------------------------------
FUNCTION INS_MANU_CTRY_IND_TO_CHILDREN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item                IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                       I_child_item          IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                       I_supplier            IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                       I_manu_country_id     IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE,
                                       I_primary_country_ind IN     VARCHAR2,
                                       I_update_ind          IN     VARCHAR2,
                                       I_insert_ind          IN     VARCHAR2)
return BOOLEAN is

   L_program                VARCHAR2(64)   := 'ITEM_SUPP_MANU_COUNTRY_SQL.INS_MANU_CTRY_IND_TO_CHILDREN';

   cursor C_LOCK_COUNTRY_Y is
      select 'x'
        from item_supp_manu_country
       where primary_manu_ctry_ind    = 'Y'
         and manu_country_id != I_manu_country_id
         and supplier         = I_supplier
         and item             = I_child_item
         for update nowait;

   cursor C_LOCK_COUNTRY_N is
      select 'x'
        from item_supp_manu_country
       where primary_manu_ctry_ind    = 'N'
         and manu_country_id  = I_manu_country_id
         and supplier         = I_supplier
         and item             = I_child_item
         for update nowait;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_child_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_child_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_manu_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_manu_country_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_primary_country_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_primary_country_ind',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_update_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_update_ind',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_insert_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_insert_ind',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_insert_ind = 'Y' then
      ---
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'ITEM_SUPP_MANU_COUNTRY',
                       'Item: '||I_child_item||' Supplier: '||to_char(I_supplier)||' Origin_Country_Id: '|| I_manu_country_id);
      insert into item_supp_manu_country(item,
                                         supplier,
                                         manu_country_id,
                                         primary_manu_ctry_ind)
                                 values (I_child_item,
                                         I_supplier,
                                         I_manu_country_id,
                                         I_primary_country_ind);

   end if;
   --
   if I_update_ind = 'Y' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_COUNTRY_Y',
                       'ITEM_SUPP_MANU_COUNTRY',
                       'Item: '||I_child_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
      open C_LOCK_COUNTRY_Y;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_COUNTRY_Y',
                       'ITEM_SUPP_MANU_COUNTRY',
                       'Item: '||I_child_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
      close C_LOCK_COUNTRY_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_SUPP_MANU_COUNTRY',
                       'Country: '|| I_manu_country_id);
      update item_supp_manu_country imc1
         set imc1.primary_manu_ctry_ind    = 'N'
       where imc1.primary_manu_ctry_ind    = 'Y'
         and imc1.manu_country_id != I_manu_country_id
         and imc1.supplier         = I_supplier
         and imc1.item             = I_child_item;
      ---
      if I_insert_ind = 'N' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_COUNTRY_N',
                          'ITEM_SUPP_MANU_COUNTRY',
                          'Item: '||I_child_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
         open C_LOCK_COUNTRY_N;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_COUNTRY_N',
                          'ITEM_SUPP_MANU_COUNTRY',
                          'Item: '||I_child_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
         close C_LOCK_COUNTRY_N;
         ---
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'ITEM_SUPP_MANU_COUNTRY',
                          'Country: '|| I_manu_country_id);
         update item_supp_manu_country isc1
            set isc1.primary_manu_ctry_ind    = 'Y'
          where isc1.primary_manu_ctry_ind    = 'N'
            and isc1.manu_country_id  = I_manu_country_id
            and isc1.supplier         = I_supplier
            and isc1.item             = I_child_item;
         ---
      end if;
      ---
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INS_MANU_CTRY_IND_TO_CHILDREN;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PRIM_MANU_TO_CHILDREN(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_child_primary_exists_ind IN OUT BOOLEAN,
                                     I_item                     IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                     I_supplier                 IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                     I_manu_country_id          IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
return BOOLEAN is
   L_program               VARCHAR2(64)   := 'ITEM_SUPP_MANU_COUNTRY_SQL.CHECK_PRIM_MANU_TO_CHILDREN';
   L_exists                VARCHAR2(1)    := NULL;

   cursor C_PRIMARY_EXISTS is
      select 'x'
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                           from item_supp_manu_country imc
                          where imc.item = im.item
                            and imc.supplier = I_supplier
                            and imc.manu_country_id != I_manu_country_id
                            and imc.primary_manu_ctry_ind = 'Y');

BEGIN
   O_child_primary_exists_ind := FALSE;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_manu_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_manu_country_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_PRIMARY_EXISTS',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Manufacturer_Country_Id: '|| I_manu_country_id);
   open C_PRIMARY_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PRIMARY_EXISTS',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Manufacturer_Country_Id: '|| I_manu_country_id);

   fetch C_PRIMARY_EXISTS into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PRIMARY_EXISTS',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Manufacturer_Country_Id: '|| I_manu_country_id);
   close C_PRIMARY_EXISTS;

   if L_exists = 'x' then
      O_child_primary_exists_ind := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_PRIM_MANU_TO_CHILDREN;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_PRIM_MANU_EXISTS(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists                   IN OUT BOOLEAN,
                                      I_item                     IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                      I_supplier                 IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE)
return BOOLEAN is
   L_program               VARCHAR2(64)   := 'ITEM_SUPP_MANU_COUNTRY_SQL.CHECK_CHILD_PRIM_MANU_EXISTS';
   L_exists                VARCHAR2(1)    := NULL;

   cursor C_PRIMARY_EXISTS is
      select 'x'
        from item_master im
       where (im.item_parent = I_item
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         and exists (select 'x'
                           from item_supp_manu_country imc
                          where imc.item                  = im.item
                            and imc.supplier              = I_supplier
                            and imc.primary_manu_ctry_ind = 'Y');

BEGIN
   O_exists := FALSE;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_PRIMARY_EXISTS',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   open C_PRIMARY_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PRIMARY_EXISTS',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   fetch C_PRIMARY_EXISTS into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PRIMARY_EXISTS',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_PRIMARY_EXISTS;
   ---
   if L_exists is NOT NULL then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CHILD_PRIM_MANU_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_MANU_CTRY_TO_COMP_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_pack_no           IN     ITEM_MASTER.ITEM%TYPE,
                                       I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                       I_manu_country_id   IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
return BOOLEAN is

   L_program       VARCHAR2(64)  := 'ITEM_SUPP_MANU_COUNTRY_SQL.INSERT_MANU_CTRY_TO_COMP_ITEM';
   L_item          ITEM_MASTER.ITEM%TYPE;
   L_qty           V_PACKSKU_QTY.QTY%TYPE;
   L_exists        BOOLEAN;
   L_prim_country  ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE;
   L_prim_supp     ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE;
   L_const_dim_ind ITEM_MASTER.CONST_DIMEN_IND%TYPE;

   cursor C_ITEM is
      select vpq.item
        from v_packsku_qty vpq
       where vpq.pack_no = I_pack_no;

BEGIN

   ---
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_pack_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_manu_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_manu_country_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM',
                    'V_PACKSKU_QTY',
                    'Item: '||I_pack_no);
   open C_ITEM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM',
                    'V_PACKSKU_QTY',
                    'Item: '||I_pack_no);
   fetch C_ITEM into L_item;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM',
                    'V_PACKSKU_QTY',
                    'Item: '||I_pack_no);
   close C_ITEM;
   ---
   if L_item is NOT NULL then
      if ITEM_SUPP_MANU_COUNTRY_SQL.ITEM_SUPP_MANU_CTRY_EXISTS(O_error_message,
                                                               L_exists,
                                                               L_item,
                                                               I_supplier) = FALSE then
         return FALSE;
      end if;

      if L_exists = TRUE then
         L_prim_country := 'N';
      else
         L_prim_country := 'Y';
      end if;

      ---
      BEGIN
         insert into item_supp_manu_country(item,
                                            supplier,
                                            manu_country_id,
                                            primary_manu_ctry_ind)
                                    values (L_item,
                                            I_supplier,
                                            I_manu_country_id,
                                            L_prim_country);

      EXCEPTION
         when DUP_VAL_ON_INDEX then
            return TRUE;
      END;
      --
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_MANU_CTRY_TO_COMP_ITEM;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPP_MANU_CTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_ismc_rec      IN ITEM_SUPP_MANU_COUNTRY%ROWTYPE)
return BOOLEAN is
   L_program      VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.INSERT_ITEM_SUPP_MANU_CTRY';
   L_exists       BOOLEAN;

BEGIN
   ---
   if ITEM_SUPP_MANU_COUNTRY_SQL.ITEM_SUPP_MANU_CTRY_EXISTS(O_error_message,
                                                            L_exists,
                                                            I_ismc_rec.item,
                                                            I_ismc_rec.supplier,
                                                            I_ismc_rec.manu_country_id) = FALSE then
      return FALSE;
   end if;
   ---
   if L_exists = FALSE then
                                                            
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'ITEM_SUPP_COUNTRY',
                       'item: '||I_ismc_rec.item||' supplier: '||I_ismc_rec.supplier||' country: '||I_ismc_rec.manu_country_id);

     insert into ITEM_SUPP_MANU_COUNTRY (item,
                                         supplier,
                                         manu_country_id,
                                         primary_manu_ctry_ind)
                                 values (I_ismc_rec.item,
                                         I_ismc_rec.supplier,
                                         I_ismc_rec.manu_country_id,
                                         I_ismc_rec.primary_manu_ctry_ind);
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ITEM_SUPP_MANU_CTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPP_MANU_CTRY(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                    IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                    I_supplier                IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                    I_manu_country_id         IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE,
                                    I_primary_manu_ctry_ind   IN     ITEM_SUPP_MANU_COUNTRY.PRIMARY_MANU_CTRY_IND%TYPE)
   RETURN BOOLEAN is
      L_program                     VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.INSERT_ITEM_SUPP_MANU_CTRY';
      L_exists                      BOOLEAN;
      L_item_supp_manu_ctry         ITEM_SUPP_MANU_COUNTRY%ROWTYPE;
BEGIN
   L_item_supp_manu_ctry.item := I_item;
   L_item_supp_manu_ctry.supplier := I_supplier;
   L_item_supp_manu_ctry.manu_country_id := I_manu_country_id;
   L_item_supp_manu_ctry.primary_manu_ctry_ind := I_primary_manu_ctry_ind;
   ---
   if ITEM_SUPP_MANU_COUNTRY_SQL.INSERT_ITEM_SUPP_MANU_CTRY(O_error_message,
                                                            L_item_supp_manu_ctry) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ITEM_SUPP_MANU_CTRY;
--------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_INDICATORS( O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                    I_manu_country   IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program               VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.UPDATE_PRIMARY_INDICATORS';
   L_table                 VARCHAR2(30);
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   --- Following cursor called when only item/supplier/country passed in---
   cursor C_LOCK_COUNTRY_Y is
      select 'x'
        from item_supp_manu_country
       where primary_manu_ctry_ind = 'Y'
         and manu_country_id      != I_manu_country
         and supplier              = I_supplier
         and item                  = I_item
         for update nowait;
BEGIN
   ---
   -- Lock the item_supp_country record, update the primary country ind to
   -- No for I_item with manu country other than the input country
   ---
   L_table := 'ITEM_SUPP_MANU_COUNTRY';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_COUNTRY_Y',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country);
   open C_LOCK_COUNTRY_Y;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_COUNTRY_Y',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country);
   close C_LOCK_COUNTRY_Y;
   ---
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Country: '|| I_manu_country);
   update item_supp_manu_country ismc1
      set ismc1.primary_manu_ctry_ind  = 'N'
    where ismc1.primary_manu_ctry_ind  = 'Y'
      and ismc1.manu_country_id       != I_manu_country
      and ismc1.supplier               = I_supplier
      and ismc1.item                   = I_item
      and exists( select 'x'
                    from item_supp_manu_country ismc2
                   where ismc2.item            = ismc1.item
                     and ismc2.supplier        = ismc1.supplier
                     and ismc2.manu_country_id = I_manu_country);
   ---

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;
END UPDATE_PRIMARY_INDICATORS;
----------------------------------------------------------------------------------------------------
FUNCTION UPDATE_YES_PRIM_MANU_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_ismc_rec      IN     ITEM_SUPP_MANU_COUNTRY%ROWTYPE)
return BOOLEAN is
   L_program      VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.UPDATE_YES_PRIM_MANU_IND';
   L_table_locked VARCHAR2(10);

--- NOTE:  only used for XITEM subscription API after UPDATE_PRIMARY_INDICATORS is called
---
BEGIN
   if LOCK_ITEM_SUPP_MANU_CTRY(O_error_message,
                               L_table_locked,
                               I_ismc_rec.item,
                               I_ismc_rec.supplier,
                               I_ismc_rec.manu_country_id) = FALSE then
      if L_table_locked != 'LOCKED' then
         return FALSE;
      end if;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                     NULL, 
                     'ITEM_SUPP_MANU_COUNTRY',
                     'item: '||I_ismc_rec.item||' supplier: '||I_ismc_rec.supplier||' country: '||I_ismc_rec.manu_country_id);

   update ITEM_SUPP_MANU_COUNTRY
      set primary_manu_ctry_ind = 'Y'
    where item                    = I_ismc_rec.item
      and supplier                = I_ismc_rec.supplier
      and manu_country_id = I_ismc_rec.manu_country_id;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_YES_PRIM_MANU_IND;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_SUPP_MANU_COUNTRY(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item               IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                       I_supplier           IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                       I_manu_country_id    IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
return BOOLEAN is
   L_program      VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.DELETE_ITEM_SUPP_MANU_COUNTRY';
   L_table_locked VARCHAR2(10);

BEGIN
   if LOCK_ITEM_SUPP_MANU_CTRY(O_error_message,
                               L_table_locked,
                               I_item,
                               I_supplier,
                               I_manu_country_id) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ITEM_SUPP_MANU_COUNTRY',
                    'item: '||I_item||' supplier: '||I_supplier||' country: '||I_manu_country_id);

   delete from ITEM_SUPP_MANU_COUNTRY
    where item            = I_item
      and supplier        = I_supplier
      and manu_country_id = I_manu_country_id;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ITEM_SUPP_MANU_COUNTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION ITEM_SUPP_MANU_CTRY_EXISTS (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exist              IN OUT BOOLEAN,
                                     I_item               IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                     I_supplier           IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                     I_manu_country_id    IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE DEFAULT NULL)
return BOOLEAN is
   L_rec_exists       VARCHAR2(1)  := NULL;
   L_program          VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.ITEM_SUPP_MANU_CTRY_EXISTS';

   cursor C_ITEM_SUPP_MANU_COUNTRY is
      select 'x'
        from item_supp_manu_country imc
       where imc.item            = I_item
         and imc.supplier        = NVL(I_supplier, imc.supplier)
         and imc.manu_country_id = NVL(I_manu_country_id, imc.manu_country_id)
         and rownum = 1;

BEGIN
   O_exist := FALSE;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_SUPP_MANU_COUNTRY',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '||I_manu_country_id);
   open C_ITEM_SUPP_MANU_COUNTRY;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_SUPP_MANU_COUNTRY',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' supplier: '||to_char(I_supplier)||' Country: '||I_manu_country_id);
   fetch C_ITEM_SUPP_MANU_COUNTRY into L_rec_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_SUPP_MANU_COUNTRY',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' supplier: '||to_char(I_supplier)||' Country: '||I_manu_country_id);
   close C_ITEM_SUPP_MANU_COUNTRY;
   ---
   if L_rec_exists is NOT NULL then
      O_exist := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_SUPP_MANU_CTRY_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MANU_COUNTRY_DELETE(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exist               IN OUT BOOLEAN,
                                   I_item                IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                   I_supplier            IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                   I_manu_country_id     IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
return BOOLEAN is
   ---
   L_program     VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.CHECK_MANU_COUNTRY_DELETE';
   L_dummy       VARCHAR2(1)  := NULL;
   ---

   cursor C_ITEM_HTS_EXISTS is
      select 'x'
        from item_hts
       where origin_country_id = I_manu_country_id
         and item = I_item
         and not exists (select 'x'
                           from item_supp_manu_country imc
                          where imc.item = I_item
                            and imc.supplier != I_supplier
                            and imc.manu_country_id = I_manu_country_id);
   ---
   cursor C_PACK_ITEM_EXISTS is
      select 'x'
        from packitem p,
             item_supp_manu_country imc
       where p.item              = I_item
         and p.pack_no           = imc.item
         and imc.supplier        = I_supplier
         and imc.manu_country_id = I_manu_country_id;
   ---

BEGIN
   O_exist := FALSE;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_manu_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_manu_country_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   -- check the ITEM_HTS table for item/supplier/origin country combos
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_HTS_EXISTS',
                    'ITEM_HTS',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   open  C_ITEM_HTS_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_HTS_EXISTS',
                    'ITEM_HTS',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   fetch C_ITEM_HTS_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_HTS_EXISTS',
                    'ITEM_HTS',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   close C_ITEM_HTS_EXISTS;
   --   
   if L_dummy is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_ON_ITEM_HTS', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---   
   -- Check if I_item is not a component of a pack that is currently associated with the
   -- Supplier/Origin Country being deleted
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_PACK_ITEM_EXISTS',
                    'PACKITEM, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   open  C_PACK_ITEM_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PACK_ITEM_EXISTS',
                    'PACKITEM, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   fetch C_PACK_ITEM_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PACK_ITEM_EXISTS',
                    'PACKITEM, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   close C_PACK_ITEM_EXISTS;
   --
   if L_dummy is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_EXISTS_ON_PACK', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_MANU_COUNTRY_DELETE;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_MANU_CTRY_FROM_CHILDREN(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                        I_supplier          IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                        I_manu_country_id   IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
return BOOLEAN is
   ---
   L_program      VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.DELETE_MANU_CTRY_FROM_CHILDREN';
   L_exists       BOOLEAN;
   L_child_item   ITEM_MASTER.ITEM%TYPE;
   L_table        VARCHAR2(30);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ITEM_SUPP_MANU_COUNTRY is
      select 'x'
        from item_master im,
             item_supp_manu_country imc
       where imc.supplier        = I_supplier
         and imc.manu_country_id = I_manu_country_id
         and imc.item            = im.item
         and (im.item_parent     = I_item or
             im.item_grandparent = I_item)
         and im.item_level      <= im.tran_level
         for update of imc.supplier nowait;
--------------------------------------------------------------------------------
-------------------------INTERNAL FUNCTIONS-------------------------------------

FUNCTION CHECK_COUNTRY_CHILD_DELETE(O_error_message   IN OUT VARCHAR2,
                                    O_exist           IN OUT BOOLEAN,
                                    I_item            IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier        IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                    I_manu_country_id IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
return BOOLEAN is
   ---
   L_program     VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.CHECK_COUNTRY_CHILD_DELETE';
   L_dummy       VARCHAR2(1);
   ---

  cursor C_ITEM_HTS_EXISTS is
      select 'x'
        from item_hts ih,
             item_master im
       where ih.item = im.item
         and ih.origin_country_id = I_manu_country_id
         and (im.item_parent     = I_item
          or im.item_grandparent = I_item)
         and im.item_level      <= im.tran_level
         and not exists (select 'x'
                           from item_supp_manu_country imc
                          where imc.item = im.item
                            and imc.supplier != I_supplier
                            and imc.manu_country_id = I_manu_country_id);
   ---
   cursor C_ITEM_SUPP_COUNTRY_EXISTS is
      select 'x'
        from item_supp_manu_country imc,
             item_master im
       where imc.supplier                = I_supplier
         and imc.manu_country_id         = I_manu_country_id
         and imc.primary_manu_ctry_ind   = 'Y'
         and (im.item_parent             = I_item or
            im.item_grandparent          = I_item)
         and item_level                 <= tran_level
         and imc.item                    = im.item;
   ---
   cursor C_PACK_ITEM_EXISTS is
      select 'x'
        from v_packsku_qty v,
             item_supp_manu_country imc
       where v.pack_no           = imc.item
         and imc.supplier        = I_supplier
         and imc.manu_country_id = I_manu_country_id
         and v.item in (select im.item
                          from item_master im,
                               item_supp_manu_country imc1
                         where imc1.supplier        = I_supplier
                           and imc1.manu_country_id = I_manu_country_id
                           and (im.item_parent      = I_item or
                                im.item_grandparent = I_item)
                           and item_level          <= tran_level
                           and imc1.item            = im.item);
   ---
BEGIN
   O_exist := FALSE;
   ---
   -- check the ITEM_HTS table for child item/supplier/manu country combos
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_HTS_EXISTS',
                    'ITEM_HTS',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   open  C_ITEM_HTS_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_HTS_EXISTS',
                    'ITEM_HTS',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   fetch C_ITEM_HTS_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_HTS_EXISTS',
                    'ITEM_HTS',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   close C_ITEM_HTS_EXISTS;
   --   
   if L_dummy is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_ON_ITEM_HTS_CHILD', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   -- Check if I_item is not a component of a pack that is currently associated with the
   -- Supplier/Origin Country being deleted
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_PACK_ITEM_EXISTS',
                    'PACKITEM, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   open  C_PACK_ITEM_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PACK_ITEM_EXISTS',
                    'PACKITEM, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   fetch C_PACK_ITEM_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PACK_ITEM_EXISTS',
                    'PACKITEM, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   close C_PACK_ITEM_EXISTS;
   --
   if L_dummy is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_EXISTS_ON_PACK', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   ---
   -- Check if I_item is not a component of a pack that is currently associated with the
   -- Supplier/Origin Country being deleted
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_SUPP_COUNTRY_EXISTS',
                    'ITEM_MASTER, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   open  C_ITEM_SUPP_COUNTRY_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_SUPP_COUNTRY_EXISTS',
                    'ITEM_MASTER, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   fetch C_ITEM_SUPP_COUNTRY_EXISTS into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_SUPP_COUNTRY_EXISTS',
                    'ITEM_MASTER, ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '|| I_manu_country_id);
   close C_ITEM_SUPP_COUNTRY_EXISTS;
   --
   if L_dummy is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_PRI_COUNTRY', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END CHECK_COUNTRY_CHILD_DELETE;
------------------------------------------------------------------------------------------
-------------------------BEGIN DELETE_COUNTRY_FROM_CHILDREN FUNCTION----------------------
------------------------------------------------------------------------------------------
BEGIN
   ---call the checking internal functions.
   if CHECK_COUNTRY_CHILD_DELETE(O_error_message,
                                 L_exists,
                                 I_item,
                                 I_supplier,
                                 I_manu_country_id) = FALSE or 
      L_exists = TRUE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_SUPP_MANU_COUNTRY',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' supplier: '||to_char(I_supplier)||' Country: '||I_manu_country_id);
   open C_LOCK_ITEM_SUPP_MANU_COUNTRY;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_SUPP_MANU_COUNTRY',
                    'ITEM_SUPP_MANU_COUNTRY',
                    'Item: '||I_item||' supplier: '||to_char(I_supplier)||' Country: '||I_manu_country_id);
   close C_LOCK_ITEM_SUPP_MANU_COUNTRY;
   --
   L_table := 'ITEM_SUPP_MANU_COUNTRY';
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPP_MANU_COUNTRY', NULL);
   delete from item_supp_manu_country
    where supplier = I_supplier
      and manu_country_id = I_manu_country_id
      and item in (select im.item
                     from item_master im,
                          item_supp_manu_country imc
                    where imc.supplier = I_supplier
                      and imc.manu_country_id = I_manu_country_id
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and item_level <= tran_level
                      and imc.item = im.item);
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_MANU_CTRY_FROM_CHILDREN;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_SUPP_MANU_CTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_table_locked      IN OUT VARCHAR2,
                                  I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                  I_supplier          IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                  I_manu_country_id   IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
return BOOLEAN is
   L_program      VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.LOCK_ITEM_SUPP_MANU_CTRY';
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ITEM_SUPP_MANU_CTRY is
      select 'x'
        from item_supp_manu_country
       where item = I_item
         and supplier = I_supplier
         and manu_country_id = I_manu_country_id
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_SUPP_MANU_CTRY',
                    'ITEM_SUPP_MANU_CTRY',
                    'Item: '||I_item||' supplier: '||to_char(I_supplier)||' Country: '||I_manu_country_id);
   open C_LOCK_ITEM_SUPP_MANU_CTRY;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_SUPP_MANU_CTRY',
                    'ITEM_SUPP_MANU_CTRY',
                    'Item: '||I_item||' supplier: '||to_char(I_supplier)||' Country: '||I_manu_country_id);
   close C_LOCK_ITEM_SUPP_MANU_CTRY;
   ---
   O_table_locked := 'LOCKED';

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_MANU_CTRY',
                                            I_item,
                                            to_char(I_supplier)||', '||I_manu_country_id);
      O_table_locked := 'LOCKED';
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(I_supplier)||', '||I_manu_country_id);
      return FALSE;

END LOCK_ITEM_SUPP_MANU_CTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_MANU_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists            IN OUT BOOLEAN,
                                  O_manu_country_id   IN OUT ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE,
                                  I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                  I_supplier          IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.GET_PRIMARY_MANU_COUNTRY';

   cursor C_GET_PRIMARY_SUPP is
      select manu_country_id
        from item_supp_manu_country
       where item = I_item
         and supplier = I_supplier
         and primary_manu_ctry_ind = 'Y';
   ---
   cursor C_GET_PRIMARY_NO_SUPP is
      select manu_country_id
        from item_supp_manu_country imc,
             item_supplier isp
       where isp.item = I_item
         and isp.primary_supp_ind = 'Y'
         and imc.item = isp.item
         and imc.supplier = isp.supplier
         and primary_manu_ctry_ind = 'Y';
BEGIN
   O_exists := TRUE;
   O_manu_country_id := NULL;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_PRIMARY_NO_SUPP',
                       'ITEM_SUPP_MANU_CTRY, ITEM_SUPPLIER',
                       'Item: '||I_item);
      open C_GET_PRIMARY_NO_SUPP;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_PRIMARY_NO_SUPP',
                       'ITEM_SUPP_MANU_CTRY, ITEM_SUPPLIER',
                       'Item: '||I_item);
      fetch C_GET_PRIMARY_NO_SUPP into O_manu_country_id;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PRIMARY_NO_SUPP',
                       'ITEM_SUPP_MANU_CTRY, ITEM_SUPPLIER',
                       'Item: '||I_item);
      close C_GET_PRIMARY_NO_SUPP;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_PRIMARY_SUPP',
                       'ITEM_SUPP_MANU_CTRY',
                       'Item: '||I_item||' supplier: '||to_char(I_supplier));
      open C_GET_PRIMARY_SUPP;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_PRIMARY_SUPP',
                       'ITEM_SUPP_MANU_CTRY',
                       'Item: '||I_item||' supplier: '||to_char(I_supplier));
      fetch C_GET_PRIMARY_SUPP into O_manu_country_id;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PRIMARY_SUPP',
                       'ITEM_SUPP_MANU_CTRY',
                       'Item: '||I_item||' supplier: '||to_char(I_supplier));
      close C_GET_PRIMARY_SUPP;
   end if;
   ---
   if O_manu_country_id is NULL then
      O_exists := FALSE;
   end if;
   ---

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            I_item);
      return FALSE;

END GET_PRIMARY_MANU_COUNTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_MANU_INDICATOR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                                       I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                       I_manu_country_id    IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(64)  := 'ITEM_SUPP_MANU_COUNTRY_SQL.UPDATE_PRIMARY_MANU_INDICATOR';
   L_table        VARCHAR2(30)  := 'ITEM_SUPP_MANU_COUNTRY';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_MANU_COUNTRY_Y is
      select 'x'
        from item_supp_manu_country imc, 
             item_master im
       where primary_manu_ctry_ind = 'Y'
         and manu_country_id    != I_manu_country_id
         and supplier            = I_supplier
         and imc.item            = im.item
         and (im.item_parent     = I_item 
          or im.item_grandparent = I_item)
         and item_level         <= tran_level
       for update of imc.supplier nowait;
   ---
   cursor C_LOCK_MANU_COUNTRY_N is
      select 'x'
        from item_supp_manu_country imc, 
             item_master im
       where primary_manu_ctry_ind = 'N'
         and manu_country_id     = I_manu_country_id
         and supplier            = I_supplier
         and imc.item            = im.item
         and (im.item_parent     = I_item 
          or im.item_grandparent = I_item)
         and item_level <= tran_level
       for update of imc.supplier nowait;
   ---
BEGIN

   ---
   -- Lock the item_supp_manu_country record, update the primary country ind to
   -- No for I_item with an Country of Manufacture other than the input country
   ---
   open C_LOCK_MANU_COUNTRY_Y;
   close C_LOCK_MANU_COUNTRY_Y;
   ---
   update item_supp_manu_country imc1
      set imc1.primary_manu_ctry_ind = 'N'
    where imc1.primary_manu_ctry_ind = 'Y'
      and imc1.manu_country_id != I_manu_country_id
      and imc1.supplier         = I_supplier
      and imc1.item in (select im.item
                          from item_supp_manu_country imc2,
                               item_master im
                         where imc2.supplier           = imc1.supplier
                           and imc2.manu_country_id    = I_manu_country_id
                           and (im.item_parent         = I_item or
                                im.item_grandparent    = I_item)
                           and im.item_level          <= im.tran_level
                           and imc2.item               = im.item );

   ---
   -- Lock the item_supp_manu_country record, update the primary country ind to
   -- Yes for I_item and the input country
   ---
   open C_LOCK_MANU_COUNTRY_N;
   close C_LOCK_MANU_COUNTRY_N;
   --
   update item_supp_manu_country imc1
      set imc1.primary_manu_ctry_ind = 'Y'
    where imc1.primary_manu_ctry_ind = 'N'
      and imc1.manu_country_id = I_manu_country_id
      and imc1.supplier        = I_supplier
      and imc1.item in (select im.item
                          from item_supp_manu_country imc2,
                               item_master im
                         where imc2.supplier        = imc1.supplier
                           and imc2.manu_country_id = imc1.manu_country_id
                           and (im.item_parent      = I_item or
                                im.item_grandparent = I_item)
                           and im.item_level       <= im.tran_level
                           and imc2.item            = im.item);

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;
END UPDATE_PRIMARY_MANU_INDICATOR;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MANU_CTRY_EXISTS_FOR_PK(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists               OUT BOOLEAN,
                                       O_supplier             OUT ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,                                     
                                       I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                       I_pk_item           IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.CHECK_MANU_CTRY_EXISTS_FOR_PK';
   L_exists      VARCHAR2(1) := NULL;
  
   
   cursor C_GET_SUPP_PK_ITEM is
      select distinct supplier
        from item_supp_manu_country
       where item = I_pk_item
       order by supplier desc ; 

   cursor C_MANU_EXISTS_FOR_PK(L_supplier item_supp_manu_country.SUPPLIER%TYPE) is
      select 'X'
        from item_supp_manu_country iscm
       where iscm.item = I_pk_item
         and iscm.supplier = L_supplier
         and exists (select 'x' 
                       from item_supp_manu_country iscm2
                      where iscm2.item = I_item
                        and iscm2.supplier = L_supplier
                        and iscm2.manu_country_id = iscm.manu_country_id);    
   

BEGIN
   O_exists := FALSE;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_pk_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_pk_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   for c_rec in C_GET_SUPP_PK_ITEM
   loop
   
      O_supplier := c_rec.supplier;
      
      SQL_LIB.SET_MARK('OPEN',
                       'C_MANU_EXISTS_FOR_PK',
                       'ITEM_SUPP_MANU_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(c_rec.supplier));

      open C_MANU_EXISTS_FOR_PK(c_rec.supplier);   

      SQL_LIB.SET_MARK('FETCH',
                       'C_MANU_EXISTS_FOR_PK',
                       'ITEM_SUPP_MANU_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(c_rec.supplier));
      fetch C_MANU_EXISTS_FOR_PK into L_exists;                       
                       

      if C_MANU_EXISTS_FOR_PK%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('COMP_ITEM_NO_ITEMMANUCTRY',
                                                I_item,
                                                c_rec.supplier,
                                                I_pk_item);         
         O_exists := FALSE;
         exit;
      else
         O_exists := TRUE;
      end if;    

      SQL_LIB.SET_MARK('CLOSE',
                       'C_MANU_EXISTS_FOR_PK',
                       'ITEM_SUPP_MANU_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(c_rec.supplier));
      close C_MANU_EXISTS_FOR_PK;
      
   end loop;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            I_item);
      return FALSE;

END CHECK_MANU_CTRY_EXISTS_FOR_PK;
-------------------------------------------------------------------------------------------------------
FUNCTION COUNT_MANU_CTRY_PRIM_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                                  O_mult_ind             OUT BOOLEAN,                                    
                                  I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE, 
                                  I_supplier          IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE) 
 RETURN BOOLEAN IS 
    L_program     VARCHAR2(64) := 'ITEM_SUPP_MANU_COUNTRY_SQL.COUNT_MANU_CTRY_PRIM_IND'; 
    L_count       NUMBER(5) := NULL; 
     
    cursor C_GET_PRIM_CTRY_COUNT is 
       select count(*)  
       from item_supp_manu_country  
        where item  = I_item  
        and supplier = I_supplier  
        and primary_manu_ctry_ind = 'Y'; 
   
 BEGIN 
   
   SQL_LIB.SET_MARK('OPEN', 
                    'C_GET_PRIM_CTRY_COUNT', 
                    'ITEM_SUPP_MANU_COUNTRY', 
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)); 
    open C_GET_PRIM_CTRY_COUNT; 
   
    SQL_LIB.SET_MARK('FETCH', 
                     'C_GET_PRIM_CTRY_COUNT', 
                     'ITEM_SUPP_MANU_COUNTRY', 
                     'Item: '||I_item||' Supplier: '||to_char(I_supplier)); 
    fetch C_GET_PRIM_CTRY_COUNT into L_count; 
  
    SQL_LIB.SET_MARK('CLOSE', 
                     'C_GET_PRIM_CTRY_COUNT', 
                     'ITEM_SUPP_MANU_COUNTRY', 
                     'Item: '||I_item||' Supplier: '||to_char(I_supplier)); 
    close C_GET_PRIM_CTRY_COUNT; 
   
    if NVL(L_count, 0) > 1 then 
       O_mult_ind := TRUE; 
    else 
       O_mult_ind := FALSE; 
    end if; 
   
    return TRUE; 
 EXCEPTION 
   when OTHERS then 
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             I_item); 
       return FALSE; 
   
 END COUNT_MANU_CTRY_PRIM_IND; 
------------------------------------------------------------------------------------------------------- 
END ITEM_SUPP_MANU_COUNTRY_SQL;
/