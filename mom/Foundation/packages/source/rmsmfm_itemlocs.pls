CREATE OR REPLACE PACKAGE RMSMFM_ITEMLOC AUTHID CURRENT_USER IS

FAMILY      VARCHAR2(64) := 'ItemLoc';

ITEMLOC_ADD     CONSTANT  VARCHAR2(20) := 'ItemLocCre';
ITEMLOC_UPD     CONSTANT  VARCHAR2(20) := 'ItemLocMod';
ITEMLOC_DEL     CONSTANT  VARCHAR2(20) := 'ItemLocDel';

REPL_UPD        CONSTANT  VARCHAR2(20) := 'ItemLocReplMod';

--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message         OUT  VARCHAR2,
                I_message_type          IN   ITEMLOC_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_itemloc_record        IN   ITEM_LOC%ROWTYPE,
                I_returnable_ind        IN   ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE,
                I_prim_repl_supplier    IN   REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                I_repl_method           IN   REPL_ITEM_LOC.REPL_METHOD%TYPE,
                I_reject_store_ord_ind  IN   REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE,
                I_next_delivery_date    IN   REPL_ITEM_LOC.NEXT_DELIVERY_DATE%TYPE,
                I_mult_runs_per_day_ind IN   REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN      NUMBER DEFAULT 1,
                 I_thread_val    IN      NUMBER DEFAULT 1);
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT);
--------------------------------------------------------------------------------
END;
/
