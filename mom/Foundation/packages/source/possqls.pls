CREATE OR REPLACE PACKAGE POS_CONFIG_SQL AUTHID CURRENT_USER IS
----------------------------------------------------------------------------
-- Procedure:  CHECK_ITEM
-- Purpose:    Verify that an item fits a valid merchandise hierarchy
--             as determined by the POS_MERCH_CRITERIA table.  If the
--             hierarchy is valid, write appropriate data to the
--             POS_CONFIG_ITEM table, and flag the POS_LOC_GRP table
--             status to 'Y' if the item is a coupon or product
--             restriction.
-- Author:     Retek (09/21/99)
----------------------------------------------------------------------------
FUNCTION CHECK_ITEM (O_error_message    IN OUT VARCHAR2,
                      I_item            IN     ITEM_MASTER.ITEM%TYPE,
                      I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                      I_class           IN     ITEM_MASTER.CLASS%TYPE,
                      I_subclass        IN     ITEM_MASTER.SUBCLASS%TYPE,
                      I_store           IN     STORE.STORE%TYPE,
                      I_action          IN     VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM (O_error_message IN OUT VARCHAR2,
                        I_group         IN     DEPS.GROUP_NO%TYPE,
                        I_dept          IN     ITEM_MASTER.DEPT%TYPE,
                        I_class         IN     ITEM_MASTER.CLASS%TYPE,
                        I_subclass      IN     ITEM_MASTER.SUBCLASS%TYPE,
                        I_item          IN     ITEM_MASTER.ITEM%TYPE,
                        O_exists        IN OUT VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION TOTAL_ITEMS (O_error_message     IN OUT VARCHAR2,
                      I_pos_config_id     IN     POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                      I_pos_config_type   IN     POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE,
                      O_total_items       IN OUT NUMBER)
   RETURN BOOLEAN;


----------------------------------------------------------------------------
FUNCTION POSMCRIT_FILTER_LIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_diff              IN OUT   VARCHAR2,
                              I_total_items       IN       NUMBER,
                              I_pos_config_id     IN       POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                              I_pos_config_type   IN       POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------- 
-- Name:    POS_STORE_FILTER_LIST
-- Purpose: This function will check if the list is a partial or full view.
--------------------------------------------------------------------------------
FUNCTION POS_STORE_FILTER_LIST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_diff              IN OUT   VARCHAR2,
                               I_count_pos_store   IN       NUMBER,
                               I_pos_config_type   IN       POS_STORE.POS_CONFIG_TYPE%TYPE,
                               I_pos_config_id     IN       POS_STORE.POS_CONFIG_ID%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Procedure:  CHECK_DUP_CRITERION
-- Purpose:    Returns O_exists = Y if the Merchandise Criterion already
--             exists, else O_exists = N. 
----------------------------------------------------------------------------
FUNCTION CHECK_DUP_CRITERION (O_error_message   IN OUT VARCHAR2,
                              I_pos_config_id   IN     POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                              I_pos_config_type IN     POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE,
                              I_group           IN     DEPS.GROUP_NO%TYPE,
                              I_dept            IN     ITEM_MASTER.DEPT%TYPE,
                              I_class           IN     ITEM_MASTER.CLASS%TYPE,
                              I_subclass        IN     ITEM_MASTER.SUBCLASS%TYPE,
                              I_item            IN     ITEM_MASTER.ITEM%TYPE,
                              I_exclude_ind     IN     POS_MERCH_CRITERIA.EXCLUDE_IND%TYPE,
                              O_exists          IN OUT VARCHAR2)
 RETURN BOOLEAN;
 
----------------------------------------------------------------------------
-- Procedure:  STORE_COUNT
-- Purpose:    To check whether any store exist with not null status
----------------------------------------------------------------------------
FUNCTION TOTAL_STORES (O_error_message   IN OUT VARCHAR2,
                       O_store_count     IN OUT NUMBER,
                       I_pos_config_id   IN     POS_STORE.POS_CONFIG_ID%TYPE,
                       I_pos_config_type IN     POS_STORE.POS_CONFIG_TYPE%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Procedure:  DELETE_MERCH_CRITERIA
-- Purpose:    Delete records from the pos_merch_criteria table.
--             Called by posmcrit.fmb.
----------------------------------------------------------------------------
FUNCTION DELETE_MERCH_CRITERIA(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_pos_config_type     IN      POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE,
                               I_pos_config_id       IN      POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                               I_seq_no              IN      POS_MERCH_CRITERIA.SEQ_NO%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Function:    DELETE_ITEM
-- Purpose :    Delete records from the respective POS tables based on the CONFIG_TYPE
----------------------------------------------------------------------------
FUNCTION DELETE_ITEM (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_pos_id                  IN       POS_COUPON_HEAD.COUPON_ID%TYPE,
                      I_pos_config_type         IN       POS_STORE.POS_CONFIG_TYPE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION DELETE_MERCH_CRITERIA_NEW(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_pos_config_type     IN      POS_MERCH_CRITERIA.POS_CONFIG_TYPE%TYPE,
                                   I_pos_config_id       IN      POS_MERCH_CRITERIA.POS_CONFIG_ID%TYPE,
                                   I_seq_no              IN      POS_MERCH_CRITERIA.SEQ_NO%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
FUNCTION UPDATE_MERCH_CRITERIA(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item           IN ITEM_MASTER.ITEM%TYPE,
                               I_dept           IN ITEM_MASTER.DEPT%TYPE,
                               I_class          IN ITEM_MASTER.CLASS%TYPE,
                               I_subclass       IN ITEM_MASTER.SUBCLASS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_POS_COUPON_HEAD_TL_TL
-- Purpose:       This function delete records from the pos_coupon_head_tl for given coupon_id table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_POS_COUPON_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_coupon_id           IN   POS_COUPON_HEAD_TL.COUPON_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END POS_CONFIG_SQL;
/
