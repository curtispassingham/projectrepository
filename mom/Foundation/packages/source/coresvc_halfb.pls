create or replace PACKAGE BODY CORESVC_HALF AS
   cursor C_SVC_HALF_TL(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_half_tl.rowid  AS pk_half_tl_rid,
             st.rowid AS st_rid,
             hlft_hlf_fk.rowid    AS hlft_hlf_fk_rid,
             st.half_name,
             st.half_no,
             st.lang,
             cd_lang.rowid as cd_lang_rid,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_half_tl st,
             half_tl pk_half_tl,
             half hlft_hlf_fk,
			 code_detail cd_lang,
             dual
       where st.process_id                = I_process_id
         and st.chunk_id                  = I_chunk_id
         and st.lang                      = pk_half_tl.lang (+)
         and st.half_no                   = pk_half_tl.half_no (+)
         and st.half_no                   = hlft_hlf_fk.half_no (+)
         and st.lang                      = cd_lang.code (+)
         and cd_lang.code_type (+)        = 'LANG';

   cursor C_SVC_HALF(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_half.rowid  AS pk_half_rid,
             st.rowid AS st_rid,
             st.half_date,
             st.half_name,
             st.half_no,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_half st,
             half pk_half,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.half_no         = pk_half.half_no (+)
;
   cursor C_SVC_CALENDAR(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             uk_calendar.rowid  AS uk_calendar_rid,
             pk_calendar.rowid  AS pk_calendar_rid,
             st.rowid AS st_rid,
             st.no_of_weeks,
             st.month_454,
             st.year_454,
             st.first_day,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_calendar st,
             calendar uk_calendar,
             calendar pk_calendar,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.first_day        = uk_calendar.first_day (+)
         and st.year_454         = pk_calendar.year_454 (+)
         and st.month_454        = pk_calendar.month_454 (+)
;
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   LP_primary_lang    LANG.LANG%TYPE;

----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E')IS

BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
---------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   HALF_TL_cols s9t_pkg.names_map_typ;
   HALF_cols s9t_pkg.names_map_typ;
   CALENDAR_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                          :=s9t_pkg.get_sheet_names(I_file_id);
   HALF_TL_cols                      :=s9t_pkg.get_col_names(I_file_id,HALF_TL_sheet);

   HALF_TL$Action                    := HALF_TL_cols('ACTION');

   HALF_TL$HALF_NAME                 := HALF_TL_cols('HALF_NAME');
   HALF_TL$HALF_NO                   := HALF_TL_cols('HALF_NO');
   HALF_TL$LANG                      := HALF_TL_cols('LANG');
   HALF_cols                         :=s9t_pkg.get_col_names(I_file_id,HALF_sheet);
   HALF$Action                       := HALF_cols('ACTION');
   HALF$HALF_DATE                    := HALF_cols('HALF_DATE');
   HALF$HALF_NAME                    := HALF_cols('HALF_NAME');
   HALF$HALF_NO                      := HALF_cols('HALF_NO');
   CALENDAR_cols                     :=s9t_pkg.get_col_names(I_file_id,CALENDAR_sheet);
   CALENDAR$Action                   := CALENDAR_cols('ACTION');
   CALENDAR$NO_OF_WEEKS              := CALENDAR_cols('NO_OF_WEEKS');
   CALENDAR$MONTH_454                := CALENDAR_cols('MONTH_454');
   CALENDAR$YEAR_454                 := CALENDAR_cols('YEAR_454');
   CALENDAR$FIRST_DAY                := CALENDAR_cols('FIRST_DAY');
END POPULATE_NAMES;
------------------------------------------------------------------
PROCEDURE POPULATE_HALF_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = HALF_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_HALF.action_mod ,half_no,lang,
                           half_name
                           ))
     from half_tl ;
END POPULATE_HALF_TL;
-------------------------------------------------------------------
PROCEDURE POPULATE_HALF( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = HALF_sheet )
   select s9t_row(s9t_cells(CORESVC_HALF.action_mod ,half_no,half_name,
                           half_date
                           
                           ))
     from half ;
END POPULATE_HALF;
--------------------------------------------------------------------
PROCEDURE POPULATE_CALENDAR( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = CALENDAR_sheet )
   select s9t_row(s9t_cells(CORESVC_HALF.action_mod ,first_day,year_454,month_454,
                           no_of_weeks
                           ))
     from calendar ;
END POPULATE_CALENDAR;
-------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(HALF_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(HALF_TL_sheet)).column_headers := s9t_cells( 'ACTION','HALF_NO'
                                                                                            ,'LANG'
                                                                                            ,'HALF_NAME'
                                                                                            );
   L_file.add_sheet(HALF_sheet);
   L_file.sheets(l_file.get_sheet_index(HALF_sheet)).column_headers := s9t_cells( 'ACTION','HALF_NO','HALF_NAME'
                                                                                            ,'HALF_DATE'
                                                                                            );
   L_file.add_sheet(CALENDAR_sheet);
   L_file.sheets(l_file.get_sheet_index(CALENDAR_sheet)).column_headers := s9t_cells( 'ACTION','FIRST_DAY','YEAR_454','MONTH_454'
                                                                                            ,'NO_OF_WEEKS'
                                                                                            
);
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
-----------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_HALF.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;   
   if I_template_only_ind = 'N' then
      POPULATE_HALF_TL(O_file_id);
      POPULATE_HALF(O_file_id);
      POPULATE_CALENDAR(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HALF_TL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_HALF_TL.process_id%TYPE) IS
   TYPE svc_HALF_TL_col_typ IS TABLE OF SVC_HALF_TL%ROWTYPE;
   L_temp_rec SVC_HALF_TL%ROWTYPE;
   svc_HALF_TL_col svc_HALF_TL_col_typ :=NEW svc_HALF_TL_col_typ();
   L_process_id SVC_HALF_TL.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_HALF_TL%ROWTYPE;
   O_error_message varchar2(500);
   cursor C_MANDATORY_IND is
      select
             HALF_NAME_mi,
             HALF_NO_mi,
             LANG_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'HALF_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'HALF_NAME' AS HALF_NAME,
                                         'HALF_NO' AS HALF_NO,
                                         'LANG' AS LANG,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns    VARCHAR2(255)  := 'Half No, Lang';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select
                       HALF_NAME_dv,
                       HALF_NO_dv,
                       LANG_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'HALF_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'HALF_NAME' AS HALF_NAME,
                                                      'HALF_NO' AS HALF_NO,
                                                      'LANG' AS LANG,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.HALF_NAME := rec.HALF_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HALF_TL ' ,
                            NULL,
                           'HALF_NAME ' ,
                           NULL,
                            'INV_DEFAULT');

      END;
      BEGIN
         L_default_rec.HALF_NO := rec.HALF_NO_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HALF_TL ' ,
                            NULL,
                           'HALF_NO ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HALF_TL ' ,
                            NULL,
                           'LANG ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(HALF_TL$Action)      AS Action,
          r.get_cell(HALF_TL$HALF_NAME)              AS HALF_NAME,
          r.get_cell(HALF_TL$HALF_NO)              AS HALF_NO,
          r.get_cell(HALF_TL$LANG)              AS LANG,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(HALF_TL_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
HALF_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.HALF_NAME := rec.HALF_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
HALF_TL_sheet,
                            rec.row_seq,
                            'HALF_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.HALF_NO := rec.HALF_NO;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
HALF_TL_sheet,
                            rec.row_seq,
                            'HALF_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
HALF_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HALF.action_new then
         L_temp_rec.HALF_NAME := NVL( L_temp_rec.HALF_NAME,L_default_rec.HALF_NAME);
         L_temp_rec.HALF_NO := NVL( L_temp_rec.HALF_NO,L_default_rec.HALF_NO);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,L_default_rec.LANG);
      end if;
      if not (
            L_temp_rec.LANG is NOT NULL and
            L_temp_rec.HALF_NO is NOT NULL and
            1 = 1
            )then

			WRITE_S9T_ERROR(I_file_id,
                            HALF_TL_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
            L_error := TRUE;
      end if;
      if NOT L_error then
         svc_HALF_TL_col.extend();
         svc_HALF_TL_col(svc_HALF_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_HALF_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_HALF_TL st
      using(select
                  (case
                   when l_mi_rec.HALF_NAME_mi    = 'N'
                    and svc_HALF_TL_col(i).action = CORESVC_HALF.action_mod
                    and s1.HALF_NAME IS NULL
                   then mt.HALF_NAME
                   else s1.HALF_NAME
                   end) AS HALF_NAME,
                  (case
                   when l_mi_rec.HALF_NO_mi    = 'N'
                    and svc_HALF_TL_col(i).action = CORESVC_HALF.action_mod
                    and s1.HALF_NO IS NULL
                   then mt.HALF_NO
                   else s1.HALF_NO
                   end) AS HALF_NO,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_HALF_TL_col(i).action = CORESVC_HALF.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  null as dummy
              from (select
                          svc_HALF_TL_col(i).HALF_NAME AS HALF_NAME,
                          svc_HALF_TL_col(i).HALF_NO AS HALF_NO,
                          svc_HALF_TL_col(i).LANG AS LANG,
                          null as dummy
                      from dual ) s1,
            HALF_TL mt
             where
                  mt.LANG (+)     = s1.LANG   and
                  mt.HALF_NO (+)     = s1.HALF_NO   and
                  1 = 1 )sq
                on (
                    st.LANG      = sq.LANG and
                    st.HALF_NO      = sq.HALF_NO and
                    svc_HALF_TL_col(i).ACTION IN (CORESVC_HALF.action_mod,CORESVC_HALF.action_del))
      when matched then
      update
         set process_id      = svc_HALF_TL_col(i).process_id ,
             chunk_id        = svc_HALF_TL_col(i).chunk_id ,
             row_seq         = svc_HALF_TL_col(i).row_seq ,
             action          = svc_HALF_TL_col(i).action ,
             process$status  = svc_HALF_TL_col(i).process$status ,
             half_name              = sq.half_name ,
             create_id       = svc_HALF_TL_col(i).create_id ,
             create_datetime = svc_HALF_TL_col(i).create_datetime ,
             last_upd_id     = svc_HALF_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_HALF_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             half_name ,
             half_no ,
             lang ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_HALF_TL_col(i).process_id ,
             svc_HALF_TL_col(i).chunk_id ,
             svc_HALF_TL_col(i).row_seq ,
             svc_HALF_TL_col(i).action ,
             svc_HALF_TL_col(i).process$status ,
             sq.half_name ,
             sq.half_no ,
             sq.lang ,
             svc_HALF_TL_col(i).create_id ,
             svc_HALF_TL_col(i).create_datetime ,
             svc_HALF_TL_col(i).last_upd_id ,
             svc_HALF_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            HALF_TL_sheet,
                            svc_HALF_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
             
         when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              null,
                                              TO_CHAR(SQLCODE));
   END;
     
   
END PROCESS_S9T_HALF_TL;
----------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HALF( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_HALF.process_id%TYPE) IS
   TYPE svc_HALF_col_typ IS TABLE OF SVC_HALF%ROWTYPE;
   L_temp_rec SVC_HALF%ROWTYPE;
   svc_HALF_col svc_HALF_col_typ :=NEW svc_HALF_col_typ();
   L_process_id SVC_HALF.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_HALF%ROWTYPE;
   O_error_message varchar2(500);
   cursor C_MANDATORY_IND is
      select
             HALF_DATE_mi,
             HALF_NAME_mi,
             HALF_NO_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'HALF'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'HALF_DATE' AS HALF_DATE,
                                         'HALF_NAME' AS HALF_NAME,
                                         'HALF_NO' AS HALF_NO,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns    VARCHAR2(255)  := 'Half No';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select
                       HALF_DATE_dv,
                       HALF_NAME_dv,
                       HALF_NO_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'HALF'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'HALF_DATE' AS HALF_DATE,
                                                      'HALF_NAME' AS HALF_NAME,
                                                      'HALF_NO' AS HALF_NO,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.HALF_DATE := rec.HALF_DATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HALF ' ,
                            NULL,
                           'HALF_DATE ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.HALF_NAME := rec.HALF_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HALF ' ,
                            NULL,
                           'HALF_NAME ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.HALF_NO := rec.HALF_NO_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'HALF ' ,
                            NULL,
                           'HALF_NO ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(HALF$Action)      AS Action,
          r.get_cell(HALF$HALF_DATE)              AS HALF_DATE,
          r.get_cell(HALF$HALF_NAME)              AS HALF_NAME,
          r.get_cell(HALF$HALF_NO)              AS HALF_NO,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(HALF_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
HALF_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.HALF_DATE := rec.HALF_DATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
HALF_sheet,
                            rec.row_seq,
                            'HALF_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.HALF_NAME := rec.HALF_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
HALF_sheet,
                            rec.row_seq,
                            'HALF_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.HALF_NO := rec.HALF_NO;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
HALF_sheet,
                            rec.row_seq,
                            'HALF_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HALF.action_new then
         L_temp_rec.HALF_DATE := NVL( L_temp_rec.HALF_DATE,L_default_rec.HALF_DATE);
         L_temp_rec.HALF_NAME := NVL( L_temp_rec.HALF_NAME,L_default_rec.HALF_NAME);
         L_temp_rec.HALF_NO := NVL( L_temp_rec.HALF_NO,L_default_rec.HALF_NO);
      end if;
      if not (
            L_temp_rec.HALF_NO is NOT NULL and
            1 = 1
            )then
			WRITE_S9T_ERROR(I_file_id,
                            HALF_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
            L_error := TRUE;
      end if;
      if NOT L_error then
         svc_HALF_col.extend();
         svc_HALF_col(svc_HALF_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_HALF_col.COUNT SAVE EXCEPTIONS
      merge into SVC_HALF st
      using(select
                  (case
                   when l_mi_rec.HALF_DATE_mi    = 'N'
                    and svc_HALF_col(i).action = CORESVC_HALF.action_mod
                    and s1.HALF_DATE IS NULL
                   then mt.HALF_DATE
                   else s1.HALF_DATE
                   end) AS HALF_DATE,
                  (case
                   when l_mi_rec.HALF_NAME_mi    = 'N'
                    and svc_HALF_col(i).action = CORESVC_HALF.action_mod
                    and s1.HALF_NAME IS NULL
                   then mt.HALF_NAME
                   else s1.HALF_NAME
                   end) AS HALF_NAME,
                  (case
                   when l_mi_rec.HALF_NO_mi    = 'N'
                    and svc_HALF_col(i).action = CORESVC_HALF.action_mod
                    and s1.HALF_NO IS NULL
                   then mt.HALF_NO
                   else s1.HALF_NO
                   end) AS HALF_NO,
                  null as dummy
              from (select
                          svc_HALF_col(i).HALF_DATE AS HALF_DATE,
                          svc_HALF_col(i).HALF_NAME AS HALF_NAME,
                          svc_HALF_col(i).HALF_NO AS HALF_NO,
                          null as dummy
                      from dual ) s1,
            HALF mt
             where
                  mt.HALF_NO (+)     = s1.HALF_NO   and
                  1 = 1 )sq
                on (
                    st.HALF_NO      = sq.HALF_NO and
                    svc_HALF_col(i).ACTION IN (CORESVC_HALF.action_mod,CORESVC_HALF.action_del))
      when matched then
      update
         set process_id      = svc_HALF_col(i).process_id ,
             chunk_id        = svc_HALF_col(i).chunk_id ,
             row_seq         = svc_HALF_col(i).row_seq ,
             action          = svc_HALF_col(i).action ,
             process$status  = svc_HALF_col(i).process$status ,
             half_name              = sq.half_name ,
             half_date              = sq.half_date ,
             create_id       = svc_HALF_col(i).create_id ,
             create_datetime = svc_HALF_col(i).create_datetime ,
             last_upd_id     = svc_HALF_col(i).last_upd_id ,
             last_upd_datetime = svc_HALF_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             half_date ,
             half_name ,
             half_no ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_HALF_col(i).process_id ,
             svc_HALF_col(i).chunk_id ,
             svc_HALF_col(i).row_seq ,
             svc_HALF_col(i).action ,
             svc_HALF_col(i).process$status ,
             sq.half_date ,
             sq.half_name ,
             sq.half_no ,
             svc_HALF_col(i).create_id ,
             svc_HALF_col(i).create_datetime ,
             svc_HALF_col(i).last_upd_id ,
             svc_HALF_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            HALF_sheet,
                            svc_HALF_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              null,
                                              TO_CHAR(SQLCODE));
 
   END;
END PROCESS_S9T_HALF;
--------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CALENDAR( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_CALENDAR.process_id%TYPE) IS
   TYPE svc_CALENDAR_col_typ IS TABLE OF SVC_CALENDAR%ROWTYPE;
   L_temp_rec SVC_CALENDAR%ROWTYPE;
   svc_CALENDAR_col svc_CALENDAR_col_typ :=NEW svc_CALENDAR_col_typ();
   L_process_id SVC_CALENDAR.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_CALENDAR%ROWTYPE;
   O_error_message varchar2(500);
      L_pk_columns    VARCHAR2(255)  := '4-5-4 Month, Year 4-5-4';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             NO_OF_WEEKS_mi,
             MONTH_454_mi,
             YEAR_454_mi,
             FIRST_DAY_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'CALENDAR'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'NO_OF_WEEKS' AS NO_OF_WEEKS,
                                         'MONTH_454' AS MONTH_454,
                                         'YEAR_454' AS YEAR_454,
                                         'FIRST_DAY' AS FIRST_DAY,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       NO_OF_WEEKS_dv,
                       MONTH_454_dv,
                       YEAR_454_dv,
                       FIRST_DAY_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'CALENDAR'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'NO_OF_WEEKS' AS NO_OF_WEEKS,
                                                      'MONTH_454' AS MONTH_454,
                                                      'YEAR_454' AS YEAR_454,
                                                      'FIRST_DAY' AS FIRST_DAY,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.NO_OF_WEEKS := rec.NO_OF_WEEKS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CALENDAR ' ,
                            NULL,
                           'NO_OF_WEEKS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.MONTH_454 := rec.MONTH_454_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CALENDAR ' ,
                            NULL,
                           'MONTH_454 ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.YEAR_454 := rec.YEAR_454_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CALENDAR ' ,
                            NULL,
                           'YEAR_454 ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FIRST_DAY := rec.FIRST_DAY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'CALENDAR ' ,
                            NULL,
                           'FIRST_DAY ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(CALENDAR$Action)      AS Action,
          r.get_cell(CALENDAR$NO_OF_WEEKS)              AS NO_OF_WEEKS,
          r.get_cell(CALENDAR$MONTH_454)              AS MONTH_454,
          r.get_cell(CALENDAR$YEAR_454)              AS YEAR_454,
          r.get_cell(CALENDAR$FIRST_DAY)              AS FIRST_DAY,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(CALENDAR_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
           if c_mandatory_ind%ISOPEN then
              close c_mandatory_ind;
           end if;

            WRITE_S9T_ERROR(I_file_id,
                            CALENDAR_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.NO_OF_WEEKS := rec.NO_OF_WEEKS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CALENDAR_sheet,
                            rec.row_seq,
                            'NO_OF_WEEKS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MONTH_454 := rec.MONTH_454;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CALENDAR_sheet,
                            rec.row_seq,
                            'MONTH_454',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.YEAR_454 := rec.YEAR_454;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CALENDAR_sheet,
                            rec.row_seq,
                            'YEAR_454',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FIRST_DAY := rec.FIRST_DAY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
CALENDAR_sheet,
                            rec.row_seq,
                            'FIRST_DAY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HALF.action_new then
         L_temp_rec.NO_OF_WEEKS := NVL( L_temp_rec.NO_OF_WEEKS,L_default_rec.NO_OF_WEEKS);
         L_temp_rec.MONTH_454 := NVL( L_temp_rec.MONTH_454,L_default_rec.MONTH_454);
         L_temp_rec.YEAR_454 := NVL( L_temp_rec.YEAR_454,L_default_rec.YEAR_454);
         L_temp_rec.FIRST_DAY := NVL( L_temp_rec.FIRST_DAY,L_default_rec.FIRST_DAY);
      end if;
      if not (
            L_temp_rec.YEAR_454 is NOT NULL and
            L_temp_rec.MONTH_454 is NOT NULL and
            1 = 1
            )then
			WRITE_S9T_ERROR(I_file_id,
                            HALF_sheet,
                            rec.row_seq,
                            NULL,
                            NULL,
                            SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
            L_error := TRUE;
      end if;
      if NOT L_error then
         svc_CALENDAR_col.extend();
         svc_CALENDAR_col(svc_CALENDAR_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_CALENDAR_col.COUNT SAVE EXCEPTIONS
      merge into SVC_CALENDAR st
      using(select
                  (case
                   when l_mi_rec.NO_OF_WEEKS_mi    = 'N'
                    and svc_CALENDAR_col(i).action = CORESVC_HALF.action_mod
                    and s1.NO_OF_WEEKS IS NULL
                   then mt.NO_OF_WEEKS
                   else s1.NO_OF_WEEKS
                   end) AS NO_OF_WEEKS,
                  (case
                   when l_mi_rec.MONTH_454_mi    = 'N'
                    and svc_CALENDAR_col(i).action = CORESVC_HALF.action_mod
                    and s1.MONTH_454 IS NULL
                   then mt.MONTH_454
                   else s1.MONTH_454
                   end) AS MONTH_454,
                  (case
                   when l_mi_rec.YEAR_454_mi    = 'N'
                    and svc_CALENDAR_col(i).action = CORESVC_HALF.action_mod
                    and s1.YEAR_454 IS NULL
                   then mt.YEAR_454
                   else s1.YEAR_454
                   end) AS YEAR_454,
                  (case
                   when l_mi_rec.FIRST_DAY_mi    = 'N'
                    and svc_CALENDAR_col(i).action = CORESVC_HALF.action_mod
                    and s1.FIRST_DAY IS NULL
                   then mt.FIRST_DAY
                   else s1.FIRST_DAY
                   end) AS FIRST_DAY,
                  null as dummy
              from (select
                          svc_CALENDAR_col(i).NO_OF_WEEKS AS NO_OF_WEEKS,
                          svc_CALENDAR_col(i).MONTH_454 AS MONTH_454,
                          svc_CALENDAR_col(i).YEAR_454 AS YEAR_454,
                          svc_CALENDAR_col(i).FIRST_DAY AS FIRST_DAY,
                          null as dummy
                      from dual ) s1,
            CALENDAR mt
             where
                  mt.FIRST_DAY (+)     = s1.FIRST_DAY   and
                  mt.YEAR_454 (+)     = s1.YEAR_454   and
                  mt.MONTH_454 (+)     = s1.MONTH_454   and
                  1 = 1 )sq
                on (
                    st.YEAR_454      = sq.YEAR_454 and
                    st.MONTH_454      = sq.MONTH_454 and
                    svc_CALENDAR_col(i).ACTION IN (CORESVC_HALF.action_mod,CORESVC_HALF.action_del))
      when matched then
      update
         set process_id      = svc_CALENDAR_col(i).process_id ,
             chunk_id        = svc_CALENDAR_col(i).chunk_id ,
             row_seq         = svc_CALENDAR_col(i).row_seq ,
             action          = svc_CALENDAR_col(i).action ,
             process$status  = svc_CALENDAR_col(i).process$status ,
             no_of_weeks              = sq.no_of_weeks ,
             first_day              = sq.first_day ,
             create_id       = svc_CALENDAR_col(i).create_id ,
             create_datetime = svc_CALENDAR_col(i).create_datetime ,
             last_upd_id     = svc_CALENDAR_col(i).last_upd_id ,
             last_upd_datetime = svc_CALENDAR_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             no_of_weeks ,
             month_454 ,
             year_454 ,
             first_day ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_CALENDAR_col(i).process_id ,
             svc_CALENDAR_col(i).chunk_id ,
             svc_CALENDAR_col(i).row_seq ,
             svc_CALENDAR_col(i).action ,
             svc_CALENDAR_col(i).process$status ,
             sq.no_of_weeks ,
             sq.month_454 ,
             sq.year_454 ,
             sq.first_day ,
             svc_CALENDAR_col(i).create_id ,
             svc_CALENDAR_col(i).create_datetime ,
             svc_CALENDAR_col(i).last_upd_id ,
             svc_CALENDAR_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            CALENDAR_sheet,
                            svc_CALENDAR_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              null,
                                              TO_CHAR(SQLCODE));
 
   END;
END PROCESS_S9T_CALENDAR;
---------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER
                      )
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_HALF.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else

      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_HALF_TL(I_file_id,I_process_id);
      
      PROCESS_S9T_HALF(I_file_id,I_process_id);
      PROCESS_S9T_CALENDAR(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
---------------------------------------------------------------
FUNCTION EXEC_HALF_TL_INS(  L_half_tl_temp_rec   IN   HALF_TL%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_HALF_TL_INS';
   L_table   VARCHAR2(255):= 'SVC_HALF_TL';
BEGIN
   insert
     into half_tl
   values L_half_tl_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HALF_TL_INS;
---------------------------------------------------------------
FUNCTION EXEC_HALF_TL_UPD( L_half_tl_temp_rec   IN   HALF_TL%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_HALF_TL_UPD';
   L_table   VARCHAR2(255):= 'SVC_HALF_TL';
BEGIN
   update half_tl
      set row = L_half_tl_temp_rec
    where 1 = 1
      and lang = L_half_tl_temp_rec.lang
      and half_no = L_half_tl_temp_rec.half_no
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HALF_TL_UPD;
----------------------------------------------------------------------
FUNCTION EXEC_HALF_TL_DEL(  L_half_tl_temp_rec   IN   HALF_TL%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_HALF_TL_DEL';
   L_table   VARCHAR2(255):= 'SVC_HALF_TL';
BEGIN
   delete
     from half_tl
    where 1 = 1
      and lang = L_half_tl_temp_rec.lang
      and half_no = L_half_tl_temp_rec.half_no
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HALF_TL_DEL;
----------------------------------------------------------------------
FUNCTION PROCESS_HALF_TL( I_process_id   IN   SVC_HALF_TL.PROCESS_ID%TYPE,
                          I_chunk_id     IN SVC_HALF_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_HALF.PROCESS_HALF_TL';
   L_error_message VARCHAR2(600);
   L_HALF_TL_temp_rec HALF_TL%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_HALF_TL';
BEGIN
   FOR rec IN c_svc_HALF_TL(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.lang = LP_primary_lang then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                     'ERR_PRIM_LANG',
                     'W');
        L_error:=true;      
      end if;
      
      if rec.action = action_new
         and rec.PK_HALF_TL_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'HALF_TL',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HALF_NO,LANG',
                    L_error_message);
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_HALF_TL_rid is NULL  
         and rec.lang is NOT NULL
         and rec.half_no is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HALF_NO,LANG',
                    'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;
      if rec.action in (action_new)
	     and rec.lang is NOT NULL
         and rec.cd_lang_rid is NULL then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                    'LANG_EXIST');
         L_error :=TRUE;
      end if;
      if rec.half_no is not null
         and rec.hlft_hlf_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HALF_NO',
                    'INV_HALF');
         L_error :=TRUE;
      end if;
    if rec.action in (action_new,action_mod)
       and NOT(  rec.HALF_NAME  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HALF_NAME',
                    'HALF_DESC_REQ');
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_half_tl_temp_rec.lang                         := rec.lang;
         L_half_tl_temp_rec.half_no                      := rec.half_no;
         L_half_tl_temp_rec.half_name                    := rec.half_name;
         L_half_tl_temp_rec.create_id                    := GET_USER;
         L_half_tl_temp_rec.last_update_id               := GET_USER;
         L_half_tl_temp_rec.create_datetime              := SYSDATE;
         L_half_tl_temp_rec.last_update_datetime         := SYSDATE;
         if rec.action = action_new then
            if EXEC_HALF_TL_INS(   L_half_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_HALF_TL_UPD( L_half_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_HALF_TL_DEL( L_half_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_half_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_half_tl st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_half_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_HALF_TL%ISOPEN then
         close C_SVC_HALF_TL;
      end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HALF_TL;
----------------------------------------------------------------------
FUNCTION EXEC_HALF_INS(  L_half_temp_rec   IN   HALF%ROWTYPE,
                         O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_HALF_INS';
   L_table   VARCHAR2(255):= 'SVC_HALF';
BEGIN
   insert
     into half
   values L_half_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HALF_INS;
-------------------------------------------------------------------------
FUNCTION EXEC_HALF_UPD( L_half_temp_rec   IN   HALF%ROWTYPE,
                        O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_HALF_UPD';
   L_table   VARCHAR2(255):= 'SVC_HALF';
BEGIN
   update half
      set row = L_half_temp_rec
    where 1 = 1
      and half_no = L_half_temp_rec.half_no
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HALF_UPD;
----------------------------------------------------------------------
FUNCTION EXEC_HALF_DEL(  L_half_temp_rec   IN       HALF%ROWTYPE ,
                         O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_HALF_DEL';
   L_table   VARCHAR2(255):= 'SVC_HALF';

   cursor C_HALF_LOCK is
      select 'X'
        from half
       where half_no = L_half_temp_rec.half_no
         for update nowait;

   cursor C_HALF_TL_LOCK is
      select 'X'
        from half_tl
       where half_no = L_half_temp_rec.half_no
         for update nowait;  
BEGIN
   open C_HALF_LOCK;
   close C_HALF_LOCK;
   
   open C_HALF_TL_LOCK;
   close C_HALF_TL_LOCK;
   
   delete
     from half_tl
    where 1 = 1
      and half_no = L_half_temp_rec.half_no
;
   delete
     from half
    where 1 = 1
      and half_no = L_half_temp_rec.half_no
;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_HALF_LOCK%ISOPEN then
         close C_HALF_LOCK;
      end if;
      if C_HALF_TL_LOCK%ISOPEN then
         close C_HALF_TL_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HALF_DEL;
-----------------------------------------------------------------
FUNCTION PROCESS_HALF( I_process_id   IN   SVC_HALF.PROCESS_ID%TYPE,
                       I_chunk_id     IN SVC_HALF.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_HALF.PROCESS_HALF';
   L_error_message VARCHAR2(600);
   L_HALF_temp_rec HALF%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_HALF';
   L_Year  calendar.year_454%type;
   L_half_exists   VARCHAR2(3):='No';

   cursor C_MONTH_HALF_EXISTS(Ihalf_no number) is
      select 'yes'
      from month_data
      where half_no = Ihalf_no;

   cursor C_PERIOD_HALF_EXISTS(Ihalf_no number) is
      select 'yes'
      from period
      where half_no = Ihalf_no;

   cursor C_SUP_HALF_EXISTS(Ihalf_no number) is
      select 'yes'
      from sup_month
      where half_no = Ihalf_no;

   cursor C_SYSTEM_HALF_EXISTS(Ihalf_no number) is
      select 'yes'
      from system_variables
      where last_eom_half_no = Ihalf_no;

   cursor C_HALF_DATA_BUDGET_EXISTS(Ihalf_no number) is
      select 'yes'
      from half_data_budget
      where half_no = Ihalf_no; 
     
   cursor C_HALF_DATA_EXISTS(Ihalf_no number) is
      select 'yes'
      from half_data
      where half_no = Ihalf_no;

   cursor C_YEAR(half_no SVC_HALF.HALF_NO%TYPE) is
      select year_454
	      from calendar
       where year_454 = trunc(half_no/10);

BEGIN
   FOR rec IN c_svc_HALF(I_process_id,
                         I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      
      if rec.action = action_new
         and rec.PK_HALF_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HALF_NO',
                     'HALF_NUM_ALREADY_EXIST');
         L_error :=TRUE;
      end if;
      
      if rec.action IN (action_mod,action_del)
         and rec.PK_HALF_rid is NULL then
 
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HALF_NO',
                    'INV_HALF');
         L_error :=TRUE;
      end if;
      
      if rec.action in (action_new) then
         open  C_YEAR(rec.half_no);
         fetch C_YEAR into L_year;
         
         if C_YEAR%NOTFOUND then
            close C_YEAR;
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'HALF_NO',
                        'INV_HALF');
            L_error :=TRUE;
         else
            close C_YEAR;
            L_year := MOD(rec.half_no,10);
            if L_year != 1 and L_year !=2 then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'HALF_NO',
                           'INV_HALF');
              L_error :=TRUE;
            end if;
         end if;
        
     end if;    
    
    if (rec.action in (action_new,action_mod)) then
       if NOT(  rec.HALF_NAME  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HALF_NAME',
                     'HALF_DESC_REQ');
         L_error :=TRUE;
        end if;
      end if;
     if (rec.action in (action_new,action_mod)) then
        if NOT(  rec.HALF_DATE  IS NOT NULL ) then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'HALF_DATE',
                       'HALF_DATE_REQ');
           L_error :=TRUE;
        end if;
      end if;

      if rec.action in (action_del) then
         open C_MONTH_HALF_EXISTS(rec.half_no);
         fetch C_MONTH_HALF_EXISTS into L_half_exists;
         close C_MONTH_HALF_EXISTS;
         ---
         if L_half_exists = 'yes' then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'HALF_NO',
                       'USED_BY_MTH');
           L_error :=TRUE;         
        end if;
         ---	
         open C_PERIOD_HALF_EXISTS(rec.half_no);
         fetch C_PERIOD_HALF_EXISTS into L_half_exists;
         close C_PERIOD_HALF_EXISTS;
         ---
         if L_half_exists = 'yes' then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'HALF_NO',
                       'USED_BY_PERIOD');
           L_error :=TRUE;         
         end if;
         ---	
         open C_SUP_HALF_EXISTS(rec.half_no);
         fetch C_SUP_HALF_EXISTS into L_half_exists;
         close C_SUP_HALF_EXISTS;
         ---
         if L_half_exists = 'yes' then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'HALF_NO',
                       'USED_BY_SUP_MTH');
           L_error :=TRUE;         
         end if;
         ---	
         open C_SYSTEM_HALF_EXISTS(rec.half_no);
         fetch C_SYSTEM_HALF_EXISTS into L_half_exists;
         close C_SYSTEM_HALF_EXISTS;
         ---  


         if L_half_exists = 'yes' then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'HALF_NO',
                       'USED_BY_SYS_VAR');
           L_error :=TRUE;         

         end if;
         ---
         open C_HALF_DATA_BUDGET_EXISTS(rec.half_no);
         fetch C_HALF_DATA_BUDGET_EXISTS into L_half_exists;
         close C_HALF_DATA_BUDGET_EXISTS;
         ---  

         if L_half_exists = 'yes' then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'HALF_NO',
                       'USED_BY_HALF_DATA_BUD');
           L_error :=TRUE;         
         end if;
         ---

         open C_HALF_DATA_EXISTS(rec.half_no);
         fetch C_HALF_DATA_EXISTS into L_half_exists;
         close C_HALF_DATA_EXISTS;
         ---  

         if L_half_exists = 'yes' then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'HALF_NO',
                       'USED_BY_HALF_DATA');
           L_error :=TRUE;         

         end if;
         ---

      end if;
      if NOT L_error then
         L_half_temp_rec.half_no              := rec.half_no;
         L_half_temp_rec.half_name              := rec.half_name;
         L_half_temp_rec.half_date              := rec.half_date;
         if rec.action = action_new then
            if EXEC_HALF_INS(   L_half_temp_rec,
                                L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_HALF_UPD( L_half_temp_rec,
                              L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_HALF_DEL( L_half_temp_rec,
                              L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_half st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_half st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_half st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
       if C_MONTH_HALF_EXISTS%ISOPEN then
          close C_MONTH_HALF_EXISTS;
       end if;
       if C_PERIOD_HALF_EXISTS%ISOPEN then
          close C_PERIOD_HALF_EXISTS;
       end if;

       if C_HALF_DATA_EXISTS%ISOPEN then
          close C_HALF_DATA_EXISTS;
       end if;
       if C_HALF_DATA_BUDGET_EXISTS%ISOPEN then
          close C_HALF_DATA_BUDGET_EXISTS;
       end if;
       if C_SYSTEM_HALF_EXISTS%ISOPEN then
          close C_SYSTEM_HALF_EXISTS;
       end if;
       if C_SUP_HALF_EXISTS%ISOPEN then
          close C_SUP_HALF_EXISTS;
       end if;
       if C_SVC_HALF%ISOPEN then
          close C_SVC_HALF;
       end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HALF;
------------------------------------------------------------
FUNCTION EXEC_CALENDAR_INS(  L_calendar_temp_rec   IN   CALENDAR%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_CALENDAR_INS';
   L_table   VARCHAR2(255):= 'SVC_CALENDAR';
BEGIN
   insert
     into calendar
   values L_calendar_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CALENDAR_INS;
------------------------------------------------------------------------------
FUNCTION EXEC_CALENDAR_UPD( L_calendar_temp_rec   IN   CALENDAR%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_CALENDAR_UPD';
   L_table   VARCHAR2(255):= 'SVC_CALENDAR';
BEGIN
   update calendar
      set row = L_calendar_temp_rec
    where 1 = 1
      and first_day = L_calendar_temp_rec.first_day
      and year_454 = L_calendar_temp_rec.year_454
      and month_454 = L_calendar_temp_rec.month_454
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CALENDAR_UPD;
------------------------------------------------------------------------------
FUNCTION EXEC_CALENDAR_DEL(  L_calendar_temp_rec   IN   CALENDAR%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_HALF.EXEC_CALENDAR_DEL';
   L_table   VARCHAR2(255):= 'SVC_CALENDAR';
BEGIN
   delete
     from calendar
    where 1 = 1
      and first_day = L_calendar_temp_rec.first_day
      and year_454 = L_calendar_temp_rec.year_454
      and month_454 = L_calendar_temp_rec.month_454
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CALENDAR_DEL;
------------------------------------------------------------------------
FUNCTION PROCESS_CALENDAR( I_process_id   IN   SVC_CALENDAR.PROCESS_ID%TYPE,
                           I_chunk_id     IN SVC_CALENDAR.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_HALF.PROCESS_CALENDAR';
   L_error_message VARCHAR2(600);
   L_CALENDAR_temp_rec CALENDAR%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_CALENDAR';
BEGIN
   FOR rec IN c_svc_CALENDAR(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.UK_CALENDAR_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'CALENDAR',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FIRST_DAY',
                     L_error_message);
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.first_day is not null
         and rec.UK_CALENDAR_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FIRST_DAY',
                    'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_CALENDAR_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'CALENDAR',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MONTH_454,YEAR_454',
                    L_error_message);
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_CALENDAR_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MONTH_454,YEAR_454',
                    'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;
	  
      if rec.action in (action_new,action_mod)
	     and NOT(  rec.NO_OF_WEEKS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'NO_OF_WEEKS',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
	  if rec.action in (action_new,action_mod)
	     and NOT(  rec.FIRST_DAY  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FIRST_DAY',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_calendar_temp_rec.no_of_weeks              := rec.no_of_weeks;
         L_calendar_temp_rec.month_454                := rec.month_454;
         L_calendar_temp_rec.year_454                 := rec.year_454;
         L_calendar_temp_rec.first_day                := rec.first_day;
         if rec.action = action_new then
            if EXEC_CALENDAR_INS(   L_calendar_temp_rec,
                                    L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_CALENDAR_UPD( L_calendar_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_CALENDAR_DEL( L_calendar_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_calendar st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_calendar st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_calendar st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_CALENDAR%ISOPEN then
         close C_SVC_CALENDAR;
      end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CALENDAR;
------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete 
     from svc_calendar 
    where process_id=I_process_id;

   delete 
     from svc_half_tl 
    where process_id=I_process_id;

   delete 
     from svc_half 
    where process_id=I_process_id;
END;

-------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER
                 )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_HALF.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';

BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   
   if PROCESS_CALENDAR(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;

   commit;
   
   if PROCESS_HALF(I_process_id,
                   I_chunk_id)=FALSE then
      return FALSE;
   end if;

   commit;

   if PROCESS_HALF_TL(I_process_id,
                      I_chunk_id)=FALSE then
      return FALSE;
   end if; 

   O_error_count := LP_errors_tab.COUNT();
   
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      if C_GET_ERR_COUNT%ISOPEN then
         close C_GET_ERR_COUNT;
      end if;
      
      if C_GET_WARN_COUNT%ISOPEN then
         close C_GET_WARN_COUNT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_HALF;
/