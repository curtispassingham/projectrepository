
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SYSTEM_CALENDAR_SQL AS
--------------------------------------------------------------------
FUNCTION GET_HALF_NO	(O_error_message   IN OUT VARCHAR2,
			 O_half_no	   IN OUT NUMBER)
		return BOOLEAN IS

   L_program	VARCHAR2(64)	:= 'SYSTEM_CALENDAR_SQL.GET_HALF_NO';

   CURSOR c_period IS
	SELECT half_no
	FROM period;

BEGIN
   OPEN c_period;
   FETCH c_period INTO O_half_no;
   if c_period%NOTFOUND then
	CLOSE c_period;
	O_error_message := SQL_LIB.CREATE_MSG ('INV_CURSOR','C_period',L_program,NULL);
	return FALSE;
   else
	CLOSE c_period;
	return TRUE;
   end if;

EXCEPTION
   when OTHERS then
	O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
						SQLERRM,
						L_program,
						NULL);
	return FALSE;

END GET_HALF_NO;
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
FUNCTION GET_CAL_OPTS (	O_error_message	      IN OUT VARCHAR2,
			O_cal_ind	      IN OUT VARCHAR2,
                        O_start_of_half_month IN OUT NUMBER,
                        O_start_last_year     IN OUT NUMBER)
			return BOOLEAN IS

   L_program	VARCHAR2(64)	:= 'SYSTEM_CALENDAR_SQL.GET_CAL_OPTS';

   CURSOR c_cal_opts IS
	SELECT calendar_454_ind,
               ABS(start_of_half_month),
               SIGN(start_of_half_month)
	FROM system_options;

BEGIN
   OPEN c_cal_opts;
   FETCH c_cal_opts INTO O_cal_ind,
                         O_start_of_half_month,
                         O_start_last_year;
   if c_cal_opts%NOTFOUND then
	CLOSE c_cal_opts;
	O_error_message := SQL_LIB.CREATE_MSG ('INV_CURSOR','C_cal_opts',L_program,NULL);
	return FALSE;
   else
	CLOSE c_cal_opts;
	return TRUE;
   end if;

EXCEPTION
   when OTHERS then
	O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
						SQLERRM,
						L_program,
						NULL);
	return FALSE;

END GET_CAL_OPTS;
-----------------------------------------------------------------------------



END SYSTEM_CALENDAR_SQL;
/


