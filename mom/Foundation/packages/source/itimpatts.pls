
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_IMPORT_ATTR_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------
--Name:         ITEM_CHILD_EXISTS	
--Purpose: 	    Checks to see if child items exist on the ITEM_IMPORT_ATTR table
--Created By:   Ryan Glanzer, 10-10-00
--------------------------------------------------------------------------------------------

FUNCTION ITEM_CHILD_EXISTS(O_error_message   IN OUT     VARCHAR2,
                           O_exists          IN OUT     BOOLEAN,
                           I_item            IN         ITEM_IMPORT_ATTR.ITEM%TYPE)
			RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
--Name:	    DELETE_INSERT_ITEM_CHILD	
--Purpose:      This function will delete all records that are children of the selected
--              record down to the transaction level on the ITEM_IMPORT_ATTR table.  It 
--              will then reinsert the children records containing the records from 
--              the parent.
--Created By:   Ryan Glanzer, 10-10-00
--------------------------------------------------------------------------------------------
FUNCTION DELETE_INSERT_ITEM_CHILD(O_error_message    IN OUT     VARCHAR2,
                                  I_item             IN         ITEM_IMPORT_ATTR.ITEM%TYPE,
                                  I_tooling          IN         ITEM_IMPORT_ATTR.TOOLING%TYPE,
                                  I_first_order_ind  IN         ITEM_IMPORT_ATTR.FIRST_ORDER_IND%TYPE,
                                  I_amortize_base    IN         ITEM_IMPORT_ATTR.AMORTIZE_BASE%TYPE,
                                  I_open_balance     IN         ITEM_IMPORT_ATTR.OPEN_BALANCE%TYPE,
                                  I_commodity        IN         ITEM_IMPORT_ATTR.COMMODITY%TYPE,
                                  I_import_desc      IN         ITEM_IMPORT_ATTR.IMPORT_DESC%TYPE)
			      RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
END ITEM_IMPORT_ATTR_SQL;
/


