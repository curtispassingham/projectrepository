
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY PRIORITY_GROUP_SQL AS
--------------------------------------------------------------------------------

   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(RECORD_LOCKED, -54);
   
--------------------------------------------------------------------------------
FUNCTION NEXT_PRIORITY_GROUP_ID_NUMBER (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_priority_group_id  IN OUT PRIORITY_GROUP.PRIORITY_GROUP_ID%TYPE) 
   RETURN BOOLEAN IS
   
   L_program                  VARCHAR2(60)   := 'PRIORITY_GROUP_SQL.NEXT_PRIORITY_GROUP_ID_NUMBER';
   
   L_priority_group_number    PRIORITY_GROUP.PRIORITY_GROUP_ID%TYPE;
   L_wrap_sequence_number     PRIORITY_GROUP.PRIORITY_GROUP_ID%TYPE;
   L_first_time               VARCHAR2(1) := 'Y';
   L_dummy                    VARCHAR2(1);   

   CURSOR C_PRIORITY_GROUP_ID_EXISTS IS
      SELECT 'x'
        FROM priority_group
       WHERE priority_group_id = O_priority_group_id;

BEGIN
   
   LOOP

      SELECT priority_group_seq.NEXTVAL
      INTO   L_priority_group_number
      FROM   sys.dual;

      IF (L_first_time = 'Y') THEN
          L_wrap_sequence_number :=L_priority_group_number;
          L_first_time := 'N';
      ELSIF (L_priority_group_number = L_wrap_sequence_number) THEN
          O_error_message := 'Fatal error - No available priority group id numbers';
          return FALSE;
      END IF;

      O_priority_group_id := L_priority_group_number;

      OPEN C_PRIORITY_GROUP_ID_EXISTS;
      FETCH C_PRIORITY_GROUP_ID_EXISTS INTO L_dummy;

      IF C_PRIORITY_GROUP_ID_EXISTS%NOTFOUND THEN
         CLOSE C_PRIORITY_GROUP_ID_EXISTS;
         exit;
      END IF;
         
      CLOSE C_PRIORITY_GROUP_ID_EXISTS;

   END LOOP;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'PRIORITY_GROUP_SQL.NEXT_PRIORITY_GROUP_ID_NUMBER',
                                             to_char(SQLCODE));
      return FALSE;
END NEXT_PRIORITY_GROUP_ID_NUMBER;

--------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE_PRIORITY (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists             IN OUT BOOLEAN,
                                   I_priority_group_id  IN     PRIORITY_GROUP.PRIORITY_GROUP_ID%TYPE,
                                   I_priority           IN     PRIORITY_GROUP.PRIORITY%TYPE) 
   RETURN BOOLEAN IS 

   L_program          VARCHAR2(60)   := 'PRIORITY_GROUP_SQL.CHECK_DUPLICATE_PRIORITY';
   L_priority_exist   VARCHAR2(1);

   cursor C_PRIORITY_EXISTS is
      select 'x'
        from priority_group
       where priority = I_priority
         and priority_group_id <> I_priority_group_id;

BEGIN
   if I_priority is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_priority,
                                            L_program,
                                            NULL);
      return FALSE;
   else
      --
      O_exists := FALSE;
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_PRIORITY_EXISTS',
                       'PRIORITY_GROUP',
                       I_priority);
      open C_PRIORITY_EXISTS;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_PRIORITY_EXISTS',
                       'PRIORITY_GROUP',
                       I_priority);
      fetch C_PRIORITY_EXISTS into L_priority_exist;
      --
      if L_priority_exist= 'x' then
         O_exists := TRUE;
      end if;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PRIORITY_EXISTS',
                       'PRIORITY_GROUP',
                       I_priority);

      close C_PRIORITY_EXISTS;
      --
   end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CHECK_DUPLICATE_PRIORITY; 

--------------------------------------------------------------------------------
FUNCTION CHECK_LOCATION_PRIORITY(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists             IN OUT BOOLEAN,
                                 I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                                 I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE)
   RETURN BOOLEAN IS
   
   L_program           VARCHAR2(60) := 'PRIORITY_GROUP_SQL.CHECK_LOCATION_PRIORITY';
   L_exists            VARCHAR2(1);   
   
   cursor C_LOCATION_EXISTS is
      select 'Y'
        from priority_group_locations
       where priority_group_id <> I_priority_group_id
         and location          = I_location;
    
BEGIN
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_location,
                                            L_program,
                                            NULL);
      return FALSE;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCATION_EXISTS',
                       'PRIORITY_GROUP_LOCATIONS',
                       'I_location : '||TO_CHAR(I_location));
      open C_LOCATION_EXISTS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOCATION_EXISTS',
                       'PRIORITY_GROUP_LOCATIONS',
                       'I_location : '||TO_CHAR(I_location));
      fetch C_LOCATION_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCATION_EXISTS',
                       'PRIORITY_GROUP_LOCATIONS',
                       'I_location : '||TO_CHAR(I_location));
      close C_LOCATION_EXISTS;
      
      O_exists := FALSE;
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   end if;
   return TRUE;
                                                    
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;   
END CHECK_LOCATION_PRIORITY;

-------------------------------------------------------------------------------- 
FUNCTION CHECK_SAME_LOC_PRIORITY_GROUP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists             IN OUT BOOLEAN,
                                       I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                                       I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE)                                       
   RETURN BOOLEAN IS
   
   L_program      VARCHAR2(60) := 'PRIORITY_GROUP_SQL.CHECK_SAME_LOC_PRIORITY_GROUP';
   L_exists       VARCHAR2(1);  
   
   cursor C_LOC_IN_PRIORITY_GROUP_EXISTS is
      select 'Y'
        from priority_group_locations 
       where priority_group_id = I_priority_group_id
         and location          = I_location;
   
BEGIN
   
   open C_LOC_IN_PRIORITY_GROUP_EXISTS;
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOC_IN_PRIORITY_GROUP_EXISTS',
                    'PRIORITY_GROUP_LOCATIONS',
                    'I_location : '||TO_CHAR(I_location));
   fetch C_LOC_IN_PRIORITY_GROUP_EXISTS into L_exists;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOC_IN_PRIORITY_GROUP_EXISTS',
                    'PRIORITY_GROUP_LOCATIONS',
                    'I_location : '||TO_CHAR(I_location));
   close C_LOC_IN_PRIORITY_GROUP_EXISTS;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOC_IN_PRIORITY_GROUP_EXISTS',
                    'PRIORITY_GROUP_LOCATIONS',
                    'I_location : '||TO_CHAR(I_location));
                    
   O_exists := FALSE;
   if L_exists = 'Y' then 
      O_exists := TRUE;
   end if;                    
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;   
END CHECK_SAME_LOC_PRIORITY_GROUP; 

--------------------------------------------------------------------------------
FUNCTION ADD_LOCATIONS_PRIORITY_GROUP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_diff_priority_grp  IN OUT VARCHAR2,
                                      I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                                      I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE,
                                      I_location_type      IN     PRIORITY_GROUP_LOCATIONS.LOCATION_TYPE%TYPE,
                                      I_create_date        IN     PRIORITY_GROUP_LOCATIONS.CREATE_DATE%TYPE,
                                      I_update_date        IN     PRIORITY_GROUP_LOCATIONS.LAST_UPDATE_DATE%TYPE,
                                      I_create_user_id     IN     PRIORITY_GROUP_LOCATIONS.CREATE_USER_ID%TYPE,      
                                      I_update_user_id     IN     PRIORITY_GROUP_LOCATIONS.LAST_UPDATE_USER_ID%TYPE)
   RETURN BOOLEAN IS       

   L_program           VARCHAR2(60) := 'PRIORITY_GROUP_SQL.ADD_LOCATIONS_PRIORITY_GROUP';   
   L_exists            BOOLEAN;
   L_exists1           BOOLEAN;
   L_loc_list_desc     LOC_LIST_HEAD.LOC_LIST_DESC%TYPE;
   L_create_date       LOC_LIST_HEAD.CREATE_DATE%TYPE;
   L_last_rebuild_date LOC_LIST_HEAD.LAST_REBUILD_DATE%TYPE;
   L_create_id         LOC_LIST_HEAD.CREATE_ID%TYPE;
   L_static_ind        LOC_LIST_HEAD.STATIC_IND%TYPE;
   L_batch_rebuild_ind LOC_LIST_HEAD.BATCH_REBUILD_IND%TYPE;
   L_source            LOC_LIST_HEAD.SOURCE%TYPE;
   L_external_ref_no   LOC_LIST_HEAD.EXTERNAL_REF_NO%TYPE;
   L_comment_desc      LOC_LIST_HEAD.COMMENT_DESC%TYPE;
   L_criteria_exist    VARCHAR2(1);
   
   cursor C_GET_LOC_LIST is
   select location, loc_type
     from loc_list_detail
    where loc_list = I_location;
    
   TYPE C_GET_LOC_LIST_TBL is TABLE OF C_GET_LOC_LIST%ROWTYPE
      INDEX BY BINARY_INTEGER;              
   
   L_loc_tbl           C_GET_LOC_LIST_TBL;
    
BEGIN      
   
   if I_priority_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_priority_group_id',L_program,NULL);
      return FALSE;
   end if;
   
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_location',L_program,NULL);
      return FALSE;
   end if;
   
   if I_location_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_location_type',L_program,NULL);
      return FALSE;
   end if;
   
   if I_create_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_create_date',L_program,NULL);
      return FALSE;
   end if;
   
   if I_update_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_update_date',L_program,NULL);
      return FALSE;
   end if;
   
   if I_create_user_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_create_user_id',L_program,NULL);
      return FALSE;
   end if;
   
   if I_update_user_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_update_user_id',L_program,NULL);
      return FALSE;
   end if;
   
   if I_location_type = 'S' or I_location_type = 'W' then
      -- if location is a store or warehouse      
      
      I_diff_priority_grp := 'N';               
      if PRIORITY_GROUP_SQL.CHECK_SAME_LOC_PRIORITY_GROUP(O_error_message,
                                                          L_exists,
                                                          I_priority_group_id,
                                                          I_location) = FALSE then
         return FALSE;
      end if;
            
      if PRIORITY_GROUP_SQL.CHECK_LOCATION_PRIORITY(O_error_message,
		 	  	  	                                      L_exists1,
                                                    I_priority_group_id,
		 	  	  	                                      I_location) = FALSE then	
         return FALSE;
		 	end if;
            
      if L_exists = FALSE and L_exists1 = FALSE then
                  
         SQL_LIB.SET_MARK('INSERT',NULL,'PRIORITY_GROUP_LOCATIONS','LOCATION : '||TO_CHAR(I_location)||' PRIORITY GROUP : '||TO_CHAR(I_priority_group_id));
         insert into priority_group_locations(priority_group_id,
                                             location_type,
                                             location,
                                             create_date,
                                             last_update_date,
                                             create_user_id,
                                             last_update_user_id)
                                       values(I_priority_group_id,
                                             I_location_type,
                                             I_location,
                                             I_create_date,
                                             I_update_date,
                                             I_create_user_id,
                                             I_update_user_id);
      else
         if L_exists = TRUE then              
            O_error_message := SQL_LIB.CREATE_MSG('LOC_ALREADY_PRESENT', NULL, NULL, NULL);            
            return FALSE;
         else            
            I_diff_priority_grp := 'Y';                        
         end if;  
      end if;      
   else  
      -- if location is a location list
      if LOCLIST_ATTRIBUTE_SQL.GET_HEADER_INFO(O_error_message,
                                               L_loc_list_desc,
                                               L_create_date,
                                               L_last_rebuild_date,
                                               L_create_id,
                                               L_static_ind,
                                               L_batch_rebuild_ind,
                                               L_source,
                                               L_external_ref_no,
                                               L_comment_desc,
                                               I_location) = FALSE then
         return FALSE;
      end if;
      
      -- If loclist is dynamic, rebuild the loclist
      if L_static_ind = 'N' then
         if LOCLIST_BUILD_SQL.REBUILD_LIST(O_error_message,
                                           L_criteria_exist,
                                           I_location) = FALSE then
            return FALSE;
         end if;                               
      end if;                                         
             
      open C_GET_LOC_LIST;
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_LOC_LIST',
                       'LOC_LIST_DETAIL',
                       I_location);
      --                       
      fetch C_GET_LOC_LIST BULK COLLECT INTO L_loc_tbl;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_LOC_LIST',
                       'LOC_LIST_DETAIL',
                       I_location);
      --
      close C_GET_LOC_LIST;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_LOC_LIST',
                       'LOC_LIST_DETAIL',
                       I_location);
      
      I_diff_priority_grp := 'N';
      FOR i in 1..L_loc_tbl.COUNT LOOP         
         -- checking if the locations in the location list are already present in the same priority group
         if PRIORITY_GROUP_SQL.CHECK_SAME_LOC_PRIORITY_GROUP(O_error_message,
                                                             L_exists,
                                                             I_priority_group_id,
                                                             L_loc_tbl(i).location) = FALSE then
            return FALSE;
         end if;
         
         -- checking if the locations in the location list are already present in other priority groups
         if PRIORITY_GROUP_SQL.CHECK_LOCATION_PRIORITY(O_error_message,
		 	  	  	                                         L_exists1,
                                                       I_priority_group_id,
		 	  	  	                                         L_loc_tbl(i).location) = FALSE then	
		 	  	     return FALSE;
		 	   end if;                
          
         if L_exists = FALSE and L_exists1 = FALSE then
            SQL_LIB.SET_MARK('INSERT',NULL,'PRIORITY_GROUP_LOCATIONS','LOCATION : '||TO_CHAR(L_loc_tbl(i).location)||' PRIORITY GROUP : '||TO_CHAR(I_priority_group_id));
            insert into priority_group_locations(priority_group_id,
                                                 location_type,
                                                 location,
                                                 create_date,
                                                 last_update_date,
                                                 create_user_id,
                                                 last_update_user_id)
                                           values(I_priority_group_id,
                                                 L_loc_tbl(i).loc_type,
                                                 L_loc_tbl(i).location,
                                                 I_create_date,
                                                 I_update_date,
                                                 I_create_user_id,
                                                 I_update_user_id);
         else                        
            if L_exists1 = TRUE then               
               I_diff_priority_grp := 'Y';                              
            end if;  
         end if;                                                                      
      END LOOP;            
   end if;                                              
      
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;                                                        
END ADD_LOCATIONS_PRIORITY_GROUP;
--------------------------------------------------------------------------------      
FUNCTION UPDATE_PRIORITY_GROUP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                               I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE,
                               I_location_type      IN     PRIORITY_GROUP_LOCATIONS.LOCATION_TYPE%TYPE)
                               
RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'PRIORITY_GROUP_SQL.UPDATE_PRIORITY_GROUP';        
   
   cursor C_UPDATABLE_LOCS is
      select ld.location 
        from loc_list_detail ld, 
             priority_group_locations pgl
       where ld.loc_list = I_location
         and ld.location = pgl.location
         and pgl.priority_group_id <> I_priority_group_id;
         
   TYPE LOCS_TBL is TABLE of PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE;
   
   L_updatable_locs    LOCS_TBL;
      
BEGIN   
      
   L_updatable_locs := LOCS_TBL();   
   if I_location_type = 'S' or I_location_type = 'W' then        
      L_updatable_locs.EXTEND();      
      L_updatable_locs(L_updatable_locs.COUNT) := I_location;
   else      
      open C_UPDATABLE_LOCS;
      fetch C_UPDATABLE_LOCS BULK COLLECT INTO L_updatable_locs;
      close C_UPDATABLE_LOCS;    
   end if;
   
   if L_updatable_locs.COUNT > 0 then
      FOR i in L_updatable_locs.FIRST .. L_updatable_locs.LAST LOOP         
         update priority_group_locations
            set priority_group_id = I_priority_group_id,
                last_update_date = SYSDATE,
                last_update_user_id = GET_USER
          where location = L_updatable_locs(i);
      END LOOP;
   end if;      
       
   return TRUE;   
   
EXCEPTION
   when OTHERS then      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE; 
END UPDATE_PRIORITY_GROUP;                              
--------------------------------------------------------------------------------                                                                  
FUNCTION LOCS_EXIST_IN_LOCLIST(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_loc_list           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'PRIORITY_GROUP_SQL.LOCS_EXIST_IN_LOCLIST';        
   L_exists            VARCHAR2(1)  := 'N';
   
   cursor C_LOCS_EXIST_IN_LOCLIST is
      select 'Y' 
        from loc_list_detail
       where loc_list = I_loc_list;
    
BEGIN
   
   open C_LOCS_EXIST_IN_LOCLIST;
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCS_EXIST_IN_LOCLIST',
                    'LOC_LIST_DETAIL',
                     I_loc_list);
   --
   fetch C_LOCS_EXIST_IN_LOCLIST into L_exists;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCS_EXIST_IN_LOCLIST',
                    'LOC_LIST_DETAIL',
                     I_loc_list);
   --
   close C_LOCS_EXIST_IN_LOCLIST;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCS_EXIST_IN_LOCLIST',
                    'LOC_LIST_DETAIL',
                     I_loc_list);  
   
   if L_exists = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_LOCS_GROUP',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   return TRUE;
     
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE; 
END LOCS_EXIST_IN_LOCLIST;                              
--------------------------------------------------------------------------------                                                     
FUNCTION DELETE_PRIORITY_GRP_LOC(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_priority_group_id  IN     PRIORITY_GROUP_LOCATIONS.PRIORITY_GROUP_ID%TYPE,
                                 I_location           IN     PRIORITY_GROUP_LOCATIONS.LOCATION%TYPE)

   RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'PRIORITY_GROUP_SQL.DELETE_PRIORITY_GRP_LOC';    
   L_table             VARCHAR2(50) := 'PRIORITY_GROUP_LOCATIONS';         
   
   cursor C_LOCK_PRIORITY_GRP_LOC is
      select 'x'
        from priority_group_locations
       where priority_group_id = I_priority_group_id
         and location          = I_location
         for update nowait;
    
BEGIN

   open C_LOCK_PRIORITY_GRP_LOC;
   close C_LOCK_PRIORITY_GRP_LOC;
   
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'PRIORITY_GROUP_LOCATIONS',
                    'LOCATION : '||I_location);

   delete from priority_group_locations
      where priority_group_id = I_priority_group_id
        and location          = I_location;
        
   return TRUE;                   
   
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DEL_PRT_LOC_LOCKED',
                                            L_table,
                                            I_location);
   return FALSE;
   
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE; 
END DELETE_PRIORITY_GRP_LOC;  
--------------------------------------------------------------------------------
FUNCTION DELETE_PRIORITY_GRP(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_priority_group_id  IN     PRIORITY_GROUP.PRIORITY_GROUP_ID%TYPE)
                             
   RETURN BOOLEAN IS
   
   L_program           VARCHAR2(60) := 'PRIORITY_GROUP_SQL.DELETE_PRIORITY_GRP';    
   L_table             VARCHAR2(50) := 'PRIORITY_GROUP_LOCATIONS';         
   
   cursor C_LOCK_PRT_GRP_LOCS is
      select 'x'
        from priority_group_locations
       where priority_group_id = I_priority_group_id
         for update nowait;

BEGIN                  
   
   open C_LOCK_PRT_GRP_LOCS;
   close C_LOCK_PRT_GRP_LOCS;
   
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'PRIORITY_GROUP_LOCATIONS',
                    'PRIORITY_GROUP_ID : '||I_priority_group_id);

   delete from priority_group_locations
      where priority_group_id = I_priority_group_id;
        
   return TRUE; 
         
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DEL_PRT_LOC_LOCKED',
                                            L_table);
   return FALSE;
   
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE; 
END DELETE_PRIORITY_GRP;  
--------------------------------------------------------------------------------                                  
FUNCTION DELETE_PRIORITY_GROUP_TL (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_priority_group_id  IN       PRIORITY_GROUP_TL.PRIORITY_GROUP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'PRIORITY_GROUP_SQL.DELETE_PRIORITY_GROUP_TL';    
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PRIORITY_GROUP_TL is
      select 'x'
        from priority_group_tl
       where priority_group_id = I_priority_group_id
         for update nowait;
BEGIN

   if I_priority_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_priority_group_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'PRIORITY_GROUP_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_PRIORITY_GROUP_TL',
                    L_table,
                    'I_priority_group_id '||I_priority_group_id);
   open C_LOCK_PRIORITY_GROUP_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_PRIORITY_GROUP_TL',
                    L_table,
                    'I_priority_group_id '||I_priority_group_id);
   close C_LOCK_PRIORITY_GROUP_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'I_priority_group_id '||I_priority_group_id);
   delete from priority_group_tl
          where priority_group_id = I_priority_group_id;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_PRIORITY_GROUP_TL;
----------------------------------------------------------------------------------------------------------
END;
/