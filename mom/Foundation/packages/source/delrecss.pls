CREATE OR REPLACE PACKAGE DELETE_RECORDS_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------
-- Name:      DEL_DEPS
-- Purpose:   This function will perform all the delete logic
--            associated with deleting a department.
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_DEPS(I_key_value       IN       VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------
-- Name:      DEL_CLASS
-- Purpose:   This function will perform all the delete logic
--            associated with deleting a class.
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_CLASS(I_key_value       IN       VARCHAR2,
                   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------
-- Name:    DEL_SUBCLASS
-- Purpose: This function will perform all the delete logic
--          associated with deleting a subclass.
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_SUBCLASS(I_key_value       IN       VARCHAR2,
                      O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------
-- Name:    DEL_STORE
-- Purpose: This function will perform all the delete logic
--          associated with deleting a store.
-- Create By: Retek
----------------------------------------------------------------

FUNCTION DEL_STORE(I_key_value       IN       VARCHAR2,
                   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------
-- Name:      DEL_WH
-- Purpose:   This function will perform all the delete logic
--            associated with deleting a warehouse.
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_WH(I_key_value     IN     VARCHAR2,
                O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE) RETURN BOOLEAN;


----------------------------------------------------------------
-- Name:      DEL_COST_ZONE_GROUP
-- Purpose:   This function will perform all the delete logic
--            associated with deleting a cost zone group.
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_COST_ZONE_GROUP(i_key_value   IN     VARCHAR2,
                             error_message IN OUT VARCHAR2) RETURN BOOLEAN;

------------------------------------------------------------------
-- Name:      DEL_MRT
-- Purpose:   This function will perform most purging logic
--            originally contained within mrtprg.pc
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_MRT (O_SQLCODE     OUT VARCHAR2,
                  O_TABLE       OUT VARCHAR2,
                  O_ERR_DATA    OUT VARCHAR2,
                  I_TSF_NO        IN  NUMBER) RETURN BOOLEAN;
------------------------------------------------------------------
-- Name:      DEL_DEAL
-- Purpose:   This function contains most purging logic
--            originally contained within dealprg.pc
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_DEAL (O_SQLCODE     OUT VARCHAR2,
                   O_TABLE       OUT VARCHAR2,
                   O_ERR_DATA    OUT VARCHAR2,
                   I_DEAL_NO     IN  NUMBER) RETURN BOOLEAN;
------------------------------------------------------------------
-- Name:      DEL_FIXED_DEAL
-- Purpose:   This function will perform fixed deal purging logic
--            originally contained within dealprg.pc
-- Created By: Retek
----------------------------------------------------------------
FUNCTION DEL_FIXED_DEAL (O_SQLCODE     OUT VARCHAR2,
                         O_TABLE       OUT VARCHAR2,
                         O_ERR_DATA    OUT VARCHAR2,
                         I_FIXED_DEAL_NO     IN  NUMBER) RETURN BOOLEAN;
------------------------------------------------------------------
-- Name:      DEL_EXTERNAL_FINISHER
-- Purpose:   This function will perform all the delete logic
--            associated with deleting an external finisher.
-- Created By: Albert Veloso
----------------------------------------------------------------
FUNCTION DEL_EXTERNAL_FINISHER(i_key_value   IN     VARCHAR2,
                               error_message IN OUT VARCHAR2) RETURN BOOLEAN;

------------------------------------------------------------------
END;
/


