create or replace PACKAGE BODY CORESVC_COMPHEAD as
   cursor C_SVC_COMPHEAD(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
      select pk_comphead.rowid                as pk_comphead_rid,
             com_cnt_fk.rowid                 as com_cnt_fk_rid,
             pk_comphead.co_name              as base_co_name,
             pk_comphead.co_add1              as base_co_add1,
             pk_comphead.co_add2              as base_co_add2,
             pk_comphead.co_add3              as base_co_add3,
             pk_comphead.co_city              as base_co_city,
             UPPER(pk_comphead.co_state)      as base_co_state,
             UPPER(pk_comphead.co_country)    as base_co_country,
             UPPER(pk_comphead.co_post)              as base_co_post,
             pk_comphead.co_name_secondary    as base_co_name_secondary,
             UPPER(pk_comphead.co_jurisdiction_code) as base_co_jurisdiction_code,
             UPPER(st.co_jurisdiction_code) as co_jurisdiction_code,
             st.co_name_secondary,
             st.co_post,
             UPPER(st.co_country) as co_country,
             UPPER(st.co_state) as co_state,
             st.co_city,
             st.co_add3,
             st.co_add2,
             st.co_add1,
             st.co_name,
             st.company,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)                 as action,
             st.process$status
        from svc_comphead  st,
             comphead      pk_comphead,
             country       com_cnt_fk,
             dual
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.company    = pk_comphead.company (+)
         and UPPER(st.co_country) = UPPER(com_cnt_fk.country_id (+));

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;

   LP_errors_tab      errors_tab_typ;

   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;

   LP_s9t_errors_tab  S9T_errors_tab_typ;

   Type COMPHEAD_TL_TAB IS TABLE OF COMPHEAD_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
end GET_SHEET_NAME_TRANS;
------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id  IN  S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet    IN  VARCHAR2,
                          I_row_seq  IN  NUMBER,
                          I_col      IN  VARCHAR2,
                          I_sqlcode  IN  NUMBER,
                          I_sqlerrm  IN  VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key             :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-------------------------------------------------------------------------------------------------------------   --------------------------------------------------------
PROCEDURE POPULATE_NAMES( I_file_id   NUMBER) IS
   L_sheets          S9T_PKG.NAMES_map_typ;
   COMPHEAD_cols     S9T_PKG.NAMES_map_typ;
   COMPHEAD_TL_cols  S9T_PKG.NAMES_map_typ;
BEGIN
   L_sheets                      := S9T_PKG.get_sheet_names(I_file_id);
   COMPHEAD_cols                 := S9T_PKG.get_col_names(I_file_id,COMPHEAD_sheet);
   COMPHEAD$ACTION               := COMPHEAD_cols('ACTION');
   COMPHEAD$COMPANY              := COMPHEAD_cols('COMPANY');
   COMPHEAD$CO_NAME              := COMPHEAD_cols('CO_NAME');
   COMPHEAD$CO_ADD1              := COMPHEAD_cols('CO_ADD1');
   COMPHEAD$CO_ADD2              := COMPHEAD_cols('CO_ADD2');
   COMPHEAD$CO_ADD3              := COMPHEAD_cols('CO_ADD3');
   COMPHEAD$CO_CITY              := COMPHEAD_cols('CO_CITY');
   COMPHEAD$CO_STATE             := COMPHEAD_cols('CO_STATE');
   COMPHEAD$CO_COUNTRY           := COMPHEAD_cols('CO_COUNTRY');
   COMPHEAD$CO_POST              := COMPHEAD_cols('CO_POST');
   COMPHEAD$CO_NAME_SECONDARY    := COMPHEAD_cols('CO_NAME_SECONDARY');
   COMPHEAD$CO_JURISDICTION_CODE := COMPHEAD_cols('CO_JURISDICTION_CODE');

   L_sheets                         := S9T_PKG.get_sheet_names(I_file_id);
   COMPHEAD_TL_cols                 := S9T_PKG.get_col_names(I_file_id,COMPHEAD_TL_sheet);
   COMPHEAD_TL$ACTION               := COMPHEAD_TL_cols('ACTION');
   COMPHEAD_TL$LANG                 := COMPHEAD_TL_cols('LANG');
   COMPHEAD_TL$COMPANY              := COMPHEAD_TL_cols('COMPANY');
   COMPHEAD_TL$CO_NAME              := COMPHEAD_TL_cols('CO_NAME');
   COMPHEAD_TL$CO_ADD1              := COMPHEAD_TL_cols('CO_ADD1');
   COMPHEAD_TL$CO_ADD2              := COMPHEAD_TL_cols('CO_ADD2');
   COMPHEAD_TL$CO_ADD3              := COMPHEAD_TL_cols('CO_ADD3');
   COMPHEAD_TL$CO_CITY              := COMPHEAD_TL_cols('CO_CITY');
   COMPHEAD_TL$CO_NAME_SECONDARY    := COMPHEAD_TL_cols('CO_NAME_SECONDARY');
END POPULATE_NAMES;
--------------------------------------------------------------------------------------------------------------------------------------------- --------------------------
PROCEDURE POPULATE_COMPHEAD( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = COMPHEAD_sheet )

   select s9t_row(s9t_cells( CORESVC_COMPHEAD.action_mod,
                             company,
                             co_name,
                             co_add1,
                             co_add2,
                             co_add3,
                             co_city,
                             co_state,
                             co_country,
                             co_post,
                             co_name_secondary,
                             co_jurisdiction_code ))
     from comphead ;
END POPULATE_COMPHEAD;
--------------------------------------------------------------------------------------------------------------------------------------------- --------------------------
PROCEDURE POPULATE_COMPHEAD_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = COMPHEAD_TL_sheet )

   select s9t_row(s9t_cells( CORESVC_COMPHEAD.action_mod,
                             company,
                             lang,
                             co_name,
                             co_add1,
                             co_add2,
                             co_add3,
                             co_city,
                             co_name_secondary ))
     from comphead_tl ;
END POPULATE_COMPHEAD_TL;
--------------------------------------------------------------------------------------------------------------------------- --------------------------------------------
PROCEDURE INIT_S9T( O_file_id IN OUT NUMBER) IS
   L_file      S9T_FILE;
   L_file_name S9T_FOLDER.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||sysdate||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;


   L_file.user_lang := GET_USER_LANG;

   L_file.add_sheet(COMPHEAD_sheet);
   L_file.sheets(L_file.get_sheet_index(COMPHEAD_sheet)).column_headers := s9t_cells('ACTION',
                                                                                     'COMPANY',
                                                                                     'CO_NAME',
                                                                                     'CO_ADD1',
                                                                                     'CO_ADD2',
                                                                                     'CO_ADD3',
                                                                                     'CO_CITY',
                                                                                     'CO_STATE',
                                                                                     'CO_COUNTRY',
                                                                                     'CO_POST',
                                                                                     'CO_NAME_SECONDARY',
                                                                                     'CO_JURISDICTION_CODE');

   L_file.add_sheet(COMPHEAD_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(COMPHEAD_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                        'LANG',
                                                                                        'COMPANY',
                                                                                        'CO_NAME',
                                                                                        'CO_ADD1',
                                                                                        'CO_ADD2',
                                                                                        'CO_ADD3',
                                                                                        'CO_CITY',
                                                                                        'CO_NAME_SECONDARY');
   s9t_pkg.save_obj(L_file);
END INIT_S9T;
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_COMPHEAD.CREATE_S9T';
   L_file    S9T_FILE;
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N'then
      POPULATE_COMPHEAD(O_file_id);
      POPULATE_COMPHEAD_TL(O_file_id);
      commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------
PROCEDURE PROCESS_S9T_COMPHEAD (I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id   IN   SVC_COMPHEAD.PROCESS_ID%TYPE ) IS

   TYPE svc_COMPHEAD_col_typ IS TABLE OF SVC_COMPHEAD%ROWTYPE;

   L_error            BOOLEAN              := FALSE;
   L_temp_rec         SVC_COMPHEAD%ROWTYPE;
   L_default_rec      SVC_COMPHEAD%ROWTYPE;
   L_process_id       SVC_COMPHEAD.process_id%TYPE;
   svc_comphead_col   svc_COMPHEAD_col_typ := NEW svc_comphead_col_typ();


   cursor C_MANDATORY_IND is
      select co_jurisdiction_code_mi,
             co_name_secondary_mi,
             co_post_mi,
             co_country_mi,
             co_state_mi,
             co_city_mi,
             co_add3_mi,
             co_add2_mi,
             co_add1_mi,
             co_name_mi,
             company_mi,
             1   as   dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_COMPHEAD.template_key
                 and wksht_key     ='COMPHEAD')
               PIVOT (MAX(mandatory) as mi FOR (column_key) in ( 'CO_JURISDICTION_CODE' as  CO_JURISDICTION_CODE,
                                                                 'CO_NAME_SECONDARY'    as  CO_NAME_SECONDARY,
                                                                 'CO_POST'              as  CO_POST,
                                                                 'CO_COUNTRY'           as  CO_COUNTRY,
                                                                 'CO_STATE'             as  CO_STATE,
                                                                 'CO_CITY'              as  CO_CITY,
                                                                 'CO_ADD3'              as  CO_ADD3,
                                                                 'CO_ADD2'              as  CO_ADD2,
                                                                 'CO_ADD1'              as  CO_ADD1,
                                                                 'CO_NAME'              as  CO_NAME,
                                                                 'COMPANY'              as  COMPANY,
                                                                  NULL                  as  dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMPHEAD';
   L_pk_columns    VARCHAR2(255)  := 'Company';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec in
   (select co_jurisdiction_code_dv,
           co_name_secondary_dv,
           co_post_dv,
           co_country_dv,
           co_state_dv,
           co_city_dv,
           co_add3_dv,
           co_add2_dv,
           co_add1_dv,
           co_name_dv,
           company_dv,
           NULL   as   dummy
      from
         (select column_key,
                 default_value
            from s9t_tmpL_cols_def
           where template_key  = CORESVC_COMPHEAD.template_key
             and wksht_key    = 'COMPHEAD')
         PIVOT (MAX(default_value) as dv FOR (column_key) in ( 'co_jurisdiction_code' as  co_jurisdiction_code,
                                                               'CO_NAME_SECONDARY'    as  CO_NAME_SECONDARY,
                                                               'CO_POST'              as  CO_POST,
                                                               'CO_COUNTRY'           as  CO_COUNTRY,
                                                               'CO_STATE'             as  CO_STATE,
                                                               'CO_CITY'              as  CO_CITY,
                                                               'CO_ADD3'              as  CO_ADD3,
                                                               'CO_ADD2'              as  CO_ADD2,
                                                               'CO_ADD1'              as  CO_ADD1,
                                                               'CO_NAME'              as  CO_NAME,
                                                               'COMPANY'              as  COMPANY,
                                                                NULL                  as  dummy )))   LOOP
   BEGIN
      L_default_rec.co_jurisdiction_code := rec.co_jurisdiction_code_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',NULL,
                         'CO_JURISDICTION_CODE',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CO_NAME_SECONDARY := rec.co_name_secondary_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',NULL,
                         'CO_NAME_SECONDARY',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CO_POST := rec.co_post_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'CO_POST',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_country := rec.co_country_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'CO_COUNTRY',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_state := rec.co_state_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'CO_STATE',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_city := rec.co_city_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'CO_CITY',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_add3 := rec.co_add3_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'CO_ADD3',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_add2 := rec.co_add2_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'CO_ADD2',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CO_ADD1 := rec.co_add1_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'CO_ADD1',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CO_NAME := rec.co_name_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'CO_NAME',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.COMPANY := rec.company_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPHEAD',
                          NULL,
                         'COMPANY',
                          NULL,
                         'INV_DEFAULT');
   END;

   END LOOP;

   open C_mandatory_ind;
   fetch C_mandatory_ind
   into L_mi_rec;
   close C_mandatory_ind;
   FOR  rec in
    (select r.get_cell(COMPHEAD$ACTION)                as  Action,
            r.get_cell(COMPHEAD$CO_JURISDICTION_CODE)  as  co_jurisdiction_code,
            r.get_cell(COMPHEAD$CO_NAME_SECONDARY)     as  CO_NAME_SECONDARY,
            r.get_cell(COMPHEAD$CO_POST)               as  CO_POST,
            r.get_cell(COMPHEAD$CO_COUNTRY)            as  CO_COUNTRY,
            r.get_cell(COMPHEAD$CO_STATE)              as  CO_STATE,
            r.get_cell(COMPHEAD$CO_CITY)               as  CO_CITY,
            r.get_cell(COMPHEAD$CO_ADD3)               as  CO_ADD3,
            r.get_cell(COMPHEAD$CO_ADD2)               as  CO_ADD2,
            r.get_cell(COMPHEAD$CO_ADD1)               as  CO_ADD1,
            r.get_cell(COMPHEAD$CO_NAME)               as  CO_NAME,
            r.get_cell(COMPHEAD$COMPANY)               as  COMPANY,
            r.get_row_seq()                            as  row_seq
       from s9t_folder sf,
            TABLE (sf.s9t_file_obj.sheets) ss,
            TABLE (ss.s9t_rows) r
      where sf.file_id    = I_file_id
        and ss.sheet_name = sheet_name_trans(COMPHEAD_sheet))   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          comphead_sheet,
                          rec.row_seq,
                          action_column,
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.co_jurisdiction_code := rec.co_jurisdiction_code;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          comphead_sheet,
                          rec.row_seq,
                         'co_jurisdiction_code',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.CO_NAME_SECONDARY := rec.co_name_secondary;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          comphead_sheet,
                          rec.row_seq,
                         'CO_NAME_SECONDARY',
                          SQLCODE,
                          SQLERRM);
            L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_POST := rec.CO_POST;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,
                         'CO_POST',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_COUNTRY := rec.CO_COUNTRY;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,
                         'CO_COUNTRY',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_STATE := rec.CO_STATE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,
                         'CO_STATE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_CITY := rec.CO_CITY;
   EXCEPTION
      when OTHERS then
          WRITE_S9T_ERROR( I_file_id,
                           COMPHEAD_sheet,
                           rec.row_seq,
                          'CO_CITY',
                           SQLCODE,
                           SQLERRM);
          L_error := TRUE;
   END;

   BEGIN
       L_temp_rec.CO_ADD3 := rec.CO_ADD3;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,
                         'CO_ADD3',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_ADD2 := rec.CO_ADD2;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,
                         'CO_ADD2',
                          SQLCODE,
                          SQLERRM);
            L_error := TRUE;
      END;

   BEGIN
      L_temp_rec.CO_ADD1 := rec.CO_ADD1;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,
                         'CO_ADD1',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_NAME := rec.CO_NAME;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,
                         'CO_NAME',
                          SQLCODE,
                          SQLERRM);
        L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.COMPANY := rec.COMPANY;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,
                         'COMPANY',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   if rec.action = CORESVC_COMPHEAD.action_new then
      L_temp_rec.co_jurisdiction_code := NVL( L_temp_rec.co_jurisdiction_code,
                                              L_default_rec.co_jurisdiction_code);
      L_temp_rec.CO_NAME_SECONDARY    := NVL( L_temp_rec.CO_NAME_SECONDARY,
                                              L_default_rec.CO_NAME_SECONDARY);
      L_temp_rec.CO_POST              := NVL( L_temp_rec.CO_POST,
                                              L_default_rec.CO_POST);
      L_temp_rec.CO_COUNTRY           := NVL( L_temp_rec.CO_COUNTRY,
                                              L_default_rec.CO_COUNTRY);
      L_temp_rec.CO_STATE             := NVL( L_temp_rec.CO_STATE,
                                              L_default_rec.CO_STATE);
      L_temp_rec.CO_CITY              := NVL( L_temp_rec.CO_CITY,
                                              L_default_rec.CO_CITY);
      L_temp_rec.CO_ADD3              := NVL( L_temp_rec.CO_ADD3,
                                              L_default_rec.CO_ADD3);
      L_temp_rec.CO_ADD2              := NVL( L_temp_rec.CO_ADD2,
                                              L_default_rec.CO_ADD2);
      L_temp_rec.CO_ADD1              := NVL( L_temp_rec.CO_ADD1,
                                              L_default_rec.CO_ADD1);
      L_temp_rec.CO_NAME              := NVL( L_temp_rec.CO_NAME,
                                              L_default_rec.CO_NAME);
      L_temp_rec.COMPANY              := NVL( L_temp_rec.COMPANY,
                                              L_default_rec.COMPANY);
   end if;

   if NOT ( L_temp_rec.COMPANY is NOT NULL
      and 1 = 1 ) then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_sheet,
                          rec.row_seq,

                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
   end if;

   if NOT L_error then
      svc_COMPHEAD_col.EXTEND();
      svc_COMPHEAD_col(svc_COMPHEAD_col.COUNT()) := L_temp_rec;
   end if;

   END LOOP;

BEGIN
   forall i in 1..svc_comphead_col.COUNT SAVE EXCEPTIONS
      merge into svc_comphead st
      using (select (case
                        when l_mi_rec.co_jurisdiction_code_mi = 'N' and
                             svc_comphead_col(i).action       = CORESVC_COMPHEAD.action_mod and
                             s1.co_jurisdiction_code   is null then
                             mt.co_jurisdiction_code
                        else s1.co_jurisdiction_code
                        end) as co_jurisdiction_code,
                    (case
                        when l_mi_rec.co_name_secondary_mi = 'N' and
                             svc_comphead_col(i).action    = CORESVC_COMPHEAD.action_mod and
                             s1.co_name_secondary  is null then mt.co_name_secondary
                        else s1.co_name_secondary
                        end) as co_name_secondary,
                    (case
                        when l_mi_rec.co_post_mi        = 'N' and
                             svc_comphead_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_post is null then
                             mt.co_post
                        else s1.co_post
                        end) as co_post,
                    (case
                        when l_mi_rec.co_country_mi     = 'N' and
                             svc_comphead_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_country is null then
                             mt.co_country
                        else s1.co_country
                        end) as co_country,
                    (case
                        when l_mi_rec.co_state_mi       = 'N' and
                             svc_comphead_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_state is null then
                             mt.co_state
                        else s1.co_state
                        end) as co_state,
                    (case
                        when l_mi_rec.co_city_mi        = 'N' and
                             svc_comphead_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_city is null then
                             mt.co_city
                        else s1.co_city
                        end) as co_city,
                    (case
                        when l_mi_rec.co_add3_mi        = 'N' and
                             svc_comphead_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_add3 is null then
                             mt.co_add3
                        else s1.co_add3
                        end) as co_add3,
                    (case
                        when l_mi_rec.co_add2_mi        = 'N' and
                             svc_comphead_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_add2 is null then
                             mt.co_add2
                        else s1.co_add2
                        end) as co_add2,
                    (case
                        when l_mi_rec.co_add1_mi        = 'N' and
                             svc_comphead_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_add1 is null then
                             mt.co_add1
                        else s1.co_add1
                        end) as co_add1,
                   (case
                       when l_mi_rec.co_name_mi         = 'N' and
                            svc_comphead_col(i).action  = CORESVC_COMPHEAD.action_mod and
                            s1.co_name is null then
                            mt.co_name
                      else s1.co_name
                      end) as co_name,
                   (case
                       when l_mi_rec.company_mi         = 'N' and
                            svc_comphead_col(i).action  = CORESVC_COMPHEAD.action_mod and
                            s1.company is null then
                            mt.company
                       else s1.company
                       end)   as   company,
                       NULL   as   dummy
               from (select svc_comphead_col(i).co_jurisdiction_code as co_jurisdiction_code,
                            svc_comphead_col(i).co_name_secondary    as co_name_secondary,
                            svc_comphead_col(i).co_post              as co_post,
                            svc_comphead_col(i).co_country           as co_country,
                            svc_comphead_col(i).co_state             as co_state,
                            svc_comphead_col(i).co_city              as co_city,
                            svc_comphead_col(i).co_add3              as co_add3,
                            svc_comphead_col(i).co_add2              as co_add2,
                            svc_comphead_col(i).co_add1              as co_add1,
                            svc_comphead_col(i).co_name              as co_name,
                            svc_comphead_col(i).company              as company,
                            NULL   as   dummy
                       from dual  )  s1 , COMPHEAD mt
                      where mt.COMPANY (+) = s1.COMPANY
                        and              1 = 1 ) sq
         on (st.company = sq.company
         and svc_comphead_col(i).action in (coresvc_comphead.action_mod,coresvc_comphead.action_del))
         when matched then
            update
               set process_id           = svc_comphead_col(i).process_id ,
                   chunk_id             = svc_comphead_col(i).chunk_id ,
                   row_seq              = svc_comphead_col(i).row_seq ,
                   action               = svc_comphead_col(i).action,
                   process$status       = svc_comphead_col(i).process$status ,
                   co_name_secondary    = sq.co_name_secondary ,
                   co_city              = sq.co_city ,
                   co_country           = sq.co_country ,
                   co_add1              = sq.co_add1 ,
                   co_jurisdiction_code = sq.co_jurisdiction_code ,
                   co_state             = sq.co_state ,
                   co_name              = sq.co_name ,
                   co_add2              = sq.co_add2 ,
                   co_post              = sq.co_post ,
                   co_add3              = sq.co_add3 ,
                   create_id            = svc_comphead_col(i).create_id ,
                   create_datetime      = svc_comphead_col(i).create_datetime ,
                   last_upd_id          = svc_comphead_col(i).last_upd_id ,
                   last_upd_datetime    = svc_comphead_col(i).last_upd_datetime
         when not matched then
            insert( process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    co_jurisdiction_code ,
                    co_name_secondary ,
                    co_post ,
                    co_country ,
                    co_state ,
                    co_city ,
                    co_add3 ,
                    co_add2 ,
                    co_add1 ,
                    co_name ,
                    company ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime
                    )
            values( svc_comphead_col(i).process_id ,
                    svc_comphead_col(i).chunk_id ,
                    svc_comphead_col(i).row_seq ,
                    svc_comphead_col(i).action ,
                    svc_comphead_col(i).process$status ,
                    sq.co_jurisdiction_code ,
                    sq.co_name_secondary ,
                    sq.co_post ,
                    sq.co_country ,
                    sq.co_state ,
                    sq.co_city ,
                    sq.co_add3 ,
                    sq.co_add2 ,
                    sq.co_add1 ,
                    sq.co_name ,
                    sq.company ,
                    svc_comphead_col(i).create_id ,
                    svc_comphead_col(i).create_datetime ,
                    svc_comphead_col(i).last_upd_id ,
                    svc_comphead_col(i).last_upd_datetime
                     );
EXCEPTION
   when DML_ERRORS then
      FOR i in 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            COMPHEAD_sheet,
                            svc_comphead_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_COMPHEAD;
------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------
PROCEDURE PROCESS_S9T_COMPHEAD_TL (I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_COMPHEAD_TL.PROCESS_ID%TYPE ) IS

   TYPE svc_comphead_tl_col_typ IS TABLE OF SVC_COMPHEAD_TL%ROWTYPE;

   L_error               BOOLEAN              := FALSE;
   L_temp_rec            SVC_COMPHEAD_TL%ROWTYPE;
   L_default_rec         SVC_COMPHEAD_TL%ROWTYPE;
   L_process_id          SVC_COMPHEAD_TL.PROCESS_ID%TYPE;
   svc_comphead_tl_col   SVC_COMPHEAD_TL_COL_TYP := NEW SVC_COMPHEAD_TL_COL_TYP();

   cursor C_MANDATORY_IND is
      select co_name_secondary_mi,
             co_city_mi,
             co_add3_mi,
             co_add2_mi,
             co_add1_mi,
             co_name_mi,
             company_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_COMPHEAD.template_key
                 and wksht_key     ='COMPHEAD_TL')
               PIVOT (MAX(mandatory) as mi FOR (column_key) in ( 'CO_NAME_SECONDARY'    as  CO_NAME_SECONDARY,
                                                                 'CO_CITY'              as  CO_CITY,
                                                                 'CO_ADD3'              as  CO_ADD3,
                                                                 'CO_ADD2'              as  CO_ADD2,
                                                                 'CO_ADD1'              as  CO_ADD1,
                                                                 'CO_NAME'              as  CO_NAME,
                                                                 'COMPANY'              as  COMPANY,
                                                                 'LANG'                 as  LANG));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMPHEAD_TL';
   L_pk_columns    VARCHAR2(255)  := 'Company, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec in
   (select co_name_secondary_dv,
           co_city_dv,
           co_add3_dv,
           co_add2_dv,
           co_add1_dv,
           co_name_dv,
           company_dv,
           lang_dv
      from
         (select column_key,
                 default_value
            from s9t_tmpl_cols_def
           where template_key  = CORESVC_COMPHEAD.template_key
             and wksht_key    = 'COMPHEAD_TL')
         PIVOT (MAX(default_value) as dv FOR (column_key) in ( 'CO_NAME_SECONDARY'    as  CO_NAME_SECONDARY,
                                                               'CO_CITY'              as  CO_CITY,
                                                               'CO_ADD3'              as  CO_ADD3,
                                                               'CO_ADD2'              as  CO_ADD2,
                                                               'CO_ADD1'              as  CO_ADD1,
                                                               'CO_NAME'              as  CO_NAME,
                                                               'COMPANY'              as  COMPANY,
                                                               'LANG'                 as  LANG )))   LOOP

   BEGIN
      L_default_rec.co_name_secondary := rec.co_name_secondary_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          NULL,
                         'CO_NAME_SECONDARY',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_city := rec.co_city_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          NULL,
                         'CO_CITY',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_add3 := rec.co_add3_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          NULL,
                         'CO_ADD3',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_add2 := rec.co_add2_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          NULL,
                         'CO_ADD2',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_add1 := rec.co_add1_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          NULL,
                         'CO_ADD1',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.co_name := rec.co_name_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          NULL,
                         'CO_NAME',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.company := rec.company_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          NULL,
                         'COMPANY',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.lang := rec.lang_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          NULL,
                         'LANG',
                          NULL,
                         'INV_DEFAULT');
   END;

   END LOOP;

   open C_mandatory_ind;
   fetch C_mandatory_ind
   into L_mi_rec;
   close C_mandatory_ind;
   FOR  rec in
    (select r.get_cell(COMPHEAD_TL$ACTION)                as  Action,
            r.get_cell(COMPHEAD_TL$CO_NAME_SECONDARY)     as  CO_NAME_SECONDARY,
            r.get_cell(COMPHEAD_TL$CO_CITY)               as  CO_CITY,
            r.get_cell(COMPHEAD_TL$CO_ADD3)               as  CO_ADD3,
            r.get_cell(COMPHEAD_TL$CO_ADD2)               as  CO_ADD2,
            r.get_cell(COMPHEAD_TL$CO_ADD1)               as  CO_ADD1,
            r.get_cell(COMPHEAD_TL$CO_NAME)               as  CO_NAME,
            r.get_cell(COMPHEAD_TL$COMPANY)               as  COMPANY,
            r.get_cell(COMPHEAD_TL$LANG)                  as  LANG,
            r.get_row_seq()                               as  row_seq
       from s9t_folder sf,
            TABLE (sf.s9t_file_obj.sheets) ss,
            TABLE (ss.s9t_rows) r
      where sf.file_id    = I_file_id
        and ss.sheet_name = sheet_name_trans(COMPHEAD_tl_sheet))   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          rec.row_seq,
                          action_column,
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_NAME_SECONDARY := rec.co_name_secondary;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          rec.row_seq,
                         'CO_NAME_SECONDARY',
                          SQLCODE,
                          SQLERRM);
            L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_CITY := rec.CO_CITY;
   EXCEPTION
      when OTHERS then
          WRITE_S9T_ERROR( I_file_id,
                           COMPHEAD_TL_sheet,
                           rec.row_seq,
                          'CO_CITY',
                           SQLCODE,
                           SQLERRM);
          L_error := TRUE;
   END;

   BEGIN
       L_temp_rec.CO_ADD3 := rec.CO_ADD3;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          rec.row_seq,
                         'CO_ADD3',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_ADD2 := rec.CO_ADD2;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          rec.row_seq,
                         'CO_ADD2',
                          SQLCODE,
                          SQLERRM);
            L_error := TRUE;
      END;

   BEGIN
      L_temp_rec.CO_ADD1 := rec.CO_ADD1;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          rec.row_seq,
                         'CO_ADD1',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CO_NAME := rec.CO_NAME;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          rec.row_seq,
                         'CO_NAME',
                          SQLCODE,
                          SQLERRM);
        L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.COMPANY := rec.COMPANY;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          rec.row_seq,
                         'COMPANY',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.lang := rec.lang;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          COMPHEAD_TL_sheet,
                          rec.row_seq,
                         'LANG',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   if rec.action = CORESVC_COMPHEAD.action_new then
      L_temp_rec.CO_NAME_SECONDARY    := NVL( L_temp_rec.CO_NAME_SECONDARY, L_default_rec.CO_NAME_SECONDARY);
      L_temp_rec.CO_CITY              := NVL( L_temp_rec.CO_CITY, L_default_rec.CO_CITY);
      L_temp_rec.CO_ADD3              := NVL( L_temp_rec.CO_ADD3, L_default_rec.CO_ADD3);
      L_temp_rec.CO_ADD2              := NVL( L_temp_rec.CO_ADD2, L_default_rec.CO_ADD2);
      L_temp_rec.CO_ADD1              := NVL( L_temp_rec.CO_ADD1, L_default_rec.CO_ADD1);
      L_temp_rec.CO_NAME              := NVL( L_temp_rec.CO_NAME, L_default_rec.CO_NAME);
      L_temp_rec.COMPANY              := NVL( L_temp_rec.COMPANY, L_default_rec.COMPANY);
      L_temp_rec.LANG                 := NVL( L_temp_rec.LANG, L_default_rec.LANG);
   end if;

   if NOT ( L_temp_rec.COMPANY is NOT NULL and L_temp_rec.LANG is NOT NULL) then
      WRITE_S9T_ERROR( I_file_id,
                       COMPHEAD_TL_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
      L_error := TRUE;
   end if;

   if NOT L_error then
      svc_COMPHEAD_TL_col.EXTEND();
      svc_COMPHEAD_TL_col(svc_COMPHEAD_TL_col.COUNT()) := L_temp_rec;
   end if;

   END LOOP;

BEGIN
   forall i in 1..svc_comphead_tl_col.COUNT SAVE EXCEPTIONS
      merge into svc_comphead_tl st
      using (select (case
                        when l_mi_rec.co_name_secondary_mi = 'N' and
                             svc_comphead_tl_col(i).action    = CORESVC_COMPHEAD.action_mod and
                             s1.co_name_secondary  is null then mt.co_name_secondary
                        else s1.co_name_secondary
                        end) as co_name_secondary,
                    (case
                        when l_mi_rec.co_city_mi        = 'N' and
                             svc_comphead_tl_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_city is null then
                             mt.co_city
                        else s1.co_city
                        end) as co_city,
                    (case
                        when l_mi_rec.co_add3_mi        = 'N' and
                             svc_comphead_tl_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_add3 is null then
                             mt.co_add3
                        else s1.co_add3
                        end) as co_add3,
                    (case
                        when l_mi_rec.co_add2_mi        = 'N' and
                             svc_comphead_tl_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_add2 is null then
                             mt.co_add2
                        else s1.co_add2
                        end) as co_add2,
                    (case
                        when l_mi_rec.co_add1_mi        = 'N' and
                             svc_comphead_tl_col(i).action = CORESVC_COMPHEAD.action_mod and
                             s1.co_add1 is null then
                             mt.co_add1
                        else s1.co_add1
                        end) as co_add1,
                   (case
                       when l_mi_rec.co_name_mi         = 'N' and
                            svc_comphead_tl_col(i).action  = CORESVC_COMPHEAD.action_mod and
                            s1.co_name is null then
                            mt.co_name
                      else s1.co_name
                      end) as co_name,
                   (case
                       when l_mi_rec.company_mi         = 'N' and
                            svc_comphead_tl_col(i).action  = CORESVC_COMPHEAD.action_mod and
                            s1.company is null then
                            mt.company
                       else s1.company
                       end)   as   company,
                   (case
                       when l_mi_rec.lang_mi         = 'N' and
                            svc_comphead_tl_col(i).action  = CORESVC_COMPHEAD.action_mod and
                            s1.lang is null then
                            mt.lang
                       else s1.lang
                       end)   as   lang
               from (select svc_comphead_tl_col(i).co_name_secondary    as co_name_secondary,
                            svc_comphead_tl_col(i).co_city              as co_city,
                            svc_comphead_tl_col(i).co_add3              as co_add3,
                            svc_comphead_tl_col(i).co_add2              as co_add2,
                            svc_comphead_tl_col(i).co_add1              as co_add1,
                            svc_comphead_tl_col(i).co_name              as co_name,
                            svc_comphead_tl_col(i).company              as company,
                            svc_comphead_tl_col(i).lang                 as lang
                       from dual  )  s1 ,
                      COMPHEAD_TL mt
              where mt.lang (+) = s1.lang
                and mt.company (+) = s1.company) sq
         on (st.lang = sq.lang
         and st.company = sq.company
         and svc_comphead_tl_col(i).action in (coresvc_comphead.action_mod,coresvc_comphead.action_del))
         when matched then
            update
               set process_id           = svc_comphead_tl_col(i).process_id ,
                   chunk_id             = svc_comphead_tl_col(i).chunk_id ,
                   row_seq              = svc_comphead_tl_col(i).row_seq ,
                   action               = svc_comphead_tl_col(i).action,
                   process$status       = svc_comphead_tl_col(i).process$status ,
                   co_name              = sq.co_name ,
                   co_add1              = sq.co_add1 ,
                   co_add2              = sq.co_add2 ,
                   co_add3              = sq.co_add3 ,
                   co_city              = sq.co_city ,
                   co_name_secondary    = sq.co_name_secondary
         when not matched then
            insert( process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    lang,
                    company,
                    co_name,
                    co_add1,
                    co_add2,
                    co_add3,
                    co_city,
                    co_name_secondary )
            values( svc_comphead_tl_col(i).process_id ,
                    svc_comphead_tl_col(i).chunk_id ,
                    svc_comphead_tl_col(i).row_seq ,
                    svc_comphead_tl_col(i).action ,
                    svc_comphead_tl_col(i).process$status ,
                    sq.lang ,
                    sq.company ,
                    sq.co_name ,
                    sq.co_add1 ,
                    sq.co_add2 ,
                    sq.co_add3 ,
                    sq.co_city ,
                    sq.co_name_secondary );
EXCEPTION
   when DML_ERRORS then
      FOR i in 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            COMPHEAD_TL_sheet,
                            svc_comphead_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_COMPHEAD_TL;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   rtk_errors.rtk_text%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       s9t_folder.file_id%TYPE,
                     I_process_id      IN       NUMBER )
return BOOLEAN IS
   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);

   L_program         VARCHAR2(64) := 'CORESVC_COMPHEAD.PROCESS_S9T';
   L_file            s9t_file;
   L_sheets          S9T_PKG.names_map_typ;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
   MAX_CHAR      EXCEPTION;
   PRAGMA        EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   commit;
   s9t_pkg.ods2obj(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE   then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.sheet_trans(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_COMPHEAD(I_file_id,
                           I_process_id);
      PROCESS_S9T_COMPHEAD_TL(I_file_id,
                              I_process_id);
   end if;

   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i in 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   commit;

   return TRUE;
EXCEPTION

   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values lp_s9t_errors_tab(i);
         update svc_process_tracker
            set status  = 'PE',
                file_id = I_file_id
          where process_id = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
          values Lp_s9t_errors_tab(i);

         update svc_process_tracker
            set status = 'PE',
                file_id  = I_file_id
          where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPHEAD_INS( O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_comphead_temp_rec  IN      COMPHEAD%ROWTYPE )
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_COMPHEAD.EXEC_COMPHEAD_INS';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPHEAD';

BEGIN

   insert into comphead
        values I_comphead_temp_rec;


   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPHEAD_INS;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION  EXEC_COMPHEAD_UPD(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_comphead_temp_rec  IN      COMPHEAD%ROWTYPE )
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                      := 'CORESVC_COMPHEAD.EXEC_COMPHEAD_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPHEAD';

   cursor C_COMPHEAD_LOCK is
      select 'X'
        from COMPHEAD
       where COMPANY = I_comphead_temp_rec.COMPANY
       for update nowait;
BEGIN
   open C_COMPHEAD_LOCK;
   close C_COMPHEAD_LOCK;

   update COMPHEAD
      set row     = I_comphead_temp_rec
    where COMPANY = I_comphead_temp_rec.COMPANY;


   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'COMPHEAD',
                                                                I_comphead_temp_rec.COMPANY,
                                                                NULL);

      return FALSE;

   when OTHERS then
      if C_COMPHEAD_LOCK%ISOPEN then
         close C_COMPHEAD_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPHEAD_UPD;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPHEAD_DEL(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_comphead_temp_rec  IN      COMPHEAD%ROWTYPE )
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                      := 'CORESVC_COMPHEAD.EXEC_COMPHEAD_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPHEAD';

   cursor C_COMPHEAD_LOCK is
      select 'x'
        from comphead
       where company = I_comphead_temp_rec.company
       for update nowait;

   cursor C_COMPHEAD_TL_LOCK is
      select 'x'
        from comphead_tl
       where company = I_comphead_temp_rec.company
       for update nowait;

BEGIN
   open C_COMPHEAD_TL_LOCK;
   close C_COMPHEAD_TL_LOCK;
   
   delete
     from comphead_tl
    where company = I_comphead_temp_rec.company;

   open C_COMPHEAD_LOCK;
   close C_COMPHEAD_LOCK;

   delete
     from comphead
    where company = I_comphead_temp_rec.company;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'COMPHEAD',
                                                                I_comphead_temp_rec.COMPANY,
                                                                NULL);

      return FALSE;

   when OTHERS then
      if C_COMPHEAD_LOCK%ISOPEN then
         close C_COMPHEAD_LOCK;
      end if;

      if C_COMPHEAD_TL_LOCK%ISOPEN then
         close C_COMPHEAD_TL_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPHEAD_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPHEAD_TL_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_comphead_tl_ins_tab    IN       COMPHEAD_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COMPHEAD.EXEC_COMPHEAD_TL_INS';

BEGIN
   if I_comphead_tl_ins_tab is NOT NULL and I_comphead_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_comphead_tl_ins_tab.COUNT()
         insert into comphead_tl
              values I_comphead_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_COMPHEAD_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPHEAD_TL_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_comphead_tl_upd_tab   IN       COMPHEAD_TL_TAB,
                              I_comphead_tl_upd_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_COMPHEAD_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_COMPHEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_COMPHEAD_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMPHEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_COMPHEAD_TL_UPD(I_company COMPHEAD_TL.COMPANY%TYPE,
                                 I_lang    COMPHEAD_TL.LANG%TYPE) is
      select 'x'
        from comphead_tl
       where company = I_company
         and lang = I_lang
         for update nowait;

BEGIN
   if I_comphead_tl_upd_tab is NOT NULL and I_comphead_tl_upd_tab.count > 0 then
      for i in I_comphead_tl_upd_tab.FIRST..I_comphead_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_comphead_tl_upd_tab(i).lang);
            L_key_val2 := 'Company: '||to_char(I_comphead_tl_upd_tab(i).company);
            open C_LOCK_COMPHEAD_TL_UPD(I_comphead_tl_upd_tab(i).company,
                                        I_comphead_tl_upd_tab(i).lang);
            close C_LOCK_COMPHEAD_TL_UPD;
            
            update comphead_tl
               set co_name = I_comphead_tl_upd_tab(i).co_name,
                   co_add1 = I_comphead_tl_upd_tab(i).co_add1,
                   co_add2 = I_comphead_tl_upd_tab(i).co_add2,
                   co_add3 = I_comphead_tl_upd_tab(i).co_add3,
                   co_city = I_comphead_tl_upd_tab(i).co_city,
                   co_name_secondary = I_comphead_tl_upd_tab(i).co_name_secondary,
                   last_update_id = I_comphead_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_comphead_tl_upd_tab(i).last_update_datetime
             where lang = I_comphead_tl_upd_tab(i).lang
               and company = I_comphead_tl_upd_tab(i).company;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_COMPHEAD_TL',
                           I_comphead_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_COMPHEAD_TL_UPD%ISOPEN then
         close C_LOCK_COMPHEAD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_COMPHEAD_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPHEAD_TL_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_comphead_tl_del_tab   IN       COMPHEAD_TL_TAB,
                              I_comphead_tl_del_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_COMPHEAD_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_COMPHEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COMPHEAD.EXEC_COMPHEAD_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMPHEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_COMPHEAD_TL_DEL(I_company  COMPHEAD_TL.COMPANY%TYPE,
                                 I_lang     COMPHEAD_TL.LANG%TYPE) is
      select 'x'
        from comphead_tl
       where company = I_company
         and lang = I_lang
         for update nowait;

BEGIN
   if I_comphead_tl_del_tab is NOT NULL and I_comphead_tl_del_tab.count > 0 then
      for i in I_comphead_tl_del_tab.FIRST..I_comphead_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_comphead_tl_del_tab(i).lang);
            L_key_val2 := 'Company: '||to_char(I_comphead_tl_del_tab(i).company);
            open C_LOCK_COMPHEAD_TL_DEL(I_comphead_tl_del_tab(i).company,
                                        I_comphead_tl_del_tab(i).lang);
            close C_LOCK_COMPHEAD_TL_DEL;
            
            delete comphead_tl
             where lang = I_comphead_tl_del_tab(i).lang
               and company = I_comphead_tl_del_tab(i).company;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_COMPHEAD_TL',
                           I_comphead_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_COMPHEAD_TL_DEL%ISOPEN then
         close C_LOCK_COMPHEAD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_COMPHEAD_TL_DEL;
--------------------------------------------------------------------------------------------- -
FUNCTION  VALIDATE_STATECOUNTRYJURIS(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_state             IN OUT  STATE.STATE%TYPE,
                                     O_state_desc        IN OUT  STATE.DESCRIPTION%TYPE,
                                     O_country_id        IN OUT  COUNTRY.COUNTRY_ID%TYPE,
                                     O_country_desc      IN OUT  COUNTRY.COUNTRY_DESC%TYPE,
                                     O_jurisdiction_code IN OUT  COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                                     O_jurisdiction_desc IN OUT  COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                     O_dup_exist         IN OUT  VARCHAR2,
                                     I_calling_module    IN      VARCHAR2,
                                     I_calling_context   IN      VARCHAR2 )
RETURN BOOLEAN IS
   L_calling_context VARCHAR2(12);
BEGIN
   O_dup_exist      := 'N';
   L_calling_context := I_calling_context;

   if ((L_calling_context = ADDRESS_SQL.COUNTRY_CON
      and O_country_id is NOT NULL)
      or (L_calling_context = ADDRESS_SQL.STATE_CON
      and O_state is NOT NULL )
      or (L_calling_context = ADDRESS_SQL.JURIS_CON
      and O_jurisdiction_code is NOT NULL ))   then

          if O_jurisdiction_code is NOT NULL then
             L_calling_context := ADDRESS_SQL.JURIS_CON;
          elsif O_state is NOT NULL then
                L_calling_context := ADDRESS_SQL.STATE_CON;
          end if;

          if ADDRESS_SQL.CHECK_ADDR(
                                     O_error_message,
                                     O_country_id,
                                     O_country_desc,
                                     O_state,
                                     O_state_desc,
                                     O_jurisdiction_code,
                                     O_jurisdiction_desc,
                                     O_dup_exist,
                                     I_calling_module,
                                     L_calling_context) = FALSE then
             return FALSE;
          end if;

   else
      if I_calling_module    = ADDRESS_SQL.COUNTRY_CON then
         O_country_desc      := NULL;
         O_jurisdiction_code := NULL;
         O_jurisdiction_desc := NULL;
         O_state             := NULL;
         O_state_desc        := NULL;
      elsif I_calling_module = ADDRESS_SQL.STATE_CON then
         O_state_desc        := NULL;
         O_jurisdiction_code := NULL;
         O_jurisdiction_desc := NULL;
      elsif I_calling_module = ADDRESS_SQL.JURIS_CON then
         O_jurisdiction_desc := NULL;
      end if;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS THEN
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                             'CORESVC_COMPHEAD.VALIDATE_STATECOUNTRYJURIS',
                                              TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_STATECOUNTRYJURIS;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_COMPHEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error         IN OUT BOOLEAN,
                              O_rec           IN OUT C_SVC_COMPHEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_calling_module      VARCHAR2(12);
   L_calling_context     VARCHAR2(12);
   L_dup_exist           VARCHAR2(1)                :='N';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPHEAD';
   L_state_desc          STATE.DESCRIPTION%TYPE;
   L_country_desc        COUNTRY.COUNTRY_DESC%TYPE;
   L_jurisdiction_desc   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
BEGIN
   if NVL(O_rec.base_co_state,'-1') <> NVL(O_rec.co_state,'-1')
      and O_rec.co_state  is  NULL
      and O_rec.co_jurisdiction_code is  NULL  then
      WRITE_ERROR( O_rec.process_id,
                   svc_admin_upld_er_seq.NEXTVAL,
                   O_rec.chunk_id,
                   L_table,
                   O_rec.row_seq,
                   'CO_STATE',
                   'MUST_ENTER_FIELD');
      O_error :=TRUE;
   end if;

   if O_rec.co_state is NULL and O_rec.co_jurisdiction_code is NOT NULL  then
      WRITE_ERROR( O_rec.process_id,
                   svc_admin_upld_er_seq.NEXTVAL,
                   O_rec.chunk_id,
                   L_table,
                   O_rec.row_seq,
                   'CO_STATE',
                   'MUST_ENTER_FIELD');
      O_error :=TRUE;
   end if;

   if ((O_rec.co_country is NOT NULL
      and O_rec.com_cnt_fk_rid is NOT NULL
      and O_rec.co_state is NOT NULL  )
      or (O_rec.co_country is NOT NULL
      and O_rec.com_cnt_fk_rid is NOT NULL
      and O_rec.co_jurisdiction_code is NOT NULL  ))   then

         L_calling_module  := ADDRESS_SQL.COUNTRY_CON;
         L_calling_context := ADDRESS_SQL.COUNTRY_CON;

         if(VALIDATE_STATECOUNTRYJURIS( O_error_message,
                                        O_rec.co_state,
                                        L_state_desc,
                                        O_rec.co_country ,
                                        L_country_desc,
                                        O_rec.co_jurisdiction_code,
                                        L_jurisdiction_desc ,
                                        L_dup_exist,
                                        L_calling_module,
                                        L_calling_context ) = FALSE )   then
            WRITE_ERROR( O_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         O_rec.chunk_id,
                         L_table,
                         O_rec.row_seq,
                         'Jurisdiction,State,Country',
                         O_error_message);
            O_error :=TRUE;
         end if;

         if  L_dup_exist = 'Y'   then
             WRITE_ERROR( O_rec.process_id,
                          svc_admin_upld_er_seq.NEXTVAL,
                          O_rec.chunk_id,
                          L_table,
                          O_rec.row_seq,
                          'Jurisdiction,State,Country',
                          O_error_message);
             O_error :=TRUE;
         end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPHEAD.PROCESS_VAL_COMPHEAD',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_COMPHEAD;
------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMPHEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id   IN   SVC_COMPHEAD.PROCESS_ID%TYPE,
                          I_chunk_id     IN   SVC_COMPHEAD.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error               BOOLEAN                           := FALSE;
   L_process_error       BOOLEAN                           := FALSE;
   L_comphead_temp_rec   COMPHEAD%ROWTYPE;
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPHEAD';
BEGIN
   FOR rec in C_SVC_COMPHEAD(I_process_id,I_chunk_id)
   LOOP

      if  rec.action is NULL
          or rec.action <> action_mod   then
          WRITE_ERROR( I_process_id,
                       svc_admin_upld_er_seq.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                      'ACTION',
                      'INV_ACT');
          L_error := TRUE;
      end if;

      if rec.action = action_mod   then

         if  rec.COMPANY is NOT NULL
             and rec.PK_COMPHEAD_rid is NULL   then
             WRITE_ERROR( I_process_id,
                          svc_admin_upld_er_seq.NEXTVAL,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                         'COMPANY',
                         'NO_RECORD');
             L_error := TRUE;
         end if;

         if rec.base_co_country <> NVL(rec.co_country,'-1')
            and rec.co_country is not null
            and rec.COM_CNT_FK_rid is NULL   then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'CO_COUNTRY',
                        'INV_COUNTRY');
            L_error := TRUE;
         end if;

         if rec.base_co_country <> NVL(rec.co_country,'-1')
            and NOT( rec.co_country is NOT NULL )   then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'CO_COUNTRY',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;

         if rec.base_co_city <> NVL(rec.co_city,'-1')
            and NOT(rec.co_city is NOT NULL)   then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'CO_CITY',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;

         if rec.base_co_add1 <> NVL(rec.co_add1, '-1')
            and NOT(rec.co_add1 is NOT NULL)   then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'CO_ADD1',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;

         if rec.base_co_name <> NVL(rec.co_name, '-1')
            and NOT(rec.co_name is NOT NULL)   then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'CO_NAME',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;

         if NOT(rec.COMPANY is NOT NULL)   then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'COMPANY',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;

         if(PROCESS_VAL_COMPHEAD(O_error_message,
                                 L_error,
                                 rec) = FALSE )   then
            WRITE_ERROR( rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         rec.chunk_id,
                         L_table,
                         rec.row_seq,
                         NULL,
                         O_error_message);
            L_error := TRUE;
         end if;

      end if;

      if NOT L_error then
         L_comphead_temp_rec.co_jurisdiction_code := rec.co_jurisdiction_code;
         L_comphead_temp_rec.co_name_secondary    := rec.co_name_secondary;
         L_comphead_temp_rec.co_post              := rec.co_post;
         L_comphead_temp_rec.co_country           := rec.co_country;
         L_comphead_temp_rec.co_state             := rec.co_state;
         L_comphead_temp_rec.co_city              := rec.co_city;
         L_comphead_temp_rec.co_add3              := rec.co_add3;
         L_comphead_temp_rec.co_add2              := rec.co_add2;
         L_comphead_temp_rec.co_add1              := rec.co_add1;
         L_comphead_temp_rec.co_name              := rec.co_name;
         L_comphead_temp_rec.company              := rec.company;
         L_comphead_temp_rec.create_id            := GET_USER;
         L_comphead_temp_rec.create_datetime      := SYSDATE;

         if rec.action = action_new   then
            if(EXEC_COMPHEAD_INS( O_error_message,
                                  L_comphead_temp_rec ) = FALSE ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod   then
            if(EXEC_COMPHEAD_UPD(O_error_message,
                                 L_comphead_temp_rec ) = FALSE ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del   then
            if(EXEC_COMPHEAD_DEL(O_error_message,
                                 L_comphead_temp_rec ) = FALSE ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPHEAD.VALIDATE_STATECOUNTRYJURIS',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COMPHEAD;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMPHEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_COMPHEAD_TL.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_COMPHEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64) := 'CORESVC_COMPHEAD.PROCESS_COMPHEAD_TL';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPHEAD_TL';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMPHEAD_TL';
   L_error                   BOOLEAN := FALSE;
   L_process_error           BOOLEAN := FALSE;
   L_comphead_tl_temp_rec    COMPHEAD_TL%ROWTYPE;
   L_comphead_tl_upd_rst     ROW_SEQ_TAB;
   L_comphead_tl_del_rst     ROW_SEQ_TAB;
   

   cursor C_SVC_COMPHEAD_TL(I_process_id NUMBER,
                            I_chunk_id NUMBER) is
      select pk_comphead_tl.rowid  as pk_comphead_tl_rid,
             fk_comphead.rowid     as fk_comphead_rid,
             fk_lang.rowid         as fk_lang_rid,
             st.lang,
             st.company,
             st.co_name,
             st.co_add1,
             st.co_add2,
             st.co_add3,
             st.co_city,
             st.co_name_secondary,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_comphead_tl  st,
             comphead         fk_comphead,
             comphead_tl      pk_comphead_tl,
             lang             fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.company     =  fk_comphead.company (+)
         and st.lang        =  pk_comphead_tl.lang (+)
         and st.company     =  pk_comphead_tl.company (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_COMPHEAD_TL is TABLE OF C_SVC_COMPHEAD_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_comphead_tl_tab        SVC_COMPHEAD_TL;

   L_comphead_tl_ins_tab         COMPHEAD_TL_tab         := NEW COMPHEAD_TL_tab();
   L_comphead_tl_upd_tab         COMPHEAD_TL_tab         := NEW COMPHEAD_TL_tab();
   L_comphead_tl_del_tab         COMPHEAD_TL_tab         := NEW COMPHEAD_TL_tab();

BEGIN
   if C_SVC_COMPHEAD_TL%ISOPEN then
      close C_SVC_COMPHEAD_TL;
   end if;

   open C_SVC_COMPHEAD_TL(I_process_id,
                       I_chunk_id);
   LOOP
      fetch C_SVC_COMPHEAD_TL bulk collect into L_svc_comphead_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_comphead_tl_tab.COUNT > 0 then
         FOR i in L_svc_comphead_tl_tab.FIRST..L_svc_comphead_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_comphead_tl_tab(i).action is NULL
               or L_svc_comphead_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_comphead_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_comphead_tl_tab(i).action = action_new
               and L_svc_comphead_tl_tab(i).pk_comphead_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_comphead_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_comphead_tl_tab(i).lang is NOT NULL
               and L_svc_comphead_tl_tab(i).company is NOT NULL
               and L_svc_comphead_tl_tab(i).pk_comphead_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_comphead_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_comphead_tl_tab(i).action = action_new
               and L_svc_comphead_tl_tab(i).company is NOT NULL
               and L_svc_comphead_tl_tab(i).fk_comphead_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     'COMPHEAD',
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_comphead_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_comphead_tl_tab(i).action = action_new
               and L_svc_comphead_tl_tab(i).lang is NOT NULL
               and L_svc_comphead_tl_tab(i).company is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_comphead_tl_tab(i).row_seq,
                            'COMPANY',
                            'INV_VALUE');
               L_error :=TRUE;
            end if;

            if L_svc_comphead_tl_tab(i).action = action_new
               and L_svc_comphead_tl_tab(i).lang is NOT NULL
               and L_svc_comphead_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_comphead_tl_tab(i).action IN (action_new, action_mod) and L_svc_comphead_tl_tab(i).co_name_secondary is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_comphead_tl_tab(i).row_seq,
                           'CO_NAME_SECONDARY',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_comphead_tl_tab(i).action IN (action_new, action_mod) and L_svc_comphead_tl_tab(i).co_name is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           'CO_NAME',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_comphead_tl_tab(i).action IN (action_new, action_mod) and L_svc_comphead_tl_tab(i).co_add1 is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           'CO_ADD1',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_comphead_tl_tab(i).action IN (action_new, action_mod) and L_svc_comphead_tl_tab(i).co_city is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           'CO_CITY',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_comphead_tl_tab(i).company is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           'COMPANY',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_comphead_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_comphead_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_comphead_tl_temp_rec.lang := L_svc_comphead_tl_tab(i).lang;
               L_comphead_tl_temp_rec.company := L_svc_comphead_tl_tab(i).company;
               L_comphead_tl_temp_rec.co_name := L_svc_comphead_tl_tab(i).co_name;
               L_comphead_tl_temp_rec.co_add1 := L_svc_comphead_tl_tab(i).co_add1;
               L_comphead_tl_temp_rec.co_add2 := L_svc_comphead_tl_tab(i).co_add2;
               L_comphead_tl_temp_rec.co_add3 := L_svc_comphead_tl_tab(i).co_add3;
               L_comphead_tl_temp_rec.co_city := L_svc_comphead_tl_tab(i).co_city;
               L_comphead_tl_temp_rec.co_name_secondary := L_svc_comphead_tl_tab(i).co_name_secondary;
               L_comphead_tl_temp_rec.create_datetime := SYSDATE;
               L_comphead_tl_temp_rec.create_id := GET_USER;
               L_comphead_tl_temp_rec.last_update_datetime := SYSDATE;
               L_comphead_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_comphead_tl_tab(i).action = action_new then
                  L_comphead_tl_ins_tab.extend;
                  L_comphead_tl_ins_tab(L_comphead_tl_ins_tab.count()) := L_comphead_tl_temp_rec;
               end if;

               if L_svc_comphead_tl_tab(i).action = action_mod then
                  L_comphead_tl_upd_tab.extend;
                  L_comphead_tl_upd_tab(L_comphead_tl_upd_tab.count()) := L_comphead_tl_temp_rec;
                  L_comphead_tl_upd_rst(L_comphead_tl_upd_tab.count()) := L_svc_comphead_tl_tab(i).row_seq;
               end if;

               if L_svc_comphead_tl_tab(i).action = action_del then
                  L_comphead_tl_del_tab.extend;
                  L_comphead_tl_del_tab(L_comphead_tl_del_tab.count()) := L_comphead_tl_temp_rec;
                  L_comphead_tl_del_rst(L_comphead_tl_del_tab.count()) := L_svc_comphead_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_COMPHEAD_TL%NOTFOUND;
   END LOOP;
   close C_SVC_COMPHEAD_TL;

   if EXEC_COMPHEAD_TL_INS(O_error_message,
                          L_comphead_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_COMPHEAD_TL_UPD(O_error_message,
                           L_comphead_tl_upd_tab,
                           L_comphead_tl_upd_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_COMPHEAD_TL_DEL(O_error_message,
                           L_comphead_tl_del_tab,
                           L_comphead_tl_del_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COMPHEAD_TL;
----------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_COMPHEAD.PROCESS_ID%TYPE) IS

BEGIN
   Delete
     from svc_comphead_tl
    where process_id= I_process_id;

   Delete
     from svc_comphead
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT NUMBER,
                  I_process_id    IN     NUMBER,
                  I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_COMPHEAD.PROCESS';
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_COMPHEAD(O_error_message,
                       I_process_id,
                       I_chunk_id) = FALSE   then
      return FALSE;
   end if;

   if PROCESS_COMPHEAD_TL(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE   then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   return TRUE;

EXCEPTION
   when OTHERS THEN
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
------------------------------------------------------------------------------------------------------
END CORESVC_COMPHEAD;
/
