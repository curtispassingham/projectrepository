
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XITEMRCLS_VALIDATE AS

-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQUIRED_FIELDS
-- Purpose      : This function will check all required fields
--                to ensure that they are NOT null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XItemRclsDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQUIRED_FIELDS
-- Purpose      : This function will check all required fields for the delete message
--                to ensure that they are NOT null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XItemRclsRef_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_FOR_EXISTING_RECLASS
-- Purpose      : Check whether the items exist on another reclassification event.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FOR_EXISTING_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_message         IN       "RIB_XItemRclsDesc_REC",
                                    I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: VALID_RECLASS_HIERARCHY
-- Purpose      : Check whether item can be reclassified to department, class, and subclass.
-------------------------------------------------------------------------------------------------------
FUNCTION VALID_RECLASS_HIERARCHY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_message         IN       "RIB_XItemRclsDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_EVENT_DATE
-- Purpose      : Check whether the date of the reclassification is at least one day after today.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_EVENT_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_event_date      IN       DATE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: ITEMRECLASS_EXISTS
-- Purpose      : Check whether reclass no exists for reclass head
--                and item no for reclass item.
-------------------------------------------------------------------------------------------------------
FUNCTION ITEMRECLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XItemRclsRef_REC",
                            I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_RECORD
-- Purpose      : This function will convert the RIB item reclassification object
--                into an item reclassification record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message     IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_itemreclass_rec   OUT    NOCOPY   RECLASS_SQL.RECLASS_REC,
                         I_message           IN              "RIB_XItemRclsDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_RECORD
-- Purpose      : This function will convert the RIB item reclassification object
--                into an item reclassification record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message     IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_itemreclass_rec   OUT    NOCOPY   RECLASS_SQL.RECLASS_REC,
                         I_message           IN              "RIB_XItemRclsRef_REC",
                         I_message_type      IN              VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_CONSIGN_DEPT
-- Purpose      : This function will check if the purchase type of the to_department is the same as that
--                of the current department.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CONSIGN_DEPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XItemRclsDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: APPROVED_ORDER_EXIST
-- Purpose      : This function will check is there any approved purchase order for an item/child/grandchild.
-------------------------------------------------------------------------------------------------------
FUNCTION APPROVED_ORDER_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE)
  
  RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message     IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_itemreclass_rec   OUT    NOCOPY   RECLASS_SQL.RECLASS_REC,
                       I_message           IN              "RIB_XItemRclsDesc_REC",
                       I_message_type      IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60)  := 'RMSSUB_XITEMRCLS_VALIDATE.CHECK_MESSAGE';

BEGIN

   if CHECK_REQUIRED_FIELDS(O_error_message,
                            I_message) = FALSE then
      return FALSE;
   end if;

   if CHECK_FOR_EXISTING_RECLASS(O_error_message,
                                 I_message,
                                 I_message_type) = FALSE then
      return FALSE;
   end if;

   if VALIDATE_EVENT_DATE(O_error_message,
                          I_message.reclass_date) = FALSE then
      return FALSE;
   end if;

   if VALID_RECLASS_HIERARCHY(O_error_message,
                              I_message) = FALSE then
      return FALSE;
   end if;

   if NOT CHECK_CONSIGN_DEPT(O_error_message,
                             I_message) then
      return FALSE;
   end if;

   if POPULATE_RECORD(O_error_message,
                      O_itemreclass_rec,
                      I_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message     IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_itemreclass_rec   OUT    NOCOPY   RECLASS_SQL.RECLASS_REC,
                       I_message           IN              "RIB_XItemRclsRef_REC",
                       I_message_type      IN              VARCHAR2)

   RETURN BOOLEAN IS

   L_program          VARCHAR2(60)  := 'RMSSUB_XITEMRCLS_VALIDATE.CHECK_MESSAGE';

BEGIN

   if CHECK_REQUIRED_FIELDS(O_error_message,
                            I_message,
                            I_message_type) = FALSE then
      return FALSE;
   end if;

   if ITEMRECLASS_EXISTS(O_error_message,
                         I_message,
                         I_message_type) = FALSE then
      return FALSE;
   end if;

   if POPULATE_RECORD(O_error_message,
                      O_itemreclass_rec,
                      I_message,
                      I_message_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message :=  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XItemRclsDesc_REC")
   RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'RMSSUB_XITEMRCLS_VALIDATE.CHECK_REQUIRED_FIELDS';

   L_api_item_rec      ITEM_ATTRIB_SQL.API_ITEM_REC;

   L_item              ITEM_MASTER.ITEM%TYPE;

   L_dept_level_orders   PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE;
   L_purch_type          DEPS.PURCHASE_TYPE%TYPE;
   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
BEGIN

   if I_message.reclass_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Reclass no',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.reclass_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Reclass Description',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.reclass_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Date',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.to_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Department',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.to_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Class',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   if I_message.to_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                            'Subclass',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   -- Check Dept Level
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_dept_level_orders := L_system_options_row.dept_level_orders;
   if I_message.xitemrclsdtl_tbl is NULL or
      I_message.xitemrclsdtl_tbl.COUNT < 1 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   FOR i in I_message.xitemrclsdtl_tbl.FIRST..I_message.xitemrclsdtl_tbl.LAST LOOP

      L_item := I_message.xitemrclsdtl_tbl(i).item;
      ---
      if ITEM_ATTRIB_SQL.GET_API_INFO(O_error_message,
                                      L_api_item_rec,
                                      L_item) = FALSE then
         return FALSE;
      end if;

      if L_api_item_rec.pack_ind = 'Y' and L_api_item_rec.simple_pack_ind = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('MUST_NOT_BE_SIMPLE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
      end if;

      if L_api_item_rec.item_level != 1 then
         O_error_message := SQL_LIB.CREATE_MSG('RECLASS_ITEM_LEVEL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if APPROVED_ORDER_EXIST (O_error_message,
                               L_api_item_rec.item) = FALSE then
          return FALSE;         
      end if;

      if L_dept_level_orders = 'Y' and
         (L_api_item_rec.pack_type = 'B' and
          L_api_item_rec.orderable_ind = 'Y' and
          L_api_item_rec.order_as_type = 'E') then
         O_error_message := SQL_LIB.CREATE_MSG('ORD_BUYR_PACK_COMP');
         return FALSE;
      end if;
                  
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XItemRclsRef_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(60) := 'RMSSUB_XITEMRCLS_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message_type = RMSSUB_XITEMRCLS.LP_del_type then
      if I_message.reclass_no is NULL AND
         I_message.reclass_date is NULL AND
         I_message.purge_all_ind is NULL then
         ---
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELDS_NULL',
                                               'reclass_no',
                                               'reclass_date',
                                               'purge_all_ind');
         return FALSE;
      elsif I_message.purge_all_ind is NOT NULL AND I_message.purge_all_ind = 'Y' AND
         (I_message.reclass_no is NOT NULL OR I_message.reclass_date is NOT NULL) then
         ---
         O_error_message := SQL_LIB.CREATE_MSG('ONE_FIELD_REQ',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      elsif I_message.reclass_no is NOT NULL AND I_message.reclass_date is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RECLASS_NUM_DATE_NULL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if I_message.purge_all_ind is NOT NULL AND
         UPPER(I_message.purge_all_ind) NOT in ('Y', 'N') then
         O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_YESNO',
                                               'I_message.purge_all_ind',
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEMRCLS.LP_dtl_del_type then
      if I_message.XItemRclsDtl_TBL is NULL OR I_message.XItemRclsDtl_TBL.COUNT < 1 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FOR_EXISTING_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_message         IN       "RIB_XItemRclsDesc_REC",
                                    I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(60) := 'RMSSUB_XITEMRCLS_VALIDATE.CHECK_FOR_EXISTING_RECLASS';
   L_exists      BOOLEAN;

BEGIN

   if I_message_type = RMSSUB_XITEMRCLS.LP_dtl_cre_type then
      if NOT RECLASS_SQL.EXIST(O_error_message,
                               L_exists,
                               I_message.reclass_no) then                                
         return FALSE;
      end if;
      
      if NOT L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('INV_RECLASS_NO',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;   
   end if;

   FOR i in I_message.xitemrclsdtl_tbl.FIRST..I_message.xitemrclsdtl_tbl.LAST LOOP
      if NOT RECLASS_SQL.ITEM_ON_EXISTING_RECLASS(O_error_message,
                                                  L_exists,
                                                  I_message.xitemrclsdtl_tbl(i).item,
                                                  NULL) then
         return FALSE;
      end if;
      ---
      if L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('RECLASS_ITEM_EXISTS',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FOR_EXISTING_RECLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION VALID_RECLASS_HIERARCHY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_message         IN       "RIB_XItemRclsDesc_REC")
   RETURN BOOLEAN IS

   L_program    VARCHAR2(60) := 'RMSSUB_XITEMRCLS_VALIDATE.VALID_RECLASS_HIERARCHY';
   L_exists     VARCHAR2(1)  := NULL;

   cursor C_RECLASS_EXISTS is
      select 'x'
        from V_MERCH_HIER
       where hierarchy_id = I_message.to_subclass
         and parent_hierarchy_id = I_message.to_class
         and grandparent_hierarchy_id = I_message.to_dept
         and (effective_date is NULL or
              effective_date <= TRUNC(I_message.reclass_date));

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_RECLASS_EXISTS',
                    'V_MERCH_HIER',
                    ' hierarchy_id: ' || TO_CHAR(I_message.to_subclass) ||
                    ' parent_hierarchy_id: ' || TO_CHAR(I_message.to_class) ||
                    ' grandparent_hierarchy_id: ' || TO_CHAR(I_message.to_dept));
   open C_RECLASS_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_RECLASS_EXISTS',
                    'V_MERCH_HIER',
                    ' hierarchy_id: ' || TO_CHAR(I_message.to_subclass) ||
                    ' parent_hierarchy_id: ' || TO_CHAR(I_message.to_class) ||
                    ' grandparent_hierarchy_id: ' || TO_CHAR(I_message.to_dept));
   fetch C_RECLASS_EXISTS into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_RECLASS_EXISTS',
                    'V_MERCH_HIER',
                    ' hierarchy_id: ' || TO_CHAR(I_message.to_subclass) ||
                    ' parent_hierarchy_id: ' || TO_CHAR(I_message.to_class) ||
                    ' grandparent_hierarchy_id: ' || TO_CHAR(I_message.to_dept));
   close C_RECLASS_EXISTS;

   if L_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_COMB',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;

END VALID_RECLASS_HIERARCHY;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_EVENT_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_event_date      IN       DATE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_ITEMRECLASS_VALIDATE.VALIDATE_EVENT_DATE';

BEGIN

   if TRUNC(I_event_date) < GET_VDATE then
      O_error_message :=  SQL_LIB.CREATE_MSG('INV_DATE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;

END VALIDATE_EVENT_DATE;
-------------------------------------------------------------------------------------------------------
FUNCTION ITEMRECLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XItemRclsRef_REC",
                            I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(60) := 'RMSSUB_ITEMRECLASS_VALIDATE.ITEMRECLASS_EXISTS';
   L_exists      BOOLEAN := FALSE;

BEGIN

   if I_message.reclass_no is NOT NULL then
      if NOT RECLASS_SQL.EXIST(O_error_message,
                               L_exists,
                               I_message.reclass_no) then                                
         return FALSE;
      end if;   

      if NOT L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('INV_RECLASS_NO',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;
   
   if I_message_type = RMSSUB_XITEMRCLS.LP_del_type and
      I_message.reclass_no is NULL and
      I_message.reclass_date is NOT NULL then

      if NOT RECLASS_SQL.RECLASS_DATE_EXIST(O_error_message,
                                            L_exists,
                                            TRUNC(I_message.reclass_date)) then
         return FALSE;
      end if;

      if NOT L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('INV_RECLASS_DATE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      ---

   elsif I_message_type = RMSSUB_XITEMRCLS.LP_dtl_del_type then
      FOR i in I_message.xitemrclsdtl_tbl.FIRST..I_message.xitemrclsdtl_tbl.LAST LOOP
         if I_message.xitemrclsdtl_tbl(i).item is NOT NULL then

            if RECLASS_SQL.ITEM_ON_EXISTING_RECLASS(O_error_message,
                                                    L_exists,
                                                    I_message.xitemrclsdtl_tbl(i).item,
                                                    I_message.reclass_no) = FALSE then
               return FALSE;
            end if;

            if NOT L_exists then
               O_error_message := SQL_LIB.CREATE_MSG('ITEM_RECLASS_NOT_EXISTS',
                                                     I_message.xitemrclsdtl_tbl(i).item,
                                                     I_message.reclass_no,
                                                     NULL);
               return FALSE;
            end if;

         end if;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END ITEMRECLASS_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message     IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_itemreclass_rec   OUT    NOCOPY   RECLASS_SQL.RECLASS_REC,
                         I_message           IN              "RIB_XItemRclsDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XITEMRCLS_VALIDATE.POPULATE_RECORD';

BEGIN

   O_itemreclass_rec.reclass_head_row.reclass_no    := I_message.reclass_no;
   O_itemreclass_rec.reclass_head_row.reclass_desc  := I_message.reclass_desc;
   O_itemreclass_rec.reclass_head_row.reclass_date  := TRUNC(I_message.reclass_date);
   O_itemreclass_rec.reclass_head_row.to_dept       := I_message.to_dept;
   O_itemreclass_rec.reclass_head_row.to_class      := I_message.to_class;
   O_itemreclass_rec.reclass_head_row.to_subclass   := I_message.to_subclass;

   if I_message.xitemrclsdtl_tbl.COUNT > 0 and
      I_message.xitemrclsdtl_tbl is NOT NULL then
      ---
      -- initialize collection
      O_itemreclass_rec.reclass_item_tbl := ITEM_TBL();

      FOR i in I_message.xitemrclsdtl_tbl.FIRST..I_message.xitemrclsdtl_tbl.LAST LOOP
         if I_message.xitemrclsdtl_tbl(i).item is NOT NULL then
            O_itemreclass_rec.reclass_item_tbl.extend();
            O_itemreclass_rec.reclass_item_tbl(O_itemreclass_rec.reclass_item_tbl.COUNT) := I_message.xitemrclsdtl_tbl(i).item;
         end if;
      END LOOP;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message     IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_itemreclass_rec   OUT    NOCOPY   RECLASS_SQL.RECLASS_REC,
                         I_message           IN              "RIB_XItemRclsRef_REC",
                         I_message_type      IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'RMSSUB_XITEMRCLS_VALIDATE.POPULATE_RECORD';

BEGIN

   O_itemreclass_rec.purge_all_ind := NVL(I_message.purge_all_ind, 'N');

   if O_itemreclass_rec.purge_all_ind != 'Y' then

      O_itemreclass_rec.reclass_head_row.reclass_no    := I_message.reclass_no;
      O_itemreclass_rec.reclass_head_row.reclass_date  := TRUNC(I_message.reclass_date);

      --- populate the detail node if it is a detail delete

      if I_message_type = RMSSUB_XITEMRCLS.LP_dtl_del_type then

         if I_message.xitemrclsdtl_tbl is NOT NULL AND
            I_message.xitemrclsdtl_tbl.COUNT > 0  then
            ---
            -- initialize collection
            O_itemreclass_rec.reclass_item_tbl := ITEM_TBL();

            FOR i in I_message.xitemrclsdtl_tbl.FIRST..I_message.xitemrclsdtl_tbl.LAST LOOP
               ---
               O_itemreclass_rec.reclass_item_tbl.extend();
               O_itemreclass_rec.reclass_item_tbl(O_itemreclass_rec.reclass_item_tbl.COUNT) := I_message.xitemrclsdtl_tbl(i).item;
            END LOOP;
         ---
         end if;
      ---
      end if;
   ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CONSIGN_DEPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XItemRclsDesc_REC")
   RETURN BOOLEAN IS
   
   L_program   VARCHAR2(60) := 'RMSSUB_XITEMRCLS_VALIDATE.CHECK_CONSIGN_DEPT';
   L_exist     VARCHAR2(1)  := NULL;
   
   cursor C_CHK_CONSIGN is
      select 'x'
        from deps d,
             item_master im,
             TABLE(CAST(I_message.xitemrclsdtl_tbl AS "RIB_XItemRclsDtl_TBL")) xi
       where im.dept = d.dept
         and im.item = xi.item
         and d.purchase_type != (select purchase_type
                                   from deps
                                  where dept = I_message.to_dept
                                 UNION
                                 select purchase_type
                                   from pend_merch_hier
                                  where hier_type = 'D'
                                    and merch_hier_id = I_message.to_dept
                                    and action_type = 'A')
         and rownum = 1;
         
BEGIN

   open C_CHK_CONSIGN;
   
   fetch C_CHK_CONSIGN into L_exist;
   
   close C_CHK_CONSIGN;
   
   if L_exist is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_RECLASS_CONSIGN',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   
   return TRUE;
   
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CONSIGN_DEPT;
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
FUNCTION APPROVED_ORDER_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE)
                              
  RETURN BOOLEAN IS
  
  L_program             VARCHAR2(64)              :=   'RMSSUB_XITEMRCLS_VALIDATE.APPROVED_ORDER_EXIST';
  L_system_options      SYSTEM_OPTIONS%ROWTYPE;
  L_dummy               VARCHAR2(1);
  
  cursor C_APP_ORD_EXIST IS
       select 'Y'
         from ordhead oh,
         ordloc ol,
         item_master im
       where oh.order_no = ol.order_no
         and oh.status = 'A'
         and ol.item = im.item
         and (im.item_parent = I_item or im.item_grandparent = I_item)
         and NVL(ol.qty_received,0) < ol.qty_ordered
         and rownum = 1
     UNION
        select 'Y'
        from ordhead oh,
             ordloc ol
       where oh.order_no = ol.order_no
         and oh.status   = 'A'
         and ol.item     = I_item
         and NVL(ol.qty_received,0) < ol.qty_ordered
         and rownum = 1;
         
  BEGIN
  
      -- Retrieve the value of the system level variable RECLASS_APPR_ORDER_IND
      IF SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options) = FALSE THEN
         RETURN FALSE;
      END IF;
      
      -- Search for any existing Purchase Orders that consist of this item
      if L_system_options.RECLASS_APPR_ORDER_IND = 'N' then
         --
         SQL_LIB.SET_MARK('OPEN',
                          'C_APP_ORD_EXIST',
                          'ORDHEAD, ORDLOC',
                          'item: ' ||TO_CHAR(I_item));
         open C_APP_ORD_EXIST;
         SQL_LIB.SET_MARK('FETCH',
                          'C_APP_ORD_EXIST',
                          'ORDHEAD, ORDLOC',
                          'item: ' ||TO_CHAR(I_item));
         fetch C_APP_ORD_EXIST into L_dummy;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_APP_ORD_EXIST',
                          'ORDHEAD, ORDLOC',
                          'item: ' ||TO_CHAR(I_item));
         close C_APP_ORD_EXIST;
         --
         if L_dummy = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('ITEM_ON_APP_ORDER',
                                            'Item:'||I_item,
                                            NULL,
                                            NULL);
             return FALSE;
         end if;
      end if;
      
      return TRUE;
   
  EXCEPTION

     when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;

END APPROVED_ORDER_EXIST;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XITEMRCLS_VALIDATE;
/
