
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE MERCH_RECLASS_VALIDATE_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_div_code    VARCHAR2(1) := 'V';
   LP_grp_code    VARCHAR2(1) := 'G';
   LP_dept_code   VARCHAR2(1) := 'D';
   LP_cls_code    VARCHAR2(1) := 'C';
   LP_scls_code   VARCHAR2(1) := 'S';
----------------------------------------------------------------------------
   -- Function    : EXIST
   -- Purpose     : This public function checks if a pending merchandise hierarchy
   --               reclassification event already exists on the PEND_MERCH_HIER table.
----------------------------------------------------------------------------
FUNCTION EXIST(O_error_message         IN OUT   VARCHAR2,
               O_exists                IN OUT   BOOLEAN,
               I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
               I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
               I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
               I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
   -- Function    : CHILD_EXIST
   -- Purpose     : This public function check the existence of a child element
   --               on the PEND_MERCH_HIER table for a given merchandise hierarchy.
----------------------------------------------------------------------------
FUNCTION CHILD_EXIST(O_error_message    IN OUT   VARCHAR2,
                     O_exists           IN OUT   BOOLEAN,
                     I_hier_type        IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                     I_hier_id          IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                     I_hier_parent_id   IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                     I_effective_date   IN       PEND_MERCH_HIER.EFFECTIVE_DATE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
   -- Function Name: GET_MERCH_ACTION
   -- Purpose      : This private function checks the action type in the PEND_MERCH_HIER table
-------------------------------------------------------------------------------------------------------
FUNCTION GET_MERCH_ACTION(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_action_type           OUT      PEND_MERCH_HIER.ACTION_TYPE%TYPE,
                          I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                          I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                          I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                          I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END MERCH_RECLASS_VALIDATE_SQL;
/
