CREATE OR REPLACE PACKAGE ITEMLOC_UPDATE_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------
FUNCTION REC_COST_ADJ_INVENTORY (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_new_wac_loc           IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                                 O_neg_soh_wac_adj_amt   IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                                 O_adj_qty               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                 I_location              IN       ORDLOC.LOCATION%TYPE,
                                 I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE,
                                 I_new_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_old_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_receipt_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_recalc_ind            IN       VARCHAR2,
                                 I_order_number          IN       ORDHEAD.ORDER_NO%TYPE   DEFAULT NULL,
                                 I_ref_no_2              IN       TRAN_DATA.REF_NO_2%TYPE DEFAULT NULL,
                                 I_pack_item             IN       ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                 I_pgm_name              IN       TRAN_DATA.PGM_NAME%TYPE DEFAULT NULL,
                                 I_adj_code              IN       TRAN_DATA.ADJ_CODE%TYPE DEFAULT 'C',
                                 I_shipment              IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                 I_new_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE  DEFAULT NULL,
                                 I_old_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE  DEFAULT NULL,
                                 I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N',
                                 I_posting_ind           IN       BOOLEAN DEFAULT TRUE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Name : UPD_AV_COST_CHANGE_COST
-- Purpose: update av_cost due to cost change of a PO.  Cost change could be
--          due to either merchandise cost change or difference between
--          ELC and ALC.
------------------------------------------------------------------------------
FUNCTION UPD_AV_COST_CHANGE_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                 I_location              IN       ORDLOC.LOCATION%TYPE,
                                 I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE,
                                 I_new_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_old_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_receipt_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_new_wac_loc           IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_neg_soh_wac_adj_amt   IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_recalc_ind            IN       VARCHAR2,
                                 I_order_number          IN       ORDHEAD.ORDER_NO%TYPE   DEFAULT NULL,
                                 I_ref_no_2              IN       TRAN_DATA.REF_NO_2%TYPE DEFAULT NULL,
                                 I_pack_item             IN       ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                 I_pgm_name              IN       TRAN_DATA.PGM_NAME%TYPE DEFAULT NULL,
                                 I_adj_code              IN       TRAN_DATA.ADJ_CODE%TYPE DEFAULT 'C',
                                 I_shipment              IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                 I_new_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE  DEFAULT NULL,
                                 I_old_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE  DEFAULT NULL,
                                 I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name   :  UPD_AVG_COST_CHANGE_QTY
-- Purpose:  This function will only be called from the Product Transformation form.
--           It will update the stock on hand and average cost of an item/location
--           combination.
-------------------------------------------------------------------------------
FUNCTION UPD_AVG_COST_CHANGE_QTY(I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                                 I_location        IN       INV_ADJ.LOCATION%TYPE,
                                 I_new_cost        IN       ORDLOC.UNIT_COST%TYPE,
                                 I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                                 O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name   :  BULK_LOCK_ITEM_LOC_SOH
-- Purpose:  This function will lock the rows on item_loc_soh table for all the
--           items on api_item_loc_temp table. This function is called by reqext.pc
-------------------------------------------------------------------------------
FUNCTION BULK_LOCK_ITEM_LOC_SOH (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name : UPD_ALC_AV_COST_CHANGE_COST
-- Purpose: Update av_cost due to cost change of a PO via obligation when
--          finalizing an ALC due to a difference in ELC and ALC.
------------------------------------------------------------------------------
FUNCTION UPD_ALC_AV_COST_CHANGE_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                     I_location        IN       ORDLOC.LOCATION%TYPE,
                                     I_loc_type        IN       ORDLOC.LOC_TYPE%TYPE,
                                     I_new_cost        IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                     I_old_cost        IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                     I_receipt_qty     IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                     I_recalc_ind      IN       VARCHAR2,
                                     I_order_number    IN       ORDHEAD.ORDER_NO%TYPE,
                                     I_pack_item       IN       ITEM_MASTER.ITEM%TYPE,
                                     I_pgm_name        IN       TRAN_DATA.PGM_NAME%TYPE,
                                     I_adj_code        IN       TRAN_DATA.ADJ_CODE%TYPE,
                                     I_shipment        IN       SHIPMENT.SHIPMENT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
END ITEMLOC_UPDATE_SQL;
/
