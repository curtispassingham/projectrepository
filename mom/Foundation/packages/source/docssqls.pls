
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DOCUMENTS_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: NEXT_DOC_NO
-- Purpose      : Get new doc_id and validate that it doesn't already exist on doc table.
---------------------------------------------------------------------------------------------
FUNCTION NEXT_DOC_NO( O_error_message IN OUT VARCHAR2,
                      O_doc_id        IN OUT doc.doc_id%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: NEXT_REQ_DOC_KEY
-- Purpose      : Get new doc_key(seq_no) and validate that it doesn't already exist on doc table.
---------------------------------------------------------------------------------------------
FUNCTION NEXT_REQ_DOC_KEY( O_error_message IN OUT VARCHAR2,
                           O_doc_key       IN OUT REQ_DOC.DOC_KEY%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_INFO
-- Purpose      : Provides cursor that retrieves the document description and text for 
--                the given document ID. If no description is found give an error message 
--                saying the document ID is invalid, otherwise pass out the description and 
--                text.  If the function fails, return FALSE, otherwise return TRUE.

---------------------------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message  IN OUT  VARCHAR2,
                   O_exists         IN OUT  BOOLEAN,
                   O_doc_desc	    IN OUT  DOC.DOC_DESC%TYPE,
                   O_text           IN OUT  DOC.TEXT%TYPE,
                   I_doc_id         IN      DOC.DOC_ID%TYPE)
    RETURN BOOLEAN;
---------------------------------------------------------------------------------------------

-- Function Name: VALIDATE_TYPE
-- Purpose      : Validates whether a given document id exists within a given module.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TYPE (O_error_message IN OUT VARCHAR2,
                        O_exists	IN OUT BOOLEAN,
                        I_doc_module    IN     REQ_DOC.MODULE%TYPE,
                        I_doc_id        IN     DOC.DOC_ID%TYPE)
    RETURN BOOLEAN;
---------------------------------------------------------------------------------------------

-- Function Name: REQ_DOCS_EXIST_ID
-- Purpose      : Provides a cursor that validates the existence of the current form doc_id 
--                with previous doc_ids committed to the req_doc table. 
---------------------------------------------------------------------------------------------
FUNCTION REQ_DOCS_EXIST_ID( O_error_message IN OUT VARCHAR2,
                            O_exists        IN OUT BOOLEAN,
                            I_doc_id        IN     DOC.DOC_ID%TYPE)
    RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: REQ_DOCS_EXIST_MOD_KEY
-- Purpose      : Determines if required documents exist for a given module
--                type, key value 1, key value 2, and doc_id combination.
---------------------------------------------------------------------------------------------
FUNCTION REQ_DOCS_EXIST_MOD_KEY( O_error_message IN OUT VARCHAR2,
                                 O_exists        IN OUT BOOLEAN,
                                 I_module        IN     REQ_DOC.MODULE%TYPE,
                                 I_key_value_1   IN     REQ_DOC.KEY_VALUE_1%TYPE,
                                 I_key_value_2   IN     REQ_DOC.KEY_VALUE_2%TYPE,
                                 I_doc_id        IN     REQ_DOC.DOC_ID%TYPE)
	RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name :  LOC_REQ_DOCS
-- Purpose       :  to lock records by module, key value 1 and key value
--                  2 prior to deletion
---------------------------------------------------------------------------------------------
FUNCTION LOCK_REQ_DOCS   (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_module         IN      REQ_DOC.MODULE%TYPE,
                          I_key_value_1    IN      REQ_DOC.KEY_VALUE_1%TYPE,
                          I_key_value_2    IN      REQ_DOC.KEY_VALUE_2%TYPE,
                          I_doc_id         IN      REQ_DOC.DOC_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------- 
-- Function Name :  DELETE_REQ_DOCS
-- Purpose       :  to delete docs by module, key value 1 and key value 2
--                  after locking logic is called.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_REQ_DOCS (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_module         IN      REQ_DOC.MODULE%TYPE,
                          I_key_value_1    IN      REQ_DOC.KEY_VALUE_1%TYPE,
                          I_key_value_2    IN      REQ_DOC.KEY_VALUE_2%TYPE,
                          I_doc_id         IN      REQ_DOC.DOC_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULTS
-- Purpose      : This function defaults required documents from a specific module to 
--                another module.  For example, when a partner is added to a PO the 
--                required documents defined for the partner will be defaulted as the 
--                required documents defined for the PO.
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULTS( O_error_message      IN OUT  VARCHAR2,
                       I_from_module        IN      REQ_DOC.MODULE%TYPE,
                       I_to_module          IN      REQ_DOC.MODULE%TYPE,
                       I_from_key_value_1   IN      REQ_DOC.KEY_VALUE_1%TYPE,
                       I_to_key_value_1     IN      REQ_DOC.KEY_VALUE_1%TYPE,
                       I_from_key_value_2   IN      REQ_DOC.KEY_VALUE_2%TYPE,
                       I_to_key_value_2     IN      REQ_DOC.KEY_VALUE_2%TYPE)
    RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: INSERT_REQ_DOC
-- Purpose      : This function writes a record to the req_docs table.
--------------------------------------------------------------------------------------
FUNCTION INSERT_REQ_DOC(O_error_message  IN OUT  VARCHAR2,
                        I_module         IN      REQ_DOC.MODULE%TYPE,
                        I_key_value_1    IN      REQ_DOC.KEY_VALUE_1%TYPE,
                        I_key_value_2    IN      REQ_DOC.KEY_VALUE_2%TYPE,
                        I_doc_id         IN      REQ_DOC.DOC_ID%TYPE,
                        I_doc_text       IN      REQ_DOC.DOC_TEXT%TYPE)
    RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: NEXT_MISSING_DOC_KEY
-- Purpose      : This function gets a new missing_doc_key(seq_no) and validates that it doesn't
--                already exist on MISSING DOC table.
---------------------------------------------------------------------------------------------
FUNCTION NEXT_MISSING_DOC_KEY(O_error_message     IN OUT  VARCHAR2,
                              O_missing_doc_key   IN OUT  MISSING_DOC.MISSING_DOC_KEY%TYPE)
   return   BOOLEAN; 
---------------------------------------------------------------------------------------------
-- Function Name: MISSING_DOCS_EXIST_ID
-- Purpose      : This function will validate that the passed in DOC_ID is unique for the passed
--                in transportation_id, ce_id, doc_id combination.
---------------------------------------------------------------------------------------------
FUNCTION MISSING_DOCS_EXIST_ID(O_error_message         IN OUT  VARCHAR2,
                               O_exists                IN OUT  BOOLEAN,
                               I_doc_id                IN      MISSING_DOC.DOC_ID%TYPE,
      				 I_transportation_id     IN      MISSING_DOC.TRANSPORTATION_ID%TYPE,
 					 I_ce_id                 IN      MISSING_DOC.CE_ID%TYPE,
	    				 I_vessel_id             IN      MISSING_DOC.VESSEL_ID%TYPE,
 					 I_voyage_flt_id         IN      MISSING_DOC.VOYAGE_FLT_ID%TYPE,
				 	 I_estimated_depart_date IN      MISSING_DOC.ESTIMATED_DEPART_DATE%TYPE,
					 I_order_no              IN      MISSING_DOC.ORDER_NO%TYPE,
					 I_item                  IN      MISSING_DOC.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------------
-- Function Name: GET_LC_IND
-- Purpose      : This function will retireve the lc_ind for the passed 
--                DOC_ID.
--------------------------------------------------------------------------             
FUNCTION GET_LC_IND(O_error_message IN OUT VARCHAR2,
                    O_lc_ind        IN OUT DOC.LC_IND%TYPE,
                    I_doc_id        IN     DOC.DOC_ID%TYPE)
    RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Function : COPY_DOWN_PARENT_REQ_DOC
-- Purpose  : This function will default identical required document records
--            down to the children and grandchildren levels for the inputted
--            item.  The defaulting will go down as far as the transaction
--            level.
-------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_REQ_DOC (O_error_message  IN OUT VARCHAR2,
                                   I_item           IN     ITEM_MASTER.ITEM%TYPE)
    RETURN BOOLEAN;
-------------------------------------------------------------------------------
END;
/


