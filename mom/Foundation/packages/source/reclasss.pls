CREATE OR REPLACE PACKAGE RECLASS_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Public Variables
---------------------------------------------------------------------------------------------
TYPE RECLASS_REC IS RECORD(reclass_head_row   RECLASS_HEAD%ROWTYPE,
                           purge_all_ind      VARCHAR2(1),
                           reclass_item_tbl   ITEM_TBL);

   L_reclass_action          COST_EVENT.ACTION%TYPE;
   L_username                USERS.USER_NAME%TYPE := GET_USER;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_reclass_tbl             OBJ_RCLS_COST_EVENT_TBL := OBJ_RCLS_COST_EVENT_TBL();
---------------------------------------------------------------------------------------------
-- Function Name: NEXT_RECLASS_NO
-- Purpose      : Get the next Reclassification Number from Reclass_Head table. Verify that
--                the return number is unique within the database.
-- Created By   : Retek Information Systems - Bruce Van
-- Date         : 02-Apr-97
---------------------------------------------------------------------------------------------
FUNCTION NEXT_RECLASS_NO( O_error_message IN OUT VARCHAR2,
                          O_reclass_no    IN OUT reclass_head.reclass_no%TYPE )
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: EXIST
-- Purpose      : Checks for the existence of a reclassification number on the reclass_head
--                table.
-- Created By   : Retek Information Systems - Bruce Van
-- Date         : 20-Apr-97
--------------------------------------------------------j-------------------------------------
FUNCTION EXIST( O_error_message IN OUT VARCHAR2,
                O_exists        IN OUT BOOLEAN,
                I_reclass_no    IN     reclass_head.reclass_no%TYPE )
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_RECLASS_DESC
-- Purpose      : Accept a reclassification number and return the associated description from
--                the reclass_head table.
-- Created By   : Retek Information Systems - Bruce Van
-- Date         : 20-Apr-97
---------------------------------------------------------------------------------------------
FUNCTION GET_RECLASS_DESC( O_error_message   IN OUT VARCHAR2,
                           O_reclass_desc    IN OUT reclass_head.reclass_desc%TYPE,
                           O_reclass_desc_tl IN OUT reclass_head.reclass_desc%TYPE,
                           I_reclass_no      IN     reclass_head.reclass_no%TYPE )
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: ITEM_VALIDATION
-- Purpose      : Verify that a specified item can be reclassified. It will also indicate if
--                an item is on active order so that the calling form can appropriately warn
--                the user. But it will not stop the reclassification from taking place.
-- Created By   : Retek Information Systems - Bruce Van
-- Date         : 02-Apr-97
---------------------------------------------------------------------------------------------
FUNCTION ITEM_VALIDATION( O_error_message           IN OUT   VARCHAR2,
                          O_invalid_item_level      IN OUT   VARCHAR2,
                          O_consign_conflict_flag   IN OUT   VARCHAR2,
                          O_reclass_exist           IN OUT   VARCHAR2,
                          O_order_flag              IN OUT   VARCHAR2,
                          O_uda_reqd                IN OUT   VARCHAR2,
                          O_simple_pack             IN OUT   ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                          IO_no_domain_exists       IN OUT   VARCHAR2,
                          I_system_forecast_ind     IN       VARCHAR2,
                          I_domain_exists           IN       BOOLEAN,
                          I_item                    IN       ITEM_MASTER.ITEM%TYPE,
                          I_item_level              IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                          I_tran_level              IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                          I_pack_ind                IN       ITEM_MASTER.PACK_IND%TYPE,
                          I_old_dept                IN       ITEM_MASTER.DEPT%TYPE,
                          I_new_dept                IN       ITEM_MASTER.DEPT%TYPE,
                          I_new_class               IN       ITEM_MASTER.CLASS%TYPE,
                          I_new_subclass            IN       ITEM_MASTER.SUBCLASS%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ITEM_PROCESS
-- Purpose      : Update or insert records to item_master, item, and tran_data tables
--                If the item can not be reclassed, the IO_reclass_failed returns value TRUE.
--                Otherwise, it returns a value of FALSE. if reclass failed, the IO_failed_reason
--                contains the failed reason.
-- Created By   : Retek Information Systems - Bruce Van
-- Date         : 02-Apr-97
---------------------------------------------------------------------------------------------

FUNCTION ITEM_PROCESS( O_error_message         IN OUT   VARCHAR2,
                       IO_retry_count          IN OUT   INTEGER,
                       IO_reclass_failed       IN OUT   BOOLEAN,
                       IO_failed_reason        IN OUT   MC_REJECTION_REASONS.REASON_KEY%TYPE,
                       I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                       I_item_level            IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level            IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_pack_ind              IN       ITEM_MASTER.PACK_IND%TYPE,
                       I_reclass_date          IN       DATE,
                       I_system_forecast_ind   IN       VARCHAR2,
                       I_otb_system_ind        IN       SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE,
                       I_stake_lockout_days    IN       SYSTEM_OPTIONS.STAKE_LOCKOUT_DAYS%TYPE,
                       I_domain_exists         IN       BOOLEAN,
                       I_new_domain_id         IN       DOMAIN.DOMAIN_ID%TYPE,
                       I_old_dept              IN       ITEM_MASTER.DEPT%TYPE,
                       I_old_class             IN       ITEM_MASTER.CLASS%TYPE,
                       I_old_subclass          IN       ITEM_MASTER.SUBCLASS%TYPE,
                       I_new_dept              IN       ITEM_MASTER.DEPT%TYPE,
                       I_new_class             IN       ITEM_MASTER.CLASS%TYPE,
                       I_new_subclass          IN       ITEM_MASTER.SUBCLASS%TYPE,
                       I_mode                  IN       VARCHAR2)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: ITEMLIST_PROCESS
-- Purpose      : Performs the reclassification validation for all items in an itemlist or a
--                single item and inserts into tables reclass_head and reclass_item.  It calls
--                ITEM_VALIDATION, and inserts into the mc_rejections table based on the flags
--                and reasons passed from ITEM_VALIDATION.  It also sets output flags (one for
--                reject, and one for approved order existence) that allow the calling form to
--                display the appropriate message to the user.
-- Created By   : Retek Information Systems - Bruce Van
-- Date         : 02-Apr-97
---------------------------------------------------------------------------------------------
FUNCTION ITEMLIST_PROCESS( O_error_message  IN OUT VARCHAR2,
                           O_reject         IN OUT BOOLEAN,
                           O_order_flag     IN OUT VARCHAR2,
                           O_no_domain_flag IN OUT VARCHAR2,
                           I_item           IN     skulist_detail.item%TYPE,
                           I_skulist        IN     skulist_detail.skulist%TYPE,
                           I_item_level     IN     skulist_detail.item_level%TYPE,
                           I_tran_level     IN     skulist_detail.tran_level%TYPE,
                           I_reclass_no     IN     reclass_head.reclass_no%TYPE,
                           I_reclass_date   IN     DATE,
                           I_reclass_desc   IN     reclass_head.reclass_desc%TYPE,
                           I_new_dept       IN     item_master.dept%TYPE,
                           I_new_class      IN     item_master.CLASS%TYPE,
                           I_new_subclass   IN     item_master.subclass%TYPE,
                           I_username       IN     user_users.username%TYPE )
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Purpose      : This function will validate that a passed hierarchy entity exists in the
--                entity if it exists.
-- Created by   : Portia Dela Rosa
-- Date         : 26-Aug-03
---------------------------------------------------------------------------------------------
FUNCTION GET_PEND_HIER_DESC(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_hier_desc                   IN OUT   PEND_MERCH_HIER.MERCH_HIER_NAME%TYPE,
                            I_hier_type                   IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                            I_merch_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                            I_merch_parent_hier_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                            I_merch_grandparent_hier_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_HIER_DESC
-- Purpose      : This function will validate that a passed hierarchy entity exists in RMS
--                and then retrieve the description of the passed entity if it exists.
-- Created by   : Portia Dela Rosa
-- Date         : 26-Aug-03
---------------------------------------------------------------------------------------------
FUNCTION GET_HIER_DESC(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_hier_desc                   IN OUT   PEND_MERCH_HIER.MERCH_HIER_NAME%TYPE,
                       I_hier_type                   IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                       I_merch_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                       I_merch_parent_hier_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                       I_merch_grandparent_hier_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_EFFECTIVE_DATE
-- Purpose      : This function will ensure that for any pending hierarchy change, the reclass
--                event effective date is equal to or greater than the effective date for the
--                entities on the shadow table V_MERCH_HIER.
-- Created by   : Portia Dela Rosa
-- Date         : 26-Aug-03
---------------------------------------------------------------------------------------------
FUNCTION CHECK_EFFECTIVE_DATE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid                  IN OUT VARCHAR2,
                              O_valid_reclass_date     IN OUT RECLASS_HEAD.RECLASS_DATE%TYPE,
                              I_dept                   IN     DEPS.DEPT%TYPE,
                              I_class                  IN     CLASS.CLASS%TYPE,
                              I_subclass               IN     SUBCLASS.SUBCLASS%TYPE,
                              I_reclass_effective_date IN     RECLASS_HEAD.RECLASS_DATE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_RECLASS_DATE
-- Purpose      : This function will validate that the effective date of a hierarchy is not
--                updated to a date that is after an existing scheduled item reclassification
--                date.
-- Created by   : Portia Dela Rosa
-- Date         : 26-Aug-03
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_RECLASS_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_date            IN OUT   DATE,
                               I_dept            IN       DEPS.DEPT%TYPE,
                               I_class           IN       CLASS.CLASS%TYPE,
                               I_subclass        IN       SUBCLASS.SUBCLASS%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ITEM_ON_EXISTING_RECLASS
-- Purpose      : This function will check whether or not the item exists on another
--                reclassification event.
-- Created by   : Asher Ledesma
-- Date         : 02-Sept-03
---------------------------------------------------------------------------------------------
FUNCTION ITEM_ON_EXISTING_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT   BOOLEAN,
                                  I_item_no         IN       RECLASS_ITEM.ITEM%TYPE,
                                  I_reclass_no      IN       RECLASS_ITEM.RECLASS_NO%TYPE)

   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: RECLASS_DATE_EXIST
-- Purpose      : This function will check if a reclassification event exists on the
--                date passed
-- Created by   : Asher Ledesma
-- Date         : 02-Sep-03
---------------------------------------------------------------------------------------------
FUNCTION RECLASS_DATE_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist           IN OUT   BOOLEAN,
                            I_reclass_date    IN       RECLASS_HEAD.RECLASS_DATE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CREATE_RECLASS
-- Purpose      : This function will take in an item reclassification record
--                and insert it into the RECLASS_HEAD table.
-- Created by   : Asher Ledesma
-- Date         : 02-Sep-03
---------------------------------------------------------------------------------------------
FUNCTION CREATE_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_reclass_rec     IN       RECLASS_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CREATE_RECLASS_ITEM
-- Purpose      : This function will take in an item reclassification record
--                then insert it into the RECLASS_ITEM table.
-- Created by   : Asher Ledesma
-- Date         : 02-Sep-03
---------------------------------------------------------------------------------------------
FUNCTION CREATE_RECLASS_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_reclass_rec     IN       RECLASS_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_RECLASS
-- Purpose      : This function will take in an item reclassification record
--                then delete it from the RECLASS_HEAD table.
-- Created by   : Asher Ledesma
-- Date         : 02-Sep-03
---------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_reclass_rec     IN       RECLASS_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_RECLASS_ITEM
-- Purpose      : This function will take in an item reclassification record
--                then delete it from the RECLASS_ITEM table.
-- Created by   : Asher Ledesma
-- Date         : 02-Sep-03
---------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_reclass_rec     IN       RECLASS_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PARENT_EFFECTIVE_DATE
-- Purpose      : This function will return the hier parent's effective date from V_MERCH_HIER
--              : for a passed in hier_id.
--              : Michael Johnson
--              : 25-Sept-03
---------------------------------------------------------------------------------------------
FUNCTION PARENT_EFFECTIVE_DATE(O_error_message         IN OUT   VARCHAR2,
                               O_date                  IN OUT   DATE,
                               I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                               I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                               I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: POST_PROCESS
-- Purpose      : This function performs the department level inserts and updates
--              : These can't be multithreaded so they are performed in the post process
---------------------------------------------------------------------------------------------
FUNCTION POST_PROCESS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: RECLASS_ITEM_EXIST
-- Purpose      : Checks for the existence of a reclassification items on the reclass_item
--                table.
---------------------------------------------------------------------------------------------
FUNCTION RECLASS_ITEM_EXIST(O_error_message   IN OUT   VARCHAR2,
                            O_exists          IN OUT   BOOLEAN,
                            I_reclass_no      IN       RECLASS_HEAD.RECLASS_NO%TYPE )
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CREATE_FC_FOR_RECLASS
-- Purpose      : Create future_cost events for reclassification.
---------------------------------------------------------------------------------------------
FUNCTION CREATE_FC_FOR_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_reclass_no      IN       RECLASS_HEAD.RECLASS_NO%TYPE )
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_FC_FOR_RECLASS
-- Purpose      : Create future_cost events when reclassification is deleted.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_FC_FOR_RECLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_reclass_tbl     IN       OBJ_RCLS_COST_EVENT_TBL)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Function Name: GET_RECLASS_DETAIL
-- Purpose      : Get the detail of reclassification.
---------------------------------------------------------------------------------------------
FUNCTION GET_RECLASS_DETAIL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_reclass_delete_tbl   IN OUT   OBJ_RCLS_COST_EVENT_TBL,
                            I_reclass_no           IN       RECLASS_HEAD.RECLASS_NO%TYPE,
                            I_reclass_dtl_tbl      IN       OBJ_RECLASS_ITEM_TBL,
                            I_reclass_ind          IN       VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ITEM_LOC_DTL_EXPLD
-- Purpose      : Update the CLOSE_DATE of the ITEM_LOC_DETAIL_EXPLODE during reclassification.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC_DTL_EXPLD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: LOCK_RECLASS_ITEM
-- Purpose      : Locks Reclass Item table records for the given Reclass Number.
---------------------------------------------------------------------------------------------
FUNCTION LOCK_RECLASS_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_reclass_no      IN       RECLASS_HEAD.RECLASS_NO%TYPE )
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;

/
