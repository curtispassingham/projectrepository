



CREATE OR REPLACE PACKAGE ITEMLIST_VALIDATE_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------
-- Function Name: exist
-- Purpose	: to check if a given skulist number exists
--		  on the skulist_head table
-- Calls	: None
-- Input Values : I_skulist, Number
-- Return Values: O_exist, BOOLEAN, set to TRUE if the skulist exists
--				on table, FALSE otherwise.
-- Created	: 27-Aug-96 by Matt Sniffen
-------------------------------------------------------------------
FUNCTION EXIST(O_error_message	IN OUT	VARCHAR2,
		       I_skulist	    IN	    NUMBER,
		       O_exist		    IN OUT	BOOLEAN)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: check_edit_exists
-- Purpose      : to check if the entered skulist should be able to be
--                modified by the current user
-- Calls        : None
-- Input Values : I_skulist,
--                I_user_id
-- Return Values: O_exist, BOOLEAN, set to TRUE if the skulist was created by the 
--                current user, or if the skulist security indicator is set to 'N'o,
--                FALSE, otherwise.
-------------------------------------------------------------------
FUNCTION CHECK_EDIT_EXISTS(O_error_message	IN OUT	VARCHAR2,
		           O_exist		    IN OUT	BOOLEAN,
                           I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                           I_user_id         IN      SKULIST_HEAD.CREATE_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------
-- Function Name: supplier_exists
-- Purpose      : to check if the passed in supplier I_supplier exists for any of the items
--                in passed item list I_skulist
-- Calls        : None
-- Input Values : I_skulist,
--                I_supplier
-- Return Values: O_exist, BOOLEAN, set to TRUE the passed in supplier I_supplier exists for
--                any of the items in passed item list I_skulist, FALSE, otherwise.
-------------------------------------------------------------------
FUNCTION SUPPLIER_EXISTS(O_error_message    IN OUT VARCHAR2,
                         O_item_supp_exists IN OUT BOOLEAN,
                         I_skulist          IN     SKULIST_HEAD.SKULIST%TYPE,
                         I_supplier         IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------
END ITEMLIST_VALIDATE_SQL;
/


