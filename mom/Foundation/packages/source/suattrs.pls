



CREATE OR REPLACE PACKAGE SUPP_ATTRIB_SQL AUTHID CURRENT_USER AS
   ---------------------------------------------------------------------
   --- Function:  COURIER
   --- Purpose:   Gets the return courier for a given supplier
   --- Created:   21-NOV-96 Richard Stockton
   ---------------------------------------------------------------------
   FUNCTION COURIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_supplier        IN       NUMBER,
                    O_desc            IN OUT   VARCHAR2) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   --- Function:       GET_SUPP_DESC
   --- Purpose:        Uses supp number to bring back supp description.
   --- Calls:          None
   --- Called By:      FM_shipfind
   --- Input Values:   I_supplier
   --- Return Values:  O_supp_desc, O_error_message
   ---------------------------------------------------------------------
   FUNCTION GET_SUPP_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_supplier        IN       NUMBER,
                          O_supp_desc       IN OUT   VARCHAR2) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   --- Function:       GET_PAYMENT_DESC
   --- Purpose:        Takes the currency code, terms, and freight terms
   ---                 and sends back the description for each.
   --- Calls:          None
   --- Called By:      FM_supfind
   --- Input Values:   I_currency, I_terms, I_frght_terms
   --- Return Values:  O_currency_desc,O_terms_desc, O_frght_terms_desc
   ---------------------------------------------------------------------
   FUNCTION GET_PAYMENT_DESC(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_currency           IN       VARCHAR2,
                             I_terms              IN       VARCHAR2,
                             I_frght_terms        IN       VARCHAR2,
                             O_currency_desc      IN OUT   VARCHAR2,
                             O_terms_code         IN OUT   VARCHAR2,
                             O_terms_desc         IN OUT   VARCHAR2,
                             O_frght_terms_desc   IN OUT   VARCHAR2) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   --- Function:       GET_STATUS
   --- Purpose:        Uses supplier number to bring back SUPPLIER STATUS.
   --- Calls:          None
   --- Called By:      None
   --- Input Values:   I_supplier
   --- Return Values:  O_status, O_error_message
   ---------------------------------------------------------------------
   FUNCTION GET_STATUS(I_supplier        IN       NUMBER,
                       O_status          IN OUT   VARCHAR2,
                       O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   -- Function : GET_RET_AUTH_REQ
   -- Purpose  : takes a supplier number and gets the return
   --            authorization required indicator from the sups
   --            table
   -- Calls    : none
   -- Created  : 07-OCT-96 by Matt Sniffen
   --------------------------------------------------------------------
   FUNCTION GET_RET_AUTH_REQ(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_supplier        IN       NUMBER,
                             O_ret_auth_req    IN OUT   VARCHAR2) RETURN BOOLEAN;
   --------------------------------------------------------------------
   -- Function : GET_RET_ALLOW_IND
   -- Purpose  : takes a supplier number and gets the return
   --            allowed indicator from the sups table
   -- Calls    : none
   -- Created  : 07-OCT-96 by Matt Sniffen
   --------------------------------------------------------------------
   FUNCTION GET_RET_ALLOW_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_supplier        IN       NUMBER,
                              O_ret_allow_ind   IN OUT   VARCHAR2) RETURN BOOLEAN;
   --------------------------------------------------------------------
   -- Function Name: GET_MIN_RET_AMT
   -- Purpose  : takes a supplier number and gets the minimum
   --            return amount from the sups table
   -- Calls    : none
   -- Created  : 07-OCT-96 by Matt Sniffen
   --------------------------------------------------------------------
   FUNCTION GET_MIN_RET_AMT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_supplier        IN       NUMBER,
                            O_min_ret_amt     IN OUT   NUMBER) RETURN BOOLEAN;
   --------------------------------------------------------------------
   -- Function : ORDER_SUP_DETAILS
   -- Purpose  : This function will return various supplier attributes
   --            related to POs.
   --------------------------------------------------------------------
   FUNCTION ORDER_SUP_DETAILS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_currency_code    IN OUT   SUPS.CURRENCY_CODE%TYPE,
                              O_terms            IN OUT   SUPS.TERMS%TYPE,
                              O_freight_terms    IN OUT   SUPS.FREIGHT_TERMS%TYPE,
                              O_sup_status       IN OUT   SUPS.SUP_STATUS%TYPE,
                              O_qc_ind           IN OUT   SUPS.QC_IND%TYPE,
                              O_edi_po_ind       IN OUT   SUPS.EDI_PO_IND%TYPE,
                              O_pre_mark_ind     IN OUT   SUPS.PRE_MARK_IND%TYPE,
                              O_ship_method      IN OUT   SUPS.SHIP_METHOD%TYPE,
                              O_payment_method   IN OUT   SUPS.PAYMENT_METHOD%TYPE,
                              I_supplier         IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------
   -- Function Name: NEXT_ADDR
   -- Purpose      : This function will retrieve the next address key
   --                number in the sequence.
   -- Calls        :
   --------------------------------------------------------------------
   FUNCTION NEXT_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_addr_key        IN OUT   ADDR.ADDR_KEY%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------
   -- Function Name: GET_CURRENCY_CODE
   -- Purpose      : This function will be used to determine the currency
   --                for the given supplier
   -- Calls        : <none>
   --------------------------------------------------------------------
   FUNCTION GET_CURRENCY_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_currency_code   IN OUT   SUPS.CURRENCY_CODE%TYPE,
                              I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------
   -- Function : ORDER_IMPORT_DETAILS
   -- Purpose  : This function will return various supplier import
   --            attributes related to POs.
   --------------------------------------------------------------------
   FUNCTION ORDER_IMPORT_DETAILS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_agent            IN OUT   SUP_IMPORT_ATTR.AGENT%TYPE,
                                 O_lading_port      IN OUT   SUP_IMPORT_ATTR.LADING_PORT%TYPE,
                                 O_discharge_port   IN OUT   SUP_IMPORT_ATTR.DISCHARGE_PORT%TYPE,

                                 O_factory          IN OUT   SUP_IMPORT_ATTR.FACTORY%TYPE,
                                 O_partner_type_1   IN OUT   SUP_IMPORT_ATTR.PARTNER_TYPE_1%TYPE,
                                 O_partner_1        IN OUT   SUP_IMPORT_ATTR.PARTNER_1%TYPE,
                                 O_partner_type_2   IN OUT   SUP_IMPORT_ATTR.PARTNER_TYPE_2%TYPE,
                                 O_partner_2        IN OUT   SUP_IMPORT_ATTR.PARTNER_2%TYPE,
                                 O_partner_type_3   IN OUT   SUP_IMPORT_ATTR.PARTNER_TYPE_3%TYPE,
                                 O_partner_3        IN OUT   SUP_IMPORT_ATTR.PARTNER_2%TYPE,

                                 I_supplier         IN       SUP_IMPORT_ATTR.SUPPLIER%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------
   --Function Name: DBT_MEMO_CODE
   --Purpose:       To accept a valid supplier number and retrieve the dbt_memo_code from
   --               the sups table.  The package will return FALSE if I_supplier
   --               is passed in NULL or invalid.
   --------------------------------------------------------------------
   FUNCTION DBT_MEMO_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_dbt_memo_code   IN OUT   sups.dbt_memo_code%TYPE,
                          I_supplier        IN       sups.supplier%TYPE) RETURN BOOLEAN;
   -------------------------------------------------------------------------------------
   -- Function : VALID_BENEFICIARY
   -- Purpose  : Validates whether a passed in supplier is a beneficiary.
   -------------------------------------------------------------------------------------
   FUNCTION VALID_BENEFICIARY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_supplier        IN       SUP_IMPORT_ATTR.SUPPLIER%TYPE) RETURN BOOLEAN;

   -------------------------------------------------------------------------------------
   -- Function : BENEFICIARY_DETAILS
   -- Purpose  : Retrieves the with recourse indicator, variance percentage,
   --            negotiation days, place of expiry, drafts at, and presentation
   --            terms from sup_import_attr for the passed in supplier.
   -------------------------------------------------------------------------------------
   FUNCTION BENEFICIARY_DETAILS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_with_recourse_ind   IN OUT   SUP_IMPORT_ATTR.WITH_RECOURSE_IND%TYPE,
                                O_variance_pct        IN OUT   SUP_IMPORT_ATTR.VARIANCE_PCT%TYPE,
                                O_neg_days            IN OUT   SUP_IMPORT_ATTR.LC_NEG_DAYS%TYPE,
                                O_place_of_expiry     IN OUT   SUP_IMPORT_ATTR.PLACE_OF_EXPIRY%TYPE,
                                O_drafts_at           IN OUT   SUP_IMPORT_ATTR.DRAFTS_AT%TYPE,
                                O_present_terms       IN OUT   SUP_IMPORT_ATTR.PRESENTATION_TERMS%TYPE,
                                O_issuing_bank        IN OUT   SUP_IMPORT_ATTR.ISSUING_BANK%TYPE,
                                O_advising_bank       IN OUT   SUP_IMPORT_ATTR.ADVISING_BANK%TYPE,
                                I_supplier            IN       SUP_IMPORT_ATTR.SUPPLIER%TYPE) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   --- Function:  NEXT_INV_MGMT_SEQ_NO
   --- Purpose:   Returns next sequence number from SUP_INV_MGMT_SEQUENCE
   --- Created:   10-AUG-99 Shane Kville
   ---------------------------------------------------------------------
   FUNCTION NEXT_INV_MGMT_SEQ_NO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_sup_dept_seq_no IN OUT   NUMBER) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   --Function Name : ADDR_EXISTS
   --Purpose       : To check the addr table for an existence of an
   --                address for a supplier.
   ---------------------------------------------------------------------
   FUNCTION ADDR_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_key_value_1     IN       VARCHAR2,
                        I_key_value_2     IN       VARCHAR2) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   ---Function:  GET_SUP_PRIMARY_ADDR
   ---Purpose:   Returns the primary address of a given type for a given
   ---           supplier.
   ---Created:   17-JAN-00 Clarence Fajardo
   ---------------------------------------------------------------------
   FUNCTION GET_SUP_PRIMARY_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_addr_key        IN OUT   ADDR.ADDR_KEY%TYPE,
                                 I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                 I_addr_type       IN       ADDR.ADDR_TYPE%TYPE) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   ---Function:  GET_SUP_PRIMARY_ADDR_SEQ_NO
   ---Purpose:   Returns the primary address seq no of a given type for
   ---           a given supplier.
   ---Created:   11-DEC-07 Subhadeep Sahu
   ---------------------------------------------------------------------

   FUNCTION GET_SUP_PRIMARY_ADDR_SEQ_NO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_addr_seq_no     IN OUT   ADDR.SEQ_NO%TYPE,
                                        I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                        I_addr_type       IN       ADDR.ADDR_TYPE%TYPE) RETURN BOOLEAN;
   ---------------------------------------------------------------------
   -- Function Name : GET_LEAD_TIME
   -- Purpose       : Run a cursor to get the default item lead time
   ---------------------------------------------------------------------------

   FUNCTION GET_LEAD_TIME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_lead_time       IN OUT   SUPS.DEFAULT_ITEM_LEAD_TIME%TYPE,
                          I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   -------------------------------------------------------------------------
   -- Function Name : GET_BRACKET_COSTING_IND
   -- Purpose       : Retrieves the bracket indicator for the passed in supplier.
   ---------------------------------------------------------------------

   FUNCTION GET_BRACKET_COSTING_IND(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_bracket_ind     IN OUT   SUPS.BRACKET_COSTING_IND%TYPE,
                                    I_supplier        IN       SUPS.SUPPLIER%TYPE)RETURN BOOLEAN;
   ---------------------------------------------------------------------------
   -- Function Name : NEXT_SUPPLIER
   -- Purpose       : Returns the next available supplier from the
   --                 SUPPLIER_SEQUENCE sequence.
   -------------------------------------------------------------------------

   FUNCTION NEXT_SUPPLIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_supplier_id     IN OUT   SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------------------------------
   -- Function: GET SUPS
   -- Purpose : This function fetches all the attributes of supplier.
   --------------------------------------------------------------------------------------------

   FUNCTION GET_SUPS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_sups_record     IN OUT   SUPS%ROWTYPE,
                      I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------------------------------
   -- Function: CHECK_SUPPLIER_SITES
   -- Purpose : This function will return True if the input supplier is a supplier site.
   --------------------------------------------------------------------------------------------

   FUNCTION CHECK_SUPPLIER_SITES (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists          IN OUT   BOOLEAN,
                                  I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------------------------------
   -- Function: GET_SUPP_PARENT_DESC
   -- Purpose : This function will return the Supplier_parent and its description
   --           for the input supplier site.
   --------------------------------------------------------------------------------------------

   FUNCTION GET_SUPP_PARENT_DESC (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_supplier_parent_name   IN OUT   SUPS.SUP_NAME%TYPE,
                                  O_supplier_parent        IN OUT   SUPS.SUPPLIER_PARENT%TYPE,
                                  I_supplier               IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------------------------------
   -- Function: CHECK_ORG_UNIT_OPEN_PO_EXIST
   -- Purpose : This function will return True if a PO exists for the Supplier passed as input
   --           that is not closed for a location associated to the Org Unit Id passed as input
   --------------------------------------------------------------------------------------------

   FUNCTION CHECK_ORG_UNIT_OPEN_PO_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                          O_exists          IN OUT   BOOLEAN,
                                          I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                          I_org_unit_id     IN       ORG_UNIT.ORG_UNIT_ID%TYPE) RETURN BOOLEAN;
   --------------------------------------------------------------------------------------------
   -- Function: CHECK_SUPPLIER_SITE_EXIST
   -- Purpose : This function will return True if a supplier site is associated with the
   --           Supplier passed as input with status and DSD ind same as the values passed
   --           as input
   --------------------------------------------------------------------------------------------
   FUNCTION CHECK_SUPPLIER_SITE_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists          IN OUT   BOOLEAN,
                                       I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                       I_status          IN       SUPS.SUP_STATUS%TYPE   DEFAULT NULL,
                                       I_dsd_ind         IN       SUPS.DSD_IND%TYPE      DEFAULT NULL) RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------------- 
   -- Function: GET_IMPORT_ID
   -- Purpose : This function will return default importer/exporter and it's type for 
   --           the given supplier. Returns NULL if no importer/exporter mapped.
   -----------------------------------------------------------------------------------------------
   FUNCTION GET_IMPORT_ID (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_import_id       IN OUT   SUPS_IMP_EXP.IMPORT_ID%TYPE,
                           O_import_type     IN OUT   SUPS_IMP_EXP.IMPORT_TYPE%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------------
   -- Function: GET_ROUTING_LOC
   -- Purpose : This function will return default routing location for the given supplier.
   --           Returns NULL if no routing location mapped.
   -----------------------------------------------------------------------------------------------
   FUNCTION GET_ROUTING_LOC (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_routing_loc_id  IN OUT   SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_supplier        IN       SUPS.SUPPLIER%TYPE) RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------------
   -- Function: CHECK_SUP_IMPORT_EXIST
   -- Purpose : This function will check duplicate records for a supplier/importer
   --------------------------------------------------------------------------------------------
   FUNCTION CHECK_SUP_IMPORT_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                    I_importer        IN       SUPS_IMP_EXP.IMPORT_ID%TYPE)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------------
   -- Function: CHECK_RLOC_EXIST
   -- Purpose : This function will check duplicate records for a supplier/routing location
   --------------------------------------------------------------------------------------------
   FUNCTION CHECK_RLOC_EXIST (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_supplier        IN       SUPS.SUPPLIER%TYPE,
                              I_routing_loc     IN       SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------------
   -- Function: CHECK_ENT_IN_USE
   -- Purpose : This function will check if a supplier and import id is in use by a PO.
   -- If it is, it cannot be deleted.
   --------------------------------------------------------------------------------------------
   FUNCTION CHECK_ENT_IN_USE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   VARCHAR2,
                             I_import_id       IN       SUPS_IMP_EXP.IMPORT_ID%TYPE,
                             I_supplier        IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------------
   -- Function: CHECK_RL_IN_USE
   -- Purpose : This function will check if a supplier and routing location is in use by a PO.
   -- If it is, it cannot be deleted.
   --------------------------------------------------------------------------------------------
   FUNCTION CHECK_RL_IN_USE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   VARCHAR2,
                            I_routing_id      IN       SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE,
                            I_supplier        IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------------------
   -- Function: UPDATE_SUPPLIER_SITE
   -- Purpose : This function will update the status of supplier sites under a supplier 
   --           based on the status of the supplier.
   -------------------------------------------------------------------------------------------------------------------
   FUNCTION UPDATE_SUPPLIER_SITE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                  I_status          IN       SUPS.SUP_status%TYPE)
      RETURN BOOLEAN;
   --------------------------------------------------------------------------------------------------------------------
   -- Function: UPDATE_STATUS_UPD_BY_RMS
   -- Purpose : This function will update the status_upd_by_rms flag of supplier / supplier site when updated from RMS
   -------------------------------------------------------------------------------------------------------------------
   FUNCTION UPDATE_STATUS_UPD_BY_RMS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_supplier          IN       SUPS.SUPPLIER%TYPE,
                                      I_status            IN       SUPS.SUP_status%TYPE,
                                      I_old_status        IN       SUPS.SUP_STATUS%TYPE)
      RETURN BOOLEAN;
   --------------------------------------------------------------------------------------------------------------------
END SUPP_ATTRIB_SQL;
/
