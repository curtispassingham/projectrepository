CREATE OR REPLACE PACKAGE RMSMFM_ITEMS AUTHID CURRENT_USER IS

FAMILY      VARCHAR2(64) := 'items';

ROOT_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ItemDesc';
ITEM_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ItemHdrDesc';
ITEM_REF_MSG    CONSTANT  VARCHAR2(30) := 'ItemRef';
ISUP_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ItemSupDesc';
ISUP_REF_MSG    CONSTANT  VARCHAR2(30) := 'ItemSupRef';
ISC_DESC_MSG    CONSTANT  VARCHAR2(30) := 'ItemSupCtyDesc';
ISC_REF_MSG     CONSTANT  VARCHAR2(30) := 'ItemSupCtyRef';
ISMC_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ItemSupCtyMfrDesc';
ISMC_REF_MSG    CONSTANT  VARCHAR2(30) := 'ItemSupCtyMfrRef';
ISCD_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ISCDimDesc';
ISCD_REF_MSG    CONSTANT  VARCHAR2(30) := 'ISCDimRef';
UPC_DESC_MSG    CONSTANT  VARCHAR2(30) := 'ItemUPCDesc';
UPC_REF_MSG     CONSTANT  VARCHAR2(30) := 'ItemUPCRef';
BOM_DESC_MSG    CONSTANT  VARCHAR2(30) := 'ItemBOMDesc';
BOM_REF_MSG     CONSTANT  VARCHAR2(30) := 'ItemBOMRef';
UDAF_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ItemUDAFFDesc';
UDAF_REF_MSG    CONSTANT  VARCHAR2(30) := 'ItemUDAFFRef';
UDAD_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ItemUDADateDesc';
UDAD_REF_MSG    CONSTANT  VARCHAR2(30) := 'ItemUDADateRef';
UDAL_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ItemUDALOVDesc';
UDAL_REF_MSG    CONSTANT  VARCHAR2(30) := 'ItemUDALOVRef';
IMG_DESC_MSG    CONSTANT  VARCHAR2(30) := 'ItemImageDesc';
IMG_REF_MSG     CONSTANT  VARCHAR2(30) := 'ItemImageRef';
TCKT_DESC_MSG   CONSTANT  VARCHAR2(30) := 'ItemTcktDesc';
TCKT_REF_MSG    CONSTANT  VARCHAR2(30) := 'ItemTcktRef';

ITEM_ADD        CONSTANT  VARCHAR2(30) := 'ItemCre';
ITEM_UPD        CONSTANT  VARCHAR2(30) := 'ItemHdrMod';
ITEM_DEL        CONSTANT  VARCHAR2(30) := 'ItemDel';
ISUP_ADD        CONSTANT  VARCHAR2(30) := 'ItemSupCre';
ISUP_UPD        CONSTANT  VARCHAR2(30) := 'ItemSupMod';
ISUP_DEL        CONSTANT  VARCHAR2(30) := 'ItemSupDel';
ISC_ADD         CONSTANT  VARCHAR2(30) := 'ItemSupCtyCre';
ISC_UPD         CONSTANT  VARCHAR2(30) := 'ItemSupCtyMod';
ISC_DEL         CONSTANT  VARCHAR2(30) := 'ItemSupCtyDel';
ISMC_ADD        CONSTANT  VARCHAR2(30) := 'ISCMfrCre';
ISMC_UPD        CONSTANT  VARCHAR2(30) := 'ISCMfrMod';
ISMC_DEL        CONSTANT  VARCHAR2(30) := 'ISCMfrDel';
ISCD_ADD        CONSTANT  VARCHAR2(30) := 'ISCDimCre';
ISCD_UPD        CONSTANT  VARCHAR2(30) := 'ISCDimMod';
ISCD_DEL        CONSTANT  VARCHAR2(30) := 'ISCDimDel';
UPC_ADD         CONSTANT  VARCHAR2(30) := 'ItemUPCCre';
UPC_UPD         CONSTANT  VARCHAR2(30) := 'ItemUPCMod';
UPC_DEL         CONSTANT  VARCHAR2(30) := 'ItemUPCDel';
BOM_ADD         CONSTANT  VARCHAR2(30) := 'ItemBOMCre';
BOM_UPD         CONSTANT  VARCHAR2(30) := 'ItemBOMMod';
BOM_DEL         CONSTANT  VARCHAR2(30) := 'ItemBOMDel';
UDAF_ADD        CONSTANT  VARCHAR2(30) := 'ItemUDAFFCre';
UDAF_DEL        CONSTANT  VARCHAR2(30) := 'ItemUDAFFDel';
UDAD_ADD        CONSTANT  VARCHAR2(30) := 'ItemUDADateCre';
UDAD_DEL        CONSTANT  VARCHAR2(30) := 'ItemUDADateDel';
UDAL_ADD        CONSTANT  VARCHAR2(30) := 'ItemUDALOVCre';
UDAL_DEL        CONSTANT  VARCHAR2(30) := 'ItemUDALOVDel';
IMG_ADD         CONSTANT  VARCHAR2(30) := 'ItemImageCre';
IMG_UPD         CONSTANT  VARCHAR2(30) := 'ItemImageMod';
IMG_DEL         CONSTANT  VARCHAR2(30) := 'ItemImageDel';
TCKT_ADD        CONSTANT  VARCHAR2(30) := 'ItemTcktCre';
TCKT_DEL        CONSTANT  VARCHAR2(30) := 'ItemTcktDel';
RIH_ADD         CONSTANT  VARCHAR2(30) := 'RelItemHeadCre';
RIH_UPD         CONSTANT  VARCHAR2(30) := 'RelItemHeadMod';
RIH_DEL         CONSTANT  VARCHAR2(30) := 'RelItemHeadDel';
RID_ADD         CONSTANT  VARCHAR2(30) := 'RelItemDetCre';
RID_UPD         CONSTANT  VARCHAR2(30) := 'RelItemDetMod';
RID_DEL         CONSTANT  VARCHAR2(30) := 'RelItemDetDel';

TYPE ROWID_TBL   is TABLE OF ROWID;
TYPE SEQ_TBL     is TABLE OF ITEM_MFQUEUE.SEQ_NO%TYPE;
TYPE ITEM_TBL    is TABLE OF ITEM_MASTER.ITEM%TYPE;

TYPE ITEM_ROWID_REC is RECORD
(queue_rowid_tbl       ROWID_TBL,
 queue_seq_tbl         SEQ_TBL,
 pub_info_rowid_tbl    ROWID_TBL,
 pub_info_item_tbl     ITEM_TBL,
 queue_upd_rowid_tbl   ROWID_TBL,
 queue_upd_seq_tbl     SEQ_TBL,
 itemloc_rowid_tbl     ROWID_TBL,
 itemloc_seq_tbl       SEQ_TBL
);
TYPE bom_rectype IS RECORD
(pack_no                     VARCHAR2(25),
 seq_no                      NUMBER(4),
 item                        VARCHAR2(25),
 item_parent                 VARCHAR2(25),
 pack_tmpl_id                NUMBER(8),
 comp_pack_no                VARCHAR2(25),
 item_qty                    NUMBER(12,4),
 item_parent_pt_qty          NUMBER(12,4),
 comp_pack_qty               NUMBER(12,4),
 pack_item_qty               NUMBER(12,4));

TYPE bom_tabtype is TABLE of bom_rectype
     INDEX BY BINARY_INTEGER;

bom_table   bom_tabtype;
empty_bom   bom_tabtype;
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message       OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_queue_rec        IN       ITEM_MFQUEUE%ROWTYPE,
                I_sellable_ind     IN       ITEM_PUB_INFO.SELLABLE_IND%TYPE,
                I_tran_level_ind   IN       ITEM_PUB_INFO.TRAN_LEVEL_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code       OUT   VARCHAR2,
                 O_error_msg         OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_message_type      OUT   VARCHAR2,
                 O_message           OUT   RIB_OBJECT,
                 O_bus_obj_id        OUT   RIB_BUSOBJID_TBL,
                 O_routing_info      OUT   RIB_ROUTINGINFO_TBL,
                 I_num_threads    IN       NUMBER DEFAULT 1,
                 I_thread_val     IN       NUMBER DEFAULT 1);
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code        OUT   VARCHAR2,
                    O_error_msg          OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_message_type   IN  OUT   VARCHAR2,
                    O_message            OUT   RIB_OBJECT,
                    O_bus_obj_id     IN  OUT   RIB_BUSOBJID_TBL,
                    O_routing_info   IN  OUT   RIB_ROUTINGINFO_TBL,
                    I_ref_object     IN        RIB_OBJECT);
--------------------------------------------------------------------------------
END;
/