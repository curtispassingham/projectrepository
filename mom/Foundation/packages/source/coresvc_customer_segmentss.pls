CREATE OR REPLACE PACKAGE CORESVC_CUSTOMER_SEGMENTS AUTHID CURRENT_USER AS

   template_key                   CONSTANT VARCHAR2(255)         := 'CUSTOMER_SEGMENTS_DATA';
   template_category              CONSTANT VARCHAR2(255)         := 'RMSPCO';
   action_column                           VARCHAR2(255)         := 'ACTION';
   action_new                              VARCHAR2(25)          := 'NEW';
   action_mod                              VARCHAR2(25)          := 'MOD';
   action_del                              VARCHAR2(25)          := 'DEL';
   CUS_SMTS_sheet                          VARCHAR2(255)         := 'CUSTOMER_SEGMENTS';
   CUS_SMTS$Action                         NUMBER                :=1;
   CUS_SMTS$CUSTOMER_SEGMENT_ID            NUMBER                :=2;
   CUS_SMTS$CUSTOMER_SEGMENT_DESC          NUMBER                :=3;
   CUS_SMTS$CUSTOMER_SEGMENT_TYPE          NUMBER                :=4;

   CUS_SMTS_TL_sheet                       VARCHAR2(255)         := 'CUSTOMER_SEGMENTS_TL';
   CUS_SMTS_TL$Action                      NUMBER                :=1;
   CUS_SMTS_TL$Lang                        NUMBER                :=2;
   CUS_SMTS_TL$CUST_SEGMENT_ID             NUMBER                :=3;
   CUS_SMTS_TL$CUST_SEGMENT_DESC           NUMBER                :=4;
   
   cus_smt_ty_sheet                     VARCHAR2(255)            := 'CUSTOMER_SEGMENT_TYPES';
   cus_smt_ty$action                    NUMBER                   := 1;
   cus_smt_ty$cus_smt_ty_des            NUMBER                   := 3;
   cus_smt_ty$cus_smt_ty                NUMBER                   := 2;
   
   cus_smt_ty_tl_sheet                  VARCHAR2(255)            := 'CUSTOMER_SEGMENT_TYPES_TL';
   cus_smt_ty_tl$action                 NUMBER                   := 1;
   cus_smt_ty_tl$lang                   NUMBER                   := 2;
   cus_smt_ty_tl$cus_smt_ty             NUMBER                   := 3;
   cus_smt_ty_tl$cus_smt_ty_des         NUMBER                   := 4;


   Type CUS_SMTS_rec_tab IS TABLE OF CUSTOMER_SEGMENTS%ROWTYPE;
   Type cus_smt_ty_rec_tab IS TABLE OF CUSTOMER_SEGMENT_TYPES%ROWTYPE;
   sheet_name_trans    S9T_PKG.trans_map_typ;

   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;

   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;

   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;

   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;

END CORESVC_CUSTOMER_SEGMENTS;
/
