
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY VAR_UPC_SQL AS
-------------------------------------------------------------------------------------------
-- Function: GET_FORMAT_INFO
--  Purpose: Returns info for given format_id.
-------------------------------------------------------------------------------------------
FUNCTION GET_FORMAT_INFO(O_error_message     IN OUT  VARCHAR2,
                         O_valid             IN OUT  BOOLEAN,
                         O_format_desc       IN OUT  VAR_UPC_EAN.FORMAT_DESC%TYPE,
                         O_prefix_length     IN OUT  VAR_UPC_EAN.PREFIX_LENGTH%TYPE,
                         O_begin_item_digit  IN OUT  VAR_UPC_EAN.BEGIN_ITEM_DIGIT%TYPE,
                         O_begin_var_digit   IN OUT  VAR_UPC_EAN.BEGIN_VAR_DIGIT%TYPE,
                         O_check_digit       IN OUT  VAR_UPC_EAN.CHECK_DIGIT%TYPE,
                         O_default_prefix    IN OUT  VAR_UPC_EAN.DEFAULT_PREFIX%TYPE,
                         I_format_id         IN OUT  VAR_UPC_EAN.FORMAT_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'VAR_UPC_SQL.GET_FORMAT_INFO';

   cursor C_GET_INFO is
      select format_desc
           , prefix_length
           , begin_item_digit
           , begin_var_digit
           , check_digit
           , default_prefix
        from var_upc_ean
       where format_id = I_format_id;   

BEGIN
   O_valid := FALSE;

   open  C_GET_INFO;
   fetch C_GET_INFO into O_format_desc
                       , O_prefix_length
                       , O_begin_item_digit
                       , O_begin_var_digit
                       , O_check_digit
                       , O_default_prefix;
   if C_GET_INFO%FOUND then
      O_valid := TRUE;
   end if;
   close C_GET_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_FORMAT_INFO;
-------------------------------------------------------------------------------------------
END VAR_UPC_SQL;
/


