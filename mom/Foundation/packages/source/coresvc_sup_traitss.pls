-- File Name : CORESVC_SUP_TRAITS_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_SUP_TRAITS AUTHID CURRENT_USER AS
   
   template_key               CONSTANT VARCHAR2(255)      := 'SUP_TRAITS_DATA';
   template_category          CODE_DETAIL.CODE%TYPE       := 'RMSFND';
   action_new                 VARCHAR2(25)                := 'NEW';
   action_mod                 VARCHAR2(25)                := 'MOD';
   action_del                 VARCHAR2(25)                := 'DEL';
   sup_traits_sheet           VARCHAR2(255)               := 'SUP_TRAITS';
   sup_traits$action          NUMBER                      := 1;
   sup_traits$master_sup      NUMBER                      := 5;
   sup_traits$master_sup_ind  NUMBER                      := 4;
   sup_traits$description     NUMBER                      := 3;
   sup_traits$sup_trait       NUMBER                      := 2;
   
   sup_traits_tl_sheet           VARCHAR2(255)               := 'SUP_TRAITS_TL';
   sup_traits_tl$action          NUMBER                      := 1;
   sup_traits_tl$lang            NUMBER                      := 2;
   sup_traits_tl$sup_trait       NUMBER                      := 3;
   sup_traits_tl$description     NUMBER                      := 4;
   sheet_name_trans           S9T_PKG.trans_map_typ;
   action_column              VARCHAR2(255)               := 'ACTION';
   
   TYPE SUP_TRAITS_REC_TAB IS TABLE OF SUP_TRAITS%ROWTYPE;
   
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
END CORESVC_SUP_TRAITS;
/
