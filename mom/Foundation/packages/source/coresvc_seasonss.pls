-- File Name : CORESVC_SEASONS_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_SEASONS AUTHID CURRENT_USER AS
   template_key                     CONSTANT VARCHAR2(255):= 'SEASONS_DATA';
   template_category                CODE_DETAIL.CODE%TYPE := 'RMSITM';
   action_new                       VARCHAR2(25)          := 'NEW';
   action_mod                       VARCHAR2(25)          := 'MOD';
   action_del                       VARCHAR2(25)          := 'DEL';
   seasons_sheet                    VARCHAR2(255)         := 'SEASONS';
   seasons$action                   NUMBER                :=1;
   seasons$season_id                NUMBER                :=2;
   seasons$season_desc              NUMBER                :=3;
   seasons$start_date               NUMBER                :=4;
   seasons$end_date                 NUMBER                :=5;
   seasons$filter_org_id            NUMBER                :=6;
   seasons$filter_merch_id          NUMBER                :=7;
   seasons$filter_merch_id_class    NUMBER                :=8;
   seasons$filter_merch_id_sclass   NUMBER                :=9;
   
   seasons_tl_sheet                 VARCHAR2(255)         := 'SEASONS_TL';
   seasons_tl$action                NUMBER                :=1;
   seasons_tl$lang                  NUMBER                :=2;
   seasons_tl$season_id             NUMBER                :=3;
   seasons_tl$season_desc           NUMBER                :=4;

   sheet_name_trans                 S9T_PKG.TRANS_MAP_TYP;
   action_column                    VARCHAR2(255) := 'ACTION';

   TYPE SEASONS_rec_tab IS TABLE OF SEASONS%ROWTYPE;

   phases_sheet                     VARCHAR2(255)         := 'PHASES';
   phases$action                    NUMBER                :=1;
   phases$season_id                 NUMBER                :=2;
   phases$phase_id                  NUMBER                :=3;
   phases$phase_desc                NUMBER                :=4;
   phases$start_date                NUMBER                :=5;
   phases$end_date                  NUMBER                :=6;
   
   phases_tl_sheet                  VARCHAR2(255)         := 'PHASES_TL';
   phases_tl$action                 NUMBER                :=1;
   phases_tl$lang                   NUMBER                :=2;
   phases_tl$season_id              NUMBER                :=3;
   phases_tl$phase_id               NUMBER                :=4;
   phases_tl$phase_desc             NUMBER                :=5;
  
                                  
   TYPE PHASES_rec_tab IS TABLE OF PHASES%ROWTYPE;
--------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT s9t_folder.file_id%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_SEASONS;
/
