create or replace PACKAGE CORESVC_HALF AUTHID CURRENT_USER AS
   template_key                                  CONSTANT VARCHAR2(255):='HALF_DATA';
   action_new                                             VARCHAR2(25)          := 'NEW';
   action_mod                                             VARCHAR2(25)          := 'MOD';
   action_del                                             VARCHAR2(25)          := 'DEL';
   HALF_TL_sheet                                          VARCHAR2(255)         := 'HALF_TL';
   HALF_TL$Action                                         NUMBER                :=1;
   HALF_TL$HALF_NAME                                      NUMBER                :=4;
   HALF_TL$HALF_NO                                        NUMBER                :=3;
   HALF_TL$LANG                                           NUMBER                :=2;
   
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE := 'RMSADM';
   
   TYPE HALF_TL_rec_tab IS TABLE OF HALF_TL%ROWTYPE;
   HALF_sheet                                           VARCHAR2(255)         := 'HALF';
   HALF$Action                                          NUMBER                :=1;
   HALF$HALF_DATE                                       NUMBER                :=4;
   HALF$HALF_NAME                                       NUMBER                :=3;
   HALF$HALF_NO                                         NUMBER                :=2;

   TYPE HALF_rec_tab IS TABLE OF HALF%ROWTYPE;
   CALENDAR_sheet                                       VARCHAR2(255)         := 'CALENDAR';
   CALENDAR$Action                                      NUMBER                :=1;
   CALENDAR$NO_OF_WEEKS                                 NUMBER                :=5;
   CALENDAR$MONTH_454                                   NUMBER                :=4;
   CALENDAR$YEAR_454                                    NUMBER                :=3;
   CALENDAR$FIRST_DAY                                   NUMBER                :=2;

   TYPE CALENDAR_rec_tab IS TABLE OF CALENDAR%ROWTYPE;

---------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER
                        )
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER
                   )
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
END CORESVC_HALF;
/