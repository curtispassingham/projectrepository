CREATE OR REPLACE PACKAGE BODY CORESVC_STKLEDGR_INSERTS_SQL AS
  ------------------------------------------------------------------------------------------------
FUNCTION PROCESS_STKLEDGR_INSERTS(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_rms_async_id  IN RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
  RETURN BOOLEAN
IS
  L_counter NUMBER:=0;
BEGIN
  FOR rec IN
  (SELECT sli.location,
    sli.type_code,
    sli.dept,
    sli.class,
    sli.subclass,
    s.currency_code,
    sli.rowid
  FROM stock_ledger_inserts sli,
    store s
  WHERE sli.location = s.store
  AND sli.rms_async_id = I_rms_async_id
  UNION
  SELECT sli.location,
    sli.type_code,
    sli.dept,
    sli.class,
    sli.subclass,
    w.currency_code,
    sli.rowid
  FROM stock_ledger_inserts sli,
    wh w
  WHERE sli.location = w.wh
  AND sli.rms_async_id = I_rms_async_id
  AND NOT EXISTS
    (SELECT 'X' FROM wh WHERE wh.wh = sli.location AND wh.wh = wh.physical_wh
    )
  UNION
  SELECT sli.location,
    sli.type_code,
    sli.dept,
    sli.class,
    sli.subclass,
    NULL AS currency_code,
    sli.rowid
  FROM stock_ledger_inserts sli
  WHERE sli.type_code NOT IN ('S','W')
  AND sli.rms_async_id = I_rms_async_id
  ORDER BY type_code DESC
  )
  LOOP
    IF rec.type_code IN ('S','W','E') THEN
      IF STKLEDGR_INSERTS_SQL.ADD_LOC (rec.type_code, rec.location, rec.currency_code, L_counter, O_error_message)=false THEN
        RETURN false;
      END IF;
    ELSIF rec.type_code                                                                                    = 'B' THEN
      IF STKLEDGR_INSERTS_SQL.ADD_SUBCLASS (rec.dept, rec.class, rec.subclass, L_counter, O_error_message) = false THEN
        RETURN false;
      END IF;
    ELSIF rec.type_code                                                              = 'D' THEN
      IF STKLEDGR_INSERTS_SQL.ADD_DEPT_BUDGET (rec.dept, L_counter, O_error_message) = false THEN
        RETURN false;
      END IF;
    END IF;
    DELETE FROM stock_ledger_inserts WHERE rowid = rec.rowid;
  END LOOP;
  Return true;
END PROCESS_STKLEDGR_INSERTS;
------------------------------------------------------------------------------------------------
END CORESVC_STKLEDGR_INSERTS_SQL;
/
