CREATE OR REPLACE PACKAGE CORESVC_DIFFID_DIFFTYPE AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
   template_key                       CONSTANT VARCHAR2(255) := 'DIFFID_DIFFTYPE_DATA';
   template_category                  CONSTANT VARCHAR2(255) := 'RMSITM';
   action_column                      VARCHAR2(255)          := 'ACTION';
   action_new                         VARCHAR2(25)           := 'NEW';
   action_mod                         VARCHAR2(25)           := 'MOD';
   action_del                         VARCHAR2(25)           := 'DEL';

   DIFF_TYPE_SHEET                    VARCHAR2(255)          := 'DIFF_TYPE';
   DIFF_TYPE$ACTION                   NUMBER                 :=1;
   DIFF_TYPE$DIFF_TYPE_DESC           NUMBER                 :=3;
   DIFF_TYPE$DIFF_TYPE                NUMBER                 :=2;

   DIFF_TYPE_TL_SHEET                 VARCHAR2(255)          := 'DIFF_TYPE_TL';
   DIFF_TYPE_TL$ACTION                NUMBER                 :=1;
   DIFF_TYPE_TL$LANG                  NUMBER                 :=2;
   DIFF_TYPE_TL$DIFF_TYPE             NUMBER                 :=3;
   DIFF_TYPE_TL$DIFF_TYPE_DESC        NUMBER                 :=4;

   DIFF_TYPE_CFA_EXT_sheet            VARCHAR2(255)          := 'DIFF_TYPE_CFA_EXT';
   DIFF_TYPE_CFA_EXT$ACTION           NUMBER                 :=1;
   DIFF_TYPE_CFA_EXT$DIFF_TYPE        NUMBER                 :=2;
   DIFF_TYPE_CFA_EXT$GROUP_ID         NUMBER                 :=3;
   DIFF_TYPE_CFA_EXT$VARCHAR2_1       NUMBER                 :=4;
   DIFF_TYPE_CFA_EXT$VARCHAR2_2       NUMBER                 :=5;
   DIFF_TYPE_CFA_EXT$VARCHAR2_3       NUMBER                 :=6;
   DIFF_TYPE_CFA_EXT$VARCHAR2_4       NUMBER                 :=7;
   DIFF_TYPE_CFA_EXT$VARCHAR2_5       NUMBER                 :=8;
   DIFF_TYPE_CFA_EXT$VARCHAR2_6       NUMBER                 :=9;
   DIFF_TYPE_CFA_EXT$VARCHAR2_7       NUMBER                 :=10;
   DIFF_TYPE_CFA_EXT$VARCHAR2_8       NUMBER                 :=11;
   DIFF_TYPE_CFA_EXT$VARCHAR2_9       NUMBER                 :=12;
   DIFF_TYPE_CFA_EXT$VARCHAR2_10      NUMBER                 :=13;
   DIFF_TYPE_CFA_EXT$NUMBER_11        NUMBER                 :=14;
   DIFF_TYPE_CFA_EXT$NUMBER_12        NUMBER                 :=15;
   DIFF_TYPE_CFA_EXT$NUMBER_13        NUMBER                 :=16;
   DIFF_TYPE_CFA_EXT$NUMBER_14        NUMBER                 :=17;
   DIFF_TYPE_CFA_EXT$NUMBER_15        NUMBER                 :=18;
   DIFF_TYPE_CFA_EXT$NUMBER_16        NUMBER                 :=19;
   DIFF_TYPE_CFA_EXT$NUMBER_17        NUMBER                 :=20;
   DIFF_TYPE_CFA_EXT$NUMBER_18        NUMBER                 :=21;
   DIFF_TYPE_CFA_EXT$NUMBER_19        NUMBER                 :=22;
   DIFF_TYPE_CFA_EXT$NUMBER_20        NUMBER                 :=23;
   DIFF_TYPE_CFA_EXT$DATE_21          NUMBER                 :=24;
   DIFF_TYPE_CFA_EXT$DATE_22          NUMBER                 :=25;
   DIFF_TYPE_CFA_EXT$DATE_23          NUMBER                 :=26;
   DIFF_TYPE_CFA_EXT$DATE_24          NUMBER                 :=27;
   DIFF_TYPE_CFA_EXT$DATE_25          NUMBER                 :=28;

   DIFF_IDS_sheet                     VARCHAR2(255)          := 'DIFF_IDS';
   DIFF_IDS$Action                    NUMBER                 :=1;
   DIFF_IDS$INDUSTRY_SUBGROUP         NUMBER                 :=6;
   DIFF_IDS$INDUSTRY_CODE             NUMBER                 :=5;
   DIFF_IDS$DIFF_DESC                 NUMBER                 :=4;
   DIFF_IDS$DIFF_TYPE                 NUMBER                 :=3;
   DIFF_IDS$DIFF_ID                   NUMBER                 :=2;

   DIFF_IDS_TL_sheet                  VARCHAR2(255)          := 'DIFF_IDS_TL';
   DIFF_IDS_TL$Action                 NUMBER                 :=1;
   DIFF_IDS_TL$LANG                   NUMBER                 :=2;
   DIFF_IDS_TL$DIFF_ID                NUMBER                 :=3;
   DIFF_IDS_TL$DIFF_DESC              NUMBER                 :=4;
   
   TYPE DIFF_TYPE_rec_tab is TABLE OF DIFF_TYPE%ROWTYPE;
   sheet_name_trans S9T_PKG.trans_map_typ;
--------------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------------------
END CORESVC_DIFFID_DIFFTYPE;
/
