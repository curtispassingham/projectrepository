CREATE OR REPLACE PACKAGE DIFF_DIST_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_RATIO
-- Purpose:       Validates the entered diff ratio.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RATIO (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diff_ratio_desc IN OUT DIFF_RATIO_HEAD.DESCRIPTION%TYPE,
                         O_valid           IN OUT BOOLEAN,
                         I_diff_ratio      IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                         I_diff_group_1    IN     DIFF_RATIO_HEAD.DIFF_GROUP_1%TYPE)
                         RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_RATIO_STORE
-- Purpose:       Validates that the entered store exists for the entered diff ratio.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RATIO_STORE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_store_name    IN OUT STORE.STORE_NAME%TYPE,
                              I_diff_ratio    IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                              I_store         IN     STORE.STORE%TYPE)
                              RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: APPLY_RATIO
-- Purpose:       Applies the pct's from a diff ratio to applied diffs.
---------------------------------------------------------------------------------------------
FUNCTION APPLY_RATIO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_ratio         IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                     I_store         IN     DIFF_RATIO_DETAIL.STORE%TYPE) 
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: APPLY_ALL
-- Purpose:       Moves all displayed records from unapplied status to applied.
---------------------------------------------------------------------------------------------
FUNCTION APPLY_ALL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_diff_range    IN     DIFF_RANGE_HEAD.DIFF_RANGE%TYPE)
                   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: REMOVE_ALL
-- Purpose:       Moves all records from applied status to unapplied.
---------------------------------------------------------------------------------------------
FUNCTION REMOVE_ALL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
                    RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: POP_DIFF_APPLY_TEMP
-- Purpose:       Populates the diff_apply_temp table when the diff dist form is opened.
---------------------------------------------------------------------------------------------
FUNCTION POP_DIFF_APPLY_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_diff_group    IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
                             RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_APPLIED_RECS
-- Purpose:       Based on the dist type, this function will ensure the qty, pct or ratio columns
--                are all populated for applied diffs on diff_apply_temp
---------------------------------------------------------------------------------------------
FUNCTION CHECK_APPLIED_RECS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid         IN OUT BOOLEAN,
                            I_dist_type     IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_UPDATE_ORDER_RECS
-- Purpose:       This function will update any existing ordloc_wksht records and insert needed ones
---------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_ORDER_RECS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_dist_uom_type IN     VARCHAR2,
                                  I_sum_ratio     IN     NUMBER,
                                  I_dist_type     IN     VARCHAR2,
                                  I_dist_uom      IN     UOM_CLASS.UOM%TYPE,
                                  I_purch_uom     IN     UOM_CLASS.UOM%TYPE,
                                  I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                                  I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                                  I_diff_no       IN     NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_UPDATE_CONTRACT_RECS
-- Purpose:       This function will update any existing contract_matrix_temp records and insert needed ones
---------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_CONTRACT_RECS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_sum_ratio     IN     NUMBER,
                                     I_dist_type     IN     VARCHAR2,
                                     I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                                     I_contract_no   IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                                     I_diff_no       IN     NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_TEMP_TABLES
-- Purpose:       This function will delete from ordloc_wksht, contract_matrix_temp, temp_pack_tmpl
--                for the inputted identifier.  It will also delete from diff_apply_temp
---------------------------------------------------------------------------------------------
FUNCTION DELETE_TEMP_TABLES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                            I_contract_no   IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                            I_pack_tmpl_id  IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                            I_diff_no       IN     NUMBER,
                            I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE)
 RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  DELETE_ORDDIST_ITEM_TEMP
--Purpose      :  This will delete all records on orddist_item_temp for the entered order or contract
-------------------------------------------------------------------------------
FUNCTION DELETE_ORDDIST_ITEM_TEMP(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_order             IN     ORDHEAD.ORDER_NO%TYPE,
                                  I_contract          IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  CREATE_NEW_DIFF_CHILDREN
--Purpose      :  This will create the new items as needed from the distlist form.
-------------------------------------------------------------------------------
FUNCTION CREATE_NEW_DIFF_CHILDREN(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_order             IN     ORDHEAD.ORDER_NO%TYPE,
                                  I_contract          IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  DELETE_PARENT_DIFF
--Purpose      :  This will delete any rejected parent/diff combinations from the 
--                ordloc_wksht or contract_matrix_temp table.
-------------------------------------------------------------------------------
FUNCTION DELETE_PARENT_DIFF(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order             IN     ORDHEAD.ORDER_NO%TYPE,
                            I_contract          IN     CONTRACT_HEADER.CONTRACT_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  ORDDIST_ITEM_TEMP_EXIST
--Purpose      :  This will check if an item number already exists on orddist_item_temp 
-------------------------------------------------------------------------------
FUNCTION ORDDIST_ITEM_TEMP_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exist             IN OUT BOOLEAN,
                                 I_order             IN     ORDHEAD.ORDER_NO%TYPE,
                                 I_contract          IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                                 I_item              IN     ORDDIST_ITEM_TEMP.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  APPLY_ALL_QTY
--Purpose      :  This will apply diff range to diff_apply_temp table 
-------------------------------------------------------------------------------
FUNCTION APPLY_ALL_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diff_range      IN       DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
                       I_diff_group      IN       DIFF_RANGE_HEAD.DIFF_GROUP_1%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
--Function Name:  DELETE_DIFF_APPLY_TEMP
--Purpose      :  This will delete records from diff_apply_temp depending on status 
---------------------------------------------------------------------------------
FUNCTION DELETE_DIFF_APPLY_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_status          IN       DIFF_APPLY_TEMP.STATUS%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------

END DIFF_DIST_SQL;
/