CREATE OR REPLACE PACKAGE BODY CORESVC_CUSTCREDITCHK IS
--------------------------------------------------------------------------------
--FUNCTION NAME : VALIDATE_CUSTCREDITCHK
--PURPOSE       : Validates the input data and updates the error messages.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CUSTCREDITCHK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN     SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                                I_chunk_id        IN     SVC_CUSTCREDITCHK.CHUNK_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--FUNCTION NAME : PERSIST_CUSTCREDITCHK
--PURPOSE       : Persists the validated records from the staging into the wf_customer table.
-------------------------------------------------------------------------------- 
FUNCTION PERSIST_CUSTCREDITCHK(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN     SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                               I_chunk_id        IN     SVC_CUSTCREDITCHK.CHUNK_ID%TYPE)
   RETURN BOOLEAN;

FUNCTION CREATE_CREDIT_IND(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id    IN      SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                           I_chunk_id      IN      SVC_CUSTCREDITCHK.CHUNK_ID%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64)             := 'CORESVC_CUSTCREDITCHK.CREATE_CREDIT_IND';

BEGIN
   
   if CORESVC_CUSTCREDITCHK.VALIDATE_CUSTCREDITCHK(O_error_message,
                                                   I_process_id,
                                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;
   ---
   if CORESVC_CUSTCREDITCHK.PERSIST_CUSTCREDITCHK(O_error_message,
                                                  I_process_id,
                                                  I_chunk_id) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_CREDIT_IND;
--------------------------------------------------------------------------------     
FUNCTION VALIDATE_CUSTCREDITCHK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN     SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                                I_chunk_id        IN     SVC_CUSTCREDITCHK.CHUNK_ID%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64)             := 'CORESVC_CUSTCREDITCHK.VALIDATE_CUSTCREDITCHK';
   L_count_err        NUMBER;
   L_table            VARCHAR2(30)             := 'SVC_CUSTCREDITCHK';

   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_CHECK_ERR is
      select count(1)
        from svc_custcreditchk
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_ERROR;
         
   cursor C_LOCK_SVC_CUSTCREDITCHK is
      select 'x'
        from svc_custcreditchk
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         for update nowait;


BEGIN

   open C_LOCK_SVC_CUSTCREDITCHK;
   close C_LOCK_SVC_CUSTCREDITCHK;

   --Validate Credit_Ind Value             
   update svc_custcreditchk scc
      set process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_ERROR,
          error_msg = error_msg || SQL_LIB.GET_MESSAGE_TEXT('INV_CREDIT_VALUE',scc.wf_customer_id,scc.wf_customer_group_id,NULL) || ';'
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and credit_ind not in ('Y','N');

   -- Validating CUSTOMER_ID
   update svc_custcreditchk scc
      set process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_ERROR,
          error_msg = error_msg || SQL_LIB.GET_MESSAGE_TEXT('INVALID_CUST_ID',scc.wf_customer_id,scc.wf_customer_group_id,NULL) || ';'
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and not exists (select 'x'
                        from wf_customer wc
                       where wc.wf_customer_id = scc.wf_customer_id
                         and rownum = 1);

   -- Validating CUSTOMER_GROUP_ID
   
   update svc_custcreditchk scc
      set process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_ERROR,
          error_msg = error_msg ||  SQL_LIB.GET_MESSAGE_TEXT('INVALID_CUST_GROUP',scc.wf_customer_id,scc.wf_customer_group_id,NULL) || ';'
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and not exists (select 'x'
                        from wf_customer_group wc
                       where wc.wf_customer_group_id = scc.wf_customer_group_id
                         and rownum = 1);

   --Validating CUSTOMER_ID and CUSTOMER_GROUP_ID combination.
   --Only the records with valid CUSTOMER_ID and CUSTOMER_GROUP_ID need to be validated.
   
   update svc_custcreditchk scc
      set process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_ERROR,
          error_msg = error_msg || SQL_LIB.GET_MESSAGE_TEXT('INV_CUST_ID_CUST_GROUP',scc.wf_customer_id,scc.wf_customer_group_id,NULL) || ';'
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_NEW
      and not exists (select 'x'
                        from wf_customer wc
                       where scc.wf_customer_id = wc.wf_customer_id
                         and scc.wf_customer_group_id = wc.wf_customer_group_id
                         and rownum = 1);

   --Checking that no multiple records exist in the staging table.
   --Only the records with valid CUSTOMER_ID and CUSTOMER_GROUP_ID need to be validated.

   update svc_custcreditchk scc
      set process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_ERROR,
          error_msg = error_msg || SQL_LIB.GET_MESSAGE_TEXT('MULT_RECORDS_FOUND',scc.wf_customer_id,scc.wf_customer_group_id,NULL) || ';'
    where exists (select 'x'
                    from svc_custcreditchk inner
                   where inner.wf_customer_id = scc.wf_customer_id
                     and inner.wf_customer_group_id = scc.wf_customer_group_id
                     and inner.process_id = I_process_id
                     and inner.chunk_id = I_chunk_id
                  having count(*) > 1)
      and process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_NEW
      and scc.process_id = I_process_id
      and scc.chunk_id = I_chunk_id;

   --Update the process status for the remaining valid records.

   update svc_custcreditchk
      set process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_VALIDATED
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_NEW;

   open C_CHECK_ERR;
   fetch C_CHECK_ERR into L_count_err;
   close C_CHECK_ERR;

   if L_count_err != 0 then
      return FALSE;
   else
      return TRUE;
   end if;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
         return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_CUSTCREDITCHK;
--------------------------------------------------------------------------------
FUNCTION PERSIST_CUSTCREDITCHK(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN     SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                               I_chunk_id        IN     SVC_CUSTCREDITCHK.CHUNK_ID%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(64)             := 'CORESVC_CUSTCREDITCHK.PERSIST_CUSTCREDITCHK';
   L_table            VARCHAR2(30)             := 'SVC_CUSTCREDITCHK';

   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_TABLES is
      select 'x'
        from wf_customer wc,svc_custcreditchk scc
       where wc.wf_customer_id = scc.wf_customer_id
         and wc.wf_customer_group_id = scc.wf_customer_group_id
         and scc.process_id = I_process_id
         and scc.chunk_id = I_chunk_id
         and scc.process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_VALIDATED
         for update nowait; 
    
BEGIN

   open C_LOCK_TABLES;
   close C_LOCK_TABLES;

   merge into wf_customer wc using svc_custcreditchk scc
      on (    wc.wf_customer_id = scc.wf_customer_id
          and wc.wf_customer_group_id = scc.wf_customer_group_id
          and scc.process_id = I_process_id
          and scc.chunk_id = I_chunk_id
          and scc.process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_VALIDATED)
    when matched then update
     set wc.credit_ind = scc.credit_ind;

     update svc_custcreditchk scc
        set scc.process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_COMPLETED
        where scc.process_id = I_process_id
          and scc.chunk_id = I_chunk_id
          and scc.process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_VALIDATED;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
         return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE; 
END PERSIST_CUSTCREDITCHK; 
END CORESVC_CUSTCREDITCHK;
/