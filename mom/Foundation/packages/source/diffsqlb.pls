
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY DIFF_SQL AS
FUNCTION GET_DIFF_INFO_BASED_ON_TYPE ( O_error_message IN OUT VARCHAR2,
                                       O_description   IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                                       O_diff_type     IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                                       O_id_group_ind  IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                                       I_id            IN     V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE) return BOOLEAN IS

   L_program       VARCHAR2(64) := 'GET_DIFF_INFO_BASED_ON_TYPE';

   cursor C_INFO is
      select description, diff_type, id_group_ind
        from v_diff_id_group_type
       where id_group = I_id;

BEGIN

   if I_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_id',
                                            'NULL',
                                            'ID');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   open C_INFO;
   SQL_LIB.SET_MARK('FETCH','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   fetch C_INFO into O_description, O_diff_type, O_id_group_ind;
   ---
   if C_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
      close C_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DIFF',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   close C_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END GET_DIFF_INFO_BASED_ON_TYPE;
--------------------------------------------------------------------------
FUNCTION GET_DIFF_INFO (O_error_message IN OUT VARCHAR2,
                        O_description   IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                        O_diff_type     IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                        O_id_group_ind  IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                        I_id            IN     V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE) return BOOLEAN IS

   L_program       VARCHAR2(64) := 'DIFF_SQL.GET_DIFF_INFO';


   cursor C_INFO is
      select   ditl.diff_desc,
               di.diff_type,
               'ID'
        from   v_diff_group_head vdgh,
               diff_group_detail dgd,
               diff_ids          di,
               v_diff_ids_tl     ditl
       where   di.diff_id        = I_id
         and   di.diff_id        = dgd.diff_id
         and   dgd.diff_group_id = vdgh.diff_group_id
         and   ditl.diff_id      = di.diff_id
       union
      select   ditl.diff_desc,
               di.diff_type,
               'ID'
        from   diff_ids  di ,
               v_diff_ids_tl  ditl 
       where   di.diff_id not in (select dgd.diff_id 
                                 from v_diff_group_head vdgh,
                                      diff_group_detail dgd
                                where dgd.diff_group_id = vdgh.diff_group_id)
         and   di.diff_id = I_id 
         and   ditl.diff_id =di.diff_id
       union
      select   vdgh.diff_group_desc,
               vdgh.diff_type,
               'GROUP'
        from   v_diff_group_head  vdgh
       where   vdgh.diff_group_id = I_id;

BEGIN

   if I_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_id',
                                            'NULL',
                                            'ID');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   open C_INFO;
   SQL_LIB.SET_MARK('FETCH','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   fetch C_INFO into O_description, O_diff_type, O_id_group_ind;
   ---
   if C_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
      close C_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DIFF',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   close C_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END GET_DIFF_INFO;
--------------------------------------------------------------------------
FUNCTION DIFF_ID_IN_GROUP (O_error_message  IN OUT   VARCHAR2,
                           O_exists         IN OUT   BOOLEAN,
                           I_diff_grp       IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                           I_diff_id        IN       DIFF_IDS.DIFF_ID%TYPE) return BOOLEAN IS

   L_program       VARCHAR2(64) := 'DIFF_SQL.DIFF_ID_IN_GROUP';
   L_dummy         VARCHAR2(1);

   cursor C_CHECK_GROUPS is
      select 'x'
        from diff_group_detail
       where diff_id = I_diff_id
         and diff_group_id = I_diff_grp
         and rownum = 1;  --- added as result of API tkprof for XITEM API performance

BEGIN
   if I_diff_grp is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_diff_grp',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_diff_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_diff_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_GROUPS','DIFF_GROUP_DETAIL','Diff ID: '||I_diff_id||', Diff Group ID: '||I_diff_grp);
   open C_CHECK_GROUPS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_GROUPS','DIFF_GROUP_DETAIL','Diff ID: '||I_diff_id||', Diff Group ID: '||I_diff_grp);
   fetch C_CHECK_GROUPS into L_dummy;
   ---
   if C_CHECK_GROUPS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_GROUPS','DIFF_GROUP_DETAIL','Diff ID: '||I_diff_id||', Diff Group ID: '||I_diff_grp);
   close C_CHECK_GROUPS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END DIFF_ID_IN_GROUP;
--------------------------------------------------------------------------
FUNCTION CHILDREN_EXIST(O_error_message  IN OUT   VARCHAR2,
                        O_children_exist IN OUT   BOOLEAN,
                        I_item           IN       ITEM_MASTER.ITEM%TYPE) return BOOLEAN IS

   L_program    VARCHAR2(64) := 'DIFF_SQL.CHILDREN_EXIST';
   L_dummy      VARCHAR2(1);

   cursor C_CHILDREN_EXIST is
      select 'x'
        from item_master
       where (item_parent = I_item
          or item_grandparent = I_item)
         and (diff_1 is not NULL
          or diff_2 is not NULL);

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_CHILDREN_EXIST','ITEM_MASTER','Item: '||I_item);
   open C_CHILDREN_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_CHILDREN_EXIST','ITEM_MASTER','Item: '||I_item);
   fetch C_CHILDREN_EXIST into L_dummy;
   ---
   if C_CHILDREN_EXIST%NOTFOUND then
      O_children_exist := FALSE;
   else
      O_children_exist := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHILDREN_EXIST','ITEM_MASTER','Item: '||I_item);
   close C_CHILDREN_EXIST;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END CHILDREN_EXIST;
--------------------------------------------------------------------------
FUNCTION GET_DIFF_TYPE (O_error_message IN OUT   VARCHAR2,
                        O_diff_type     IN OUT   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                        I_id            IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE) return BOOLEAN IS

   L_program       VARCHAR2(64) := 'DIFF_SQL.GET_DIFF_INFO';

   cursor C_TYPE is
      select diff_type
        from v_diff_id_group_type
       where id_group = I_id;

BEGIN

   if I_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_id',
                                            'NULL',
                                            'ID');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_TYPE','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   open C_TYPE;
   SQL_LIB.SET_MARK('FETCH','C_TYPE','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   fetch C_TYPE into O_diff_type;
   ---
   if C_TYPE%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_TYPE','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
      close C_TYPE;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DIFF',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_TYPE','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   close C_TYPE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END GET_DIFF_TYPE;

--------------------------------------------------------------------------
--     Name: GET_DIFF_INFO
--  Purpose: Retrieve id_group_ind, diff_type, diff_type_desc, diff_description
--------------------------------------------------------------------------
FUNCTION GET_DIFF_INFO (O_error_message   IN OUT   VARCHAR2,
                        O_diff_desc       IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                        O_diff_type_desc  IN OUT   DIFF_TYPE.DIFF_TYPE_DESC%TYPE,
                        O_diff_type       IN OUT   V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                        O_id_group_ind    IN OUT   V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                        I_id              IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE) 
return BOOLEAN is

   L_program       VARCHAR2(64) := 'DIFF_SQL.GET_DIFF_INFO';

   cursor C_INFO is
      SELECT v.description, 
             dt.diff_type_desc,
             v.diff_type, 
             v.id_group_ind
        FROM v_diff_id_group_type v,
             v_diff_type_tl       dt
       WHERE v.id_group  = I_id
         and v.diff_type = dt.diff_type;


BEGIN

   if I_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_id',
                                            'NULL',
                                            'ID');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   open C_INFO;
   SQL_LIB.SET_MARK('FETCH','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   fetch C_INFO into O_diff_desc, 
                     O_diff_type_desc,
                     O_diff_type, 
                     O_id_group_ind;
   SQL_LIB.SET_MARK('CLOSE','C_INFO','V_DIFF_ID_GROUP_TYPE','ID: '||I_id);
   close C_INFO;
   ---
   if O_diff_desc is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DIFF',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   
 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END GET_DIFF_INFO;
--------------------------------------------------------------------------
FUNCTION GET_DIFF_DESC (O_error_message IN OUT VARCHAR2,
                        O_description   IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                        I_diff_id       IN     DIFF_IDS.DIFF_ID%TYPE) return BOOLEAN IS

   L_program       VARCHAR2(64) := 'DIFF_SQL.GET_DIFF_DESC';


   cursor C_DESC is
      select  diff_desc
        from  v_diff_ids_tl 
       where  diff_id = I_diff_id;

BEGIN
/* This function returns the description of a diff_id and should only be used
 * in instances where security policy has already validated Diff. */
 
   if I_diff_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_id',
                                            'NULL',
                                            'ID');
      return FALSE;
   end if;

   open C_DESC;
   fetch C_DESC into O_description;
   ---
   if C_DESC%NOTFOUND then
      close C_DESC;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DIFF',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   close C_DESC;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;

END GET_DIFF_DESC;
--------------------------------------------------------------------------
END DIFF_SQL;
/
SHO ERR
