CREATE OR REPLACE PACKAGE CORESVC_STKLEDGR_INSERTS_SQL AUTHID CURRENT_USER AS
  ------------------------------------------------------------------------------------------------
  FUNCTION PROCESS_STKLEDGR_INSERTS(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_rms_async_id  IN RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------------------------
END CORESVC_STKLEDGR_INSERTS_SQL;
/
