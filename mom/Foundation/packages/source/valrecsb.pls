CREATE OR REPLACE PACKAGE BODY VALIDATE_RECORDS_SQL AS
--------------------------------------------------------------------
FUNCTION DEL_ITEM(I_key_value       IN      VARCHAR2,
                  O_relations_exist IN OUT  VARCHAR2,
                  L_error_message   IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

   L_dummy                     VARCHAR2(1);
   L_num_sellables             NUMBER;
   L_found                     BOOLEAN;
   L_delete_sellable           BOOLEAN;
   L_sellable_deletable        BOOLEAN;
   L_nwp_exixts                VARCHAR2(1);
   L_single_sellable           ITEM_MASTER.ITEM%TYPE;
   L_program                   VARCHAR2(255) := 'VALIDATE_RECORDS_SQL.DEL_ITEM';

   cursor C_ITEM_LOC_SOH is
      select /*+ first_rows(1) */ 'x'
        from item_loc_soh
       where item = I_key_value
         and stock_on_hand != 0
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from item_loc_soh
       where item_parent = I_key_value
         and stock_on_hand != 0
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from item_loc_soh
       where item_grandparent = I_key_value
         and stock_on_hand != 0
         and rownum = 1;

   cursor C_PACKITEM is
      select /*+ first_rows(1) */ 'x'
        from packitem_breakout pib,
             item_master im
       where im.item = pib.item
         and im.item = I_key_value
         and rownum = 1
       UNION
      select /*+ first_rows(1) */ 'x'
        from packitem pi,
             item_master im
       where im.item = pi.item
         and im.item = I_key_value
         and im.pack_ind = 'Y'
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from packitem_breakout pib,
             item_master im
       where im.item = pib.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from packitem_breakout pib,
             item_master im
       where im.item = pib.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_ITEM_LOC_TRAITS is
      select /*+ first_rows(1) */ 'x'
        from item_loc_traits ilt,
             item_master im
       where im.item = ilt.natl_brand_comp_item
         and im.item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from item_loc_traits ilt,
             item_master im
       where im.item = ilt.natl_brand_comp_item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from item_loc_traits ilt,
             item_master im
       where im.item = ilt.natl_brand_comp_item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_ITEM_LOC is
      select 'x'
        from item_loc
       where primary_variant = I_key_value;

   cursor C_ORDLOC_WKSHT is
      select /*+ first_rows(1) */ 'x'
        from ordloc_wksht olw
       where item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from ordloc_wksht olw
       where item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from ordloc_wksht olw
       where ref_item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from ordloc_wksht olw,
             item_master im
       where im.item = olw.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_ORDERS_EXIST is
      select /*+ first_rows(1) */ 'x'
        from ordsku osk
       where osk.ref_item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from ordsku osk,
             item_master im
       where im.item = osk.item
         and im.item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from ordsku osk,
             item_master im
       where im.item = osk.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from ordsku osk,
             item_master im
       where im.item = osk.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_ALC_HEAD is
      select 'x'
        from alc_head ah,
             item_master im
       where im.item = ah.item
         and im.item = I_key_value
         and rownum = 1
         UNION ALL
      select 'x'
        from alc_head ah,
             item_master im
       where im.item = ah.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select 'x'
        from alc_head ah,
             item_master im
       where im.item = ah.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_ORDLOC_EXP is
      select 'x'
        from ordloc_exp ole,
             item_master im
       where im.item = ole.item
         and ole.item = I_key_value
         and rownum = 1
       UNION ALL
      select 'x'
        from ordloc_exp ole,
             item_master im
       where im.item = ole.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select 'x'
        from ordloc_exp ole,
             item_master im
       where im.item = ole.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_TRANSFER is
     select /*+ ordered */ 'x'
        from item_master im,
             tsfdetail tdt
       where im.item = tdt.item
         and im.item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ ordered */ 'x'
        from item_master im,
             tsfdetail tdt
         where im.item = tdt.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ ordered */ 'x'
        from item_master im,
             tsfdetail tdt
       where im.item = tdt.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_MRT_ITEM is
      select /*+ first_rows(1) */ 'x'
        from mrt_item mri,
             item_master im
       where im.item = mri.item
         and im.item = I_key_value
         and rownum = 1;

   cursor C_CONTENTS_ITEM is
      select /*+ first_rows(1) */ 'x'
        from item_master im
       where im.container_item = I_key_value
         and rownum = 1;

      cursor C_INV_ADJ is
      select 'x'
        from item_master im
       where (im.item = I_key_value
              or im.item_parent = I_key_value
              or im.item_grandparent = I_key_value)
         and rownum = 1
         and exists (select 'x'
                       from inv_adj ivj
                      where ivj.item = im.item
                        and rownum = 1);

   cursor C_RTV_DETAIL is
      select /*+ first_rows(1) */ 'x'
        from rtv_detail rdt,
             item_master im
       where im.item = rdt.item
         and im.item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from rtv_detail rdt,
             item_master im
       where im.item = rdt.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from rtv_detail rdt,
             item_master im
       where im.item = rdt.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_ALLOC_HEADER is
      select /*+ first_rows(1) */ 'x'
        from alloc_header alh,
             item_master im
       where im.item = alh.item
         and im.item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from alloc_header alh,
             item_master im
       where im.item = alh.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from alloc_header alh,
             item_master im
       where im.item = alh.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_STAKE_SKU_LOC is
            select 'x'
        from dual
       where exists (select 'x'
                       from stake_sku_loc ssl,
                            item_master im
                      where im.item = ssl.item
                        and im.item = I_key_value
                        and rownum = 1)
          or exists (select /*+ ORDERED USE_NL(IM SSL) */
                            'x'
                       from item_master im,
                            stake_sku_loc ssl
                      where im.item = ssl.item
                        and im.item_parent = I_key_value
                        and rownum = 1)
          or exists (select /*+ ORDERED USE_NL(IM SSL) */
                            'x'
                       from item_master im,
                            stake_sku_loc ssl
                      where im.item = ssl.item
                        and im.item_grandparent = I_key_value
                        and rownum = 1);

   cursor C_CONTRACT_EXISTS is
      select 'x'
        from contract_cost
       where item             = I_key_value
          or item_parent      = I_key_value
          or item_grandparent = I_key_value;

   cursor C_CONTRACT_IND is
      select contract_ind
        from system_options;

   cursor C_INVC_DETAIL is
      select /*+ first_rows(1) */ 'x'
        from invc_detail ind
       where ind.ref_item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from invc_detail ind,
             item_master im
       where im.item = ind.item
         and im.item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from invc_detail ind,
             item_master im
       where im.item = ind.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from invc_detail ind,
             item_master im
       where im.item = ind.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_INV_STATUS_QTY is
      select /*+ first_rows(1) */ 'x'
        from inv_status_qty isq,
             item_master im
       where im.item = isq.item
         and im.item = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from inv_status_qty isq,
             item_master im
       where im.item = isq.item
         and im.item_parent = I_key_value
         and rownum = 1
       UNION ALL
      select /*+ first_rows(1) */ 'x'
        from inv_status_qty isq,
             item_master im
       where im.item = isq.item
         and im.item_grandparent = I_key_value
         and rownum = 1;

   cursor C_NWP is
      select 'x'
      from   nwp
      where  item = I_key_value;

   cursor C_GET_XFORM_TYPE is
      select 'O'
        from item_xform_head ixh
       where ixh.head_item in
             (select item
                from item_master
               where item_parent = I_key_value
                  or item_grandparent = I_key_value
                  or item_master.item = I_key_value)
         and rownum = 1
       UNION
      select 'S'
        from item_xform_detail ixd
       where ixd.detail_item in
             (select item
                from item_master
               where item_parent = I_key_value
                  or item_grandparent = I_key_value
                  or item_master.item = I_key_value)
         and rownum = 1;

   cursor C_GET_NUM_SELLABLES is
      select COUNT(*)
        from item_xform_detail ixd,
             item_xform_head ixh
       where ixh.head_item in
              (select item
                 from item_master
                where item_parent = I_key_value
                   or item_grandparent = I_key_value
                   or item_master.item = I_key_value)
         and ixd.item_xform_head_id = ixh.item_xform_head_id;

   cursor C_SINGLE_SELLABLE_ON_DP is
      select dp.key_value
        from daily_purge dp,
             item_xform_head ixh,
             item_xform_detail ixd
       where ixh.head_item in
              (select item
                 from item_master
                where item_parent = i_key_value
                   or item_grandparent = i_key_value
                   or item_master.item = i_key_value)
         and ixd.item_xform_head_id = ixh.item_xform_head_id
         and ixd.detail_item = dp.key_value
         and dp.table_name = 'ITEM_MASTER'
         and dp.delete_type = 'D';

   cursor C_REPL_ITEM_LOC is
      select /*+ first_rows(1) */ 'x'
        from repl_item_loc rpl
        where rpl.primary_pack_no = I_key_value
          and rownum = 1;

    cursor C_DEAL_ACTUALS_ITEM_LOC is
      select /*+ first_rows(1) */ 'x'
        from deal_actuals_item_loc
       where item
          in (select item
                from item_master
               where (item_parent = I_key_value
                  or item_grandparent = I_key_value
                  or item_master.item = I_key_value));
                  
    cursor C_WF_COST_RELATIONSHIP is
      select /*+ first_rows(1) */ 'x'
        from wf_cost_relationship
       where item
          in (select item
                from item_master
               where (item_master.item = I_key_value
                  or item_parent       = I_key_value
                  or item_grandparent  = I_key_value));                         
            
   cursor C_CONCESSION_DATA is
       select /*+ first_rows(1) */ 'x'
         from concession_data
        where item
           in (select item
                 from item_master
                where (item_master.item = I_key_value
                   or item_parent       = I_key_value
                   or item_grandparent  = I_key_value));
                   
BEGIN

   O_relations_exist := 'N';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_NWP',
                    'NWP',
                    'ITEM: '||I_key_value);
   open C_NWP;
   SQL_LIB.SET_MARK('FETCH',
                    'C_NWP',
                    'NWP',
                    'ITEM: '||I_key_value);
   fetch C_NWP INTO L_nwp_exixts;

   if C_NWP%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'NWP_EXISTS');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_NWP',
                    'NWP',
                    'ITEM: '||I_key_value);
   close C_NWP;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    'ITEM: '||I_key_value);
   open C_ITEM_LOC_SOH;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    'ITEM: '||I_key_value);
   fetch C_ITEM_LOC_SOH INTO L_dummy;

   if C_ITEM_LOC_SOH%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'ITEM_LOC_SOH_EXIST');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    'ITEM: '||I_key_value);
   close C_ITEM_LOC_SOH;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_PACKITEM',
                    'PACKITEM',
                    'ITEM: '||I_key_value);
   open C_PACKITEM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PACKITEM',
                    'PACKITEM',
                    'ITEM: '||I_key_value);
   fetch C_PACKITEM INTO L_dummy;

   if C_PACKITEM%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'ITEM_EXIST_PACK');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PACKITEM',
                    'PACKITEM',
                    'ITEM: '||I_key_value);
   close C_PACKITEM;

   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC_TRAITS',
                    'ITEM_LOC_TRAITS',
                    'ITEM: '||I_key_value);
   open C_ITEM_LOC_TRAITS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_LOC_TRAITS',
                    'ITEM_LOC_TRAITS',
                    'ITEM: '||I_key_value);
   fetch C_ITEM_LOC_TRAITS INTO L_dummy;

   if C_ITEM_LOC_TRAITS%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'ITEM_LOC_TRAITS_EXIST');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_LOC_TRAITS',
                    'ITEM_LOC_TRAITS',
                    'ITEM: '||I_key_value);
   close C_ITEM_LOC_TRAITS;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC',
                    'ITEM_LOC',
                    'ITEM: '||I_key_value);
   open C_ITEM_LOC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_LOC',
                    'ITEM_LOC',
                    'ITEM: '||I_key_value);
   fetch C_ITEM_LOC INTO L_dummy;

   if C_ITEM_LOC%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'ITEM_LOC_EXIST');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_LOC_',
                    'ITEM_LOC',
                    'ITEM: '||I_key_value);
   close C_ITEM_LOC;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'ITEM: '||I_key_value);
   open C_ORDLOC_WKSHT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'ITEM: '||I_key_value);
   fetch C_ORDLOC_WKSHT INTO L_dummy;

   if C_ORDLOC_WKSHT%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'ORDLOC_WKSHT_STYLE');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'ITEM: '||I_key_value);
   close C_ORDLOC_WKSHT;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDERS_EXIST',
                    'ORDSKU',
                    'ITEM: '||I_key_value);
   open C_ORDERS_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDERS_EXIST',
                    'ORDSKU',
                    'ITEM: '||I_key_value);
   fetch C_ORDERS_EXIST INTO L_dummy;

   if C_ORDERS_EXIST%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'ORD_EXIST_STYLE');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDERS_EXIST',
                    'ORDSKU',
                    'ITEM: '||I_key_value);
   close C_ORDERS_EXIST;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ALC_HEAD',
                    'ALC_HEAD',
                    'ITEM: '||i_key_value);
   open C_ALC_HEAD;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ALC_HEAD',
                    'ALC_HEAD','ITEM: '||i_key_value);
   fetch C_ALC_HEAD into L_dummy;

   if C_ALC_HEAD%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||i_key_value);

      insert into DAILY_PURGE_ERROR_LOG
         values(i_key_value,
                'ALC_HEAD',
                'ITEM_EXIST_ALC');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                   'C_ALC_HEAD',
                    'ALC_HEAD','ITEM: '||i_key_value);
   close C_ALC_HEAD;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDLOC_EXP',
                    'ORDLOC_EXP',
                    'ITEM: '||I_key_value);
   open C_ORDLOC_EXP;
      
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDLOC_EXP',
                    'ORDLOC_EXP',
                    'ITEM: '||I_key_value);
   fetch C_ORDLOC_EXP INTO L_dummy;
      
   if C_ORDLOC_EXP%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
         insert into DAILY_PURGE_ERROR_LOG
            values(I_key_value,
                   'ORDLOC_EXP',
                   'ORD_EXIST_STYLE');
         O_relations_exist := 'Y';
   end if;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDLOC_EXP',
                    'ORDLOC_EXP',
                    'ITEM: '||I_key_value);
   close C_ORDLOC_EXP;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_TRANSFER',
                    'TSFDETAIL',
                    'ITEM: '||I_key_value);
   open C_TRANSFER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_TRANSFER',
                    'TSFDETAIL',
                    'ITEM: '||I_key_value);
   fetch C_TRANSFER INTO L_dummy;

   if C_TRANSFER%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'TSF_EXIST_STYLE');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_TRANSFER',
                    'TSFDETAIL',
                    'ITEM: '||I_key_value);
   close C_TRANSFER;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_MRT_ITEM',
                    'MRT_ITEM',
                    'ITEM: '||I_key_value);
   open C_MRT_ITEM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_MRT_ITEM',
                    'MRT_ITEM',
                    'ITEM: '||I_key_value);
   fetch C_MRT_ITEM INTO L_dummy;

   if C_MRT_ITEM%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'MRT_ITEM',
                'MRT_ITEM_EXIST');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_MRT_ITEM',
                    'MRT_ITEM',
                    'ITEM: '||I_key_value);
   close C_MRT_ITEM;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CONTENTS_ITEM',
                    'ITEM_MASTER',
                    'ITEM: '||I_key_value);
   open C_CONTENTS_ITEM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CONTENTS_ITEM',
                    'ITEM_MASTER',
                    'ITEM: '||I_key_value);
   fetch C_CONTENTS_ITEM INTO L_dummy;

   if C_CONTENTS_ITEM%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'CONTENTS_ITEM_EXIST');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CONTENTS_ITEM',
                    'ITEM_MASTER',
                    'ITEM: '||I_key_value);
   close C_CONTENTS_ITEM;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_INV_ADJ',
                    'INV_ADJ',
                    'ITEM: '||I_key_value);
   open C_INV_ADJ;

   SQL_LIB.SET_MARK('FETCH',
                    'C_INV_ADJ',
                    'INV_ADJ',
                    'ITEM: '||I_key_value);
   fetch C_INV_ADJ INTO L_dummy;

   if C_INV_ADJ%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'INV_ADJ_STYLE');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_INV_ADJ',
                    'INV_ADJ',
                    'ITEM: '||I_key_value);
   close C_INV_ADJ;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_RTV_DETAIL',
                    'RTV_DETAIL',
                    'ITEM: '||I_key_value);
   open C_RTV_DETAIL;

   SQL_LIB.SET_MARK('FETCH',
                    'C_RTV_DETAIL',
                    'RTV_DETAIL',
                    'ITEM: '||I_key_value);
   fetch C_RTV_DETAIL INTO L_dummy;

   if C_RTV_DETAIL%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'RTV_EXIST_STYLE');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_RTV_DETAIL',
                    'RTV_DETAIL',
                    'ITEM: '||I_key_value);
   close C_RTV_DETAIL;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ALLOC_HEADER',
                    'ALLOC_HEADER',
                    'ITEM: '||I_key_value);
   open C_ALLOC_HEADER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ALLOC_HEADER',
                    'ALLOC_HEADER',
                    'ITEM: '||I_key_value);
   fetch C_ALLOC_HEADER INTO L_dummy;

   if C_ALLOC_HEADER%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'ALLOC_EXIST_STYLE');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ALLOC_HEADER',
                    'ALLOC_HEADER',
                    'ITEM: '||I_key_value);
   close C_ALLOC_HEADER;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_STAKE_SKU_LOC',
                    'STAKE_SKU_LOC',
                    'ITEM: '||I_key_value);
   open C_STAKE_SKU_LOC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_STAKE_SKU_LOC',
                    'STAKE_SKU_LOC',
                    'ITEM: '||I_key_value);
   fetch C_STAKE_SKU_LOC INTO L_dummy;

   if C_STAKE_SKU_LOC%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'STAKE_STYLE_LOC_EXIST');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_STAKE_SKU_LOC_',
                    'STAKE_SKU_LOC',
                    'ITEM: '||I_key_value);
   close C_STAKE_SKU_LOC;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CONTRACT_IND',
                    'SYSTEM_OPTIONS',
                     NULL);
   open C_CONTRACT_IND;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CONTRACT_IND',
                    'SYSTEM_OPTIONS'
                    ,NULL);
   fetch C_CONTRACT_IND into L_dummy;

   if L_dummy = 'Y' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CONTRACT_EXISTS',
                       'CONTRACT_COST',
                       'ITEM: '||I_key_value);
      open C_CONTRACT_EXISTS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CONTRACT_EXISTS',
                       'CONTRACT_COST',
                       'ITEM: '||I_key_value);
      fetch C_CONTRACT_EXISTS INTO L_dummy;

      if C_CONTRACT_EXISTS%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||I_key_value);
         insert into DAILY_PURGE_ERROR_LOG
            values(I_key_value,
                   'ITEM_MASTER',
                   'CONTRACT_ITEM_EXIST');
         O_relations_exist := 'Y';
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CONTRACT_EXISTS',
                       'CONTRACT_COST',
                       'ITEM: '||I_key_value);
      close C_CONTRACT_EXISTS;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CONTRACT_IND',
                    'SYSTEM_OPTIONS',
                    NULL);
   close C_CONTRACT_IND;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_INVC_DETAIL',
                    'INVC_DETAIL',
                    'ITEM: '||I_key_value);
   open C_INVC_DETAIL;

   SQL_LIB.SET_MARK('FETCH',
                    'C_INVC_DETAIL',
                    'INVC_DETAIL',
                    'ITEM: '||I_key_value);
   fetch C_INVC_DETAIL INTO L_dummy;

   if C_INVC_DETAIL%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'INVOICES_EXIST_ITEM');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_INVC_DETAIL',
                    'INVC_DETAIL',
                    'ITEM: '||I_key_value);
   close C_INVC_DETAIL;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_REPL_ITEM_LOC',
                    'REPL_ITEM_LOC',
                    'ITEM: '||I_key_value);
   open C_REPL_ITEM_LOC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_REPL_ITEM_LOC',
                    'REPL_ITEM_LOC',
                    'ITEM: '||I_key_value);
   fetch C_REPL_ITEM_LOC INTO L_dummy;

   if C_REPL_ITEM_LOC%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'REPL_ITEM_LOC',
                'CHECK_PRIMARY_REPL_PACK');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_REPL_ITEM_LOC',
                    'REPL_ITEM_LOC',
                    'ITEM: '||I_key_value);
   close C_REPL_ITEM_LOC;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_INV_STATUS_QTY',
                    'INV_STATUS_QTY',
                    'ITEM: '||I_key_value);
   open C_INV_STATUS_QTY;

   SQL_LIB.SET_MARK('FETCH',
                    'C_INV_STATUS_QTY',
                    'INV_STATUS_QTY',
                    'ITEM: '||I_key_value);
   fetch C_INV_STATUS_QTY INTO L_dummy;

   if C_INV_STATUS_QTY%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'INV_STATUS_EXIST');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_INV_STATUS_QTY',
                    'INV_STATUS_QTY',
                    'ITEM: '||I_key_value);
   close C_INV_STATUS_QTY;
   ---

   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_DEAL_ACTUALS_ITEM_LOC',
                    'DEAL_ACTUALS_ITEM_LOC',
                    'ITEM: '||I_key_value);
   open C_DEAL_ACTUALS_ITEM_LOC;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_DEAL_ACTUALS_ITEM_LOC',
                    'DEAL_ACTUALS_ITEM_LOC',
                    'ITEM: '||I_key_value);
   fetch C_DEAL_ACTUALS_ITEM_LOC INTO L_dummy;
   
   if C_DEAL_ACTUALS_ITEM_LOC%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'DEAL_ACTUALS_ITEM_LOC',
                'DEAL_ACTUALS_ITEM_LOC_EXIST');
      O_relations_exist := 'Y';
   end if;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_DEAL_ACTUALS_ITEM_LOC',
                    'DEAL_ACTUALS_ITEM_LOC',
                    'ITEM: '||I_key_value);
   close C_DEAL_ACTUALS_ITEM_LOC;   
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'c_WF_COST_RELATIONSHIP',
                    'WF_COST_RELATIONSHIP',
                    'ITEM : '||I_key_value);
   open C_WF_COST_RELATIONSHIP;                 
   
   SQL_LIB.SET_MARK('OPEN',
                    'c_WF_COST_RELATIONSHIP',
                    'WF_COST_RELATIONSHIP',
                    'ITEM : '||I_key_value);
   fetch C_WF_COST_RELATIONSHIP INTO L_dummy;
   
   if C_WF_COST_RELATIONSHIP%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'WF_COST_RELATIONSHIP_EXIST');
      O_relations_exist := 'Y';
   end if;                       
   
   SQL_LIB.SET_MARK('CLOSE',
                    'c_WF_COST_RELATIONSHIP',
                    'WF_COST_RELATIONSHIP',
                    'ITEM : '||I_key_value);
   close C_WF_COST_RELATIONSHIP;                                                                  
   ---   
   SQL_LIB.SET_MARK('OPEN',
                    'C_CONCESSION_DATA',
                    'CONCESSION_DATA',
                    'ITEM : '||I_key_value);
   open C_CONCESSION_DATA;                 
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_CONCESSION_DATA',
                    'CONCESSION_DATA',
                    'ITEM : '||I_key_value);
   fetch C_CONCESSION_DATA INTO L_dummy;
   
   if C_CONCESSION_DATA%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(I_key_value,
                'ITEM_MASTER',
                'CONCESSION_DATA');
      O_relations_exist := 'Y';
   end if;                       
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CONCESSION_DATA',
                    'CONCESSION_DATA',
                    'ITEM : '||I_key_value);
   close C_CONCESSION_DATA; 
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_XFORM_TYPE',
                    'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL',
                    'ITEM: '||I_key_value);
   open C_GET_XFORM_TYPE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_XFORM_TYPE',
                    'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL',
                    'ITEM: '||I_key_value);
   fetch C_GET_XFORM_TYPE INTO L_dummy;

   L_found := C_GET_XFORM_TYPE%FOUND;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_XFORM_TYPE',
                    'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL',
                    'ITEM: '||I_key_value);
   close C_GET_XFORM_TYPE;
   --
   if L_found then
      if L_dummy = 'O' then

         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_NUM_SELLABLES',
                          'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL',
                          'ITEM: '||I_key_value);
         open C_GET_NUM_SELLABLES;

         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_NUM_SELLABLES',
                          'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL',
                          'ITEM: '||I_key_value);
         fetch C_GET_NUM_SELLABLES INTO L_num_sellables;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_NUM_SELLABLES',
                          'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL',
                          'ITEM: '||I_key_value);
         close C_GET_NUM_SELLABLES;

         if L_num_sellables > 0 then
            if L_num_sellables = 1 then
               SQL_LIB.SET_MARK('OPEN',
                                'C_SINGLE_SELLABLE_ON_DP',
                                'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL,DAILY_PURGE',
                                'ITEM: ' || I_key_value);
               open C_SINGLE_SELLABLE_ON_DP;

               SQL_LIB.SET_MARK('FETCH',
                                'C_SINGLE_SELLABLE_ON_DP',
                                'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL,DAILY_PURGE',
                                'ITEM: ' || I_key_value);
               fetch C_SINGLE_SELLABLE_ON_DP into L_single_sellable;

               if L_single_sellable is NULL then

                  SQL_LIB.SET_MARK('INSERT',
                                   NULL,
                                   'DAILY_PURGE_ERROR_LOG',
                                   'KEY_VALUE: ' || I_key_value);
                  insert into daily_purge_error_log(key_value,
                                                    table_name,
                                                    error_desc)
                                             values(I_key_value,
                                                    'ITEM_XFORM_HEAD',
                                                    'ORDERABLE_HAS_SELLABLES');
                  O_relations_exist := 'Y';
               else
                     SQL_LIB.SET_MARK('CLOSE',
                                'C_SINGLE_SELLABLE_ON_DP',
                                'ITEM_XFORM_HEAD,ITEM_XFORM_DETAIL,DAILY_PURGE',
                                'ITEM: ' || I_key_value);
                     close C_SINGLE_SELLABLE_ON_DP;

                     if VALIDATE_SELLABLE(L_single_sellable,
                              L_sellable_deletable,
                              L_error_message ) = FALSE then
                     return FALSE;
                     end if;

                     if NOT L_sellable_deletable then
                         SQL_LIB.SET_MARK('INSERT',
                                   NULL,
                                   'DAILY_PURGE_ERROR_LOG',
                                   'KEY_VALUE: ' || I_key_value);
                         insert into daily_purge_error_log(key_value,
                                                    table_name,
                                                    error_desc)
                                             values(I_key_value,
                                                    'ITEM_XFORM_HEAD',
                                                    'ORDERABLE_HAS_SELLABLES');
                         O_relations_exist := 'Y';
                    end if;
                end if;
            else
               SQL_LIB.SET_MARK('INSERT',
                                NULL,
                                'DAILY_PURGE_ERROR_LOG',
                                'KEY_VALUE: '||I_key_value);
               insert into DAILY_PURGE_ERROR_LOG(key_value,
                                                 table_name,
                                                 error_desc)
                                         values(I_key_value,
                                                'ITEM_XFORM_HEAD',
                                                'ORDERABLE_HAS_SELLABLES');
               O_relations_exist := 'Y';
            end if;
         end if;
      else
         if VALIDATE_SELLABLE(I_key_value,
                              L_delete_sellable,
                              L_error_message ) = FALSE then
            return FALSE;
         end if;

         if NOT L_delete_sellable then
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'DAILY_PURGE_ERROR_LOG',
                             'KEY_VALUE: '||I_key_value);
            insert into DAILY_PURGE_ERROR_LOG
               values(I_key_value,
                      'ITEM_XFORM_HEAD',
                      'CANNOT_DELETE_SELLABLE');
           O_relations_exist := 'Y';
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      L_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END;
--------------------------------------------------------------------
FUNCTION VALIDATE_SELLABLE(I_sellable          IN       ITEM_MASTER.ITEM%TYPE,
                           O_delete_sellable   OUT      BOOLEAN,
                           O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'VALIDATE_RECORDS_SQL.VALIDATE_SELLABLE';
   L_sum_qty_pct NUMBER ;
   L_dummy VARCHAR2(1);
   L_multi_parent_ind BOOLEAN ;
   L_single_orderable_on_dp BOOLEAN ;
   L_other_sellables_exist BOOLEAN ;
   L_xform_head_id ITEM_XFORM_HEAD.ITEM_XFORM_HEAD_ID%TYPE;

    cursor C_SUM_QTY_PCT is
      select SUM(item_quantity_pct)
        from item_xform_detail
       where detail_item = I_sellable;

   cursor C_SINGLE_ORDERABLE_ON_DP is
      select ixh.item_xform_head_id
        from daily_purge dp,
             item_xform_head ixh,
             item_xform_detail ixd
       where ixd.detail_item in
              (select item
                 from item_master
                where item_parent = I_sellable
                   or item_grandparent = I_sellable
                   or item_master.item = I_sellable)
         and ixh.item_xform_head_id = ixd.item_xform_head_id
         and ixh.head_item = dp.key_value
         and dp.table_name = 'ITEM_MASTER'
         and dp.delete_type = 'D';

   cursor C_OTHER_SELLABLES_EXIST is
      select detail_item
        from item_xform_detail
       where item_xform_head_id = L_xform_head_id
         and detail_item != I_sellable
      minus
      select dp.key_value
        from daily_purge dp,
             item_xform_detail ixd
       where ixd.item_xform_head_id = L_xform_head_id
         and ixd.detail_item = dp.key_value
         and dp.key_value != I_sellable
         and dp.table_name = 'ITEM_MASTER'
         and dp.delete_type = 'D';

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_SUM_QTY_PCT','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
   OPEN C_SUM_QTY_PCT;
   SQL_LIB.SET_MARK('FETCH','C_SUM_QTY_PCT','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
   FETCH C_SUM_QTY_PCT INTO L_sum_qty_pct;

   SQL_LIB.SET_MARK('CLOSE','C_SUM_QTY_PCT','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
   CLOSE C_SUM_QTY_PCT;

   if L_sum_qty_pct = 0 then
      O_delete_sellable := TRUE ;
   else
      -- Is sellable attached to only one orderable, and is that orderable on daily purge ?
      if ITEM_XFORM_SQL.CHECK_MULTI_PARENTS(O_error_message,
                                            L_multi_parent_ind,
                                            I_sellable ) = FALSE then
         return FALSE ;
      end if;

      if L_multi_parent_ind then
         O_delete_sellable := FALSE ;
      else
         SQL_LIB.SET_MARK('OPEN','C_SINGLE_ORDERABLE_ON_DP','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
         OPEN C_SINGLE_ORDERABLE_ON_DP;

         SQL_LIB.SET_MARK('FETCH','C_SINGLE_ORDERABLE_ON_DP','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
         FETCH C_SINGLE_ORDERABLE_ON_DP INTO L_xform_head_id;

         L_single_orderable_on_dp := C_SINGLE_ORDERABLE_ON_DP%FOUND;

         SQL_LIB.SET_MARK('CLOSE','C_SINGLE_ORDERABLE_ON_DP','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
         CLOSE C_SINGLE_ORDERABLE_ON_DP;

         if NOT L_single_orderable_on_dp then
            O_delete_sellable := FALSE ;
         else
         -- Check that this is the only sellable on the template.
         SQL_LIB.SET_MARK('OPEN','C_OTHER_SELLABLES_EXIST','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
         OPEN C_OTHER_SELLABLES_EXIST;

         SQL_LIB.SET_MARK('FETCH','C_OTHER_SELLABLES_EXIST','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
         FETCH C_OTHER_SELLABLES_EXIST INTO L_dummy;

         L_other_sellables_exist:= C_OTHER_SELLABLES_EXIST%FOUND;

         SQL_LIB.SET_MARK('CLOSE','C_OTHER_SELLABLES_EXIST','ITEM_XFORM_DETAIL','ITEM: '||I_sellable);
         CLOSE C_OTHER_SELLABLES_EXIST;

            if L_other_sellables_exist then
               O_delete_sellable := FALSE;
            else
               O_delete_sellable := TRUE;
            end if;

         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                            'VALIDATE_SELLABLE', SQLCODE);
      return FALSE;
END;
--------------------------------------------------------------------
FUNCTION DEL_DEPS(I_key_value         IN       VARCHAR2,
                  O_relations_exist   IN OUT   VARCHAR2,
                  O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'VALIDATE_RECORDS_SQL.DEL_DEPS';
   L_dummy              VARCHAR2(1);

   L_promotion_exists   BOOLEAN      := FALSE;
   L_threshhold_exists  BOOLEAN      := FALSE;

   cursor C_CLASS is
      select 'x'
        from class
       where dept = to_number(I_key_value);

   --- There is no need to check the stocktake_date on stake_head
   --- because stkprg should delete records that are out of date
   --- and any other records left should be valid.
   cursor C_CYCLE_COUNT is
      select 'x'
        from stake_product
       where stake_product.dept = to_number(I_key_value);

   cursor C_ORDHEAD is
      select 'x'
        from ordhead
       where dept = to_number(I_key_value);

   cursor C_CONTRACT_HEADER is
      select 'x'
        from contract_header
       where dept = to_number(I_key_value);

   cursor C_TSFHEAD is
      select 'x'
        from tsfhead
       where dept = to_number(I_key_value);

BEGIN
   O_relations_exist := 'N';
   ---
   SQL_LIB.SET_MARK('OPEN','C_CLASS', 'CLASS','DEPT: '||I_key_value);
   open C_CLASS;

   SQL_LIB.SET_MARK('FETCH','C_CLASS', 'CLASS','DEPT: '||I_key_value);
   fetch C_CLASS into L_dummy;

   if C_CLASS%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'DEPS',
                'CLASS_EXIST_IN_DEPT');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CLASS', 'CLASS','DEPT: '||I_key_value);
   close C_CLASS;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CYCLE_COUNT', 'STAKE_LOCATION','DEPT: '||I_key_value);
   open C_CYCLE_COUNT;

   SQL_LIB.SET_MARK('FETCH','C_CYCLE_COUNT', 'STAKE_LOCATION','DEPT: '||I_key_value);
   fetch C_CYCLE_COUNT INTO L_dummy;

   if C_CYCLE_COUNT%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'DEPS',
                'CYCLE_COUNT_EXIST');
      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CYCLE_COUNT', 'STAKE_LOCATION','DEPT: '||I_key_value);
   close C_CYCLE_COUNT;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDHEAD', 'ORDHEAD','DEPT: '||I_key_value);
   open C_ORDHEAD;

   SQL_LIB.SET_MARK('FETCH','C_ORDHEAD', 'ORDHEAD','DEPT: '||I_key_value);
   fetch C_ORDHEAD into L_dummy;

   if C_ORDHEAD%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL,
                    'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'DEPS',
                'ORDHEAD_EXIST');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_ORDHEAD', 'ORDHEAD','DEPT: '||I_key_value);
   close C_ORDHEAD;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CONTRACT_HEADER', 'CONTRACT_HEADER','DEPT: '||I_key_value);
   open C_CONTRACT_HEADER;

   SQL_LIB.SET_MARK('FETCH','C_CONTRACT_HEADER', 'CONTRACT_HEADER','DEPT: '||I_key_value);
   fetch C_CONTRACT_HEADER into L_dummy;

   if C_CONTRACT_HEADER%FOUND then
      SQL_LIB.SET_MARK('INSERT', NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'DEPS',
                'CONTRACT_EXIST');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CONTRACT_HEADER', 'CONTRACT_HEADER','DEPT: '||I_key_value);
   close C_CONTRACT_HEADER;
   ---
   SQL_LIB.SET_MARK('OPEN','C_TSFHEAD', 'TSFHEAD','DEPT: '||I_key_value);
   open C_TSFHEAD;

   SQL_LIB.SET_MARK('FETCH','C_TSFHEAD', 'TSFHEAD','DEPT: '||I_key_value);
   fetch C_TSFHEAD into L_dummy;

   if C_TSFHEAD%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'DEPS',
                'TSFHEAD_EXIST');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_TSFHEAD', 'TSFHEAD','DEPT: '||I_key_value);
   close C_TSFHEAD;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
      return FALSE;
END;
----------------------------------------------------------------
-- There is no need to check the stocktake_date on stake_head
-- because stkprg should delete records that are out of date
-- and any other records left should be valid.
--
FUNCTION DEL_CLASS(I_key_value         IN       VARCHAR2,
                   O_relations_exist   IN OUT   VARCHAR2,
                   O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'VALIDATE_RECORDS_SQL.DEL_CLASS';

   L_class              CLASS.CLASS%TYPE;
   L_dept               CLASS.DEPT%TYPE;
   L_dummy              VARCHAR2(1);

   L_promotion_exists   BOOLEAN      := FALSE;
   L_threshhold_exists  BOOLEAN      := FALSE;

   cursor C_SUBCLASS is
      select 'x'
        from subclass
       where class = L_class
         and dept = L_dept;

   cursor C_CYCLE_COUNT is
      select 'x'
        from stake_product
       where dept = L_dept
         and class = L_class;


BEGIN
   O_relations_exist := 'N';

   L_dept  := to_number(substr(I_key_value,1,4));
   L_class := to_number(substr(I_key_value,6,4));
   ---
   SQL_LIB.SET_MARK('OPEN','C_SUBCLASS', 'SUBCLASS','CLASS: '||I_key_value);
   open C_SUBCLASS;

   SQL_LIB.SET_MARK('FETCH','C_SUBCLASS', 'SUBCLASS','CLASS: '||I_key_value);
   fetch C_SUBCLASS into L_dummy;

   if C_SUBCLASS%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL,
                    'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
               'CLASS',
               'CANNOT_DEL_CLASS');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_SUBCLASS', 'SUBCLASS','CLASS: '||I_key_value);
   close C_SUBCLASS;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CYCLE_COUNT', 'STAKE_PRODUCT','CLASS: '||I_key_value);
   open C_CYCLE_COUNT;

   SQL_LIB.SET_MARK('FETCH','C_CYCLE_COUNT', 'STAKE_PRODUCT','CLASS: '||I_key_value);
   fetch C_CYCLE_COUNT into L_dummy;

   if C_CYCLE_COUNT%FOUND then
      SQL_LIB.SET_MARK('INSERT', NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'CLASS',
                'CYCLE_COUNT_CLASS');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CYCLE_COUNT', 'STAKE_PRODUCT','CLASS: '||I_key_value);
   close C_CYCLE_COUNT;
   ---
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
      return FALSE;
END;
-----------------------------------------------------------------
FUNCTION DEL_SUBCLASS(I_key_value         IN       VARCHAR2,
                      O_relations_exist   IN OUT   VARCHAR2,
                      O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'VALIDATE_RECORDS_SQL.DEL_SUBCLASS';
   L_dummy              VARCHAR2(1);
   L_subclass           SUBCLASS.SUBCLASS%TYPE;
   L_class              SUBCLASS.CLASS%TYPE;
   L_dept               SUBCLASS.DEPT%TYPE;

   L_promotion_exists   BOOLEAN      := FALSE;
   L_threshhold_exists  BOOLEAN      := FALSE;

   cursor C_CHK_ITEM is
      select 'x'
        from item_master
       where dept     = L_dept
         and class    = L_class
         and subclass = L_subclass
         and rownum   = 1;

   cursor C_CHK_MTH_DATA is
      select 'x'
        from month_data
       where dept     = L_dept
         and class    = L_class
         and subclass = L_subclass
         and (   nvl(cls_stk_cost,               0) <> 0
              or nvl(cls_stk_retail,             0) <> 0
              or nvl(inter_stocktake_sales_amt,  0) <> 0
              or nvl(inter_stocktake_shrink_amt, 0) <> 0
              or nvl(htd_gafs_retail,            0) <> 0
              or nvl(htd_gafs_cost,              0) <> 0)
         and rownum = 1;

   --- There is no need to check the stocktake_date on stake_head
   --- because stkprg should delete records that are out of date
   --- and any other records left should be valid.
   cursor C_CHK_STOCK_COUNT is
      select 'x'
        from stake_product
       where dept = L_dept
         and class = L_class
         and subclass = L_subclass
         and rownum = 1;

   cursor C_STAKE_PROD_LOC is
      select 'x'
        from stake_prod_loc
       where dept = L_dept
         and class = L_class
         and subclass = L_subclass
         and rownum = 1;

   cursor C_CHK_RECLASS_HEAD is
      select 'x'
        from reclass_head
       where to_dept = L_dept
         and to_class = L_class
         and to_subclass = L_subclass
         and rownum = 1;


   cursor C_TRAN_DATA_HISTORY is
      select 'x'
        from tran_data_history
       where dept       = L_dept
         and class      = L_class
         and subclass   = L_subclass
         and tran_date >= (select last_eom_date
                            from system_variables)
         and (   nvl(total_cost,   0) <> 0
              or nvl(total_retail, 0) <> 0)
         and rownum = 1;

   cursor C_CHK_POS_MERCH_CRITERIA is
      select 'x'
        from pos_merch_criteria
       where dept = L_dept
         and class = L_class
         and subclass = L_subclass;

BEGIN
   O_relations_exist := 'N';

   L_dept := to_number(substr(I_key_value,1,4));
   L_class := to_number(substr(I_key_value,6,4));
   L_subclass := to_number(substr(I_key_value,11,4));
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHK_ITEM', 'ITEM_MASTER','SUBCLASS: '||I_key_value);
   open C_CHK_ITEM;

   SQL_LIB.SET_MARK('FETCH','C_CHK_ITEM', 'ITEM_MASTER','SUBCLASS: '||I_key_value);
   fetch C_CHK_ITEM INTO L_dummy;

   if C_CHK_ITEM%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'SUBCLASS',
                'ITEM_EXIST_SUBCLASS');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHK_ITEM', 'ITEM_MASTER','SUBCLASS: '||I_key_value);
   close C_CHK_ITEM;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHK_MTH_DATA', 'MONTH_DATA','SUBCLASS: '||I_key_value);
   open C_CHK_MTH_DATA;

   SQL_LIB.SET_MARK('FETCH','C_CHK_MTH_DATA', 'MONTH_DATA','SUBCLASS: '||I_key_value);
   fetch C_CHK_MTH_DATA INTO L_dummy;

   if C_CHK_MTH_DATA%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'SUBCLASS',
                'MTH_DATA_EXIST_SUB');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHK_MTH_DATA', 'MONTH_DATA','SUBCLASS: '||I_key_value);
   close C_CHK_MTH_DATA;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHK_STOCK_COUNT', 'STAKE_LOCATION','SUBCLASS: '||I_key_value);
   open C_CHK_STOCK_COUNT;

   SQL_LIB.SET_MARK('FETCH','C_CHK_STOCK_COUNT', 'STAKE_LOCATION','SUBCLASS: '||I_key_value);
   fetch C_CHK_STOCK_COUNT INTO L_dummy;

   if C_CHK_STOCK_COUNT%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL,
                    'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'SUBCLASS',
                'CYCLE_COUNT_SUBCLASS');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHK_STOCK_COUNT', 'STAKE_LOCATION','SUBCLASS: '||I_key_value);
   close C_CHK_STOCK_COUNT;
   ---
   SQL_LIB.SET_MARK('OPEN','C_STAKE_PROD_LOC', 'STAKE_PROD_LOC','SUBCLASS: '||I_key_value);
   open C_STAKE_PROD_LOC;

   SQL_LIB.SET_MARK('FETCH','C_STAKE_PROD_LOC', 'STAKE_PROD_LOC','SUBCLASS: '||I_key_value);
   fetch C_STAKE_PROD_LOC INTO L_dummy;

   if C_STAKE_PROD_LOC%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'SUBCLASS',
                'CYCLE_COUNT_SUBCLASS');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_STAKE_PROD_LOC', 'STAKE_PROD_LOC','SUBCLASS: '||I_key_value);
   close C_STAKE_PROD_LOC;

   SQL_LIB.SET_MARK('OPEN','C_CHK_RECLASS_HEAD', 'RECLASS_HEAD','SUBCLASS: '||I_key_value);
   open C_CHK_RECLASS_HEAD;

   SQL_LIB.SET_MARK('FETCH','C_CHK_RECLASS_HEAD', 'RECLASS_HEAD','SUBCLASS: '||I_key_value);
   fetch C_CHK_RECLASS_HEAD INTO L_dummy;

   if C_CHK_RECLASS_HEAD%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'SUBCLASS',
                'RECLASS_EXIST_SUBCLASS');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHK_RECLASS_HEAD', 'RECLASS_HEAD','SUBCLASS: '||I_key_value);
   close C_CHK_RECLASS_HEAD;
   ---



   ---
   SQL_LIB.SET_MARK('OPEN','C_TRAN_DATA_HISTORY', 'TRAN_DATA_HISTORY','SUBCLASS: '||I_key_value);
   open C_TRAN_DATA_HISTORY;

   SQL_LIB.SET_MARK('FETCH','C_TRAN_DATA_HISTORY', 'TRAN_DATA_HISTORY','SUBCLASS: '||I_key_value);
   fetch C_TRAN_DATA_HISTORY into L_dummy;

   if C_TRAN_DATA_HISTORY%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL, 'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'SUBCLASS',
                'TRAN_HIST_EXIST_SUB');

      O_relations_exist := 'Y';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_TRAN_DATA_HISTORY', 'TRAN_DATA_HISTORY','SUBCLASS: '||I_key_value);
   close C_TRAN_DATA_HISTORY;
   ---

   SQL_LIB.SET_MARK('OPEN','C_CHK_POS_MERCH_CRITERIA',
                    'POS_MERCH_CRITERIA','SUBCLASS: '||I_key_value);
   OPEN C_CHK_POS_MERCH_CRITERIA;
   SQL_LIB.SET_MARK('FETCH','C_CHK_POS_MERCH_CRITERIA',
                    'POS_MERCH_CRITERIA','SUBCLASS: '||I_key_value);
   FETCH C_CHK_POS_MERCH_CRITERIA INTO L_dummy;
   if C_CHK_POS_MERCH_CRITERIA%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL,
                    'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||I_key_value);
      INSERT INTO daily_purge_error_log
         VALUES(I_key_value,
                'SUBCLASS',
                'SBCLAS_EXIST_POS_MERCH_CT');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CHK_POS_MERCH_CRITERIA',
                    'POS_MERCH_CRITERIA','SUBCLASS: '||I_key_value);
   CLOSE C_CHK_POS_MERCH_CRITERIA; 
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM, 'DEL_SUBCLASS', SQLCODE);
      return FALSE;
END;
-----------------------------------------------------------------
FUNCTION DEL_STORE(I_key_value         IN       VARCHAR2,
                   O_relations_exist   IN OUT   VARCHAR2,
                   O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'VALIDATE_RECORDS_SQL.DEL_STORE';
   L_store           STORE.STORE%TYPE;
   L_dummy           VARCHAR2(1);
   L_store_count     NUMBER;
   L_last_eom_date   SYSTEM_VARIABLES.LAST_EOM_DATE%TYPE;

   L_promotion_exists     BOOLEAN := FALSE;
   L_price_change_exists  BOOLEAN := FALSE;
   L_clearance_exists     BOOLEAN := FALSE;

   cursor C_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where location = L_store
         and rownum = 1;

   cursor C_CHK_STAKE_LOCATION is
      select 'x'
        from stake_location
       where location = L_store
         and rownum = 1;

   cursor C_CHK_ORDLOC is
      select 'x'
        from ordloc
       where location = L_store
         and rownum = 1;

   cursor C_CHK_ITEMLOC_ST is
      select 'x'
        from item_loc
       where loc_type = 'S'
         and loc = L_store
         and rownum = 1;

   cursor C_LAST_EOM_DATE is
      select last_eom_date
        from system_variables;

   cursor C_CHK_TRAN_DATA_HISTORY is
      select 'x'
        from tran_data_history
       where location = L_store
         and tran_date > L_last_eom_date
         and rownum = 1;

   cursor C_CHK_MONTH_DATA is
      select 'x'
        from month_data
       where location = L_store
         and eom_date = L_last_eom_date
         and (nvl(cls_stk_cost,0) <> 0
              or nvl(cls_stk_retail,0) <> 0)
         and rownum = 1;

   cursor C_CHK_TSFHEAD is
      select 'x'
        from tsfhead
       where (to_loc = L_store
          or from_loc = L_store)
         and rownum = 1;

   cursor C_CHK_ALLOC_DETAIL is
      select 'x'
        from alloc_detail
       where to_loc = L_store
         and rownum = 1;

   cursor C_CHK_RTV_HEAD is
      select 'x'
        from rtv_head
       where store = L_store
         and rownum = 1;

   cursor C_CHK_CONTRACT_DETAIL is
      select 'x'
        from contract_detail
       where location = L_store
         and rownum = 1;

   cursor C_CHK_WALK_THROUGH_STORE is
      select 'x'
        from walk_through_store
       where walk_through_store = L_store
         and rownum = 1;

   cursor C_LOCK_SISTER_STORE is
      select 'x'
        from store
       where sister_store = L_store
         and rownum = 1;

   cursor C_CHK_DEPT_CHARGE is
      select 'x'
        from dept_chrg_head
       where (to_loc = L_store
          or from_loc = L_store)
         and rownum = 1;

   cursor C_CHK_ITEM_CHARGE is
      select 'x'
        from item_chrg_head
       where (to_loc = L_store
          or from_loc = L_store)
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   open C_LAST_EOM_DATE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   fetch C_LAST_EOM_DATE INTO L_last_eom_date;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   close C_LAST_EOM_DATE;
   ---
   L_store := TO_NUMBER(I_key_value);
   ---
   O_relations_exist := 'N';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'COLOR: '||I_key_value);
   open C_ORDLOC_WKSHT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'COLOR: '||I_key_value);
   fetch C_ORDLOC_WKSHT INTO L_dummy;
   if C_ORDLOC_WKSHT%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'ORDLOC_WKSHT_STORE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'COLOR: '||I_key_value);
   close C_ORDLOC_WKSHT;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_STAKE_LOCATION',
                    'STAKE_LOCATION',
                    'STORE: '||I_key_value);
   open C_CHK_STAKE_LOCATION;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_STAKE_LOCATION',
                    'STAKE_LOCATION',
                    'STORE: '||I_key_value);
   fetch C_CHK_STAKE_LOCATION INTO L_dummy;
   if C_CHK_STAKE_LOCATION%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'STOCK_IN_STORE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_STAKE_LOCATION',
                    'STAKE_LOCATION',
                    'STORE: '||I_key_value);
   close C_CHK_STAKE_LOCATION;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_ORDLOC',
                    'ORDLOC',
                    'LOCATION: '||I_key_value);
   open C_CHK_ORDLOC;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_ORDLOC',
                    'ORDLOC',
                    'LOCATION: '||I_key_value);
   fetch C_CHK_ORDLOC INTO L_dummy;
   if C_CHK_ORDLOC%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'ORD_EXIST_STORE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_ORDLOC',
                    'ORDLOC',
                    'LOCATION: '||I_key_value);
   close C_CHK_ORDLOC;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_ITEMLOC_ST',
                    'ITEM_LOC',
                    'STORE: '||I_key_value);
   open C_CHK_ITEMLOC_ST;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_ITEMLOC_ST',
                    'ITEM_LOC',
                    'STORE: '||I_key_value);
   fetch C_CHK_ITEMLOC_ST INTO L_dummy;
   if C_CHK_ITEMLOC_ST%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'ITEM_EXIST_STORE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_ITEMLOC_ST',
                    'ITEM_LOC',
                    'STORE: '||I_key_value);
   close C_CHK_ITEMLOC_ST;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_TRAN_DATA_HISTORY',
                    'TRAN_DATA_HISTORY',
                    'STORE: '||I_key_value);
   open C_CHK_TRAN_DATA_HISTORY;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_TRAN_DATA_HISTORY',
                    'TRAN_DATA_HISTORY',
                    'STORE: '||I_key_value);
   fetch C_CHK_TRAN_DATA_HISTORY INTO L_dummy;
   if C_CHK_TRAN_DATA_HISTORY%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'TRAN_DATA_HISTORY_EXIST');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_TRAN_DATA_HISTORY',
                    'TRAN_DATA_HISTORY',
                    'STORE: '||I_key_value);
   close C_CHK_TRAN_DATA_HISTORY;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_TSFHEAD',
                    'TSFHEAD',
                    'STORE: '||I_key_value);
   open C_CHK_TSFHEAD;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_TSFHEAD',
                    'TSFHEAD',
                    'STORE: '||I_key_value);
   fetch C_CHK_TSFHEAD INTO L_dummy;
   if C_CHK_TSFHEAD%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'TSFHEAD_EXIST_STORE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_TSFHEAD',
                    'TSFHEAD',
                    'STORE: '||I_key_value);
   close C_CHK_TSFHEAD;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_MONTH_DATA',
                    'MONTH_DATA',
                    'STORE: '||I_key_value);
   open C_CHK_MONTH_DATA;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_MONTH_DATA',
                    'MONTH_DATA',
                    'STORE: '||I_key_value);
   fetch C_CHK_MONTH_DATA INTO L_dummy;
   if C_CHK_MONTH_DATA%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'STORE_ACTIVE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_MONTH_DATA',
                    'MONTH_DATA',
                    'STORE: '||I_key_value);
   close C_CHK_MONTH_DATA;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_ALLOC_DETAIL',
                    'ALLOC_DETAIL',
                    'TO_LOC: '||I_key_value);
   open C_CHK_ALLOC_DETAIL;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_ALLOC_DETAIL',
                    'ALLOC_DETAIL',
                    'TO_LOC: '||I_key_value);
   fetch C_CHK_ALLOC_DETAIL into L_dummy;
   if C_CHK_ALLOC_DETAIL%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'ALLOC_EXIST_STORE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_ALLOC_DETAIL',
                    'ALLOC_DETAIL',
                    'TO_LOC: '||I_key_value);
   close C_CHK_ALLOC_DETAIL;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_RTV_HEAD',
                    'RTV_HEAD',
                    'STORE: '||I_key_value);
   open C_CHK_RTV_HEAD;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_RTV_HEAD',
                    'RTV_HEAD',
                    'STORE: '||I_key_value);
   fetch C_CHK_RTV_HEAD into L_dummy;
   if C_CHK_RTV_HEAD%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'RTV_EXIST_STORE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_RTV_HEAD',
                    'RTV_HEAD',
                    'STORE: '||I_key_value);
   close C_CHK_RTV_HEAD;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_CONTRACT_DETAIL',
                    'CONTRACT_DETAIL',
                    'STORE: '||I_key_value);
   open C_CHK_CONTRACT_DETAIL;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_CONTRACT_DETAIL',
                    'CONTRACT_DETAIL',
                    'STORE: '||I_key_value);
   fetch C_CHK_CONTRACT_DETAIL into L_dummy;
   if C_CHK_CONTRACT_DETAIL%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'CONTRACT_EXIST_STORE');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_CONTRACT_DETAIL',
                    'CONTRACT_DETAIL',
                    'STORE: '||I_key_value);
   close C_CHK_CONTRACT_DETAIL;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_WALK_THROUGH_STORE',
                    'WALK_THROUGH_STORE',
                    'STORE: '||I_key_value);
   open C_CHK_WALK_THROUGH_STORE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_WALK_THROUGH_STORE',
                    'WALK_THROUGH_STORE',
                    'STORE: '||I_key_value);
   fetch C_CHK_WALK_THROUGH_STORE into L_dummy;
   if C_CHK_WALK_THROUGH_STORE%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'WALK_THROUGH_STORE_EXISTS');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_WALK_THROUGH_STORE',
                    'WALK_THROUGH_STORE',
                    'STORE: '||I_key_value);
   close C_CHK_WALK_THROUGH_STORE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_SISTER_STORE',
                    'STORE',
                    'STORE: '||I_key_value);
   open C_LOCK_SISTER_STORE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOCK_SISTER_STORE',
                    'STORE',
                    'STORE: '||I_key_value);
   fetch C_LOCK_SISTER_STORE into L_dummy;
   if C_LOCK_SISTER_STORE%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(I_key_value,
                'STORE',
                'SISTER_STORE_EXISTS');
      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_SISTER_STORE',
                    'STORE',
                    'STORE: '||I_key_value);
   close C_LOCK_SISTER_STORE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_DEPT_CHARGE',
                    'DEPT_CHRG_HEAD',
                    'STORE: '||I_key_value);
   open C_CHK_DEPT_CHARGE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_DEPT_CHARGE',
                    'DEPT_CHRG_HEAD',
                    'STORE: '||I_key_value);
   fetch C_CHK_DEPT_CHARGE into L_dummy;
   if C_CHK_DEPT_CHARGE%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
        VALUES(I_key_value,
               'DEPT_CHRG_HEAD',
               'DEPT_CHRG_EXIST');
      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_DEPT_CHARGE',
                    'DEPT_CHRG_HEAD',
                    'STORE: '||I_key_value);
   close C_CHK_DEPT_CHARGE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_ITEM_CHARGE',
                    'ITEM_CHRG_HEAD',
                    'STORE: '||I_key_value);
   open C_CHK_ITEM_CHARGE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_ITEM_CHARGE',
                    'ITEM_CHRG_HEAD',
                    'STORE: '||I_key_value);
   fetch C_CHK_ITEM_CHARGE into L_dummy;
   if C_CHK_ITEM_CHARGE%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||I_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
        VALUES(I_key_value,
               'ITEM_CHRG_HEAD',
               'ITEM_CHRG_EXIST');
      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_ITEM_CHARGE',
                    'ITEM_CHRG_HEAD',
                    'STORE: '||I_key_value);
   close C_CHK_ITEM_CHARGE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
   
      if C_ORDLOC_WKSHT%ISOPEN then
         close C_ORDLOC_WKSHT;
      end if;
      
      if C_CHK_STAKE_LOCATION%ISOPEN then
         close C_CHK_STAKE_LOCATION;
      end if; 
      
      if C_CHK_ORDLOC%ISOPEN then
         close C_CHK_ORDLOC;
      end if;
      
      if C_CHK_ITEMLOC_ST%ISOPEN then
         close C_CHK_ITEMLOC_ST;
      end if;
      
      if C_LAST_EOM_DATE%ISOPEN then
         close C_LAST_EOM_DATE;
      end if;  
      
      if C_CHK_TRAN_DATA_HISTORY%ISOPEN then
         close C_CHK_TRAN_DATA_HISTORY;
      end if;   
      
      if C_CHK_MONTH_DATA%ISOPEN then
         close C_CHK_MONTH_DATA;
      end if; 

      if C_CHK_TSFHEAD%ISOPEN then
         close C_CHK_TSFHEAD;
      end if; 

      if C_CHK_ALLOC_DETAIL%ISOPEN then
         close C_CHK_ALLOC_DETAIL;
      end if; 
      
      if C_CHK_RTV_HEAD%ISOPEN then
         close C_CHK_RTV_HEAD;
      end if; 
      
      if C_CHK_CONTRACT_DETAIL%ISOPEN then
         close C_CHK_CONTRACT_DETAIL;
      end if; 
      
      
      if C_CHK_WALK_THROUGH_STORE%ISOPEN then
         close C_CHK_WALK_THROUGH_STORE;
      end if;
      
      if C_LOCK_SISTER_STORE%ISOPEN then
         close C_LOCK_SISTER_STORE;
      end if; 

      if C_CHK_DEPT_CHARGE%ISOPEN then
         close C_CHK_DEPT_CHARGE;
      end if;

      if C_CHK_ITEM_CHARGE%ISOPEN then
         close C_CHK_ITEM_CHARGE;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DEL_STORE;
-----------------------------------------------------------------
FUNCTION DEL_WH(i_key_value       IN     VARCHAR2,
                o_relations_exist IN OUT VARCHAR2,
                error_message     IN OUT VARCHAR2) RETURN BOOLEAN IS

   L_dummy              VARCHAR2(1);
   L_physical_wh        BOOLEAN        := FALSE;
   L_count_vwh          INTEGER;
   L_primary_vwh        VARCHAR2(1)    := 'N';
   L_pwh_on_dly_prg     VARCHAR2(1)    := 'N';
   L_checked_for_pwh    VARCHAR2(1)    := 'N';
   L_last_eom_date      SYSTEM_VARIABLES.LAST_EOM_DATE%TYPE;

   cursor C_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where location = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_ORDLOC is
   select 'x'
     from ordloc
    where location = TO_NUMBER(i_key_value)
      and rownum = 1;

   cursor C_CHK_STAKE_LOCATION is
   select 'x'
     from stake_location
    where location = TO_NUMBER(i_key_value)
      and rownum = 1;

   cursor C_LAST_EOM_DATE is
      select last_eom_date
        from system_variables;

   cursor C_CHK_TRAN_DATA_HISTORY is
   select 'x'
     from tran_data_history
    where location = TO_NUMBER(i_key_value)
      and tran_date > L_last_eom_date
      and rownum = 1;

   cursor C_CHK_TSFHEAD is
   select 'x'
     from tsfhead
    where (to_loc = TO_NUMBER(i_key_value)
       or from_loc = TO_NUMBER(i_key_value))
      and rownum = 1;

   cursor C_CHK_ITEMLOC_WH is
      select 'x'
        from item_loc
       where loc_type = 'W'
         and loc = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_MONTH_DATA is
   select 'x'
     from month_data
    where location = TO_NUMBER(i_key_value)
      and eom_date = L_last_eom_date
      and (nvl(cls_stk_cost,0) <> 0
       or nvl(cls_stk_retail,0) <> 0)
      and rownum = 1;

   cursor C_CHK_ALLOC_DETAIL is
      select 'x'
        from alloc_detail
       where to_loc = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_ALLOC_HEADER is
      select 'x'
        from alloc_header
       where wh = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_RTV is
      select 'x'
        from rtv_head
       where wh = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_STORE is
      select 'x'
        from store
       where default_wh = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_CONTRACT_DETAIL is
      select 'x'
        from contract_detail
       where location = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_WH is
      select 'Y'
        from wh
       where physical_wh = TO_NUMBER(i_key_value)
         and stockholding_ind = 'Y'
         and rownum = 1;

   cursor C_COUNT_VWH is
      select count(wh)
        from wh
       where stockholding_ind = 'Y'
         and physical_wh = (select physical_wh
                              from wh
                             where wh = TO_NUMBER(i_key_value));

   cursor C_CHK_PRIMARY_VWH is
      select 'Y'
        from wh
       where primary_vwh = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_DLYPRG_PWH is
      select 'Y'
        from daily_purge
       where table_name = 'WH'
         and key_value = (select to_char(physical_wh)
                            from wh
                           where wh = TO_NUMBER(i_key_value))
         and rownum = 1;

  cursor C_CHK_DEPT_CHARGE is
      select 'x'
        from dept_chrg_head
       where (to_loc = TO_NUMBER(i_key_value)
          or from_loc = TO_NUMBER(i_key_value))
         and rownum = 1;

   cursor C_CHK_ITEM_CHARGE is
      select 'x'
        from item_chrg_head
       where (to_loc = TO_NUMBER(i_key_value)
          or from_loc = TO_NUMBER(i_key_value))
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   OPEN C_LAST_EOM_DATE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   FETCH C_LAST_EOM_DATE INTO L_last_eom_date;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   CLOSE C_LAST_EOM_DATE;
   ---
   o_relations_exist := 'N';
   
   if WH_ATTRIB_SQL.CHECK_PWH(error_message,
                              L_physical_wh,
                              TO_NUMBER(i_key_value)) = FALSE then
      return FALSE;
   end if;
   
   --- If a pwh, check to see if vwhs exist for it. If they do, write
   --- a message to the daily_purge_error_log table and set the relations
   --- exist flag to 'Y'. All vwh scheduled for deletion will be taken care
   --- of before the pwh by setting the delete order on daily_purge.
   --- If a vwh, make sure that 1) at least one vwh remains in the system
   --- if the pwh is not being deleted with it, and 2) that the vwh being
   --- deleted is not a primary_vwh if the pwh is not being deleted with it.

   if L_physical_wh = TRUE then
      L_dummy := NULL;
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_WH',
                       'WH',
                       'physical_wh: '||i_key_value);
      open C_CHK_WH;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_WH',
                       'WH',
                       'physical_wh: '||i_key_value);
      fetch C_CHK_WH into L_dummy;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_WH',
                       'WH',
                       'physical_wh: '||i_key_value);
      close C_CHK_WH;
      ---
      if L_dummy = 'Y' then
         o_relations_exist := 'Y';
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'physical_wh: '||i_key_value);
         INSERT INTO DAILY_PURGE_ERROR_LOG
               VALUES(i_key_value,
                      'WH',
                     'CANNOT_DEL_PWH');
      end if;
   else
     --- Count the vwh, need more than one to delete this vwh unless the pwh
     --- is also on daily_purge.
      SQL_LIB.SET_MARK('OPEN',
                       'C_COUNT_VWH',
                       'WH',
                       'virtual wh: '||i_key_value);
      open C_COUNT_VWH;
      --
      SQL_LIB.SET_MARK('FETCH',
                       'C_COUNT_VWH',
                       'WH',
                       'virtual wh: '||i_key_value);
      fetch C_COUNT_VWH into L_count_vwh;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_COUNT_VWH',
                       'WH',
                       'virtual wh: '||i_key_value);
      close C_COUNT_VWH;
      --
      if L_count_vwh = 1 then
      --
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHK_DLYPRG_PWH',
                          'WH',
                          'virtual wh: '||i_key_value);
         open C_CHK_DLYPRG_PWH;
         --
         SQL_LIB.SET_MARK('FETCH',
                          'C_CHK_DLYPRG_PWH',
                          'WH',
                          'virtual wh: '||i_key_value);
         fetch C_CHK_DLYPRG_PWH into L_pwh_on_dly_prg;
         --
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHK_DLYPRG_PWH',
                          'WH',
                          'virtual wh: '||i_key_value);
         close C_CHK_DLYPRG_PWH;
         --
         if L_pwh_on_dly_prg = 'N' then
            o_relations_exist := 'Y';
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'DAILY_PURGE_ERROR_LOG',
                             'virtual wh: '||i_key_value);
            INSERT INTO DAILY_PURGE_ERROR_LOG
                 VALUES(i_key_value,
                        'WH',
                        'CANNOT_DEL_VWH');
         end if;
         --
         L_checked_for_pwh := 'Y';
            --
      end if;

      --- Check if this is a primary_vwh.  If so, can't delete unless the pwh
      --- is also on daily_purge.
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_PRIMARY_VWH',
                       'WH',
                       'virtual wh: '||i_key_value);
      open C_CHK_PRIMARY_VWH;
       --
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_PRIMARY_VWH',
                       'WH',
                       'virtual wh: '||i_key_value);
      fetch C_CHK_PRIMARY_VWH into L_primary_vwh;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_PRIMARY_VWH',
                       'WH',
                       'virtual wh: '||i_key_value);
      close C_CHK_PRIMARY_VWH;
      --
      if L_primary_vwh = 'Y' then
      --- If we already checked for the pwh on daily purge above, then we don't
      --- need to open/fetch/close that cursor again.
         if L_checked_for_pwh = 'N' then
         --
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHK_DLYPRG_PWH',
                             'WH',
                             'virtual wh: '||i_key_value);
            open C_CHK_DLYPRG_PWH;
            --
            SQL_LIB.SET_MARK('FETCH',
                             'C_CHK_DLYPRG_PWH',
                             'WH',
                              'virtual wh: '||i_key_value);
            fetch C_CHK_DLYPRG_PWH into L_pwh_on_dly_prg;
            --
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHK_DLYPRG_PWH',
                             'WH',
                             'virtual wh: '||i_key_value);
            close C_CHK_DLYPRG_PWH;
         end if;
         --
         if L_pwh_on_dly_prg = 'N' then
            o_relations_exist := 'Y';
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                            'DAILY_PURGE_ERROR_LOG',
                            'virtual wh: '||i_key_value);
            INSERT INTO DAILY_PURGE_ERROR_LOG
                 VALUES(i_key_value,
                        'WH',
                        'CANNOT_DEL_PRIMARY_VWH');
         else -- remove this vwh as the primary_vwh so it can be deleted
            update wh
               set primary_vwh = NULL
             where primary_vwh = i_key_value;
         end if;
      end if;
      --
      SQL_LIB.SET_MARK('OPEN',
                       'C_ORDLOC_WKSHT',
                       'ORDLOC_WKSHT',
                       'COLOR: '||i_key_value);
      OPEN C_ORDLOC_WKSHT;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ORDLOC_WKSHT',
                       'ORDLOC_WKSHT',
                       'COLOR: '||i_key_value);
      FETCH C_ORDLOC_WKSHT INTO L_dummy;
      if C_ORDLOC_WKSHT%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO DAILY_PURGE_ERROR_LOG
            VALUES(i_key_value,
                   'WH',
                   'ORDLOC_WKSHT_WH');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ORDLOC_WKSHT',
                       'ORDLOC_WKSHT',
                       'COLOR: '||i_key_value);
      CLOSE C_ORDLOC_WKSHT;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_ORDLOC',
                       'ORDLOC',
                       'LOCATION: '||i_key_value);
      OPEN C_CHK_ORDLOC;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_ORDLOC',
                       'ORDLOC',
                       'LOCATION: '||i_key_value);
      FETCH C_CHK_ORDLOC INTO L_dummy;
      if C_CHK_ORDLOC%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'ORD_EXIST_WH');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_ORDLOC',
                       'ORDLOC',
                       'LOCATION: '||i_key_value);
      CLOSE C_CHK_ORDLOC;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_STAKE_LOCATION',
                       'STAKE_LOCATION',
                       'WH: '||i_key_value);
      OPEN C_CHK_STAKE_LOCATION;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_STAKE_LOCATION',
                       'STAKE_LOCATION',
                       'WH: '||i_key_value);
      FETCH C_CHK_STAKE_LOCATION INTO L_dummy;
      if C_CHK_STAKE_LOCATION%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'CYCLE_COUNT_EXIST_WH');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_STAKE_LOCATION',
                       'STAKE_LOCATION',
                       'WH: '||i_key_value);
      CLOSE C_CHK_STAKE_LOCATION;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_TRAN_DATA_HISTORY',
                       'TRAN_DATA_HISTORY',
                       'WH: '||i_key_value);
      OPEN C_CHK_TRAN_DATA_HISTORY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_TRAN_DATA_HISTORY',
                       'TRAN_DATA_HISTORY',
                       'WH: '||i_key_value);
      FETCH C_CHK_TRAN_DATA_HISTORY INTO L_dummy;
      if C_CHK_TRAN_DATA_HISTORY%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'TRAN_DATA_HISTORY_EXIST');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_TRAN_DATA_HISTORY',
                       'TRAN_DATA_HISTORY',
                       'WH: '||i_key_value);
      CLOSE C_CHK_TRAN_DATA_HISTORY;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_TSFHEAD',
                       'TSFHEAD',
                       'WH: '||i_key_value);
      OPEN C_CHK_TSFHEAD;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_TSFHEAD',
                       'TSFHEAD',
                       'WH: '||i_key_value);
      FETCH C_CHK_TSFHEAD INTO L_dummy;
      if C_CHK_TSFHEAD%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'TSFHEAD_EXIST_WH');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_TSFHEAD',
                       'TSFHEAD',
                       'WH: '||i_key_value);
      CLOSE C_CHK_TSFHEAD;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_ITEMLOC_WH',
                       'ITEM_LOC',
                       'WH: '||i_key_value);
      OPEN C_CHK_ITEMLOC_WH;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_ITEMLOC_WH',
                       'ITEM_LOC',
                       'WH: '||i_key_value);
      FETCH C_CHK_ITEMLOC_WH INTO L_dummy;
      if C_CHK_ITEMLOC_WH%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'ITEM_EXIST_WH');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_ITEMLOC_WH',
                       'ITEM_LOC',
                       'WH: '||i_key_value);
      CLOSE C_CHK_ITEMLOC_WH;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_MONTH_DATA',
                       'MONTH_DATA',
                       'WH: '||i_key_value);
      OPEN C_CHK_MONTH_DATA;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_MONTH_DATA',
                       'MONTH_DATA',
                       'WH: '||i_key_value);
      FETCH C_CHK_MONTH_DATA INTO L_dummy;
      if C_CHK_MONTH_DATA%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'MTH_DATA_WH');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_MONTH_DATA',
                       'MONTH_DATA',
                       'WH: '||i_key_value);
      CLOSE C_CHK_MONTH_DATA;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_ALLOC_DETAIL',
                       'ALLOC_DETAIL',
                       'TO_LOC: '||i_key_value);
      open C_CHK_ALLOC_DETAIL;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_ALLOC_DETAIL',
                       'ALLOC_DETAIL',
                       'TO_LOC: '||i_key_value);
      fetch C_CHK_ALLOC_DETAIL into L_dummy;
      if C_CHK_ALLOC_DETAIL%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'ALLOC_DETAIL_EXIST');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_ALLOC_DETAIL',
                       'ALLOC_DETAIL',
                       'TO_LOC: '||i_key_value);
      close C_CHK_ALLOC_DETAIL;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_ALLOC_HEADER',
                       'ALLOC_HEADER',
                       'WH: '||i_key_value);
      open C_CHK_ALLOC_HEADER;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_ALLOC_HEADER',
                       'ALLOC_HEADER',
                       'WH: '||i_key_value);
      fetch C_CHK_ALLOC_HEADER into L_dummy;
      if C_CHK_ALLOC_HEADER%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'ALLOC_DETAIL_EXIST');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_ALLOC_HEADER',
                       'ALLOC_HEADER',
                       'WH: '||i_key_value);
      close C_CHK_ALLOC_HEADER;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_RTV',
                       'RTV_HEAD',
                       'WH: '||i_key_value);
      open C_CHK_RTV;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_RTV',
                       'RTV_HEAD',
                       'WH: '||i_key_value);
      fetch C_CHK_RTV into L_dummy;
      if C_CHK_RTV%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'RTV_EXIST_WH');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_RTV',
                       'RTV_HEAD',
                       'WH: '||i_key_value);
      close C_CHK_RTV;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_STORE',
                       'STORE',
                       'WH: '||i_key_value);
      open C_CHK_STORE;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_STORE',
                       'STORE',
                       'WH: '||i_key_value);
      fetch C_CHK_STORE into L_dummy;
      if C_CHK_STORE%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO daily_purge_error_log
            VALUES(i_key_value,
                   'WH',
                   'STORE_WH_EXIST');

         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_STORE',
                       'STORE',
                       'WH: '||i_key_value);
      close C_CHK_STORE;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_CONTRACT_DETAIL',
                       'CONTRACT_DETAIL',
                       'WH: '||i_key_value);
      open C_CHK_CONTRACT_DETAIL;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_CONTRACT_DETAIL',
                       'CONTRACT_DETAIL',
                       'WH: '||i_key_value);
      fetch C_CHK_CONTRACT_DETAIL into L_dummy;
      if C_CHK_CONTRACT_DETAIL%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||i_key_value);
         INSERT INTO DAILY_PURGE_ERROR_LOG
            VALUES(i_key_value,
                   'WH',
                   'CONTRACT_EXIST_WH');
         o_relations_exist := 'Y';
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_CONTRACT_DETAIL',
                       'CONTRACT_DETAIL',
                       'WH: '||i_key_value);
      close C_CHK_CONTRACT_DETAIL;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_DEPT_CHARGE',
                    'DEPT_CHRG_HEAD',
                    'WH: '||i_key_value);
   open C_CHK_DEPT_CHARGE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_DEPT_CHARGE',
                    'DEPT_CHRG_HEAD',
                    'WH: '||i_key_value);
   fetch C_CHK_DEPT_CHARGE into L_dummy;
   if C_CHK_DEPT_CHARGE%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
        VALUES(I_key_value,
               'DEPT_CHRG_HEAD',
               'DEPT_CHRG_EXIST');
      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_DEPT_CHARGE',
                    'DEPT_CHRG_HEAD',
                    'WH: '||i_key_value);
   close C_CHK_DEPT_CHARGE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_ITEM_CHARGE',
                    'ITEM_CHRG_HEAD',
                    'WH: '||i_key_value);
   open C_CHK_ITEM_CHARGE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_ITEM_CHARGE',
                    'ITEM_CHRG_HEAD',
                    'WH: '||i_key_value);
   fetch C_CHK_ITEM_CHARGE into L_dummy;
   if C_CHK_ITEM_CHARGE%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                        NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
        VALUES(I_key_value,
               'ITEM_CHRG_HEAD',
               'ITEM_CHRG_EXIST');
      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_ITEM_CHARGE',
                    'ITEM_CHRG_HEAD',
                    'WH: '||i_key_value);
   close C_CHK_ITEM_CHARGE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'DEL_WH',
                                          TO_CHAR(SQLCODE));
      return FALSE;
END DEL_WH;
------------------------------------------------------------------
FUNCTION DEL_COST_ZONE_GROUP(i_key_value       IN     VARCHAR2,
                             o_relations_exist IN OUT VARCHAR2,
                             error_message     IN OUT VARCHAR2)
                                                               RETURN BOOLEAN IS
   L_dummy VARCHAR2(1);

   cursor C_ITEM_MASTER is
      select 'x'
        from item_master
       where cost_zone_group_id = to_number(i_key_value);

BEGIN
   o_relations_exist := 'N';

   SQL_LIB.SET_MARK('OPEN','C_ITEM_MASTER',
                    'ITEM_MASTER','COST_ZONE_GROUP_ID: '||i_key_value);
   OPEN  C_ITEM_MASTER;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_MASTER',
                    'ITEM_MASTER','COST_ZONE_GROUP_ID: '||i_key_value);
   FETCH C_ITEM_MASTER INTO L_dummy;
   if C_ITEM_MASTER%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL,
                    'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||i_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(i_key_value,
                'COST_ZONE_GROUP',
                'ITEM_IN_ZONE_GROUP');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_MASTER',
                    'ITEM_MASTER','COST_ZONE_GROUP_ID: '||i_key_value);
   CLOSE C_ITEM_MASTER;

   return TRUE;

EXCEPTION
   when OTHERS then
      error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                          'DEL_COST_ZONE_GROUP', SQLCODE);
      return FALSE;
END;
------------------------------------------------------------------
FUNCTION DEL_SHIPMENT(i_key_value       IN     VARCHAR2,
                      o_relations_exist IN OUT VARCHAR2,
                      error_message     IN OUT VARCHAR2)
                     RETURN BOOLEAN IS
   L_dummy VARCHAR2(1);

   cursor C_VAL_SHIP is
      select 'x'
        from shipsku
       where shipsku.shipment = to_number(i_key_value);
BEGIN
   o_relations_exist := 'N';

   SQL_LIB.SET_MARK('OPEN','C_VAL_SHIPMENT',
                    'SHIPMENT','SHIPMENT: '||i_key_value);
   OPEN C_VAL_SHIP;
   SQL_LIB.SET_MARK('FETCH','C_VAL_SHIPMENT',
                    'SHIPMENT','SHIPMENT: '||i_key_value);
   FETCH C_VAL_SHIP INTO L_dummy;
   if C_VAL_SHIP%FOUND then
      SQL_LIB.SET_MARK('INSERT',NULL,
                    'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||i_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(i_key_value,
                'SHIPMENT',
                'CANNOT_DEL_RECV_SHIP');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_VAL_SHIPMENT',
                    'SHIPMENT','SHIPMENT: '||i_key_value);
   CLOSE C_VAL_SHIP;

   return TRUE;

EXCEPTION
   when OTHERS then
      error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                          'DEL_SHIPMENT', SQLCODE);
      return FALSE;
END;
------------------------------------------------------------------
FUNCTION DEL_PACK_TEMPLATE(i_key_value       IN      VARCHAR2,
                           o_relations_exist IN OUT  VARCHAR2,
                           error_message     IN OUT  VARCHAR2) RETURN BOOLEAN IS

   L_dummy   VARCHAR2(1);

   cursor C_PACK_TEMPLATE is
      select 'x'
        from packitem
       where pack_tmpl_id = to_number(i_key_value);
BEGIN
   o_relations_exist := 'N';
   SQL_LIB.SET_MARK('OPEN','C_PACK_TEMPLATE',
                    'PACKITEM','PACK TEMPLATE: '||i_key_value);
   open C_PACK_TEMPLATE;
   SQL_LIB.SET_MARK('FETCH','C_PACK_TEMPLATE',
                    'PACKITEM','PACK TEMPLATE: '||i_key_value);
   fetch C_PACK_TEMPLATE into L_dummy;
   if C_PACK_TEMPLATE%FOUND then
      o_relations_exist := 'Y';
      SQL_LIB.SET_MARK('INSERT',NULL,
                    'DAILY_PURGE_ERROR_LOG','KEY_VALUE: '||i_key_value);
      insert into DAILY_PURGE_ERROR_LOG
         values(i_key_value,
                'PACKITEM',
                'PCK_TEMP_EXISTS_PACKITEM');
      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_PACK_TEMPLATE',
                    'PACKITEM','PACK TEMPLATE: '||i_key_value);
   close C_PACK_TEMPLATE;

   return TRUE;

EXCEPTION
   when OTHERS then
      error_message := sql_lib.create_msg('PACKAGE_ERROR',SQLERRM,
                                          'DEL_PACK_TEMPLATE', SQLCODE);
      return FALSE;

END;
----------------------------------------------------------------------------------
FUNCTION DEL_EXTERNAL_FINISHER(i_key_value       IN     VARCHAR2,
                               o_relations_exist IN OUT VARCHAR2,
                               error_message     IN OUT VARCHAR2) RETURN BOOLEAN IS

   L_dummy              VARCHAR2(1);
   L_last_eom_date   SYSTEM_VARIABLES.LAST_EOM_DATE%TYPE;

   cursor C_LAST_EOM_DATE is
      select last_eom_date
        from system_variables;

   cursor C_ORDLOC_WKSHT is
      select 'x'
        from ordloc_wksht
       where location = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_ORDLOC is
   select 'x'
     from ordloc
    where location = TO_NUMBER(i_key_value)
      and rownum = 1;

   cursor C_CHK_STAKE_LOCATION is
   select 'x'
     from stake_location
    where location = TO_NUMBER(i_key_value)
      and rownum = 1;

   cursor C_CHK_TRAN_DATA_HISTORY is
   select 'x'
     from tran_data_history
    where location = TO_NUMBER(i_key_value)
      and tran_date > L_last_eom_date
      and rownum = 1;

   cursor C_CHK_TSFHEAD is
   select 'x'
     from tsfhead
    where (to_loc = TO_NUMBER(i_key_value)
       or from_loc = TO_NUMBER(i_key_value))
      and rownum = 1;

   cursor C_CHK_ITEMLOC_EF is
      select 'x'
        from item_loc
       where loc_type = 'E'
         and loc = TO_NUMBER(i_key_value)
         and rownum = 1;

   cursor C_CHK_MONTH_DATA is
   select 'x'
     from month_data
    where location = TO_NUMBER(i_key_value)
      and eom_date = L_last_eom_date
      and (nvl(cls_stk_cost,0) <> 0
       or nvl(cls_stk_retail,0) <> 0)
      and rownum = 1;

   cursor C_CHK_FIF_GL_CROSS_REF is
      select 'x'
        from fif_gl_cross_ref
       where location = TO_NUMBER(I_key_value)
         and rownum = 1;


BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   OPEN C_LAST_EOM_DATE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   FETCH C_LAST_EOM_DATE INTO L_last_eom_date;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LAST_EOM_DATE',
                    'SYSTEM_VARIABLES',
                    NULL);
   CLOSE C_LAST_EOM_DATE;
   ---

   o_relations_exist := 'N';

   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'COLOR: '||i_key_value);
   OPEN C_ORDLOC_WKSHT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'COLOR: '||i_key_value);
   FETCH C_ORDLOC_WKSHT INTO L_dummy;
   if C_ORDLOC_WKSHT%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO DAILY_PURGE_ERROR_LOG
         VALUES(i_key_value,
                'PARTNER',
                'ORDLOC_WKSHT_EF');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDLOC_WKSHT',
                    'ORDLOC_WKSHT',
                    'COLOR: '||i_key_value);
   CLOSE C_ORDLOC_WKSHT;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_ORDLOC',
                    'ORDLOC',
                    'LOCATION: '||i_key_value);
   OPEN C_CHK_ORDLOC;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_ORDLOC',
                    'ORDLOC',
                    'LOCATION: '||i_key_value);
   FETCH C_CHK_ORDLOC INTO L_dummy;
   if C_CHK_ORDLOC%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO daily_purge_error_log
         VALUES(i_key_value,
                'PARTNER',
                'ORD_EXIST_EF');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_ORDLOC',
                    'ORDLOC',
                    'LOCATION: '||i_key_value);
   CLOSE C_CHK_ORDLOC;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_STAKE_LOCATION',
                    'STAKE_LOCATION',
                    'LOCATION: '||i_key_value);
   OPEN C_CHK_STAKE_LOCATION;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_STAKE_LOCATION',
                    'STAKE_LOCATION',
                    'LOCATION: '||i_key_value);
   FETCH C_CHK_STAKE_LOCATION INTO L_dummy;
   if C_CHK_STAKE_LOCATION%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO daily_purge_error_log
         VALUES(i_key_value,
                'PARTNER',
                'CYCLE_COUNT_EXIST_EF');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_STAKE_LOCATION',
                    'STAKE_LOCATION',
                    'LOCATION: '||i_key_value);
   CLOSE C_CHK_STAKE_LOCATION;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_TRAN_DATA_HISTORY',
                    'TRAN_DATA_HISTORY',
                    'LOCATION: '||i_key_value);
   OPEN C_CHK_TRAN_DATA_HISTORY;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_TRAN_DATA_HISTORY',
                    'TRAN_DATA_HISTORY',
                    'LOCATION: '||i_key_value);
   FETCH C_CHK_TRAN_DATA_HISTORY INTO L_dummy;
   if C_CHK_TRAN_DATA_HISTORY%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO daily_purge_error_log
         VALUES(i_key_value,
                'PARTNER',
                'TRAN_DATA_HISTORY_EXIST');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_TRAN_DATA_HISTORY',
                    'TRAN_DATA_HISTORY',
                    'LOCATION: '||i_key_value);
   CLOSE C_CHK_TRAN_DATA_HISTORY;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_TSFHEAD',
                    'TSFHEAD',
                    'LOCATION: '||i_key_value);
   OPEN C_CHK_TSFHEAD;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_TSFHEAD',
                    'TSFHEAD',
                    'LOCATION: '||i_key_value);
   FETCH C_CHK_TSFHEAD INTO L_dummy;
   if C_CHK_TSFHEAD%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO daily_purge_error_log
         VALUES(i_key_value,
                'PARTNER',
                'TSFHEAD_EXIST_EF');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_TSFHEAD',
                    'TSFHEAD',
                    'LOCATION: '||i_key_value);
   CLOSE C_CHK_TSFHEAD;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_ITEMLOC_EF',
                    'ITEM_LOC',
                    'LOCATION: '||i_key_value);
   OPEN C_CHK_ITEMLOC_EF;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_ITEMLOC_EF',
                    'ITEM_LOC',
                    'LOCATION: '||i_key_value);
   FETCH C_CHK_ITEMLOC_EF INTO L_dummy;
   if C_CHK_ITEMLOC_EF%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO daily_purge_error_log
         VALUES(i_key_value,
                'PARTNER',
                'ITEM_EXIST_EF');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_ITEMLOC_EF',
                    'ITEM_LOC',
                    'LOCATION: '||i_key_value);
   CLOSE C_CHK_ITEMLOC_EF;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_MONTH_DATA',
                    'MONTH_DATA',
                    'LOCATION: '||i_key_value);
   OPEN C_CHK_MONTH_DATA;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_MONTH_DATA',
                    'MONTH_DATA',
                    'LOCATION: '||i_key_value);
   FETCH C_CHK_MONTH_DATA INTO L_dummy;
   if C_CHK_MONTH_DATA%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: '||i_key_value);
      INSERT INTO daily_purge_error_log
         VALUES(i_key_value,
                'PARTNER',
                'MTH_DATA_EF');

      o_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_MONTH_DATA',
                    'MONTH_DATA',
                    'LOCATION: '||i_key_value);
   CLOSE C_CHK_MONTH_DATA;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_FIF_GL_CROSS_REF',
                    'FIF_GL_CROSS_REF',
                    'LOCATION: ' || I_key_value);
   OPEN C_CHK_FIF_GL_CROSS_REF;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_FIF_GL_CROSS_REF',
                    'FIF_GL_CROSS_REF',
                    'LOCATION: ' || I_key_value);
   FETCH C_CHK_FIF_GL_CROSS_REF INTO L_dummy;
   if C_CHK_FIF_GL_CROSS_REF%FOUND then
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'DAILY_PURGE_ERROR_LOG',
                       'KEY_VALUE: ' || I_key_value);
      insert into daily_purge_error_log
         values(I_key_value,
                'PARTNER',
                'XREF_EXIST');

      O_relations_exist := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_FIF_GL_CROSS_REF',
                    'FIF_GL_CROSS_REF',
                    'LOCATION: ' || I_key_value);
   CLOSE C_CHK_FIF_GL_CROSS_REF;

   return TRUE;

EXCEPTION
   when OTHERS then
      error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          'DEL_EXTERNAL_FINISHER',
                                          TO_CHAR(SQLCODE));
      return FALSE;
END DEL_EXTERNAL_FINISHER;
------------------------------------------------------------------
FUNCTION VALIDATE_PRIMARY_PACK_NO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists            IN OUT   VARCHAR2,
                                  I_item              IN       ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'VALIDATE_RECORDS_SQL.VALIDATE_PRIMARY_PACK_NO';
   L_exists           VARCHAR2(1);

   cursor C_REPL_ITEM_LOC is
      select /*+ first_rows(1) */ 'x'
        from repl_item_loc rpl
       where rpl.primary_pack_no = I_item
         and rownum = 1;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   if I_item is not NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_REPL_ITEM_LOC',
                       'REPL_ITEM_LOC',
                       'ITEM: '||I_item);
      open C_REPL_ITEM_LOC;

      SQL_LIB.SET_MARK('FETCH',
                       'C_REPL_ITEM_LOC',
                       'REPL_ITEM_LOC',
                       'ITEM: '||I_item);
      fetch C_REPL_ITEM_LOC INTO O_exists;

      if C_REPL_ITEM_LOC%FOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'DAILY_PURGE_ERROR_LOG',
                          'KEY_VALUE: '||I_item);
         insert into DAILY_PURGE_ERROR_LOG
            values(I_item,
                   'REPL_ITEM_LOC',
                   'CHECK_PRIMARY_REPL_PACK');
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_REPL_ITEM_LOC',
                       'REPL_ITEM_LOC',
                       'ITEM: '||I_item);
      close C_REPL_ITEM_LOC;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_PRIMARY_PACK_NO;
------------------------------------------------------------------
FUNCTION DEL_DAILY_PURGE_ERROR_LOG(I_key_value       IN      VARCHAR2,
                                   O_error_message   IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(60) := 'VALIDATE_RECORDS_SQL.DEL_DAILY_PURGE_ERROR_LOG';

BEGIN

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'DAILY_PURGE_ERROR_LOG',
                    'KEY_VALUE: ' || I_key_value);

   delete from daily_purge_error_log
          where key_value = I_key_value;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DEL_DAILY_PURGE_ERROR_LOG;
------------------------------------------------------------------
END;
/
