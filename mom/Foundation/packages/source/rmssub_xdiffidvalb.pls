
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XDIFFID_VALIDATE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the create and modify messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XDiffIDDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will populate all the fields in the DIFF_IDS%ROWTYPE with
   --                the values from the create and modify RIB messages.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diffid_rec      OUT    NOCOPY   DIFF_IDS%ROWTYPE,
                         I_message         IN              "RIB_XDiffIDDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will populate all the fields in the DIFF_IDS%ROWTYPE with
   --                the values from the delete RIB message.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diffid_rec      OUT    NOCOPY   DIFF_IDS%ROWTYPE,
                         I_message         IN              "RIB_XDiffIDRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: DIFFID_EXISTS
   -- Purpose      : This function will check Diff_id existence by calling DIFF_ID_SQL.DIFF_ID_EXISTS.
-------------------------------------------------------------------------------------------------------
FUNCTION DIFFID_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid          IN       DIFF_IDS.DIFF_ID%TYPE,
                       I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DELETE
   -- Purpose      : This function will check if the Diff_id is associated with an item, diff range, or
   --                pack template.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_diffid          IN       DIFF_IDS.DIFF_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diffid_rec      OUT    NOCOPY   DIFF_IDS%ROWTYPE,
                       I_message         IN              "RIB_XDiffIDDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XDIFFID_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   -- RDF cannot handle white space or underscores in diff ids. 
   -- For RMS to integrate with RDF, diff_ids cannot contain white spaces or undersores.  
   if I_message_type = RMSSUB_XDIFFID.LP_cre_type then
      if instr(I_message.diff_id, ' ') > 0 or
         instr(I_message.diff_id, '_') > 0 then
         O_error_message := SQL_LIB.CREATE_MSG('DIFF_ID_NO_WS_US', I_message.diff_id, NULL, NULL);
         return FALSE;
      end if;
   end if;

   if not DIFFID_EXISTS(O_error_message,
                        I_message.diff_id,
                        I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_diffid_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message  IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diffid_rec     OUT    NOCOPY   DIFF_IDS%ROWTYPE,
                       I_message        IN              "RIB_XDiffIDRef_REC",
                       I_message_type   IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFID_VALIDATE.CHECK_MESSAGE';

BEGIN

   if I_message.diff_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'diff_id', NULL, NULL);
      return FALSE;
   end if;

   if not DIFFID_EXISTS(O_error_message,
                        I_message.diff_id,
                        I_message_type) then
      return FALSE;
   end if;
      
   if CHECK_DELETE(O_error_message,
                   I_message.diff_id) = FALSE then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_diffid_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message          IN       "RIB_XDiffIDDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFID_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.diff_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'diff_id', NULL, NULL);
      return FALSE;
   end if;

   if I_message.diff_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'diff_type', NULL, NULL);
      return FALSE;
   end if;

   if I_message.diff_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'diff_desc', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diffid_rec      OUT    NOCOPY   DIFF_IDS%ROWTYPE,
                         I_message         IN              "RIB_XDiffIDDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)                        := 'RMSSUB_XDIFFID_VALIDATE.POPULATE_RECORD';
   L_user         DIFF_IDS.LAST_UPDATE_ID%TYPE        := get_user;
   L_date         DIFF_IDS.LAST_UPDATE_DATETIME%TYPE  := sysdate;


BEGIN

   O_diffid_rec.diff_id              := I_message.diff_id;
   O_diffid_rec.diff_type            := I_message.diff_type;
   O_diffid_rec.diff_desc            := I_message.diff_desc;
   O_diffid_rec.industry_code        := I_message.industry_code;
   O_diffid_rec.industry_subgroup    := I_message.industry_subgroup;
   O_diffid_rec.create_datetime      := NVL(I_message.create_datetime, L_date);
   O_diffid_rec.last_update_id       := L_user;
   O_diffid_rec.last_update_datetime := L_date;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diffid_rec      OUT    NOCOPY   DIFF_IDS%ROWTYPE,
                         I_message         IN              "RIB_XDiffIDRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFID_VALIDATE.POPULATE_RECORD';

BEGIN

   O_diffid_rec.diff_id := I_message.diff_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION DIFFID_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_diffid          IN       DIFF_IDS.DIFF_ID%TYPE,
                       I_message_type    IN       VARCHAR2)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFID_VALIDATE.DIFFID_EXISTS';
   L_exists       BOOLEAN      := FALSE;

BEGIN

   if DIFF_ID_SQL.DIFF_ID_EXISTS(O_error_message,
                                 L_exists,
                                 I_diffid) = FALSE then
      return FALSE;
   end if;

   if L_exists and I_message_type = RMSSUB_XDIFFID.LP_cre_type then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE_EXIST', 'Diff ID', NULL, NULL);
      return FALSE;
   elsif not L_exists and I_message_type in (RMSSUB_XDIFFID.LP_mod_type, RMSSUB_XDIFFID.LP_del_type) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Diff ID', I_diffid, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DIFFID_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_diffid          IN       DIFF_IDS.DIFF_ID%TYPE)

   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'RMSSUB_XDIFFID_VALIDATE.CHECK_DELETE';
   L_items_exists           BOOLEAN      := FALSE;
   L_group_details_exists   BOOLEAN      := FALSE;
   L_range_details_exists   BOOLEAN      := FALSE;

BEGIN

   if DIFF_ID_SQL.CHECK_DELETE(O_error_message,
                               L_items_exists,
                               L_group_details_exists,
                               L_range_details_exists,
                               I_diffid) = FALSE then
      return FALSE;
   end if;
   
   if L_items_exists then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_DIFF_EXISTS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;   
   elsif L_group_details_exists then
      O_error_message := SQL_LIB.CREATE_MSG('DIFF_GROUP',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   elsif L_range_details_exists then
      O_error_message := SQL_LIB.CREATE_MSG('DIFF_RANGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_DELETE;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XDIFFID_VALIDATE;
/
