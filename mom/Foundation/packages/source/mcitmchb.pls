
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY MCITEM_CHRG_SQL AS
-------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEM_LIST_AND_SAVE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_count           IN OUT   VARCHAR2,
                                    I_item_list       IN       MC_CHRG_HEAD.ITEM_LIST%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(50) := 'MCITEM_CHRG_SQL.EXPLODE_ITEM_LIST_AND_SAVE';
   L_table                    VARCHAR2(20);
   L_exists                   BOOLEAN;

   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(Record_locked,-54);
   ADD_COMPONENT              CONSTANT MC_CHRG_DETAIL.MAINTENANCE_TYPE%TYPE := 'A';
   DELETE_COMPONENT           CONSTANT MC_CHRG_DETAIL.MAINTENANCE_TYPE%TYPE := 'D';
   L_previous_item            SKULIST_DETAIL.ITEM%TYPE                      := NULL;
   L_previous_from_loc        MC_CHRG_DETAIL.FROM_LOC%TYPE                  := NULL;
   L_previous_to_loc          MC_CHRG_DETAIL.TO_LOC%TYPE                    := NULL;
   L_previous_from_loc_type   MC_CHRG_DETAIL.FROM_LOC_TYPE%TYPE             := NULL;
   L_previous_to_loc_type     MC_CHRG_DETAIL.TO_LOC_TYPE%TYPE               := NULL;
   L_item_type                VARCHAR2(2);
   L_total_count              NUMBER := 0;
   L_reject_count             NUMBER := 0;
   L_accept_count             NUMBER := 0;

   l_detcount                 NUMBER := 0;
   l_headcount                NUMBER := 0;

   l_insheadcount             NUMBER := 0;
   l_insdetcount              NUMBER := 0;

   L_mc_item                  ITEM_MASTER.ITEM%TYPE := 0;
   L_mc_previous_item         ITEM_MASTER.ITEM%TYPE := 0;


   TYPE il_item IS TABLE OF ITEM_CHRG_DETAIL.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_from_loc IS TABLE OF ITEM_CHRG_DETAIL.FROM_LOC%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_to_loc IS TABLE OF ITEM_CHRG_DETAIL.TO_LOC%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_from_loc_type IS TABLE OF ITEM_CHRG_DETAIL.FROM_LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_to_loc_type IS TABLE OF ITEM_CHRG_DETAIL.TO_LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_comp_id IS TABLE OF ITEM_CHRG_DETAIL.COMP_ID%TYPE INDEX BY BINARY_INTEGER;

   TYPE iil_item IS TABLE OF ITEM_CHRG_DETAIL.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_from_loc IS TABLE OF ITEM_CHRG_DETAIL.FROM_LOC%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_to_loc IS TABLE OF ITEM_CHRG_DETAIL.TO_LOC%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_from_loc_type IS TABLE OF ITEM_CHRG_DETAIL.FROM_LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_to_loc_type IS TABLE OF ITEM_CHRG_DETAIL.TO_LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_comp_id IS TABLE OF ITEM_CHRG_DETAIL.COMP_ID%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_comp_rate IS TABLE OF ITEM_CHRG_DETAIL.COMP_RATE%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_per_count IS TABLE OF ITEM_CHRG_DETAIL.PER_COUNT%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_per_count_uom IS TABLE OF ITEM_CHRG_DETAIL.PER_COUNT_UOM%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_up_chrg_group IS TABLE OF ITEM_CHRG_DETAIL.UP_CHRG_GROUP%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_comp_currency IS TABLE OF ITEM_CHRG_DETAIL.COMP_CURRENCY%TYPE INDEX BY BINARY_INTEGER;
   TYPE iil_display_order IS TABLE OF ITEM_CHRG_DETAIL.DISPLAY_ORDER%TYPE INDEX BY BINARY_INTEGER;

   p_il_item il_item;
   p_il_from_loc il_from_loc;
   p_il_to_loc il_to_loc;
   p_il_from_loc_type il_from_loc_type;
   p_il_to_loc_type il_to_loc_type;
   p_il_comp_id il_comp_id;

   p_iilhead_item il_item;
   p_iilhead_from_loc il_from_loc;
   p_iilhead_to_loc il_to_loc;
   p_iilhead_from_loc_type il_from_loc_type;
   p_iilhead_to_loc_type il_to_loc_type;

   p_iil_item il_item;
   p_iil_from_loc il_from_loc;
   p_iil_to_loc il_to_loc;
   p_iil_from_loc_type il_from_loc_type;
   p_iil_to_loc_type il_to_loc_type;
   p_iil_comp_id il_comp_id;
   p_iil_comp_rate iil_comp_rate;
   p_iil_per_count iil_per_count;
   p_iil_per_count_uom iil_per_count_uom;
   p_iil_up_chrg_group iil_up_chrg_group;
   p_iil_comp_currency iil_comp_currency;
   p_iil_display_order iil_display_order;

   p_ilhead_item il_item;
   p_ilhead_from_loc il_from_loc;
   p_ilhead_to_loc il_to_loc;
   p_ilhead_from_loc_type il_from_loc_type;
   p_ilhead_to_loc_type il_to_loc_type;

   TYPE mcd_item_list IS TABLE OF SKULIST_DETAIL.SKULIST%TYPE INDEX BY BINARY_INTEGER;
   TYPE mcd_item IS TABLE OF SKULIST_DETAIL.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE mcd_maintenance_type IS TABLE OF MC_CHRG_DETAIL.MAINTENANCE_TYPE%TYPE INDEX BY BINARY_INTEGER;

   p_mcd_item_list mcd_item_list;
   p_mcd_from_loc il_from_loc;
   p_mcd_to_loc il_to_loc;
   p_mcd_comp_id il_comp_id;
   p_mcd_from_loc_type il_from_loc_type;
   p_mcd_to_loc_type il_to_loc_type;
   p_mcd_comp_rate iil_comp_rate;
   p_mcd_per_count iil_per_count;
   p_mcd_per_count_uom iil_per_count_uom;
   p_mcd_up_chrg_group iil_up_chrg_group;
   p_mcd_comp_currency iil_comp_currency;
   p_mcd_display_order iil_display_order;
   p_mcd_maintenance_type mcd_maintenance_type;
   p_mcd_item mcd_item;

   cursor C_LOCK_ICH_ICD is
   select 1
     from item_chrg_detail icd,
          item_chrg_head ich,
          v_item_master viem,
          skulist_detail skd
    where ich.item          = icd.item
      and ich.from_loc      = icd.from_loc
      and ich.to_loc        = icd.to_loc
      and skd.item          = viem.item
      and skd.item          = ich.item
      and skd.skulist       = I_item_list
      for update nowait;

   cursor C_LOCK_MCH_MCD is
   select 1
     from mc_chrg_detail mcd,
          mc_chrg_head mch
    where mch.item_list     = I_item_list
      and mch.item_list     = mcd.item_list
      and mch.from_loc_type = mcd.from_loc_type
      and mch.from_loc      = mcd.from_loc
      and mch.to_loc_type   = mcd.to_loc_type
      and mch.to_loc        = mcd.to_loc
      for update nowait;

   cursor C_MCD is
   select mcd.item_list,
          mcd.from_loc,
          mcd.to_loc,
          mcd.comp_id,
          mcd.from_loc_type,
          mcd.to_loc_type,
          mcd.comp_rate,
          mcd.per_count,
          mcd.per_count_uom,
          mcd.up_chrg_group,
          mcd.comp_currency,
          mcd.display_order,
          mcd.maintenance_type,
          skd.item
     from v_item_master  viem,
          skulist_detail skd,
          mc_chrg_detail mcd
    where ((not exists (select 'x'
                        from item_chrg_detail id
                       where skd.item      = id.item
                         and mcd.from_loc  = id.from_loc
                         and mcd.to_loc    = id.to_loc
                         and mcd.comp_id   = id.comp_id) and mcd.maintenance_type = ADD_COMPONENT)
           or mcd.maintenance_type = DELETE_COMPONENT)
      and mcd.item_list  = I_item_list
      and mcd.item_list  = skd.skulist
      and skd.item       = viem.item
    order by skd.item,
             mcd.from_loc,
             mcd.to_loc,
             mcd.from_loc_type,
             mcd.to_loc_type;

BEGIN
   if I_item_list is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_list',
                                            L_program,
                                            NULL);
      return false;
   end if;

   open C_MCD;
   fetch C_MCD BULK COLLECT into p_mcd_item_list,
                                 p_mcd_from_loc,
                                 p_mcd_to_loc,
                                 p_mcd_comp_id,
                                 p_mcd_from_loc_type,
                                 p_mcd_to_loc_type,
                                 p_mcd_comp_rate,
                                 p_mcd_per_count,
                                 p_mcd_per_count_uom,
                                 p_mcd_up_chrg_group,
                                 p_mcd_comp_currency,
                                 p_mcd_display_order,
                                 p_mcd_maintenance_type,
                                 p_mcd_item;
   close C_MCD;

   FOR i IN 1 .. p_mcd_item.COUNT LOOP
      L_total_count := L_total_count + 1;
      L_mc_item := p_mcd_item(i); 
      if p_mcd_maintenance_type(i) = ADD_COMPONENT then
         if (L_mc_item != L_mc_previous_item) then 
            if Item_Attrib_Sql.GET_ITEM_TYPE(O_error_message,
                                             L_item_type,
                                             p_mcd_item(i)) = FALSE then
               return FALSE;
            end if;
         end if;
         if L_item_type NOT IN ('I', 'L', 'A', 'T') then
            if (L_previous_item          is NULL
            or  L_previous_item          != p_mcd_item(i)
            or  L_previous_from_loc      != p_mcd_from_loc(i)
            or  L_previous_to_loc        != p_mcd_to_loc(i)
            or  L_previous_from_loc_type != p_mcd_from_loc_type(i)
            or  L_previous_to_loc_type   != p_mcd_to_loc_type(i)) then

               if ITEM_CHARGE_SQL.CHARGES_EXIST(O_error_message,
                                                L_exists,
                                                p_mcd_item(i),
                                                p_mcd_from_loc(i),
                                                p_mcd_to_loc(i)) = FALSE then
                  return FALSE;
               end if;

               if NOT L_exists then
                  l_insheadcount := l_insheadcount + 1;
                  p_iilhead_item(l_insheadcount) := p_mcd_item(i);
                  p_iilhead_from_loc(l_insheadcount) := p_mcd_from_loc(i);
                  p_iilhead_to_loc(l_insheadcount) := p_mcd_to_loc(i);
                  p_iilhead_from_loc_type(l_insheadcount) := p_mcd_from_loc_type(i);
                  p_iilhead_to_loc_type(l_insheadcount) :=  p_mcd_to_loc_type(i);
               end if;
            end if;

            l_insdetcount := l_insdetcount + 1;
            p_iil_item(l_insdetcount)          := p_mcd_item(i);
            p_iil_from_loc(l_insdetcount)      := p_mcd_from_loc(i);
            p_iil_to_loc(l_insdetcount)        := p_mcd_to_loc(i);
            p_iil_from_loc_type(l_insdetcount) := p_mcd_from_loc_type(i);
            p_iil_to_loc_type(l_insdetcount)   := p_mcd_to_loc_type(i);
            p_iil_comp_id(l_insdetcount)       := p_mcd_comp_id(i);
            p_iil_comp_rate(l_insdetcount)     := p_mcd_comp_rate(i);
            p_iil_per_count(l_insdetcount)     := p_mcd_per_count(i);
            p_iil_per_count_uom(l_insdetcount) := p_mcd_per_count_uom(i);
            p_iil_up_chrg_group(l_insdetcount) := p_mcd_up_chrg_group(i);
            p_iil_comp_currency(l_insdetcount) := p_mcd_comp_currency(i);
            p_iil_display_order(l_insdetcount) := p_mcd_display_order(i);

            L_accept_count := L_accept_count + 1;

         else
            L_reject_count := L_reject_count + 1;
         end if;
      elsif p_mcd_maintenance_type(i) = DELETE_COMPONENT then

         l_detcount := l_detcount + 1;
         p_il_item(l_detcount)          := p_mcd_item(i);
         p_il_from_loc(l_detcount)      := p_mcd_from_loc(i);
         p_il_to_loc(l_detcount)        := p_mcd_to_loc(i);
         p_il_from_loc_type(l_detcount) := p_mcd_from_loc_type(i);
         p_il_to_loc_type(l_detcount)   := p_mcd_to_loc_type(i);
         p_il_comp_id(l_detcount)       := p_mcd_comp_id(i);

            if (L_previous_item is NULL
               or L_previous_item          != p_mcd_item(i)
               or L_previous_from_loc      != p_mcd_from_loc(i)
               or L_previous_to_loc        != p_mcd_to_loc(i)
               or L_previous_from_loc_type != p_mcd_from_loc_type(i)
               or L_previous_to_loc_type   != p_mcd_to_loc_type(i)) then
               l_headcount := l_headcount + 1;
               p_ilhead_item(l_headcount)          := p_mcd_item(i);
               p_ilhead_from_loc(l_headcount)      := p_mcd_from_loc(i);
               p_ilhead_to_loc(l_headcount)        := p_mcd_to_loc(i);
               p_ilhead_from_loc_type(l_headcount) := p_mcd_from_loc_type(i);
               p_ilhead_to_loc_type(l_headcount)   := p_mcd_to_loc_type(i);
            end if;

      end if;
      L_previous_item          := p_mcd_item(i);
      L_previous_from_loc      := p_mcd_from_loc(i);
      L_previous_to_loc        := p_mcd_to_loc(i);
      L_previous_from_loc_type := p_mcd_from_loc_type(i);
      L_previous_to_loc_type   := p_mcd_to_loc_type(i);
      L_mc_previous_item       := p_mcd_item(i);
   END LOOP;

   if l_insheadcount > 0 then
      FORALL i in p_iilhead_item.FIRST .. p_iilhead_item.LAST
         insert into item_chrg_head (item,
                                     from_loc,
                                     to_loc,
                                     from_loc_type,
                                     to_loc_type)
                             values (p_iilhead_item(i),
                                     p_iilhead_from_loc(i),
                                     p_iilhead_to_loc(i),
                                     p_iilhead_from_loc_type(i),
                                     p_iilhead_to_loc_type(i));

   end if;

   if l_insdetcount > 0 then
      FORALL i in p_iil_item.FIRST .. p_iil_item.LAST
         insert into item_chrg_detail (item,
                                       from_loc,
                                       to_loc,
                                       comp_id,
                                       from_loc_type,
                                       to_loc_type,
                                       comp_rate,
                                       per_count,
                                       per_count_uom,
                                       up_chrg_group,
                                       comp_currency,
                                       display_order)
                               values (p_iil_item(i),
                                       p_iil_from_loc(i),
                                       p_iil_to_loc(i),
                                       p_iil_comp_id(i),
                                       p_iil_from_loc_type(i),
                                       p_iil_to_loc_type(i),
                                       p_iil_comp_rate(i),
                                       p_iil_per_count(i),
                                       p_iil_per_count_uom(i),
                                       p_iil_up_chrg_group(i),
                                       p_iil_comp_currency(i),
                                       p_iil_display_order(i));
   end if;

   if l_detcount > 0 then

      open  C_LOCK_ICH_ICD;
      close C_LOCK_ICH_ICD;

      open  C_LOCK_MCH_MCD;
      close C_LOCK_MCH_MCD;

      FORALL i in p_il_item.FIRST .. p_il_item.LAST
         delete
            from item_chrg_detail icd
           where icd.item          = p_il_item(i)
             and icd.from_loc      = p_il_from_loc(i)
             and icd.to_loc        = p_il_to_loc(i)
             and icd.comp_id       = p_il_comp_id(i)
             and icd.from_loc_type = p_il_from_loc_type(i)
             and icd.to_loc_type   = p_il_to_loc_type(i);

        if l_headcount > 0 then
           FORALL i in p_ilhead_item.FIRST .. p_ilhead_item.LAST
              delete
                 from item_chrg_head ich
                where not exists (select 1
                                    from item_chrg_detail  icd,
                                         v_item_master     viem,
                                         skulist_detail    skd
                                   where skd.skulist       = I_item_list
                                     and skd.item          = viem.item
                                     and skd.item          = icd.item
                                     and icd.item          = ich.item
                                     and icd.from_loc_type = ich.from_loc_type
                                     and icd.from_loc      = ich.from_loc
                                     and icd.to_loc_type   = ich.to_loc_type
                                     and icd.to_loc        = ich.to_loc
                                     and rownum = 1)
                  and ich.item          = p_ilhead_item(i)
                  and ich.from_loc      = p_ilhead_from_loc(i)
                  and ich.to_loc        = p_ilhead_to_loc(i)
                  and ich.from_loc_type = p_ilhead_from_loc_type(i)
                  and ich.to_loc_type   = p_ilhead_to_loc_type(i);
      end if;
   end if;

   delete 
     from mc_chrg_detail;

   delete 
     from mc_chrg_head;

   if L_total_count = L_reject_count then
      O_count := 'E';
   elsif L_accept_count > 0 AND L_total_count > L_accept_count then
      O_count := 'P';
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_CHRG_HEAD/ITEM_CHRG_DETAIL/MC_CHRG_HEAD/MC_CHRG_DETAIL',
                                            'I_item_list='||I_item_list,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXPLODE_ITEM_LIST_AND_SAVE;
-------------------------------------------------------------------------------
END MCITEM_CHRG_SQL;
/

