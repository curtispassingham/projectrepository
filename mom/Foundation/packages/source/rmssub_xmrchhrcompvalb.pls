
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRCOMP_VALIDATE AS

-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_FOR_COMPANY
-- Purpose      : This function will check if a company already exists.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FOR_COMPANY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists            OUT      BOOLEAN,
                           I_message_type      IN       VARCHAR2,
                           I_company           IN       COMPHEAD.COMPANY%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQUIRED_FIELDS
-- Purpose      : This function will check all required fields for the create and modify messages
--                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message           IN       "RIB_XMrchHrCompDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: POPULATE_RECORD
-- Purpose      : This function will populate all the fields in the output record with
--                the values from the create and modify RIB messages.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                         O_company_rec     OUT    NOCOPY COMPHEAD%ROWTYPE,
                         I_message         IN            "RIB_XMrchHrCompDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_company_rec   OUT      NOCOPY COMPHEAD%ROWTYPE,
                       I_message       IN              "RIB_XMrchHrCompDesc_REC",
                       I_message_type  IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XMRCHHRCOMPDESC_VALIDATE.CHECK_MESSAGE';
   L_exists       BOOLEAN       := FALSE;

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_FOR_COMPANY(O_error_message,
                            L_exists,
                            I_message_type,
                            I_message.company) then
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XMRCHHRCOMP.LP_cre_type and L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('COMPANY_EXISTS',I_message.company,NULL,NULL);
      return FALSE;
   elsif I_message_type = RMSSUB_XMRCHHRCOMP.LP_mod_type and not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('COMPANY_NOT_EXISTS',I_message.company,NULL,NULL);
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_company_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------

-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message           IN       "RIB_XMrchHrCompDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCOMP_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.company is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Company ID', NULL, NULL);
      return FALSE;
   end if;

   if I_message.company_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Company Name', NULL, NULL);
      return FALSE;
   end if;

   if I_message.add_1 is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Address', NULL, NULL);
      return FALSE;
   end if;

   if I_message.city is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'City', NULL, NULL);
      return FALSE;
   end if;

   if I_message.country_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Country', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FOR_COMPANY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists            OUT      BOOLEAN,
                           I_message_type      IN       VARCHAR2,
                           I_company           IN       COMPHEAD.COMPANY%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCOMP_VALIDATE.CHECK_FOR_COMPANY';
   L_exists       VARCHAR2(1)  := NULL;

   cursor C_COMPANY_EXISTS is
      select 'x'
        from comphead
       where company = DECODE(I_message_type, RMSSUB_XMRCHHRCOMP.LP_cre_type, company, I_company);

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_COMPANY_EXISTS','comphead','company'|| I_company);
   open C_COMPANY_EXISTS;

   SQL_LIB.SET_MARK('FETCH','C_COMPANY_EXISTS','comphead','company'|| I_company);
   fetch C_COMPANY_EXISTS into L_exists;

   SQL_LIB.SET_MARK('CLOSE','C_COMPANY_EXISTS','comphead','company'|| I_company);
   close C_COMPANY_EXISTS;

   if L_exists is not NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FOR_COMPANY;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_company_rec     OUT      NOCOPY COMPHEAD%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrCompDesc_REC")
   RETURN BOOLEAN IS
   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCOMP_VALIDATE.POPULATE_RECORD';

BEGIN

   O_company_rec.company    := I_message.company;
   O_company_rec.co_name    := I_message.company_name;
   O_company_rec.co_add1    := I_message.add_1;
   O_company_rec.co_add2    := I_message.add_2;
   O_company_rec.co_add3    := I_message.add_3;
   O_company_rec.co_state   := I_message.state;
   O_company_rec.co_city    := I_message.city;
   O_company_rec.co_country := I_message.country_code;
   O_company_rec.co_post    := I_message.postal_code;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRCOMP_VALIDATE;
/
