
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XORGHR AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHANGE_CASE
   -- Purpose      : This private function converts specific char fields from the rib message to
   --                uppercase and the message_type to lowercase.
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XOrgHrDesc_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHANGE_CASE
   -- Purpose      : This private function converts specific char fields from the rib message to
   --                uppercase and the message_type to lowercase.
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XOrgHrRef_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code          IN OUT  VARCHAR2,
                        IO_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause                IN      VARCHAR2,
                        I_program              IN      VARCHAR2);
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code   IN OUT   VARCHAR2,
                  O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN       RIB_OBJECT,
                  I_message_type  IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_ref_message           "RIB_XOrgHrRef_REC";
   L_message               "RIB_XOrgHrDesc_REC";
   L_orghier_rec           ORGANIZATION_SQL.ORG_HIER_REC;
   L_message_type          VARCHAR2(15) := LOWER(I_message_type);

   L_program               VARCHAR2(50) := 'RMSSUB_XORGHR.CONSUME';

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   if LOWER(I_message_type) in  (LP_cre_type,
                                 LP_mod_type,
                                 LP_loctrt_cre_type) then

      L_message := treat(I_MESSAGE AS "RIB_XOrgHrDesc_REC");

      if L_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', 'No message', NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      if CHANGE_CASE(O_error_message,
                     L_message,
                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Validate Message Contents
      if RMSSUB_XORGHR_VALIDATE.CHECK_MESSAGE(O_error_message,
                                              L_orghier_rec,
                                              L_message,
                                              L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---

      -- INSERT/UPDATE table
      if RMSSUB_XORGHR_SQL.PERSIST(O_error_message,
                                   L_message.hier_level,
                                   L_message_type,
                                   L_orghier_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   elsif LOWER(I_message_type) in (LP_del_type, LP_loctrt_del_type)  then

      L_ref_message := treat(I_MESSAGE AS "RIB_XOrgHrRef_REC");
      ---
      if L_ref_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', 'No ref message for delete', NULL, NULL);
         raise PROGRAM_ERROR;
      end if;

      if CHANGE_CASE(O_error_message,
                     L_ref_message,
                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- Validate Message Contents
      if RMSSUB_XORGHR_VALIDATE.CHECK_MESSAGE(O_error_message,
                                              L_orghier_rec,
                                              L_ref_message,
                                              L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      -- DELETE from table
      if RMSSUB_XORGHR_SQL.PERSIST(O_error_message,
                                   L_ref_message.hier_level,
                                   L_message_type,
                                   L_orghier_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE', I_message_type, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XOrgHrDesc_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'RMSSUB_XORGHIER.CHANGE_CASE';

BEGIN

   IO_message.hier_level     := UPPER(IO_message.hier_level);
   IO_message.currency_code  := UPPER(IO_message.currency_code);
   IO_message_type           := LOWER(IO_message_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHANGE_CASE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XOrgHrRef_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XORGHIER.CHANGE_CASE';

BEGIN

   IO_message.hier_level    := UPPER(IO_message.hier_level);
   IO_message_type          := LOWER(IO_message_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHANGE_CASE;
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code               IN OUT  VARCHAR2,
                        IO_error_message            IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause                     IN      VARCHAR2,
                        I_program                   IN      VARCHAR2) IS

   L_program  VARCHAR2(50)   :=  'RMSSUB_XORGHR.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);
EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------
END RMSSUB_XORGHR;
/
