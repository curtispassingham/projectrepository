
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
create or replace PACKAGE BODY FILTER_GROUP_HIER_SQL as

--------------------------------------------------------------------------------
-- CHECK_DUP_GROUP_MERCH( ) 
--------------------------------------------------------------------------------
FUNCTION CHECK_DUP_GROUP_MERCH(O_error_message            IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_dup_exists               IN OUT  BOOLEAN,
                               I_sec_group_id             IN      FILTER_GROUP_MERCH.SEC_GROUP_ID%TYPE,
                               I_filter_merch_level       IN      FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                               I_filter_merch_id          IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                               I_filter_merch_id_class    IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                               I_filter_merch_id_subclass IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_GROUP_HIER_SQL.CHECK_DUP_GROUP_MERCH';

   L_dummy                VARCHAR2(1);
   L_filter_merch_level   FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE;
   L_code_desc            CODE_DETAIL.CODE_DESC%TYPE;
   L_exists               BOOLEAN;
   L_inv_parm             VARCHAR2(30)   := NULL;

   cursor C_CHECK_MERCH is
      select 'X'
        from filter_group_merch
       where sec_group_id = I_sec_group_id
         and filter_merch_id = I_filter_merch_id
         and filter_merch_level = I_filter_merch_level
         and nvl(filter_merch_id_class, -1) = nvl(I_filter_merch_id_class, -1)
         and nvl(filter_merch_id_subclass, -1) = nvl(I_filter_merch_id_subclass, -1);

   cursor C_GET_CODE_DESC is
      select code_desc
        from code_detail
       where code_type = 'FLTM'
         and code = I_filter_merch_level;

   cursor C_CHECK_DIV_HIER is
      select filter_merch_level
        from filter_group_merch fm
       where sec_group_id = I_sec_group_id
         and ((filter_merch_level = 'G'
               and exists (select 'X'
                             from groups gp
                            where gp.group_no = fm.filter_merch_id
                              and gp.division = I_filter_merch_id))							  
           or (filter_merch_level in ('P','C','S')
               and exists (select 'X'
                             from deps dp
                            where dp.dept = fm.filter_merch_id
                              and exists (select 'X'
                                            from groups gps
                                           where gps.group_no = dp.group_no
                                             and gps.division = I_filter_merch_id))));

   cursor C_CHECK_GRP_HIER is
      select filter_merch_level
        from filter_group_merch fm
       where sec_group_id = I_sec_group_id
         and ((filter_merch_level = 'D'
               and exists (select 'X'
                             from groups gp
                            where gp.division = fm.filter_merch_id
                              and gp.group_no = I_filter_merch_id))
           or (filter_merch_level in ('P','C','S')
               and exists (select 'X'
                             from deps dp
                            where dp.dept = fm.filter_merch_id
                              and dp.group_no = I_filter_merch_id)));

   cursor C_CHECK_DEPT_HIER is
      select filter_merch_level
        from filter_group_merch fm
       where sec_group_id = I_sec_group_id
         and ((filter_merch_level = 'D'
               and exists (select 'X'
                             from groups gp
                            where gp.division = fm.filter_merch_id
                              and exists (select 'X'
                                            from deps dp
                                           where dp.group_no = gp.group_no
                                             and dp.dept = I_filter_merch_id)))
           or (filter_merch_level = 'G'
               and exists (select 'X'
                             from deps dps
                            where dps.group_no = fm.filter_merch_id
                              and dps.dept = I_filter_merch_id))
           or (filter_merch_level in ('C','S')
               and fm.filter_merch_id = I_filter_merch_id));

   cursor C_CHECK_CLASS_HIER is
      select filter_merch_level
        from filter_group_merch fm
       where sec_group_id = I_sec_group_id
         and ((filter_merch_level = 'D'
               and exists (select 'X'
                             from groups gp,
                                  deps dp
                            where gp.division = fm.filter_merch_id
                              and gp.group_no = dp.group_no
                              and dp.dept = I_filter_merch_id))
           or (filter_merch_level = 'G'
               and exists (select 'X'
                             from deps dps
                            where dps.group_no = fm.filter_merch_id
                              and dps.dept = I_filter_merch_id))
           or (filter_merch_level = 'P'
               and fm.filter_merch_id = I_filter_merch_id)                                                                               
           or (filter_merch_level = 'S'
               and fm.filter_merch_id = I_filter_merch_id
               and fm.filter_merch_id_class = I_filter_merch_id_class));

   cursor C_CHECK_SUBCLASS_HIER is
      select filter_merch_level
        from filter_group_merch fm
       where sec_group_id = I_sec_group_id
         and ((filter_merch_level = 'D'
               and exists (select 'X'
                             from groups gp,
                                  deps dp
                            where gp.division = fm.filter_merch_id
                              and gp.group_no = dp.group_no
                              and dp.dept = I_filter_merch_id))  
          or (filter_merch_level = 'G'                                                           
              and exists (select 'X'
                            from deps dps
                           where dps.group_no = fm.filter_merch_id
                             and dps.dept = I_filter_merch_id))                                                           
          or (filter_merch_level = 'P'
              and fm.filter_merch_id = I_filter_merch_id) 
          or (filter_merch_level = 'C'
              and fm.filter_merch_id = I_filter_merch_id
              and fm.filter_merch_id_class = I_filter_merch_id_class));
                              

BEGIN
   if I_sec_group_id is NULL then 
      L_inv_parm := 'I_sec_group_id'; 
   elsif I_filter_merch_level is NULL then 
      L_inv_parm := 'I_filter_merch_level'; 
   elsif I_filter_merch_id is NULL then 
      L_inv_parm := 'I_filter_merch_id'; 
   end if; 

   if L_inv_parm is not NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_parm, 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 

   open C_GET_CODE_DESC;
   fetch C_GET_CODE_DESC into L_code_desc;
   close C_GET_CODE_DESC;
   if L_code_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MERCH_LEVEL', NULL, NULL, NULL);
      return FALSE;
   else
      open C_CHECK_MERCH;
      fetch C_CHECK_MERCH into L_dummy;
      close C_CHECK_MERCH;

      if L_dummy is NULL then
         O_dup_exists := FALSE;

         if I_filter_merch_level = 'D' then
            open C_CHECK_DIV_HIER;
            fetch C_CHECK_DIV_HIER into L_filter_merch_level;
            close C_CHECK_DIV_HIER;            
            if L_filter_merch_level is NULL then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;
         elsif I_filter_merch_level = 'G' then
            open C_CHECK_GRP_HIER;
            fetch C_CHECK_GRP_HIER into L_filter_merch_level;
            close C_CHECK_GRP_HIER;            
            if L_filter_merch_level is NULL then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;
         elsif I_filter_merch_level = 'P' then
            open C_CHECK_DEPT_HIER;
            fetch C_CHECK_DEPT_HIER into L_filter_merch_level;
            close C_CHECK_DEPT_HIER;            
            if L_filter_merch_level is NULL then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;
         elsif I_filter_merch_level = 'C' then
            open C_CHECK_CLASS_HIER;
            fetch C_CHECK_CLASS_HIER into L_filter_merch_level;
            close C_CHECK_CLASS_HIER;            
            if L_filter_merch_level is NULL then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;   
         elsif I_filter_merch_level = 'S' then
            open C_CHECK_SUBCLASS_HIER;
            fetch C_CHECK_SUBCLASS_HIER into L_filter_merch_level;
            close C_CHECK_SUBCLASS_HIER; 
            if L_filter_merch_level is NULL then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;              
         end if;

         if L_exists = TRUE then
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FLTM',
                                          L_filter_merch_level,
                                          L_code_desc) = FALSE then
               return FALSE;
             end if;
             O_error_message := SQL_LIB.CREATE_MSG('GRP_HIER_EXISTS', I_sec_group_id, L_code_desc, NULL);
             O_dup_exists := TRUE;
         end if;
      else
         if I_filter_merch_level = 'C' then
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FLTM',
                                          'P',
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;
            O_error_message := SQL_LIB.CREATE_MSG('DUP_HIER', I_sec_group_id, L_code_desc, I_filter_merch_id_class);
         elsif I_filter_merch_level = 'S' then
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FLTM',
                                          'C',
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;
            O_error_message := SQL_LIB.CREATE_MSG('DUP_HIER', I_sec_group_id, L_code_desc, I_filter_merch_id_subclass);
         else
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FLTM',
                                          I_filter_merch_level,
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;
            O_error_message := SQL_LIB.CREATE_MSG('DUP_HIER', I_sec_group_id, L_code_desc, I_filter_merch_id);
         end if;
         --
         O_dup_exists := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DUP_GROUP_MERCH;

--------------------------------------------------------------------------------
-- CHECK_DUP_GROUP_ORG( ) 
--------------------------------------------------------------------------------
FUNCTION CHECK_DUP_GROUP_ORG(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_dup_exists       IN OUT  BOOLEAN,
                             I_sec_group_id     IN      FILTER_GROUP_ORG.SEC_GROUP_ID%TYPE,
                             I_filter_org_level IN      FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                             I_filter_org_id    IN      FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_GROUP_HIER_SQL.CHECK_DUP_GROUP_ORG';

   L_dummy              VARCHAR2(1);
   L_filter_org_level   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE;
   L_code_desc          CODE_DETAIL.CODE_DESC%TYPE;
   L_exists             BOOLEAN;
   L_inv_parm           VARCHAR2(30)   := NULL;

   cursor C_CHECK_ORG is
      select 'X'
        from filter_group_org
       where sec_group_id = I_sec_group_id
         and filter_org_id = I_filter_org_id
         and filter_org_level = I_filter_org_level;

   cursor C_GET_CODE_DESC is
      select code_desc
        from code_detail
       where code_type = 'FLOW'
         and code = I_filter_org_level;

   cursor C_CHECK_CHAIN_HIER is
      select filter_org_level
        from filter_group_org fo
       where sec_group_id = I_sec_group_id
         and ((filter_org_level = 'A'
               and exists (select 'X'
                             from area a
                            where a.area = fo.filter_org_id
                              and a.chain = I_filter_org_id))
           or (filter_org_level = 'R'
               and exists (select 'X'
                             from region r
                            where r.region = fo.filter_org_id
                              and exists (select 'X'
                                            from area ar
                                           where ar.area = r.area
                                             and ar.chain = I_filter_org_id)))
           or (filter_org_level = 'D'
               and exists (select 'X'
                             from district d
                            where d.district = fo.filter_org_id
                              and exists (select 'X'
                                            from region r
                                           where r.region = d.region
                                             and exists (select 'X'
                                                           from area ar
                                                          where ar.area = r.area
                                                            and ar.chain = I_filter_org_id)))));

   cursor C_CHECK_AREA_HIER is
      select filter_org_level
        from filter_group_org fo
       where sec_group_id = I_sec_group_id
         and ((filter_org_level = 'C'
               and exists (select 'X'
                             from area a
                            where a.chain = fo.filter_org_id
                              and a.area = I_filter_org_id))
           or (filter_org_level = 'R'
               and exists (select 'X'
                             from region r
                            where r.region = fo.filter_org_id
                              and r.area = I_filter_org_id))
           or (filter_org_level = 'D'
               and exists (select 'X'
                             from district d
                            where d.district = fo.filter_org_id
                              and exists (select 'X'
                                            from region r
                                           where r.region = d.region
                                             and r.area = I_filter_org_id))));

   cursor C_CHECK_REGION_HIER is
      select filter_org_level
        from filter_group_org fo
       where sec_group_id = I_sec_group_id
         and ((filter_org_level = 'C'
               and exists (select 'X'
                             from area a
                            where a.chain = fo.filter_org_id
                              and exists (select 'X'
                                            from region r
                                           where r.area = a.area 
                                             and r.region = I_filter_org_id)))
           or (filter_org_level = 'A'
               and exists (select 'X'
                             from region r
                            where r.area = fo.filter_org_id
                              and r.region = I_filter_org_id))
           or (filter_org_level = 'D'
               and exists (select 'X'
                             from district d
                            where d.district = fo.filter_org_id
                              and d.region = I_filter_org_id)));

   cursor C_CHECK_DISTRICT_HIER is
      select filter_org_level
        from filter_group_org fo
       where sec_group_id = I_sec_group_id
         and ((filter_org_level = 'C'
               and exists (select 'X'
                             from area a
                            where a.chain = fo.filter_org_id
                              and exists (select 'X'
                                            from region r
                                           where r.area = a.area 
                                             and exists (select 'X'
                                                           from district d
                                                          where d.region = r.region
                                                            and d.district = I_filter_org_id))))
           or (filter_org_level = 'A'
               and exists (select 'X'
                             from region r
                            where r.area = fo.filter_org_id
                              and exists (select 'X'
                                            from district d
                                           where d.region = r.region
                                             and d.district = I_filter_org_id)))
           or (filter_org_level = 'R'
               and exists (select 'X'
                             from district d
                            where d.region = fo.filter_org_id
                              and d.district = I_filter_org_id)));


BEGIN
   if I_sec_group_id is NULL then 
      L_inv_parm := 'I_sec_group_id'; 
   elsif I_filter_org_level is NULL then 
      L_inv_parm := 'I_filter_org_level'; 
   elsif I_filter_org_id is NULL then 
      L_inv_parm := 'I_filter_org_id'; 
   end if; 

   if L_inv_parm is not NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_parm, 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 

   open C_GET_CODE_DESC;
   fetch C_GET_CODE_DESC into L_code_desc;
   if C_GET_CODE_DESC%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_LEVEL', NULL, NULL, NULL);
      return FALSE;
   else
      close C_GET_CODE_DESC;
      open C_CHECK_ORG;
      fetch C_CHECK_ORG into L_dummy;
      if C_CHECK_ORG%NOTFOUND then
         O_dup_exists := FALSE;

         if I_filter_org_level = 'C' then
            open C_CHECK_CHAIN_HIER;
            fetch C_CHECK_CHAIN_HIER into L_filter_org_level;
            if C_CHECK_CHAIN_HIER%NOTFOUND then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;
            close C_CHECK_CHAIN_HIER;
         elsif I_filter_org_level = 'A' then
            open C_CHECK_AREA_HIER;
            fetch C_CHECK_AREA_HIER into L_filter_org_level;
            if C_CHECK_AREA_HIER%NOTFOUND then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;
            close C_CHECK_AREA_HIER;
         elsif I_filter_org_level = 'R' then
            open C_CHECK_REGION_HIER;
            fetch C_CHECK_REGION_HIER into L_filter_org_level;
            if C_CHECK_REGION_HIER%NOTFOUND then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;
            close C_CHECK_REGION_HIER;
         elsif I_filter_org_level = 'D' then
            open C_CHECK_DISTRICT_HIER;
            fetch C_CHECK_DISTRICT_HIER into L_filter_org_level;
            if C_CHECK_DISTRICT_HIER%NOTFOUND then
               L_exists := FALSE;
            else
               L_exists := TRUE;
            end if;
            close C_CHECK_DISTRICT_HIER;
        end if;

         if L_exists = TRUE then
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'FLOW',
                                          L_filter_org_level,
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;
            O_error_message := SQL_LIB.CREATE_MSG('GRP_HIER_EXISTS', I_sec_group_id, L_code_desc, NULL);
            O_dup_exists := TRUE;
         end if;
      else
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'FLOW',
                                       I_filter_org_level,
                                       L_code_desc) = FALSE then
            return FALSE;
         end if;
         O_dup_exists := TRUE;
         O_error_message := SQL_LIB.CREATE_MSG('DUP_HIER', I_sec_group_id, L_code_desc, I_filter_org_id);
      end if;
   end if;

   close C_CHECK_ORG;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END CHECK_DUP_GROUP_ORG;

--------------------------------------------------------------------------------
-- CHECK_GROUP_MERCH( ) 
--------------------------------------------------------------------------------
FUNCTION CHECK_GROUP_MERCH  (O_error_message IN OUT VARCHAR2,
                             O_exist         IN OUT BOOLEAN,
                             I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_GROUP_HIER_SQL.CHECK_GROUP_MERCH';
   L_exist               VARCHAR2(1) := 'N';

   cursor C_MERCH_EXIST is
     select 'Y'
     from   filter_group_merch
     where  sec_group_id = I_group_id
     group by 'Y';
BEGIN
   if I_group_id is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_group_id', 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 
   ---
   o_exist := FALSE;

   open C_MERCH_EXIST;
   fetch C_MERCH_EXIST into L_exist;
   close C_MERCH_EXIST;
   ---
   if L_exist = 'Y' then
      O_exist := TRUE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END CHECK_GROUP_MERCH;

--------------------------------------------------------------------------------
-- CHECK_GROUP_ORG( ) 
--------------------------------------------------------------------------------
FUNCTION CHECK_GROUP_ORG  (O_error_message IN OUT VARCHAR2,
                           O_exist         IN OUT BOOLEAN,
                           I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN IS

   L_program   VARCHAR2(64)  := 'FILTER_GROUP_HIER_SQL.CHECK_GROUP_ORG';
   L_exist     VARCHAR2(1)   := 'N';

   cursor C_ORG_EXIST is
     select 'Y'
     from   filter_group_org
     where  sec_group_id = I_group_id
     group by 'Y';
BEGIN
   if I_group_id is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_group_id', 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 
   ---
   o_exist := FALSE;

   open C_ORG_EXIST;
   fetch C_ORG_EXIST into L_exist;
   close C_ORG_EXIST;
   ---
   if L_exist = 'Y' then
      O_exist := TRUE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END CHECK_GROUP_ORG;

--------------------------------------------------------------------------------
-- DELETE_GROUP_MERCH( ) 
--------------------------------------------------------------------------------
FUNCTION DELETE_GROUP_MERCH(O_error_message            IN OUT VARCHAR2,
                            I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                            I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                            I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                            I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)
return BOOLEAN IS

   L_program             VARCHAR2(64)   := 'FILTER_GROUP_HIER_SQL.DELETE_GROUP_MERCH';
   L_inv_parm            VARCHAR2(30)   := NULL;  
      
BEGIN
   if I_filter_merch_level is NULL then 
      L_inv_parm := 'I_filter_merch_level'; 
   elsif I_filter_merch_id is NULL then 
      L_inv_parm := 'I_filter_merch_id'; 
   elsif I_filter_merch_level = 'C' then
      if I_filter_merch_id_class is NULL then
         L_inv_parm := 'I_filter_merch_id_class';
      end if;
   elsif I_filter_merch_level = 'S' then
      if I_filter_merch_id_class is NULL then
         L_inv_parm := 'I_filter_merch_id_class';
      elsif I_filter_merch_id_subclass is NULL then
         L_inv_parm := 'I_filter_merch_id_subclass';
      end if;
   end if;

   if L_inv_parm is not NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_parm, 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 
  
   delete from filter_group_merch
    where filter_merch_level = I_filter_merch_level
      and filter_merch_id = I_filter_merch_id
      and nvl(filter_merch_id_class,-1) = nvl(I_filter_merch_id_class,-1)		 
      and nvl(filter_merch_id_subclass,-1) = nvl(I_filter_merch_id_subclass,-1);
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				                    SQLERRM,
	  			                    L_program,
				                    to_char(SQLCODE));
      return FALSE;

END DELETE_GROUP_MERCH;

--------------------------------------------------------------------------------
-- DELETE_GROUP_ORG( ) 
--------------------------------------------------------------------------------
FUNCTION DELETE_GROUP_ORG(O_error_message    IN OUT VARCHAR2,
                          I_filter_org_level IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                          I_filter_org_id    IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
return BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_GROUP_HIER_SQL.DELETE_GROUP_ORG';
   L_inv_parm  VARCHAR2(30)   := NULL;

BEGIN
   if I_filter_org_level is NULL then 
      L_inv_parm := 'I_filter_org_level'; 
   elsif I_filter_org_id is NULL then 
      L_inv_parm := 'I_filter_org_id'; 
   end if; 

   if L_inv_parm is not NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_parm, 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 

   delete from filter_group_org
         where filter_org_level = I_filter_org_level
           and filter_org_id = I_filter_org_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				                    SQLERRM,
	  			                    L_program,
				                    to_char(SQLCODE));
      return FALSE;

END DELETE_GROUP_ORG;

--------------------------------------------------------------------------------
-- VALIDATE_GROUP_ORG( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_ORG(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                            O_exist               IN OUT BOOLEAN,
                            I_filter_org_level    IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                            I_filter_org_id       IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE) 
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_GROUP_ORG';
   L_code_desc  CODE_DETAIL.CODE_DESC%TYPE;
   L_inv_parm   VARCHAR2(30)   := NULL;

   cursor C_GET_ORG_CODE is
      select code_desc
        from code_detail
       where code_type = 'FLTO'
         and code = I_filter_org_level;

BEGIN
   if I_filter_org_level is NULL then 
      L_inv_parm := 'I_filter_org_level'; 
   elsif I_filter_org_id is NULL then 
      L_inv_parm := 'I_filter_org_id'; 
   end if; 

   if L_inv_parm is not NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_parm, 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 

   open C_GET_ORG_CODE;
   fetch C_GET_ORG_CODE into L_code_desc;
   close C_GET_ORG_CODE;
   if L_code_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_LEVEL', NULL, NULL, NULL);
      return FALSE;
   end if;

   if VALIDATE_DIFF_GROUP_HEAD(O_error_message,
                               O_exist,
                               NULL,
                               NULL,
                               I_filter_org_level,
                               I_filter_org_id,
                               NULL,
                               NULL) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_DIFF_GROUP', L_code_desc, I_filter_org_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_LOC_LIST_HEAD(O_error_message,
                             O_exist,
                             I_filter_org_level,
                             I_filter_org_id) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_LOC_LIST', L_code_desc, I_filter_org_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_LOC_TRAITS(O_error_message,
                          O_exist,
                          I_filter_org_level,
                          I_filter_org_id) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_LOC_TRAIT', L_code_desc, I_filter_org_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_SKULIST_HEAD(O_error_message,
                            O_exist,
                            I_filter_org_level,
                            I_filter_org_id) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_ITEM_LIST', L_code_desc, I_filter_org_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_SEASONS(O_error_message,
                       O_exist,
                       NULL,
                       NULL,
                       I_filter_org_level,
                       I_filter_org_id,
                       NULL,
                       NULL) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_SEASON', L_code_desc, I_filter_org_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_TICKET_TYPE_HEAD(O_error_message,
                                O_exist,
                                NULL,
                                NULL,
                                I_filter_org_level,
                                I_filter_org_id,
                                NULL,
                                NULL) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_TICKET', L_code_desc, I_filter_org_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_UDA(O_error_message,
                   O_exist,
                   NULL,
                   NULL,
                   I_filter_org_level,
                   I_filter_org_id,
                   NULL,
                   NULL) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_UDA', L_code_desc, I_filter_org_id, NULL);
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_GROUP_ORG;

--------------------------------------------------------------------------------
-- VALIDATE_GROUP_MERCH( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_MERCH(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                              O_exist                    IN OUT BOOLEAN,
                              I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                              I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                              I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                              I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE) 
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_GROUP_MERCH';
   L_code_desc  CODE_DETAIL.CODE_DESC%TYPE;
   L_inv_parm   VARCHAR2(30)   := NULL;
   L_merch_id   FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE;

   cursor C_GET_ORG_CODE is
      select code_desc
        from code_detail
       where code_type = 'FLTM'
         and code = I_filter_merch_level;

BEGIN
   if I_filter_merch_level is NULL then 
      L_inv_parm := 'I_filter_merch_level'; 
   elsif I_filter_merch_id is NULL then 
      L_inv_parm := 'I_filter_merch_id'; 
   end if; 

   if L_inv_parm is not NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_parm, 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 

   open C_GET_ORG_CODE;
   fetch C_GET_ORG_CODE into L_code_desc;
   close C_GET_ORG_CODE;
   if L_code_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MERCH_LEVEL', NULL, NULL, NULL);
      return FALSE;
   end if;
   
   if I_filter_merch_level in ('D','G','P') then
      L_merch_id := I_filter_merch_id;
   elsif I_filter_merch_level = 'C' then
      L_merch_id := I_filter_merch_id_class;
   elsif I_filter_merch_level = 'S' then
      L_merch_id := I_filter_merch_id_subclass;
   end if; 

   if VALIDATE_DIFF_GROUP_HEAD(O_error_message,
                               O_exist,
                               I_filter_merch_level,
                               I_filter_merch_id,
                               NULL,
                               NULL,
                               I_filter_merch_id_class,
                               I_filter_merch_id_subclass) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_DIFF_GROUP', L_code_desc, L_merch_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_SEASONS(O_error_message,
                       O_exist,
                       I_filter_merch_level,
                       I_filter_merch_id,
                       NULL,
                       NULL,
                       I_filter_merch_id_class,
                       I_filter_merch_id_subclass) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_SEASON', L_code_desc, L_merch_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_TICKET_TYPE_HEAD(O_error_message,
                                O_exist,
                                I_filter_merch_level,
                                I_filter_merch_id,
                                NULL,
                                NULL,
                                I_filter_merch_id_class,
                                I_filter_merch_id_subclass) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_TICKET', L_code_desc, L_merch_id, NULL);
      return TRUE;
   end if;

   if VALIDATE_UDA(O_error_message,
                   O_exist,
                   I_filter_merch_level,
                   I_filter_merch_id,
                   NULL,
                   NULL,
                   I_filter_merch_id_class,
                   I_filter_merch_id_subclass) = FALSE then
      return FALSE;
   end if;
   if O_exist then
      O_error_message := SQL_LIB.CREATE_MSG('HIER_EXISTS_UDA', L_code_desc, L_merch_id, NULL);
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_GROUP_MERCH;

--------------------------------------------------------------------------------
-- VALIDATE_GROUP_MERCH( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_MERCH(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist               IN OUT  BOOLEAN,
                              I_filter_merch_level  IN      FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                              I_filter_merch_id     IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                              I_user_id             IN      SEC_USER.DATABASE_USER_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_GROUP_MERCH';
   L_dummy      VARCHAR2(1)    := NULL;
   L_inv_parm   VARCHAR2(30)   := NULL;
   
   cursor C_CHECK_FILTER_GROUP is
      select 'x'
        from filter_group_merch fgm,
             sec_user_group     sug,
             sec_user           su
       where fgm.filter_merch_id    = I_filter_merch_id
         and fgm.filter_merch_level = I_filter_merch_level
         and fgm.sec_group_id       = sug.group_id
         and sug.user_seq           = su.user_seq
         and su.database_user_id    = I_user_id
       group by 'x';         
      
BEGIN
   O_exist := FALSE;
   O_error_message := NULL;

   if I_filter_merch_level is NULL then 
      L_inv_parm := 'I_filter_merch_level'; 
   elsif I_filter_merch_id is NULL then 
      L_inv_parm := 'I_filter_merch_id'; 
   elsif I_user_id is NULL then
      L_inv_parm := 'I_user_id';
   end if; 

   if L_inv_parm is not NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_parm, 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 
   
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_FILTER_GROUP', 'FILTER_GROUP_MERCH', NULL );
   open C_CHECK_FILTER_GROUP;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_FILTER_GROUP',
                    'FILTER_GROUP_MERCH',
                    'user_id: '||I_user_id||', filter_merch_level: '||I_filter_merch_level||', filter_merch_id: '||I_filter_merch_id);
   fetch C_CHECK_FILTER_GROUP into L_dummy;
                       
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FILTER_GROUP', 'FILTER_GROUP_MERCH', NULL );                 
   close C_CHECK_FILTER_GROUP;
   
   if L_dummy = 'x' then
      O_exist := TRUE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,L_program,to_char(SQLCODE));
      return FALSE;
                    
END VALIDATE_GROUP_MERCH;


--------------------------------------------------------------------------------
-- VALIDATE_DIFF_GROUP_HEAD( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF_GROUP_HEAD(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                                  O_exist                    IN OUT BOOLEAN,
                                  I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                                  I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                                  I_filter_org_level         IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                                  I_filter_org_id            IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE,
                                  I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                                  I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE) 
RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_DIFF_GROUP_HEAD';
   L_dummy          VARCHAR2(1);

   cursor C_CHECK_EXISTS is
      select 'X'
        from diff_group_head d, system_options s
       where d.filter_merch_id = nvl(I_filter_merch_id, -1)
         and s.diff_group_merch_level_code = nvl(I_filter_merch_level, '?')
         and nvl(d.filter_merch_id_class, -1) = nvl(I_filter_merch_id_class, -1)
         and nvl(d.filter_merch_id_subclass, -1) = nvl(I_filter_merch_id_subclass, -1)
       union
      select 'X'
        from diff_group_head , system_options s 
       where filter_org_id = nvl(I_filter_org_id, -1)
         and s.diff_group_org_level_code = nvl(I_filter_org_level, '?');

BEGIN
   O_exist := TRUE;
   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_dummy;
   close C_CHECK_EXISTS;
   if L_dummy is NULL then
      O_exist := FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DIFF_GROUP_HEAD;

--------------------------------------------------------------------------------
-- VALIDATE_LOC_LIST_HEAD( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOC_LIST_HEAD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                                O_exist             IN OUT BOOLEAN,
                                I_filter_org_level  IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                                I_filter_org_id     IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE) 
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_LOC_LIST_HEAD';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_EXISTS is
      select 'X'
        from loc_list_head , system_options s 
       where filter_org_id = nvl(I_filter_org_id, -1)
         and s.loc_list_org_level_code = nvl(I_filter_org_level, '?');

BEGIN
   O_exist := TRUE;
   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_dummy;
   if C_CHECK_EXISTS%NOTFOUND then
      O_exist := FALSE;
   end if;
   close C_CHECK_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_LOC_LIST_HEAD;

--------------------------------------------------------------------------------
-- VALIDATE_LOC_TRAITS( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOC_TRAITS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                             O_exist             IN OUT BOOLEAN,
                             I_filter_org_level  IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                             I_filter_org_id     IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE) 
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_LOC_TRAITS';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_EXISTS is
      select 'X'
        from loc_traits , system_options s 
       where filter_org_id = nvl(I_filter_org_id, -1)
         and s.loc_trait_org_level_code = nvl(I_filter_org_level, '?');

BEGIN
   O_exist := TRUE;
   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_dummy;
   if C_CHECK_EXISTS%NOTFOUND then
      O_exist := FALSE;
   end if;
   close C_CHECK_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_LOC_TRAITS;

--------------------------------------------------------------------------------
-- VALIDATE_SKULIST_HEAD( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SKULIST_HEAD(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                               O_exist               IN OUT BOOLEAN,
                               I_filter_org_level    IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                               I_filter_org_id       IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE) 
RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_SKULIST_HEAD';
   L_dummy          VARCHAR2(1);

   cursor C_CHECK_EXISTS is
      select 'X'
        from skulist_head , system_options s 
       where filter_org_id = nvl(I_filter_org_id, -1)
         and s.skulist_org_level_code = nvl(I_filter_org_level, '?');

BEGIN
   O_exist := TRUE;
   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_dummy;
   if C_CHECK_EXISTS%NOTFOUND then
      O_exist := FALSE;
   end if;
   close C_CHECK_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SKULIST_HEAD;

--------------------------------------------------------------------------------
-- VALIDATE_SEASONS( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SEASONS(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                          O_exist                    IN OUT BOOLEAN,
                          I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                          I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                          I_filter_org_level         IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                          I_filter_org_id            IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE,
                          I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                          I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE) 
RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_SEASONS';
   L_dummy          VARCHAR2(1);

   cursor C_CHECK_EXISTS is
      select 'X'
        from seasons e, system_options s
       where e.filter_merch_id = nvl(I_filter_merch_id, -1)
         and s.season_merch_level_code = nvl(I_filter_merch_level, '?')
         and nvl(e.filter_merch_id_class, -1) = nvl(I_filter_merch_id_class, -1)
         and nvl(e.filter_merch_id_subclass, -1) = nvl(I_filter_merch_id_subclass, -1)
       union
      select 'X'
        from seasons , system_options s 
       where filter_org_id = nvl(I_filter_org_id, -1)
         and s.season_org_level_code = nvl(I_filter_org_level, '?');

BEGIN
   O_exist := TRUE;
   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_dummy;
   close C_CHECK_EXISTS;
   if L_dummy is NULL then
      O_exist := FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SEASONS;

--------------------------------------------------------------------------------
-- VALIDATE_TICKET_TYPE_HEAD( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TICKET_TYPE_HEAD(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                                   O_exist                    IN OUT BOOLEAN,
                                   I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                                   I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                                   I_filter_org_level         IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                                   I_filter_org_id            IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE,
                                   I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                                   I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE) 
RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_TICKET_TYPE_HEAD';
   L_dummy          VARCHAR2(1);

   cursor C_CHECK_EXISTS is
      select 'X'
        from ticket_type_head t, system_options s
       where t.filter_merch_id = nvl(I_filter_merch_id, -1)
         and s.ticket_type_merch_level_code = nvl(I_filter_merch_level, '?')
         and nvl(t.filter_merch_id_class, -1) = nvl(I_filter_merch_id_class, -1)
         and nvl(t.filter_merch_id_subclass, -1) = nvl(I_filter_merch_id_subclass, -1)
       union
      select 'X'
        from ticket_type_head , system_options s 
       where filter_org_id = nvl(I_filter_org_id, -1)
         and s.ticket_type_org_level_code = nvl(I_filter_org_level, '?');

BEGIN
   O_exist := TRUE;
   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_dummy;
   close C_CHECK_EXISTS;
   if L_dummy is NULL then
      O_exist := FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_TICKET_TYPE_HEAD;

--------------------------------------------------------------------------------
-- VALIDATE_UDA( ) 
--------------------------------------------------------------------------------
FUNCTION VALIDATE_UDA(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                      O_exist                    IN OUT BOOLEAN,
                      I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                      I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                      I_filter_org_level         IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                      I_filter_org_id            IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE,
                      I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                      I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)                       
RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'VALIDATE_GROUP_HIER_SQL.VALIDATE_TICKET_TYPE_HEAD';
   L_dummy          VARCHAR2(1);

   cursor C_CHECK_EXISTS is
      select 'X'
        from uda u, system_options s
       where u.filter_merch_id = nvl(I_filter_merch_id, -1)
         and s.uda_merch_level_code = nvl(I_filter_merch_level, '?')
         and nvl(u.filter_merch_id_class, -1) = nvl(I_filter_merch_id_class, -1)
         and nvl(u.filter_merch_id_subclass, -1) = nvl(I_filter_merch_id_subclass, -1)
       union
      select 'X'
        from uda , system_options s 
       where filter_org_id = nvl(I_filter_org_id, -1)
         and s.uda_org_level_code = nvl(I_filter_org_level, '?');

BEGIN
   O_exist := TRUE;
   open C_CHECK_EXISTS;
   fetch C_CHECK_EXISTS into L_dummy;
   close C_CHECK_EXISTS;
   if L_dummy is NULL then
      O_exist := FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;

END VALIDATE_UDA;
--------------------------------------------------------------------------------
FUNCTION LOCK_FILTER_GROUP_RECORD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE, 
                                  O_locked_ind     IN OUT  BOOLEAN,
                                  I_merch_org      IN      VARCHAR2,
                                  I_sec_group_id   IN      FILTER_GROUP_MERCH.SEC_GROUP_ID%TYPE,
                                  I_filter_level   IN      FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                                  I_filter_id      IN      NUMBER,
                                  I_class          IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                                  I_subclass       IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)
RETURN BOOLEAN IS
   
   L_program        VARCHAR2(61) := 'FILTER_GROUP_HIER_SQL.LOCK_FILTER_GROUP_RECORD';
   L_inv_param      VARCHAR2(30);
   L_table          VARCHAR2(30);
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_MERCH is
      select 'x'
        from filter_group_merch
       where sec_group_id              = I_sec_group_id
         and filter_merch_level        = I_filter_level
         and filter_merch_id           = I_filter_id
         and nvl(filter_merch_id_class, -1) = nvl(I_class, -1)
         and nvl(filter_merch_id_subclass, -1) = nvl(I_subclass, -1)
         for update nowait;

   cursor C_LOCK_ORG is
      select 'x'
        from filter_group_org
       where sec_group_id     = I_sec_group_id
         and filter_org_level = I_filter_level
         and filter_org_id    = I_filter_id
         for update nowait;

BEGIN

   if I_merch_org is NULL then 
      L_inv_param := 'I_merch_org'; 
   elsif I_sec_group_id is NULL then 
      L_inv_param := 'I_sec_group_id'; 
   elsif I_filter_level is NULL then 
      L_inv_param := 'I_filter_level'; 
   elsif I_filter_id is NULL then 
      L_inv_param := 'I_filter_id'; 
   end if; 

   if L_inv_param is not NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            L_inv_param, 
                                            L_program, 
                                            NULL); 
      return FALSE; 
   end if; 

   L_table := 'FILTER_GROUP_'||I_merch_org;
   
   if I_merch_org = 'MERCH' then
      open  C_LOCK_MERCH;
      close C_LOCK_MERCH;
   else  -- I_merch_org = 'ORG'
      open  C_LOCK_ORG;
      close C_LOCK_ORG;
   end if;

   O_locked_ind := TRUE;
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_locked_ind := FALSE;
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
				            SQLERRM,
	  			            L_program,
				            to_char(SQLCODE));
      return FALSE;
END LOCK_FILTER_GROUP_RECORD;
--------------------------------------------------------------------------------

END FILTER_GROUP_HIER_SQL;
/
