CREATE OR REPLACE PACKAGE BODY RMSSUB_XITEM AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code          IN OUT  VARCHAR2,
                        IO_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause                IN      VARCHAR2,
                        I_program              IN      VARCHAR2);
-------------------------------------------------------------------------------------------------------
-- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code   IN OUT   VARCHAR2,
                  O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN       RIB_OBJECT,
                  I_message_type  IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_program        VARCHAR2(50) := 'RMSSUB_XITEM.CONSUME';
   L_message_type   VARCHAR2(30) := LOWER(I_message_type);
   L_ref_message    "RIB_XItemRef_REC";
   L_message        "RIB_XItemDesc_REC";
   L_item_rec       RMSSUB_XITEM.ITEM_API_REC;
   L_item_del_rec   RMSSUB_XITEM.ITEM_API_DEL_REC;


BEGIN
   O_STATUS_CODE := API_CODES.SUCCESS;
   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;
  

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;
   if L_message_type  in (ITEM_ADD, ITEM_UPD, 
                          ISUP_ADD, ISUP_UPD, 
                          ISC_ADD,  ISC_UPD,
                          ISMC_ADD, ISMC_UPD, 
                          ISCL_ADD, ISCL_UPD, 
                          IVAT_ADD, ISCD_ADD,
                          ISCD_UPD, SEASON_ADD, 
                          ITEMUDA_ADD, ITEMUDA_UPD, 
                          IMAGE_ADD, IMAGE_UPD, 
                          ICTRY_ADD, ITCOST_ADD,
                          IMTL_ADD, IMTL_UPD,
                          ISTL_ADD, ISTL_UPD,
                          IITL_ADD, IITL_UPD) then
                             
      L_message := treat(I_MESSAGE AS "RIB_XItemDesc_REC");  
 
      if L_message is NULL then  
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE', 'No Message', NULL, NULL);
          raise PROGRAM_ERROR;
      end if;
       -- This will just populate the records
      if RMSSUB_XITEM_POP_RECORD.POPULATE(O_error_message,
                                          L_item_rec,
                                          L_message,
                                          L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      ---
      -- INSERT/UPDATE table
      if RMSSUB_XITEM_SQL.PERSIST(O_error_message,
                                  L_message_type,
                                  L_message,
                                  L_item_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      
      if NVL(L_message.attempt_rms_load, 'STG') NOT IN ('STG','RMS') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_RMS_LOAD_VALUE',
                                               L_message.attempt_rms_load,
                                               NULL,
                                               NULL);
         raise PROGRAM_ERROR;
      end if;
   elsif L_message_type in (ITEM_DEL,
                            ISUP_DEL,
                            ISC_DEL,
                            ISMC_DEL,
                            ISCL_DEL,
                            IVAT_DEL,
                            ISCD_DEL,
                            SEASON_DEL,
                            ITEMUDA_DEL,
                            IMAGE_DEL,
                            ICTRY_DEL,
                            ITCOST_DEL,
                            IMTL_DEL,
                            ISTL_DEL,
                            IITL_DEL) then
    
      L_ref_message := treat(I_MESSAGE AS "RIB_XItemRef_REC");
      if L_ref_message is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', 'No ref message for delete', NULL, NULL);
         raise PROGRAM_ERROR;
      end if;
      -- populate the record
      if RMSSUB_XITEM_POP_RECORD.POPULATE(O_error_message,
                                          L_item_del_rec,
                                          L_ref_message,
                                          L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      -- DELETE from table
      if RMSSUB_XITEM_SQL.PERSIST(O_error_message,
                                  L_message_type,
                                  L_item_del_rec,
                                  L_ref_message) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      
--      if NVL(L_ref_message.attempt_rms_load, 'STG') NOT IN ('STG','RMS') then
--         O_error_message := SQL_LIB.CREATE_MSG('INV_RMS_LOAD_VALUE',
--                                               L_ref_message.attempt_rms_load,
--                                               NULL,
--                                               NULL);
--         raise PROGRAM_ERROR;
--      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE', L_message_type, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code               IN OUT  VARCHAR2,
                        IO_error_message            IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause                     IN      VARCHAR2,
                        I_program                   IN      VARCHAR2) IS

   L_program  VARCHAR2(50)   :=  'RMSSUB_XITEM.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);
EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------
END RMSSUB_XITEM;
/
