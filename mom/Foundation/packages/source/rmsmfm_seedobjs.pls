
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_SEEDOBJ AUTHID CURRENT_USER AS

FAMILY   CONSTANT   RIB_SETTINGS.FAMILY%TYPE := 'seedobj';

COUNTRY_ADD   CONSTANT   VARCHAR2(15)   := 'countrycre'; 
COUNTRY_UPD   CONSTANT   VARCHAR2(15)   := 'countrymod';
COUNTRY_DEL   CONSTANT   VARCHAR2(15)   := 'countrydel';

CURR_ADD      CONSTANT VARCHAR2(15)     := 'currratecre'; 
CURR_UPD      CONSTANT VARCHAR2(15)     := 'currratemod';
--------------------------------------------------------------------------------
-- Function : ADDTOQ
-- Purpose  : Add new entry to SEEDOBJ_MFQUEUE
-- Calls    : None
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message    IN OUT   VARCHAR2,
                I_message_type     IN       SEEDOBJ_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_country_id       IN       SEEDOBJ_MFQUEUE.COUNTRY_ID%TYPE,
                I_currency_code    IN       SEEDOBJ_MFQUEUE.CURRENCY_CODE%TYPE,
                I_country_desc     IN       SEEDOBJ_MFQUEUE.COUNTRY_DESC%TYPE,
                I_effective_date   IN       SEEDOBJ_MFQUEUE.EFFECTIVE_DATE%TYPE,
                I_exchange_type    IN       SEEDOBJ_MFQUEUE.EXCHANGE_TYPE%TYPE,
                I_exchange_rate    IN       SEEDOBJ_MFQUEUE.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function : GETNXT
-- Purpose  : Get next entry from SEEDOBJ_MFQUEUE
-- Calls    : PROCESS_QUEUE_REC_X, HANDLE_ERRORS
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code    IN OUT   VARCHAR2,
                 O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_message_type   IN OUT   VARCHAR2,
                 O_message        IN OUT   RIB_OBJECT,
                 O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                 O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                 I_num_threads    IN       NUMBER DEFAULT 1,
                 I_thread_val     IN       NUMBER DEFAULT 1);
--------------------------------------------------------------------------------
-- Function : PUB_RETRY
-- Purpose  : Retry next hospital entry from SEEDOBJ_MFQUEUE
-- Calls    : PROCESS_QUEUE_REC_X, HANDLE_ERRORS
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code    IN OUT   VARCHAR2,
                    O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_message        IN OUT   RIB_OBJECT,
                    O_message_type   IN OUT   VARCHAR2,
                    O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                    O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                    I_ref_object     IN       RIB_OBJECT);
                    
END RMSMFM_SEEDOBJ;
/
