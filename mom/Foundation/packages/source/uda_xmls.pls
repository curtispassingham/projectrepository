
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE UDA_XML AUTHID CURRENT_USER AS

--------------------------------------------------------
FUNCTION BUILD_UDA_MSG(O_status          OUT VARCHAR2,
                       O_text            OUT VARCHAR2,
                       O_message         OUT CLOB,
                       I_record          IN  UDA%ROWTYPE,
                       I_action_type     IN  VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION BUILD_UDAV_MSG(O_status          OUT VARCHAR2,
                        O_text            OUT VARCHAR2,
                        O_message         OUT CLOB,
                        I_record          IN  UDA_VALUES%ROWTYPE,
                        I_action_type     IN  VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
END UDA_XML;
/
