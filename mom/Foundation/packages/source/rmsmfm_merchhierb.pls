CREATE OR REPLACE PACKAGE BODY RMSMFM_MERCHHIER AS

   LP_seq_no       MERCHHIER_MFQUEUE.SEQ_NO%TYPE  := NULL;
   LP_error_status VARCHAR2(1)                    := NULL;
   
   cursor C_QUEUE( P_thread_val in number) is
      select q.rowid,
             q.seq_no,
             q.division,
             q.group_no,
             q.dept,
             q.class,
             q.subclass,
             q.div_name,
             q.buyer,
             q.merch,
             q.total_market_amount,
             q.group_name,
             q.dept_name,
             q.profit_calc_type,
             q.purchase_type,
             q.bud_int,
             q.bud_mkup,
             q.markup_calc_type,
             q.otb_calc_type,
             q.dept_vat_incl_ind,
             q.class_name,
             q.class_vat_ind,
             q.subclass_name,
             q.message_type,
             q.pub_status
        from merchhier_mfqueue q
       where q.seq_no = nvl(LP_seq_no,(select min(q2.seq_no)
                                         from merchhier_mfqueue q2
                                        where q2.thread_no = nvl(P_thread_val, q2.thread_no)
                                          and q2.pub_status = 'U'))
         for update NOWAIT;
		 
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_message         OUT        VARCHAR2,
                              O_message           IN  OUT nocopy RIB_OBJECT,
                              O_bus_obj_id        IN  OUT nocopy RIB_BUSOBJID_TBL,
                              I_message_type      IN             VARCHAR2,
                              I_rec               IN             C_QUEUE%ROWTYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code       IN OUT         VARCHAR2,
                        O_error_message     IN OUT         VARCHAR2,
                        O_message           IN OUT  nocopy RIB_OBJECT,
                        O_bus_obj_id        IN OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info      IN OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_rec               IN             C_QUEUE%ROWTYPE);
--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg                OUT  VARCHAR2,
                I_message_type          IN      MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_division              IN      DIVISION.DIVISION%TYPE,
                I_division_rec          IN      DIVISION%ROWTYPE,
                I_group_no              IN      GROUPS.GROUP_NO%TYPE,
                I_groups_rec            IN      GROUPS%ROWTYPE,
                I_dept                  IN      DEPS.DEPT%TYPE,
                I_deps_rec              IN      DEPS%ROWTYPE,
                I_class                 IN      CLASS.CLASS%TYPE,
                I_class_rec             IN      CLASS%ROWTYPE,
                I_subclass              IN      SUBCLASS.SUBCLASS%TYPE,
                I_subclass_rec          IN      SUBCLASS%ROWTYPE)
RETURN BOOLEAN IS

   L_status_code        VARCHAR2(1):='N';
   L_max_details        rib_settings.max_details_to_publish%TYPE:=NULL;
   L_num_threads        rib_settings.num_threads%TYPE:=NULL;
   L_min_time_lag       rib_settings.minutes_time_lag%TYPE:=NULL;
   L_thread_no          rib_settings.num_threads%TYPE:=NULL;

BEGIN

   -- Irrespective of the message type add message to queue

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_MERCHHIER.FAMILY);
   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;
   
   -- This does not use multi-threading, therefore hardcoded instead of using
   -- eg. L_thread_no := MOD(I_division, L_num_threads) + 1;
   L_thread_no := 1; 

   if I_message_type = DIV_ADD or
      I_message_type = DIV_UPD then
      insert into merchhier_mfqueue( seq_no,
                                     division,
                                     div_name,
                                     buyer,
                                     merch,
                                     total_market_amount,
                                     message_type,
                                     thread_no,
                                     family,
                                     custom_message_type,
                                     pub_status,
                                     transaction_number,
                                     transaction_time_stamp )
                            values ( merchhier_mfsequence.NEXTVAL,
                                     I_division,
                                     I_division_rec.div_name,
                                     I_division_rec.buyer,
                                     I_division_rec.merch,
                                     I_division_rec.total_market_amt,
                                     I_message_type,
                                     L_thread_no,
                                     RMSMFM_MERCHHIER.FAMILY,
                                     'N',
                                     'U',
                                     I_division,
                                     SYSDATE);                                        
      return TRUE;
   elsif I_message_type = DIV_DEL then
      insert into merchhier_mfqueue( seq_no,
                                     division,
                                     message_type,
                                     thread_no,
                                     family,
                                     custom_message_type,
                                     pub_status,
                                     transaction_number,
                                     transaction_time_stamp )
                            values ( merchhier_mfsequence.NEXTVAL,
                                     I_division,
                                     I_message_type,
                                     L_thread_no,
                                     RMSMFM_MERCHHIER.FAMILY,
                                     'N',
                                     'U',
                                     I_division,
                                     SYSDATE);
      return TRUE;
   end if;

   if I_message_type = GRP_ADD or
      I_message_type = GRP_UPD then
      insert into merchhier_mfqueue( seq_no,
                                     group_no,
                                     group_name,
                                     buyer,
                                     merch,
                                     division,
                                     message_type,
                                     thread_no,
                                     family,
                                     custom_message_type,
                                     pub_status,
                                     transaction_number,
                                     transaction_time_stamp )
                            values ( merchhier_mfsequence.NEXTVAL,
                                     I_group_no,
                                     I_groups_rec.group_name,
                                     I_groups_rec.buyer,
                                     I_groups_rec.merch,
                                     I_groups_rec.division,
                                     I_message_type,
                                     L_thread_no,
                                     RMSMFM_MERCHHIER.FAMILY,
                                     'N',
                                     'U',
                                     I_group_no,
                                     SYSDATE);
      return TRUE;     
   elsif I_message_type = GRP_DEL then
      insert into merchhier_mfqueue( seq_no,
                                     group_no,
                                     message_type,
                                     thread_no,
                                     family,
                                     custom_message_type,
                                     pub_status,
                                     transaction_number,
                                     transaction_time_stamp )
                            values ( merchhier_mfsequence.NEXTVAL,
                                     I_group_no,
                                     I_message_type,
                                     L_thread_no,
                                     RMSMFM_MERCHHIER.FAMILY,
                                     'N',
                                     'U',
                                     I_group_no,
                                     SYSDATE); 
      return TRUE;
   end if;

   if I_message_type = DEP_ADD or
      I_message_type = DEP_UPD then
      insert into merchhier_mfqueue ( seq_no,
                                      dept,
                                      dept_name,
                                      buyer,
                                      merch,
                                      profit_calc_type,
                                      purchase_type,
                                      group_no,
                                      bud_int,
                                      bud_mkup,
                                      total_market_amount,
                                      markup_calc_type,
                                      otb_calc_type,
                                      dept_vat_incl_ind,
                                      message_type,
                                      thread_no,
                                      family,
                                      custom_message_type,
                                      pub_status,
                                      transaction_number,
                                      transaction_time_stamp )
                             values ( merchhier_mfsequence.NEXTVAL,
                                      I_dept,
                                      I_deps_rec.dept_name,
                                      I_deps_rec.buyer,
                                      I_deps_rec.merch,
                                      I_deps_rec.profit_calc_type,
                                      I_deps_rec.purchase_type,
                                      I_deps_rec.group_no,
                                      I_deps_rec.bud_int,
                                      I_deps_rec.bud_mkup,
                                      I_deps_rec.total_market_amt,
                                      I_deps_rec.markup_calc_type,
                                      I_deps_rec.otb_calc_type,
                                      I_deps_rec.dept_vat_incl_ind,
                                      I_message_type,
                                      L_thread_no,
                                      RMSMFM_MERCHHIER.FAMILY,
                                      'N',
                                      'U',
                                      I_dept,
                                      SYSDATE);
      return TRUE;
   elsif I_message_type = DEP_DEL then
      insert into merchhier_mfqueue( seq_no,
                                     dept,
                                     message_type,
                                     thread_no,
                                     family,
                                     custom_message_type,
                                     pub_status,
                                     transaction_number,
                                     transaction_time_stamp )
                            values ( merchhier_mfsequence.NEXTVAL,
                                     I_dept,
                                     I_message_type,
                                     L_thread_no,
                                     RMSMFM_MERCHHIER.FAMILY,
                                     'N',
                                     'U',
                                     I_dept,
                                     SYSDATE);

      delete from merchhier_mfqueue
            where dept = (select dept
                            from merchhier_mfqueue
                           where pub_status='U'
                             and message_type ='deptdel'
                             and dept = I_dept
                             and exists (select dept
                                           from merchhier_mfqueue
                                          where pub_status = 'U'
                                            and message_type ='deptcre'
                                            and dept = I_dept));
      return TRUE;
   end if;

   if I_message_type = CLS_ADD or
      I_message_type = CLS_UPD then
      insert into merchhier_mfqueue ( seq_no,
                                      dept,
                                      class,
                                      class_name,
                                      class_vat_ind,
                                      message_type,
                                      thread_no,
                                      family,
                                      custom_message_type,
                                      pub_status,
                                      transaction_number,
                                      transaction_time_stamp )
                             values ( merchhier_mfsequence.NEXTVAL,
                                      I_class_rec.dept,
                                      I_class_rec.class,
                                      I_class_rec.class_name,
                                      I_class_rec.class_vat_ind,
                                      I_message_type,
                                      L_thread_no,
                                      RMSMFM_MERCHHIER.FAMILY,
                                      'N',
                                      'U',
                                      I_class_rec.class,
                                      SYSDATE);
      return TRUE;
   elsif I_message_type = CLS_DEL then
      insert into merchhier_mfqueue( seq_no,
                                     dept,
                                     class,
                                     message_type,
                                     thread_no,
                                     family,
                                     custom_message_type,
                                     pub_status,
                                     transaction_number,
                                     transaction_time_stamp )
                            values ( merchhier_mfsequence.NEXTVAL,
                                     I_dept,
                                     I_class,
                                     I_message_type,
                                     L_thread_no,
                                     RMSMFM_MERCHHIER.FAMILY,
                                     'N',
                                     'U',
                                     I_class,
                                     SYSDATE);
      return TRUE;
   end if;
   
   if I_message_type = SUB_ADD or
      I_message_type = SUB_UPD then
      insert into merchhier_mfqueue( seq_no,
                                     dept,
                                     class,
                                     subclass,
                                     subclass_name,
                                     message_type,
                                     thread_no,
                                     family,
                                     custom_message_type,
                                     pub_status,
                                     transaction_number,
                                     transaction_time_stamp )
                            values ( merchhier_mfsequence.NEXTVAL,
                                     I_subclass_rec.dept,
                                     I_subclass_rec.class,
                                     I_subclass_rec.subclass,
                                     I_subclass_rec.sub_name,
                                     I_message_type,
                                     L_thread_no,
                                     RMSMFM_MERCHHIER.FAMILY,
                                     'N',
                                     'U',
                                     I_subclass_rec.subclass,
                                     SYSDATE);
      return TRUE;
   elsif I_message_type = SUB_DEL then
      insert into merchhier_mfqueue( seq_no,
                                     dept,
                                     class,
                                     subclass,
                                     message_type,
                                     thread_no,
                                     family,
                                     custom_message_type,
                                     pub_status,
                                     transaction_number,
                                     transaction_time_stamp )
                            values ( merchhier_mfsequence.NEXTVAL,
                                     I_dept,
                                     I_class,
                                     I_subclass,
                                     I_message_type,
                                     L_thread_no,
                                     RMSMFM_MERCHHIER.FAMILY,
                                     'N',
                                     'U',
                                     I_subclass,
                                     SYSDATE);
      return TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_MERCHHIER.ADDTOQ',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code   OUT  VARCHAR2,
                 O_error_msg     OUT  VARCHAR2,
                 O_message_type  OUT  VARCHAR2,
                 O_message       OUT  RIB_OBJECT,
                 O_bus_obj_id    OUT  RIB_BUSOBJID_TBL,
                 O_routing_info  OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads   IN   NUMBER DEFAULT 1,
                 I_thread_val    IN   NUMBER DEFAULT 1)
IS

   L_hospital_found     VARCHAR2(1)                   := 'N';
   QUEUE_CURSOR_REC     C_QUEUE%ROWTYPE;
   
   cursor C_CHECK_FOR_HOSPITAL_MSGS is
      select 'Y'
        from merchhier_mfqueue
       where ((division  = queue_cursor_rec.division and
               message_type in (DIV_ADD, DIV_UPD, DIV_DEL)) or
              (group_no = queue_cursor_rec.group_no and
 		       message_type in (GRP_ADD, GRP_UPD, GRP_DEL)) or
 		      (dept = queue_cursor_rec.dept and
 		       message_type in (DEP_ADD, DEP_UPD, DEP_DEL)) or
 		      (dept = queue_cursor_rec.dept and
 		       class = queue_cursor_rec.class and
 			   message_type in (CLS_ADD, CLS_UPD, CLS_DEL)) or
 		      (dept = queue_cursor_rec.dept and
 		       class = queue_cursor_rec.class and
 			   subclass = queue_cursor_rec.subclass and
 			   message_type in (SUB_ADD, SUB_UPD, SUB_DEL)))
         and pub_status = 'H';
   
BEGIN
   -- status of 'H'ospital
   LP_error_status := API_CODES.HOSPITAL;
   
   -- set LP_seq_no to null, this should only be populated when
   -- pub_retry is processing.
   LP_seq_no       :=NULL;

   open  C_QUEUE(I_thread_val);
   fetch C_QUEUE into queue_cursor_rec;
   close C_QUEUE;
   
   if queue_cursor_rec.seq_no is NULL then    -- If no rows to process
      O_status_code := API_CODES.NO_MSG;      -- Set 'Nothing to Publish'
      return;                                 -- Nothing else to do, Bye bye
   end if;

   open  C_CHECK_FOR_HOSPITAL_MSGS;
   fetch C_CHECK_FOR_HOSPITAL_MSGS into L_hospital_found;
   close C_CHECK_FOR_HOSPITAL_MSGS;
   
   if L_hospital_found = 'Y' then
      O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                        NULL,
                                        NULL,
                                        NULL);
      raise PROGRAM_ERROR;
   end if;
   
   -- We will publish (unless something very unusual happens), so set up common stuff
   O_status_code  := API_CODES.NEW_MSG;
   O_message_type := queue_cursor_rec.message_type;
   O_routing_info := RIB_ROUTINGINFO_TBL();      -- no routing info required

   if PROCESS_QUEUE_RECORD(O_error_msg,
                           O_message,
                           O_bus_obj_id,
                           O_message_type,
                           queue_cursor_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   
   LP_error_status := API_CODES.UNHANDLED_ERROR;
   -- Delete the row even if not published because merchandise heirarchy wasn't there
   delete from merchhier_mfqueue b
    where b.rowid = queue_cursor_rec.rowid;

EXCEPTION
   when OTHERS then
      if O_error_msg is NULL then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_MERCHIER.GETNXT',
                                            TO_CHAR(SQLCODE));
      end if;

      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    queue_cursor_rec);
END GETNXT;
---------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT   VARCHAR2,
                    O_error_msg           OUT   VARCHAR2,
                    O_message_type    IN  OUT   VARCHAR2,
                    O_message             OUT   RIB_OBJECT,
                    O_bus_obj_id      IN  OUT   RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT   RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN        RIB_OBJECT)
IS

   L_seq_no             MERCHHIER_MFQUEUE.SEQ_NO%TYPE := NULL;
   L_hospital_found     VARCHAR2(1)                   := 'N';
   QUEUE_CURSOR_REC     C_QUEUE%ROWTYPE;

BEGIN

   -- status of 'H'ospital
   LP_error_status := API_CODES.HOSPITAL;

   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   LP_seq_no      := O_routing_info(1).value;
   O_message      := NULL;
   O_routing_info := RIB_ROUTINGINFO_TBL();      -- no routing info required

   -- pass NULL for the thread number.  The thread_no does not matter
   -- because the sequence number of the record being processed is already known.
   open  C_QUEUE(NULL);
   fetch C_QUEUE into queue_cursor_rec;
   close C_QUEUE;

   if queue_cursor_rec.seq_no is NULL then    -- If no rows to process
      O_status_code := API_CODES.NO_MSG;      -- Set 'Nothing to Publish'
      return;                                 -- Nothing else to do, Bye bye
   end if;
   
   -- We will publish (unless something very unusual happens), so set up common stuff
   O_status_code  := API_CODES.NEW_MSG;
   O_message_type := queue_cursor_rec.message_type;

   if PROCESS_QUEUE_RECORD(O_error_msg,
                           O_message,
                           O_bus_obj_id,
                           O_message_type,
                           queue_cursor_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   
   LP_error_status := API_CODES.UNHANDLED_ERROR;
   -- Delete the row even if not published because merchandise heirarchy wasn't there
   delete from merchhier_mfqueue b
    where b.rowid = queue_cursor_rec.rowid;
   
EXCEPTION
   when OTHERS then
      if O_error_msg is NULL then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_MERCHHIER.PUB_RETRY',
                                            TO_CHAR(SQLCODE));
      end if;

      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    queue_cursor_rec);
END PUB_RETRY;
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_message         OUT        VARCHAR2,
                              O_message           IN  OUT nocopy RIB_OBJECT,
                              O_bus_obj_id        IN  OUT nocopy RIB_BUSOBJID_TBL,
                              I_message_type      IN             VARCHAR2,
                              I_rec               IN             C_QUEUE%ROWTYPE)
RETURN BOOLEAN IS

   L_rib_divisiondesc_rec  "RIB_MrchHrDivDesc_REC"  := NULL;
   L_rib_divisionref_rec   "RIB_MrchHrDivRef_REC"   := NULL;
   L_rib_groupdesc_rec     "RIB_MrchHrGrpDesc_REC"  := NULL;
   L_rib_groupref_rec      "RIB_MrchHrGrpRef_REC"   := NULL;
   L_rib_deptdesc_rec      "RIB_MrchHrDeptDesc_REC" := NULL;
   L_rib_deptref_rec       "RIB_MrchHrDeptRef_REC"  := NULL;
   L_rib_classdesc_rec     "RIB_MrchHrClsDesc_REC"  := NULL;
   L_rib_classref_rec      "RIB_MrchHrClsRef_REC"   := NULL;
   L_rib_subclassdesc_rec  "RIB_MrchHrSclsDesc_REC" := NULL;
   L_rib_subclassref_rec   "RIB_MrchHrSclsRef_REC"  := NULL;
   L_bus_objs              RIB_BUSOBJID_TBL;

BEGIN
   L_bus_objs := RIB_BUSOBJID_TBL();

   -- Division Add/Update/Delete
   if I_rec.message_type in (DIV_DEL,DIV_ADD,DIV_UPD) then
     if I_rec.message_type = DIV_DEL then  -- No division details to look up      
        L_rib_divisionref_rec := "RIB_MrchHrDivRef_REC"(0, I_rec.division); -- Publish the Delete
        O_message := L_rib_divisionref_rec;
     else  -- DIV_ADD or DIV_UPD
        L_rib_divisiondesc_rec := "RIB_MrchHrDivDesc_REC"(0,
                                                        I_rec.division,
                                                        I_rec.div_name,
                                                        I_rec.buyer,
														I_rec.merch,
                                                        I_rec.total_market_amount);
        O_message := L_rib_divisiondesc_rec;
     end if;
     L_bus_objs.extend(1);
     L_bus_objs(1) := I_rec.division;
     
   -- Group Add/Update/Delete
   elsif I_rec.message_type in(GRP_DEL, GRP_ADD, GRP_UPD) then
     if I_rec.message_type = GRP_DEL then  -- No Group details to look up      
        L_rib_groupref_rec := "RIB_MrchHrGrpRef_REC"(0, I_rec.group_no); -- Publish the Delete
        O_message := L_rib_groupref_rec;
     else  -- GRP_ADD or GRP_UPD
        L_rib_groupdesc_rec := "RIB_MrchHrGrpDesc_REC"(0,
                                                     I_rec.group_no,
                                                     I_rec.group_name,
                                                     I_rec.buyer,
                                                     I_rec.merch,
                                                     I_rec.division);
        O_message := L_rib_groupdesc_rec;
     end if;
     L_bus_objs.extend(1);
     L_bus_objs(1) := I_rec.group_no;   

   -- Department Add/Update/Delete
   elsif I_rec.message_type in(DEP_DEL, DEP_ADD, DEP_UPD) then
     if I_rec.message_type = DEP_DEL then  -- No Department details to look up      
        L_rib_deptref_rec := "RIB_MrchHrDeptRef_REC"(0, I_rec.dept); -- Publish the Delete
        O_message := L_rib_deptref_rec;
     else  -- DEP_ADD or DEP_UPD
        L_rib_deptdesc_rec := "RIB_MrchHrDeptDesc_REC"(0,
                                                     I_rec.dept,
                                                     I_rec.dept_name,
                                                     I_rec.buyer,
                                                     I_rec.purchase_type,
                                                     I_rec.total_market_amount,
                                                     I_rec.merch,
                                                     I_rec.group_no,
                                                     I_rec.bud_mkup,
                                                     I_rec.profit_calc_type,
                                                     I_rec.markup_calc_type,
                                                     I_rec.otb_calc_type,
                                                     NULL,
                                                     NULL,
                                                     I_rec.bud_int,
                                                     I_rec.dept_vat_incl_ind);
        O_message := L_rib_deptdesc_rec;
     end if;
     L_bus_objs.extend(1);
     L_bus_objs(1) := I_rec.dept;   

   -- Class Add/Update/Delete
   elsif I_rec.message_type in(CLS_DEL, CLS_ADD, CLS_UPD) then
     if I_rec.message_type = CLS_DEL then  -- No Class details to look up      
        L_rib_classref_rec := "RIB_MrchHrClsRef_REC"(0, 
		                                           I_rec.class, 
												   I_rec.dept); -- Publish the Delete
        O_message := L_rib_classref_rec;
     else  -- CLS_ADD or CLS_UPD
        L_rib_classdesc_rec := "RIB_MrchHrClsDesc_REC"(0,
                                                     I_rec.class,
                                                     I_rec.class_name,
                                                     I_rec.class_vat_ind,
                                                     I_rec.dept);
        O_message := L_rib_classdesc_rec;
     end if;
     L_bus_objs.extend(2);
     L_bus_objs(1) := I_rec.class;   
     L_bus_objs(2) := I_rec.dept;   

   -- Subclass Add/Update/Delete
   else
     if I_rec.message_type = SUB_DEL then  -- No Subclass details to look up      
        L_rib_subclassref_rec := "RIB_MrchHrSclsRef_REC"(0, 
		                                               I_rec.subclass, 
													   I_rec.class, 
													   I_rec.dept); -- Publish the Delete
        O_message := L_rib_subclassref_rec;
     else  -- SUB_ADD or SUB_UPD
        L_rib_subclassdesc_rec := "RIB_MrchHrSclsDesc_REC"(0,
                                                         I_rec.subclass,
                                                         I_rec.subclass_name,
                                                         I_rec.class,
                                                         I_rec.dept);
        O_message := L_rib_subclassdesc_rec;
     end if;
     L_bus_objs.extend(3);
     L_bus_objs(1) := I_rec.subclass;   
     L_bus_objs(2) := I_rec.class;   
     L_bus_objs(3) := I_rec.dept;   

   end if;
   
   O_bus_obj_id   := L_bus_objs;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_MERCHHIER.PROCESS_QUEUE_RECORD',
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_RECORD;
---------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code       IN OUT         VARCHAR2,
                        O_error_message     IN OUT         VARCHAR2,
                        O_message           IN OUT  nocopy RIB_OBJECT,
                        O_bus_obj_id        IN OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info      IN OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_rec               IN             C_QUEUE%ROWTYPE)
IS

   L_module     VARCHAR2(64)      := 'RMSMFM_MERCHHIER.HANDLE_ERRORS';
   L_error_type VARCHAR2(5)       := NULL;
   
   L_rib_divisionref_rec   "RIB_MrchHrDivRef_REC"   := NULL;
   L_rib_groupref_rec      "RIB_MrchHrGrpRef_REC"   := NULL;
   L_rib_deptref_rec       "RIB_MrchHrDeptRef_REC"  := NULL;
   L_rib_classref_rec      "RIB_MrchHrClsRef_REC"   := NULL;
   L_rib_subclassref_rec   "RIB_MrchHrSclsRef_REC"  := NULL;
   L_bus_objs              RIB_BUSOBJID_TBL;

BEGIN
   L_bus_objs := RIB_BUSOBJID_TBL();
   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then   
      if I_rec.message_type in (DIV_DEL,DIV_ADD,DIV_UPD) then
         L_rib_divisionref_rec := "RIB_MrchHrDivRef_REC"(0, 
                                                       I_rec.division);
         O_message := L_rib_divisionref_rec;
         --- Build Business Object
         L_bus_objs.extend(1);
         L_bus_objs(1) := I_rec.division;
        
      -- Group Add/Update/Delete
      elsif I_rec.message_type in(GRP_DEL, GRP_ADD, GRP_UPD) then
         L_rib_groupref_rec := "RIB_MrchHrGrpRef_REC"(0, 
                                                    I_rec.group_no); 
         O_message := L_rib_groupref_rec;
         --- Build Business Object
         L_bus_objs.extend(1);
         L_bus_objs(1) := I_rec.group_no;   
   
      -- Department Add/Update/Delete
      elsif I_rec.message_type in(DEP_DEL, DEP_ADD, DEP_UPD) then  
         L_rib_deptref_rec := "RIB_MrchHrDeptRef_REC"(0, I_rec.dept);
         O_message := L_rib_deptref_rec;
         --- Build Business Object
         L_bus_objs.extend(1);
         L_bus_objs(1) := I_rec.dept;   
   
      -- Class Add/Update/Delete
      elsif I_rec.message_type in(CLS_DEL, CLS_ADD, CLS_UPD) then
         L_rib_classref_rec := "RIB_MrchHrClsRef_REC"(0, 
		                                            I_rec.class, 
												    I_rec.dept);
         O_message := L_rib_classref_rec;
		 --- Build Business Object
		 L_bus_objs.extend(2);
         L_bus_objs(1) := I_rec.class;   
         L_bus_objs(2) := I_rec.dept;   
   
      -- Subclass Add/Update/Delete
      else
	     L_rib_subclassref_rec := "RIB_MrchHrSclsRef_REC"(0, 
		                                                I_rec.subclass, 
													    I_rec.class, 
													    I_rec.dept);
		 O_message := L_rib_subclassref_rec;
         --- Build Business Object
         L_bus_objs.extend(3);
         L_bus_objs(1) := I_rec.subclass;   
         L_bus_objs(2) := I_rec.class;   
         L_bus_objs(3) := I_rec.dept;   
      end if;
  

      O_bus_obj_id    := L_bus_objs;
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_rec.seq_no,
                                                                 null,null,null,null));
      ---
      update merchhier_mfqueue
         set pub_status = LP_error_status
       where seq_no    = I_rec.seq_no;

   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type,
                   O_error_message);

EXCEPTION
   when OTHERS then
      O_status_code   := API_CODES.UNHANDLED_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSMFM_MERCHHIER.HANDLE_ERRORS',
                                            TO_CHAR(SQLCODE));
END HANDLE_ERRORS;
---------------------------------------------------------------------------------
end RMSMFM_MERCHHIER;
/
