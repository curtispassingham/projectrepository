create or replace PACKAGE BODY SIMPLE_PACK_SQL AS
-----------------------------------------------------------------------------------------------------
FUNCTION SET_RPM_ITEM_ZONE_PRICE(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                                 I_dept                 IN      DEPS.DEPT%TYPE,
                                 I_class                IN      CLASS.CLASS%TYPE,
                                 I_subclass             IN      SUBCLASS.SUBCLASS%TYPE,
                                 I_cost_currency_code   IN      CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_unit_cost            IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------


FUNCTION CALCULATE_UNIT_COST ( O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE                     ,
                               O_cost_UOM_unit_cost_prim       IN OUT   SIMPLE_PACK_TEMP.UNIT_COST%TYPE              ,
                               O_standard_UOM_unit_cost_prim   IN OUT   SIMPLE_PACK_TEMP.UNIT_COST%TYPE              ,
                               IO_cost_UOM_unit_cost           IN OUT   SIMPLE_PACK_TEMP.UNIT_COST%TYPE              ,
                               IO_standard_UOM_unit_cost       IN OUT   SIMPLE_PACK_TEMP.UNIT_COST%TYPE              ,
                               I_cost_uom                      IN       UOM_CLASS.UOM%TYPE                           ,
                               I_std_uom                       IN       UOM_CLASS.UOM%TYPE                           ,
                               I_case_weight_uom               IN       SIMPLE_PACK_TEMP.CASE_WEIGHT_UOM%TYPE        ,
                               I_case_weight                   IN       SIMPLE_PACK_TEMP.CASE_WEIGHT%TYPE            ,
                               I_case_length_uom               IN       SIMPLE_PACK_TEMP.CASE_LWH_UOM%TYPE           ,
                               I_case_length                   IN       SIMPLE_PACK_TEMP.CASE_LENGTH%TYPE            ,
                               I_case_liquid_volume_uom        IN       SIMPLE_PACK_TEMP.CASE_LIQUID_VOLUME_UOM%TYPE ,
                               I_case_liquid_volume            IN       SIMPLE_PACK_TEMP.CASE_LIQUID_VOLUME%TYPE     ,
                               I_total_qty                     IN       SIMPLE_PACK_TEMP.SUPP_PACK_SIZE%TYPE         ,
                               I_primary_currency              IN       CURRENCIES.CURRENCY_CODE%TYPE                ,
                               I_supplier_currency             IN       CURRENCIES.CURRENCY_CODE%TYPE                ,
                               I_item                          IN       ITEM_SUPP_COUNTRY.ITEM%TYPE                  ,
                               I_supplier                      IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE              ,
                               I_origin_country_id             IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE
                             )
RETURN BOOLEAN IS

   L_cost_UOM          VARCHAR2(8) := 'COST'     ;
   L_standard_UOM      VARCHAR2(8) := 'STANDARD' ;
   L_driver            VARCHAR2(8)               ;

   L_cost_UOM_class    UOM_CLASS.UOM_CLASS%TYPE       ;
   L_dimension_qty     SIMPLE_PACK_TEMP.ITEM_QTY%TYPE ;
   L_dimension_uom     UOM_CLASS.UOM%TYPE             ;

   L_total_cost        SIMPLE_PACK_TEMP.UNIT_COST%TYPE ;

   L_vdate             PERIOD.VDATE%TYPE  := GET_VDATE ;

   L_supplier_currency CURRENCIES.CURRENCY_CODE%TYPE := I_supplier_currency ;
   L_primary_currency  CURRENCIES.CURRENCY_CODE%TYPE := I_primary_currency ;

   L_program           VARCHAR2(61)                := 'SIMPLE_PACK_SQL.CALCULATE_UNIT_COST';

BEGIN

   -- Decide which direction the conversion must go in
   if IO_cost_UOM_unit_cost     IS     NULL
    and IO_standard_UOM_unit_cost IS NOT NULL then
      L_driver := L_standard_UOM ;
   elsif IO_cost_UOM_unit_cost  IS NOT NULL
    and IO_standard_UOM_unit_cost IS     NULL then
      L_driver := L_cost_UOM ;
   else
      if IO_standard_UOM_unit_cost IS NOT NULL then
         L_driver := L_standard_UOM ;
      else
         O_cost_UOM_unit_cost_prim     := NULL;
         O_standard_UOM_unit_cost_prim := NULL;
         return TRUE;
      end if;
   end if;


   if L_primary_currency IS NULL then
      -- Retrieve the primary currency if its not passed in.
      if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                              L_primary_currency) then
         return FALSE;
      end if;
   end if;

   if L_supplier_currency IS NULL then

      if not CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                       I_supplier,
                                       'V',
                                       NULL,   -- zone group ID
                                       L_supplier_currency) then
         return FALSE;
      end if;

   end if;


   if UOM_SQL.GET_CLASS(O_error_message  ,
                        L_cost_uom_class ,
                        I_cost_uom       ) = FALSE then
      return FALSE;
   end if;


   -- decide which dimension quantity to use
   if L_cost_uom_class = 'MASS' then
      L_dimension_uom := I_case_weight_uom ;
      L_dimension_qty := I_case_weight     ;

   elsif L_cost_uom_class = 'DIMEN' then
      L_dimension_uom := I_case_length_uom ;
      L_dimension_qty := I_case_length     ;

   elsif L_cost_uom_class = 'LVOL' then
      L_dimension_uom := I_case_liquid_volume_uom ;
      L_dimension_qty := I_case_liquid_volume     ;

   elsif I_cost_uom = 'EA' then
      L_dimension_uom := 'EA' ;
      L_dimension_qty := I_total_qty     ;
   end if;

   -- check parameters
   if  L_dimension_uom IS NULL
    or L_dimension_qty IS NULL then
       if L_driver = L_cost_UOM then
          IO_standard_UOM_unit_cost := NULL;
          O_standard_UOM_unit_cost_prim    := NULL;
       else
          IO_cost_UOM_unit_cost := NULL;
          O_cost_UOM_unit_cost_prim     := NULL;
       end if;
       return TRUE;
   else

      -- ensure the dimension quantity is in the cost UOM
      if L_dimension_uom != I_cost_uom then
         if UOM_SQL.CONVERT(O_error_message               ,
                            L_dimension_qty               ,
                            I_cost_uom                    ,
                            L_dimension_qty               ,
                            L_dimension_uom               ,
                            I_item                        ,
                            I_supplier                    ,
                            I_origin_country_id            ) = FALSE THEN
            return FALSE ;
         end if;
      end if;

      if L_driver = L_standard_UOM then

         -- work out the total cost of the pack
         L_total_cost := IO_standard_UOM_unit_cost * I_total_qty ;

         -- distribute the total cost over the dimension quantity to give a unit cost
         IO_cost_UOM_unit_cost := L_total_cost / L_dimension_qty ;

      elsif L_driver = L_cost_UOM then

         -- work out the total cost of the pack
         L_total_cost := IO_cost_UOM_unit_cost * L_dimension_qty ;

         -- distribute the total cost over the supplier pack size to give a unit cost
         IO_standard_UOM_unit_cost := L_total_cost / I_total_qty ;

      end if;


      -- populat the unit costs in the primary currency
      O_cost_UOM_unit_cost_prim     := IO_cost_UOM_unit_cost     ;
      O_standard_UOM_unit_cost_prim := IO_standard_UOM_unit_cost ;
      if L_primary_currency != L_supplier_currency then

         if not CURRENCY_SQL.CONVERT(O_error_message           ,
                                     IO_cost_UOM_unit_cost     ,
                                     L_supplier_currency       ,
                                     L_primary_currency        ,
                                     O_cost_UOM_unit_cost_prim ,
                                     'C'                       ,
                                     L_vdate                   ,
                                     NULL                      ,
                                     NULL                      ,
                                     NULL                       ) then
             return FALSE;
         end if;

         if not CURRENCY_SQL.CONVERT(O_error_message               ,
                                     IO_standard_UOM_unit_cost     ,
                                     L_supplier_currency           ,
                                     L_primary_currency            ,
                                     O_standard_UOM_unit_cost_prim ,
                                     'C'                           ,
                                     L_vdate                       ,
                                     NULL                          ,
                                     NULL                          ,
                                     NULL                           ) then
             return FALSE;
         end if;

      end if;

   end if;


   return TRUE ;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      return FALSE;
END;

----------------------------------------------------------------------------------------
FUNCTION BUILD_PACK(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists              IN OUT   BOOLEAN,
                    O_exists_item         IN OUT   ITEM_MASTER.ITEM%TYPE,
                    I_item                IN       ITEM_MASTER.ITEM%TYPE,
                    I_supplier            IN       VARCHAR2,
                    I_wh                  IN       VARCHAR2,
                    I_store               IN       VARCHAR2,
                    I_expenses            IN       VARCHAR2,
                    I_hts                 IN       VARCHAR2,
                    I_uda                 IN       VARCHAR2,
                    I_ticket              IN       VARCHAR2,
                    I_seasons             IN       VARCHAR2,
                    I_docs                IN       VARCHAR2,
                    I_vat                 IN       VARCHAR2,
                    I_up_chrgs            IN       VARCHAR2,
                    I_curr_prim           IN       CURRENCIES.CURRENCY_CODE%TYPE,
                    I_item_xform_ind      IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                    I_inventory_ind       IN       ITEM_MASTER.INVENTORY_IND%TYPE,
                    I_internal_finisher   IN       VARCHAR2,
                    I_external_finisher   IN       VARCHAR2,
                    I_edi_ind             IN       VARCHAR2 DEFAULT 'N'
                    )
RETURN BOOLEAN IS

   L_function                 VARCHAR2(60)                      := 'SIMPLE_PACK_SQL.BUILD_PACK';
   L_vdate                    PERIOD.VDATE%TYPE                 := GET_VDATE;
   L_user_id                  ITEM_MASTER.LAST_UPDATE_ID%TYPE;
   L_sysdate                  ITEM_MASTER.CREATE_DATETIME%TYPE;
   L_curr_prim                SYSTEM_OPTIONS.CURRENCY_CODE%TYPE := I_curr_prim;
   L_primary_supp             SUPS.SUPPLIER%TYPE;
   L_pack_no                  SIMPLE_PACK_TEMP.PACK_NO%TYPE;
   L_item_aggregate_ind       ITEM_MASTER.ITEM_AGGREGATE_IND%TYPE;
   L_diff_1_aggregate_ind     ITEM_MASTER.DIFF_1_AGGREGATE_IND%TYPE;
   L_diff_2_aggregate_ind     ITEM_MASTER.DIFF_2_AGGREGATE_IND%TYPE;
   L_diff_3_aggregate_ind     ITEM_MASTER.DIFF_3_AGGREGATE_IND%TYPE;
   L_diff_4_aggregate_ind     ITEM_MASTER.DIFF_4_AGGREGATE_IND%TYPE;
   L_product_classification   ITEM_MASTER.PRODUCT_CLASSIFICATION%TYPE;
   L_brand_name               ITEM_MASTER.BRAND_NAME%TYPE;
   L_dept                     DEPS.DEPT%TYPE;
   L_class                    CLASS.CLASS%TYPE;
   L_subclass                 SUBCLASS.SUBCLASS%TYPE;
   L_store_ord_mult           ITEM_MASTER.STORE_ORD_MULT%TYPE;
   L_unit_retail_zone         ITEM_LOC.UNIT_RETAIL%TYPE;
   L_primary_ref_item_ind     ITEM_MASTER.PRIMARY_REF_ITEM_IND%TYPE;
   L_unit_cost_supp           SIMPLE_PACK_TEMP.UNIT_COST%TYPE;
   L_unit_cost_loc            SIMPLE_PACK_TEMP.UNIT_COST%TYPE;
   L_cost_zone_group_id       ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE;
   L_merch_ind                ITEM_MASTER.MERCHANDISE_IND%TYPE;
   L_catch_weight_ind         ITEM_MASTER.CATCH_WEIGHT_IND%TYPE;
   L_handling_temp            ITEM_MASTER.HANDLING_TEMP%TYPE;
   L_handling_sensitivity     ITEM_MASTER.HANDLING_SENSITIVITY%TYPE;
   L_item_parent              ITEM_MASTER.ITEM_PARENT%TYPE;
   L_average_weight           ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_default_packing_method   SYSTEM_OPTIONS.DEFAULT_PACKING_METHOD%TYPE;
   L_inventory_ind            ITEM_MASTER.INVENTORY_IND%TYPE := NULL;
   L_unit_retail_loc          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail_loc  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_loc          ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_loc          ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_loc    ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_loc    ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_system_options_rec       SYSTEM_OPTIONS%ROWTYPE;
   L_tax_info_tbl             OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   L_tax_info_rec             OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();
   L_run_report               BOOLEAN;
   L_base_cost                ITEM_COST_HEAD.BASE_COST%TYPE;
   L_negotiated_item_cost     ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE;
   L_extended_base_cost       ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE;
   L_unit_cost                ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost           ITEM_COST_HEAD.INCLUSIVE_COST%TYPE;
   L_supplier                 SUPS.SUPPLIER%TYPE;
   L_loc_type                 ITEM_SUPP_COUNTRY_LOC.LOC_TYPE%TYPE;
   L_neg_item_cost_iscl       ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE;
   L_default_loc_type         COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE;
   L_base_cost_iscl           ITEM_COST_HEAD.BASE_COST%TYPE;
   L_country_attrib_row       COUNTRY_ATTRIB%ROWTYPE;
   L_supp_country_row         ITEM_SUPP_COUNTRY%ROWTYPE;
   L_item_add_rec             ITEM_COST_SQL.ITEM_ADD_REC;
   L_default_po_cost          COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;

   L_lead_time              ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE;
   L_pickup_lead_time       ITEM_SUPP_COUNTRY.PICKUP_LEAD_TIME%TYPE;
   L_round_lvl              ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE;
   L_round_to_inner_pct     ITEM_SUPP_COUNTRY.ROUND_TO_INNER_PCT%TYPE;
   L_round_to_case_pct      ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE;
   L_round_to_layer_pct     ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE;
   L_round_to_pallet_pct    ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE;
   L_supp_hier_type_1       ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_1%TYPE;
   L_supp_hier_lvl_1        ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_1%TYPE;
   L_supp_hier_type_2       ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_2%TYPE;
   L_supp_hier_lvl_2        ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_2%TYPE;
   L_supp_hier_type_3       ITEM_SUPP_COUNTRY.SUPP_HIER_TYPE_3%TYPE;
   L_supp_hier_lvl_3        ITEM_SUPP_COUNTRY.SUPP_HIER_LVL_3%TYPE;
   L_pallet_name            ITEM_SUPPLIER.PALLET_NAME%TYPE;
   L_case_name              ITEM_SUPPLIER.CASE_NAME%TYPE;
   L_inner_name             ITEM_SUPPLIER.INNER_NAME%TYPE;
   L_direct_ship_ind        ITEM_SUPPLIER.DIRECT_SHIP_IND%TYPE;
   L_primary_supplier       SIMPLE_PACK_TEMP.PRIMARY_SUPP%TYPE;
   L_primary_country_id     SIMPLE_PACK_TEMP.PRIMARY_CNTRY_ID%TYPE;
   
   L_curr_selling_unit_retail ITEM_MASTER.CURR_SELLING_UNIT_RETAIL%TYPE;
   L_curr_selling_uom         ITEM_MASTER.CURR_SELLING_UOM%TYPE; 
   L_rpm_ind                  varchar2(1):=NULL;
   cursor C_RPM_IND is 
      select RPM_IND
	 from system_options; 

   cursor C_GET_SYSTEM_INFO is
      select user,
             sysdate
        from dual;

   cursor C_GET_PARENT is
      select item_parent
        from item_master
       where item = I_item;

   cursor C_ITEM is
      select item_aggregate_ind,
             diff_1_aggregate_ind,
             diff_2_aggregate_ind,
             diff_3_aggregate_ind,
             diff_4_aggregate_ind,
             dept,
             class,
             subclass,
             primary_ref_item_ind,
             cost_zone_group_id,
             merchandise_ind,
             catch_weight_ind,
             store_ord_mult,
             handling_temp,
             handling_sensitivity,
             inventory_ind,
             product_classification,
             brand_name,
			 curr_selling_unit_retail,
			 curr_selling_uom
        from item_master
       where item = I_item;

   cursor C_SIMPLE_PACK is
      select spt.pack_no,
             spt.item_number_type,
             spt.pack_desc,
             spt.pack_desc_secondary,
             spt.item_qty,
             spt.primary_supp,
             spt.primary_cntry_id,
             spt.primary_manu_ctry_id,
             spt.unit_cost,
             spt.const_dimen_ind,
             spt.sellable_ind,
             spt.unit_retail,
             spt.vpn,
             spt.supp_label,
             spt.supp_pack_size,
             spt.ti,
             spt.hi,
             spt.case_length,
             spt.case_width,
             spt.case_height,
             spt.case_lwh_uom,
             spt.case_liquid_volume,
             spt.case_liquid_volume_uom,
             spt.case_weight,
             spt.case_net_weight,
             spt.case_tare_weight,
             spt.case_weight_uom,
             spt.case_tare_type,
             spt.case_stat_cube,
             spt.pallet_length,
             spt.pallet_width,
             spt.pallet_height,
             spt.pallet_lwh_uom,
             spt.pallet_liquid_volume,
             spt.pallet_liquid_volume_uom,
             spt.pallet_weight,
             spt.pallet_net_weight,
             spt.pallet_tare_weight,
             spt.pallet_weight_uom,
             spt.pallet_tare_type,
             spt.pallet_stat_cube,
             s.currency_code,
             spt.tolerance_type,
             spt.max_tolerance,
             spt.min_tolerance,
             spt.cost_uom,
             spt.order_type,
             spt.sale_type,
             spt.notional_pack_ind,
             spt.soh_inquiry_at_pack_ind,
             spt.delivery_country_id,
             spt.default_loc,
             spt.nic_static_ind
        from simple_pack_temp spt,
             sups s
       where spt.item         = I_item
         and spt.exists_ind   = 'N'
         and spt.primary_supp = s.supplier;

   cursor C_SUPPLIERS is
      select s.supplier,
             s.currency_code
        from item_supplier its,
             sups s
       where its.item      = L_pack_no
         and its.supplier  = s.supplier;

   cursor C_SUPP_CTRY(L_supp VARCHAR2) is
      select its.supplier,
             its.origin_country_id,
             ich.delivery_country_id,
             ich.prim_dlvy_ctry_ind
        from item_supp_country its,
             item_cost_head ich
       where its.item              = I_item
         and its.supplier          = L_supp
         and ich.origin_country_id = its.origin_country_id
         and ich.supplier          = its.supplier
         and ich.item              = its.item;

   cursor C_WH is
      select il.loc,
             il.primary_supp,
             il.primary_cntry,
             il.taxable_ind,
             il.store_ord_mult,
             w.currency_code,
             w.finisher_ind,
             il.inbound_handling_days,
             il.ranged_ind
        from item_loc il,
             wh w
       where il.loc         = w.wh
         and il.item        = I_item
         and il.loc_type    = 'W'
         and w.finisher_ind = 'N';


   cursor C_STORE is
      select il.loc,
             il.primary_supp,
             il.primary_cntry,
             il.taxable_ind,
             il.store_ord_mult,
             s.currency_code,
             il.inbound_handling_days,
             il.source_method,
             il.source_wh,
             il.uin_type,
             il.uin_label,
             il.capture_time,
             il.ext_uin_ind,
             il.ranged_ind,
             il.costing_loc,
             il.costing_loc_type
        from item_loc il,
             store s
       where il.loc       = s.store
         and il.item      = I_item
         and il.loc_type  = 'S';

   cursor C_GET_DEFAULT_PCK_METHOD is
    select default_packing_method
      from system_options;

   cursor C_INTERNAL_FIN is
      select il.loc,
             il.primary_supp,
             il.primary_cntry,
             il.taxable_ind,
             il.store_ord_mult,
             w.currency_code,
             w.finisher_ind,
             w.inbound_handling_days
        from item_loc il,
             wh w
       where il.loc      = w.wh
         and il.item     = I_item
         and il.loc_type = 'W'
         and w.finisher_ind = 'Y';

   cursor C_EXTERNAL_FIN is
      select il.loc,
             il.primary_supp,
             il.primary_cntry,
             il.taxable_ind,
             il.store_ord_mult,
             p.currency_code,
             il.inbound_handling_days
        from item_loc il,
             partner p
       where il.loc         = p.partner_id
         and il.item        = I_item
         and il.loc_type    = 'E'
         and p.partner_type = 'E';

   cursor C_LOCS is
      select distinct iscl.loc
        from item_supp_country_loc iscl
       where iscl.item = I_item
         and iscl.supplier = L_supplier
         and iscl.loc_type = L_loc_type;

   cursor C_GET_ITEM_SUPP_INFO is
      select isc.lead_time,
             isc.pickup_lead_time,
             isc.round_lvl,
             isc.round_to_inner_pct,
             isc.round_to_case_pct,
             isc.round_to_layer_pct,
             isc.round_to_pallet_pct,
             isc.supp_hier_type_1,
             isc.supp_hier_lvl_1,
             isc.supp_hier_type_2,
             isc.supp_hier_lvl_2,
             isc.supp_hier_type_3,
             isc.supp_hier_lvl_3
        from item_supp_country isc
       where isc.item              = I_item
         and isc.supplier          = L_primary_supplier
         and isc.origin_country_id = L_primary_country_id;


BEGIN

   O_exists := FALSE;

   open C_GET_SYSTEM_INFO;
   fetch C_GET_SYSTEM_INFO into L_user_id,
                                L_sysdate;
   close C_GET_SYSTEM_INFO;

   ---
   -- If the passed in primary currency is NULL, retrieve it.
   ---
   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   L_curr_prim := NVL(I_curr_prim, L_system_options_rec.currency_code);
   ---
   -- Retrieve information for the component item that will be copied to the simple packs
   -- being built.
   ---
   open C_ITEM;
   fetch C_ITEM into L_item_aggregate_ind,
                     L_diff_1_aggregate_ind,
                     L_diff_2_aggregate_ind,
                     L_diff_3_aggregate_ind,
                     L_diff_4_aggregate_ind,
                     L_dept,
                     L_class,
                     L_subclass,
                     L_primary_ref_item_ind,
                     L_cost_zone_group_id,
                     L_merch_ind,
                     L_catch_weight_ind,
                     L_store_ord_mult,
                     L_handling_temp,
                     L_handling_sensitivity,
                     L_inventory_ind,
                     L_product_classification,
                     L_brand_name,
					 L_curr_selling_unit_retail,
					 L_curr_selling_uom;

   if C_ITEM%NOTFOUND then
      close C_ITEM;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEM', NULL, NULL, NULL);
      return FALSE;
   end if;

   close C_ITEM;
   ---
   ---
   -- Loop through all the simple packs on the SIMPLE_PACK_TEMP table that need to be built.
   ---

   FOR C_REC in C_SIMPLE_PACK LOOP

      L_primary_supplier     := C_REC.primary_supp;
      L_primary_country_id   := C_REC.primary_cntry_id;

      open C_GET_ITEM_SUPP_INFO;
      fetch C_GET_ITEM_SUPP_INFO into L_lead_time,
                                      L_pickup_lead_time,
                                      L_round_lvl,
                                      L_round_to_inner_pct,
                                      L_round_to_case_pct,
                                      L_round_to_layer_pct,
                                      L_round_to_pallet_pct,
                                      L_supp_hier_type_1,
                                      L_supp_hier_lvl_1,
                                      L_supp_hier_type_2,
                                      L_supp_hier_lvl_2,
                                      L_supp_hier_type_3,
                                      L_supp_hier_lvl_3;
      close C_GET_ITEM_SUPP_INFO;

      L_pack_no      := C_REC.pack_no;
      L_primary_supp := C_REC.primary_supp;
      ---
      -- Check if the pack number already exists for another item. If it does, exit the function
      -- passing out the pack number that failed validation. This can happen if the item number
      -- for a pack being built was used for another item before the pack was actually built.
      ---

      if not ITEM_VALIDATE_SQL.EXIST(O_error_message,
                                     O_exists,
                                     C_REC.pack_no) then
         return FALSE;
      end if;

      if O_exists then
         O_exists_item := C_REC.pack_no;
         return TRUE;
      else
         O_error_message := NULL;
      end if;
      ---
      -- Get the item_parent for the item/s being placed in the pack.
      ---
      open  C_GET_PARENT;
      fetch C_GET_PARENT into L_item_parent;
      close C_GET_PARENT;
      ---

      -- Insert a record for the pack item into the ITEM_MASTER table using the appropriate
      -- component item information.
      ---
      insert into item_master (item,
                               item_number_type,
                               item_parent,
                               item_grandparent,
                               pack_ind,
                               item_level,
                               tran_level,
                               item_aggregate_ind,
                               diff_1_aggregate_ind,
                               diff_2_aggregate_ind,
                               diff_3_aggregate_ind,
                               diff_4_aggregate_ind,
                               diff_1,
                               diff_2,
                               dept,
                               class,
                               subclass,
                               status,
                               item_desc,
                               item_desc_secondary,
                               short_desc,
                               desc_up,
                               primary_ref_item_ind,
                               cost_zone_group_id,
                               standard_uom,
                               uom_conv_factor,
                               package_size,
                               package_uom,
                               merchandise_ind,
                               store_ord_mult,
                               forecast_ind,
                               original_retail,
                               mfg_rec_retail,
                               retail_label_type,
                               retail_label_value,
                               handling_temp,
                               handling_sensitivity,
                               catch_weight_ind,
                               waste_type,
                               waste_pct,
                               default_waste_pct,
                               const_dimen_ind,
                               simple_pack_ind,
                               contains_inner_ind,
                               sellable_ind,
                               orderable_ind,
                               pack_type,
                               order_as_type,
                               comments,
                               gift_wrap_ind,
                               ship_alone_ind,
                               check_uda_ind,
                               create_datetime,
                               last_update_datetime,
                               last_update_id,
                               order_type,
                               sale_type ,
                               item_xform_ind,
                               inventory_ind,
                               aip_case_type,
                               perishable_ind,
                               notional_pack_ind,
                               soh_inquiry_at_pack_ind,
                               product_classification,
                               brand_name,
							   curr_selling_unit_retail,
							   curr_selling_uom)
                       values (C_REC.pack_no,
                               C_REC.item_number_type,
                               NULL,               -- item parent
                               NULL,               -- item grandparent
                               'Y',                -- pack ind
                               1,                  -- item level
                               1,                  -- transaction level
                               L_item_aggregate_ind,
                               L_diff_1_aggregate_ind,
                               L_diff_2_aggregate_ind,
                               L_diff_3_aggregate_ind,
                               L_diff_4_aggregate_ind,
                               NULL,               -- diff 1
                               NULL,               -- diff 2
                               L_dept,
                               L_class,
                               L_subclass,
                               'W',                -- status
                               C_REC.pack_desc,
                               C_REC.pack_desc_secondary,
                               rtrim(substrb(C_REC.pack_desc, 1, 120)),
                               upper(C_REC.pack_desc),
                               L_primary_ref_item_ind,
                               L_cost_zone_group_id,
                               'EA',               -- standard UOM
                               NULL,               -- UOM conversion factor
                               NULL,               -- package size,
                               NULL,               -- package UOM,
                               L_merch_ind,
                               L_store_ord_mult,
                               'N',                -- forecast ind
                               C_REC.unit_retail,  -- original retail
                               C_REC.unit_retail,  -- mfg rec retail
                               NULL,               -- retail label type
                               NULL,               -- retail label value
                               L_handling_temp,
                               L_handling_sensitivity,
                               L_catch_weight_ind,
                               NULL,               -- waste type
                               NULL,               -- waste pct
                               NULL,               -- default waste pct
                               C_REC.const_dimen_ind,
                               'Y',                -- simple pack ind
                               'N',                -- contains inner ind
                               C_REC.sellable_ind,
                               'Y',                -- orderable ind
                               'V',                -- pack type
                               NULL,               -- order as type
                               NULL,               -- comments
                               'N',
                               'N',
                               I_uda,              -- check_uda_ind
                               L_sysdate,
                               L_sysdate,
                               L_user_id,
                               C_REC.order_type,
                               C_REC.sale_type,
                               I_item_xform_ind,
                               L_inventory_ind,
                               NULL,
                               'N',                --perishable_ind
                               C_REC.notional_pack_ind,
                               C_REC.soh_inquiry_at_pack_ind,
                               L_product_classification,
                               L_brand_name,
							   L_curr_selling_unit_retail,
							   L_curr_selling_uom);

      ---
      -- Insert record(s) for the pack item into the ITEM_COUNTRY table.
      ---

      insert into item_country (item,
                                country_id)
                         select C_REC.pack_no,
                                country_id
                           from item_country
                          where item = I_item;

      ---
      -- Insert a record for the pack item into the PACKITEM table.
      ---

      insert into packitem (pack_no,
                            seq_no,
                            item,
                            item_parent,
                            pack_tmpl_id,
                            pack_qty,
                            create_datetime,
                            last_update_datetime,
                            last_update_id)
                    values (C_REC.pack_no,
                            1,             -- seq no
                            I_item,
                            L_item_parent,
                            NULL,          -- pack tmpl id
                            C_REC.item_qty,
                            L_sysdate,
                            L_sysdate,
                            L_user_id);

      ---
      -- Insert a record for the pack item into the PACKITEM_BREAKOUT table.
      ---
      insert into packitem_breakout (pack_no,
                                     seq_no,
                                     item,
                                     item_parent,
                                     pack_tmpl_id,
                                     comp_pack_no,
                                     item_qty,
                                     item_parent_pt_qty,
                                     comp_pack_qty,
                                     pack_item_qty,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id)
                             values (C_REC.pack_no,
                                     1,            -- seq no
                                     I_item,
                                     NULL,         -- item parent
                                     NULL,         -- pack tmpl id
                                     NULL,         -- comp pack no
                                     C_REC.item_qty,         -- item qty
                                     NULL,         -- item parent pt qty
                                     NULL,         -- comp pack qty
                                     C_REC.item_qty,
                                     L_sysdate,
                                     L_sysdate,
                                     L_user_id);

      ---
      -- Insert the primary supplier record into the ITEM_SUPPLIER table for the pack,
      -- copying the pallet, case and inner names from the component item.
      ---
      insert into item_supplier (item,
                                 supplier,
                                 primary_supp_ind,
                                 vpn,
                                 supp_label,
                                 consignment_rate,
                                 supp_diff_1,
                                 supp_diff_2,
                                 pallet_name,
                                 case_name,
                                 inner_name,
                                 supp_discontinue_date,
                                 direct_ship_ind,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id,
                                 primary_case_size)
                          select C_REC.pack_no,
                                 C_REC.primary_supp,
                                 'Y',           -- primary supp ind
                                 C_REC.vpn,
                                 C_REC.supp_label,
                                 NULL,          -- consignment rate
                                 NULL,          -- supp diff 1
                                 NULL,          -- supp diff 2
                                 pallet_name,
                                 case_name,
                                 inner_name,
                                 NULL,          -- supp discontinue dat
                                 direct_ship_ind,
                                 L_sysdate,
                                 L_sysdate,
                                 L_user_id,
                                 NULL
                            from item_supplier
                           where item     = I_item
                             and supplier = C_REC.primary_supp;

      ---
      -- Insert the primary supplier/origin country record into the ITEM_SUPP_COUNTRY table
      -- for the pack, copying the lead time and supplier hierarchy information
      -- from the component item.
      ---

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_DEFAULT_PCK_METHOD',
                       'SYSTEM_OPTIONS',
                       NULL);
      open C_GET_DEFAULT_PCK_METHOD;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_DEFAULT_PCK_METHOD',
                       'SYSTEM_OPTIONS',
                       NULL);
      fetch C_GET_DEFAULT_PCK_METHOD into L_default_packing_method;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_DEFAULT_PCK_METHOD',
                       'SYSTEM_OPTIONS',
                       NULL);
      close C_GET_DEFAULT_PCK_METHOD;
      ---

      if L_default_packing_method is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('ERROR_SYS_OPTION',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      ---
      ---
      -- If the copy VAT indicator is set to 'Y'es, copy the VAT info from the component
      -- item to the pack item.
      ---

      if I_vat = 'Y' then
         L_tax_info_rec := OBJ_TAX_INFO_REC();
         L_tax_info_rec.item := C_rec.pack_no;
         L_tax_info_rec.item_parent := I_item;
         L_tax_info_tbl.DELETE;
         L_tax_info_tbl.EXTEND;
         L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
         ---
         if L_system_options_rec.default_tax_type = 'SVAT' then
             if TAX_SQL.GET_TAX_INFO(O_error_message,
                                    L_tax_info_tbl) = FALSE then
               return FALSE;
            end if;
            ---
            if TAX_SQL.INSERT_UPDATE_TAX_SKU(O_error_message,
                                             L_run_report,
                                             L_tax_info_tbl) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---

      if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                 L_country_attrib_row,
                                                 NULL,
                                                 C_REC.delivery_country_id) = FALSE and L_system_options_rec.default_tax_type = 'GTAX' then
         return FALSE;
      end if;
      ---
      ---
      if C_REC.default_loc is NOT NULL then
         if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                         L_default_loc_type,
                                         C_REC.default_loc) = FALSE then
            return FALSE;
         end if;
      end if;
      ---

      L_item_add_rec.item                 := C_REC.pack_no;
      L_item_add_rec.supplier             := C_REC.primary_supp;
      L_item_add_rec.origin_country       := C_REC.primary_cntry_id;
      L_item_add_rec.vpn                  := NULL;
      L_item_add_rec.consignment_rate     := NULL;
      L_item_add_rec.pallet_name          := NULL;
      L_item_add_rec.case_name            := NULL;
      L_item_add_rec.inner_name           := NULL;
      L_item_add_rec.unit_cost            := C_REC.unit_cost;
      L_item_add_rec.lead_time            := L_lead_time;
      L_item_add_rec.supp_pack_size       := C_REC.supp_pack_size;
      L_item_add_rec.inner_pack_size      := 1;
      L_item_add_rec.round_lvl            := L_round_lvl;
      L_item_add_rec.round_to_inner_pct   := L_round_to_inner_pct;
      L_item_add_rec.round_to_case_pct    := L_round_to_case_pct;
      L_item_add_rec.round_to_layer_pct   := L_round_to_layer_pct;
      L_item_add_rec.round_to_pallet_pct  := L_round_to_pallet_pct;
      L_item_add_rec.packing_method       := L_default_packing_method;
      L_item_add_rec.default_uop          := 'EA';
      L_item_add_rec.ti                   := C_REC.ti;
      L_item_add_rec.hi                   := C_REC.hi;
      L_item_add_rec.length               := NULL;
      L_item_add_rec.width                := NULL;
      L_item_add_rec.height               := NULL;
      L_item_add_rec.lwh_uom              := NULL;
      L_item_add_rec.weight               := NULL;
      L_item_add_rec.weight_uom           := NULL;
      L_item_add_rec.primary_case_size    := NULL;
      L_item_add_rec.tolerance_type       := C_REC.tolerance_type;
      L_item_add_rec.max_tolerance        := C_REC.max_tolerance;
      L_item_add_rec.min_tolerance        := C_REC.min_tolerance;
      L_item_add_rec.cost_uom             := C_REC.cost_uom;


      if (L_system_options_rec.default_tax_type = 'GTAX' and I_edi_ind = 'N') then

         if L_country_attrib_row.item_cost_tax_incl_ind = 'Y' then
            L_negotiated_item_cost := C_REC.unit_cost;
         else
            L_base_cost := C_REC.unit_cost;
         end if;
         ---
         if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                             L_base_cost,
                                             L_extended_base_cost,
                                             L_inclusive_cost,
                                             L_negotiated_item_cost,
                                             C_REC.pack_no,
                                             C_REC.nic_static_ind,
                                             C_REC.primary_supp,
                                             C_REC.default_loc,
                                             L_default_loc_type,
                                             'ITEMSUPPCTRY',
                                             C_REC.primary_cntry_id,
                                             C_REC.delivery_country_id,
                                             'Y',  --I_prim_dlvy_ctry_ind
                                             'Y',  --I_update_itemcost_ind
                                             'N',
                                             'Y',  -- insert to item supp country ind
                                             L_item_add_rec) = FALSE then
            return FALSE;
         end if;
      else
         if (L_system_options_rec.default_tax_type in  ('SVAT', 'SALES')) then
            L_base_cost             := NULL;
            L_negotiated_item_cost  := NULL;
            L_extended_base_cost    := NULL;
            L_inclusive_cost        := NULL;
         end if;

         insert into item_supp_country (item,
                                        supplier,
                                        origin_country_id,
                                        unit_cost,
                                        lead_time,
                                        pickup_lead_time,
                                        supp_pack_size,
                                        inner_pack_size,
                                        round_lvl,
                                        round_to_inner_pct,
                                        round_to_case_pct,
                                        round_to_layer_pct,
                                        round_to_pallet_pct,
                                        min_order_qty,
                                        max_order_qty,
                                        packing_method,
                                        primary_supp_ind,
                                        primary_country_ind,
                                        default_uop,
                                        ti,
                                        hi,
                                        supp_hier_type_1,
                                        supp_hier_lvl_1,
                                        supp_hier_type_2,
                                        supp_hier_lvl_2,
                                        supp_hier_type_3,
                                        supp_hier_lvl_3,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        cost_uom,
                                        tolerance_type,
                                        max_tolerance,
                                        min_tolerance,
                                        negotiated_item_cost,
                                        extended_base_cost,
                                        inclusive_cost,
                                        base_cost)
                                 select C_REC.pack_no,
                                        C_REC.primary_supp,
                                        C_REC.primary_cntry_id,
                                        C_REC.unit_cost,
                                        lead_time,
                                        pickup_lead_time,
                                        C_REC.supp_pack_size,
                                        1,                 -- inner pack size
                                        round_lvl,
                                        round_to_inner_pct,
                                        round_to_case_pct,
                                        round_to_layer_pct,
                                        round_to_pallet_pct,
                                        NULL,                     -- min order qty
                                        NULL,                     -- max order qty
                                        L_default_packing_method, -- packing method
                                        'Y',                      -- primary supp ind
                                        'Y',                      -- primary country ind
                                        'EA',                     -- default UOP
                                        C_REC.ti,
                                        C_REC.hi,
                                        supp_hier_type_1,
                                        supp_hier_lvl_1,
                                        supp_hier_type_2,
                                        supp_hier_lvl_2,
                                        supp_hier_type_3,
                                        supp_hier_lvl_3,
                                        L_sysdate,
                                        L_sysdate,
                                        L_user_id,
                                        C_REC.cost_uom,
                                        C_REC.tolerance_type,
                                        C_REC.max_tolerance,
                                        C_REC.min_tolerance,
                                        L_negotiated_item_cost,
                                        L_extended_base_cost,
                                        L_inclusive_cost,
                                        L_base_cost
                                   from item_supp_country
                                  where item              = I_item
                                    and supplier          = C_REC.primary_supp
                                    and origin_country_id = C_REC.primary_cntry_id;

      end if;
      ---
      ---
      -- Insert the primary supplier/manu country record into the ITEM_SUPP_MANU_COUNTRY table
      -- for the pack, copying information from the component item.
      ---
      insert into item_supp_manu_country (item,
                                          supplier,
                                          manu_country_id,
                                          primary_manu_ctry_ind)
                                  select C_REC.pack_no,
                                         C_REC.primary_supp,
                                         C_REC.primary_manu_ctry_id,
                                         'Y'
                                    from item_supp_manu_country
                                   where item              = I_item
                                     and supplier          = C_REC.primary_supp
                                     and manu_country_id   = C_REC.primary_manu_ctry_id;

      ---
      -- Insert the primary supplier/origin country/bracket record into the
      -- ITEM_SUPP_COUNTRY_BRACKET_COST table for the pack, copying the information
      -- from the component item.
      ---
         insert into item_supp_country_bracket_cost (item,
                                                     supplier,
                                                     origin_country_id,
                                                     location,
                                                     bracket_value1,
                                                     loc_type,
                                                     default_bracket_ind,
                                                     unit_cost,
                                                     bracket_value2,
                                                     sup_dept_seq_no)
                                              select C_REC.pack_no,
                                                     supplier,
                                                     origin_country_id,
                                                     location,
                                                     bracket_value1,
                                                     loc_type,
                                                     default_bracket_ind,
                                                     C_REC.unit_cost,
                                                     bracket_value2,
                                                     sup_dept_seq_no
                                                from item_supp_country_bracket_cost
                                               where location is NULL
                                                 and origin_country_id = C_REC.primary_cntry_id
                                                 and supplier = C_REC.primary_supp
                                                 and item     = I_item;

      ---
      -- If the copy supplier info indicator is set to 'Y'es, copy the item supplier and item
      -- supplier origin country information from the component item to the pack item (not
      -- including the supplier/origin country info perviously created, above).
      ---
      if I_supplier = 'Y' then

         insert into item_supplier (item,
                                    supplier,
                                    primary_supp_ind,
                                    vpn,
                                    supp_label,
                                    consignment_rate,
                                    supp_diff_1,
                                    supp_diff_2,
                                    pallet_name,
                                    case_name,
                                    inner_name,
                                    supp_discontinue_date,
                                    direct_ship_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id,
                                    primary_case_size)
                             select C_REC.pack_no,
                                    supplier,
                                    'N',
                                    NULL,    -- VPN
                                    NULL,    -- supp label
                                    NULL,    -- consignment rate
                                    NULL,    -- supp diff 1
                                    NULL,    -- supp diff 2
                                    pallet_name,
                                    case_name,
                                    inner_name,
                                    NULL,    -- sup discontinue date
                                    direct_ship_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id,
                                    NULL
                               from item_supplier
                              where item      = I_item
                                and supplier != C_REC.primary_supp;

         FOR C_SUPP_REC in C_SUPPLIERS LOOP

            ---
            -- If the currency of the current supplier being processed is different than
            -- the currency of the primary supplier for the pack, the unit cost of the pack
            -- needs to be converted to the currency of the current supplier.
            ---
            if C_REC.currency_code != C_SUPP_REC.currency_code then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           C_REC.unit_cost,
                                           C_REC.currency_code,
                                           C_SUPP_REC.currency_code,
                                           L_unit_cost_supp,
                                           'C',
                                           L_vdate,
                                           NULL,
                                           NULL,
                                           NULL) then
                    return FALSE;
               end if;
            else
               L_unit_cost_supp := C_REC.unit_cost;
            end if;

            if L_system_options_rec.default_tax_type = 'GTAX' then

               if L_country_attrib_row.item_cost_tax_incl_ind = 'Y' then
                  L_negotiated_item_cost := L_unit_cost_supp;

               else
                  -- Setting L_negotiated_item_cost back to NULL. If L_negotiated_item_cost is not null, it gives incorrect value to L_extended_base_cost and other values.
                  L_negotiated_item_cost := NULL;
                  L_base_cost := L_unit_cost_supp;
               end if;


               ---
               -- Update the supplier on L_item_add_rec to the supplier being processed

             FOR C_SUPP_CTRY_REC in C_SUPP_CTRY(C_SUPP_REC.supplier) LOOP
               L_item_add_rec.supplier             := C_SUPP_CTRY_REC.supplier;
               L_item_add_rec.origin_country       := C_SUPP_CTRY_REC.origin_country_id;
               L_item_add_rec.unit_cost            := L_unit_cost_supp;

               if ITEM_COST_SQL.COMPUTE_ITEM_COST (O_error_message,
                                                   L_base_cost,
                                                   L_extended_base_cost,
                                                   L_inclusive_cost,
                                                   L_negotiated_item_cost,
                                                   C_REC.pack_no,
                                                   C_REC.nic_static_ind,
                                                   C_SUPP_CTRY_REC.supplier,
                                                   C_REC.default_loc,
                                                   L_default_loc_type,
                                                   'ITEMSUPPCTRY',
                                                   C_SUPP_CTRY_REC.origin_country_id,
                                                   C_SUPP_CTRY_REC.delivery_country_id,
                                                   C_SUPP_CTRY_REC.prim_dlvy_ctry_ind,
                                                   'Y',  --I_update_itemcost_ind
                                                   'N',
                                                   'Y',  -- Insert to item supp country indicator
                                                   L_item_add_rec) = FALSE then
                  return FALSE;
               end if;
             END LOOP;
            else
               if (L_system_options_rec.default_tax_type in ('SVAT', 'SALES') ) then
                   L_base_cost             := NULL;
                   L_negotiated_item_cost  := NULL;
                   L_extended_base_cost    := NULL;
                   L_inclusive_cost        := NULL;
               end if;
               ---
               insert into item_supp_country (item,
                                              supplier,
                                              origin_country_id,
                                              unit_cost,
                                              lead_time,
                                              pickup_lead_time,
                                              supp_pack_size,
                                              inner_pack_size,
                                              round_lvl,
                                              round_to_inner_pct,
                                              round_to_case_pct,
                                              round_to_layer_pct,
                                              round_to_pallet_pct,
                                              min_order_qty,
                                              max_order_qty,
                                              packing_method,
                                              primary_supp_ind,
                                              primary_country_ind,
                                              default_uop,
                                              ti,
                                              hi,
                                              supp_hier_type_1,
                                              supp_hier_lvl_1,
                                              supp_hier_type_2,
                                              supp_hier_lvl_2,
                                              supp_hier_type_3,
                                              supp_hier_lvl_3,
                                              create_datetime,
                                              last_update_datetime,
                                              last_update_id,
                                              cost_uom,
                                              tolerance_type,
                                              max_tolerance,
                                              min_tolerance,
                                              negotiated_item_cost,
                                              extended_base_cost,
                                              inclusive_cost,
                                              base_cost)
                                       select C_REC.pack_no,
                                              supplier,
                                              origin_country_id,
                                              L_unit_cost_supp,
                                              lead_time,
                                              pickup_lead_time,
                                              C_REC.supp_pack_size,
                                              1,                 -- inner pack size
                                              round_lvl,
                                              round_to_inner_pct,
                                              round_to_case_pct,
                                              round_to_layer_pct,
                                              round_to_pallet_pct,
                                              NULL,              -- min order qty
                                              NULL,              -- max order qty
                                              NULL,              -- packing method
                                              decode(supplier,
                                                     C_REC.primary_supp, 'Y',
                                                     'N'),
                                              decode(supplier,
                                                     C_REC.primary_supp, 'N',
                                                     primary_country_ind),
                                              'EA',              -- default UOP
                                              C_REC.ti,
                                              C_REC.hi,
                                              supp_hier_type_1,
                                              supp_hier_lvl_1,
                                              supp_hier_type_2,
                                              supp_hier_lvl_2,
                                              supp_hier_type_3,
                                              supp_hier_lvl_3,
                                              L_sysdate,
                                              L_sysdate,
                                              L_user_id,
                                              C_REC.cost_uom,
                                              C_REC.tolerance_type,
                                              C_REC.max_tolerance,
                                              C_REC.min_tolerance,
                                              L_negotiated_item_cost,
                                              L_extended_base_cost,
                                              L_inclusive_cost,
                                              L_base_cost
                                         from item_supp_country
                                        where item               = I_item
                                          and supplier           = C_SUPP_REC.supplier
                                          and (supplier, origin_country_id) not in
                                                                 (select C_REC.primary_supp,
                                                                         C_REC.primary_cntry_id
                                                                         from dual);

            end if;
            ---

            insert into item_supp_manu_country(item,
                                               supplier,
                                               manu_country_id,
                                               primary_manu_ctry_ind)
                                        select C_REC.pack_no,
                                               supplier,
                                               manu_country_id,
                                               decode(supplier,
                                                      C_REC.primary_supp, 'N',
                                                                          primary_manu_ctry_ind)
                                          from item_supp_manu_country
                                         where item              = I_item
                                           and supplier          = C_SUPP_REC.supplier
                                           and (supplier, manu_country_id) not in
                                                              (select C_REC.primary_supp,
                                                                      C_REC.primary_manu_ctry_id
                                                                 from dual);

           insert into item_supp_country_bracket_cost (item,
                                                        supplier,
                                                        origin_country_id,
                                                        location,
                                                        bracket_value1,
                                                        loc_type,
                                                        default_bracket_ind,
                                                        unit_cost,
                                                        bracket_value2,
                                                        sup_dept_seq_no)
                                                 select C_REC.pack_no,
                                                        supplier,
                                                        origin_country_id,
                                                        location,
                                                        bracket_value1,
                                                        loc_type,
                                                        default_bracket_ind,
                                                        L_unit_cost_supp,
                                                        bracket_value2,
                                                        sup_dept_seq_no
                                                   from item_supp_country_bracket_cost
                                                  where location is NULL
                                                    and (supplier, origin_country_id) NOT in
                                                                  (select C_REC.primary_supp,
                                                                          C_REC.primary_cntry_id
                                                                     from dual)
                                                    and supplier = C_SUPP_REC.supplier
                                                    and item     = I_item;
         END LOOP;

      end if;  -- I_supplier = 'Y'
      ---
      -- Insert 'CA'se and 'PA'llet records into the ITEM_SUPP_COUNTRY_DIM table
      -- for all the item/supplier/origin country relationships just created for the pack item
      -- These inserts should only be performed if dimension information has been retrieved
      -- for the pack in the main cursor loop.
      ---
      if C_REC.case_lwh_uom is not NULL or C_REC.case_liquid_volume_uom is not NULL or
         C_REC.case_weight_uom is not NULL or C_REC.case_stat_cube is not NULL then
         insert into item_supp_country_dim (item,
                                            supplier,
                                            origin_country,
                                            dim_object,
                                            presentation_method,
                                            length,
                                            width,
                                            height,
                                            lwh_uom,
                                            weight,
                                            net_weight,
                                            weight_uom,
                                            liquid_volume,
                                            liquid_volume_uom,
                                            stat_cube,
                                            tare_weight,
                                            tare_type,
                                            create_datetime,
                                            last_update_datetime,
                                            last_update_id)
                                     select item,
                                            supplier,
                                            origin_country_id,
                                            'CA',              -- dim object
                                            NULL,              -- presentation method
                                            C_REC.case_length,
                                            C_REC.case_width,
                                            C_REC.case_height,
                                            C_REC.case_lwh_uom,
                                            C_REC.case_weight,
                                            C_REC.case_net_weight,
                                            C_REC.case_weight_uom,
                                            C_REC.case_liquid_volume,
                                            C_REC.case_liquid_volume_uom,
                                            C_REC.case_stat_cube,
                                            C_REC.case_tare_weight,
                                            C_REC.case_tare_type,
                                            L_sysdate,
                                            L_sysdate,
                                            L_user_id
                                       from item_supp_country
                                      where item = C_REC.pack_no;

      end if;

      if C_REC.pallet_lwh_uom is not NULL or C_REC.pallet_liquid_volume_uom is not NULL or
         C_REC.pallet_weight_uom is not NULL or C_REC.pallet_stat_cube is not NULL then
         insert into item_supp_country_dim (item,
                                            supplier,
                                            origin_country,
                                            dim_object,
                                            presentation_method,
                                            length,
                                            width,
                                            height,
                                            lwh_uom,
                                            weight,
                                            net_weight,
                                            weight_uom,
                                            liquid_volume,
                                            liquid_volume_uom,
                                            stat_cube,
                                            tare_weight,
                                            tare_type,
                                            create_datetime,
                                            last_update_datetime,
                                            last_update_id)
                                     select item,
                                            supplier,
                                            origin_country_id,
                                            'PA',              -- dim object
                                            NULL,              -- presentation method
                                            C_REC.pallet_length,
                                            C_REC.pallet_width,
                                            C_REC.pallet_height,
                                            C_REC.pallet_lwh_uom,
                                            C_REC.pallet_weight,
                                            C_REC.pallet_net_weight,
                                            C_REC.pallet_weight_uom,
                                            C_REC.pallet_liquid_volume,
                                            C_REC.pallet_liquid_volume_uom,
                                            C_REC.pallet_stat_cube,
                                            C_REC.pallet_tare_weight,
                                            C_REC.pallet_tare_type,
                                            L_sysdate,
                                            L_sysdate,
                                            L_user_id
                                       from item_supp_country
                                      where item = C_REC.pack_no;
      end if;
      ---
      -- If the simple pack is a catch weight, compute for average weight.
      ---
      if L_catch_weight_ind = 'Y' then
         L_average_weight := C_REC.case_net_weight/C_REC.supp_pack_size;
      else
         L_average_weight := NULL;
      end if;
      ---
      -- If the copy expenses indicator is set to 'Y'es, copy the expenses from the component item
      -- to the pack item.  For zone level expenses, only the expenses for the supplier's associated
      -- to the pack item should be copied.  For country level expenses, only the expenses for the
      -- supplier/origin countries associated to the pack item should be copied.
      ---
      if I_expenses = 'Y' then
         insert into item_exp_head (item,
                                    supplier,
                                    item_exp_type,
                                    item_exp_seq,
                                    origin_country_id,
                                    zone_id,
                                    lading_port,
                                    discharge_port,
                                    zone_group_id,
                                    base_exp_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select C_REC.pack_no,
                                    ieh.supplier,
                                    ieh.item_exp_type,
                                    ieh.item_exp_seq,
                                    ieh.origin_country_id,
                                    ieh.zone_id,
                                    ieh.lading_port,
                                    ieh.discharge_port,
                                    ieh.zone_group_id,
                                    ieh.base_exp_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from item_supplier its,
                                    item_exp_head ieh
                              where ieh.supplier      = its.supplier
                                and its.item          = C_REC.pack_no
                                and ieh.item_exp_type = 'Z'
                                and ieh.item          = I_item;

         insert into item_exp_head (item,
                                    supplier,
                                    item_exp_type,
                                    item_exp_seq,
                                    origin_country_id,
                                    zone_id,
                                    lading_port,
                                    discharge_port,
                                    zone_group_id,
                                    base_exp_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select C_REC.pack_no,
                                    ieh.supplier,
                                    ieh.item_exp_type,
                                    ieh.item_exp_seq,
                                    ieh.origin_country_id,
                                    ieh.zone_id,
                                    ieh.lading_port,
                                    ieh.discharge_port,
                                    ieh.zone_group_id,
                                    ieh.base_exp_ind,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from item_supp_country isc,
                                    item_exp_head ieh
                              where ieh.supplier          = isc.supplier
                                and ieh.origin_country_id = isc.origin_country_id
                                and isc.item              = C_REC.pack_no
                                and ieh.item_exp_type     = 'C'
                                and ieh.item              = I_item;

         insert into item_exp_detail (item,
                                      supplier,
                                      item_exp_type,
                                      item_exp_seq,
                                      comp_id,
                                      cvb_code,
                                      comp_rate,
                                      comp_currency,
                                      per_count,
                                      per_count_uom,
                                      est_exp_value,
                                      nom_flag_1,
                                      nom_flag_2,
                                      nom_flag_3,
                                      nom_flag_4,
                                      nom_flag_5,
                                      display_order,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id,
                                      defaulted_from,
                                      key_value_1,
                                      key_value_2)
                               select C_REC.pack_no,
                                      ied.supplier,
                                      ied.item_exp_type,
                                      ied.item_exp_seq,
                                      ied.comp_id,
                                      ied.cvb_code,
                                      ied.comp_rate,
                                      ied.comp_currency,
                                      ied.per_count,
                                      ied.per_count_uom,
                                      ied.est_exp_value,
                                      ied.nom_flag_1,
                                      ied.nom_flag_2,
                                      ied.nom_flag_3,
                                      ied.nom_flag_4,
                                      ied.nom_flag_5,
                                      ied.display_order,
                                      L_sysdate,
                                      L_sysdate,
                                      L_user_id,
                                      ied.defaulted_from,
                                      ied.key_value_1,
                                      ied.key_value_2
                                 from item_supplier its,
                                      item_exp_head ieh,
                                      item_exp_detail ied
                                where ied.item_exp_type = ieh.item_exp_type
                                  and ied.item_exp_seq  = ieh.item_exp_seq
                                  and ied.item          = ieh.item
                                  and ied.supplier      = ieh.supplier
                                  and ieh.supplier      = its.supplier
                                  and its.item          = C_REC.pack_no
                                  and ieh.item_exp_type = 'Z'
                                  and ied.item          = I_item;

         insert into item_exp_detail (item,
                                      supplier,
                                      item_exp_type,
                                      item_exp_seq,
                                      comp_id,
                                      cvb_code,
                                      comp_rate,
                                      comp_currency,
                                      per_count,
                                      per_count_uom,
                                      est_exp_value,
                                      nom_flag_1,
                                      nom_flag_2,
                                      nom_flag_3,
                                      nom_flag_4,
                                      nom_flag_5,
                                      display_order,
                                      create_datetime,
                                      last_update_datetime,
                                      last_update_id,
                                      defaulted_from,
                                      key_value_1,
                                      key_value_2)
                               select C_REC.pack_no,
                                      ied.supplier,
                                      ied.item_exp_type,
                                      ied.item_exp_seq,
                                      ied.comp_id,
                                      ied.cvb_code,
                                      ied.comp_rate,
                                      ied.comp_currency,
                                      ied.per_count,
                                      ied.per_count_uom,
                                      ied.est_exp_value,
                                      ied.nom_flag_1,
                                      ied.nom_flag_2,
                                      ied.nom_flag_3,
                                      ied.nom_flag_4,
                                      ied.nom_flag_5,
                                      ied.display_order,
                                      L_sysdate,
                                      L_sysdate,
                                      L_user_id,
                                      ied.defaulted_from,
                                      ied.key_value_1,
                                      ied.key_value_2
                                 from item_supp_country isc,
                                      item_exp_head ieh,
                                      item_exp_detail ied
                                where ied.item_exp_type     = ieh.item_exp_type
                                  and ied.item_exp_seq      = ieh.item_exp_seq
                                  and ied.item              = ieh.item
                                  and ied.supplier          = ieh.supplier
                                  and ieh.supplier          = isc.supplier
                                  and ieh.origin_country_id = isc.origin_country_id
                                  and isc.item              = C_REC.pack_no
                                  and ieh.item_exp_type     = 'C'
                                  and ied.item              = I_item;

         ---
         -- Since the cost of the pack will be different than the cost of the component item,
         -- expenses need to be recalculated.
         ---
         if not ELC_CALC_SQL.CALC_COMP(O_error_message,
                                       'IE',
                                       C_REC.pack_no,
                                       NULL,                 -- supplier
                                       NULL,                 -- item exp type
                                       NULL,                 -- item exp seq
                                       NULL,                 -- order no
                                       NULL,                 -- order seq no
                                       NULL,                 -- pack item
                                       NULL,                 -- zone ID
                                       NULL,                 -- HTS
                                       NULL,                 -- import country ID
                                       NULL,                 -- origin country ID
                                       NULL,                 -- effect from
                                       NULL) then            -- effect to
           return FALSE;
         end if;

      end if;   -- I_expenses = 'Y'
      ---
      -- If the copy HTS indicator is set to 'Y'es, copy the HTS info from the component item
      -- to the pack item.  Only the HTS info for the origin countries associated to the pack
      -- item should be copied.
      ---
      if I_hts = 'Y' then
         if L_system_options_rec.hts_tracking_level = 'S' then
            insert into item_hts (item,
                                  hts,
                                  import_country_id,
                                  origin_country_id,
                                  effect_from,
                                  effect_to,
                                  clearing_zone_id,
                                  status,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
                           select C_REC.pack_no,
                                  hts,
                                  import_country_id,
                                  origin_country_id,
                                  effect_from,
                                  effect_to,
                                  clearing_zone_id,
                                  status,
                                  L_sysdate,
                                  L_sysdate,
                                  L_user_id
                             from item_hts
                            where item = I_item
                              and origin_country_id in (select distinct origin_country_id
                                                             from item_supp_country
                                                            where item = C_REC.pack_no);

            insert into item_hts_assess (item,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         comp_id,
                                         cvb_code,
                                         comp_rate,
                                         per_count,
                                         per_count_uom,
                                         est_assess_value,
                                         nom_flag_1,
                                         nom_flag_2,
                                         nom_flag_3,
                                         nom_flag_4,
                                         nom_flag_5,
                                         display_order,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id)
                                  select C_REC.pack_no,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         comp_id,
                                         cvb_code,
                                         comp_rate,
                                         per_count,
                                         per_count_uom,
                                         est_assess_value,
                                         nom_flag_1,
                                         nom_flag_2,
                                         nom_flag_3,
                                         nom_flag_4,
                                         nom_flag_5,
                                         display_order,
                                         L_sysdate,
                                         L_sysdate,
                                         L_user_id
                                    from item_hts_assess
                                   where item = I_item
                                     and origin_country_id in (select distinct origin_country_id
                                                                 from item_supp_country
                                                                where item = C_REC.pack_no);
         else
            insert into item_hts (item,
                                  hts,
                                  import_country_id,
                                  origin_country_id,
                                  effect_from,
                                  effect_to,
                                  clearing_zone_id,
                                  status,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
                           select C_REC.pack_no,
                                  hts,
                                  import_country_id,
                                  origin_country_id,
                                  effect_from,
                                  effect_to,
                                  clearing_zone_id,
                                  status,
                                  L_sysdate,
                                  L_sysdate,
                                  L_user_id
                             from item_hts
                            where item = I_item
                              and origin_country_id in (select distinct manu_country_id
                                                             from item_supp_manu_country
                                                            where item = C_REC.pack_no);

            insert into item_hts_assess (item,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         comp_id,
                                         cvb_code,
                                         comp_rate,
                                         per_count,
                                         per_count_uom,
                                         est_assess_value,
                                         nom_flag_1,
                                         nom_flag_2,
                                         nom_flag_3,
                                         nom_flag_4,
                                         nom_flag_5,
                                         display_order,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id)
                                  select C_REC.pack_no,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         comp_id,
                                         cvb_code,
                                         comp_rate,
                                         per_count,
                                         per_count_uom,
                                         est_assess_value,
                                         nom_flag_1,
                                         nom_flag_2,
                                         nom_flag_3,
                                         nom_flag_4,
                                         nom_flag_5,
                                         display_order,
                                         L_sysdate,
                                         L_sysdate,
                                         L_user_id
                                    from item_hts_assess
                                   where item = I_item
                                     and origin_country_id in (select distinct manu_country_id
                                                                 from item_supp_manu_country
                                                                where item = C_REC.pack_no);
         end if;

         insert into cond_tariff_treatment (item,
                                            tariff_treatment,
                                            create_datetime,
                                            last_update_datetime,
                                            last_update_id)
                                     select C_REC.pack_no,
                                            tariff_treatment,
                                            L_sysdate,
                                            L_sysdate,
                                            L_user_id
                                       from cond_tariff_treatment
                                      where item = I_item;

         ---
         -- Since the cost of the pack will be different than the cost of the component item,
         -- assessments need to be recalculated.
         ---
         if not ELC_CALC_SQL.CALC_COMP(O_error_message,
                                       'IA',
                                       C_REC.pack_no,
                                       NULL,                 -- supplier
                                       NULL,                 -- item exp type
                                       NULL,                 -- item exp seq
                                       NULL,                 -- order no
                                       NULL,                 -- order seq no
                                       NULL,                 -- pack item
                                       NULL,                 -- zone ID
                                       NULL,                 -- HTS
                                       NULL,                 -- import country ID
                                       NULL,                 -- origin country ID
                                       NULL,                 -- effect from
                                       NULL) then            -- effect to
            return FALSE;
         end if;
      end if;   -- I_hts = 'Y'

      ---
      -- If both expense and HTS info has been copied, recalc the expenses one more time to ensure
      -- that the expense/assessment values are correct.
      ---
      if I_expenses = 'Y' and I_hts = 'Y' then
         if not ELC_CALC_SQL.CALC_COMP(O_error_message,
                                       'IE',
                                       C_REC.pack_no,
                                       NULL,                 -- supplier
                                       NULL,                 -- item exp type
                                       NULL,                 -- item exp seq
                                       NULL,                 -- order no
                                       NULL,                 -- order seq no
                                       NULL,                 -- pack item
                                       NULL,                 -- zone ID
                                       NULL,                 -- HTS
                                       NULL,                 -- import country ID
                                       NULL,                 -- origin country ID
                                       NULL,                 -- effect from
                                       NULL) then            -- effect to
            return FALSE;
         end if;

      end if;
      ---
      -- If the pack is a sellable pack, copy the price zone info from the main item to the pack
      -- item.  The base retail of the pack needs to be converted into each zones' respective
      -- currency.
      ---

      If L_system_options_rec.default_tax_type = 'GTAX' then
         L_unit_cost := L_extended_base_cost;
      else
         L_unit_cost := C_REC.UNIT_COST;
      end if;

      open c_rpm_ind;
      fetch c_rpm_ind into L_rpm_ind;
      close c_rpm_ind;

      -- passed in L_extended_base_cost instead of base cost to get correct selling_unit_retail value
      --if C_REC.sellable_ind = 'Y' and I_edi_ind = 'N'then
	  if C_REC.sellable_ind = 'Y' and I_edi_ind = 'N' and L_rpm_ind <> 'N' then 
         -- End of change	  
         --- Insert into RPM_ITEM_ZONE_PRICE
         if NOT SET_RPM_ITEM_ZONE_PRICE(O_error_message,
                                        C_REC.pack_no,
                                        L_dept,
                                        L_class,
                                        L_subclass,
                                        C_REC.currency_code,
                                        L_unit_cost) then
            return FALSE;
         end if;
         ---
      end if;   -- C_REC.sellable_ind = 'Y'

      ---
      -- If the copy warehouse info indicator is set to 'Y'es, copy the warehouses from the
      -- component item to the pack item.
      ---
      if I_wh = 'Y' then
         ---
         -- Loop through all the warehouses associated with the component item.  A cursor loop
         -- is required so the unit costs can be converted to the appropriate
         -- currencies before the inserts into the tables are performed.
         ---
         L_loc_type := 'W';

         FOR C_WH_REC in C_WH LOOP

            ---
            -- Convert the unit cost in the supplier currency to the location's currency.
            ---
            if C_REC.currency_code != C_WH_REC.currency_code then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           C_REC.unit_cost,
                                           C_REC.currency_code,
                                           C_WH_REC.currency_code,
                                           L_unit_cost_loc,
                                           'C',
                                           L_vdate,
                                           NULL,
                                           NULL,
                                           NULL) then
                  return FALSE;
               end if;

            else

               L_unit_cost_loc := C_REC.unit_cost;

            end if;
            ---
            -- Get the unit retail of the pack item for each warehouse
            ---
            if C_REC.sellable_ind = 'Y' then
               if not PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                                    L_unit_retail_loc,
                                                    L_selling_unit_retail_loc,
                                                    L_selling_uom_loc,
                                                    L_multi_units_loc,
                                                    L_multi_unit_retail_loc,
                                                    L_multi_selling_uom_loc,
                                                    C_REC.pack_no,
                                                    'W',
                                                    C_WH_REC.loc) then
                  return FALSE;
               end if;
            end if;

            if ITEM_LOC_SQL.CHECK_ITEM_COUNTRY(O_error_message,
                                               O_exists,
                                               C_REC.pack_no,
                                               C_WH_REC.loc,
                                               'W') = FALSE then
               return FALSE;
            end if;

            if O_exists = TRUE then
               ---
               -- Insert a record into the item location table for the warehouse being processed.
               ---
               insert into item_loc (item,
                                     loc,
                                     item_parent,
                                     item_grandparent,
                                     loc_type,
                                     unit_retail,
                                     selling_unit_retail,
                                     selling_uom,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_selling_uom,
                                     regular_unit_retail,
                                     rpm_ind,
                                     clear_ind,
                                     taxable_ind,
                                     local_item_desc,
                                     local_short_desc,
                                     ti,
                                     hi,
                                     store_ord_mult,
                                     status,
                                     status_update_date,
                                     daily_waste_pct,
                                     meas_of_each,
                                     meas_of_price,
                                     uom_of_price,
                                     primary_variant,
                                     primary_cost_pack,
                                     primary_supp,
                                     primary_cntry,
                                     receive_as_type,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     inbound_handling_days,
                                     store_price_ind,
                                     ranged_ind)
                             values (C_REC.pack_no,
                                     C_WH_REC.loc,
                                     NULL,                          -- item parent
                                     NULL,                          -- item grandparent
                                     'W',
                                     decode(C_REC.sellable_ind,
                                     'Y', L_unit_retail_loc, NULL),         -- unit retail
                                     decode(C_REC.sellable_ind, 'Y', L_selling_unit_retail_loc, NULL), -- selling unit retail
                                     decode(C_REC.sellable_ind, 'Y', L_selling_uom_loc, NULL),         -- selling unit UOM
                                     NULL,                                  -- multi_units
                                     NULL,                                  -- multi_unit_retail
                                     NULL,                                  -- multi_selling_uom
                                     decode(C_REC.sellable_ind,
                                     'Y', L_unit_retail_loc, NULL),         -- regular_unit_retail
                                     'N',                                   -- rpm_ind
                                     'N',                                   -- clearance ind
                                     decode(C_REC.sellable_ind,
                                            'Y', C_WH_REC.taxable_ind,
                                            'N'),
                                     C_REC.pack_desc,
                                     rtrim(substrb(C_REC.pack_desc, 1, 120)),
                                     C_REC.ti,
                                     C_REC.hi,
                                     C_WH_REC.store_ord_mult,
                                     'A',                           -- status,
                                     L_vdate,
                                     NULL,                          -- daily waste percent
                                     NULL,                          -- measure of each
                                     NULL,                          -- measure of price
                                     NULL,                          -- UOM of price
                                     NULL,                          -- primary variant
                                     NULL,
                                     decode(I_supplier,
                                            'Y', C_WH_REC.primary_supp,
                                            C_REC.primary_supp),
                                     decode(I_supplier,
                                            'Y', C_WH_REC.primary_cntry,
                                            C_REC.primary_cntry_id),
                                     decode(C_WH_REC.finisher_ind, 'Y', 'E', 'P'),
                                     L_sysdate,
                                     L_sysdate,
                                     L_user_id,
                                     C_WH_REC.inbound_handling_days,
                                     'N',
                                     C_WH_REC.ranged_ind);

               ---
               -- Insert the item location stock on hand records for all the warehouse locations
               -- associated to the pack.
               ---
               insert into item_loc_soh (item,
                                         item_parent,
                                         item_grandparent,
                                         loc,
                                         loc_type,
                                         av_cost,
                                         unit_cost,
                                         stock_on_hand,
                                         soh_update_datetime,
                                         last_hist_export_date,
                                         in_transit_qty,
                                         pack_comp_intran,
                                         pack_comp_soh,
                                         tsf_reserved_qty,
                                         pack_comp_resv,
                                         tsf_expected_qty,
                                         pack_comp_exp,
                                         rtv_qty,
                                         non_sellable_qty,
                                         customer_resv,
                                         customer_backorder,
                                         pack_comp_cust_resv,
                                         pack_comp_cust_back,
                                         pack_comp_non_sellable,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id,
                                         primary_supp,
                                         primary_cntry,
                                         average_weight)
                                 values (C_REC.pack_no,
                                         NULL,
                                         NULL,
                                         C_WH_REC.loc,
                                         L_loc_type,
                                         NULL,
                                         L_unit_cost_loc,
                                         0,
                                         NULL,
                                         NULL,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         L_sysdate,
                                         L_sysdate,
                                         L_user_id,
                                         DECODE(I_supplier,
                                                'Y', C_WH_REC.primary_supp,
                                                C_REC.primary_supp),
                                         DECODE(I_supplier,
                                                'Y', C_WH_REC.primary_cntry,
                                                C_REC.primary_cntry_id),
                                         L_average_weight);
            else
               O_error_message := SQL_LIB.CREATE_MSG('CANT_APPLY_LOC',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            O_exists := FALSE;

         END LOOP;    -- End of C_WH cursor loop
         ---
         FOR C_SUPP_REC in C_SUPPLIERS LOOP
            ---
            L_supplier := C_SUPP_REC.supplier;
            L_loc_type := 'W';
            ---
            -- the currency of the primary supplier for the pack, the unit cost of the pack
            -- needs to be converted to the currency of the current supplier.
            ---
            if C_REC.currency_code != C_SUPP_REC.currency_code then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           C_REC.unit_cost,
                                           C_REC.currency_code,
                                           C_SUPP_REC.currency_code,
                                           L_unit_cost_supp,
                                           'C',
                                           L_vdate,
                                           NULL,
                                           NULL,
                                           NULL) then
                  return FALSE;
               end if;
            else
               L_unit_cost_supp := C_REC.unit_cost;
            end if;
            ---
            FOR C_LOCS_REC in C_LOCS LOOP
               ---
               if L_system_options_rec.default_tax_type = 'GTAX' then
                  if C_REC.nic_static_ind = 'Y' then
                     L_neg_item_cost_iscl := L_unit_cost_supp;
                     L_base_cost_iscl     := NULL;
                  elsif C_REC.nic_static_ind  = 'N' then
                     L_base_cost_iscl     := L_unit_cost_supp;
                     L_neg_item_cost_iscl := NULL;
                  end if;
                  ---
                  if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                                  L_default_loc_type,
                                                  C_LOCS_REC.loc) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                                     L_base_cost_iscl,
                                                     L_extended_base_cost,
                                                     L_inclusive_cost,
                                                     L_neg_item_cost_iscl,
                                                     C_REC.pack_no,
                                                     C_REC.nic_static_ind,
                                                     C_SUPP_REC.supplier,
                                                     C_LOCS_REC.loc,
                                                     L_default_loc_type,
                                                     'ITEMSUPPCTRYLOC',
                                                     C_REC.primary_cntry_id,
                                                     C_REC.delivery_country_id,
                                                     'Y',  --I_prim_dlvy_ctry_ind
                                                     'N',  --I_update_itemcost_ind
                                                     'N',
                                                     'N',
                                                     NULL) = FALSE then
                     return FALSE;
                  end if;

               else
                  if (L_system_options_rec.default_tax_type in ('SVAT', 'SALES') ) then
                     L_base_cost_iscl        := NULL;
                     L_neg_item_cost_iscl    := NULL;
                     L_extended_base_cost    := NULL;
                     L_inclusive_cost        := NULL;
                  end if;
                  ---
               end if;

               ---
               -- If the currency of the current supplier being processed is different than
               -- Insert item supplier country location (warehouse) records for all the
               -- supplier/country combinations for the pack item.
               ---
               insert into item_supp_country_loc (item,
                                                  supplier,
                                                  origin_country_id,
                                                  loc,
                                                  loc_type,
                                                  primary_loc_ind,
                                                  unit_cost,
                                                  round_lvl,
                                                  round_to_inner_pct,
                                                  round_to_case_pct,
                                                  round_to_layer_pct,
                                                  round_to_pallet_pct,
                                                  supp_hier_type_1,
                                                  supp_hier_lvl_1,
                                                  supp_hier_type_2,
                                                  supp_hier_lvl_2,
                                                  supp_hier_type_3,
                                                  supp_hier_lvl_3,
                                                  pickup_lead_time,
                                                  create_datetime,
                                                  last_update_datetime,
                                                  last_update_id,
                                                  negotiated_item_cost,
                                                  extended_base_cost,
                                                  inclusive_cost,
                                                  base_cost)
                                           select C_REC.pack_no,
                                                  supplier,
                                                  origin_country_id,
                                                  loc,
                                                  loc_type,
                                                  primary_loc_ind,
                                                  L_unit_cost_supp,
                                                  round_lvl,
                                                  round_to_inner_pct,
                                                  round_to_case_pct,
                                                  round_to_layer_pct,
                                                  round_to_pallet_pct,
                                                  supp_hier_type_1,
                                                  supp_hier_lvl_1,
                                                  supp_hier_type_2,
                                                  supp_hier_lvl_2,
                                                  supp_hier_type_3,
                                                  supp_hier_lvl_3,
                                                  pickup_lead_time,
                                                  L_sysdate,
                                                  L_sysdate,
                                                  L_user_id,
                                                  L_neg_item_cost_iscl,
                                                  L_extended_base_cost,
                                                  L_inclusive_cost,
                                                  L_base_cost_iscl
                                             from item_supp_country_loc,
                                                  wh w
                                            where item = I_item
                                              and loc_type ='W'
                                              and loc = C_LOCS_REC.loc
                                              and loc = w.wh
                                              and w.finisher_ind = 'N'
                                              and supplier = C_SUPP_REC.supplier;
            END LOOP;   -- End of C_LOCS cursor
            ---
            insert into item_supp_country_bracket_cost (item,
                                                        supplier,
                                                        origin_country_id,
                                                        location,
                                                        bracket_value1,
                                                        loc_type,
                                                        default_bracket_ind,
                                                        unit_cost,
                                                        bracket_value2,
                                                        sup_dept_seq_no)
                                                 select C_REC.pack_no,
                                                        supplier,
                                                        origin_country_id,
                                                        location,
                                                        bracket_value1,
                                                        loc_type,
                                                        default_bracket_ind,
                                                        L_unit_cost_supp,
                                                        bracket_value2,
                                                        sup_dept_seq_no
                                                   from item_supp_country_bracket_cost bc,
                                                        wh w
                                                  where bc.loc_type = 'W'
                                                    and bc.location    = w.wh
                                                    and w.finisher_ind = 'N'
                                                    and bc.supplier = C_SUPP_REC.supplier
                                                    and bc.item     = I_item;
         END LOOP;   -- End of C_SUPPLIERS loop.
      end if;

      ---
      -- If the copy store info indicator is set to 'Y'es, copy the stores from the
      -- component item to the pack item.  No item location stock on hand inserts are performed
      -- for packs at stores.
      ---
      if I_store = 'Y' then
         ---
         -- Loop through all the stores associated with the component item.  A cursor loop
         -- is required so the unit costs can be converted to the appropriate
         -- currencies before the inserts into the tables are performed.
         ---
         L_loc_type := 'S';

         FOR C_STORE_REC in C_STORE LOOP
            ---
            -- Convert the unit cost in the supplier currency to the location's currency.
            ---
            if C_REC.currency_code != C_STORE_REC.currency_code then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           C_REC.unit_cost,
                                           C_REC.currency_code,
                                           C_STORE_REC.currency_code,
                                           L_unit_cost_loc,
                                           'C',
                                           L_vdate,
                                           NULL,
                                           NULL,
                                           NULL) then
                  return FALSE;
               end if;
            else

               L_unit_cost_loc := C_REC.unit_cost;

            end if;
            ---
            -- Get the unit retail of the pack item for each store
            ---
            if C_REC.sellable_ind = 'Y' then
               if not PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                                    L_unit_retail_loc,
                                                    L_selling_unit_retail_loc,
                                                    L_selling_uom_loc,
                                                    L_multi_units_loc,
                                                    L_multi_unit_retail_loc,
                                                    L_multi_selling_uom_loc,
                                                    C_REC.pack_no,
                                                    'S',
                                                    C_STORE_REC.loc) then
                  return FALSE;
               end if;
            end if;
            if ITEM_LOC_SQL.CHECK_ITEM_COUNTRY(O_error_message,
                                               O_exists,
                                               C_REC.pack_no,
                                               C_STORE_REC.loc,
                                               'S')= FALSE then
               return FALSE;
            end if;

            if O_exists = TRUE then
               ---
               -- Insert a record into the item location table for the stores being processed.
               ---
               insert into item_loc (item,
                                     loc,
                                     item_parent,
                                     item_grandparent,
                                     loc_type,
                                     unit_retail,
                                     selling_unit_retail,
                                     selling_uom,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_selling_uom,
                                     regular_unit_retail,
                                     rpm_ind,
                                     clear_ind,
                                     taxable_ind,
                                     local_item_desc,
                                     local_short_desc,
                                     ti,
                                     hi,
                                     store_ord_mult,
                                     status,
                                     status_update_date,
                                     daily_waste_pct,
                                     meas_of_each,
                                     meas_of_price,
                                     uom_of_price,
                                     primary_variant,
                                     primary_cost_pack,
                                     primary_supp,
                                     primary_cntry,
                                     receive_as_type,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     inbound_handling_days,
                                     store_price_ind,
                                     source_method,
                                     source_wh,
                                     uin_type,
                                     uin_label,
                                     capture_time,
                                     ext_uin_ind,
                                     ranged_ind,
                                     costing_loc,
                                     costing_loc_type)
                             values (C_REC.pack_no,
                                     C_STORE_REC.loc,
                                     NULL,                           -- item parent
                                     NULL,                           -- item grandparent
                                     'S',
                                     decode(C_REC.sellable_ind,
                                     'Y', L_unit_retail_loc, NULL),  -- unit retail
                                     decode(C_REC.sellable_ind,
                                     'Y', L_selling_unit_retail_loc, NULL),  -- selling unit retail
                                     decode(C_REC.sellable_ind,
                                     'Y', L_selling_uom_loc, NULL),  -- selling unit UOM
                                     NULL,                           -- multi_units
                                     NULL,                           -- multi_unit_retail
                                     NULL,                           -- multi_selling_uom
                                     decode(C_REC.sellable_ind,
                                     'Y', L_unit_retail_loc, NULL),  -- regular_unit_retail
                                     'N',                            -- rpm_ind
                                     'N',                            -- clearance ind
                                     decode(C_REC.sellable_ind,
                                            'Y', C_STORE_REC.taxable_ind,
                                            'N'),
                                     C_REC.pack_desc,
                                     rtrim(substrb(C_REC.pack_desc, 1, 120)),
                                     C_REC.ti,
                                     C_REC.hi,
                                     C_STORE_REC.store_ord_mult,
                                     'A',                            -- status,
                                      L_vdate,
                                     NULL,                           -- daily waste percent
                                     NULL,                           -- measure of each
                                     NULL,                           -- measure of price
                                     NULL,                           -- UOM of price
                                     NULL,                           -- primary variant
                                     NULL,
                                     decode(I_supplier,
                                            'Y', C_STORE_REC.primary_supp,
                                             C_REC.primary_supp),
                                     decode(I_supplier,
                                            'Y', C_STORE_REC.primary_cntry,
                                             C_REC.primary_cntry_id),
                                     'E',
                                     L_sysdate,
                                     L_sysdate,
                                     L_user_id,
                                     C_STORE_REC.inbound_handling_days,
                                    'N',
                                     C_STORE_REC.source_method,
                                     C_STORE_REC.source_wh,
                                     decode(C_REC.notional_pack_ind,
                                            'N', C_STORE_REC.uin_type,
                                            NULL),
                                     decode(C_REC.notional_pack_ind,
                                            'N', C_STORE_REC.uin_label,
                                            NULL),
                                     decode(C_REC.notional_pack_ind,
                                            'N', C_STORE_REC.capture_time,
                                            NULL),
                                     decode(C_REC.notional_pack_ind,
                                            'N', C_STORE_REC.ext_uin_ind,
                                            'N'),
                                     C_STORE_REC.ranged_ind,
                                     C_STORE_REC.costing_loc,
                                     C_STORE_REC.costing_loc_type);

               ---
               -- Insert the item location stock on hand records for all the store locations
               -- associated to the pack.
               ---
               insert into item_loc_soh (item,
                                         item_parent,
                                         item_grandparent,
                                         loc,
                                         loc_type,
                                         av_cost,
                                         unit_cost,
                                         stock_on_hand,
                                         soh_update_datetime,
                                         last_hist_export_date,
                                         in_transit_qty,
                                         pack_comp_intran,
                                         pack_comp_soh,
                                         tsf_reserved_qty,
                                         pack_comp_resv,
                                         tsf_expected_qty,
                                         pack_comp_exp,
                                         rtv_qty,
                                         non_sellable_qty,
                                         customer_resv,
                                         customer_backorder,
                                         pack_comp_cust_resv,
                                         pack_comp_cust_back,
                                         pack_comp_non_sellable,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id,
                                         primary_supp,
                                         primary_cntry,
                                         average_weight)
                                 values (C_REC.pack_no,
                                         NULL,
                                         NULL,
                                         C_STORE_REC.loc,
                                         L_loc_type,
                                         NULL,
                                         L_unit_cost_loc,
                                         0,
                                         NULL,
                                         NULL,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         L_sysdate,
                                         L_sysdate,
                                         L_user_id,
                                         DECODE(I_supplier,
                                                'Y', C_STORE_REC.primary_supp,
                                                 C_REC.primary_supp),
                                         DECODE(I_supplier,
                                                'Y', C_STORE_REC.primary_cntry,
                                                 C_REC.primary_cntry_id),
                                         L_average_weight);
            else
               O_error_message := SQL_LIB.CREATE_MSG('CANT_APPLY_LOC',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

            O_exists := FALSE;

         END LOOP;
         ---
         FOR C_SUPP_REC in C_SUPPLIERS LOOP
            ---
            L_supplier := C_SUPP_REC.supplier;
            L_loc_type := 'S';
            ---
            -- the currency of the primary supplier for the pack, the unit cost of the pack
            -- needs to be converted to the currency of the current supplier.
            ---
            if C_REC.currency_code != C_SUPP_REC.currency_code then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           C_REC.unit_cost,
                                           C_REC.currency_code,
                                           C_SUPP_REC.currency_code,
                                           L_unit_cost_supp,
                                           'C',
                                           L_vdate,
                                           NULL,
                                           NULL,
                                           NULL) then
                  return FALSE;
               end if;
            else
               L_unit_cost_supp := C_REC.unit_cost;
            end if;
            FOR C_LOCS_REC in C_LOCS LOOP
               ---
               if L_system_options_rec.default_tax_type = 'GTAX' then
                  if C_REC.nic_static_ind = 'Y' then
                     L_neg_item_cost_iscl := L_unit_cost_supp;
                     L_base_cost_iscl     := NULL;
                  elsif C_REC.nic_static_ind = 'N' then
                     L_base_cost_iscl     := L_unit_cost_supp;
                     L_neg_item_cost_iscl := NULL;
                  end if;
                  ---
                  if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                                  L_default_loc_type,
                                                  C_LOCS_REC.loc) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                                     L_base_cost_iscl,
                                                     L_extended_base_cost,
                                                     L_inclusive_cost,
                                                     L_neg_item_cost_iscl,
                                                     C_REC.pack_no,
                                                     C_REC.nic_static_ind,
                                                     C_SUPP_REC.supplier,
                                                     C_LOCS_REC.loc,
                                                     L_default_loc_type,
                                                     'ITEMSUPPCTRYLOC',
                                                     C_REC.primary_cntry_id,
                                                     C_REC.delivery_country_id,
                                                     'Y',  --I_prim_dlvy_ctry_ind
                                                     'N',  --I_update_itemcost_ind
                                                     'N',
                                                     'N',
                                                     NULL) = FALSE then
                     return FALSE;
                  end if;

               else
                  ---
                  if (L_system_options_rec.default_tax_type in ('SVAT', 'SALES')) then
                     L_base_cost_iscl        := NULL;
                     L_neg_item_cost_iscl    := NULL;
                     L_extended_base_cost    := NULL;
                     L_inclusive_cost        := NULL;
                  end if;
                  ---
               end if;
               ---
               ---
               -- Insert item supplier country location (store) records for all the
               -- supplier/country combinations for the pack item.
               ---
               insert into item_supp_country_loc (item,
                                                  supplier,
                                                  origin_country_id,
                                                  loc,
                                                  loc_type,
                                                  primary_loc_ind,
                                                  unit_cost,
                                                  round_lvl,
                                                  round_to_inner_pct,
                                                  round_to_case_pct,
                                                  round_to_layer_pct,
                                                  round_to_pallet_pct,
                                                  supp_hier_type_1,
                                                  supp_hier_lvl_1,
                                                  supp_hier_type_2,
                                                  supp_hier_lvl_2,
                                                  supp_hier_type_3,
                                                  supp_hier_lvl_3,
                                                  pickup_lead_time,
                                                  create_datetime,
                                                  last_update_datetime,
                                                  last_update_id,
                                                  extended_base_cost,
                                                  negotiated_item_cost,
                                                  inclusive_cost,
                                                  base_cost)
                                           select C_REC.pack_no,
                                                  supplier,
                                                  origin_country_id,
                                                  loc,
                                                  loc_type,
                                                  primary_loc_ind,
                                                  L_unit_cost_supp,
                                                  round_lvl,
                                                  round_to_inner_pct,
                                                  round_to_case_pct,
                                                  round_to_layer_pct,
                                                  round_to_pallet_pct,
                                                  supp_hier_type_1,
                                                  supp_hier_lvl_1,
                                                  supp_hier_type_2,
                                                  supp_hier_lvl_2,
                                                  supp_hier_type_3,
                                                  supp_hier_lvl_3,
                                                  pickup_lead_time,
                                                  L_sysdate,
                                                  L_sysdate,
                                                  L_user_id,
                                                  L_extended_base_cost,
                                                  L_neg_item_cost_iscl,
                                                  L_inclusive_cost,
                                                  L_base_cost_iscl
                                             from item_supp_country_loc
                                            where item = I_item
                                              and loc = C_LOCS_REC.loc
                                              and loc_type = 'S'
                                              and supplier = C_SUPP_REC.supplier;
            END LOOP;
         END LOOP;
      end if;   -- I_store = 'Y'

     ---
      -- If the copy seasons indicator is set to 'Y'es, copy the season info from the component
      -- item to the pack item.
      ---

      if I_seasons = 'Y' then

         insert into item_seasons (item,
                                   season_id,
                                   phase_id,
                                   item_season_seq_no,
                                   diff_id,
                                   last_update_id,
                                   last_update_datetime,
                                   create_datetime)
                            select C_REC.pack_no,
                                   season_id,
                                   phase_id,
                                   item_season_seq_no,
                                   NULL,
                                   L_user_id,
                                   L_sysdate,
                                   L_sysdate
                              from item_seasons
                             where item = I_item
                               and diff_id is NULL;

      end if;
      ---
      -- If the copy ticket info indicator is set to 'Y'es, copy the ticket info from the component
      -- item to the pack item.
      ---
      if I_ticket = 'Y' then

         insert into item_ticket (item,
                                  ticket_type_id,
                                  po_print_type,
                                  print_on_pc_ind,
                                  ticket_over_pct,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
                           select C_REC.pack_no,
                                  ticket_type_id,
                                  po_print_type,
                                  print_on_pc_ind,
                                  ticket_over_pct,
                                  L_sysdate,
                                  L_sysdate,
                                  L_user_id
                             from item_ticket
                            where item = I_item;

      end if;

      ---
      -- If the copy UDAs indicator is set to 'Y'es, copy the UDA info from the component
      -- item to the pack item for the LOV, free form and date UDA types.
      ---
      if I_uda = 'Y' then

         insert into uda_item_lov (uda_id,
                                   uda_value,
                                   item,
                                   create_datetime,
                                   last_update_datetime,
                                   last_update_id)
                            select uda_id,
                                   uda_value,
                                   C_REC.pack_no,
                                   L_sysdate,
                                   L_sysdate,
                                   L_user_id
                              from uda_item_lov
                             where item = I_item;

         insert into uda_item_ff (uda_id,
                                  uda_text,
                                  item,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id)
                           select uda_id,
                                  uda_text,
                                  C_REC.pack_no,
                                  L_sysdate,
                                  L_sysdate,
                                  L_user_id
                             from uda_item_ff
                            where item = I_item;

         insert into uda_item_date (uda_id,
                                    uda_date,
                                    item,
                                    create_datetime,
                                    last_update_datetime,
                                    last_update_id)
                             select uda_id,
                                    uda_date,
                                    C_REC.pack_no,
                                    L_sysdate,
                                    L_sysdate,
                                    L_user_id
                               from uda_item_date
                              where item = I_item;

      end if;
      -- If the up chrgs info indicator is set to 'Y'es, copy the upcharge info from the component
      -- item to the pack item.
      ---
       if I_up_chrgs = 'Y' then

          insert into item_chrg_head(item,
                                     from_loc,
                                     to_loc,
                                     from_loc_type,
                                     to_loc_type)
                     select distinct C_REC.pack_no,
                                     d.from_loc,
                                     d.to_loc,
                                     d.from_loc_type,
                                     d.to_loc_type
                                from item_chrg_head d
                               where item = I_item;

          insert into item_chrg_detail(item,
                                       from_loc,
                                       to_loc,
                                       comp_id,
                                       from_loc_type,
                                       to_loc_type,
                                       comp_rate,
                                       per_count,
                                       per_count_uom,
                                       up_chrg_group,
                                       comp_currency,
                                       display_order)
                       select distinct C_REC.pack_no,
                                       d.from_loc,
                                       d.to_loc,
                                       d.comp_id,
                                       d.from_loc_type,
                                       d.to_loc_type,
                                       d.comp_rate,
                                       d.per_count,
                                       d.per_count_uom,
                                       d.up_chrg_group,
                                       d.comp_currency,
                                       e.display_order
                                  from item_chrg_detail d,
                                       elc_comp e
                        where d.item = I_item
                          and e.comp_id = d.comp_id;


       end if;
      ---
      -- If the copy docs indicator is set to 'Y'es, copy the docs from the component
      -- item to the pack item.
      ---
      if I_docs = 'Y' then

         if not DOCUMENTS_SQL.GET_DEFAULTS(O_error_message,
                                           'IT',
                                           'IT',
                                           I_item,
                                           C_REC.pack_no,
                                           NULL,
                                           NULL) then
            return FALSE;
         end if;

      end if;

      if I_internal_finisher = 'Y' then
         -- Loop through all the internal finishers associated with the component item.  A cursor loop
         -- is required so the unit costs can be converted to the appropriate
         -- currencies before the inserts into the tables are performed.
         ---
         L_loc_type := 'W';

         FOR C_INTERNAL_FIN_REC in C_INTERNAL_FIN LOOP

            ---
            -- Convert the unit cost in the supplier currency to the location's currency.
            ---
            if C_REC.currency_code != C_INTERNAL_FIN_REC.currency_code then

               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           C_REC.unit_cost,
                                           C_REC.currency_code,
                                           C_INTERNAL_FIN_REC.currency_code,
                                           L_unit_cost_loc,
                                           'C',
                                           L_vdate,
                                           NULL,
                                           NULL,
                                           NULL) then
                  return FALSE;
               end if;

            else

               L_unit_cost_loc := C_REC.unit_cost;

            end if;

            if ITEM_LOC_SQL.CHECK_ITEM_COUNTRY(O_error_message,
                                               O_exists,
                                               C_REC.pack_no,
                                               C_INTERNAL_FIN_REC.loc,
                                               'W')= FALSE then
               return FALSE;
            end if;

            if O_exists = TRUE then
               ---
               -- Insert a record into the item location table for the warehouse being processed.
               ---
               insert into item_loc (item,
                                     loc,
                                     item_parent,
                                     item_grandparent,
                                     loc_type,
                                     unit_retail,
                                     selling_unit_retail,
                                     selling_uom,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_selling_uom,
                                     regular_unit_retail,
                                     rpm_ind,
                                     clear_ind,
                                     taxable_ind,
                                     local_item_desc,
                                     local_short_desc,
                                     ti,
                                     hi,
                                     store_ord_mult,
                                     status,
                                     status_update_date,
                                     daily_waste_pct,
                                     meas_of_each,
                                     meas_of_price,
                                     uom_of_price,
                                     primary_variant,
                                     primary_cost_pack,
                                     primary_supp,
                                     primary_cntry,
                                     receive_as_type,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     inbound_handling_days,
                                     store_price_ind
                                     )
                             values (C_REC.pack_no,
                                     C_INTERNAL_FIN_REC.loc,
                                     NULL,
                                     NULL,
                                     'W',
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,               -- multi_units
                                     NULL,               -- multi_unit_retail
                                     NULL,               -- multi_selling_uom
                                     NULL,               -- regular_unit_retail
                                     'N',                -- rpm_ind
                                     'N',
                                     decode(C_REC.sellable_ind,
                                            'Y', C_INTERNAL_FIN_REC.taxable_ind,
                                            'N'),
                                     C_REC.pack_desc,
                                     rtrim(substrb(C_REC.pack_desc, 1, 120)),
                                     C_REC.ti,
                                     C_REC.hi,
                                     C_INTERNAL_FIN_REC.store_ord_mult,
                                     'A',
                                     L_vdate,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     decode(I_supplier,
                                            'Y', C_INTERNAL_FIN_REC.primary_supp,
                                            C_REC.primary_supp),
                                     decode(I_supplier,
                                            'Y', C_INTERNAL_FIN_REC.primary_cntry,
                                            C_REC.primary_cntry_id),
                                     'E',
                                     L_sysdate,
                                     L_sysdate,
                                     L_user_id,
                                     C_INTERNAL_FIN_REC.inbound_handling_days,
                                     'N');

               ---
               -- Insert the item location stock on hand records for all the warehouse locations
               -- associated to the pack.
               ---
               insert into item_loc_soh (item,
                                         item_parent,
                                         item_grandparent,
                                         loc,
                                         loc_type,
                                         av_cost,
                                         unit_cost,
                                         stock_on_hand,
                                         soh_update_datetime,
                                         last_hist_export_date,
                                         in_transit_qty,
                                         pack_comp_intran,
                                         pack_comp_soh,
                                         tsf_reserved_qty,
                                         pack_comp_resv,
                                         tsf_expected_qty,
                                         pack_comp_exp,
                                         rtv_qty,
                                         non_sellable_qty,
                                         customer_resv,
                                         customer_backorder,
                                         pack_comp_cust_resv,
                                         pack_comp_cust_back,
                                         pack_comp_non_sellable,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id,
                                         primary_supp,
                                         primary_cntry,
                                         average_weight)
                                  values(C_REC.pack_no,
                                         NULL,
                                         NULL,
                                         C_INTERNAL_FIN_REC.loc,
                                         L_loc_type,
                                         NULL,
                                         NULL,
                                         0,
                                         NULL,
                                         NULL,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         L_sysdate,
                                         L_sysdate,
                                         L_user_id,
                                         DECODE(I_supplier,
                                                'Y', C_INTERNAL_FIN_REC.primary_supp,
                                                C_REC.primary_supp),
                                         DECODE(I_supplier,
                                                'Y', C_INTERNAL_FIN_REC.primary_cntry,
                                                C_REC.primary_cntry_id),
                                         L_average_weight);
            else
               O_error_message := SQL_LIB.CREATE_MSG('CANT_APPLY_LOC',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

         END LOOP;
         ---
         FOR C_SUPP_REC in C_SUPPLIERS LOOP
            ---
            L_supplier := C_SUPP_REC.supplier;
            L_loc_type := 'W';
            FOR C_LOCS_REC in C_LOCS LOOP
               ---
               if L_system_options_rec.default_tax_type = 'GTAX' then
                  if C_REC.nic_static_ind = 'Y' then
                     L_neg_item_cost_iscl := L_negotiated_item_cost;
                  elsif C_REC.nic_static_ind = 'N' then
                     L_neg_item_cost_iscl := L_base_cost;
                  end if;
                  ---
                  if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                                  L_default_loc_type,
                                                  C_LOCS_REC.loc) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                                     L_base_cost,
                                                     L_extended_base_cost,
                                                     L_inclusive_cost,
                                                     L_neg_item_cost_iscl,
                                                     C_REC.pack_no,
                                                     C_REC.nic_static_ind,
                                                     C_SUPP_REC.supplier,
                                                     C_LOCS_REC.loc,
                                                     L_default_loc_type,
                                                     'ITEMSUPPCTRYLOC',
                                                     C_REC.primary_cntry_id,
                                                     C_REC.delivery_country_id,
                                                     'Y',  --I_prim_dlvy_ctry_ind
                                                     'N',  --I_update_itemcost_ind
                                                     'N',
                                                     'N',
                                                     NULL) = FALSE then
                      return FALSE;
                  end if;

               else
                  ---
                  if (L_system_options_rec.default_tax_type in ('SVAT','SALES')) then
                     L_base_cost             := NULL;
                     L_negotiated_item_cost  := NULL;
                     L_extended_base_cost    := NULL;
                     L_inclusive_cost        := NULL;
                  end if;
                  ---
                  L_neg_item_cost_iscl    := L_negotiated_item_cost;
                  ---
               end if;
               --- Convert the unit cost in the supplier currency to the location's currency.
               ---
               if C_REC.currency_code != C_SUPP_REC.currency_code then
                  if not CURRENCY_SQL.CONVERT(O_error_message,
                                              L_base_cost,
                                              C_REC.currency_code,
                                              C_SUPP_REC.currency_code,
                                              L_unit_cost_supp,
                                              'C',
                                              L_vdate,
                                              NULL,
                                              NULL,
                                              NULL) then
                     return FALSE;
                  end if;
               else
                  L_unit_cost_supp := C_REC.unit_cost;
               end if;
               ---
               -- Insert item supplier country location (warehouse) records for all the
               -- supplier/country combinations for the component item.
               ---
               insert into item_supp_country_loc (item,
                                                  supplier,
                                                  origin_country_id,
                                                  loc,
                                                  loc_type,
                                                  primary_loc_ind,
                                                  unit_cost,
                                                  round_lvl,
                                                  round_to_inner_pct,
                                                  round_to_case_pct,
                                                  round_to_layer_pct,
                                                  round_to_pallet_pct,
                                                  supp_hier_type_1,
                                                  supp_hier_lvl_1,
                                                  supp_hier_type_2,
                                                  supp_hier_lvl_2,
                                                  supp_hier_type_3,
                                                  supp_hier_lvl_3,
                                                  pickup_lead_time,
                                                  create_datetime,
                                                  last_update_datetime,
                                                  last_update_id,
                                                  extended_base_cost,
                                                  negotiated_item_cost,
                                                  inclusive_cost,
                                                  base_cost)
                                           select C_REC.pack_no,
                                                  iscl.supplier,
                                                  iscl.origin_country_id,
                                                  iscl.loc,
                                                  iscl.loc_type,
                                                  iscl.primary_loc_ind,
                                                  L_unit_cost_supp,
                                                  iscl.round_lvl,
                                                  iscl.round_to_inner_pct,
                                                  iscl.round_to_case_pct,
                                                  iscl.round_to_layer_pct,
                                                  iscl.round_to_pallet_pct,
                                                  iscl.supp_hier_type_1,
                                                  iscl.supp_hier_lvl_1,
                                                  iscl.supp_hier_type_2,
                                                  iscl.supp_hier_lvl_2,
                                                  iscl.supp_hier_type_3,
                                                  iscl.supp_hier_lvl_3,
                                                  iscl.pickup_lead_time,
                                                  L_sysdate,
                                                  L_sysdate,
                                                  L_user_id,
                                                  L_extended_base_cost,
                                                  L_neg_item_cost_iscl,
                                                  L_inclusive_cost,
                                                  L_base_cost
                                             from item_supp_country_loc iscl,
                                                  wh w
                                            where item = I_item
                                              and loc_type ='W'
                                              and loc = w.wh
                                              and loc = C_LOCS_REC.loc
                                              and w.finisher_ind = 'Y'
                                              and supplier = C_SUPP_REC.supplier;
               ---
            END LOOP;
            insert into item_supp_country_bracket_cost (item,
                                                        supplier,
                                                        origin_country_id,
                                                        location,
                                                        bracket_value1,
                                                        loc_type,
                                                        default_bracket_ind,
                                                        unit_cost,
                                                        bracket_value2,
                                                        sup_dept_seq_no)
                                                 select C_REC.pack_no,
                                                        supplier,
                                                        origin_country_id,
                                                        location,
                                                        bracket_value1,
                                                        loc_type,
                                                        default_bracket_ind,
                                                        L_unit_cost_supp,
                                                        bracket_value2,
                                                        sup_dept_seq_no
                                                   from item_supp_country_bracket_cost bc,
                                                        wh w
                                                  where bc.loc_type = 'W'
                                                    and bc.location = w.wh
                                                    and w.finisher_ind = 'Y'
                                                    and bc.supplier = C_SUPP_REC.supplier
                                                    and bc.item     = I_item;
         END LOOP;
      end if;

      if I_external_finisher = 'Y' then
         -- Loop through all the external finishers associated with the component item.  A cursor loop
         -- is required so the unit costs can be converted to the appropriate
         -- currencies before the inserts into the tables are performed.
         ---
         L_loc_type := 'E';

         FOR C_EXTERNAL_FIN_REC in C_EXTERNAL_FIN LOOP

            ---
            -- Convert the unit cost in the supplier currency to the location's currency.
            ---
            if C_REC.currency_code != C_EXTERNAL_FIN_REC.currency_code then

               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           C_REC.unit_cost,
                                           C_REC.currency_code,
                                           C_EXTERNAL_FIN_REC.currency_code,
                                           L_unit_cost_loc,
                                           'C',
                                           L_vdate,
                                           NULL,
                                           NULL,
                                           NULL) then
                  return FALSE;
               end if;

            else

               L_unit_cost_loc := C_REC.unit_cost;

            end if;

            if ITEM_LOC_SQL.CHECK_ITEM_COUNTRY(O_error_message,
                                               O_exists,
                                               C_REC.pack_no,
                                               C_EXTERNAL_FIN_REC.loc,
                                               'E')= FALSE then
               return FALSE;
            end if;

            if O_exists = TRUE then
               ---
               -- Insert a record into the item location table for the warehouse being processed.
               ---
               insert into item_loc (item,
                                     loc,
                                     item_parent,
                                     item_grandparent,
                                     loc_type,
                                     unit_retail,
                                     selling_unit_retail,
                                     selling_uom,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_selling_uom,
                                     regular_unit_retail,
                                     rpm_ind,
                                     clear_ind,
                                     taxable_ind,
                                     local_item_desc,
                                     local_short_desc,
                                     ti,
                                     hi,
                                     store_ord_mult,
                                     status,
                                     status_update_date,
                                     daily_waste_pct,
                                     meas_of_each,
                                     meas_of_price,
                                     uom_of_price,
                                     primary_variant,
                                     primary_cost_pack,
                                     primary_supp,
                                     primary_cntry,
                                     receive_as_type,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     inbound_handling_days,
                                     store_price_ind
                                     )
                             values (C_REC.pack_no,
                                     C_EXTERNAL_FIN_REC.loc,
                                     NULL,
                                     NULL,
                                     'E',
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,                    -- multi_units
                                     NULL,                    -- multi_unit_retail
                                     NULL,                    -- multi_selling_uom
                                     NULL,                    -- regular_unit_retail
                                     'N',                     -- rpm_ind
                                     'N',
                                     decode(C_REC.sellable_ind,
                                            'Y', C_EXTERNAL_FIN_REC.taxable_ind,
                                            'N'),
                                     C_REC.pack_desc,
                                     rtrim(substrb(C_REC.pack_desc, 1, 120)),
                                     C_REC.ti,
                                     C_REC.hi,
                                     C_EXTERNAL_FIN_REC.store_ord_mult,
                                     'A',
                                     L_vdate,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     NULL,
                                     decode(I_supplier,
                                            'Y', C_EXTERNAL_FIN_REC.primary_supp,
                                            C_REC.primary_supp),
                                     decode(I_supplier,
                                            'Y', C_EXTERNAL_FIN_REC.primary_cntry,
                                            C_REC.primary_cntry_id),
                                     'E',
                                     L_sysdate,
                                     L_sysdate,
                                     L_user_id,
                                     C_EXTERNAL_FIN_REC.inbound_handling_days,
                                     'N'
                                     );

               ---
               -- Insert the item location stock on hand records for all the external finishers
               -- associated to the pack.
               ---
               insert into item_loc_soh (item,
                                         item_parent,
                                         item_grandparent,
                                         loc,
                                         loc_type,
                                         av_cost,
                                         unit_cost,
                                         stock_on_hand,
                                         soh_update_datetime,
                                         last_hist_export_date,
                                         in_transit_qty,
                                         pack_comp_intran,
                                         pack_comp_soh,
                                         tsf_reserved_qty,
                                         pack_comp_resv,
                                         tsf_expected_qty,
                                         pack_comp_exp,
                                         rtv_qty,
                                         non_sellable_qty,
                                         customer_resv,
                                         customer_backorder,
                                         pack_comp_cust_resv,
                                         pack_comp_cust_back,
                                         pack_comp_non_sellable,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id,
                                         primary_supp,
                                         primary_cntry,
                                         average_weight)
                                  values(C_REC.pack_no,
                                         NULL,
                                         NULL,
                                         C_EXTERNAL_FIN_REC.loc,
                                         L_loc_type,
                                         NULL,
                                         NULL,
                                         0,
                                         NULL,
                                         NULL,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         0,
                                         L_sysdate,
                                         L_sysdate,
                                         L_user_id,
                                         DECODE(I_supplier,
                                                'Y', C_EXTERNAL_FIN_REC.primary_supp,
                                                C_REC.primary_supp),
                                         DECODE(I_supplier,
                                                'Y', C_EXTERNAL_FIN_REC.primary_cntry,
                                                C_REC.primary_cntry_id),
                                         L_average_weight);
            else
               O_error_message := SQL_LIB.CREATE_MSG('CANT_APPLY_LOC',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

         END LOOP;
         ---
         FOR C_SUPP_REC in C_SUPPLIERS LOOP
            ---
            L_supplier := C_SUPP_REC.supplier;
            L_loc_type := 'E';
            --- Convert the unit cost in the supplier currency.
            ---
            if C_REC.currency_code != C_SUPP_REC.currency_code then
               if not CURRENCY_SQL.CONVERT(O_error_message,
                                           C_REC.unit_cost,
                                           C_REC.currency_code,
                                           C_SUPP_REC.currency_code,
                                           L_unit_cost_supp,
                                           'C',
                                           L_vdate,
                                           NULL,
                                           NULL,
                                           NULL) then
                  return FALSE;
               end if;
            else
               L_unit_cost_supp := C_REC.unit_cost;
            end if;
            ---
            FOR C_LOCS_REC in C_LOCS LOOP
               ---
               if L_system_options_rec.default_tax_type = 'GTAX' then
                  if C_REC.nic_static_ind = 'Y' then
                     L_neg_item_cost_iscl := L_unit_cost_supp;
                     L_base_cost_iscl     := NULL;
                  elsif C_REC.nic_static_ind = 'N' then
                     L_base_cost_iscl         := L_unit_cost_supp;
                     L_neg_item_cost_iscl     := NULL;
                  end if;
                  ---
                  if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                                  L_default_loc_type,
                                                  C_LOCS_REC.loc) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  -- Update the supplier on L_item_add_rec to the supplier being
                  L_item_add_rec.supplier := C_SUPP_REC.supplier;

                  if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                                     L_base_cost_iscl,
                                                     L_extended_base_cost,
                                                     L_inclusive_cost,
                                                     L_neg_item_cost_iscl,
                                                     C_REC.pack_no,
                                                     C_REC.nic_static_ind,
                                                     C_SUPP_REC.supplier,
                                                     C_LOCS_REC.loc,
                                                     L_default_loc_type,
                                                     'ITEMSUPPCTRYLOC',
                                                     C_REC.primary_cntry_id,
                                                     C_REC.delivery_country_id,
                                                     'Y',  --I_prim_dlvy_ctry_ind
                                                     'N',  --I_update_itemcost_ind
                                                     'N',
                                                     'N',
                                                     L_item_add_rec) = FALSE then
                     return FALSE;
                  end if;

               else
                  ---
                  if (L_system_options_rec.default_tax_type in ('SVAT', 'SALES')) then
                     L_base_cost_iscl        := NULL;
                     L_neg_item_cost_iscl    := NULL;
                     L_extended_base_cost    := NULL;
                     L_inclusive_cost        := NULL;
                  end if;
                  ---
               end if;
               ---
               -- Insert item supplier country location (warehouse) records for all the
               -- supplier/country combinations for the component item.
               ---
               insert into item_supp_country_loc (item,
                                                  supplier,
                                                  origin_country_id,
                                                  loc,
                                                  loc_type,
                                                  primary_loc_ind,
                                                  unit_cost,
                                                  round_lvl,
                                                  round_to_inner_pct,
                                                  round_to_case_pct,
                                                  round_to_layer_pct,
                                                  round_to_pallet_pct,
                                                  supp_hier_type_1,
                                                  supp_hier_lvl_1,
                                                  supp_hier_type_2,
                                                  supp_hier_lvl_2,
                                                  supp_hier_type_3,
                                                  supp_hier_lvl_3,
                                                  pickup_lead_time,
                                                  create_datetime,
                                                  last_update_datetime,
                                                  last_update_id,
                                                  extended_base_cost,
                                                  negotiated_item_cost,
                                                  inclusive_cost,
                                                  base_cost)
                                           select C_REC.pack_no,
                                                  supplier,
                                                  origin_country_id,
                                                  loc,
                                                  loc_type,
                                                  primary_loc_ind,
                                                  L_unit_cost_supp,
                                                  round_lvl,
                                                  round_to_inner_pct,
                                                  round_to_case_pct,
                                                  round_to_layer_pct,
                                                  round_to_pallet_pct,
                                                  supp_hier_type_1,
                                                  supp_hier_lvl_1,
                                                  supp_hier_type_2,
                                                  supp_hier_lvl_2,
                                                  supp_hier_type_3,
                                                  supp_hier_lvl_3,
                                                  pickup_lead_time,
                                                  L_sysdate,
                                                  L_sysdate,
                                                  L_user_id,
                                                  L_extended_base_cost,
                                                  L_neg_item_cost_iscl,
                                                  L_inclusive_cost,
                                                  L_base_cost_iscl
                                             from item_supp_country_loc
                                            where item = I_item
                                              and loc_type ='E'
                                              and loc = C_LOCS_REC.loc
                                              and supplier = C_SUPP_REC.supplier;
            END LOOP;
            ---
            insert into item_supp_country_bracket_cost (item,
                                                        supplier,
                                                        origin_country_id,
                                                        location,
                                                        bracket_value1,
                                                        loc_type,
                                                        default_bracket_ind,
                                                        unit_cost,
                                                        bracket_value2,
                                                        sup_dept_seq_no)
                                                 select C_REC.pack_no,
                                                        supplier,
                                                        origin_country_id,
                                                        location,
                                                        bracket_value1,
                                                        loc_type,
                                                        default_bracket_ind,
                                                        L_unit_cost_supp,
                                                        bracket_value2,
                                                        sup_dept_seq_no
                                                   from item_supp_country_bracket_cost bc
                                                  where bc.loc_type = 'E'
                                                    and bc.supplier = C_SUPP_REC.supplier
                                                    and bc.item     = I_item;
         END LOOP;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      if C_GET_SYSTEM_INFO%ISOPEN then
         close C_GET_SYSTEM_INFO;
      end if;
      if C_GET_PARENT%ISOPEN then
         close C_GET_PARENT;
      end if;
      if C_ITEM%ISOPEN then
         close C_ITEM;
      end if;
      if C_SIMPLE_PACK%ISOPEN then
         close C_SIMPLE_PACK;
      end if;
      if C_SUPPLIERS%ISOPEN then
         close C_SUPPLIERS;
      end if;
      if C_SUPP_CTRY%ISOPEN then
         close C_SUPP_CTRY;
      end if;
      if C_WH%ISOPEN then
         close C_WH;
      end if;
      if C_STORE%ISOPEN then
         close C_STORE;
      end if;

      if C_GET_DEFAULT_PCK_METHOD%ISOPEN then
         close C_GET_DEFAULT_PCK_METHOD;
      end if;
      if C_INTERNAL_FIN%ISOPEN then
         close C_INTERNAL_FIN;
      end if;
      if C_EXTERNAL_FIN%ISOPEN then
         close C_EXTERNAL_FIN;
      end if;
      if C_LOCS%ISOPEN then
         close C_LOCS;
      end if;
      if C_GET_ITEM_SUPP_INFO%ISOPEN then
         close C_GET_ITEM_SUPP_INFO;
      end if;

      O_exists_item := L_pack_no;
      O_exists      := TRUE;
      return TRUE;

   when OTHERS then
      if C_GET_SYSTEM_INFO%ISOPEN then
         close C_GET_SYSTEM_INFO;
      end if;
      if C_GET_PARENT%ISOPEN then
         close C_GET_PARENT;
      end if;
      if C_ITEM%ISOPEN then
         close C_ITEM;
      end if;
      if C_SIMPLE_PACK%ISOPEN then
         close C_SIMPLE_PACK;
      end if;
      if C_SUPPLIERS%ISOPEN then
         close C_SUPPLIERS;
      end if;
      if C_SUPP_CTRY%ISOPEN then
         close C_SUPP_CTRY;
      end if;
      if C_WH%ISOPEN then
         close C_WH;
      end if;
      if C_STORE%ISOPEN then
         close C_STORE;
      end if;

      if C_GET_DEFAULT_PCK_METHOD%ISOPEN then
         close C_GET_DEFAULT_PCK_METHOD;
      end if;
      if C_INTERNAL_FIN%ISOPEN then
         close C_INTERNAL_FIN;
      end if;
      if C_EXTERNAL_FIN%ISOPEN then
         close C_EXTERNAL_FIN;
      end if;
      if C_LOCS%ISOPEN then
         close C_LOCS;
      end if;
      if C_GET_ITEM_SUPP_INFO%ISOPEN then
         close C_GET_ITEM_SUPP_INFO;
      end if;

      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_function,
                                            to_char(SQLCODE));
      return FALSE;
END BUILD_PACK;
----------------------------------------------------------------------------------------
FUNCTION GET_RECORD_INFO(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_no_type_decode     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_case_tare_type_decode   IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_pallet_tare_type_decode IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_supp_desc               IN OUT SUPS.SUP_NAME%TYPE,
                         O_origin_country_desc     IN OUT COUNTRY.COUNTRY_DESC%TYPE,
                         O_manu_country_desc       IN OUT COUNTRY.COUNTRY_DESC%TYPE,
                         O_curr_supp               IN OUT SUPS.CURRENCY_CODE%TYPE,
                         O_cost_prim               IN OUT SIMPLE_PACK_TEMP.UNIT_COST%TYPE,
                         O_retail_supp             IN OUT SIMPLE_PACK_TEMP.UNIT_RETAIL%TYPE,
                         O_markup                  IN OUT DEPS.BUD_MKUP%TYPE,
                         O_order_type_decode       IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_sale_type_decode        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_tolerance_type_decode   IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_cost_UOM_unit_cost_prim IN OUT SIMPLE_PACK_TEMP.UNIT_COST%TYPE,
                         O_cost_UOM_unit_cost_supp IN OUT SIMPLE_PACK_TEMP.UNIT_COST%TYPE,
                         I_action                  IN     VARCHAR2,
                         I_item_no_type            IN     SIMPLE_PACK_TEMP.ITEM_NUMBER_TYPE%TYPE,
                         I_case_tare_type          IN     SIMPLE_PACK_TEMP.CASE_TARE_TYPE%TYPE,
                         I_pallet_tare_type        IN     SIMPLE_PACK_TEMP.PALLET_TARE_TYPE%TYPE,
                         I_supplier                IN     SIMPLE_PACK_TEMP.PRIMARY_SUPP%TYPE,
                         I_origin_country_id       IN     SIMPLE_PACK_TEMP.PRIMARY_CNTRY_ID%TYPE,
                         I_manu_country_id         IN     SIMPLE_PACK_TEMP.PRIMARY_MANU_CTRY_ID%TYPE,
                         I_cost_supp               IN     SIMPLE_PACK_TEMP.UNIT_COST%TYPE,
                         I_retail_prim             IN     SIMPLE_PACK_TEMP.UNIT_RETAIL%TYPE,
                         I_curr_prim               IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                         I_curr_supp               IN     SUPS.CURRENCY_CODE%TYPE,
                         I_markup_type             IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                         I_dept                    IN     DEPS.DEPT%TYPE,
                         I_order_type              IN     SIMPLE_PACK_TEMP.ORDER_TYPE%TYPE,
                         I_sale_type               IN     SIMPLE_PACK_TEMP.SALE_TYPE%TYPE,
                         I_tolerance_type          IN     SIMPLE_PACK_TEMP.TOLERANCE_TYPE%TYPE,
                         I_case_weight_uom         IN     SIMPLE_PACK_TEMP.CASE_WEIGHT_UOM%TYPE,
                         I_case_weight             IN     SIMPLE_PACK_TEMP.CASE_WEIGHT%TYPE,
                         I_case_length_uom         IN     SIMPLE_PACK_TEMP.CASE_LWH_UOM%TYPE,
                         I_case_length             IN     SIMPLE_PACK_TEMP.CASE_LENGTH%TYPE,
                         I_case_liquid_volume_uom  IN     SIMPLE_PACK_TEMP.CASE_LIQUID_VOLUME_UOM%TYPE,
                         I_case_liquid_volume      IN     SIMPLE_PACK_TEMP.CASE_LIQUID_VOLUME%TYPE,
                         I_cost_uom                IN     UOM_CLASS.UOM%TYPE,
                         I_std_uom                 IN     UOM_CLASS.UOM%TYPE,
                         I_total_qty               IN     SIMPLE_PACK_TEMP.SUPP_PACK_SIZE%TYPE,
                         I_item                    IN     ITEM_SUPP_COUNTRY.ITEM%TYPE
                        )
RETURN BOOLEAN is

   L_cost_supp_std  SIMPLE_PACK_TEMP.UNIT_COST%TYPE ;
   L_cost_supp_prim SIMPLE_PACK_TEMP.UNIT_COST%TYPE;
   L_cost_UOM_std   SIMPLE_PACK_TEMP.UNIT_COST%TYPE ;
   L_cost_UOM_prim  SIMPLE_PACK_TEMP.UNIT_COST%TYPE ;

   L_supp_curr      SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;


   L_function        VARCHAR2(60)                      := 'SIMPLE_PACK_SQL.GET_RECORD_INFO';
   L_date            PERIOD.VDATE%TYPE                 := GET_VDATE;
   L_markup_type     DEPS.MARKUP_CALC_TYPE%TYPE        := I_markup_type;
   L_curr_prim       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE := I_curr_prim;
   L_supp_prim_ratio NUMBER;
   L_bud_mkup        DEPS.BUD_MKUP%TYPE;
   L_bud_int         DEPS.BUD_INT%TYPE;
   L_item_record     ITEM_MASTER%ROWTYPE;


BEGIN

   ---
   -- Check for invalid input parameters.
   ---
   if I_item_no_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_item_no_type', L_function, NULL);
      return FALSE;

   elsif I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_supplier', L_function, NULL);
      return FALSE;

   elsif I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_origin_country_id', L_function, NULL);
      return FALSE;

   elsif I_cost_supp is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_cost_supp', L_function, NULL);
      return FALSE;

   elsif I_retail_prim is not NULL and I_markup_type is NULL and I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_dept', L_function, NULL);
      return FALSE;
   end if;


   if I_order_type is not NULL then
      if not LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                        'ORDT',
                                        I_order_type,
                                        O_order_type_decode) then
         return FALSE;
      end if;
   end if;
   if I_sale_type is not NULL then
      if not LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                        'STYP',
                                        I_sale_type,
                                        O_sale_type_decode) then
         return FALSE;
      end if;
   end if;
   if I_tolerance_type is not NULL then
      if not LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                        'TOLE',
                                        I_tolerance_type,
                                        O_tolerance_type_decode) then
         return FALSE;
      end if;
   end if;
   ---
   -- Retrieve the decode for the item number type.
   ---
   if not LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                     'UPCT',
                                     I_item_no_type,
                                     O_item_no_type_decode) then
      return FALSE;
   end if;

   ---
   -- Retrieve the decode for the case tare type.
   ---
   if I_case_tare_type is not NULL then
      if not LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                        'TARE',
                                        I_case_tare_type,
                                        O_case_tare_type_decode) then
         return FALSE;
      end if;
   end if;

   ---
   -- Retrieve the decode for the pallet tare type.
   ---
   if I_pallet_tare_type is not NULL then
      if not LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                        'TARE',
                                        I_pallet_tare_type,
                                        O_pallet_tare_type_decode) then
         return FALSE;
      end if;
   end if;


   ---
   -- If the passed in supplier's currency is NULL, retrieve it.
   ---
   if I_curr_supp is NULL then

      if not CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                       I_supplier,
                                       'V',
                                       NULL,   -- zone group ID
                                       O_curr_supp) then
         return FALSE;
      end if;
   else
      O_curr_supp := I_curr_supp;

   end if;

   L_supp_curr := O_curr_supp;

   ---
   -- If the action is 'P'ost Query, retrieve the supplier's description
   -- and the country's description, otherwise they are not required.
   ---
   if I_action = 'P' then

      ---
      -- Retrieve the supplier's description.
      ---
      if not SUPP_ATTRIB_SQL.GET_SUPP_DESC(O_error_message,
                                           I_supplier,
                                           O_supp_desc) then
         return FALSE;
      end if;

      ---
      -- Retrieve the origin country's name.
      ---
      if not COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                           I_origin_country_id,
                                           O_origin_country_desc) then
         return FALSE;
      end if;

      ---
      -- Retrieve the country of manufacture description
      ---
      if not COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                           I_manu_country_id,
                                           O_manu_country_desc) then
         return FALSE;
      end if;

   end if;

   ---
   -- Convert the cost in supplier currency to primary currency and calculate the conversion
   -- ratio. This ratio will be used for the retail conversion.
   ---
   L_cost_supp_std  := I_cost_supp ;
   L_cost_supp_prim := NULL        ;
   L_cost_UOM_std   := NULL        ;
   L_cost_UOM_prim  := NULL        ;
   if CALCULATE_UNIT_COST ( O_error_message           ,
                            L_cost_UOM_prim           ,
                            L_cost_supp_prim          ,
                            L_cost_UOM_std            ,
                            L_cost_supp_std           ,
                            I_cost_uom                ,
                            I_std_uom                 ,
                            I_case_weight_uom         ,
                            I_case_weight             ,
                            I_case_length_uom         ,
                            I_case_length             ,
                            I_case_liquid_volume_uom  ,
                            I_case_liquid_volume      ,
                            I_total_qty               ,
                            L_curr_prim               ,
                            O_curr_supp               ,
                            I_item                    ,
                            I_supplier                ,
                            I_origin_country_id
                         )= FALSE then
      return FALSE;
   end if;
   O_cost_prim               := L_cost_supp_prim  ;
   O_cost_UOM_unit_cost_supp := L_cost_UOM_std    ;
   O_cost_UOM_unit_cost_prim := L_cost_UOM_prim   ;

   if O_cost_prim = 0 then
      O_cost_prim := 1;
   end if;

   L_supp_prim_ratio := I_cost_supp / O_cost_prim;

   ---
   -- If the retail is not NULL, convert the retail in primary currency to supplier currency and
   -- calculate the markup %.
   ---
   if I_retail_prim is not NULL then

      ---
      -- Convert the retail to the supplier currency using the ratio previously calculated.
      ---
      O_retail_supp := I_retail_prim * L_supp_prim_ratio;

      ---
      -- If the action is 'P'ost Query, calculate the markup percent between the cost and retail.
      ---
      if I_action = 'P' then

         ---
         -- Retreive the markup type if its not passed in.
         ---
         if L_markup_type is NULL or L_markup_type = 'C' then
            if not DEPT_ATTRIB_SQL.GET_MARKUP(O_error_message,
                                              L_markup_type,
                                              L_bud_mkup,
                                              L_bud_int,
                                              I_dept) then
               return FALSE;
            end if;
            if L_markup_type = 'C' then
               O_markup := L_bud_mkup;
            end if;
         end if;

         if L_markup_type = 'R' then
            if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                               L_item_record,
                                               I_item) = FALSE then
               return FALSE;
            end if;

            if PM_RETAIL_API_SQL.GET_RPM_MARKUP_INFO(O_error_message,
                                                     L_markup_type,
                                                     O_markup,
                                                     I_dept,
                                                     L_item_record.class,
                                                     L_item_record.subclass) = FALSE then
               return FALSE;
            end if;
         end if;

      end if;

   else

      O_markup      := NULL;
      O_retail_supp := NULL;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_function,
                                            to_char(SQLCODE));
      return FALSE;
END GET_RECORD_INFO;
----------------------------------------------------------------------------------------
FUNCTION PACK_NUMBER_EXISTS(O_error_message IN OUT VARCHAR2,
                            O_exists        IN OUT BOOLEAN,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_function    VARCHAR2(60) := 'SIMPLE_PACK_SQL.PACK_NUMBER_EXISTS';
   L_dummy       VARCHAR2(1);

   cursor C_EXISTS is
      select 'x'
        from simple_pack_temp
       where pack_no = I_item;

BEGIN

   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', 'SIMPLE_PACK_TEMP', 'Pack no:'||I_item);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', 'SIMPLE_PACK_TEMP', 'Pack no:'||I_item);
   fetch C_EXISTS into L_dummy;
   if C_EXISTS%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', 'SIMPLE_PACK_TEMP', 'Pack no:'||I_item);
   close C_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_function,
                                            to_char(SQLCODE));
      return FALSE;
END PACK_NUMBER_EXISTS;
----------------------------------------------------------------------------------------
FUNCTION RETRIEVE_SIMPLE_PACKS(O_error_message   IN OUT   VARCHAR2,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_function                    VARCHAR2(60) := 'SIMPLE_PACK_SQL.RETRIEVE_SIMPLE_PACKS';
   L_pack_no                     ITEM_MASTER.ITEM%TYPE;
   L_item_desc                   ITEM_MASTER.ITEM_DESC%TYPE;
   L_item_desc_secondary         ITEM_MASTER.ITEM_DESC_SECONDARY%TYPE;
   L_case_length                 ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_case_width                  ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_case_height                 ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_case_lwh_uom                ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_case_weight                 ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_case_net_weight             ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_case_tare_weight            ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE;
   L_case_weight_uom             ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_case_liquid_volume          ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_case_liquid_volume_uom      ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_case_tare_type              ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE;
   L_case_stat_cube              ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE;
   L_pallet_length               ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_pallet_width                ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_pallet_height               ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_pallet_lwh_uom              ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_pallet_weight               ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_pallet_net_weight           ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE;
   L_pallet_tare_weight          ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE;
   L_pallet_weight_uom           ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_pallet_liquid_volume        ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_pallet_liquid_volume_uom    ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_pallet_tare_type            ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE;
   L_pallet_stat_cube            ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE;
   L_standard_unit_retail_zone   ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_zone           ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_zone    ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_zone            ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_unit_retail_zone      ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_zone      ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_multi_units_zone            ITEM_LOC.MULTI_UNITS%TYPE;
   L_std_unit_retail_zone_prim   ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail_pri     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_multi_unit_retail_pri       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_system_options_rec          SYSTEM_OPTIONS%ROWTYPE;
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_item_cost_info_rec          ITEM_COST_SQL.ITEM_COST_INFO_REC;
   L_default_loc                 COUNTRY_ATTRIB.DEFAULT_LOC%TYPE;
   L_default_loc_type            COUNTRY_ATTRIB.DEFAULT_LOC_TYPE%TYPE;
   L_item_cost_tax_incl_ind      COUNTRY_ATTRIB.ITEM_COST_TAX_INCL_IND%TYPE;
   L_country_attrib_row          COUNTRY_ATTRIB%ROWTYPE;
   L_delivery_country_id         ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;

   cursor C_RETRIEVE_PACKS is
      select im.item_number_type,
             im.item,
             imtl.item_desc,
             imtl.item_desc_secondary,
             im.const_dimen_ind,
             im.notional_pack_ind,
             im.soh_inquiry_at_pack_ind,
             im.sellable_ind,
             im.order_type,
             im.sale_type,
             pi.pack_qty,
             isc.supplier,
             isc.origin_country_id,
             isc.unit_cost,
             isc.supp_pack_size,
             isc.ti,
             isc.hi,
             isc.cost_uom,
             isc.tolerance_type,
             isc.max_tolerance,
             isc.min_tolerance,
             its.vpn,
             its.supp_label,
             ismc.manu_country_id
        from item_master im,
             v_item_master_tl imtl,
             packitem pi,
             item_supp_country isc,
             item_supplier its,
             item_supp_manu_country ismc
       where pi.item                    = I_item
         and pi.pack_no                 = im.item
         and im.simple_pack_ind         = 'Y'
         and isc.item                   = pi.pack_no
         and isc.primary_supp_ind       = 'Y'
         and isc.primary_country_ind    = 'Y'
         and ismc.item                  = pi.pack_no
         and ismc.supplier              = its.supplier
         and ismc.primary_manu_ctry_ind = 'Y'
         and its.item                   = pi.pack_no
         and imtl.item                  = im.item
         and its.primary_supp_ind       = 'Y';


   cursor C_DIM_INFO (I_dim_object ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE) is
      select iscd.length,
             iscd.width,
             iscd.height,
             iscd.lwh_uom,
             iscd.weight,
             iscd.net_weight,
             iscd.tare_weight,
             iscd.weight_uom,
             iscd.liquid_volume,
             iscd.liquid_volume_uom,
             iscd.tare_type,
             iscd.stat_cube
        from item_supp_country_dim iscd,
             item_supp_country isc
       where iscd.supplier           = isc.supplier
         and iscd.origin_country     = isc.origin_country_id
         and isc.primary_supp_ind    = 'Y'
         and isc.primary_country_ind = 'Y'
         and iscd.dim_object         = I_dim_object
         and iscd.item               = L_pack_no;

BEGIN

   ---
   -- Make sure no records exist for the item in the SIMPLE_PACK_TEMP table.
   ---
   if not SIMPLE_PACK_SQL.DELETE_SIMPLE_PACK_TEMP(O_error_message,
                                                  I_item) then
      return FALSE;
   end if;

   ---
   -- Loop through all the simple packs associated with the item and for each
   -- one, retrieve the appropriate information and populate the SIMPLE_PACK_TEMP table.
   ---
   FOR C_REC in C_RETRIEVE_PACKS LOOP

      ---
      -- Initialize variables for new pack.
      ---
      L_pack_no                   := C_REC.item;
      L_case_length               := NULL;
      L_case_width                := NULL;
      L_case_height               := NULL;
      L_case_lwh_uom              := NULL;
      L_case_weight               := NULL;
      L_case_net_weight           := NULL;
      L_case_tare_weight          := NULL;
      L_case_weight_uom           := NULL;
      L_case_liquid_volume        := NULL;
      L_case_liquid_volume_uom    := NULL;
      L_case_tare_type            := NULL;
      L_case_stat_cube            := NULL;
      L_pallet_length             := NULL;
      L_pallet_width              := NULL;
      L_pallet_height             := NULL;
      L_pallet_lwh_uom            := NULL;
      L_pallet_weight             := NULL;
      L_pallet_net_weight         := NULL;
      L_pallet_tare_weight        := NULL;
      L_pallet_weight_uom         := NULL;
      L_pallet_liquid_volume      := NULL;
      L_pallet_liquid_volume_uom  := NULL;
      L_pallet_tare_type          := NULL;
      L_pallet_stat_cube          := NULL;
      L_standard_unit_retail_zone := NULL;

      ---
      -- Retrieve the case dimensions for the pack's primary supplier/origin country.
      ---
      open C_DIM_INFO ('CA');
      fetch C_DIM_INFO into L_case_length,
                            L_case_width,
                            L_case_height,
                            L_case_lwh_uom,
                            L_case_weight,
                            L_case_net_weight,
                            L_case_tare_weight,
                            L_case_weight_uom,
                            L_case_liquid_volume,
                            L_case_liquid_volume_uom,
                            L_case_tare_type,
                            L_case_stat_cube;
      close C_DIM_INFO;

      ---
      -- Retrieve the pallet dimensions for the pack's primary supplier/origin country.
      ---
      open C_DIM_INFO ('PA');
      fetch C_DIM_INFO into L_pallet_length,
                            L_pallet_width,
                            L_pallet_height,
                            L_pallet_lwh_uom,
                            L_pallet_weight,
                            L_pallet_net_weight,
                            L_pallet_tare_weight,
                            L_pallet_weight_uom,
                            L_pallet_liquid_volume,
                            L_pallet_liquid_volume_uom,
                            L_pallet_tare_type,
                            L_pallet_stat_cube;
      close C_DIM_INFO;

     L_item_desc := C_REC.item_desc;
     L_item_desc_secondary := C_REC.item_desc_secondary;
      ---
      -- If the pack is a sellable pack, retrieve the base retail of the pack.
      ---
      if C_REC.sellable_ind = 'Y' then

         if not PRICING_ATTRIB_SQL.GET_BASE_ZONE_RETAIL(O_error_message,
                                                        L_std_unit_retail_zone_prim,
                                                        L_standard_unit_retail_zone,
                                                        L_standard_uom_zone,
                                                        L_selling_unit_retail_pri,
                                                        L_selling_unit_retail_zone,
                                                        L_selling_uom_zone,
                                                        L_multi_units_zone,
                                                        L_multi_unit_retail_pri,
                                                        L_multi_unit_retail_zone,
                                                        L_multi_selling_uom_zone,
                                                        C_REC.item) then
            return FALSE;
         end if;

      end if;

      if L_system_options_rec.default_tax_type = 'GTAX' then

         ---
         if ITEM_COST_SQL.GET_COST_INFO(L_error_message,
                                        L_item_cost_info_rec,
                                        C_REC.item,
                                        C_REC.supplier,
                                        C_REC.origin_country_id) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_COST_SQL.GET_DLVY_CTRY_INFO(L_error_message,
                                             L_default_loc,
                                             L_default_loc_type,
                                             L_item_cost_tax_incl_ind,
                                             L_item_cost_info_rec.delivery_country_id) = FALSE then
            return FALSE;
         end if;

         L_delivery_country_id := L_item_cost_info_rec.delivery_country_id;

      else  -- for 'SALES' and 'SVAT'

         if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                    L_country_attrib_row,
                                                    NULL,
                                                    C_REC.origin_country_id) = FALSE then
            return FALSE;
         end if;

         L_default_loc         := L_country_attrib_row.default_loc;
         L_default_loc_type    := L_country_attrib_row.default_loc_type;
         L_delivery_country_id := C_REC.origin_country_id;

      end if;

      ---
      -- Insert a record into the SIMPLE_PACK_TEMP for the retrieved pack.
      ---
      insert into simple_pack_temp(pack_no,
                                   item_number_type,
                                   item,
                                   pack_desc,
                                   pack_desc_secondary,
                                   item_qty,
                                   primary_supp,
                                   primary_cntry_id,
                                   unit_cost,
                                   const_dimen_ind,
                                   sellable_ind,
                                   unit_retail,
                                   vpn,
                                   supp_label,
                                   supp_pack_size,
                                   ti,
                                   hi,
                                   case_length,
                                   case_width,
                                   case_height,
                                   case_lwh_uom,
                                   case_weight,
                                   case_net_weight,
                                   case_tare_weight,
                                   case_weight_uom,
                                   case_liquid_volume,
                                   case_liquid_volume_uom,
                                   case_tare_type,
                                   case_stat_cube,
                                   pallet_length,
                                   pallet_width,
                                   pallet_height,
                                   pallet_lwh_uom,
                                   pallet_weight,
                                   pallet_net_weight,
                                   pallet_tare_weight,
                                   pallet_weight_uom,
                                   pallet_liquid_volume,
                                   pallet_liquid_volume_uom,
                                   pallet_tare_type,
                                   pallet_stat_cube,
                                   exists_ind,
                                   order_type,
                                   sale_type,
                                   cost_uom,
                                   tolerance_type,
                                   max_tolerance,
                                   min_tolerance,
                                   notional_pack_ind,
                                   soh_inquiry_at_pack_ind,
                                   primary_manu_ctry_id,
                                   default_loc,
                                   nic_static_ind,
                                   delivery_country_id)
                            values(C_REC.item,
                                   C_REC.item_number_type,
                                   I_item,
                                   L_item_desc,
                                   L_item_desc_secondary,
                                   C_REC.pack_qty,
                                   C_REC.supplier,
                                   C_REC.origin_country_id,
                                   C_REC.unit_cost,
                                   C_REC.const_dimen_ind,
                                   C_REC.sellable_ind,
                                   L_std_unit_retail_zone_prim,
                                   C_REC.vpn,
                                   C_REC.supp_label,
                                   C_REC.supp_pack_size,
                                   C_REC.ti,
                                   C_REC.hi,
                                   L_case_length,
                                   L_case_width,
                                   L_case_height,
                                   L_case_lwh_uom,
                                   L_case_weight,
                                   L_case_net_weight,
                                   L_case_tare_weight,
                                   L_case_weight_uom,
                                   L_case_liquid_volume,
                                   L_case_liquid_volume_uom,
                                   L_case_tare_type,
                                   L_case_stat_cube,
                                   L_pallet_length,
                                   L_pallet_width,
                                   L_pallet_height,
                                   L_pallet_lwh_uom,
                                   L_pallet_weight,
                                   L_pallet_net_weight,
                                   L_pallet_tare_weight,
                                   L_pallet_weight_uom,
                                   L_pallet_liquid_volume,
                                   L_pallet_liquid_volume_uom,
                                   L_pallet_tare_type,
                                   L_pallet_stat_cube,
                                   'Y',
                                   C_REC.order_type,
                                   C_REC.sale_type,
                                   C_REC.cost_uom,
                                   C_REC.tolerance_type,
                                   C_REC.max_tolerance,
                                   C_REC.min_tolerance,
                                   C_REC.notional_pack_ind,
                                   C_REC.soh_inquiry_at_pack_ind,
                                   C_REC.manu_country_id,
                                   L_default_loc,
                                   NVL(L_item_cost_info_rec.nic_static_ind,'N'),
                                   L_delivery_country_id);

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_function,
                                            to_char(SQLCODE));
      return FALSE;
END RETRIEVE_SIMPLE_PACKS;
----------------------------------------------------------------------------------------
FUNCTION UNBUILT_PACKS_EXIST(O_error_message IN OUT VARCHAR2,
                             O_exists        IN OUT BOOLEAN,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_function  VARCHAR2(60) := 'SIMPLE_PACK_SQL.UNBUILT_PACKS_EXIST';
   L_dummy     VARCHAR2(1);

   cursor C_EXIST is
      select 'x'
        from simple_pack_temp
       where item       = I_item
         and exists_ind = 'N';

BEGIN

   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN', 'C_EXIST', 'SIMPLE_PACK_TEMP', 'Item:'||I_item);
   open C_EXIST;
   SQL_LIB.SET_MARK('FETCH', 'C_EXIST', 'SIMPLE_PACK_TEMP', 'Item:'||I_item);
   fetch C_EXIST into L_dummy;
   if C_EXIST%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_EXIST', 'SIMPLE_PACK_TEMP', 'Item:'||I_item);
   close C_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UNBUILT_PACKS_EXIST;
----------------------------------------------------------------------------------------
FUNCTION GET_COPY_INFO(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_default_tax_type     IN OUT   SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE,
                       O_elc_ind              IN OUT   SYSTEM_OPTIONS.ELC_IND%TYPE,
                       O_import_ind           IN OUT   SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                       O_whs_exist            IN OUT   BOOLEAN,
                       O_stores_exist         IN OUT   BOOLEAN,
                       O_expenses_exist       IN OUT   BOOLEAN,
                       O_hts_exist            IN OUT   BOOLEAN,
                       O_tax_exist            IN OUT   BOOLEAN,
                       O_udas_exist           IN OUT   BOOLEAN,
                       O_seasons_exist        IN OUT   BOOLEAN,
                       O_docs_exist           IN OUT   BOOLEAN,
                       O_up_chrgs_exist       IN OUT   BOOLEAN,
                       O_tickets_exist        IN OUT   BOOLEAN,
                       O_int_finisher_exist   IN OUT   BOOLEAN,
                       O_ext_finisher_exist   IN OUT   BOOLEAN,
                       I_item                 IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_function                 VARCHAR2(60) := 'SIMPLE_PACK_SQL.GET_COPY_INFO';

   L_exist                    VARCHAR2(1);

   L_tax_info_rec             OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();
   L_tax_info_tbl             OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();

   cursor C_INDICATORS is
      select default_tax_type,
             elc_ind,
             import_ind
        from system_options;

   cursor C_CHECK_WH is
      select 'x'
        from item_loc il,
             wh w
       where il.item        = I_item
         and il.loc_type    = 'W'
         and il.loc         = w.wh
         and w.finisher_ind = 'N';

   cursor C_CHECK_INT_FINISHER is
      select 'x'
        from item_loc il,
             wh w
       where il.item        = I_item
         and il.loc_type    = 'W'
         and il.loc         = w.wh
         and w.finisher_ind = 'Y';

BEGIN

   ---
   -- If the passed in item is NULL, exit the function with an error.
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_function,
                                             NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_INDICATORS',
                    'SYSTEM_OPTIONS',
                     NULL);

   open C_INDICATORS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_INDICATORS',
                    'SYSTEM_OPTIONS',
                     NULL);

   fetch C_INDICATORS into O_default_tax_type,
                           O_elc_ind,
                           O_import_ind;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_INDICATORS',
                    'SYSTEM_OPTIONS',
                     NULL);

   close C_INDICATORS;

   if ITEMLOC_ATTRIB_SQL.LOCATIONS_EXIST(O_error_message,
                                         I_item,
                                         'E',
                                         O_ext_finisher_exist) = FALSE then
      return FALSE;
   end if;


   L_exist := NULL;
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_WH',
                    'ITEM_LOC, WH',
                    'I_item: ' || I_item);
   open C_CHECK_WH;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_WH',
                    'ITEM_LOC, WH',
                    'I_item: ' || I_item);
   fetch C_CHECK_WH into L_exist;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_WH',
                    'ITEM_LOC, WH',
                    'I_item: ' || I_item);
   close C_CHECK_WH;

   if L_exist is NULL then
      O_whs_exist := FALSE;
   else
      O_whs_exist := TRUE;
   end if;

   L_exist := NULL;
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_INT_FINISHER',
                    'ITEM_LOC, WH',
                    'I_item: ' || I_item);
   open C_CHECK_INT_FINISHER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_INT_FINISHER',
                    'ITEM_LOC, WH',
                    'I_item: ' || I_item);
   fetch C_CHECK_INT_FINISHER into L_exist;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_INT_FINISHER',
                    'ITEM_LOC, WH',
                    'I_item: ' || I_item);
   close C_CHECK_INT_FINISHER;

   if L_exist is NULL then
      O_int_finisher_exist := FALSE;
   else
      O_int_finisher_exist := TRUE;
   end if;

   ---
   -- Check if stores exist for the component item.
   ---
   if not ITEMLOC_ATTRIB_SQL.LOCATIONS_EXIST(O_error_message,
                                             I_item,
                                             'S',
                                             O_stores_exist) then
      return FALSE;
   end if;

   ---
   -- Check if expenses exist for the component item if the system ELC indicator is set
   -- to 'Y'es.
   ---
   if O_elc_ind = 'Y' then
      if not ITEM_ATTRIB_SQL.EXPENSE_EXISTS(O_error_message,
                                            O_expenses_exist,
                                            I_item) then
         return FALSE;
      end if;
   else
      O_expenses_exist := FALSE;
   end if;

   ---
   -- Check if hts info exists for the component item if the system import indicator is set
   -- to 'Y'es.
   ---
   if O_import_ind = 'Y' then
      if not ITEM_HTS_SQL.ITEM_HTS_EXIST(O_error_message,
                                         O_hts_exist,
                                         I_item) then
         return FALSE;
      end if;
   else
      O_hts_exist := FALSE;
   end if;

   ---
   -- Check if UDAs exist for the component item.
   ---
   if not ITEM_ATTRIB_SQL.UDA_EXISTS(O_error_message,
                                     O_udas_exist,
                                     I_item) then
      return FALSE;
   end if;

   ---
   -- Check if seasons exist for the component item.
   ---
   if not SEASON_SQL.CHECK_SEASON(O_error_message,
                                  O_seasons_exist,
                                  I_item) then
      return FALSE;
   end if;

   ---
   -- Check if documents exist for the component item.
   ---
   if not DOCUMENTS_SQL.REQ_DOCS_EXIST_MOD_KEY(O_error_message,
                                               O_docs_exist,
                                               'IT',
                                               I_item,
                                               NULL,
                                               NULL) then
      return FALSE;
   end if;

   ---
   -- Check if up chage details exist for the component item.
   ---
   if not ITEM_CHARGE_SQL.CHARGES_EXIST(O_error_message,
                                        O_up_chrgs_exist,
                                        I_item)  then
         return FALSE;
   end if;
   ---
   -- Check if tickets exist for the component item.
   ---
   if not ITEM_ATTRIB_SQL.TICKET_EXISTS(O_error_message,
                                        O_tickets_exist,
                                        I_item) then
      return FALSE;
   end if;

   ---Assign the item info to ABO object and rest of them are default NULL
   L_tax_info_rec.item := I_item;
   L_tax_info_tbl.EXTEND;
   L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
   ---
   -- Check if TAX info exists for the component item.
   ---
   if TAX_SQL.GET_TAX_INFO(O_error_message,
                           L_tax_info_tbl) = FALSE then
      return FALSE;
   end if;
   ---
   if L_tax_info_tbl.COUNT = 0 then
      O_tax_exist := FALSE;
   else
      O_tax_exist := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_COPY_INFO;
----------------------------------------------------------------------------------------
FUNCTION DELETE_SIMPLE_PACK_TEMP(O_error_message IN OUT VARCHAR2,
                                 I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_function VARCHAR2(60) := 'SIMPLE_PACK_SQL.DELETE_SIMPLE_PACK_TEMP';

BEGIN

   ---
   -- No locking is required for this delete since records are never committed to this table.
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, 'SIMPLE_PACK_TEMP', 'Item:'||I_item);
   delete from simple_pack_temp
    where item = I_item;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_function,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_SIMPLE_PACK_TEMP;
----------------------------------------------------------------------------------------
FUNCTION ITEM_IN_SIMPLE_PACK(O_error_message   IN OUT VARCHAR2,
                             O_exist           IN OUT BOOLEAN,
                             I_item            IN     ITEM_MASTER.ITEM%TYPE,
                             I_simple_pack     IN     PACKITEM.PACK_NO%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(40) := 'SIMPLE_PACK_SQL.ITEM_IN_SIMPLE_PACK';
   L_in_pack   VARCHAR2(1) := 'N';

   cursor C_ITEM_IN_PACK is
      select 'x'
        from packitem pi,
             item_master im
       where pi.item            = I_item
         and pi.pack_no         = im.item
         and im.simple_pack_ind = 'Y'
         and pi.pack_no         = nvl(I_simple_pack,pi.pack_no);

BEGIN
   open C_ITEM_IN_PACK;
   fetch C_ITEM_IN_PACK into L_in_pack;
      if C_ITEM_IN_PACK%FOUND then
         O_exist := TRUE;
      else
         O_exist := FALSE;
      end if;
   close C_ITEM_IN_PACK;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      return FALSE;

END ITEM_IN_SIMPLE_PACK;
-----------------------------------------------------------------------------------------------------
FUNCTION SET_RPM_ITEM_ZONE_PRICE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                                 I_dept                 IN       DEPS.DEPT%TYPE,
                                 I_class                IN       CLASS.CLASS%TYPE,
                                 I_subclass             IN       SUBCLASS.SUBCLASS%TYPE,
                                 I_cost_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_unit_cost            IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(61) := 'SIMPLE_PACK_SQL.SET_RPM_ITEM_ZONE_PRICE';
   L_obj_item_pricing_table   PM_RETAIL_API_SQL.ITEM_PRICING_TABLE;
   L_first                    NUMBER(10);
   L_last                     NUMBER(10);
   L_selling_uom              ITEM_LOC.SELLING_UOM%TYPE;
   L_primary_curr             CURRENCIES.CURRENCY_CODE%TYPE;
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_system_options_rec       SYSTEM_OPTIONS%ROWTYPE;
   L_primary_unit_cost        ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_supp_unit_cost           ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;


BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;

   L_primary_curr   := L_system_options_rec.currency_code;
   L_supp_unit_cost := I_unit_cost;

   if CURRENCY_SQL.CONVERT(L_error_message,
                           L_supp_unit_cost,
                           I_cost_currency_code,
                           L_primary_curr,
                           L_primary_unit_cost,
                           'R',
                           NULL,
                           NULL)= FALSE then
      return FALSE;
   end if;

   PM_RETAIL_API_SQL.GET_ITEM_PRICING_INFO(L_obj_item_pricing_table,
                                           I_item,
                                           I_dept,
                                           I_class,
                                           I_subclass,
                                           L_primary_curr,
                                           L_primary_unit_cost);

   -- check for errors.  The above procedure was designed to be used by the itemretail form.  if an
   -- error was encountered in processing, the return_code is set to FALSE.  need to loop through
   -- the object to ensure no errors are found before calling the set pricing procedure.

   --Selling UOM for simple pack is always defaulted to 'EA'. This will fix standard retail value.
   L_selling_uom := 'EA';

   L_first         := L_obj_item_pricing_table.FIRST;
   L_last          := L_obj_item_pricing_table.LAST;
   for i in L_first..L_last LOOP
      if L_obj_item_pricing_table(i).return_code = 'FALSE' then
         O_error_message := L_obj_item_pricing_table(i).error_message;
         return FALSE;
      end if;
      L_obj_item_pricing_table(i).selling_uom := L_selling_uom;
   end LOOP;

   PM_RETAIL_API_SQL.SET_ITEM_PRICING_INFO(L_obj_item_pricing_table);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END SET_RPM_ITEM_ZONE_PRICE;
-------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_item            IN       PACKITEM.ITEM%TYPE,
                             I_qty             IN       PACKITEM.PACK_QTY%TYPE,
                             I_pack_no         IN       PACKITEM.PACK_NO%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'SIMPLE_PACK_SQL.CHECK_DUPLICATE_QTY';
   L_duplicate_qty_exists   VARCHAR2(1)  := NULL;
   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   L_error_message          RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_DUPLICATE_QTY_EXISTS is
      select 'x'
        from simple_pack_temp s
       where s.item = I_item
         and s.item_qty = I_qty
         and L_system_options_rec.aip_ind = 'Y'
         and s.pack_no <> I_pack_no;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_rec) = FALSE then
      return false;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_DUPLICATE_QTY_EXISTS',
                    'SIMPLE_PACK_TEMP',
                    'Item: '||I_item);
   open C_DUPLICATE_QTY_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_DUPLICATE_QTY_EXISTS',
                    'SIMPLE_PACK_TEMP',
                    'Item: '||I_item);
   fetch C_DUPLICATE_QTY_EXISTS into L_duplicate_qty_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_DUPLICATE_QTY_EXISTS',
                    'SIMPLE_PACK_TEMP',
                    'Item: '||I_item);
   close C_DUPLICATE_QTY_EXISTS;

   if L_duplicate_qty_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_DUPLICATE_QTY;
-------------------------------------------------------------------------------
FUNCTION CHECK_FS_COSTING_LOC_WH (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item            IN       PACKITEM.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(50) := 'SIMPLE_PACK_SQL.CHECK_FS_COSTING_LOC_WH';
   L_check_fs_costing_loc_wh    VARCHAR2(1);

   cursor C_CHECK_FS_COSTING_LOC_WH IS
      select 'Y'
        from item_loc il,
             store s
       where il.loc       = s.store
         and il.item      = I_item
         and il.loc_type  = 'S'
         and s.store_type = 'F'
         and il.costing_loc IS NOT NULL
         and il.costing_loc IN (select w.wh
                                  from wh w ,item_loc il
                                 where il.loc      = w.wh
                                   and il.item     = I_item
                                   and il.loc_type = 'W');

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_FS_COSTING_LOC_WH',
                    'item_loc,store',
                    'Item: '||I_item);
   open C_CHECK_FS_COSTING_LOC_WH;
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_FS_COSTING_LOC_WH',
                    'item_loc,store',
                    'Item: '||I_item);
   fetch C_CHECK_FS_COSTING_LOC_WH into L_check_fs_costing_loc_wh;
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_FS_COSTING_LOC_WH',
                    'item_loc,store',
                    'Item: '||I_item);
   close C_CHECK_FS_COSTING_LOC_WH;

   if L_check_fs_costing_loc_wh IS NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('FS_COSTING_LOC_NOT_RANGED', NULL, NULL, NULL);
      return TRUE;
   end if;

   return FALSE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_FS_COSTING_LOC_WH;
-------------------------------------------------------------------------------
END SIMPLE_PACK_SQL;
/