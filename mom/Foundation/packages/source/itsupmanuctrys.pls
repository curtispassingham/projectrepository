CREATE OR REPLACE PACKAGE ITEM_SUPP_MANU_COUNTRY_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------------
-- Function Name: INSERT_MANU_CTRY_TO_CHILDREN
-- Purpose      : Default the input origin country to all child items for the input
--                item-supplier-Manufacture country combination that do not already have
--                the country associated.
--------------------------------------------------------------------------------
FUNCTION INSERT_MANU_CTRY_TO_CHILDREN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_item                IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                      I_supplier            IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                      I_manu_country_id     IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE,
                                      I_primary_country_ind IN     VARCHAR2,
                                      I_replace_ind         IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: INS_MANU_CTRY_IND_TO_CHILDREN
-- Purpose      : This function will be called by INSERT_MANU_CTRY_TO_CHILDREN function.
--                It will default the input country of manufacture to all child items for the input
--                item-supplier-country of manufacture combination that do not already have
--                the country of manufacture and moreover it will set primary country indicator
--                according to the replace indicator, for the children who already have different
--                countries of manufacture.
--------------------------------------------------------------------------------
FUNCTION INS_MANU_CTRY_IND_TO_CHILDREN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item                IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                       I_child_item          IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                       I_supplier            IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                       I_manu_country_id     IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE,
                                       I_primary_country_ind IN     VARCHAR2,
                                       I_update_ind          IN     VARCHAR2,
                                       I_insert_ind          IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_PRIM_MANU_TO_CHILDREN
-- Purpose      : Determine if there are manufacturer countries already existing for
--                child items which are designated as primary country of manufacture not
--                same as the country of manufacture input for the parent item.  If there
--                are records which exist, then the user must decide whether to override
--                the primary country of manufacture for the child or to insert the new origin
--                country as non-primary.
--------------------------------------------------------------------------------
FUNCTION CHECK_PRIM_MANU_TO_CHILDREN(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_child_primary_exists_ind IN OUT BOOLEAN,
                                     I_item                     IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                     I_supplier                 IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                     I_manu_country_id          IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name: CHECK_CHILD_PRIM_MANU_EXISTS
--Purpose      : Check if any primary country of manufacture exists for child items.
--------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_PRIM_MANU_EXISTS(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists                   IN OUT BOOLEAN,
                                      I_item                     IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                      I_supplier                 IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name: INSERT_MANU_CTRY_TO_COMP_ITEM
--Purpose      : Inserts the input to component items
--------------------------------------------------------------------------------
FUNCTION INSERT_MANU_CTRY_TO_COMP_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_pack_no           IN     ITEM_MASTER.ITEM%TYPE,
                                       I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                       I_manu_country_id   IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name: INSERT_ITEM_SUPP_MANU_CTRY
--Purpose      : Does a 1 row insert into ITEM_SUPP_MANU_COUNTRY.
--------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPP_MANU_CTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_ismc_rec      IN     ITEM_SUPP_MANU_COUNTRY%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name: INSERT_ITEM_SUPP_MANU_CTRY
--Purpose      : Overloaded function,the input parameters are single attributes instead of %ROWTYPE.
--               Does a 1 row insert into ITEM_SUPP_MANU_COUNTRY.
--------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPP_MANU_CTRY(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                    IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                    I_supplier                IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                    I_manu_country_id         IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE,
                                    I_primary_manu_ctry_ind   IN     ITEM_SUPP_MANU_COUNTRY.PRIMARY_MANU_CTRY_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_INDICATORS( O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                    I_manu_country   IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function: UPDATE_YES_PRIM_MANU_IND
--  Purpose: Updates primary_manu_country_ind to Y for 1 row in the ITEM_SUPP_MANU_COUNTRY table.
--           NOTE:  only used for XITEM subscription API after UPDATE_PRIMARY_INDICATORS is called
--------------------------------------------------------------------------------
FUNCTION UPDATE_YES_PRIM_MANU_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_ismc_rec      IN ITEM_SUPP_MANU_COUNTRY%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--Function Name: DELETE_ITEM_SUPP_MANU_COUNTRY
--Purpose      : Deletes 1 row from ITEM_SUPP_MANU_COUNTRY.
--------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_SUPP_MANU_COUNTRY(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item               IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                       I_supplier           IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                       I_manu_country_id    IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
--- Function Name:  ITEM_SUPP_MANU_CTRY_EXISTS
--- Purpose: If the supplier is not null, identifies the presence of an 
---          item/supplier/country combination on the item_supp_country table 
---          for the input origin country id otherwise checks the item/origin_country
---          relationship.
--------------------------------------------------------------------------------
FUNCTION ITEM_SUPP_MANU_CTRY_EXISTS (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exist              IN OUT BOOLEAN,
                                     I_item               IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                     I_supplier           IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                     I_manu_country_id    IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_MANU_COUNTRY_DELETE
-- Purpose:       Checks for existence of item/supplier/manu country combinations in Item_hts.
--                Checks if item is not a pack, makes sure it is 
--                not a component of a pack that is associated with the supplier/manu
--                country being deleted.  If it is, displays error and stops processing.
--------------------------------------------------------------------------------
FUNCTION CHECK_MANU_COUNTRY_DELETE(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exist               IN OUT BOOLEAN,
                                   I_item                IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                   I_supplier            IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                   I_manu_country_id     IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_MANU_CTRY_FROM_CHILDREN
-- Purpose: This function will delete from ITEM_SUPP_MANU_COUNTRY table.
--------------------------------------------------------------------------------
FUNCTION DELETE_MANU_CTRY_FROM_CHILDREN(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                        I_supplier          IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                        I_manu_country_id   IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: LOCK_ITEM_SUPP_MANU_CTRY
-- Purpose: This function will Lock the country in ITEM_SUPP_MANU_COUNTRY table.
--------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_SUPP_MANU_CTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_table_locked      IN OUT VARCHAR2,
                                  I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                  I_supplier          IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,
                                  I_manu_country_id   IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_PRIMARY_MANU_COUNTRY
-- Purpose: This function will return the Primary Manufacture country in ITEM_SUPP_MANU_COUNTRY table.
--          for the input item/supplier
--------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_MANU_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists            IN OUT BOOLEAN,
                                  O_manu_country_id   IN OUT ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE,
                                  I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                  I_supplier          IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: UPDATE_PRIMARY_MANU_INDICATOR
-- Purpose: This function will update the Primary Manufacture country in ITEM_SUPP_MANU_COUNTRY table.
--          for the child items for the input item/supplier
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_MANU_INDICATOR(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                                       I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                       I_manu_country_id    IN     ITEM_SUPP_MANU_COUNTRY.MANU_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_MANU_CTRY_EXISTS_FOR_PK
-- Purpose: This function will check if atleast one supplier/manufacturing country combination of the pack.
--          item matches with one of supplier/manufacturing country of the component item.
--------------------------------------------------------------------------------
FUNCTION CHECK_MANU_CTRY_EXISTS_FOR_PK(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_exists               OUT BOOLEAN,
                                       O_supplier             OUT ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE,                                     
                                       I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE,
                                       I_pk_item           IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: COUNT_MANU_CTRY_PRIM_IND 
-- Purpose: This function will get the Primary Manufacture country count from ITEM_SUPP_MANU_COUNTRY, 
--          for the input item/supplier. 
------------------------------------------------------------------------------------------------------- 
FUNCTION COUNT_MANU_CTRY_PRIM_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                                  O_mult_ind             OUT BOOLEAN, 
                                  I_item              IN     ITEM_SUPP_MANU_COUNTRY.ITEM%TYPE, 
                                  I_supplier          IN     ITEM_SUPP_MANU_COUNTRY.SUPPLIER%TYPE) 
   RETURN BOOLEAN; 
------------------------------------------------------------------------------------------------------- 



END ITEM_SUPP_MANU_COUNTRY_SQL;
/

