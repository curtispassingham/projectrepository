CREATE OR REPLACE PACKAGE RMSSUB_XITEMLOC AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC VARIABLES
--------------------------------------------------------------------------------

ITEM_LOC_ADD   CONSTANT VARCHAR2(30) := 'xitemloccre';
ITEM_LOC_UPD   CONSTANT VARCHAR2(30) := 'xitemlocmod';

--------------------------------------------------------------------------------
-- PUBLIC RECORD TYPES
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code   IN OUT   VARCHAR2,
                  O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN       RIB_OBJECT,
                  I_message_type  IN       VARCHAR2);
----------------------------------------------------------------------------

END RMSSUB_XITEMLOC;
/
