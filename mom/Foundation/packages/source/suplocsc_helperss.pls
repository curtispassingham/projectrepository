CREATE OR REPLACE PACKAGE SUPLOCSC_HELPERS AUTHID CURRENT_USER AS 

----------------------------------------------------------------
-- Function Name: POST_QUERY
-- Purpose	: Used by Delivery Schedule (suplocsc) module in ADF.
----------------------------------------------------------------
FUNCTION POST_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
					O_offset_day      IN OUT SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE,
					O_offset_day_desc IN OUT VARCHAR2,
					O_delivery_cycle  IN OUT SOURCE_DLVRY_SCHED.delivery_cycle%TYPE,
					O_delivery_cycle_desc IN OUT VARCHAR2,
					O_start_date      IN OUT SOURCE_DLVRY_SCHED.start_date%TYPE,
					O_loc_desc        IN OUT VARCHAR2,                                    
					I_day			  IN     SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE,
					I_source      	  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
					I_source_type  	  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
					I_location        IN     repl_day.location%TYPE,
					I_loc_type        IN     VARCHAR2) 
	RETURN NUMBER;
		
END SUPLOCSC_HELPERS;
/	
