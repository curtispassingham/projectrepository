CREATE OR REPLACE PACKAGE SYSTEM_OPTIONS_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--- GLOBAL VARIABLES
---
--- GP_options_populated is set to TRUE the first time the system_options
--- table is hit in this session.  Later calls to this package will access
--- the following package level variables so the system_options table is
--- not accessed repeatedly.
--- GP_system_options_row is a package level variable that is held for the
--- duration of the session.  This row value can be used rather than hitting
--- the database each time a variable is fetched.
-------------------------------------------------------------------------------
GP_options_populated    BOOLEAN := FALSE;
GP_system_options_row   SYSTEM_OPTIONS%ROWTYPE;


-------------------------------------------------------------------------------
--    Name: POPULATE_SYSTEM_OPTIONS
-- Purpose: This function gets all of the system options and stores
--          them in package level variables so that the system options
--          table only needs to be hit one time per session.  This function will
--          always fetch the system options even if they are already populated.
--          This is the function that would be called when any of the options
--          are changed.
-------------------------------------------------------------------------------
FUNCTION POPULATE_SYSTEM_OPTIONS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
--    Name: GET_SYSTEM_OPTIONS
-- Purpose: This function returns all system_options in one rowtype variable.
-------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_system_options_row     OUT  SYSTEM_OPTIONS%ROWTYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: GET_STKLDGR_LEVEL
-- Purpose  : This function gets the time level code
--      from the system_options materialized view.
--------------------------------------------------------------------
FUNCTION GET_STKLDGR_LEVEL(O_error_message                 IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_stock_ledger_time_level_code     OUT  SYSTEM_OPTIONS.STOCK_LEDGER_TIME_LEVEL_CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------
-- Function Name: GET_AIP_IND
-- Purpose  : This function gets the AIP indicator from the
--                system_options table.
--------------------------------------------------------------------
FUNCTION GET_AIP_IND(O_aip_ind           OUT  SYSTEM_OPTIONS.AIP_IND%TYPE,
                     O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name:  MULTI_CURRENCY_IND
-- Purpose  :  Used to retrieve the value of the multi_currency_ind
----------------------------------------------------------------
FUNCTION MULTI_CURRENCY_IND(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_multi_currency_ind     OUT  SYSTEM_OPTIONS.MULTI_CURRENCY_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: CURRENCY_CODE
-- Purpose  :  Used to retrieve the value of the currency_code
----------------------------------------------------------------
FUNCTION CURRENCY_CODE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_currency_code     OUT  SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: CONSOLIDATION_IND
-- Purpose  :  Used to retrieve the value of the consolidation_ind
----------------------------------------------------------------
FUNCTION CONSOLIDATION_IND(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_consolidation_ind     OUT  SYSTEM_OPTIONS.CONSOLIDATION_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: STD_AV_IND
-- Purpose  : Used to retrieve the value of std_av_ind.
----------------------------------------------------------------
FUNCTION STD_AV_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_std_av_ind        OUT  SYSTEM_OPTIONS.STD_AV_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: GET_DEPT_LEVEL_TSF
-- Purpose  : Used to retrieve the value of dept_level_transfers.
----------------------------------------------------------------
FUNCTION GET_DEPT_LEVEL_TSF(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_dept_level_tsf    OUT  SYSTEM_OPTIONS.DEPT_LEVEL_TRANSFERS%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: GET_FINANCIAL_AP
-- Purpose      : This function gets the financial_ap value
--                from the system_options table.  This value tells us
--                who Retek is interfacing to.
-- Created      : 27-MAR-98
--------------------------------------------------------------------
FUNCTION GET_FINANCIAL_AP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_financial_ap      OUT  SYSTEM_OPTIONS.FINANCIAL_AP%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
FUNCTION GET_IMPORT_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_import_ind        OUT  SYSTEM_OPTIONS.IMPORT_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
FUNCTION GET_ELC_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_elc_ind           OUT  SYSTEM_OPTIONS.ELC_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
FUNCTION GET_IMPORT_ELC_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_import_ind        OUT  SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                            O_elc_ind           OUT  SYSTEM_OPTIONS.ELC_IND%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function Name: GET_CONTRACT_IND
-- Purpose      : This function returns the contract indicator from the system_options table.
----------------------------------------------------------------
FUNCTION GET_CONTRACT_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_contract_ind      OUT  SYSTEM_OPTIONS.CONTRACT_IND%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GET_IMAGE_PATH
-- Purpose      : This function returns the image path from the system_options table.
-----------------------------------------------------------------------------------------
FUNCTION GET_IMAGE_PATH(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_image_path        OUT  SYSTEM_OPTIONS.IMAGE_PATH%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GET_LATEST_SHIP_DAYS
-- Purpose      : This function returns the number of days after which the latest ship
--                date should default from the earliest ship date; it retrieves this
--                information from the system_options table.
-----------------------------------------------------------------------------------------
FUNCTION GET_LATEST_SHIP_DAYS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_latest_ship_days    OUT  SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GET_TITLE_PASS
-- Purpose      : This function retrieves the default title pass code and decode; it
--                retrieves this information from the system_options table.
-----------------------------------------------------------------------------------------
FUNCTION GET_TITLE_PASS(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_fob_title_pass          OUT  SYSTEM_OPTIONS.FOB_TITLE_PASS%TYPE,
                        O_fob_title_pass_desc     OUT  SYSTEM_OPTIONS.FOB_TITLE_PASS_DESC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: GET_BASE_COUNTRY
-- Purpose      : Retrieves the base country ID
-------------------------------------------------------------------------------------------
FUNCTION GET_BASE_COUNTRY(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_base_country_id     OUT  SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: GET_ALL_DEFAULT_UOM
-- Purpose:       Used to retrieve all default UOMs(standard, dimension, weight) and
--                to retrieve the default packing method
-------------------------------------------------------------------------------------------
FUNCTION GET_ALL_DEFAULT_UOM(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_default_standard_uom       OUT  SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE,
                             O_default_dimension_uom      OUT  SYSTEM_OPTIONS.DEFAULT_DIMENSION_UOM%TYPE,
                             O_default_weight_uom         OUT  SYSTEM_OPTIONS.DEFAULT_WEIGHT_UOM%TYPE,
                             O_default_packing_method     OUT  SYSTEM_OPTIONS.DEFAULT_PACKING_METHOD%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULT_ROUNDING
-- Purpose:       Used to retrieve all rounding percentages (inner, case, pallet)
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ROUNDING(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_round_to_inner_pct      OUT  SYSTEM_OPTIONS.ROUND_TO_INNER_PCT%TYPE,
                              O_round_to_case_pct       OUT  SYSTEM_OPTIONS.ROUND_TO_CASE_PCT%TYPE,
                              O_round_to_layer_pct      OUT  SYSTEM_OPTIONS.ROUND_TO_LAYER_PCT%TYPE,
                              O_round_to_pallet_pct     OUT  SYSTEM_OPTIONS.ROUND_TO_PALLET_PCT%TYPE,
                              O_round_lvl               OUT  SYSTEM_OPTIONS.ROUND_LVL%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULT_UOM_AND_NAMES
-- Purpose:       Used to retrieve all default UOMs(standard, dimension, weight),
--                the default packing method, and the default names for pallet,
--                case and inner
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_UOM_AND_NAMES(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_default_standard_uom       OUT  SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE,
                                   O_default_dimension_uom      OUT  SYSTEM_OPTIONS.DEFAULT_DIMENSION_UOM%TYPE,
                                   O_default_weight_uom         OUT  SYSTEM_OPTIONS.DEFAULT_WEIGHT_UOM%TYPE,
                                   O_default_packing_method     OUT  SYSTEM_OPTIONS.DEFAULT_PACKING_METHOD%TYPE,
                                   O_default_pallet_name        OUT  SYSTEM_OPTIONS.DEFAULT_PALLET_NAME%TYPE,
                                   O_default_case_name          OUT  SYSTEM_OPTIONS.DEFAULT_CASE_NAME%TYPE,
                                   O_default_inner_name         OUT  SYSTEM_OPTIONS.DEFAULT_INNER_NAME%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name: GET_LC_EXP_DAYS
-- Purpose      : Retrieves the LC expiration days from the system options table.
--------------------------------------------------------------------------------------
FUNCTION GET_LC_EXP_DAYS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_lc_exp_days       OUT  SYSTEM_OPTIONS.LC_EXP_DAYS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_LC_DEFAULTS
-- Purpose      : Retrieves the system applicant for Letter of Credit processing.
--------------------------------------------------------------------------------------
FUNCTION GET_LC_DEFAULTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_lc_form_type      OUT  SYSTEM_OPTIONS.LC_FORM_TYPE%TYPE,
                         O_lc_type           OUT  SYSTEM_OPTIONS.LC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULT_UOP
-- Purpose      : Retrieves default_uop from system_options table.
--------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_UOP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_default_uop       OUT  SYSTEM_OPTIONS.DEFAULT_UOP%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_ROUND_LEVEL
-- Purpose      : Retrieves round_lvl from system_options table.
--------------------------------------------------------------------------------------
FUNCTION GET_ROUND_LVL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_round_lvl         OUT  SYSTEM_OPTIONS.ROUND_LVL%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_STAKE_REVIEW_DAYS
-- Purpose      : Retrieves the value in stake_review_days from the system_options
--      table.
--------------------------------------------------------------------------------------
FUNCTION GET_STAKE_REVIEW_DAYS(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_stake_review_days     OUT  SYSTEM_OPTIONS.STAKE_REVIEW_DAYS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_SA_IND
-- Purpose:       Retrieves SALES_AUDIT_IND from system_options table.
--------------------------------------------------------------------------------------
FUNCTION GET_SA_IND(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_sales_audit_ind     OUT  SYSTEM_OPTIONS.SALES_AUDIT_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_DEAL_LEAD_DAYS
-- Purpose:       This function will retrieve the deal_lead_days off of the SYSTEM_OPTIONS table.
--                Open a cursor that selects the deal_lead_days from the SYSTEM_OPTIONS table.
--                Set the output parameter O_deal_lead_days to the retrieved value.
----------------------------------------------------------------------------------------
FUNCTION GET_DEAL_LEAD_DAYS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_deal_lead_days     OUT  SYSTEM_OPTIONS.DEAL_LEAD_DAYS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
-- Function Name: GET_UPDATE_HTS_INDS
-- Purpose:       This function will retrieve the UPDATE_ITEM_HTS_IND and the
--                UPDATE_ORDER_HTS_IND off of the SYSTEM_OPTIONS table.  Open a cursor
--                that retrieves these values and then fetch the values into the
--                corresponding output parameters.
----------------------------------------------------------------------------------------
FUNCTION GET_UPDATE_HTS_INDS(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_update_item_hts_ind      OUT  SYSTEM_OPTIONS.UPDATE_ITEM_HTS_IND%TYPE,
                             O_update_order_hts_ind     OUT  SYSTEM_OPTIONS.UPDATE_ORDER_HTS_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_SCHEDULE_IND
-- Purpose:       This function will retrieve the LOC_DLVRY_IND and the LOC_ACTIVITY_IND
--                off of the SYSTEM_OPTIONS table.  Open a cursor that retrieves these
--                values and then fetch the values into the corresponding output parameters.
----------------------------------------------------------------------------------------
FUNCTION GET_SCHEDULE_IND(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_loc_dlvry_ind        OUT  SYSTEM_OPTIONS.LOC_DLVRY_IND%TYPE,
                          O_loc_activity_ind     OUT  SYSTEM_OPTIONS.LOC_ACTIVITY_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULT_ORDER_TYPE
-- Purpose:       This function will retrieve the DEFAULT_ORDER_TYPE.
--------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ORDER_TYPE(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_default_order_type        OUT  SYSTEM_OPTIONS.DEFAULT_ORDER_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_ELC_VAT_STD_AV
-- Purpose  : used to retrieve the elc_ind,
--                vat_ind and std_av from the system_options table
--------------------------------------------------------------------------------
FUNCTION GET_ELC_VAT_STD_AV(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_elc_ind           OUT  SYSTEM_OPTIONS.ELC_IND%TYPE,
                            O_std_av_ind        OUT  SYSTEM_OPTIONS.STD_AV_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_ITEM_MASTER_OPTIONS
-- Purpose:       Used to group and retrieve all system options needed by
--                the Item Master form.
--------------------------------------------------------------------------------
FUNCTION GET_ITEM_MASTER_OPTIONS(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_import_ind                 OUT  SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                                 O_elc_ind                    OUT  SYSTEM_OPTIONS.ELC_IND%TYPE,
                                 O_multi_currency_ind         OUT  SYSTEM_OPTIONS.MULTI_CURRENCY_IND%TYPE,
                                 O_currency_code              OUT  SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                                 O_forecast_ind               OUT  SYSTEM_OPTIONS.FORECAST_IND%TYPE,
                                 O_default_standard_uom       OUT  SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE,
                                 O_auto_approve_child_ind     OUT  SYSTEM_OPTIONS.AUTO_APPROVE_CHILD_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_ITEMLOC_OPTIONS
-- Purpose:       Retrieves values needed by the Item Location form
--------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_OPTIONS(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_multi_currency_ind           OUT  SYSTEM_OPTIONS.MULTI_CURRENCY_IND%TYPE,
                             O_currency_code                OUT  SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                             O_stkldgr_vat_incl_retl_ind    OUT  SYSTEM_OPTIONS.STKLDGR_VAT_INCL_RETL_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_HELP_INDS
-- Purpose:       This function gets the sales audit indicator, the
--                trade manaagement indicator, and the ari indicator
--                from the system_options table.
--------------------------------------------------------------------------------
FUNCTION GET_HELP_INDS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_sales_audit_ind     OUT  SYSTEM_OPTIONS.SALES_AUDIT_IND%TYPE,
                       O_import_ind          OUT  SYSTEM_OPTIONS.IMPORT_IND%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION SOFT_CONTRACT_IND(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_soft_contract_ind     OUT  SYSTEM_OPTIONS.SOFT_CONTRACT_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_BRACKET_COST_IND
-- Purpose:       This function gets the bracket_cost_ind
--                from the system_options table.
--------------------------------------------------------------------------------
FUNCTION GET_BRACKET_COST_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_bracket_costing_ind     OUT  SYSTEM_OPTIONS.BRACKET_COSTING_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_IB_DEFAULTS
-- Purpose:       This function returns the Investment Buy default values from
--                the system_options table.
--------------------------------------------------------------------------------
FUNCTION GET_IB_DEFAULTS(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_look_ahead_days           OUT  SYSTEM_OPTIONS.LOOK_AHEAD_DAYS%TYPE,
                         O_cost_wh_storage_meas      OUT  SYSTEM_OPTIONS.COST_WH_STORAGE_MEAS%TYPE,
                         O_cost_wh_storage           OUT  SYSTEM_OPTIONS.COST_WH_STORAGE%TYPE,
                         O_cost_wh_storage_uom       OUT  SYSTEM_OPTIONS.COST_WH_STORAGE_UOM%TYPE,
                         O_cost_out_storage_meas     OUT  SYSTEM_OPTIONS.COST_OUT_STORAGE_MEAS%TYPE,
                         O_cost_out_storage          OUT  SYSTEM_OPTIONS.COST_OUT_STORAGE%TYPE,
                         O_cost_out_storage_uom      OUT  SYSTEM_OPTIONS.COST_OUT_STORAGE_UOM%TYPE,
                         O_cost_level                OUT  SYSTEM_OPTIONS.COST_LEVEL%TYPE,
                         O_storage_type              OUT  SYSTEM_OPTIONS.STORAGE_TYPE%TYPE,
                         O_max_weeks_supply          OUT  SYSTEM_OPTIONS.MAX_WEEKS_SUPPLY%TYPE,
                         O_target_roi                OUT  SYSTEM_OPTIONS.TARGET_ROI%TYPE,
                         O_cost_money                OUT  SYSTEM_OPTIONS.COST_MONEY%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: GET_WH_CROSS_LINK_IND
-- Purpose:       This function returns the warehouse cross link indicator from
--                the system_options table.
-------------------------------------------------------------------------------
FUNCTION GET_WH_CROSS_LINK_IND(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_wh_cross_link_ind     OUT  SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: GET_REJECT_STORE_ORD_IND
-- Purpose:       This function returns the reject store orders indicator from
--                the system_options table.
-------------------------------------------------------------------------------
FUNCTION GET_REJECT_STORE_ORD_IND(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_reject_store_ord_ind     OUT  SYSTEM_OPTIONS.REJECT_STORE_ORD_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  GET_DEFAULT_ALLOC_CHRG_IND
--Purpose      :  Retrieves the default allocation up charge indicator
--                This indicator indicates whether or not Up Charges
--                should be associated with allocations.
-------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ALLOC_CHRG_IND(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_default_alloc_chrg_ind     OUT  SYSTEM_OPTIONS.DEFAULT_ALLOC_CHRG_IND%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------
-- Function Name: GET_CLASS_LEVEL_VAT_IND
-- Purpose  : This function gets the class level VAT indicator from the
--            system_options table.
-------------------------------------------------------------------------------
FUNCTION GET_CLASS_LEVEL_VAT_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_class_level_vat_ind     OUT  SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
FUNCTION GET_STKLDGR_VAT_INCL_RETL_IND(O_error_message              IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_stkldgr_vat_incl_retl_ind  IN OUT  SYSTEM_OPTIONS.STKLDGR_VAT_INCL_RETL_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_CYCLE_COUNT_LAG_DAYS(O_error_message         IN OUT VARCHAR2,
                                  O_cycle_count_lag_days  IN OUT SYSTEM_OPTIONS.CYCLE_COUNT_LAG_DAYS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_GROCERY_ITEMS_IND
-- Purpose      : This function gets the Grocery Items indicator from the
--                system_options table.  This column indicates
--                whether or not the retailer handles grocery items.
--------------------------------------------------------------------------------
FUNCTION GET_GROCERY_ITEMS_IND(O_error_message       IN OUT   VARCHAR2,
                               O_grocery_items_ind   IN OUT   SYSTEM_OPTIONS.GROCERY_ITEMS_IND%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: GET_NWP_IND
-- Purpose      : This function gets the NWP indicator from the
--                system_options table.  This column indicates
--                whether or not NWP functionality will be used.
--------------------------------------------------------------------------------
FUNCTION GET_NWP_IND(O_error_message  IN OUT VARCHAR2,
                     O_nwp_ind        IN OUT SYSTEM_OPTIONS.NWP_IND%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_DATA_LEVEL_SECURITY_IND(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_data_level_security_ind    OUT  SYSTEM_OPTIONS.DATA_LEVEL_SECURITY_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_WRONG_ST_RECEIPT_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_wrong_st_receipt_ind    OUT  SYSTEM_OPTIONS.WRONG_ST_RECEIPT_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: CHECK_RECORD_LOCKED
-- Purpose  : This function checks if the system options record is being used by another user.
-------------------------------------------------------------------------------
FUNCTION CHECK_RECORD_LOCKED(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_TSF_FORCE_CLOSE_IND(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_tsf_force_close_ind   IN OUT   SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_MIN_MAX_CMO
-- Purpose      : This function gets the minimum and maximum CMO% values from
--                the system_options table.  These values are used to constrain
--                WEEK_DATA.CUM_MARKON_PCT and MONTH_DATA.CUM_MARKON_PCT.
--------------------------------------------------------------------------------
FUNCTION GET_MIN_MAX_CMO(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_min_cum_markon_pct   IN OUT   SYSTEM_OPTIONS.MIN_CUM_MARKON_PCT%TYPE,
                         O_max_cum_markon_pct   IN OUT   SYSTEM_OPTIONS.MAX_CUM_MARKON_PCT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_STORE_PACK_COMP_RCV_IND
-- Purpose      : This function retrieves the STORE_PACK_COMP_RCV_IND column
--                from the SYSTEM_OPTIONS table.
-------------------------------------------------------------------------------
FUNCTION GET_STORE_PACK_COMP_RCV_IND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_store_pack_comp_rcv_ind IN OUT SYSTEM_OPTIONS.STORE_PACK_COMP_RCV_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_DUPLICATE_RECEIVING_IND
-- Purpose      : This function retrieves the DUPLICATE_RECEIVING_IND column
--                from the SYSTEM_OPTIONS table.
-------------------------------------------------------------------------------
FUNCTION GET_DUPLICATE_RECEIVING_IND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_duplicate_receiving_ind IN OUT SYSTEM_OPTIONS.DUPLICATE_RECEIVING_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_INCREASE_TSF_QTY_IND
-- Purpose      : This function retrieves the Increase Transfer Quantity
--                Indicator from the SYSTEM_OPTIONS table
-------------------------------------------------------------------------------
FUNCTION GET_INCREASE_TSF_QTY_IND(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_increase_tsf_qty_ind   IN OUT   SYSTEM_OPTIONS.INCREASE_TSF_QTY_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_SUPPLIER_SITES_IND
-- Purpose      : This function retrieves the SUPPLIER_SITES_IND column
--                from the SYSTEM_OPTIONS table.
-------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_SITES_IND(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_supplier_sites_ind      IN OUT  SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_TAX_TYPE(O_error_message           IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                              O_default_tax_type        IN OUT    SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_OTB_SYSTEM_IND
-- Purpose  : Used to retrieve the value of otb_system_ind.
-------------------------------------------------------------------------------
FUNCTION GET_OTB_SYSTEM_IND(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_otb_system_ind      OUT   SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END SYSTEM_OPTIONS_SQL;
/