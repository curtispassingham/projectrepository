
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSMFM_DIFFID AS

--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 O_diff_id      OUT DIFFID_MFQUEUE.DIFF_ID%TYPE,
                 O_message_type OUT VARCHAR2, 
                 O_message      OUT CLOB);
--------------------------------------------------------------------------------

           /*** Public Program Bodies***/

--------------------------------------------------------------------------------
PROCEDURE ADDTOQ(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 I_message_type IN  DIFFID_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_diff_id      IN  DIFFID_MFQUEUE.DIFF_ID%TYPE,
                 I_message      IN  CLOB)
IS

   /* This procedure takes a message in CLOB format and adds it to the
      message queue table.
   */

BEGIN
   insert into diffid_mfqueue(seq_no,
                              pub_status,
                              message_type,
                              diff_id,
                              message)
                     values(diffid_mfsequence.NEXTVAL,
                            'U',
                            I_message_type,
                            I_diff_Id,
                            I_message);

    O_status := API_CODES.SUCCESS;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_DIFFID.ADDTOQ');

END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code   OUT  VARCHAR2,
                 O_error_msg     OUT  VARCHAR2,
                 O_message_type  OUT  VARCHAR2,
                 O_message       OUT  CLOB,
                 O_diff_id       OUT  DIFFGRP_MFQUEUE.DIFF_ID%TYPE)
IS

  
BEGIN

   GETNXT(O_status_code,
          O_error_msg,
          O_diff_id,                
          O_message_type,
          O_message);


EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_DIFFID.GETNXT');

END GETNXT;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 O_diff_id      OUT DIFFID_MFQUEUE.DIFF_ID%TYPE,
                 O_message_type OUT VARCHAR2, 
                 O_message      OUT CLOB)
IS
   
   /* This procedure fetches the row from the message queue table that has
      the lowest sequence number.  The message is retrieved, then the row
      is removed from the queue.
   */
   L_queue_rowid  ROWID;

   cursor C_GET_MESSAGE is
      select diff_id,
             message_type,
             message,
             rowid
        from diffid_mfqueue
       where seq_no = (select min(seq_no)
                         from diffid_mfqueue)
         for update nowait;

BEGIN
   open C_GET_MESSAGE;
   fetch C_GET_MESSAGE into O_diff_id,
                            O_message_type,
                            O_message,
                            L_queue_rowid;
   if C_GET_MESSAGE%NOTFOUND then
      O_status := API_CODES.NO_MSG;      
   else
      delete from diffid_mfqueue
       where rowid = L_queue_rowid;

      O_status := API_CODES.SUCCESS;
   end if;

   close C_GET_MESSAGE;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_DIFFID.GETNXT');

END GETNXT;
--------------------------------------------------------------------------------
END RMSMFM_DIFFID;
/
