CREATE OR REPLACE PACKAGE CORESVC_FCUSTOMER_UPLOAD_SQL AUTHID CURRENT_USER AS

GP_fcustupld_fhead_row   SVC_FCUSTUPLD_FHEAD%ROWTYPE;
GP_fcustupld_thead_row   SVC_FCUSTUPLD_THEAD%ROWTYPE;
GP_fcustupld_tdetl_row   SVC_FCUSTUPLD_TDETL%ROWTYPE;

-- Parameter tables
FHEAD   CONSTANT  VARCHAR2(30) := 'SVC_FCUSTUPLD_FHEAD';
THEAD   CONSTANT  VARCHAR2(30) := 'SVC_FCUSTUPLD_THEAD';
TDETL   CONSTANT  VARCHAR2(30) := 'SVC_FCUSTUPLD_TDETL';
TTAIL   CONSTANT  VARCHAR2(30) := 'SVC_FCUSTUPLD_TTAIL';
FTAIL   CONSTANT  VARCHAR2(30) := 'SVC_FCUSTUPLD_FTAIL';

-- Process/chunk status
NEW_RECORD   CONSTANT  VARCHAR2(30) := 'N';
ERROR        CONSTANT  VARCHAR2(30) := 'E';
REJECTED     CONSTANT  VARCHAR2(30) := 'R';
PROCESSED    CONSTANT  VARCHAR2(30) := 'P';
ADD_HEAD     CONSTANT  VARCHAR2(30) := 'fcustgrpcre';
UPD_HEAD     CONSTANT  VARCHAR2(30) := 'fcustgrpupd';
DEL_HEAD     CONSTANT  VARCHAR2(30) := 'fcustgrpdel';
ADD_DETL     CONSTANT  VARCHAR2(30) := 'fcustcre';
UPD_DETL     CONSTANT  VARCHAR2(30) := 'fcustupd';
DEL_DETL     CONSTANT  VARCHAR2(30) := 'fcustdel';

-------------------------------------------------------------------------------------------------
-- Function:    VALIDATE_FATAL
-- Description: Validates information in the parameter tables after loading prior to main process.
--              This is for FATAL errors.
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_FATAL(O_error_message   IN OUT NOCOPY VARCHAR2,
                        I_process_id      IN     SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function:   VALIDATE_PARAMTER_TABLES
-- Description:Validates information in the parameter tables after loading prior to main process.
--             This is for non FATAL rejects.
-------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PARAMETER_TABLES(O_error_message   IN OUT NOCOPY VARCHAR2,
                                   I_process_id      IN     SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function:    INITIALIZE_PROCESS_STATUS
-- Description: Inserts the initial entry for the franchise customer upload process.
--              This is an asynchronous function.
-------------------------------------------------------------------------------------------------
FUNCTION INITIALIZE_PROCESS_STATUS(O_error_message   IN OUT NOCOPY VARCHAR2,
                                   I_process_id      IN     SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE,
                                   I_reference_id    IN     SVC_FCUSTUPLD_STATUS.REFERENCE_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function:    GET_TRANSACTION
-- Description: To process one transcation at a time.This calls the core logic.
-------------------------------------------------------------------------------------------------
FUNCTION GET_TRANSACTION(O_error_message   IN OUT NOCOPY VARCHAR2,
                         I_process_id      IN            SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;   
-------------------------------------------------------------------------------------------------
-- Function:    PROCESS_TRANSACTION
-- Description: Core logic to insert,update and delete of franchise customers and customer groups.
----------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TRANSACTION (O_error_message     OUT VARCHAR2,
                              O_rowid             OUT VARCHAR2,
                              O_table_name        OUT VARCHAR2,
                              I_fcust_seq_no      IN  SVC_FCUSTUPLD_FHEAD.FCUST_SEQ_NO%TYPE,
                              I_process_id        IN  SVC_FCUSTUPLD_FHEAD.PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function:    LOCK_WF_CUSTOMER_GROUP_TABLE
-- Description: To lock the table WF_CUSTOMER_GROUP.
-------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_WF_CUSTOMER_GROUP_TABLE(O_error_message     IN OUT NOCOPY VARCHAR2,
                                      I_customer_group_id IN     SVC_FCUSTUPLD_THEAD.F_CUSTOMER_GROUP_ID%TYPE
                                      ) 
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function:    LOCK_WF_CUSTOMER_GRP_TABLE_TL
-- Description: To lock the table WF_CUSTOMER_GROUP_TL.
-------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_WF_CUSTOMER_GRP_TABLE_TL(O_error_message     IN OUT NOCOPY VARCHAR2,
                                       I_customer_group_id IN     SVC_FCUSTUPLD_THEAD.F_CUSTOMER_GROUP_ID%TYPE
                                       ) 
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function:    LOCK_WF_CUSTOMER_TABLE
-- Description: To lock the table WF_CUSTOMER.
------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_WF_CUSTOMER_TABLE(O_error_message     IN OUT NOCOPY VARCHAR2,
                                I_customer_id       IN     SVC_FCUSTUPLD_TDETL.FCUST_SEQ_NO%TYPE
                                )
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function:    LOCK_WF_CUSTOMER_TABLE_TL
-- Description: To lock the table WF_CUSTOMER_TL.
------------------------------------------------------------------------------------------------------------------
FUNCTION LOCK_WF_CUSTOMER_TABLE_TL(O_error_message     IN OUT NOCOPY VARCHAR2,
                                   I_customer_id       IN     SVC_FCUSTUPLD_TDETL.FCUST_SEQ_NO%TYPE
                                   )
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function:    PURGE_FCUSTUPLD_TABLES
-- Description: TO purge the records in staging tables.
------------------------------------------------------------------------------------------------------------------
FUNCTION PURGE_FCUSTUPLD_TABLES(O_error_message IN OUT NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------------------
END CORESVC_FCUSTOMER_UPLOAD_SQL;
/
