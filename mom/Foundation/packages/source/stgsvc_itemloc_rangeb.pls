CREATE OR REPLACE PACKAGE BODY STGSVC_ITEM_LOC_RANGING_SQL AS
--------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_LOC_RANGING(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_rms_async_id             IN OUT   RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                     I_item                     IN       SVC_ITEM_LOC_RANGING.ITEM%TYPE,
                                     I_item_parent              IN       SVC_ITEM_LOC_RANGING.ITEM_PARENT%TYPE,
                                     I_item_grandparent         IN       SVC_ITEM_LOC_RANGING.ITEM_GRANDPARENT%TYPE,
                                     I_locs_tbl                 IN       LOC_TBL,
                                     I_loc_group_type           IN       SVC_ITEM_LOC_RANGING.LOC_GROUP_TYPE%TYPE,
                                     I_loc_group_value          IN       SVC_ITEM_LOC_RANGING.LOC_GROUP_VALUE%TYPE,
                                     I_currency_code            IN       SVC_ITEM_LOC_RANGING.CURRENCY_CODE%TYPE,
                                     I_short_desc               IN       SVC_ITEM_LOC_RANGING.SHORT_DESC%TYPE,
                                     I_dept                     IN       SVC_ITEM_LOC_RANGING.DEPT%TYPE,
                                     I_class                    IN       SVC_ITEM_LOC_RANGING.CLASS%TYPE,
                                     I_subclass                 IN       SVC_ITEM_LOC_RANGING.SUBCLASS%TYPE,
                                     I_item_level               IN       SVC_ITEM_LOC_RANGING.ITEM_LEVEL%TYPE,
                                     I_tran_level               IN       SVC_ITEM_LOC_RANGING.TRAN_LEVEL%TYPE,
                                     I_sellable_ind             IN       SVC_ITEM_LOC_RANGING.SELLABLE_IND%TYPE,
                                     I_orderable_ind            IN       SVC_ITEM_LOC_RANGING.ORDERABLE_IND%TYPE,
                                     I_pack_ind                 IN       SVC_ITEM_LOC_RANGING.PACK_IND%TYPE,
                                     I_pack_type                IN       SVC_ITEM_LOC_RANGING.PACK_TYPE%TYPE,
                                     I_item_status              IN       SVC_ITEM_LOC_RANGING.ITEM_STATUS%TYPE,
                                     I_waste_type               IN       SVC_ITEM_LOC_RANGING.WASTE_TYPE%TYPE,
                                     I_item_loc_status          IN       SVC_ITEM_LOC_RANGING.ITEM_LOC_STATUS%TYPE,
                                     I_local_item_desc          IN       SVC_ITEM_LOC_RANGING.LOCAL_ITEM_DESC%TYPE,
                                     I_local_short_desc         IN       SVC_ITEM_LOC_RANGING.LOCAL_SHORT_DESC%TYPE,
                                     I_ranged_ind               IN       SVC_ITEM_LOC_RANGING.RANGED_IND%TYPE,
                                     I_taxable_ind              IN       SVC_ITEM_LOC_RANGING.TAXABLE_IND%TYPE,
                                     I_store_price_ind          IN       SVC_ITEM_LOC_RANGING.STORE_PRICE_IND%TYPE,
                                     I_primary_supp             IN       SVC_ITEM_LOC_RANGING.PRIMARY_SUPP%TYPE,
                                     I_primary_cntry            IN       SVC_ITEM_LOC_RANGING.PRIMARY_CNTRY%TYPE,
                                     I_primary_variant          IN       SVC_ITEM_LOC_RANGING.PRIMARY_VARIANT%TYPE,
                                     I_primary_cost_pack        IN       SVC_ITEM_LOC_RANGING.PRIMARY_COST_PACK%TYPE,
                                     I_source_method            IN       SVC_ITEM_LOC_RANGING.SOURCE_METHOD%TYPE,
                                     I_source_wh                IN       SVC_ITEM_LOC_RANGING.SOURCE_WH%TYPE,
                                     I_store_ord_mult           IN       SVC_ITEM_LOC_RANGING.STORE_ORD_MULT%TYPE,
                                     I_receive_as_type          IN       SVC_ITEM_LOC_RANGING.RECEIVE_AS_TYPE%TYPE,
                                     I_inbound_handling_days    IN       SVC_ITEM_LOC_RANGING.INBOUND_HANDLING_DAYS%TYPE,
                                     I_daily_waste_pct          IN       SVC_ITEM_LOC_RANGING.DAILY_WASTE_PCT%TYPE,
                                     I_ti                       IN       SVC_ITEM_LOC_RANGING.TI%TYPE,
                                     I_hi                       IN       SVC_ITEM_LOC_RANGING.HI%TYPE,
                                     I_unit_cost_loc            IN       SVC_ITEM_LOC_RANGING.UNIT_COST_LOC%TYPE,
                                     I_unit_retail_loc          IN       SVC_ITEM_LOC_RANGING.UNIT_RETAIL_LOC%TYPE,
                                     I_selling_unit_retail_loc  IN       SVC_ITEM_LOC_RANGING.SELLING_UNIT_RETAIL_LOC%TYPE,
                                     I_selling_uom              IN       SVC_ITEM_LOC_RANGING.SELLING_UOM%TYPE,
                                     I_multi_units              IN       SVC_ITEM_LOC_RANGING.MULTI_UNITS%TYPE,
                                     I_multi_unit_retail        IN       SVC_ITEM_LOC_RANGING.MULTI_UNIT_RETAIL%TYPE,
                                     I_multi_selling_uom        IN       SVC_ITEM_LOC_RANGING.MULTI_SELLING_UOM%TYPE,
                                     I_meas_of_each             IN       SVC_ITEM_LOC_RANGING.MEAS_OF_EACH%TYPE,
                                     I_meas_of_price            IN       SVC_ITEM_LOC_RANGING.MEAS_OF_PRICE%TYPE,
                                     I_uom_of_price             IN       SVC_ITEM_LOC_RANGING.UOM_OF_PRICE%TYPE,
                                     I_ext_uin_ind              IN       SVC_ITEM_LOC_RANGING.EXT_UIN_IND%TYPE,
                                     I_uin_type                 IN       SVC_ITEM_LOC_RANGING.UIN_TYPE%TYPE,
                                     I_uin_label                IN       SVC_ITEM_LOC_RANGING.UIN_LABEL%TYPE,
                                     I_default_to_children      IN       SVC_ITEM_LOC_RANGING.DEFAULT_TO_CHILDREN%TYPE,
                                     I_capture_time             IN       SVC_ITEM_LOC_RANGING.CAPTURE_TIME%TYPE,
                                     I_costing_loc              IN       SVC_ITEM_LOC_RANGING.COSTING_LOC%TYPE DEFAULT NULL,
                                     I_costing_loc_type         IN       SVC_ITEM_LOC_RANGING.COSTING_LOC_TYPE%TYPE DEFAULT NULL)                                     
RETURN BOOLEAN IS
   L_program                     VARCHAR2(64)          := 'STGSVC_ITEM_LOC_RANGING_SQL.INSERT_SVC_ITEM_LOC_RANGING';

   cursor C_GET_NEXT_ASYNC_ID is
      select rms_async_id_seq.nextval
        from dual;
BEGIN

   open C_GET_NEXT_ASYNC_ID;
   fetch C_GET_NEXT_ASYNC_ID into O_rms_async_id;
   close C_GET_NEXT_ASYNC_ID;

   insert into svc_item_loc_ranging(rms_async_id,
                                    item,
                                    item_parent,
                                    item_grandparent,
                                    loc_group_type,
                                    loc_group_value,
                                    currency_code,
                                    short_desc,
                                    dept,
                                    class,
                                    subclass,
                                    item_level,
                                    tran_level,
                                    sellable_ind,
                                    orderable_ind,
                                    pack_ind,
                                    pack_type,
                                    item_status,
                                    waste_type,
                                    item_loc_status,
                                    local_item_desc,
                                    local_short_desc,
                                    ranged_ind,
                                    taxable_ind,
                                    store_price_ind,
                                    primary_supp,
                                    primary_cntry,
                                    primary_variant,
                                    primary_cost_pack,
                                    source_method,
                                    source_wh,
                                    store_ord_mult,
                                    receive_as_type,
                                    inbound_handling_days,
                                    daily_waste_pct,
                                    ti,
                                    hi,
                                    unit_cost_loc,
                                    unit_retail_loc,
                                    selling_unit_retail_loc,
                                    selling_uom,
                                    multi_units,
                                    multi_unit_retail,
                                    multi_selling_uom,
                                    meas_of_each,
                                    meas_of_price,
                                    uom_of_price,
                                    ext_uin_ind,
                                    uin_type,
                                    uin_label,
                                    default_to_children,
                                    capture_time,
                                    costing_loc,
                                    costing_loc_type,
                                    create_id,
                                    create_datetime,
                                    last_update_id,
                                    last_update_datetime)
                             values(O_rms_async_id,
                                    I_item,
                                    I_item_parent,
                                    I_item_grandparent,
                                    I_loc_group_type,
                                    I_loc_group_value,
                                    I_currency_code,
                                    I_short_desc,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    I_item_level,
                                    I_tran_level,
                                    I_sellable_ind,
                                    I_orderable_ind,
                                    I_pack_ind,
                                    I_pack_type,
                                    I_item_status,
                                    I_waste_type,
                                    I_item_loc_status,
                                    I_local_item_desc,
                                    I_local_short_desc,
                                    I_ranged_ind,
                                    I_taxable_ind,
                                    I_store_price_ind,
                                    I_primary_supp,
                                    I_primary_cntry,
                                    I_primary_variant,
                                    I_primary_cost_pack,
                                    I_source_method,
                                    I_source_wh,
                                    I_store_ord_mult,
                                    I_receive_as_type,
                                    I_inbound_handling_days,
                                    I_daily_waste_pct,
                                    I_ti,
                                    I_hi,
                                    I_unit_cost_loc,
                                    I_unit_retail_loc,
                                    I_selling_unit_retail_loc,
                                    I_selling_uom,
                                    I_multi_units,
                                    I_multi_unit_retail,
                                    I_multi_selling_uom,
                                    I_meas_of_each,
                                    I_meas_of_price,
                                    I_uom_of_price,
                                    I_ext_uin_ind,
                                    I_uin_type,
                                    I_uin_label,
                                    I_default_to_children,
                                    I_capture_time,
                                    I_costing_loc,
                                    I_costing_loc_type,
                                    USER,
                                    SYSDATE,
                                    USER,
                                    SYSDATE);

   insert into svc_item_loc_ranging_locs (rms_async_id,
                                          loc)
                                  select O_rms_async_id,
                                         value(l)
                                    from TABLE(CAST (I_locs_tbl as LOC_TBL))l;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_SVC_ITEM_LOC_RANGING;
--------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_LOC_RANGING(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_rms_async_id  IN OUT RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                     I_input         IN     NEW_ITEM_LOC_SQL.NIL_INPUT_TBL)                                     
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'STGSVC_ITEM_LOC_RANGING_SQL.INSERT_SVC_ITEM_LOC_RANGING';

   cursor C_GET_NEXT_ASYNC_ID is
      select rms_async_id_seq.nextval
        from dual;
BEGIN

   open C_GET_NEXT_ASYNC_ID;
   fetch C_GET_NEXT_ASYNC_ID into O_rms_async_id;
   close C_GET_NEXT_ASYNC_ID;
                               
   FORALL I_indx IN I_input.FIRST .. I_input.LAST
      INSERT INTO svc_item_loc_ranging (rms_async_id,
                                        ITEM, 
                                        ITEM_PARENT, 
                                        ITEM_GRANDPARENT, 
                                        SHORT_DESC, 
                                        DEPT, 
                                        CLASS, 
                                        SUBCLASS, 
                                        ITEM_LEVEL, 
                                        TRAN_LEVEL, 
                                        ITEM_STATUS, 
                                        WASTE_TYPE, 
                                        SELLABLE_IND, 
                                        ORDERABLE_IND, 
                                        PACK_IND, 
                                        PACK_TYPE, 
                                        ITEM_DESC, 
                                        DIFF_1, 
                                        DIFF_2, 
                                        DIFF_3, 
                                        DIFF_4, 
                                        LOC, 
                                        LOC_TYPE, 
                                        DAILY_WASTE_PCT, 
                                        UNIT_COST_LOC, 
                                        UNIT_RETAIL_LOC, 
                                        SELLING_UNIT_RETAIL_LOC, 
                                        SELLING_UOM,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_selling_uom,
                                        ITEM_LOC_STATUS, 
                                        TAXABLE_IND, 
                                        TI, 
                                        HI, 
                                        STORE_ORD_MULT, 
                                        MEAS_OF_EACH, 
                                        MEAS_OF_PRICE, 
                                        UOM_OF_PRICE, 
                                        PRIMARY_VARIANT, 
                                        PRIMARY_SUPP, 
                                        PRIMARY_CNTRY, 
                                        LOCAL_ITEM_DESC, 
                                        LOCAL_SHORT_DESC, 
                                        PRIMARY_COST_PACK, 
                                        RECEIVE_AS_TYPE, 
                                        STORE_PRICE_IND, 
                                        UIN_TYPE, 
                                        UIN_LABEL, 
                                        CAPTURE_TIME, 
                                        EXT_UIN_IND, 
                                        SOURCE_METHOD, 
                                        SOURCE_WH, 
                                        INBOUND_HANDLING_DAYS,
                                        CURRENCY_CODE,
                                        COSTING_LOC,
                                        COSTING_LOC_TYPE,
                                        LIKE_STORE, 
                                        DEFAULT_TO_CHILDREN,
                                        HIER_LEVEL, 
                                        HIER_NUM_VALUE, 
                                        HIER_CHAR_VALUE,
                                        RANGED_IND,
                                        CREATE_ID,
                                        CREATE_DATETIME,
                                        LAST_UPDATE_ID,
                                        LAST_UPDATE_DATETIME)
                                VALUES (O_rms_async_id,
                                        I_input(I_indx).ITEM, 
                                        I_input(I_indx).ITEM_PARENT, 
                                        I_input(I_indx).ITEM_GRANDPARENT, 
                                        I_input(I_indx).ITEM_SHORT_DESC, 
                                        I_input(I_indx).DEPT, 
                                        I_input(I_indx).ITEM_CLASS, 
                                        I_input(I_indx).SUBCLASS, 
                                        I_input(I_indx).ITEM_LEVEL, 
                                        I_input(I_indx).TRAN_LEVEL, 
                                        I_input(I_indx).ITEM_STATUS, 
                                        I_input(I_indx).WASTE_TYPE, 
                                        I_input(I_indx).SELLABLE_IND, 
                                        I_input(I_indx).ORDERABLE_IND, 
                                        I_input(I_indx).PACK_IND, 
                                        I_input(I_indx).PACK_TYPE, 
                                        I_input(I_indx).ITEM_DESC, 
                                        I_input(I_indx).DIFF_1, 
                                        I_input(I_indx).DIFF_2, 
                                        I_input(I_indx).DIFF_3, 
                                        I_input(I_indx).DIFF_4, 
                                        I_input(I_indx).LOC, 
                                        I_input(I_indx).LOC_TYPE, 
                                        I_input(I_indx).DAILY_WASTE_PCT, 
                                        I_input(I_indx).UNIT_COST_LOC, 
                                        I_input(I_indx).UNIT_RETAIL_LOC, 
                                        I_input(I_indx).SELLING_RETAIL_LOC, 
                                        I_input(I_indx).SELLING_UOM,
                                        I_input(I_indx).multi_units,
                                        I_input(I_indx).multi_unit_retail,
                                        I_input(I_indx).multi_selling_uom,
                                        I_input(I_indx).ITEM_LOC_STATUS, 
                                        I_input(I_indx).TAXABLE_IND, 
                                        I_input(I_indx).TI, 
                                        I_input(I_indx).HI, 
                                        I_input(I_indx).STORE_ORD_MULT, 
                                        I_input(I_indx).MEAS_OF_EACH, 
                                        I_input(I_indx).MEAS_OF_PRICE, 
                                        I_input(I_indx).UOM_OF_PRICE, 
                                        I_input(I_indx).PRIMARY_VARIANT, 
                                        I_input(I_indx).PRIMARY_SUPP, 
                                        I_input(I_indx).PRIMARY_CNTRY, 
                                        I_input(I_indx).LOCAL_ITEM_DESC, 
                                        I_input(I_indx).LOCAL_SHORT_DESC, 
                                        I_input(I_indx).PRIMARY_COST_PACK, 
                                        I_input(I_indx).RECEIVE_AS_TYPE, 
                                        I_input(I_indx).STORE_PRICE_IND, 
                                        I_input(I_indx).UIN_TYPE, 
                                        I_input(I_indx).UIN_LABEL, 
                                        I_input(I_indx).CAPTURE_TIME, 
                                        I_input(I_indx).EXT_UIN_IND, 
                                        I_input(I_indx).SOURCE_METHOD, 
                                        I_input(I_indx).SOURCE_WH, 
                                        I_input(I_indx).INBOUND_HANDLING_DAYS,
                                        I_input(I_indx).CURRENCY_CODE, 
                                        I_input(I_indx).COSTING_LOC, 
                                        I_input(I_indx).COSTING_LOC_TYPE, 
                                        I_input(I_indx).LIKE_STORE, 
                                        I_input(I_indx).DEFAULT_TO_CHILDREN_IND, 
                                        I_input(I_indx).HIER_LEVEL, 
                                        I_input(I_indx).HIER_NUM_VALUE, 
                                        I_input(I_indx).HIER_CHAR_VALUE,
                                        'Y', -- RANGED_IND
                                        USER,
                                        SYSDATE,
                                        USER,
                                        SYSDATE);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_SVC_ITEM_LOC_RANGING;
--------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_LOC_RANGING_WR(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_rms_async_id  IN OUT RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                        I_input         IN     WRP_NIL_INPUT_TBL)                                     
RETURN NUMBER IS
   L_program VARCHAR2(64) := 'STGSVC_ITEM_LOC_RANGING_SQL.INSERT_SVC_ITEM_LOC_RANGING_WR';
   L_input_tbl     NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
   L_input_rec     NEW_ITEM_LOC_SQL.NIL_INPUT_RECORD;
BEGIN

   --copy the data in database type IO_sup_inv_mgmt_tbl to PLSQL type L_sup_inv_mgmt_tbl
   for i in I_input.FIRST .. I_input.last loop
      L_input_rec.ITEM                    := I_input(i).ITEM;
      L_input_rec.ITEM_PARENT             := I_input(i).ITEM_PARENT;
      L_input_rec.ITEM_GRANDPARENT        := I_input(i).ITEM_GRANDPARENT;
      L_input_rec.ITEM_SHORT_DESC         := I_input(i).ITEM_SHORT_DESC;
      L_input_rec.DEPT                    := I_input(i).DEPT;
      L_input_rec.ITEM_CLASS              := I_input(i).ITEM_CLASS;
      L_input_rec.SUBCLASS                := I_input(i).SUBCLASS;
      L_input_rec.ITEM_LEVEL              := I_input(i).ITEM_LEVEL;
      L_input_rec.TRAN_LEVEL              := I_input(i).TRAN_LEVEL;
      L_input_rec.ITEM_STATUS             := I_input(i).ITEM_STATUS;
      L_input_rec.WASTE_TYPE              := I_input(i).WASTE_TYPE;
      L_input_rec.SELLABLE_IND            := I_input(i).SELLABLE_IND;
      L_input_rec.ORDERABLE_IND           := I_input(i).ORDERABLE_IND;
      L_input_rec.PACK_IND                := I_input(i).PACK_IND;
      L_input_rec.PACK_TYPE               := I_input(i).PACK_TYPE;
      L_input_rec.ITEM_DESC               := I_input(i).ITEM_DESC;
      L_input_rec.DIFF_1                  := I_input(i).DIFF_1;
      L_input_rec.DIFF_2                  := I_input(i).DIFF_2;
      L_input_rec.DIFF_3                  := I_input(i).DIFF_3;
      L_input_rec.DIFF_4                  := I_input(i).DIFF_4;
      L_input_rec.LOC                     := I_input(i).LOC;
      L_input_rec.LOC_TYPE                := I_input(i).LOC_TYPE;
      L_input_rec.DAILY_WASTE_PCT         := I_input(i).DAILY_WASTE_PCT;
      L_input_rec.UNIT_COST_LOC           := I_input(i).UNIT_COST_LOC;
      L_input_rec.UNIT_RETAIL_LOC         := I_input(i).UNIT_RETAIL_LOC;
      L_input_rec.SELLING_RETAIL_LOC      := I_input(i).SELLING_RETAIL_LOC;
      L_input_rec.SELLING_UOM             := I_input(i).SELLING_UOM;
      L_input_rec.MULTI_UNITS             := I_input(i).MULTI_UNITS;
      L_input_rec.MULTI_UNIT_RETAIL       := I_input(i).MULTI_UNIT_RETAIL;
      L_input_rec.MULTI_SELLING_UOM       := I_input(i).MULTI_SELLING_UOM;
      L_input_rec.ITEM_LOC_STATUS         := I_input(i).ITEM_LOC_STATUS;
      L_input_rec.TAXABLE_IND             := I_input(i).TAXABLE_IND;
      L_input_rec.TI                      := I_input(i).TI;
      L_input_rec.HI                      := I_input(i).HI;
      L_input_rec.STORE_ORD_MULT          := I_input(i).STORE_ORD_MULT;
      L_input_rec.MEAS_OF_EACH            := I_input(i).MEAS_OF_EACH;
      L_input_rec.MEAS_OF_PRICE           := I_input(i).MEAS_OF_PRICE;
      L_input_rec.UOM_OF_PRICE            := I_input(i).UOM_OF_PRICE;
      L_input_rec.PRIMARY_VARIANT         := I_input(i).PRIMARY_VARIANT;
      L_input_rec.PRIMARY_SUPP            := I_input(i).PRIMARY_SUPP;
      L_input_rec.PRIMARY_CNTRY           := I_input(i).PRIMARY_CNTRY;
      L_input_rec.LOCAL_ITEM_DESC         := I_input(i).LOCAL_ITEM_DESC;
      L_input_rec.LOCAL_SHORT_DESC        := I_input(i).LOCAL_SHORT_DESC;
      L_input_rec.PRIMARY_COST_PACK       := I_input(i).PRIMARY_COST_PACK;
      L_input_rec.RECEIVE_AS_TYPE         := I_input(i).RECEIVE_AS_TYPE;
      L_input_rec.STORE_PRICE_IND         := I_input(i).STORE_PRICE_IND;
      L_input_rec.UIN_TYPE                := I_input(i).UIN_TYPE;
      L_input_rec.UIN_LABEL               := I_input(i).UIN_LABEL;
      L_input_rec.CAPTURE_TIME            := I_input(i).CAPTURE_TIME;
      L_input_rec.EXT_UIN_IND             := I_input(i).EXT_UIN_IND;
      L_input_rec.SOURCE_METHOD           := I_input(i).SOURCE_METHOD;
      L_input_rec.SOURCE_WH               := I_input(i).SOURCE_WH;
      L_input_rec.INBOUND_HANDLING_DAYS   := I_input(i).INBOUND_HANDLING_DAYS;
      L_input_rec.CURRENCY_CODE           := I_input(i).CURRENCY_CODE;
      L_input_rec.LIKE_STORE              := I_input(i).LIKE_STORE;
      L_input_rec.DEFAULT_TO_CHILDREN_IND := I_input(i).DEFAULT_TO_CHILDREN_IND;
      L_input_rec.CLASS_VAT_IND           := I_input(i).CLASS_VAT_IND;
      L_input_rec.HIER_LEVEL              := I_input(i).HIER_LEVEL;
      L_input_rec.HIER_NUM_VALUE          := I_input(i).HIER_NUM_VALUE;
      L_input_rec.HIER_CHAR_VALUE         := I_input(i).HIER_CHAR_VALUE;
      L_input_rec.COSTING_LOC             := I_input(i).COSTING_LOC;
      L_input_rec.COSTING_LOC_TYPE        := I_input(i).COSTING_LOC_TYPE;
      L_input_rec.RANGED_IND              := I_input(i).RANGED_IND;
      L_input_rec.DEFAULT_WH              := I_input(i).DEFAULT_WH;
      L_input_rec.ITEM_LOC_IND            := I_input(i).ITEM_LOC_IND;
   
      L_input_tbl(i) := L_input_rec;
   end loop;

   if STGSVC_ITEM_LOC_RANGING_SQL.INSERT_SVC_ITEM_LOC_RANGING(O_error_message,
                                                              O_rms_async_id,
                                                              L_input_tbl) = FALSE then
      return 0;
   end if;

   return 1;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END INSERT_SVC_ITEM_LOC_RANGING_WR;
--------------------------------------------------------------------------------
END STGSVC_ITEM_LOC_RANGING_SQL;
/