CREATE OR REPLACE PACKAGE BODY CORESVC_OUTLOC AS
----------------------------------------------------------------------
LP_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
----------------------------------------------------------------------
   cursor C_SVC_OUTLOC(I_process_id NUMBER,
                       I_chunk_id NUMBER) is
      select pk_outloc.rowid               as pk_outloc_rid,
             st.rowid                      as st_rid,
             oul_vre_fk.rowid              as oul_vre_fk_rid,
             oul_cnt_fk.rowid              as oul_cnt_fk_rid,
             oul_cur_fk.rowid              as oul_cur_fk_rid,
             UPPER(st.outloc_jur_code) as outloc_jur_code,
             st.outloc_name_sec,
             st.contact_email,
             st.contact_telex,
             st.contact_fax,
             st.contact_phone,
             st.contact_name,
             st.outloc_vat_region,
             st.outloc_post,
             UPPER(st.outloc_country_id)   as outloc_country_id,
             pk_outloc.outloc_country_id   as old_outloc_country_id,
             pk_outloc.primary_ind         as old_primary_ind,
             UPPER(st.outloc_state)        as outloc_state,
             UPPER(st.outloc_city) as outloc_city,
             st.outloc_add2,
             st.outloc_add1,
             UPPER(st.outloc_currency) as outloc_currency,
             st.outloc_desc,
             UPPER(st.outloc_id)       as outloc_id,
             st.outloc_type,
             st.primary_ind,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)              as action,
             st.process$status
        from svc_outloc   st,
             outloc       pk_outloc,
             vat_region   oul_vre_fk,
             country      oul_cnt_fk,
             currencies   oul_cur_fk
       where st.process_id          = I_process_id
         and st.chunk_id            = I_chunk_id
         and UPPER(st.outloc_id)           = pk_outloc.outloc_id (+)
         and st.outloc_type         = pk_outloc.outloc_type (+)
         and st.outloc_vat_region   = oul_vre_fk.vat_region (+)
         and UPPER(st.outloc_currency)     = oul_cur_fk.currency_code (+)
         and UPPER(st.outloc_country_id)   = oul_cnt_fk.country_id (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab      errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab  s9t_errors_tab_typ;

   Type OUTLOC_TAB IS TABLE OF OUTLOC_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
----------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets       s9t_pkg.names_map_typ;
   OUTLOC_cols    s9t_pkg.names_map_typ;
   OUTLOC_TL_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                 := s9t_pkg.get_sheet_names(I_file_id);
   OUTLOC_cols              := s9t_pkg.get_col_names(I_file_id,
                                                     OUTLOC_sheet);
   OUTLOC$ACTION            := OUTLOC_cols('ACTION');
   OUTLOC$OUTLOC_TYPE       := OUTLOC_cols('OUTLOC_TYPE');
   OUTLOC$OUTLOC_ID         := OUTLOC_cols('OUTLOC_ID');
   OUTLOC$OUTLOC_DESC       := OUTLOC_cols('OUTLOC_DESC');
   OUTLOC$OUTLOC_CURRENCY   := OUTLOC_cols('OUTLOC_CURRENCY');
   OUTLOC$OUTLOC_ADD1       := OUTLOC_cols('OUTLOC_ADD1');
   OUTLOC$OUTLOC_ADD2       := OUTLOC_cols('OUTLOC_ADD2');
   OUTLOC$OUTLOC_CITY       := OUTLOC_cols('OUTLOC_CITY');
   OUTLOC$OUTLOC_STATE      := OUTLOC_cols('OUTLOC_STATE');
   OUTLOC$OUTLOC_COUNTRY_ID := OUTLOC_cols('OUTLOC_COUNTRY_ID');
   OUTLOC$OUTLOC_POST       := OUTLOC_cols('OUTLOC_POST');
   OUTLOC$OUTLOC_VAT_REGION := OUTLOC_cols('OUTLOC_VAT_REGION');
   OUTLOC$CONTACT_NAME      := OUTLOC_cols('CONTACT_NAME');
   OUTLOC$CONTACT_PHONE     := OUTLOC_cols('CONTACT_PHONE');
   OUTLOC$CONTACT_FAX       := OUTLOC_cols('CONTACT_FAX');
   OUTLOC$CONTACT_TELEX     := OUTLOC_cols('CONTACT_TELEX');
   OUTLOC$CONTACT_EMAIL     := OUTLOC_cols('CONTACT_EMAIL');
   OUTLOC$PRIMARY_IND       := OUTLOC_cols('PRIMARY_IND');
   OUTLOC$OUTLOC_NAME_SEC   := OUTLOC_cols('OUTLOC_NAME_SECONDARY');
   OUTLOC$OUTLOC_JUR_CODE   := OUTLOC_cols('OUTLOC_JURISDICTION_CODE');

   OUTLOC_TL_cols              := s9t_pkg.get_col_names(I_file_id,
                                                        OUTLOC_TL_SHEET);
   OUTLOC_TL$ACTION            := OUTLOC_TL_cols('ACTION');
   OUTLOC_TL$LANG              := OUTLOC_TL_cols('LANG');
   OUTLOC_TL$OUTLOC_TYPE       := OUTLOC_TL_cols('OUTLOC_TYPE');
   OUTLOC_TL$OUTLOC_ID         := OUTLOC_TL_cols('OUTLOC_ID');
   OUTLOC_TL$OUTLOC_DESC       := OUTLOC_TL_cols('OUTLOC_DESC');
   OUTLOC_TL$OUTLOC_ADD1       := OUTLOC_TL_cols('OUTLOC_ADD1');
   OUTLOC_TL$OUTLOC_ADD2       := OUTLOC_TL_cols('OUTLOC_ADD2');
   OUTLOC_TL$OUTLOC_CITY       := OUTLOC_TL_cols('OUTLOC_CITY');
   OUTLOC_TL$CONTACT_NAME      := OUTLOC_TL_cols('CONTACT_NAME');
   OUTLOC_TL$OUTLOC_NAME_SEC   := OUTLOC_TL_cols('OUTLOC_NAME_SECONDARY');

END POPULATE_NAMES;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_OUTLOC( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id  = I_file_id
         and ss.sheet_name = OUTLOC_sheet )
         select s9t_row(s9t_cells(CORESVC_OUTLOC.action_mod ,
                                  outloc_type,
                                  outloc_id,
                                  outloc_desc,
                                  outloc_currency,
                                  outloc_add1,
                                  outloc_add2,
                                  outloc_city,
                                  outloc_state,
                                  outloc_country_id,
                                  outloc_post,
                                  outloc_vat_region,
                                  contact_name,
                                  contact_phone,
                                  contact_fax,
                                  contact_telex,
                                  contact_email,
                                  primary_ind,
                                  outloc_name_secondary,
                                  outloc_jurisdiction_code))
           from outloc;
END POPULATE_OUTLOC;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_OUTLOC_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id  = I_file_id
         and ss.sheet_name = OUTLOC_TL_SHEET )
         select s9t_row(s9t_cells(CORESVC_OUTLOC.action_mod ,
                                  lang,
                                  outloc_type,
                                  outloc_id,
                                  outloc_desc,
                                  outloc_add1,
                                  outloc_add2,
                                  outloc_city,
                                  contact_name,
                                  outloc_name_secondary))
           from outloc_tl;
END POPULATE_OUTLOC_TL;
----------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id IN OUT NUMBER) IS
   L_file       s9t_file;
   L_file_name  s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.ADD_SHEET(OUTLOC_sheet);
   L_file.SHEETS(L_file.get_sheet_index(OUTLOC_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                    'OUTLOC_TYPE',
                                                                                    'OUTLOC_ID',
                                                                                    'OUTLOC_DESC',
                                                                                    'OUTLOC_CURRENCY',
                                                                                    'OUTLOC_ADD1',
                                                                                    'OUTLOC_ADD2',
                                                                                    'OUTLOC_CITY',
                                                                                    'OUTLOC_STATE',
                                                                                    'OUTLOC_COUNTRY_ID',
                                                                                    'OUTLOC_POST',
                                                                                    'OUTLOC_VAT_REGION',
                                                                                    'CONTACT_NAME',
                                                                                    'CONTACT_PHONE',
                                                                                    'CONTACT_FAX',
                                                                                    'CONTACT_TELEX',
                                                                                    'CONTACT_EMAIL',
                                                                                    'PRIMARY_IND',
                                                                                    'OUTLOC_NAME_SECONDARY',
                                                                                    'OUTLOC_JURISDICTION_CODE');
   S9T_PKG.SAVE_OBJ(L_file);

   L_file.ADD_SHEET(OUTLOC_TL_SHEET);
   L_file.SHEETS(L_file.get_sheet_index(OUTLOC_TL_SHEET)).column_headers := s9t_cells( 'ACTION',
                                                                                       'LANG',
                                                                                       'OUTLOC_TYPE',
                                                                                       'OUTLOC_ID',
                                                                                       'OUTLOC_DESC',
                                                                                       'OUTLOC_ADD1',
                                                                                       'OUTLOC_ADD2',
                                                                                       'OUTLOC_CITY',
                                                                                       'CONTACT_NAME',
                                                                                       'OUTLOC_NAME_SECONDARY');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT s9t_folder.file_id%TYPE,
                     I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program  VARCHAR2(64):='CORESVC_OUTLOC.CREATE_S9T';
   L_file     s9t_file;
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE   then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_OUTLOC(O_file_id);
      POPULATE_OUTLOC_TL(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OUTLOC( I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id IN SVC_OUTLOC.process_id%TYPE) IS
   TYPE svc_OUTLOC_col_typ IS TABLE OF SVC_OUTLOC%ROWTYPE;
   L_temp_rec      SVC_OUTLOC%ROWTYPE;
   svc_outloc_col  svc_outloc_col_typ :=NEW svc_OUTLOC_col_typ();
   L_process_id    SVC_OUTLOC.process_id%TYPE;
   L_error         BOOLEAN            :=FALSE;
   L_default_rec   SVC_OUTLOC%ROWTYPE;

   cursor C_MANDATORY_IND is
      select outloc_jur_code_mi,
             outloc_name_sec_mi,
             contact_email_mi,
             contact_telex_mi,
             contact_fax_mi,
             contact_phone_mi,
             contact_name_mi,
             outloc_vat_region_mi,
             outloc_post_mi,
             outloc_country_id_mi,
             outloc_state_mi,
             outloc_city_mi,
             outloc_add2_mi,
             outloc_add1_mi,
             outloc_currency_mi,
             outloc_desc_mi,
             outloc_id_mi,
             outloc_type_mi,
             primary_ind_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = CORESVC_OUTLOC.template_key
                 and wksht_key      = 'OUTLOC' )
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ( 'OUTLOC_JURISDICTION_CODE' as OUTLOC_JUR_CODE,
                                                                 'OUTLOC_NAME_SECONDARY'    as OUTLOC_NAME_SEC,
                                                                 'CONTACT_EMAIL'            as CONTACT_EMAIL,
                                                                 'CONTACT_TELEX'            as CONTACT_TELEX,
                                                                 'CONTACT_FAX'              as CONTACT_FAX,
                                                                 'CONTACT_PHONE'            as CONTACT_PHONE,
                                                                 'CONTACT_NAME'             as CONTACT_NAME,
                                                                 'OUTLOC_VAT_REGION'        as OUTLOC_VAT_REGION,
                                                                 'OUTLOC_POST'              as OUTLOC_POST,
                                                                 'OUTLOC_COUNTRY_ID'        as OUTLOC_COUNTRY_ID,
                                                                 'OUTLOC_STATE'             as OUTLOC_STATE,
                                                                 'OUTLOC_CITY'              as OUTLOC_CITY,
                                                                 'OUTLOC_ADD2'              as OUTLOC_ADD2,
                                                                 'OUTLOC_ADD1'              as OUTLOC_ADD1,
                                                                 'OUTLOC_CURRENCY'          as OUTLOC_CURRENCY,
                                                                 'OUTLOC_DESC'              as OUTLOC_DESC,
                                                                 'OUTLOC_ID'                as OUTLOC_ID,
                                                                 'OUTLOC_TYPE'              as OUTLOC_TYPE,
                                                                 'PRIMARY_IND'              as PRIMARY_IND,
                                                                  NULL                      as DUMMY));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   -- 20743752 BEGIN
   -- L_pk_columns must be set to list of primary key column(s) seperated by comma. Check if length of the string exceeds 255.
   L_table         VARCHAR2(30)   := 'SVC_OUTLOC';
   L_pk_columns    VARCHAR2(255)  := 'Location Type,Location';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
   -- 20743752 END
BEGIN
  -- Get default values.
   FOR rec IN (select outloc_jur_code_dv,
                      outloc_name_sec_dv,
                      contact_email_dv,
                      contact_telex_dv,
                      contact_fax_dv,
                      contact_phone_dv,
                      contact_name_dv,
                      outloc_vat_region_dv,
                      outloc_post_dv,
                      outloc_country_id_dv,
                      outloc_state_dv,
                      outloc_city_dv,
                      outloc_add2_dv,
                      outloc_add1_dv,
                      outloc_currency_dv,
                      outloc_desc_dv,
                      outloc_id_dv,
                      outloc_type_dv,
                      primary_ind_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key   = CORESVC_OUTLOC.template_key
                          and wksht_key      = 'OUTLOC' )
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('OUTLOC_JURISDICTION_CODE' as OUTLOC_JUR_CODE,
                                                                             'OUTLOC_NAME_SECONDARY'    as OUTLOC_NAME_SEC,
                                                                             'CONTACT_EMAIL'            as CONTACT_EMAIL,
                                                                             'CONTACT_TELEX'            as CONTACT_TELEX,
                                                                             'CONTACT_FAX'              as CONTACT_FAX,
                                                                             'CONTACT_PHONE'            as CONTACT_PHONE,
                                                                             'CONTACT_NAME'             as CONTACT_NAME,
                                                                             'OUTLOC_VAT_REGION'        as OUTLOC_VAT_REGION,
                                                                             'OUTLOC_POST'              as OUTLOC_POST,
                                                                             'OUTLOC_COUNTRY_ID'        as OUTLOC_COUNTRY_ID,
                                                                             'OUTLOC_STATE'             as OUTLOC_STATE,
                                                                             'OUTLOC_CITY'              as OUTLOC_CITY,
                                                                             'OUTLOC_ADD2'              as OUTLOC_ADD2,
                                                                             'OUTLOC_ADD1'              as OUTLOC_ADD1,
                                                                             'OUTLOC_CURRENCY'          as OUTLOC_CURRENCY,
                                                                             'OUTLOC_DESC'              as OUTLOC_DESC,
                                                                             'OUTLOC_ID'                as OUTLOC_ID,
                                                                             'OUTLOC_TYPE'              as OUTLOC_TYPE,
                                                                             'PRIMARY_IND'              as PRIMARY_IND,
                                                                              NULL                      as DUMMY)))   LOOP
   BEGIN
      L_default_rec.OUTLOC_JUR_CODE := rec.OUTLOC_JUR_CODE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_JURISDICTION_CODE ' ,
                         NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.OUTLOC_NAME_SEC := rec.OUTLOC_NAME_SEC_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_NAME_SECONDARY ' ,
                         NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CONTACT_EMAIL := rec.CONTACT_EMAIL_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'CONTACT_EMAIL ' ,
                         NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CONTACT_TELEX := rec.CONTACT_TELEX_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'CONTACT_TELEX ' ,
                         NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CONTACT_FAX := rec.CONTACT_FAX_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'CONTACT_FAX ' ,
                         NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CONTACT_PHONE := rec.CONTACT_PHONE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'CONTACT_PHONE ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.CONTACT_NAME := rec.CONTACT_NAME_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'CONTACT_NAME ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_VAT_REGION := rec.OUTLOC_VAT_REGION_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_VAT_REGION ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_POST := rec.OUTLOC_POST_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_POST ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_COUNTRY_ID := rec.OUTLOC_COUNTRY_ID_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_COUNTRY_ID ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_STATE := rec.OUTLOC_STATE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_STATE ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_CITY := rec.OUTLOC_CITY_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_CITY ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_ADD2 := rec.OUTLOC_ADD2_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_ADD2 ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_ADD1 := rec.OUTLOC_ADD1_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_ADD1 ' ,
                         'INV_DEFAULT',
                         SQLERRM);
   END;
   BEGIN
      L_default_rec.OUTLOC_CURRENCY := rec.OUTLOC_CURRENCY_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_CURRENCY ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_DESC := rec.OUTLOC_DESC_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_DESC ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_ID := rec.OUTLOC_ID_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_ID ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.OUTLOC_TYPE := rec.OUTLOC_TYPE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'OUTLOC_TYPE ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.PRIMARY_IND := rec.PRIMARY_IND_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'OUTLOC ' ,
                         NULL,
                         'PRIMARY_IND ' ,
                         NULL,
                         'INV_DEFAULT');
   END;
   END LOOP;

 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(OUTLOC$ACTION)            as ACTION,
          UPPER(r.get_cell(OUTLOC$OUTLOC_JUR_CODE))   as OUTLOC_JUR_CODE,
          r.get_cell(OUTLOC$OUTLOC_NAME_SEC)   as OUTLOC_NAME_SEC,
          r.get_cell(OUTLOC$CONTACT_EMAIL)     as CONTACT_EMAIL,
          r.get_cell(OUTLOC$CONTACT_TELEX)     as CONTACT_TELEX,
          r.get_cell(OUTLOC$CONTACT_FAX)       as CONTACT_FAX,
          r.get_cell(OUTLOC$CONTACT_PHONE)     as CONTACT_PHONE,
          r.get_cell(OUTLOC$CONTACT_NAME)      as CONTACT_NAME,
          r.get_cell(OUTLOC$OUTLOC_VAT_REGION) as OUTLOC_VAT_REGION,
          r.get_cell(OUTLOC$OUTLOC_POST)       as OUTLOC_POST,
          UPPER(r.get_cell(OUTLOC$OUTLOC_COUNTRY_ID)) as OUTLOC_COUNTRY_ID,
          UPPER(r.get_cell(OUTLOC$OUTLOC_STATE))      as OUTLOC_STATE,
          UPPER(r.get_cell(OUTLOC$OUTLOC_CITY))       as OUTLOC_CITY,
          r.get_cell(OUTLOC$OUTLOC_ADD2)       as OUTLOC_ADD2,
          r.get_cell(OUTLOC$OUTLOC_ADD1)       as OUTLOC_ADD1,
          UPPER(r.get_cell(OUTLOC$OUTLOC_CURRENCY))   as OUTLOC_CURRENCY,
          r.get_cell(OUTLOC$OUTLOC_DESC)       as OUTLOC_DESC,
          UPPER(r.get_cell(OUTLOC$OUTLOC_ID))         as OUTLOC_ID,
          r.get_cell(OUTLOC$OUTLOC_TYPE)       as OUTLOC_TYPE,
          r.get_cell(OUTLOC$PRIMARY_IND)       as PRIMARY_IND,
          r.get_row_seq()                      as ROW_SEQ
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
    where sf.file_id    = I_file_id
      and ss.sheet_name = sheet_name_trans(OUTLOC_sheet))
LOOP
    L_temp_rec                   := NULL;
    L_temp_rec.process_id        := I_process_id;
    L_temp_rec.chunk_id          := 1;
    L_temp_rec.row_seq           := rec.row_seq;
    L_temp_rec.process$status    := 'N';
    L_temp_rec.create_id         := GET_USER;
    L_temp_rec.last_upd_id       := GET_USER;
    L_temp_rec.create_datetime   := SYSDATE;
    L_temp_rec.last_upd_datetime := SYSDATE;
    L_error                      := FALSE;

   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         OUTLOC_sheet,
                         rec.row_seq,
                         action_column,
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_JUR_CODE := rec.OUTLOC_JUR_CODE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_JURISDICTION_CODE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_NAME_SEC := rec.OUTLOC_NAME_SEC;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_NAME_SECONDARY',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CONTACT_EMAIL := rec.CONTACT_EMAIL;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'CONTACT_EMAIL',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CONTACT_TELEX := rec.CONTACT_TELEX;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'CONTACT_TELEX',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CONTACT_FAX := rec.CONTACT_FAX;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'CONTACT_FAX',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CONTACT_PHONE := rec.CONTACT_PHONE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'CONTACT_PHONE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CONTACT_NAME := rec.CONTACT_NAME;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'CONTACT_NAME',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_VAT_REGION := rec.OUTLOC_VAT_REGION;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_VAT_REGION',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_POST := rec.OUTLOC_POST;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_POST',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_COUNTRY_ID := rec.OUTLOC_COUNTRY_ID;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_COUNTRY_ID',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_STATE := rec.OUTLOC_STATE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_STATE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_CITY := rec.OUTLOC_CITY;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_CITY',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_ADD2 := rec.OUTLOC_ADD2;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_ADD2',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_ADD1 := rec.OUTLOC_ADD1;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_ADD1',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_CURRENCY := rec.OUTLOC_CURRENCY;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_CURRENCY',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_DESC := rec.OUTLOC_DESC;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_DESC',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_ID := rec.OUTLOC_ID;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_ID',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.OUTLOC_TYPE := rec.OUTLOC_TYPE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'OUTLOC_TYPE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.PRIMARY_IND := rec.PRIMARY_IND;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          rec.row_seq,
                         'PRIMARY_IND',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   if rec.action = CORESVC_OUTLOC.action_new then
      L_temp_rec.OUTLOC_JUR_CODE    := NVL( L_temp_rec.OUTLOC_JUR_CODE,
                                            L_default_rec.OUTLOC_JUR_CODE);
      L_temp_rec.OUTLOC_NAME_SEC    := NVL( L_temp_rec.OUTLOC_NAME_SEC,
                                            L_default_rec.OUTLOC_NAME_SEC);
      L_temp_rec.CONTACT_EMAIL      := NVL( L_temp_rec.CONTACT_EMAIL,
                                            L_default_rec.CONTACT_EMAIL);
      L_temp_rec.CONTACT_TELEX      := NVL( L_temp_rec.CONTACT_TELEX,
                                            L_default_rec.CONTACT_TELEX);
      L_temp_rec.CONTACT_FAX        := NVL( L_temp_rec.CONTACT_FAX,
                                            L_default_rec.CONTACT_FAX);
      L_temp_rec.CONTACT_PHONE      := NVL( L_temp_rec.CONTACT_PHONE,
                                            L_default_rec.CONTACT_PHONE);
      L_temp_rec.CONTACT_NAME       := NVL( L_temp_rec.CONTACT_NAME,
                                            L_default_rec.CONTACT_NAME);
      L_temp_rec.OUTLOC_VAT_REGION  := NVL( L_temp_rec.OUTLOC_VAT_REGION,
                                            L_default_rec.OUTLOC_VAT_REGION);
      L_temp_rec.OUTLOC_POST        := NVL( L_temp_rec.OUTLOC_POST,
                                            L_default_rec.OUTLOC_POST);
      L_temp_rec.OUTLOC_COUNTRY_ID  := NVL( L_temp_rec.OUTLOC_COUNTRY_ID,
                                            L_default_rec.OUTLOC_COUNTRY_ID);
      L_temp_rec.OUTLOC_STATE       := NVL( L_temp_rec.OUTLOC_STATE,
                                            L_default_rec.OUTLOC_STATE);
      L_temp_rec.OUTLOC_CITY        := NVL( L_temp_rec.OUTLOC_CITY,
                                            L_default_rec.OUTLOC_CITY);
      L_temp_rec.OUTLOC_ADD2        := NVL( L_temp_rec.OUTLOC_ADD2,
                                            L_default_rec.OUTLOC_ADD2);
      L_temp_rec.OUTLOC_ADD1        := NVL( L_temp_rec.OUTLOC_ADD1,
                                            L_default_rec.OUTLOC_ADD1);
      L_temp_rec.OUTLOC_CURRENCY    := NVL( L_temp_rec.OUTLOC_CURRENCY,
                                            L_default_rec.OUTLOC_CURRENCY);
      L_temp_rec.OUTLOC_DESC        := NVL( L_temp_rec.OUTLOC_DESC,
                                            L_default_rec.OUTLOC_DESC);
      L_temp_rec.OUTLOC_ID          := NVL( L_temp_rec.OUTLOC_ID,
                                            L_default_rec.OUTLOC_ID);
      L_temp_rec.OUTLOC_TYPE        := NVL( L_temp_rec.OUTLOC_TYPE,
                                            L_default_rec.OUTLOC_TYPE);
      L_temp_rec.PRIMARY_IND        := NVL( L_temp_rec.PRIMARY_IND,
                                            L_default_rec.PRIMARY_IND);
   end if;

   if NOT (L_temp_rec.OUTLOC_ID is NOT NULL
           and L_temp_rec.OUTLOC_TYPE is NOT NULL )   then
       WRITE_S9T_ERROR( I_file_id,
                       OUTLOC_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
      L_error := TRUE;
   end if;

   if NOT L_error then
      svc_outloc_col.EXTEND();
      svc_outloc_col(svc_OUTLOC_col.COUNT()):=L_temp_rec;
   end if;

   END LOOP;
BEGIN
   forall i IN 1..svc_OUTLOC_col.COUNT SAVE EXCEPTIONS
      merge into SVC_OUTLOC st
      using(select (case
                       when L_mi_rec.OUTLOC_JUR_CODE_mi = 'N'      and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_JUR_CODE IS NULL   then
                            mt.OUTLOC_JURISDICTION_CODE
                       else s1.OUTLOC_JUR_CODE
                       end) as OUTLOC_JUR_CODE,
                   (case
                       when L_mi_rec.OUTLOC_NAME_SEC_mi = 'N'      and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_NAME_SEC IS NULL   then
                            mt.OUTLOC_NAME_SECONDARY
                       else s1.OUTLOC_NAME_SEC
                       end) as OUTLOC_NAME_SEC,
                   (case
                       when L_mi_rec.CONTACT_EMAIL_mi   = 'N'      and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.CONTACT_EMAIL IS NULL   then
                            mt.CONTACT_EMAIL
                       else s1.CONTACT_EMAIL
                       end) as CONTACT_EMAIL,
                   (case
                       when L_mi_rec.CONTACT_TELEX_mi   = 'N'      and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.CONTACT_TELEX IS NULL   then
                            mt.CONTACT_TELEX
                       else s1.CONTACT_TELEX
                       end) as CONTACT_TELEX,
                   (case
                       when L_mi_rec.CONTACT_FAX_mi     = 'N'      and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.CONTACT_FAX IS NULL   then
                            mt.CONTACT_FAX
                       else s1.CONTACT_FAX
                       end) as CONTACT_FAX,
                   (case
                       when L_mi_rec.CONTACT_PHONE_mi    = 'N'     and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.CONTACT_PHONE IS NULL   then
                            mt.CONTACT_PHONE
                       else s1.CONTACT_PHONE
                       end) as CONTACT_PHONE,
                   (case
                       when L_mi_rec.CONTACT_NAME_mi         = 'N' and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.CONTACT_NAME IS NULL   then
                            mt.CONTACT_NAME
                       else s1.CONTACT_NAME
                       end) as CONTACT_NAME,
                   (case
                       when L_mi_rec.OUTLOC_VAT_REGION_mi    = 'N' and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_VAT_REGION IS NULL   then
                            mt.OUTLOC_VAT_REGION
                       else s1.OUTLOC_VAT_REGION
                       end) as OUTLOC_VAT_REGION,
                   (case
                       when L_mi_rec.OUTLOC_POST_mi          = 'N' and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_POST IS NULL   then
                            mt.OUTLOC_POST
                       else s1.OUTLOC_POST
                       end) as OUTLOC_POST,
                   (case
                       when L_mi_rec.OUTLOC_COUNTRY_ID_mi    = 'N' and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_COUNTRY_ID IS NULL   then
                            mt.OUTLOC_COUNTRY_ID
                       else s1.OUTLOC_COUNTRY_ID
                       end) as OUTLOC_COUNTRY_ID,
                   (case
                       when L_mi_rec.OUTLOC_STATE_mi         = 'N' and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_STATE IS NULL   then
                            mt.OUTLOC_STATE
                       else s1.OUTLOC_STATE
                       end) as OUTLOC_STATE,
                   (case
                       when L_mi_rec.OUTLOC_CITY_mi          = 'N' and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_CITY IS NULL   then
                            mt.OUTLOC_CITY
                       else s1.OUTLOC_CITY
                       end) as OUTLOC_CITY,
                   (case
                       when L_mi_rec.OUTLOC_ADD2_mi          = 'N' and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_ADD2 IS NULL   then
                            mt.OUTLOC_ADD2
                       else s1.OUTLOC_ADD2
                       end) as OUTLOC_ADD2,
                   (case
                       when L_mi_rec.OUTLOC_ADD1_mi    = 'N'       and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_ADD1 IS NULL   then
                            mt.OUTLOC_ADD1
                       else s1.OUTLOC_ADD1
                       end) as OUTLOC_ADD1,
                   (case
                       when L_mi_rec.OUTLOC_CURRENCY_mi    = 'N'   and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_CURRENCY IS NULL   then
                            mt.OUTLOC_CURRENCY
                       else s1.OUTLOC_CURRENCY
                       end) as OUTLOC_CURRENCY,
                   (case
                       when L_mi_rec.OUTLOC_DESC_mi        = 'N'   and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_DESC IS NULL   then
                            mt.OUTLOC_DESC
                       else s1.OUTLOC_DESC
                       end) as OUTLOC_DESC,
                   (case
                       when L_mi_rec.OUTLOC_ID_mi          = 'N'   and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_ID IS NULL   then
                            mt.OUTLOC_ID
                       else s1.OUTLOC_ID
                       end) as OUTLOC_ID,
                   (case
                       when L_mi_rec.OUTLOC_TYPE_mi        = 'N'   and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.OUTLOC_TYPE IS NULL   then
                            mt.OUTLOC_TYPE
                       else s1.OUTLOC_TYPE
                       end) as OUTLOC_TYPE,
                   (case
                       when L_mi_rec.PRIMARY_IND_mi        = 'N'   and
                            svc_OUTLOC_col(i).action = CORESVC_OUTLOC.action_mod and
                            s1.PRIMARY_IND IS NULL   then
                            mt.PRIMARY_IND
                       else s1.PRIMARY_IND
                       end) as PRIMARY_IND,
                       null as dummy
              from (select svc_outloc_col(i).OUTLOC_JUR_CODE   as OUTLOC_JUR_CODE,
                           svc_outloc_col(i).OUTLOC_NAME_SEC   as OUTLOC_NAME_SEC,
                           svc_outloc_col(i).CONTACT_EMAIL     as CONTACT_EMAIL,
                           svc_outloc_col(i).CONTACT_TELEX     as CONTACT_TELEX,
                           svc_outloc_col(i).CONTACT_FAX       as CONTACT_FAX,
                           svc_outloc_col(i).CONTACT_PHONE     as CONTACT_PHONE,
                           svc_outloc_col(i).CONTACT_NAME      as CONTACT_NAME,
                           svc_outloc_col(i).OUTLOC_VAT_REGION as OUTLOC_VAT_REGION,
                           svc_outloc_col(i).OUTLOC_POST       as OUTLOC_POST,
                           svc_outloc_col(i).OUTLOC_COUNTRY_ID as OUTLOC_COUNTRY_ID,
                           svc_outloc_col(i).OUTLOC_STATE      as OUTLOC_STATE,
                           svc_outloc_col(i).OUTLOC_CITY       as OUTLOC_CITY,
                           svc_outloc_col(i).OUTLOC_ADD2       as OUTLOC_ADD2,
                           svc_outloc_col(i).OUTLOC_ADD1       as OUTLOC_ADD1,
                           svc_outloc_col(i).OUTLOC_CURRENCY   as OUTLOC_CURRENCY,
                           svc_outloc_col(i).OUTLOC_DESC       as OUTLOC_DESC,
                           svc_outloc_col(i).OUTLOC_ID         as OUTLOC_ID,
                           svc_outloc_col(i).OUTLOC_TYPE       as OUTLOC_TYPE,
                           svc_outloc_col(i).PRIMARY_IND       as PRIMARY_IND,
                           NULL                                as DUMMY
                      from dual ) s1,
                           OUTLOC mt
                     where mt.OUTLOC_ID (+)    = s1.OUTLOC_ID
                           and mt.OUTLOC_TYPE (+)  = s1.OUTLOC_TYPE )sq
         on (st.OUTLOC_ID   = sq.OUTLOC_ID and
             st.OUTLOC_TYPE = sq.OUTLOC_TYPE and
             svc_OUTLOC_col(i).ACTION IN (CORESVC_OUTLOC.action_mod,CORESVC_OUTLOC.action_del))
         when matched then
            update
               set process_id          = svc_OUTLOC_col(i).process_id ,
                   chunk_id            = svc_OUTLOC_col(i).chunk_id ,
                   row_seq             = svc_OUTLOC_col(i).row_seq ,
                   action              = svc_OUTLOC_col(i).action ,
                   process$status      = svc_OUTLOC_col(i).process$status ,
                   outloc_desc         = sq.outloc_desc ,
                   outloc_currency     = sq.outloc_currency ,
                   contact_email       = sq.contact_email ,
                   primary_ind         = sq.primary_ind ,
                   outloc_name_sec     = sq.outloc_name_sec ,
                   contact_fax         = sq.contact_fax ,
                   outloc_vat_region   = sq.outloc_vat_region ,
                   contact_telex       = sq.contact_telex ,
                   contact_name        = sq.contact_name ,
                   contact_phone       = sq.contact_phone ,
                   outloc_country_id   = sq.outloc_country_id ,
                   outloc_city         = sq.outloc_city ,
                   outloc_jur_code     = sq.outloc_jur_code ,
                   outloc_add1         = sq.outloc_add1 ,
                   outloc_state        = sq.outloc_state ,
                   outloc_post         = sq.outloc_post ,
                   outloc_add2         = sq.outloc_add2 ,
                   create_id           = svc_outloc_col(i).create_id ,
                   create_datetime     = svc_outloc_col(i).create_datetime ,
                   last_upd_id         = svc_outloc_col(i).last_upd_id ,
                   last_upd_datetime   = svc_outloc_col(i).last_upd_datetime
         when NOT matched then
            insert (process_id,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    outloc_jur_code ,
                    outloc_name_sec ,
                    contact_email ,
                    contact_telex ,
                    contact_fax ,
                    contact_phone ,
                    contact_name ,
                    outloc_vat_region ,
                    outloc_post ,
                    outloc_country_id ,
                    outloc_state ,
                    outloc_city ,
                    outloc_add2 ,
                    outloc_add1 ,
                    outloc_currency ,
                    outloc_desc ,
                    outloc_id ,
                    outloc_type ,
                    primary_ind ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime)
             values (svc_OUTLOC_col(i).process_id ,
                     svc_OUTLOC_col(i).chunk_id ,
                     svc_OUTLOC_col(i).row_seq ,
                     svc_OUTLOC_col(i).action ,
                     svc_OUTLOC_col(i).process$status ,
                     sq.outloc_jur_code ,
                     sq.outloc_name_sec ,
                     sq.contact_email ,
                     sq.contact_telex ,
                     sq.contact_fax ,
                     sq.contact_phone ,
                     sq.contact_name ,
                     sq.outloc_vat_region ,
                     sq.outloc_post ,
                     sq.outloc_country_id ,
                     sq.outloc_state ,
                     sq.outloc_city ,
                     sq.outloc_add2 ,
                     sq.outloc_add1 ,
                     sq.outloc_currency ,
                     sq.outloc_desc ,
                     sq.outloc_id ,
                     sq.outloc_type ,
                     sq.primary_ind ,
                     svc_OUTLOC_col(i).create_id ,
                     svc_OUTLOC_col(i).create_datetime ,
                     svc_OUTLOC_col(i).last_upd_id ,
                     svc_OUTLOC_col(i).last_upd_datetime );
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
         WRITE_S9T_ERROR( I_file_id,
                          OUTLOC_sheet,
                          svc_outloc_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);

      END LOOP;
END;
END PROCESS_S9T_OUTLOC;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_OUTLOC_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id   IN   SVC_OUTLOC_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_OUTLOC_TL_COL_TYP IS TABLE OF SVC_OUTLOC_TL%ROWTYPE;
   L_temp_rec          SVC_OUTLOC_TL%ROWTYPE;
   SVC_OUTLOC_TL_COL   SVC_OUTLOC_TL_COL_TYP := NEW SVC_OUTLOC_TL_COL_TYP();
   L_process_id        SVC_OUTLOC_TL.PROCESS_ID%TYPE;
   L_error             BOOLEAN := FALSE;
   L_default_rec       SVC_OUTLOC_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             outloc_type_mi,
             outloc_id_mi,
             outloc_desc_mi,
             outloc_add1_mi,
             outloc_add2_mi,
             outloc_city_mi,
             contact_name_mi,
             outloc_name_secondary_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'OUTLOC_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('LANG'              as lang,
                                       'OUTLOC_TYPE'       as outloc_type,
                                       'OUTLOC_ID'         as outloc_id,
                                       'OUTLOC_DESC'       as outloc_desc,
                                       'OUTLOC_ADD1'       as outloc_add1,
                                       'OUTLOC_ADD2'       as outloc_add2,
                                       'OUTLOC_CITY'       as outloc_city,
                                       'CONTACT_NAME'      as contact_name,
                                       'OUTLOC_NAME_SECONDARY'  as outloc_name_secondary));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_OUTLOC_TL';
   L_pk_columns    VARCHAR2(255)  := 'Outloc Type, Outloc ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select lang_dv,
                      outloc_type_dv,
                      outloc_id_dv,
                      outloc_desc_dv,
                      outloc_add1_dv,
                      outloc_add2_dv,
                      outloc_city_dv,
                      contact_name_dv,
                      outloc_name_secondary_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'OUTLOC_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('LANG'              as lang,
                                                'OUTLOC_TYPE'       as outloc_type,
                                                'OUTLOC_ID'         as outloc_id,
                                                'OUTLOC_DESC'       as outloc_desc,
                                                'OUTLOC_ADD1'       as outloc_add1,
                                                'OUTLOC_ADD2'       as outloc_add2,
                                                'OUTLOC_CITY'       as outloc_city,
                                                'CONTACT_NAME'      as contact_name,
                                                'OUTLOC_NAME_SECONDARY'  as outloc_name_secondary)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.outloc_type := rec.outloc_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_TYPE' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         l_default_rec.lang := rec.outloc_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         l_default_rec.outloc_desc := rec.outloc_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         l_default_rec.outloc_add1 := rec.outloc_add1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_ADD1' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         l_default_rec.outloc_add2 := rec.outloc_add2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_ADD2' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         l_default_rec.outloc_city := rec.outloc_city_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_CITY' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         l_default_rec.contact_name := rec.contact_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'CONTACT_NAME' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         l_default_rec.outloc_name_secondary := rec.outloc_name_secondary_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_NAME_SECONDARY' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(outloc_TL$action))         as action,
                      r.get_cell(outloc_TL$lang)                  as lang,
                      r.get_cell(outloc_TL$outloc_type)           as outloc_type,
                      UPPER(r.get_cell(outloc_TL$outloc_id))      as outloc_id,
                      r.get_cell(outloc_TL$outloc_desc)           as outloc_desc,
                      r.get_cell(outloc_TL$outloc_add1)           as outloc_add1,
                      r.get_cell(outloc_TL$outloc_add2)           as outloc_add2,
                      UPPER(r.get_cell(outloc_TL$outloc_city))    as outloc_city,
                      r.get_cell(outloc_TL$contact_name)          as contact_name,
                      r.get_cell(outloc_TL$outloc_name_sec)       as outloc_name_secondary,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(OUTLOC_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            SQLCODE,
                            SQLERRM);
      END;
      BEGIN
         L_temp_rec.outloc_type := rec.outloc_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_TYPE' ,
                            SQLCODE,
                            SQLERRM);
      END;
      BEGIN
         L_temp_rec.outloc_id := rec.outloc_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_ID' ,
                            SQLCODE,
                            SQLERRM);
      END;
      BEGIN
         L_temp_rec.outloc_desc := rec.outloc_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_DESC' ,
                            SQLCODE,
                            SQLERRM);
      END;
      BEGIN
         L_temp_rec.outloc_add1 := rec.outloc_add1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_ADD1' ,
                            SQLCODE,
                            SQLERRM);
      END;
      BEGIN
         L_temp_rec.outloc_add2 := rec.outloc_add2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_ADD2' ,
                            SQLCODE,
                            SQLERRM);
      END;
      BEGIN
         L_temp_rec.outloc_city := rec.outloc_city;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_CITY' ,
                            SQLCODE,
                            SQLERRM);
      END;
      BEGIN
         L_temp_rec.contact_name := rec.contact_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'CONTACT_NAME' ,
                            SQLCODE,
                            SQLERRM);
      END;
      BEGIN
         L_temp_rec.outloc_name_secondary := rec.outloc_name_secondary;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            OUTLOC_TL_SHEET ,
                            NULL,
                           'OUTLOC_NAME_SECONDARY' ,
                            SQLCODE,
                            SQLERRM);
      END;
      if rec.action = CORESVC_OUTLOC.action_new then
         L_temp_rec.lang                       := NVL( L_temp_rec.lang,L_default_rec.lang);
         L_temp_rec.outloc_type                := NVL( L_temp_rec.outloc_type,L_default_rec.outloc_type);
         L_temp_rec.outloc_id                  := NVL( L_temp_rec.outloc_id,L_default_rec.outloc_id);
         L_temp_rec.outloc_desc                := NVL( L_temp_rec.outloc_desc,L_default_rec.outloc_desc);
         L_temp_rec.outloc_add1                := NVL( L_temp_rec.outloc_add1,L_default_rec.outloc_add1);
         L_temp_rec.outloc_add2                := NVL( L_temp_rec.outloc_add2,L_default_rec.outloc_add2);
         L_temp_rec.outloc_city                := NVL( L_temp_rec.outloc_city,L_default_rec.outloc_city);
         L_temp_rec.contact_name               := NVL( L_temp_rec.contact_name,L_default_rec.contact_name);
         L_temp_rec.outloc_name_secondary      := NVL( L_temp_rec.outloc_name_secondary,L_default_rec.outloc_name_secondary);
      end if;
      if NOT (L_temp_rec.outloc_type is NOT NULL and L_temp_rec.outloc_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         OUTLOC_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_outloc_TL_col.extend();
         svc_outloc_TL_col(svc_outloc_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_outloc_TL_col.COUNT SAVE EXCEPTIONS
      merge into svc_outloc_tl st
      using(select
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.outloc_type_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.outloc_type IS NULL then
                        mt.outloc_type
                   else s1.outloc_type
                   end) as outloc_type,
                  (case
                   when l_mi_rec.outloc_id_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.outloc_id IS NULL then
                        mt.outloc_id
                   else s1.outloc_id
                   end) as outloc_id,
                  (case
                   when l_mi_rec.outloc_desc_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.outloc_desc IS NULL then
                        mt.outloc_desc
                   else s1.outloc_desc
                   end) as outloc_desc,
                  (case
                   when l_mi_rec.outloc_add1_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.outloc_add1 IS NULL then
                        mt.outloc_add1
                   else s1.outloc_add1
                   end) as outloc_add1,
                  (case
                   when l_mi_rec.outloc_add2_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.outloc_add2 IS NULL then
                        mt.outloc_add2
                   else s1.outloc_add2
                   end) as outloc_add2,
                  (case
                   when l_mi_rec.outloc_city_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.outloc_city IS NULL then
                        mt.outloc_city
                   else s1.outloc_city
                   end) as outloc_city,
                  (case
                   when l_mi_rec.contact_name_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.contact_name IS NULL then
                        mt.contact_name
                   else s1.contact_name
                   end) as contact_name,
                  (case
                   when l_mi_rec.outloc_name_secondary_mi = 'N'
                    and svc_outloc_TL_col(i).action = CORESVC_OUTLOC.action_mod
                    and s1.outloc_name_secondary IS NULL then
                        mt.outloc_name_secondary
                   else s1.outloc_name_secondary
                   end) as outloc_name_secondary
              from (select svc_outloc_tl_col(i).lang                    as lang,
                           svc_outloc_tl_col(i).outloc_type             as outloc_type,
                           svc_outloc_tl_col(i).outloc_id               as outloc_id,
                           svc_outloc_tl_col(i).outloc_desc             as outloc_desc,
                           svc_outloc_tl_col(i).outloc_add1             as outloc_add1,
                           svc_outloc_tl_col(i).outloc_add2             as outloc_add2,
                           svc_outloc_tl_col(i).outloc_city             as outloc_city,
                           svc_outloc_tl_col(i).contact_name            as contact_name,
                           svc_outloc_tl_col(i).outloc_name_secondary   as outloc_name_secondary
                      from dual) s1,
                   outloc_TL mt
             where mt.lang (+)         = s1.lang
               and mt.outloc_type (+)  = s1.outloc_type
               and mt.outloc_id (+)    = s1.outloc_id) sq
                on (st.lang = sq.lang and
                    st.outloc_type = sq.outloc_type and
                    st.outloc_id = sq.outloc_id and
                    svc_outloc_TL_col(i).ACTION IN (CORESVC_OUTLOC.action_mod,CORESVC_OUTLOC.action_del))
      when matched then
      update
         set process_id        = svc_outloc_TL_col(i).process_id ,
             chunk_id          = svc_outloc_TL_col(i).chunk_id ,
             row_seq           = svc_outloc_TL_col(i).row_seq ,
             action            = svc_outloc_TL_col(i).action ,
             process$status    = svc_outloc_TL_col(i).process$status ,
             outloc_desc       = sq.outloc_desc,
             outloc_add1       = sq.outloc_add1,
             outloc_add2       = sq.outloc_add2,
             outloc_city       = sq.outloc_city,
             contact_name      = sq.contact_name,
             outloc_name_secondary = sq.outloc_name_secondary
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             lang,
             outloc_type,
             outloc_id,
             outloc_desc,
             outloc_add1,
             outloc_add2,
             outloc_city,
             contact_name,
             outloc_name_secondary)
      values(svc_outloc_TL_col(i).process_id ,
             svc_outloc_TL_col(i).chunk_id ,
             svc_outloc_TL_col(i).row_seq ,
             svc_outloc_TL_col(i).action ,
             svc_outloc_TL_col(i).process$status ,
             sq.lang,
             sq.outloc_type,
             sq.outloc_id,
             sq.outloc_desc,
             sq.outloc_add1,
             sq.outloc_add2,
             sq.outloc_city,
             sq.contact_name,
             sq.outloc_name_secondary);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            OUTLOC_TL_SHEET,
                            svc_outloc_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_OUTLOC_TL;
----------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count   IN OUT NUMBER,
                     I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_OUTLOC.PROCESS_S9T';
   L_file           S9T_FILE;
   L_sheets         S9T_PKG.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR    EXCEPTION;
   PRAGMA    EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE   then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_OUTLOC(I_file_id,I_process_id);
      PROCESS_S9T_OUTLOC_TL(I_file_id,I_process_id);
   end if;

   o_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..o_error_count
     insert into s9t_errors
          values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;

      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
         values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_STATECOUNTRYJURIS(O_state             IN OUT state.state%TYPE,
                                    O_state_desc        IN OUT state.description%TYPE,
                                    O_country_id        IN OUT country.country_id%TYPE,
                                    O_country_desc      IN OUT country.country_desc%TYPE,
                                    O_jurisdiction_code IN OUT country_tax_jurisdiction.jurisdiction_code%TYPE,
                                    O_jurisdiction_desc IN OUT country_tax_jurisdiction.jurisdiction_desc%TYPE,
                                    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error             IN OUT BOOLEAN,
                                    I_rec               IN OUT C_SVC_OUTLOC%ROWTYPE,
                                    I_calling_module    IN     VARCHAR2,
                                    I_calling_context   IN     VARCHAR2)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_OUTLOC.VALIDATE_STATECOUNTRYJURIS';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OUTLOC';
   L_dup_exists       VARCHAR2(1)                       := 'N';
   L_error_message    RTK_ERRORS.RTK_TEXT%TYPE;
   L_calling_context  VARCHAR2(12);
BEGIN
   L_calling_context := I_calling_context;
   if (L_calling_context = ADDRESS_SQL.COUNTRY_CON
       and O_country_id is NOT NULL )
       or (L_calling_context = ADDRESS_SQL.STATE_CON
       and O_state is NOT NULL)
       or (L_calling_context = ADDRESS_SQL.JURIS_CON
       and O_jurisdiction_code is NOT NULL)   then

       if O_jurisdiction_code is NOT NULL   then
          L_calling_context := ADDRESS_SQL.JURIS_CON;
       elsif O_state is NOT NULL   then
          L_calling_context := ADDRESS_SQL.STATE_CON;
       end if;

       if ADDRESS_SQL.CHECK_ADDR (O_error_message,
                                  O_country_id,
                                  O_country_desc,
                                  O_STATE,
                                  O_state_desc,
                                  O_jurisdiction_code,
                                  O_jurisdiction_desc,
                                  L_dup_exists,
                                  I_calling_module,
                                  L_calling_context) = FALSE then
          O_error := TRUE;
          WRITE_ERROR(I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'County,State,Jurisdiction',
                      O_error_message);
       end if;

       if L_dup_exists = 'Y' then
          O_error := TRUE;
          WRITE_ERROR(I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'County,State,Jurisdiction',
                      O_error_message);
       end if;

   else
      if I_calling_module = ADDRESS_SQL.COUNTRY_CON then
         O_country_desc      := NULL;
         O_jurisdiction_code := NULL;
         O_jurisdiction_desc := NULL;
         O_state             := NULL;
         O_state_desc := NULL;
      elsif I_calling_module = ADDRESS_SQL.STATE_CON then
         O_state_desc        := NULL;
         O_jurisdiction_code := NULL;
         O_jurisdiction_desc := NULL;
      elsif I_calling_module = ADDRESS_SQL.JURIS_CON then
         O_jurisdiction_desc := NULL;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_STATECOUNTRYJURIS;
-------------------------------------------------------------------------
FUNCTION PROCESS_OUTLOC_VAL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            L_error         IN OUT BOOLEAN,
                            I_rec           IN OUT C_SVC_OUTLOC%ROWTYPE )
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      := 'CORESVC_OUTLOC.PROCESS_OUTLOC_VAL';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OUTLOC';
   L_error_message      VARCHAR2(4000);
   L_exists             BOOLEAN;
   L_calling_module     VARCHAR2(20);
   L_calling_context    VARCHAR2(12);
   L_prim_rec_exists    BOOLEAN;
   L_prim_outloc_id     OUTLOC.OUTLOC_ID%TYPE;
   L_state_desc         STATE.description%TYPE;
   L_prim_outloc_desc   OUTLOC.OUTLOC_DESC%TYPE;
   L_country_desc       COUNTRY.country_desc%TYPE;
   L_currency_desc      CURRENCIES.CURRENCY_DESC%TYPE;
   L_jurisdiction_desc  COUNTRY_TAX_JURISDICTION.jurisdiction_desc%TYPE;
BEGIN
   L_exists := FALSE;
   if I_rec.action IN (action_new, action_mod) then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;

      if LP_system_options_row.default_tax_type = 'SVAT' then
         if I_rec.outloc_vat_region is NULL then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'OUTLOC_VAT_REGION',
                        'ENT_VAT_REG');
            L_error :=TRUE;
         end if;
      end if;

      if LP_system_options_row.default_tax_type = 'GTAX' then
         if I_rec.outloc_jur_code is NULL then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'OUTLOC_JURISDICTION_CODE',
                        'ENTER_JURISDICTION');
            L_error :=TRUE;
         end if;
      end if;

      if I_rec.action = action_new then
         if I_rec.outloc_country_id is NOT NULL
            and I_rec.outloc_type = 'CZ' then
            if I_rec.primary_ind = 'Y' then
               if OUTSIDE_LOCATION_SQL.UPDATE_PRIMARY_IND(O_error_message,
                                                          I_rec.outloc_country_id) = FALSE then
                  WRITE_ERROR( I_rec.process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_rec.chunk_id,
                               L_table,
                               I_rec.row_seq,
                              'PRIMARY_IND',
                               O_error_message);
                  L_error :=TRUE;
               end if;
            end if;

            if OUTSIDE_LOCATION_SQL.GET_PRIM_CLEAR_ZONE_IMP_CTRY(O_error_message,
                                                                 L_prim_rec_exists,
                                                                 L_prim_outloc_id,
                                                                 L_prim_outloc_desc,
                                                                 I_rec.outloc_country_id) = FALSE then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'PRIMARY_IND',
                           O_error_message);
               L_error :=TRUE;
            end if;
            if L_prim_rec_exists = FALSE then
               I_rec.primary_ind := 'Y';
            else
               I_rec.primary_ind := 'N';
            end if;
         end if;
      end if;

      if I_rec.outloc_currency is not NULL then
         if CURRENCY_SQL.GET_NAME(O_error_message,
                                  I_rec.outloc_currency,
                                  L_currency_desc)= FALSE then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                         'OUTLOC_CURRENCY',
                         O_error_message);
            L_error :=TRUE;
          end if;
      end if;

      if I_rec.outloc_jur_code is NOT NULL then
         if I_rec.outloc_state is NULL then
            WRITE_ERROR( I_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'OUTLOC_STATE',
                        'ENTER_STATE');
            L_error :=TRUE;
         end if;
      end if;

      if I_rec.outloc_type in ('BT','DP','RL') then
         if I_rec.outloc_currency is NULL then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'OUTLOC_CURRENCY',
                        'ENTER_CURRENCY_CODE');
            L_error :=TRUE;
         end if;
      end if;

      if I_rec.outloc_country_id = 'BR' and LP_system_options_row.default_tax_type = 'GTAX'then
         if I_rec.outloc_name_sec is NULL then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'OUTLOC_NAME_SECONDARY',
                        'ENTER_SECONDARY_NAME');
            L_error :=TRUE;
         end if;
      end if;

      if I_rec.action = action_mod then
         if I_rec.outloc_country_id is NOT NULL then
            if I_rec.primary_ind = 'Y'
               and I_rec.old_primary_ind = 'N' then
               if I_rec.outloc_type = 'CZ' then
       if OUTSIDE_LOCATION_SQL.UPDATE_PRIMARY_IND(O_error_message,
                                                  I_rec.outloc_country_id) = FALSE then
                     WRITE_ERROR( I_rec.process_id,
                                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                  I_rec.chunk_id,
                                  L_table,
                                  I_rec.row_seq,
                                  'PRIMARY_IND',
                                  O_error_message);
                     L_error :=TRUE;
                  end if;
               else
                  WRITE_ERROR( I_rec.process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_rec.chunk_id,
                               L_table,
                               I_rec.row_seq,
                               'PRIMARY_IND',
                               'CANNOT_MOD_PRIMARY_IND');
                  L_error :=TRUE;
   end if;
end if;
         end if;

         if I_rec.outloc_type = 'CZ'
            and I_rec.primary_ind <> I_rec.old_primary_ind
            and I_rec.old_primary_ind = 'Y' then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                         'PRIMARY_IND',
                         'CANNOT_UPD_PRIMARY_IND_Y');
            L_error :=TRUE;
         end if;

    if I_rec.outloc_type = 'CZ' then
            if I_rec.outloc_country_id <> I_rec.old_outloc_country_id then
               WRITE_ERROR( I_rec.process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_rec.chunk_id,
                            L_table,
                            I_rec.row_seq,
                           'OUTLOC_COUNTRY_ID',
                           'CANNOT_UPD_COUNTRY');
               L_error :=TRUE;
            end if;
         end if;
      end if;

      O_error_message := NULL;
      if I_rec.outloc_country_id is NOT NULL   then
         L_calling_module  := ADDRESS_SQL.COUNTRY_CON ;
         L_calling_context := ADDRESS_SQL.COUNTRY_CON;
         if VALIDATE_STATECOUNTRYJURIS(I_rec.outloc_state,
                                       L_state_desc,
                                       I_rec.outloc_country_id,
                                       L_country_desc,
                                       I_rec.outloc_jur_code,
                                       L_jurisdiction_desc,
                                       O_error_message,
                                       L_error,
                                       I_rec,
                                       L_calling_module,
                                       L_calling_context) = FALSE   then
            WRITE_ERROR(I_rec.process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'County,State,Jurisdiction',
                        O_error_message);
         end if;
      elsif(I_rec.outloc_state is NOT NULL) then
         L_calling_module  := ADDRESS_SQL.STATE_CON;
         L_calling_context := ADDRESS_SQL.STATE_CON;
         if VALIDATE_STATECOUNTRYJURIS(I_rec.outloc_state,
                                       L_state_desc,
                                       I_rec.outloc_country_id,
                                       L_country_desc,
                                       I_rec.outloc_jur_code,
                                       L_jurisdiction_desc,
                                       O_error_message,
                                       L_error,
                                       I_rec,
                                       L_calling_module,
                                       L_calling_context ) = FALSE   then
            WRITE_ERROR(I_rec.process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'County,State,Jurisdiction',
                        O_error_message);
         end if;
      elsif(I_rec.outloc_jur_code is NOT NULL) then
         L_calling_module := ADDRESS_SQL.JURIS_CON;
         L_calling_module := ADDRESS_SQL.JURIS_CON;
         if(VALIDATE_STATECOUNTRYJURIS(I_rec.outloc_state,
                                       L_state_desc,
                                       I_rec.outloc_country_id,
                                       L_country_desc,
                                       I_rec.outloc_jur_code,
                                       L_jurisdiction_desc,
                                       O_error_message,
                                       L_error,
                                       I_rec,
                                       L_calling_module,
                                       L_calling_context) = FALSE)   then
            WRITE_ERROR(I_rec.process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'County,State,Jurisdiction',
                        O_error_message);
         end if;
      end if;
   end if; --if rec.action IN (action_new, action_mod)

   if I_rec.action = action_del then
      if I_rec.outloc_type = 'CZ' then
         if OUTSIDE_LOCATION_SQL.CHECK_DELETE_CLEAR_ZONE(O_error_message,
                                                         L_exists,
                                                         I_rec.outloc_id) = FALSE then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'OUTLOC_ID',
                         O_error_message);
            L_error :=TRUE;
         elsif L_exists = TRUE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'OUTLOC_ID',
                        O_error_message);
            L_error :=TRUE;
         end if;
      end if;

      if OUTSIDE_LOCATION_SQL.DEL_OUTLOC_ATTRIB(O_error_message,
                                                I_rec.outloc_id,
                                                I_rec.outloc_type) = FALSE then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'Location Type,Location',
                      O_error_message);
         L_error :=TRUE;
      end if;

      if I_rec.outloc_type in ('DP','LP','BT','RL') then
         if OUTSIDE_LOCATION_SQL.CHECK_DELETE_OUTLOC(O_error_message,
                                                     L_exists,
                                                     I_rec.outloc_id,
                                                     I_rec.outloc_type)= FALSE then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'Location Type,Location',
                         O_error_message);
            L_error :=TRUE;
         elsif L_exists = TRUE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Location Type,Location',
                        O_error_message);
            L_error :=TRUE;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_OUTLOC_VAL;
----------------------------------------------------------------------------------
FUNCTION EXEC_OUTLOC_INS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_outloc_temp_rec IN     OUTLOC%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_OUTLOC.EXEC_OUTLOC_INS';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OUTLOC';
BEGIN

   insert into outloc
        values I_outloc_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_OUTLOC_INS;
----------------------------------------------------------------------------------
FUNCTION EXEC_OUTLOC_UPD( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_outloc_temp_rec IN     OUTLOC%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_OUTLOC.EXEC_OUTLOC_UPD';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OUTLOC';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   cursor C_OUTLOC_LOCK is
      select 'X'
        from OUTLOC
       where outloc_id   = I_outloc_temp_rec.outloc_id
         and outloc_type = I_outloc_temp_rec.outloc_type
       for update nowait;
BEGIN

   open C_OUTLOC_LOCK;
   close C_OUTLOC_LOCK;

   update outloc
      set row = I_outloc_temp_rec
    where outloc_id   = I_outloc_temp_rec.outloc_id
      and outloc_type = I_outloc_temp_rec.outloc_type;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'OUTLOC',
                                                                I_outloc_temp_rec.outloc_id,
                                                                I_outloc_temp_rec.outloc_type);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_OUTLOC_LOCK%ISOPEN   then
         close C_OUTLOC_LOCK;
      end if;
      return FALSE;
END EXEC_OUTLOC_UPD;
----------------------------------------------------------------------------------
FUNCTION EXEC_OUTLOC_DEL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_outloc_temp_rec IN     OUTLOC%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_OUTLOC.EXEC_OUTLOC_DEL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OUTLOC';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_OUTLOC_TL_LOCK is
      select 'X'
        from outloc_tl
       where outloc_id   = I_outloc_temp_rec.outloc_id
         and outloc_type = I_outloc_temp_rec.outloc_type
       for update nowait;

   cursor C_OUTLOC_LOCK is
      select 'X'
        from outloc
       where outloc_id   = I_outloc_temp_rec.outloc_id
         and outloc_type = I_outloc_temp_rec.outloc_type
       for update nowait;
BEGIN
   open C_OUTLOC_TL_LOCK;
   close C_OUTLOC_TL_LOCK;

   delete
     from outloc_tl
    where outloc_id   = I_outloc_temp_rec.outloc_id
      and outloc_type = I_outloc_temp_rec.outloc_type;

   open C_OUTLOC_LOCK;
   close C_OUTLOC_LOCK;

   delete
     from outloc
    where outloc_id   = I_outloc_temp_rec.outloc_id
      and outloc_type = I_outloc_temp_rec.outloc_type;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'OUTLOC',
                                                                I_outloc_temp_rec.outloc_id,
                                                                I_outloc_temp_rec.outloc_type);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_OUTLOC_LOCK%ISOPEN   then
         close C_OUTLOC_LOCK;
      end if;
      return FALSE;
END EXEC_OUTLOC_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_OUTLOC_TL_INS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_outloc_tl_ins_tab    IN       OUTLOC_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_OUTLOC.EXEC_OUTLOC_TL_INS';

BEGIN
   if I_outloc_tl_ins_tab is NOT NULL and I_outloc_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_outloc_tl_ins_tab.COUNT()
         insert into outloc_TL
              values I_outloc_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_OUTLOC_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_OUTLOC_TL_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_outloc_tl_upd_tab  IN       OUTLOC_TAB,
                            I_outloc_tl_upd_rst  IN       ROW_SEQ_TAB,
                            I_process_id         IN       SVC_OUTLOC_TL.PROCESS_ID%TYPE,
                            I_chunk_id           IN       SVC_OUTLOC_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_OUTLOC.EXEC_OUTLOC_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'OUTLOC_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_OUTLOC_TL_UPD(I_lang          OUTLOC_TL.LANG%TYPE,
                               I_outloc_type   OUTLOC_TL.OUTLOC_TYPE%TYPE,
                               I_outloc_id     OUTLOC_TL.OUTLOC_ID%TYPE) is
      select 'x'
        from outloc_tl
       where lang = I_lang
         and outloc_type = I_outloc_type
         and outloc_id = I_outloc_id
         for update nowait;

BEGIN
   if I_outloc_tl_upd_tab is NOT NULL and I_outloc_tl_upd_tab.count > 0 then
      for i in I_outloc_tl_upd_tab.FIRST..I_outloc_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_outloc_tl_upd_tab(i).lang);
            L_key_val2 := 'Outloc ID: '||to_char(I_outloc_tl_upd_tab(i).outloc_id);
            open C_LOCK_OUTLOC_TL_UPD(I_outloc_tl_upd_tab(i).lang,
                                      I_outloc_tl_upd_tab(i).outloc_type,
                                      I_outloc_tl_upd_tab(i).outloc_id);
            close C_LOCK_OUTLOC_TL_UPD;

            update outloc_tl
               set outloc_desc = I_outloc_tl_upd_tab(i).outloc_desc,
                   outloc_add1 = I_outloc_tl_upd_tab(i).outloc_add1,
                   outloc_add2 = I_outloc_tl_upd_tab(i).outloc_add2,
                   outloc_city = I_outloc_tl_upd_tab(i).outloc_city,
                   contact_name = I_outloc_tl_upd_tab(i).contact_name,
                   outloc_name_secondary = I_outloc_tl_upd_tab(i).outloc_name_secondary,
                   last_update_id = I_outloc_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_outloc_tl_upd_tab(i).last_update_datetime
             where lang = I_outloc_tl_upd_tab(i).lang
               and outloc_type = I_outloc_tl_upd_tab(i).outloc_type
               and outloc_id = I_outloc_tl_upd_tab(i).outloc_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_OUTLOC_TL',
                           I_outloc_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_OUTLOC_TL_UPD%ISOPEN then
         close C_LOCK_OUTLOC_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_OUTLOC_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_OUTLOC_TL_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_outloc_tl_del_tab  IN       OUTLOC_TAB,
                            I_outloc_tl_del_rst  IN       ROW_SEQ_TAB,
                            I_process_id         IN       SVC_OUTLOC_TL.PROCESS_ID%TYPE,
                            I_chunk_id           IN       SVC_OUTLOC_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_OUTLOC.EXEC_OUTLOC_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'OUTLOC_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_OUTLOC_TL_DEL(I_lang          OUTLOC_TL.LANG%TYPE,
                               I_outloc_type   OUTLOC_TL.OUTLOC_TYPE%TYPE,
                               I_outloc_id     OUTLOC_TL.OUTLOC_ID%TYPE) is
      select 'x'
        from outloc_tl
       where lang = I_lang
         and outloc_type = I_outloc_type
         and outloc_id = I_outloc_id
         for update nowait;

BEGIN
   if I_outloc_tl_del_tab is NOT NULL and I_outloc_tl_del_tab.count > 0 then
      for i in I_outloc_tl_del_tab.FIRST..I_outloc_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_outloc_tl_del_tab(i).lang);
            L_key_val2 := 'Outloc ID: '||to_char(I_outloc_tl_del_tab(i).outloc_id);
            open C_LOCK_OUTLOC_TL_DEL(I_outloc_tl_del_tab(i).lang,
                                      I_outloc_tl_del_tab(i).outloc_type,
                                      I_outloc_tl_del_tab(i).outloc_id);
            close C_LOCK_OUTLOC_TL_DEL;
            
            delete outloc_tl
             where lang = I_outloc_tl_del_tab(i).lang
               and outloc_type = I_outloc_tl_del_tab(i).outloc_type
               and outloc_id = I_outloc_tl_del_tab(i).outloc_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_OUTLOC_TL',
                           I_outloc_tl_DEL_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_OUTLOC_TL_DEL%ISOPEN then
         close C_LOCK_OUTLOC_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_OUTLOC_TL_DEL;
----------------------------------------------------------------------------------
FUNCTION PROCESS_OUTLOC( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id    IN     SVC_OUTLOC.PROCESS_ID%TYPE,
                         I_chunk_id      IN     SVC_OUTLOC.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      :='CORESVC_OUTLOC.PROCESS_OUTLOC';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_OUTLOC';
   L_process_error    BOOLEAN                           := FALSE;
   L_error            BOOLEAN;
   L_outloc_temp_rec  OUTLOC%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_OUTLOC(I_process_id,
                           I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_OUTLOC_rid is NOT NULL then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'Location Type,Location',
                     'DUP_OUTLOC');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_OUTLOC_rid is NULL then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'Location Type,Location',
                     'INV_OUTLOC');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new, action_mod) then
         if LP_system_options_row.default_tax_type = 'SVAT'
            and rec.outloc_vat_region is NOT NULL
            and rec.oul_vre_fk_rid is NULL then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'OUTLOC_VAT_REGION',
                           'INV_VAT_REGION');
               L_error :=TRUE;
         end if;

         if NOT(  rec.OUTLOC_DESC  IS NOT NULL ) then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'OUTLOC_DESC',
                        'ENTER_OUTLOC_DESC');
            L_error :=TRUE;
         end if;

         if NOT(  rec.OUTLOC_COUNTRY_ID  IS NOT NULL ) then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'OUTLOC_COUNTRY_ID',
                        'ENTER_COUNTRY');
            L_error :=TRUE;
         end if;

         if rec.OUTLOC_TYPE='CZ'
            and(rec.PRIMARY_IND NOT IN  ( 'Y','N' ) or rec.PRIMARY_IND IS NULL) then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'PRIMARY_IND',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;
      end if; --if rec.action IN (action_new, action_mod)

      if PROCESS_OUTLOC_VAL( O_error_message,
                             L_error,
                             rec) = FALSE then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_outloc_temp_rec.outloc_jurisdiction_code := rec.outloc_jur_code;
         L_outloc_temp_rec.outloc_name_secondary    := rec.outloc_name_sec;
         L_outloc_temp_rec.contact_email            := rec.contact_email;
         L_outloc_temp_rec.contact_telex            := rec.contact_telex;
         L_outloc_temp_rec.contact_fax              := rec.contact_fax;
         L_outloc_temp_rec.contact_phone            := rec.contact_phone;
         L_outloc_temp_rec.contact_name             := rec.contact_name;
         L_outloc_temp_rec.outloc_vat_region        := rec.outloc_vat_region;
         L_outloc_temp_rec.outloc_post              := rec.outloc_post;
         L_outloc_temp_rec.outloc_country_id        := rec.outloc_country_id;
         L_outloc_temp_rec.outloc_state             := rec.outloc_state;
         L_outloc_temp_rec.outloc_city              := rec.outloc_city;
         L_outloc_temp_rec.outloc_add2              := rec.outloc_add2;
         L_outloc_temp_rec.outloc_add1              := rec.outloc_add1;
         L_outloc_temp_rec.outloc_currency          := rec.outloc_currency;
         L_outloc_temp_rec.outloc_desc              := rec.outloc_desc;
         L_outloc_temp_rec.outloc_id                := rec.outloc_id;
         L_outloc_temp_rec.outloc_type              := rec.outloc_type;
         if rec.outloc_type = 'CZ' then
            L_outloc_temp_rec.primary_ind           := rec.primary_ind;
         else
            L_outloc_temp_rec.primary_ind           := 'N';
         end if;
         L_outloc_temp_rec.create_id                := GET_USER;
         L_outloc_temp_rec.create_datetime          := SYSDATE;

         if rec.action = action_new then
            if EXEC_OUTLOC_INS(O_error_message,
                               L_outloc_temp_rec ) = FALSE   then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_OUTLOC_UPD( O_error_message,
                                L_outloc_temp_rec )=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_OUTLOC_DEL( O_error_message,
                                L_outloc_temp_rec )=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_OUTLOC;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_OUTLOC_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_OUTLOC_TL.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_OUTLOC_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program               VARCHAR2(64) := 'CORESVC_OUTLOC.PROCESS_OUTLOC_TL';
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_OUTLOC_TL';
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'OUTLOC_TL';
   L_base_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'OUTLOC';
   L_error                 BOOLEAN := FALSE;
   L_process_error         BOOLEAN := FALSE;
   L_outloc_tl_temp_rec    OUTLOC_TL%ROWTYPE;
   L_outloc_tl_upd_rst     ROW_SEQ_TAB;
   L_outloc_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_OUTLOC_TL(I_process_id NUMBER,
                          I_chunk_id NUMBER) is
      select pk_outloc_TL.rowid  as pk_outloc_tl_rid,
             fk_outloc.rowid     as fk_outloc_rid,
             fk_lang.rowid       as fk_lang_rid,
             st.lang,
             st.outloc_type,
             st.outloc_id,
             st.outloc_desc,
             st.outloc_add1,
             st.outloc_add2,
             st.outloc_city,
             st.contact_name,
             st.outloc_name_secondary,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_outloc_tl  st,
             outloc         fk_outloc,
             outloc_tl      pk_outloc_tl,
             lang           fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.outloc_type =  fk_outloc.outloc_type (+)
         and st.outloc_id   =  fk_outloc.outloc_id (+)
         and st.lang        =  pk_outloc_tl.lang (+)
         and st.outloc_type =  pk_outloc_tl.outloc_type (+)
         and st.outloc_id   =  pk_outloc_tl.outloc_id (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_OUTLOC_TL is TABLE OF C_SVC_OUTLOC_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_outloc_tab        SVC_OUTLOC_TL;

   L_outloc_tl_ins_tab         outloc_tab         := NEW outloc_tab();
   L_outloc_tl_upd_tab         outloc_tab         := NEW outloc_tab();
   L_outloc_tl_del_tab         outloc_tab         := NEW outloc_tab();

BEGIN
   if C_SVC_OUTLOC_TL%ISOPEN then
      close C_SVC_OUTLOC_TL;
   end if;

   open C_SVC_OUTLOC_TL(I_process_id,
                        I_chunk_id);
   LOOP
      fetch C_SVC_OUTLOC_TL bulk collect into L_svc_outloc_tab limit LP_bulk_fetch_limit;
      if L_svc_outloc_tab.COUNT > 0 then
         FOR i in L_svc_outloc_tab.FIRST..L_svc_outloc_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_outloc_tab(i).action is NULL
               or L_svc_outloc_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_outloc_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_outloc_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_outloc_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_outloc_tab(i).action = action_new
               and L_svc_outloc_tab(i).pk_outloc_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_outloc_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_outloc_tab(i).action IN (action_mod, action_del)
               and L_svc_outloc_tab(i).lang is NOT NULL
               and L_svc_outloc_tab(i).outloc_type is NOT NULL
               and L_svc_outloc_tab(i).outloc_id is NOT NULL
               and L_svc_outloc_tab(i).pk_outloc_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_outloc_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_outloc_tab(i).action = action_new
               and L_svc_outloc_tab(i).outloc_type is NOT NULL
               and L_svc_outloc_tab(i).outloc_id is NOT NULL
               and L_svc_outloc_tab(i).fk_outloc_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_outloc_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_outloc_tab(i).action = action_new
               and L_svc_outloc_tab(i).fk_lang_rid is NULL
               and L_svc_outloc_tab(i).lang is NOT NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_outloc_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required field
            if L_svc_outloc_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_outloc_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if L_svc_outloc_tab(i).outloc_type is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_outloc_tab(i).row_seq,
                           'OUTLOC_TYPE',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_outloc_tab(i).outloc_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_outloc_tab(i).row_seq,
                           'OUTLOC_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_outloc_tab(i).action in (action_new, action_mod) then
               if L_svc_outloc_tab(i).outloc_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_outloc_tab(i).row_seq,
                              'OUTLOC_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if NOT L_error then
               L_outloc_tl_temp_rec.lang := L_svc_outloc_tab(i).lang;
               L_outloc_tl_temp_rec.outloc_type := L_svc_outloc_tab(i).outloc_type;
               L_outloc_tl_temp_rec.outloc_id := L_svc_outloc_tab(i).outloc_id;
               L_outloc_tl_temp_rec.outloc_desc := L_svc_outloc_tab(i).outloc_desc;
               L_outloc_tl_temp_rec.outloc_add1 := L_svc_outloc_tab(i).outloc_add1;
               L_outloc_tl_temp_rec.outloc_add2 := L_svc_outloc_tab(i).outloc_add2;
               L_outloc_tl_temp_rec.outloc_city := L_svc_outloc_tab(i).outloc_city;
               L_outloc_tl_temp_rec.contact_name := L_svc_outloc_tab(i).contact_name;
               L_outloc_tl_temp_rec.outloc_name_secondary := L_svc_outloc_tab(i).outloc_name_secondary;
               L_outloc_tl_temp_rec.create_datetime := SYSDATE;
               L_outloc_tl_temp_rec.create_id := GET_USER;
               L_outloc_tl_temp_rec.last_update_datetime := SYSDATE;
               L_outloc_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_outloc_tab(i).action = action_new then
                  L_outloc_TL_ins_tab.extend;
                  L_outloc_TL_ins_tab(L_outloc_TL_ins_tab.count()) := L_outloc_tl_temp_rec;
               end if;

               if L_svc_outloc_tab(i).action = action_mod then
                  L_outloc_TL_upd_tab.extend;
                  L_outloc_TL_upd_tab(L_outloc_TL_upd_tab.count()) := L_outloc_tl_temp_rec;
                  L_outloc_tl_upd_rst(L_outloc_TL_upd_tab.count()) := L_svc_outloc_tab(i).row_seq;
               end if;

               if L_svc_outloc_tab(i).action = action_del then
                  L_outloc_TL_del_tab.extend;
                  L_outloc_TL_del_tab(L_outloc_TL_del_tab.count()) := L_outloc_tl_temp_rec;
                  L_outloc_tl_del_rst(L_outloc_TL_del_tab.count()) := L_svc_outloc_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_OUTLOC_TL%NOTFOUND;
   END LOOP;
   close C_SVC_OUTLOC_TL;

   if EXEC_OUTLOC_TL_INS(O_error_message,
                         L_outloc_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_OUTLOC_TL_UPD(O_error_message,
                         L_outloc_tl_upd_tab,
                         L_outloc_tl_upd_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_OUTLOC_TL_DEL(O_error_message,
                         L_outloc_tl_del_tab,
                         L_outloc_tl_del_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_OUTLOC_TL;
-----------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN SVC_OUTLOC.PROCESS_ID%TYPE) IS
BEGIN
   delete
     from svc_outloc_tl
    where process_id = I_process_id;

   delete
     from svc_outloc
    where process_id = I_process_id;
END;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT NUMBER,
                  I_process_id    IN     NUMBER,
                  I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    :='CORESVC_OUTLOC.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_OUTLOC(O_error_message,
                     I_process_id,
                     I_chunk_id)= FALSE   then
      return FALSE;
   end if;

   if PROCESS_OUTLOC_TL(O_error_message,
                        I_process_id,
                        I_chunk_id)= FALSE   then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                    when status = 'PE'
                    then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------
END CORESVC_OUTLOC;
/