CREATE OR REPLACE PACKAGE BODY FILTER_LOV_VALIDATE_SQL AS

--------------------------------------------------------------------------------
-- GET_ORG_DYNAMIC_CODE( )
--------------------------------------------------------------------------------
FUNCTION GET_ORG_DYNAMIC_CODE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_org_code      IN OUT VARCHAR2,
                               I_org_level     IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.GET_ORG_DYNAMIC_CODE';

BEGIN
   if ( I_org_level = 'C' ) then
      O_org_code := '@OH2';
   elsif ( I_org_level = 'A' ) then
      O_org_code := '@OH3';
   elsif ( I_org_level = 'R' ) then
      O_org_code := '@OH4';
   elsif ( I_org_level = 'D' ) then
      O_org_code := '@OH5';
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ORG_DYNAMIC_CODE;
--------------------------------------------------------------------------------
-- GET_MERCH_DYNAMIC_CODE( )
--------------------------------------------------------------------------------
FUNCTION GET_MERCH_DYNAMIC_CODE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_merch_code    IN OUT VARCHAR2,
                                 I_merch_level   IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.GET_MERCH_DYNAMIC_CODE';

BEGIN
   if ( I_merch_level = 'D' ) then
      O_merch_code := '@MH2';
   elsif ( I_merch_level = 'G' ) then
      O_merch_code := '@MH3';
   elsif ( I_merch_level = 'P' ) then
      O_merch_code := '@MH4';
   elsif ( I_merch_level = 'C' ) then
      O_merch_code := '@MH5';
   elsif ( I_merch_level = 'S' ) then
      O_merch_code := '@MH6';
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_MERCH_DYNAMIC_CODE;
--------------------------------------------------------------------------------
-- GET_NON_VISIBLE_HIER( )
--------------------------------------------------------------------------------
FUNCTION GET_NON_VISIBLE_HIER (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_hier          IN OUT VARCHAR2,
                               I_org_id        IN     NUMBER,
                               I_org_level     IN     VARCHAR2,
                               I_merch_id      IN     NUMBER,
                               I_merch_level   IN     VARCHAR2,
                               I_class         IN     V_CLASS.CLASS%TYPE,
                               I_subclass      IN     V_SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.GET_NON_VISIBLE_HIER';

   L_valid       BOOLEAN;
   L_org_name    DIVISION.DIV_NAME%TYPE;
   L_merch_name  AREA.AREA_NAME%TYPE;
   L_code        VARCHAR2(4);
   L_code_desc   CODE_DETAIL.CODE_DESC%TYPE;
   L_cde_desc    CODE_DETAIL.CODE_DESC%TYPE;

BEGIN
   if I_org_id is NOT NULL then
      if VALIDATE_ORG_LEVEL(O_error_message,
                            L_valid,
                            L_org_name,
                            I_org_level,
                            I_org_id) = FALSE then
         return FALSE;
      end if;

      if L_valid = FALSE then
         if GET_ORG_DYNAMIC_CODE(O_error_message,
                                 L_code,
                                 I_org_level) = FALSE then
            return FALSE;
         end if;

         O_hier := L_code || ' ' || TO_CHAR(I_org_id);
         return TRUE;
      end if;
   end if;

   if I_merch_id is not NULL then
      if VALIDATE_MERCH_LEVEL(O_error_message,
                              L_valid,
                              L_merch_name,
                              I_merch_level,
                              I_merch_id,
                              I_class,
                              I_subclass) = FALSE then
         return FALSE;
      end if;

      if L_valid = FALSE then
         if GET_MERCH_DYNAMIC_CODE(O_error_message,
                                   L_code,
                                   I_merch_level) = FALSE then
            return FALSE;
         end if;

         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'LABL',
                                       'DEP',
                                       L_code_desc) = FALSE then
            return FALSE;
         end if;
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'MER1',
                                       'C',
                                       L_cde_desc) = FALSE then
            return FALSE;
         end if;

         if I_merch_level = 'C' then
            O_hier := L_code_desc || '/' || L_code || ':' || ' ' || TO_CHAR(I_merch_id) || '/' || TO_CHAR(I_class);
         elsif I_merch_level = 'S' then
            O_hier := L_code_desc || '/' || L_cde_desc || '/' || L_code || ':' || ' ' || TO_CHAR(I_merch_id)|| '/' || TO_CHAR(I_class) || '/' || TO_CHAR(I_subclass);
         else
            O_hier := L_code || ' ' || TO_CHAR(I_merch_id);
         end if;

         return TRUE;
      end if;
   end if;

   -- If it's not the org or the merch there must be another reason.
   -- Return NULL so a generic error can be raised
   O_hier := NULL;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_NON_VISIBLE_HIER;
--------------------------------------------------------------------------------
-- VALIDATE_CHAIN( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_chain_name    IN OUT V_CHAIN.CHAIN_NAME%TYPE,
                        I_chain         IN     V_CHAIN.CHAIN%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_CHAIN';

   cursor C_CHECK_CHAIN_TB is
      select chain_name
        from v_chain_tl
       where chain = I_chain;

   cursor C_CHECK_CHAIN_V is
      select chain_name
        from v_chain
       where chain = I_chain;

BEGIN
   open C_CHECK_CHAIN_V;
   fetch C_CHECK_CHAIN_V into O_chain_name;
   if C_CHECK_CHAIN_V%NOTFOUND then
      close C_CHECK_CHAIN_V;
      open C_CHECK_CHAIN_TB;
      fetch C_CHECK_CHAIN_TB into O_chain_name;
      if C_CHECK_CHAIN_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CHAIN', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@OH2', I_chain, NULL);
      end if;
      close C_CHECK_CHAIN_TB;
      O_valid := FALSE;
   else
      close C_CHECK_CHAIN_V;
      
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_CHAIN;

--------------------------------------------------------------------------------
-- VALIDATE_AREA( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_area_name     IN OUT V_AREA.AREA_NAME%TYPE,
                       I_area          IN     V_AREA.AREA%TYPE,
                       I_org_level     IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                       I_org_id        IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_AREA';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_AREA_REGION_V is
      select 'X'
        from v_region
       where area = I_area
         and region = I_org_id;

   cursor C_CHECK_AREA_DISTRICT_V is
      select 'X'
        from v_district
       where area = I_area
         and district = I_org_id;

BEGIN

   if(I_org_level = 'C') then
      if(VALIDATE_AREA(O_error_message,
                       O_valid,
                       O_area_name,
                       I_org_id,
                       I_area) = FALSE) then
         return FALSE;
      end if;
   else
      if(VALIDATE_AREA(O_error_message,
                       O_valid,
                       O_area_name,
                       NULL, -- chain
                       I_area) = FALSE) then
         return FALSE;
      end if;

      if(I_org_id IS NOT NULL) then
         if(I_org_level = 'R') then
            open C_CHECK_AREA_REGION_V;
            fetch C_CHECK_AREA_REGION_V into L_dummy;
            if C_CHECK_AREA_REGION_V%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH3', '@OH4', NULL);
               O_valid := FALSE;
            else
               O_valid := TRUE;
            end if;
            close C_CHECK_AREA_REGION_V;
         elsif(I_org_level = 'D') then
            open C_CHECK_AREA_DISTRICT_V;
            fetch C_CHECK_AREA_DISTRICT_V into L_dummy;
            if C_CHECK_AREA_DISTRICT_V%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH3', '@OH5', NULL);
               O_valid := FALSE;
            else
               O_valid := TRUE;
            end if;
            close C_CHECK_AREA_DISTRICT_V;
         else
            O_valid := TRUE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_AREA;

--------------------------------------------------------------------------------
-- VALIDATE_AREA( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_area_name     IN OUT V_AREA.AREA_NAME%TYPE,
                       I_chain         IN     V_CHAIN.CHAIN%TYPE,
                       I_area          IN     V_AREA.AREA%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_AREA';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_AREA_TB is
      select area_name
        from v_area_tl
       where area = I_area;

   cursor C_CHECK_AREA_V is
      select area_name
        from v_area
       where area = I_area;

   cursor C_CHECK_AREA_CHAIN_V is
      select 'X'
        from v_area
       where area = I_area
         and chain = I_chain;

BEGIN
   open C_CHECK_AREA_V;
   fetch C_CHECK_AREA_V into O_area_name;
   if C_CHECK_AREA_V%NOTFOUND then
      close C_CHECK_AREA_V;
      open C_CHECK_AREA_TB;
      fetch C_CHECK_AREA_TB into O_area_name;
      if C_CHECK_AREA_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_AREA', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@OH3', I_area, NULL);
      end if;
      close C_CHECK_AREA_TB;
      O_valid := FALSE;
   else
      close C_CHECK_AREA_V;
      if I_chain is not null then
         open C_CHECK_AREA_CHAIN_V;
         fetch C_CHECK_AREA_CHAIN_V into L_dummy;
         if C_CHECK_AREA_CHAIN_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH3', '@OH2', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_AREA_CHAIN_V;
      else
         O_valid := TRUE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_AREA;

--------------------------------------------------------------------------------
-- VALIDATE_AREA( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_area_name     IN OUT V_AREA.AREA_NAME%TYPE,
                       I_area          IN     V_AREA.AREA%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_AREA';

BEGIN
   if VALIDATE_AREA(O_error_message,
                    O_valid,
                    O_area_name,
                    null,
                    I_area) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_AREA;

--------------------------------------------------------------------------------
-- VALIDATE_REGION( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_region_name   IN OUT V_REGION.REGION_NAME%TYPE,
                         I_chain         IN     V_CHAIN.CHAIN%TYPE,
                         I_area          IN     V_AREA.AREA%TYPE,
                         I_region        IN     V_REGION.REGION%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_REGION';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_REGION_TB is
      select region_name
        from v_region_tl
       where region = I_region;

   cursor C_CHECK_REGION_V is
      select region_name
        from v_region
       where region = I_region;

   cursor C_CHECK_REGION_CHAIN_V is
      select 'X'
        from v_region
       where region = I_region
         and chain = I_chain;

   cursor C_CHECK_REGION_AREA_V is
      select 'X'
        from v_region
       where region = I_region
         and area = I_area;

BEGIN
   open C_CHECK_REGION_V;
   fetch C_CHECK_REGION_V into O_region_name;
   if C_CHECK_REGION_V%NOTFOUND then
      close C_CHECK_REGION_V;
      open C_CHECK_REGION_TB;
      fetch C_CHECK_REGION_TB into O_region_name;
      if C_CHECK_REGION_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_REGION', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@OH4', I_region, NULL);
      end if;
      close C_CHECK_REGION_TB;
      O_valid := FALSE;
   else
      close C_CHECK_REGION_V;
      if I_area is not null then
         open C_CHECK_REGION_AREA_V;
         fetch C_CHECK_REGION_AREA_V into L_dummy;
         if C_CHECK_REGION_AREA_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH4', '@OH3', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_REGION_AREA_V;
      elsif I_chain is not null then
         open C_CHECK_REGION_CHAIN_V;
         fetch C_CHECK_REGION_CHAIN_V into L_dummy;
         if C_CHECK_REGION_CHAIN_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH4', '@OH2', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_REGION_CHAIN_V;
      else
         O_valid := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_REGION;

--------------------------------------------------------------------------------
-- VALIDATE_REGION( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_look_down_ind IN OUT VARCHAR2,
                         O_region_name   IN OUT V_REGION.REGION_NAME%TYPE,
                         I_region        IN     V_REGION.REGION%TYPE,
                         I_org_level     IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                         I_org_id        IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_REGION';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_REGION_DISTRICT_V is
      select 'X'
        from v_district
       where region = I_region
         and district = I_org_id;

BEGIN

   if(I_org_level = 'C') then
      O_look_down_ind := 'N';
      if(VALIDATE_REGION(O_error_message,
                         O_valid,
                         O_region_name,
                         I_org_id, -- chain
                         NULL, -- area
                         I_region) = FALSE) then
         return FALSE;
      end if;
   elsif(I_org_level = 'A') then
      O_look_down_ind := 'N';
      if(VALIDATE_REGION(O_error_message,
                         O_valid,
                         O_region_name,
                         NULL, -- chain
                         I_org_id, -- area
                         I_region) = FALSE) then
         return FALSE;
      end if;
   else
      O_look_down_ind := 'Y';
      open C_CHECK_REGION_DISTRICT_V;
      fetch C_CHECK_REGION_DISTRICT_V into L_dummy;
      if C_CHECK_REGION_DISTRICT_V%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH4', '@OH5', NULL);
         O_valid := FALSE;
      else
         O_valid := TRUE;
      end if;
      close C_CHECK_REGION_DISTRICT_V;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_REGION;

--------------------------------------------------------------------------------
-- VALIDATE_REGION( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_region_name   IN OUT V_REGION.REGION_NAME%TYPE,
                         I_region        IN     V_REGION.REGION%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_REGION';

BEGIN
   if VALIDATE_REGION(O_error_message,
                      O_valid,
                      O_region_name,
                      NULL,
                      NULL,
                      I_region) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_REGION;

--------------------------------------------------------------------------------
-- VALIDATE_DISTRICT( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_district_name IN OUT V_DISTRICT.DISTRICT_NAME%TYPE,
                           I_chain         IN     V_CHAIN.CHAIN%TYPE,
                           I_area          IN     V_AREA.AREA%TYPE,
                           I_region        IN     V_REGION.REGION%TYPE,
                           I_district      IN     V_DISTRICT.DISTRICT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_DISTRICT';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_DISTRICT_TB is
      select district_name
        from v_district_tl
       where district = I_district;

   cursor C_CHECK_DISTRICT_V is
      select district_name
        from v_district
       where district = I_district;

   cursor C_CHECK_DISTRICT_CHAIN_V is
      select 'X'
        from v_district
       where district = I_district
         and chain = I_chain;

   cursor C_CHECK_DISTRICT_AREA_V is
      select 'X'
        from v_district
       where district = I_district
         and area = I_area;

   cursor C_CHECK_DISTRICT_REGION_V is
      select 'X'
        from v_district
       where district = I_district
         and region = I_region;

BEGIN
   open C_CHECK_DISTRICT_V;
   fetch C_CHECK_DISTRICT_V into O_district_name;
   if C_CHECK_DISTRICT_V%NOTFOUND then
      close C_CHECK_DISTRICT_V;
      open C_CHECK_DISTRICT_TB;
      fetch C_CHECK_DISTRICT_TB into O_district_name;
      if C_CHECK_DISTRICT_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRICT', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@OH5', I_district, NULL);
      end if;
      close C_CHECK_DISTRICT_TB;
      O_valid := FALSE;
   else
      close C_CHECK_DISTRICT_V;
      if I_region is not null then
         open C_CHECK_DISTRICT_REGION_V;
         fetch C_CHECK_DISTRICT_REGION_V into L_dummy;
         if C_CHECK_DISTRICT_REGION_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH5', '@OH4', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_DISTRICT_REGION_V;
      elsif I_area is not null then
            open C_CHECK_DISTRICT_AREA_V;
            fetch C_CHECK_DISTRICT_AREA_V into L_dummy;
            if C_CHECK_DISTRICT_AREA_V%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH5', '@OH3', NULL);
               O_valid := FALSE;
            else
               O_valid := TRUE;
            end if;
            close C_CHECK_DISTRICT_AREA_V;
         elsif I_chain is not null then
               open C_CHECK_DISTRICT_CHAIN_V;
               fetch C_CHECK_DISTRICT_CHAIN_V into L_dummy;
               if C_CHECK_DISTRICT_CHAIN_V%NOTFOUND then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER', '@OH5', '@OH2', NULL);
                  O_valid := FALSE;
               else
                  O_valid := TRUE;
               end if;
               close C_CHECK_DISTRICT_CHAIN_V;
            else
               O_valid := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DISTRICT;

--------------------------------------------------------------------------------
-- VALIDATE_DISTRICT( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_district_name IN OUT V_DISTRICT.DISTRICT_NAME%TYPE,
                           I_district      IN     V_DISTRICT.DISTRICT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_DISTRICT';

BEGIN
   if VALIDATE_DISTRICT(O_error_message,
                        O_valid,
                        O_district_name,
                        null,
                        null,
                        null,
                        I_district) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DISTRICT;

--------------------------------------------------------------------------------
-- VALIDATE_STORE( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_store         IN OUT V_STORE%ROWTYPE,
                        I_chain         IN     V_CHAIN.CHAIN%TYPE,
                        I_area          IN     V_AREA.AREA%TYPE,
                        I_region        IN     V_REGION.REGION%TYPE,
                        I_district      IN     V_DISTRICT.DISTRICT%TYPE,
                        I_store         IN     V_STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_STORE_TB is
      select store_name
        from v_store_tl
       where store = I_store;

   cursor C_CHECK_STORE_V is
      select *
        from v_store
       where store = I_store;

   cursor C_CHECK_STORE_CHAIN_V is
      select 'X'
        from v_store
       where store = I_store
         and chain = I_chain;

   cursor C_CHECK_STORE_AREA_V is
      select 'X'
        from v_store
       where store = I_store
         and area = I_area;

   cursor C_CHECK_STORE_REGION_V is
      select 'X'
        from v_store
       where store = I_store
         and region = I_region;

   cursor C_CHECK_STORE_DISTRICT_V is
      select 'X'
        from v_store
       where store = I_store
         and district = I_district;

BEGIN
   open C_CHECK_STORE_V;
   fetch C_CHECK_STORE_V into O_store;
   if C_CHECK_STORE_V%NOTFOUND then
      close C_CHECK_STORE_V;
      open C_CHECK_STORE_TB;
      fetch C_CHECK_STORE_TB into O_store.store_name;
      if C_CHECK_STORE_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_store, NULL);
      end if;
      close C_CHECK_STORE_TB;
      O_valid := FALSE;
   else
      close C_CHECK_STORE_V;
      if I_district is not null then
         open C_CHECK_STORE_DISTRICT_V;
         fetch C_CHECK_STORE_DISTRICT_V into L_dummy;
         if C_CHECK_STORE_DISTRICT_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_STORE', '@OH5', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_STORE_DISTRICT_V;
      elsif I_region is not null then
            open C_CHECK_STORE_REGION_V;
            fetch C_CHECK_STORE_REGION_V into L_dummy;
            if C_CHECK_STORE_REGION_V%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_STORE', '@OH4', NULL);
               O_valid := FALSE;
            else
               O_valid := TRUE;
            end if;
            close C_CHECK_STORE_REGION_V;
         elsif I_area is not null then
               open C_CHECK_STORE_AREA_V;
               fetch C_CHECK_STORE_AREA_V into L_dummy;
               if C_CHECK_STORE_AREA_V%NOTFOUND then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_STORE', '@OH3', NULL);
                  O_valid := FALSE;
               else
                  O_valid := TRUE;
               end if;
               close C_CHECK_STORE_AREA_V;
            elsif I_chain is not null then
                  open C_CHECK_STORE_CHAIN_V;
                  fetch C_CHECK_STORE_CHAIN_V into L_dummy;
                  if C_CHECK_STORE_CHAIN_V%NOTFOUND then
                     O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_STORE', '@OH2', NULL);
                     O_valid := FALSE;
                  else
                     O_valid := TRUE;
                  end if;
                  close C_CHECK_STORE_CHAIN_V;
               else
                  O_valid := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_STORE;

--------------------------------------------------------------------------------
-- VALIDATE_STORE( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_store_name    IN OUT V_STORE.STORE_NAME%TYPE,
                        I_chain         IN     V_CHAIN.CHAIN%TYPE,
                        I_area          IN     V_AREA.AREA%TYPE,
                        I_region        IN     V_REGION.REGION%TYPE,
                        I_district      IN     V_DISTRICT.DISTRICT%TYPE,
                        I_store         IN     V_STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE';

   L_store     V_STORE%ROWTYPE;

BEGIN
   if VALIDATE_STORE(O_error_message,
                     O_valid,
                     L_store,
                     I_chain,
                     I_area,
                     I_region,
                     I_district,
                     I_store) = FALSE then
      return FALSE;
   end if;

   O_store_name := L_store.store_name;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_STORE;
--------------------------------------------------------------------------------
-- VALIDATE_STORE( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_store_name    IN OUT V_STORE.STORE_NAME%TYPE,
                        I_store         IN     V_STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE';

   L_store     V_STORE%ROWTYPE;

BEGIN
   if VALIDATE_STORE(O_error_message,
                     O_valid,
                     L_store,
                     null,
                     null,
                     null,
                     null,
                     I_store) = FALSE then
      return FALSE;
   end if;

   O_store_name := L_store.store_name;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_STORE;

--------------------------------------------------------------------------------
-- VALIDATE_STORE( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid            IN OUT BOOLEAN,
                         I_store            IN     V_STORE.STORE%TYPE,
                         I_total_id         IN     SA_TOTAL_LOC_TRAIT.TOTAL_ID%TYPE,
                         I_set_of_books_id  IN     MV_LOC_SOB.SET_OF_BOOKS_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_STORE is
      select 'x'
        from mv_loc_sob mvls,
             (select vs.store
                from v_store vs,
                     v_sa_store vsa
               where vs.store = I_store
                 and vs.store = vsa.store
                 and exists (select 'x'
                               from loc_traits_matrix l
                              where vs.store = l.store
                                and ((I_total_id is not NULL and
                                      I_total_id != '-1' and
                                      l.loc_trait in (select q.loc_trait
                                                        from sa_total_loc_trait q
                                                       where q.total_id = I_total_id
                                                         and q.total_rev_no = (select max(q.total_rev_no)
                                                                                 from sa_total_loc_trait q
                                                                                where q.total_id = I_total_id)))
                                 or ((I_total_id is NULL or I_total_id = '-1'))))) loc
       where mvls.location = loc.store
         and mvls.location_type = 'S'
         and mvls.set_of_books_id = NVL(I_set_of_books_id, mvls.set_of_books_id);

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_store,L_program,NULL);
      return FALSE;
   else
      open  C_CHECK_STORE;
      fetch C_CHECK_STORE into L_dummy;
      close C_CHECK_STORE;

      if L_dummy is NULL then
         O_error_message := SQL_LIB.CREATE_MSG ('INV_STORE', NULL, NULL, NULL);
         O_valid := FALSE;
         return TRUE;
      else
         O_valid := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_STORE;

--------------------------------------------------------------------------------
-- VALIDATE_WH( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_WH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_valid         IN OUT BOOLEAN,
                     O_wh            IN OUT V_WH%ROWTYPE,
                     I_wh            IN     V_WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_WH';

   CURSOR C_CHECK_WH_TB IS
      SELECT wtl.wh_name,
             w.stockholding_ind
        FROM v_wh_tl wtl,
             wh w
       WHERE w.wh = I_wh
         And w.wh = wtl.wh 
         AND w.finisher_ind = 'N';

   CURSOR C_CHECK_WH_V IS
      SELECT *
        FROM v_wh
       WHERE wh = I_wh;

BEGIN
   open C_CHECK_WH_V;
   fetch C_CHECK_WH_V into O_wh;
   if C_CHECK_WH_V%NOTFOUND then
      close C_CHECK_WH_V;
      open C_CHECK_WH_TB;
      fetch C_CHECK_WH_TB into O_wh.wh_name, O_wh.stockholding_ind;
      if C_CHECK_WH_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_WH', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_wh, NULL);
      end if;
      close C_CHECK_WH_TB;
      O_valid := FALSE;
   else
      close C_CHECK_WH_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_WH;

--------------------------------------------------------------------------------
-- VALIDATE_PHYSICAL_WH( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PHYSICAL_WH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid         IN OUT BOOLEAN,
                              O_full_ind      IN OUT VARCHAR2,
                              O_wh            IN OUT V_PHYSICAL_WH%ROWTYPE,
                              I_wh            IN     V_PHYSICAL_WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_PHYSICAL_WH';
   L_dummy     VARCHAR2(1)    := 'N';

   CURSOR C_CHECK_WH_TB IS
      SELECT wtl.wh_name
        FROM v_wh_tl wtl,
             wh      w
       WHERE w.wh = I_wh
         and w.wh = wtl.wh
         and w.wh = physical_wh;

   CURSOR C_CHECK_WH_FULL IS
      SELECT *
        FROM v_physical_wh
       WHERE wh = I_wh;

   CURSOR C_CHECK_WH_PARTIAL IS
      SELECT 'Y'
        FROM wh
       WHERE wh = I_wh
         and wh = physical_wh
         and exists (select 'X'
                       from v_wh
                      where v_wh.physical_wh = wh.wh
                        and v_wh.physical_wh <> v_wh.wh);

BEGIN
   O_full_ind := 'N';

   open C_CHECK_WH_FULL;
   fetch C_CHECK_WH_FULL into O_wh;
   if C_CHECK_WH_FULL%NOTFOUND then
      close C_CHECK_WH_FULL;
      open C_CHECK_WH_TB;
      fetch C_CHECK_WH_TB into O_wh.wh_name;
      if C_CHECK_WH_TB%NOTFOUND then
         close C_CHECK_WH_TB;
         O_error_message := SQL_LIB.CREATE_MSG('INV_PHYSICAL_WH', NULL, NULL, NULL);
         O_valid := FALSE;
      else
         close C_CHECK_WH_TB;
         -- still populate message for both cases.  This will be used if full access is required
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_wh, NULL);

         open C_CHECK_WH_PARTIAL;
         fetch C_CHECK_WH_PARTIAL into L_dummy;
         if C_CHECK_WH_PARTIAL%NOTFOUND then
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_WH_PARTIAL;
      end if;
   else
      close C_CHECK_WH_FULL;
      O_full_ind := 'Y';
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_PHYSICAL_WH;

--------------------------------------------------------------------------------
-- VALIDATE_LOCATION( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid            IN OUT BOOLEAN,
                           O_loc_name         IN OUT V_STORE.STORE_NAME%TYPE,
                           O_stockholding_ind IN OUT V_STORE.STOCKHOLDING_IND%TYPE,
                           I_location         IN     V_STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION';

   L_loc_type  VARCHAR2(1);
   L_dummy     VARCHAR2(1);

   CURSOR C_CHECK_LOC_TB IS
      SELECT wtl.wh_name loc_name,
             w.stockholding_ind,
             'W' AS loc_type
        FROM v_wh_tl wtl,
             wh      w
       WHERE w.wh = I_location
         and w.wh = wtl.wh
         and w.finisher_ind = 'N'
      UNION
      SELECT stl.store_name loc_name,
             s.stockholding_ind,
             'S'
        FROM store      s,
             v_store_tl stl
       WHERE s.store = I_location
         and s.store = stl.store;

   CURSOR C_CHECK_WH_V IS
      SELECT 'X'
        FROM v_wh
       WHERE wh = I_location
      UNION
      SELECT 'X'
        FROM v_wh
       WHERE physical_wh = I_location;

   CURSOR C_CHECK_STORE_V IS
      SELECT 'X'
        FROM v_store
       WHERE store = I_location;

BEGIN
   O_valid := FALSE;
   open C_CHECK_LOC_TB;
   fetch C_CHECK_LOC_TB into O_loc_name, O_stockholding_ind, L_loc_type;
   if C_CHECK_LOC_TB%NOTFOUND then
      close C_CHECK_LOC_TB;
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC', NULL, NULL, NULL);
   else
      close C_CHECK_LOC_TB;
      if L_loc_type = 'S' then
         open C_CHECK_STORE_V;
         fetch C_CHECK_STORE_V into L_dummy;
         if C_CHECK_STORE_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_location, NULL);
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_STORE_V;
      else
         open C_CHECK_WH_V;
         fetch C_CHECK_WH_V into L_dummy;
         if C_CHECK_WH_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_location, NULL);
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_WH_V;
      end if;

      
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_LOCATION;

--------------------------------------------------------------------------------
-- VALIDATE_COSTING_LOCATION( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_COSTING_LOCATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_valid            IN OUT BOOLEAN,
                                   O_loc_name         IN OUT V_STORE.STORE_NAME%TYPE,
                                   O_loc_type         IN OUT V_STORE.STORE_TYPE%TYPE,                                   
                                   I_location         IN     V_STORE.STORE%TYPE)                                           
                           
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_COSTING_LOCATION';
   L_dummy     VARCHAR2(1);   

   CURSOR C_CHECK_LOCATION IS
      SELECT wh_name loc_name,
             'W'     loc_type
        FROM v_wh
       WHERE wh = I_location        
         AND stockholding_ind = 'Y'
      UNION ALL
      SELECT store_name loc_name,
             'S'        loc_type 
        FROM v_store
       WHERE store = I_location
         AND store_type = 'C'
         AND stockholding_ind = 'Y';    

BEGIN
   O_valid := FALSE;
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;   
  
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_LOCATION',
                    'V_STORE',
                    NULL);  
   open C_CHECK_LOCATION;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_LOCATION',
                    'V_STORE',
                    NULL);   
   fetch C_CHECK_LOCATION into O_loc_name,O_loc_type;
   
   if C_CHECK_LOCATION%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_LOCATION',
                       'V_STORE',
                        NULL);   
      close C_CHECK_LOCATION;   
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC', NULL, NULL, NULL);
      O_valid  := FALSE;
      return FALSE;       
   else
      close C_CHECK_LOCATION; 
      O_valid  := TRUE;
  end if; 
  
  return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_LOCATION%ISOPEN then
          close C_CHECK_LOCATION; 
      end if;    
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END VALIDATE_COSTING_LOCATION;

--------------------------------------------------------------------------------
-- VALIDATE_DIVISION( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DIVISION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_div_name      IN OUT V_DIVISION.DIV_NAME%TYPE,
                           I_division      IN     V_DIVISION.DIVISION%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_DIVISION';

   cursor C_CHECK_DIVISION_TB is
      select div_name
        from v_division_tl
       where division = I_division;

   cursor C_CHECK_DIVISION_V is
      select div_name
        from v_division
       where division = I_division;

BEGIN
   open C_CHECK_DIVISION_V;
   fetch C_CHECK_DIVISION_V into O_div_name;
   if C_CHECK_DIVISION_V%NOTFOUND then
      close C_CHECK_DIVISION_V;
      open C_CHECK_DIVISION_TB;
      fetch C_CHECK_DIVISION_TB into O_div_name;
      if C_CHECK_DIVISION_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DIVISION', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@MH2', I_division, NULL);
      end if;
      close C_CHECK_DIVISION_TB;
      O_valid := FALSE;
   else
      close C_CHECK_DIVISION_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DIVISION;

--------------------------------------------------------------------------------
-- VALIDATE_GROUPS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUPS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_group_name    IN OUT V_GROUPS.GROUP_NAME%TYPE,
                         I_division      IN     V_DIVISION.DIVISION%TYPE,
                         I_group_no      IN     V_GROUPS.GROUP_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_GROUPS';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_GROUPS_TB is
      select group_name
        from v_groups_tl
       where group_no = I_group_no;

   cursor C_CHECK_GROUPS_V is
      select group_name
        from v_groups
       where group_no = I_group_no;

   cursor C_CHECK_GROUPS_DIVISION_V is
      select 'X'
        from v_groups
       where group_no = I_group_no
         and division = I_division;

BEGIN
   open C_CHECK_GROUPS_V;
   fetch C_CHECK_GROUPS_V into O_group_name;
   if C_CHECK_GROUPS_V%NOTFOUND then
      close C_CHECK_GROUPS_V;
      open C_CHECK_GROUPS_TB;
      fetch C_CHECK_GROUPS_TB into O_group_name;
      if C_CHECK_GROUPS_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_GROUP', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@MH3', I_group_no, NULL);
      end if;
      close C_CHECK_GROUPS_TB;
      O_valid := FALSE;
   else
      close C_CHECK_GROUPS_V;
      if I_division is not null then
         open C_CHECK_GROUPS_DIVISION_V;
         fetch C_CHECK_GROUPS_DIVISION_V into L_dummy;
         if C_CHECK_GROUPS_DIVISION_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_MERCH_HIER', '@MH3', '@MH2', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_GROUPS_DIVISION_V;
      else
         O_valid := TRUE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_GROUPS;

--------------------------------------------------------------------------------
-- VALIDATE_GROUPS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_GROUPS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_group_name    IN OUT V_GROUPS.GROUP_NAME%TYPE,
                         I_group_no      IN     V_GROUPS.GROUP_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_GROUPS';

BEGIN
   if VALIDATE_GROUPS(O_error_message,
                      O_valid,
                      O_group_name,
                      null,
                      I_group_no) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_GROUPS;

--------------------------------------------------------------------------------
-- VALIDATE_DEPS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DEPS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_dept_name     IN OUT V_DEPS.DEPT_NAME%TYPE,
                       I_division      IN     V_DIVISION.DIVISION%TYPE,
                       I_group_no      IN     V_GROUPS.GROUP_NO%TYPE,
                       I_dept          IN     V_DEPS.DEPT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_DEPS';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_DEPS_TB is
      select dept_name
        from v_deps_tl
       where dept = I_dept;

   cursor C_CHECK_DEPS_V is
      select dept_name
        from v_deps
       where dept = I_dept;

   cursor C_CHECK_DEPS_GROUPS_V is
      select 'X'
        from v_deps
       where dept = I_dept
         and group_no = I_group_no;

   cursor C_CHECK_DEPS_DIVISION_V is
      select 'X'
        from v_deps
       where dept = I_dept
         and division = I_division;

BEGIN
   open C_CHECK_DEPS_V;
   fetch C_CHECK_DEPS_V into O_dept_name;
   if C_CHECK_DEPS_V%NOTFOUND then
      close C_CHECK_DEPS_V;
      open C_CHECK_DEPS_TB;
      fetch C_CHECK_DEPS_TB into O_dept_name;
      if C_CHECK_DEPS_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@MH4', I_dept, NULL);
      end if;
      close C_CHECK_DEPS_TB;
      O_valid := FALSE;
   else
      close C_CHECK_DEPS_V;
      if I_group_no is not null then
         open C_CHECK_DEPS_GROUPS_V;
         fetch C_CHECK_DEPS_GROUPS_V into L_dummy;
         if C_CHECK_DEPS_GROUPS_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_MERCH_HIER', '@MH4', '@MH3', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_DEPS_GROUPS_V;
      elsif I_division is not null then
         open C_CHECK_DEPS_DIVISION_V;
         fetch C_CHECK_DEPS_DIVISION_V into L_dummy;
         if C_CHECK_DEPS_DIVISION_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_MERCH_HIER', '@MH4', '@MH2', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_DEPS_DIVISION_V;
      else
         O_valid := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DEPS;

--------------------------------------------------------------------------------
-- VALIDATE_DEPS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DEPS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_dept_name     IN OUT V_DEPS.DEPT_NAME%TYPE,
                       I_dept          IN     V_DEPS.DEPT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_DEPS';

BEGIN
   if VALIDATE_DEPS(O_error_message,
                    O_valid,
                    O_dept_name,
                    null,
                    null,
                    I_dept) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DEPS;
--------------------------------------------------------------------------------
-- VALIDATE_DEPS_INV( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DEPS_INV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_dept_name     IN OUT V_DEPS.DEPT_NAME%TYPE,
                           I_dept          IN     V_DEPS.DEPT%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_DEPS';
   L_purchase_type   DEPS.PURCHASE_TYPE%TYPE;

   cursor C_CHECK_DEPS_TB is
      select dtl.dept_name,
             d.purchase_type
        from v_deps_tl dtl,
             deps      d
       where d.dept = I_dept
         and d.dept = dtl.dept;

   cursor C_CHECK_DEPS_V is
      select dept_name,
             purchase_type
        from v_deps
       where dept = I_dept;

BEGIN
   O_valid := TRUE;
   --
   open C_CHECK_DEPS_V;
   fetch C_CHECK_DEPS_V into O_dept_name, L_purchase_type;
   if C_CHECK_DEPS_V%NOTFOUND then
      close C_CHECK_DEPS_V;
      open C_CHECK_DEPS_TB;
      fetch C_CHECK_DEPS_TB into O_dept_name, L_purchase_type;
      if C_CHECK_DEPS_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@MH4', I_dept, NULL);
      end if;
      close C_CHECK_DEPS_TB;
      O_valid := FALSE;
   else
      close C_CHECK_DEPS_V;
   end if;
   ---
   if O_valid = TRUE and
      L_purchase_type != 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT_CON_ITM', NULL, NULL, NULL);
      O_valid := FALSE;
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DEPS_INV;
--------------------------------------------------------------------------------
-- VALIDATE_CLASS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_class_name    IN OUT V_CLASS.CLASS_NAME%TYPE,
                        I_dept          IN     V_CLASS.DEPT%TYPE,
                        I_class         IN     V_CLASS.CLASS%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_CLASS_TB is
      select class_name
        from v_class_tl
       where dept = I_dept
         and class = I_class;

   cursor C_CHECK_CLASS_V is
      select class_name
        from v_class
       where dept = I_dept
         and class = I_class;

BEGIN
   O_class_name := NULL;

   open C_CHECK_CLASS_V;
   fetch C_CHECK_CLASS_V into O_class_name;
   close C_CHECK_CLASS_V;
   if O_class_name is NULL then
      open C_CHECK_CLASS_TB;
      fetch C_CHECK_CLASS_TB into O_class_name;
      close C_CHECK_CLASS_TB;
      if O_class_name is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CLASS', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@MH5', I_class, NULL);
      end if;
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_CLASS;

--------------------------------------------------------------------------------
-- VALIDATE_SUBCLASS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SUBCLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_subclass_name IN OUT V_SUBCLASS.SUB_NAME%TYPE,
                           I_dept          IN     V_SUBCLASS.DEPT%TYPE,
                           I_class         IN     V_SUBCLASS.CLASS%TYPE,
                           I_subclass      IN     V_SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SUBCLASS';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_SUBCLASS_TB is
      select sub_name
        from v_subclass_tl
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass;

   cursor C_CHECK_SUBCLASS_V is
      select sub_name
        from v_subclass
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass;

BEGIN
   O_subclass_name := NULL;

   open C_CHECK_SUBCLASS_V;
   fetch C_CHECK_SUBCLASS_V into O_subclass_name;
   close C_CHECK_SUBCLASS_V;
   if O_subclass_name is NULL then
      open C_CHECK_SUBCLASS_TB;
      fetch C_CHECK_SUBCLASS_TB into O_subclass_name;
      close C_CHECK_SUBCLASS_TB;
      if O_subclass_name is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUBCLASS', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', '@MH6', I_subclass, NULL);
      end if;
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SUBCLASS;

--------------------------------------------------------------------------------
-- VALIDATE_ITEM_MASTER( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_MASTER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid         IN OUT BOOLEAN,
                              O_item_master   IN OUT V_ITEM_MASTER%ROWTYPE,
                              I_division      IN     V_ITEM_MASTER.DIVISION%TYPE,
                              I_group_no      IN     V_ITEM_MASTER.GROUP_NO%TYPE,
                              I_dept          IN     V_ITEM_MASTER.DEPT%TYPE,
                              I_class         IN     V_ITEM_MASTER.CLASS%TYPE,
                              I_subclass      IN     V_ITEM_MASTER.SUBCLASS%TYPE,
                              I_item          IN     V_ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_ITEM_MASTER';

   L_dummy              VARCHAR2(1);

   cursor C_CHECK_ITEM_MASTER_TB is
      select 'X'
        from item_master
       where item = I_item;

   cursor C_CHECK_ITEM_MASTER_V is
      select *
        from v_item_master
       where item = I_item;

   cursor C_CHECK_ITEM_DIVISION_V is
      select 'X'
        from v_item_master
       where item = I_item
         and division = I_division;

   cursor C_CHECK_ITEM_GROUP_V is
      select 'X'
        from v_item_master
       where item = I_item
         and group_no = I_group_no;

   cursor C_CHECK_ITEM_DEPT_V is
      select 'X'
        from v_item_master
       where item = I_item
         and dept = I_dept;

   cursor C_CHECK_ITEM_CLASS_V is
      select 'X'
        from v_item_master
       where item = I_item
         and dept = I_dept
         and class = I_class;

   cursor C_CHECK_ITEM_SUBCLASS_V is
      select 'X'
        from v_item_master
       where item = I_item
         and dept = I_dept
         and class = I_class
         and subclass = I_subclass;

BEGIN
   open C_CHECK_ITEM_MASTER_V;
   fetch C_CHECK_ITEM_MASTER_V into O_item_master;
   if C_CHECK_ITEM_MASTER_V%NOTFOUND then
      close C_CHECK_ITEM_MASTER_V;
      open C_CHECK_ITEM_MASTER_TB;
      fetch C_CHECK_ITEM_MASTER_TB into L_dummy;
      if C_CHECK_ITEM_MASTER_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_ITEM', I_item, NULL);
      end if;
      close C_CHECK_ITEM_MASTER_TB;
      O_valid := FALSE;
   else
      close C_CHECK_ITEM_MASTER_V;
      if I_subclass is not null then
         open C_CHECK_ITEM_SUBCLASS_V;
         fetch C_CHECK_ITEM_SUBCLASS_V into L_dummy;
         if C_CHECK_ITEM_SUBCLASS_V%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_ITEM', '@MH4/@MH5/@MH6', NULL);
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         close C_CHECK_ITEM_SUBCLASS_V;
      elsif I_class is not null then
            open C_CHECK_ITEM_CLASS_V;
            fetch C_CHECK_ITEM_CLASS_V into L_dummy;
            if C_CHECK_ITEM_CLASS_V%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_ITEM', '@MH4/@MH5', NULL);
               O_valid := FALSE;
            else
               O_valid := TRUE;
            end if;
            close C_CHECK_ITEM_CLASS_V;
         elsif I_dept is not null then
               open C_CHECK_ITEM_DEPT_V;
               fetch C_CHECK_ITEM_DEPT_V into L_dummy;
               if C_CHECK_ITEM_DEPT_V%NOTFOUND then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_ITEM', '@MH4', NULL);
                  O_valid := FALSE;
               else
                  O_valid := TRUE;
               end if;
               close C_CHECK_ITEM_DEPT_V;
            elsif I_group_no is not null then
                  open C_CHECK_ITEM_GROUP_V;
                  fetch C_CHECK_ITEM_GROUP_V into L_dummy;
                  if C_CHECK_ITEM_GROUP_V%NOTFOUND then
                     O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_ITEM', '@MH3', NULL);
                     O_valid := FALSE;
                  else
                     O_valid := TRUE;
                  end if;
                  close C_CHECK_ITEM_GROUP_V;
               elsif I_division is not null then
                     open C_CHECK_ITEM_DIVISION_V;
                     fetch C_CHECK_ITEM_DIVISION_V into L_dummy;
                     if C_CHECK_ITEM_DIVISION_V%NOTFOUND then
                        O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_HIER_ITEM', '@MH2', NULL);
                        O_valid := FALSE;
                     else
                        O_valid := TRUE;
                     end if;
                     close C_CHECK_ITEM_DIVISION_V;
                  else
                     O_valid := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ITEM_MASTER;

--------------------------------------------------------------------------------
-- VALIDATE_ITEM_MASTER( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_MASTER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid         IN OUT BOOLEAN,
                              O_item_master   IN OUT V_ITEM_MASTER%ROWTYPE,
                              I_item          IN     V_ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_ITEM_MASTER';

BEGIN
   if VALIDATE_ITEM_MASTER(O_error_message,
                           O_valid,
                           O_item_master,
                           null,
                           null,
                           null,
                           null,
                           null,
                           I_item) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ITEM_MASTER;

--------------------------------------------------------------------------------
-- VALIDATE_DIFF_GROUP_HEAD( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF_GROUP_HEAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_valid           IN OUT BOOLEAN,
                                  O_diff_group_desc IN OUT V_DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                                  I_diff_type       IN     V_DIFF_GROUP_HEAD.DIFF_TYPE%TYPE,
                                  I_diff_group_id   IN     V_DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_DIFF_GROUP_HEAD';

   L_hier                       VARCHAR2(128);
   L_org_id                     DIFF_GROUP_HEAD.FILTER_ORG_ID%TYPE;
   L_merch_id                   DIFF_GROUP_HEAD.FILTER_MERCH_ID%TYPE;
   L_filter_merch_id_class      DIFF_GROUP_HEAD.FILTER_MERCH_ID_CLASS%TYPE;
   L_filter_merch_id_subclass   DIFF_GROUP_HEAD.FILTER_MERCH_ID_SUBCLASS%TYPE;


   cursor C_CHECK_DIFF_GROUP_TB is
      select dghtl.diff_group_desc,
             dgh.filter_org_id,
             dgh.filter_merch_id,
             dgh.filter_merch_id_class,
             dgh.filter_merch_id_subclass
        from diff_group_head       dgh,
             v_diff_group_head_tl  dghtl
       where dgh.diff_group_id = I_diff_group_id
         and dgh.diff_group_id = dghtl.diff_group_id
         and dgh.diff_type = I_diff_type;

   cursor C_CHECK_DIFF_GROUP_V is
      select diff_group_desc
        from v_diff_group_head
       where diff_group_id = I_diff_group_id
         and diff_type = I_diff_type;



BEGIN
   O_diff_group_desc := NULL;
   open C_CHECK_DIFF_GROUP_V;
   fetch C_CHECK_DIFF_GROUP_V into O_diff_group_desc;
   close C_CHECK_DIFF_GROUP_V;
   if O_diff_group_desc is NULL then
      open C_CHECK_DIFF_GROUP_TB;
      fetch C_CHECK_DIFF_GROUP_TB into O_diff_group_desc,
                                       L_org_id,
                                       L_merch_id,
                                       L_filter_merch_id_class,
                                       L_filter_merch_id_subclass;
      close C_CHECK_DIFF_GROUP_TB;
      if O_diff_group_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_GROUP', NULL, NULL, NULL);
      else
         if GET_NON_VISIBLE_HIER(O_error_message,
                                 L_hier,
                                 L_org_id,
                                 FILTER_POLICY_SQL.GP_system_options.diff_group_org_level_code,
                                 L_merch_id,
                                 FILTER_POLICY_SQL.GP_system_options.diff_group_merch_level_code,
                                 L_filter_merch_id_class,
                                 L_filter_merch_id_subclass) = FALSE then
            return FALSE;
         end if;

         -- If no hierarchy was returned, raise a more generic error.
         if L_hier is not NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_DIFF_GRP', L_hier, I_diff_group_id);
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_ANY_DIFF_GRP', I_diff_group_id);
         end if;
      end if;
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DIFF_GROUP_HEAD;

--------------------------------------------------------------------------------
-- VALIDATE_LOC_LISTS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOC_LIST_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid         IN OUT BOOLEAN,
                                O_loc_list_desc IN OUT V_LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                                I_loc_list      IN     V_LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_LOC_LIST_HEAD';

   cursor C_CHECK_LOC_LIST_TB is
      select loc_list_desc
        from v_loc_list_head_tl
       where loc_list = I_loc_list;

   cursor C_CHECK_LOC_LIST_V is
      select loc_list_desc
        from v_loc_list_head
       where loc_list = I_loc_list;

BEGIN
   open C_CHECK_LOC_LIST_V;
   fetch C_CHECK_LOC_LIST_V into O_loc_list_desc;
   if C_CHECK_LOC_LIST_V%NOTFOUND then
      close C_CHECK_LOC_LIST_V;
      open C_CHECK_LOC_LIST_TB;
      fetch C_CHECK_LOC_LIST_TB into O_loc_list_desc;
      if C_CHECK_LOC_LIST_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_LIST', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ANY_LOC_LIST', I_loc_list);
      end if;
      close C_CHECK_LOC_LIST_TB;
      O_valid := FALSE;
   else
      close C_CHECK_LOC_LIST_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_LOC_LIST_HEAD;

--------------------------------------------------------------------------------
-- VALIDATE_LOC_TRAITS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOC_TRAITS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT BOOLEAN,
                             O_description   IN OUT V_LOC_TRAITS.DESCRIPTION%TYPE,
                             I_loc_trait     IN     V_LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_LOC_TRAITS';
   L_org_code  VARCHAR2(4);
   L_org_id    LOC_TRAITS.FILTER_ORG_ID%TYPE;

   cursor C_CHECK_LOC_TRAIT_TB is
      select lttl.description,
             lt.filter_org_id
        from loc_traits lt,
             v_loc_traits_tl lttl
       where lt.loc_trait = I_loc_trait
         and lt.loc_trait = lttl.loc_trait;

   cursor C_CHECK_LOC_TRAIT_V is
      select description
        from v_loc_traits
       where loc_trait = I_loc_trait;

BEGIN
   open C_CHECK_LOC_TRAIT_V;
   fetch C_CHECK_LOC_TRAIT_V into O_description;
   if C_CHECK_LOC_TRAIT_V%NOTFOUND then
      close C_CHECK_LOC_TRAIT_V;
      open C_CHECK_LOC_TRAIT_TB;
      fetch C_CHECK_LOC_TRAIT_TB into O_description, L_org_id;
      if C_CHECK_LOC_TRAIT_TB%NOTFOUND then
         close C_CHECK_LOC_TRAIT_TB;
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TRAIT', NULL, NULL, NULL);
      else
         close C_CHECK_LOC_TRAIT_TB;
         if GET_ORG_DYNAMIC_CODE(O_error_message,
                                 L_org_code,
                                 FILTER_POLICY_SQL.GP_system_options.loc_trait_org_level_code ) = FALSE then
            return FALSE;
         end if;

         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_LOC_TRAIT', L_org_code ||' '|| TO_CHAR(L_org_id), I_loc_trait);
      end if;
      O_valid := FALSE;
   else
      close C_CHECK_LOC_TRAIT_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_LOC_TRAITS;
--------------------------------------------------------------------------------
-- VALIDATE_SKULIST_HEAD( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SKULIST_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid         IN OUT BOOLEAN,
                               O_skulist_desc  IN OUT V_SKULIST_HEAD.SKULIST_DESC%TYPE,
                               I_skulist       IN     V_SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SKULIST_HEAD';

   L_skulist_head  V_SKULIST_HEAD%ROWTYPE;

BEGIN

   if VALIDATE_SKULIST_HEAD (O_error_message,
                             O_valid,
                             L_skulist_head,
                             I_skulist) = FALSE then
      return FALSE;
   end if;

   if O_valid then
      O_skulist_desc := L_skulist_head.skulist_desc;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SKULIST_HEAD;
--------------------------------------------------------------------------------
-- VALIDATE_SKULIST_HEAD( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SKULIST_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid         IN OUT BOOLEAN,
                               O_skulist_head  IN OUT V_SKULIST_HEAD%ROWTYPE,
                               I_skulist       IN     V_SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SKULIST_HEAD';

   L_org_id    SKULIST_HEAD.FILTER_ORG_ID%TYPE;
   L_org_name  AREA.AREA_NAME%TYPE;
   L_valid     BOOLEAN;
   L_code      VARCHAR2(4);

   cursor C_CHECK_SKULIST_TB is
      select shtl.skulist_desc,
             sh.filter_org_id
        from v_skulist_head_tl shtl,
             skulist_head      sh
       where sh.skulist = I_skulist
         and sh.skulist = shtl.skulist;

   cursor C_CHECK_SKULIST_V is
      select *
        from v_skulist_head
       where skulist = I_skulist;

BEGIN
   open C_CHECK_SKULIST_V;
   fetch C_CHECK_SKULIST_V into O_skulist_head;
   if C_CHECK_SKULIST_V%NOTFOUND then
      close C_CHECK_SKULIST_V;
      open C_CHECK_SKULIST_TB;
      fetch C_CHECK_SKULIST_TB into O_skulist_head.skulist_desc, L_org_id;
      if C_CHECK_SKULIST_TB%NOTFOUND then
         close C_CHECK_SKULIST_TB;
         O_error_message := SQL_LIB.CREATE_MSG('INV_SKU_LIST', NULL, NULL, NULL);
      else
         close C_CHECK_SKULIST_TB;
         L_valid := TRUE;
         if L_org_id is not NULL then
            if VALIDATE_ORG_LEVEL(O_error_message,
                                  L_valid,
                                  L_org_name,
                                  FILTER_POLICY_SQL.GP_system_options.skulist_org_level_code,
                                  L_org_id) = FALSE then
               return FALSE;
            end if;
         end if;

         if L_valid = FALSE then
            if GET_ORG_DYNAMIC_CODE(O_error_message,
                                    L_code,
                                    FILTER_POLICY_SQL.GP_system_options.skulist_org_level_code) = FALSE then
               return FALSE;
            end if;

            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_ITEM_LIST', L_code || ' ' || TO_CHAR(L_org_id), I_skulist);
         else
            -- must not have visibility to ALL departments
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ALL_DEPS_IL', I_skulist);
         end if;
      end if;
      O_valid := FALSE;
   else
      close C_CHECK_SKULIST_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SKULIST_HEAD;
--------------------------------------------------------------------------------
-- VALIDATE_SEASONS( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SEASONS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_valid         IN OUT BOOLEAN,
                          O_season_desc   IN OUT V_SEASONS.SEASON_DESC%TYPE,
                          I_season_id     IN     V_SEASONS.SEASON_ID%TYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(64)  := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SEASONS';

   L_hier                       VARCHAR2(128);
   L_org_id                     SEASONS.FILTER_ORG_ID%TYPE;
   L_merch_id                   SEASONS.FILTER_MERCH_ID%TYPE;
   L_filter_merch_id_class      SEASONS.FILTER_MERCH_ID_CLASS%TYPE;
   L_filter_merch_id_subclass   SEASONS.FILTER_MERCH_ID_SUBCLASS%TYPE;

   cursor C_CHECK_SEASONS_TB is
      select stl.season_desc,
             s.filter_org_id,
             s.filter_merch_id,
             s.filter_merch_id_class,
             s.filter_merch_id_subclass
        from seasons      s,
             v_seasons_tl stl
       where s.season_id = I_season_id
         and s.season_id = stl.season_id;

   cursor C_CHECK_SEASONS_V is
      select season_desc
        from v_seasons
       where season_id = I_season_id;

BEGIN
   O_season_desc := NULL;
   open C_CHECK_SEASONS_V;
   fetch C_CHECK_SEASONS_V into O_season_desc;
   close C_CHECK_SEASONS_V;
   if O_season_desc is NULL then
      open C_CHECK_SEASONS_TB;
      fetch C_CHECK_SEASONS_TB into O_season_desc,
                                    L_org_id,
                                    L_merch_id,
                                    L_filter_merch_id_class,
                                    L_filter_merch_id_subclass;
      close C_CHECK_SEASONS_TB;
      if O_season_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SEASON', NULL, NULL, NULL);
      else
         if GET_NON_VISIBLE_HIER(O_error_message,
                                 L_hier,
                                 L_org_id,
                                 FILTER_POLICY_SQL.GP_system_options.season_org_level_code,
                                 L_merch_id,
                                 FILTER_POLICY_SQL.GP_system_options.season_merch_level_code,
                                 L_filter_merch_id_class,
                                 L_filter_merch_id_subclass) = FALSE then
            return FALSE;
         end if;

         if L_hier is not NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_SEASON', L_hier, I_season_id);
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_ANY_SEASON', I_season_id);
         end if;
      end if;
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SEASONS;

--------------------------------------------------------------------------------
-- VALIDATE_TICKET_TYPE_HEAD( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TICKET_TYPE_HEAD(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_valid            IN OUT BOOLEAN,
                                   O_ticket_type_desc IN OUT V_TICKET_TYPE_HEAD.TICKET_TYPE_DESC%TYPE,
                                   I_ticket_type_id   IN     V_TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_TICKET_TYPE_HEAD';

   L_hier                       VARCHAR2(128);
   L_org_id                     TICKET_TYPE_HEAD.FILTER_ORG_ID%TYPE;
   L_merch_id                   TICKET_TYPE_HEAD.FILTER_MERCH_ID%TYPE;
   L_filter_merch_id_class      TICKET_TYPE_HEAD.FILTER_MERCH_ID_CLASS%TYPE;
   L_filter_merch_id_subclass   TICKET_TYPE_HEAD.FILTER_MERCH_ID_SUBCLASS%TYPE;

   cursor C_CHECK_TICKET_TYPE_TB is
      select tthtl.ticket_type_desc,
             tth.filter_org_id,
             tth.filter_merch_id,
             tth.filter_merch_id_class,
             tth.filter_merch_id_subclass
        from ticket_type_head tth,
             v_ticket_type_head_tl tthtl
       where tth.ticket_type_id = I_ticket_type_id
         and tth.ticket_type_id = tthtl.ticket_type_id;

   cursor C_CHECK_TICKET_TYPE_V is
      select ticket_type_desc
        from v_ticket_type_head
       where ticket_type_id = I_ticket_type_id;

BEGIN
   O_ticket_type_desc := NULL;
   open C_CHECK_TICKET_TYPE_V;
   fetch C_CHECK_TICKET_TYPE_V into O_ticket_type_desc;
   close C_CHECK_TICKET_TYPE_V;
   if O_ticket_type_desc is NULL then
      open C_CHECK_TICKET_TYPE_TB;
      fetch C_CHECK_TICKET_TYPE_TB into O_ticket_type_desc,
                                        L_org_id,
                                        L_merch_id,
                                        L_filter_merch_id_class,
                                        L_filter_merch_id_subclass;
      close C_CHECK_TICKET_TYPE_TB;
      if O_ticket_type_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TCKT_TYPE', NULL, NULL, NULL);
      else
         if GET_NON_VISIBLE_HIER(O_error_message,
                                 L_hier,
                                 L_org_id,
                                 FILTER_POLICY_SQL.GP_system_options.ticket_type_org_level_code,
                                 L_merch_id,
                                 FILTER_POLICY_SQL.GP_system_options.ticket_type_merch_level_code,
                                 L_filter_merch_id_class,
                                 L_filter_merch_id_subclass) = FALSE then
            return FALSE;
         end if;

         if L_hier is not NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_TKT_TYPE', L_hier, I_ticket_type_id);
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_ANY_TKT_TYPE', I_ticket_type_id);
         end if;
      end if;
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_TICKET_TYPE_HEAD;

--------------------------------------------------------------------------------
-- VALIDATE_UDA( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_UDA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_valid         IN OUT BOOLEAN,
                      O_uda           IN OUT V_UDA%ROWTYPE,
                      O_uda_desc      IN OUT V_UDA.UDA_DESC%TYPE,
                      I_uda_id        IN     V_UDA.UDA_ID%TYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_UDA';

   L_hier                       VARCHAR2(128);
   L_org_id                     UDA.FILTER_ORG_ID%TYPE;
   L_merch_id                   UDA.FILTER_MERCH_ID%TYPE;
   L_filter_merch_id_class      UDA.FILTER_MERCH_ID_CLASS%TYPE;
   L_filter_merch_id_subclass   UDA.FILTER_MERCH_ID_SUBCLASS%TYPE;

   cursor C_CHECK_UDA_TB is
      select utl.uda_desc,
             u.filter_org_id,
             u.filter_merch_id,
             u.filter_merch_id_class,
             u.filter_merch_id_subclass
        from uda      u,
             v_uda_tl utl
       where u.uda_id = I_uda_id
         and u.uda_id = utl.uda_id; 
      
   cursor C_CHECK_UDA_V is
      select *
        from v_uda
       where uda_id = I_uda_id;

BEGIN
   O_uda_desc := NULL;
   open C_CHECK_UDA_V;
   fetch C_CHECK_UDA_V into O_uda;
   if C_CHECK_UDA_V%NOTFOUND then
      close C_CHECK_UDA_V;
      open C_CHECK_UDA_TB;
      fetch C_CHECK_UDA_TB into O_uda_desc,
                                L_org_id,
                                L_merch_id,
                                L_filter_merch_id_class,
                                L_filter_merch_id_subclass;
      close C_CHECK_UDA_TB;
      if O_uda_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_UDA', NULL, NULL, NULL);
      else
         if GET_NON_VISIBLE_HIER(O_error_message,
                                 L_hier,
                                 L_org_id,
                                 FILTER_POLICY_SQL.GP_system_options.uda_org_level_code,
                                 L_merch_id,
                                 FILTER_POLICY_SQL.GP_system_options.uda_merch_level_code,
                                 L_filter_merch_id_class,
                                 L_filter_merch_id_subclass) = FALSE then
            return FALSE;
         end if;
         if L_hier is not NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_UDA', L_hier, I_uda_id);
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_ANY_UDA', I_uda_id);
         end if;
      end if;
      O_valid := FALSE;
   else
      close C_CHECK_UDA_V;
      O_uda_desc := O_uda.uda_desc;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_UDA;

--------------------------------------------------------------------------------
-- VALIDATE_UDA( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_UDA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_valid         IN OUT BOOLEAN,
                      O_uda_desc      IN OUT V_UDA.UDA_DESC%TYPE,
                      I_uda_id        IN     V_UDA.UDA_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_UDA';
   L_temp_uda V_UDA%ROWTYPE;

BEGIN

   if(VALIDATE_UDA(O_error_message,
                   O_valid,
                   L_temp_uda,
                   O_uda_desc,
                   I_uda_id) = FALSE) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_UDA;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TRANSFER_STORE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid          IN OUT BOOLEAN,
                                 O_store_name     IN OUT V_TRANSFER_FROM_STORE.STORE_NAME%TYPE,
                                 IO_tsf_entity_id IN OUT V_TRANSFER_FROM_STORE.TSF_ENTITY_ID%TYPE,
                                 I_store          IN     V_TRANSFER_FROM_STORE.STORE%TYPE,
                                 I_to_from_ind    IN     VARCHAR2)
RETURN BOOLEAN IS

   L_inv_parm           VARCHAR2(30);
   L_stockholding_ind   STORE.STOCKHOLDING_IND%TYPE;
   L_tsf_entity_id      STORE.TSF_ENTITY_ID%TYPE;
   L_program            VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_TRANSFER_STORE';

   cursor C_CHECK_STORE_TB is
      select stl.store_name,
             s.stockholding_ind
        from store s,
             v_store_tl stl
       where s.store = I_store
         and s.store = stl.store;

   cursor C_CHECK_STORE_V is
      select store_name,
             tsf_entity_id
        from v_transfer_from_store
       where store = I_store
         and I_to_from_ind = 'F'
       UNION
      select store_name,
             tsf_entity_id
        from v_transfer_to_store
       where store = I_store
         and I_to_from_ind = 'T';

BEGIN

   if I_store is NULL then
      L_inv_parm := 'I_store';
   elsif I_to_from_ind is NULL then
      L_inv_parm := 'I_to_from_ind';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_CHECK_STORE_TB;
   fetch C_CHECK_STORE_TB into O_store_name, L_stockholding_ind;
   close C_CHECK_STORE_TB;

   if L_stockholding_ind is not NULL then

      open C_CHECK_STORE_V;
      fetch C_CHECK_STORE_V into O_store_name,
                                 L_tsf_entity_id;
      if C_CHECK_STORE_V%NOTFOUND then
         close C_CHECK_STORE_V;
         if I_to_from_ind = 'T' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_TRANS_TO', I_store);
         elsif I_to_from_ind = 'F' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_TRANS_FROM', I_store);
         end if;
         O_valid := FALSE;
      else
         close C_CHECK_STORE_V;
         if IO_tsf_entity_id is not NULL and
         (L_tsf_entity_id is NULL or L_tsf_entity_id != IO_tsf_entity_id) then
            O_error_message:= SQL_LIB.CREATE_MSG('LOC_NOTIN_TSF_ENT', I_store, IO_tsf_entity_id, NULL);
            O_valid := FALSE;
         else
            IO_tsf_entity_id := L_tsf_entity_id;
            O_valid := TRUE;
         end if;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_STORE', NULL, NULL, NULL);
      O_valid := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_TRANSFER_STORE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TRANSFER_WH(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid          IN OUT BOOLEAN,
                              O_wh_name        IN OUT V_TRANSFER_FROM_WH.WH_NAME%TYPE,
                              IO_tsf_entity_id IN OUT V_TRANSFER_FROM_WH.TSF_ENTITY_ID%TYPE,
                              I_wh             IN     V_TRANSFER_FROM_WH.WH%TYPE,
                              I_pwh_vwh_ind    IN     VARCHAR2,
                              I_to_from_ind    IN     VARCHAR2)
RETURN BOOLEAN IS

   L_inv_parm          VARCHAR2(30);
   L_code_desc         CODE_DETAIL.CODE_DESC%TYPE;
   L_view_query        VARCHAR2(100);
   L_program           VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_TRANSFER_WH';
   L_stockholding_ind  WH.STOCKHOLDING_IND%TYPE;
   L_org_entity_type   WH.ORG_ENTITY_TYPE%TYPE;
   L_wh                WH.WH_NAME%TYPE;

   cursor C_CHECK_WH_TB is
      select wtl.wh_name,
             w.stockholding_ind,
             w.org_entity_type
        from wh w,
             v_wh_tl wtl
       where w.wh = I_wh
         and w.wh = wtl.wh
         and w.finisher_ind = 'N';
         
   cursor C_CHECK_P_WH is
      select wh_name 
        from v_physical_wh 
       where wh= I_wh;      

BEGIN

   if I_wh is NULL then
      L_inv_parm := 'I_wh';
   elsif I_pwh_vwh_ind is NULL then
      L_inv_parm := 'I_pwh_vwh_ind';
   elsif I_to_from_ind is NULL then
      L_inv_parm := 'I_to_from_ind';
   end if;

   if L_inv_parm is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             L_inv_parm,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   open C_CHECK_WH_TB;
   fetch C_CHECK_WH_TB into O_wh_name,
                            L_stockholding_ind,
                            L_org_entity_type;
   close C_CHECK_WH_TB;
   if O_wh_name is NULL and I_pwh_vwh_ind = 'P' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PWH', NULL, NULL, NULL);
      O_valid := FALSE;
   elsif O_wh_name is NULL and I_pwh_vwh_ind = 'V' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_VWH', NULL, NULL, NULL);
      O_valid := FALSE;
   elsif  I_pwh_vwh_ind = 'V' and L_stockholding_ind = 'N' then
      if L_org_entity_type in('M','X') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('PHYSICAL_WH_NOT_ALLOWED', NULL, NULL, NULL);
      end if;
      O_valid := FALSE;
   else
      -- warehouse exists and has the correct physical/virtual distinction
      if I_pwh_vwh_ind = 'V' then
         L_view_query := 'select wh_name, tsf_entity_id ';

         if I_to_from_ind = 'T' then
            L_view_query := L_view_query||' from v_transfer_to_wh';
         elsif I_to_from_ind = 'F' then
            L_view_query := L_view_query||' from v_transfer_from_wh';
         end if;

         L_view_query := L_view_query||' where wh = :I_wh';

         ---
         DECLARE
            L_tsf_entity_id   WH.TSF_ENTITY_ID%TYPE;
         BEGIN
            -- this sub function is used to more precisely catch errors
            -- thrown by the dynamic query
            EXECUTE IMMEDIATE L_view_query into O_wh_name,
                                             L_tsf_entity_id USING I_wh;

            if IO_tsf_entity_id is not NULL
            and (L_tsf_entity_id is NULL or L_tsf_entity_id != IO_tsf_entity_id) then
               O_error_message:= SQL_LIB.CREATE_MSG('LOC_NOTIN_TSF_ENT', I_wh, IO_tsf_entity_id, NULL);
               O_valid := FALSE;
            else
               IO_tsf_entity_id := L_tsf_entity_id;
            end if;

         EXCEPTION
            when NO_DATA_FOUND then
               -- this exception is thrown if the dynamic query returns no rows
               if I_to_from_ind = 'F' then
                  O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_TRANS_FROM', I_wh);
               elsif I_to_from_ind = 'T' then
                  O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_TRANS_TO', I_wh);
               end if;
               O_valid := FALSE;

            when OTHERS then
               O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                      SQLERRM,
                                                      L_program,
                                                      to_char(SQLCODE));
               return FALSE;
         END;
      ---
      elsif I_pwh_vwh_ind = 'P' then
         open C_CHECK_P_WH;
         fetch C_CHECK_P_WH into L_wh;
                  
         if C_CHECK_P_WH%NOTFOUND then
                 
            if I_to_from_ind = 'F' then
               O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_TRANS_FROM', I_wh);
            elsif I_to_from_ind = 'T' then
               O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_TRANS_TO', I_wh);
            end if;
                  
            O_valid := FALSE;
            return FALSE;
         end if;
         close C_CHECK_P_WH;                     
      end if;  

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_TRANSFER_WH;
--------------------------------------------------------------------------------
-- VALIDATE_LOCATION_TYPE
--------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION_TYPE(O_message       IN OUT VARCHAR2,
                                O_valid         IN OUT BOOLEAN,
                                O_value_desc    IN OUT VARCHAR2,
                                IO_second_value IN OUT VARCHAR2,
                                I_loc_type      IN     VARCHAR2,
                                I_value         IN     VARCHAR2)
         return BOOLEAN is

   QUICK_EXIT    EXCEPTION;
   L_program     VARCHAR2(60) := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION_TYPE';

   L_stockholding_ind  STORE.STOCKHOLDING_IND%TYPE;
   L_wh                V_WH%ROWTYPE;
   L_physical_wh       V_PHYSICAL_WH%ROWTYPE;
   L_tsf_entity_id     V_INTERNAL_FINISHER.TSF_ENTITY_ID%TYPE;

BEGIN
   -- Store, District, Region, Area  (Chain is not done - C = Store Class)
   if I_loc_type in ('S', 'D','R','A') then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_ORG_LEVEL(O_message,
                                                    O_valid,
                                                    O_value_desc,
                                                    I_loc_type,
                                                    I_value) = FALSE then
         raise QUICK_EXIT;
      end if;
   --
      if O_valid and I_loc_type = 'S' then
         if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_message,
                                                   L_stockholding_ind,
                                                   I_value,
                                                   I_loc_type) = FALSE then
            raise QUICK_EXIT;
         end if;

         IO_second_value := L_stockholding_ind;
      end if;

   -- Location Traits
   elsif I_loc_type = 'L' then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOC_TRAITS(O_message,
                                                     O_valid,
                                                     O_value_desc,
                                                     I_value) = FALSE then
         raise QUICK_EXIT;
      end if;

   -- Location Lists (LL), Location Lists - Wh (LLW), Location Lists - St (LLS)
   elsif I_loc_type in ('LL', 'LLW', 'LLS') then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOC_LIST_HEAD(O_message,
                                                        O_valid,
                                                        O_value_desc,
                                                        I_value) = FALSE then
         raise QUICK_EXIT;
      end if;

   -- Store Classes
   elsif I_loc_type = 'C' then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE_CLASS(O_message,
                                                      O_valid,
                                                      O_value_desc,
                                                      I_value) = FALSE then
         raise QUICK_EXIT;
      end if;


   -- Transfer Zone
   elsif I_loc_type = 'T' then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_TSF_ZONE(O_message,
                                                   O_valid,
                                                   O_value_desc,
                                                   I_value) = FALSE then
         raise QUICK_EXIT;
      end if;

   -- Warehouse
   elsif I_loc_type in ('DW', 'W') then

      if FILTER_LOV_VALIDATE_SQL.VALIDATE_WH(O_message,
                                             O_valid,
                                             L_wh,
                                             I_value) = FALSE then
         raise QUICK_EXIT;
      else
         O_value_desc    := L_wh.wh_name;
         IO_second_value := L_wh.stockholding_ind;
         if L_wh.stockholding_ind = 'N' then
            O_message := SQL_LIB.CREATE_MSG('ENTER_STOCK_LOCS', NULL, NULL, NULL);
            O_valid := FALSE;
         end if;
      end if;

   elsif I_loc_type = 'PW' then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_PHYSICAL_WH(O_message,
                                                      O_valid,
                                                      IO_second_value,
                                                      L_physical_wh,
                                                      I_value) = FALSE then
         raise QUICK_EXIT ;
      else
         O_value_desc    := L_physical_wh.wh_name;
      end if;
   elsif I_loc_type = 'I' then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_INTERNAL_FINISHER(O_message,
                                                            O_valid,
                                                            O_value_desc,
                                                            L_tsf_entity_id,
                                                            I_value) = FALSE then
        raise QUICK_EXIT;
      end if;

   elsif I_loc_type = 'E' then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_EXTERNAL_FINISHER(O_message,
                                                            O_valid,
                                                            O_value_desc,
                                                            L_tsf_entity_id,
                                                            I_value) = FALSE then
        raise QUICK_EXIT;
      end if;

   elsif I_loc_type in ('M','X') then
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_IMP_EXP(O_message,
                                                  O_valid,
                                                  O_value_desc,
                                                  I_loc_type,
                                                  I_value) = FALSE then
         raise QUICK_EXIT;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when QUICK_EXIT then
      O_message := sql_lib.create_msg('PACKAGE_ERROR',
                                      O_message,
                                      L_program,
                                      null);

      return FALSE;
   when VALUE_ERROR then
      O_message := SQL_LIB.CREATE_MSG('ENTER_NUM',
                                       NULL,
                                       NULL,
                                       NULL);
      return FALSE;
   when OTHERS then
      O_message := sql_lib.create_msg('PACKAGE_ERROR',
                                      SQLERRM,
                                      L_program,
                                      null);

      return FALSE;
END VALIDATE_LOCATION_TYPE;
--------------------------------------------------------------------------------
-- VALIDATE_MERCH_LEVEL( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_MERCH_LEVEL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid           IN OUT BOOLEAN,
                              O_name            IN OUT VARCHAR2,
                              I_merch_level_ind IN     VARCHAR2,
                              I_merch_level_id  IN     NUMBER,
                              I_class           IN     V_CLASS.CLASS%TYPE,
                              I_subclass        IN     V_SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_MERCH_LEVEL';

BEGIN
   if I_merch_level_ind = 'D' then
      if VALIDATE_DIVISION(O_error_message,
                           O_valid,
                           O_name,
                           I_merch_level_id) = FALSE then
         return FALSE;
      end if;
   elsif I_merch_level_ind = 'G' then
      if VALIDATE_GROUPS(O_error_message,
                         O_valid,
                         O_name,
                         null,
                         I_merch_level_id) = FALSE then
         return FALSE;
      end if;
   elsif I_merch_level_ind = 'P' then
      if VALIDATE_DEPS(O_error_message,
                       O_valid,
                       O_name,
                       null,
                       null,
                       I_merch_level_id) = FALSE then
         return FALSE;
      end if;
   elsif I_merch_level_ind = 'C' then
      if I_class is not NULL then
         if VALIDATE_CLASS(O_error_message,
                           O_valid,
                           O_name,
                           I_merch_level_id,
                           I_class) = FALSE then
            return FALSE;
         end if;
      end if;
   elsif I_merch_level_ind = 'S' then
      if I_subclass is not NULL then
         if VALIDATE_SUBCLASS(O_error_message,
                              O_valid,
                              O_name,
                              I_merch_level_id,
                              I_class,
                              I_subclass) = FALSE then
            return FALSE;
         end if;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_MERCH_LEVEL', NULL, NULL, NULL);
      O_valid := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_MERCH_LEVEL;
--------------------------------------------------------------------------------
-- VALIDATE_ORG_LEVEL( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ORG_LEVEL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT  BOOLEAN,
                            O_name           IN OUT  VARCHAR2,
                            I_org_level_ind  IN      VARCHAR2,
                            I_org_level_id   IN      NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_ORG_LEVEL';

BEGIN
   if I_org_level_ind = 'C' then
      if VALIDATE_CHAIN(O_error_message,
                        O_valid,
                        O_name,
                        I_org_level_id) = FALSE then
         return FALSE;
      end if;
   elsif I_org_level_ind = 'A' then
         if VALIDATE_AREA(O_error_message,
                          O_valid,
                          O_name,
                          null,
                          I_org_level_id) = FALSE then
            return FALSE;
         end if;
      elsif I_org_level_ind = 'R' then
            if VALIDATE_REGION(O_error_message,
                               O_valid,
                               O_name,
                               null,
                               null,
                               I_org_level_id) = FALSE then
               return FALSE;
            end if;
         elsif I_org_level_ind = 'D' then
               if VALIDATE_DISTRICT(O_error_message,
                                    O_valid,
                                    O_name,
                                    null,
                                    null,
                                    null,
                                    I_org_level_id) = FALSE then
                  return FALSE;
               end if;
            elsif I_org_level_ind = 'S' then
                  if VALIDATE_STORE(O_error_message,
                                    O_valid,
                                    O_name,
                                    I_org_level_id) = FALSE then
                     return FALSE;
                  end if;
               else
                  O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_LEVEL', NULL, NULL, NULL);
                  O_valid := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ORG_LEVEL;
----------------------------------------------------------------------------
FUNCTION VALIDATE_STORE_CLASS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid            IN OUT  BOOLEAN,
                              O_store_class_desc IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              I_store_class      IN      STORE.STORE_CLASS%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50) := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE_CLASS';
   L_store_class_desc  CODE_DETAIL.CODE_DESC%TYPE := NULL;
   L_dummy             VARCHAR2(1);

   cursor C_CHECK_STORE_CLASS is
      select 'X'
        from code_detail
       where code_type = 'CSTR'
         and code = I_store_class;

   cursor C_CHECK_STORE_CLASS_TB is
      select c.code_desc
        from store s,
             code_detail c
       where s.store_class = I_store_class
         and c.code = s.store_class
         and c.code_type = 'CSTR'
         and rownum      = 1;

   cursor C_CHECK_STORE_CLASS_V is
      select c.code_desc
        from v_store s,
             code_detail c
       where s.store_class = I_store_class
         and c.code = s.store_class
         and c.code_type = 'CSTR'
         and rownum      = 1;

BEGIN

   if I_store_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_store_class,L_program,NULL);
      return FALSE;
   else
      open  C_CHECK_STORE_CLASS;
      fetch C_CHECK_STORE_CLASS into L_dummy;
      close C_CHECK_STORE_CLASS;

      if L_dummy is NULL then
         O_error_message := SQL_LIB.CREATE_MSG ('INV_STORE_CLASS', NULL, NULL, NULL);
         O_valid := FALSE;
         return TRUE;
      end if;
   end if;

   O_valid := TRUE;

   open  C_CHECK_STORE_CLASS_V;
   fetch C_CHECK_STORE_CLASS_V into L_store_class_desc;
   close C_CHECK_STORE_CLASS_V;
   ---
   O_store_class_desc := L_store_class_desc;

   if L_store_class_desc is NULL then
      ---
      open  C_CHECK_STORE_CLASS_TB;
      fetch C_CHECK_STORE_CLASS_TB into L_store_class_desc;
      close C_CHECK_STORE_CLASS_TB;
      ---
      if L_store_class_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_STORES_EXIST_CLASS', I_store_class, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ANY_STORE_CLASS', I_store_class);
      end if;
      ---
      O_valid := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_STORE_CLASS;
---------------------------------------------------------------------------------------
-- VALIDATE_SHIPMENT()
---------------------------------------------------------------------------------------
FUNCTION VALIDATE_SHIPMENT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid           IN OUT  BOOLEAN,
                           O_shipment        IN OUT  V_SHIPMENT%ROWTYPE,
                           I_shipment        IN      SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SHIPMENT';
   L_from_loc  SHIPMENT.FROM_LOC%TYPE;
   L_to_loc    SHIPMENT.TO_LOC%TYPE;
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_SHIPMENT_V is
      select *
        from v_shipment
       where shipment = I_shipment;

   cursor C_CHECK_SHIPMENT_TB is
      select from_loc, to_loc
        from shipment
       where shipment = I_shipment;

   cursor C_CHECK_FROM_LOC is
      select 'X'
        from shipment s
       where shipment = I_shipment
         and (s.from_loc is NULL
             or s.from_loc in (select store
                                 from v_store
                                union all
                               select wh
                                 from v_wh
                               union all
                               select physical_wh
                                 from v_wh
                               union all
                               select finisher_id
                                 from v_external_finisher
                               union all
                               select finisher_id
                                 from v_internal_finisher))
         and rownum = 1;

   cursor C_CHECK_TO_LOC is
      select 'X'
        from shipment s
       where shipment = I_shipment
         and s.to_loc in (select store
                              from v_store
                             union all
                            select wh
                              from v_wh
                           union all
                           select physical_wh
                             from v_wh
                           union all
                           select finisher_id
                             from v_external_finisher
                           union all
                           select finisher_id
                             from v_internal_finisher)
          and rownum = 1;

   cursor C_CHECK_ITEMS is
      select 'X'
        from shipsku ss,
             v_item_master vim
       where ss.shipment = I_shipment
         and vim.item = ss.item
         and rownum = 1;

BEGIN
   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_shipment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_CHECK_SHIPMENT_V;
   fetch C_CHECK_SHIPMENT_V into O_shipment;
   ---
   if C_CHECK_SHIPMENT_V%NOTFOUND then
      close C_CHECK_SHIPMENT_V;
      ---
      open C_CHECK_SHIPMENT_TB;
      fetch C_CHECK_SHIPMENT_TB into L_from_loc, L_to_loc;
      ---
      if C_CHECK_SHIPMENT_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SHIP', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ANY_ITEM_SHIP', I_shipment);
      end if;
      ---
      close C_CHECK_SHIPMENT_TB;
      ---
      O_valid := FALSE;
   else
      close C_CHECK_SHIPMENT_V;
      ---
      O_valid := TRUE;
      ---
      open C_CHECK_FROM_LOC;
      fetch C_CHECK_FROM_LOC into L_dummy;
      ---
      if C_CHECK_FROM_LOC%NOTFOUND then
         close C_CHECK_FROM_LOC;
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_LOC_SHIP',O_shipment.from_loc, I_shipment);
         O_valid := FALSE;
      else
         close C_CHECK_FROM_LOC;
         open C_CHECK_TO_LOC;
         fetch C_CHECK_TO_LOC into L_dummy;
         ---
         if C_CHECK_TO_LOC%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_LOC_SHIP', O_shipment.to_loc, I_shipment);
            O_valid := FALSE;
         end if;
         close C_CHECK_TO_LOC;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SHIPMENT;
----------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid          IN OUT  BOOLEAN,
                        O_ordhead        IN OUT  V_ORDHEAD%ROWTYPE,
                        I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)       := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_ORDER';

   cursor C_CHECK_ORDER_TB is
      select *
        from ordhead
       where order_no = I_order_no;

   cursor C_CHECK_ORDER_V is
      select *
        from v_ordhead
       where order_no = I_order_no;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_order_no,L_program,NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   open  C_CHECK_ORDER_V;
   fetch C_CHECK_ORDER_V into O_ordhead;
   ---
   if C_CHECK_ORDER_V%NOTFOUND then
      close C_CHECK_ORDER_V;
      ---
      open  C_CHECK_ORDER_TB;
      fetch C_CHECK_ORDER_TB into O_ordhead;
      ---
      if C_CHECK_ORDER_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO');
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ORDER', I_order_no, NULL);
      end if;
      ---
      O_valid := FALSE;
      close C_CHECK_ORDER_TB;
   else
      close C_CHECK_ORDER_V;
   end if;

 return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ORDER;
----------------------------------------------------------------------------
FUNCTION VALIDATE_RTV_ORDER_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid          IN OUT  BOOLEAN,
                               O_rtv_head_row   IN OUT  V_RTV_HEAD%ROWTYPE,
                               I_rtv_order_no   IN      RTV_HEAD.RTV_ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'FILTER_LOV_VALIDATE_SQL.EXISTS_ON_VIEW';
   L_rtv_order_no  RTV_HEAD.RTV_ORDER_NO%TYPE;

   cursor C_RTV_HEAD_V is
      select *
        from v_rtv_head
       where rtv_order_no = I_rtv_order_no;

   cursor C_RTV_HEAD_TB is
      select rtv_order_no
        from rtv_head
       where rtv_order_no = I_rtv_order_no;

BEGIN

   if I_rtv_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC', 'I_rtv_order_no', 'NULL', L_program);
      return FALSE;
   end if;

   O_rtv_head_row := NULL;
   O_valid        := TRUE;

   open  C_RTV_HEAD_V;
   fetch C_RTV_HEAD_V into O_rtv_head_row;
   close C_RTV_HEAD_V;
   ---
   if O_rtv_head_row.rtv_order_no is NULL then
      open  C_RTV_HEAD_TB;
      fetch C_RTV_HEAD_TB into L_rtv_order_no;
      close C_RTV_HEAD_TB;
      ---
      if L_rtv_order_no is NULL then
         O_error_message := sql_lib.create_msg('INV_RTV', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_RTV', I_rtv_order_no, NULL);
      end if;
      O_valid := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
      return FALSE;
END VALIDATE_RTV_ORDER_NO;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_tsfhead_row   IN OUT V_TSFHEAD%ROWTYPE,
                         I_tsf_no        IN     V_TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_TSF_NO';

   cursor C_TSFHEAD_V is
      select *
        from v_tsfhead
       where tsf_no = I_tsf_no;

   cursor C_TSFHEAD_TB is
      select tsf_no
        from tsfhead
       where tsf_no = I_tsf_no
         and (tsf_no = tsf_parent_no
              or tsf_parent_no is NULL);  --eliminates the tsf no for the second leg of transfers with finishing

BEGIN

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_tsf_no',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_TSFHEAD_V;
   fetch C_TSFHEAD_V into O_tsfhead_row;
   if C_TSFHEAD_V%NOTFOUND then
      close C_TSFHEAD_V;
      open C_TSFHEAD_TB;
      fetch C_TSFHEAD_TB into O_tsfhead_row.tsf_no;
      if C_TSFHEAD_TB%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_TSF', I_tsf_no, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF', NULL, NULL, NULL);
      end if;
      close C_TSFHEAD_TB;
      O_valid := FALSE;
   else
      close C_TSFHEAD_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_TSF_NO;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_TMPL_HEAD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid          IN OUT BOOLEAN,
                                 O_pack_tmpl_head IN OUT V_PACK_TMPL_HEAD%ROWTYPE,
                                 I_pack_tmpl_id   IN     V_PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_PACK_TMPL_HEAD';

   cursor C_CHECK_PACK_TMPL_HEAD_TB is
      select pack_tmpl_desc
        from v_pack_tmpl_head_tl
       where pack_tmpl_id = I_pack_tmpl_id;

   cursor C_CHECK_PACK_TMPL_HEAD_V is
      select *
        from v_pack_tmpl_head
       where pack_tmpl_id = I_pack_tmpl_id;

BEGIN

   if I_pack_tmpl_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_pack_tmpl_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_CHECK_PACK_TMPL_HEAD_V;
   fetch C_CHECK_PACK_TMPL_HEAD_V into O_pack_tmpl_head;
   if C_CHECK_PACK_TMPL_HEAD_V%NOTFOUND then
      close C_CHECK_PACK_TMPL_HEAD_V;
      open C_CHECK_PACK_TMPL_HEAD_TB;
      fetch C_CHECK_PACK_TMPL_HEAD_TB into O_pack_tmpl_head.pack_tmpl_desc;
      if C_CHECK_PACK_TMPL_HEAD_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('PACK_TMPL_NOT_EXIST', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_PACK_TMPL', I_pack_tmpl_id, NULL);
      end if;
      close C_CHECK_PACK_TMPL_HEAD_TB;

      O_valid := FALSE;
   else
      close C_CHECK_PACK_TMPL_HEAD_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_PACK_TMPL_HEAD;
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF_RATIO_ID(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT BOOLEAN,
                                O_diff_ratio_head IN OUT V_DIFF_RATIO_HEAD%ROWTYPE,
                                I_diff_ratio_id   IN     V_DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_DIFF_RATIO_ID';
   L_dept      DIFF_RATIO_HEAD.DEPT%TYPE;

   cursor C_DIFF_RATIO_HEAD_TB is
      select drhtl.description, 
             drh.dept
        from diff_ratio_head drh,
             v_diff_ratio_head_tl drhtl
       where drh.diff_ratio_id = I_diff_ratio_id
         and drh.diff_ratio_id = drhtl.diff_ratio_id;

   cursor C_DIFF_RATIO_HEAD_V is
      select *
        from v_diff_ratio_head
       where diff_ratio_id = I_diff_ratio_id;

BEGIN

   if I_diff_ratio_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_diff_ratio_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_DIFF_RATIO_HEAD_V;
   fetch C_DIFF_RATIO_HEAD_V into O_diff_ratio_head;
   if C_DIFF_RATIO_HEAD_V%NOTFOUND then
      close C_DIFF_RATIO_HEAD_V;
      open C_DIFF_RATIO_HEAD_TB;
      fetch C_DIFF_RATIO_HEAD_TB into O_diff_ratio_head.description, L_dept;
      if C_DIFF_RATIO_HEAD_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_RATIO', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_DIFF_RATIO', I_diff_ratio_id);
      end if;
      close C_DIFF_RATIO_HEAD_TB;
      O_valid := FALSE;
   else
      close C_DIFF_RATIO_HEAD_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_DIFF_RATIO_ID;
----------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_ZONE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid         IN OUT BOOLEAN,
                            O_tsf_desc      IN OUT TSFZONE.DESCRIPTION%TYPE,
                            I_tsf_zone      IN     TSFZONE.TRANSFER_ZONE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_TSF_ZONE';

   cursor C_TSF_ZONE_V is
      select distinct ttl.description
        from v_tsfzone_tl ttl,
             v_store v
       where ttl.transfer_zone = I_tsf_zone
         and ttl.transfer_zone = v.transfer_zone;

   cursor C_TSF_ZONE_TB is
      select description
        from v_tsfzone_tl
       where transfer_zone = I_tsf_zone;

BEGIN

   if I_tsf_zone is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_zone',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_TSF_ZONE_V;
   fetch C_TSF_ZONE_V into O_tsf_desc;

   if C_TSF_ZONE_V%NOTFOUND then
      close C_TSF_ZONE_V;
      open C_TSF_ZONE_TB;
      fetch C_TSF_ZONE_TB into O_tsf_desc;
      if C_TSF_ZONE_TB%FOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ANY_TSF_ZONE', I_tsf_zone);
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_TRAN_ZONE', NULL, NULL, NULL);
      end if;
      close C_TSF_ZONE_TB;
      O_valid := FALSE;
   else
      close C_TSF_ZONE_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_TSF_ZONE;
--------------------------------------------------------------------------------
-- VALIDATE_INTERNAL_FINISHER( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_INTERNAL_FINISHER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_valid            IN OUT BOOLEAN,
                                    O_finisher_desc    IN OUT V_INTERNAL_FINISHER.FINISHER_DESC%TYPE,
                                    IO_tsf_entity_id   IN OUT V_INTERNAL_FINISHER.TSF_ENTITY_ID%TYPE,
                                    I_finisher         IN     V_INTERNAL_FINISHER.FINISHER_ID%TYPE)

RETURN BOOLEAN IS

   L_tsf_entity_id      STORE.TSF_ENTITY_ID%TYPE;
   L_program            VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_INT_FINISHER';

   cursor C_CHECK_FINISHER_TB is
      select wtl.wh_name
        from v_wh_tl wtl,
             wh w
       where w.wh = I_finisher
         and w.wh = wtl.wh
         and w.finisher_ind = 'Y';

   cursor C_CHECK_FINISHER_V is
      select finisher_desc,
             tsf_entity_id
        from v_internal_finisher
       where finisher_id = I_finisher;

BEGIN

   if I_finisher is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             I_finisher,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_CHECK_FINISHER_V;
   fetch C_CHECK_FINISHER_V into O_finisher_desc,
                                 L_tsf_entity_id;

   if C_CHECK_FINISHER_V%NOTFOUND then
      close C_CHECK_FINISHER_V;
      open C_CHECK_FINISHER_TB;
      fetch C_CHECK_FINISHER_TB into O_finisher_desc;
      if C_CHECK_FINISHER_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_INT_FINISHER', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_finisher, NULL);
      end if;
      close C_CHECK_FINISHER_TB;
      O_valid := FALSE;
   else
      close C_CHECK_FINISHER_V;
      if IO_tsf_entity_id is not NULL and
        (L_tsf_entity_id is NULL or L_tsf_entity_id != IO_tsf_entity_id) then
            O_error_message:= SQL_LIB.CREATE_MSG('FIN_NOTIN_TSF_ENT', I_finisher, IO_tsf_entity_id, NULL);
            O_valid := FALSE;
      else
         IO_tsf_entity_id := L_tsf_entity_id;
            O_valid := TRUE;
       end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_INTERNAL_FINISHER;
--------------------------------------------------------------------------------
-- VALIDATE_EXTERNAL_FINISHER( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_EXTERNAL_FINISHER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_valid            IN OUT BOOLEAN,
                                    O_finisher_desc    IN OUT V_EXTERNAL_FINISHER.FINISHER_DESC%TYPE,
                                    IO_tsf_entity_id   IN OUT V_EXTERNAL_FINISHER.TSF_ENTITY_ID%TYPE,
                                    I_finisher         IN     V_EXTERNAL_FINISHER.FINISHER_ID%TYPE)

RETURN BOOLEAN IS

   L_tsf_entity_id      V_EXTERNAL_FINISHER.TSF_ENTITY_ID%TYPE;
   L_program            VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_EXTERNAL_FINISHER';

   cursor C_CHECK_FINISHER_TB is
      select partner_desc
        from v_partner_tl
       where partner_id = I_finisher
         and partner_type = 'E';

   cursor C_CHECK_FINISHER_V is
      select finisher_desc,
             tsf_entity_id
        from v_external_finisher
       where finisher_id = I_finisher;

BEGIN

   if I_finisher is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             I_finisher,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_CHECK_FINISHER_V;
   fetch C_CHECK_FINISHER_V into O_finisher_desc,
                                 L_tsf_entity_id;

   if C_CHECK_FINISHER_V%NOTFOUND then
      close C_CHECK_FINISHER_V;
      open C_CHECK_FINISHER_TB;
      fetch C_CHECK_FINISHER_TB into O_finisher_desc;
      if C_CHECK_FINISHER_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_FINISHER', NULL, NULL, NULL);
      else
        O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_finisher, NULL);
      end if;
      close C_CHECK_FINISHER_TB;
      O_valid := FALSE;
   else
      close C_CHECK_FINISHER_V;
      if  IO_tsf_entity_id is not NULL
      and (L_tsf_entity_id is NULL or L_tsf_entity_id != IO_tsf_entity_id) then
         O_error_message:= SQL_LIB.CREATE_MSG('FIN_NOTIN_TSF_ENT', I_finisher, IO_tsf_entity_id, NULL);
         O_valid := FALSE;
      else
         IO_tsf_entity_id := L_tsf_entity_id;
         O_valid := TRUE;
       end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_EXTERNAL_FINISHER;
--------------------------------------------------------------------------------
-- VALIDATE_TSF_ENTITY( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_ENTITY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid           IN OUT BOOLEAN,
                             O_tsf_entity_row  IN OUT V_TSF_ENTITY%ROWTYPE,
                             I_tsf_entity_id   IN     V_TSF_ENTITY.TSF_ENTITY_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_TSF_ENTITY';

   cursor C_CHECK_TSF_ENTITY_V is
      select vte.*
        from v_tsf_entity vte
       where vte.tsf_entity_id = I_tsf_entity_id;

   cursor C_CHECK_TSF_ENTITY_TB is
      select tsf_entity_desc
        from v_tsf_entity_tl
       where tsf_entity_id = I_tsf_entity_id;

BEGIN

   if I_tsf_entity_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_entity_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   open C_CHECK_TSF_ENTITY_V;
   fetch C_CHECK_TSF_ENTITY_V into O_tsf_entity_row;
   if C_CHECK_TSF_ENTITY_V%NOTFOUND then
      close C_CHECK_TSF_ENTITY_V;
      open C_CHECK_TSF_ENTITY_TB;
      fetch C_CHECK_TSF_ENTITY_TB into O_tsf_entity_row.tsf_entity_desc;
      if C_CHECK_TSF_ENTITY_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ENTITY', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_TSF_ENT', I_tsf_entity_id, NULL);
      end if;
      close C_CHECK_TSF_ENTITY_TB;
      O_valid := FALSE;
   else
      close C_CHECK_TSF_ENTITY_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
RETURN FALSE;

END VALIDATE_TSF_ENTITY;
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_SHIP_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid          IN OUT  BOOLEAN,
                             O_ordhead        IN OUT  ORDHEAD.ORDER_NO%TYPE,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)       := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SHIP_ORDER';

   cursor C_CHECK_SHIP_ORDER_TB is
      select distinct(ordhead.order_no)
        from ordhead,
             shipment s,
             shipsku ss
        where ordhead.order_no = s.order_no
          and s.shipment = ss.shipment
          and ordhead.status in ('A', 'C')
          and ordhead.order_no = I_order_no;

   cursor C_CHECK_SHIP_ORDER_V is
      select distinct(ordhead.order_no)
        from ordhead,
             v_shipment,
             shipsku
       where v_shipment.order_no = I_order_no
         and ordhead.order_no = v_shipment.order_no
         and ordhead.status in ('A', 'C')
         and v_shipment.shipment = shipsku.shipment;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_order_no,L_program,NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   open  C_CHECK_SHIP_ORDER_V;
   fetch C_CHECK_SHIP_ORDER_V into O_ordhead;
   ---
   if C_CHECK_SHIP_ORDER_V%NOTFOUND then
      close C_CHECK_SHIP_ORDER_V;
      ---
      open  C_CHECK_SHIP_ORDER_TB;
      fetch C_CHECK_SHIP_ORDER_TB into O_ordhead;
      ---
      if C_CHECK_SHIP_ORDER_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_SEARCH', I_order_no);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ORDER', I_order_no, NULL);
      end if;
      ---
      O_valid := FALSE;
      close C_CHECK_SHIP_ORDER_TB;
   else
      close C_CHECK_SHIP_ORDER_V;
   end if;

 return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SHIP_ORDER;

----------------------------------------------------------------------------
FUNCTION VALIDATE_CONTRACT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid            IN OUT  BOOLEAN,
                           O_contract_header  IN OUT  V_CONTRACT_HEADER%ROWTYPE,
                           I_contract_no      IN      CONTRACT_HEADER.CONTRACT_NO%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50)       := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_CONTRACT';
   cursor C_CHECK_CONTRACT_TB is
      select *
        from contract_header
       where contract_no = I_contract_no;

   cursor C_CHECK_CONTRACT_V is
      select *
        from v_contract_header
       where contract_no = I_contract_no;

BEGIN

   if I_contract_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_contract_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_CONTRACT_V',
                    'V_CONTRACT_HEADER',
                     NULL);
   open C_CHECK_CONTRACT_V;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_CONTRACT_V',
                    'V_CONTRACT_HEADER',
                     NULL);
   fetch C_CHECK_CONTRACT_V into O_contract_header;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_CONTRACT_V',
                    'V_CONTRACT_HEADER',
                     NULL);
   close C_CHECK_CONTRACT_V;
   if O_contract_header.contract_no is NULL then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_CONTRACT_TB',
                       'CONTRACT_HEADER',
                       NULL);
      open C_CHECK_CONTRACT_TB;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_CONTRACT_TB',
                       'CONTRACT_HEADER',
                       NULL);
      fetch C_CHECK_CONTRACT_TB into O_contract_header;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_CONTRACT_TB',
                       'CONTRACT_HEADER',
                       NULL);
      close C_CHECK_CONTRACT_TB;
      ---
      if O_contract_header.contract_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CONTRACT_SEARCH', I_contract_no);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_CONTRACT', I_contract_no, NULL);
      end if;
      ---
      O_valid := FALSE;
      ---
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_CONTRACT;

----------------------------------------------------------------------------
FUNCTION VALIDATE_COUPON_ID (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid          IN OUT  BOOLEAN,
                             I_coupon_id      IN      POS_COUPON_HEAD.COUPON_ID%TYPE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(100) := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_COUPON_ID';
   L_dummy                      VARCHAR2(1);
   L_hier                       VARCHAR2(128);
   L_filter_org_id              FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE;
   L_filter_org_level           FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE;
   L_filter_merch_id            FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE;
   L_filter_merch_level         FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE;
   L_filter_merch_id_class      FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE;
   L_filter_merch_id_subclass   FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE;


   cursor C_CHECK_POS_COUPON_TB is
      select 'x'
        from pos_coupon_head
       where coupon_id = I_coupon_id;

   cursor C_CHECK_POS_COUPON_V is
      select 'x'
        from v_pos_coupon_head
       where coupon_id = I_coupon_id;

   cursor C_CHECK_FILTER_ORG is
      select b.filter_org_id,
             b.filter_org_level
        from filter_group_org b,
             sec_user_group a,
             sec_user su
       where a.group_id          = b.sec_group_id
         AND a.user_seq          = su.user_seq
         AND (su.application_user_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO'))
          OR (SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') is NULL and su.database_user_id = NVL(SYS_CONTEXT('USERENV','CLIENT_INFO'),SYS_CONTEXT('USERENV','SESSION_USER'))));

   cursor C_CHECK_FILTER_MERCH is
      select b.filter_merch_id,
             b.filter_merch_level,
             b.filter_merch_id_class,
             b.filter_merch_id_subclass
        from filter_group_merch b,
             sec_user_group a,
             sec_user su
       where a.group_id          = b.sec_group_id
         AND a.user_seq          = su.user_seq
         AND (su.application_user_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO'))
          OR (SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') is NULL and su.database_user_id = NVL(SYS_CONTEXT('USERENV','CLIENT_INFO'),SYS_CONTEXT('USERENV','SESSION_USER'))));

BEGIN

   if I_coupon_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_coupon_id,L_program,NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   open C_CHECK_POS_COUPON_V;
   fetch C_CHECK_POS_COUPON_V into L_dummy;
   ---
   if C_CHECK_POS_COUPON_V%NOTFOUND then
      close C_CHECK_POS_COUPON_V;
      open C_CHECK_POS_COUPON_TB;
      fetch C_CHECK_POS_COUPON_TB into L_dummy;
      ---
      if C_CHECK_POS_COUPON_TB%NOTFOUND then
         close C_CHECK_POS_COUPON_TB;
         O_error_message := SQL_LIB.CREATE_MSG('POS_INV_COUPON');
         return FALSE;
      else
         close C_CHECK_POS_COUPON_TB;
         ---
         open C_CHECK_FILTER_ORG;
         fetch C_CHECK_FILTER_ORG into L_filter_org_id,
                                       L_filter_org_level;
         close C_CHECK_FILTER_ORG;
         ---
         open C_CHECK_FILTER_MERCH;
         fetch C_CHECK_FILTER_MERCH into L_filter_merch_id,
                                         L_filter_merch_level,
                                         L_filter_merch_id_class,
                                         L_filter_merch_id_subclass;
         close C_CHECK_FILTER_MERCH;
         ---
         if GET_NON_VISIBLE_HIER(O_error_message,
                                 L_hier,
                                 L_filter_org_id,
                                 L_filter_org_level,
                                 L_filter_merch_id,
                                 L_filter_merch_level,
                                 L_filter_merch_id_class,
                                 L_filter_merch_id_subclass) = FALSE then
            return FALSE;
         end if;
         ---
         if L_hier IS NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY', L_hier, I_coupon_id);
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ANY_COUPON', I_coupon_id);
         end if;
         ---
         O_valid := FALSE;
      end if;   -- c_check_coupon_merch found
   else
      close C_CHECK_POS_COUPON_V;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END VALIDATE_COUPON_ID;

----------------------------------------------------------------------------
FUNCTION VALIDATE_CE_ID (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid           IN OUT   BOOLEAN,
                         O_ce_head         IN OUT   CE_HEAD%ROWTYPE,
                         I_ce_id           IN       CE_HEAD.CE_ID%TYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(50)  := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_CE_ID';
   L_hier                       VARCHAR2(128);
   L_filter_org_id              FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE;
   L_filter_org_level           FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE;
   L_filter_merch_id            FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE;
   L_filter_merch_level         FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE;
   L_filter_merch_id_class      FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE;
   L_filter_merch_id_subclass   FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE;
   L_found                      BOOLEAN;

   cursor   C_CEH is
   select   ceh.*
     from   ce_head   ceh
    where   ceh.ce_id = I_ce_id;

   cursor C_CHECK_CE_V is
      select *
        from v_ce_head
       where ce_id = I_ce_id;

   cursor C_CHECK_FILTER_ORG is
      select b.filter_org_id,
             b.filter_org_level
        from filter_group_org b,
             sec_user_group a,
             sec_user su
       where a.group_id = b.sec_group_id
         and a.user_seq = su.user_seq
         and (su.application_user_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO'))
          or (SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') is NULL and su.database_user_id = NVL(SYS_CONTEXT('USERENV','CLIENT_INFO'),SYS_CONTEXT('USERENV','SESSION_USER'))));


   cursor C_CHECK_FILTER_MERCH is
      select b.filter_merch_id,
             b.filter_merch_level,
             b.filter_merch_id_class,
             b.filter_merch_id_subclass
        from filter_group_merch b,
             sec_user_group a,
             sec_user su
       where a.group_id = b.sec_group_id
         and a.user_seq = su.user_seq
         and (su.application_user_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO'))
          or (SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') is NULL and su.database_user_id = NVL(SYS_CONTEXT('USERENV','CLIENT_INFO'),SYS_CONTEXT('USERENV','SESSION_USER'))));


BEGIN
   if I_ce_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_ce_id,L_program,NULL);
      return FALSE;
   end if;
   ---
   O_valid := TRUE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CEH',
                    'CE_HEAD',
                    'I_ce_id='||I_ce_id);

   open C_CEH;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CEH',
                    'CE_HEAD',
                    'I_ce_id='||I_ce_id);

   fetch C_CEH
    into O_ce_head;

   L_found := C_CEH%FOUND;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CEH',
                    'CE_HEAD',
                    'I_ce_id='||I_ce_id);

   close C_CEH;

   if not L_found then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CE_ID',
                                            I_ce_id);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_CE_V',
                    'V_CE_HEAD',
                    I_ce_id);
   open C_CHECK_CE_V;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_CE_V',
                    'V_CE_HEAD',
                    I_ce_id);
   fetch C_CHECK_CE_V into O_ce_head;
   ---
   if C_CHECK_CE_V%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_CE_V',
                       'CE_HEAD',
                       I_ce_id);
      close C_CHECK_CE_V;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_FILTER_ORG',
                       'FILTER_GROUP_ORG',
                       NULL);
      open C_CHECK_FILTER_ORG;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_FILTER_ORG',
                       'FILTER_GROUP_ORG',
                       NULL);
      fetch C_CHECK_FILTER_ORG into L_filter_org_id,
                                    L_filter_org_level;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_FILTER_ORG',
                       'FILTER_GROUP_ORG',
                       NULL);
      close C_CHECK_FILTER_ORG;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_FILTER_MERCH',
                       'FILTER_GROUP_MERCH',
                        NULL);
      open C_CHECK_FILTER_MERCH;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_FILTER_MERCH',
                       'FILTER_GROUP_MERCH',
                       NULL);
      fetch C_CHECK_FILTER_MERCH into L_filter_merch_id,
                                      L_filter_merch_level,
                                      L_filter_merch_id_class,
                                      L_filter_merch_id_subclass;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_FILTER_MERCH',
                       'FILTER_GROUP_MERCH',
                        NULL);
      close C_CHECK_FILTER_MERCH;
      ---
      if NOT GET_NON_VISIBLE_HIER(O_error_message,
                                  L_hier,
                                  L_filter_org_id,
                                  L_filter_org_level,
                                  L_filter_merch_id,
                                  L_filter_merch_level,
                                  L_filter_merch_id_class,
                                  L_filter_merch_id_subclass) then
         return FALSE;
      end if;
      ---
      if L_hier is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY', L_hier, I_ce_id);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ANY_CE_ID', I_ce_id);
      end if;
      ---
      O_valid := FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_CE_V',
                       'CE_HEAD',
                       I_ce_id);
      close C_CHECK_CE_V;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END VALIDATE_CE_ID;

--------------------------------------------------------------------------------
FUNCTION MANUAL_LOC_TRAITS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid           IN OUT   BOOLEAN,
                           O_description     IN OUT   V_LOC_TRAITS.DESCRIPTION%TYPE,
                           I_loc_trait       IN       V_LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)                    := 'FILTER_LOV_VALIDATE_SQL.MANUAL_LOC_TRAITS';
   L_exists     VARCHAR2(1);
   L_org_code   VARCHAR2(4);
   L_org_id     LOC_TRAITS.FILTER_ORG_ID%TYPE   := NULL;

   cursor C_CHECK_LOC_TRAIT_TB is
      select lttl.description,
             lt.filter_org_id
        from loc_traits lt,
             v_loc_traits_tl lttl
       where lt.loc_trait = I_loc_trait
         and lt.loc_trait = lttl.loc_trait;

   cursor C_CHECK_LOC_TRAIT_V is
      select description
        from v_loc_traits
       where loc_trait = I_loc_trait;

   cursor C_CHECK_LOC_TRAITS_MATRIX is
      select 'X'
        from loc_traits_matrix l,
             v_store s
       where l.loc_trait = I_loc_trait
         and l.store     = s.store
         and rownum      = 1;

BEGIN
   if I_loc_trait is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_loc_trait,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_description := NULL;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_LOC_TRAIT_V',
                    'v_loc_traits',
                    'loc_trait: ' ||  to_char(I_loc_trait));
   open C_CHECK_LOC_TRAIT_V;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_LOC_TRAIT_V',
                    'v_loc_traits',
                    'loc_trait: ' ||  to_char(I_loc_trait));
   fetch C_CHECK_LOC_TRAIT_V into O_description;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_LOC_TRAIT_V',
                    'v_loc_traits',
                    'loc_trait: ' ||  to_char(I_loc_trait));
   close C_CHECK_LOC_TRAIT_V;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_LOC_TRAITS_MATRIX',
                    'loc_traits_matrix, v_store',
                    'loc_trait: ' ||  to_char(I_loc_trait));
   open C_CHECK_LOC_TRAITS_MATRIX;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_LOC_TRAITS_MATRIX',
                    'loc_traits_matrix, v_store',
                    'loc_trait: ' ||  to_char(I_loc_trait));
   fetch C_CHECK_LOC_TRAITS_MATRIX into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_LOC_TRAITS_MATRIX',
                    'loc_traits_matrix, v_store',
                    'loc_trait: ' ||  to_char(I_loc_trait));
   close C_CHECK_LOC_TRAITS_MATRIX;

   if O_description is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_LOC_TRAIT_TB',
                       'loc_traits',
                       'loc_trait: ' ||  to_char(I_loc_trait));
      open C_CHECK_LOC_TRAIT_TB;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_LOC_TRAIT_TB',
                       'loc_traits',
                       'loc_trait: ' ||  to_char(I_loc_trait));
      fetch C_CHECK_LOC_TRAIT_TB into O_description, L_org_id;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_LOC_TRAIT_TB',
                       'loc_traits',
                       'loc_trait: ' ||  to_char(I_loc_trait));
      close C_CHECK_LOC_TRAIT_TB;

      if O_description is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TRAIT',
                                               NULL,
                                               NULL,
                                               NULL);
      else
         if GET_ORG_DYNAMIC_CODE(O_error_message,
                                 L_org_code,
                                 FILTER_POLICY_SQL.GP_system_options.loc_trait_org_level_code) = FALSE then
            return FALSE;
         end if;
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_FUNC_LOC_TRAIT',
                                               L_org_code ||' '|| TO_CHAR(L_org_id),
                                               I_loc_trait,
                                               NULL);
      end if;
      O_valid := FALSE;
   elsif L_exists is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ALL_LOCS_LT',
                                            I_loc_trait,
                                            NULL,
                                            NULL);
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MANUAL_LOC_TRAITS;

----------------------------------------------------------------------------
FUNCTION VALIDATE_MRT_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid_mrt      IN OUT  BOOLEAN,
                         O_mrt_row        IN OUT  V_MRT_ITEM%ROWTYPE,
                         I_mrt_no         IN      MRT.MRT_NO%TYPE)
RETURN BOOLEAN
 IS

   L_program      VARCHAR2(50)       := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_MRT_NO';

   cursor C_CHECK_MRT_NO is
      select *
        from v_mrt_item
       where mrt_no = I_mrt_no;

   L_dummy   VARCHAR2(1);

BEGIN

   if I_mrt_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_mrt_no,L_program,NULL);
      return FALSE;
   end if;

   open  C_CHECK_MRT_NO;
   fetch C_CHECK_MRT_NO into O_mrt_row;
   O_valid_mrt := C_CHECK_MRT_NO%FOUND;
   close C_CHECK_MRT_NO;

return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_MRT_NO;

----------------------------------------------------------------------------
FUNCTION MANUAL_COMP_STORE(O_error_message   IN OUT   VARCHAR2,
                           O_valid           IN OUT   BOOLEAN,
                           O_store_name      IN OUT   V_COMP_STORE.STORE_NAME%TYPE,
                           O_competitor      IN OUT   COMPETITOR.COMPETITOR%TYPE,
                           O_comp_name       IN OUT   COMPETITOR.COMP_NAME%TYPE,
                           O_currency        IN OUT   V_COMP_STORE.CURRENCY_CODE%TYPE,
                           I_comp_store      IN       V_COMP_STORE.STORE%TYPE)
   RETURN BOOLEAN IS

   L_competitor  competitor.competitor%TYPE := O_competitor;
   L_comp_name   competitor.comp_name%TYPE  := O_comp_name;

   cursor C_STORE is
      select cs.store_name
        from comp_store cs,
             competitor c
       where c.competitor = cs.competitor
         and cs.store = I_comp_store;
   cursor C_STORE_V is
      select cs.store_name,
             cs.competitor,
             c.comp_name,
             cs.currency_code
        from v_comp_store cs,
             competitor c
       where c.competitor = cs.competitor
         and cs.store = I_comp_store;

BEGIN

   if I_comp_store is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_COMP_STORE',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;

   --select from v_comp_store
   SQL_LIB.SET_MARK('OPEN',
                    'C_STORE_V',
                    'V_COMP_STORE',
                    'V_COMP_STORE: '|| I_comp_store);
   open C_STORE_V;

   SQL_LIB.SET_MARK('FETCH',
                    'C_STORE_V',
                    'V_COMP_STORE',
                    'V_COMP_STORE: '|| I_comp_store);
   fetch C_STORE_V into O_store_name,
                        O_competitor,
                        O_comp_name,
                        O_currency;
   --

   if C_STORE_V%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_STORE_V',
                       'V_COMP_STORE',
                       'V_COMP_STORE: '|| I_comp_store);
      close C_STORE_V;

      --select from comp_store
      SQL_LIB.SET_MARK('OPEN',
                       'C_STORE',
                       'COMP_STORE',
                       'COMP_STORE: '|| I_comp_store);
      open C_STORE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_STORE',
                       'COMP_STORE',
                       'COMP_STORE: '|| I_comp_store);
      fetch C_STORE into O_store_name;
      --
      if C_STORE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COMP_STORE', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_comp_store, NULL);
      end if;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_STORE',
                       'COMP_STORE',
                       'COMP_STORE: '|| I_comp_store);
      close C_STORE;
      O_valid := FALSE;

   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_STORE_V',
                       'V_COMP_STORE',
                       'V_COMP_STORE: '|| I_comp_store);
      O_valid := TRUE;
      close C_STORE_V;

      if L_competitor is NOT NULL then
         if L_competitor != O_competitor then
            O_competitor := L_competitor;
            O_comp_name  := L_comp_name;

            --- Invalid competitor/competitor store combination ---
            O_error_message:= SQL_LIB.CREATE_MSG('INVALID_COMP_LINK',
                                                 NULL,
                                                 NULL,
                                                 NULL);
            return FALSE;
         end if;
      end if;

   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'FILTER_LOV_VALIDATE_SQL.MANUAL_COMP_STORE',
                                             TO_CHAR(SQLCODE));
   RETURN FALSE;
END MANUAL_COMP_STORE;
----------------------------------------------------------------------------
FUNCTION VALIDATE_MRT_ITEM(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid_mrt      IN OUT  BOOLEAN,
                         O_mrt_row        IN OUT  V_MRT_ITEM%ROWTYPE,
                         I_mrt_no         IN      V_MRT_ITEM.MRT_NO%TYPE ,
                         I_item           IN      V_MRT_ITEM.ITEM%TYPE )
RETURN BOOLEAN
 IS

   L_program      VARCHAR2(50)       := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_MRT_ITEM';

   cursor C_CHECK_MRT_ITEM is
      select *
        from v_mrt_item
       where mrt_no = I_mrt_no
         and item   = I_item ;

   L_dummy   VARCHAR2(1);

BEGIN

   if I_mrt_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_mrt_no,L_program,NULL);
      return FALSE;
   end if;

   open  C_CHECK_MRT_ITEM;
   fetch C_CHECK_MRT_ITEM into O_mrt_row;
   O_valid_mrt := C_CHECK_MRT_ITEM%FOUND;
   close C_CHECK_MRT_ITEM;

return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_MRT_ITEM;
----------------------------------------------------------------------------
FUNCTION VALIDATE_MRT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_valid_mrt      IN OUT  BOOLEAN,
                      I_mrt_no         IN      V_MRT_ITEM.MRT_NO%TYPE)
RETURN BOOLEAN
 IS

   L_program      VARCHAR2(50)       := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_MRT';
   L_dummy        VARCHAR2(1);

   cursor C_CHECK_MRT is
      select 'Y'
        from mrt m,
             v_mrt_item_loc vm,
             v_wh vwh,
             v_mrt_item vmi
       where m.mrt_no = I_mrt_no
         and m.mrt_status in ('A','C','R')
         and m.mrt_no  = vm.mrt_no
         and vwh.wh = m.wh
         and vmi.item = vm.item
         and vmi.mrt_no = vm.mrt_no
         and rownum = 1;

BEGIN

   if I_mrt_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_mrt_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_MRT',
                    'MRT, V_MRT_ITEM_LOC, V_WH, V_MRT_ITEM',
                    'mrt_no: '|| I_mrt_no);
   open  C_CHECK_MRT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_MRT',
                    'MRT, V_MRT_ITEM_LOC, V_WH, V_MRT_ITEM',
                    'mrt_no: '|| I_mrt_no);
   fetch C_CHECK_MRT into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_MRT',
                    'MRT, V_MRT_ITEM_LOC, V_WH, V_MRT_ITEM',
                    'mrt_no: '|| I_mrt_no);
   close C_CHECK_MRT;

   if L_dummy is NULL then
      O_valid_mrt := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_MRT',
                                             I_mrt_no,
                                             NULL,
                                             NULL);
   else
      O_valid_mrt := TRUE;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_MRT;
----------------------------------------------------------------------------
FUNCTION VALIDATE_ENTRY_NO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid           IN OUT   BOOLEAN,
                            O_ce_head         IN OUT   CE_HEAD%ROWTYPE,
                            I_entry_no        IN       CE_HEAD.ENTRY_NO%TYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(50)  := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_ENTRY_NO';
   L_hier                       VARCHAR2(128);
   L_filter_org_id              FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE;
   L_filter_org_level           FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE;
   L_filter_merch_id            FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE;
   L_filter_merch_level         FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE;
   L_filter_merch_id_class      FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE;
   L_filter_merch_id_subclass   FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE;
   L_found                      BOOLEAN;

   cursor   C_CEH
       is
   select   ceh.*
     from   ce_head   ceh
    where   ceh.entry_no = I_entry_no;

   cursor C_CHECK_CE_V is
      select *
        from v_ce_head
       where entry_no = I_entry_no;

   cursor C_CHECK_FILTER_ORG is
      select b.filter_org_id,
             b.filter_org_level
        from filter_group_org b,
             sec_user_group a,
             sec_user su
       where a.group_id = b.sec_group_id
         and a.user_seq = su.user_seq
         and (su.application_user_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO'))
          or (SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') is NULL and su.database_user_id = NVL(SYS_CONTEXT('USERENV','CLIENT_INFO'),SYS_CONTEXT('USERENV','SESSION_USER'))));

   cursor C_CHECK_FILTER_MERCH is
      select b.filter_merch_id,
             b.filter_merch_level,
             b.filter_merch_id_class,
             b.filter_merch_id_subclass
        from filter_group_merch b,
             sec_user_group a,
             sec_user su
       where a.group_id = b.sec_group_id
         and a.user_seq = su.user_seq
         and (su.application_user_id = NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'), SYS_CONTEXT('USERENV', 'CLIENT_INFO'))
          or (SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID') is NULL and su.database_user_id = NVL(SYS_CONTEXT('USERENV','CLIENT_INFO'),SYS_CONTEXT('USERENV','SESSION_USER'))));

BEGIN
   if I_entry_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_entry_no,L_program,NULL);
      return FALSE;
   end if;
   ---
   O_valid := TRUE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CEH',
                    'CE_HEAD',
                    'I_entry_no='||I_entry_no);

   open C_CEH;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CEH',
                    'CE_HEAD',
                    'I_entry_no='||I_entry_no);

   fetch C_CEH
    into O_ce_head;

   L_found := C_CEH%FOUND;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CEH',
                    'CE_HEAD',
                    'I_entry_no='||I_entry_no);

   close C_CEH;

   if not L_found then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ENTRY_NO',
                                            I_entry_no);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_CE_V',
                    'V_CE_HEAD',
                    I_entry_no);
   open C_CHECK_CE_V;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_CE_V',
                    'V_CE_HEAD',
                    I_entry_no);
   fetch C_CHECK_CE_V into O_ce_head;
   ---
   if C_CHECK_CE_V%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_CE_V',
                    'CE_HEAD',
                    I_entry_no);
      close C_CHECK_CE_V;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_FILTER_ORG',
                       'FILTER_GROUP_ORG',
                       NULL);
      open C_CHECK_FILTER_ORG;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_FILTER_ORG',
                       'FILTER_GROUP_ORG',
                       NULL);
      fetch C_CHECK_FILTER_ORG into L_filter_org_id,
                                    L_filter_org_level;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_FILTER_ORG',
                       'FILTER_GROUP_ORG',
                       NULL);
      close C_CHECK_FILTER_ORG;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_FILTER_MERCH',
                       'FILTER_GROUP_MERCH',
                        NULL);
      open C_CHECK_FILTER_MERCH;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_FILTER_MERCH',
                       'FILTER_GROUP_MERCH',
                       NULL);
      fetch C_CHECK_FILTER_MERCH into L_filter_merch_id,
                                      L_filter_merch_level,
                                      L_filter_merch_id_class,
                                      L_filter_merch_id_subclass;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_FILTER_MERCH',
                       'FILTER_GROUP_MERCH',
                        NULL);
      close C_CHECK_FILTER_MERCH;
      ---
      if NOT GET_NON_VISIBLE_HIER(O_error_message,
                                  L_hier,
                                  L_filter_org_id,
                                  L_filter_org_level,
                                  L_filter_merch_id,
                                  L_filter_merch_level,
                                  L_filter_merch_id_class,
                                  L_filter_merch_id_subclass) then
         return FALSE;
      end if;
      ---
      if L_hier is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY', L_hier, I_entry_no);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_ANY_CE_ID', I_entry_no);
      end if;
      ---
      O_valid := FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_CE_V',
                       'CE_HEAD',
                       I_entry_no);
      close C_CHECK_CE_V;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END VALIDATE_ENTRY_NO;
----------------------------------------------------------------------------
FUNCTION VALIDATE_COST_CHANGE(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid              IN OUT  BOOLEAN,
                              I_cost_change        IN      COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE)
RETURN BOOLEAN
 IS

   L_program     VARCHAR2(50)  := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_COST_CHANGE';
   L_dummy       VARCHAR2(1);
   L_code_desc   CODE_DETAIL.CODE_DESC%TYPE;

   cursor C_CHECK_COST_CHANGE_VALID is
      select 'X'
        from cost_susp_sup_detail d
       where d.cost_change = I_cost_change
         and exists (select'X'
                       from v_item_master v
                      where v.item = d.item)
      union all
      select 'X'
        from cost_susp_sup_detail_loc dl
       where dl.cost_change = I_cost_change
         and exists (select'X'
                       from v_item_master v
                      where v.item = dl.item);

   cursor C_CHECK_COST_CHANGE_TB is
      select 'X'
        from cost_susp_sup_head
       where cost_change = I_cost_change;

BEGIN

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'PETP',
                                 'COST',
                                 L_code_desc) = FALSE then
      return FALSE;
   end if;

   if I_cost_change is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_cost_change', L_program, NULL);
      return FALSE;
   end if;

   open  C_CHECK_COST_CHANGE_VALID;
   fetch C_CHECK_COST_CHANGE_VALID into L_dummy;
   close C_CHECK_COST_CHANGE_VALID;
   if L_dummy is NULL then
      open C_CHECK_COST_CHANGE_TB;
      fetch C_CHECK_COST_CHANGE_TB into L_dummy;
      if L_dummy is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COST_CHANGE', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', L_code_desc, I_cost_change, NULL);
      end if;
      close C_CHECK_COST_CHANGE_TB;
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_COST_CHANGE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_TRANPO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid          IN OUT  BOOLEAN,
                         I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)       := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_TRANPO';
   L_dummy        VARCHAR2(1);

   cursor C_CHECK_ORDER is
      select 'x'
        from ordhead
       where order_no = I_order_no;

   cursor C_CHECK_TRANPO is
      SELECT 'x'
        FROM code_detail    cdd,
             transportation trnp,
             v_ordhead      vohe,
             ordsku         osk
       WHERE cdd.code      = vohe.status
         AND cdd.code_type = 'ORST'
         AND vohe.order_no = I_order_no
         AND trnp.order_no = vohe.order_no
         AND vohe.status   = 'A'
         AND vohe.order_no = osk.order_no
         AND osk.item IS NOT NULL
         AND osk.order_no > 0;
BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_order_no,L_program,NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   open C_CHECK_ORDER;
   fetch C_CHECK_ORDER into L_dummy;
   ---
   if C_CHECK_ORDER%NOTFOUND then
      close C_CHECK_ORDER;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO');
      O_valid := FALSE;
   else
      close C_CHECK_ORDER;
      open  C_CHECK_TRANPO;
      fetch C_CHECK_TRANPO into L_dummy;
      ---
      if C_CHECK_TRANPO%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_TRAN_ORD', I_order_no);
         O_valid := FALSE;
      end if;
      ---
      close C_CHECK_TRANPO;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_TRANPO;
--------------------------------------------------------------------------------
-- VALIDATE_SEC_WH( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SEC_WH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_wh            IN OUT V_SEC_WH%ROWTYPE,
                         I_wh            IN     V_WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SEC_WH';

   CURSOR C_CHECK_WH_TB IS
      SELECT wtl.wh_name, 
             w.stockholding_ind
        FROM wh w,
             v_wh_tl wtl
       WHERE w.wh = I_wh
         and w.wh = wtl.wh
         AND w.finisher_ind = 'N';

   CURSOR C_CHECK_WH_V IS
      SELECT *
        FROM v_sec_wh
       WHERE wh = I_wh;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_WH_V',
                    'V_SEC_WH',
                     NULL);
   open C_CHECK_WH_V;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_WH_V',
                    'V_SEC_WH',
                     NULL);
   fetch C_CHECK_WH_V into O_wh;
   if C_CHECK_WH_V%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_WH_V',
                       'V_SEC_WH',
                        NULL);
      close C_CHECK_WH_V;
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_WH_TB',
                       'WH',
                        NULL);
      open C_CHECK_WH_TB;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_WH_TB',
                       'WH',
                        NULL);
      fetch C_CHECK_WH_TB into O_wh.wh_name, O_wh.stockholding_ind;
      if C_CHECK_WH_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_WH', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER_LOC', I_wh, NULL);
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_WH_TB',
                       'WH',
                        NULL);
      close C_CHECK_WH_TB;
      O_valid := FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_WH_V',
                       'V_SEC_WH',
                        NULL);
      close C_CHECK_WH_V;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SEC_WH;
--------------------------------------------------------------------------------
-- VALIDATE_CARTON( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CARTON(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         I_carton_id     IN     SHIPSKU.CARTON%TYPE,
                         I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_CARTON';
   L_carton_id SHIPSKU.CARTON%TYPE;

   cursor C_CHECK_SHIPSKU_TB is
      select sk.carton
        from shipsku sk,
             shipment sh
       where sk.carton = I_carton_id
         and sk.shipment = sh.shipment
         and sk.status_code != 'R'
         and sh.status_code not in ('C','R')
         and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
         and rownum = 1;

   cursor C_CHECK_SHIPSKU_V is
      select vsk.carton
        from shipsku vsk,
             shipment s
       where vsk.carton = I_carton_id
         and vsk.shipment = s.shipment
         and vsk.status_code != 'R'
         and s.status_code not in ('C','R')
         and s.to_loc_type = NVL(I_loc_type, s.to_loc_type)
         and exists (select 1
                       from v_item_master i
                      where i.item = vsk.item
                        and rownum = 1)
         and s.to_loc in (select store
                            from v_store
                           where store = s.to_loc
                           union all
                          select vw.physical_wh
                            from v_wh vw
                           where vw.physical_wh = s.to_loc
                           union all
                          select vif.physical_wh
                            from v_internal_finisher vif
                           where vif.physical_wh = s.to_loc
                           union all
                          select vef.finisher_id
                            from v_external_finisher vef
                           where vef.finisher_id = s.to_loc)
         and (s.from_loc in (select store
                               from v_store
                              where store = s.from_loc
                              union all
                             select vw.physical_wh
                               from v_wh vw
                              where vw.physical_wh = s.from_loc
                              union all
                             select vif.physical_wh
                               from v_internal_finisher vif
                              where physical_wh = s.from_loc
                              union all
                             select vef.finisher_id
                               from v_external_finisher vef
                              where finisher_id = s.from_loc)
              or s.from_loc is null)
         and rownum = 1;

BEGIN
   O_valid := TRUE;

   if I_carton_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_carton_id',L_program,NULL);
      return FALSE;
   end if;

   open C_CHECK_SHIPSKU_V;
   fetch C_CHECK_SHIPSKU_V into L_carton_id;

   if C_CHECK_SHIPSKU_V%NOTFOUND then
      close C_CHECK_SHIPSKU_V;
      open C_CHECK_SHIPSKU_TB;
      fetch C_CHECK_SHIPSKU_TB into L_carton_id;

      if C_CHECK_SHIPSKU_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_CARTON',
                                               NULL,
                                               NULL,
                                               NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_CARTON',
                                               I_carton_id,
                                               NULL,
                                               NULL);
      end if;

      O_valid := FALSE;
      close C_CHECK_SHIPSKU_TB;
   else
      close C_CHECK_SHIPSKU_V;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_CARTON;
--------------------------------------------------------------------------------
-- VALIDATE_ALLOC_NO( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ALLOC_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_alloc_header  IN OUT ALLOC_HEADER%ROWTYPE,
                           I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_ALLOC_NO';

   cursor C_CHECK_ALLOC_TB is
      select ah.*
        from alloc_header ah,
             alloc_detail ad
       where ah.alloc_no = I_alloc_no
         and ah.alloc_no = ad.alloc_no;

   cursor C_CHECK_ALLOC_V is
      select ah.*
        from alloc_header ah,
             v_alloc_detail vad,
             v_item_master vitm,
             v_wh vwh
       where ah.alloc_no = I_alloc_no
         and ah.alloc_no = vad.alloc_no
         and ah.item = vitm.item
         and ah.wh = vwh.wh;

BEGIN
   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_alloc_no',L_program,NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   open C_CHECK_ALLOC_V;
   fetch C_CHECK_ALLOC_V into O_alloc_header;

   if C_CHECK_ALLOC_V%NOTFOUND then
      close C_CHECK_ALLOC_V;
      open C_CHECK_ALLOC_TB;
      fetch C_CHECK_ALLOC_TB into O_alloc_header;

      if C_CHECK_ALLOC_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ALLOC_NUM', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ALLOC', I_alloc_no, NULL, NULL);
      end if;

      O_valid := FALSE;
      close C_CHECK_ALLOC_TB;
   else
      close C_CHECK_ALLOC_V;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ALLOC_NO;
--------------------------------------------------------------------------------
-- VALIDATE_RECV_TSF( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_RECV_TSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                           I_ctn_ind       IN     VARCHAR2,
                           I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_RECV_TSF';
   L_tsf_no    TSFHEAD.TSF_NO%TYPE;

   cursor C_CHECK_TSF_TB is
      select th.tsf_no
        from tsfhead th
       where th.tsf_no = I_tsf_no
         and th.status = 'S'
         and exists (select 'x'
                       from shipsku sk,
                            shipment sh
                      where sk.distro_no = th.tsf_no
                        and sk.shipment = sh.shipment
                        and sk.distro_type = 'T'
                        and sk.status_code != 'R'
                        and sh.status_code NOT IN ('C','R')
                        and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
                        and rownum = 1)
         and rownum = 1;

   cursor C_CHECK_TSF_CTN_TB is
      select th.tsf_no
        from tsfhead th
       where th.tsf_no = I_tsf_no
         and th.status = 'S'
         and exists (select 'x'
                       from shipsku sk,
                            shipment sh
                      where sk.distro_no = th.tsf_no
                        and sk.shipment = sh.shipment
                        and sk.distro_type = 'T'
                        and sk.status_code != 'R'
                        and sk.carton is NOT NULL
                        and sh.status_code NOT IN ('C','R')
                        and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
                        and rownum = 1)
         and rownum = 1;

   cursor C_CHECK_SHIP_TSF_V is
      select th.tsf_no
        from tsfhead th,
             v_shipsku vsk,
             shipment s
       where th.status     = 'S'
         and vsk.distro_no = I_tsf_no
         and vsk.distro_no = th.tsf_no
         and vsk.shipment = s.shipment
         and vsk.distro_type = 'T'
         and vsk.status_code != 'R'
         and s.status_code NOT IN ('C','R')
         and s.to_loc_type = NVL(I_loc_type, s.to_loc_type)
         and s.to_loc in (select store
                            from v_store
                           where store = s.to_loc
                           union all
                          select vw.physical_wh
                            from v_wh vw
                           where vw.physical_wh = s.to_loc
                           union all
                          select vif.physical_wh
                            from v_internal_finisher vif
                           where vif.physical_wh = s.to_loc
                           union all
                          select vef.finisher_id
                            from v_external_finisher vef
                           where vef.finisher_id = s.to_loc)
         and (s.from_loc in (select store
                               from v_store
                              where store = s.from_loc
                              union all
                             select vw.physical_wh
                               from v_wh vw
                              where vw.physical_wh = s.from_loc
                              union all
                             select vif.physical_wh
                               from v_internal_finisher vif
                              where physical_wh = s.from_loc
                              union all
                             select vef.finisher_id
                               from v_external_finisher vef
                              where finisher_id = s.from_loc)
              or s.from_loc is NULL)
         and rownum = 1;

   cursor C_CHECK_SHIP_TSF_CTN_V is
      select th.tsf_no
        from tsfhead th,
             v_shipsku vsk,
             shipment s
       where th.status     = 'S'
         and vsk.distro_no = I_tsf_no
         and vsk.distro_no = th.tsf_no
         and vsk.shipment = s.shipment
         and vsk.distro_type = 'T'
         and vsk.status_code != 'R'
         and vsk.carton is NOT NULL
         and s.status_code NOT IN ('C','R')
         and s.to_loc_type = NVL(I_loc_type, s.to_loc_type)
         and s.to_loc in (select store
                            from v_store
                           where store = s.to_loc
                           union all
                          select vw.physical_wh
                            from v_wh vw
                           where vw.physical_wh = s.to_loc
                           union all
                          select vif.physical_wh
                            from v_internal_finisher vif
                           where vif.physical_wh = s.to_loc
                           union all
                          select vef.finisher_id
                            from v_external_finisher vef
                           where vef.finisher_id = s.to_loc)
         and (s.from_loc in (select store
                               from v_store
                              where store = s.from_loc
                              union all
                             select vw.physical_wh
                               from v_wh vw
                              where vw.physical_wh = s.from_loc
                              union all
                             select vif.physical_wh
                               from v_internal_finisher vif
                              where physical_wh = s.from_loc
                              union all
                             select vef.finisher_id
                               from v_external_finisher vef
                              where finisher_id = s.from_loc)
              or s.from_loc is NULL)
         and rownum = 1;

BEGIN
   O_valid := TRUE;

   if I_tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_ctn_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ctn_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_ctn_ind = 'N' then
      open C_CHECK_SHIP_TSF_V;
      fetch C_CHECK_SHIP_TSF_V into L_tsf_no;

      if C_CHECK_SHIP_TSF_V%NOTFOUND then
         close C_CHECK_SHIP_TSF_V;
         open C_CHECK_TSF_TB;
         fetch C_CHECK_TSF_TB into L_tsf_no;

         if C_CHECK_TSF_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('TSF_NOT_EXIST_SHIP');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_TSF',
                                                  I_tsf_no,
                                                  NULL,
                                                  NULL);
         end if;

         close C_CHECK_TSF_TB;
         O_valid := FALSE;
      else
         close C_CHECK_SHIP_TSF_V;
      end if;
   elsif I_ctn_ind = 'Y' then
      open C_CHECK_SHIP_TSF_CTN_V;
      fetch C_CHECK_SHIP_TSF_CTN_V into L_tsf_no;

      if C_CHECK_SHIP_TSF_CTN_V%NOTFOUND then
         close C_CHECK_SHIP_TSF_CTN_V;
         open C_CHECK_TSF_CTN_TB;
         fetch C_CHECK_TSF_CTN_TB into L_tsf_no;

         if C_CHECK_TSF_CTN_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('TSF_NOT_EXIST_SHIP');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_TSF',
                                                  I_tsf_no,
                                                  NULL,
                                                  NULL);
         end if;

         close C_CHECK_TSF_CTN_TB;
         O_valid := FALSE;
      else
         close C_CHECK_SHIP_TSF_CTN_V;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_RECV_TSF;
--------------------------------------------------------------------------------
-- VALIDATE_RECV_ALLOC( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_RECV_ALLOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT BOOLEAN,
                             I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_ctn_ind       IN     VARCHAR2,
                             I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_RECV_ALLOC';
   L_alloc_no  ALLOC_DETAIL.ALLOC_NO%TYPE;

   cursor C_CHECK_ALLOC_TB is
      select ah.alloc_no
        from alloc_header ah
       where ah.alloc_no = I_alloc_no
         and ah.status = 'A'
         and exists (select 'x'
                       from shipsku sk,
                            shipment sh
                      where sk.distro_no = ah.alloc_no
                        and sk.distro_type = 'A'
                        and sh.status_code NOT IN ('C','R')
                        and sk.status_code != 'R'
                        and sh.shipment = sk.shipment
                        and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
                        and rownum = 1)
         and rownum = 1;

   cursor C_CHECK_ALLOC_CTN_TB is
      select ah.alloc_no
        from alloc_header ah
       where ah.alloc_no = I_alloc_no
         and ah.status = 'A'
         and exists (select 'x'
                       from shipsku sk,
                            shipment sh
                      where sk.distro_no = ah.alloc_no
                        and sk.distro_type = 'A'
                        and sh.status_code NOT IN ('C','R')
                        and sk.status_code != 'R'
                        and sk.carton is NOT NULL
                        and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
                        and rownum = 1)
         and rownum = 1;

   cursor C_CHECK_ALLOC_V is
      select ah.alloc_no
        from alloc_header ah,
             v_shipsku vsk,
             shipment s
       where vsk.distro_no = I_alloc_no
         and vsk.distro_type = 'A'
         and vsk.distro_no = ah.alloc_no
         and ah.status = 'A'
         and vsk.shipment = s.shipment
         and vsk.status_code != 'R'
         and s.status_code not in ('C','R')
         and s.to_loc_type = NVL(I_loc_type, s.to_loc_type)
         and s.to_loc in (select store
                            from v_store
                           where store = s.to_loc
                           union all
                          select vw.physical_wh
                            from v_wh vw
                           where vw.physical_wh = s.to_loc
                           union all
                          select vif.physical_wh
                            from v_internal_finisher vif
                           where vif.physical_wh = s.to_loc
                           union all
                          select vef.finisher_id
                            from v_external_finisher vef
                           where vef.finisher_id = s.to_loc)
         and (s.from_loc in (select store
                               from v_store
                              where store = s.from_loc
                              union all
                             select vw.physical_wh
                               from v_wh vw
                              where vw.physical_wh = s.from_loc
                              union all
                             select vif.physical_wh
                               from v_internal_finisher vif
                              where physical_wh = s.from_loc
                              union all
                             select vef.finisher_id
                               from v_external_finisher vef
                              where finisher_id = s.from_loc)
              or s.from_loc is NULL)
         and rownum = 1;

   cursor C_CHECK_ALLOC_CTN_V is
      select ah.alloc_no
        from alloc_header ah,
             v_shipsku vsk,
             shipment s
       where vsk.distro_no = I_alloc_no
         and vsk.distro_type = 'A'
         and vsk.distro_no = ah.alloc_no
         and ah.status = 'A'
         and vsk.shipment = s.shipment
         and vsk.status_code != 'R'
         and vsk.carton is NOT NULL
         and s.status_code not in ('C','R')
         and s.to_loc_type = NVL(I_loc_type, s.to_loc_type)
         and s.to_loc in (select store
                            from v_store
                           where store = s.to_loc
                           union all
                          select vw.physical_wh
                            from v_wh vw
                           where vw.physical_wh = s.to_loc
                           union all
                          select vif.physical_wh
                            from v_internal_finisher vif
                           where vif.physical_wh = s.to_loc
                           union all
                          select vef.finisher_id
                            from v_external_finisher vef
                           where vef.finisher_id = s.to_loc)
         and (s.from_loc in (select store
                               from v_store
                              where store = s.from_loc
                              union all
                             select vw.physical_wh
                               from v_wh vw
                              where vw.physical_wh = s.from_loc
                              union all
                             select vif.physical_wh
                               from v_internal_finisher vif
                              where physical_wh = s.from_loc
                              union all
                             select vef.finisher_id
                               from v_external_finisher vef
                              where finisher_id = s.from_loc)
              or s.from_loc is NULL)
         and rownum = 1;

BEGIN
   O_valid := TRUE;

   if I_alloc_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_alloc_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_ctn_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ctn_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_ctn_ind = 'N' then
      open C_CHECK_ALLOC_V;
      fetch C_CHECK_ALLOC_V into L_alloc_no;

      if C_CHECK_ALLOC_V%NOTFOUND then
         close C_CHECK_ALLOC_V;
         open C_CHECK_ALLOC_TB;
         fetch C_CHECK_ALLOC_TB into L_alloc_no;
         if C_CHECK_ALLOC_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ALLOC_NOT_EXIST_SHIP');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ALLOC',
                                                  I_alloc_no,
                                                  NULL,
                                                  NULL);
         end if;
         close C_CHECK_ALLOC_TB;
         O_valid := FALSE;
      else
         close C_CHECK_ALLOC_V;
      end if;

   elsif I_ctn_ind = 'Y' then
      open C_CHECK_ALLOC_CTN_V;
      fetch C_CHECK_ALLOC_CTN_V into L_alloc_no;

      if C_CHECK_ALLOC_CTN_V%NOTFOUND then
         close C_CHECK_ALLOC_CTN_V;
         open C_CHECK_ALLOC_CTN_TB;
         fetch C_CHECK_ALLOC_CTN_TB into L_alloc_no;
         if C_CHECK_ALLOC_CTN_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ALLOC_NOT_EXIST_SHIP');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ALLOC',
                                                  I_alloc_no,
                                                  NULL,
                                                  NULL);
         end if;
         close C_CHECK_ALLOC_CTN_TB;
         O_valid := FALSE;
      else
         close C_CHECK_ALLOC_CTN_V;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_RECV_ALLOC;
--------------------------------------------------------------------------------
-- VALIDATE_BOL_NO( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_BOL_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_bol_shipment  IN OUT BOL_SHIPMENT%ROWTYPE,
                         I_bol_no        IN     SHIPMENT.BOL_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_BOL_NO';

   cursor C_CHECK_BOL_TB is
      select *
        from bol_shipment
       where bol_no = I_bol_no;

   cursor C_CHECK_BOL_V is
      select vbs.*
        from v_bol_shipment vbs
       where vbs.bol_no = I_bol_no;

BEGIN
   if I_bol_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_bol_no',L_program,NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   open C_CHECK_BOL_V;
   fetch C_CHECK_BOL_V into O_bol_shipment;

   if C_CHECK_BOL_V%NOTFOUND then
      close C_CHECK_BOL_V;
      open C_CHECK_BOL_TB;
      fetch C_CHECK_BOL_TB into O_bol_shipment;

      if C_CHECK_BOL_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_BOL', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_BOL', I_bol_no, NULL, NULL);
      end if;

      O_valid := FALSE;
      close C_CHECK_BOL_TB;
   else
      close C_CHECK_BOL_V;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_BOL_NO;
--------------------------------------------------------------------------------
-- VALIDATE_RECV_ORDER( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_RECV_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT BOOLEAN,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                             I_ctn_ind       IN     VARCHAR2,
                             I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_RECV_ORDER';
   L_order_no  ORDHEAD.ORDER_NO%TYPE;

   cursor C_CHECK_ORDER_TB is
      select oh.order_no
        from ordhead oh,
             shipment sh,
             shipsku sk
       where oh.order_no = I_order_no
         and oh.order_no = sh.order_no
         and sk.shipment = sh.shipment
         and oh.status in ('A','C')
         and sh.status_code not in ('C','R')
         and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
         and sk.status_code != 'R'
         and rownum = 1;

   cursor C_CHECK_ORDER_CTN_TB is
      select oh.order_no
        from ordhead oh,
             shipment sh,
             shipsku sk
       where oh.order_no = I_order_no
         and oh.order_no = sh.order_no
         and sh.shipment = sk.shipment
         and oh.status in ('A','C')
         and sh.status_code not in ('C','R')
         and sh.to_loc_type = NVL(I_loc_type, sh.to_loc_type)
         and sk.status_code != 'R'
         and sk.carton is NOT NULL
         and rownum = 1;

   cursor C_CHECK_ORDER_V is
      select oh.order_no
        from v_ordhead oh,
             v_shipment vs,
             shipsku sk
       where vs.order_no = I_order_no
         and vs.order_no = oh.order_no
         and vs.shipment = sk.shipment
         and oh.status in ('A','C')
         and vs.status_code not in ('C','R')
         and vs.to_loc_type = NVL(I_loc_type, vs.to_loc_type)
         and sk.status_code != 'R'
         and rownum = 1;

   cursor C_CHECK_ORDER_CTN_V is
      select oh.order_no
        from v_ordhead oh,
             v_shipment vs,
             shipsku sk
       where vs.order_no = I_order_no
         and vs.order_no = oh.order_no
         and vs.shipment = sk.shipment
         and oh.status in ('A','C')
         and vs.status_code not in ('C','R')
         and vs.to_loc_type = NVL(I_loc_type, vs.to_loc_type)
         and sk.status_code != 'R'
         and sk.carton is NOT NULL
         and rownum = 1;

BEGIN
   O_valid := TRUE;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_ctn_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_ctn_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_ctn_ind = 'N' then
      open C_CHECK_ORDER_V;
      fetch C_CHECK_ORDER_V into L_order_no;

      if C_CHECK_ORDER_V%NOTFOUND then
         close C_CHECK_ORDER_V;
         open C_CHECK_ORDER_TB;
         fetch C_CHECK_ORDER_TB into L_order_no;

         if C_CHECK_ORDER_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ORD_NOT_EXIST_SHIP');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ORDER',
                                                  I_order_no,
                                                  NULL,
                                                  NULL);
         end if;

         close C_CHECK_ORDER_TB;
         O_valid := FALSE;
      else
         close C_CHECK_ORDER_V;
      end if;

   elsif I_ctn_ind = 'Y' then
      open C_CHECK_ORDER_CTN_V;
      fetch C_CHECK_ORDER_CTN_V into L_order_no;

      if C_CHECK_ORDER_CTN_V%NOTFOUND then
         close C_CHECK_ORDER_CTN_V;
         open C_CHECK_ORDER_CTN_TB;
         fetch C_CHECK_ORDER_CTN_TB into L_order_no;

         if C_CHECK_ORDER_CTN_TB%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('ORD_NOT_EXIST_SHIP');
         else
            O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ORDER',
                                                  I_order_no,
                                                  NULL,
                                                  NULL);
         end if;

         close C_CHECK_ORDER_CTN_TB;
         O_valid := FALSE;
      else
         close C_CHECK_ORDER_CTN_V;
      end if;

   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_RECV_ORDER;
--------------------------------------------------------------------------------
-- VALIDATE_SEC_ORDER( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SEC_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT  BOOLEAN,
                            O_ordhead        IN OUT  V_SEC_ORDHEAD%ROWTYPE,
                            I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)       := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SEC_ORDER';

   cursor C_CHECK_ORDER_TB is
      select *
        from ordhead
       where order_no = I_order_no;

   cursor C_CHECK_ORDER_V is
      select *
        from v_sec_ordhead
       where order_no = I_order_no;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_order_no,L_program,NULL);
      return FALSE;
   end if;

   O_valid := TRUE;

   open  C_CHECK_ORDER_V;
   fetch C_CHECK_ORDER_V into O_ordhead;
   ---
   if C_CHECK_ORDER_V%NOTFOUND then
      close C_CHECK_ORDER_V;
      ---
      open  C_CHECK_ORDER_TB;
      fetch C_CHECK_ORDER_TB into O_ordhead;
      ---
      if C_CHECK_ORDER_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_SEARCH', I_order_no);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ORDER', I_order_no, NULL, NULL);
      end if;
      ---
      O_valid := FALSE;
      close C_CHECK_ORDER_TB;
   else
      close C_CHECK_ORDER_V;
   end if;

    return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;

END VALIDATE_SEC_ORDER;
--------------------------------------------------------------------------------
-- COMPANY_STORE( )
--------------------------------------------------------------------------------
FUNCTION COMPANY_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_company_store   IN OUT   BOOLEAN,
                       O_store_name      IN OUT   STORE.STORE_NAME%TYPE,
                       I_store           IN       STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.COMPANY_STORE';

   L_valid   BOOLEAN;
   L_store   V_STORE%ROWTYPE;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(O_error_message,
                                             L_valid,
                                             L_store,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_store) = FALSE then
      return FALSE;
   end if;
   ---
   if L_valid = TRUE then
      if L_store.store_type = 'C' then
         O_company_store := TRUE;
      else
         O_company_store := FALSE;
      end if;
      ---
      O_store_name := L_store.store_name;
      ---
   else -- L_valid is FALSE
      return FALSE;
   end if; -- check L_valid

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END COMPANY_STORE;
--------------------------------------------------------------------------------
-- WS_FR_STORE( )
--------------------------------------------------------------------------------
FUNCTION WS_FR_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_ws_fr_store     IN OUT   BOOLEAN,
                     O_wf_store_name   IN OUT   STORE.STORE_NAME%TYPE,
                     I_wf_store        IN       STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.WS_FR_STORE';

   L_valid        BOOLEAN;
   L_store_name   STORE.STORE_NAME%TYPE;
   L_store        V_STORE%ROWTYPE;

BEGIN
   if I_wf_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wf_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(O_error_message,
                                             L_valid,
                                             L_store,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_wf_store) = FALSE then
      return FALSE;
   end if;
   --
   if L_valid = TRUE then
      if L_store.store_type in ('W', 'F') then
         O_ws_fr_store := TRUE;
      else
         O_ws_fr_store := FALSE;
      end if;
      ---
      O_wf_store_name := L_store.store_name;
      ---
   else -- L_valid is FALSE
      return FALSE;
   end if; -- check L_valid

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END WS_FR_STORE;
--------------------------------------------------------------------------------
-- LOC_LIST_HEAD_TYPE( )
--------------------------------------------------------------------------------
FUNCTION LOC_LIST_HEAD_TYPE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_loc_list_desc        IN OUT   LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                            I_loc_list             IN       LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)                       := 'FILTER_LOV_VALIDATE_SQL.LOC_LIST_HEAD_TYPE';
   L_loc_list_desc   LOC_LIST_HEAD.LOC_LIST_DESC%TYPE   := NULL;

   cursor C_GET_LOC_LIST_TYPE is
      select loc_list_desc
        from loc_list_head
       where loc_list = I_loc_list;

BEGIN
   if I_loc_list is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_list',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_LOC_LIST_TYPE',
                    'STORE',
                    NULL);
   open C_GET_LOC_LIST_TYPE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_LOC_LIST_TYPE',
                    'STORE',
                    NULL);
   fetch C_GET_LOC_LIST_TYPE into L_loc_list_desc;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_LOC_LIST_TYPE',
                    'STORE',
                    NULL);

   close C_GET_LOC_LIST_TYPE;

   if (L_loc_list_desc is NULL) then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_LIST',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   else
      O_loc_list_desc      := L_loc_list_desc;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END LOC_LIST_HEAD_TYPE;
--------------------------------------------------------------------------------
-- STOCKHOLDING_STORE( )
--------------------------------------------------------------------------------
FUNCTION STOCKHOLDING_STORE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_stockholding_store   IN OUT   BOOLEAN,
                            O_store_name           IN OUT   STORE.STORE_NAME%TYPE,
                            O_store_type           IN OUT   STORE.STORE_TYPE%TYPE,
                            I_store                IN       STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.STOCKHOLDING_STORE';

   L_valid        BOOLEAN;
   L_store_name   STORE.STORE_NAME%TYPE;
   L_store        V_STORE%ROWTYPE;

BEGIN
   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(O_error_message,
                                             L_valid,
                                             L_store,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_store) = FALSE then
      return FALSE;
   end if;
   ---
   if L_valid = TRUE then
      if L_store.stockholding_ind = 'Y' then
         O_stockholding_store := TRUE;
      else
         O_stockholding_store := FALSE;
      end if;
      ---
      O_store_name := L_store.store_name;
      O_store_type := L_store.store_type;
      ---
   else -- L_valid is FALSE
      return FALSE;
   end if; -- check L_valid

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END STOCKHOLDING_STORE;
--------------------------------------------------------------------------------
-- REGION_STORE_TYPES( )
--------------------------------------------------------------------------------
FUNCTION REGION_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_ws_fr_store     IN OUT   BOOLEAN,
                            O_company_store   IN OUT   BOOLEAN,
                            I_region          IN       REGION.REGION%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.REGION_STORE_TYPES';
   L_valid         BOOLEAN;
   L_region_name   REGION.REGION_NAME%TYPE;
   L_wf_fr         VARCHAR2(1);
   L_comp          VARCHAR2(1);

   cursor C_CHK_WS_FR_STORE is
      select 'x'
        from v_store
       where region = I_region
         and store_type in ('W','F')
         and rownum = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_store
       where region = I_region
         and store_type = 'C'
         and rownum = 1;

BEGIN
   if I_region is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_region',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_REGION(O_error_message,
                                              L_valid,
                                              L_region_name,
                                              NULL,
                                              NULL,
                                              I_region) = FALSE then
      return FALSE;
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   open C_CHK_WS_FR_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   fetch C_CHK_WS_FR_STORE into L_wf_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   close C_CHK_WS_FR_STORE;

   if L_wf_fr is NULL then
      O_ws_fr_store := FALSE;
   else
      O_ws_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END REGION_STORE_TYPES;
--------------------------------------------------------------------------------
-- DISTRICT_STORE_TYPES( )
--------------------------------------------------------------------------------
FUNCTION DISTRICT_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_ws_fr_store     IN OUT   BOOLEAN,
                              O_company_store   IN OUT   BOOLEAN,
                              I_district        IN       DISTRICT.DISTRICT%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50)                  := 'FILTER_LOV_VALIDATE_SQL.DISTRICT_STORE_TYPES';
   L_wf_fr           VARCHAR2(1)                   := NULL;
   L_comp            VARCHAR2(1)                   := NULL;
   L_valid           BOOLEAN;
   L_district_name   DISTRICT.DISTRICT_NAME%TYPE;

   cursor C_CHK_WS_FR_STORE is
      select 'x'
        from v_store
       where district = I_district
         and store_type in ('W','F')
         and rownum = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_store
       where district = I_district
         and store_type = 'C'
         and rownum = 1;

BEGIN
   if I_district is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_district',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_DISTRICT(O_error_message,
                                                L_valid,
                                                L_district_name,
                                                NULL,
                                                NULL,
                                                NULL,
                                                I_district) = FALSE then
      return FALSE;
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   open C_CHK_WS_FR_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   fetch C_CHK_WS_FR_STORE into L_wf_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   close C_CHK_WS_FR_STORE;

   if L_wf_fr is NULL then
      O_ws_fr_store := FALSE;
   else
      O_ws_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END DISTRICT_STORE_TYPES;

-----------------------------------------------------------------------------------------------
-- Name:    AREA_STORE_TYPES
-- Purpose: This function will check if the Area being passed contains
--          Wholesale/Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------

FUNCTION AREA_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_ws_fr_store     IN OUT   BOOLEAN,
                          O_company_store   IN OUT   BOOLEAN,
                          I_area          IN         V_AREA.AREA%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.AREA_STORE_TYPES';
   L_valid         BOOLEAN;
   L_area_name     AREA.AREA_NAME%TYPE;
   L_wf_fr         VARCHAR2(1);
   L_comp          VARCHAR2(1);

   cursor C_CHK_WS_FR_STORE is
      select 'x'
        from v_store
       where area = I_area
         and store_type in ('W','F')
         and rownum = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_store
       where area = I_area
         and store_type = 'C'
         and rownum = 1;

BEGIN
   if I_area is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_region',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_AREA(O_error_message,
                                            L_valid,
                                            L_area_name,
                                            NULL,
                                            NULL,
                                            I_area) = FALSE then
      return FALSE;
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   open C_CHK_WS_FR_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   fetch C_CHK_WS_FR_STORE into L_wf_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_WS_FR_STORE',
                    'STORE',
                    NULL);
   close C_CHK_WS_FR_STORE;

   if L_wf_fr is NULL then
      O_ws_fr_store := FALSE;
   else
      O_ws_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END AREA_STORE_TYPES;

-----------------------------------------------------------------------------------------------
-- Name:    LOC_LIST_STORE_TYPES
-- Purpose: This function will check if the Location list being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION LOC_LIST_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_fr_store        IN OUT   BOOLEAN,
                              O_company_store   IN OUT   BOOLEAN,
                              I_loc_list        IN       LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.LOC_LIST_STORE_TYPES';
   L_valid         BOOLEAN;
   L_loc_list_desc LOC_LIST_HEAD.LOC_LIST_DESC%TYPE;
   L_fr            VARCHAR2(1);
   L_comp          VARCHAR2(1);

   cursor C_CHK_FR_STORE is
      select 'x'
        from v_store vs,
             loc_list_detail lld
       where lld.loc_list  = I_loc_list
         and lld.location  = vs.store
         and vs.store_type = 'F'
         and ROWNUM = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_store vs,
             loc_list_detail lld
       where lld.loc_list  = I_loc_list
         and lld.location  = vs.store
         and vs.store_type = 'C'
         and ROWNUM = 1;

BEGIN
   if I_loc_list is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_list',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOC_LIST_HEAD(O_error_message, 
                                                     L_valid,         
                                                     L_loc_list_desc, 
                                                     I_loc_list) = false then
      return FALSE;
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',   
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_FR_STORE;                 

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_FR_STORE into L_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_FR_STORE;

   if L_fr is NULL then
      O_fr_store := FALSE;
   else
      O_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHK_FR_STORE%ISOPEN then
         close C_CHK_FR_STORE;
      end if;
      if C_CHK_COMP_STORE%ISOPEN then
         close C_CHK_COMP_STORE;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END LOC_LIST_STORE_TYPES;
-----------------------------------------------------------------------------------------------
-- Name:    LOC_LIST_FR_NONSKHD_STORE_TYPES
-- Purpose: This function will check if the Location list being passed contains
--      Non stockholding Franchise Stores.
---------------------------------------------------------------------------------------------
FUNCTION LOC_LIST_FR_NONSKHD_TYPES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_fr_nonskhd_store    IN OUT   BOOLEAN,
                                   I_loc_list            IN         LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN
IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.LOC_LIST_FR_NONSKHD_TYPES';
   L_valid         BOOLEAN;
   L_loc_list_desc LOC_LIST_HEAD.LOC_LIST_DESC%TYPE;
   L_fr_nonskhd    VARCHAR2(1)    :=  'y';

   cursor C_CHK_FR_NON_SKHD_STORE is
      select 'x'
        from v_store vs,
             loc_list_detail lld
       where lld.loc_list  = I_loc_list
         and lld.location  = vs.store
         and vs.store_type = 'F'
         and vs.stockholding_ind = 'N'
         and ROWNUM = 1;

BEGIN
   if I_loc_list is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                        'I_loc_list',
                         L_program,
                         NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOC_LIST_HEAD(O_error_message,
                                                     L_valid,
                                                     L_loc_list_desc,
                                                     I_loc_list) = false then
      return FALSE;
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if; 

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_FR_NON_SKHD_STORE',
                    'V_STORE',
                     NULL);
   open C_CHK_FR_NON_SKHD_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_FR_NON_SKHD_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_FR_NON_SKHD_STORE into L_fr_nonskhd;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_FR_NON_SKHD_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_FR_NON_SKHD_STORE;

   if L_fr_nonskhd = 'x' then
      O_fr_nonskhd_store := TRUE;
   else
      O_fr_nonskhd_store := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHK_FR_NON_SKHD_STORE%ISOPEN then
         close C_CHK_FR_NON_SKHD_STORE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;
END LOC_LIST_FR_NONSKHD_TYPES;
-----------------------------------------------------------------------------------------------
-- Name:    TSF_ZONE_STORE_TYPES
-- Purpose: This function will check if the Transfer Zone being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------

FUNCTION TSF_ZONE_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_fr_store        IN OUT   BOOLEAN,
                              O_company_store   IN OUT   BOOLEAN,
                              I_tsf_zone        IN       TSFZONE.TRANSFER_ZONE%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.TSF_ZONE_STORE_TYPES';
   L_valid         BOOLEAN;
   L_tsf_zone_name TSFZONE.DESCRIPTION%TYPE;
   L_fr            VARCHAR2(1);
   L_comp          VARCHAR2(1);

   cursor C_CHK_FR_STORE is
      select 'x'
        from v_store
       where transfer_zone = I_tsf_zone
         and store_type    ='F'
         and rownum = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_store
       where transfer_zone = I_tsf_zone
         and store_type = 'C'
         and rownum = 1;

BEGIN
   if I_tsf_zone is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_tsf_zone',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_TSF_ZONE(O_error_message,
                                                L_valid,
                                                L_tsf_zone_name,
                                                I_tsf_zone) = FALSE then
      return FALSE;
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_FR_STORE',
                    'STORE',
                     NULL);
   open C_CHK_FR_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_FR_STORE',
                    'STORE',
                    NULL);
   fetch C_CHK_FR_STORE into L_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_FR_STORE',
                    'STORE',
                    NULL);
   close C_CHK_FR_STORE;

   if L_fr is NULL then
      O_fr_store := FALSE;
   else
      O_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHK_FR_STORE%ISOPEN then
         close C_CHK_FR_STORE;
      end if;
      if C_CHK_COMP_STORE%ISOPEN then
         close C_CHK_COMP_STORE;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END TSF_ZONE_STORE_TYPES;
-----------------------------------------------------------------------------------------------
-- Name:    STORE_CLASS_STORE_TYPES
-- Purpose: This function will check if the Store Class being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION STORE_CLASS_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_fr_store        IN OUT   BOOLEAN,
                                 O_company_store   IN OUT   BOOLEAN,
                                 I_store_class     IN       STORE.STORE_CLASS%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.STORE_CLASS_STORE_TYPES';
   L_valid         BOOLEAN;
   L_value_desc    CODE_DETAIL.CODE_DESC%TYPE;
   L_fr            VARCHAR2(1);
   L_comp          VARCHAR2(1);

   cursor C_CHK_FR_STORE is
      select 'x'
        from v_store vs
       where vs.store_class  = I_store_class         
         and vs.store_type = 'F'
         and ROWNUM = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_store vs
       where vs.store_class  = I_store_class         
         and vs.store_type = 'C'
         and ROWNUM = 1;

BEGIN
   if I_store_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_class',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE_CLASS(O_error_message,
                                                   L_valid,
                                                   L_value_desc,
                                                   I_store_class) = FALSE then
      return FALSE;                                             
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',   
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_FR_STORE;                 

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_FR_STORE into L_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_FR_STORE;

   if L_fr is NULL then
      O_fr_store := FALSE;
   else
      O_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHK_FR_STORE%ISOPEN then
         close C_CHK_FR_STORE;
      end if;
      if C_CHK_COMP_STORE%ISOPEN then
         close C_CHK_COMP_STORE;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END STORE_CLASS_STORE_TYPES;

-----------------------------------------------------------------------------------------------
-- Name:    LOC_TRAITS_STORE_TYPES
-- Purpose: This function will check if the Location trait being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION LOC_TRAITS_STORE_TYPES(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_fr_store          IN OUT   BOOLEAN,
                                O_company_store     IN OUT   BOOLEAN,
                                I_loc_trait         IN       LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.LOC_TRAITS_STORE_TYPES';
   L_valid         BOOLEAN;
   L_value_desc    V_LOC_TRAITS.DESCRIPTION%TYPE;
   L_fr            VARCHAR2(1);
   L_comp          VARCHAR2(1);

   cursor C_CHK_FR_STORE is
      select 'x'
        from v_loc_traits_matrix vl ,
             v_store vs
       where vl.loc_trait  = I_loc_trait
         and vl.store      = vs.store
         and vs.store_type = 'F'
         and ROWNUM = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_loc_traits_matrix vl ,
             v_store vs
       where vl.loc_trait  = I_loc_trait
         and vl.store      = vs.store
         and vs.store_type = 'C'
         and ROWNUM = 1;

BEGIN
   if I_loc_trait is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_trait',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOC_TRAITS(O_error_message,
                                                  L_valid,
                                                  L_value_desc,
                                                  I_loc_trait) = FALSE then
      return FALSE;
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',   
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_FR_STORE;                 

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_FR_STORE into L_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_FR_STORE;

   if L_fr is NULL then
      O_fr_store := FALSE;
   else
      O_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHK_FR_STORE%ISOPEN then
         close C_CHK_FR_STORE;
      end if;
      if C_CHK_COMP_STORE%ISOPEN then
         close C_CHK_COMP_STORE;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END LOC_TRAITS_STORE_TYPES;

-----------------------------------------------------------------------------------------------
-- Name:    DEFAULT_WH_STORE_TYPES
-- Purpose: This function will check if the Default wh being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION DEFAULT_WH_STORE_TYPES(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_fr_store          IN OUT   BOOLEAN,
                                O_company_store     IN OUT   BOOLEAN,
                                I_default_wh        IN       STORE.DEFAULT_WH%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.DEFAULT_WH_STORE_TYPES';
   L_valid         BOOLEAN;
   L_wh            V_WH%ROWTYPE;
   L_fr            VARCHAR2(1);
   L_comp          VARCHAR2(1);

   cursor C_CHK_FR_STORE is
      select 'x'
        from v_store vs
       where vs.default_wh = I_default_wh         
         and vs.store_type = 'F'
         and ROWNUM = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_store vs
       where vs.default_wh = I_default_wh         
         and vs.store_type = 'C'
         and ROWNUM = 1;

BEGIN
   if I_default_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_default_wh',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_WH(O_error_message,
                                          L_valid,
                                          L_wh,
                                          I_default_wh) = FALSE then
      return FALSE;
   end if;

   if L_valid = FALSE then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',   
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_FR_STORE;                 

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_FR_STORE into L_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_FR_STORE;

   if L_fr is NULL then
      O_fr_store := FALSE;
   else
      O_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHK_FR_STORE%ISOPEN then
         close C_CHK_FR_STORE;
      end if;
      if C_CHK_COMP_STORE%ISOPEN then
         close C_CHK_COMP_STORE;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END DEFAULT_WH_STORE_TYPES;
-----------------------------------------------------------------------------------------------
-- Name:    ALL_STORE_TYPES
-- Purpose: This function will check if the Store table contains both
--          Company and Franchise Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION ALL_STORE_TYPES (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_fr_store        IN OUT   BOOLEAN,
                          O_company_store   IN OUT   BOOLEAN)
                              
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)   := 'FILTER_LOV_VALIDATE_SQL.ALL_STORE_TYPES';   
   L_fr            VARCHAR2(1);
   L_comp          VARCHAR2(1);

   cursor C_CHK_FR_STORE is
      select 'x'
        from v_store vs             
       where vs.store_type = 'F'          
         and ROWNUM = 1;

   cursor C_CHK_COMP_STORE is
      select 'x'
        from v_store vs             
       where vs.store_type = 'C'
         and ROWNUM = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN',   
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_FR_STORE;                 

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_FR_STORE into L_fr;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_FR_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_FR_STORE;

   if L_fr is NULL then
      O_fr_store := FALSE;
   else
      O_fr_store := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   open C_CHK_COMP_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   fetch C_CHK_COMP_STORE into L_comp;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHK_COMP_STORE',
                    'V_STORE',
                    NULL);
   close C_CHK_COMP_STORE;

   if L_comp is NULL then
      O_company_store := FALSE;
   else
      O_company_store := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHK_FR_STORE%ISOPEN then
         close C_CHK_FR_STORE;
      end if;
      if C_CHK_COMP_STORE%ISOPEN then
         close C_CHK_COMP_STORE;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
         return FALSE;
END ALL_STORE_TYPES;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CLOSED_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_closed          IN OUT   BOOLEAN,
                               I_store           IN       V_STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE';
   L_store     VARCHAR2(1);

   cursor C_CHECK_STORE is
      select 'X'
        from v_store v,
             period p
       where store = I_store
         and v.store_close_date <= p.vdate;

BEGIN

   open C_CHECK_STORE;
   fetch C_CHECK_STORE into L_store;
   close C_CHECK_STORE;

   if L_store is NOT NULL then
      O_closed := TRUE;
   else
      O_closed := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_CLOSED_STORE;

--------------------------------------------------------------------------------
-- VALIDATE_IMP_EXP( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_IMP_EXP(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_valid           IN OUT BOOLEAN,
                          O_imp_exp_desc    IN OUT WH.WH_NAME%TYPE,
                          I_loc_type        IN     VARCHAR2,
                          I_value           IN     WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_IMP_EXP';

   cursor C_CHECK_IMP IS
      select wh_name
        from wh
       where wh = I_value
         and org_entity_type = 'M';

   cursor C_CHECK_EXP IS
      select wh_name
        from wh
       where wh = I_value
         and org_entity_type = 'X';

BEGIN
   if I_loc_type = 'M' then
      open C_CHECK_IMP;
      fetch C_CHECK_IMP into O_imp_exp_desc;
      close C_CHECK_IMP;
   else
      open C_CHECK_EXP;
      fetch C_CHECK_EXP into O_imp_exp_desc;
      close C_CHECK_EXP;
   end if;

   if O_imp_exp_desc is NOT NULL then
      O_valid := TRUE;
   elsif I_loc_type = 'M' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_IMPORTER', NULL, NULL, NULL);
      O_valid := FALSE;
   elsif I_loc_type = 'X' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_EXPORTER', NULL, NULL, NULL);
      O_valid := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_IMP_EXP;
--------------------------------------------------------------------------------
FUNCTION SA_STORE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_sa_store             IN OUT   BOOLEAN,
                  O_store_name           IN OUT   STORE.STORE_NAME%TYPE,
                  O_store_type           IN OUT   STORE.STORE_TYPE%TYPE,
                  I_store                IN       STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.SA_STORE';

   L_valid        BOOLEAN;
   L_store        V_STORE%ROWTYPE;

   cursor C_CHECK_SA_STORE is
      select store_name,
             store_type
        from v_sa_store
       where store = I_store;

BEGIN

   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(O_error_message,
                                             L_valid,
                                             L_store,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_store) = FALSE then
      return FALSE;
   end if;
   ---

   if L_valid = TRUE then
      open C_CHECK_SA_STORE;
      fetch C_CHECK_SA_STORE into O_store_name, O_store_type;

      if C_CHECK_SA_STORE%FOUND then
         O_sa_store := TRUE;
      else
         O_sa_store := FALSE;
      end if;

      close C_CHECK_SA_STORE;

   else -- L_valid is FALSE
      return FALSE;
   end if; -- check L_valid

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SA_STORE;
--------------------------------------------------------------------------------
-- VALIDATE_SUPPLIER( )
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid           IN OUT   BOOLEAN,
                           O_sups_row        IN OUT   V_SUPS%ROWTYPE,
                           I_supplier        IN       V_SUPS.SUPPLIER%TYPE,
                           I_financial_ind   IN       VARCHAR2 DEFAULT 'Y')
return BOOLEAN IS

   L_program   VARCHAR2(64) := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER';

   cursor C_CHECK_SUPS_TB is
      select stl.sup_name,
             s.supplier_parent
        from sups s,
             v_sups_tl stl
       where s.supplier = I_supplier
         and s.supplier = stl.supplier;

   cursor C_CHECK_SUPS_V is
      select *
        from v_sups
       where supplier = I_supplier;

BEGIN
   open C_CHECK_SUPS_V;
   fetch C_CHECK_SUPS_V into O_sups_row;
   if C_CHECK_SUPS_V%NOTFOUND then
      close C_CHECK_SUPS_V;
      ---
      open C_CHECK_SUPS_TB;
      fetch C_CHECK_SUPS_TB into O_sups_row.sup_name,
                                 O_sups_row.supplier_parent;
      if C_CHECK_SUPS_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', 'supplier' ,I_supplier, NULL);
      end if;
      close C_CHECK_SUPS_TB;
      ---
      O_valid := FALSE;
   else
      close C_CHECK_SUPS_V;
      if O_sups_row.sup_status = 'I' and I_financial_ind = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('SUPP_NOT_ACTIVE', NULL, NULL, NULL);
         O_valid := FALSE;
      else
         O_valid := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_SUPPLIER;
-----------------------------------------------------------------------------------------------
FUNCTION SUPPLIER_EXTERNAL_ID(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid           IN OUT BOOLEAN,
                              O_sups_row        IN OUT V_SUPS%ROWTYPE,
                              I_external_ref_id IN     SUPS.EXTERNAL_REF_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'FILTER_LOV_VALIDATE_SQL.SUPPLIER_EXTERNAL_ID';
   L_count     NUMBER         := NULL;

   cursor C_CHECK_SUPS_TB is
      select stl.sup_name,
             s.supplier_parent
        from sups s,
             v_sups_tl stl
       where s.external_ref_id = I_external_ref_id
         and s.supplier = stl.supplier;

   cursor C_CHECK_SUPS_V is
      select *
        from v_sups
       where supplier in (select supplier
                            from sups
                           where external_ref_id = I_external_ref_id);
      
   cursor C_COUT_SUPS is
      select count(supplier)
        from sups
       where external_ref_id = I_external_ref_id
       order by supplier;                     
      

BEGIN
   open C_CHECK_SUPS_V;
   fetch C_CHECK_SUPS_V into O_sups_row;
   if C_CHECK_SUPS_V%NOTFOUND then
      ---
      open C_CHECK_SUPS_TB;
      fetch C_CHECK_SUPS_TB into O_sups_row.sup_name,O_sups_row.supplier_parent;
      if C_CHECK_SUPS_TB%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUPPLIER', NULL, NULL, NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VIS_HIER', 'supplier' ,I_external_ref_id, NULL);
      end if;
      close C_CHECK_SUPS_TB;
      ---
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   close C_CHECK_SUPS_V;
   --
   open C_COUT_SUPS;
   fetch C_COUT_SUPS into L_count;
   close C_COUT_SUPS;
   
   if L_count > 1 then
      O_sups_row := NULL;
   end if;
   --
  
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END SUPPLIER_EXTERNAL_ID;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SEC_MASTER_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists          IN OUT   BOOLEAN,
                                I_master_po       IN       ORDHEAD.MASTER_PO_NO%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'FILTER_LOV_VALIDATE_SQL.VALIDATE_SEC_MASTER_PO';
   L_dummy     VARCHAR2(1)  := NULL;

   cursor C_CHK_MASTER_PO is
      select 'x'
        from ordhead
       where master_po_no = I_master_po;

   cursor C_CHK_MASTER_PO_V is
      select 'x'
        from v_sec_ordhead
       where master_po_no = I_master_po;

BEGIN

   if I_master_po is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_master_po,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   O_exists := TRUE;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHK_MASTER_PO_V',
                    'V_SEC_ORDHEAD',
                    'MASTER ORDER NO: '||I_master_po);
   open  C_CHK_MASTER_PO_V;
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHK_MASTER_PO_V',
                    'V_SEC_ORDHEAD',
                    'MASTER ORDER NO: '||I_master_po);
   fetch C_CHK_MASTER_PO_V into L_dummy;
   ---
   if C_CHK_MASTER_PO_V%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_MASTER_PO_V',
                       'V_SEC_ORDHEAD',
                       'MASTER ORDER NO: '||I_master_po);
      close C_CHK_MASTER_PO_V;
      ---
      L_dummy := NULL;
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHK_MASTER_PO',
                       'ORDHEAD',
                       'MASTER ORDER NO: '||I_master_po);
      open  C_CHK_MASTER_PO;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHK_MASTER_PO',
                       'ORDHEAD',
                       'MASTER ORDER NO: '||I_master_po);
      fetch C_CHK_MASTER_PO into L_dummy;
      ---
      if C_CHK_MASTER_PO%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_MASTER_PO',
                                               NULL,
                                               NULL,
                                               NULL);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_MASTER_PO',
                                               I_master_po,
                                               NULL,
                                               NULL);
      end if;
      ---
      O_exists := FALSE;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_MASTER_PO',
                       'ORDHEAD',
                       'MASTER ORDER NO: '||I_master_po);
      close C_CHK_MASTER_PO;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHK_MASTER_PO_V',
                       'V_SEC_ORDHEAD',
                       'MASTER ORDER NO: '||I_master_po);
      close C_CHK_MASTER_PO_V;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END VALIDATE_SEC_MASTER_PO;
-----------------------------------------------------------------------------------------------
END FILTER_LOV_VALIDATE_SQL;
/
