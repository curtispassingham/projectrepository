-- File Name : CORESVC_COMPANY_CLOSED_body.pls
CREATE OR REPLACE PACKAGE BODY CORESVC_COMPANY_CLOSED as
   cursor C_SVC_COMPANY_CLOSED(I_process_id NUMBER,
                               I_chunk_id NUMBER) is
      select pk_company_closed.rowid as pk_company_closed_rid,
             st.close_desc,
             st.close_date,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_company_closed  st,
             company_closed      pk_company_closed
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.close_date  =  pk_company_closed.close_date (+);

   cursor C_SVC_SCX(I_process_id NUMBER,
                    I_chunk_id NUMBER) is
      select pk_company_closed_excep.rowid     as  pk_company_closed_excep_rid,
             scx_scc_fk.rowid                  as  scx_scc_fk_rid,
             pk_company_closed_excep.ship_ind  as  base_ship_ind,
             pk_company_closed_excep.recv_ind  as  base_recv_ind,
             pk_company_closed_excep.sales_ind as  base_sales_ind,
             pk_company_closed_excep.loc_type  as  base_loc_type,
             UPPER(st.ship_ind)                as  ship_ind,
             UPPER(st.recv_ind)                as  recv_ind,
             UPPER(st.sales_ind)               as  sales_ind,
             UPPER(st.loc_type)                as  loc_type,
             st.location,
             st.close_date,
             st.chunk_id,
             st.process_id,
             st.row_seq,
             UPPER(st.action)                  as  action,
             st.process$status
        from svc_company_closed_excep  st,
             company_closed_excep      pk_company_closed_excep,
             company_closed            scx_scc_fk,
             dual
       where st.process_id   = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.location     = pk_company_closed_excep.location (+)
         and st.close_date   = pk_company_closed_excep.close_date (+)
         and st.close_date   = scx_scc_fk.close_date (+)
         and (st.process$status = 'N' or st.process$status  is NULL);

   TYPE errors_tab_typ is TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;

   LP_errors_tab errors_tab_typ;

   TYPE s9t_errors_tab_typ is TABLE OF S9T_ERRORS%ROWTYPE;

   LP_s9t_errors_tab S9T_errors_tab_typ;

   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   Type CCTL_rec_tab IS TABLE OF COMPANY_CLOSED_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;

   LP_primary_lang    LANG.LANG%TYPE;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
end GET_SHEET_NAME_TRANS;
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id  IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet    IN VARCHAR2,
                           I_row_seq  IN NUMBER,
                           I_col      IN VARCHAR2,
                           I_sqlcode  IN NUMBER,
                           I_sqlerrm  IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
           'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id  NUMBER) IS
   L_sheets                 S9T_PKG.NAMES_MAP_TYP;
   COMPANY_CLOSED_cols      S9T_PKG.NAMES_MAP_TYP;
   COMPANY_CLOSED_TL_cols   S9T_PKG.NAMES_MAP_TYP;
   SCX_cols                 S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                  := S9T_PKG.get_sheet_names(I_file_id);
   COMPANY_CLOSED_cols       := S9T_PKG.get_col_names(I_file_id,COMPANY_CLOSED_sheet);
   COMPANY_CLOSED$ACTION     := COMPANY_CLOSED_cols('ACTION');
   COMPANY_CLOSED$CLOSE_DATE := COMPANY_CLOSED_cols('CLOSE_DATE');
   COMPANY_CLOSED$CLOSE_DESC := COMPANY_CLOSED_cols('CLOSE_DESC');

   COMPANY_CLOSED_TL_cols       := S9T_PKG.get_col_names(I_file_id,COMPANY_CLOSED_TL_sheet);
   COMPANY_CLOSED_TL$ACTION     := COMPANY_CLOSED_TL_cols('ACTION');
   COMPANY_CLOSED_TL$LANG       := COMPANY_CLOSED_TL_cols('LANG');
   COMPANY_CLOSED_TL$CLOSE_DATE := COMPANY_CLOSED_TL_cols('CLOSE_DATE');
   COMPANY_CLOSED_TL$CLOSE_DESC := COMPANY_CLOSED_TL_cols('CLOSE_DESC');

   SCX_cols       := S9T_PKG.get_col_names(I_file_id,SCX_sheet);
   SCX$Action     := SCX_cols('ACTION');
   SCX$CLOSE_DATE := SCX_cols('CLOSE_DATE');
   SCX$LOCATION   := SCX_cols('LOCATION');
   SCX$LOC_TYPE   := SCX_cols('LOC_TYPE');
   SCX$SALES_IND  := SCX_cols('SALES_IND');
   SCX$RECV_IND   := SCX_cols('RECV_IND');
   SCX$SHIP_IND   := SCX_cols('SHIP_IND');
END POPULATE_NAMES;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COMPANY_CLOSED( I_file_id IN NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = COMPANY_CLOSED_sheet )
   select S9T_ROW(S9T_CELLS(CORESVC_COMPANY_CLOSED.action_mod,
                            close_date,
                            close_desc  ))
     from company_closed;
END POPULATE_COMPANY_CLOSED;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COMPANY_CLOSED_TL( I_file_id IN NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = COMPANY_CLOSED_TL_sheet )
   select S9T_ROW(S9T_CELLS(CORESVC_COMPANY_CLOSED.action_mod,
                            lang,
                            close_date,
                            close_desc  ))
     from company_closed_tl;
END POPULATE_COMPANY_CLOSED_TL;
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SCX( I_file_id IN NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id  = I_file_id
          and ss.sheet_name = SCX_sheet )
   select s9t_row(s9t_cells(CORESVC_COMPANY_CLOSED.action_mod,
                            close_date,
                            location,
                            loc_type,
                            sales_ind,
                            recv_ind,
                            ship_ind))
     from company_closed_excep ;
END POPULATE_SCX;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id IN OUT NUMBER) IS
   L_file       S9T_FILE;
   L_file_name  S9T_FOLDER.file_name%TYPE;
BEGIN
   L_file              := NEW S9T_FILE();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;


   L_file.ADD_SHEET(COMPANY_CLOSED_sheet);
   L_file.SHEETS(L_file.GET_SHEET_INDEX(COMPANY_CLOSED_SHEET)).column_headers := S9T_CELLS( 'ACTION',
                                                                                            'CLOSE_DATE',
                                                                                            'CLOSE_DESC' );

   L_file.ADD_SHEET(COMPANY_CLOSED_TL_sheet);
   L_file.SHEETS(L_file.GET_SHEET_INDEX(COMPANY_CLOSED_TL_SHEET)).column_headers := S9T_CELLS( 'ACTION',
                                                                                               'LANG',
                                                                                               'CLOSE_DATE',
                                                                                               'CLOSE_DESC' );

   L_file.add_sheet(SCX_sheet);
   L_file.sheets(L_file.get_sheet_index(SCX_sheet)).column_headers := s9t_cells('ACTION',
                                                                                'CLOSE_DATE',
                                                                                'LOCATION',
                                                                                'LOC_TYPE',
                                                                                'SALES_IND',
                                                                                'RECV_IND',
                                                                                'SHIP_IND');

   s9t_pkg.save_obj(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_file    s9t_file;
   L_program VARCHAR2(64):='CORESVC_COMPANY_CLOSED.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_COMPANY_CLOSED(O_file_id);
      POPULATE_COMPANY_CLOSED_TL(O_file_id);
      POPULATE_SCX(O_file_id);
      Commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COMPANY_CLOSED( I_file_id    IN s9t_folder.file_id%TYPE,
                                      I_process_id IN SVC_COMPANY_CLOSED.process_id%TYPE ) IS

   TYPE svc_COMPANY_CLOSED_col_typ IS TABLE OF SVC_COMPANY_CLOSED%ROWTYPE;

   L_error                 BOOLEAN := FALSE;
   L_temp_rec              SVC_COMPANY_CLOSED%rowtype;
   L_default_rec           SVC_COMPANY_CLOSED%rowtype;
   L_process_id            SVC_COMPANY_CLOSED.process_id%type;
   svc_company_closed_col  SVC_COMPANY_CLOSED_col_typ := NEW SVC_COMPANY_CLOSED_col_typ();

   cursor C_MANDATORY_IND is
      select close_desc_mi,
             close_date_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_COMPANY_CLOSED.template_key
                 and wksht_key     ='COMPANY_CLOSED' )
               PIVOT (MAX(mandatory) as mi FOR (column_key) in ( 'CLOSE_DESC' as CLOSE_DESC,
                                                                 'CLOSE_DATE' as CLOSE_DATE,
                                                                        null  as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA     EXCEPTION_INIT(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMPANY_CLOSED';
   L_pk_columns    VARCHAR2(255)  := 'Close Date';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  FOR rec in
  (select CLOSE_DESC_dv,
          CLOSE_DATE_dv,
          NULL as dummy
     from
        (select column_key,
                default_value
           from s9t_tmpl_cols_def
          where template_key = CORESVC_COMPANY_CLOSED.template_key
            and wksht_key    ='COMPANY_CLOSED' )
        PIVOT (MAX(default_value) as dv FOR (column_key) in ('CLOSE_DESC' as CLOSE_DESC,
                                                             'CLOSE_DATE' as CLOSE_DATE,
                                                                    NULL  as dummy)))   LOOP
   BEGIN
      L_default_rec.CLOSE_DESC := rec.CLOSE_DESC_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPANY_CLOSED',
                          NULL,
                         'CLOSE_DESC',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.CLOSE_DATE := rec.CLOSE_DATE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'COMPANY_CLOSED',
                          NULL,
                         'CLOSE_DATE',
                          NULL,
                         'INV_DEFAULT');
   END;
   END LOOP;

   open C_mandatory_ind;
   fetch C_mandatory_ind
   into L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN
    (select r.get_cell(COMPANY_CLOSED$Action)      as ACTION,
            r.get_cell(COMPANY_CLOSED$CLOSE_DESC)  as CLOSE_DESC,
            r.get_cell(COMPANY_CLOSED$CLOSE_DATE)  as CLOSE_DATE,
            r.get_row_seq()                        as row_seq
       from s9t_folder sf,
            TABLE(sf.s9t_file_obj.sheets) ss,
            TABLE(ss.s9t_rows) r
      where sf.file_id  = I_file_id
        and ss.sheet_name = GET_SHEET_NAME_TRANS(COMPANY_CLOSED_sheet))
LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                       COMPANY_CLOSED_sheet,
                       rec.row_seq,
                       action_column,
                       SQLCODE,
                       SQLERRM );
      L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CLOSE_DESC := rec.CLOSE_DESC;
   EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                      COMPANY_CLOSED_sheet,
                      rec.row_seq,
                     'CLOSE_DESC',
                      SQLCODE,
                      SQLERRM );
      L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CLOSE_DATE := rec.CLOSE_DATE;
   EXCEPTION
   when OTHERS then
      WRITE_S9T_ERROR( I_file_id,
                       COMPANY_CLOSED_sheet,
                       rec.row_seq,
                      'CLOSE_DATE',
                       SQLCODE,
                       SQLERRM );
      L_error := TRUE;
   END;

   if rec.action = CORESVC_COMPANY_CLOSED.action_new then
      L_temp_rec.CLOSE_DESC := NVL( L_temp_rec.CLOSE_DESC,
                                    L_default_rec.CLOSE_DESC);
      L_temp_rec.CLOSE_DATE := NVL( L_temp_rec.CLOSE_DATE,
                                    L_default_rec.CLOSE_DATE);
   end if;

   if NOT ( L_temp_rec.CLOSE_DATE is not null
            and 1 = 1 ) then
         WRITE_S9T_ERROR( I_file_id,
                          COMPANY_CLOSED_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
   end if;

   if NOT L_error then
      svc_COMPANY_CLOSED_col.EXTEND();
      svc_COMPANY_CLOSED_col(svc_COMPANY_CLOSED_col.COUNT()):=L_temp_rec;
   end if;

   END LOOP;

   BEGIN
   forall i IN 1..svc_COMPANY_CLOSED_col.count SAVE EXCEPTIONS
      merge into SVC_COMPANY_CLOSED st
      using (select (case
                        when L_mi_rec.CLOSE_DESC_mi               = 'N' and
                             svc_COMPANY_CLOSED_col(i).action     = coresvc_item.action_mod and
                             s1.CLOSE_DESC IS NULL then
                             mt.CLOSE_DESC
                        else s1.CLOSE_DESC
                        end) as CLOSE_DESC,
                    (case
                        when L_mi_rec.CLOSE_DATE_mi               = 'N'
                             and svc_COMPANY_CLOSED_col(i).action = coresvc_item.action_mod
                             and s1.CLOSE_DATE             IS NULL then
                             mt.CLOSE_DATE
                        else s1.CLOSE_DATE
                        end) as CLOSE_DATE,
                        null as dummy
               from (select svc_COMPANY_CLOSED_col(i).CLOSE_DESC AS CLOSE_DESC,
                            svc_COMPANY_CLOSED_col(i).CLOSE_DATE AS CLOSE_DATE,
                            null as dummy
                       from dual ) s1, COMPANY_CLOSED mt
                      where mt.CLOSE_DATE (+)         = s1.CLOSE_DATE  and
                            1                         = 1 ) sq
         on (  st.CLOSE_DATE  =  sq.CLOSE_DATE and
               svc_COMPANY_CLOSED_col(i).action in (CORESVC_COMPANY_CLOSED.action_mod,CORESVC_COMPANY_CLOSED.action_del ))
         when matched then
            update
               set process_id        = svc_COMPANY_CLOSED_col(i).PROCESS_ID ,
                   chunk_id          = svc_COMPANY_CLOSED_col(i).CHUNK_ID ,
                   row_seq           = svc_COMPANY_CLOSED_col(i).ROW_SEQ ,
                   action            = svc_COMPANY_CLOSED_col(i).ACTION,
                   process$status    = svc_COMPANY_CLOSED_col(i).PROCESS$STATUS ,
                   close_desc        = sq.CLOSE_DESC,
                   create_id         = svc_COMPANY_CLOSED_col(i).CREATE_ID ,
                   create_datetime   = svc_COMPANY_CLOSED_col(i).CREATE_DATETIME ,
                   last_upd_id       = svc_COMPANY_CLOSED_col(i).LAST_UPD_ID ,
                   last_upd_datetime = svc_COMPANY_CLOSED_col(i).LAST_UPD_DATETIME
         when NOT matched then
            insert( process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    close_desc ,
                    close_date ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime )
            values( svc_COMPANY_CLOSED_col(i).PROCESS_ID ,
                    svc_COMPANY_CLOSED_col(i).CHUNK_ID ,
                    svc_COMPANY_CLOSED_col(i).ROW_SEQ ,
                    svc_COMPANY_CLOSED_col(i).ACTION ,
                    svc_COMPANY_CLOSED_col(i).PROCESS$STATUS ,
                    sq.CLOSE_DESC ,
                    sq.CLOSE_DATE ,
                    svc_COMPANY_CLOSED_col(i).CREATE_ID ,
                    svc_COMPANY_CLOSED_col(i).CREATE_DATETIME ,
                    svc_COMPANY_CLOSED_col(i).LAST_UPD_ID ,
                    svc_COMPANY_CLOSED_col(i).LAST_UPD_DATETIME );
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            COMPANY_CLOSED_sheet,
                            svc_company_closed_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_COMPANY_CLOSED;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COMPANY_CLOSED_TL(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                                        I_process_id IN SVC_COMPANY_CLOSED.PROCESS_ID%TYPE) IS

   TYPE SVC_COMPANY_CLOSED_TL_COL_TYP IS TABLE OF SVC_COMPANY_CLOSED_TL%ROWTYPE;

   L_error                    BOOLEAN := FALSE;
   L_temp_rec                 SVC_COMPANY_CLOSED_TL%ROWTYPE;
   L_default_rec              SVC_COMPANY_CLOSED_TL%ROWTYPE;
   L_process_id               SVC_COMPANY_CLOSED_TL.process_id%TYPE;
   svc_company_closed_tl_col  SVC_COMPANY_CLOSED_TL_col_typ := NEW SVC_COMPANY_CLOSED_TL_col_typ();

   cursor C_MANDATORY_IND is
      select lang_mi,
             close_desc_mi,
             close_date_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_COMPANY_CLOSED.TEMPLATE_KEY
                 and wksht_key     = 'COMPANY_CLOSED_TL' )
               PIVOT (MAX(mandatory) as mi FOR (column_key) in ( 'LANG'       as LANG,
                                                                 'CLOSE_DESC' as CLOSE_DESC,
                                                                 'CLOSE_DATE' as CLOSE_DATE));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA     EXCEPTION_INIT(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMPANY_CLOSED_TL';
   L_pk_columns    VARCHAR2(255)  := 'Lang, Close Date';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  FOR rec in
  (select LANG_dv,
          CLOSE_DESC_dv,
          CLOSE_DATE_dv
     from
        (select column_key,
                default_value
           from s9t_tmpl_cols_def
          where template_key = CORESVC_COMPANY_CLOSED.TEMPLATE_KEY
            and wksht_key    = 'COMPANY_CLOSED_TL' )
        PIVOT (MAX(default_value) as dv FOR (column_key) in ('LANG'       as LANG,
                                                             'CLOSE_DESC' as CLOSE_DESC,
                                                             'CLOSE_DATE' as CLOSE_DATE))) LOOP
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'COMPANY_CLOSED_TL',
                             NULL,
                            'LANG',
                             NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.CLOSE_DESC := rec.CLOSE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'COMPANY_CLOSED_TL',
                             NULL,
                            'CLOSE_DESC',
                             NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.CLOSE_DATE := rec.CLOSE_DATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'COMPANY_CLOSED_TL',
                             NULL,
                            'CLOSE_DATE',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
    (select r.get_cell(COMPANY_CLOSED_TL$Action)      as ACTION,
            r.get_cell(COMPANY_CLOSED_TL$LANG)        as LANG,
            r.get_cell(COMPANY_CLOSED_TL$CLOSE_DESC)  as CLOSE_DESC,
            r.get_cell(COMPANY_CLOSED_TL$CLOSE_DATE)  as CLOSE_DATE,
            r.get_row_seq()                           as row_seq
       from s9t_folder sf,
            TABLE(sf.s9t_file_obj.sheets) ss,
            TABLE(ss.s9t_rows) r
      where sf.file_id  = I_file_id
        and ss.sheet_name = GET_SHEET_NAME_TRANS(COMPANY_CLOSED_TL_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPANY_CLOSED_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM );
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPANY_CLOSED_TL_sheet,
                            rec.row_seq,
                           'LANG',
                            SQLCODE,
                            SQLERRM );
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.CLOSE_DESC := rec.CLOSE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPANY_CLOSED_TL_sheet,
                            rec.row_seq,
                           'CLOSE_DESC',
                            SQLCODE,
                            SQLERRM );
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.CLOSE_DATE := rec.CLOSE_DATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPANY_CLOSED_TL_sheet,
                            rec.row_seq,
                            'CLOSE_DATE',
                            SQLCODE,
                            SQLERRM );
            L_error := TRUE;
      END;

      if rec.action = CORESVC_COMPANY_CLOSED.action_new then
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,
                                 L_default_rec.LANG);
         L_temp_rec.CLOSE_DESC := NVL( L_temp_rec.CLOSE_DESC,
                                       L_default_rec.CLOSE_DESC);
         L_temp_rec.CLOSE_DATE := NVL( L_temp_rec.CLOSE_DATE,
                                       L_default_rec.CLOSE_DATE);
      end if;

      if L_temp_rec.LANG is NULL then
         WRITE_S9T_ERROR(I_file_id,
                         COMPANY_CLOSED_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if L_temp_rec.CLOSE_DATE is NULL then
         WRITE_S9T_ERROR(I_file_id,
                         COMPANY_CLOSED_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_company_closed_tl_col.EXTEND();
         svc_company_closed_tl_col(svc_company_closed_tl_col.COUNT()) := L_temp_rec;
      end if;

   END LOOP;

   BEGIN
      FORALL i IN 1..svc_company_closed_tl_col.count SAVE EXCEPTIONS
         merge into SVC_COMPANY_CLOSED_TL st
         using (select (case
                           when L_mi_rec.LANG_mi               = 'N' and
                                svc_company_closed_tl_col(i).action     = coresvc_item.action_mod and
                                s1.LANG IS NULL then
                                mt.LANG
                           else s1.LANG
                           end) as LANG,
                       (case
                           when L_mi_rec.CLOSE_DESC_mi               = 'N' and
                                svc_company_closed_tl_col(i).action     = coresvc_item.action_mod and
                                s1.CLOSE_DESC IS NULL then
                                mt.CLOSE_DESC
                           else s1.CLOSE_DESC
                           end) as CLOSE_DESC,
                       (case
                           when L_mi_rec.CLOSE_DATE_mi               = 'N'
                                and svc_company_closed_tl_col(i).action = coresvc_item.action_mod
                                and s1.CLOSE_DATE             IS NULL then
                                mt.CLOSE_DATE
                           else s1.CLOSE_DATE
                           end) as CLOSE_DATE,
                           null as dummy
                  from (select svc_company_closed_tl_col(i).LANG AS LANG,
                               svc_company_closed_tl_col(i).CLOSE_DESC AS CLOSE_DESC,
                               svc_company_closed_tl_col(i).CLOSE_DATE AS CLOSE_DATE
                          from dual ) s1,
                       COMPANY_CLOSED_TL mt
                 where mt.CLOSE_DATE (+)         = s1.CLOSE_DATE  and
                       mt.LANG (+)               = s1.LANG ) sq
            on (  st.LANG = sq.lang and
                  st.CLOSE_DATE  =  sq.CLOSE_DATE and
                  svc_company_closed_tl_col(i).action in (CORESVC_COMPANY_CLOSED.action_mod,CORESVC_COMPANY_CLOSED.action_del ))
            when matched then
               update
                  set process_id        = svc_company_closed_tl_col(i).PROCESS_ID ,
                      chunk_id          = svc_company_closed_tl_col(i).CHUNK_ID ,
                      row_seq           = svc_company_closed_tl_col(i).ROW_SEQ ,
                      action            = svc_company_closed_tl_col(i).ACTION,
                      process$status    = svc_company_closed_tl_col(i).PROCESS$STATUS ,
                      close_desc        = sq.CLOSE_DESC
            when NOT matched then
               insert( process_id ,
                       chunk_id ,
                       row_seq ,
                       action ,
                       process$status ,
                       lang ,
                       close_desc ,
                       close_date )
               values( svc_company_closed_tl_col(i).PROCESS_ID ,
                       svc_company_closed_tl_col(i).CHUNK_ID ,
                       svc_company_closed_tl_col(i).ROW_SEQ ,
                       svc_company_closed_tl_col(i).ACTION ,
                       svc_company_closed_tl_col(i).PROCESS$STATUS ,
                       sq.LANG,
                       sq.CLOSE_DESC ,
                       sq.CLOSE_DATE );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
               L_error_code:=sql%bulk_exceptions(i).error_code;
               if L_error_code=1 then
                  L_error_code:=NULL;
                  L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
               end if;

               WRITE_S9T_ERROR( I_file_id,
                               COMPANY_CLOSED_TL_sheet,
                               svc_company_closed_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                               NULL,
                               L_error_code,
                               L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_COMPANY_CLOSED_TL;
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SCX( I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                           I_process_id IN SVC_COMPANY_CLOSED_EXCEP.PROCESS_ID%TYPE ) IS

   TYPE svc_SCX_col_typ is TABLE OF SVC_COMPANY_CLOSED_EXCEP%ROWTYPE;
   L_error       BOOLEAN := FALSE;
   L_temp_rec    SVC_COMPANY_CLOSED_EXCEP%ROWTYPE;
   L_process_id  SVC_COMPANY_CLOSED_EXCEP.process_id%TYPE;
   L_default_rec SVC_COMPANY_CLOSED_EXCEP%ROWTYPE;
   svc_scx_col   svc_SCX_col_typ := NEW svc_scx_col_typ();

   cursor C_MANDATORY_IND is
      select ship_ind_mi,
             recv_ind_mi,
             sales_ind_mi,
             loc_type_mi,
             location_mi,
             close_date_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
              where template_key  = CORESVC_COMPANY_CLOSED.template_key
                 and wksht_key    = 'COMPANY_CLOSED_EXCEP') 
               PIVOT (MAX(mandatory) as mi FOR (column_key) in ('SHIP_IND'   as SHIP_IND,
                                                                'RECV_IND'   as RECV_IND,
                                                                'SALES_IND'  as SALES_IND,
                                                                'LOC_TYPE'   as LOC_TYPE,
                                                                'LOCATION'   as LOCATION,
                                                                'CLOSE_DATE' as CLOSE_DATE,
                                                                 NULL        as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMPANY_CLOSED_EXCEP';
   L_pk_columns    VARCHAR2(255)  := 'Location,Close Date';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec IN
   (select SHIP_IND_dv,
           RECV_IND_dv,
           SALES_IND_dv,
           LOC_TYPE_dv,
           LOCATION_dv,
           CLOSE_DATE_dv,
           NULL          as dummy
     from (select column_key,
                  default_value
             from s9t_tmpl_cols_def
            where template_key  = CORESVC_COMPANY_CLOSED.template_key
              and wksht_key     = 'COMPANY_CLOSED_EXCEP') 
          PIVOT (MAX(default_value) as dv FOR (column_key) in ('SHIP_IND'   as SHIP_IND,
                                                               'RECV_IND'   as RECV_IND,
                                                               'SALES_IND'  as SALES_IND,
                                                               'LOC_TYPE'   as LOC_TYPE,
                                                               'LOCATION'   as LOCATION,
                                                               'CLOSE_DATE' as CLOSE_DATE,
                                                                NULL        as dummy))) LOOP  
   BEGIN  
      L_default_rec.SHIP_IND := rec.SHIP_IND_dv;  
   EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR( I_file_id,
                         'COMPANY_CLOSED_EXCEP',
                          NULL,
                         'SHIP_IND',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.RECV_IND := rec.RECV_IND_dv;  
   EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR( I_file_id,
                         'COMPANY_CLOSED_EXCEP',
                          NULL,
                         'RECV_IND',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
      L_default_rec.SALES_IND := rec.SALES_IND_dv;  
   EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR( I_file_id,
                         'COMPANY_CLOSED_EXCEP',
                          NULL,
                         'SALES_IND',
                          NULL,
                         'INV_DEFAULT');  
   END;

   BEGIN  
      L_default_rec.LOC_TYPE := rec.LOC_TYPE_dv;  
   EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR( I_file_id,
                         'COMPANY_CLOSED_EXCEP',
                          NULL,
                         'LOC_TYPE',
                          NULL,
                         'INV_DEFAULT');  
   END;

   BEGIN
      L_default_rec.LOCATION := rec.LOCATION_dv;  
   EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR( I_file_id,
                         'COMPANY_CLOSED_EXCEP',
                          NULL,
                         'LOCATION',
                          NULL,
                         'INV_DEFAULT');  
   END;

   BEGIN  
      L_default_rec.CLOSE_DATE := rec.CLOSE_DATE_dv;  
   EXCEPTION  
      when OTHERS then  
         WRITE_S9T_ERROR( I_file_id,
                         'COMPANY_CLOSED_EXCEP',
                          NULL,
                         'CLOSE_DATE',
                          NULL,
                         'INV_DEFAULT');  
   END;

   END LOOP;

   open  C_mandatory_ind;
   fetch C_mandatory_ind
   into  L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN
    (select r.get_cell(SCX$Action)     as ACTION,
            r.get_cell(SCX$SHIP_IND)   as SHIP_IND,
            r.get_cell(SCX$RECV_IND)   as RECV_IND,
            r.get_cell(SCX$SALES_IND)  as SALES_IND,
            r.get_cell(SCX$LOC_TYPE)   as LOC_TYPE,
            r.get_cell(SCX$LOCATION)   as LOCATION,
            r.get_cell(SCX$CLOSE_DATE) as CLOSE_DATE,
            r.get_row_seq()            as row_seq
       from s9t_folder sf,
            TABLE(sf.s9t_file_obj.sheets) ss,
            TABLE(ss.s9t_rows) r
      where sf.file_id    = I_file_id
        and ss.sheet_name = GET_SHEET_NAME_TRANS(SCX_sheet))   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          SCX_sheet,
                          rec.row_seq,
                          action_column,
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.SHIP_IND := rec.SHIP_IND;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          SCX_sheet,
                          rec.row_seq,
                          'SHIP_IND',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.RECV_IND := rec.RECV_IND;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          SCX_sheet,
                          rec.row_seq,
                         'RECV_IND',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.SALES_IND := rec.SALES_IND;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          SCX_sheet,
                          rec.row_seq,
                         'SALES_IND',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.LOC_TYPE := rec.LOC_TYPE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          SCX_sheet,
                          rec.row_seq,
                         'LOC_TYPE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.LOCATION := rec.LOCATION;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          SCX_sheet,
                          rec.row_seq,
                         'LOCATION',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.CLOSE_DATE := rec.CLOSE_DATE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          SCX_sheet,
                          rec.row_seq,
                         'CLOSE_DATE',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;
   if rec.action = CORESVC_COMPANY_CLOSED.action_new then
      L_temp_rec.SHIP_IND   := NVL( L_temp_rec.SHIP_IND,
                                    L_default_rec.SHIP_IND);
      L_temp_rec.RECV_IND   := NVL( L_temp_rec.RECV_IND,
                                    L_default_rec.RECV_IND);
      L_temp_rec.SALES_IND  := NVL( L_temp_rec.SALES_IND,
                                    L_default_rec.SALES_IND);
      L_temp_rec.LOC_TYPE   := NVL( L_temp_rec.LOC_TYPE,
                                    L_default_rec.LOC_TYPE);
      L_temp_rec.LOCATION   := NVL( L_temp_rec.LOCATION,
                                    L_default_rec.LOCATION);
      L_temp_rec.CLOSE_DATE := NVL( L_temp_rec.CLOSE_DATE,
                                    L_default_rec.CLOSE_DATE);
   end if;
   if NOT ( L_temp_rec.LOCATION is NOT NULL 
            and L_temp_rec.CLOSE_DATE is NOT NULL 
            and 1 = 1 ) then
         WRITE_S9T_ERROR( I_file_id,
                          SCX_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
   end if;
   if NOT L_error then
      svc_SCX_col.EXTEND();
      svc_SCX_col(svc_SCX_col.count()):=L_temp_rec;
   end if;
  END LOOP;

BEGIN
   forall i IN 1..svc_SCX_col.count SAVE EXCEPTIONS 
      merge INTO SVC_COMPANY_CLOSED_EXCEP st 
      using (select (case
                        when l_mi_rec.SHIP_IND_mi    = 'N' and 
                             svc_SCX_col(i).action   = coresvc_item.action_mod and 
                             s1.ship_ind   is NULL then 
                             mt.ship_ind
                        else s1.ship_ind
                        end) as SHIP_IND,
                    (case
                        when l_mi_rec.RECV_IND_mi    = 'N' and 
                             svc_SCX_col(i).action   = coresvc_item.action_mod and 
                             s1.recv_ind   is NULL then 
                             mt.recv_ind
                        else s1.recv_ind
                        end) as recv_ind,
                    (case
                        when l_mi_rec.SALES_IND_mi   = 'N' and 
                             svc_SCX_col(i).action   = coresvc_item.action_mod and 
                             s1.sales_ind  is NULL then 
                             mt.sales_ind
                        else s1.sales_ind
                        end) as sales_ind,
                    (case
                        when l_mi_rec.LOC_TYPE_mi    = 'N' and 
                             svc_SCX_col(i).action   = coresvc_item.action_mod and 
                             s1.loc_type   is NULL then 
                             mt.loc_type
                        else s1.loc_type
                        end) as loc_type,
                    (case
                        when l_mi_rec.LOCATION_mi    = 'N' and 
                             svc_SCX_col(i).action   = coresvc_item.action_mod and 
                             s1.location   is NULL then 
                             mt.location
                        else s1.location
                        end) as location,
                    (case
                        when l_mi_rec.CLOSE_DATE_mi  = 'N' and 
                             svc_SCX_col(i).action   = coresvc_item.action_mod and 
                             s1.close_date is NULL then 
                             mt.close_date
                        else s1.close_date
                        end) as close_date,
                        NULL as dummy
               from (select svc_SCX_col(i).SHIP_IND   as SHIP_IND,
                            svc_SCX_col(i).RECV_IND   as RECV_IND,
                            svc_SCX_col(i).SALES_IND  as SALES_IND,
                            svc_SCX_col(i).LOC_TYPE   as LOC_TYPE,
                            svc_SCX_col(i).LOCATION   as LOCATION,
                            svc_SCX_col(i).CLOSE_DATE as CLOSE_DATE,
                                                 null as dummy
                       from dual) s1, COMPANY_CLOSED_EXCEP mt
                      where mt.LOCATION (+)         = s1.LOCATION 
                            and mt.CLOSE_DATE (+)   = s1.CLOSE_DATE   
                            and 1                   = 1 ) sq 
         on (       st.LOCATION      = sq.LOCATION and
                    st.CLOSE_DATE    = sq.CLOSE_DATE and
                    svc_SCX_col(i).action in (CORESVC_COMPANY_CLOSED.action_mod,CORESVC_COMPANY_CLOSED.action_del))
         when matched then
            update
               set process_id        = svc_SCX_col(i).PROCESS_ID ,
                   chunk_id          = svc_SCX_col(i).CHUNK_ID ,
                   row_seq           = svc_SCX_col(i).ROW_SEQ ,
                   action            = svc_SCX_col(i).ACTION,
                   process$status    = svc_SCX_col(i).PROCESS$STATUS ,
                   recv_ind          = sq.RECV_IND ,
                   ship_ind          = sq.SHIP_IND ,
                   loc_type          = sq.LOC_TYPE ,
                   sales_ind         = sq.SALES_IND ,
                   create_id         = svc_SCX_col(i).CREATE_ID ,
                   create_datetime   = svc_SCX_col(i).CREATE_DATETIME ,
                   last_upd_id       = svc_SCX_col(i).LAST_UPD_ID ,
                   last_upd_datetime = svc_SCX_col(i).LAST_UPD_DATETIME 
         when NOT matched then
            insert( process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    ship_ind ,
                    recv_ind ,
                    sales_ind ,
                    loc_type ,
                    location ,
                    close_date ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime)
            values( svc_SCX_col(i).PROCESS_ID ,
                    svc_SCX_col(i).CHUNK_ID ,
                    svc_SCX_col(i).ROW_SEQ ,
                    svc_SCX_col(i).ACTION ,
                    svc_SCX_col(i).PROCESS$STATUS ,
                    sq.SHIP_IND ,
                    sq.RECV_IND ,
                    sq.SALES_IND ,
                    sq.LOC_TYPE ,
                    sq.LOCATION ,
                    sq.CLOSE_DATE ,
                    svc_SCX_col(i).CREATE_ID ,
                    svc_SCX_col(i).CREATE_DATETIME ,
                    svc_SCX_col(i).LAST_UPD_ID ,
                    svc_SCX_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            
            WRITE_S9T_ERROR( I_file_id,
                            SCX_sheet,
                            svc_scx_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_SCX;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message IN OUT rtk_errors.rtk_text%TYPE,
                      O_error_COUNT   IN OUT NUMBER,
                      I_file_id       IN     s9t_folder.file_id%TYPE,
                      I_process_id    IN     NUMBER )
return BOOLEAN IS
   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);

   L_program         VARCHAR2(64) := 'CORESVC_COMPANY_CLOSED.PROCESS_S9T';
   L_file            s9t_file;
   L_sheets          s9t_pkg.names_map_typ;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
   MAX_CHAR      EXCEPTION;
   PRAGMA        EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   commit;
   s9t_pkg.ODS2OBJ(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if s9t_pkg.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        true) = FALSE   then
      return FALSE;
   end if;

   s9t_pkg.SAVE_OBJ(L_file);
   if s9t_pkg.VALIDATE_TEMPLATE(I_file_id) = false then
   WRITE_S9T_ERROR( I_file_id,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                   'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_COMPANY_CLOSED(I_file_id,
                                 I_process_id);
      PROCESS_S9T_COMPANY_CLOSED_TL(I_file_id,
                                   I_process_id);
      PROCESS_S9T_SCX(I_file_id,
                      I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   commit;

   return TRUE;
EXCEPTION

   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values lp_s9t_errors_tab(i);
         update svc_process_tracker
            set status  = 'PE',
                file_id = I_file_id
          where process_id = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END process_s9t;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPANY_CLOSED_INS(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_company_closed_temp_rec   IN      COMPANY_CLOSED%ROWTYPE )
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_COMPANY_CLOSED.EXEC_COMPANY_CLOSED_INS';
   L_delete_choise  BOOLEAN                           := TRUE;
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED';

BEGIN

   insert into company_closed
        values I_company_closed_temp_rec;
   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPANY_CLOSED_INS;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION  EXEC_COMPANY_CLOSED_UPD(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_company_closed_temp_rec IN     COMPANY_CLOSED%ROWTYPE )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                      :='CORESVC_COMPANY_CLOSED.EXEC_COMPHEAD_UPD';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,-54);
   L_delete_choise   BOOLEAN                           := TRUE;
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED';

   cursor C_COMPANY_CLOSED_LOCK is
      select 'X'
        from COMPANY_CLOSED
       where CLOSE_DATE =I_company_closed_temp_rec.CLOSE_DATE
       for update nowait;
BEGIN

   open C_COMPANY_CLOSED_LOCK;
   close C_COMPANY_CLOSED_LOCK;

   update COMPANY_CLOSED
      set row = I_company_closed_temp_rec
    where CLOSE_DATE =I_company_closed_temp_rec.CLOSE_DATE;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'COMPANY_CLOSED',
                                                                I_company_closed_temp_rec.CLOSE_DATE,
                                                                NULL);
      return FALSE;

   when OTHERS THEN
      if C_COMPANY_CLOSED_LOCK%ISOPEN then
         close C_COMPANY_CLOSED_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPANY_CLOSED_UPD;
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION  EXEC_COMPANY_CLOSED_DEL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_company_closed_temp_rec IN     COMPANY_CLOSED%ROWTYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      :='CORESVC_COMPANY_CLOSED.EXEC_COMPANY_CLOSED_DEL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED';

   cursor C_COMPANY_CLOSED_LOCK is
      select 'X'
        from COMPANY_CLOSED
       where CLOSE_DATE = I_company_closed_temp_rec.CLOSE_DATE
       for update nowait;

   cursor C_COMPANY_CLOSED_TL_LOCK is
      select 'X'
        from COMPANY_CLOSED_TL
       where CLOSE_DATE = I_company_closed_temp_rec.CLOSE_DATE
       for update nowait;

BEGIN

   open C_COMPANY_CLOSED_TL_LOCK;
   close C_COMPANY_CLOSED_TL_LOCK;

   open C_COMPANY_CLOSED_LOCK;
   close C_COMPANY_CLOSED_LOCK;

   delete
     from company_closed_tl
    where close_date = I_company_closed_temp_rec.close_date;

   delete
     from company_closed
    where close_date = I_company_closed_temp_rec.close_date;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'COMPANY_CLOSED',
                                                                I_company_closed_temp_rec.CLOSE_DATE,
                                                                NULL);
      return FALSE;

   when OTHERS THEN
      if C_COMPANY_CLOSED_TL_LOCK%ISOPEN then
         close C_COMPANY_CLOSED_LOCK;
      end if;
      if C_COMPANY_CLOSED_TL_LOCK%ISOPEN then
         close C_COMPANY_CLOSED_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPANY_CLOSED_DEL;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_COMPANY_CLOSED(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error          IN OUT  BOOLEAN,
                                    I_rec            IN      C_SVC_COMPANY_CLOSED%ROWTYPE )
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_COMPANY_CLOSED.PROCESS_VAL_COMPANY_CLOSED';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED';

BEGIN

   if I_rec.action = action_del
      and I_rec.PK_COMPANY_CLOSED_rid is NOT NULL
      and I_rec.close_date is NOT NULL   then

      if CLOSE_SQL.DELETE_EXCEP(O_error_message,
                                I_rec.close_date ) = FALSE  then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'CLOSE_DATE',
                      O_error_message);
         O_error := TRUE;
      else
          update svc_company_closed_tl 
             set process$status ='P'
           where close_date = I_rec.close_date;

         update svc_company_closed_excep 
            set process$status ='P'
          where close_date = I_rec.close_date;
      end if;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_COMPANY_CLOSED;
------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMPANY_CLOSED(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id    IN      SVC_COMPANY_CLOSED.PROCESS_ID%TYPE,
                                I_chunk_id      IN      SVC_COMPANY_CLOSED.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                       BOOLEAN;
   L_process_error               BOOLEAN                           := FALSE;
   L_company_closed_temp_rec  COMPANY_CLOSED%ROWTYPE;
   L_table                       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED';

BEGIN
   FOR rec IN C_SVC_COMPANY_CLOSED(I_process_id,I_chunk_id)
   LOOP
      L_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new, action_mod, action_del)   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_COMPANY_CLOSED_rid is NOT NULL   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'CLOSE_DATE',
                     'CLOSE_DATE_EXISTS');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod, action_del)
         and rec.close_date is NOT NULL
         and rec.PK_COMPANY_CLOSED_rid is NULL   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'CLOSE_DATE',
                     'NO_RECORD');
         L_error :=TRUE;
      END IF;

      if NOT( rec.close_date  is NOT NULL ) then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'CLOSE_DATE',
                     'ENTER_CLOSE_DATE');
         L_error :=TRUE;
      end if;

      if PROCESS_VAL_COMPANY_CLOSED(O_error_message,
                                    L_error,
                                    rec) = FALSE   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'CLOSE_DATE',
                      O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_company_closed_temp_rec.close_date := rec.close_date;
         L_company_closed_temp_rec.close_desc := rec.close_desc;

         if rec.action = action_new then
            if(EXEC_COMPANY_CLOSED_INS(O_error_message,
                                       L_company_closed_temp_rec) = FALSE )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod   then
            if(EXEC_COMPANY_CLOSED_UPD(O_error_message,
                                       L_company_closed_temp_rec ) = FALSE )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                           rec.row_seq,
                           NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del   then
            if(EXEC_COMPANY_CLOSED_DEL(O_error_message,
                                       L_company_closed_temp_rec ) = FALSE )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;


      end if;
   end loop;
   return TRUE;
EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPANY_CLOSED.process',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COMPANY_CLOSED;
-------------------------------------------------------------------------------
FUNCTION EXEC_COMPANY_CLOSED_TL_INS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cctl_ins_tab   IN     CCTL_REC_TAB)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_COMPANY_CLOSED.EXEC_COMPANY_CLOSED_TL_INS';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,-54);
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPANY_CLOSED_TL';

BEGIN
   if I_cctl_ins_tab is NOT NULL and I_cctl_ins_tab.count > 0 then

      FORALL i IN 1..I_cctl_ins_tab.COUNT()
         insert into company_closed_tl(lang,
                                       close_date,
                                       close_desc,
                                       create_datetime,
                                       create_id,
                                       last_update_datetime,
                                       last_update_id)
              values(I_cctl_ins_tab(i).lang,
                     I_cctl_ins_tab(i).close_date,
                     I_cctl_ins_tab(i).close_desc,
                     I_cctl_ins_tab(i).create_datetime,
                     I_cctl_ins_tab(i).create_id,
                     I_cctl_ins_tab(i).last_update_datetime,
                     I_cctl_ins_tab(i).last_update_id);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPANY_CLOSED_TL_INS;
-------------------------------------------------------------------------------
FUNCTION EXEC_COMPANY_CLOSED_TL_UPD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cctl_upd_tab      IN       CCTL_REC_TAB,
                                    I_cctl_tl_upd_rst   IN       ROW_SEQ_TAB,
                                    I_process_id        IN       SVC_COMPANY_CLOSED_TL.PROCESS_ID%TYPE,
                                    I_chunk_id          IN       SVC_COMPANY_CLOSED_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_COMPANY_CLOSED.EXEC_COMPANY_CLOSED_TL_UPD';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,-54);
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPANY_CLOSED_TL';
   L_key_val1        VARCHAR2(30) := NULL;
   L_key_val2        VARCHAR2(50) := NULL;

   cursor C_COMPANY_CLOSED_TL_LOCK(I_close_date  DATE,
                                   I_lang        LANG.LANG%TYPE) is
      select 'x'
        from company_closed_tl
       where close_date = I_close_date
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cctl_upd_tab is NOT NULL and I_cctl_upd_tab.count > 0 then
      for i in I_cctl_upd_tab.FIRST..I_cctl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_cctl_upd_tab(i).lang);
            L_key_val2 := 'Close Date: '||to_char(I_cctl_upd_tab(i).close_date);
            open C_COMPANY_CLOSED_TL_LOCK(I_cctl_upd_tab(i).close_date,
                                          I_cctl_upd_tab(i).lang);
            close C_COMPANY_CLOSED_TL_LOCK;
            update company_closed_tl
               set close_desc = I_cctl_upd_tab(i).close_desc,
                   last_update_id = I_cctl_upd_tab(i).last_update_id,
                   last_update_datetime = I_cctl_upd_tab(i).last_update_datetime
             where lang = I_cctl_upd_tab(i).lang
               and close_date = I_cctl_upd_tab(i).close_date;
         EXCEPTION
            when RECORD_LOCKED then     
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     'COMPANY_CLOSED_TL',
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_cctl_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS THEN
      if C_COMPANY_CLOSED_TL_LOCK%ISOPEN then
         close C_COMPANY_CLOSED_TL_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPANY_CLOSED_TL_UPD;
-------------------------------------------------------------------------------
FUNCTION EXEC_COMPANY_CLOSED_TL_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cctl_del_tab      IN       CCTL_REC_TAB,
                                    I_cctl_tl_del_rst   IN       ROW_SEQ_TAB,
                                    I_process_id        IN       SVC_COMPANY_CLOSED_TL.PROCESS_ID%TYPE,
                                    I_chunk_id          IN       SVC_COMPANY_CLOSED_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_COMPANY_CLOSED.EXEC_COMPANY_CLOSED_TL_DEL';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,-54);
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPANY_CLOSED_TL';
   L_key_val1        VARCHAR2(30) := NULL;
   L_key_val2        VARCHAR2(50) := NULL;

   cursor C_COMPANY_CLOSED_TL_LOCK(I_close_date  DATE,
                                   I_lang        LANG.LANG%TYPE) is
      select 'x'
        from company_closed_tl
       where close_date = I_close_date
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cctl_del_tab is NOT NULL and I_cctl_del_tab.count > 0 then
      for i in I_cctl_del_tab.FIRST..I_cctl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_cctl_del_tab(i).lang);
            L_key_val2 := 'Close Date: '||to_char(I_cctl_del_tab(i).close_date);
            open C_COMPANY_CLOSED_TL_LOCK(I_cctl_del_tab(i).close_date,
                                          I_cctl_del_tab(i).lang);
            close C_COMPANY_CLOSED_TL_LOCK;

            delete company_closed_tl
             where lang = I_cctl_del_tab(i).lang
               and close_date = I_cctl_del_tab(i).close_date;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     'COMPANY_CLOSED_TL',
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_cctl_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS THEN
      if C_COMPANY_CLOSED_TL_LOCK%ISOPEN then
         close C_COMPANY_CLOSED_TL_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMPANY_CLOSED_TL_DEL;
-------------------------------------------------------------------------------
FUNCTION PROCESS_COMPANY_CLOSED_TL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id    IN      SVC_COMPANY_CLOSED.PROCESS_ID%TYPE,
                                   I_chunk_id      IN      SVC_COMPANY_CLOSED.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                       BOOLEAN;
   L_process_error               BOOLEAN                           := FALSE;
   L_company_closed_tl_temp_rec  COMPANY_CLOSED_TL%ROWTYPE;
   L_table                       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPANY_CLOSED_TL';
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMPANY_CLOSED_TL';
   L_cctl_tl_upd_rst             ROW_SEQ_TAB;
   L_cctl_tl_del_rst             ROW_SEQ_TAB;

   cursor C_SVC_COMPANY_CLOSED_TL(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select pk_company_closed_tl.rowid as pk_company_closed_tl_rid,
             fk_company_closed.rowid as fk_company_closed_rid,
             fk_lang.rowid as fk_lang_rid,
             st.lang,
             st.close_desc,
             st.close_date,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_company_closed_tl  st,
             company_closed      fk_company_closed,
             company_closed_tl   pk_company_closed_tl,
             lang              fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.close_date  =  fk_company_closed.close_date (+)
         and st.lang        =  pk_company_closed_tl.lang (+)
         and st.close_date  =  pk_company_closed_tl.close_date (+)
         and st.lang        =  fk_lang.lang (+)
         and (st.process$status = 'N' or st.process$status  is NULL);

   TYPE SVC_CCTL is TABLE OF C_SVC_COMPANY_CLOSED_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_company_closed_tl_tab       SVC_CCTL;

   L_cctl_ins_tab         CCTL_rec_tab         := NEW CCTL_rec_tab();
   L_cctl_upd_tab         CCTL_rec_tab         := NEW CCTL_rec_tab();
   L_cctl_del_tab         CCTL_rec_tab         := NEW CCTL_rec_tab();

BEGIN

   if C_SVC_COMPANY_CLOSED_TL%ISOPEN then
      close C_SVC_COMPANY_CLOSED_TL;
   end if;
   ---
   open C_SVC_COMPANY_CLOSED_TL(I_process_id,I_chunk_id);
   LOOP
      fetch C_SVC_COMPANY_CLOSED_TL bulk collect into L_svc_company_closed_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_company_closed_tl_tab.COUNT > 0 then
         FOR i in L_svc_company_closed_tl_tab.FIRST..L_svc_company_closed_tl_tab.LAST
         LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_company_closed_tl_tab(i).action is NULL
               or L_svc_company_closed_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_company_closed_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_company_closed_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_company_closed_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key exists
            if L_svc_company_closed_tl_tab(i).action = action_new
               and L_svc_company_closed_tl_tab(i).pk_company_closed_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_company_closed_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_company_closed_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_company_closed_tl_tab(i).lang is NOT NULL
               and L_svc_company_closed_tl_tab(i).close_date is NOT NULL
               and L_svc_company_closed_tl_tab(i).pk_company_closed_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_company_closed_tl_tab(i).row_seq,
                           'CLOSE_DATE',
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_company_closed_tl_tab(i).action = action_new
               and L_svc_company_closed_tl_tab(i).close_date is NOT NULL
               and L_svc_company_closed_tl_tab(i).fk_company_closed_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_company_closed_tl_tab(i).row_seq,
                            NULL,
                           'INVALID_CLOSE_DATE');
               L_error :=TRUE;
            end if;

            if L_svc_company_closed_tl_tab(i).action = action_new
               and L_svc_company_closed_tl_tab(i).lang is NOT NULL
               and L_svc_company_closed_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_company_closed_tl_tab(i).row_seq,
                            NULL,
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_company_closed_tl_tab(i).action IN (action_new, action_mod) and L_svc_company_closed_tl_tab(i).close_desc is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_company_closed_tl_tab(i).row_seq,
                           'CLOSE_DESC',
                           'ENTER_DESC');
               L_error :=TRUE;
            end if;

            if L_svc_company_closed_tl_tab(i).close_date is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_company_closed_tl_tab(i).row_seq,
                           'CLOSE_DATE',
                           'CLOSE_DATE_EXIST');
               L_error :=TRUE;
            end if;

            if L_svc_company_closed_tl_tab(i).lang is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_company_closed_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_company_closed_tl_temp_rec.lang := L_svc_company_closed_tl_tab(i).lang;
               L_company_closed_tl_temp_rec.close_date := L_svc_company_closed_tl_tab(i).close_date;
               L_company_closed_tl_temp_rec.close_desc := L_svc_company_closed_tl_tab(i).close_desc;
               L_company_closed_tl_temp_rec.create_datetime := SYSDATE;
               L_company_closed_tl_temp_rec.create_id := GET_USER;
               L_company_closed_tl_temp_rec.last_update_datetime := SYSDATE;
               L_company_closed_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_company_closed_tl_tab(i).action = action_new then
                  L_cctl_ins_tab.extend;
                  L_cctl_ins_tab(L_cctl_ins_tab.count()) := L_company_closed_tl_temp_rec;
               end if;

               if L_svc_company_closed_tl_tab(i).action = action_mod then
                  L_cctl_upd_tab.extend;
                  L_cctl_upd_tab(L_cctl_upd_tab.count()) := L_company_closed_tl_temp_rec;
                  L_cctl_tl_upd_rst(L_cctl_upd_tab.count()) := L_svc_company_closed_tl_tab(i).row_seq;
               end if;

               if L_svc_company_closed_tl_tab(i).action = action_del then
                  L_cctl_del_tab.extend;
                  L_cctl_del_tab(L_cctl_del_tab.count()) := L_company_closed_tl_temp_rec;
                  L_cctl_tl_del_rst(L_cctl_del_tab.count()) := L_svc_company_closed_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_COMPANY_CLOSED_TL%NOTFOUND;
   END LOOP;
   close C_SVC_COMPANY_CLOSED_TL;

   if EXEC_COMPANY_CLOSED_TL_INS(O_error_message,
                                 L_cctl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_COMPANY_CLOSED_TL_UPD(O_error_message,
                                 L_cctl_upd_tab,
                                 L_cctl_tl_upd_rst,
                                 I_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_COMPANY_CLOSED_TL_DEL(O_error_message,
                                 L_cctl_del_tab,
                                 L_cctl_tl_del_rst,
                                 I_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      if C_SVC_COMPANY_CLOSED_TL%ISOPEN then
         close C_SVC_COMPANY_CLOSED_TL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPANY_CLOSED.PROCESS_COMPANY_CLOSED_TL',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_COMPANY_CLOSED_TL;
----------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_SCX_INS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_scx_temp_rec   IN      COMPANY_CLOSED_EXCEP%ROWTYPE )
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      :='CORESVC_COMPANY_CLOSED_EXCP.EXEC_SCX_INS';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED_EXCEP';
BEGIN

  insert into COMPANY_CLOSED_EXCEP 
       values I_scx_temp_rec;

   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SCX_INS;
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_SCX_UPD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_scx_temp_rec   IN      COMPANY_CLOSED_EXCEP%ROWTYPE )
RETURN BOOLEAN IS
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   L_program     VARCHAR2(64)                      :='CORESVC_COMPANY_CLOSED.EXEC_SCX_UPD';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED_EXCEP';

   cursor C_COMPANY_CLOSED_EXCEP_LOCK is
      select 'X'
        from COMPANY_CLOSED_EXCEP
       where LOCATION   = I_scx_temp_rec.LOCATION
         and CLOSE_DATE = I_scx_temp_rec.CLOSE_DATE
       for update nowait;
BEGIN

   open C_COMPANY_CLOSED_EXCEP_LOCK;
   close C_COMPANY_CLOSED_EXCEP_LOCK;

   update COMPANY_CLOSED_EXCEP
      set row        = I_scx_temp_rec 
    where LOCATION   = I_scx_temp_rec.LOCATION
      and CLOSE_DATE = I_scx_temp_rec.CLOSE_DATE;
      

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'COMPANY_CLOSED_EXCEP',
                                                                I_scx_temp_rec.LOCATION,
                                                                I_scx_temp_rec.CLOSE_DATE);
      return FALSE;

   when OTHERS then
      if C_COMPANY_CLOSED_EXCEP_LOCK%ISOPEN then
         close C_COMPANY_CLOSED_EXCEP_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SCX_UPD;
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_SCX_DEL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE ,
                      I_scx_temp_rec   IN      COMPANY_CLOSED_EXCEP%ROWTYPE )
RETURN BOOLEAN IS
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   L_program      VARCHAR2(64)                      :='CORESVC_COMPANY_CLOSED.EXEC_SCX_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED_EXCEP';
   cursor C_COMPANY_CLOSED_EXCEP_LOCK is
      select 'X'
        from COMPANY_CLOSED_EXCEP
       where LOCATION   = I_scx_temp_rec.LOCATION
         and CLOSE_DATE = I_scx_temp_rec.CLOSE_DATE
       for update nowait;
BEGIN

   open C_COMPANY_CLOSED_EXCEP_LOCK;
   close C_COMPANY_CLOSED_EXCEP_LOCK;

   delete from COMPANY_CLOSED_EXCEP 
    where LOCATION   = I_scx_temp_rec.LOCATION
      and CLOSE_DATE = I_scx_temp_rec.CLOSE_DATE;
      
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'COMPANY_CLOSED_EXCEP',
                                                                I_scx_temp_rec.LOCATION,
                                                                I_scx_temp_rec.CLOSE_DATE);
      return FALSE;

   when OTHERS then
      if C_COMPANY_CLOSED_EXCEP_LOCK%ISOPEN then
         close C_COMPANY_CLOSED_EXCEP_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SCX_DEL;
-------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_VALUE(O_error_message  IN OUT  VARCHAR2,
                         O_error          IN OUT  BOOLEAN,
                         O_exist          IN OUT  BOOLEAN,
                         O_rec            IN OUT  C_SVC_SCX%ROWTYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)  :='CORESVC_COMPANY_CLOSED.CHECK_LOC_VALUE';   
   L_company_exist   BOOLEAN;
   L_second_value    VARCHAR2(20)  := NULL;   
   L_loc_type        VARCHAR2(5)   := NULL;  
   L_group_desc      VARCHAR2(255);          
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED_EXCEP' ;
BEGIN
   if O_rec.loc_type = 'W' then
      L_loc_type := 'PW';
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION_TYPE( O_error_message,                                  
                                                         O_exist,                                          
                                                         L_group_desc,                                     
                                                         L_second_value,                                    
                                                         L_loc_type,                                        
                                                         O_rec.location )  = FALSE then
      
         WRITE_ERROR( O_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      O_rec.chunk_id,
                      L_table,
                      O_rec.row_seq,
                      'LOCATION_TYPE',
                      O_error_message); 
         O_error := TRUE;

      end if;

      if O_exist = TRUE then
         WRITE_ERROR( O_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      O_rec.chunk_id,
                      L_table,
                      O_rec.row_seq,
                     'LOCATION',
                     'INV_VWH' ); 
         O_error := TRUE;

      elsif O_error_message <>'@0INV_PHYSICAL_WH' then 
            WRITE_ERROR( O_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         O_rec.chunk_id,
                         L_table,
                         O_rec.row_seq,
                        'LOCATION',
                        'NO_VIS_HIERARCHY');
            O_error := TRUE;
      end if;
   end if;

   if O_rec.loc_type = 'S' then
      if FILTER_LOV_VALIDATE_SQL.COMPANY_STORE( O_error_message,
                                                L_company_exist,
                                                L_group_desc,
                                                O_rec.location) = FALSE then
         if O_error_message <> '@0INV_STORE' then
            O_error := TRUE;
            WRITE_ERROR( O_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         O_rec.chunk_id,
                         L_table,
                         O_rec.row_seq,
                        'LOCATION',
                        'NO_VIS_HIERARCHY'); 
         end if;
      end if;

      if L_company_exist   = FALSE then  
         L_group_desc    := NULL;
         O_exist         := FALSE;
         O_error_message :='MUST_BE_COMP_STORE'; 
         O_error         := TRUE;
         WRITE_ERROR( O_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      O_rec.chunk_id,
                      L_table,
                      O_rec.row_seq,
                     'LOCATION',
                      O_error_message );
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                              SQLERRM, 
                                              L_program, 
                                              TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_LOC_VALUE;
-------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_SCX(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error         IN OUT   BOOLEAN,
                         O_rec           IN OUT   C_SVC_SCX%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      :='CORESVC_COMPANY_CLOSED.PROCESS_VAL_SCX';   
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='COMPANY_CLOSED_EXCEP';   
   L_status            BOOLEAN                           := FALSE;
   L_exist             BOOLEAN                           := NULL;       
   L_stockholding_ind  WH.STOCKHOLDING_IND%TYPE;
BEGIN
   if O_rec.sales_ind     = 'N'
      and O_rec.ship_ind  = 'N'
      and O_rec.recv_ind  = 'N'   then
      WRITE_ERROR( O_rec.process_id,
                   svc_admin_upld_er_seq.NEXTVAL,
                   O_rec.chunk_id,
                   L_table,
                   O_rec.row_seq,
                  'Sales,Receiving,Shipping',
                  'ENTER_LOC_ACTIVITY');
      O_error :=TRUE;
   end if;

   if ((O_rec.action = action_new 
      and O_rec.loc_type = 'W' )
      or (O_rec.action = action_mod
      and O_rec.loc_type = 'W'
      and O_rec.base_sales_ind <> NVL(O_rec.sales_ind,'-1')))   then
          -- Sales indicator for a warehouse cannot be 'Y'.
          if O_rec.sales_ind = 'Y'   then
             WRITE_ERROR( O_rec.process_id,
                          svc_admin_upld_er_seq.NEXTVAL,
                          O_rec.chunk_id,
                          L_table,
                          O_rec.row_seq,
                         'SALES_IND',
                         'INV_WH_CRIT'); 
             O_error :=TRUE;
          end if;
   end if;
      
   if ((O_rec.action = action_new
      and O_rec.location is NOT NULL 
      and O_rec.loc_type IN ('S','W'))                         
      or (O_rec.action = action_mod 
      and O_rec.location is NOT NULL
      and O_rec.loc_type IN ('S','W'))) then
      if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_error_message,
                                                L_stockholding_ind,
                                                O_rec.location,
                                                O_rec.loc_type )= FALSE   then
         O_error  := TRUE;
         L_status := TRUE;
         WRITE_ERROR( O_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      O_rec.chunk_id,
                      L_table,
                      O_rec.row_seq,
                      'LOCATION',
                      O_error_message);
      end if;

      if O_rec.action = action_new 
         and L_status = FALSE   then
         if CHECK_LOC_VALUE(O_error_message,
                            O_error,
                            L_exist,
                            O_rec ) = FALSE   then
            return FALSE;
         end if;
      end if;

      if L_stockholding_ind is NOT NULL
         and L_stockholding_ind = 'N'
         and O_rec.loc_type     = 'S'   then
         
         if O_rec.action = action_mod then
            if O_rec.pk_company_closed_excep_rid is NOT NULL then
               if O_rec.base_recv_ind <> NVL(O_rec.recv_ind,'-1') then
                  WRITE_ERROR( O_rec.process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               O_rec.chunk_id,
                               L_table,
                               O_rec.row_seq,
                               'RECV_IND',
                               'CANT_UPD_NSTOCK');
                  O_error :=TRUE;
               end if;
               if O_rec.base_ship_ind <> NVL(O_rec.ship_ind,'-1')   then
                  WRITE_ERROR( O_rec.process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               O_rec.chunk_id,
                               L_table,
                               O_rec.row_seq,
                               'SHIP_IND',
                               'CANT_UPD_NSTOCK');
                  O_error :=TRUE;
               end if;
            end if;
         elsif O_rec.action = action_new   then
            if O_rec.recv_ind = 'Y'   then
               WRITE_ERROR( O_rec.process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            O_rec.chunk_id,
                            L_table,
                            O_rec.row_seq,
                            'RECV_IND',
                            'NON_STOCK_Y');
                  O_error :=TRUE;
            end if;
            if O_rec.ship_ind = 'Y'   then
               WRITE_ERROR( O_rec.process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            O_rec.chunk_id,
                            L_table,
                            O_rec.row_seq,
                            'SHIP_IND',
                            'NON_STOCK_Y');
               O_error :=TRUE;
            end if;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_SCX;
-------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SCX( O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id    IN      SVC_COMPANY_CLOSED_EXCEP.PROCESS_ID%TYPE,
                      I_chunk_id      IN      SVC_COMPANY_CLOSED_EXCEP.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error             BOOLEAN;
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMPANY_CLOSED_EXCEP';
   L_process_error     BOOLEAN                           := FALSE;
   L_scx_temp_rec      COMPANY_CLOSED_EXCEP%ROWTYPE;

BEGIN
   
   FOR rec IN c_svc_SCX(I_process_id,I_chunk_id)
   LOOP
      L_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del)   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new 
         and rec.PK_COMPANY_CLOSED_EXCEP_rid is NOT NULL   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'Close Date, Location',
                     'DUP_RECORD');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del) 
         and rec.PK_COMPANY_CLOSED_EXCEP_rid is NULL 
         and rec.CLOSE_DATE  is NOT NULL 
         and rec.LOCATION    is NOT NULL then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'Close Date, Location',
                     'NO_RECORD');
         L_error :=TRUE;
      end if;

      if NOT( rec.CLOSE_DATE  is NOT NULL  )   then
              WRITE_ERROR( I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'CLOSE_DATE',
                           'MUST_ENTER_FIELD');
              L_error :=TRUE;
      end if;

      if NOT( rec.LOCATION  is NOT NULL )   then
              WRITE_ERROR( I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                          'LOCATION',
                          'MUST_ENTER_FIELD');
              L_error :=TRUE;
      end if;

      if rec.action    = action_new
         or rec.action = action_mod   then

         if rec.SCX_SCC_FK_rid is NULL 
            and rec.CLOSE_DATE  is NOT NULL   then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'CLOSE_DATE',
                        'INVALID_CLOSE_DATE');
            L_error :=TRUE;
         end if;

         if ( rec.action = action_new
            or (rec.action = action_mod
            and NVL(rec.base_ship_ind,'-2') <> NVL(rec.ship_ind,'-1') ))   then
            if rec.SHIP_IND is NULL
               or rec.SHIP_IND NOT IN ( 'Y','N' )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'SHIP_IND',
                           'INV_Y_N_IND');
               L_error :=TRUE;
            end if;
         end if;

         if (rec.action = action_new 
            or (rec.action = action_mod 
            and NVL(rec.base_recv_ind,'-2') <> NVL(rec.recv_ind,'-1'))) then
            if rec.RECV_IND is NULL 
               or rec.RECV_IND NOT IN ( 'Y','N' )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'RECV_IND',
                           'INV_Y_N_IND');
               L_error :=TRUE;
            end if;
         end if;

         if (rec.action = action_new 
            or (rec.action = action_mod 
            and NVL(rec.base_sales_ind,'-2') <> NVL(rec.sales_ind,'-1'))) then 
            if rec.SALES_IND is NULL 
               or rec.SALES_IND NOT IN ( 'Y','N' )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'SALES_IND',
                           'INV_Y_N_IND');
               L_error :=TRUE;
            end if;
         end if;

         if (rec.action = action_new 
            or (rec.action = action_mod 
            and NVL(rec.base_loc_type,'-2') <> NVL(rec.loc_type,'-1'))) then
            if rec.LOC_TYPE is NULL 
               or rec.LOC_TYPE NOT IN ( 'S','W' )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'LOC_TYPE',
                           'INVALID_LOC_TYPE_S_W');
               L_error :=TRUE;
            end if;
         end if;

         if PROCESS_VAL_SCX(O_error_message,
                            L_error,
                            rec) = FALSE   then
             WRITE_ERROR( I_process_id,
                          svc_admin_upld_er_seq.NEXTVAL,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                          'Close Date, Location',
                          O_error_message);
             L_error := TRUE;
         end if;

      end if;

      if NOT L_error then
         L_scx_temp_rec.close_date := rec.close_date;
         L_scx_temp_rec.location   := rec.location;
         L_scx_temp_rec.loc_type   := rec.loc_type;
         L_scx_temp_rec.sales_ind  := rec.sales_ind;
         L_scx_temp_rec.recv_ind   := rec.recv_ind;
         L_scx_temp_rec.ship_ind   := rec.ship_ind;

         if rec.action = action_new   then
            if(EXEC_SCX_INS(O_error_message,
                            L_scx_temp_rec ) = FALSE ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod   then
            if(EXEC_SCX_UPD(O_error_message,
                            L_scx_temp_rec ) = FALSE ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del   then
            if(EXEC_SCX_DEL(O_error_message,
                            L_scx_temp_rec ) = FALSE ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   end loop;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPANY_CLOSED.PROCESS',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SCX;
----------------------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_COMPANY_CLOSED.PROCESS_ID%TYPE) IS

BEGIN
   delete
     from svc_company_closed
    where process_id= I_process_id;

   delete
     from svc_company_closed_tl
    where process_id= I_process_id;

   Delete 
     from svc_company_closed_excep 
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT  NUMBER,
                  I_process_id    IN      NUMBER,
                  I_chunk_id      IN      NUMBER )
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_COMPANY_CLOSED.PROCESS';
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_COMPANY_CLOSED(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE   then
      return FALSE;
   end if;

   if PROCESS_COMPANY_CLOSED_TL(O_error_message,
                                I_process_id,
                                I_chunk_id) = FALSE   then
      return FALSE;
   end if;

   if PROCESS_SCX(O_error_message,
                     I_process_id,
                     I_chunk_id)= FALSE   then
         return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;

END CORESVC_COMPANY_CLOSED;
--------------------------------------------------------------------------------------------------------------------------------------
/
