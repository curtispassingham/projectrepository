CREATE OR REPLACE PACKAGE BODY CORESVC_CUSTOMER_SEGMENTS AS

   cursor C_SVC_CUS_SMTS(I_process_id   NUMBER,
                         I_chunk_id     NUMBER) IS
      select pk_customer_segments.rowid                 as pk_customer_segments_rid,
             csg_cst_fk.rowid                           as csg_cst_fk_rid,
             pk_customer_segments.customer_segment_type as old_customer_segment_type,
             st.customer_segment_id,
             st.customer_segment_desc,
             upper(st.customer_segment_type)            as customer_segment_type,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             upper(st.action)                           as action,
             st.process$status
        from svc_customer_segments  st,
             customer_segments      pk_customer_segments,
             customer_segment_types csg_cst_fk
       where st.process_id                   = I_process_id
         and st.chunk_id                     = I_chunk_id
         and st.customer_segment_id          = pk_customer_segments.customer_segment_id (+)
         and upper(st.customer_segment_type) = upper(csg_cst_fk.customer_segment_type (+));

   cursor C_SVC_CUS_SMT_TY(I_process_id   NUMBER,
                           I_chunk_id     NUMBER) IS
     select pk_customer_segment_types.rowid  as pk_customer_segment_types_rid,
            st.customer_segment_type_desc,
            upper(st.customer_segment_type)  as customer_segment_type,
            st.process_id,
            st.chunk_id,
            st.row_seq,
            upper(st.action)                 as action,
            st.process$status
       from svc_customer_segment_types st,
            customer_segment_types     pk_customer_segment_types
      where st.process_id                    = I_process_id
        and st.chunk_id                     = I_chunk_id
        and UPPER(st.customer_segment_type) = UPPER(pk_customer_segment_types.customer_segment_type (+))
        and NVL(st.action, 'X') <> CORESVC_CUSTOMER_SEGMENTS.action_del ;
        
   cursor C_SVC_CUS_SMT_TY_DEL(I_process_id   NUMBER,
                               I_chunk_id     NUMBER) IS
     select pk_customer_segment_types.rowid  as pk_customer_segment_types_rid,
            st.customer_segment_type_desc,
            upper(st.customer_segment_type)  as customer_segment_type,
            st.process_id,
            st.chunk_id,
            st.row_seq,
            upper(st.action)                 as action,
            st.process$status
       from svc_customer_segment_types st,
            customer_segment_types     pk_customer_segment_types
      where st.process_id                    = I_process_id
        and st.chunk_id                     = I_chunk_id
        and UPPER(st.customer_segment_type) = UPPER(pk_customer_segment_types.customer_segment_type (+))
        and NVL(st.action, 'X') = CORESVC_CUSTOMER_SEGMENTS.action_del ;

   Type errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   Lp_errors_tab errors_tab_typ;
   Type s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   Lp_s9t_errors_tab s9t_errors_tab_typ;

   Type CUSTOMER_SEGMENTS_TL_TAB IS TABLE OF CUSTOMER_SEGMENTS_TL%ROWTYPE;
   Type CUSTOMER_SEGMENT_TYPES_TL_TAB IS TABLE OF CUSTOMER_SEGMENT_TYPES_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang    LANG.LANG%TYPE;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet     IN   VARCHAR2,
                           I_row_seq   IN   NUMBER,
                           I_col       IN   VARCHAR2,
                           I_sqlcode   IN   NUMBER,
                           I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   Lp_s9t_errors_tab.extend();
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).FILE_ID              := I_file_id;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_SEQ_NO         := s9t_errors_seq.nextval;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).TEMPLATE_KEY         := template_key;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).WKSHT_KEY            := I_sheet;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).COLUMN_KEY           := I_col;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_ID            := GET_USER;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_DATETIME      := SYSDATE;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_ID       := GET_USER;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;

------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;

---------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets             S9T_PKG.NAMES_MAP_TYP;
   CUS_SMTS_cols        S9T_PKG.NAMES_MAP_TYP;
   CUS_SMTS_TL_cols     S9T_PKG.NAMES_MAP_TYP;
   CUS_SMT_TY_cols      S9T_PKG.NAMES_MAP_TYP;
   CUS_SMT_TY_tl_cols   S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                        := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   CUS_SMTS_cols                   := S9T_PKG.GET_COL_NAMES(I_file_id,CUS_SMTS_sheet);
   cus_smts$action                 := cus_smts_cols('ACTION');
   cus_smts$customer_segment_id    := cus_smts_cols('CUSTOMER_SEGMENT_ID');
   cus_smts$customer_segment_desc  := cus_smts_cols('CUSTOMER_SEGMENT_DESC');
   cus_smts$customer_segment_type  := cus_smts_cols('CUSTOMER_SEGMENT_TYPE');

   CUS_SMTS_tl_cols                := S9T_PKG.GET_COL_NAMES(I_file_id,CUS_SMTS_TL_sheet);
   cus_smts_tl$action              := CUS_SMTS_tl_cols('ACTION');
   cus_smts_tl$lang                := CUS_SMTS_tl_cols('LANG');
   cus_smts_tl$cust_segment_id     := CUS_SMTS_tl_cols('CUSTOMER_SEGMENT_ID');
   cus_smts_tl$cust_segment_desc   := CUS_SMTS_tl_cols('CUSTOMER_SEGMENT_DESC');

   cus_smt_ty_cols                 :=S9T_PKG.GET_COL_NAMES(I_file_id,CUS_SMT_TY_sheet);
   cus_smt_ty$action               := cus_smt_ty_cols('ACTION');
   cus_smt_ty$cus_smt_ty           := cus_smt_ty_cols('CUSTOMER_SEGMENT_TYPE');
   cus_smt_ty$cus_smt_ty_des       := cus_smt_ty_cols('CUSTOMER_SEGMENT_TYPE_DESC');

   cus_smt_ty_TL_cols              :=S9T_PKG.GET_COL_NAMES(I_file_id,CUS_SMT_TY_TL_sheet);
   cus_smt_ty_TL$action            := cus_smt_ty_tl_cols('ACTION');
   cus_smt_ty_TL$lang              := cus_smt_ty_tl_cols('LANG');
   cus_smt_ty_TL$cus_smt_ty        := cus_smt_ty_tl_cols('CUSTOMER_SEGMENT_TYPE');
   cus_smt_ty_TL$cus_smt_ty_des    := cus_smt_ty_tl_cols('CUSTOMER_SEGMENT_TYPE_DESC');

END POPULATE_NAMES;

----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CUS_SMTS(I_file_id   IN   NUMBER)IS
BEGIN
  insert into TABLE
       (select ss.s9t_rows
          from s9t_folder sf,
               TABLE(sf.s9t_file_obj.sheets) ss
         where sf.file_id  = I_file_id
           and ss.sheet_name = CUS_SMTS_sheet)
   select s9t_row(s9t_cells(CORESVC_CUSTOMER_SEGMENTS.action_mod,
                            customer_segment_id,
                            customer_segment_desc,
                            customer_segment_type ))
  from customer_segments ;
END POPULATE_CUS_SMTS;
---------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CUS_SMT_TY(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE
         (select ss.s9t_rows
            from s9t_folder sf,
                 TABLE(sf.s9t_file_obj.sheets) ss
           where sf.file_id  = I_file_id
            and ss.sheet_name = CUS_SMT_TY_sheet)
    select s9t_row(s9t_cells(CORESVC_CUSTOMER_SEGMENTS.action_mod,
                             customer_segment_type,
                             customer_segment_type_desc
                           ))
    from customer_segment_types ;
END POPULATE_CUS_SMT_TY;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CUS_SMTS_TL(I_file_id   IN   NUMBER)IS
BEGIN
  insert into TABLE
       (select ss.s9t_rows
          from s9t_folder sf,
               TABLE(sf.s9t_file_obj.sheets) ss
         where sf.file_id  = I_file_id
           and ss.sheet_name = CUS_SMTS_TL_sheet)
   select s9t_row(s9t_cells(CORESVC_CUSTOMER_SEGMENTS.action_mod,
                            lang,
                            customer_segment_id,
                            customer_segment_desc))
  from customer_segments_tl ;
END POPULATE_CUS_SMTS_TL;
---------------------------------------------------------------------------------------------
PROCEDURE POPULATE_CUS_SMT_TY_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE
         (select ss.s9t_rows
            from s9t_folder sf,
                 TABLE(sf.s9t_file_obj.sheets) ss
           where sf.file_id  = I_file_id
            and ss.sheet_name = CUS_SMT_TY_TL_sheet)
    select s9t_row(s9t_cells(CORESVC_CUSTOMER_SEGMENTS.action_mod,
                             lang,
                             customer_segment_type,
                             customer_segment_type_desc
                           ))
    from customer_segment_types_tl ;
END POPULATE_CUS_SMT_TY_TL;
--------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        S9T_FILE;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.nextval;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||sysdate||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(cus_smts_sheet);
   L_file.sheets(L_file.get_sheet_index(CUS_SMTS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                      'CUSTOMER_SEGMENT_ID',
                                                                                      'CUSTOMER_SEGMENT_DESC',
                                                                                      'CUSTOMER_SEGMENT_TYPE');

   L_file.add_sheet(cus_smts_tl_sheet);
   L_file.sheets(L_file.get_sheet_index(cus_smts_tl_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                         'LANG',
                                                                                         'CUSTOMER_SEGMENT_ID',
                                                                                         'CUSTOMER_SEGMENT_DESC');

   L_file.add_sheet(CUS_SMT_TY_sheet);
   L_file.sheets(L_file.get_sheet_index(CUS_SMT_TY_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'CUSTOMER_SEGMENT_TYPE',
                                                                                        'CUSTOMER_SEGMENT_TYPE_DESC'
                                                                                       );

   L_file.add_sheet(CUS_SMT_TY_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(CUS_SMT_TY_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                           'LANG',
                                                                                           'CUSTOMER_SEGMENT_TYPE',
                                                                                           'CUSTOMER_SEGMENT_TYPE_DESC'
                                                                                          );
   S9T_PKG.SAVE_OBJ(L_FILE);
END INIT_S9T;

-----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_program   VARCHAR2(64):='CORESVC_CUSTOMER_SEGMENTS.CREATE_S9T';
   L_file      S9T_FILE;

BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      populate_CUS_SMT_TY(O_file_id);
      populate_CUS_SMT_TY_TL(O_file_id);
      populate_CUS_SMTS(O_file_id);
      populate_CUS_SMTS_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
     return FALSE;
END CREATE_S9T;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CUS_SMTS(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id   IN   SVC_CUSTOMER_SEGMENTS.PROCESS_ID%TYPE) IS

   Type svc_CUS_SMTS_col_typ IS TABLE OF SVC_CUSTOMER_SEGMENTS%ROWTYPE;
   L_temp_rec                  SVC_CUSTOMER_SEGMENTS%ROWTYPE;
   svc_CUS_SMTS_col            svc_CUS_SMTS_col_typ :=NEW svc_CUS_SMTS_col_typ();
   L_process_id                SVC_CUSTOMER_SEGMENTS.PROCESS_ID%TYPE;
   L_error                     BOOLEAN              :=FALSE;
   L_default_rec               SVC_CUSTOMER_SEGMENTS%ROWTYPE;

   cursor c_mandatory_ind IS
      select customer_segment_id_mi,
             customer_segment_desc_mi,
             customer_segment_type_mi,
             1 as dummy
        from(select column_key,
                    mandatory
               from s9t_tmpl_cols_def
              where template_key                               =  template_key
                and wksht_key                                   = 'CUSTOMER_SEGMENTS'
            ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN (
                                            'CUSTOMER_SEGMENT_ID'   as customer_segment_id,
                                            'CUSTOMER_SEGMENT_DESC' as customer_segment_desc,
                                            'CUSTOMER_SEGMENT_TYPE' as customer_segment_type,
                                            null as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CUSTOMER_SEGMENTS';
   L_pk_columns    VARCHAR2(255)  := 'Customer Segment ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN(select customer_segment_id_dv,
                     customer_segment_desc_dv,
                     UPPER(customer_segment_type_dv) as customer_segment_type_dv,
                     null as dummy
                from(select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key                                  =  template_key
                        and wksht_key                                     = 'CUSTOMER_SEGMENTS'
                     ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN (
                                        'CUSTOMER_SEGMENT_ID'   as customer_segment_id,
                                        'CUSTOMER_SEGMENT_DESC' as customer_segment_desc,
                                        'CUSTOMER_SEGMENT_TYPE' as customer_segment_type,
                                         NULL                   as dummy)
                              )
              )
   LOOP
      BEGIN
         L_default_rec.customer_segment_id := rec.customer_segment_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'CUSTOMER_SEGMENTS',
                             NULL,
                            'CUSTOMER_SEGMENT_ID',
                             NULL,
                             'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.customer_segment_desc := rec.customer_segment_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             'CUSTOMER_SEGMENTS',
                              NULL,
                             'CUSTOMER_SEGMENT_DESC',
                             NULL,
                             'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.customer_segment_type := rec.customer_segment_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             'CUSTOMER_SEGMENTS',
                              NULL,
                             'CUSTOMER_SEGMENT_TYPE',
                             NULL,
                             'INV_DEFAULT');
      END;
   END loop;
 --Get mandatory indicators
   open C_mandatory_ind;
   fetch C_mandatory_ind
      into L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN(select r.get_cell(cus_smts$action)                        as action,
                     r.get_cell(cus_smts$customer_segment_id)           as customer_segment_id,
                     r.get_cell(cus_smts$customer_segment_desc)         as customer_segment_desc,
                     UPPER(r.get_cell(cus_smts$customer_segment_type))  as customer_segment_type,
                     r.get_row_seq()                                    as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(CUS_SMTS_sheet)
             )
   loop
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := false;
      BEGIN
         L_temp_REC.ACTION := REC.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_id := rec.customer_segment_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_sheet,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_desc := rec.customer_segment_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_sheet,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_type := rec.customer_segment_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_sheet,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      if rec.action = CORESVC_CUSTOMER_SEGMENTS.action_new then
         L_temp_rec.customer_segment_id   := NVL( L_temp_rec.customer_segment_id,
                                              L_default_rec.customer_segment_id);
         L_temp_rec.customer_segment_desc := NVL( L_temp_rec.customer_segment_desc,
                                               L_default_rec.customer_segment_desc);
         L_temp_rec.customer_segment_type := NVL( L_temp_rec.customer_segment_type,
                                              L_default_rec.customer_segment_type);
      end if;
      if not (L_temp_rec.customer_segment_id is not NULL
              and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         CUS_SMTS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := true;
      end if;
      if NOT L_error then
         svc_cus_smts_col.extend();
         svc_cus_smts_col(svc_cus_smts_col.count()):=L_temp_rec;
      end if;
   END loop;
   BEGIN
      forall i IN 1..svc_cus_smts_col.count SAVE EXCEPTIONS
      Merge INTO svc_customer_segments st USING
         (select(case
                 when l_mi_rec.customer_segment_id_mi    = 'N'
                  and svc_cus_smts_col(i).action         = CORESVC_CUSTOMER_SEGMENTS.action_mod
                  and s1.customer_segment_id             IS NULL
                 then mt.customer_segment_id
                 else s1.customer_segment_id
                 end) as customer_segment_id,
                (case
                 when l_mi_rec.customer_segment_desc_mi    = 'N'
                  and svc_cus_smts_col(i).action           = CORESVC_CUSTOMER_SEGMENTS.action_mod
                  and s1.customer_segment_desc             IS NULL
                 then mt.customer_segment_desc
                 else s1.customer_segment_desc
                 end) as customer_segment_desc,
                (case
                 when l_mi_rec.customer_segment_type_mi    = 'N'
                  and svc_cus_smts_col(i).action           = CORESVC_CUSTOMER_SEGMENTS.action_mod
                  and s1.customer_segment_type             IS NULL
                 then mt.customer_segment_type
                 else s1.customer_segment_type
                  end) as customer_segment_type,
                 null as dummy
            from (select svc_cus_smts_col(i).customer_segment_id   as customer_segment_id,
                         svc_cus_smts_col(i).customer_segment_desc as customer_segment_desc,
                         svc_cus_smts_col(i).customer_segment_type as customer_segment_type,
                         null as dummy
                    from dual
                 ) s1,
                   customer_segments mt
           where mt.customer_segment_id (+)     = s1.customer_segment_id
             and 1 = 1
          ) sq on (st.customer_segment_id      = sq.customer_segment_id
                   and svc_cus_smts_col(i).action in (CORESVC_CUSTOMER_SEGMENTS.action_mod,
                                                      CORESVC_CUSTOMER_SEGMENTS.action_del))
          when matched then
             update
                set process_id                   = svc_cus_smts_col(i).process_id ,
                    chunk_id                     = svc_cus_smts_col(i).chunk_id ,
                    action                       = svc_cus_smts_col(i).action,
                    row_seq                      = svc_cus_smts_col(i).row_seq ,
                    process$status               = svc_cus_smts_col(i).process$status ,
                    customer_segment_desc        = sq.customer_segment_desc ,
                    customer_segment_type        = sq.customer_segment_type ,
                    create_id                    = svc_cus_smts_col(i).create_id ,
                    create_datetime              = svc_cus_smts_col(i).create_datetime ,
                    last_upd_id                  = svc_cus_smts_col(i).last_upd_id ,
                    last_upd_datetime            = svc_cus_smts_col(i).last_upd_datetime
          when not matched then
             insert(process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    customer_segment_id ,
                    customer_segment_desc ,
                    customer_segment_type ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime
                   )
             values(svc_cus_smts_col(i).process_id ,
                    svc_cus_smts_col(i).chunk_id ,
                    svc_cus_smts_col(i).row_seq ,
                    svc_cus_smts_col(i).action ,
                    svc_cus_smts_col(i).process$status ,
                    sq.customer_segment_id ,
                    sq.customer_segment_desc ,
                    sq.customer_segment_type ,
                    svc_cus_smts_col(i).create_id ,
                    svc_cus_smts_col(i).create_datetime ,
                    svc_cus_smts_col(i).last_upd_id ,
                    svc_cus_smts_col(i).last_upd_datetime
                    );
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.count
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          CUS_SMTS_sheet,
                          svc_CUS_SMTS_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_CUS_SMTS;
--------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CUS_SMT_TY(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_CUSTOMER_SEGMENT_TYPES.PROCESS_ID%TYPE) IS

   Type svc_cus_smt_ty_col_typ IS TABLE OF SVC_CUSTOMER_SEGMENT_TYPES%ROWTYPE;
   svc_cus_smt_ty_col svc_cus_smt_ty_col_typ :=NEW svc_CUS_SMT_TY_col_typ();
   L_temp_rec             SVC_CUSTOMER_SEGMENT_TYPES%ROWTYPE;
   L_process_id           SVC_CUSTOMER_SEGMENT_TYPES.PROCESS_ID%TYPE;
   L_error                BOOLEAN:=FALSE;
   L_default_rec          SVC_CUSTOMER_SEGMENT_TYPES%ROWTYPE;

   cursor c_mandatory_ind IS
      select customer_segment_type_mi,
             customer_segment_type_desc_mi,
             1 as dummy
        from(select column_key,
                    mandatory
               from s9t_tmpl_cols_def
              WHERE template_key                              =  template_key
                and wksht_key                                 = 'CUSTOMER_SEGMENT_TYPES'
             )PIVOT (MAX(mandatory) AS mi FOR (column_key) IN (
                        'CUSTOMER_SEGMENT_TYPE'      as customer_segment_type,
                        'CUSTOMER_SEGMENT_TYPE_DESC' as customer_segment_type_desc,
                         null as dummy                         )
                     );
   L_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      exception;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CUSTOMER_SEGMENT_TYPES';
   L_pk_columns    VARCHAR2(255)  := 'Customer Segment Type';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN
   (select UPPER(customer_segment_type_dv) as customer_segment_type_dv,
           customer_segment_type_desc_dv,
           null as dummy
     from(select column_key,
                 default_value
            from s9t_tmpl_cols_def
           WHERE template_key                                  =  template_key
             and wksht_key                                     = 'CUSTOMER_SEGMENT_TYPES'
         ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN
                     ('CUSTOMER_SEGMENT_TYPE'     as customer_segment_type,
                     'CUSTOMER_SEGMENT_TYPE_DESC' as customer_segment_type_desc,
                      NULL                        as dummy                   )
                      )
                 )
   LOOP
      BEGIN
         L_default_rec.customer_segment_type_desc := rec.customer_segment_type_desc_dv;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            'CUSTOMER_SEGMENT_TYPES',
                            NULL,
                            'CUSTOMER_SEGMENT_TYPE_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.customer_segment_type := rec.customer_segment_type_dv;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            'CUSTOMER_SEGMENT_TYPES',
                            NULL,
                            'CUSTOMER_SEGMENT_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open C_mandatory_ind;
   fetch C_mandatory_ind into L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN(select r.get_cell(cus_smt_ty$action)               as action,
                     UPPER(r.get_cell(cus_smt_ty$cus_smt_ty))    as customer_segment_type,
                     r.get_cell(cus_smt_ty$cus_smt_ty_des)       as customer_segment_type_desc,
                     r.get_row_seq()                             as row_seq
                from s9t_folder sf,
                     table(sf.s9t_file_obj.sheets) ss,
                     table(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(CUS_SMT_TY_sheet)
            )

   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := false;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
          L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_type_desc := rec.customer_segment_type_desc;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_sheet,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_TYPE_DESC',
                             SQLCODE,
                             SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_type := rec.customer_segment_type;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_sheet,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_TYPE',
                             SQLCODE,
                             SQLERRM);
            L_error := true;
      END;
      if rec.action = CORESVC_CUSTOMER_SEGMENTS.action_new then
         L_temp_rec.customer_segment_type      := NVL( L_temp_rec.customer_segment_type,
                                                       L_default_rec.customer_segment_type);
         L_temp_rec.customer_segment_type_desc := NVL( L_temp_rec.customer_segment_type_desc,
                                                       L_default_rec.customer_segment_type_desc);
         end if;
      if not (L_temp_rec.customer_segment_type is not null
         and 1 = 1) then
         write_s9t_error(I_file_id,
                         CUS_SMT_TY_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := true;
      end if;
      if NOT L_error then
         svc_cus_smt_ty_col.extend();
         svc_cus_smt_ty_col(svc_cus_smt_ty_col.count()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_cus_smt_ty_col.count SAVE EXCEPTIONS
      Merge INTO svc_customer_segment_types st USING
        (select(case
                when L_mi_rec.customer_segment_type_mi    = 'N'
                 and svc_cus_smt_ty_col(i).action         = CORESVC_CUSTOMER_SEGMENTS.action_mod
                 and s1.customer_segment_type             IS NULL
                then mt.customer_segment_type
                else s1.customer_segment_type
                end) as customer_segment_type,
              (case
                when L_mi_rec.customer_segment_type_desc_mi    = 'N'
                 and svc_cus_smt_ty_col(i).action              = CORESVC_CUSTOMER_SEGMENTS.action_mod
                 and s1.customer_segment_type_desc             IS NULL
                then mt.customer_segment_type_desc
                else s1.customer_segment_type_desc
              end) as customer_segment_type_desc,
               null as dummy
           from (select svc_cus_smt_ty_col(i).customer_segment_type      as customer_segment_type,
                        svc_cus_smt_ty_col(i).customer_segment_type_desc as customer_segment_type_desc,
                        null as dummy
                   from dual
                ) s1,
                customer_segment_types mt
          where mt.customer_segment_type (+)     = s1.customer_segment_type
            and 1 = 1
               ) sq ON (st.customer_segment_type      = sq.customer_segment_type
                        and svc_cus_smt_ty_col(i).action in (CORESVC_CUSTOMER_SEGMENTS.action_mod,
                                                             CORESVC_CUSTOMER_SEGMENTS.action_del)
                       )
          when matched then
             update
                set process_id                  = svc_cus_smt_ty_col(i).process_id ,
                    chunk_id                    = svc_cus_smt_ty_col(i).chunk_id ,
                    row_seq                     = svc_cus_smt_ty_col(i).row_seq ,
                    action                      = svc_cus_smt_ty_col(i).action,
                    process$status              = svc_cus_smt_ty_col(i).process$status ,
                    customer_segment_type_desc  = sq.customer_segment_type_desc ,
                    create_id                   = svc_cus_smt_ty_col(i).create_id ,
                    create_datetime             = svc_cus_smt_ty_col(i).create_datetime ,
                    last_upd_id                 = svc_cus_smt_ty_col(i).last_upd_id ,
                    last_upd_datetime           = svc_cus_smt_ty_col(i).last_upd_datetime
          when NOT matched then
             insert(process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    customer_segment_type ,
                    customer_segment_type_desc ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime
                    )
              values(svc_cus_smt_ty_col(i).process_id ,
                     svc_cus_smt_ty_col(i).chunk_id ,
                     svc_cus_smt_ty_col(i).row_seq ,
                     svc_cus_smt_ty_col(i).action ,
                     svc_cus_smt_ty_col(i).process$status ,
                     sq.customer_segment_type ,
                     sq.customer_segment_type_desc ,
                     svc_cus_smt_ty_col(i).create_id ,
                     svc_cus_smt_ty_col(i).create_datetime ,
                     svc_cus_smt_ty_col(i).last_upd_id ,
                     svc_cus_smt_ty_col(i).last_upd_datetime
                     );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             CUS_SMT_TY_sheet,
                             svc_CUS_SMT_TY_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_CUS_SMT_TY;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CUS_SMTS_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id   IN   SVC_CUSTOMER_SEGMENTS_TL.PROCESS_ID%TYPE) IS

   Type svc_cus_smts_tl_col_typ IS TABLE OF SVC_CUSTOMER_SEGMENTS_TL%ROWTYPE;
   L_temp_rec                    SVC_CUSTOMER_SEGMENTS_TL%ROWTYPE;
   svc_cus_smts_tl_col           SVC_CUS_SMTS_TL_COL_TYP := NEW SVC_CUS_SMTS_TL_COL_TYP();
   L_process_id                  SVC_CUSTOMER_SEGMENTS_TL.PROCESS_ID%TYPE;
   L_error                       BOOLEAN              :=FALSE;
   L_default_rec                 SVC_CUSTOMER_SEGMENTS_TL%ROWTYPE;

   cursor c_mandatory_ind IS
      select lang_mi,
             customer_segment_id_mi,
             customer_segment_desc_mi
        from(select column_key,
                    mandatory
               from s9t_tmpl_cols_def
              where template_key                               =  template_key
                and wksht_key                                   = 'CUSTOMER_SEGMENTS_TL'
            ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN (
                                            'LANG'                  as lang,
                                            'CUSTOMER_SEGMENT_ID'   as customer_segment_id,
                                            'CUSTOMER_SEGMENT_DESC' as customer_segment_desc));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CUSTOMER_SEGMENTS_TL';
   L_pk_columns    VARCHAR2(255)  := 'Customer Segment ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN(select lang_dv,
                     customer_segment_id_dv,
                     customer_segment_desc_dv
                from(select column_key,
                            default_value
                       from s9t_tmpl_cols_def
                      where template_key                                  =  template_key
                        and wksht_key                                     = 'CUSTOMER_SEGMENTS_TL'
                     ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN (
                                                                            'LANG'                  as lang,
                                                                            'CUSTOMER_SEGMENT_ID'   as customer_segment_id,
                                                                            'CUSTOMER_SEGMENT_DESC' as customer_segment_desc,
                                                                            'CUSTOMER_SEGMENT_TYPE' as customer_segment_type)
                              )
              )
   LOOP
      BEGIN
         L_default_rec.customer_segment_id := rec.customer_segment_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_TL_SHEET,
                             NULL,
                            'CUSTOMER_SEGMENT_ID',
                            'INV_DEFAULT',
                             SQLERRM);
      END;
      BEGIN
         L_default_rec.customer_segment_desc := rec.customer_segment_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             CUS_SMTS_TL_SHEET,
                              NULL,
                             'CUSTOMER_SEGMENT_DESC',
                             NULL,
                             'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             CUS_SMTS_TL_SHEET,
                              NULL,
                             'LANG',
                             NULL,
                             'INV_DEFAULT');
      END;
   END loop;
 --Get mandatory indicators
   open C_mandatory_ind;
   fetch C_mandatory_ind
      into L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN(select r.get_cell(cus_smts_tl$action)                        as action,
                     r.get_cell(cus_smts_tl$lang)                          as lang,
                     r.get_cell(cus_smts_tl$cust_segment_id)               as customer_segment_id,
                     r.get_cell(cus_smts_tl$cust_segment_desc)             as customer_segment_desc,
                     r.get_row_seq()                                       as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(CUS_SMTS_TL_sheet)
             )
   loop
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := false;
      BEGIN
         L_temp_REC.ACTION := REC.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_id := rec.customer_segment_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_TL_SHEET,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_desc := rec.customer_segment_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_TL_SHEET,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CUS_SMTS_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      if rec.action = CORESVC_CUSTOMER_SEGMENTS.action_new then
         L_temp_rec.customer_segment_id   := NVL( L_temp_rec.customer_segment_id,
                                                  L_default_rec.customer_segment_id);
         L_temp_rec.customer_segment_desc := NVL( L_temp_rec.customer_segment_desc,
                                                  L_default_rec.customer_segment_desc);
         L_temp_rec.lang                  := NVL( L_temp_rec.lang,
                                                  L_default_rec.lang);
      end if;
      if not (L_temp_rec.customer_segment_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         CUS_SMTS_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := true;
      end if;
      if NOT L_error then
         svc_cus_smts_tl_col.extend();
         svc_cus_smts_tl_col(svc_cus_smts_tl_col.count()):=L_temp_rec;
      end if;
   END loop;
   BEGIN
      forall i IN 1..svc_cus_smts_tl_col.count SAVE EXCEPTIONS
      Merge INTO svc_customer_segments_tl st USING
         (select(case
                 when l_mi_rec.customer_segment_id_mi    = 'N'
                  and svc_cus_smts_tl_col(i).action         = CORESVC_CUSTOMER_SEGMENTS.action_mod
                  and s1.customer_segment_id             IS NULL
                 then mt.customer_segment_id
                 else s1.customer_segment_id
                 end) as customer_segment_id,
                (case
                 when l_mi_rec.customer_segment_desc_mi    = 'N'
                  and svc_cus_smts_tl_col(i).action           = CORESVC_CUSTOMER_SEGMENTS.action_mod
                  and s1.customer_segment_desc             IS NULL
                 then mt.customer_segment_desc
                 else s1.customer_segment_desc
                 end) as customer_segment_desc,
                (case
                 when l_mi_rec.lang_mi    = 'N'
                  and svc_cus_smts_tl_col(i).action           = CORESVC_CUSTOMER_SEGMENTS.action_mod
                  and s1.lang             IS NULL
                 then mt.lang
                 else s1.lang
                  end) as lang
            from (select svc_cus_smts_tl_col(i).customer_segment_id   as customer_segment_id,
                         svc_cus_smts_tl_col(i).customer_segment_desc as customer_segment_desc,
                         svc_cus_smts_tl_col(i).lang                  as lang
                    from dual
                 ) s1,
                   customer_segments_tl mt
           where mt.lang (+)                   = s1.lang
             and mt.customer_segment_id (+)    = s1.customer_segment_id
          ) sq on (st.lang                     = sq.lang
                   and st.customer_segment_id      = sq.customer_segment_id
                   and svc_cus_smts_tl_col(i).action in (CORESVC_CUSTOMER_SEGMENTS.action_mod,
                                                         CORESVC_CUSTOMER_SEGMENTS.action_del))
          when matched then
             update
                set process_id                   = svc_cus_smts_tl_col(i).process_id ,
                    chunk_id                     = svc_cus_smts_tl_col(i).chunk_id ,
                    action                       = svc_cus_smts_tl_col(i).action,
                    row_seq                      = svc_cus_smts_tl_col(i).row_seq ,
                    process$status               = svc_cus_smts_tl_col(i).process$status ,
                    customer_segment_desc        = sq.customer_segment_desc
          when not matched then
             insert(process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    lang ,
                    customer_segment_id ,
                    customer_segment_desc
                   )
             values(svc_cus_smts_tl_col(i).process_id ,
                    svc_cus_smts_tl_col(i).chunk_id ,
                    svc_cus_smts_tl_col(i).row_seq ,
                    svc_cus_smts_tl_col(i).action ,
                    svc_cus_smts_tl_col(i).process$status ,
                    sq.lang ,
                    sq.customer_segment_id ,
                    sq.customer_segment_desc
                    );
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.count
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          CUS_SMTS_TL_sheet,
                          svc_cus_smts_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_CUS_SMTS_TL;
--------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CUS_SMT_TY_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id   IN   SVC_CUSTOMER_SEGMENT_TYPES_TL.PROCESS_ID%TYPE) IS

   Type svc_cus_smt_ty_tl_col_typ_tl IS TABLE OF SVC_CUSTOMER_SEGMENT_TYPES_TL%ROWTYPE;
   svc_cus_smt_ty_tl_col          svc_cus_smt_ty_tl_col_typ_tl :=NEW svc_cus_smt_ty_tl_col_typ_tl();
   L_temp_rec                     SVC_CUSTOMER_SEGMENT_TYPES_TL%ROWTYPE;
   L_process_id                   SVC_CUSTOMER_SEGMENT_TYPES_TL.PROCESS_ID%TYPE;
   L_error                        BOOLEAN:=FALSE;
   L_default_rec                  SVC_CUSTOMER_SEGMENT_TYPES_TL%ROWTYPE;

   cursor c_mandatory_ind IS
      select lang_mi,
             customer_segment_type_mi,
             customer_segment_type_desc_mi
        from(select column_key,
                    mandatory
               from s9t_tmpl_cols_def
              WHERE template_key                              =  template_key
                and wksht_key                                 = 'CUSTOMER_SEGMENT_TYPES_TL'
             )PIVOT (MAX(mandatory) AS mi FOR (column_key) IN (
                        'LANG'                       as lang,
                        'CUSTOMER_SEGMENT_TYPE'      as customer_segment_type,
                        'CUSTOMER_SEGMENT_TYPE_DESC' as customer_segment_type_desc )
                     );
   L_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      exception;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CUSTOMER_SEGMENT_TYPES_TL';
   L_pk_columns    VARCHAR2(255)  := 'Customer Segment Type, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN
   (select lang_dv,
           UPPER(customer_segment_type_dv) as customer_segment_type_dv,
           customer_segment_type_desc_dv,
           null as dummy
     from(select column_key,
                 default_value
            from s9t_tmpl_cols_def
           WHERE template_key                                  =  template_key
             and wksht_key                                     = 'CUSTOMER_SEGMENT_TYPES_TL'
         ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN
                     ('LANG                     ' as lang,
					  'CUSTOMER_SEGMENT_TYPE'     as customer_segment_type,
                      'CUSTOMER_SEGMENT_TYPE_DESC' as customer_segment_type_desc)
                      )
                 )
   LOOP
      BEGIN
         L_default_rec.customer_segment_type_desc := rec.customer_segment_type_desc_dv;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_TL_sheet,
                            NULL,
                            'CUSTOMER_SEGMENT_TYPE_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.customer_segment_type := rec.customer_segment_type_dv;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_TL_sheet,
                            NULL,
                            'CUSTOMER_SEGMENT_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_TL_sheet,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open C_mandatory_ind;
   fetch C_mandatory_ind into L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN(select r.get_cell(cus_smt_ty_tl$action)               as action,
                     r.get_cell(cus_smt_ty_tl$lang)                 as lang,
                     UPPER(r.get_cell(cus_smt_ty_tl$cus_smt_ty))    as customer_segment_type,
                     r.get_cell(cus_smt_ty_tl$cus_smt_ty_des)       as customer_segment_type_desc,
                     r.get_row_seq()                                as row_seq
                from s9t_folder sf,
                     table(sf.s9t_file_obj.sheets) ss,
                     table(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(CUS_SMT_TY_TL_sheet)
            )

   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := false;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
          L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_type_desc := rec.customer_segment_type_desc;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_TL_sheet,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_TYPE_DESC',
                             SQLCODE,
                             SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.customer_segment_type := rec.customer_segment_type;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_TL_sheet,
                            rec.row_seq,
                            'CUSTOMER_SEGMENT_TYPE',
                             SQLCODE,
                             SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            write_s9t_error(I_file_id,
                            CUS_SMT_TY_TL_sheet,
                            rec.row_seq,
                            'LANG',
                             SQLCODE,
                             SQLERRM);
            L_error := true;
      END;
      if rec.action = CORESVC_CUSTOMER_SEGMENTS.action_new then
         L_temp_rec.lang                       := NVL( L_temp_rec.lang,
                                                       L_default_rec.lang);
         L_temp_rec.customer_segment_type      := NVL( L_temp_rec.customer_segment_type,
                                                       L_default_rec.customer_segment_type);
         L_temp_rec.customer_segment_type_desc := NVL( L_temp_rec.customer_segment_type_desc,
                                                       L_default_rec.customer_segment_type_desc);
         end if;
      if NOT (L_temp_rec.customer_segment_type is NOT NULL and L_temp_rec.lang is NOT NULL) then
         write_s9t_error(I_file_id,
                         CUS_SMT_TY_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := true;
      end if;
      if NOT L_error then
         svc_cus_smt_ty_tl_col.extend();
         svc_cus_smt_ty_tl_col(svc_cus_smt_ty_tl_col.count()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_cus_smt_ty_tl_col.count SAVE EXCEPTIONS
      Merge INTO svc_customer_segment_types_tl st USING
        (select(case
                when L_mi_rec.customer_segment_type_mi    = 'N'
                 and svc_cus_smt_ty_tl_col(i).action         = CORESVC_CUSTOMER_SEGMENTS.action_mod
                 and s1.customer_segment_type             IS NULL
                then mt.customer_segment_type
                else s1.customer_segment_type
                end) as customer_segment_type,
              (case
                when L_mi_rec.customer_segment_type_desc_mi    = 'N'
                 and svc_cus_smt_ty_tl_col(i).action              = CORESVC_CUSTOMER_SEGMENTS.action_mod
                 and s1.customer_segment_type_desc             IS NULL
                then mt.customer_segment_type_desc
                else s1.customer_segment_type_desc
              end) as customer_segment_type_desc,
              (case
                when L_mi_rec.lang_mi    = 'N'
                 and svc_cus_smt_ty_tl_col(i).action              = CORESVC_CUSTOMER_SEGMENTS.action_mod
                 and s1.lang             IS NULL
                then mt.lang
                else s1.lang
              end) as lang
           from (select svc_cus_smt_ty_tl_col(i).customer_segment_type      as customer_segment_type,
                        svc_cus_smt_ty_tl_col(i).customer_segment_type_desc as customer_segment_type_desc,
                        svc_cus_smt_ty_tl_col(i).lang                       as lang
                   from dual
                ) s1,
                customer_segment_types_tl mt
          where mt.lang (+)                      = s1.lang
            and mt.customer_segment_type (+)     = s1.customer_segment_type
               ) sq ON (st.lang                           = sq.lang
			            and st.customer_segment_type      = sq.customer_segment_type
                        and svc_cus_smt_ty_tl_col(i).action in (CORESVC_CUSTOMER_SEGMENTS.action_mod,
                                                             CORESVC_CUSTOMER_SEGMENTS.action_del)
                       )
          when matched then
             update
                set process_id                  = svc_cus_smt_ty_tl_col(i).process_id ,
                    chunk_id                    = svc_cus_smt_ty_tl_col(i).chunk_id ,
                    row_seq                     = svc_cus_smt_ty_tl_col(i).row_seq ,
                    action                      = svc_cus_smt_ty_tl_col(i).action,
                    process$status              = svc_cus_smt_ty_tl_col(i).process$status ,
                    customer_segment_type_desc  = sq.customer_segment_type_desc
          when NOT matched then
             insert(process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    lang ,
                    customer_segment_type ,
                    customer_segment_type_desc
                    )
              values(svc_cus_smt_ty_tl_col(i).process_id ,
                     svc_cus_smt_ty_tl_col(i).chunk_id ,
                     svc_cus_smt_ty_tl_col(i).row_seq ,
                     svc_cus_smt_ty_tl_col(i).action ,
                     svc_cus_smt_ty_tl_col(i).process$status ,
                     sq.lang ,
                     sq.customer_segment_type ,
                     sq.customer_segment_type_desc
                     );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             CUS_SMT_TY_TL_sheet,
                             svc_CUS_SMT_TY_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_CUS_SMT_TY_TL;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
  RETURN BOOLEAN IS
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_program          VARCHAR2(64):='CORESVC_CUSTOMER_SEGMENTS.process_s9t';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR       EXCEPTION;
   PRAGMA         EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT; --to ensure that the record in s9t_folder is commited
   S9T_PKG.ODS2OBJ(I_FILE_ID);
   COMMIT;

   L_file := s9t_pkg.get_obj(I_file_id);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_FILE);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_CUS_SMT_TY(I_file_id,I_process_id);
      PROCESS_S9T_CUS_SMT_TY_TL(I_file_id,I_process_id);
      PROCESS_S9T_CUS_SMTS(I_file_id,I_process_id);
      PROCESS_S9T_CUS_SMTS_TL(I_file_id,I_process_id);
   end if;
   O_error_count := Lp_s9t_errors_tab.count();
   forall i IN 1..O_error_count
      insert INTO s9t_errors
         VALUES Lp_s9t_errors_tab(i);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();

    -- Update SVC_PROCESS_TRACKER
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;

-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_CUS_SMTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error           IN OUT   BOOLEAN ,
                              I_rec             IN       C_SVC_CUS_SMTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_CUSTOMER_SEGMENTS.PROCESS_VAL_CUS_SMTS';
   L_assoc_exists  BOOLEAN;
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_CUSTOMER_SEGMENTS';

BEGIN
 ---for during insert customer_segment_type is null
   if I_rec.action =action_new
      and I_rec.customer_segment_type is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.nextval,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'CUSTOMER_SEGMENT_TYPE',
                  'CUS_SMT_TY_REQ');
      O_error :=TRUE;
   end if;
   ---for customer_segment_desc is NULL
   if I_rec.action IN (action_new,action_mod)
      and I_rec.customer_segment_desc is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.nextval,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'CUSTOMER_SEGMENT_DESC',
                  'CUS_SMT_DES_REQ');
      O_error :=TRUE;
   end if;
   ---for during update cant modify cus_smt_type of existing cus_segment
   if I_rec.action=action_mod then
      if nvl(I_rec.old_customer_segment_type,-1)<>nvl(I_rec.customer_segment_type,-1) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CUSTOMER_SEGMENT_TYPE',
                     'CANT_MOD_CUS_SMT_TY');
         O_error :=TRUE;
      end if;
   end if;

  -------- FOR foreign key constraint
   if I_rec.action=action_del
      and I_rec.PK_CUSTOMER_SEGMENTS_rid is not NULL then
      if CUST_SEGMENT_SQL.CUST_SEGMENT_ID_EXCEP_EXISTS(O_error_message,
                                                       L_assoc_exists,
                                                       I_rec.customer_segment_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CUSTOMER_SEGMENT_ID',
                     O_error_message);
         O_error :=TRUE;
      elsif L_assoc_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CUSTOMER_SEGMENT_ID',
                     'CANT_DEL_PROM_EXISTS');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_VAL_CUS_SMTS;
-----------------------------------------------------------------------------
FUNCTION PROCESS_VAL_CUS_SMT_TY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error           IN OUT   BOOLEAN ,
                                I_rec             IN       C_SVC_CUS_SMT_TY%ROWTYPE)
RETURN BOOLEAN IS
   L_assoc_exists  BOOLEAN                              := FALSE;
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_CUSTOMER_SEGMENT_TYPES';
BEGIN
  -------- FOR foreign key constraint
   if I_rec.action=action_del
      and I_rec.CUSTOMER_SEGMENT_TYPE is NOT NULL  then
      if CUST_SEGMENT_TYPE_SQL.CUST_SEGMENT_TYPE_EXCEP_EXISTS(O_error_message,
                                                              L_assoc_exists,
                                                              I_rec.CUSTOMER_SEGMENT_TYPE)
                                                              = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CUSTOMER_SEGMENT_TYPE',
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_assoc_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CUSTOMER_SEGMENT_TYPE',
                     'CANT_DEL_CUST_SEG_EXIST');
         O_error :=TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_VAL_CUS_SMT_TY;
--------------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMTS_INS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cus_smts_temp_rec IN     CUSTOMER_SEGMENTS%ROWTYPE)
  RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUS_SMTS_INS';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENTS';
BEGIN
   insert into customer_segments
      values I_cus_smts_temp_rec;
   return TRUE;
EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CUS_SMTS_INS;

----------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMTS_UPD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cus_smts_temp_rec IN     CUSTOMER_SEGMENTS%ROWTYPE)
  RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUS_SMTS_UPD';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENTS';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor c_cus_smts is
      select 'x'
        from customer_segments
       where customer_segment_id = I_cus_smts_temp_rec.customer_segment_id
         for update nowait;
BEGIN
   open c_cus_smts;
   close c_cus_smts;
   update customer_segments
      set row = I_cus_smts_temp_rec
    where customer_segment_id =I_cus_smts_temp_rec.customer_segment_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_cus_smts_temp_rec.customer_segment_id,
                                             NULL);
      return FALSE;

   when others then
      if c_cus_smts%ISOPEN then
         close c_cus_smts;
      end if;
      O_error_message :=SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
       return FALSE;
END EXEC_CUS_SMTS_UPD;

--------------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMTS_DEL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cus_smts_temp_rec IN     CUSTOMER_SEGMENTS%ROWTYPE)
  RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUS_SMTS_DEL';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENTS';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor C_CUS_SMTS_TL is
      select 'x'
        from customer_segments
       where customer_segment_id = I_cus_smts_temp_rec.customer_segment_id
         for update nowait;

   cursor C_CUS_SMTS is
      select 'x'
        from customer_segments_tl
       where customer_segment_id = I_cus_smts_temp_rec.customer_segment_id
         for update nowait;
BEGIN
   open C_CUS_SMTS_TL;
   close C_CUS_SMTS_TL;

   open C_CUS_SMTS;
   close C_CUS_SMTS;

   delete from customer_segments_tl
    where customer_segment_id =I_cus_smts_temp_rec.customer_segment_id;

   delete from customer_segments
    where customer_segment_id =I_cus_smts_temp_rec.customer_segment_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_cus_smts_temp_rec.customer_segment_id,
                                             NULL);
      return FALSE;

   when others then
      if C_CUS_SMTS_TL%ISOPEN then
         close C_CUS_SMTS;
      end if;
      if C_CUS_SMTS_TL%ISOPEN then
         close C_CUS_SMTS_TL;
      end if;
      O_error_message :=SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            null,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_CUS_SMTS_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CUSTOMER_SEGMENTS_TL_INS(O_error_message                   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cust_segments_tl_ins_tab    IN       CUSTOMER_SEGMENTS_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUSTOMER_SEGMENTS_TL_INS';

BEGIN
   if I_cust_segments_tl_ins_tab is NOT NULL and I_cust_segments_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_cust_segments_tl_ins_tab.COUNT()
         insert into customer_segments_tl
              values I_cust_segments_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CUSTOMER_SEGMENTS_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CUSTOMER_SEGMENTS_TL_UPD(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cust_segments_tl_upd_tab   IN       CUSTOMER_SEGMENTS_TL_TAB,
                                       I_cust_segments_tl_upd_rst   IN       ROW_SEQ_TAB,
                                       I_process_id                 IN       SVC_CUSTOMER_SEGMENTS_TL.PROCESS_ID%TYPE,
                                       I_chunk_id                   IN       SVC_CUSTOMER_SEGMENTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_CUSTOMER_SEGMENTS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENTS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CUST_SEGMENTS_TL_UPD(I_customer_segment_id  CUSTOMER_SEGMENTS_TL.CUSTOMER_SEGMENT_ID%TYPE,
                                      I_lang                 CUSTOMER_SEGMENTS_TL.LANG%TYPE) is
      select 'x'
        from customer_segments_tl
       where customer_segment_id = I_customer_segment_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cust_segments_tl_upd_tab is NOT NULL and I_cust_segments_tl_upd_tab.count > 0 then
      for i in I_cust_segments_tl_upd_tab.FIRST..I_cust_segments_tl_upd_tab.LAST loop
         BEGIN
         L_key_val1 := 'Lang: '||to_char(I_cust_segments_tl_upd_tab(i).lang);
         L_key_val2 := 'Customer Segment ID: '||to_char(I_cust_segments_tl_upd_tab(i).customer_segment_id);
         open C_LOCK_CUST_SEGMENTS_TL_UPD(I_cust_segments_tl_upd_tab(i).customer_segment_id,
                                          I_cust_segments_tl_upd_tab(i).lang);
         close C_LOCK_CUST_SEGMENTS_TL_UPD;

         update customer_segments_tl
            set customer_segment_desc = I_cust_segments_tl_upd_tab(i).customer_segment_desc,
                last_update_id = I_cust_segments_tl_upd_tab(i).last_update_id,
                last_update_datetime = I_cust_segments_tl_upd_tab(i).last_update_datetime
          where lang = I_cust_segments_tl_upd_tab(i).lang
            and customer_segment_id = I_cust_segments_tl_upd_tab(i).customer_segment_id;
            
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_CUSTOMER_SEGMENTS_TL',
                           I_cust_segments_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CUST_SEGMENTS_TL_UPD%ISOPEN then
         close C_LOCK_CUST_SEGMENTS_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CUSTOMER_SEGMENTS_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CUSTOMER_SEGMENTS_TL_DEL(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cust_segments_tl_del_tab   IN       CUSTOMER_SEGMENTS_TL_TAB,
                                       I_cust_segments_tl_del_rst   IN       ROW_SEQ_TAB,
                                       I_process_id                 IN       SVC_CUSTOMER_SEGMENTS_TL.PROCESS_ID%TYPE,
                                       I_chunk_id                   IN       SVC_CUSTOMER_SEGMENTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUSTOMER_SEGMENTS_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENTS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CUST_SEGMENTS_TL_DEL(I_customer_segment_id  CUSTOMER_SEGMENTS_TL.CUSTOMER_SEGMENT_ID%TYPE,
                                      I_lang                 CUSTOMER_SEGMENTS_TL.LANG%TYPE) is
      select 'x'
        from customer_segments_tl
       where customer_segment_id = I_customer_segment_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cust_segments_tl_del_tab is NOT NULL and I_cust_segments_tl_del_tab.count > 0 then
      for i in I_cust_segments_tl_del_tab.FIRST..I_cust_segments_tl_del_tab.LAST loop
         BEGIN
         L_key_val1 := 'Lang: '||to_char(I_cust_segments_tl_del_tab(i).lang);
         L_key_val2 := 'Customer Segment ID: '||to_char(I_cust_segments_tl_del_tab(i).customer_segment_id);
         open C_LOCK_CUST_SEGMENTS_TL_DEL(I_cust_segments_tl_del_tab(i).customer_segment_id,
                                          I_cust_segments_tl_del_tab(i).lang);
         close C_LOCK_CUST_SEGMENTS_TL_DEL;
         
         delete customer_segments_tl
          where lang = I_cust_segments_tl_del_tab(i).lang
            and customer_segment_id = I_cust_segments_tl_del_tab(i).customer_segment_id;
            
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_CUSTOMER_SEGMENTS_TL',
                           I_cust_segments_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CUST_SEGMENTS_TL_DEL%ISOPEN then
         close C_LOCK_CUST_SEGMENTS_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CUSTOMER_SEGMENTS_TL_DEL;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMT_TY_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cus_smt_ty_temp_rec   IN       CUSTOMER_SEGMENT_TYPES%ROWTYPE)
RETURN BOOLEAN IS
BEGIN
   insert
     into CUSTOMER_SEGMENT_TYPES
   values I_cus_smt_ty_temp_rec;
   RETURN TRUE;
EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CUS_SMT_TY_INS;

---------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMT_TY_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cus_smt_ty_temp_rec   IN       CUSTOMER_SEGMENT_TYPES%ROWTYPE)
return BOOLEAN is
   L_program     VARCHAR2(64)                      := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUS_SMT_TY_UPD';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENT_TYPES';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked, -54);

   cursor c_cus_smt_ty is
      select 'x'
        from customer_segment_types
       where customer_segment_type = I_cus_smt_ty_temp_rec.customer_segment_type
         for update nowait;

BEGIN
   open c_cus_smt_ty;
   close c_cus_smt_ty;
   update customer_segment_types
      set row =I_cus_smt_ty_temp_rec
    where UPPER(customer_segment_type) = UPPER(I_cus_smt_ty_temp_rec.customer_segment_type);
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_cus_smt_ty_temp_rec.customer_segment_type,
                                             NULL);
      return FALSE;

   when others then
      if c_cus_smt_ty%ISOPEN then
         close c_cus_smt_ty;
      end if;
      O_error_message:=SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           NULL,
                                           to_char(SQLCODE));
      return FALSE;
END EXEC_CUS_SMT_TY_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMT_TY_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cus_smt_ty_temp_rec   IN       CUSTOMER_SEGMENT_TYPES%ROWTYPE)
  RETURN BOOLEAN is
   L_program       VARCHAR2(64)                      := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUS_SMT_TY_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENT_TYPES';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor c_cus_smt_ty_tl is
      select 'x'
        from customer_segment_types_tl
       where customer_segment_type = I_cus_smt_ty_temp_rec.customer_segment_type
         for update nowait;

   cursor c_cus_smt_ty is
      select 'x'
        from customer_segment_types
       where customer_segment_type = I_cus_smt_ty_temp_rec.customer_segment_type
         for update nowait;

BEGIN
   open c_cus_smt_ty_tl;
   close c_cus_smt_ty_tl;

   open c_cus_smt_ty;
   close c_cus_smt_ty;

   delete from customer_segment_types_tl
    where customer_segment_type = I_cus_smt_ty_temp_rec.customer_segment_type;

   delete from customer_segment_types
    where customer_segment_type = I_cus_smt_ty_temp_rec.customer_segment_type;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_cus_smt_ty_temp_rec.customer_segment_type,
                                             NULL);
      return FALSE;

   when others then
      if c_cus_smt_ty%ISOPEN then
         close c_cus_smt_ty;
      end if;
      O_error_message:=SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            NULL,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CUS_SMT_TY_DEL;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMT_TY_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cus_smt_ty_tl_ins_tab    IN       CUSTOMER_SEGMENT_TYPES_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUS_SMT_TY_TL_INS';

BEGIN
   if I_cus_smt_ty_tl_ins_tab is NOT NULL and I_cus_smt_ty_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_cus_smt_ty_tl_ins_tab.COUNT()
         insert into customer_segment_types_tl
              values I_cus_smt_ty_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CUS_SMT_TY_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMT_TY_TL_UPD(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cus_smt_ty_tl_upd_tab     IN       CUSTOMER_SEGMENT_TYPES_TL_TAB,
                                I_cus_smt_tl_upd_rst        IN       ROW_SEQ_TAB,
                                I_process_id                IN       SVC_CUSTOMER_SEGMENT_TYPES_TL.PROCESS_ID%TYPE,
                                I_chunk_id                  IN       SVC_CUSTOMER_SEGMENT_TYPES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_CUS_SMT_TY_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENT_TYPES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CUS_SMT_TY_TL_UPD(I_customer_segment_type  CUSTOMER_SEGMENT_TYPES_TL.CUSTOMER_SEGMENT_TYPE%TYPE,
                                   I_lang                   CUSTOMER_SEGMENT_TYPES_TL.LANG%TYPE) is
      select 'x'
        from customer_segment_types_tl
       where customer_segment_type = I_customer_segment_type
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cus_smt_ty_tl_upd_tab is NOT NULL and I_cus_smt_ty_tl_upd_tab.count > 0 then
      for i in I_cus_smt_ty_tl_upd_tab.FIRST..I_cus_smt_ty_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_cus_smt_ty_tl_upd_tab(i).lang);
            L_key_val2 := 'Customer Segment Type: '||to_char(I_cus_smt_ty_tl_upd_tab(i).customer_segment_type);
            open C_LOCK_CUS_SMT_TY_TL_UPD(I_cus_smt_ty_tl_upd_tab(i).customer_segment_type,
                                          I_cus_smt_ty_tl_upd_tab(i).lang);
            close C_LOCK_CUS_SMT_TY_TL_UPD;
            
            update customer_segment_types_tl
               set customer_segment_type_desc = I_cus_smt_ty_tl_upd_tab(i).customer_segment_type_desc,
                   last_update_id = I_cus_smt_ty_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_cus_smt_ty_tl_upd_tab(i).last_update_datetime
             where lang = I_cus_smt_ty_tl_upd_tab(i).lang
               and UPPER(customer_segment_type) = UPPER(I_cus_smt_ty_tl_upd_tab(i).customer_segment_type);
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_CUSTOMER_SEGMENT_TYPES_TL',
                           I_cus_smt_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CUS_SMT_TY_TL_UPD%ISOPEN then
         close C_LOCK_CUS_SMT_TY_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CUS_SMT_TY_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CUS_SMT_TY_TL_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cus_smt_ty_tl_del_tab   IN       CUSTOMER_SEGMENT_TYPES_TL_TAB,
                                I_cus_smt_tl_del_rst      IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_CUSTOMER_SEGMENT_TYPES_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_CUSTOMER_SEGMENT_TYPES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_CUSTOMER_SEGMENTS.EXEC_CUS_SMT_TY_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENT_TYPES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CUS_SMT_TY_TL_DEL(I_customer_segment_type  CUSTOMER_SEGMENT_TYPES_TL.CUSTOMER_SEGMENT_TYPE%TYPE,
                                   I_lang                   CUSTOMER_SEGMENT_TYPES_TL.LANG%TYPE) is
      select 'x'
        from customer_segment_types_tl
       where customer_segment_type = I_customer_segment_type
         and lang = I_lang
         for update nowait;

BEGIN
   if I_cus_smt_ty_tl_del_tab is NOT NULL and I_cus_smt_ty_tl_del_tab.count > 0 then
      for i in I_cus_smt_ty_tl_del_tab.FIRST..I_cus_smt_ty_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_cus_smt_ty_tl_del_tab(i).lang);
            L_key_val2 := 'Customer Segment Type: '||to_char(I_cus_smt_ty_tl_del_tab(i).customer_segment_type);
            open C_LOCK_CUS_SMT_TY_TL_DEL(I_cus_smt_ty_tl_del_tab(i).customer_segment_type,
                                          I_cus_smt_ty_tl_del_tab(i).lang);
            close C_LOCK_CUS_SMT_TY_TL_DEL;
            
            delete customer_segment_types_tl
             where lang = I_cus_smt_ty_tl_del_tab(i).lang
               and customer_segment_type = I_cus_smt_ty_tl_del_tab(i).customer_segment_type;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_CUSTOMER_SEGMENT_TYPES_TL',
                           I_cus_smt_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CUS_SMT_TY_TL_DEL%ISOPEN then
         close C_LOCK_CUS_SMT_TY_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CUS_SMT_TY_TL_DEL;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CUS_SMTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id    IN     SVC_CUSTOMER_SEGMENTS.PROCESS_ID%TYPE,
                          I_chunk_id      IN     SVC_CUSTOMER_SEGMENTS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN;
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
   L_program               VARCHAR2(64)                      :='CORESVC_CUSTOMER_SEGMENTS.PROCESS_CUS_SMTS';
   L_cus_smts_temp_rec     CUSTOMER_SEGMENTS%ROWTYPE;
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CUSTOMER_SEGMENTS';
BEGIN
   FOR rec IN c_svc_CUS_SMTS(I_process_id,
                             I_chunk_id)
   loop
      L_error           := False;
      L_process_error   := FALSE;

     --for invalid action
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

    ---for duplicate customer segments
      if rec.action = action_new
         and rec.pk_customer_segments_rid IS NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CUSTOMER_SEGMENT_ID',
                     'DUP_CUS_SMT');
         L_error :=true;
      end if;

    ---for during update/delete if customer_segment_id doesnt exist
      if rec.action IN (action_mod,action_del)
         and rec.CUSTOMER_SEGMENT_ID is NOT NULL then
         if rec.pk_customer_segments_rid IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CUSTOMER_SEGMENT_ID',
                        'CUS_SMT_ID_NOT_EXIST');
            L_error :=true;
         end if;
      end if;

     --for during insertion,customer_segment_type foriegn key voilation
      if rec.action =action_new
         and rec.customer_segment_type is not NULL then
         if rec.csg_cst_fk_rid IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CUSTOMER_SEGMENT_TYPE',
                        'CSG_CST_FK_VOILATION');
            L_error :=true;
         end if;
      end if;
    ---for other validations
      if PROCESS_VAL_CUS_SMTS(O_error_message,
                              L_error,
                              rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;


      if NOT L_error then
         L_cus_smts_temp_rec.customer_segment_id                := rec.customer_segment_id;
         L_cus_smts_temp_rec.customer_segment_desc              := rec.customer_segment_desc;
         L_cus_smts_temp_rec.customer_segment_type              := rec.customer_segment_type;

         if rec.action  =action_new then
            if EXEC_CUS_SMTS_INS(O_error_message,
                                 L_cus_smts_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action  =action_mod then
            if EXEC_CUS_SMTS_UPD(O_error_message,
                                 L_cus_smts_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action  =action_del then
            if EXEC_CUS_SMTS_DEL(O_error_message,
                                 L_cus_smts_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CUS_SMTS;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_CUSTOMER_SEGMENTS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_process_id      IN       SVC_CUSTOMER_SEGMENTS_TL.PROCESS_ID%TYPE,
                                      I_chunk_id        IN       SVC_CUSTOMER_SEGMENTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                      VARCHAR2(64) := 'CORESVC_CUSTOMER_SEGMENTS.PROCESS_CUSTOMER_SEGMENTS_TL';
   L_table                        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CUSTOMER_SEGMENTS_TL';
   L_error_message                RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENTS_TL';
   L_base_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENTS';
   L_error                        BOOLEAN := FALSE;
   L_process_error                BOOLEAN := FALSE;
   L_cust_segments_tl_temp_rec    CUSTOMER_SEGMENTS_TL%ROWTYPE;
   L_cust_segments_tl_upd_rst     ROW_SEQ_TAB;
   L_cust_segments_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_CUSTOMER_SEGMENTS_TL(I_process_id NUMBER,
                                     I_chunk_id   NUMBER) is
      select pk_customer_segments_tl.rowid  as pk_customer_segments_tl_rid,
             fk_customer_segments.rowid     as fk_customer_segments_rid,
             fk_lang.rowid           as fk_lang_rid,
             st.lang,
             st.customer_segment_id,
             st.customer_segment_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_customer_segments_tl  st,
             customer_segments         fk_customer_segments,
             customer_segments_tl      pk_customer_segments_tl,
             lang                      fk_lang
       where st.process_id           =  I_process_id
         and st.chunk_id             =  I_chunk_id
         and st.customer_segment_id  =  fk_customer_segments.customer_segment_id (+)
         and st.lang                 =  pk_customer_segments_tl.lang (+)
         and st.customer_segment_id  =  pk_customer_segments_tl.customer_segment_id (+)
         and st.lang                 =  fk_lang.lang (+);

   TYPE SVC_CUSTOMER_SEGMENTS_TL is TABLE OF C_SVC_CUSTOMER_SEGMENTS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_customer_segments_tl_tab        SVC_CUSTOMER_SEGMENTS_TL;

   L_customer_segments_tl_ins_tab         CUSTOMER_SEGMENTS_TL_TAB         := NEW CUSTOMER_SEGMENTS_TL_TAB();
   L_customer_segments_tl_upd_tab         CUSTOMER_SEGMENTS_TL_TAB         := NEW CUSTOMER_SEGMENTS_TL_TAB();
   L_customer_segments_tl_del_tab         CUSTOMER_SEGMENTS_TL_TAB         := NEW CUSTOMER_SEGMENTS_TL_TAB();

BEGIN
   if C_SVC_CUSTOMER_SEGMENTS_TL%ISOPEN then
      close C_SVC_CUSTOMER_SEGMENTS_TL;
   end if;

   open C_SVC_CUSTOMER_SEGMENTS_TL(I_process_id,
                                   I_chunk_id);
   LOOP
      fetch C_SVC_CUSTOMER_SEGMENTS_TL bulk collect into L_svc_customer_segments_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_customer_segments_tl_tab.COUNT > 0 then
         FOR i in L_svc_customer_segments_tl_tab.FIRST..L_svc_customer_segments_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_customer_segments_tl_tab(i).action is NULL
               or L_svc_customer_segments_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_customer_segments_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_customer_segments_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_customer_segments_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_customer_segments_tl_tab(i).action = action_new
               and L_svc_customer_segments_tl_tab(i).pk_customer_segments_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_customer_segments_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_customer_segments_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_customer_segments_tl_tab(i).lang is NOT NULL
               and L_svc_customer_segments_tl_tab(i).customer_segment_id is NOT NULL
               and L_svc_customer_segments_tl_tab(i).pk_customer_segments_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_customer_segments_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_customer_segments_tl_tab(i).action = action_new
               and L_svc_customer_segments_tl_tab(i).customer_segment_id is NOT NULL
               and L_svc_customer_segments_tl_tab(i).fk_customer_segments_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_customer_segments_tl_tab(i).row_seq,
                            'CUSTOMER_SEGMENT_ID',
                            'CUS_SMT_ID_NOT_EXIST');
               L_error :=TRUE;
            end if;

            if L_svc_customer_segments_tl_tab(i).action = action_new
               and L_svc_customer_segments_tl_tab(i).lang is NOT NULL
               and L_svc_customer_segments_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_customer_segments_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_customer_segments_tl_tab(i).customer_segment_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_customer_segments_tl_tab(i).row_seq,
                           'CUSTOMER_SEGMENT_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_customer_segments_tl_tab(i).action in (action_new, action_mod) and L_svc_customer_segments_tl_tab(i).customer_segment_desc is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_customer_segments_tl_tab(i).row_seq,
                           'CUSTOMER_SEGMENT_DESC',
                           'CUS_SMT_DES_REQ');
               L_error :=TRUE;
            end if;

            if L_svc_customer_segments_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_customer_segments_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_cust_segments_tl_temp_rec.lang := L_svc_customer_segments_tl_tab(i).lang;
               L_cust_segments_tl_temp_rec.customer_segment_id := L_svc_customer_segments_tl_tab(i).customer_segment_id;
               L_cust_segments_tl_temp_rec.customer_segment_desc := L_svc_customer_segments_tl_tab(i).customer_segment_desc;
               L_cust_segments_tl_temp_rec.create_datetime := SYSDATE;
               L_cust_segments_tl_temp_rec.create_id := GET_USER;
               L_cust_segments_tl_temp_rec.last_update_datetime := SYSDATE;
               L_cust_segments_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_customer_segments_tl_tab(i).action = action_new then
                  L_customer_segments_tl_ins_tab.extend;
                  L_customer_segments_tl_ins_tab(L_customer_segments_tl_ins_tab.count()) := L_cust_segments_tl_temp_rec;
               end if;

               if L_svc_customer_segments_tl_tab(i).action = action_mod then
                  L_customer_segments_tl_upd_tab.extend;
                  L_customer_segments_tl_upd_tab(L_customer_segments_tl_upd_tab.count()) := L_cust_segments_tl_temp_rec;
                  L_cust_segments_tl_upd_rst(L_customer_segments_tl_upd_tab.count()) := L_svc_customer_segments_tl_tab(i).row_seq;
               end if;

               if L_svc_customer_segments_tl_tab(i).action = action_del then
                  L_customer_segments_tl_del_tab.extend;
                  L_customer_segments_tl_del_tab(L_customer_segments_tl_del_tab.count()) := L_cust_segments_tl_temp_rec;
                  L_cust_segments_tl_del_rst(L_customer_segments_tl_del_tab.count()) := L_svc_customer_segments_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_CUSTOMER_SEGMENTS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_CUSTOMER_SEGMENTS_TL;

   if EXEC_CUSTOMER_SEGMENTS_TL_INS(O_error_message,
                                    L_customer_segments_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_CUSTOMER_SEGMENTS_TL_UPD(O_error_message,
                                    L_customer_segments_tl_upd_tab,
                                    L_cust_segments_tl_upd_rst,
                                    I_process_id,
                                    I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_CUSTOMER_SEGMENTS_TL_DEL(O_error_message,
                                    L_customer_segments_tl_del_tab,
                                    L_cust_segments_tl_del_rst,
                                    I_process_id,
                                    I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CUSTOMER_SEGMENTS_TL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_CUS_SMT_TY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_CUSTOMER_SEGMENT_TYPES.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_CUSTOMER_SEGMENT_TYPES.CHUNK_ID%TYPE)
  RETURN BOOLEAN is
   L_error               BOOLEAN;
   L_process_error       BOOLEAN;
   L_program             VARCHAR2(64)               :='CORESVC_CUSTOMER_SEGMENTS.PROCESS_CUS_SMT_TY';
   L_cus_smt_ty_temp_rec CUSTOMER_SEGMENT_TYPES%ROWTYPE;
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_CUSTOMER_SEGMENT_TYPES';
BEGIN
   FOR rec IN c_svc_cus_smt_ty(I_process_id,
                               I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

------for invalid action type
      if rec.action IS NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

------for duplicate customer_segment_type
      if rec.action = action_new
         and rec.pk_customer_segment_types_rid IS NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CUSTOMER_SEGMENT_TYPE',
                     'DUP_CUS_SMT_TY');
         L_error :=true;
      end if;

-------for modify/delete if customer_segment_type doesnt exists
      if rec.action = action_mod
         and rec.customer_segment_type is not NULL
         and rec.pk_customer_segment_types_rid IS NULL then
         write_error(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CUSTOMER_SEGMENT_TYPE',
                     'CUS_SMT_TY_NOT_EXIST');
         L_error :=true;
      end if;

--------for insert/modify if customer_segment_type_desc is NULL
      if rec.action in (action_new,action_mod)
         and NOT(  rec.customer_segment_type_desc  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CUSTOMER_SEGMENT_TYPE_DESC',
                     'CUS_SMT_TY_DES_REQ');
         L_error :=true;
      end if;

      if NOT L_error then
         L_cus_smt_ty_temp_rec.CUSTOMER_SEGMENT_TYPE              := rec.CUSTOMER_SEGMENT_TYPE;
         L_cus_smt_ty_temp_rec.CUSTOMER_SEGMENT_TYPE_DESC         := rec.CUSTOMER_SEGMENT_TYPE_DESC;


         if rec.action  =action_new then
            if EXEC_CUS_SMT_TY_INS(O_error_message,
                                   L_cus_smt_ty_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action  =action_mod then
            IF EXEC_CUS_SMT_TY_UPD(O_error_message,
                                   L_cus_smt_ty_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   end LOOP;
   return TRUE;
EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     if c_svc_cus_smt_ty%ISOPEN then
        close c_svc_cus_smt_ty;
     end if;
     return FALSE;
END PROCESS_CUS_SMT_TY;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_CUS_SMT_TY_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_CUSTOMER_SEGMENT_TYPES.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_CUSTOMER_SEGMENT_TYPES.CHUNK_ID%TYPE)
  RETURN BOOLEAN is
   L_error               BOOLEAN;
   L_process_error       BOOLEAN;
   L_program             VARCHAR2(64)               :='CORESVC_CUSTOMER_SEGMENTS.PROCESS_CUS_SMT_TY_DEL';
   L_cus_smt_ty_temp_rec CUSTOMER_SEGMENT_TYPES%ROWTYPE;
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_CUSTOMER_SEGMENT_TYPES';
BEGIN
   FOR rec IN C_SVC_CUS_SMT_TY_DEL(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

------for invalid action type
      if rec.action IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

-------for referential integrity  check
      if PROCESS_VAL_CUS_SMT_TY(O_error_message,
                                L_error,
                                rec) =FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
            L_error :=TRUE;
      end if;

      if NOT L_error then
         L_cus_smt_ty_temp_rec.CUSTOMER_SEGMENT_TYPE              := rec.CUSTOMER_SEGMENT_TYPE;
         L_cus_smt_ty_temp_rec.CUSTOMER_SEGMENT_TYPE_DESC         := rec.CUSTOMER_SEGMENT_TYPE_DESC;

         if rec.action  =action_del then
            if EXEC_CUS_SMT_TY_DEL(O_error_message,
                                   L_cus_smt_ty_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   end LOOP;
   return TRUE;
EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
     if c_svc_cus_smt_ty%ISOPEN then
        close c_svc_cus_smt_ty;
     end if;
     return FALSE;
END PROCESS_CUS_SMT_TY_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CUS_SMT_TY_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_CUSTOMER_SEGMENT_TYPES_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_CUSTOMER_SEGMENT_TYPES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64) := 'CORESVC_CUSTOMER_SEGMENTS.PROCESS_CUS_SMT_TY_TL';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CUSTOMER_SEGMENT_TYPES_TL';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CUSTOMER_SEGMENT_TYPES_TL';
   L_error                     BOOLEAN := FALSE;
   L_process_error             BOOLEAN := FALSE;
   L_cus_smt_ty_tl_temp_rec    CUSTOMER_SEGMENT_TYPES_TL%ROWTYPE;
   L_cus_smt_tl_upd_rst        ROW_SEQ_TAB;
   L_cus_smt_tl_del_rst        ROW_SEQ_TAB;

   cursor C_SVC_CUS_SMT_TY_TL(I_process_id NUMBER,
                              I_chunk_id   NUMBER) is
      select pk_cus_smt_ty_tl.rowid              as pk_cus_smt_ty_tl_rid,
             fk_customer_segment_types.rowid     as fk_customer_segment_types_rid,
			 fk_lang.rowid                       as fk_lang_rid,
			 fk_lang.lang,
             st.customer_segment_type,
             st.customer_segment_type_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             upper(st.action)                    as action,
             st.process$status
        from svc_customer_segment_types_tl  st,
             customer_segment_types         fk_customer_segment_types,
             customer_segment_types_tl      pk_cus_smt_ty_tl,
             lang                           fk_lang
       where st.process_id       =  I_process_id
         and st.chunk_id         =  I_chunk_id
         and st.customer_segment_type    =  fk_customer_segment_types.customer_segment_type (+)
         and st.lang             =  pk_cus_smt_ty_tl.lang (+)
         and UPPER(st.customer_segment_type)    =  UPPER(pk_cus_smt_ty_tl.customer_segment_type (+))
         and st.lang             =  fk_lang.lang (+);

   TYPE SVC_CUSTOMER_SEGMENT_TYPES_TL is TABLE OF C_SVC_CUS_SMT_TY_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_cus_smt_ty_tl_tab         SVC_CUSTOMER_SEGMENT_TYPES_TL;

   L_cus_smt_ty_tl_ins_tab         CUSTOMER_SEGMENT_TYPES_TL_TAB         := NEW CUSTOMER_SEGMENT_TYPES_TL_TAB();
   L_cus_smt_ty_tl_upd_tab         CUSTOMER_SEGMENT_TYPES_TL_TAB         := NEW CUSTOMER_SEGMENT_TYPES_TL_TAB();
   L_cus_smt_ty_tl_del_tab         CUSTOMER_SEGMENT_TYPES_TL_TAB         := NEW CUSTOMER_SEGMENT_TYPES_TL_TAB();

BEGIN
   if C_SVC_CUS_SMT_TY_TL%ISOPEN then
      close C_SVC_CUS_SMT_TY_TL;
   end if;

   open C_SVC_CUS_SMT_TY_TL(I_process_id,
                            I_chunk_id);
   LOOP
      fetch C_SVC_CUS_SMT_TY_TL bulk collect into L_svc_cus_smt_ty_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_cus_smt_ty_tl_tab.COUNT > 0 then
         FOR i in L_svc_cus_smt_ty_tl_tab.FIRST..L_svc_cus_smt_ty_tl_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_cus_smt_ty_tl_tab(i).action is NULL
               or L_svc_cus_smt_ty_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cus_smt_ty_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_cus_smt_ty_tl_tab(i).lang = LP_primary_lang and L_svc_cus_smt_ty_tl_tab(i).action = action_new then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cus_smt_ty_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               L_error :=TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_cus_smt_ty_tl_tab(i).action = action_new
               and L_svc_cus_smt_ty_tl_tab(i).pk_cus_smt_ty_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cus_smt_ty_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_cus_smt_ty_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_cus_smt_ty_tl_tab(i).lang is NOT NULL
               and L_svc_cus_smt_ty_tl_tab(i).customer_segment_type is NOT NULL
               and L_svc_cus_smt_ty_tl_tab(i).pk_cus_smt_ty_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_cus_smt_ty_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_cus_smt_ty_tl_tab(i).action = action_new
               and L_svc_cus_smt_ty_tl_tab(i).customer_segment_type is NULL
               and L_svc_cus_smt_ty_tl_tab(i).fk_customer_segment_types_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_cus_smt_ty_tl_tab(i).row_seq,
                            NULL,
                           'CUS_SMT_TY_NOT_EXIST');
               L_error :=TRUE;
            end if;

            if L_svc_cus_smt_ty_tl_tab(i).action = action_new
               and L_svc_cus_smt_ty_tl_tab(i).lang is NOT NULL
               and L_svc_cus_smt_ty_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cus_smt_ty_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_cus_smt_ty_tl_tab(i).customer_segment_type is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cus_smt_ty_tl_tab(i).row_seq,
                           'CUSTOMER_SEGMENT_TYPE',
                           'CUS_SMT_TY_REQ');
               L_error :=TRUE;
            end if;

            if L_svc_cus_smt_ty_tl_tab(i).customer_segment_type_desc is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cus_smt_ty_tl_tab(i).row_seq,
                           'CUSTOMER_SEGMENT_TYPE_DESC',
                           'CUS_SMT_TY_DES_REQ');
               L_error :=TRUE;
            end if;

            if L_svc_cus_smt_ty_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_cus_smt_ty_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_cus_smt_ty_tl_temp_rec.lang := L_svc_cus_smt_ty_tl_tab(i).lang;
               L_cus_smt_ty_tl_temp_rec.customer_segment_type := L_svc_cus_smt_ty_tl_tab(i).customer_segment_type;
               L_cus_smt_ty_tl_temp_rec.customer_segment_type_desc := L_svc_cus_smt_ty_tl_tab(i).customer_segment_type_desc;
               L_cus_smt_ty_tl_temp_rec.create_datetime := SYSDATE;
               L_cus_smt_ty_tl_temp_rec.create_id := GET_USER;
               L_cus_smt_ty_tl_temp_rec.last_update_datetime := SYSDATE;
               L_cus_smt_ty_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_cus_smt_ty_tl_tab(i).action = action_new then
                  L_cus_smt_ty_tl_ins_tab.extend;
                  L_cus_smt_ty_tl_ins_tab(L_cus_smt_ty_tl_ins_tab.count()) := L_cus_smt_ty_tl_temp_rec;
               end if;

               if L_svc_cus_smt_ty_tl_tab(i).action = action_mod then
                  L_cus_smt_ty_tl_upd_tab.extend;
                  L_cus_smt_ty_tl_upd_tab(L_cus_smt_ty_tl_upd_tab.count()) := L_cus_smt_ty_tl_temp_rec;
                  L_cus_smt_tl_upd_rst(L_cus_smt_ty_tl_upd_tab.count()) := L_svc_cus_smt_ty_tl_tab(i).row_seq;
               end if;

               if L_svc_cus_smt_ty_tl_tab(i).action = action_del then
                  L_cus_smt_ty_tl_del_tab.extend;
                  L_cus_smt_ty_tl_del_tab(L_cus_smt_ty_tl_del_tab.count()) := L_cus_smt_ty_tl_temp_rec;
                  L_cus_smt_tl_del_rst(L_cus_smt_ty_tl_del_tab.count()) := L_svc_cus_smt_ty_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_CUS_SMT_TY_TL%NOTFOUND;
   END LOOP;
   close C_SVC_CUS_SMT_TY_TL;

   if EXEC_CUS_SMT_TY_TL_INS(O_error_message,
                             L_cus_smt_ty_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_CUS_SMT_TY_TL_UPD(O_error_message,
                             L_cus_smt_ty_tl_upd_tab,
                             L_cus_smt_tl_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_CUS_SMT_TY_TL_DEL(O_error_message,
                             L_cus_smt_ty_tl_del_tab,
                             L_cus_smt_tl_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CUS_SMT_TY_TL;
-----------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_customer_segments_tl
      where process_id = I_process_id;

   delete from svc_customer_segments
      where process_id = I_process_id;

   delete from svc_customer_segment_types_tl
      where process_id = I_process_id;

   delete from svc_customer_segment_types
      where process_id = I_process_id;
END;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count   IN OUT NUMBER,
                 I_process_id    IN     NUMBER,
                 I_chunk_id      IN     NUMBER)
  RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_CUSTOMER_SEGMENTS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   Lp_errors_tab := NEW errors_tab_typ();

   if process_CUS_SMT_TY(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_CUS_SMT_TY_TL(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_CUS_SMTS(O_error_message,
                       I_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_CUSTOMER_SEGMENTS_TL(O_error_message,
                                   I_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   if PROCESS_CUS_SMT_TY_DEL(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
          set status = (CASE
                          when status = 'PE'
                          then 'PE'
                    else L_process_status
                    END),
              action_date = SYSDATE
        where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_CUSTOMER_SEGMENTS;
/
