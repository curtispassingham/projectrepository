
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY MERCH_RECLASS_SQL AS

   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_PEND_MERCH_HIER_TL
   -- Purpose      : This function will lock the PEND_MERCH_HIER_TL table for delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_PEND_MERCH_HIER_TL(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_hier_type                   IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                                 I_merch_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                                 I_merch_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                                 I_merch_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_PEND_MERCH_HIER
   -- Purpose      : This function will lock the PEND_MERCH_HIER table for update and delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_PEND_MERCH_HIER(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hier_type                   IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                              I_merch_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                              I_merch_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                              I_merch_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'MERCH_RECLASS_SQL.INSERT_RECLASS';

BEGIN

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'PEND_MERCH_HIER',
                    ' hier_type: '||I_pend_merch_hier_rec.hier_type||
                    ' merch_hier_id: '  ||TO_CHAR(I_pend_merch_hier_rec.merch_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(I_pend_merch_hier_rec.merch_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(I_pend_merch_hier_rec.merch_hier_grandparent_id));

   insert into pend_merch_hier (action_type,
                                effective_date,
                                hier_type,
                                merch_hier_id,
                                merch_hier_parent_id,
                                merch_hier_grandparent_id,
                                merch_hier_name,
                                domain,
                                buyer,
                                merch,
                                profit_calc_type,
                                purchase_type,
                                bud_int,
                                bud_mkup,
                                total_market_amt,
                                markup_calc_type,
                                otb_calc_type,
                                max_avg_counter,
                                avg_tolerance_pct,
                                dept_vat_incl_ind,
                                class_vat_incl_ind)
                        values (I_pend_merch_hier_rec.action_type,
                                I_pend_merch_hier_rec.effective_date,
                                I_pend_merch_hier_rec.hier_type,
                                I_pend_merch_hier_rec.merch_hier_id,
                                I_pend_merch_hier_rec.merch_hier_parent_id,
                                I_pend_merch_hier_rec.merch_hier_grandparent_id,
                                I_pend_merch_hier_rec.merch_hier_name,
                                I_pend_merch_hier_rec.domain,
                                I_pend_merch_hier_rec.buyer,
                                I_pend_merch_hier_rec.merch,
                                I_pend_merch_hier_rec.profit_calc_type,
                                I_pend_merch_hier_rec.purchase_type,
                                I_pend_merch_hier_rec.bud_int,
                                I_pend_merch_hier_rec.bud_mkup,
                                I_pend_merch_hier_rec.total_market_amt,
                                I_pend_merch_hier_rec.markup_calc_type,
                                I_pend_merch_hier_rec.otb_calc_type,
                                I_pend_merch_hier_rec.max_avg_counter,
                                I_pend_merch_hier_rec.avg_tolerance_pct,
                                I_pend_merch_hier_rec.dept_vat_incl_ind,
                                I_pend_merch_hier_rec.class_vat_incl_ind);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_RECLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'MERCH_RECLASS_SQL.UPDATE_RECLASS';

   -- used for locking and update where clause
   L_hier_id              PEND_MERCH_HIER.MERCH_HIER_ID%TYPE;
   L_hier_parent_id       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE;
   L_hier_grandparent_id  PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE;

BEGIN

   -- since update can involve a merch reclassification, to determine which
   -- record to lock and update, parent id should only be used for class and subclass,
   -- grandparent id should only be used for subclass.
   if not DETERMINE_HIERARCHY(O_error_message,
                              L_hier_id,
                              L_hier_parent_id,
                              L_hier_grandparent_id,
                              I_pend_merch_hier_rec.hier_type,
                              I_pend_merch_hier_rec.merch_hier_id,
                              I_pend_merch_hier_rec.merch_hier_parent_id,
                              I_pend_merch_hier_rec.merch_hier_grandparent_id) then
         return FALSE;
   end if;

   if not LOCK_PEND_MERCH_HIER(O_error_message,
                               I_pend_merch_hier_rec.hier_type,
                               L_hier_id,
                               L_hier_parent_id,
                               L_hier_grandparent_id) then
         return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'PEND_MERCH_HIER',
                    ' hier type: ' || I_pend_merch_hier_rec.hier_type||
                    ' merch_hier_id: '  ||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id : '||TO_CHAR(L_hier_grandparent_id));

   update pend_merch_hier
      set action_type               = NVL(I_pend_merch_hier_rec.action_type, action_type),
          effective_date            = I_pend_merch_hier_rec.effective_date,
          hier_type                 = I_pend_merch_hier_rec.hier_type,
          merch_hier_parent_id      = I_pend_merch_hier_rec.merch_hier_parent_id,
          merch_hier_grandparent_id = I_pend_merch_hier_rec.merch_hier_grandparent_id,
          merch_hier_name           = I_pend_merch_hier_rec.merch_hier_name,
          domain                    = I_pend_merch_hier_rec.domain,
          buyer                     = I_pend_merch_hier_rec.buyer,
          merch                     = I_pend_merch_hier_rec.merch,
          profit_calc_type          = I_pend_merch_hier_rec.profit_calc_type,
          purchase_type             = I_pend_merch_hier_rec.purchase_type,
          bud_int                   = I_pend_merch_hier_rec.bud_int,
          bud_mkup                  = I_pend_merch_hier_rec.bud_mkup,
          total_market_amt          = I_pend_merch_hier_rec.total_market_amt,
          markup_calc_type          = I_pend_merch_hier_rec.markup_calc_type,
          otb_calc_type             = I_pend_merch_hier_rec.otb_calc_type,
          max_avg_counter           = I_pend_merch_hier_rec.max_avg_counter,
          avg_tolerance_pct         = I_pend_merch_hier_rec.avg_tolerance_pct,
          dept_vat_incl_ind         = I_pend_merch_hier_rec.dept_vat_incl_ind,
          class_vat_incl_ind        = I_pend_merch_hier_rec.class_vat_incl_ind
    where hier_type = I_pend_merch_hier_rec.hier_type
      and merch_hier_id = L_hier_id
      and NVL(merch_hier_parent_id,-999) =
          NVL(L_hier_parent_id, NVL(merch_hier_parent_id,-999))
      and NVL(merch_hier_grandparent_id,-999) =
          NVL(L_hier_grandparent_id, NVL(merch_hier_grandparent_id,-999));

   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END UPDATE_RECLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec  IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_RECLASS_SQL.DELETE_RECLASS';

   -- used for locking and delete where clause
   L_hier_id	        PEND_MERCH_HIER.MERCH_HIER_ID%TYPE;
   L_hier_parent_id       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE;
   L_hier_grandparent_id  PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE;

BEGIN

   -- To determine which record to lock and delete, parent id should only be used
   -- for class and subclass, grandparent id should only be used for subclass.
   if not DETERMINE_HIERARCHY(O_error_message,
                              L_hier_id,
                              L_hier_parent_id,
                              L_hier_grandparent_id,
                              I_pend_merch_hier_rec.hier_type,
                              I_pend_merch_hier_rec.merch_hier_id,
                              I_pend_merch_hier_rec.merch_hier_parent_id,
                              I_pend_merch_hier_rec.merch_hier_grandparent_id) then
         return FALSE;
   end if;

   if not LOCK_PEND_MERCH_HIER_TL(O_error_message,
                               I_pend_merch_hier_rec.hier_type,
                               L_hier_id,
                               L_hier_parent_id,
                               L_hier_grandparent_id) then
         return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'PEND_MERCH_HIER_TL',
                    ' hier type: ' || I_pend_merch_hier_rec.hier_type||
                    ' merch_hier_id: '  ||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id : '||TO_CHAR(L_hier_grandparent_id));

   delete from PEND_MERCH_HIER_TL
    where hier_type = I_pend_merch_hier_rec.hier_type
      and merch_hier_id = I_pend_merch_hier_rec.merch_hier_id
      and NVL(merch_hier_parent_id, -999) =
          NVL(L_hier_parent_id, NVL(merch_hier_parent_id, -999))
      and NVL(merch_hier_grandparent_id, -999) =
          NVL(L_hier_grandparent_id, NVL(merch_hier_grandparent_id, -999));
   
    if not LOCK_PEND_MERCH_HIER(O_error_message,
                               I_pend_merch_hier_rec.hier_type,
                               L_hier_id,
                               L_hier_parent_id,
                               L_hier_grandparent_id) then
         return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'PEND_MERCH_HIER',
                    ' hier type: ' || I_pend_merch_hier_rec.hier_type||
                    ' merch_hier_id: '  ||TO_CHAR(L_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(L_hier_parent_id)||
                    ' merch_hier_grandparent_id : '||TO_CHAR(L_hier_grandparent_id));

   delete from PEND_MERCH_HIER
    where hier_type = I_pend_merch_hier_rec.hier_type
      and merch_hier_id = I_pend_merch_hier_rec.merch_hier_id
      and NVL(merch_hier_parent_id, -999) =
          NVL(L_hier_parent_id, NVL(merch_hier_parent_id, -999))
      and NVL(merch_hier_grandparent_id, -999) =
          NVL(L_hier_grandparent_id, NVL(merch_hier_grandparent_id, -999));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DELETE_RECLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_PEND_MERCH_HIER_TL(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hier_type                   IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                              I_merch_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                              I_merch_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                              I_merch_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'MERCH_RECLASS_SQL.LOCK_PEND_MERCH_HIER_TL';
   L_table     VARCHAR2(50)  := 'PEND_MERCH_HIER_TL';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PEND_MERCH_HIER_TL IS
      select 'x'
        from pend_merch_hier_tl
       where hier_type = I_hier_type
         and merch_hier_id = I_merch_hier_id
         and NVL(merch_hier_parent_id, -999) =
             NVL(I_merch_hier_parent_id, NVL(merch_hier_parent_id, -999))
         and NVL(merch_hier_grandparent_id, -999) =
             NVL(I_merch_hier_grandparent_id, NVL(merch_hier_grandparent_id, -999))
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_PEND_MERCH_HIER_TL',
                    'PEND_MERCH_HIER_TL',
                    ' hier_type: '||I_hier_type ||
                    ' merch_hier_id: '  ||TO_CHAR(I_merch_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(I_merch_hier_parent_id)||
                    ' merch_hier_grandparent_id : '||TO_CHAR(I_merch_hier_grandparent_id));
   open C_LOCK_PEND_MERCH_HIER_TL;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_PEND_MERCH_HIER_TL',
                    'PEND_MERCH_HIER_TL',
                    ' hier_type: '||I_hier_type||
                    ' merch_hier_id: '  ||TO_CHAR(I_merch_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(I_merch_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(I_merch_hier_grandparent_id));
   close C_LOCK_PEND_MERCH_HIER_TL;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            ' hier_type: '||I_hier_type,
                                            ' merch_hier_id: ' ||TO_CHAR(I_merch_hier_id)||
                                            ' merch_hier_parent_id: '||TO_CHAR(I_merch_hier_parent_id)||
                                            ' merch_hier_grandparent_id: '||TO_CHAR(I_merch_hier_grandparent_id));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_PEND_MERCH_HIER_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_PEND_MERCH_HIER(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hier_type                   IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                              I_merch_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                              I_merch_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                              I_merch_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'MERCH_RECLASS_SQL.LOCK_PEND_MERCH_HIER';
   L_table     VARCHAR2(30)  := 'PEND_MERCH_HIER';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PEND_MERCH_HIER IS
      select 'x'
        from pend_merch_hier
       where hier_type = I_hier_type
         and merch_hier_id = I_merch_hier_id
         and NVL(merch_hier_parent_id, -999) =
             NVL(I_merch_hier_parent_id, NVL(merch_hier_parent_id, -999))
         and NVL(merch_hier_grandparent_id, -999) =
             NVL(I_merch_hier_grandparent_id, NVL(merch_hier_grandparent_id, -999))
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_PEND_MERCH_HIER',
                    'PEND_MERCH_HIER',
                    ' hier_type: '||I_hier_type ||
                    ' merch_hier_id: '  ||TO_CHAR(I_merch_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(I_merch_hier_parent_id)||
                    ' merch_hier_grandparent_id : '||TO_CHAR(I_merch_hier_grandparent_id));
   open C_LOCK_PEND_MERCH_HIER;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_PEND_MERCH_HIER',
                    'PEND_MERCH_HIER',
                    ' hier_type: '||I_hier_type||
                    ' merch_hier_id: '  ||TO_CHAR(I_merch_hier_id)||
                    ' merch_hier_parent_id: '||TO_CHAR(I_merch_hier_parent_id)||
                    ' merch_hier_grandparent_id: '||TO_CHAR(I_merch_hier_grandparent_id));
   close C_LOCK_PEND_MERCH_HIER;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            ' hier_type: '||I_hier_type,
                                            ' merch_hier_id: ' ||TO_CHAR(I_merch_hier_id)||
                                            ' merch_hier_parent_id: '||TO_CHAR(I_merch_hier_parent_id)||
                                            ' merch_hier_grandparent_id: '||TO_CHAR(I_merch_hier_grandparent_id));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END LOCK_PEND_MERCH_HIER;
-------------------------------------------------------------------------------------------------
FUNCTION DETERMINE_HIERARCHY(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_hier_id               IN OUT   PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                             O_hier_parent_id        IN OUT   PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                             O_hier_grandparent_id   IN OUT   PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE,
                             I_hier_type             IN       PEND_MERCH_HIER.HIER_TYPE%TYPE,
                             I_hier_id               IN       PEND_MERCH_HIER.MERCH_HIER_ID%TYPE,
                             I_hier_parent_id        IN       PEND_MERCH_HIER.MERCH_HIER_PARENT_ID%TYPE,
                             I_hier_grandparent_id   IN       PEND_MERCH_HIER.MERCH_HIER_GRANDPARENT_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'MERCH_RECLASS_SQL.DETERMINE_HIERARCHY';

BEGIN

   -- This method will help determine the correct merch hierarchy to lock, update and delete.
   -- It will return null parent id for hier types other than class and subclass
   -- and return null grandparent id for hier types other than subclass.

   O_hier_id := I_hier_id;
   O_hier_parent_id := NULL;
   O_hier_grandparent_id := NULL;

   if (I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_cls_code or
       I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_hier_parent_id := I_hier_parent_id;
   end if;

   if (I_hier_type = MERCH_RECLASS_VALIDATE_SQL.LP_scls_code) then
      O_hier_grandparent_id := I_hier_grandparent_id;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DETERMINE_HIERARCHY;
-------------------------------------------------------------------------------------------------
END MERCH_RECLASS_SQL;
/
