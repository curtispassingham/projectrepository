CREATE OR REPLACE PACKAGE BODY ITEMLOC_UPDATE_SQL AS
-----------------------------------------------------------------------------------------------
FUNCTION REC_COST_ADJ_INVENTORY (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_new_wac_loc           IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                                 O_neg_soh_wac_adj_amt   IN OUT   ITEM_LOC_SOH.AV_COST%TYPE,
                                 O_adj_qty               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                 I_location              IN       ORDLOC.LOCATION%TYPE,
                                 I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE,
                                 I_new_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_old_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_receipt_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_recalc_ind            IN       VARCHAR2,
                                 I_order_number          IN       ORDHEAD.ORDER_NO%TYPE   DEFAULT NULL,
                                 I_ref_no_2              IN       TRAN_DATA.REF_NO_2%TYPE DEFAULT NULL,
                                 I_pack_item             IN       ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                 I_pgm_name              IN       TRAN_DATA.PGM_NAME%TYPE DEFAULT NULL,
                                 I_adj_code              IN       TRAN_DATA.ADJ_CODE%TYPE DEFAULT 'C',
                                 I_shipment              IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                 I_new_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE  DEFAULT NULL,
                                 I_old_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE  DEFAULT NULL,
                                 I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N',
                                 I_posting_ind           IN       BOOLEAN DEFAULT TRUE)

RETURN BOOLEAN IS

   L_l10n_fin_rec           L10N_FIN_REC := L10N_FIN_REC();

   L_stock_on_hand          ITEM_LOC_SOH.STOCK_ON_HAND%TYPE   := 0;
   L_old_av_cost_loc        ITEM_LOC_SOH.AV_COST%TYPE         := 0;
   L_new_av_cost_loc        ITEM_LOC_SOH.AV_COST%TYPE         := NULL;
   L_temp_av_cost_loc       ITEM_LOC_SOH.AV_COST%TYPE;
   L_neg_soh_wac_adj_amt    ITEM_LOC_SOH.AV_COST%TYPE         := 0;
   L_neg_soh_wac_adj_qty    ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

   L_recalc_ind             VARCHAR2(1)    := NVL(I_recalc_ind, 'Y');
   L_item_master            ITEM_MASTER%ROWTYPE;
   L_tran_code              TRAN_DATA.TRAN_CODE%TYPE;
   L_tran_date              DATE           := get_vdate;
   L_post_date              DATE;

   L_rowid                  ROWID;
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);

   L_loc_string             VARCHAR2(25);
   L_table                  VARCHAR2(30);
   L_key1                   VARCHAR2(100);
   L_key2                   VARCHAR2(100);
   L_systems_options_row    SYSTEM_OPTIONS%ROWTYPE;
   L_adj_qty                ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_program                VARCHAR2(45)   := 'ITEMLOC_UPDATE_SQL.REC_COST_ADJ_INVENTORY';
   L_new_cst                TRAN_DATA.TOTAL_COST%TYPE;
   L_tran_data_units        TRAN_DATA.UNITS%TYPE;
   L_tot_cost_excl_elc      TRAN_DATA.TOTAL_COST_EXCL_ELC%TYPE;

   L_loc                    SHIPMENT.TO_LOC%TYPE;
   L_process_item           ITEM_MASTER.ITEM%TYPE;
   L_pack_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_receive_as_type        ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_shs_qty_adjust         SHIPSKU.QTY_RECEIVED%TYPE;
   L_vir_qty_match          SHIPSKU.QTY_MATCHED%TYPE;
   L_backpost_rca_rua_ind   PROCUREMENT_UNIT_OPTIONS.BACKPOST_RCA_RUA_IND%TYPE;
   L_ref_pack_no            TRAN_DATA.REF_PACK_NO%TYPE := I_pack_item;
   L_fifo_on_hand           ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_new_on_hand            ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_temp_on_hand           ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_first_received         DATE;
   L_wf_order_po            ORDHEAD.WF_ORDER_NO%TYPE;
   L_shipment               SHIPMENT.SHIPMENT%TYPE;
   L_location               ITEM_LOC.LOC%TYPE;
   L_loc_type               ITEM_LOC.LOC_TYPE%TYPE;
   L_old_cost               ITEM_LOC_SOH.AV_COST%TYPE := I_old_cost;
   L_new_cost               ITEM_LOC_SOH.AV_COST%TYPE := I_new_cost;
   L_new_cost_excl_elc      ORDLOC.UNIT_COST%TYPE := I_new_cost_excl_elc;
   L_old_cost_excl_elc      ORDLOC.UNIT_COST%TYPE := I_old_cost_excl_elc;

   cursor C_PHY_SHIPMENT is
      select physical_wh
        from wh
       where wh = I_location;

   cursor C_GET_PACK_COMP_QTY is
      select qty
        from v_packsku_qty
       where pack_no = I_pack_item
         and item = I_item;

   cursor C_FIRST_RECEIVED is
      select MIN(s2.receive_date)
        from shipment s2
       where s2.order_no = I_order_number
         and s2.to_loc = L_loc
         and s2.shipment = NVL(I_shipment,s2.shipment)
         and I_adj_code in ('A','C');


   cursor C_SHIPMENT is
      with matching_items   -- the first subquery handles the receipt for L_process_item
         as(select (case when (L_systems_options_row.rcv_cost_adj_type = 'S')
                          and I_pack_item is not null
                          and L_receive_as_type = 'E' then I_pack_item
                         else L_process_item
                     end) item,
                   (case when (L_systems_options_row.rcv_cost_adj_type = 'F')
                          and  I_pack_item is not null
                          and  L_receive_as_type = 'E' then 1
                          else L_pack_qty
                     end) comp_qty
              from dual
             union   -- the second subquery handles pack items broken on receipt*/
            select il.item,
                   v.qty comp_qty -- pack component quantity
              from v_packsku_qty v,
                   item_loc il
             where L_receive_as_type = 'E'
               and il.item = v.pack_no
               and il.loc = I_location
               and nvl(il.receive_as_type,'E') = 'E'
               and v.item = I_item
               and L_systems_options_row.rcv_cost_adj_type = 'F'
             )
      select sh.order_no,
             nvl(sh.parent_shipment,sh.shipment) shipment,
             min(sh.receive_date) receive_date,
             sum(ss.qty_received) qty_received,
             sum(ss.qty_received) vir_qty_received,
             sum(nvl(decode(sh.invc_match_status, 'M', ss.qty_received, ss.qty_matched),0)) qty_matched,
             ss.item,
             ss.seq_no,
             i.comp_qty
        from matching_items i,
             shipment sh,
             shipsku ss
       where I_loc_type !='W'
         and ss.item         = i.item
         and sh.shipment     = ss.shipment
         and sh.order_no     is not null
         and sh.to_loc       = I_location
         and ss.qty_received != 0
         and ((L_systems_options_row.rcv_cost_adj_type = 'F' and sh.receive_date >= L_first_received)
          or ((L_systems_options_row.rcv_cost_adj_type = 'S' and sh.order_no = I_order_number and I_adj_code<>'A' )
          or (L_systems_options_row.rcv_cost_adj_type = 'S' and sh.order_no = I_order_number and sh.shipment = nvl(I_Shipment,sh.shipment)
         and I_adj_code='A' )))
       group by sh.order_no, nvl(sh.parent_shipment,sh.shipment), sh.receive_date, ss.item, ss.seq_no, i.comp_qty
       union all
      select /*+ index (ssl pk_shipsku_loc) */
             sh.order_no,
             nvl(sh.parent_shipment,sh.shipment) shipment,
             min(sh.receive_date) receive_date,
             sum(ss.qty_received) qty_received,
             sum(ssl.qty_received) vir_qty_received,
             sum(nvl(decode(sh.invc_match_status, 'M', ss.qty_received, ss.qty_matched),0)) qty_matched,
             ss.item,
             ss.seq_no,
             i.comp_qty
        from matching_items i,
             shipment sh,
             shipsku ss,
             shipsku_loc ssl
       where I_loc_type ='W'
         and ss.item             = i.item
         and sh.shipment         = ss.shipment
         and sh.order_no         is not null
         and ssl.shipment        = ss.shipment
         and ssl.seq_no          = ss.seq_no
         and ssl.item            = ss.item
         and ssl.to_loc          = I_location
         and ssl.qty_received   != 0
         and ((L_systems_options_row.rcv_cost_adj_type = 'F' and sh.receive_date >= L_first_received)
          or ((L_systems_options_row.rcv_cost_adj_type = 'S' and sh.order_no = I_order_number and I_adj_code<>'A' )
          or (L_systems_options_row.rcv_cost_adj_type = 'S' and sh.order_no = I_order_number and sh.shipment = nvl(I_Shipment,sh.shipment)
         and I_adj_code='A' )))
       group by sh.order_no, nvl(sh.parent_shipment,sh.shipment), sh.receive_date, ss.item, ss.seq_no, i.comp_qty
       order by receive_date desc,  order_no, item , seq_no;

   cursor C_PROCUREMENT_UNIT_OPTIONS is
      select backpost_rca_rua_ind
        from procurement_unit_options;

   cursor C_GET_WF_ORDER_NO is 
      select wf_order_no
        from ordhead
       where order_no = I_order_number;
BEGIN

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_master,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_LOC';
   L_key1  := I_item;
   L_key2  := TO_CHAR(I_location);
   ---
   if L_item_master.item_level = L_item_master.tran_level then
      L_loc_string := ' Location: '||TO_CHAR(I_location);

      if I_pack_item is NULL then
         L_process_item := I_item;
         L_receive_as_type := 'E';
         L_pack_qty := 1;
      else
         if I_loc_type = 'W' then
            if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                      L_receive_as_type,
                                                      I_pack_item,
                                                      I_location) = FALSE then
               return FALSE;
            end if;
         else
            L_receive_as_type := 'E';
         end if;

         if L_receive_as_type = 'E' then
            L_process_item := I_item;
         else
            L_process_item := I_pack_item;
         end if;

         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_PACK_COMP_QTY',
                          NULL,
                          'Pack Item: '||TO_CHAR(I_pack_item)|| ', Component Item: '||TO_CHAR(I_item));
         open C_GET_PACK_COMP_QTY;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_PACK_COMP_QTY',
                          NULL,
                          'Pack Item: '||TO_CHAR(I_pack_item)|| ', Component Item: '||TO_CHAR(I_item));
         fetch C_GET_PACK_COMP_QTY into L_pack_qty;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_PACK_COMP_QTY',
                          NULL,
                          'Pack Item: '||TO_CHAR(I_pack_item)|| ', Component Item: '||TO_CHAR(I_item));
         close C_GET_PACK_COMP_QTY;

         if L_pack_qty is null then
            O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                   L_program,
                                                   'V_PACKSKU_QTY',
                                                   'PACK_NO:'||I_pack_item||', ITEM:'||I_item);
            return FALSE;
         end if;
      end if;
         ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_WF_ORDER_NO',
                        'ORDHEAD',
                        'Order No: ' || I_order_number);
      open C_GET_WF_ORDER_NO;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_WF_ORDER_NO',
                        'ORDHEAD',
                        'Order No: ' || I_order_number);
      fetch C_GET_WF_ORDER_NO into L_wf_order_po;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_WF_ORDER_NO',
                        'ORDHEAD',
                        'Order No: ' || I_order_number);
      close C_GET_WF_ORDER_NO;
      -- For a franchise order RCA, WAC recalculation and Tran Data postings  
      -- should be done at costing location.
      if L_wf_order_po is NOT NULL then
         select costing_loc,
                costing_loc_type
           into L_location,
                L_loc_type
           from item_loc
          where item = I_item
            and loc = I_location
            and loc_type = I_loc_type; 

         -- Convert Input Costs to Costing Loc Currency
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             L_location,
                                             L_loc_type,
                                             NULL,
                                             L_old_cost,
                                             L_old_cost,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             L_location,
                                             L_loc_type,
                                             NULL,
                                             L_new_cost,
                                             L_new_cost,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             L_location,
                                             L_loc_type,
                                             NULL,
                                             L_old_cost_excl_elc,
                                             L_old_cost_excl_elc,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             L_location,
                                             L_loc_type,
                                             NULL,
                                             L_new_cost_excl_elc,
                                             L_new_cost_excl_elc,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
      else
         L_location := I_location;
         L_loc_type := I_loc_type;
      end if;
      ---
      L_l10n_fin_rec.procedure_key := 'GET_CURRENT_SOH_WAC';

      if I_order_number is Not Null then
         L_l10n_fin_rec.doc_type   := 'PO';
         L_l10n_fin_rec.doc_id     := I_order_number;
      end if;

      L_l10n_fin_rec.source_entity := 'LOC';
      L_l10n_fin_rec.source_id     := L_location;
      L_l10n_fin_rec.source_type   := L_loc_type;
      L_l10n_fin_rec.item          := L_process_item;
      L_l10n_fin_rec.calc_fld_type := 'TOTSTK';

      --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.GET_CURRENT_SOH_WAC to
      --get the SOH for all the virtual locations having the same CNPJ number.
      --For non-localized country, the L10N_SQL.EXEC_FUNCTION will call ITEMLOC_ATTRIB_SQL.GET_CURRENT_SOH_WAC to
      --get the SOH of a particular location.
      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_fin_rec) = FALSE then
         return FALSE;
      end if;

      L_stock_on_hand   := L_l10n_fin_rec.stock_on_hand;
      L_old_av_cost_loc := L_l10n_fin_rec.av_cost;
      L_new_av_cost_loc := L_old_av_cost_loc;
      L_rowid           := CHARTOROWID(L_l10n_fin_rec.fin_rowid);

      if L_process_item != I_item then
         L_stock_on_hand := L_pack_qty * L_stock_on_hand;
         L_l10n_fin_rec               := L10N_FIN_REC();
         L_l10n_fin_rec.procedure_key := 'GET_CURRENT_SOH_WAC';
         if I_order_number is Not Null then
            L_l10n_fin_rec.doc_type   := 'PO';
            L_l10n_fin_rec.doc_id     := I_order_number;
         end if;

         L_l10n_fin_rec.source_entity := 'LOC';
         L_l10n_fin_rec.source_id     := L_location;
         L_l10n_fin_rec.source_type   := L_loc_type;
         L_l10n_fin_rec.item          := I_item;
         L_l10n_fin_rec.calc_fld_type := 'TOTSTK';

         --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.GET_CURRENT_SOH_WAC to
         --get the SOH for all the virtual locations having the same CNPJ number.
         --For non-localized country, the L10N_SQL.EXEC_FUNCTION will call ITEMLOC_ATTRIB_SQL.GET_CURRENT_SOH_WAC to
         --get the SOH of a particular location.
         if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                    L_l10n_fin_rec) = FALSE then
            return FALSE;
         end if;
         L_old_av_cost_loc := L_l10n_fin_rec.av_cost;
         L_new_av_cost_loc := L_old_av_cost_loc;
         L_rowid           := CHARTOROWID(L_l10n_fin_rec.fin_rowid);
      end if;
      ---
      if L_recalc_ind = 'Y' then
         ---
         if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                  L_systems_options_row) = FALSE then
            return FALSE;
         end if;
         ---
         open C_PROCUREMENT_UNIT_OPTIONS;
         SQL_LIB.SET_MARK('OPEN',
                           'C_PROCUREMENT_UNIT_OPTIONS',
                           NULL,
                           NULL);
         fetch C_PROCUREMENT_UNIT_OPTIONS into L_backpost_rca_rua_ind;
         SQL_LIB.SET_MARK('FETCH',
                           'C_PROCUREMENT_UNIT_OPTIONS',
                           NULL,
                           NULL);
         close C_PROCUREMENT_UNIT_OPTIONS;
         SQL_LIB.SET_MARK('CLOSE',
                           'C_PROCUREMENT_UNIT_OPTIONS',
                           NULL,
                           NULL);

         if I_loc_type = 'W' then
            SQL_LIB.SET_MARK('OPEN',
                             'C_PHY_SHIPMENT',
                             NULL,
                             'Location: '||TO_CHAR(I_location));
            open C_PHY_SHIPMENT;
            SQL_LIB.SET_MARK('FETCH',
                             'C_PHY_SHIPMENT',
                             NULL,
                             'Location: '||TO_CHAR(I_location));
            fetch C_PHY_SHIPMENT into L_loc;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_PHY_SHIPMENT',
                             NULL,
                             'Location: '||TO_CHAR(I_location));
            close C_PHY_SHIPMENT;
            if L_loc is null then
               O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                     L_program,
                                                     'WH',
                                                     'WH:'||I_location);
               return FALSE;
            end if;
         else
            L_loc := I_location;
         end if;

         SQL_LIB.SET_MARK('OPEN',
                          'C_FIRST_RECEIVED',
                          NULL,
                          'Order no: ' || I_order_number || ', To loc: '||L_loc);
         open C_FIRST_RECEIVED;
         SQL_LIB.SET_MARK('FETCH',
                          'C_FIRST_RECEIVED',
                          NULL,
                          'Order no: ' || I_order_number || ', To loc: '||L_loc);
         fetch C_FIRST_RECEIVED into L_first_received;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_FIRST_RECEIVED',
                           NULL,
                          'Order no: ' || I_order_number || ', To loc: '||L_loc);
         close C_FIRST_RECEIVED;
         L_fifo_on_hand := L_stock_on_hand;
         L_new_on_hand := L_stock_on_hand;
         L_temp_on_hand := L_stock_on_hand;
         L_temp_av_cost_loc := L_old_av_cost_loc;

         for rec in C_SHIPMENT LOOP
            if (I_adjust_matched_ind ='Y'  or I_adj_code = 'A') then
               L_shs_qty_adjust := rec.vir_qty_received ;
            else
               if I_loc_type = 'W' then
                  if rec.qty_matched > 0 then
                     if REC_COST_ADJ_SQL.GET_VIR_UNMATCHED_QTY(O_error_message,
                                                               L_vir_qty_match,
                                                               rec.item,
                                                               rec.order_no,
                                                               rec.shipment,
                                                               rec.seq_no,
                                                               L_loc,
                                                               I_location,
                                                               rec.qty_matched) = FALSE then
                        return FALSE;
                     end if;
                     L_shs_qty_adjust := rec.vir_qty_received - L_vir_qty_match;
                  else
                     L_shs_qty_adjust := rec.vir_qty_received;
                  end if; 
               else
                  L_shs_qty_adjust := rec.qty_received - rec.qty_matched;
               end if;
            end if;

            L_shs_qty_adjust := L_shs_qty_adjust * rec.comp_qty;

            if L_systems_options_row.rcv_cost_adj_type = 'S' then
               --
                  if ( L_new_on_hand >= L_shs_qty_adjust ) then
                     L_temp_av_cost_loc := L_new_av_cost_loc;
                     L_temp_on_hand := L_stock_on_hand;
                  elsif (L_new_on_hand <= 0) then
                     L_temp_av_cost_loc := L_new_av_cost_loc;
                     L_temp_on_hand := L_new_on_hand;
                  else
                     L_temp_av_cost_loc := L_old_av_cost_loc;
                     L_temp_on_hand := L_new_on_hand;
               end if;
               --
               if STKLEDGR_ACCTING_SQL.WAC_CALC_COST_CHANGE(O_error_message,
                                                            L_new_av_cost_loc,
                                                            L_neg_soh_wac_adj_amt,
                                                            L_temp_av_cost_loc,
                                                            L_temp_on_hand,
                                                            L_new_cost,
                                                            L_old_cost,
                                                            L_shs_qty_adjust) = FALSE then
                  return FALSE;
               end if;

               L_adj_qty := L_shs_qty_adjust;
               O_new_wac_loc := L_new_av_cost_loc;
               O_neg_soh_wac_adj_amt := L_neg_soh_wac_adj_amt;

            elsif  L_systems_options_row.RCV_COST_ADJ_TYPE = 'F' then
               L_adj_qty              := 0;
               L_neg_soh_wac_adj_qty  := 0;
               L_new_on_hand := L_fifo_on_hand - (rec.vir_qty_received * rec.comp_qty);
               --
               if rec.order_no = I_order_number and rec.item = NVL(I_pack_item, I_item) and rec.shipment = NVL(I_shipment, rec.shipment) then
                  if L_new_on_hand > 0 and L_fifo_on_hand > 0 then
                     L_adj_qty := L_adj_qty + L_shs_qty_adjust;
                  elsif L_shs_qty_adjust > 0 then
                     -- if on hand is going negative, do a partial adjustment
                     L_adj_qty := L_adj_qty + GREATEST(L_fifo_on_hand, 0);
                  elsif L_shs_qty_adjust < 0 then
                     -- if on hand is going positive, do a partial adjustment
                     L_adj_qty := L_adj_qty - GREATEST(L_new_on_hand, 0);
                  end if;
                  -- this gives the total shipment quantity that is eligible for adjustment
                  if L_new_on_hand < 0 then
                     L_neg_soh_wac_adj_qty  := L_shs_qty_adjust - L_adj_qty;
                  end if;
               end if;
               --
               if L_new_on_hand > 0 then
                  L_fifo_on_hand := L_new_on_hand;
               end if;
               -- We will only recalculate if the total SOH is positive,
               -- the eligible shipment qty is positive,
               -- and the allocated qty is positive
               if L_stock_on_hand > 0 and L_adj_qty > 0  then
                  L_adj_qty := LEAST(L_adj_qty, L_stock_on_hand);
               else
                  L_adj_qty := 0;
               end if;
               ---
               if STKLEDGR_ACCTING_SQL.WAC_CALC_COST_CHANGE(O_error_message,
                                                            L_new_av_cost_loc,
                                                            L_neg_soh_wac_adj_amt,
                                                            L_old_av_cost_loc,
                                                            L_stock_on_hand,
                                                            L_new_cost,
                                                            L_old_cost,
                                                            L_adj_qty) = FALSE then
                  return FALSE;
               end if;
               O_new_wac_loc := L_new_av_cost_loc;
               O_neg_soh_wac_adj_amt := L_neg_soh_wac_adj_amt;
               O_adj_qty := L_neg_soh_wac_adj_qty;
               L_old_av_cost_loc := L_new_av_cost_loc;
            end if;

            if I_posting_ind = TRUE then
               if L_backpost_rca_rua_ind = 'Y' and I_adj_code !='A' then
                  L_post_date := rec.receive_date; --Post tran_data on receipt date
               else
                  L_post_date := L_tran_date;
               end if;


               -- RCA would happen at Costing location, as receipt and invoice would come for the costing location.
               -- No RCA would be posted for Franchise location as franchise costing would be independent of PO costing.


               if L_wf_order_po is NULL then
                  L_shipment := rec.shipment;
               end if;

               if L_adj_qty > 0 then
                  L_tran_code := 20;
                  L_new_cst := (L_adj_qty * (L_new_cost - L_old_cost));
                  L_tot_cost_excl_elc := (L_adj_qty * (L_new_cost_excl_elc - L_old_cost_excl_elc));
                  if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                   I_item,
                                                   L_item_master.dept,
                                                   L_item_master.class,
                                                   L_item_master.subclass,
                                                   L_location,
                                                   L_loc_type,
                                                   L_post_date,
                                                   L_tran_code,
                                                   I_adj_code,
                                                   L_adj_qty,
                                                   L_new_cst,
                                                   0,
                                                   I_order_number,-- I_ref_no_1,
                                                   L_shipment,    -- I_ref_no_2,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   nvl(I_pgm_name, L_program),
                                                   NULL,
                                                   L_ref_pack_no,
                                                   L_tot_cost_excl_elc) = FALSE then

                     return FALSE;
                  end if;
                  ---

                  -- For Franchise PO system generated transfers, there is no need to write tsf tran data records.
                  if L_wf_order_po is NULL then
                     if CREATE_ORD_TSF_SQL.BUILD_ADJ_TRAN_DATA_SG(O_error_message,
                                                                  I_order_number,
                                                                  I_item,
                                                                  L_new_cst,
                                                                  0,
                                                                  L_adj_qty,
                                                                  I_pack_item) = FALSE then
                        return FALSE;
                     end if;
                  end if;
               end if;

               if L_systems_options_row.rcv_cost_adj_type = 'S' then
                  ---
                  L_tran_code := 70;
                  ---
                  if L_new_on_hand > 0 and L_new_on_hand < L_adj_qty then
                     L_tran_data_units := L_adj_qty - L_new_on_hand;
                  else
                     L_tran_data_units := L_adj_qty;
                  end if;
                  L_new_cst := L_neg_soh_wac_adj_amt;
                  L_tot_cost_excl_elc := (L_tran_data_units * (L_new_cost_excl_elc - L_old_cost_excl_elc));
               elsif  L_systems_options_row.RCV_COST_ADJ_TYPE = 'F' then
                  L_tran_code := 73;
                  L_new_cst := (L_neg_soh_wac_adj_qty  * (L_new_cost - L_old_cost));
                  L_tot_cost_excl_elc := (L_neg_soh_wac_adj_qty * (L_new_cost_excl_elc - L_old_cost_excl_elc));
                  L_tran_data_units := L_neg_soh_wac_adj_qty;
               end if;

               if ( L_new_cst != 0 and L_systems_options_row.RCV_COST_ADJ_TYPE ='S')
                  or (L_tran_data_units > 0 and L_systems_options_row.RCV_COST_ADJ_TYPE ='F') then
                  if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                                   I_item,
                                                   L_item_master.dept,
                                                   L_item_master.class,
                                                   L_item_master.subclass,
                                                   L_location,
                                                   L_loc_type,
                                                   L_post_date,
                                                   L_tran_code,
                                                   NULL,
                                                   L_tran_data_units, -- unit
                                                   L_new_cst,
                                                   NULL,              -- Total Retail
                                                   I_order_number,    -- I_ref_no_1,
                                                   L_shipment,        -- I_ref_no_2,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   nvl(I_pgm_name, L_program),
                                                   NULL,
                                                   L_ref_pack_no,
                                                   L_tot_cost_excl_elc) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if; --I_post_ind is true

            if (L_systems_options_row.rcv_cost_adj_type = 'S') then
               L_new_on_hand := L_new_on_hand - L_shs_qty_adjust;
            end if;
         end LOOP;
      else  --L_recalc_ind = 'N'
         L_new_av_cost_loc := O_new_wac_loc;
         L_neg_soh_wac_adj_amt := O_neg_soh_wac_adj_amt;
      end if;
      ---
      if I_posting_ind = TRUE then
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'ITEM_LOC_SOH',
                          'ITEM: '||I_item||L_loc_string);

         update item_loc_soh
            set av_cost = ROUND(L_new_av_cost_loc,4),
                last_update_datetime = SYSDATE,
                last_update_id = GET_USER
          where rowid = L_rowid;

         L_l10n_fin_rec               := L10N_FIN_REC();
         L_l10n_fin_rec.procedure_key := 'UPDATE_AV_COST';

         if I_order_number is Not Null then
            L_l10n_fin_rec.doc_type   := 'PO';
            L_l10n_fin_rec.doc_id     := I_order_number;
         end if;

         L_l10n_fin_rec.source_entity := 'LOC';
         L_l10n_fin_rec.source_id     := I_location;
         L_l10n_fin_rec.source_type   := I_loc_type;
         L_l10n_fin_rec.item          := I_item;
         L_l10n_fin_rec.av_cost       := ROUND(L_new_av_cost_loc, 4);

         --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.UPDATE_AV_COST to
         --update the av_cost for all the virtual locations having the same CNPJ number.
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REC_COST_ADJ_INVENTORY;

--------------------------------------------------------------------------------
FUNCTION UPD_AV_COST_CHANGE_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                                 I_location              IN       ORDLOC.LOCATION%TYPE,
                                 I_loc_type              IN       ORDLOC.LOC_TYPE%TYPE,
                                 I_new_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_old_cost              IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_receipt_qty           IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                 I_new_wac_loc           IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_neg_soh_wac_adj_amt   IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                 I_recalc_ind            IN       VARCHAR2,
                                 I_order_number          IN       ORDHEAD.ORDER_NO%TYPE   DEFAULT NULL,
                                 I_ref_no_2              IN       TRAN_DATA.REF_NO_2%TYPE DEFAULT NULL,
                                 I_pack_item             IN       ITEM_MASTER.ITEM%TYPE DEFAULT NULL,
                                 I_pgm_name              IN       TRAN_DATA.PGM_NAME%TYPE DEFAULT NULL,
                                 I_adj_code              IN       TRAN_DATA.ADJ_CODE%TYPE DEFAULT 'C',
                                 I_shipment              IN       SHIPMENT.SHIPMENT%TYPE DEFAULT NULL,
                                 I_new_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE  DEFAULT NULL,
                                 I_old_cost_excl_elc     IN       ORDLOC.UNIT_COST%TYPE  DEFAULT NULL,
                                 I_adjust_matched_ind    IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   L_program               VARCHAR2(45)   := 'ITEMLOC_UPDATE_SQL.UPD_AV_COST_CHANGE_COST';
   L_table                 VARCHAR2(30);
   L_key1                  VARCHAR2(100);
   L_key2                  VARCHAR2(100);
   L_adj_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_new_wac_loc           ITEM_LOC_SOH.AV_COST%TYPE :=I_new_wac_loc;
   L_neg_soh_wac_adj_amt   ITEM_LOC_SOH.AV_COST%TYPE :=I_neg_soh_wac_adj_amt;
   RECORD_LOCKED           EXCEPTION;

BEGIN
   if REC_COST_ADJ_INVENTORY(O_error_message,
                             L_new_wac_loc,
                             L_neg_soh_wac_adj_amt,
                             L_adj_qty,
                             I_item,
                             I_location,
                             I_loc_type,
                             I_new_cost,
                             I_old_cost,
                             I_receipt_qty,
                             I_recalc_ind,
                             I_order_number,
                             I_ref_no_2,
                             I_pack_item,
                             I_pgm_name,
                             I_adj_code,
                             I_shipment,
                             I_new_cost_excl_elc,
                             I_old_cost_excl_elc,
                             I_adjust_matched_ind,
                             TRUE) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPD_AV_COST_CHANGE_COST;
-------------------------------------------------------------------------------
FUNCTION UPD_AVG_COST_CHANGE_QTY(I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_loc_type        IN       INV_ADJ.LOC_TYPE%TYPE,
                                 I_location        IN       INV_ADJ.LOCATION%TYPE,
                                 I_new_cost        IN       ORDLOC.UNIT_COST%TYPE,
                                 I_adj_qty         IN       INV_ADJ.ADJ_QTY%TYPE,
                                 O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN is

   L_program   VARCHAR2(64)   := 'ITEMLOC_UPDATE_SQL.UPD_AVG_COST_CHANGE_QTY';

   L_l10n_fin_rec          L10N_FIN_REC := L10N_FIN_REC();

   L_old_av_cost_loc       ITEM_LOC_SOH.AV_COST%TYPE         := 0;
   L_stock_on_hand         ITEM_LOC_SOH.STOCK_ON_HAND%TYPE   := 0;

   L_new_av_cost_loc       ITEM_LOC_SOH.AV_COST%TYPE         := 0;
   L_neg_soh_wac_adj_amt   ITEM_LOC_SOH.AV_COST%TYPE;

   L_item_master           ITEM_MASTER%ROWTYPE;
   L_tran_code             TRAN_DATA.TRAN_CODE%TYPE;
   L_tran_date             DATE   := get_vdate;

   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);
   L_rowid                 ROWID;

   L_loc_string            VARCHAR2(25);
   L_table                 VARCHAR2(30);
   L_key1                  VARCHAR2(100);
   L_key2                  VARCHAR2(100);

BEGIN
   ---
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_master,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_LOC';
   L_key1  := I_item;
   L_key2  := TO_CHAR(I_location);
   ---
   if L_item_master.item_level = L_item_master.tran_level then
      ---
      L_loc_string := ' Location: '||TO_CHAR(I_location);
      ---
      L_l10n_fin_rec.procedure_key := 'GET_CURRENT_SOH_WAC';
      L_l10n_fin_rec.source_entity := 'LOC';
      L_l10n_fin_rec.source_id     := I_location;
      L_l10n_fin_rec.source_type   := I_loc_type;
      L_l10n_fin_rec.item          := I_item;
      L_l10n_fin_rec.calc_fld_type := 'TOTSTK';

      --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.GET_CURRENT_SOH_WAC to
      --get the SOH for all the virtual locations having the same CNPJ number.
      --For non-localized country, the L10N_SQL.EXEC_FUNCTION will call ITEMLOC_ATTRIB_SQL.GET_CURRENT_SOH_WAC to
      --get the SOH of a particular location.
      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_fin_rec) = FALSE then
         return FALSE;
      end if;

      L_stock_on_hand   := L_l10n_fin_rec.stock_on_hand;
      L_old_av_cost_loc := L_l10n_fin_rec.av_cost;
      L_rowid           := CHARTOROWID(L_l10n_fin_rec.fin_rowid);
      ---
      if STKLEDGR_ACCTING_SQL.WAC_CALC_QTY_CHANGE(O_error_message,
                                                  L_new_av_cost_loc,
                                                  L_neg_soh_wac_adj_amt,
                                                  L_old_av_cost_loc,
                                                  L_stock_on_hand,
                                                  I_new_cost,
                                                  I_adj_qty) = FALSE then
         return FALSE;
      end if;
      ---
      if L_neg_soh_wac_adj_amt != 0 then
         ---
         L_tran_code := 70;
         ---
         if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                          I_item,
                                          L_item_master.dept,
                                          L_item_master.class,
                                          L_item_master.subclass,
                                          I_location,
                                          I_loc_type,
                                          L_tran_date,
                                          L_tran_code,
                                          NULL,
                                          0,     -- unit
                                          L_neg_soh_wac_adj_amt,
                                          NULL,         -- Total Retail
                                          NULL,         -- I_ref_no_1,
                                          NULL,         -- I_ref_no_2,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          L_program,
                                          NULL) = FALSE then
            RETURN FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_LOC_SOH',
                       'ITEM: '||I_item||L_loc_string);
      update item_loc_soh
         set stock_on_hand        = stock_on_hand + I_adj_qty,
             av_cost              = ROUND(L_new_av_cost_loc,4),
             soh_update_datetime  = DECODE(I_adj_qty, 0, soh_update_datetime,
                                                         SYSDATE),
             last_update_datetime = SYSDATE,
             last_update_id       = GET_USER
       where rowid = L_rowid;
      ---
      L_l10n_fin_rec               := L10N_FIN_REC();

      L_l10n_fin_rec.procedure_key := 'UPDATE_AV_COST';
      L_l10n_fin_rec.source_entity := 'LOC';
      L_l10n_fin_rec.source_id     := I_location;
      L_l10n_fin_rec.source_type   := I_loc_type;
      L_l10n_fin_rec.item          := I_item;
      L_l10n_fin_rec.av_cost       := ROUND(L_new_av_cost_loc, 4);

      --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.UPDATE_AV_COST to
      --update the av_cost for all the virtual locations having the same CNPJ number.
      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_fin_rec) = FALSE then
         return FALSE;
      end if;
      ---
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPD_AVG_COST_CHANGE_QTY;
-----------------------------------------------------------------------------------
FUNCTION BULK_LOCK_ITEM_LOC_SOH (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh ils
       where exists (select 'x'
                       from api_item_loc_temp ail
                      where ail.item = ils.item
                        and ail.loc = ils.loc
                        and rownum <= 1)
      for update nowait;


BEGIN

   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_LOC_SOH','ITEM_LOC_SOH_LOCK',NULL);
   open C_LOCK_ITEM_LOC_SOH;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_LOC_SOH','ITEM_LOC_SOH_LOCK',NULL);
   close C_LOCK_ITEM_LOC_SOH;

   SQL_LIB.SET_MARK('DELETE',NULL,'API_ITEM_LOC_TEMP',NULL);
   delete from api_item_loc_temp;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_LOC_SOH',
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEMLOC_UPDATE_SQL.BULK_LOCK_ITEM_LOC_SOH',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BULK_LOCK_ITEM_LOC_SOH;
--------------------------------------------------------------------------------
FUNCTION UPD_ALC_AV_COST_CHANGE_COST(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item           IN       ITEM_MASTER.ITEM%TYPE,
                                     I_location       IN       ORDLOC.LOCATION%TYPE,
                                     I_loc_type       IN       ORDLOC.LOC_TYPE%TYPE,
                                     I_new_cost       IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                     I_old_cost       IN       ITEM_LOC_SOH.AV_COST%TYPE,
                                     I_receipt_qty    IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                                     I_recalc_ind     IN       VARCHAR2,
                                     I_order_number   IN       ORDHEAD.ORDER_NO%TYPE,
                                     I_pack_item      IN       ITEM_MASTER.ITEM%TYPE,
                                     I_pgm_name       IN       TRAN_DATA.PGM_NAME%TYPE,
                                     I_adj_code       IN       TRAN_DATA.ADJ_CODE%TYPE,
                                     I_shipment       IN       SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(100)   := 'ITEMLOC_UPDATE_SQL.UPD_ALC_AV_COST_CHANGE_COST';

   L_rowid                  ROWID;
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(Record_Locked, -54);

   L_l10n_fin_rec           L10N_FIN_REC := L10N_FIN_REC();

   L_adj_qty                ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_stock_on_hand          ITEM_LOC_SOH.STOCK_ON_HAND%TYPE   := 0;
   L_old_av_cost_loc        ITEM_LOC_SOH.AV_COST%TYPE         := 0;
   L_new_av_cost_loc        ITEM_LOC_SOH.AV_COST%TYPE         := NULL;
   L_temp_av_cost_loc       ITEM_LOC_SOH.AV_COST%TYPE;
   L_neg_soh_wac_adj_amt    ITEM_LOC_SOH.AV_COST%TYPE         := 0;
   L_neg_soh_wac_adj_qty    ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_item_master            ITEM_MASTER%ROWTYPE;
   L_tran_code              TRAN_DATA.TRAN_CODE%TYPE;
   L_tran_date              DATE           := get_vdate;
   L_post_date              DATE;
   L_loc_string             VARCHAR2(25);
   L_systems_options_row    SYSTEM_OPTIONS%ROWTYPE;
   L_new_cst                TRAN_DATA.TOTAL_COST%TYPE;
   L_tran_data_units        TRAN_DATA.UNITS%TYPE;
   L_loc                    SHIPMENT.TO_LOC%TYPE;
   L_process_item           ITEM_MASTER.ITEM%TYPE;
   L_pack_qty               ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_receive_as_type        ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_shs_qty_adjust         SHIPSKU.QTY_RECEIVED%TYPE;
   L_fifo_on_hand           ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_new_on_hand            ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_temp_on_hand           ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_first_received         DATE;
   L_wf_order_po            ORDHEAD.WF_ORDER_NO%TYPE;
   L_shipment               SHIPMENT.SHIPMENT%TYPE;
   L_location               ITEM_LOC.LOC%TYPE;
   L_loc_type               ITEM_LOC.LOC_TYPE%TYPE;
   L_old_cost               ITEM_LOC_SOH.AV_COST%TYPE := I_old_cost;
   L_new_cost               ITEM_LOC_SOH.AV_COST%TYPE := I_new_cost;

   cursor C_PHY_SHIPMENT is
      select physical_wh
        from wh
       where wh = I_location;

   cursor C_GET_PACK_COMP_QTY is
      select qty
        from v_packsku_qty
       where pack_no = I_pack_item
         and item = I_item;

   cursor C_FIRST_RECEIVED is
      select MIN(s2.receive_date)
        from shipment s2
       where s2.order_no = I_order_number
         and s2.to_loc = L_loc
         and s2.shipment = NVL(I_shipment,s2.shipment);

   cursor C_SHIPMENT is
      with matching_items   -- the first subquery handles the receipt for L_process_item
         as(select (case when (L_systems_options_row.rcv_cost_adj_type = 'S')
                          and I_pack_item is not null
                          and L_receive_as_type = 'E' then I_pack_item
                         else L_process_item
                     end) item,
                   (case when (L_systems_options_row.rcv_cost_adj_type = 'F')
                          and  I_pack_item is not null
                          and  L_receive_as_type = 'E' then 1
                          else L_pack_qty
                     end) comp_qty
              from dual
             union   -- the second subquery handles pack items broken on receipt*/
            select il.item,
                   v.qty comp_qty -- pack component quantity
              from v_packsku_qty v,
                   item_loc il
             where L_receive_as_type = 'E'
               and il.item = v.pack_no
               and il.loc = I_location
               and nvl(il.receive_as_type,'E') = 'E'
               and v.item = I_item
               and L_systems_options_row.rcv_cost_adj_type = 'F'
             )
      select sh.order_no,
             nvl2(I_shipment,nvl(sh.parent_shipment,sh.shipment),null) shipment,
             min(sh.receive_date) receive_date,
             sum(ss.qty_received) qty_received,
             sum(ss.qty_received) vir_qty_received,
             sum(nvl(decode(sh.invc_match_status, 'M', ss.qty_received, ss.qty_matched),0)) qty_matched,
             ss.item,
             ss.seq_no,
             i.comp_qty
        from matching_items i,
             shipment sh,
             shipsku ss
       where I_loc_type !='W'
         and ss.item         = i.item
         and sh.shipment     = ss.shipment
         and sh.order_no     is not null
         and sh.to_loc       = I_location
         and ss.qty_received != 0
         and ((L_systems_options_row.rcv_cost_adj_type = 'F' and sh.receive_date >= L_first_received)
          or L_systems_options_row.rcv_cost_adj_type = 'S' and sh.order_no = I_order_number and sh.shipment = nvl(I_shipment,sh.shipment))
       group by sh.order_no, nvl2(I_shipment,nvl(sh.parent_shipment,sh.shipment),null), sh.receive_date, ss.item, ss.seq_no, i.comp_qty
       union all
      select /*+ index (ssl pk_shipsku_loc) */
             sh.order_no,
             nvl2(I_shipment,nvl(sh.parent_shipment,sh.shipment),null) shipment,
             min(sh.receive_date) receive_date,
             sum(ss.qty_received) qty_received,
             sum(ssl.qty_received) vir_qty_received,
             sum(nvl(decode(sh.invc_match_status, 'M', ss.qty_received, ss.qty_matched),0)) qty_matched,
             ss.item,
             ss.seq_no,
             i.comp_qty
        from matching_items i,
             shipment sh,
             shipsku ss,
             shipsku_loc ssl
       where I_loc_type ='W'
         and ss.item             = i.item
         and sh.shipment         = ss.shipment
         and sh.order_no         is not null
         and ssl.shipment        = ss.shipment
         and ssl.seq_no          = ss.seq_no
         and ssl.item            = ss.item
         and ssl.to_loc          = I_location
         and ssl.qty_received   != 0
         and ((L_systems_options_row.rcv_cost_adj_type = 'F' and sh.receive_date >= L_first_received)
          or L_systems_options_row.rcv_cost_adj_type = 'S' and sh.order_no = I_order_number and sh.shipment = nvl(I_shipment,sh.shipment))
       group by sh.order_no, nvl2(I_shipment,nvl(sh.parent_shipment,sh.shipment),null), sh.receive_date, ss.item, ss.seq_no, i.comp_qty
       order by receive_date desc,  order_no, item , seq_no;

   cursor C_GET_WF_ORDER_NO is 
      select wf_order_no
        from ordhead
       where order_no = I_order_number;
BEGIN

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_master,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if L_item_master.item_level = L_item_master.tran_level then
      L_loc_string := ' Location: '||TO_CHAR(I_location);

      if I_pack_item is NULL then
         L_process_item := I_item;
         L_receive_as_type := 'E';
         L_pack_qty := 1;
      else
         if I_loc_type = 'W' then
            if ITEMLOC_ATTRIB_SQL.GET_RECEIVE_AS_TYPE(O_error_message,
                                                      L_receive_as_type,
                                                      I_pack_item,
                                                      I_location) = FALSE then
               return FALSE;
            end if;
         else
            L_receive_as_type := 'E';
         end if;

         if L_receive_as_type = 'E' then
            L_process_item := I_item;
         else
            L_process_item := I_pack_item;
         end if;

         open C_GET_PACK_COMP_QTY;
         fetch C_GET_PACK_COMP_QTY into L_pack_qty;
         close C_GET_PACK_COMP_QTY;

         if L_pack_qty is null then
            O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                   L_program,
                                                   'V_PACKSKU_QTY',
                                                   'PACK_NO:'||I_pack_item||', ITEM:'||I_item);
            return FALSE;
         end if;
      end if;
         ---

      open C_GET_WF_ORDER_NO;
      fetch C_GET_WF_ORDER_NO into L_wf_order_po;
      close C_GET_WF_ORDER_NO;
      -- For a franchise order RCA, WAC recalculation and Tran Data postings  
      -- should be done at costing location.
      if L_wf_order_po is NOT NULL then
         select costing_loc,
                costing_loc_type
           into L_location,
                L_loc_type
           from item_loc
          where item = I_item
            and loc = I_location
            and loc_type = I_loc_type;

         -- Convert Input Costs to Costing Loc Currency
         ---
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             L_location,
                                             L_loc_type,
                                             NULL,
                                             L_old_cost,
                                             L_old_cost,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             L_location,
                                             L_loc_type,
                                             NULL,
                                             L_new_cost,
                                             L_new_cost,
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         ---
      else
         L_location := I_location;
         L_loc_type := I_loc_type;
      end if;
      ---
      L_l10n_fin_rec.procedure_key := 'GET_CURRENT_SOH_WAC';

      if I_order_number is Not Null then
         L_l10n_fin_rec.doc_type   := 'PO';
         L_l10n_fin_rec.doc_id     := I_order_number;
      end if;

      L_l10n_fin_rec.source_entity := 'LOC';
      L_l10n_fin_rec.source_id     := L_location;
      L_l10n_fin_rec.source_type   := L_loc_type;
      L_l10n_fin_rec.item          := L_process_item;
      L_l10n_fin_rec.calc_fld_type := 'TOTSTK';

      --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.GET_CURRENT_SOH_WAC to
      --get the SOH for all the virtual locations having the same CNPJ number.
      --For non-localized country, the L10N_SQL.EXEC_FUNCTION will call ITEMLOC_ATTRIB_SQL.GET_CURRENT_SOH_WAC to
      --get the SOH of a particular location.
      if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                 L_l10n_fin_rec) = FALSE then
         return FALSE;
      end if;

      L_stock_on_hand   := L_l10n_fin_rec.stock_on_hand;
      L_old_av_cost_loc := L_l10n_fin_rec.av_cost;
      L_new_av_cost_loc := L_old_av_cost_loc;
      L_rowid           := CHARTOROWID(L_l10n_fin_rec.fin_rowid);

      if L_process_item != I_item then
         L_stock_on_hand := L_pack_qty * L_stock_on_hand;
         L_l10n_fin_rec               := L10N_FIN_REC();
         L_l10n_fin_rec.procedure_key := 'GET_CURRENT_SOH_WAC';
         if I_order_number is Not Null then
            L_l10n_fin_rec.doc_type   := 'PO';
            L_l10n_fin_rec.doc_id     := I_order_number;
         end if;

         L_l10n_fin_rec.source_entity := 'LOC';
         L_l10n_fin_rec.source_id     := L_location;
         L_l10n_fin_rec.source_type   := L_loc_type;
         L_l10n_fin_rec.item          := I_item;
         L_l10n_fin_rec.calc_fld_type := 'TOTSTK';

         --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.GET_CURRENT_SOH_WAC to
         --get the SOH for all the virtual locations having the same CNPJ number.
         --For non-localized country, the L10N_SQL.EXEC_FUNCTION will call ITEMLOC_ATTRIB_SQL.GET_CURRENT_SOH_WAC to
         --get the SOH of a particular location.
         if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                    L_l10n_fin_rec) = FALSE then
            return FALSE;
         end if;
         L_old_av_cost_loc := L_l10n_fin_rec.av_cost;
         L_new_av_cost_loc := L_old_av_cost_loc;
         L_rowid           := CHARTOROWID(L_l10n_fin_rec.fin_rowid);
      end if;
      ---

      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_systems_options_row) = FALSE then
         return FALSE;
      end if;
      ---
      if I_loc_type = 'W' then
         open C_PHY_SHIPMENT;
         fetch C_PHY_SHIPMENT into L_loc;
         close C_PHY_SHIPMENT;
         if L_loc is null then
            O_error_message := SQL_LIB.CREATE_MSG('DATA_NOT_FOUND',
                                                  L_program,
                                                  'WH',
                                                  'WH:'||I_location);
            return FALSE;
         end if;
      else
         L_loc := I_location;
      end if;

      open C_FIRST_RECEIVED;
      fetch C_FIRST_RECEIVED into L_first_received;
      close C_FIRST_RECEIVED;
      L_fifo_on_hand := L_stock_on_hand;
      L_new_on_hand := L_stock_on_hand;
      L_temp_on_hand := L_stock_on_hand;
      L_temp_av_cost_loc := L_old_av_cost_loc;

      for rec in C_SHIPMENT LOOP

         /*If shipment is NULL then Obligation quantity will be used instead. This will only occur once so
           this will not loop. */
         if I_shipment is null then
            L_shs_qty_adjust := I_receipt_qty;
         else
           L_shs_qty_adjust := rec.vir_qty_received ;
         end if;

         L_shs_qty_adjust := L_shs_qty_adjust * rec.comp_qty;

         if L_systems_options_row.rcv_cost_adj_type = 'S' then
            --
               if(L_new_on_hand >= L_shs_qty_adjust ) then
                  L_temp_av_cost_loc := L_new_av_cost_loc;
                  L_temp_on_hand := L_stock_on_hand;
               elsif(L_new_on_hand <= 0) then
                  L_temp_av_cost_loc := L_new_av_cost_loc;
                  L_temp_on_hand := L_new_on_hand;
               else
                  L_temp_av_cost_loc := L_old_av_cost_loc;
                  L_temp_on_hand := L_new_on_hand;
            end if;
            --
            if STKLEDGR_ACCTING_SQL.WAC_CALC_COST_CHANGE(O_error_message,
                                                         L_new_av_cost_loc,
                                                         L_neg_soh_wac_adj_amt,
                                                         L_temp_av_cost_loc,
                                                         L_temp_on_hand,
                                                         L_new_cost,
                                                         L_old_cost,
                                                         L_shs_qty_adjust) = FALSE then
               return FALSE;
            end if;

            L_adj_qty := L_shs_qty_adjust;

         elsif  L_systems_options_row.rcv_cost_adj_type = 'F' then
            L_adj_qty              := 0;
            L_neg_soh_wac_adj_qty  := 0;

            /*Need to adjust only the obligation quantity if shipment is null*/
            if I_shipment is null then
               L_new_on_hand := L_fifo_on_hand - (I_receipt_qty * rec.comp_qty);
            else
                L_new_on_hand := L_fifo_on_hand - (rec.vir_qty_received * rec.comp_qty);
            end if;
            --
            if rec.order_no = I_order_number and rec.item = NVL(I_pack_item, I_item) and (rec.shipment = NVL(I_shipment, rec.shipment) or I_shipment is null) then
               if L_new_on_hand > 0 and L_fifo_on_hand > 0 then
                  L_adj_qty := L_adj_qty + L_shs_qty_adjust;
               elsif L_shs_qty_adjust > 0 then
                  -- if on hand is going negative, do a partial adjustment
                  L_adj_qty := L_adj_qty + GREATEST(L_fifo_on_hand, 0);
               elsif L_shs_qty_adjust < 0 then
                  -- if on hand is going positive, do a partial adjustment
                  L_adj_qty := L_adj_qty - GREATEST(L_new_on_hand, 0);
               end if;
               -- this gives the total shipment quantity that is eligible for adjustment
               if L_new_on_hand < 0 then
                  L_neg_soh_wac_adj_qty  := L_shs_qty_adjust - L_adj_qty;
               end if;
            end if;
            --
            if L_new_on_hand > 0 then
               L_fifo_on_hand := L_new_on_hand;
            end if;
            -- We will only recalculate if the total SOH is positive,
            -- the eligible shipment qty is positive,
            -- and the allocated qty is positive
            if L_stock_on_hand > 0 and L_adj_qty > 0  then
               L_adj_qty := LEAST(L_adj_qty, L_stock_on_hand);
            else
               L_adj_qty := 0;
            end if;
            ---
            if STKLEDGR_ACCTING_SQL.WAC_CALC_COST_CHANGE(O_error_message,
                                                         L_new_av_cost_loc,
                                                         L_neg_soh_wac_adj_amt,
                                                         L_old_av_cost_loc,
                                                         L_stock_on_hand,
                                                         L_new_cost,
                                                         L_old_cost,
                                                         L_adj_qty) = FALSE then
               return FALSE;
            end if;
            L_old_av_cost_loc := L_new_av_cost_loc;
         end if;


         L_post_date := L_tran_date;

         -- RCA would happen at Costing location, as receipt and invoice would come for the costing location.
         -- No RCA would be posted for Franchise location as franchise costing would be independent of PO costing.

         if L_wf_order_po is NULL then
            L_shipment := rec.shipment;
         end if;

         if L_adj_qty > 0 then
            L_tran_code := 20;
            L_new_cst := (L_adj_qty * (L_new_cost - L_old_cost));
            if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_item_master.dept,
                                             L_item_master.class,
                                             L_item_master.subclass,
                                             L_location,
                                             L_loc_type,
                                             L_post_date,
                                             L_tran_code,
                                             I_adj_code,
                                             L_adj_qty,
                                             L_new_cst,
                                             0,
                                             I_order_number,-- REF_NO_1,
                                             L_shipment,    -- REF_NO_2,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_pgm_name,
                                             NULL,
                                             I_pack_item,
                                             NULL) = FALSE then

               return FALSE;
            end if;
            ---

            -- For Franchise PO system generated transfers, there is no need to write tsf tran data records.
            if L_wf_order_po is NULL then
               if CREATE_ORD_TSF_SQL.BUILD_ADJ_TRAN_DATA_SG(O_error_message,
                                                            I_order_number,
                                                            I_item,
                                                            L_new_cst,
                                                            0,
                                                            L_adj_qty,
                                                            I_pack_item) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;

         if L_systems_options_row.rcv_cost_adj_type = 'S' then
            ---
            L_tran_code := 70;
            ---
            if L_new_on_hand > 0 and L_new_on_hand < L_adj_qty then
               L_tran_data_units := L_adj_qty - L_new_on_hand;
            else
               L_tran_data_units := L_adj_qty;
            end if;
            L_new_cst := L_neg_soh_wac_adj_amt;
         elsif  L_systems_options_row.RCV_COST_ADJ_TYPE = 'F' then
            L_tran_code := 73;
            L_new_cst := (L_neg_soh_wac_adj_qty  * (L_new_cost - L_old_cost));
            L_tran_data_units := L_neg_soh_wac_adj_qty;
         end if;

         if ( L_new_cst != 0 and L_systems_options_row.RCV_COST_ADJ_TYPE ='S')
            or (L_tran_data_units > 0 and L_systems_options_row.RCV_COST_ADJ_TYPE ='F') then
            if STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                             I_item,
                                             L_item_master.dept,
                                             L_item_master.class,
                                             L_item_master.subclass,
                                             L_location,
                                             L_loc_type,
                                             L_post_date,
                                             L_tran_code,
                                             NULL,
                                             L_tran_data_units, -- unit
                                             L_new_cst,
                                             NULL,              -- Total Retail
                                             I_order_number,    -- REF_NO_1,
                                             L_shipment,        -- REF_NO_2,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             I_pgm_name,
                                             NULL,
                                             I_pack_item,
                                             NULL) = FALSE then
               return FALSE;
            end if;
         end if;

         if (L_systems_options_row.rcv_cost_adj_type = 'S') then
            L_new_on_hand := L_new_on_hand - L_shs_qty_adjust;
         end if;
      end LOOP;
      ---

      update item_loc_soh
         set av_cost = ROUND(L_new_av_cost_loc,4),
             last_update_datetime = SYSDATE,
             last_update_id = GET_USER
       where rowid = L_rowid;

      L_l10n_fin_rec               := L10N_FIN_REC();
      L_l10n_fin_rec.procedure_key := 'UPDATE_AV_COST';

      if I_order_number is Not Null then
         L_l10n_fin_rec.doc_type   := 'PO';
         L_l10n_fin_rec.doc_id     := I_order_number;
      end if;

      L_l10n_fin_rec.source_entity := 'LOC';
      L_l10n_fin_rec.source_id     := I_location;
      L_l10n_fin_rec.source_type   := I_loc_type;
      L_l10n_fin_rec.item          := I_item;
      L_l10n_fin_rec.av_cost       := ROUND(L_new_av_cost_loc, 4);

      --For localized country Brazil, the L10N_SQL.EXEC_FUNCTION will call L10N_BR_FIN_SQL.UPDATE_AV_COST to
      --update the av_cost for all the virtual locations having the same CNPJ number.
      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_fin_rec) = FALSE then
         return FALSE;
      end if;

   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_LOC',
                                            I_item,
                                            TO_CHAR(I_location));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END UPD_ALC_AV_COST_CHANGE_COST;
-------------------------------------------------------------------------------
END ITEMLOC_UPDATE_SQL;
/
