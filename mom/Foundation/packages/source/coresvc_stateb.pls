create or replace PACKAGE BODY CORESVC_STATE AS
   cursor C_SVC_STATE_TL(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
      select pk_state_tl.rowid   AS pk_state_tl_rid,
             st.rowid            AS st_rid,
             stt_st_fk.rowid     AS stt_st_fk_rid,
             st.description,
             st.country_id,
             st.state,
             st.lang,
             cd_lang.rowid as cd_lang_rid,
             st.process_id,
             st.row_seq, 
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_state_tl st,
             state_tl pk_state_tl,
             state stt_st_fk,
             code_detail cd_lang,
             dual
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.country_id         = pk_state_tl.country_id (+)
         and st.state              = pk_state_tl.state (+)
         and st.lang               = pk_state_tl.lang (+)
         and st.lang               = cd_lang.code (+)
         and cd_lang.code_type (+) = 'LANG'
         and st.country_id         = stt_st_fk.country_id (+)
         and st.state              = stt_st_fk.state (+);
         
   cursor C_SVC_STATE(I_process_id NUMBER,
                      I_chunk_id NUMBER) is
      select
             pk_state.rowid      AS pk_state_rid,
             st.rowid            AS st_rid,
             sta_cnt_fk.rowid    AS sta_cnt_fk_rid,
             st.country_id,
             st.description,
             st.state,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)    AS action,
             st.process$status
        from svc_state st,
             state pk_state,
             country sta_cnt_fk,
             dual
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.country_id         = pk_state.country_id (+)
         and st.state              = pk_state.state (+)
         and st.country_id         = sta_cnt_fk.country_id (+)
;

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E')IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-----------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   STATE_TL_cols s9t_pkg.names_map_typ;
   STATE_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                       :=s9t_pkg.get_sheet_names(I_file_id);
   STATE_TL_cols                  :=s9t_pkg.get_col_names(I_file_id,STATE_TL_sheet);
   STATE_TL$Action                := STATE_TL_cols('ACTION');
   STATE_TL$DESCRIPTION           := STATE_TL_cols('DESCRIPTION');
   STATE_TL$COUNTRY_ID            := STATE_TL_cols('COUNTRY_ID');
   STATE_TL$STATE                 := STATE_TL_cols('STATE');
   STATE_TL$LANG                  := STATE_TL_cols('LANG');
   STATE_cols                     :=s9t_pkg.get_col_names(I_file_id,STATE_sheet);
   STATE$Action                   := STATE_cols('ACTION');
   STATE$COUNTRY_ID               := STATE_cols('COUNTRY_ID');
   STATE$DESCRIPTION              := STATE_cols('DESCRIPTION');
   STATE$STATE                    := STATE_cols('STATE');
END POPULATE_NAMES;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_STATE_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = STATE_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_STATE.action_mod ,state,country_id,lang,description
                           ))
     from state_tl ;
END POPULATE_STATE_TL;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_STATE( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = STATE_sheet )
   select s9t_row(s9t_cells(CORESVC_STATE.action_mod ,state,country_id,description
                           ))
     from state ;
END POPULATE_STATE;
----------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
   cursor C_USER_LANG is
      select lang
        from user_attrib
       where user_id = get_user;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(STATE_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(STATE_TL_sheet)).column_headers := s9t_cells( 'ACTION','STATE','COUNTRY_ID','LANG'
                                                                                            ,'DESCRIPTION'
                                                                                            );
   L_file.add_sheet(STATE_sheet);
   L_file.sheets(l_file.get_sheet_index(STATE_sheet)).column_headers := s9t_cells( 'ACTION','STATE'
                                                                                            ,'COUNTRY_ID'
                                                                                            ,'DESCRIPTION'
                                                                                            
);
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_STATE.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;   

   if I_template_only_ind = 'N' then
      POPULATE_STATE_TL(O_file_id);
      POPULATE_STATE(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
---------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_STATE_TL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_STATE_TL.process_id%TYPE) IS
   TYPE svc_STATE_TL_col_typ IS TABLE OF SVC_STATE_TL%ROWTYPE;
   L_temp_rec SVC_STATE_TL%ROWTYPE;
   svc_STATE_TL_col svc_STATE_TL_col_typ :=NEW svc_STATE_TL_col_typ();
   L_process_id SVC_STATE_TL.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_STATE_TL%ROWTYPE;
      L_pk_columns    VARCHAR2(255)  := 'State , Language and Country';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             DESCRIPTION_mi,
             COUNTRY_ID_mi,
             STATE_mi,
             LANG_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'STATE_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'DESCRIPTION' AS DESCRIPTION,
                                         'COUNTRY_ID' AS COUNTRY_ID,
                                         'STATE' AS STATE,
                                         'LANG' AS LANG,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       DESCRIPTION_dv,
                       COUNTRY_ID_dv,
                       STATE_dv,
                       LANG_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'STATE_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'DESCRIPTION' AS DESCRIPTION,
                                                      'COUNTRY_ID' AS COUNTRY_ID,
                                                      'STATE' AS STATE,
                                                      'LANG' AS LANG,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.DESCRIPTION := rec.DESCRIPTION_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'STATE_TL ' ,
                            NULL,
                           'DESCRIPTION ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COUNTRY_ID := rec.COUNTRY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'STATE_TL ' ,
                            NULL,
                           'COUNTRY_ID ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.STATE := rec.STATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'STATE_TL ' ,
                            NULL,
                           'STATE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'STATE_TL ' ,
                            NULL,
                           'LANG ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(STATE_TL$Action)      AS Action,
          r.get_cell(STATE_TL$DESCRIPTION)              AS DESCRIPTION,
          r.get_cell(STATE_TL$COUNTRY_ID)              AS COUNTRY_ID,
          r.get_cell(STATE_TL$STATE)              AS STATE,
          r.get_cell(STATE_TL$LANG)              AS LANG,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(STATE_TL_sheet)
  )
   LOOP
      L_temp_rec                   := null;   
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DESCRIPTION := rec.DESCRIPTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_TL_sheet,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COUNTRY_ID := rec.COUNTRY_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_TL_sheet,
                            rec.row_seq,
                            'COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.STATE := rec.STATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_TL_sheet,
                            rec.row_seq,
                            'STATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_STATE.action_new then
         L_temp_rec.DESCRIPTION := NVL( L_temp_rec.DESCRIPTION,L_default_rec.DESCRIPTION);
         L_temp_rec.COUNTRY_ID := NVL( L_temp_rec.COUNTRY_ID,L_default_rec.COUNTRY_ID);
         L_temp_rec.STATE := NVL( L_temp_rec.STATE,L_default_rec.STATE);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,L_default_rec.LANG);
      end if;
      if not (
            L_temp_rec.COUNTRY_ID is NOT NULL and
            L_temp_rec.STATE is NOT NULL and
            L_temp_rec.LANG is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
STATE_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_STATE_TL_col.extend();
         svc_STATE_TL_col(svc_STATE_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_STATE_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_STATE_TL st
      using(select
                  (case
                   when l_mi_rec.DESCRIPTION_mi    = 'N'
                    and svc_STATE_TL_col(i).action = CORESVC_STATE.action_mod
                    and s1.DESCRIPTION IS NULL
                   then mt.DESCRIPTION
                   else s1.DESCRIPTION
                   end) AS DESCRIPTION,
                  (case
                   when l_mi_rec.COUNTRY_ID_mi    = 'N'
                    and svc_STATE_TL_col(i).action = CORESVC_STATE.action_mod
                    and s1.COUNTRY_ID IS NULL
                   then mt.COUNTRY_ID
                   else s1.COUNTRY_ID
                   end) AS COUNTRY_ID,
                  (case
                   when l_mi_rec.STATE_mi    = 'N'
                    and svc_STATE_TL_col(i).action = CORESVC_STATE.action_mod
                    and s1.STATE IS NULL
                   then mt.STATE
                   else s1.STATE
                   end) AS STATE,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_STATE_TL_col(i).action = CORESVC_STATE.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  null as dummy
              from (select
                          svc_STATE_TL_col(i).DESCRIPTION AS DESCRIPTION,
                          svc_STATE_TL_col(i).COUNTRY_ID AS COUNTRY_ID,
                          svc_STATE_TL_col(i).STATE AS STATE,
                          svc_STATE_TL_col(i).LANG AS LANG,
                          null as dummy
                      from dual ) s1,
            STATE_TL mt
             where
                  mt.COUNTRY_ID (+)     = s1.COUNTRY_ID   and
                  mt.STATE (+)     = s1.STATE   and
                  mt.LANG (+)     = s1.LANG   and
                  1 = 1 )sq
                on (
                    st.COUNTRY_ID      = sq.COUNTRY_ID and
                    st.STATE      = sq.STATE and
                    st.LANG      = sq.LANG and
                    svc_STATE_TL_col(i).ACTION IN (CORESVC_STATE.action_mod,CORESVC_STATE.action_del))
      when matched then
      update
         set process_id      = svc_STATE_TL_col(i).process_id ,
             chunk_id        = svc_STATE_TL_col(i).chunk_id ,
             row_seq         = svc_STATE_TL_col(i).row_seq ,
             action          = svc_STATE_TL_col(i).action ,
             process$status  = svc_STATE_TL_col(i).process$status ,
             description              = sq.description ,
             create_id       = svc_STATE_TL_col(i).create_id ,
             create_datetime = svc_STATE_TL_col(i).create_datetime ,
             last_upd_id     = svc_STATE_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_STATE_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             description ,
             country_id ,
             state ,
             lang ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_STATE_TL_col(i).process_id ,
             svc_STATE_TL_col(i).chunk_id ,
             svc_STATE_TL_col(i).row_seq ,
             svc_STATE_TL_col(i).action ,
             svc_STATE_TL_col(i).process$status ,
             sq.description ,
             sq.country_id ,
             sq.state ,
             sq.lang ,
             svc_STATE_TL_col(i).create_id ,
             svc_STATE_TL_col(i).create_datetime ,
             svc_STATE_TL_col(i).last_upd_id ,
             svc_STATE_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            STATE_TL_sheet,
                            svc_STATE_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;

END PROCESS_S9T_STATE_TL;
----------------------------------------------------------------------
PROCEDURE PROCESS_S9T_STATE( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_STATE.process_id%TYPE) IS
   TYPE svc_STATE_col_typ IS TABLE OF SVC_STATE%ROWTYPE;
   L_temp_rec SVC_STATE%ROWTYPE;
   svc_STATE_col svc_STATE_col_typ :=NEW svc_STATE_col_typ();
   L_process_id SVC_STATE.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_STATE%ROWTYPE;
      L_pk_columns    VARCHAR2(255)  := 'State and Country';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             COUNTRY_ID_mi,
             DESCRIPTION_mi,
             STATE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'STATE'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'COUNTRY_ID' AS COUNTRY_ID,
                                         'DESCRIPTION' AS DESCRIPTION,
                                         'STATE' AS STATE,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       COUNTRY_ID_dv,
                       DESCRIPTION_dv,
                       STATE_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'STATE'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'COUNTRY_ID' AS COUNTRY_ID,
                                                      'DESCRIPTION' AS DESCRIPTION,
                                                      'STATE' AS STATE,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.COUNTRY_ID := rec.COUNTRY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'STATE ' ,
                            NULL,
                           'COUNTRY_ID ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DESCRIPTION := rec.DESCRIPTION_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'STATE ' ,
                            NULL,
                           'DESCRIPTION ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.STATE := rec.STATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'STATE ' ,
                            NULL,
                           'STATE ' ,
                             NULL,
                            'INV_DEFAULT');
     END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(STATE$Action)      AS Action,
          r.get_cell(STATE$COUNTRY_ID)              AS COUNTRY_ID,
          r.get_cell(STATE$DESCRIPTION)              AS DESCRIPTION,
          r.get_cell(STATE$STATE)              AS STATE,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(STATE_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COUNTRY_ID := rec.COUNTRY_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_sheet,
                            rec.row_seq,
                            'COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DESCRIPTION := rec.DESCRIPTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_sheet,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.STATE := rec.STATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
STATE_sheet,
                            rec.row_seq,
                            'STATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_STATE.action_new then
         L_temp_rec.COUNTRY_ID := NVL( L_temp_rec.COUNTRY_ID,L_default_rec.COUNTRY_ID);
         L_temp_rec.DESCRIPTION := NVL( L_temp_rec.DESCRIPTION,L_default_rec.DESCRIPTION);
         L_temp_rec.STATE := NVL( L_temp_rec.STATE,L_default_rec.STATE);
      end if;
      if not (
            L_temp_rec.COUNTRY_ID is NOT NULL and
            L_temp_rec.STATE is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
STATE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                        SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_STATE_col.extend();
         svc_STATE_col(svc_STATE_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_STATE_col.COUNT SAVE EXCEPTIONS
      merge into SVC_STATE st
      using(select
                  (case
                   when l_mi_rec.COUNTRY_ID_mi    = 'N'
                    and svc_STATE_col(i).action = CORESVC_STATE.action_mod
                    and s1.COUNTRY_ID IS NULL
                   then mt.COUNTRY_ID
                   else s1.COUNTRY_ID
                   end) AS COUNTRY_ID,
                  (case
                   when l_mi_rec.DESCRIPTION_mi    = 'N'
                    and svc_STATE_col(i).action = CORESVC_STATE.action_mod
                    and s1.DESCRIPTION IS NULL
                   then mt.DESCRIPTION
                   else s1.DESCRIPTION
                   end) AS DESCRIPTION,
                  (case
                   when l_mi_rec.STATE_mi    = 'N'
                    and svc_STATE_col(i).action = CORESVC_STATE.action_mod
                    and s1.STATE IS NULL
                   then mt.STATE
                   else s1.STATE
                   end) AS STATE,
                  null as dummy
              from (select
                          svc_STATE_col(i).COUNTRY_ID AS COUNTRY_ID,
                          svc_STATE_col(i).DESCRIPTION AS DESCRIPTION,
                          svc_STATE_col(i).STATE AS STATE,
                          null as dummy
                      from dual ) s1,
            STATE mt
             where
                  mt.COUNTRY_ID (+)     = s1.COUNTRY_ID   and
                  mt.STATE (+)     = s1.STATE   and
                  1 = 1 )sq
                on (
                    st.COUNTRY_ID      = sq.COUNTRY_ID and
                    st.STATE      = sq.STATE and
                    svc_STATE_col(i).ACTION IN (CORESVC_STATE.action_mod,CORESVC_STATE.action_del))
      when matched then
      update
         set process_id      = svc_STATE_col(i).process_id ,
             chunk_id        = svc_STATE_col(i).chunk_id ,
             row_seq         = svc_STATE_col(i).row_seq ,
             action          = svc_STATE_col(i).action ,
             process$status  = svc_STATE_col(i).process$status ,
             description              = sq.description ,
             create_id       = svc_STATE_col(i).create_id ,
             create_datetime = svc_STATE_col(i).create_datetime ,
             last_upd_id     = svc_STATE_col(i).last_upd_id ,
             last_upd_datetime = svc_STATE_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             country_id ,
             description ,
             state ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_STATE_col(i).process_id ,
             svc_STATE_col(i).chunk_id ,
             svc_STATE_col(i).row_seq ,
             svc_STATE_col(i).action ,
             svc_STATE_col(i).process$status ,
             sq.country_id ,
             sq.description ,
             sq.state ,
             svc_STATE_col(i).create_id ,
             svc_STATE_col(i).create_datetime ,
             svc_STATE_col(i).last_upd_id ,
             svc_STATE_col(i).last_upd_datetime ); 
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            STATE_sheet,
                            svc_STATE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;
END PROCESS_S9T_STATE;
----------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT        OUT   NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_STATE.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_STATE_TL(I_file_id,I_process_id);
      PROCESS_S9T_STATE(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
------------------------------------------------------------
FUNCTION EXEC_STATE_TL_INS(  L_state_tl_temp_rec   IN   STATE_TL%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_STATE.EXEC_STATE_TL_INS';
   L_table   VARCHAR2(255):= 'SVC_STATE_TL';
BEGIN
   insert
     into state_tl
   values L_state_tl_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_STATE_TL_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_STATE_TL_UPD( L_state_tl_temp_rec   IN   STATE_TL%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_STATE.EXEC_STATE_TL_UPD';
   L_table   VARCHAR2(255):= 'SVC_STATE_TL';
BEGIN
   update state_tl
      set row = L_state_tl_temp_rec
    where 1 = 1
      and country_id = L_state_tl_temp_rec.country_id
      and state = L_state_tl_temp_rec.state
      and lang = L_state_tl_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_STATE_TL_UPD;
------------------------------------------------------------------------------------
FUNCTION EXEC_STATE_TL_DEL(  L_state_tl_temp_rec   IN   STATE_TL%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_STATE.EXEC_STATE_TL_DEL';
   L_table   VARCHAR2(255):= 'SVC_STATE_TL';
BEGIN
   delete
     from state_tl
    where 1 = 1
      and country_id = L_state_tl_temp_rec.country_id
      and state = L_state_tl_temp_rec.state
      and lang = L_state_tl_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_STATE_TL_DEL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_STATE_TL( I_process_id   IN   SVC_STATE_TL.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_STATE_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_STATE.PROCESS_STATE_TL';
   L_error_message VARCHAR2(600);
   L_STATE_TL_temp_rec STATE_TL%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_STATE_TL';
BEGIN
   FOR rec IN c_svc_STATE_TL(I_process_id,
                             I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      
      if rec.action = action_new
         and rec.PK_STATE_TL_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'STATE_TL',
                                                NULL,
                                                NULL);          

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STATE/COUNTRY/LANG',
                     L_error_message);
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_STATE_TL_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STATE/COUNTRY/LANG',
                     'NO_RECORDS_UPDATE');
         L_error :=TRUE;
      end if;
      
      if rec.action in (action_new)
         and rec.stt_st_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STATE/COUNTRY',
                    'NO_COUNTRY_STATE');
         L_error :=TRUE;
      end if;
      
      if rec.action in (action_new)
	     and rec.lang is NOT NULL
         and rec.cd_lang_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                    'LANG_EXIST');
         L_error :=TRUE;
      end if;

      if rec.lang = LP_primary_lang then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                     'ERR_PRIM_LANG',
                     'W');
        L_error:=true;      
      end if;
      
      if rec.action in (action_new,action_mod)
         and NOT(  rec.DESCRIPTION  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DESCRIPTION',
                     'DESC_COL_REQ');
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_state_tl_temp_rec.description              := rec.description;
         L_state_tl_temp_rec.country_id               := rec.country_id;
         L_state_tl_temp_rec.state                    := rec.state;
         L_state_tl_temp_rec.lang                     := rec.lang;
         L_state_tl_temp_rec.create_id                := USER;
         L_state_tl_temp_rec.last_update_id           := USER;         
         L_state_tl_temp_rec.create_datetime          := SYSDATE;
         L_state_tl_temp_rec.last_update_datetime     := SYSDATE;
 
         if rec.action = action_new then
            if EXEC_STATE_TL_INS(   L_state_tl_temp_rec,
                                    L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_STATE_TL_UPD( L_state_tl_temp_rec,
                                  L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_STATE_TL_DEL( L_state_tl_temp_rec,
                                  L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_state_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_state_tl st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_state_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_STATE_TL%isopen then
	     close c_svc_STATE_TL;
	  end if;      
	  L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_STATE_TL;
-----------------------------------------------------------------------
FUNCTION EXEC_STATE_INS(  L_state_temp_rec   IN   STATE%ROWTYPE,
                          O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_STATE.EXEC_STATE_INS';
   L_table   VARCHAR2(255):= 'SVC_STATE';
BEGIN
   insert
     into state
   values L_state_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_STATE_INS;
-------------------------------------------------------------------------------------
FUNCTION EXEC_STATE_UPD( L_state_temp_rec   IN   STATE%ROWTYPE,
                         O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_STATE.EXEC_STATE_UPD';
   L_table   VARCHAR2(255):= 'SVC_STATE';
BEGIN
   update state
      set row = L_state_temp_rec
    where 1 = 1
      and country_id = L_state_temp_rec.country_id
      and state = L_state_temp_rec.state
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_STATE_UPD;
-------------------------------------------------------------------------------------
FUNCTION EXEC_STATE_DEL(  L_state_temp_rec   IN   STATE%ROWTYPE ,
                          O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_STATE.EXEC_STATE_DEL';
   L_table   VARCHAR2(255):= 'SVC_STATE';

   cursor C_STATE_LOCK is
      select 'X'
        from STATE
       where country_id = L_state_temp_rec.country_id
         and state = L_state_temp_rec.state
         for update nowait;

   cursor C_STATE_TL_LOCK is
      select 'X'
        from STATE_tl
       where country_id = L_state_temp_rec.country_id
         and state = L_state_temp_rec.state
         for update nowait;
BEGIN
   open C_STATE_LOCK;
   close C_STATE_LOCK;
   
   open C_STATE_TL_LOCK;
   close C_STATE_TL_LOCK;

   delete
     from state_tl
    where 1 = 1
      and country_id = L_state_temp_rec.country_id
      and state = L_state_temp_rec.state;
      
   delete
     from state
    where 1 = 1
      and country_id = L_state_temp_rec.country_id
      and state = L_state_temp_rec.state;
      
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_STATE_LOCK%isopen then
	     close C_STATE_LOCK;
	  end if;
      if C_STATE_TL_LOCK%isopen then
	     close C_STATE_TL_LOCK;
	  end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_STATE_DEL;
----------------------------------------------------------------------------------------
FUNCTION PROCESS_STATE( I_process_id   IN   SVC_STATE.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_STATE.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error          BOOLEAN;
   L_process_error  BOOLEAN := FALSE;
   L_program        VARCHAR2(255):='CORESVC_STATE.PROCESS_STATE';
   L_error_message  VARCHAR2(600);
   L_STATE_temp_rec STATE%ROWTYPE;
   L_table          VARCHAR2(255)    :='SVC_STATE';
   L_exists         VARCHAR2(1);
   cursor c_child_state_exists(I_state svc_state.state%type,
                               I_country svc_state.country_id%type) is
	  select 'x'
	    from COUNTRY_TAX_JURISDICTION
	   where state=I_state
	     and country_id=I_country;
BEGIN
   FOR rec IN c_svc_STATE(I_process_id,
                          I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      
      if rec.action = action_new
         and rec.state is not null
         and rec.country_id is not null
         and rec.PK_STATE_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'STATE',
                                                NULL,
                                                NULL);          

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STATE/COUNTRY',
                     L_error_message);
         L_error :=TRUE;
      end if;
      
      if rec.action IN (action_mod,action_del)
         and rec.state is not null
         and rec.country_id is not null
         and rec.PK_STATE_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STATE/COUNTRY',
                     'NO_COUNTRY_STATE');
         L_error :=TRUE;
      end if;

      if rec.action in (action_new)
         and rec.country_id is not null
         and rec.sta_cnt_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COUNTRY_ID',
                     'INV_COUNTRY');
         L_error :=TRUE;
      end if;

      if rec.action in (action_new,action_mod)
         and NOT(  rec.DESCRIPTION  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DESCRIPTION',
                     'DESC_COL_REQ');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_del) then
	       open c_child_state_exists(rec.state,
		                               rec.country_id);
         fetch c_child_state_exists into L_exists;
         close c_child_state_exists;
         if L_exists='x' then		 
			      L_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_REC',
                                    						   'COUNTRY_TAX_JURISDICTION',
                                                    NULL,
                                                    NULL);
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'STATE,COUNTRY_ID',
                       L_error_message);
             L_error :=TRUE; 
         end if;		 
      end if;
      
      if NOT L_error then
         L_state_temp_rec.country_id               := rec.country_id;
         L_state_temp_rec.description              := rec.description;
         L_state_temp_rec.state                    := rec.state;
         L_state_temp_rec.create_id                := USER;
         L_state_temp_rec.create_datetime          := SYSDATE;
         if rec.action = action_new then
            if EXEC_STATE_INS(   L_state_temp_rec,
                                 L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_STATE_UPD( L_state_temp_rec,
                               L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_STATE_DEL( L_state_temp_rec,
                               L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_state st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_state st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_state st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_child_state_exists%isopen then
	     close c_child_state_exists;
	  end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_STATE;
-------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete 
     from svc_state_tl 
    where process_id=I_process_id;

   delete 
     from svc_state 
    where process_id=I_process_id;
END;
------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS 
   L_program VARCHAR2(255):='CORESVC_STATE.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';

BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;

   if PROCESS_STATE(I_process_id,
                    I_chunk_id)=FALSE then
      return FALSE;
   end if;
   commit;
   if PROCESS_STATE_TL(I_process_id,
                       I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
         
   LP_errors_tab := NEW errors_tab_typ();
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;


   return TRUE;
EXCEPTION
   when OTHERS then
      if c_get_err_count%isopen then
	     close c_get_err_count;
	  end if;
      if c_get_warn_count%isopen then
	     close c_get_warn_count;
	  end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_STATE;
/