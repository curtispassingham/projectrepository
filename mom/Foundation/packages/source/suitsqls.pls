CREATE OR REPLACE PACKAGE SUPP_ITEM_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------
--- Function Name:  ITEM_SUPP_COUNTRY_EXISTS
--- Purpose: If the supplier is not null, identifies the presence of an 
---          item/supplier/country combination on the item_supp_country table 
---          for the input origin country id otherwise checks the item/origin_country
---          relationship.
--- Calls: <none>
----------------------------------------------------------------------------------------
   FUNCTION ITEM_SUPP_COUNTRY_EXISTS (O_error_message  IN OUT VARCHAR2,
                                      O_exist          IN OUT BOOLEAN,
                                      I_item           IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                      I_supplier       IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                      I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
      RETURN BOOLEAN;
------------------------------------------------------------------------
--- Function Name:  ITEM_PART_COUNTRY_EXISTS
--- Purpose: If the partner is not null, identifies the presence of an 
---          item/partner/country combination on the item_supp_country table 
---          for the input origin country id.
--- Calls: <none>
----------------------------------------------------------------------------------------
   FUNCTION ITEM_PART_COUNTRY_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exist            IN OUT   BOOLEAN,
                                      I_item             IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                      I_partner_id       IN       PARTNER.PARTNER_ID%TYPE,
                                      I_origin_country   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                      I_partner_type     IN       PARTNER.PARTNER_TYPE%TYPE)
      RETURN BOOLEAN;
------------------------------------------------------------------------
--- Function Name:  COUNTRY_EXISTS
--- Purpose: Identifies the presence of an item/supplier/country combination on the
--           item_supp_country table if the origin country id is not null.
--- Calls: <none>
----------------------------------------------------------------------------------------
   FUNCTION COUNTRY_EXISTS (O_error_message IN OUT VARCHAR2,
                            O_exist         IN OUT BOOLEAN,
                            I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                            I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
      RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function Name: GET_COST
-- Purpose:       Get unit cost from item/supplier/country table for given 
--                item supplier and country.
-- Calls:         
----------------------------------------------------------------------------
   FUNCTION GET_COST ( O_error_message       IN OUT  VARCHAR2,
                       O_unit_cost           IN OUT  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                       I_item                IN      ITEM_SUPPLIER.ITEM%TYPE,
                       I_supplier            IN      ITEM_SUPPLIER.SUPPLIER%TYPE,
                       I_origin_country      IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_location            IN      ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                       I_delivery_country_id IN      ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE DEFAULT NULL,
                       I_new_item_loc_ind    IN      VARCHAR2 DEFAULT NULL)
   return BOOLEAN;
------------------------------------------------------------------------
-- Function Name: GET_PRI_SUP_COST
-- Purpose:       Get the primary supplier and unit cost from 
--                item_supp_country table for a given item number.
-- Calls:         ITEM_ATTRIB_SQL.GET_PACK_INDS
------------------------------------------------------------------------
   FUNCTION GET_PRI_SUP_COST ( O_error_message IN OUT VARCHAR2,
                               O_supplier      IN OUT ITEM_SUPPLIER.SUPPLIER%TYPE,
                               O_unit_cost_sup IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                               I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                               I_location      IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                               I_retrieve_ebc  IN     VARCHAR2 DEFAULT 'N')
            return BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CHECK_COUNTRY_DELETE
-- Purpose:       Checks the purchasing  and item/supplier/country modules 
--                for existence of item/supplier/origin country combinations.
--                Checks if item is not a pack, makes sure it is 
--                not a component of a pack that is associated with the supplier/origin 
--                country being deleted.  If it is displays error and stops processing.
-- Calls:         
--------------------------------------------------------------------------------
   FUNCTION CHECK_COUNTRY_DELETE( O_error_message  IN OUT VARCHAR2,
                                  O_exist          IN OUT BOOLEAN,
                                  I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                  I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                  I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
            return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: LOCK_COUNTRY_DELETE
-- Purpose: Locks records before deletion of any children of the Item_supplier table 
-- Calls:   <none>
--------------------------------------------------------------------------------
   FUNCTION LOCK_COUNTRY_DELETE(O_error_message  IN OUT VARCHAR2,
                                I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
            return BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DELETE_COUNTRY_RELATIONS
-- Purpose: Locks, then deletes any dependencies to the Item/Supplier/Origin Country relationship
-- Calls:   LOCK_COUNTRY_DELETE
--------------------------------------------------------------------------------
   FUNCTION DELETE_COUNTRY_RELATIONS( O_error_message  IN OUT VARCHAR2,
                                      I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                      I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                      I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
            return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_SUPPLIER_DELETE
-- Purpose:       Checks the packs, purchasing, rtv, and contracting modules 
--                for existence of item/supplier combinations.  Makes sure the 
--                supplier is not the primary supplier.
--                Checks if item is not a pack, makes sure it is 
--                not a component of a pack that is associated with the supplier/origin 
--                country being deleted.  If it is displays error and stops processing.
-- Calls:         ORDDBA_SQL.DBA_EXISTS, SYSTEM_OPTIONS_SQL.GET_CONTRACT_IND,
--                ITEM_ATTRIB_SQL.TYPE
--------------------------------------------------------------------------------
   FUNCTION CHECK_SUPPLIER_DELETE(O_error_message  IN OUT VARCHAR2,
                                  O_exist          IN OUT BOOLEAN,
                                  I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                  I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
            return BOOLEAN;

----------------------------------------------------------------------------------------
-- Function Name: DELETE_SUPPLIER_RELATIONS
-- Purpose: Locks, then deletes any dependencies of the Item_Supplier table
-- Calls:   LOCK_SUPPLIER_DELETE
--------------------------------------------------------------------------------
   FUNCTION DELETE_SUPPLIER_RELATIONS( O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                       I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
      return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: VALID_COUNTRY
-- Purpose: Checks if entered origin country is valid on the countries table
--          and that the item/supplier/origin country combination doesn't already
--          exist
-- Calls:   ITEM_ATTRIB_SQL.TYPE, PACKITEM_ATTRIB_SQL.GET_PACK_INFO, 
--          COUNTRY_VALIDATE_SQL.EXISTS_ON_TABLE
--------------------------------------------------------------------------------
   FUNCTION VALID_COUNTRY( O_error_message  IN OUT VARCHAR2,
                           O_valid          IN OUT BOOLEAN,
                           I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                           I_supplier       IN OUT ITEM_SUPPLIER.SUPPLIER%TYPE,
                           I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
      RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: UPDATE_PRIMARY_INDICATORS
-- Purpose: Updates applicable primary supplier, primary country, and location 
--          indicators on the item_supplier, item_supp_country, and 
--          item_supp_country_loc tables. If I_default_ind is yes, then the function
--          will verify that all of the lower level items can be changed, and then
--          call SUPP_ITEM_SQL.UPDATE_PRIMARY_CHILD_INDS to update those indicators.
--
FUNCTION UPDATE_PRIMARY_INDICATORS( O_error_message  IN OUT VARCHAR2,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                    I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                    I_location       IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                                    I_default_ind    IN     VARCHAR2)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: EXISTS
-- Purpose:       Identifies the presence of an item/supplier combination in the
--                item_supplier table.
--------------------------------------------------------------------------------
FUNCTION EXIST ( O_error_message IN OUT VARCHAR2,
                 O_exist         IN OUT BOOLEAN,
                 I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                 I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name : UOM_EXISTS 
-- Purpose       : Checks if the passed in Item/Supplier/UOM relationship exists
-- Calls         : <none>
--------------------------------------------------------------------------------
FUNCTION UOM_EXISTS(O_error_message IN OUT VARCHAR2,
                    O_exists        IN OUT BOOLEAN,
                    I_item          IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                    I_supplier      IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                    I_uom           IN     UOM_CLASS.UOM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: VALIDATE_SUPPLIER
-- Purpose: Verifies if a supplier/item exist.  If item is a pack, verifies all
--          items in pack have passed in supplier.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER(O_error_message IN OUT VARCHAR2,
                           I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                           I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                           I_item          IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: LOCK_SUPPLER_DELETE
-- Purpose: This function locks the following tables for the item/supplier passed in
-- SUP_AVAIL, COST_SUSP_SUP_DETAIL, ITEM_EXP_HEAD, 
-- ITEM_EXP_DETAIL, ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_LOC, DEAL_ITEMLOC,
-- DEAL_SKU_TEMP, DEAL_SKU_COST.
--------------------------------------------------------------------------------
FUNCTION LOCK_SUPPLIER_DELETE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_supplier        IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                              I_item            IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: EXPENSES_EXIST
-- Purpose: This function checks if a record exists on ITEM_EXP_DETAIL for the 
--          item/supplier combination.
--------------------------------------------------------------------------------
 FUNCTION EXPENSES_EXIST(O_error_message    IN OUT VARCHAR2,
                         O_exists           IN OUT BOOLEAN,
                         I_supplier         IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                         I_item             IN     ITEM_SUPPLIER.ITEM%TYPE)
    RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: UPDATE_INSERT_SUPP_TO_CHILDREN
-- Purpose: This function will first update the supplier info for all child items
--          if they already exist for the passed in supplier.  Then it will 
--          default the inputted supplier to all of the child
--          items of the inputted item that do not already have the supplier
--          and are at or above the transaction level.
---------------------------------------------------------------------------------------
FUNCTION UPDATE_INSERT_SUPP_TO_CHILDREN(O_error_message         IN OUT VARCHAR2,
                                        I_consignment_rate      IN     ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
										I_concession_rate       IN     ITEM_SUPPLIER.CONCESSION_RATE%TYPE,
                                        I_pallet_name           IN     ITEM_SUPPLIER.PALLET_NAME%TYPE,
                                        I_case_name             IN     ITEM_SUPPLIER.CASE_NAME%TYPE,
                                        I_inner_name            IN     ITEM_SUPPLIER.INNER_NAME%TYPE,
                                        I_supp_discontinue_date IN     ITEM_SUPPLIER.SUPP_DISCONTINUE_DATE%TYPE,
                                        I_direct_ship_ind       IN     ITEM_SUPPLIER.DIRECT_SHIP_IND%TYPE,
                                        I_supplier              IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_item                  IN     ITEM_SUPPLIER.ITEM%TYPE,
                                        I_update_only_ind       IN     VARCHAR2,
                                        I_primary_case_size     IN     ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: DELETE_SUPPLIER_FROM_CHILDREN
-- Purpose: This function will delete from item_supplier and all it's related tables.
-- Calls: Three internal functions: CHECK_SUPPLIER_CHILD_DELETE, 
--        LOCK_SUPPLIER_CHILD_DELETE, DELETE_SUPPLIER_CHILD_RELATIONS.
--------------------------------------------------------------------------------
FUNCTION DELETE_SUPPLIER_FROM_CHILDREN(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                       I_item           IN     ITEM_SUPPLIER.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: UPDATE_PRIMARY_CHILD_INS
-- Purpose: This function update the primary supplier indicator on item_supp_country
-- and item_supplier to the inputted supplier/country.
-- Calls: 
--------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_CHILD_INDS( O_error_message  IN OUT VARCHAR2,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                    I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                    I_location       IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: UPDATE_PRIM_SUPPLIER_FOR_CHILDREN
-- Purpose: This function will delete from item_supplier and all it's related tables.
-- Calls: SUP_ITEM_SQL.COUNTRY_EXISTS, SUP_ITEM_SQL.UPDATE_PRIMARY_INDICATORS,
--        UPDATE_BASE_COST.CHANGE_SUPPLIER.
--------------------------------------------------------------------------------
FUNCTION UPDATE_PRIM_SUPP_FOR_CHILDREN(O_error_message  IN OUT VARCHAR2,
                                       I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                       I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_PRIM_SUPPLIER_FOR_CHILDREN
-- Purpose: This function will check if the inputted item has any children that
-- have a primary supplier different from inputted supplier.
-- Calls: 
--------------------------------------------------------------------------------
FUNCTION CHECK_PRIM_SUPP_FOR_CHILDREN(O_error_message   IN OUT VARCHAR2,
                                      O_prim_supp_ind   IN OUT ITEM_SUPPLIER.PRIMARY_SUPP_IND%TYPE,
                                      I_supplier        IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                      I_item            IN     ITEM_SUPPLIER.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: DELETE_SUPPLIER_FROM_CHILDREN
-- Purpose: This function will delete from item_supp_country and all it's related tables.
-- Calls: Three internal functions: CHECK_COUNTRY_CHILD_DELETE, 
--        LOCK_COUNTRY_CHILD_DELETE, DELETE_COUNTRY_CHILD_RELATIONS.
--------------------------------------------------------------------------------
FUNCTION DELETE_COUNTRY_FROM_CHILDREN(O_error_message   IN OUT VARCHAR2,
                                      I_item            IN     ITEM_SUPPLIER.ITEM%TYPE,
                                      I_supplier        IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                      I_origin_country  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: UPDATE_SUPP_TO_SUB_TRAN_CHILD
-- Purpose: This function will update the item-supplier information for all 
--          child items for the specified item-supplier combination. The 
--          information updated is only what is madatory to be defaulted down
--          from the parent. If the user wants all of the information to be
--          defaulted down, then a call to UPDATE_INSERT_SUPP_TO_CHILDREN will
--          be made instead of this function. This function will only be called
--          for specified items that are at the transaction level. This function
--          should never be called for items above the transaction level.
--
FUNCTION UPDATE_SUPP_TO_SUB_TRAN_CHILD(O_error_message     IN OUT VARCHAR2,
                                       I_consignment_rate  IN OUT ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
									   I_concession_rate   IN OUT ITEM_SUPPLIER.CONCESSION_RATE%TYPE,
                                       I_pallet_name       IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                                       I_case_name         IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                                       I_inner_name        IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                                       I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                       I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                                       I_primary_case_size IN     ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CNTRY_EXISTS_FOR_ALL_SUPPS
-- Purpose: This function will check if there are any item-supplier combinations
--          that do not have a corresponding item-supplier-origin country record.
--          O_all_exist will be true if each supplier has at least one origin
--          country record for the specified item. O_all_exist will be false if 
--          there is any supplier without an origin country record for the 
--          specified item, and O_supplier will be populated with the first supplier
--          found to be without an origin country record.
--
FUNCTION CNTRY_EXISTS_FOR_ALL_SUPPS( O_error_message  IN OUT VARCHAR2,
                                     O_all_exist      IN OUT BOOLEAN,
                                     O_supplier       IN OUT ITEM_SUPPLIER.SUPPLIER%TYPE,
                                     I_item           IN     ITEM_SUPPLIER.ITEM%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function: CHECK_CHILD_ITSUPPCNTRY
-- Purpose:  This function will verify that all lower level items of a specified
--           item have a common item-supplier-origin country relationship with the
--           specified item, specified supplier, and optionally specified origin
--           country. O_valid will be TRUE when the common relationship exists. It
--           will be FALSE if any lower level item does not have the common
--           relationship.
--
FUNCTION CHECK_CHILD_ITSUPPCNTRY( O_error_message      IN OUT VARCHAR2,
                                  O_valid              IN OUT BOOLEAN,
                                  I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                  I_supplier           IN OUT ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                  I_origin_country_id  IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function: CHECK_NEW_PRIMARY_IND
-- Purpose:  This function will be called when attempting to select a new primary
--           supplier from the item-supplier form or a new primary origin country
--           for the primary supplier from the item-supplier-origin country form.
--           The origin country ID will not be specified if the primary supplier
--           is being selected, and this will require a check to make sure a 
--           item-supplier-origin country relationship already exists for the new
--           primary supplier. If the item uses a standard unit of measure that is 
--           a quantity, then a unit of measure check needs to be done to identify 
--           if there are any price zones or price changes for the item that use a 
--           selling unit of measure that is not a quantity. If the unit of measure
--           check finds a record, then a case check needs to be done to identify if
--           there case dimension object that exists for the primary item-supplier 
--           relationship where the origin country either is primary or going to be
--           primary. O_valid will be FALSE if an origin country does not exist for
--           item-supplier relationship. O_valid will be FALSE the case check does
--           not find a record for the item-supplier-origin country-dimension. When
--           O_valid is FALSE, O_error_message will get the proper error message, 
--           and the function will return TRUE.
--
FUNCTION CHECK_NEW_PRIMARY_IND( O_error_message      IN OUT VARCHAR2,
                                O_valid              IN OUT BOOLEAN,
                                I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                                I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
-- Function: EXIST (another EXIST function has an additional parameter I_supplier)
--  Purpose: Identifies the presence of an item (with any supplier) on the
--           item_supplier table.
--------------------------------------------------------------------------------
FUNCTION EXIST(O_error_message IN OUT VARCHAR2,
               O_exist         IN OUT BOOLEAN,
               I_item          IN     ITEM_SUPPLIER.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION INSERT_SUPP_TO_COMP_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                                  I_supplier      IN     SUPS.SUPPLIER%TYPE) 
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function: INSERT_ITEM_SUPPLIER 
--  Purpose: Inserts 1 row into item_supplier table.
--------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item_supp_rec IN ITEM_SUPPLIER%ROWTYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function: UPDATE_ITEM_SUPPLIER 
--  Purpose: Updates 1 row in the item_supplier table.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item_supp_rec IN ITEM_SUPPLIER%ROWTYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function: UPDATE_YES_PRIM_IND
--  Purpose: Updates primary_supp_ind to Y for 1 row in the item_supplier table.
--           NOTE:  only used for XITEM subscription API after UPDATE_PRIMARY_INDICATORS is called
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_YES_PRIM_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item_supp_rec IN ITEM_SUPPLIER%ROWTYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function: DELETE_ITEM_SUPPLIER 
--  Purpose: Deletes 1 row from the item_supplier table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                              I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: VALID_DEAL_ITEM
--       Purpose: This function will validate that items have an existing 
--                item/supplier/country record in the item_supp_country table
----------------------------------------------------------------------------------
FUNCTION VALID_DEAL_ITEM(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid           IN OUT BOOLEAN,
                         I_item            IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                         I_supplier        IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_country_id      IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_diff            IN     ITEM_MASTER.DIFF_1%TYPE,
                         I_merch_lvl       IN     CODE_DETAIL.CODE%TYPE,
                         I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
                         I_partner_type    IN     PARTNER.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
-- Function:  COST_CONTROL_LEVEL
--  Purpose:  This function will determine if the Cost is controlled at the 
--            Item/Supp/Country level or at the Item/Supp/Country/Location level.
----------------------------------------------------------------------------------
FUNCTION COST_CONTROL_LEVEL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_cost_control_level IN OUT VARCHAR2,
                            I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                            I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                            I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function:  NULLIFY_PRIMARY_CASE_SIZE
--  Purpose:  This function will nullify the Primary Case Size of the passed Item.
----------------------------------------------------------------------------------
FUNCTION NULLIFY_PRIMARY_CASE_SIZE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item               IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function:  PRIMARY_CASE_SIZE_EXIST
--  Purpose:  This function checks if Primary Case Size exists on ITEM_SUPLLIER for the 
--            given item for all suppliers.
----------------------------------------------------------------------------------
FUNCTION PRIMARY_CASE_SIZE_EXIST(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exist              IN OUT BOOLEAN,
                                 I_item               IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function:  DEFAULT_CHILD_AIP_INFO 
--  Purpose:  If the passed Item's AIP_CASE_TYPE is 'F', this function updates the 
--            AIP_CASE_TYPE to 'formal' for all its children and grandchildren in item_master table.
--            Updates PRIMARY_CASE_SIZE to NULL for all its children and grandchildren
--            in item_supplier table.
--            If the passed item's AIP_CASE_TYPE is 'I', this function updates the 
--            AIP_CASE_TYPE to 'informal' for all its children and grandchildren in item_master table.
----------------------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_AIP_INFO (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                                 I_aip_case_type      IN     ITEM_MASTER.AIP_CASE_TYPE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- Function Name: CHECK_SUPP_DEAL_EXISTS
-- Purpose:       Checks the deal module for existence of item/supplier combination.
--                If system_options.supplier_sites ind is 'Y', then the check for the 
--                last item/supplier site combination. If it is displays error then 
--                send a warning message to the user before confirming the deletion.
-- Calls:         SYSTEM_OPTIONS_SQL.SUPPLIER_SITES_IND
--------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_DEAL_EXISTS(O_error_message  IN OUT VARCHAR2,
                                O_exist          IN OUT BOOLEAN,
                                I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function:  DELETE_ITEM_SUPPLIER_TL
--  Purpose:  This function is used to delete values from ITEM_SUPPLIER_TL.
----------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_SUPPLIER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item            IN       ITEM_SUPPLIER_TL.ITEM%TYPE,
                                 I_supplier        IN       ITEM_SUPPLIER_TL.SUPPLIER%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function:  DEFAULT_PRIMARY_CASE_SIZE
--  Purpose:  This function will default the Primary Case Size of the passed Item with 
--            the item-supp-country default_uop for the primary country for the supplier.
FUNCTION DEFAULT_PRIMARY_CASE_SIZE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item               IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------

END SUPP_ITEM_SQL;
/
