
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SIT_ATTRIB_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------
-- Function Name:  GET_HEADER_INFO
-- Purpose:        This function will retrieve the description of the passed in
--                 itemloc link id, the corresponding item list, and location list.
--------------------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message      IN OUT VARCHAR2,
                         O_itemloc_link_desc  IN OUT SIT_HEAD.ITEMLOC_LINK_DESC%TYPE,
                         O_itemlist           IN OUT SIT_HEAD.SKULIST%TYPE,
                         O_loc_list           IN OUT SIT_HEAD.LOC_LIST%TYPE,
                         I_itemloc_link_id    IN     SIT_HEAD.ITEMLOC_LINK_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name:  GET_NEXT_ITEMLOC_LINK_ID
-- Purpose:        This function will retrieve the next value for the Itemloc link ID, 
--                 which is a system generated primary key.  This function will use the 
--                 ITEMLOC_LINK_SEQUENCE to keep the key unique.
--------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ITEMLOC_LINK_ID(O_error_message    IN OUT VARCHAR2,
                                  O_itemloc_link_id  IN OUT SIT_HEAD.ITEMLOC_LINK_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------- 
-- Function Name: LINK_EXISTS
-- Purpose:       This function will check to see if a passed in itemloc_link_id exists
--                on SIT_HEAD table.  If a record exists the O_exists indicator will be 
--                set to true, if it does not exist the O_exists indicator will be 
--                set to false.
--------------------------------------------------------------------------------------------
FUNCTION LINK_EXISTS(O_error_message         IN OUT VARCHAR2,
                     O_exists                IN OUT BOOLEAN,
                     I_itemloc_link_id       IN     SIT_HEAD.ITEMLOC_LINK_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function: VALIDATE_DATE
-- Purpose: This function will check the sit_detail table to determine if the
--          passed in date is already used for the passed in itemloc_link_id.
--          If the date is found the O_exists indicator outputs TRUE otherwise FALSE.
--------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DATE(O_error_message    IN OUT VARCHAR2,
                       O_exists           IN OUT BOOLEAN,
                       I_itemloc_link_id  IN     SIT_DETAIL.ITEMLOC_LINK_ID%TYPE,
                       I_status_date      IN     SIT_DETAIL.STATUS_UPDATE_DATE%TYPE) 
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END SIT_ATTRIB_SQL;
/
