CREATE OR REPLACE PACKAGE CORESVC_RTK_ERRORS AUTHID CURRENT_USER AS
   template_key              CONSTANT VARCHAR2(255):= 'RTK_ERRORS_DATA';
   template_category         CONSTANT VARCHAR2(255):= 'RMSADM';
   action_new                VARCHAR2(25)          := 'NEW';
   action_mod                VARCHAR2(25)          := 'MOD';
   action_del                VARCHAR2(25)          := 'DEL';
   
   RTK_ERRORS_sheet          VARCHAR2(255)         := 'RTK_ERRORS';
   RTK_ERRORS$Action         NUMBER                :=1;
   RTK_ERRORS$RTK_TYPE       NUMBER                :=2;
   RTK_ERRORS$RTK_KEY        NUMBER                :=3;
   RTK_ERRORS$RTK_TEXT       NUMBER                :=4;
   RTK_ERRORS$RTK_APPROVED   NUMBER                :=5;
   
   RTK_ERRORS_TL_sheet       VARCHAR2(255)         := 'RTK_ERRORS_TL';
   RTK_ERRORS_TL$Action      NUMBER                :=1;
   RTK_ERRORS_TL$LANG        NUMBER                :=2;
   RTK_ERRORS_TL$RTK_KEY     NUMBER                :=3;
   RTK_ERRORS_TL$RTK_TEXT    NUMBER                :=4;


   Type RTK_ERRORS_rec_tab IS TABLE OF RTK_ERRORS%ROWTYPE;
   sheet_name_trans   S9T_PKG.trans_map_typ;
   action_column      VARCHAR2(255) := 'ACTION';
---------------------------------------------------------------------------------  
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
---------------------------------------------------------------------------------
END CORESVC_RTK_ERRORS;
/