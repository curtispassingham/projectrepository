
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SCRIPTBUILDER AS

   C1             INTEGER;
   TYPE           t_column_type_table IS TABLE OF VARCHAR2(30) INDEX BY BINARY_INTEGER;
   LP_column_type t_column_type_table;
-------------------------------------------------------------
FUNCTION PREPARE_CURSOR(I_table_name      IN VARCHAR2,
                        I_owner           IN VARCHAR2,
                        I_where_clause    IN VARCHAR2,
                        O_column_names    IN OUT VARCHAR2,
                        O_num_columns     IN OUT NUMBER,
                        IO_error_message  IN OUT VARCHAR2)
   RETURN BOOLEAN IS
   L_column_name                    VARCHAR2(30)         := NULL;
   L_program                        VARCHAR2(40)         := 'SCRIPTBUILDER.PREPARE_CURSOR';
   L_data_type                      VARCHAR2(30)         := NULL;
   L_data_length                    NUMBER               := NULL;
   L_col_num                        NUMBER               := 0;            
   L_string                         VARCHAR2(2000)       := NULL;
   RC                               INTEGER; 
   L_stmt                           VARCHAR2(2000);
   O_done                           BOOLEAN              := FALSE;

   cursor C_COLUMNS is
      select column_name,
             data_type,
             data_length
      from   all_tab_columns
     where   owner = I_owner
       and   table_name = I_table_name
  order by   column_name;

BEGIN
   open C_COLUMNS;
   LOOP
      FETCH C_COLUMNS into L_column_name,
                           L_data_type,
                           L_data_length;
      exit when C_COLUMNS%NOTFOUND;

      if L_string is null then   
         L_string := L_column_name;
      else                        
         L_string := L_string ||', '||L_column_name;
      end if;
   END LOOP;

   if I_where_clause is null then
      L_stmt := 'SELECT '||L_string|| ' FROM '||I_owner||'.'||I_table_name; 
   else
      L_stmt := 'SELECT '||L_string|| ' FROM '||I_owner||'.'||I_table_name || ' WHERE '|| I_where_clause;
   end if;
   close C_COLUMNS;
   C1 := DBMS_SQL.OPEN_CURSOR;
   L_col_num := 0;
   BEGIN
      DBMS_SQL.PARSE(C1,L_stmt,DBMS_SQL.V7);
   EXCEPTION
      when OTHERS then
         DBMS_SQL.CLOSE_CURSOR(C1);  
         IO_error_message := 'ARI_INV_WHERE_CLAUSE';
         return FALSE;
   END; 
     
   open C_COLUMNS;

   LOOP
      FETCH C_COLUMNS into L_column_name,
                           L_data_type,
                           L_data_length;
      exit when C_COLUMNS%NOTFOUND;
      L_col_num := L_col_num +1;
      L_data_length := 255;
      DBMS_SQL.DEFINE_COLUMN(C1, L_col_num, L_data_type, L_data_length);
      LP_column_type(L_col_num) := L_data_type;
   END LOOP;
   O_column_names := L_string;
   O_num_columns := L_col_num;
   RC := DBMS_SQL.EXECUTE(C1);
   close C_COLUMNS;
   
   return TRUE;

EXCEPTION

   when OTHERS then
      DBMS_SQL.CLOSE_CURSOR(C1);
      IO_error_message := to_char(SQLCODE);
      return FALSE;      

END PREPARE_CURSOR;

-----------------------------------------------------------------------------------------
FUNCTION FETCH_NEXT_ROW(O_done           IN OUT    BOOLEAN,
                        IO_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40):= 'SCRIPTBUILDER.FETCH_NEXT_ROW';
   RC                   NUMBER;

BEGIN

   if DBMS_SQL.FETCH_ROWS(C1) = 0 then
      DBMS_SQL.CLOSE_CURSOR(C1);
      O_done := TRUE;
   else
      O_done := FALSE;
   end if;

   Return TRUE;

EXCEPTION

   when OTHERS then
     IO_error_message := to_char(SQLCODE);
     Return FALSE;

END FETCH_NEXT_ROW; 
---------------------------------------------------------------------------------------

FUNCTION FETCH_NEXT_COLUMN_VALUE(I_col_num        IN        NUMBER,
                                 O_column_value   IN OUT    VARCHAR2,
                                 IO_error_message IN OUT    VARCHAR2)
   Return BOOLEAN IS

   L_varchar2_type        VARCHAR2(2000);
   L_program              VARCHAR2(40):= 'SCRIPTBUILDER.FETCH_NEXT_COLUMN_VALUE';

BEGIN 

   if LP_column_type(I_col_num) = 'VARCHAR2' then
      DBMS_SQL.COLUMN_VALUE(C1, I_col_num, L_varchar2_type);
      L_varchar2_type := REPLACE(L_varchar2_type,'''','''''');
      
      O_column_value := REPLACE(L_varchar2_type,chr(10),''''||'||chr(10)||'||'''');
      
      Return TRUE;
   else
      DBMS_SQL.COLUMN_VALUE(C1, I_col_num, L_varchar2_type);
      O_column_value := L_varchar2_type;
      Return TRUE;
   end if;

EXCEPTION

   when OTHERS then
      IO_error_message := to_char(SQLCODE);
      Return FALSE;

END FETCH_NEXT_COLUMN_VALUE; 
END SCRIPTBUILDER;
/
