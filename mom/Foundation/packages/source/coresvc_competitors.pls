CREATE OR REPLACE PACKAGE CORESVC_COMPETITOR AUTHID CURRENT_USER AS

   template_key                 CONSTANT VARCHAR2(255) := 'COMPETITOR_DATA';
   template_category            CONSTANT VARCHAR2(255) := 'RMSPCO';
   action_new                   VARCHAR2(25)           := 'NEW';
   action_mod                   VARCHAR2(25)           := 'MOD';
   action_del                   VARCHAR2(25)           := 'DEL';
   
   competitor_sheet             VARCHAR2(255)          := 'COMPETITOR';
   competitor$action            NUMBER                 :=1;
   competitor$jurisdiction_code NUMBER                 :=14;
   competitor$website           NUMBER                 :=13;
   competitor$fax               NUMBER                 :=12;
   competitor$phone             NUMBER                 :=11;
   competitor$post_code         NUMBER                 :=10;
   competitor$country_id        NUMBER                 :=9;
   competitor$state             NUMBER                 :=8;
   competitor$city              NUMBER                 :=7;
   competitor$address_3         NUMBER                 :=6;
   competitor$address_2         NUMBER                 :=5;
   competitor$address_1         NUMBER                 :=4;
   competitor$comp_name         NUMBER                 :=3;
   competitor$competitor        NUMBER                 :=2;
   
   COMP_STORE_sheet                 VARCHAR2(255)      := 'COMP_STORE';
   COMP_STORE$Action                NUMBER             :=1;
   COMP_STORE$JURISDICTION_CODE     NUMBER             :=21;
   COMP_STORE$ESTIMATED_VOLUME      NUMBER             :=20;
   COMP_STORE$CLOSE_DATE            NUMBER             :=19;
   COMP_STORE$OPEN_DATE             NUMBER             :=18;
   COMP_STORE$SELLING_SQUARE_FEET   NUMBER             :=17;
   COMP_STORE$TOTAL_SQUARE_FEET     NUMBER             :=16;
   COMP_STORE$CURRENCY_CODE         NUMBER             :=15;
   COMP_STORE$STORE_FORMAT          NUMBER             :=14;
   COMP_STORE$FAX                   NUMBER             :=13;
   COMP_STORE$PHONE                 NUMBER             :=12;
   COMP_STORE$POST_CODE             NUMBER             :=11;
   COMP_STORE$COUNTRY_ID            NUMBER             :=10;
   COMP_STORE$STATE                 NUMBER             :=9;
   COMP_STORE$CITY                  NUMBER             :=8;
   COMP_STORE$ADDRESS_3             NUMBER             :=7;
   COMP_STORE$ADDRESS_2             NUMBER             :=6;
   COMP_STORE$ADDRESS_1             NUMBER             :=5;
   COMP_STORE$STORE_NAME            NUMBER             :=4;
   COMP_STORE$COMPETITOR            NUMBER             :=3;
   COMP_STORE$STORE                 NUMBER             :=2;
   
   COMP_STORE_LINK_sheet             VARCHAR2(255)     := 'COMP_STORE_LINK';
   COMP_STORE_LINK$Action            NUMBER            :=1;
   COMP_STORE_LINK$UOM            	 NUMBER            :=7;
   COMP_STORE_LINK$DISTANCE          NUMBER            :=6;
   COMP_STORE_LINK$RANK              NUMBER            :=5;
   COMP_STORE_LINK$TAR_COMP_IND      VARCHAR2(3)       :='N';
   COMP_STORE_LINK$COMP_STORE        NUMBER            :=3;
   COMP_STORE_LINK$STORE             NUMBER            :=2;
   
   COMP_SHOPPER_sheet          VARCHAR2(255)           := 'COMP_SHOPPER';
   COMP_SHOPPER$Action         NUMBER                  := 1;
   COMP_SHOPPER$SHOPPER        NUMBER                  := 2;
   COMP_SHOPPER$SHOPPER_NAME   NUMBER                  := 3;
   COMP_SHOPPER$SHOPPER_PHONE  NUMBER                  := 4;
   COMP_SHOPPER$SHOPPER_FAX    NUMBER                  := 5;
   
   TYPE COMPETITOR_rec_tab IS TABLE OF COMPETITOR%ROWTYPE;
   TYPE COMP_STORE_rec_tab IS TABLE OF COMP_STORE%ROWTYPE;
   TYPE COMP_STORE_LINK_rec_tab IS TABLE OF COMP_STORE_LINK%ROWTYPE;
   TYPE COMP_SHOPPER_rec_tab IS TABLE OF COMP_SHOPPER%ROWTYPE;
  
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
   
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
	
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
	
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,   
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
	
	FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
	
END CORESVC_COMPETITOR;
/
