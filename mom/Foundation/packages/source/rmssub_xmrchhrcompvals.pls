
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRCOMP_VALIDATE AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       O_company_rec     OUT    NOCOPY  COMPHEAD%ROWTYPE,
                       I_message         IN             "RIB_XMrchHrCompDesc_REC",
                       I_message_type    IN             VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRCOMP_VALIDATE;
/
