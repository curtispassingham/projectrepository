CREATE OR REPLACE PACKAGE BODY DIFF_FINALIZATION
AS
  --------------------------------------------------------------------------------
  
PROCEDURE GET_DESC_LENGTH(O_error_message       IN OUT  VARCHAR2,
                          O_parent_desc_length  IN OUT  NUMBER,
                          O_diff_desc_length    IN OUT  NUMBER,
                          I_item_desc           IN      SVC_ITEM_MASTER.ITEM_DESC%TYPE,
                          I_diff_no             IN      NUMBER) ;    
-------------------------------------------------------------------------------- 
FUNCTION VALIDATE_INPUTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_parent_item     IN OUT   ITEM_MASTER.ITEM%TYPE,
                         O_VPN             IN OUT   ITEM_SUPPLIER.VPN%TYPE,
                         I_ITEM            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)   := 'DIFF_FINALIZATION.VALIDATE_INPUTS';

   cursor C_VALIDATE is
      select DECODE(item_level,1,item,item_parent)
        from svc_item_master sim
       where (item = I_item
              or item_parent = I_item)
         and (item_level = 1
              or item_level = 2)
         and NOT EXISTS(select 1 
                          from svc_item_master sim2 
                         where (sim.item = sim2.item 
                                or sim.item_parent = sim2.item)
                           and sim2.diff_finalized_ind = 'Y'
                           and ROWNUM = 1);

BEGIN
   O_parent_item := NULL;

   open C_VALIDATE;
   fetch C_VALIDATE into O_parent_item;
   close C_VALIDATE;

   if O_parent_item is NULL then
      O_error_message:='DIFF_INVALID_ITEM';
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END VALIDATE_INPUTS;
  --------------------------------------------------------------------------------
FUNCTION INITIALIZE(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.INITIALIZE';
  CURSOR C_diff_editable_ind
  IS
    SELECT 'N'
    FROM item_master im
    WHERE item     = I_parent_item
    AND ( (status <> 'W')
    OR EXISTS
      (SELECT 1 FROM item_master im1 WHERE im1.item_parent = im.item
      ) );
    L_diff_editable_ind DIFF_FNLZ_HEAD_GTT.DIFF_GROUPS_EDITABLE%TYPE:='Y';
  BEGIN
    --Insert one record in head gtt table
    INSERT
    INTO DIFF_FNLZ_HEAD_GTT
      (
        ITEM,
        ITEM_PARENT,
        ITEM_PARENT_DESC,
        VPN,
        ITEM_NUMBER_TYPE,
        DIFF_1,
        DIFF_1_DESC,
        DIFF_1_AGGREGATE_IND,
        DIFF_1_ID_GROUP_IND,
        DIFF_2,
        DIFF_2_DESC,
        DIFF_2_AGGREGATE_IND,
        DIFF_2_ID_GROUP_IND,
        DIFF_3,
        DIFF_3_DESC,
        DIFF_3_AGGREGATE_IND,
        DIFF_3_ID_GROUP_IND,
        DIFF_4,
        DIFF_4_DESC,
        DIFF_4_AGGREGATE_IND,
        DIFF_4_ID_GROUP_IND,
        DIFF_GROUPS_EDITABLE
      )
  WITH inp AS
    -- Initialize row to make sure it always brings one row
    (
    SELECT I_item   AS item,
      I_parent_item AS item_parent,
      I_vpn         AS vpn
    FROM dual
    ),
    step1 AS
    --Get other details for the parent item from either svc_item_master or from item_master
    (
    SELECT inp.item,
      inp.item_parent,
      inp.vpn,
      DECODE(sim.item,NULL,im.item_desc,sim.item_desc)                       AS item_parent_desc,
      DECODE(sim.item,NULL,im.diff_1,sim.diff_1)                             AS diff_1,
      DECODE(sim.item,NULL,im.DIFF_1_AGGREGATE_IND,sim.DIFF_1_AGGREGATE_IND) AS DIFF_1_AGGREGATE_IND,
      DECODE(sim.item,NULL,im.diff_2,sim.diff_2)                             AS diff_2,
      DECODE(sim.item,NULL,im.DIFF_2_AGGREGATE_IND,sim.DIFF_2_AGGREGATE_IND) AS DIFF_2_AGGREGATE_IND,
      DECODE(sim.item,NULL,im.diff_3,sim.diff_3)                             AS diff_3,
      DECODE(sim.item,NULL,im.DIFF_3_AGGREGATE_IND,sim.DIFF_3_AGGREGATE_IND) AS DIFF_3_AGGREGATE_IND,
      DECODE(sim.item,NULL,im.diff_4,sim.diff_4)                             AS diff_4,
      DECODE(sim.item,NULL,im.DIFF_4_AGGREGATE_IND,sim.DIFF_4_AGGREGATE_IND) AS DIFF_4_AGGREGATE_IND
    FROM inp,
      svc_item_master sim,
      item_master im
    WHERE inp.item_parent = sim.item (+)
    AND inp.item_parent   = im.item (+)
    ),
    step2 AS
    --Get diff descriptions and id_group indicators for the 4 diffs
    (
    SELECT s.item,
      s.item_parent,
      s.item_parent_desc,
      s.vpn,
      NULL AS item_number_type,
      s.diff_1,
      v1.description AS DIFF_1_DESC,
      s.DIFF_1_AGGREGATE_IND,
      v1.id_group_ind AS diff_1_id_group_ind,
      s.diff_2,
      v2.description AS DIFF_2_DESC,
      s.DIFF_2_AGGREGATE_IND,
      v2.id_group_ind AS diff_2_id_group_ind,
      s.diff_3,
      v3.description AS DIFF_3_DESC,
      s.DIFF_3_AGGREGATE_IND,
      v3.id_group_ind AS diff_3_id_group_ind,
      s.diff_4,
      v4.description AS DIFF_4_DESC,
      s.DIFF_4_AGGREGATE_IND,
      v4.id_group_ind AS diff_4_id_group_ind,
      NULL            AS DIFF_GROUPS_EDITABLE
    FROM step1 s,
      v_diff_id_group_type v1,
      v_diff_id_group_type v2,
      v_diff_id_group_type v3,
      v_diff_id_group_type v4
    WHERE s.diff_1 = v1.id_group (+)
    AND s.diff_2   = v2.id_group (+)
    AND s.diff_3   = v3.id_group (+)
    AND s.diff_4   = v4.id_group (+)
    )
  SELECT ITEM,
    ITEM_PARENT,
    ITEM_PARENT_DESC,
    VPN,
    ITEM_NUMBER_TYPE,
    DIFF_1,
    DIFF_1_DESC,
    DIFF_1_AGGREGATE_IND,
    DIFF_1_ID_GROUP_IND,
    DIFF_2,
    DIFF_2_DESC,
    DIFF_2_AGGREGATE_IND,
    DIFF_2_ID_GROUP_IND,
    DIFF_3,
    DIFF_3_DESC,
    DIFF_3_AGGREGATE_IND,
    DIFF_3_ID_GROUP_IND,
    DIFF_4,
    DIFF_4_DESC,
    DIFF_4_AGGREGATE_IND,
    DIFF_4_ID_GROUP_IND,
    DIFF_GROUPS_EDITABLE
  FROM step2;
  OPEN C_diff_editable_ind;
  FETCH C_diff_editable_ind INTO L_diff_editable_ind;
  CLOSE C_diff_editable_ind;
  UPDATE DIFF_FNLZ_HEAD_GTT SET DIFF_GROUPS_EDITABLE = L_diff_editable_ind;
  IF POPULATE_DIFF_1(O_error_message,
                     I_ITEM ,
                     I_parent_item ,
                     I_VPN)=false THEN
     RETURN false;
  END IF;
  IF POPULATE_DIFF_2(O_error_message,
                     I_ITEM ,
                     I_parent_item ,
                     I_VPN)=false THEN
     RETURN false;
  END IF;
  IF POPULATE_DIFF_3(O_error_message,
                     I_ITEM ,
                     I_parent_item ,
                     I_VPN)=false THEN
     RETURN false;
  END IF;
  IF POPULATE_DIFF_4(O_error_message,
                     I_ITEM ,
                     I_parent_item ,
                     I_VPN)=false THEN
     RETURN false;
  END IF;
  --Call MAP_DIFFS for each diff seq (1,2,3 and 4) to map the user_diff_vals to RMS Diffs.
  FOR i IN 1..4
  LOOP
    IF MAP_DIFFS(O_error_message,i)=false THEN
      RETURN false;
    END IF;
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END INITIALIZE;
--------------------------------------------------------------------------------
FUNCTION MAP_DIFFS(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_DIFF_SEQ      IN     NUMBER)
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.MAP_DIFFS';
  l_id_group_ind v_diff_id_group_type.id_group_ind%type;
  l_diff_id diff_ids.diff_id%type;
  l_head_row diff_fnlz_head_gtt%rowtype;
  CURSOR c_get_head_row
  IS
    SELECT * FROM diff_fnlz_head_gtt;
BEGIN
  -- get the header row
  OPEN c_get_head_row;
  FETCH c_get_head_row
  INTO l_head_row;
  CLOSE c_get_head_row;
  -- Copy appropriate diff values to local variables
  -- depending upon the diff_seq
  CASE I_DIFF_SEQ
  WHEN 1 THEN
    l_diff_id      := l_head_row.diff_1;
    l_id_group_ind := l_head_row.diff_1_id_group_ind;
  WHEN 2 THEN
    l_diff_id      := l_head_row.diff_2;
    l_id_group_ind := l_head_row.diff_2_id_group_ind;
  WHEN 3 THEN
    l_diff_id      := l_head_row.diff_3;
    l_id_group_ind := l_head_row.diff_3_id_group_ind;
  WHEN 4 THEN
    l_diff_id      := l_head_row.diff_4;
    l_id_group_ind := l_head_row.diff_4_id_group_ind;
  ELSE
    O_error_message := 'DIFF_INVALID_DIFF_SEQ';
    RETURN false;
  END CASE;
  --Using the local variables
  -- execute the merge i.e. map the diffs
  merge INTO diff_fnlz_detail_gtt g1 USING
  ( WITH diffs AS
  (SELECT dd.diff_id,
    di.diff_desc
  FROM diff_group_detail dd,
    v_diff_group_head vh,
    diff_ids di
  WHERE vh.diff_group_id = l_diff_id
  AND l_id_group_ind     = 'GROUP'
  AND vh.diff_group_id   = dd.diff_group_id
  AND dd.diff_id         = di.diff_id
  UNION ALL
  SELECT diff_id,
    diff_desc
  FROM diff_ids
  WHERE l_id_group_ind = 'ID'
  AND diff_id          = l_diff_id
  )
SELECT gtt.rowid AS rid,
  diffs.diff_id,
  diffs.diff_desc
FROM diffs,
  diff_fnlz_detail_gtt gtt
WHERE gtt.diff_seq    = I_diff_seq
AND gtt.user_diff_val = diffs.diff_id (+)
  ) sq ON (g1.rowid   = sq.rid)
WHEN matched THEN
  --
  UPDATE SET g1.diff_id = sq.diff_id ,g1.diff_desc = sq.diff_desc;
  --
  DELETE FROM diff_fnlz_detail_gtt WHERE DIFF_ID IS NULL AND DIFF_DESC IS NULL AND USER_DIFF_VAL IS NULL ; 
  --
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END MAP_DIFFS;
--------------------------------------------------------------------------------
FUNCTION POPULATE_DIFF_1(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.POPULATE_DIFF_1';
BEGIN
  DELETE FROM DIFF_FNLZ_DETAIL_GTT WHERE diff_seq = 1;
  INSERT INTO DIFF_FNLZ_DETAIL_GTT
    (diff_seq,user_diff_val,diff_id,diff_desc
    )
  SELECT diff_seq,
    user_diff_val,
    diff_id,
    diff_desc
  FROM 
    (SELECT 1 AS diff_seq,
      diff_1  AS user_diff_val,
      NULL    AS diff_id,
      NULL    AS diff_desc
    FROM svc_item_master sim
    WHERE I_parent_item IS NOT NULL
    AND sim.item_parent  = I_parent_item
    AND NVL(DIFF_FINALIZED_IND,'N') = 'N'
    AND item_level = 2
    AND diff_1    IS NOT NULL
    UNION
    SELECT 1 AS diff_seq,
      diff_1 AS user_diff_val,
      NULL   AS diff_id,
      NULL   AS diff_desc
    FROM svc_item_master sim,
      svc_item_supplier sis
    WHERE I_parent_item IS NULL
    AND I_VPN           IS NOT NULL
    AND sim.item         = sis.item
    AND sis.vpn          = I_VPN
    AND NVL(DIFF_FINALIZED_IND,'N') = 'N'
    AND item_level = 2
    AND diff_1    IS NOT NULL
    );
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END POPULATE_DIFF_1;
-------------------------------------------------------------------------------
FUNCTION POPULATE_DIFF_2(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.POPULATE_DIFF_2';
BEGIN
  DELETE FROM DIFF_FNLZ_DETAIL_GTT WHERE diff_seq = 2;
  INSERT INTO DIFF_FNLZ_DETAIL_GTT
    (diff_seq,user_diff_val,diff_id,diff_desc
    )
  SELECT diff_seq,
    user_diff_val,
    diff_id,
    diff_desc
  FROM 
    (SELECT 2 AS diff_seq,
      diff_2  AS user_diff_val,
      NULL    AS diff_id,
      NULL    AS diff_desc
    FROM svc_item_master sim
    WHERE I_parent_item IS NOT NULL
    AND sim.item_parent  = I_parent_item
    AND NVL(DIFF_FINALIZED_IND,'N') = 'N'
    AND item_level = 2
    AND diff_2    IS NOT NULL
    UNION
    SELECT 2 AS diff_seq,
      diff_2 AS user_diff_val,
      NULL   AS diff_id,
      NULL   AS diff_desc
    FROM svc_item_master sim,
      svc_item_supplier sis
    WHERE I_parent_item IS NULL
    AND I_VPN           IS NOT NULL
    AND sim.item         = sis.item
    AND sis.vpn          = I_VPN
    AND NVL(DIFF_FINALIZED_IND,'N') = 'N'
    AND item_level = 2
    AND diff_2    IS NOT NULL
    );
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END POPULATE_DIFF_2;
-------------------------------------------------------------------------------
FUNCTION POPULATE_DIFF_3(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.POPULATE_DIFF_3';
BEGIN
  DELETE FROM DIFF_FNLZ_DETAIL_GTT WHERE diff_seq = 3;
  INSERT INTO DIFF_FNLZ_DETAIL_GTT
    (diff_seq,user_diff_val,diff_id,diff_desc
    )
  SELECT diff_seq,
    user_diff_val,
    diff_id,
    diff_desc
  FROM 
    (SELECT 3 AS diff_seq,
      diff_3  AS user_diff_val,
      NULL    AS diff_id,
      NULL    AS diff_desc
    FROM svc_item_master sim
    WHERE I_parent_item IS NOT NULL
    AND sim.item_parent  = I_parent_item
    AND NVL(DIFF_FINALIZED_IND,'N') = 'N'
    AND item_level = 2
    AND diff_3    IS NOT NULL
    UNION
    SELECT 3 AS diff_seq,
      diff_3 AS user_diff_val,
      NULL   AS diff_id,
      NULL   AS diff_desc
    FROM svc_item_master sim,
      svc_item_supplier sis
    WHERE I_parent_item IS NULL
    AND I_VPN           IS NOT NULL
    AND sim.item         = sis.item
    AND sis.vpn          = I_VPN
    AND NVL(DIFF_FINALIZED_IND,'N') = 'N'
    AND item_level = 2
    AND diff_3    IS NOT NULL
    );
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END POPULATE_DIFF_3;
-------------------------------------------------------------------------------
FUNCTION POPULATE_DIFF_4(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_ITEM          IN     ITEM_MASTER.ITEM%TYPE ,
    I_parent_item   IN     ITEM_MASTER.ITEM%TYPE ,
    I_VPN           IN     ITEM_SUPPLIER.VPN%TYPE )
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.POPULATE_DIFF_4';
BEGIN
  DELETE FROM DIFF_FNLZ_DETAIL_GTT WHERE diff_seq = 4;
  INSERT INTO DIFF_FNLZ_DETAIL_GTT
    (diff_seq,user_diff_val,diff_id,diff_desc
    )
  SELECT diff_seq,
    user_diff_val,
    diff_id,
    diff_desc
  FROM 
    (SELECT 4 AS diff_seq,
      diff_4  AS user_diff_val,
      NULL    AS diff_id,
      NULL    AS diff_desc
    FROM svc_item_master sim
    WHERE I_parent_item IS NOT NULL
    AND sim.item_parent  = I_parent_item
    AND NVL(DIFF_FINALIZED_IND,'N') = 'N'
    AND item_level = 2
    AND diff_1    IS NOT NULL
    UNION
    SELECT 4 AS diff_seq,
      diff_4 AS user_diff_val,
      NULL   AS diff_id,
      NULL   AS diff_desc
    FROM svc_item_master sim,
      svc_item_supplier sis
    WHERE I_parent_item IS NULL
    AND I_VPN           IS NOT NULL
    AND sim.item         = sis.item
    AND sis.vpn          = I_VPN
    AND NVL(DIFF_FINALIZED_IND,'N') = 'N'
    AND item_level = 2
    AND diff_4    IS NOT NULL
    );
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END POPULATE_DIFF_4;
-------------------------------------------------------------------------------
FUNCTION FINALIZE(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.FINALIZE';

  CURSOR c_check_unmapped_diffs
  IS
    SELECT 'Y' FROM diff_fnlz_detail_gtt WHERE diff_id IS NULL;
  L_unmapped_diffs_ind VARCHAR2(1):='N';
  CURSOR c_get_max_row_seq
    -- Get the max row seq so that new rows can be inserted in svc_item_master.
  IS
    SELECT MAX(row_seq)
    FROM svc_item_master
    WHERE process_id =
      (SELECT process_id
      FROM svc_item_master s1,
        diff_fnlz_head_gtt gtt
      WHERE s1.item = gtt.item
      );
  L_max_row_seq svc_item_master.row_seq%type;
BEGIN
  OPEN c_check_unmapped_diffs;
  FETCH c_check_unmapped_diffs INTO L_unmapped_diffs_ind;
  CLOSE c_check_unmapped_diffs;
  IF L_unmapped_diffs_ind = 'Y' THEN
    O_error_message      := 'DIFF_UNMAPPED_DIFFS';
    RETURN false;
  END IF;
  OPEN c_get_max_row_seq;
  FETCH c_get_max_row_seq INTO L_max_row_seq;
  CLOSE c_get_max_row_seq;
  L_max_row_seq := NVL(L_max_row_seq,0);

  /*If parent item is not present in staging then insert record with values from child item that was selected by the user.*/
  merge INTO svc_item_master sim_dest USING
  (SELECT sim.*,
    gtt.item_parent          AS gtt_item_parent,
    gtt.diff_1               AS gtt_diff_1,
    gtt.diff_1_aggregate_ind AS gtt_diff_1_aggregate_ind,
    gtt.diff_2               AS gtt_diff_2,
    gtt.diff_2_aggregate_ind AS gtt_diff_2_aggregate_ind,
    gtt.diff_3               AS gtt_diff_3,
    gtt.diff_3_aggregate_ind AS gtt_diff_3_aggregate_ind,
    gtt.diff_4               AS gtt_diff_4,
    gtt.diff_4_aggregate_ind AS gtt_diff_4_aggregate_ind,
    rownum                   AS row_num,
    (select item from item_master im where im.item = gtt.item_parent) AS im_item_parent
  FROM diff_fnlz_head_gtt gtt,
    svc_item_master sim
  WHERE sim.item         = gtt.item
  ) sq ON (sim_dest.item = sq.gtt_item_parent)
WHEN matched THEN
  UPDATE
  SET diff_1             = gtt_diff_1 ,
    diff_1_aggregate_ind = gtt_diff_1_aggregate_ind ,
    diff_2               = gtt_diff_2 ,
    diff_2_aggregate_ind = gtt_diff_2_aggregate_ind ,
    diff_3               = gtt_diff_3 ,
    diff_3_aggregate_ind = gtt_diff_3_aggregate_ind,
    diff_finalized_ind   = 'Y',
    last_upd_id          = GET_USER,
    last_upd_datetime    = SYSDATE
WHEN NOT matched THEN
  INSERT
    (
      PROCESS_ID,
      CHUNK_ID,
      ROW_SEQ,
      ACTION,
      PROCESS$STATUS,
      ITEM,
      ITEM_NUMBER_TYPE,
      FORMAT_ID,
      PREFIX,
      ITEM_PARENT,
      ITEM_GRANDPARENT,
      PACK_IND,
      ITEM_LEVEL,
      TRAN_LEVEL,
      ITEM_AGGREGATE_IND,
      DIFF_1,
      DIFF_1_AGGREGATE_IND,
      DIFF_2,
      DIFF_2_AGGREGATE_IND,
      DIFF_3,
      DIFF_3_AGGREGATE_IND,
      DIFF_4,
      DIFF_4_AGGREGATE_IND,
      DEPT,
      CLASS,
      SUBCLASS,
      STATUS,
      ITEM_DESC,
      ITEM_DESC_SECONDARY,
      SHORT_DESC,
      PRIMARY_REF_ITEM_IND,
      COST_ZONE_GROUP_ID,
      STANDARD_UOM,
      UOM_CONV_FACTOR,
      PACKAGE_SIZE,
      PACKAGE_UOM,
      MERCHANDISE_IND,
      STORE_ORD_MULT,
      FORECAST_IND,
      ORIGINAL_RETAIL,
      MFG_REC_RETAIL,
      RETAIL_LABEL_TYPE,
      RETAIL_LABEL_VALUE,
      HANDLING_TEMP,
      HANDLING_SENSITIVITY,
      CATCH_WEIGHT_IND,
      WASTE_TYPE,
      WASTE_PCT,
      DEFAULT_WASTE_PCT,
      CONST_DIMEN_IND,
      SIMPLE_PACK_IND,
      CONTAINS_INNER_IND,
      SELLABLE_IND,
      ORDERABLE_IND,
      PACK_TYPE,
      ORDER_AS_TYPE,
      COMMENTS,
      ITEM_SERVICE_LEVEL,
      GIFT_WRAP_IND,
      SHIP_ALONE_IND,
      ITEM_XFORM_IND,
      INVENTORY_IND,
      ORDER_TYPE,
      SALE_TYPE,
      DEPOSIT_ITEM_TYPE,
      CONTAINER_ITEM,
      DEPOSIT_IN_PRICE_PER_UOM,
      AIP_CASE_TYPE,
      PERISHABLE_IND,
      NOTIONAL_PACK_IND,
      SOH_INQUIRY_AT_PACK_IND,
      PRODUCT_CLASSIFICATION,
      BRAND_NAME,
      CREATE_ID,
      CREATE_DATETIME,
      LAST_UPD_ID,
      NEXT_UPD_ID,
      LAST_UPD_DATETIME,
      ORIG_REF_NO,
      PRE_RESERVED_IND,
      DIFF_FINALIZED_IND
    )
    VALUES
    (
      sq.PROCESS_ID,
      sq.CHUNK_ID,
      L_max_row_seq+sq.row_num,
      DECODE(im_item_parent, NULL,'NEW','MOD'),
      /*ACTION*/
      'N',
      sq.GTT_ITEM_PARENT,
      sq.ITEM_NUMBER_TYPE,
      sq.FORMAT_ID,
      sq.PREFIX,
      NULL
      /*ITEM_PARENT*/
      ,
      NULL
      /*ITEM_GRANDPARENT*/
      ,
      sq.PACK_IND,
      1
      /*ITEM_LEVEL*/
      ,
      sq.TRAN_LEVEL,
      sq.item_aggregate_ind
      /*ITEM_AGGREGATE_IND*/
      ,
      sq.gtt_DIFF_1,
      sq.gtt_DIFF_1_AGGREGATE_IND,
      sq.gtt_DIFF_2,
      sq.gtt_DIFF_2_AGGREGATE_IND,
      sq.gtt_DIFF_3,
      sq.gtt_DIFF_3_AGGREGATE_IND,
      sq.gtt_DIFF_4,
      sq.gtt_DIFF_4_AGGREGATE_IND,
      sq.DEPT,
      sq.CLASS,
      sq.SUBCLASS,
      sq.STATUS,
      sq.ITEM_DESC,
      sq.ITEM_DESC_SECONDARY,
      sq.SHORT_DESC,
      sq.PRIMARY_REF_ITEM_IND,
      sq.COST_ZONE_GROUP_ID,
      sq.STANDARD_UOM,
      sq.UOM_CONV_FACTOR,
      sq.PACKAGE_SIZE,
      sq.PACKAGE_UOM,
      sq.MERCHANDISE_IND,
      sq.STORE_ORD_MULT,
      sq.FORECAST_IND,
      sq.ORIGINAL_RETAIL,
      sq.MFG_REC_RETAIL,
      sq.RETAIL_LABEL_TYPE,
      sq.RETAIL_LABEL_VALUE,
      sq.HANDLING_TEMP,
      sq.HANDLING_SENSITIVITY,
      sq.CATCH_WEIGHT_IND,
      sq.WASTE_TYPE,
      sq.WASTE_PCT,
      sq.DEFAULT_WASTE_PCT,
      sq.CONST_DIMEN_IND,
      sq.SIMPLE_PACK_IND,
      sq.CONTAINS_INNER_IND,
      sq.SELLABLE_IND,
      sq.ORDERABLE_IND,
      sq.PACK_TYPE,
      sq.ORDER_AS_TYPE,
      sq.COMMENTS,
      sq.ITEM_SERVICE_LEVEL,
      sq.GIFT_WRAP_IND,
      sq.SHIP_ALONE_IND,
      sq.ITEM_XFORM_IND,
      sq.INVENTORY_IND,
      sq.ORDER_TYPE,
      sq.SALE_TYPE,
      sq.DEPOSIT_ITEM_TYPE,
      sq.CONTAINER_ITEM,
      sq.DEPOSIT_IN_PRICE_PER_UOM,
      sq.AIP_CASE_TYPE,
      sq.PERISHABLE_IND,
      sq.NOTIONAL_PACK_IND,
      sq.SOH_INQUIRY_AT_PACK_IND,
      sq.PRODUCT_CLASSIFICATION,
      sq.BRAND_NAME,
      sq.CREATE_ID,
      sq.CREATE_DATETIME,
      sq.LAST_UPD_ID,
      sq.NEXT_UPD_ID,
      sq.LAST_UPD_DATETIME,
      sq.item
      /*ORIG_REF_NO insert same as item so that item induction generates a new item*/
      ,
      sq.PRE_RESERVED_IND,
      'Y');
  IF sql%rowcount  > 0 THEN
    L_max_row_seq := L_max_row_seq+1;
  END IF;
  DECLARE
    CURSOR c_diffs
    IS
    WITH sim_ref AS
      --This subquery gets the item attributes of the item that
      --user had selected. These attributes will be used to insert
      --New rows in SVC_ITEM_MASTER as part of diff finalization
      (
      SELECT sim.*
      FROM svc_item_master sim,
        diff_fnlz_head_gtt gtt
      WHERE sim.item = gtt.item_parent
      ),
    diff1 AS
    (SELECT user_diff_val,
      diff_id,
      diff_desc,
      1 seq
    FROM diff_fnlz_detail_gtt
    WHERE diff_seq=1
    ) ,
    diff2 AS
    (SELECT user_diff_val,
      diff_id,
      diff_desc,
      1 seq
    FROM diff_fnlz_detail_gtt
    WHERE diff_seq=2
    ) ,
    diff3 AS
    (SELECT user_diff_val,
      diff_id,
      diff_desc,
      1 seq
    FROM diff_fnlz_detail_gtt
    WHERE diff_seq=3
    ) ,
    diff4 AS
    (SELECT user_diff_val,
      diff_id,
      diff_desc,
      1 seq
    FROM diff_fnlz_detail_gtt
    WHERE diff_seq=4
    ),
    diffs AS
    (SELECT NVL(diff1.user_diff_val,-1) AS user_diff1,
      diff1.diff_id                     AS diff_1 ,
      diff1.diff_desc                   AS diff_desc_1,
      NVL(diff2.user_diff_val,-1)       AS user_diff2,
      diff2.diff_id                     AS diff_2 ,
      diff2.diff_desc                   AS diff_desc_2,
      NVL(diff3.user_diff_val,-1)       AS user_diff3,
      diff3.diff_id                     AS diff_3 ,
      diff3.diff_desc                   AS diff_desc_3,
      NVL(diff4.user_diff_val,-1)       AS user_diff4,
      diff4.diff_id                     AS diff_4,
      diff4.diff_desc                   AS diff_desc_4
    FROM diff1
    FULL OUTER JOIN diff2
    ON diff2.seq = diff1.seq
    FULL OUTER JOIN diff3
    ON diff3.seq = COALESCE(diff1.seq, diff2.seq)
    FULL OUTER JOIN diff4
    ON diff4.seq = COALESCE(diff1.seq, diff3.seq)
    )
  SELECT rownum          AS row_num,
    gtt.item_number_type AS new_item_number_type,
    gtt.item_parent      AS gtt_item_parent,
    d.user_diff1,
    d.user_diff2,
    d.user_diff3,
    d.user_diff4,
    d.diff_1 AS diff_1_finalized,
    d.diff_2 AS diff_2_finalized,
    d.diff_3 AS diff_3_finalized,
    d.diff_4 AS diff_4_finalized,
    d.diff_desc_1,
    d.diff_desc_2,
    d.diff_desc_3,
    d.diff_desc_4,
    sim_ref.*
  FROM diffs d,
    diff_fnlz_head_gtt gtt,
    sim_ref;
  CURSOR c_child_items(I_udiff1 VARCHAR2,I_udiff2 VARCHAR2,I_udiff3 VARCHAR2,I_udiff4 VARCHAR2)
  IS
  WITH child_items AS
    --This subquery gets all child items which are
    --candidate to be updated
    (
    SELECT sim.rowid       AS sim_rid,
      NVL(sim.diff_1,'-1') AS diff_1,
      NVL(sim.diff_2,'-1') AS diff_2,
      NVL(sim.diff_3,'-1') AS diff_3,
      NVL(sim.diff_4,'-1') AS diff_4
    FROM svc_item_master sim,
      diff_fnlz_head_gtt gtt
    WHERE sim.item_parent = gtt.item_parent
    AND(sim.diff_1       IS NOT NULL
    OR sim.diff_2        IS NOT NULL
    OR sim.diff_3        IS NOT NULL
    OR sim.diff_4        IS NOT NULL)
    OR EXISTS
      (SELECT 1
      FROM svc_item_supplier sis
      WHERE sis.item = sim.item
      AND sis.vpn    = gtt.vpn
      )
    )
  SELECT *
  FROM child_items i
  WHERE i.diff_1 = I_udiff1
  AND i.diff_2   = I_udiff2
  AND i.diff_3   = I_udiff3
  AND i.diff_4   = I_udiff4;
  CURSOR c_diff_length
  IS
  SELECT CASE WHEN diff_4 is not null THEN
                4
             WHEN diff_3 is not null THEN
                3
             WHEN diff_2 is not null THEN
                2
             WHEN diff_1 is not null THEN
                1
             ELSE
               0
             END  diff_num,
        item_parent_desc     
  FROM diff_fnlz_head_gtt ;

  l_child_items_rec c_child_items%rowtype;
  L_diff_no               NUMBER;
  L_item_desc             SVC_ITEM_MASTER.ITEM_DESC%TYPE;  
  L_parent_desc_length    NUMBER; 
  L_diff_desc_length      NUMBER;
BEGIN
  OPEN c_diff_length;
  FETCH c_diff_length INTO L_diff_no,L_item_desc;
  GET_DESC_LENGTH(O_error_message,
                   L_parent_desc_length,
                   L_diff_desc_length,
                   L_item_desc,
                   L_diff_no);
   if O_error_message is not NULL then
      CLOSE c_diff_length;
      RETURN FALSE;
   end if;
  CLOSE c_diff_length; 
  FOR sq IN c_diffs
  LOOP
    OPEN c_child_items(sq.user_diff1,sq.user_diff2,sq.user_diff3,sq.user_diff4);
    FETCH c_child_items INTO l_child_items_rec;
    IF c_child_items%found THEN
      UPDATE svc_item_master sim1
      SET diff_1           = sq.diff_1_finalized ,
        diff_2             = sq.diff_2_finalized ,
        diff_3             = sq.diff_3_finalized ,
        diff_4             = sq.diff_4_finalized ,
        last_upd_id        = GET_USER ,
        last_upd_datetime  = SYSDATE ,
        diff_finalized_ind = 'Y'
      WHERE sim1.rowid     = l_child_items_rec.sim_rid;
    ELSE
      INSERT
      INTO svc_item_master
        (
          PROCESS_ID,
          CHUNK_ID,
          ROW_SEQ,
          ACTION,
          PROCESS$STATUS,
          ITEM,
          ITEM_NUMBER_TYPE,
          FORMAT_ID,
          PREFIX,
          ITEM_PARENT,
          ITEM_GRANDPARENT,
          PACK_IND,
          ITEM_LEVEL,
          TRAN_LEVEL,
          ITEM_AGGREGATE_IND,
          DIFF_1,
          DIFF_1_AGGREGATE_IND,
          DIFF_2,
          DIFF_2_AGGREGATE_IND,
          DIFF_3,
          DIFF_3_AGGREGATE_IND,
          DIFF_4,
          DIFF_4_AGGREGATE_IND,
          DEPT,
          CLASS,
          SUBCLASS,
          STATUS,
          ITEM_DESC,
          ITEM_DESC_SECONDARY,
          SHORT_DESC,
          PRIMARY_REF_ITEM_IND,
          COST_ZONE_GROUP_ID,
          STANDARD_UOM,
          UOM_CONV_FACTOR,
          PACKAGE_SIZE,
          PACKAGE_UOM,
          MERCHANDISE_IND,
          STORE_ORD_MULT,
          FORECAST_IND,
          ORIGINAL_RETAIL,
          MFG_REC_RETAIL,
          RETAIL_LABEL_TYPE,
          RETAIL_LABEL_VALUE,
          HANDLING_TEMP,
          HANDLING_SENSITIVITY,
          CATCH_WEIGHT_IND,
          WASTE_TYPE,
          WASTE_PCT,
          DEFAULT_WASTE_PCT,
          CONST_DIMEN_IND,
          SIMPLE_PACK_IND,
          CONTAINS_INNER_IND,
          SELLABLE_IND,
          ORDERABLE_IND,
          PACK_TYPE,
          ORDER_AS_TYPE,
          COMMENTS,
          ITEM_SERVICE_LEVEL,
          GIFT_WRAP_IND,
          SHIP_ALONE_IND,
          ITEM_XFORM_IND,
          INVENTORY_IND,
          ORDER_TYPE,
          SALE_TYPE,
          DEPOSIT_ITEM_TYPE,
          CONTAINER_ITEM,
          DEPOSIT_IN_PRICE_PER_UOM,
          AIP_CASE_TYPE,
          PERISHABLE_IND,
          NOTIONAL_PACK_IND,
          SOH_INQUIRY_AT_PACK_IND,
          PRODUCT_CLASSIFICATION,
          BRAND_NAME,
          CREATE_ID,
          CREATE_DATETIME,
          LAST_UPD_ID,
          NEXT_UPD_ID,
          LAST_UPD_DATETIME,
          ORIG_REF_NO,
          PRE_RESERVED_IND,
          DIFF_FINALIZED_IND
        )
        VALUES
        (
          sq.PROCESS_ID,
          sq.CHUNK_ID,
          L_max_row_seq + sq.row_num,
          'NEW',
          'N',
          diff_fnlz_item_number_seq.nextval,
          sq.NEW_ITEM_NUMBER_TYPE,
          sq.FORMAT_ID,
          sq.PREFIX,
          sq.GTT_ITEM_PARENT,
          NULL
          /*ITEM_GRANDPARENT*/
          ,
          sq.PACK_IND,
          2
          /*ITEM_LEVEL*/
          ,--The item level will always be 2 because through diff apply it is only possible to create level 2 items
          sq.TRAN_LEVEL,
          NULL
          /*ITEM_AGGREGATE_IND*/
          ,
          sq.DIFF_1_finalized,
          NULL
          /*DIFF_1_AGGREGATE_IND*/
          ,
          sq.DIFF_2_finalized,
          NULL
          /*DIFF_2_AGGREGATE_IND*/
          ,
          sq.DIFF_3_finalized,
          NULL
          /*DIFF_3_AGGREGATE_IND*/
          ,
          sq.DIFF_4_finalized,
          NULL
          /*DIFF_4_AGGREGATE_IND*/
          ,
          sq.DEPT,
          sq.CLASS,
          sq.SUBCLASS,
          sq.STATUS,
          CASE WHEN L_diff_no=4 THEN 
                  rtrim(substrb(sq.item_desc,1,L_parent_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_1,1,L_diff_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_2,1,L_diff_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_3,1,L_diff_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_4,1,L_diff_desc_length))
         WHEN L_diff_no=3 THEN 
                  rtrim(substrb(sq.item_desc,1,L_parent_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_1,1,L_diff_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_2,1,L_diff_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_3,1,L_diff_desc_length))
         WHEN L_diff_no=2 THEN 
                  rtrim(substrb(sq.item_desc,1,L_parent_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_1,1,L_diff_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_2,1,L_diff_desc_length))
         WHEN L_diff_no=1 THEN 
                  rtrim(substrb(sq.item_desc,1,L_parent_desc_length))
                  ||':'
                  ||rtrim(substrb(sq.diff_desc_1,1,L_diff_desc_length))
          ELSE sq.item_desc
          END,
          sq.ITEM_DESC_SECONDARY,
          sq.SHORT_DESC,
          sq.PRIMARY_REF_ITEM_IND,
          sq.COST_ZONE_GROUP_ID,
          sq.STANDARD_UOM,
          sq.UOM_CONV_FACTOR,
          sq.PACKAGE_SIZE,
          sq.PACKAGE_UOM,
          sq.MERCHANDISE_IND,
          sq.STORE_ORD_MULT,
          sq.FORECAST_IND,
          sq.ORIGINAL_RETAIL,
          sq.MFG_REC_RETAIL,
          sq.RETAIL_LABEL_TYPE,
          sq.RETAIL_LABEL_VALUE,
          sq.HANDLING_TEMP,
          sq.HANDLING_SENSITIVITY,
          sq.CATCH_WEIGHT_IND,
          sq.WASTE_TYPE,
          sq.WASTE_PCT,
          sq.DEFAULT_WASTE_PCT,
          sq.CONST_DIMEN_IND,
          sq.SIMPLE_PACK_IND,
          sq.CONTAINS_INNER_IND,
          sq.SELLABLE_IND,
          sq.ORDERABLE_IND,
          sq.PACK_TYPE,
          sq.ORDER_AS_TYPE,
          sq.COMMENTS,
          sq.ITEM_SERVICE_LEVEL,
          sq.GIFT_WRAP_IND,
          sq.SHIP_ALONE_IND,
          sq.ITEM_XFORM_IND,
          sq.INVENTORY_IND,
          sq.ORDER_TYPE,
          sq.SALE_TYPE,
          sq.DEPOSIT_ITEM_TYPE,
          sq.CONTAINER_ITEM,
          sq.DEPOSIT_IN_PRICE_PER_UOM,
          sq.AIP_CASE_TYPE,
          sq.PERISHABLE_IND,
          sq.NOTIONAL_PACK_IND,
          sq.SOH_INQUIRY_AT_PACK_IND,
          sq.PRODUCT_CLASSIFICATION,
          sq.BRAND_NAME,
          GET_USER
          /*CREATE_ID*/
          ,
          sysdate
          /*CREATE_DATETIME*/
          ,
          GET_USER
          /*LAST_UPD_ID*/
          ,
          sq.NEXT_UPD_ID,
          sysdate,
          /*LAST_UPD_DATETIME*/
          diff_fnlz_item_number_seq.currval,
          /*ORIG_REF_NO*/
          -- Insert same value as item orig_ref_no so that item induction generates new item for this
          sq.PRE_RESERVED_IND,
          'Y'
          --DIFF_FINALIZED_IND
        );
    END IF;
    CLOSE c_child_items;
  END LOOP;
END;
  --Delete all child rows for the item_parent or VPN that still have finalized flag as N.
  DELETE
  FROM svc_item_master sim
  WHERE diff_finalized_ind = 'N'
    AND (sim.item_parent =
    (SELECT item_parent FROM diff_fnlz_head_gtt gtt
    )
  OR EXISTS
    (SELECT 1
    FROM svc_item_supplier sis
    WHERE sis.item = sim.item
    AND sis.vpn    =
      (SELECT VPN FROM diff_fnlz_head_gtt gtt
      )
    ));
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END FINALIZE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF_GROUP(
    O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_DIFF_GROUP_DESC IN OUT DIFF_IDS.DIFF_DESC%TYPE,
    I_DIFF            IN     DIFF_IDS.DIFF_ID%TYPE)
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.VALIDATE_DIFF_GROUP';
  CURSOR c_diff_group
  IS
    SELECT diff_group_desc,
      'Y'
    FROM v_diff_group_head
    WHERE diff_group_id     = I_DIFF;
  L_diff_valid VARCHAR2(1) := 'N';
BEGIN
  OPEN c_diff_group;
  FETCH c_diff_group INTO O_DIFF_GROUP_DESC,L_diff_valid;
  CLOSE c_diff_group;
  IF L_diff_valid    = 'N' THEN
    O_error_message := 'DIFF_INVALID_DIFF_GROUP';
    RETURN false;
  END IF;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END VALIDATE_DIFF_GROUP;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF_ID(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_DIFF_DESC     IN OUT DIFF_IDS.DIFF_DESC%TYPE,
    I_DIFF_SEQ      IN     NUMBER,
    I_DIFF_GROUP    IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
    I_DIFF_ID       IN     DIFF_IDS.DIFF_ID%TYPE )
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.VALIDATE_DIFF_ID';
  CURSOR c_check_diff
  IS
    SELECT di.diff_desc,
      'Y'
    FROM diff_group_detail dgd,
      diff_ids di
    WHERE dgd.diff_group_id = I_DIFF_GROUP
    AND dgd.diff_id         = di.diff_id
    AND di.diff_id = I_DIFF_ID;
  CURSOR c_dup_diff_id
  IS
    SELECT 'Y'
    FROM diff_fnlz_detail_gtt gtt
    WHERE gtt.diff_seq = I_DIFF_SEQ
    AND gtt.diff_id = I_DIFF_ID;
  L_valid_diff VARCHAR2(1):='N';
  L_dup_diff   VARCHAR2(1):='N';
BEGIN
  --
  OPEN c_dup_diff_id;
  FETCH c_dup_diff_id INTO L_dup_diff;
  CLOSE c_dup_diff_id;
  IF L_dup_diff    = 'Y' THEN
    O_error_message := 'DIFF_DUP_DIFF_ID';
    RETURN false;
  END IF;
  --
  OPEN c_check_diff;
  FETCH c_check_diff INTO O_DIFF_DESC,L_valid_diff;
  CLOSE c_check_diff;
  IF L_valid_diff    = 'N' THEN
    O_error_message := 'DIFF_INVALID_DIFF_ID';
    RETURN false;
  END IF;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END VALIDATE_DIFF_ID;
--------------------------------------------------------------------------------
FUNCTION UPDATE_DIFF_GROUP(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
    I_DIFF_GROUP    IN     DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
    I_DIFF_DESC     IN     DIFF_IDS.DIFF_DESC%TYPE, 
    I_DIFF_SEQ      IN     NUMBER)
  RETURN BOOLEAN
IS
  L_program VARCHAR2( 64) := 'DIFF_FINALIZATION.UPDATE_DIFF_GROUP';
BEGIN
  UPDATE diff_fnlz_head_gtt
     SET DIFF_1 = DECODE(I_DIFF_SEQ,1,I_DIFF_GROUP,DIFF_1),
        DIFF_1_DESC = DECODE(I_DIFF_SEQ,1,I_DIFF_DESC,DIFF_1_DESC),
        DIFF_2 = DECODE(I_DIFF_SEQ,2,I_DIFF_GROUP,DIFF_2),
        DIFF_2_DESC = DECODE(I_DIFF_SEQ,2,I_DIFF_DESC,DIFF_2_DESC),
        DIFF_3 = DECODE(I_DIFF_SEQ,3,I_DIFF_GROUP,DIFF_3),
        DIFF_3_DESC = DECODE(I_DIFF_SEQ,3,I_DIFF_DESC,DIFF_3_DESC),
        DIFF_4 = DECODE(I_DIFF_SEQ,4,I_DIFF_GROUP,DIFF_4),
        DIFF_4_DESC = DECODE(I_DIFF_SEQ,4,I_DIFF_DESC,DIFF_4_DESC);
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END UPDATE_DIFF_GROUP;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--    Name: GET_DESC_LENGTH
-- Purpose: Calculate the number of characters that will be included from the parent desc
--          and any diff's that are associated with the parent.
--------------------------------------------------------------------------------
/***** Local Package PROCEDURE ************/
PROCEDURE GET_DESC_LENGTH(O_error_message       IN OUT  VARCHAR2,
                          O_parent_desc_length  IN OUT  NUMBER,
                          O_diff_desc_length    IN OUT  NUMBER,
                          I_item_desc           IN      SVC_ITEM_MASTER.ITEM_DESC%TYPE,
                          I_diff_no             IN      NUMBER)
   IS

   L_program              VARCHAR2(40) := 'DIFF_FINALIZATION.GET_DESC_LENGTH';


BEGIN

  if lengthb(I_item_desc) < 150 then
      O_parent_desc_length := lengthb(I_item_desc);
      O_diff_desc_length := trunc( ( (250 - I_diff_no) - O_parent_desc_length) / I_diff_no );
   else
     if I_diff_no = 4 then
        O_parent_desc_length := 134;
        O_diff_desc_length   := 28;
     elsif I_diff_no = 3 then
         O_parent_desc_length := 142;
         O_diff_desc_length   := 35;
      elsif I_diff_no = 2 then
         O_parent_desc_length := 148;
         O_diff_desc_length   := 50;
      elsif I_diff_no = 1 then
         O_parent_desc_length := 174;
         O_diff_desc_length   := 75;
      end if;
   end if;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);

END GET_DESC_LENGTH;
/***** End  Local Procedure  ******/

END DIFF_FINALIZATION;
/