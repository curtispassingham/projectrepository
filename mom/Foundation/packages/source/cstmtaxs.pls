CREATE OR REPLACE PACKAGE CUSTOM_TAX_SQL AUTHID CURRENT_USER AS
---
-------------------------------------------------------------------------------------------------------
-- Function Name: CALC_CTAX_COST_TAX
-- Purpose: This function will be used to define custom logic for purchase tax calculation.It is called
--          from TAX_SQL if VAT_CALC_TYPE for VAT_REGION is 'C'ustom.
-------------------------------------------------------------------------------------------------------
FUNCTION CALC_CTAX_COST_TAX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_tax_calc_tbl IN OUT NOCOPY OBJ_TAX_CALC_TBL)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
-- Function Name: CALC_CTAX_RETAIL_TAX
-- Purpose: This function will be used to define custom logic for retail sales tax calculation.It is 
--          called from TAX_SQL if VAT_CALC_TYPE for VAT_REGION is 'C'ustom.
-------------------------------------------------------------------------------------------------------
FUNCTION CALC_CTAX_RETAIL_TAX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_tax_calc_tbl IN OUT NOCOPY OBJ_TAX_CALC_TBL)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
-- Function Name: CALC_CTAX_RETAIL_TAX
-- Purpose: This function will add taxes to Retail when Item is initially setup.It is 
--          called from TAX_SQL if VAT_CALC_TYPE for VAT_REGION is 'C'ustom.
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_CTAX_RETAIL_TAX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_tax_add_remove_tbl IN OUT NOCOPY OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END CUSTOM_TAX_SQL;
/