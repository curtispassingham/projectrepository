
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEPT_ATTRIB_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------
-- Name:      GET_ACCTNG_METHODS
-- Purpose:   This function gets the markup type and profit
--		  type from the department table for a given
--		  department
----------------------------------------------------------------
   FUNCTION GET_ACCTNG_METHODS (O_error_message IN OUT	VARCHAR2,
				        I_dept		IN	      NUMBER,
				        O_profit_type	IN OUT	NUMBER,
			               O_markup_type	IN OUT	VARCHAR2)
			return BOOLEAN;

--------------------------------------------------------------------
--- Name:       GET_NAME
--- Purpose:    Looks up a DEPT name from DEPS table.
--------------------------------------------------------------------
   FUNCTION GET_NAME( O_error_message IN OUT VARCHAR2,
                      I_dept           IN     NUMBER,
                      O_dept_desc      IN OUT VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------
---Name:       GET_DEPT_HIER
---Purpose:    Retrieves the division and group for a given department.
--------------------------------------------------------------------
   FUNCTION GET_DEPT_HIER (O_error_message IN OUT VARCHAR2,
                           O_group         IN OUT deps.group_no%TYPE,
                           O_division      IN OUT groups.division%TYPE,
                           I_dept          IN     deps.dept%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
---Name:       GET_PURCHASE_TYPE
---Purpose:    Retrieves purchase_type for a given department.
---            1 indicates normal merchadise.  2 indicates consignment
---            stock.
--------------------------------------------------------------------
   FUNCTION GET_PURCHASE_TYPE (O_error_message IN OUT VARCHAR2,
                               O_purchase_type IN OUT DEPS.PURCHASE_TYPE%TYPE,
                               I_dept          IN     DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
---Name:       GET_MARKUP
---Purpose:    This function will retrieve the markup calculation type, 
---            budgeted intake, and budgeted markup for a given department.
--------------------------------------------------------------------
   FUNCTION GET_MARKUP( O_error_message	 	IN OUT	VARCHAR2,
                        O_markup_calc_type   	IN OUT	DEPS.MARKUP_CALC_TYPE%TYPE, 
                        O_budgeted_intake		IN OUT	DEPS.BUD_INT%TYPE,
                        O_budgeted_markup		IN OUT	DEPS.BUD_MKUP%TYPE,
                        I_dept			IN		DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
---Name:       GET_BUYER
---Purpose:    This function will retrieve the buyer and buyer name 
---            for a given department.
--------------------------------------------------------------------
FUNCTION GET_BUYER(O_error_message   IN OUT   VARCHAR2,
                   O_buyer           IN OUT   DEPS.BUYER%TYPE,
                   O_buyer_name      IN OUT   BUYER.BUYER_NAME%TYPE,
                   I_dept            IN       DEPS.DEPT%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------
-- Name:       GET_DEPT_VAT_IND
-- Purpose:    This function will retrieve the department vat include indicator
--             for a given department.
--------------------------------------------------------------------
FUNCTION GET_DEPT_VAT_IND (O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_dept_vat_incl_ind  IN OUT DEPS.DEPT_VAT_INCL_IND%TYPE,
                           I_dept               IN     DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------
END DEPT_ATTRIB_SQL;
/


