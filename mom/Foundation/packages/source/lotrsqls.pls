CREATE OR REPLACE PACKAGE LOC_TRAITS_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------------------
--
--Function Name : Get_Desc
--Purpose : This function gets the location trait description from the
--          table loc_traits.
--
--Date Created : 21-MAR-97
--
--Created by : Mark Strashun
------------------------------------------------------------------------
FUNCTION GET_DESC (O_error_message    IN OUT  VARCHAR2,
                   O_loc_trait_desc   IN OUT  VARCHAR2,
                   I_loc_trait        IN      NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------
--
--Function Name : relation_exists
--Purpose : This function calls the other RELATION_EXISTS function,
--          passing in a NULL value for I_store.
--
------------------------------------------------------------------------
FUNCTION RELATION_EXISTS (O_error_message IN OUT  VARCHAR2,
                          O_exists        IN OUT  BOOLEAN,
                          I_loc_trait     IN      NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------
--
--Function Name : relation_exists
--Purpose : This function indicats if the selected organizational
--          hierarchy/location trait relationship exists on the
--          appropriate location trait table
--
--Date Created : 21-MAR-97
--
--Created by : Mark Strashun
------------------------------------------------------------------------
FUNCTION RELATION_EXISTS (O_error_message IN OUT  VARCHAR2,
                          O_exists        IN OUT  BOOLEAN,
                          I_loc_trait     IN      NUMBER,
                          I_store         IN      NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------
--
--Function Name : DELETE_LOC_MATRIX
--Purpose : This function delets traits from LOC_TRAITS_MATRIX table.
--
--Date Created : 23-MAR-97
--
--Created by : Mark Strashun
------------------------------------------------------------------------
FUNCTION DELETE_LOC_TRAITS_MATRIX(O_error_message IN OUT  VARCHAR2,
                                  I_loc_trait     IN      NUMBER,
                                  I_area          IN      NUMBER,
                                  I_region        IN      NUMBER,
                                  I_district      IN      NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function Name: VALIDATE_AREA_TRAIT
--       Purpose: Check to see whether the area belongs to the chain to which
--                the location trait is assigned.
FUNCTION VALIDATE_AREA_TRAIT(O_error_message IN OUT  VARCHAR2,
                             O_valid         IN OUT  BOOLEAN,
                             I_area          IN      AREA.AREA%TYPE,
                             I_loc_trait     IN      LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: VALIDATE_DISTRICT_TRAIT
--       Purpose: Check to see whether the district belongs to the filter_org_id
--                in the table loc_traits for loc_trait.
FUNCTION VALIDATE_DISTRICT_TRAIT(O_error_message IN OUT  VARCHAR2,
                                 O_valid         IN OUT  BOOLEAN,
                                 I_district      IN      DISTRICT.DISTRICT%TYPE,
                                 I_loc_trait     IN      LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------
-- Function    : INSERT_LOCTRAIT
-- Purpose     : Takes in a loc_trait table record and inserts all of
--               it's contents into the LOC_TRAITS table.
--Created      : 08-Aug-03 Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION INSERT_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function    : UPDATE_LOCTRAIT
-- Purpose     : Takes in a loc_trait table record and updates all of
--               it's contents into the LOC_TRAITS table.
--Created      : 08-Aug-03 Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION UPDATE_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function    : DELETE_LOCTRAIT
-- Purpose     : Takes in a loc_trait table record and deletes all of
--               it's contents from the LOC_TRAITS table.
--Created      : 08-Aug-03 Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION DELETE_LOCTRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_loctrait_rec    IN       LOC_TRAITS%ROWTYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function    : LOC_TRAIT_EXISTS
-- Purpose     : Takes in a loc_trait and checks if it exists in the LOC_TRAITS table.
--Created      : 08-Aug-03 Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION LOC_TRAIT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          OUT      BOOLEAN,
                          I_loctrait        IN       LOC_TRAITS.LOC_TRAIT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function    : DELETE_PARENT_AREA
-- Purpose     : Deletes all the loc traits for the region's area from the
--               LOC_TRAITS_MATRIX table.
--Created      : 20-Aug-03 Ash Ledesma
---------------------------------------------------------------------
FUNCTION DELETE_PARENT_AREA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_hier_value      IN       NUMBER,
                            I_parent_id       IN       NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------
FUNCTION DELETE_PARENT_REGION(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hier_value      IN      NUMBER,
                              I_parent_id       IN      NUMBER)
   RETURN BOOLEAN;
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Function Name:  INSERT_LOC_TRAITS
-- Purpose      :  Defaults the values down the hierarchy level once
--                 create message types are passed.
---------------------------------------------------------------------
FUNCTION INSERT_LOC_TRAITS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name:  DELETE_LOC_TRAITS
-- Purpose      :  Defaults records down the hierarchy level
--                 once delete loc_trait message type is passed.
---------------------------------------------------------------------
FUNCTION DELETE_LOC_TRAITS(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_org_hier_rec   IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  INSERT_STORE_TRAIT
-- Purpose      :  Inserts records into the LOC_TRAITS_MATRIX table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_STORE_TRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_loc_traits      IN       ORGANIZATION_SQL.LOC_TRAIT_TBL,
                            I_store           IN       STORE_ADD.STORE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  DELETE_STORE_TRAIT
-- Purpose      :  Deletes records from the LOC_TRAITS_MATRIX table
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_STORE_TRAIT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_loc_traits      IN       ORGANIZATION_SQL.LOC_TRAIT_TBL,
                            I_store           IN       STORE_ADD.STORE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END;
/
