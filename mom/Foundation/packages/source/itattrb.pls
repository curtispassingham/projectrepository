CREATE OR REPLACE PACKAGE BODY ITEM_ATTRIB_SQL AS
--------------------------------------------------------------------------------
FUNCTION NEXT_ITEM (O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_item          IN OUT item_master.item%TYPE)
RETURN BOOLEAN IS
   L_wrap_sequence_number   item_master.item%TYPE;
   L_check_digit            NUMBER(1);
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);
   L_check_digit_ind        system_options.check_digit_ind%TYPE;
   L_sequence_name          VARCHAR2(20);
   NO_SEQ_NO_AVAIL          EXCEPTION;
   PRAGMA EXCEPTION_INIT(NO_SEQ_NO_AVAIL, -8004);

   cursor C_CHECK_DIGIT is
      select check_digit_ind
        from system_options;

   cursor C_EXISTS is
      select 'x'
        from item_master
       where item = O_item
       union
      select 'x'
        from svc_item_reservation
       where item = O_item;

   cursor C_ITEM_SEQUENCE is
         select item_sequence.NEXTVAL
           from sys.dual;

   cursor C_ITEM_CHKDIG_SEQUENCE is
         select item_chkdig_sequence.NEXTVAL
           from sys.dual;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DIGIT',
                    'system_options',
                    NULL);
   open C_CHECK_DIGIT;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DIGIT',
                    'system_options',
                    NULL);
   fetch C_CHECK_DIGIT into L_check_digit_ind;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DIGIT',
                    'system_options',
                    NULL);
   close C_CHECK_DIGIT;
   LOOP
      if L_check_digit_ind ='N' then
         L_sequence_name := 'ITEM_SEQUENCE';
         SQL_LIB.SET_MARK('OPEN',
                          'item_sequence.NEXTVAL',
                          'sys.dual',
                          NULL);
         open C_ITEM_SEQUENCE;
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'item_sequence.NEXTVAL',
                          'sys.dual',
                          NULL);
         fetch C_ITEM_SEQUENCE into O_item;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'item_sequence.NEXTVAL',
                          'sys.dual',
                          NULL);
         close C_ITEM_SEQUENCE;
         L_check_digit := 0;
              --  Even though not doing check digit logic, we still want
              --  to do wrap sequence logic so we'll set L_check_digit
              --  = 0 to get past the if statement a few lines down
      elsif L_check_digit_ind ='Y' then
         L_sequence_name := 'ITEM_CHKDIG_SEQUENCE';
         SQL_LIB.SET_MARK('OPEN',
                          'item_chkdig_sequence.NEXTVAL',
                          'sys.dual',
                          NULL);
         open C_ITEM_CHKDIG_SEQUENCE;
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'item_chkdig_sequence.NEXTVAL',
                          'sys.dual',
                          NULL);
         fetch C_ITEM_CHKDIG_SEQUENCE into O_item;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'item_chkdig_sequence.NEXTVAL',
                          'sys.dual',
                          NULL);
         close C_ITEM_CHKDIG_SEQUENCE;
         CHKDIG_ADD(O_item, L_check_digit,'Y');
      end if;
      if (L_check_digit >= 0) then
         if L_first_time = 'Yes' then
            L_wrap_sequence_number := O_item;
            L_first_time := 'No';
         elsif O_item = L_wrap_sequence_number then
            O_error_message := 'Fatal error - no available item numbers';
            return FALSE;
         end if;
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_EXISTS',
                          'item_master',
                          NULL);
         open C_EXISTS;
         ---
         SQL_LIB.SET_MARK('FETCH',
                          'C_EXISTS',
                          'item_master',
                          NULL);
         fetch C_EXISTS into L_dummy;
         if (C_EXISTS%NOTFOUND) then
           ---
           SQL_LIB.SET_MARK('CLOSE',
                            'C_EXISTS',
                            'item_master',
                            NULL);
            close C_EXISTS;
            exit;
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXISTS',
                          'item_master',
                          NULL);
         close C_EXISTS;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   WHEN NO_SEQ_NO_AVAIL THEN
      O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                            L_sequence_name,
                                            NULL,
                                            NULL);
      return FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.NEXT_ITEM',
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_ITEM;
--------------------------------------------------------------------
FUNCTION INV_STATUS_DESC(O_error_message    IN OUT VARCHAR2,
                         I_inv_status       IN     NUMBER,
                         O_desc             IN OUT VARCHAR2)
RETURN BOOLEAN IS

   cursor C_ATTRIB is
   select inv_status_desc
     from v_inv_status_types_tl
    where inv_status = I_inv_status;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_ATTRIB', 'INV_STATUS_TYPES', to_char(I_inv_status));
   open C_ATTRIB;

   SQL_LIB.SET_MARK('FETCH' , 'C_ATTRIB', 'INV_STATUS_TYPES', to_char(I_inv_status));
   fetch C_ATTRIB into O_desc;

   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_STATUS',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'INV_STATUS_TYPES', to_char(I_inv_status));
      close C_ATTRIB;
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE' , 'C_ATTRIB', 'INV_STATUS_TYPES', to_char(I_inv_status));
   close C_ATTRIB;

   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.INV_STATUS_DESC',
                                            to_char(SQLCODE));
   RETURN FALSE;
END INV_STATUS_DESC;
---------------------------------------------------------------------------------------------
FUNCTION GET_MERCH_HIER( O_error_message IN OUT VARCHAR2,
                         I_item          IN     item_master.item%TYPE,
                         O_dept          IN OUT item_master.dept%TYPE,
                         O_class         IN OUT item_master.class%TYPE,
                         O_subclass      IN OUT item_master.subclass%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(64)        := 'ITEM_ATTRIB_SQL.GET_MERCH_HIER';
   cursor C_MERCH_HIER is
      select dept,
             class,
             subclass
        from item_master
       where item = I_item;
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN' , 'C_MERCH_HIER', 'ITEM_MASTER', I_item);
   open C_MERCH_HIER;
   ---
   SQL_LIB.SET_MARK('FETCH' , 'C_MERCH_HIER', 'ITEM_MASTER', I_item);
   fetch C_MERCH_HIER into O_dept,
                           O_class,
                           O_subclass;
   ---
   if C_MERCH_HIER%notfound then
      SQL_LIB.SET_MARK('CLOSE' , 'C_MERCH_HIER', 'ITEM_MASTER', I_item);
      close C_MERCH_HIER;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             null,
                                             null,
                                             null);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE' , 'C_MERCH_HIER', 'ITEM_MASTER', I_item);
   close C_MERCH_HIER;
   return TRUE;

EXCEPTION
   when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          NULL);
        RETURN FALSE;
END GET_MERCH_HIER;
---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_COST_RETAIL(O_error_message     IN OUT    VARCHAR2,
                              O_unit_cost_prim            IN OUT  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              O_standard_unit_retail_prim IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom_prim         IN OUT  ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail_prim  IN OUT  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom_prim          IN OUT  ITEM_LOC.SELLING_UOM%TYPE,
                              I_item                      IN      ITEM_SUPPLIER.ITEM%TYPE,
                              I_calc_type                 IN      VARCHAR2)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL';
   L_return_code                NUMBER;
   L_error_msg                  VARCHAR2(255);
   L_unit_retail_zon            ITEM_LOC.UNIT_RETAIL%TYPE;
   L_unit_cost_sup              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_supplier                   ITEM_SUPPLIER.SUPPLIER%TYPE;
   L_orderable_ind              ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_sellable_ind               ITEM_MASTER.SELLABLE_IND%TYPE;
   L_pack_no                    ITEM_MASTER.ITEM%TYPE;
   L_dept                       ITEM_MASTER.DEPT%TYPE;
   L_class                      ITEM_MASTER.CLASS%TYPE;
   L_subclass                   ITEM_MASTER.SUBCLASS%TYPE;
   L_cost_zone_group_id         ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE;
   L_contains_inner_ind         ITEM_MASTER.CONTAINS_INNER_IND%TYPE;
   L_order_as_type              ITEM_MASTER.ORDER_AS_TYPE%TYPE;
   L_pack_type                  ITEM_MASTER.PACK_TYPE%TYPE;
   L_cost                       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_pack_ind                   ITEM_MASTER.PACK_IND%TYPE;
   L_multi_unit_retail_zon      ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_unit_retail_prim     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_zon      ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_multi_units_zon            ITEM_LOC.MULTI_UNITS%TYPE;
   L_standard_class             UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor                ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_selling_unit_retail_zon    ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;

   L_item_xform_ind             ITEM_MASTER.ITEM_XFORM_IND%TYPE;

   ---
   cursor C_PACK_SKU IS
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_item;


   cursor C_ITEM IS
      select item_xform_ind
        from item_master
       where item = I_item;


BEGIN

   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;


   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM',
                    'item_master',
                    'I_item: ' || I_Item);
   open C_ITEM;

   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM',
                    'item_master',
                    'I_item: ' || I_Item);
   fetch C_ITEM into L_item_xform_ind;

   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM',
                    'item_master',
                    'I_item: ' || I_Item);
   close C_ITEM;

   if (I_calc_type is NULL) or (I_calc_type = 'C') then
      if (L_orderable_ind = 'N'and L_pack_ind = 'Y') then
         O_unit_cost_prim := 0;
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_PACK_SKU', 'V_PACKSKU_QTY', 'PACK_NO: '||I_item);
         FOR rec_in IN C_PACK_SKU LOOP
            ---get primary supplier cost for each sku
            if SUPP_ITEM_SQL.GET_PRI_SUP_COST(O_error_message,
                                              L_supplier,
                                              L_cost,
                                              rec_in.item,
                                              NULL) = FALSE then
               return FALSE;
            end if;
            ---
            if L_supplier is NOT NULL then
               ---convert primary supplier cost to the primary currency
               if not CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                       L_supplier,
                                                       'V',
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       L_cost,
                                                       L_cost,
                                                       'C',NULL,NULL) then
                  return FALSE;
               end if;
               O_unit_cost_prim := O_unit_cost_prim + (L_cost * rec_in.qty);
            end if;
         END LOOP;
      else
         -- Get primary supplier's cost
         if not SUPP_ITEM_SQL.GET_PRI_SUP_COST(O_error_message,
                                               L_supplier,
                                               L_unit_cost_sup,
                                               I_item,
                                               NULL) then
            return FALSE;
         end if;
         if L_unit_cost_sup is NOT NULL then
            -- Convert cost from supplier's currency to primary currency
            if not CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                    L_supplier,
                                                    'V',
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    L_unit_cost_sup,
                                                    O_unit_cost_prim,
                                                    'C',
                                                    NULL,
                                                    NULL) then
               return FALSE;
            end if;
         end if;
      end if;
   end if;
   if (I_calc_type is NULL) or (I_calc_type = 'R') then

      if (L_orderable_ind = 'Y') and
         (L_item_xform_ind = 'Y') then
         if ITEM_XFORM_SQL.CALCULATE_RETAIL(O_error_message,
                                            I_item,
                                            NULL,
                                            L_unit_retail_zon) = FALSE then
            return FALSE;
         end if;
         O_selling_unit_retail_prim := L_unit_retail_zon;
         O_standard_unit_retail_prim := L_unit_retail_zon;

      elsif (L_sellable_ind = 'N') and
            (L_pack_ind = 'N') then
          O_selling_unit_retail_prim := 0;
          O_standard_unit_retail_prim := 0;
      elsif (L_sellable_ind = 'N') and
            (L_pack_ind = 'Y') then

         O_selling_unit_retail_prim := 0;
         O_standard_unit_retail_prim := 0;

         if not ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                                 O_standard_uom_prim,
                                                 L_conv_factor,
                                                 L_standard_class,
                                                 I_item,
                                                 NULL) then
               return FALSE;
         else
            O_selling_uom_prim := O_standard_uom_prim;
         end if;
      else
         -- Get base retail
         if not PRICING_ATTRIB_SQL.GET_BASE_ZONE_RETAIL(O_error_message,
                                                        O_standard_unit_retail_prim,
                                                        L_unit_retail_zon,
                                                        O_standard_uom_prim,
                                                        O_selling_unit_retail_prim,
                                                        L_selling_unit_retail_zon,
                                                        O_selling_uom_prim,
                                                        L_multi_units_zon,
                                                        L_multi_unit_retail_prim,
                                                        L_multi_unit_retail_zon,
                                                        L_multi_selling_uom_zon,
                                                        I_item) then
            return FALSE;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                SQLERRM,
                                L_program,
                                to_char(SQLCODE));
      return FALSE;

END GET_BASE_COST_RETAIL;
---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_COST_RETAIL(O_error_message             IN OUT  VARCHAR2,
                              O_unit_cost_prim            IN OUT  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              O_standard_unit_retail_prim IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom_prim         IN OUT  ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail_prim  IN OUT  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom_prim          IN OUT  ITEM_LOC.SELLING_UOM%TYPE,
                              I_item                      IN      ITEM_SUPPLIER.ITEM%TYPE)
                              RETURN BOOLEAN IS
   L_calc_type   VARCHAR2(1) := NULL;
   L_program     VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL';
BEGIN
   if not GET_BASE_COST_RETAIL(O_error_message,
                               O_unit_cost_prim,
                               O_standard_unit_retail_prim,
                               O_standard_uom_prim,
                               O_selling_unit_retail_prim,
                               O_selling_uom_prim,
                               I_item,
                               L_calc_type) then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                SQLERRM,
                                L_program,
                                to_char(SQLCODE));
    return FALSE;
END GET_BASE_COST_RETAIL;
-----------------------------------------------------------------------------
FUNCTION CONSIGNMENT_ITEM(O_error_message IN OUT VARCHAR2,
                          I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                          O_consignment   IN OUT BOOLEAN)
RETURN BOOLEAN IS
   L_dummy VARCHAR2(1);
   cursor C_ITEM_SUPPLIER is
      select 'x'
        from item_supplier
       where item_supplier.item = I_item
         and primary_supp_ind = 'Y'
         and consignment_rate is not NULL;
BEGIN
   O_consignment := FALSE;
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM_SUPPLIER',
                    'ITEM_SUPPLIER', 'ITEM:  '||I_item);
   open C_ITEM_SUPPLIER;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM_SUPPLIER',
                    'ITEM_SUPPLIER', 'ITEM:  '||I_item);
   fetch C_ITEM_SUPPLIER into L_dummy;
   if C_ITEM_SUPPLIER%FOUND then
      O_consignment := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_SUPPLIER',
                    'ITEM_SUPPLIER', 'ITEM:  '||I_item);
   close C_ITEM_SUPPLIER;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.CONSIGNMENT_ITEM',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CONSIGNMENT_ITEM;
------------------------------------------------------------------------
FUNCTION GET_COST_ZONE_GROUP(O_error_message           IN OUT VARCHAR2,
                             O_cost_zone_group_id      IN OUT COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                             I_item                    IN     ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN IS

L_system_ind  VARCHAR2(1);

   cursor C_ITEM is
      select cost_zone_group_id
        from item_master
       where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ITEM','ITEM_MASTER','item:'||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH','C_ITEM','ITEM_MASTER','item:'||I_item);
   fetch C_ITEM into O_cost_zone_group_id;
   if C_ITEM%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                               null,
                                               null,
                                               null);
         SQL_LIB.SET_MARK('CLOSE','C_ITEM','ITEM_MASTER','item:'||I_item);
         close C_ITEM;
         return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM','ITEM_MASTER','ITEM:'||I_item);
   close C_ITEM;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_COST_ZONE_GROUP',
                                            TO_CHAR(SQLCODE));
   RETURN FALSE;
END GET_COST_ZONE_GROUP;
---------------------------------------------------------------------------------------------
FUNCTION IMPORT_ATTR_EXISTS(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_dummy    VARCHAR2(1);
   ---
   cursor C_ITEM_IMPORT_ATTR is
      select 'x'
        from item_import_attr
       where item = I_item
       and ROWNUM = 1;
BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_IMPORT_ATTR','ITEM_IMPORT_ATTR','Item: '||I_item);
   open C_ITEM_IMPORT_ATTR;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_IMPORT_ATTR','ITEM_IMPORT_ATTR','Item: '||I_item);
   fetch C_ITEM_IMPORT_ATTR into L_dummy;
   if C_ITEM_IMPORT_ATTR%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_IMPORT_ATTR','ITEM_IMPORT_ATTR','Item: '||I_item);
   close C_ITEM_IMPORT_ATTR;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.IMPORT_ATTR_EXISTS',
                                            to_char(SQLCODE));
   RETURN FALSE;
END IMPORT_ATTR_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION ITEM_ELIGIBLE_EXISTS(O_error_message  IN OUT VARCHAR2,
                              O_exists         IN OUT BOOLEAN,
                              I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_dummy    VARCHAR2(1);
   ---
   cursor C_COND_TARIFF_TREATMENT is
      select 'x'
        from cond_tariff_treatment
        where item = I_item
        and ROWNUM = 1;
BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_COND_TARIFF_TREATMENT','COND_TARIFF_TREATMENT','Item: '||I_item);
   open C_COND_TARIFF_TREATMENT;
   SQL_LIB.SET_MARK('FETCH','C_COND_TARIFF_TREATMENT','COND_TARIFF_TREATMENT','Item: '||I_item);
   fetch C_COND_TARIFF_TREATMENT into L_dummy;
   if C_COND_TARIFF_TREATMENT%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_COND_TARIFF_TREATMENT','COND_TARIFF_TREATMENT','Item: '||I_item);
   close C_COND_TARIFF_TREATMENT;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.ITEM_ELIGIBLE_EXISTS',
                                            to_char(SQLCODE));
   RETURN FALSE;
END ITEM_ELIGIBLE_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION SUPPLIER_EXISTS(O_error_message  IN OUT VARCHAR2,
                         O_exists         IN OUT BOOLEAN,
                         I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_dummy      VARCHAR2(1);
   ---
   cursor C_ITEM_SUPPLIER is
      select 'x'
        from item_supplier
       where item = I_item
       and ROWNUM = 1;
BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPPLIER','ITEM_SUPPLIER','Item: '||I_item);
   open C_ITEM_SUPPLIER;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPPLIER','ITEM_SUPPLIER','Item: '||I_item);
   fetch C_ITEM_SUPPLIER into L_dummy;
   if C_ITEM_SUPPLIER%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPPLIER','ITEM_SUPPLIER','Item: '||I_item);
   close C_ITEM_SUPPLIER;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.SUPPLIER_EXISTS',
                                            to_char(SQLCODE));
   RETURN FALSE;
END SUPPLIER_EXISTS;
----------------------------------------------------------------------------------------
FUNCTION GET_COST_ZONE(O_error_message IN OUT VARCHAR2,
                       O_zone_id       IN OUT COST_ZONE.ZONE_ID%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                       I_zone_group_id IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                       I_loc           IN     STORE.STORE%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program         VARCHAR2(255) := 'ITEM_ATTRIB_SQL.GET_COST_ZONE';
   L_zone_group_id   COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   ---
   cursor C_COST_ZONE_GROUP_LOC is
      select zone_id
        from cost_zone_group_loc
       where zone_group_id = L_zone_group_id
         and location = I_loc;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_item',
                                            'NULL');
      return FALSE;
   end if;

   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_program,
                                            'I_loc',
                                            'NULL');
      return FALSE;
   end if;

   L_zone_group_id := I_zone_group_id;

   if L_zone_group_id is NULL then
      if not ITEM_ATTRIB_SQL.GET_COST_ZONE_GROUP(O_error_message,
                                                 L_zone_group_id,
                                                 I_item) then
         return FALSE;
      end if;
   end if;
   SQL_LIB.SET_MARK('OPEN', 'C_COST_ZONE_GROUP_LOC', 'COST_ZONE_GROUP_LOC',
                    'Zone group id: '||to_char(I_zone_group_id)||' Location: '||to_char(I_loc));
   open C_COST_ZONE_GROUP_LOC;
   SQL_LIB.SET_MARK('FETCH', 'C_COST_ZONE_GROUP_LOC', 'COST_ZONE_GROUP_LOC',
                    'Zone group id: '||to_char(I_zone_group_id)||' Location: '||to_char(I_loc));
   fetch C_COST_ZONE_GROUP_LOC into O_zone_id;
   if C_COST_ZONE_GROUP_LOC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_COST_ZONE_GROUP_LOC', 'COST_ZONE_GROUP_LOC',
                       'Zone group id: '||to_char(I_zone_group_id)||' Location: '||to_char(I_loc));
      close C_COST_ZONE_GROUP_LOC;
      O_error_message := SQL_LIB.CREATE_MSG('NO_ZONE_ID', I_item, to_char(I_loc), NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_COST_ZONE_GROUP_LOC', 'COST_ZONE_GROUP_LOC',
                    'Zone group id: '||to_char(I_zone_group_id)||' Location: '||to_char(I_loc));
   close C_COST_ZONE_GROUP_LOC;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_COST_ZONE;
---------------------------------------------------------------------------------------------
FUNCTION GET_STANDARD_UOM(O_error_message               IN OUT VARCHAR2,
                          O_standard_uom                IN OUT UOM_CLASS.UOM%TYPE,
                          O_standard_class              IN OUT UOM_CLASS.UOM_CLASS%TYPE,
                          O_conv_factor                 IN OUT ITEM_MASTER.UOM_CONV_FACTOR%TYPE,
                          I_item                        IN     ITEM_MASTER.ITEM%TYPE,
                          I_get_class_ind               IN     VARCHAR2)
   RETURN BOOLEAN IS
   ---
   L_program         VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_STANDARD_UOM';
   ---
   cursor C_ITEM is
      select standard_uom, uom_conv_factor
        from item_master
       where item = I_item;

 BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   fetch C_ITEM into O_standard_uom, O_conv_factor;
   if C_ITEM%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
      close C_ITEM;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',NULL,NULL,NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   close C_ITEM;

   ---
   if I_get_class_ind = 'Y' then
      ---
      if UOM_SQL.GET_CLASS (O_error_message,
                            O_standard_class,
                            O_standard_uom) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STANDARD_UOM;
-----------------------------------------------------------------------------------------------
FUNCTION GET_STORE_ORD_MULT_AND_UOM(O_error_message   IN OUT VARCHAR2,
                            O_store_ord_mult  IN OUT ITEM_MASTER.STORE_ORD_MULT%TYPE,
                            O_standard_uom    IN OUT ITEM_MASTER.STANDARD_UOM%TYPE,
                            I_item            IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(64)                := 'ITEM_ATTRIB_SQL.GET_STORE_ORD_MULT_AND_UOM';
   ---
   cursor C_ITEM is
      select store_ord_mult, standard_uom
        from item_master
       where item = I_item;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   fetch C_ITEM into O_store_ord_mult, O_standard_uom;
   if C_ITEM%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
      close C_ITEM;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',NULL,NULL,NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   close C_ITEM;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STORE_ORD_MULT_AND_UOM;
----------------------------------------------------------------------------------------------
FUNCTION GET_WASTAGE(O_error_message     IN OUT  VARCHAR2,
                     O_waste_type        IN OUT  ITEM_MASTER.WASTE_TYPE%TYPE,
                     O_waste_pct         IN OUT  ITEM_MASTER.WASTE_PCT%TYPE,
                     O_default_waste_pct IN OUT  ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                     I_item              IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_WASTAGE';
   cursor C_WASTE is
      select waste_type,
             NVL(waste_pct, 0),
             default_waste_pct
        from item_master
       where item = I_item;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_WASTE','ITEM_MASTER','Item:'||I_item);
   open C_WASTE;
   SQL_LIB.SET_MARK('FETCH', 'C_WASTE','ITEM_MASTER','Item:'||I_item);
   fetch C_WASTE into O_waste_type,
                      O_waste_pct,
                      O_default_waste_pct;
   SQL_LIB.SET_MARK('CLOSE', 'C_WASTE','ITEM_MASTER','Item:'||I_item);
   close C_WASTE;
   return TRUE;
EXCEPTION
   when OTHERS then
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        null);
     RETURN FALSE;
END GET_WASTAGE;
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_DEFAULT_UOP(O_error_message     IN OUT VARCHAR2,
                            I_standard_uom      IN     ITEM_MASTER.STANDARD_UOM%TYPE,
                            I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(60) := 'ITEM_ATTRIB_SQL.UPDATE_DEFAULT_UOP';

   cursor C_PARENT is
      select item
        from item_master
       where item_parent = I_item;

   cursor C_GRANDPARENT is
      select item
        from item_master
       where item_grandparent = I_item;


BEGIN
   SQL_LIB.SET_MARK('UPDATE', 'ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);
   update item_supp_country
      set default_uop = I_standard_uom,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where item = I_item
      and default_uop not in ('C','P');
---
   FOR rec IN C_PARENT LOOP
      SQL_LIB.SET_MARK('UPDATE', 'ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);
      update item_supp_country
        set default_uop = I_standard_uom,
            last_update_id = get_user,
            last_update_datetime = sysdate
       where item = rec.item
         and default_uop not in ('C','P');
   END LOOP;
---
   FOR rec IN C_GRANDPARENT LOOP
      SQL_LIB.SET_MARK('UPDATE', 'ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item);
      update item_supp_country
        set default_uop = I_standard_uom,
            last_update_id = get_user,
            last_update_datetime = sysdate
       where item = rec.item
         and default_uop not in ('C','P');
   END LOOP;
---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      return FALSE;
END UPDATE_DEFAULT_UOP;
---------------------------------------------------------------------------------------------------
FUNCTION PURCHASE_TYPE(O_error_message   IN OUT    VARCHAR2,
                       O_purchase_type   IN OUT    DEPS.PURCHASE_TYPE%TYPE,
                       I_item            IN        ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
---
L_purchase_type   DEPS.PURCHASE_TYPE%TYPE;
---
   cursor C_PURCHASE_TYPE is
      select deps.purchase_type
        from deps, item_master
       where deps.dept = item_master.dept
         and item_master.item = I_item;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_PURCHASE_TYPE','DEPS, ITEM_MASTER','ITEM: '||I_item);
   open C_PURCHASE_TYPE;
   SQL_LIB.SET_MARK('FETCH', 'C_PURCHASE_TYPE','DEPS, ITEM_MASTER','ITEM: '||I_item);
   fetch C_PURCHASE_TYPE into O_purchase_type;
   if C_PURCHASE_TYPE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             null,
                                             null,
                                             null);
      SQL_LIB.SET_MARK('CLOSE','C_PURCHASE_TYPE','ITEM_MASTER','item:'||I_item);
      close C_PURCHASE_TYPE;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_PURCHASE_TYPE','DEPS, ITEM_MASTER','ITEM: '||I_item);
   close C_PURCHASE_TYPE;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.PURCHASE_TYPE',
                                            to_char(SQLCODE));
   RETURN FALSE;
END PURCHASE_TYPE;
---------------------------------------------------------------------------------------------------
FUNCTION GET_DESC( O_error_message IN OUT VARCHAR2,
                   O_desc          IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                   I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_dummy1   ITEM_MASTER.STATUS%TYPE;
   L_dummy2   ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_dummy3   ITEM_MASTER.TRAN_LEVEL%TYPE;

BEGIN
   if GET_DESC (O_error_message,
                O_desc,
                L_dummy1,
                L_dummy2,
                L_dummy3,
                I_item) = FALSE then
      return FALSE;
   else
      return TRUE;
   end if;
END GET_DESC;
---------------------------------------------------------------------------------------------
FUNCTION GET_DESC( O_error_message IN OUT VARCHAR2,
                   O_desc          IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                   O_status        IN OUT ITEM_MASTER.STATUS%TYPE,
                   I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_dummy2   ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_dummy3   ITEM_MASTER.TRAN_LEVEL%TYPE;

BEGIN
   if GET_DESC (O_error_message,
                O_desc,
                O_status,
                L_dummy2,
                L_dummy3,
                I_item) = FALSE then
      return FALSE;
   else
      return TRUE;
   end if;
END GET_DESC;
---------------------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message IN OUT VARCHAR2,
                  O_desc          IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_status        IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_item_level    IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level    IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.GET_DESC';

   cursor C_ITEM is
     select imtl.item_desc,
            im.status,
            im.item_level,
            im.tran_level
       from item_master im,
            v_item_master_tl imtl
      where im.item = I_item
        and im.item = imtl.item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER , V_ITEM_MASTER_TL ','ITEM: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER , V_ITEM_MASTER_TL ','ITEM: '||I_item);
   fetch C_ITEM into O_desc,
                     O_status,
                     O_item_level,
                     O_tran_level;
   if C_ITEM%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER , V_ITEM_MASTER_TL ','ITEM: '||I_item);
      close C_ITEM;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             null,
                                             null,
                                             null);
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER , V_ITEM_MASTER_TL ','ITEM: '||I_item);
   close C_ITEM;
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        NULL);
      RETURN FALSE;
END GET_DESC;
---------------------------------------------------------------------------------------------
FUNCTION GET_LEVELS (O_error_message         IN OUT VARCHAR2,
                     O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                     O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                     I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_ITEM is
     select item_level,
            tran_level
       from item_master
      where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   fetch C_ITEM into O_item_level, O_tran_level;
   if C_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             null,
                                             null,
                                             null);
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
      close C_ITEM;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   close C_ITEM;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_LEVELS',
                                            to_char(SQLCODE));
   RETURN FALSE;
END GET_LEVELS;

---------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT VARCHAR2,
                  O_item_desc             IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT ITEM_MASTER.PACK_IND%TYPE,
                  O_dept                  IN OUT ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                  O_simple_pack_ind       IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                  O_waste_type            IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                  O_item_parent           IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_grandparent      IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                  O_short_desc            IN OUT ITEM_MASTER.SHORT_DESC%TYPE,
                  O_waste_pct             IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                  O_default_waste_pct     IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                  I_item    IN    ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   --- Dummy variables for the call to the longer GET_INFO function
   L_item_number_type     ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE;
   L_diff_1               ITEM_MASTER.DIFF_1%TYPE;
   L_diff_1_desc          V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_1_type          V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_1_id_group_ind  V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_diff_2               ITEM_MASTER.DIFF_2%TYPE;
   L_diff_2_desc          V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_2_type          V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_2_id_group_ind  V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_order_as_type        ITEM_MASTER.ORDER_AS_TYPE%TYPE;
   L_format_id            ITEM_MASTER.FORMAT_ID%TYPE;
   L_prefix               ITEM_MASTER.PREFIX%TYPE;
   L_contains_inner_ind   ITEM_MASTER.CONTAINS_INNER_IND%TYPE;
   L_store_ord_mult       ITEM_MASTER.STORE_ORD_MULT%TYPE;

BEGIN
   if GET_INFO(O_error_message,
               O_item_desc,
               O_item_level,
               O_tran_level,
               O_status,
               O_pack_ind,
               O_dept,
               O_dept_name,
               O_class,
               O_class_name,
               O_subclass,
               O_subclass_name,
               O_sellable_ind,
               O_orderable_ind,
               O_pack_type,
               O_simple_pack_ind,
               O_waste_type,
               O_item_parent,
               O_item_grandparent,
               O_short_desc,
               O_waste_pct,
               O_default_waste_pct,
               L_item_number_type,
               L_diff_1,
               L_diff_1_desc,
               L_diff_1_type,
               L_diff_1_id_group_ind,
               L_diff_2,
               L_diff_2_desc,
               L_diff_2_type,
               L_diff_2_id_group_ind,
               L_order_as_type,
               L_format_id,
               L_prefix,
               L_store_ord_mult,
               L_contains_inner_ind,
               I_item) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_INFO',
                                            to_char(SQLCODE));
   return FALSE;
END GET_INFO;
---------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT VARCHAR2,
                  O_item_desc             IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT ITEM_MASTER.PACK_IND%TYPE,
                  O_dept                  IN OUT ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                  O_item_parent           IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_number_type      IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                  I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   --- Dummy variables for the call to the longer GET_INFO function
   L_diff_1               ITEM_MASTER.DIFF_1%TYPE;
   L_diff_1_desc          V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_1_type          V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_1_id_group_ind  V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_diff_2               ITEM_MASTER.DIFF_2%TYPE;
   L_diff_2_desc          V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_2_type          V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_2_id_group_ind  V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_order_as_type        ITEM_MASTER.ORDER_AS_TYPE%TYPE;
   L_format_id            ITEM_MASTER.FORMAT_ID%TYPE;
   L_prefix               ITEM_MASTER.PREFIX%TYPE;
   L_contains_inner_ind   ITEM_MASTER.CONTAINS_INNER_IND%TYPE;
   L_store_ord_mult       ITEM_MASTER.STORE_ORD_MULT%TYPE;
   L_simple_pack_ind      ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
   L_waste_type           ITEM_MASTER.WASTE_TYPE%TYPE;
   L_item_grandparent     ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_short_desc           ITEM_MASTER.SHORT_DESC%TYPE;
   L_waste_pct            ITEM_MASTER.WASTE_PCT%TYPE;
   L_default_waste_pct    ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;

BEGIN
   if GET_INFO(O_error_message,
               O_item_desc,
               O_item_level,
               O_tran_level,
               O_status,
               O_pack_ind,
               O_dept,
               O_dept_name,
               O_class,
               O_class_name,
               O_subclass,
               O_subclass_name,
               O_sellable_ind,
               O_orderable_ind,
               O_pack_type,
               L_simple_pack_ind,
               L_waste_type,
               O_item_parent,
               L_item_grandparent,
               L_short_desc,
               L_waste_pct,
               L_default_waste_pct,
               O_item_number_type,
               L_diff_1,
               L_diff_1_desc,
               L_diff_1_type,
               L_diff_1_id_group_ind,
               L_diff_2,
               L_diff_2_desc,
               L_diff_2_type,
               L_diff_2_id_group_ind,
               L_order_as_type,
               L_format_id,
               L_prefix,
               L_store_ord_mult,
               L_contains_inner_ind,
               I_item) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_INFO',
                                            to_char(SQLCODE));
   return FALSE;
END GET_INFO;
---------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT VARCHAR2,
                  O_item_desc             IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT ITEM_MASTER.PACK_IND%TYPE,
                  O_dept                  IN OUT ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                  O_simple_pack_ind       IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                  O_waste_type            IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                  O_item_parent           IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_grandparent      IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                  O_short_desc            IN OUT ITEM_MASTER.SHORT_DESC%TYPE,
                  O_waste_pct             IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                  O_default_waste_pct     IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                  O_item_number_type      IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                  O_diff_1                IN OUT ITEM_MASTER.DIFF_1%TYPE,
                  O_diff_1_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_1_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_1_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_diff_2                IN OUT ITEM_MASTER.DIFF_2%TYPE,
                  O_diff_2_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_2_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_2_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_order_as_type         IN OUT ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                  O_format_id             IN OUT ITEM_MASTER.FORMAT_ID%TYPE,
                  O_prefix                IN OUT ITEM_MASTER.PREFIX%TYPE,
                  O_store_ord_mult        IN OUT ITEM_MASTER.STORE_ORD_MULT%TYPE,
                  O_contains_inner_ind    IN OUT ITEM_MASTER.CONTAINS_INNER_IND%TYPE,
                  I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_diff_3               ITEM_MASTER.DIFF_3%TYPE;
   L_diff_3_desc          V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_3_type          V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_3_id_group_ind  V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_diff_4               ITEM_MASTER.DIFF_4%TYPE;
   L_diff_4_desc          V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_4_type          V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_4_id_group_ind  V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;

BEGIN
   if GET_INFO(O_error_message,
               O_item_desc,
               O_item_level,
               O_tran_level,
               O_status,
               O_pack_ind,
               O_dept,
               O_dept_name,
               O_class,
               O_class_name,
               O_subclass,
               O_subclass_name,
               O_sellable_ind,
               O_orderable_ind,
               O_pack_type,
               O_simple_pack_ind,
               O_waste_type,
               O_item_parent,
               O_item_grandparent,
               O_short_desc,
               O_waste_pct,
               O_default_waste_pct,
               O_item_number_type,
               O_diff_1,
               O_diff_1_desc,
               O_diff_1_type,
               O_diff_1_id_group_ind,
               O_diff_2,
               O_diff_2_desc,
               O_diff_2_type,
               O_diff_2_id_group_ind,
               L_diff_3,
               L_diff_3_desc,
               L_diff_3_type,
               L_diff_3_id_group_ind,
               L_diff_4,
               L_diff_4_desc,
               L_diff_4_type,
               L_diff_4_id_group_ind,
               O_order_as_type,
               O_format_id,
               O_prefix,
               O_store_ord_mult,
               O_contains_inner_ind,
               I_item) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_INFO',
                                            to_char(SQLCODE));
   return FALSE;
END GET_INFO;
---------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT VARCHAR2,
                  O_item_desc             IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT ITEM_MASTER.PACK_IND%TYPE,
                  O_dept                  IN OUT ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                  O_simple_pack_ind       IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                  O_waste_type            IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                  O_item_parent           IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_grandparent      IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                  O_short_desc            IN OUT ITEM_MASTER.SHORT_DESC%TYPE,
                  O_waste_pct             IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                  O_default_waste_pct     IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                  O_item_number_type      IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                  O_diff_1                IN OUT ITEM_MASTER.DIFF_1%TYPE,
                  O_diff_1_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_1_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_1_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_diff_2                IN OUT ITEM_MASTER.DIFF_2%TYPE,
                  O_diff_2_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_2_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_2_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_diff_3                IN OUT ITEM_MASTER.DIFF_3%TYPE,
                  O_diff_3_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_3_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_3_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_diff_4                IN OUT ITEM_MASTER.DIFF_4%TYPE,
                  O_diff_4_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_4_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_4_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_order_as_type         IN OUT ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                  O_format_id             IN OUT ITEM_MASTER.FORMAT_ID%TYPE,
                  O_prefix                IN OUT ITEM_MASTER.PREFIX%TYPE,
                  O_store_ord_mult        IN OUT ITEM_MASTER.STORE_ORD_MULT%TYPE,
                  O_contains_inner_ind    IN OUT ITEM_MASTER.CONTAINS_INNER_IND%TYPE,
                  I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_ITEM is
     select imtl.item_desc,
            im.item_level,
            im.tran_level,
            im.status,
            im.pack_ind,
            im.dept,
            im.class,
            im.subclass,
            im.sellable_ind,
            im.orderable_ind,
            im.pack_type,
            im.simple_pack_ind,
            im.waste_type,
            im.item_parent,
            im.item_grandparent,
            imtl.short_desc,
            im.waste_pct,
            im.default_waste_pct,
            im.item_number_type,
            im.diff_1,
            im.diff_2,
            im.diff_3,
            im.diff_4,
            im.order_as_type,
            im.format_id,
            im.prefix,
            im.store_ord_mult,
            im.contains_inner_ind
       from item_master im , 
            v_item_master_tl imtl
       where im.item = I_item
       and   im.item = imtl.item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER , V_ITEM_MASTER_TL ','ITEM: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER , V_ITEM_MASTER_TL ','ITEM: '||I_item);
   fetch C_ITEM into O_item_desc,
                     O_item_level,
                     O_tran_level,
                     O_status,
                     O_pack_ind,
                     O_dept,
                     O_class,
                     O_subclass,
                     O_sellable_ind,
                     O_orderable_ind,
                     O_pack_type,
                     O_simple_pack_ind,
                     O_waste_type,
                     O_item_parent,
                     O_item_grandparent,
                     O_short_desc,
                     O_waste_pct,
                     O_default_waste_pct,
                     O_item_number_type,
                     O_diff_1,
                     O_diff_2,
                     O_diff_3,
                     O_diff_4,
                     O_order_as_type,
                     O_format_id,
                     O_prefix,
                     O_store_ord_mult,
                     O_contains_inner_ind;

   if C_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER , V_ITEM_MASTER_TL ','ITEM: '||I_item);
      close C_ITEM;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER , V_ITEM_MASTER_TL ','ITEM: '||I_item);
   close C_ITEM;

   --- Get info for diff_1
   if O_diff_1 is NOT NULL then
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                O_diff_1_desc,
                                O_diff_1_type,
                                O_diff_1_id_group_ind,
                                O_diff_1) = FALSE then
         RETURN FALSE;
      end if;
   end if;

   --- Get info for diff_2
   if O_diff_2 is NOT NULL then
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                O_diff_2_desc,
                                O_diff_2_type,
                                O_diff_2_id_group_ind,
                                O_diff_2) = FALSE then
         RETURN FALSE;
      end if;
   end if;

   --- Get info for diff_3
   if O_diff_3 is NOT NULL then
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                O_diff_3_desc,
                                O_diff_3_type,
                                O_diff_3_id_group_ind,
                                O_diff_3) = FALSE then
         RETURN FALSE;
      end if;
   end if;

   --- Get info for diff_4
   if O_diff_4 is NOT NULL then
      if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                                O_diff_4_desc,
                                O_diff_4_type,
                                O_diff_4_id_group_ind,
                                O_diff_4) = FALSE then
         RETURN FALSE;
      end if;
   end if;

   if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                               O_dept,
                               O_dept_name) = FALSE then
      RETURN FALSE;
   end if;

   if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                O_dept,
                                O_class,
                                O_class_name) = FALSE then
      RETURN FALSE;
   end if;

   if SUBCLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   O_dept,
                                   O_class,
                                   O_subclass,
                                   O_subclass_name) = FALSE then
      RETURN FALSE;
   end if;

   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_INFO',
                                            to_char(SQLCODE));
   RETURN FALSE;
END GET_INFO;
---------------------------------------------------------------------------------------------
FUNCTION GET_PACK_INDS (O_error_message  IN OUT VARCHAR2,
                        O_pack_ind       IN OUT ITEM_MASTER.PACK_IND%TYPE,
                        O_sellable_ind   IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                        O_orderable_ind  IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                        O_pack_type      IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

--Dummy variable
L_order_as_type         ITEM_MASTER.ORDER_AS_TYPE%TYPE;

BEGIN

   if GET_PACK_INDS (O_error_message,
                     O_pack_ind,
                     O_sellable_ind,
                     O_orderable_ind,
                     O_pack_type,
                     L_order_as_type,
                     I_item) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_PACK_INDS',
                                            to_char(SQLCODE));
   RETURN FALSE;

END GET_PACK_INDS;
---------------------------------------------------------------------------------------------
FUNCTION GET_PACK_INDS (O_error_message  IN OUT VARCHAR2,
                        O_pack_ind       IN OUT ITEM_MASTER.PACK_IND%TYPE,
                        O_sellable_ind   IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                        O_orderable_ind  IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                        O_pack_type      IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                        O_order_as_type  IN OUT ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_notional_pack_ind      ITEM_MASTER.NOTIONAL_PACK_IND%TYPE;
BEGIN
   if GET_PACK_INDS (O_error_message,
                     O_pack_ind,
                     O_sellable_ind,
                     O_orderable_ind,
                     O_pack_type,
                     O_order_as_type,
                     L_notional_pack_ind,
                     I_item) = FALSE then
         return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_PACK_INDS',
                                            to_char(SQLCODE));
      RETURN FALSE;

END GET_PACK_INDS;
---------------------------------------------------------------------------------------------
FUNCTION GET_PACK_INDS (O_error_message      IN OUT VARCHAR2,
                        O_pack_ind           IN OUT ITEM_MASTER.PACK_IND%TYPE,
                        O_sellable_ind       IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                        O_orderable_ind      IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                        O_pack_type          IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                        O_order_as_type      IN OUT ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                        O_notional_pack_ind  IN OUT ITEM_MASTER.NOTIONAL_PACK_IND%TYPE,
                        I_item               IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_ITEM is
     select pack_ind,
            sellable_ind,
            orderable_ind,
            pack_type,
            order_as_type,
            notional_pack_ind
       from item_master
      where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   fetch C_ITEM into O_pack_ind,
                     O_sellable_ind,
                     O_orderable_ind,
                     O_pack_type,
                     O_order_as_type,
                     O_notional_pack_ind;
   if C_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             null,
                                             null,
                                             null);
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
      close C_ITEM;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   close C_ITEM;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_PACK_INDS',
                                            to_char(SQLCODE));
   RETURN FALSE;
END GET_PACK_INDS;
---------------------------------------------------------------------------------------------
FUNCTION GET_PARENT_INFO  (O_error_message     IN OUT VARCHAR2,
                           O_parent            IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                           O_parent_desc       IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                           O_grandparent       IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                           O_grandparent_desc  IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                           I_item              IN  ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.GET_PARENT_INFO';

  cursor C_DESC IS
     select item_parent, item_grandparent
       from item_master
       where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_DESC','ITEM_MASTER','ITEM: '||I_item);
   open C_DESC;
   SQL_LIB.SET_MARK('FETCH', 'C_DESC','ITEM_MASTER','ITEM: '||I_item);
   fetch C_DESC into O_parent,O_grandparent;
   if O_grandparent IS NOT NULL then
      if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                  O_grandparent_desc,
                                  O_grandparent) = FALSE then
         return FALSE;
      end if;
   end if;
   if O_parent IS NOT NULL then
      if ITEM_ATTRIB_SQL.GET_DESC(O_error_message,
                                  O_parent_desc,
                                  O_parent) = FALSE then
         return FALSE;
      end if;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_DESC','ITEM_MASTER','ITEM: '||I_item);
   close C_DESC;

RETURN TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        NULL);
   RETURN FALSE;
END GET_PARENT_INFO;
---------------------------------------------------------------------------------------------
FUNCTION EXISTS_AS_SUB_ITEM (O_error_message  IN OUT VARCHAR2,
                             O_exists         IN OUT BOOLEAN,
                             I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.EXISTS_AS_SUB_ITEM';
L_dummy      VARCHAR2(1);

cursor C_EXISTS IS
      select 'x'
        from sub_items_detail
       where sub_item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS','SUB_ITEM_DETAIL','Sub: '||I_item);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS','SUB_ITEM_DETAIL','Sub: '||I_item);
   fetch C_EXISTS into L_dummy;
   if C_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS','SUB_ITEM_DETAIL','Sub: '||I_item);
   close C_EXISTS;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        NULL);
   RETURN FALSE;
END EXISTS_AS_SUB_ITEM;
---------------------------------------------------------------------------------------------
FUNCTION UDA_EXISTS (O_error_message  IN OUT VARCHAR2,
                     O_exists         IN OUT BOOLEAN,
                     I_item           IN    ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.UDA_EXISTS';
L_dummy      VARCHAR2(1);

cursor C_LOV_EXISTS IS
      select 'x'
        from uda_item_lov
       where item = I_item
       and ROWNUM = 1;

cursor C_DATE_EXISTS IS
      select 'x'
        from uda_item_date
       where item = I_item
       and ROWNUM = 1;

cursor C_FF_EXISTS IS
      select 'x'
        from uda_item_ff t3
       where item = I_item
       and ROWNUM = 1;


BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN', 'C_LOV_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   open C_LOV_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_LOV_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   fetch C_LOV_EXISTS into L_dummy;
   if C_LOV_EXISTS%FOUND then
      O_exists := TRUE;
      SQL_LIB.SET_MARK('CLOSE', 'C_LOV_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
      close C_LOV_EXISTS;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOV_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   close C_LOV_EXISTS;

   SQL_LIB.SET_MARK('OPEN', 'C_DATE_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   open C_DATE_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_DATE_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   fetch C_DATE_EXISTS into L_dummy;
   if C_DATE_EXISTS%FOUND then
      O_exists := TRUE;
      SQL_LIB.SET_MARK('CLOSE', 'C_DATE_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
      close C_DATE_EXISTS;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_DATE_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   close C_DATE_EXISTS;

   SQL_LIB.SET_MARK('OPEN', 'C_FF_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   open C_FF_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_FF_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   fetch C_FF_EXISTS into L_dummy;

   if C_FF_EXISTS%FOUND then
      O_exists := TRUE;
      SQL_LIB.SET_MARK('CLOSE', 'C_FF_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
      close C_FF_EXISTS;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_FF_EXISTS','UDA_ITEM_LOV','Item: '||I_item);
   close C_FF_EXISTS;

 RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        NULL);
   RETURN FALSE;
END UDA_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION TICKET_EXISTS (O_error_message  IN OUT VARCHAR2,
                        O_exists         IN OUT BOOLEAN,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.TICKET_EXISTS';
L_dummy      VARCHAR2(1);

cursor C_EXISTS IS
      select 'x'
        from item_ticket
       where item = I_item
       and ROWNUM = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS','ITEM_TICKET','Item: '||I_item);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS','ITEM_TICKET','Item: '||I_item);
   fetch C_EXISTS into L_dummy;
   if C_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS','ITEM_TICKET','Item: '||I_item);
   close C_EXISTS;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        NULL);
   RETURN FALSE;
END TICKET_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION GET_DIFFS (O_error_message  IN OUT VARCHAR2,
                    O_diff_1         IN OUT ITEM_MASTER.DIFF_1%TYPE,
                    O_diff_2         IN OUT ITEM_MASTER.DIFF_2%TYPE,
                    O_diff_3         IN OUT ITEM_MASTER.DIFF_3%TYPE,
                    O_diff_4         IN OUT ITEM_MASTER.DIFF_4%TYPE,
                    I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.GET_DIFFS';
L_dummy      VARCHAR2(1);

   cursor C_ITEM is
      select diff_1,
             diff_2,
             diff_3,
             diff_4
        from item_master
       where item = I_item;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER','Item: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER','Item: '||I_item);
   fetch C_ITEM into O_diff_1, O_diff_2, O_diff_3, O_diff_4;
   if C_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             null,
                                             null,
                                             null);
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','Item: '||I_item);
      close C_ITEM;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','Item: '||I_item);
   close C_ITEM;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
   RETURN FALSE;
END GET_DIFFS;
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_DIFF(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_diff_id        IN     DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN IS

L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.VALIDATE_ITEM_DIFF';
L_dummy      VARCHAR2(1);

cursor C_EXISTS IS
      select 'Y'
        from item_master
       where (item_parent = I_item
       or item_grandparent = I_item)
       and (diff_1 = I_diff_id
       or diff_2 = I_diff_id
       or diff_3 = I_diff_id
       or diff_4 = I_diff_id);

BEGIN
   ---
   if I_item is NULL or I_diff_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_item,I_diff_id','NULL','NOT NULL');
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS','ITEM_MASTER','Item: '||I_item);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS','ITEM_MASTER','Item: '||I_item);
   fetch C_EXISTS into L_dummy;
   if C_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_EXITS','ITEM_MASTER','Item: '||I_item);
   close C_EXISTS;

  RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        NULL);
   RETURN FALSE;
END VALIDATE_ITEM_DIFF;
---------------------------------------------------------------------------------------------
FUNCTION EXPENSE_EXISTS(O_error_message   IN OUT VARCHAR2,
                        O_exists          IN OUT BOOLEAN,
                        I_item            IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS

L_exists   VARCHAR2(255) := NULL;

cursor C_ITEM_EXISTS is
   select 'x'
     from item_exp_head
    where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_EXISTS',
                    'ITEM_EXP_HEAD',
                    NULL);
   open C_ITEM_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_EXISTS',
                    'ITEM_EXP_HEAD',
                    NULL);
   fetch C_ITEM_EXISTS into L_exists;

   if L_exists is not NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_EXISTS',
                    'ITEM_EXP_HEAD',
                    NULL);
   close C_ITEM_EXISTS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            NULL,
                                            NULL);

    return FALSE;
END EXPENSE_EXISTS;
---------------------------------------------------------------------------------------------
FUNCTION GET_CONST_DIMEN_IND(O_error_message   IN OUT VARCHAR2,
                             O_const_dim_ind   IN OUT ITEM_MASTER.CONST_DIMEN_IND%TYPE,
                             I_item            IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS

L_program               VARCHAR2(62)   := 'ITEM_ATTRIB_SQL.GET_CONST_DIMEN_IND';


cursor C_GET_CONST_DIMEN is
   select const_dimen_ind
     from item_master
    where item = I_item;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_CONST_DIMEN','ITEM_MASTER',NULL);
   open C_GET_CONST_DIMEN;

   SQL_LIB.SET_MARK('FETCH','C_GET_CONST_DIMEN','ITEM_MASTER',NULL);
   fetch C_GET_CONST_DIMEN into O_const_dim_ind;

   SQL_LIB.SET_MARK('CLOSE','C_GET_CONST_DIMEN','ITEM_MASTER', NULL);
   close C_GET_CONST_DIMEN;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
   RETURN FALSE;
END GET_CONST_DIMEN_IND;
---------------------------------------------------------------------------------------------
-- UNAV_INV_EXISTS - This function will return TRUE if any unavailable inventory records
--                   exist on inv_status_qty for the given item.
---------------------------------------------------------------------------------------------
FUNCTION UNAV_INV_EXISTS(O_error_message   IN OUT VARCHAR2,
                         O_exists          IN OUT BOOLEAN,
                         I_item            IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_ATTRIB_SQL.UNAV_INV_EXISTS';
   L_exists    VARCHAR2(1)  := 'N';

   cursor C_CHECK_UNAV is
      select 'Y'
        from inv_status_qty
       where item = I_item
       and ROWNUM = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_CHECK_UNAV','inv_status_qty',NULL);
   open C_CHECK_UNAV;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_UNAV','inv_status_qty',NULL);
   fetch C_CHECK_UNAV into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_UNAV','inv_status_qty',NULL);
   close C_CHECK_UNAV;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,L_program,NULL);
      return FALSE;
END UNAV_INV_EXISTS;

---------------------------------------------------------------------------------------------
FUNCTION CHECK_SUB_ITEM(O_error_message  IN OUT  VARCHAR2,
                        O_exists         IN OUT  BOOLEAN,
                        O_item_or_sub    IN OUT  VARCHAR2,
                        I_item           IN      SUB_ITEMS_DETAIL.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64) := 'ITEM_ATTRIB_SQL.CHECK_SUB_ITEM';

   cursor C_ITEM is
      select 'M'
        from sub_items_detail
       where item = I_item
       and ROWNUM = 1;

   cursor C_SUB is
      select 'S'
        from sub_items_detail
       where sub_item = I_item
       and ROWNUM = 1;

BEGIN
   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','SUB_ITEMS_DETAIL','Item: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','SUB_ITEMS_DETAIL','Item: '||I_item);
   fetch C_ITEM into O_item_or_sub;
   if C_ITEM%FOUND then
      --- Item contains sub items
      O_exists := TRUE;
   else
      SQL_LIB.SET_MARK('OPEN', 'C_SUB','SUB_ITEMS_DETAIL','Sub: '||I_item);
      open C_SUB;
      SQL_LIB.SET_MARK('FETCH', 'C_SUB','SUB_ITEMS_DETAIL','Sub: '||I_item);
      fetch C_SUB into O_item_or_sub;
      if C_SUB%FOUND then
         --- Item is a sub item
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_SUB','SUB_ITEMS_DETAIL','Sub: '||I_item);
      close C_SUB;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','SUB_ITEMS_DETAIL','Item: '||I_item);
   close C_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        NULL);
      return FALSE;
END CHECK_SUB_ITEM;
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_NUMBER_TYPE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_item_number_type  IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                              I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_TYPE is
      select item_number_type
        from item_master
       where item = I_item;

BEGIN
   open C_TYPE;
   fetch C_TYPE into O_item_number_type;
   close C_TYPE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_ITEM_NUMBER_TYPE',
                                            NULL);
      return FALSE;
END GET_ITEM_NUMBER_TYPE;
---------------------------------------------------------------------------------------------
FUNCTION GET_CHILD_ITEM_NUMBER_TYPE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_item_number_type  IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                                    O_exists            IN OUT BOOLEAN,
                                    I_item_parent       IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_TYPE is
      select item_number_type
        from item_master
       where item_parent = I_item_parent;

BEGIN
   open C_TYPE;
   fetch C_TYPE into O_item_number_type;
   close C_TYPE;

   if O_item_number_type is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_CHILD_ITEM_NUMBER_TYPE',
                                            NULL);
      return FALSE;
END GET_CHILD_ITEM_NUMBER_TYPE;
---------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_REF_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_primary_ref_item  IN OUT ITEM_MASTER.ITEM%TYPE,
                              O_exists            IN OUT BOOLEAN,
                              I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_REF_ITEM is
      select item
        from item_master
       where item_parent = I_item
         and primary_ref_item_ind = 'Y';

BEGIN
   open C_REF_ITEM;
   fetch C_REF_ITEM into O_primary_ref_item;
   close C_REF_ITEM;

   if O_primary_ref_item is NULL then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_PRIMARY_REF_ITEM',
                                            NULL);
      return FALSE;
END GET_PRIMARY_REF_ITEM;
---------------------------------------------------------------------------------------------
FUNCTION OUTSTAND_ORDERS_EXIST(O_error_message IN OUT VARCHAR2,
                               O_exists        IN OUT BOOLEAN,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60)  := 'ITEM_ATTRIB_SQL.OUTSTAND_ORDERS_EXIST';
   L_exists    VARCHAR2(1)   := 'N';

   cursor C_ORDERS_EXIST is
      select 'Y'
        from ordloc l
       where l.item = I_item
         and l.qty_ordered > NVL(l.qty_received,0)
         and rownum = 1
       union
      select /*+ INDEX(l ORDLOC_I1) */
             'Y'
        from ordloc l
       where l.item in (select i.item
                              from item_master i
                             where i.item_parent      = I_item
                                or i.item_grandparent = I_item)
         and l.qty_ordered > NVL(l.qty_received,0)
         and rownum = 1
       union
      select /*+ INDEX(l ORDLOC_I1) */
             'Y'
        from ordloc l,
             packitem_breakout pb
       where (pb.item= I_item
          or pb.item_parent = I_item)
         and l.item = pb.pack_no
         and l.qty_ordered > NVL(l.qty_received,0)
         and rownum = 1;


BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDERS_EXIST','ORDSKU','Item: '||I_item);
   open C_ORDERS_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_ORDERS_EXIST','ORDSKU','Item: '||I_item);
   fetch C_ORDERS_EXIST into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_ORDERS_EXIST','ORDSKU','Item: '||I_item);
   close C_ORDERS_EXIST;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END OUTSTAND_ORDERS_EXIST;
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_IND_ATTRIB(O_error_message            IN OUT VARCHAR2,
                             O_cost_zone_group_id       IN OUT ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE,
                             O_forecast_ind             IN OUT ITEM_MASTER.FORECAST_IND%TYPE,
                             O_merchandise_ind          IN OUT ITEM_MASTER.MERCHANDISE_IND%TYPE,
                             O_retail_label_type        IN OUT ITEM_MASTER.RETAIL_LABEL_TYPE%TYPE,
                             O_retail_label_value       IN OUT ITEM_MASTER.RETAIL_LABEL_VALUE%TYPE,
                             O_handling_temp            IN OUT ITEM_MASTER.HANDLING_TEMP%TYPE,
                             O_handling_sensitivity     IN OUT ITEM_MASTER.HANDLING_SENSITIVITY%TYPE,
                             O_catch_weight_ind         IN OUT ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                             O_waste_type               IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                             O_waste_pct                IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                             O_default_waste_pct        IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                             O_package_size             IN OUT ITEM_MASTER.PACKAGE_SIZE%TYPE,
                             O_package_uom              IN OUT ITEM_MASTER.PACKAGE_UOM%TYPE,
                             O_check_uda_ind            IN OUT ITEM_MASTER.CHECK_UDA_IND%TYPE,
                             O_perishable_ind           IN OUT ITEM_MASTER.PERISHABLE_IND%TYPE,
                             O_comments                 IN OUT ITEM_MASTER.COMMENTS%TYPE,
                             O_brand_name               IN OUT ITEM_MASTER.BRAND_NAME%TYPE,
                             O_product_classification   IN OUT ITEM_MASTER.PRODUCT_CLASSIFICATION%TYPE,
                             I_item                     IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.GET_ITEM_IND_ATTRIB';

   cursor C_ITEM_IND_ATTRIB is
     select cost_zone_group_id,
            forecast_ind,
            merchandise_ind,
            retail_label_type,
            retail_label_value,
            handling_temp,
            handling_sensitivity,
            catch_weight_ind,
            waste_type,
            waste_pct,
            default_waste_pct,
            package_size,
            package_uom,
            check_uda_ind,
            perishable_ind,
            comments,
            brand_name,
            product_classification
       from item_master
      where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM_IND_ATTRIB','ITEM_MASTER','ITEM: '||I_item);
   open C_ITEM_IND_ATTRIB;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM_IND_ATTRIB','ITEM_MASTER','ITEM: '||I_item);
   fetch C_ITEM_IND_ATTRIB into O_cost_zone_group_id,
                                O_forecast_ind,
                                O_merchandise_ind,
                                O_retail_label_type,
                                O_retail_label_value,
                                O_handling_temp,
                                O_handling_sensitivity,
                                O_catch_weight_ind,
                                O_waste_type,
                                O_waste_pct,
                                O_default_waste_pct,
                                O_package_size,
                                O_package_uom,
                                O_check_uda_ind,
                                O_perishable_ind,
                                O_comments,
                                O_brand_name,
                                O_product_classification;

   if C_ITEM_IND_ATTRIB%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_IND_ATTRIB','ITEM_MASTER','ITEM: '||I_item);
      close C_ITEM_IND_ATTRIB;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             null,
                                             null,
                                             null);
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_IND_ATTRIB','ITEM_MASTER','ITEM: '||I_item);
   close C_ITEM_IND_ATTRIB;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);

   if C_ITEM_IND_ATTRIB%ISOPEN then
      close C_ITEM_IND_ATTRIB;
   end if;

   RETURN FALSE;

END GET_ITEM_IND_ATTRIB;
---------------------------------------------------------------------------------------------
FUNCTION GET_SIMPLE_PACK_IND (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_simple_pack_ind  IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                              I_item             IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.GET_SIMPLE_PACK_IND';

   cursor C_GET_SIMPLE_PACK_IND is
     select simple_pack_ind
        from item_master
       where item = I_item;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_SIMPLE_PACK_IND','ITEM_MASTER','item: '||I_item);
   open C_GET_SIMPLE_PACK_IND;
   SQL_LIB.SET_MARK('FETCH','C_GET_SIMPLE_PACK_IND','ITEM_MASTER','item: '||I_item);
   fetch C_GET_SIMPLE_PACK_IND into O_simple_pack_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_SIMPLE_PACK_IND','ITEM_MASTER','item: '||I_item);
   close C_GET_SIMPLE_PACK_IND;

RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             NULL);
      RETURN FALSE;
END GET_SIMPLE_PACK_IND;
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_MASTER (O_error_message         OUT  VARCHAR2,
                          O_item_record           OUT  ITEM_MASTER%ROWTYPE,
                          I_item                  IN   ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   cursor C_ITEM is
     select *
       from item_master
      where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   fetch C_ITEM into O_item_record;
   if C_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',
                                             null,
                                             null,
                                             null);
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
      close C_ITEM;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   close C_ITEM;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_ITEM_MASTER',
                                            to_char(SQLCODE));
   RETURN FALSE;
END GET_ITEM_MASTER;
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_FILL_SHIP_RECEIVED_QTY (O_error_message           IN OUT VARCHAR2,
                                         O_fill_qty_suom           IN OUT TSFDETAIL.FILL_QTY%TYPE,
                                         O_tsf_qty_suom            IN OUT TSFDETAIL.TSF_QTY%TYPE,
                                         O_ship_qty_suom           IN OUT TSFDETAIL.SHIP_QTY%TYPE,
                                         O_received_qty            IN OUT TSFDETAIL.RECEIVED_QTY%TYPE,
                                         O_reconciled_qty          IN OUT TSFDETAIL.RECONCILED_QTY%TYPE,
                                         I_tsf_no                  IN     TSFDETAIL.TSF_NO%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program         VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_TSF_FILL_SHIP_RECEIVED_QTY';
   L_count           NUMBER(5) := 0;
   ---
   cursor c_standard_uom is
      select count(distinct standard_uom)
        from item_master
       where item
          in ( select item
                 from tsfdetail
                where tsf_no     = I_tsf_no);

   cursor c_quantities is
      select sum(fill_qty) fill_qty,
             sum(tsf_qty) tsf_qty,
             sum(ship_qty) ship_qty,
             sum(received_qty) received_qty,
             sum(reconciled_qty) reconciled_qty
        from tsfdetail
       where tsf_no = I_tsf_no;

 BEGIN

   open C_STANDARD_UOM;
   fetch C_STANDARD_UOM into L_count;
   close C_STANDARD_UOM;
   if L_count > 1 then
      O_error_message := SQL_LIB.CREATE_MSG('TOTALS_IF_SAME_SUOM',NULL,NULL,NULL);
	  O_fill_qty_suom  := NULL;
      O_tsf_qty_suom   := NULL;
      O_ship_qty_suom  := NULL;
      O_received_qty   := NULL;
      O_reconciled_qty := NULL;
      return FALSE;
   else
      open C_QUANTITIES;
      fetch C_QUANTITIES into O_fill_qty_suom,
                              O_tsf_qty_suom,
                              O_ship_qty_suom,
                              O_received_qty,
                              O_reconciled_qty;
      close C_QUANTITIES;
   end if;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TSF_FILL_SHIP_RECEIVED_QTY;
-------------------------------------------------------------------------------
FUNCTION GET_MERCH_HIER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_division       IN OUT  DIVISION.DIVISION%TYPE,
                        O_div_name       IN OUT  DIVISION.DIV_NAME%TYPE,
                        O_group_no       IN OUT  GROUPS.GROUP_NO%TYPE,
                        O_group_name     IN OUT  GROUPS.GROUP_NAME%TYPE,
                        O_dept           IN OUT  ITEM_MASTER.DEPT%TYPE,
                        O_dept_name      IN OUT  DEPS.DEPT_NAME%TYPE,
                        O_class          IN OUT  ITEM_MASTER.CLASS%TYPE,
                        O_class_name     IN OUT  CLASS.CLASS_NAME%TYPE,
                        O_subclass       IN OUT  ITEM_MASTER.SUBCLASS%TYPE,
                        O_sub_name       IN OUT  SUBCLASS.SUB_NAME%TYPE,
                        I_item           IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(60) := 'ITEM_ATTRIB_SQL.GET_MERCH_HIER';

   cursor C_GET_MERCH_HIER is
      select dv.division,
             dvtl.div_name,
             gr.group_no,
             grtl.group_name,
             dp.dept,
             dptl.dept_name,
             cl.class,
             cltl.class_name,
             sc.subclass,
             sctl.sub_name
        from division         dv,
             v_division_tl    dvtl,
             groups           gr,
             v_groups_tl      grtl,
             deps             dp,
             v_deps_tl        dptl,
             class            cl,
             v_class_tl       cltl,
             subclass         sc,
             v_subclass_tl    sctl,
             item_master      im
       where im.item     = I_item
         and sc.subclass = im.subclass
         and sc.class    = im.class
         and sc.dept     = im.dept
         and sc.subclass = sctl.subclass
         and sc.class    = sctl.class
         and sc.dept     = sctl.dept
         and cl.class    = im.class
         and cl.dept     = im.dept
         and cl.class    = cltl.class
         and cl.dept     = cltl.dept
         and dp.dept     = im.dept
         and gr.group_no = dp.group_no
         and dp.dept     = dptl.dept
         and gr.group_no = grtl.group_no
         and dv.division = gr.division
         and dv.division = dvtl.division;


BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   open  C_GET_MERCH_HIER;
   fetch C_GET_MERCH_HIER into O_division,
                               O_div_name,
                               O_group_no,
                               O_group_name,
                               O_dept,
                               O_dept_name,
                               O_class,
                               O_class_name,
                               O_subclass,
                               O_sub_name;
   close C_GET_MERCH_HIER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_MERCH_HIER;
-------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT  VARCHAR2,
                  O_item_desc             IN OUT  ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT  ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT  ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT  ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT  ITEM_MASTER.PACK_IND%TYPE,
                  O_division              IN OUT  DIVISION.DIVISION%TYPE,
                  O_div_name              IN OUT  DIVISION.DIV_NAME%TYPE,
                  O_group_no              IN OUT  GROUPS.GROUP_NO%TYPE,
                  O_group_name            IN OUT  GROUPS.GROUP_NAME%TYPE,
                  O_dept                  IN OUT  ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT  DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT  ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT  CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT  ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT  SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT  ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT  ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT  ITEM_MASTER.PACK_TYPE%TYPE,
                  O_item_parent           IN OUT  ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_number_type      IN OUT  ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                  I_item                  IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   --- Dummy variables for the call to the longer GET_INFO function
   L_diff_1               ITEM_MASTER.DIFF_1%TYPE;
   L_diff_1_desc          V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_1_type          V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_1_id_group_ind  V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_diff_2               ITEM_MASTER.DIFF_2%TYPE;
   L_diff_2_desc          V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_2_type          V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_2_id_group_ind  V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_order_as_type        ITEM_MASTER.ORDER_AS_TYPE%TYPE;
   L_format_id            ITEM_MASTER.FORMAT_ID%TYPE;
   L_prefix               ITEM_MASTER.PREFIX%TYPE;
   L_contains_inner_ind   ITEM_MASTER.CONTAINS_INNER_IND%TYPE;
   L_store_ord_mult       ITEM_MASTER.STORE_ORD_MULT%TYPE;
   L_simple_pack_ind      ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
   L_waste_type           ITEM_MASTER.WASTE_TYPE%TYPE;
   L_item_grandparent     ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_short_desc           ITEM_MASTER.SHORT_DESC%TYPE;
   L_waste_pct            ITEM_MASTER.WASTE_PCT%TYPE;
   L_default_waste_pct    ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;
   L_dept                 ITEM_MASTER.DEPT%TYPE;
   L_dept_name            DEPS.DEPT_NAME%TYPE;
   L_class                ITEM_MASTER.CLASS%TYPE;
   L_class_name           CLASS.CLASS_NAME%TYPE;
   L_subclass             ITEM_MASTER.SUBCLASS%TYPE;
   L_sub_name             SUBCLASS.SUB_NAME%TYPE;

BEGIN
   if GET_INFO(O_error_message,
               O_item_desc,
               O_item_level,
               O_tran_level,
               O_status,
               O_pack_ind,
               O_dept,
               O_dept_name,
               O_class,
               O_class_name,
               O_subclass,
               O_subclass_name,
               O_sellable_ind,
               O_orderable_ind,
               O_pack_type,
               L_simple_pack_ind,
               L_waste_type,
               O_item_parent,
               L_item_grandparent,
               L_short_desc,
               L_waste_pct,
               L_default_waste_pct,
               O_item_number_type,
               L_diff_1,
               L_diff_1_desc,
               L_diff_1_type,
               L_diff_1_id_group_ind,
               L_diff_2,
               L_diff_2_desc,
               L_diff_2_type,
               L_diff_2_id_group_ind,
               L_order_as_type,
               L_format_id,
               L_prefix,
               L_store_ord_mult,
               L_contains_inner_ind,
               I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if GET_MERCH_HIER(O_error_message,
                     O_division,
                     O_div_name,
                     O_group_no,
                     O_group_name,
                     L_dept,
                     L_dept_name,
                     L_class,
                     L_class_name,
                     L_subclass,
                     L_sub_name,
                     I_item) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_INFO',
                                            to_char(SQLCODE));
   return FALSE;
END GET_INFO;
-------------------------------------------------------------------------------
FUNCTION GET_API_INFO(O_error_message   IN OUT          VARCHAR2,
                      O_api_item_rec    OUT    NOCOPY   API_ITEM_REC,
                      I_item            IN              ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   cursor C_ITEM is
     select item,
            dept,
            class,
            subclass,
            item_level,
            tran_level,
            status,
            pack_ind,
            sellable_ind,
            orderable_ind,
            pack_type,
            order_as_type,
            standard_uom,
            simple_pack_ind,
            contains_inner_ind,
            cost_zone_group_id,
            item_parent
       from item_master
      where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   open C_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   fetch C_ITEM into O_api_item_rec;

   if C_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM', I_item, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
      close C_ITEM;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','ITEM: '||I_item);
   close C_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_ATTRIB_SQL.GET_API_INFO',
                                            to_char(SQLCODE));
   return FALSE;
END GET_API_INFO;
-------------------------------------------------------------------------------
FUNCTION ITEM_SUBQUERY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subquery          OUT VARCHAR2,
                       I_item_level     IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level     IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_diff_column    IN     VARCHAR2,
                       I_main_tab_item  IN     VARCHAR2,
                       I_item_bind_no   IN     VARCHAR,
                       I_diff_bind_no   IN     VARCHAR2,
                       I_include_parent IN     BOOLEAN)
RETURN BOOLEAN AS

   L_program         VARCHAR2(60) := 'ITEM_ATTRIB_SQL.ITEM_SUBQUERY';
   L_subquery        VARCHAR2(2000);
   L_diff_condition  VARCHAR2(1000);
   L_item_field_name VARCHAR2(20);

BEGIN
   --Create a diff sub query if diff id is passed in.  Only expecting to add
   --on diff ids, not groups, to limit item search.
   if I_diff_column is NOT NULL then
      L_diff_condition := ' im.'||I_diff_column||' = :'||I_diff_bind_no||' ';
   end if;

   if I_tran_level - I_item_level = 2 then
      L_item_field_name := 'im.item_grandparent';
   elsif I_tran_level - I_item_level = 1 then
      L_item_field_name := 'im.item_parent';
   elsif I_tran_level = I_item_level then
      L_item_field_name := 'im.item';
   end if;

   --Build query based on level of item coming in,
   --always selecting tran level items.  Have ability
   --to add on a passed in table column to join to item
   L_subquery := ' select item '||
                   ' from item_master im '||
                  ' where '||L_item_field_name||' = :'||I_item_bind_no;

   if I_main_tab_item is not null then
      L_subquery := L_subquery || ' and im.item = '||I_main_tab_item ||
                    ' and rownum = 1 ';
   end if;

   if L_diff_condition is NOT NULL then
      L_subquery := L_subquery || ' and '||L_diff_condition||' ';
   end if;

   --if include parent is passed in, then the query
   --will return tran level item(s) plus parent and/or
   --grandparent item depending on item level passed in.
   if I_include_parent then
      if I_tran_level - I_item_level = 2 then
         L_subquery := L_subquery ||
          ' union all '||
         ' select item '||
           ' from item_master im '||
          ' where im.item = :'||I_item_bind_no;

         if I_main_tab_item is not null then
            L_subquery := L_subquery || ' and im.item = '||I_main_tab_item ||
                          ' and rownum = 1 ';
         end if;

         L_subquery := L_subquery ||
          ' union all '||
         ' select item '||
           ' from item_master im '||
          ' where im.item_parent = :'||I_item_bind_no;

         if I_main_tab_item is not null then
            L_subquery := L_subquery || ' and im.item = '||I_main_tab_item ||
                          ' and rownum = 1 ';
         end if;

         if L_diff_condition is NOT NULL then
            L_subquery := L_subquery || ' and '||L_diff_condition||' ';
         end if;

      elsif I_tran_level - I_item_level = 1 then
         L_subquery := L_subquery ||
         '  union all '||
         ' select item '||
           ' from item_master im '||
          ' where im.item = :'||I_item_bind_no||' ';

         if I_main_tab_item is not null then
            L_subquery := L_subquery || ' and im.item = '||I_main_tab_item ||
                          ' and rownum = 1 ';
         end if;
      end if;
   end if;

   O_subquery := L_subquery;
   ---

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ITEM_SUBQUERY;
-------------------------------------------------------------------------------
FUNCTION ITEM_SUBQUERY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subquery          OUT VARCHAR2,
                       I_item_level     IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level     IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_diff_column    IN     VARCHAR2,
                       I_item_bind_no   IN     VARCHAR,
                       I_diff_bind_no   IN     VARCHAR2,
                       I_include_parent IN     BOOLEAN)
RETURN BOOLEAN AS

   L_program         VARCHAR2(60) := 'ITEM_ATTRIB_SQL.ITEM_SUBQUERY';
   L_subquery        VARCHAR2(2000);
   L_diff_condition  VARCHAR2(1000);
   L_item_field_name VARCHAR2(20);

BEGIN
   --Create a diff sub query if diff id is passed in.  Only expecting to add
   --on diff ids, not groups, to limit item search.
   if I_diff_column is NOT NULL then
      L_diff_condition := ' im.'||I_diff_column||' = :'||I_diff_bind_no||' ';
   end if;

   if I_tran_level - I_item_level = 2 then
      L_item_field_name := 'im.item_grandparent';
   elsif I_tran_level - I_item_level = 1 then
      L_item_field_name := 'im.item_parent';
   elsif I_tran_level = I_item_level then
      L_item_field_name := 'im.item';
   end if;

   --Build query based on level of item coming in,
   --always selecting tran level items.  Have ability
   --to add on a passed in table column to join to item
   L_subquery := ' select im.item '||
                   ' from item_master im '||
                  ' where ('||L_item_field_name||' = :'||I_item_bind_no;

   --if include parent is passed in, then the query
   --will return tran level item(s) plus parent and/or
   --grandparent item depending on item level passed in.
   if I_include_parent then
      if I_tran_level - I_item_level = 2 then
         L_subquery := L_subquery ||
          ' or im.item = :'||I_item_bind_no ||
          ' or im.item_parent = :'||I_item_bind_no || ')';

      elsif I_tran_level - I_item_level = 1 then
         L_subquery := L_subquery ||
          ' or im.item = :'||I_item_bind_no || ')';
      else
         L_subquery := L_subquery || ')';
      end if;
   else
      L_subquery := L_subquery || ')';
   end if;

   if L_diff_condition is NOT NULL then
      L_subquery := L_subquery || ' and '||L_diff_condition||' ';
   end if;

   O_subquery := L_subquery;
   ---

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ITEM_SUBQUERY;
-------------------------------------------------------------------------------
FUNCTION PACK_SUBQUERY(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subquery       OUT      VARCHAR2,
                       I_isc_exist      IN       BOOLEAN,
                       I_xpack_count    IN       NUMBER)

RETURN BOOLEAN AS

   L_program  VARCHAR2(60) := 'ITEM_ATTRIB_SQL.PACK_SUBQUERY';

BEGIN
    O_subquery := ' select /*+ cardinality(xpack ' || I_xpack_count || ') */ ' ||
                         ' distinct p.pack_no '||
                        ' from packitem p, '||
                        ' item_master im, '||
                        ' TABLE(cast(:1 as ITEM_TBL))xpack '||
                  ' where p.pack_no = im.item '||
                    ' and im.pack_type = ''B'' '||
                    ' and im.orderable_ind = ''Y'' '||
                    ' and p.item = value(xpack) ';

   if I_isc_exist then
      O_subquery := O_subquery || ' and exists (select ''x'' '||
                                  '               from item_supp_country sc '||
                                  '              where sc.item = p.pack_no '||
                                  '                and sc.supplier = :2 '||
                                  '                and sc.origin_country_id = :3 '||
                                  '                and rownum = 1) ';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END PACK_SUBQUERY;
-------------------------------------------------------------------------------
FUNCTION ITEM_DIFF_EXISTS(O_error_message   IN OUT   VARCHAR2,
                          O_exists          OUT      BOOLEAN,
                          O_diff_column     OUT      VARCHAR2,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE,
                          I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.VALIDATE_ITEM_DIFF';
   L_diff_1     ITEM_MASTER.DIFF_1%TYPE;
   L_diff_2     ITEM_MASTER.DIFF_2%TYPE;
   L_diff_3     ITEM_MASTER.DIFF_3%TYPE;
   L_diff_4     ITEM_MASTER.DIFF_4%TYPE;

   cursor C_EXISTS IS
      select diff_1,
             diff_2,
             diff_3,
             diff_4
        from item_master
       where (item_parent      = I_item
          or  item_grandparent = I_item)
         and (diff_1 = I_diff_id
          or  diff_2 = I_diff_id
          or  diff_3 = I_diff_id
          or  diff_4 = I_diff_id);

BEGIN
   ---
   if I_item is NULL or I_diff_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_item,I_diff_id','NULL','NOT NULL');
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS','ITEM_MASTER','Item: '||I_item);
   open C_EXISTS;

   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS','ITEM_MASTER','Item: '||I_item);
   fetch C_EXISTS into L_diff_1,
                       L_diff_2,
                       L_diff_3,
                       L_diff_4;

   if C_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_EXITS','ITEM_MASTER','Item: '||I_item);
   close C_EXISTS;

   if L_diff_1 = I_diff_id then
      O_diff_column := 'diff_1';
   elsif L_diff_2 = I_diff_id then
      O_diff_column := 'diff_2';
   elsif L_diff_3 = I_diff_id then
      O_diff_column := 'diff_3';
   elsif L_diff_4 = I_diff_id then
      O_diff_column := 'diff_4';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         NULL);
   RETURN FALSE;
END ITEM_DIFF_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PACKS_FOR_ITEM(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                            O_pack_items      OUT    NOCOPY   ITEM_TBL,
                            I_item            IN              ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_ATTRIB_SQL.GET_PACKS_FOR_ITEM';

   cursor C_PACKS is
      select a.pack_no
        from packitem_breakout a
       where not exists (select b.pack_no
                           from packitem_breakout b,
                                item_master im2
                          where im2.item = b.item
                            and (NVL(im2.item_grandparent, '-999') != I_item)
                            and (NVL(im2.item_parent, '-999') != I_item)
                            and (NVL(im2.item, '-999') != I_item)
                            and b.pack_no = a.pack_no)
       group by a.pack_no;

BEGIN
   O_pack_items := ITEM_TBL();

   FOR rec in C_PACKS LOOP

      O_pack_items.extend();
      O_pack_items(O_pack_items.COUNT) := rec.pack_no;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PACKS_FOR_ITEM;
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_API_ITEM_TEMP(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                IN              ITEM_MASTER.ITEM%TYPE,
                             I_diff_id             IN              DIFF_IDS.DIFF_ID%TYPE,
                             I_tran_level          IN              ITEM_MASTER.TRAN_LEVEL%TYPE,
                             I_item_level          IN              ITEM_MASTER.ITEM_LEVEL%TYPE,
                             I_diff_column         IN              DIFF_IDS.DIFF_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'ITEM_ATTRIB_SQL.BUILD_API_ITEM_TEMP';
   L_subquery  VARCHAR2(2000);

   L_item_bind_no   VARCHAR2(1) := '1';
   L_diff_bind_no   VARCHAR2(1) := '2';

BEGIN

   L_subquery := 'insert into api_item_temp ' ||
                 ' select item '||
                   ' from item_master im '||
                  ' where im.item = :'||L_item_bind_no;

   if I_tran_level - I_item_level = 2 then
      L_subquery := L_subquery ||
                    ' union all '||
                    ' select item '||
                      ' from item_master im '||
                     ' where im.item_parent = :'||L_item_bind_no ||
                   '  union all '||
                    ' select item '||
                      ' from item_master im '||
                     ' where im.item_grandparent = :'||L_item_bind_no;

   elsif I_tran_level - I_item_level = 1 then
      L_subquery := L_subquery ||
                    ' union all '||
                    ' select item '||
                      ' from item_master im '||
                     ' where im.item_parent = :'||L_item_bind_no;
   end if;

   if I_diff_id is NOT NULL then
      L_subquery := L_subquery ||
                   ' and (' || sys.DBMS_ASSERT.SIMPLE_SQL_NAME(I_diff_column ) || ' = :' || L_diff_bind_no ||
                    ' or exists (select ''x''' ||
                                 ' from diff_group_detail' ||
                                ' where diff_group_id = :' || L_diff_bind_no ||
                                  ' and diff_id = im.' || sys.DBMS_ASSERT.SIMPLE_SQL_NAME(I_diff_column )  ||'))';

      if I_tran_level - I_item_level = 2 then
         EXECUTE IMMEDIATE L_subquery
            USING I_item,    -- Item
                  I_item,    -- Item Parent
                  I_item,    -- Item Grandparent
                  I_diff_id, -- Diff ID
                  I_diff_id; -- Diff Group ID

      elsif I_tran_level - I_item_level = 1 then
         EXECUTE IMMEDIATE L_subquery
            USING I_item,    -- Item
                  I_item,    -- Item Parent
                  I_diff_id, -- Diff ID
                  I_diff_id; -- Diff Group ID
      else
         EXECUTE IMMEDIATE L_subquery
            USING I_item,    -- Item
                  I_diff_id, -- Diff ID
                  I_diff_id; -- Diff Group ID
      end if;

   else
      if I_tran_level - I_item_level = 2 then
         EXECUTE IMMEDIATE L_subquery
            USING I_item,    -- Item
                  I_item,    -- Item Parent
                  I_item;    -- Item Grandparent

      elsif I_tran_level - I_item_level = 1 then
         EXECUTE IMMEDIATE L_subquery
            USING I_item,    -- Item
                  I_item;    -- Item Parent

      else
         EXECUTE IMMEDIATE L_subquery
            USING I_item;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_API_ITEM_TEMP;

-------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_TYPE (O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_item_type_code         OUT  VARCHAR2                ,
                        I_item                IN      ITEM_MASTER.ITEM%TYPE    )
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'ITEM_ATTRIB_SQL.GET_ITEM_TYPE';


   L_item   ITEM_MASTER%ROWTYPE ;

BEGIN
   if GET_ITEM_MASTER (O_error_message   ,
                       L_item            ,
                       I_item            ) = FALSE then
      return FALSE;
   end if;
   if L_item.sellable_ind       IN ('Y','N')
      and L_item.orderable_ind      = 'Y'
      and L_item.inventory_ind      = 'Y'
      and L_item.pack_ind           = 'Y'
      and L_item.simple_pack_ind    = 'Y'
      and L_item.item_xform_ind     = 'N'
      and L_item.deposit_item_type  IS NULL then
      O_item_type_code := 'S' ;
   elsif  L_item.sellable_ind       IN ('Y','N')
      and L_item.orderable_ind      IN ('Y','N')
      and L_item.inventory_ind      = 'Y'
      and L_item.pack_ind           = 'Y'
      and L_item.simple_pack_ind    = 'N'
      and L_item.item_xform_ind     = 'N'
      and (   L_item.deposit_item_type  IS NULL
           or L_item.deposit_item_type  = 'P') then
      O_item_type_code := 'C' ;
   elsif  L_item.sellable_ind       = 'Y'
      and L_item.orderable_ind      = 'Y'
      and L_item.inventory_ind      = 'Y'
      and L_item.pack_ind           = 'N'
      and L_item.simple_pack_ind    = 'N'
      and L_item.item_xform_ind     = 'N'
      and L_item.deposit_item_type  = 'E' then
      O_item_type_code := 'E' ;
   elsif  L_item.sellable_ind       = 'Y'
      and L_item.orderable_ind      = 'N'
      and L_item.inventory_ind      = 'N'
      and L_item.pack_ind           = 'N'
      and L_item.simple_pack_ind    = 'N'
      and L_item.item_xform_ind     = 'N'
      and L_item.deposit_item_type  = 'A' then
      O_item_type_code := 'A' ;
   elsif  L_item.sellable_ind       IN ('Y','N')
      and L_item.orderable_ind      = 'Y'
      and L_item.inventory_ind      = 'Y'
      and L_item.pack_ind           = 'N'
      and L_item.simple_pack_ind    = 'N'
      and L_item.item_xform_ind     = 'N'
      and L_item.deposit_item_type  = 'Z' then
      O_item_type_code := 'Z' ;
   elsif  L_item.sellable_ind       = 'Y'
      and L_item.orderable_ind      = 'N'
      and L_item.inventory_ind      = 'Y'
      and L_item.pack_ind           = 'N'
      and L_item.simple_pack_ind    = 'N'
      and L_item.item_xform_ind     = 'N'
      and L_item.deposit_item_type  = 'T' then
      O_item_type_code := 'T' ;
   elsif  L_item.sellable_ind       = 'Y'
      and L_item.orderable_ind      = 'N'
      and L_item.inventory_ind      = 'N'
      and L_item.pack_ind           = 'N'
      and L_item.simple_pack_ind    = 'N'
      and L_item.item_xform_ind     = 'N'
      and L_item.deposit_item_type  IS NULL then
      O_item_type_code := 'I' ;
   elsif  L_item.sellable_ind       = 'N'
      and L_item.orderable_ind      = 'Y'
      and L_item.inventory_ind      = 'Y'
      and L_item.pack_ind           = 'N'
      and L_item.simple_pack_ind    = 'Y'
      and L_item.item_xform_ind     = 'N'
      and L_item.deposit_item_type  IS NULL then
      O_item_type_code := 'O' ;
   elsif  L_item.sellable_ind       = 'Y'
      and L_item.orderable_ind      = 'N'
      and L_item.inventory_ind      = 'N'
      and L_item.pack_ind           = 'N'
      and L_item.simple_pack_ind    = 'N'
      and L_item.item_xform_ind     = 'Y'
      and L_item.deposit_item_type  IS NULL then
      O_item_type_code := 'L' ;
   elsif  L_item.sellable_ind       IN ('Y','N')
      and L_item.orderable_ind      IN ('Y','N')
      and L_item.inventory_ind      IN ('Y','N')
      and L_item.pack_ind           = 'N'
      and L_item.simple_pack_ind    = 'N'
      and L_item.item_xform_ind     = 'N'
      and L_item.deposit_item_type  IS NULL then
      O_item_type_code := 'R' ;
   else
      O_item_type_code := NULL ;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_ITEM_TYPE;
-------------------------------------------------------------------------------------------------------
FUNCTION NEXT_EAN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_ean13           IN OUT   ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(25) := 'ITEM_ATTRIB_SQL.NEXT_EAN';
   L_ean_nextval            NUMBER(6);
   L_check_digit            NUMBER;
   L_valid_ean              BOOLEAN;
   L_item_record            ITEM_MASTER%ROWTYPE;
   L_system_options         SYSTEM_OPTIONS%ROWTYPE;
   L_wrap_sequence_number   ORDHEAD.ORDER_NO%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_restart                  BOOLEAN := TRUE;

   L_found                  varchar2(1);

   CURSOR C_CHK_ITEM ( c_ean IN item_master.item%TYPE ) IS
   select 'x'
     from item_master
    where item = c_ean
    union
   select 'x'
     from svc_item_reservation
    where item = c_ean;


BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   ---
   while L_restart LOOP
      O_ean13 := L_system_options.auto_ean13_prefix * 1000000;

      select EAN13_SEQ.NEXTVAL
        into L_ean_nextval
        from dual;

      if (L_first_time = 'Yes') then
         L_wrap_sequence_number := L_ean_nextval;
         L_first_time := 'No';
      elsif (L_ean_nextval = L_wrap_sequence_number) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      ---

      O_ean13 := O_ean13 + L_ean_nextval;

      if length(O_ean13) < 12 then
         O_ean13 := lpad(O_ean13,12,'0');
      end if;

      FOR dig in 0..9 LOOP

         O_ean13 := substr(O_ean13, 1, 12) || dig;

         if ITEM_NUMBER_TYPE_SQL.VALIDATE_FORMAT(O_error_message,
                                                 O_ean13,
                                                 'EAN13') then
            open C_CHK_ITEM ( O_ean13 );
            fetch C_CHK_ITEM into L_found;
            if C_CHK_ITEM%NOTFOUND then
               CLOSE C_CHK_ITEM;
               L_restart := FALSE;
               EXIT;
            else
               L_restart := TRUE;
            end if;
            close c_chk_item;

          end if;
      END LOOP;
   END LOOP;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_EAN;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_EAN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_parent_item     IN       ITEM_MASTER.ITEM%TYPE,
                    I_supplier        IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(26) := 'ITEM_ATTRIB_SQL.DELETE_EAN';
   L_items         ITEM_TBL     := ITEM_TBL();
   L_table         VARCHAR2(25);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_APPROVAL_ERROR is
      select 'x'
        from item_approval_error iae
       where exists (select 'x'
                       from item_master im
                      where im.item_number_type = 'EAN13'
                        and im.item_parent      = I_parent_item
                        and im.item             = iae.item
                        and rownum              = 1)
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPPLIER is
      select 'x'
        from item_supplier isu
       where (    isu.supplier = I_supplier
               or I_supplier is null )
         and exists (select 'x'
                       from item_master im
                      where im.item_number_type = 'EAN13'
                        and im.item_parent      = I_parent_item
                        and im.item             = isu.item
                        and rownum              = 1)
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPPLIER_TL is
      select 'x'
      from  item_supplier_tl isutl
      where (    isutl.supplier = I_supplier
              or I_supplier is null )
       and  exists (select 'x'
                    from item_master im
                    where im.item_number_type = 'EAN13'
                      and im.item_parent      = I_parent_item
                      and im.item             = isutl.item
                      and rownum              = 1)
       for update nowait;
   ---
   cursor C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master im
       where im.item_parent      = I_parent_item
         and im.item_number_type = 'EAN13'
         and im.item_level       = 3
         for update nowait;
   ---
   cursor C_LOCK_ITEM_MASTER_TL is
      select 'x'
        from item_master_tl imtl
       where imtl.item in (select item
                           from   item_master im
                           where im.item_parent      = I_parent_item
                           and   im.item_number_type = 'EAN13'
                           and   im.item_level       = 3)
         for update nowait;
   ---
BEGIN

   if I_parent_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_parent_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_APPROVAL_ERROR';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_APPROVAL_ERROR',
                    L_table,
                    'I_parent_item '||I_parent_item);
   open C_LOCK_ITEM_APPROVAL_ERROR;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_APPROVAL_ERROR',
                    L_table,
                   'I_parent_item '||I_parent_item);
   close C_LOCK_ITEM_APPROVAL_ERROR;
   ---
   L_table := 'ITEM_SUPPLIER_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_SUPPLIER_TL',
                    L_table,
                    'I_supplier '||I_supplier);
   open C_LOCK_ITEM_SUPPLIER_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_SUPPLIER_TL',
                    L_table,
                    'I_supplier '||I_supplier);
   close C_LOCK_ITEM_SUPPLIER_TL;
   ---
   L_table := 'ITEM_SUPPLIER';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_SUPPLIER',
                    L_table,
                    'I_supplier '||I_supplier);
   open C_LOCK_ITEM_SUPPLIER;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_SUPPLIER',
                    L_table,
                    'I_supplier '||I_supplier);
   close C_LOCK_ITEM_SUPPLIER;
   ---
    L_table := 'ITEM_MASTER_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_MASTER_TL',
                    L_table,
                    'I_parent_item '||I_parent_item);
   open C_LOCK_ITEM_MASTER_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_MASTER_TL',
                    L_table,
                    'I_parent_item '||I_parent_item);
   close C_LOCK_ITEM_MASTER_TL;
   ---
   L_table := 'ITEM_MASTER';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_MASTER',
                    L_table,
                    'I_parent_item '||I_parent_item);
   open C_LOCK_ITEM_MASTER;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_MASTER',
                    L_table,
                    'I_parent_item '||I_parent_item);
   close C_LOCK_ITEM_MASTER;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'item_approval_error',
                    'I_parent_item '||I_parent_item);
   delete from item_approval_error iae
    where exists (select 'x'
                    from item_master im
                   where im.item_number_type = 'EAN13'
                     and im.item_parent      = I_parent_item
                     and im.item             = iae.item
                     and rownum              = 1);
   ---
   --if I_supplier is not null then
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'item_supplier_tl',
                    'I_supplier '||I_supplier);
   delete from item_supplier_tl isutl
    where (   isutl.supplier = I_supplier
           or I_supplier is null )
      and exists (select 'x'
                    from item_master m2
                   where m2.item_number_type = 'EAN13'
                     and m2.item_parent      = I_parent_item
                     and m2.item             = isutl.item
                     and rownum              = 1);
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'item_supplier',
                    'I_supplier '||I_supplier);
   delete from item_supplier isu
    where (   isu.supplier = I_supplier
           or I_supplier is null )
      and exists (select 'x'
                    from item_master m2
                   where m2.item_number_type = 'EAN13'
                     and m2.item_parent      = I_parent_item
                     and m2.item             = isu.item
                     and rownum              = 1);
   --end if;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'item_master_tl',
                    'I_parent_item '||I_parent_item);
   delete from item_master_tl imtl
    where imtl.item in (select item from item_master im
                         where im.item_parent      = I_parent_item
                           and im.item_number_type = 'EAN13'
                           and im.item_level       = 3);
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'item_master',
                    'I_parent_item '||I_parent_item);
   delete from item_master m
    where m.item_parent      = I_parent_item
      and m.item_number_type = 'EAN13'
      and m.item_level       = 3;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_EAN;
--------------------------------------------------------------------------------
FUNCTION GET_FIRST_EAN(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists             IN OUT   BOOLEAN,
                       O_item               IN OUT   ITEM_MASTER.ITEM%TYPE,
                       O_item_number_type   IN OUT   ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                       I_item_parent        IN       ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_FIRST_EAN';

   cursor C_GET_INFO is
      select im.item,
             im.item_number_type
        from item_master im
       where im.item = (select MIN(im1.item)
                          from item_master im1
                         where im1.item_parent = I_item_parent
                           and im1.item_number_type in ('A-EAN','EAN13')
                           and im1.item_level  = 3);

BEGIN
   --- Check required input parameters
   if I_item_parent is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_parent',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   --- Initialize the output variables
   O_item             := NULL;
   O_item_number_type := NULL;

   --- Get the required information
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_INFO',
                    'item_master',
                    'I_item_parent: '||I_item_parent);
   open C_GET_INFO;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_INFO',
                    'item_master',
                    'I_item_parent: '||I_item_parent);
   fetch C_GET_INFO into O_item,
                         O_item_number_type;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_INFO',
                    'item_master',
                    'I_item_parent: '||I_item_parent);
   close C_GET_INFO;

   O_exists := (O_item is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END GET_FIRST_EAN;


-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_EAN_TABLE(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                            O_item_master         IN OUT NOCOPY   GV_ean_table_t,
                            I_item_parent         IN              ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'ITEM_ATTRIB_SQL.POPULATE_EAN_TABLE';


   cursor C_GET_ITEM_MASTER is
   select item
     from item_master      iem
    where item_grandparent = I_item_parent
      and item_number_type in ('EAN13','A-EAN');


BEGIN
   -- This function will only return parent level items.

   O_item_master := NULL;
   if I_item_parent is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_item_parent','NULL','NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_GET_ITEM_MASTER','ITEM_MASTER','Item Parent: '||I_item_parent);
   open C_GET_ITEM_MASTER;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_ITEM_MASTER','ITEM_MASTER','Item Parent: '||I_item_parent);
   fetch C_GET_ITEM_MASTER BULK COLLECT into O_item_master;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_ITEM_MASTER','ITEM_MASTER','Item Parent: '||I_item_parent);
   close C_GET_ITEM_MASTER;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_EAN_TABLE;

-------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEMCHILDRENDIFF_INFO(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_on_daily_purge     IN OUT  BOOLEAN,
                                   O_item_desc          IN OUT  ITEM_MASTER.ITEM_DESC%TYPE,
                                   O_unit_cost          IN OUT  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                   O_retail_price       IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                                   O_unit_cost_curr     IN OUT  SUPS.CURRENCY_CODE%TYPE,
                                   O_unit_retail_curr   IN OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                                   O_markup_percent     IN OUT  NUMBER,
                                   O_existing_vpn       IN OUT  ITEM_SUPPLIER.VPN%TYPE,
                                   O_diff_value         IN OUT  V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                                   O_ean                IN OUT  ITEM_MASTER.ITEM%TYPE,
                                   O_existing_ean_type  IN OUT  ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                                   O_supp_diff_1        IN OUT  ITEM_SUPPLIER.SUPP_DIFF_1%TYPE,
                                   O_supp_diff_2        IN OUT  ITEM_SUPPLIER.SUPP_DIFF_2%TYPE,
                                   O_supp_diff_3        IN OUT  ITEM_SUPPLIER.SUPP_DIFF_3%TYPE,
                                   O_supp_diff_4        IN OUT  ITEM_SUPPLIER.SUPP_DIFF_4%TYPE,
                                   I_item               IN      ITEM_MASTER.ITEM%TYPE,
                                   I_supplier           IN      SUPS.SUPPLIER%TYPE,
                                   I_retail_zone_id     IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE,
                                   I_dept               IN      DEPS.DEPT%TYPE,
                                   I_diff_id            IN      V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                                   I_elc_ind            IN      SYSTEM_OPTIONS.ELC_IND%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(45) := 'ITEM_ATTRIB_SQL.GET_ITEMCHILDRENDIFF_INFO';
   L_item_master_row           ITEM_MASTER%ROWTYPE ;
   L_origin_country_id         ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_unit_cost_prm             ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_unit_retail_prm           ITEM_LOC.UNIT_RETAIL%TYPE;
   L_markup_calc_type          DEPS.MARKUP_CALC_TYPE%TYPE;
   L_supplier                  SUPS.SUPPLIER%TYPE;

   L_item_has_suppliers        BOOLEAN;
   L_item_has_primary_country  BOOLEAN;
   L_primary_ref_ean_exists    BOOLEAN := TRUE;

   L_dummy_varchar             VARCHAR2(200);
   L_dummy_number              NUMBER(20,4);
   L_dummy_date                DATE;
   L_dummy_boolean             BOOLEAN;
   L_item_zone_price_ind       VARCHAR2(1) := 'N';
   L_item_cost_info_rec        ITEM_COST_SQL.ITEM_COST_INFO_REC;
   L_default_po_cost           COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;
   L_system_options_rec        SYSTEM_OPTIONS%ROWTYPE;

   L_exp_currency              CURRENCIES.CURRENCY_CODE%TYPE       := NULL;
   L_exchange_rate_exp         CURRENCY_RATES.EXCHANGE_RATE%TYPE   := NULL;
   L_dty_currency              CURRENCIES.CURRENCY_CODE%TYPE       := NULL;
   L_total_exp                 ORDLOC_EXP.EST_EXP_VALUE%TYPE       := NULL;
   L_total_dty                 ORDLOC_EXP.EST_EXP_VALUE%TYPE       := NULL;

   L_cost_zone_id              COST_ZONE.ZONE_ID%TYPE;
   L_cost_zone_desc            COST_ZONE.DESCRIPTION%TYPE;

   cursor C_ITEM_ZONE_PRICE_IND is
      select 'Y'
        from RPM_ITEM_ZONE_PRICE
       where item = I_item
         and rownum = 1;

   cursor C_GET_DEFAULT_PO_COST is
      select default_po_cost
        from country_attrib
       where country_id = L_item_cost_info_rec.delivery_country_id;

BEGIN

   if GET_ITEM_MASTER(O_error_message,
                      L_item_master_row,
                      I_item) = FALSE then
      return FALSE;
   end if;

   O_item_desc := L_item_master_row.item_desc;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;

   --- Check if item or its parent/grandparent is on daily purge
   if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                    O_on_daily_purge,
                                                    I_item,
                                                    'ITEM_MASTER')= FALSE then
      return FALSE;
   end if;

   if DIFF_SQL.GET_DIFF_INFO(O_error_message,
                             O_diff_value,
                             L_dummy_varchar,
                             L_dummy_varchar,
                             I_diff_id) = FALSE then
      return FALSE;
   end if;

   if SUPPLIER_EXISTS(O_error_message,
                      L_item_has_suppliers,
                      I_item )= FALSE then
      return FALSE;
   end if;

   if L_item_has_suppliers then
      --
      if I_supplier is null then
         if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP(O_error_message,
                                                  L_dummy_boolean,
                                                  L_supplier  ,
                                                  I_item) = FALSE then
            return FALSE;
         end if;
      else
         L_supplier := I_supplier;
      end if;
      --
      if SUPP_ITEM_ATTRIB_SQL.GET_INFO(O_error_message,
                                       L_dummy_boolean,
                                       L_dummy_varchar,
                                       O_existing_vpn,
                                       L_dummy_varchar,
                                       L_dummy_number,
                                       O_supp_diff_1,
                                       O_supp_diff_2,
                                       O_supp_diff_3,
                                       O_supp_diff_4,
                                       L_dummy_varchar,
                                       L_dummy_varchar,
                                       L_dummy_varchar,
                                       L_dummy_date,
                                       L_dummy_number,
                                       I_item,
                                       L_supplier) = FALSE then
         return FALSE;
      end if;
      --
   end if; -- L_item_has_suppliers

   -----------------------------------------------------------------------------
   -- Logic below this point does not apply to reference items
   -- (item_level > tran_level)
   -----------------------------------------------------------------------------
   if L_item_master_row.item_level <= L_item_master_row.tran_level then

      if L_item_has_suppliers then
         if ITEM_SUPP_COUNTRY_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                      L_item_has_primary_country,
                                                      L_origin_country_id,
                                                      I_item,
                                                      L_supplier)  = FALSE then
             return FALSE;
         end if;
         --
         if L_item_has_primary_country then
            if L_system_options_rec.default_tax_type in ('SALES','SVAT') then
               if ITEM_SUPP_COUNTRY_SQL.GET_UNIT_COST(O_error_message,
                                                      O_unit_cost,
                                                      I_item,
                                                      L_supplier,
                                                      L_origin_country_id)  = FALSE then
                  return FALSE;
               end if;
            elsif L_system_options_rec.default_tax_type = 'GTAX' then
               if ITEM_COST_SQL.GET_COST_INFO(O_error_message,
                                              L_item_cost_info_rec,
                                              I_item,
                                              L_supplier,
                                              L_origin_country_id) = FALSE then
                 return FALSE;
               end if;

               SQL_LIB.SET_MARK('OPEN', 'C_GET_DEFAULT_PO_COST','COUNTRY_ATTRIB','Country: '||L_item_cost_info_rec.delivery_country_id);
               open  C_GET_DEFAULT_PO_COST;
               SQL_LIB.SET_MARK('FETCH', 'C_GET_DEFAULT_PO_COST','COUNTRY_ATTRIB','Country: '||L_item_cost_info_rec.delivery_country_id);
               fetch C_GET_DEFAULT_PO_COST into L_default_po_cost;
               SQL_LIB.SET_MARK('CLOSE', 'C_GET_DEFAULT_PO_COST','COUNTRY_ATTRIB','Country: '||L_item_cost_info_rec.delivery_country_id);
               close C_GET_DEFAULT_PO_COST;
               if L_default_po_cost = 'NIC' then
                  O_unit_cost := L_item_cost_info_rec.negotiated_item_cost;
               elsif L_default_po_cost = 'BC' then
                  O_unit_cost := L_item_cost_info_rec.base_cost;
               end if;
            end if;

            --
            if COST_ZONE_SQL.GET_FIRST_COST_ZONE_ID(O_error_message,
                                                    L_cost_zone_id,
                                                    L_cost_zone_desc,
                                                    L_item_master_row.cost_zone_group_id,
                                                    NULL) = FALSE then

               return FALSE;
            end if;

            if I_elc_ind = 'Y' then
               if ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                           O_unit_cost,
                                           L_total_exp,
                                           L_exp_currency,
                                           L_exchange_rate_exp,
                                           L_total_dty,
                                           L_dty_currency,
                                           NULL,
                                           I_item,
                                           NULL,
                                           L_cost_zone_id,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL,
                                           NULL) = FALSE then
                  return FALSE;
               end if;
            end if; -- I_elc_ind = 'Y'
         end if; -- L_item_has_primary_country
         --
         if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                              O_unit_cost_curr,
                                              L_supplier) = FALSE then
            return FALSE;
         end if;

         if L_exp_currency is not null then
            if not CURRENCY_SQL.CONVERT(O_error_message,
                                        O_unit_cost,
                                        L_exp_currency,
                                        O_unit_cost_curr,
                                        O_unit_cost,
                                        'C',
                                        NULL,
                                        NULL)then
               return FALSE;
            end if;
         end if;

        if not CURRENCY_SQL.CONVERT(O_error_message,
                                    O_unit_cost,
                                    O_unit_cost_curr,
                                    NULL,
                                    L_unit_cost_prm,
                                    'C',
                                    NULL,
                                    NULL) then
           return FALSE;
        end if;
        --
      end if; -- L_item_has_suppliers

      open  C_ITEM_ZONE_PRICE_IND;
      fetch C_ITEM_ZONE_PRICE_IND into L_item_zone_price_ind;
      close C_ITEM_ZONE_PRICE_IND;

      if L_item_master_row.sellable_ind = 'Y' then
         if I_retail_zone_id is not null and L_item_zone_price_ind = 'Y' then
            -- Populate the retail price and currency
            if PM_RETAIL_API_SQL.GET_RPM_ITEM_ZONE_PRC_WRAPPER(O_error_message,
                                                               I_item,
                                                               I_retail_zone_id,
                                                               O_retail_price,
                                                               O_unit_retail_curr,
                                                               L_dummy_varchar,
                                                               L_dummy_number,
                                                               L_dummy_varchar,
                                                               L_dummy_varchar,
                                                               L_dummy_number,
                                                               L_dummy_number,
                                                               L_dummy_varchar,
                                                               L_dummy_varchar) = FALSE then
               return FALSE;
            end if;
            --
            if not CURRENCY_SQL.CONVERT(O_error_message,
                                        O_retail_price,
                                        O_unit_retail_curr,
                                        NULL,
                                        L_unit_retail_prm,
                                        'R',
                                        NULL,
                                        NULL)then
               return FALSE;
            end if;
            --
         else
            if PRICING_ATTRIB_SQL.GET_BASE_ZONE_RETAIL(O_error_message,
                                                       O_retail_price,
                                                       L_dummy_varchar,
                                                       L_dummy_number,
                                                       L_dummy_varchar,
                                                       L_dummy_number,
                                                       L_dummy_number,
                                                       L_dummy_varchar,
                                                       I_item) = FALSE then
               return FALSE;
            end if;
            L_unit_retail_prm := O_retail_price;
         end if; -- I_zone_group_id is not null and I_retail_zone_id is not null

         if DEPT_ATTRIB_SQL.GET_MARKUP(O_error_message,
                                       L_markup_calc_type,
                                       L_dummy_number,
                                       L_dummy_number,
                                       I_dept) = FALSE then
            return FALSE;
         end if;
         ---
         -- Compute the markup percent if the system_options.default_tax_type is not GTAX
         -- This avoid a call to the external tax engine as to not degrade performance
         -- on the itemchildrendiff form.
         if L_system_options_rec.default_tax_type != 'GTAX' then
            if MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM(O_error_message,
                                                   O_markup_percent,
                                                   'Y',
                                                   L_markup_calc_type,
                                                   L_unit_cost_prm,
                                                   L_unit_retail_prm,
                                                   I_item,
                                                   NULL,
                                                   'Z',
                                                   I_retail_zone_id,
                                                   NULL,
                                                   NULL,
                                                   NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      end if; -- L_item_master_row.sellable_ind = 'Y'

      if ITEM_ATTRIB_SQL.GET_PRIMARY_REF_EAN(O_error_message,
                                             L_primary_ref_ean_exists,
                                             O_ean,
                                             O_existing_ean_type,
                                             I_item) = FALSE then
         return FALSE;
      end if;
      ---
      if NOT L_primary_ref_ean_exists then
         if ITEM_ATTRIB_SQL.GET_FIRST_EAN(O_error_message,
                                          L_dummy_boolean,
                                          O_ean,
                                          O_existing_ean_type,
                                          I_item) = FALSE then
            return FALSE;
         end if;
      end if;

   end if; -- L_item_master_row.item_level <= L_item_master_row.tran_level

   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
        return FALSE;
END GET_ITEMCHILDRENDIFF_INFO;

-------------------------------------------------------------------------------------------------------
FUNCTION CONTENTS_ITEM_ORDERABLE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_orderable         IN OUT  ITEM_MASTER.ORDERABLE_IND%TYPE,
                                 I_container_item    IN      ITEM_MASTER.CONTAINER_ITEM%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'ITEM_ATTRIB_SQL.CONTENTS_ITEM_ORDERABLE';


   cursor C_ORDERABLE_CONTENTS is
   select 'Y'
     from item_master
    where container_item = I_container_item
      and orderable_ind = 'Y';


BEGIN

   O_orderable := 'N';
   if I_container_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_container_item','NULL','NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_ORDERABLE_CONTENTS','ITEM_MASTER','Container Item: '||I_container_item);
   open C_ORDERABLE_CONTENTS;

   SQL_LIB.SET_MARK('FETCH', 'C_ORDERABLE_CONTENTS','ITEM_MASTER','Container Item: '||I_container_item);
   fetch C_ORDERABLE_CONTENTS into O_orderable;

   SQL_LIB.SET_MARK('CLOSE', 'C_ORDERABLE_CONTENTS','ITEM_MASTER','Container Item: '||I_container_item);
   close C_ORDERABLE_CONTENTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONTENTS_ITEM_ORDERABLE;
-------------------------------------------------------------------------------------------------------
FUNCTION CONTENTS_ITEM_EXISTS (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists            IN OUT  VARCHAR2,
                               I_item              IN      ITEM_MASTER.CONTAINER_ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'ITEM_ATTRIB_SQL.CONTENTS_ITEM_EXISTS';


   cursor C_CONTENTS_ITEMS is
   select 'Y'
     from item_master
    where container_item = I_item;

BEGIN

   O_exists := 'N';
   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_container_item','NULL','NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_CONTENTS_ITEMS ','ITEM_MASTER','Container Item: '||I_item);
   open C_CONTENTS_ITEMS;

   SQL_LIB.SET_MARK('FETCH', 'C_CONTENTS_ITEMS','ITEM_MASTER','Container Item: '||I_item);
   fetch C_CONTENTS_ITEMS into O_exists;

   SQL_LIB.SET_MARK('CLOSE', 'C_CONTENTS_ITEMS','ITEM_MASTER','Container Item: '||I_item);
   close C_CONTENTS_ITEMS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONTENTS_ITEM_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_CONTAINER_ITEM(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_container_item   IN OUT   ITEM_MASTER.ITEM%TYPE,
                            I_item             IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ITEM_ATTRIB_SQL.GET_CONTAINER_ITEM';
   ---
   cursor C_CONTAINER is
      select im.container_item
        from item_master im
       where im.item              = I_item
         and im.deposit_item_type = 'E';

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CONTAINER','ITEM_MASTER','item:'||I_item);
   open C_CONTAINER;
   ---
   SQL_LIB.SET_MARK('FETCH','C_CONTAINER','ITEM_MASTER','item:'||I_item);
   fetch C_CONTAINER into O_container_item;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CONTAINER','ITEM_MASTER','ITEM:'||I_item);
   close C_CONTAINER;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CONTAINER_ITEM;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_desc            IN       ITEM_MASTER.ITEM_DESC%TYPE,
                    O_exists             OUT   BOOLEAN)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'ITEM_ATTRIB_SQL.CHECK_DESC';

BEGIN

   if I_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_desc',
                                            L_program,
                                            NULL);

      return FALSE;
   end if;
   ---
   if INSTR(I_desc,CHR(10))> 0 or
      INSTR(I_desc,CHR(13))> 0 or
      INSTR(I_desc,'|') > 0 or
      INSTR(I_desc,';') > 0 then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DESC;
--------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_REF_EAN(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists             IN OUT   BOOLEAN,
                             O_item               IN OUT   ITEM_MASTER.ITEM%TYPE,
                             O_item_number_type   IN OUT   ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                             I_item_parent        IN       ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_PRIMARY_REF_EAN';

   cursor C_GET_INFO is
      select im.item,
             im.item_number_type
        from item_master im
       where im.item = (select im1.item
                          from item_master im1
                         where im1.item_parent          = I_item_parent
                           and im1.item_number_type in ('A-EAN','EAN13')
                           and im1.item_level           = 3
                           and im1.primary_ref_item_ind = 'Y');

BEGIN
   --- Check required input parameters
   if I_item_parent is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_parent',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --- Initialize the output variables
   O_item             := NULL;
   O_item_number_type := NULL;

   --- Get the required information
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_INFO',
                    'item_master',
                    'I_item_parent: '||I_item_parent);
   open C_GET_INFO;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_INFO',
                    'item_master',
                    'I_item_parent: '||I_item_parent);
   fetch C_GET_INFO into O_item,
                         O_item_number_type;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_INFO',
                    'item_master',
                    'I_item_parent: '||I_item_parent);
   close C_GET_INFO;

   if O_item is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_PRIMARY_REF_EAN;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_TYPE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_order_type         IN OUT   ITEM_MASTER.ORDER_TYPE%TYPE,
                        I_item               IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_ORDER_TYPE';

   cursor C_GET_ORDER_TYPE is
      select im.order_type
        from item_master im
       where im.item = I_item;

BEGIN
   --- Check required input parameters
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --- Initialize the output variables
   O_order_type  := NULL;

   --- Get the required information
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ORDER_TYPE',
                    'item_master',
                    'I_item: '||I_item);
   open C_GET_ORDER_TYPE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ORDER_TYPE',
                    'item_master',
                    'I_item: '||I_item);
   fetch C_GET_ORDER_TYPE into O_order_type;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ORDER_TYPE',
                    'item_master',
                    'I_item: '||I_item);
   close C_GET_ORDER_TYPE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ORDER_TYPE;
----------------------------------------------------------------------------------------------------------
FUNCTION CHK_DIFF_AGGREGATE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_concatd_diff_desc  IN OUT VARCHAR2,
                            O_diff_1             IN OUT ITEM_MASTER.DIFF_1%TYPE,
                            O_diff_2             IN OUT ITEM_MASTER.DIFF_2%TYPE,
                            O_diff_3             IN OUT ITEM_MASTER.DIFF_3%TYPE,
                            O_diff_4             IN OUT ITEM_MASTER.DIFF_4%TYPE,
                            I_item               IN     ITEM_MASTER.ITEM%TYPE,
                            I_diff_aggr          IN     VARCHAR2
                            )
RETURN BOOLEAN IS

L_program           VARCHAR2(64) := 'ITEM_ATTRIB_SQL.CHK_DIFF_AGGREGATE';
L_concatd_diff_desc VARCHAR2(255);
L_desc_1            DIFF_IDS.DIFF_DESC%TYPE;
L_desc_2            DIFF_IDS.DIFF_DESC%TYPE;
L_desc_3            DIFF_IDS.DIFF_DESC%TYPE;
L_desc_4            DIFF_IDS.DIFF_DESC%TYPE;
L_item              ITEM_MASTER.ITEM%TYPE := I_item;
L_diff_aggr         VARCHAR2(255) := I_diff_aggr;
L_diff_desc         DIFF_IDS.DIFF_DESC%TYPE;

cursor C_SPLIT_DIFF_AGGR  is
select decode(ip.diff_1_aggregate_ind, 'Y', ic.diff_1, NULL)  diff_1,
       decode(ip.diff_2_aggregate_ind, 'Y', ic.diff_2, NULL)  diff_2,
       decode(ip.diff_3_aggregate_ind, 'Y', ic.diff_3, NULL)  diff_3,
       decode(ip.diff_4_aggregate_ind, 'Y', ic.diff_4, NULL)  diff_4
  from item_master ip,
       item_master ic
 where ip.item = L_item
   and (ip.item = ic.item_parent or
        ip.item = ic.item_grandparent)
   and ic.tran_level = ic.item_level
   and decode(ip.diff_1_aggregate_ind, 'Y', ic.diff_1||':', NULL) ||
       decode(ip.diff_2_aggregate_ind, 'Y', ic.diff_2||':', NULL) ||
       decode(ip.diff_3_aggregate_ind, 'Y', ic.diff_3||':', NULL) ||
       decode(ip.diff_4_aggregate_ind, 'Y', ic.diff_4||':', NULL) = L_diff_aggr ||':'
   and rownum =1;


BEGIN
  open C_SPLIT_DIFF_AGGR;
  fetch C_SPLIT_DIFF_AGGR into O_diff_1,
                               O_diff_2,
                               O_diff_3,
                               O_diff_4;
     if C_SPLIT_DIFF_AGGR%NOTFOUND then
        close C_SPLIT_DIFF_AGGR;
        return FALSE;
     end if;

  close C_SPLIT_DIFF_AGGR;


    if (O_diff_1 IS NOT NULL) then
      if (DIFF_SQL.GET_DIFF_DESC (O_error_message,L_diff_desc,O_diff_1)= TRUE) then
      L_desc_1 := L_diff_desc;
        L_concatd_diff_desc := L_concatd_diff_desc||L_desc_1||':';
      else
        return FALSE;
      end if;
    end if;


    if (O_diff_2 IS NOT NULL) then
      if(DIFF_SQL.GET_DIFF_DESC (O_error_message,L_diff_desc,O_diff_2)= TRUE) then
      L_desc_2 := L_diff_desc;
        L_concatd_diff_desc := L_concatd_diff_desc||L_desc_2||':';
      else
        return FALSE;
      end if;
    end if;

    if(O_diff_3 IS NOT NULL) then
      if(DIFF_SQL.GET_DIFF_DESC (O_error_message,L_diff_desc,O_diff_3)= TRUE) then
        L_desc_3 := L_diff_desc;
        L_concatd_diff_desc := L_concatd_diff_desc||L_desc_3||':';
      else
        return FALSE;
      end if;
    end if;

    if(O_diff_4 IS NOT NULL) then
      if(DIFF_SQL.GET_DIFF_DESC (O_error_message,L_diff_desc,O_diff_4)= TRUE) then
        L_desc_4 := L_diff_desc;
        L_concatd_diff_desc := L_concatd_diff_desc||L_desc_4||':';
      else
        return FALSE;
      end if;
    end if;

  O_concatd_diff_desc := substr(L_concatd_diff_desc,1,length(L_concatd_diff_desc)-1);

  return TRUE;

EXCEPTION
 when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, to_char(SQLCODE));
   return FALSE;

END CHK_DIFF_AGGREGATE;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_CATCH_WEIGHT_ATTRIB(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_catch_weight_ind   IN OUT   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                                 I_item               IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_ATTRIB';

   cursor C_GET_CATCH_WEIGHT_ATTRIB is
      select im.catch_weight_ind
        from item_master im
       where im.item = I_item;

BEGIN
   --- Check required input parameters
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --- Initialize the output variables
   O_catch_weight_ind  := NULL;

   --- Get the required information
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_CATCH_WEIGHT_ATTRIB',
                    'item_master',
                    'I_item: '||I_item);
   open C_GET_CATCH_WEIGHT_ATTRIB;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_CATCH_WEIGHT_ATTRIB',
                    'item_master',
                    'I_item: '||I_item);
   fetch C_GET_CATCH_WEIGHT_ATTRIB into O_catch_weight_ind;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_CATCH_WEIGHT_ATTRIB',
                    'item_master',
                    'I_item: '||I_item);
   close C_GET_CATCH_WEIGHT_ATTRIB;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_CATCH_WEIGHT_ATTRIB;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_CATCH_WEIGHT_UOM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_catch_weight_uom   IN OUT   ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE,
                              I_item               IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_UOM';

   L_item_rec             ITEM_MASTER%ROWTYPE;
   L_nominal_weight       ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE   := NULL;
   L_standard_class       UOM_CLASS.UOM_CLASS%TYPE;

   L_table         VARCHAR2(30) := 'ITEM_MASTER';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where item = I_item
         for update nowait;
BEGIN
   --- Check required input parameters
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_rec,
                                      I_item) = FALSE then
      return FALSE;
   end if;

   if L_item_rec.catch_weight_ind = 'Y' and L_item_rec.catch_weight_uom is NOT NULL then
       O_catch_weight_uom := L_item_rec.catch_weight_uom;
	   return TRUE;
   end if;

   if L_item_rec.catch_weight_ind = 'Y' then
      if L_item_rec.standard_uom = 'EA' then
         L_standard_class := 'QTY';
      else
         if UOM_SQL.GET_CLASS (O_error_message,
                               L_standard_class,
                               L_item_rec.standard_uom) = FALSE then
            return FALSE;
         end if;
      end if;

      if L_standard_class = 'MASS' then
         -- for items that are stocked by loose weight we will just use the standard UOM
         O_catch_weight_uom := L_item_rec.standard_uom;
      else -- stocked by qty, use the catch_weight_uom
         -- default the value using the nominal weight UOM
            if ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT_UOM(O_error_message,
                                                                L_nominal_weight,
                                                                O_catch_weight_uom,
                                                                I_item) = FALSE then
               return FALSE;
            end if;
		end if;
            if O_catch_weight_uom is NOT NULL then
         ---
         
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_ITEM_MASTER',
                          L_table,
                          'I_item '||I_item);
         open C_LOCK_ITEM_MASTER;
         ---
         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_ITEM_MASTER',
                          L_table,
                          'I_item '||I_item);
         close C_LOCK_ITEM_MASTER;

               SQL_LIB.SET_MARK('UPDATE', 'ITEM_MASTER','ITEM_MASTER','Item: '||I_item);
               update item_master
                  set catch_weight_uom = O_catch_weight_uom
                where item = I_item;

         end if;

   else -- non-catchweight item
      O_catch_weight_uom := NULL;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ITEM_MASTER%ISOPEN then
         close C_LOCK_ITEM_MASTER;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_ITEM_MASTER%ISOPEN then
         close C_LOCK_ITEM_MASTER;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_CATCH_WEIGHT_UOM;
----------------------------------------------------------------------------------------------------------
FUNCTION CHECK_STATUS_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE)


RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'ITEM_ATTRIB_SQL.CHECK_STATUS_LOC';
   L_item_exist   VARCHAR2(1)  := NULL;

   cursor C_CHECK_STATUS_LOC is
      select 'x'
        from item_master im,
             item_supp_country_loc iscl
       where im.item = I_item
         and im.item = iscl.item
         and im.status = 'A'
         and rownum = 1;

BEGIN

   open C_CHECK_STATUS_LOC;
   fetch C_CHECK_STATUS_LOC into L_item_exist;
   close C_CHECK_STATUS_LOC;

   if L_item_exist is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_STATUS_LOC;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_RECORD_COUNT_COST (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_count               IN OUT   NUMBER,
                                I_cost_change         IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_ATTRIB_SQL.GET_RECORD_COUNT_COST';

   cursor C_GET_RECORD_COUNT is
      select count(*)
        from cost_change_temp
       where cost_change = I_cost_change
         and item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id;

BEGIN

   open C_GET_RECORD_COUNT;
   fetch C_GET_RECORD_COUNT into O_count;
   close C_GET_RECORD_COUNT;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_RECORD_COUNT_COST;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_DIRECT_SHIP_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_direct_ship_ind   IN OUT   ITEM_SUPPLIER.DIRECT_SHIP_IND%TYPE,
                             I_item              IN       ITEM_MASTER.ITEM%TYPE,
                             I_supplier          IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'ITEM_ATTRIB_SQL.GET_DIRECT_SHIP_IND';

   cursor C_GET_DIRECT_SHIP_IND is
      select direct_ship_ind
        from item_supplier
       where item = I_item
         and supplier = I_supplier;

BEGIN

   --- Check required input parameters
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_DIRECT_SHIP_IND;
   fetch C_GET_DIRECT_SHIP_IND into O_direct_ship_ind;
   close C_GET_DIRECT_SHIP_IND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_DIRECT_SHIP_IND;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_BRAND_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_brand_desc        IN OUT   BRAND.BRAND_DESCRIPTION%TYPE,
                        I_brand_name        IN       BRAND.BRAND_NAME%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'ITEM_ATTRIB_SQL.GET_BRAND_DESC';
   L_brand_desc     BRAND.BRAND_DESCRIPTION%TYPE;

   cursor C_BRAND_DESC is
      select brand_description
        from brand
       where brand_name = I_brand_name;

BEGIN

   --- Check required input parameters
   if I_brand_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_brand_name',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_BRAND_DESC;
   fetch C_BRAND_DESC into L_brand_desc;

   if C_BRAND_DESC%NOTFOUND then
       O_error_message := SQL_LIB.CREATE_MSG('INV_BRAND_NAME',
                                              NULL, NULL, NULL);
       close C_BRAND_DESC;
       return FALSE;
   end if;

   close C_BRAND_DESC;

   O_brand_desc := L_brand_desc;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_BRAND_DESC%ISOPEN then
         close C_BRAND_DESC;
      end if;
      return FALSE;

END GET_BRAND_DESC;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_TRAN_CHILD_ITEMS(O_error_message  IN OUT  VARCHAR2,
                              O_item_tbl       IN OUT  ITEM_TBL,
                              I_item           IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'ITEM_ATTRIB_SQL.GET_TRAN_CHILD_ITEMS';

   cursor C_GET_TRAN_CHILD is
      select item
        from item_master
       where (item_parent = I_item
          or item_grandparent = I_item)
         and item_level = tran_level
         and not exists (select 'x' 
                           from daily_purge
                          where table_name = 'ITEM_MASTER'
                            and key_value = item
                            and rownum = 1);

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_TRAN_CHILD','ITEM_MASTER','ITEM: '||I_item);
   open C_GET_TRAN_CHILD;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_TRAN_CHILD','ITEM_MASTER','ITEM: '||I_item);
   fetch C_GET_TRAN_CHILD BULK COLLECT into O_item_tbl;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_TRAN_CHILD','ITEM_MASTER','ITEM: '||I_item);
   close C_GET_TRAN_CHILD;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_GET_TRAN_CHILD%ISOPEN then
         close C_GET_TRAN_CHILD;
      end if;
      return FALSE;
END GET_TRAN_CHILD_ITEMS;
----------------------------------------------------------------------------------------------------------
FUNCTION ITEM_CHILDREN(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_item_table_t      OUT      GV_ITEM_TABLE_T,
                       I_item              IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'ITEM_ATTRIB_SQL.ITEM_CHILDREN';

   cursor C_item_children is
   select item_parent
     from item_master
    where item_grandparent = I_item;

BEGIN

   O_item_table_t := NULL;
   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_item','NULL','NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_item_children','ITEM_MASTER','item_grandparent: '||I_item);
   open C_item_children;

   SQL_LIB.SET_MARK('FETCH', 'C_item_children','ITEM_MASTER','item_grandparent: '||I_item);
   fetch C_item_children bulk collect into O_item_table_t;

   SQL_LIB.SET_MARK('CLOSE', 'C_item_children','ITEM_MASTER','item_grandparent: '||I_item);
   close C_item_children;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_item_children%ISOPEN then
         close C_item_children;
      end if;
      return FALSE;

END ITEM_CHILDREN;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LEVEL_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_level_desc   IN OUT   CODE_DETAIL.CODE_DESC%TYPE,
                             I_pack_ind          IN       ITEM_MASTER.PACK_IND%TYPE,
                             I_item_level        IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                             I_tran_level        IN       ITEM_MASTER.TRAN_LEVEL%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'ITEM_ATTRIB_SQL.GET_ITEM_LEVEL_DESC';

   L_code_type      CODE_HEAD.CODE_TYPE%TYPE;

BEGIN

   if I_pack_ind is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_pack_ind','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_item_level is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_item_level','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_tran_level is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_tran_level','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_pack_ind = 'Y' then
      L_code_type := 'PITL';
   elsif I_tran_level = 1 then
      L_code_type := 'T1IT';
   elsif I_tran_level = 2 then
      L_code_type := 'T2IT';
   elsif I_tran_level = 3 then
      L_code_type := 'T3IT';
   else
      O_error_message:= SQL_LIB.CREATE_MSG('INV_TRAN_LVL', NULL, NULL, NULL);
      return FALSE;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 L_code_type,    --I_code_type
                                 I_item_level,   --I_code
                                 O_item_level_desc) = FALSE then   --O_code_desc
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_LEVEL_DESC;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LEVEL_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_level_desc   IN OUT   CODE_DETAIL.CODE_DESC%TYPE,
                             I_item              IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'ITEM_ATTRIB_SQL.GET_ITEM_LEVEL_DESC';

   L_item_parent      ITEM_MASTER.ITEM_PARENT%TYPE := NULL;
   L_pack_ind         ITEM_MASTER.PACK_IND%TYPE := NULL;
   L_parent_pack_ind  ITEM_MASTER.PACK_IND%TYPE := NULL;
   L_item_level       ITEM_MASTER.ITEM_LEVEL%TYPE := NULL;
   L_tran_level       ITEM_MASTER.TRAN_LEVEL%TYPE := NULL;

BEGIN

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_item','NULL','NOT NULL');
      return FALSE;
   end if;

   select item_parent,
          pack_ind,
          item_level,
          tran_level
     into L_item_parent, L_pack_ind, L_item_level, L_tran_level
     from item_master
    where item = I_item;

   if L_pack_ind is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INV_ITEM_ITEM', I_item, NULL, NULL);
      return FALSE;
   end if;

   if L_item_parent is NOT NULL then
      select pack_ind
        into L_parent_pack_ind
        from item_master
       where item = L_item_parent;

      -- If either item or its parent item is a pack, then use pack_ind of 'Y' to retrieve item level description.
      if L_parent_pack_ind = 'Y' or L_pack_ind = 'Y' then
         L_pack_ind := 'Y';
      end if;
   end if;

   if GET_ITEM_LEVEL_DESC(O_error_message,
                          O_item_level_desc,
                          L_pack_ind,
                          L_item_level,
                          L_tran_level) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEM_LEVEL_DESC;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_TRAN_LEVEL_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tran_level_desc   IN OUT   CODE_DETAIL.CODE_DESC%TYPE,
                             I_pack_ind          IN       ITEM_MASTER.PACK_IND%TYPE,
                             I_tran_level        IN       ITEM_MASTER.TRAN_LEVEL%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64)   := 'ITEM_ATTRIB_SQL.GET_TRAN_LEVEL_DESC';

   L_code_type      CODE_HEAD.CODE_TYPE%TYPE;

BEGIN

   if I_pack_ind is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_pack_ind','NULL','NOT NULL');
      return FALSE;
   end if;

   if I_tran_level is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM','I_tran_level','NULL','NOT NULL');
      return FALSE;
   end if;

   -- The transaction level description and item level description are the same
   -- when the item level equals the transaction level. Therefore, the transaction
   -- level description is fetched by passing the transaction level to GET_ITEM_LEVEL_DESC
   -- for both the item level and transaction level parameters.
   if GET_ITEM_LEVEL_DESC(O_error_message,
                          O_tran_level_desc,
                          I_pack_ind,
                          I_tran_level,
                          I_tran_level) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_TRAN_LEVEL_DESC;
----------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_MASTER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'ITEM_ATTRIB_SQL.DELETE_ITEM_MASTER_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_MASTER_TL is
      select 'x'
        from item_master_tl
       where item = I_item
         for update nowait;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_MASTER_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_MASTER_TL',
                    L_table,
                    'I_item '||I_item);
   open C_LOCK_ITEM_MASTER_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_MASTER_TL',
                    L_table,
                    'I_item '||I_item);
   close C_LOCK_ITEM_MASTER_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'item_master_tl',
                    'I_item '||I_item);
   delete from item_master_tl
         where item = I_item;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ITEM_MASTER_TL;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_LEVEL_NAMES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_level_1_name      OUT  CODE_DETAIL.CODE_DESC%TYPE,
                         O_level_2_name      OUT  CODE_DETAIL.CODE_DESC%TYPE,
                         O_level_3_name      OUT  CODE_DETAIL.CODE_DESC%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)    := 'ITEM_ATTRIB_SQL.GET_LEVEL_NAMES';

BEGIN

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'IILN',
                                 '1',
                                 O_level_1_name) = FALSE then
      return FALSE;
   end if;
   ---
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'IILN',
                                 '2',
                                 O_level_2_name) = FALSE then
      return FALSE;
   end if;
   --- 
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                 'IILN',
                                 '3',
                                 O_level_3_name) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LEVEL_NAMES;
--------------------------------------------------------------------------------
END ITEM_ATTRIB_SQL;
/