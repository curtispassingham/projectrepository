
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY UDA_XML AS

--------------------------------------------------------------------------------
FUNCTION DELETE_UDA(O_status     OUT    VARCHAR2,
                    O_text       OUT    VARCHAR2,
                    I_record     IN     UDA%ROWTYPE,
                    root         IN OUT xmldom.DOMElement)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION DELETE_UDAV(O_status     OUT    VARCHAR2,
                     O_text       OUT    VARCHAR2,
                     I_record     IN     UDA_VALUES%ROWTYPE,
                     root         IN OUT xmldom.DOMElement)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

FUNCTION ADD_UPDATE_UDA(O_status          OUT    VARCHAR2,
                        O_text            OUT    VARCHAR2,
                        I_record          IN     UDA%ROWTYPE,
                        root              IN OUT xmldom.DOMElement)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION ADD_UPDATE_UDAV(O_status          OUT    VARCHAR2,
                         O_text            OUT    VARCHAR2,
                         I_record          IN     UDA_VALUES%ROWTYPE,
                         root              IN OUT xmldom.DOMElement)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

/* BUILD_MESSAGE is the only public function in the package.  It is called 
   whenever the database trigger captures an event that needs to be published.
   The trigger should pass in a record with all of the values from the table
   row, except in the case of deletes, when the record will only contain
   the identifier.

   BUILD_MESSAGE creates the XML message, puts it in a CLOB, and returns the 
   CLOB.  The trigger should then insert the XML-CLOB in the message queue table.
*/

--------------------------------------------------------------------------------
FUNCTION BUILD_UDA_MSG(O_status          OUT VARCHAR2,
                       O_text            OUT VARCHAR2,
                       O_message         OUT CLOB,
                       I_record          IN  UDA%ROWTYPE,
                       I_action_type     IN  VARCHAR2)
RETURN BOOLEAN IS
   root            xmldom.DOMElement;
   L_doc_type      VARCHAR2(64) := NULL;

BEGIN
   if I_action_type = 'D' then
      L_doc_type:= 'UDARef';
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
         return FALSE;
      end if;
      ---
      if not DELETE_UDA(O_status,
                         O_text,
                         I_record,
                         root) then
         return FALSE;
      end if;

   else
      L_doc_type:= 'UDADesc';
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
         return FALSE;
      end if;
      ---
      if not ADD_UPDATE_UDA(O_status,
                            O_text,
                            I_record,
                            root) then
         return FALSE;
      end if;

   end if;

   if not API_LIBRARY.WRITE_DOCUMENT(O_status,
                                     O_text,
                                     O_message,
                                     root) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'UDA_XML.BUILD_UDA_MSG');
      return FALSE;

END BUILD_UDA_MSG;
--------------------------------------------------------------------------------
FUNCTION BUILD_UDAV_MSG(O_status          OUT VARCHAR2,
                        O_text            OUT VARCHAR2,
                        O_message         OUT CLOB,
                        I_record          IN  UDA_VALUES%ROWTYPE,
                        I_action_type     IN  VARCHAR2)
RETURN BOOLEAN IS
   root            xmldom.DOMElement;
   L_doc_type      VARCHAR2(64) := NULL;

BEGIN
   if I_action_type = 'D' then
      L_doc_type:= 'UDAValRef';
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
         return FALSE;
      end if;
      ---
      if not DELETE_UDAV(O_status,
                        O_text,
                        I_record,
                        root) then
         return FALSE;
      end if;

   else
      L_doc_type:= 'UDAValDesc';
      ---
      if not API_LIBRARY.CREATE_MESSAGE(O_status,
                                        O_text,
                                        root,
                                        L_doc_type) then
         return FALSE;
      end if;
      ---
      if not ADD_UPDATE_UDAV(O_status,
                             O_text,
                             I_record,
                             root) then
         return FALSE;
      end if;

   end if;

   if not API_LIBRARY.WRITE_DOCUMENT(O_status,
                                     O_text,
                                     O_message,
                                     root) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'UDA_XML.BUILD_UDAV_MSG');
      return FALSE;

END BUILD_UDAV_MSG;
--------------------------------------------------------------------------------


      /*** Private Program Bodies ***/
--------------------------------------------------------------------------------
FUNCTION DELETE_UDA(O_status     OUT    VARCHAR2,
                     O_text       OUT    VARCHAR2,
                     I_record     IN     UDA%ROWTYPE,
                     root         IN OUT xmldom.DOMElement)
RETURN BOOLEAN IS

   /* This function sets up a delete message by adding appropriate values to
      the XML msg. */

BEGIN
   /* The delete message will usually just include the identifier of the record
      that was deleted. */

   rib_xml.addElement( root, 'uda_id', I_record.uda_id);

   O_status := API_CODES.SUCCESS;

   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'UDA_XML.DELETE_UDA');
      return FALSE;

END DELETE_UDA;
--------------------------------------------------------------------------------
FUNCTION DELETE_UDAV(O_status     OUT    VARCHAR2,
                     O_text       OUT    VARCHAR2,
                     I_record     IN     UDA_VALUES%ROWTYPE,
                     root         IN OUT xmldom.DOMElement)
RETURN BOOLEAN IS

   /* This function sets up a delete message by adding appropriate values to
      the XML msg. */

BEGIN
   /* The delete message will usually just include the identifier of the record
      that was deleted. */

   rib_xml.addElement( root, 'uda_id', I_record.uda_id);
   rib_xml.addElement( root, 'uda_value', I_record.uda_value);

   O_status := API_CODES.SUCCESS;

   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'UDA_XML.DELETE_UDAV');
      return FALSE;

END DELETE_UDAV;

--------------------------------------------------------------------------------
FUNCTION ADD_UPDATE_UDA(O_status          OUT    VARCHAR2,
                        O_text            OUT    VARCHAR2,
                        I_record          IN     UDA%ROWTYPE,
                        root              IN OUT xmldom.DOMElement)
RETURN BOOLEAN IS

   /* This function sets up an add or update message by adding
      appropriate values to the XML msg. */

BEGIN

   /* The add and update messages will include the identifier and a bunch of values.
      In some cases, the add and update messages will include different values,
      and this function will have to be split. */

   rib_xml.addElement( root, 'uda_id', I_record.UDA_ID);
   rib_xml.addElement( root, 'uda_desc', I_record.UDA_DESC);
   rib_xml.addElement( root, 'module', I_record.MODULE);
   rib_xml.addElement( root, 'display_type', I_record.DISPLAY_TYPE);
   rib_xml.addElement( root, 'data_type', I_record.DATA_TYPE);
   rib_xml.addElement( root, 'data_length', I_record.DATA_LENGTH);
   rib_xml.addElement( root, 'single_value_ind', I_record.SINGLE_VALUE_IND);
   
   O_status := API_CODES.SUCCESS;

   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'UDA_XML.ADD_UPDATE_UDA');
      return FALSE;

END ADD_UPDATE_UDA;
--------------------------------------------------------------------------------
FUNCTION ADD_UPDATE_UDAV(O_status          OUT    VARCHAR2,
                         O_text            OUT    VARCHAR2,
                         I_record          IN     UDA_VALUES%ROWTYPE,
                         root              IN OUT xmldom.DOMElement)
RETURN BOOLEAN IS

   /* This function sets up an add or update message by adding
      appropriate values to the XML msg. */

BEGIN

   /* The add and update messages will include the identifier and a bunch of values.
      In some cases, the add and update messages will include different values,
      and this function will have to be split. */
   
   rib_xml.addElement( root, 'uda_id', I_record.UDA_ID);
   rib_xml.addElement( root, 'uda_value', I_record.UDA_VALUE);
   rib_xml.addElement( root, 'uda_value_desc', I_record.UDA_VALUE_DESC);
   
   O_status := API_CODES.SUCCESS;

   return true;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'UDA_XML.ADD_UPDATE_UDAV');
      return FALSE;

END ADD_UPDATE_UDAV;
--------------------------------------------------------------------------------
END UDA_XML;
/
