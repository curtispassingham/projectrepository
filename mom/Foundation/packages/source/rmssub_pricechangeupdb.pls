CREATE OR REPLACE PACKAGE BODY RMSSUB_PRICECHANGE_UPDATE AS

--------------------------------------------------------------------------------
--                           PRIVATE GLOBAL VARIABLES                         --
--------------------------------------------------------------------------------

LP_so    SYSTEM_OPTIONS%ROWTYPE;
TYPE FDOH_DATE_TBL IS TABLE OF DATE INDEX BY VARCHAR2(50);
LP_fdoh_date_tbl   FDOH_DATE_TBL;

TYPE rowid_tbl       IS TABLE OF  ROWID;
TYPE uom_tbl         IS TABLE OF  VARCHAR2(4);
TYPE pc_type_tbl     IS TABLE OF  VARCHAR2(2);
TYPE event_tbl       IS TABLE OF  VARCHAR2(6);
TYPE flag_tbl        IS TABLE OF  BOOLEAN;
TYPE curr_tbl        IS TABLE OF  VARCHAR2(3);
TYPE merch_hier_tbl  IS TABLE OF  NUMBER(4);
TYPE number_ind_tbl  IS TABLE OF  NUMBER(6);

LP_seq                     ID_TBL           :=  ID_TBL();
LP_item                    ITEM_TBL         :=  ITEM_TBL();
LP_loc                     LOC_TBL          :=  LOC_TBL();
LP_loc_type                LOC_TYPE_TBL     :=  LOC_TYPE_TBL();
LP_price_chg_type          PC_TYPE_TBL      :=  PC_TYPE_TBL();
LP_tran_type               TRAN_TYPE_TBL    :=  TRAN_TYPE_TBL();
LP_unit_cost               UNIT_COST_TBL    :=  UNIT_COST_TBL();
LP_current_unit_retail     UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_standard_unit_retail    UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_selling_unit_retail     UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_selling_uom             UOM_TBL          :=  UOM_TBL();
LP_promo_retail            UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_promo_selling_retail    UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_promo_selling_uom       UOM_TBL          :=  UOM_TBL();
LP_multi_units             QTY_TBL          :=  QTY_TBL();
LP_multi_unit_retail       UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_multi_selling_uom       UOM_TBL          :=  UOM_TBL();
LP_clear_ind               INDICATOR_TBL    :=  INDICATOR_TBL();
LP_prom_event              EVENT_TBL        :=  EVENT_TBL();
LP_item_loc_rowid          ROWID_TBL        :=  ROWID_TBL();
LP_primary_supp            SUPPLIER_TBL     :=  SUPPLIER_TBL();
LP_item_soh                QTY_TBL          :=  QTY_TBL();
LP_currency_code           CURR_TBL         :=  CURR_TBL();
LP_supplier                SUPPLIER_TBL     :=  SUPPLIER_TBL();
LP_dept                    MERCH_HIER_TBL   :=  MERCH_HIER_TBL();
LP_class                   MERCH_HIER_TBL   :=  MERCH_HIER_TBL();
LP_subclass                MERCH_HIER_TBL   :=  MERCH_HIER_TBL();
LP_std_uom                 UOM_TBL          :=  UOM_TBL();
LP_reason                  NUMBER_IND_TBL   :=  NUMBER_IND_TBL();
LP_action_date             DATE_TBL         :=  DATE_TBL();
LP_is_on_clearance         INDICATOR_TBL    :=  INDICATOR_TBL();
LP_change_type             NUMBER_IND_TBL   :=  NUMBER_IND_TBL();
LP_change_amount           UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_orig_retail             UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
--
LP_pack_ind                INDICATOR_TBL    :=  INDICATOR_TBL();
LP_sellable_ind            INDICATOR_TBL    :=  INDICATOR_TBL();
LP_orderable_ind           INDICATOR_TBL    :=  INDICATOR_TBL();
LP_pack_type               INDICATOR_TBL    :=  INDICATOR_TBL();
LP_vat_rate                UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_class_vat_ind           INDICATOR_TBL    :=  INDICATOR_TBL();
LP_catch_weight_ind        INDICATOR_TBL    :=  INDICATOR_TBL();
--
LP_current_selling_retail               UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_current_selling_uom                  UOM_TBL          :=  UOM_TBL();
LP_current_clear_retail                 UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_current_clear_uom                    UOM_TBL          :=  UOM_TBL();
LP_current_simple_promo_retail          UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_current_simple_promo_uom             UOM_TBL          :=  UOM_TBL();
LP_is_on_simple_promo                   INDICATOR_TBL    :=  INDICATOR_TBL();
LP_current_std_retail                   UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_current_std_clear_retail             UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
LP_current_std_promo_retail             UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
--
LP_current_standard_retail              UNIT_RETAIL_TBL  :=  UNIT_RETAIL_TBL();
--
LP_wholesale_loc_type      LOC_TYPE_TBL     :=  LOC_TYPE_TBL();
--
LP_pc_count                   INTEGER;
LP_cr_count                   INTEGER;
LP_vdate                      DATE          :=  GET_VDATE;
LP_user                       USER_ATTRIB.USER_NAME%TYPE :=  USER;
LP_sd_mark_type      CONSTANT INTEGER       :=  10;
LP_percent_funding   CONSTANT NUMBER(1)     :=  0;
LP_amount_funding    CONSTANT NUMBER(1)     :=  1;
LP_regular_pc        CONSTANT VARCHAR2(1)   :=  '0';
LP_vendor_funded_pc  CONSTANT VARCHAR2(1)   :=  '1';
LP_percent_off       CONSTANT VARCHAR2(1)   :=  '0';
LP_amount_off        CONSTANT VARCHAR2(1)   :=  '1';
LP_by_percent        CONSTANT NUMBER(1)     :=  0;
LP_by_amount         CONSTANT NUMBER(1)     :=  1;
LP_fixed_price       CONSTANT NUMBER(1)     :=  2;


--------------------------------------------------------------------------------
--                              PRIVATE PROCEDURES                            --
--------------------------------------------------------------------------------

FUNCTION CHECK_REQUIRED_FIELDS(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message       IN     OBJ_PRICEEVENT_IL_REC)
RETURN BOOLEAN;

FUNCTION DETERMINE_TRAN_TYPE(O_error_message          OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tran_type           IN OUT PRICE_HIST.TRAN_TYPE%TYPE,
                             O_single_changed      IN OUT BOOLEAN,
                             O_multi_changed       IN OUT BOOLEAN,
                             I_price_chg_type      IN     VARCHAR2,
                             I_selling_unit_retail IN     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                             I_multi_unit_retail   IN     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                             I_change_type         IN     RPM_PRICE_CHANGE.CHANGE_TYPE%TYPE,
                             I_is_on_clearance     IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_ORIG_RETAIL(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

FUNCTION GET_FDOH_DATE(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_fdoh_date        OUT   DATE)
RETURN BOOLEAN;

FUNCTION CHECK_DEAL_ITEMLOC(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_origin_country_id IN OUT DEAL_ITEMLOC.ORIGIN_COUNTRY_ID%TYPE,
                            O_valid             IN OUT VARCHAR2,
                            I_deal_id           IN     DEAL_ITEMLOC.DEAL_ID%TYPE,
                            I_deal_detail_id    IN     DEAL_ITEMLOC.DEAL_DETAIL_ID%TYPE,
                            I_item              IN     DEAL_ITEMLOC.ITEM%TYPE,
                            I_loc_type          IN     DEAL_ITEMLOC.LOC_TYPE%TYPE,
                            I_location          IN     DEAL_ITEMLOC.LOCATION%TYPE)
RETURN BOOLEAN;

FUNCTION CHECK_DEAL_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT VARCHAR2,
                             I_item          IN     DEAL_ITEMLOC.ITEM%TYPE,
                             I_partner_type  IN     DEAL_HEAD.PARTNER_TYPE%TYPE,
                             I_partner_id    IN     DEAL_HEAD.PARTNER_ID%TYPE,
                             I_supplier      IN     DEAL_HEAD.SUPPLIER%TYPE)
RETURN BOOLEAN;

FUNCTION CALC_REPORTING_DATE(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_reporting_date       IN OUT DATE,
                             I_deal_id              IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                             I_deal_detail_id       IN     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                             I_active_date          IN     DEAL_HEAD.ACTIVE_DATE%TYPE,
                             I_close_date           IN     DEAL_HEAD.CLOSE_DATE%TYPE,
                             I_deal_reporting_level IN    DEAL_HEAD.DEAL_REPORTING_LEVEL%TYPE,
                             I_vdate                IN     DATE)
RETURN BOOLEAN;

FUNCTION CALC_ON_HAND_INV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_on_hand_inv   IN OUT NUMBER,
                          I_item          IN     DEAL_ITEMLOC.ITEM%TYPE,
                          I_location      IN     DEAL_ITEMLOC.LOCATION%TYPE,
                          I_selling_uom   IN     ITEM_MASTER.STANDARD_UOM%TYPE)
RETURN BOOLEAN;

FUNCTION CALC_PRICE_DIFF(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_price_diff         IN OUT NUMBER,
                         I_item               IN     DEAL_ITEMLOC.ITEM%TYPE,
                         I_location           IN     DEAL_ITEMLOC.LOCATION%TYPE,
                         I_new_selling_uom    IN     ITEM_MASTER.STANDARD_UOM%TYPE,
                         I_new_selling_retail IN     NUMBER,
                         I_old_selling_uom    IN     ITEM_MASTER.STANDARD_UOM%TYPE,
                         I_old_selling_retail IN     NUMBER)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
--                              PUBLIC PROCEDURES                             --
--------------------------------------------------------------------------------

FUNCTION BUILD_PRICE_CHANGE(O_error_message      OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       OBJ_PRICEEVENT_IL_TBL)
RETURN BOOLEAN IS

  L_program               VARCHAR2(50) := 'RMSSUB_PRICECHANGE_UPDATE.BUILD_PRICE_CHANGE';
  L_pc_tbl                OBJ_PRICEEVENT_IL_TBL := NULL;
  L_single_changed        BOOLEAN;
  L_multi_changed         BOOLEAN;
  L_is_on_clearance       INDICATOR_TBL   := INDICATOR_TBL();
  L_curr_selling_retail   UNIT_RETAIL_TBL := UNIT_RETAIL_TBL();
  L_selling_uom           UOM_TBL := UOM_TBL();


   cursor C_PC_INFO is
    select inner.*
      from (
      select /*+ CARDINALITY(pct, 10) */ pct.current_unit_retail,
             pct.old_selling_unit_retail,
             pct.primary_supp,
             pct.old_selling_uom,
             decode(pct.clear_ind, 'Y', 1, 0),
             pct.il_rowid,
             --
             (ils.stock_on_hand + ils.in_transit_qty +
              NVL(ils.pack_comp_soh,0) + NVL(ils.pack_comp_intran,0)) item_soh,
             --
             iscl.unit_cost,
             iscl.supplier,
             --
             pct.dept,
             pct.class,
             pct.subclass,
             pct.standard_uom,
             pct.pack_ind,
             pct.sellable_ind,
             pct.orderable_ind,
             pct.pack_type,
             --
             pct.price_chg_type,
             pct.item,
             pct.loc,
             pct.loc_type,
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, NULL, pct.selling_unit_retail),
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, NULL, pct.selling_uom),
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, pct.selling_unit_retail, NULL),
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, pct.selling_uom, NULL),
             pct.multi_units,
             pct.multi_unit_retail,
             pct.multi_selling_uom,
             pct.prom_event,
             pct.currency_code,
             pct.is_on_clearance,
             pct.change_type,
             pct.change_amount,
             pct.pc_reason_code,
             nvl(pct.active_date, LP_vdate) active_date,
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.CLEARANCE, 'Y', 'N'),
             pct.current_selling_retail,
             pct.current_selling_uom,
             pct.current_clear_retail,
             pct.current_clear_uom,
             pct.current_simple_promo_retail,
             pct.current_simple_promo_uom,
             pct.is_on_simple_promo,
             decode(pct.loc_type,'W','C',s.store_type) wholesale_loc_type
        from item_loc_soh ils,
             item_supp_country_loc iscl,
             store s,
             TABLE(CAST(L_pc_tbl AS OBJ_PRICEEVENT_IL_TBL)) pct
      where iscl.loc(+) = pct.loc
         and iscl.origin_country_id(+) = pct.primary_cntry
         and iscl.supplier(+) = pct.primary_supp
         and iscl.item(+) = pct.item
         --
         and ils.loc = pct.loc
         and ils.item = pct.item
         --
         and s.store(+) = pct.loc
         --
         and pct.change_type is NULL
--
         and (pct.price_chg_type <> 'PC'
              or nvl(pct.is_on_clearance,0) = 0)

-- Parent item record insertion
   union all
    select /*+ CARDINALITY(pct, 10) */ distinct pct.current_unit_retail,
           pct.old_selling_unit_retail,
           pct.primary_supp,
           pct.old_selling_uom,
           decode(pct.clear_ind, 'Y', 1, 0),
           pct.il_rowid,
           --
           0 item_soh,
           --
           iscl.unit_cost,
           iscl.supplier,
           --
           pct.dept,
           pct.class,
           pct.subclass,
           pct.standard_uom,
           pct.pack_ind,
           pct.sellable_ind,
           pct.orderable_ind,
           pct.pack_type,
           --
           pct.price_chg_type,
           pct.item,
           pct.loc,
           pct.loc_type,
           decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, null, pct.selling_unit_retail),
           decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, null, pct.selling_uom),
           decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, pct.selling_unit_retail, null),
           decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, pct.selling_uom, null),
           pct.multi_units,
           pct.multi_unit_retail,
           pct.multi_selling_uom,
           pct.prom_event,
           pct.currency_code,
           pct.is_on_clearance,
           pct.change_type,
           pct.change_amount,
           pct.pc_reason_code,
           nvl(pct.active_date, LP_vdate) active_date,
           decode(pct.price_chg_type, RMSSUB_PRICECHANGE.CLEARANCE, 'Y', 'N'),
           pct.current_selling_retail,
           pct.current_selling_uom,
           pct.current_clear_retail,
           pct.current_clear_uom,
           pct.current_simple_promo_retail,
           pct.current_simple_promo_uom,
           pct.is_on_simple_promo,
           decode(pct.loc_type,'W','C',s.store_type) wholesale_loc_type
     from  item_loc il,
           item_supp_country_loc iscl,
           store s,
           TABLE(CAST(L_pc_tbl AS OBJ_PRICEEVENT_IL_TBL)) pct
    where  iscl.loc(+) = pct.loc
      and  iscl.origin_country_id(+) = pct.primary_cntry
      and  iscl.supplier(+) = pct.primary_supp
      and  iscl.item(+) = pct.item
      --
      and  il.loc = pct.loc
      and  il.item_parent = pct.item
      --
      and  s.store(+) = pct.loc
      --
      and  pct.change_type is null
--
      and (pct.price_chg_type <> 'PC'
           or nvl(pct.is_on_clearance,0) = 0)

   union all
      select /*+ CARDINALITY(pct, 10) */ pct.current_unit_retail,
             pct.old_selling_unit_retail,
             pct.primary_supp,
             pct.old_selling_uom,
             decode(pct.clear_ind, 'Y', 1, 0),
             pct.il_rowid,
             --
             NULL,
             --
             NULL,
             pct.primary_supp,
             --
             pct.dept,
             pct.class,
             pct.subclass,
             pct.standard_uom,
             pct.pack_ind,
             pct.sellable_ind,
             pct.orderable_ind,
             pct.pack_type,
             --
             pct.price_chg_type,
             pct.item,
             pct.loc,
             pct.loc_type,
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, NULL, pct.selling_unit_retail),
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, NULL, pct.selling_uom),
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, pct.selling_unit_retail, NULL),
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.PROM_ST, pct.selling_uom, NULL),
             pct.multi_units,
             pct.multi_unit_retail,
             pct.multi_selling_uom,
             pct.prom_event,
             pct.currency_code,
             pct.is_on_clearance,
             pct.change_type,
             pct.change_amount,
             pct.pc_reason_code,
             nvl(pct.active_date, LP_vdate) active_date,
             decode(pct.price_chg_type, RMSSUB_PRICECHANGE.CLEARANCE, 'Y', 'N'),
             pct.current_selling_retail,
             pct.current_selling_uom,
             pct.current_clear_retail,
             pct.current_clear_uom,
             pct.current_simple_promo_retail,
             pct.current_simple_promo_uom,
             pct.is_on_simple_promo,
             decode(pct.loc_type,'W','C',s.store_type) wholesale_loc_type
        from store s,
             TABLE(CAST(L_pc_tbl AS OBJ_PRICEEVENT_IL_TBL)) pct
       where s.store(+) = pct.loc
         and pct.change_type is NOT NULL
         and (pct.price_chg_type <> 'PC'
              or nvl(pct.is_on_clearance,0) = 0) ) inner
       order by inner.active_date ASC;

BEGIN


   LP_pc_count := I_message.count;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_so) = FALSE then
      return FALSE;
   end if;

   delete from api_pc_temp;
   delete from api_orig_retail_temp;

   --
   -- Initialize and Size local collections based on number of price changes in the message
   --
   LP_seq.delete;
   LP_item.delete;
   LP_loc.delete;
   LP_loc_type.delete;
   LP_price_chg_type.delete;
   LP_tran_type.delete;
   LP_unit_cost.delete;
   LP_current_unit_retail.delete;
   LP_standard_unit_retail.delete;
   LP_selling_unit_retail.delete;
   LP_selling_uom.delete;
   LP_promo_retail.delete;
   LP_promo_selling_retail.delete;
   LP_promo_selling_uom.delete;
   LP_multi_units.delete;
   LP_multi_unit_retail.delete;
   LP_multi_selling_uom.delete;
   LP_clear_ind.delete;
   LP_prom_event.delete;
   LP_item_loc_rowid.delete;
   LP_primary_supp.delete;
   LP_item_soh.delete;
   LP_currency_code.delete;
   LP_supplier.delete;
   LP_dept.delete;
   LP_class.delete;
   LP_subclass.delete;
   LP_std_uom.delete;
   LP_reason.delete;
   LP_action_date.delete;
   LP_is_on_clearance.delete;
   LP_change_type.delete;
   LP_change_amount.delete;
   L_is_on_clearance.delete;
   L_curr_selling_retail.delete;
   L_selling_uom.delete;
   LP_vat_rate.delete;
   LP_class_vat_ind.delete;

   LP_orig_retail.delete;
   LP_pack_ind.delete;
   LP_sellable_ind.delete;
   LP_orderable_ind.delete;
   LP_pack_type.delete;
   LP_catch_weight_ind.delete;

   LP_current_selling_retail.delete;
   LP_current_selling_uom.delete;
   LP_current_clear_retail.delete;
   LP_current_clear_uom.delete;
   LP_current_simple_promo_retail.delete;
   LP_current_simple_promo_uom.delete;
   LP_is_on_simple_promo.delete;
   LP_current_std_retail.delete;
   LP_current_std_clear_retail.delete;
   LP_current_std_promo_retail.delete;
   --
   LP_current_standard_retail.delete;
   --
   LP_wholesale_loc_type.delete;
   --
   LP_seq.extend(LP_pc_count);
   LP_item.extend(LP_pc_count);
   LP_loc.extend(LP_pc_count);
   LP_loc_type.extend(LP_pc_count);
   LP_price_chg_type.extend(LP_pc_count);
   LP_tran_type.extend(LP_pc_count);
   LP_unit_cost.extend(LP_pc_count);
   LP_current_unit_retail.extend(LP_pc_count);
   LP_standard_unit_retail.extend(LP_pc_count);
   LP_selling_unit_retail.extend(LP_pc_count);
   LP_selling_uom.extend(LP_pc_count);
   LP_promo_retail.extend(LP_pc_count);
   LP_promo_selling_retail.extend(LP_pc_count);
   LP_promo_selling_uom.extend(LP_pc_count);
   LP_multi_units.extend(LP_pc_count);
   LP_multi_unit_retail.extend(LP_pc_count);
   LP_multi_selling_uom.extend(LP_pc_count);
   LP_clear_ind.extend(LP_pc_count);
   LP_prom_event.extend(LP_pc_count);
   LP_item_loc_rowid.extend(LP_pc_count);
   LP_primary_supp.extend(LP_pc_count);
   LP_item_soh.extend(LP_pc_count);
   LP_currency_code.extend(LP_pc_count);
   LP_supplier.extend(LP_pc_count);
   LP_dept.extend(LP_pc_count);
   LP_class.extend(LP_pc_count);
   LP_subclass.extend(LP_pc_count);
   LP_std_uom.extend(LP_pc_count);
   LP_reason.extend(LP_pc_count);
   LP_action_date.extend(LP_pc_count);
   LP_is_on_clearance.extend(LP_pc_count);
   LP_change_type.extend(LP_pc_count);
   LP_change_amount.extend(LP_pc_count);
   L_is_on_clearance.extend(LP_pc_count);
   L_curr_selling_retail.extend(LP_pc_count);
   L_selling_uom.extend(LP_pc_count);
   LP_vat_rate.extend(LP_pc_count);
   LP_class_vat_ind.extend(LP_pc_count);

   LP_orig_retail.extend(LP_pc_count);
   LP_pack_ind.extend(LP_pc_count);
   LP_sellable_ind.extend(LP_pc_count);
   LP_orderable_ind.extend(LP_pc_count);
   LP_pack_type.extend(LP_pc_count);
   LP_catch_weight_ind.extend(LP_pc_count);

   LP_current_selling_retail.extend(LP_pc_count);
   LP_current_selling_uom.extend(LP_pc_count);
   LP_current_clear_retail.extend(LP_pc_count);
   LP_current_clear_uom.extend(LP_pc_count);
   LP_current_simple_promo_retail.extend(LP_pc_count);
   LP_current_simple_promo_uom.extend(LP_pc_count);
   LP_is_on_simple_promo.extend(LP_pc_count);
   LP_current_std_retail.extend(LP_pc_count);
   LP_current_std_clear_retail.extend(LP_pc_count);
   LP_current_std_promo_retail.extend(LP_pc_count);
   --
   LP_current_standard_retail.extend(LP_pc_count);
   --
   LP_wholesale_loc_type.extend(LP_pc_count);
   --

   L_pc_tbl := I_message;

   for i in 1 .. LP_pc_count loop
      if CHECK_REQUIRED_FIELDS(O_error_message,
                               L_pc_tbl(i)) = FALSE then
         return FALSE;
      end if;
   end loop;

   --

   open  C_PC_INFO;
   fetch C_PC_INFO BULK COLLECT into LP_current_unit_retail,
                                     L_curr_selling_retail,
                                     LP_primary_supp,
                                     L_selling_uom,
                                     L_is_on_clearance,
                                     LP_item_loc_rowid,
                                     LP_item_soh,
                                     LP_unit_cost,
                                     LP_supplier,
                                     LP_dept,
                                     LP_class,
                                     LP_subclass,
                                     LP_std_uom,
                                     LP_pack_ind,
                                     LP_sellable_ind,
                                     LP_orderable_ind,
                                     LP_pack_type,
                                     --
                                     LP_price_chg_type,
                                     LP_item,
                                     LP_loc,
                                     LP_loc_type,
                                     LP_selling_unit_retail,
                                     LP_selling_uom,
                                     LP_promo_selling_retail,
                                     LP_promo_selling_uom,
                                     LP_multi_units,
                                     LP_multi_unit_retail,
                                     LP_multi_selling_uom,
                                     LP_prom_event,
                                     LP_currency_code,
                                     LP_is_on_clearance,
                                     LP_change_type,
                                     LP_change_amount,
                                     LP_reason,
                                     LP_action_date,
                                     LP_clear_ind,
                                     LP_current_selling_retail,
                                     LP_current_selling_uom,
                                     LP_current_clear_retail,
                                     LP_current_clear_uom,
                                     LP_current_simple_promo_retail,
                                     LP_current_simple_promo_uom,
                                     LP_is_on_simple_promo,
                                     LP_wholesale_loc_type;
--
   LP_cr_count := C_PC_INFO%ROWCOUNT;
   close C_PC_INFO;

   for i in 1 .. LP_cr_count loop

      LP_seq(i) := i;

      if LP_supplier(i) is NOT NULL and LP_loc(i) is NOT NULL then
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             LP_supplier(i),
                                             'V',
                                             NULL,
                                             LP_loc(i),
                                             LP_loc_type(i),
                                             NULL,
                                             LP_unit_cost(i),
                                             LP_unit_cost(i),
                                             'C',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
      end if;

      -- Populate the Selling Unit Retail fields for parent items that have a price change type
      -- of "by-percent" or "by-amount".
      if LP_change_type(i) is NOT NULL then
         LP_is_on_clearance(i) := L_is_on_clearance(i);
         LP_selling_uom(i) := L_selling_uom(i);

         if LP_change_type(i) = LP_by_percent then
            LP_standard_unit_retail(i) := (LP_current_unit_retail(i) +
                                                    (LP_current_unit_retail(i) * LP_change_amount(i)));
            LP_selling_unit_retail(i) := (L_curr_selling_retail(i) +
                                                   (L_curr_selling_retail(i) * LP_change_amount(i)));
            if CURRENCY_SQL.ROUND_CURRENCY(O_error_message,
                                           LP_standard_unit_retail(i),
                                           LP_currency_code(i),
                                           'R') = FALSE then
               return FALSE;
            end if;

            if CURRENCY_SQL.ROUND_CURRENCY(O_error_message,
                                           LP_selling_unit_retail(i),
                                           LP_currency_code(i),
                                           'R') = FALSE then
               return FALSE;
            end if;

         elsif LP_change_type(i) = LP_by_amount then
            LP_standard_unit_retail(i) := LP_current_unit_retail(i) + LP_change_amount(i);
            LP_selling_unit_retail(i) := L_curr_selling_retail(i) + LP_change_amount(i);
         end if;
      end if;

      --
      -- Determine tran_type to be inserted into price_hist table for each price change
      --
      if DETERMINE_TRAN_TYPE(O_error_message,
                             LP_tran_type(i),
                             L_single_changed,
                             L_multi_changed,
                             LP_price_chg_type(i),
                             LP_selling_unit_retail(i),
                             LP_multi_unit_retail(i),
                             LP_change_type(i),
                             LP_is_on_clearance(i)) = FALSE then
         return FALSE;
      end if;

      --
      -- Standard_unit_retail only needs to be set if single unit retail is changed
      --
      if L_single_changed then
         if LP_selling_uom(i) != LP_std_uom(i) then
            if LP_price_chg_type(i) != RMSSUB_PRICECHANGE.PROM_ST then
               if UOM_SQL.CONVERT(O_error_message,
                                  LP_standard_unit_retail(i),
                                  LP_selling_uom(i),
                                  LP_selling_unit_retail(i),
                                  LP_std_uom(i),
                                  LP_item(i),
                                  NULL,
                                  NULL) = FALSE then
                  return FALSE;
               end if;

               LP_promo_retail(i) := NULL;
            else
               if UOM_SQL.CONVERT(O_error_message,
                                  LP_promo_retail(i),
                                  LP_promo_selling_uom(i),
                                  LP_promo_selling_retail(i),
                                  LP_std_uom(i),
                                  LP_item(i),
                                  NULL,
                                  NULL) = FALSE then
                  return FALSE;
               end if;

               LP_standard_unit_retail(i) := NULL;
            end if;
   --
            if UOM_SQL.CONVERT(O_error_message,
                               LP_current_standard_retail(i),
                               LP_selling_uom(i),
                               LP_current_selling_retail(i),
                               LP_std_uom(i),
                               LP_item(i),
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
   --
            if UOM_SQL.CONVERT(O_error_message,
                               LP_current_std_retail(i),
                               LP_selling_uom(i),
                               LP_current_selling_retail(i),
                               LP_std_uom(i),
                               LP_item(i),
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
   --
            if UOM_SQL.CONVERT(O_error_message,
                               LP_current_std_clear_retail(i),
                               LP_selling_uom(i),
                               LP_current_clear_retail(i),
                               LP_std_uom(i),
                               LP_item(i),
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
   --
            if UOM_SQL.CONVERT(O_error_message,
                               LP_current_std_promo_retail(i),
                               LP_selling_uom(i),
                               LP_current_simple_promo_retail(i),
                               LP_std_uom(i),
                               LP_item(i),
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
         else
            if LP_price_chg_type(i) != RMSSUB_PRICECHANGE.PROM_ST then
               LP_standard_unit_retail(i) := LP_selling_unit_retail(i);
               LP_promo_retail(i) := NULL;
            else
               LP_standard_unit_retail(i) := NULL;
               LP_promo_retail(i) := LP_promo_selling_retail(i);
            end if;
   --
            LP_current_std_retail(i):= LP_current_selling_retail(i);
            LP_current_std_clear_retail(i):= LP_current_clear_retail(i);
            LP_current_std_promo_retail(i):= LP_current_simple_promo_retail(i);
            --
            LP_current_standard_retail(i) := LP_current_selling_retail(i);
         end if;
      end if;
   end loop;

   --

   forall i in 1 .. LP_cr_count
   insert into api_pc_temp(seq_no,
                           tran_type,
                           reason,
                           event,
                           item,
                           dept,
                           class,
                           subclass,
                           loc,
                           loc_type,
                           unit_cost,
                           curr_unit_retail,
                           std_unit_retail,
                           selling_unit_retail,
                           selling_uom,
                           promo_retail,
                           promo_selling_retail,
                           promo_selling_uom,
                           action_date,
                           multi_units,
                           multi_unit_retail,
                           multi_selling_uom,
                           post_date,
                           price_chg_type,
                           currency_code,
                           clear_ind,
                           item_loc_rowid,
                           prim_supp,
                           supplier,
                           item_soh,
                           on_clearance,
                           change_type)
                    values(LP_seq(i),
                           LP_tran_type(i),
                           LP_reason(i),
                           LP_prom_event(i),
                           LP_item(i),
                           LP_dept(i),
                           LP_class(i),
                           LP_subclass(i),
                           LP_loc(i),
                           LP_loc_type(i),
                           LP_unit_cost(i),
                           LP_current_unit_retail(i),
                           LP_standard_unit_retail(i),
                           LP_selling_unit_retail(i),
                           LP_selling_uom(i),
                           LP_promo_retail(i),
                           LP_promo_selling_retail(i),
                           LP_promo_selling_uom(i),
                           LP_action_date(i),
                           LP_multi_units(i),
                           LP_multi_unit_retail(i),
                           LP_multi_selling_uom(i),
                           SYSDATE,
                           LP_price_chg_type(i),
                           LP_currency_code(i),
                           LP_clear_ind(i),
                           LP_item_loc_rowid(i),
                           LP_primary_supp(i),
                           LP_supplier(i),
                           LP_item_soh(i),
                           LP_is_on_clearance(i),
                           LP_change_type(i));

   --

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_PRICE_CHANGE;
--------------------------------------------------------------------------------

FUNCTION PERSIST(O_error_message   OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'RMSSUB_PRICECHANGE_UPDATE.PERSIST';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   L_table              VARCHAR2(100) := NULL;
   L_mark_type          NUMBER        := NULL;
   L_mark_amt           NUMBER        := NULL;
   L_mark_type_2        NUMBER        := NULL;
   L_mark_amt_2         NUMBER        := NULL;
   L_total_cost         NUMBER(20,4)  := NULL;
   L_total_amt          NUMBER(20,4)  := NULL;
   L_ril_rowid          ROWID_TBL;

   L_sd_dept            DEPT_TBL;
   L_sd_supplier        SUPPLIER_TBL;
   L_sd_mark_type_tbl   TRAN_TYPE_TBL;
   L_sd_mark_amt_tbl    UNIT_RETAIL_TBL;

   L_sd_action_date     DATE_TBL;
   L_repl_action_date   DATE_TBL;
   L_sup_data_count     INTEGER := 1;
   L_average_weight     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_nom_weight         ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;

   cursor C_LOCK_PH is
      select 'x'
        from price_hist ph,
             api_pc_temp apt
       where ph.item = apt.item
         and ph.loc = apt.loc
         and ph.tran_type = apt.tran_type
         and ph.action_date = apt.action_date
         for update of ph.item nowait;

   cursor C_LOCK_REPL is
      select ril.rowid,
             apt.action_date
        from repl_item_loc ril,
             api_pc_temp apt
       where ril.item = apt.item
         and ril.location = apt.loc
         and ril.loc_type = apt.loc_type
         and apt.price_chg_type = RMSSUB_PRICECHANGE.CLEARANCE
         and apt.action_date between ril.activate_date and NVL(ril.deactivate_date,(apt.action_date + 1))
         for update of ril.item nowait;

   cursor C_CATCH_WEIGHT_IND is
      select im.catch_weight_ind
        from item_master im,
             api_pc_temp apt
       where im.item = apt.item;

cursor C_VAT is
  select v.vat_rate,
         v.class_vat_ind
    from api_pc_temp apt,
         (select /*+ cardinality(apt, 10) */
                 distinct apt.item,
                 apt.loc loc,
                 apt.action_date,
                 --
                 first_value(vi.vat_rate) over
                 (partition by apt.item, apt.loc order by vi.active_date desc) vat_rate,
                 --
                 c.class_vat_ind
            from api_pc_temp apt,
                 (select s.store loc, s.vat_region from store s
                  union all
                  select w.wh loc, w.vat_region from wh w) l,
                 vat_item vi,
                 class c
           where c.class = apt.class
             and c.dept = apt.dept
             --
             and vi.vat_type in ('R', 'B')
             and vi.vat_region = l.vat_region
             and vi.active_date <= apt.action_date
             and vi.item = apt.item
             --
             and l.loc = apt.loc) v
   where apt.loc = v.loc (+)
     and apt.item = v.item (+)
order by apt.seq_no;


BEGIN

   --

   L_sd_dept := DEPT_TBL();
   L_sd_supplier := SUPPLIER_TBL();
   L_sd_mark_type_tbl := TRAN_TYPE_TBL();
   L_sd_mark_amt_tbl := UNIT_RETAIL_TBL();
   L_sd_action_date := DATE_TBL();
   L_repl_action_date := DATE_TBL();
   --

   L_table := 'ITEM_LOC';

   RMSSUB_PRICECHANGE_UPDATE.SKIP_STAGE_ITEM_LOC := 'Y';

   forall i in 1 .. LP_cr_count
      -- update item_loc table including the
      -- multi_unit retail and regular_unit retail columns
      -- for permanent price changes
      update item_loc
         set unit_retail = nvl(LP_current_std_clear_retail(i), unit_retail),
             selling_unit_retail = nvl(LP_current_clear_retail(i), selling_unit_retail),
             selling_uom = nvl(LP_current_clear_uom(i), selling_uom),
             --
             multi_units = nvl(LP_multi_units(i), multi_units),
             multi_unit_retail = nvl(LP_multi_unit_retail(i), multi_unit_retail),
             multi_selling_uom = nvl(LP_multi_selling_uom(i), multi_selling_uom),
             regular_unit_retail = nvl(LP_current_std_retail(i), unit_retail),
             --
             promo_retail = decode(LP_is_on_simple_promo(i), 1, LP_current_std_promo_retail(i), NULL),
             promo_selling_retail = decode(LP_is_on_simple_promo(i), 1, LP_current_simple_promo_retail(i), NULL),
             promo_selling_uom = decode(LP_is_on_simple_promo(i), 1, LP_current_simple_promo_uom(i), NULL),
             clear_ind = DECODE(LP_price_chg_type(i),RMSSUB_PRICECHANGE.PROM_ST, clear_ind, DECODE(LP_is_on_clearance(i),1,'Y','N')),
             --
             last_update_datetime = SYSDATE,
             last_update_id = LP_user
       where rowid = LP_item_loc_rowid(i);

   RMSSUB_PRICECHANGE_UPDATE.SKIP_STAGE_ITEM_LOC := 'N';

   --
   
   L_table := 'TRAN_DATA';

   if STKLEDGR_SQL.INIT_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   if POPULATE_ORIG_RETAIL(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   if LP_so.default_tax_type = 'SVAT' then
      open C_VAT;
      fetch C_VAT bulk collect into LP_vat_rate,
                                    LP_class_vat_ind;
      close C_VAT;
   end if;
   --

   open C_CATCH_WEIGHT_IND;
   fetch C_CATCH_WEIGHT_IND bulk collect into LP_catch_weight_ind;
   close C_CATCH_WEIGHT_IND;

   for i in 1 .. LP_cr_count loop
      if LP_selling_unit_retail(i) is NOT NULL and
         LP_item_soh(i) <> 0 and
         LP_change_type(i) is NULL and
         LP_price_chg_type(i) in (RMSSUB_PRICECHANGE.PERM_PC, RMSSUB_PRICECHANGE.CLEARANCE, RMSSUB_PRICECHANGE.CL_RESET) then
         if DETERMINE_MARK(LP_orig_retail(i),
                           LP_current_unit_retail(i),
                           LP_standard_unit_retail(i),
                           --
                           L_mark_type,
                           L_mark_amt,
                           L_mark_type_2,
                           L_mark_amt_2,
                           O_error_message) = 'FALSE' then
            return FALSE;
         end if;

         --

         if L_mark_type is NOT NULL and L_mark_type != 0 and L_mark_amt != 0 then
            if LP_price_chg_type(i) = RMSSUB_PRICECHANGE.CLEARANCE then
               if L_mark_type = 13 then
                  L_mark_type := 16;
               end if;
            end if;

            L_total_cost := NULL;
            if LP_catch_weight_ind(i) = 'Y' and LP_std_uom(i) = 'EA' then
               if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                            L_average_weight,
                                                            LP_item(i),
                                                            LP_loc(i),
                                                            LP_loc_type(i)) then
                  return FALSE;
               end if;

               if NOT ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                                   L_nom_weight,
                                                                   LP_item(i)) then
                  return FALSE;
               end if;

               if L_average_weight is NULL then
                  L_average_weight := L_nom_weight;
               end if;
               L_total_amt := ((L_mark_amt) * (LP_item_soh(i)) * (L_average_weight / L_nom_weight));
            else
               L_total_amt := ((L_mark_amt) * (LP_item_soh(i)));
            end if;

            if LP_wholesale_loc_type(i) in ('C','F') and LP_pack_ind(i) = 'N' then
               if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                      LP_item(i),
                                                      LP_dept(i),
                                                      LP_class(i),
                                                      LP_subclass(i),
                                                      LP_loc(i),
                                                      LP_loc_type(i),
                                                      LP_action_date(i),
                                                      L_mark_type,
                                                      NULL,
                                                      LP_item_soh(i),
                                                      L_total_cost,
                                                      L_total_amt,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      LP_current_unit_retail(i),
                                                      LP_standard_unit_retail(i),
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      L_program,
                                                      --
                                                      NULL,
                                                      LP_pack_ind(i),
                                                      LP_sellable_ind(i),
                                                      LP_orderable_ind(i),
                                                      LP_pack_type(i),
                                                      LP_vat_rate(i),
                                                      LP_class_vat_ind(i)) = FALSE then
                  return FALSE;
               end if;
            end if;

            --

            if LP_primary_supp(i) is NOT NULL then
               L_sd_dept.EXTEND;
               L_sd_supplier.EXTEND;
               L_sd_mark_type_tbl.EXTEND;
               L_sd_mark_amt_tbl.EXTEND;
               L_sd_action_date.EXTEND;

               L_sd_dept(L_sup_data_count) := LP_dept(i);
               L_sd_supplier(L_sup_data_count) := LP_supplier(i);
               L_sd_mark_type_tbl(L_sup_data_count) := LP_sd_mark_type;
               L_sd_mark_amt_tbl(L_sup_data_count) := L_mark_amt;
               L_sd_action_date(L_sup_data_count) := LP_action_date(i);

               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_mark_amt,
                                       LP_currency_code(i),
                                       NULL,
                                       L_sd_mark_amt_tbl(L_sup_data_count),
                                       'R',
                                       NULL,
                                       NULL) = FALSE then
                  return FALSE;
               end if;

               L_sup_data_count := L_sup_data_count + 1;
            end if;
         end if;

         if L_mark_type_2 is NOT NULL and L_mark_type_2 != 0  and L_mark_amt_2 != 0 then
           if LP_price_chg_type(i) = RMSSUB_PRICECHANGE.CLEARANCE then
               if L_mark_type_2 = 13 then
                     L_mark_type_2 := 16;
               end if;
            end if;

            L_total_cost := NULL;
            if LP_catch_weight_ind(i) = 'Y' and LP_std_uom(i) = 'EA' then
               if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                            L_average_weight,
                                                            LP_item(i),
                                                            LP_loc(i),
                                                            LP_loc_type(i)) then
                  return FALSE;
               end if;

               if NOT ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                                   L_nom_weight,
                                                                   LP_item(i)) then
                  return FALSE;
               end if;

               if L_average_weight is NULL then
                  L_average_weight := L_nom_weight;
               end if;
               L_total_amt := ((L_mark_amt_2) * (LP_item_soh(i)) * (L_average_weight / L_nom_weight));
            else
               L_total_amt := ((L_mark_amt_2) * (LP_item_soh(i)));
            end if;
            if LP_wholesale_loc_type(i) in ('C','F') and LP_pack_ind(i) = 'N' then
               if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                      LP_item(i),
                                                      LP_dept(i),
                                                      LP_class(i),
                                                      LP_subclass(i),
                                                      LP_loc(i),
                                                      LP_loc_type(i),
                                                      LP_action_date(i),
                                                      L_mark_type_2,
                                                      NULL,
                                                      LP_item_soh(i),
                                                      L_total_cost,
                                                      L_total_amt,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      LP_current_unit_retail(i),
                                                      LP_standard_unit_retail(i),
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      L_program,
                                                      --
                                                      NULL,
                                                      LP_pack_ind(i),
                                                      LP_sellable_ind(i),
                                                      LP_orderable_ind(i),
                                                      LP_pack_type(i),
                                                      LP_vat_rate(i),
                                                      LP_class_vat_ind(i)) = FALSE then
                  return FALSE;
               end if;
           end if;
            --

            if LP_primary_supp(i) is NOT NULL then
               L_sd_dept.EXTEND;
               L_sd_supplier.EXTEND;
               L_sd_mark_type_tbl.EXTEND;
               L_sd_mark_amt_tbl.EXTEND;
            L_sd_action_date.EXTEND;

               L_sd_dept(L_sup_data_count) := LP_dept(i);
               L_sd_supplier(L_sup_data_count) := LP_supplier(i);
               L_sd_mark_type_tbl(L_sup_data_count) := LP_sd_mark_type;
               L_sd_mark_amt_tbl(L_sup_data_count) := L_mark_amt_2;
            L_sd_action_date(L_sup_data_count) := LP_action_date(i);

               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_mark_amt_2,
                                       LP_currency_code(i),
                                       NULL,
                                       L_sd_mark_amt_tbl(L_sup_data_count),
                                       'R',
                                       NULL,
                                       NULL) = FALSE then
                  return FALSE;
               end if;

               L_sup_data_count := L_sup_data_count + 1;
            end if;
         end if;
      end if;
   end loop;

   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   --

   --


   L_table := 'PRICE_HIST';

   open  C_LOCK_PH;
   close C_LOCK_PH;

   forall i in 1 .. LP_cr_count
   MERGE INTO price_hist ph
      USING dual
         ON (   DECODE(LP_tran_type(i),11,4,LP_tran_type(i)) = ph.tran_type
             and LP_action_date(i) = ph.action_date
             and LP_loc(i) = ph.loc
             and LP_item(i) = ph.item)
         WHEN MATCHED THEN
         update
            set ph.reason = decode(LP_tran_type(i), 4, NVL(LP_reason(i),ph.reason),LP_reason(i)),
                ph.event =  decode(LP_tran_type(i), 4, decode(LP_reason(i),NULL,ph.event, LP_prom_event(i)),
                                                       LP_prom_event(i)),
                ph.unit_cost = decode(LP_tran_type(i), 4, decode(LP_reason(i),NULL,ph.unit_cost, LP_unit_cost(i)),
                                                          LP_unit_cost(i)),
                ph.unit_retail = decode(LP_tran_type(i), 4, decode(LP_reason(i),NULL,ph.unit_retail, LP_current_standard_retail(i)),
                                                         8, LP_current_std_clear_retail(i), 
                                                         9, LP_promo_retail(i), 
                                                            LP_current_standard_retail(i)),
                ph.selling_unit_retail = decode(LP_tran_type(i), 4, decode(LP_reason(i),NULL,ph.selling_unit_retail, LP_current_selling_retail(i)),
                                                                 8, LP_current_clear_retail(i), 
                                                                 9, LP_current_simple_promo_retail(i), 
                                                                    LP_current_selling_retail(i)),
                ph.selling_uom = decode(LP_tran_type(i), 4, decode(LP_reason(i),NULL,ph.selling_uom, LP_current_selling_uom(i)),
                                                         8, LP_current_clear_uom(i), 
                                                         9, LP_current_simple_promo_uom(i), 
                                                            LP_current_selling_uom(i)),
                ph.multi_units = decode(LP_tran_type(i), 4, decode(LP_reason(i),NULL,ph.multi_units, LP_multi_units(i)),
                                                        11, NULL, 
                                                            LP_multi_units(i)),
                ph.multi_unit_retail = decode(LP_tran_type(i), 4, decode(LP_reason(i),NULL,ph.multi_unit_retail, LP_multi_unit_retail(i)),
                                                              11, NULL, 
                                                                  LP_multi_unit_retail(i)),
                ph.multi_selling_uom = decode(LP_tran_type(i), 4, decode(LP_reason(i),NULL,ph.multi_selling_uom, LP_multi_selling_uom(i)),
                                                              11, NULL, 
                                                                  LP_multi_selling_uom(i))
         WHEN NOT MATCHED THEN
         insert (tran_type,
                 reason,
                 event,
                 item,
                 loc,
                 loc_type,
                 unit_cost,
                 unit_retail,
                 selling_unit_retail,
                 selling_uom,
                 action_date,
                 multi_units,
                 multi_unit_retail,
                 multi_selling_uom,
                 post_date)
         values (DECODE(LP_tran_type(i),11,4,LP_tran_type(i)),
                 LP_reason(i),
                 LP_prom_event(i),
                 LP_item(i),
                 decode( decode(LP_tran_type(i), 8, 1, 0) +
                 decode(LP_is_on_clearance(i), 1, 1, 0) +
                 decode(LP_price_chg_type(i), RMSSUB_PRICECHANGE.PERM_PC, 1, 0), 3, -999, LP_loc(i)),
                 LP_loc_type(i),
                 LP_unit_cost(i),
                 decode(LP_tran_type(i), 8, LP_current_std_clear_retail(i), 9, LP_promo_retail(i), LP_current_standard_retail(i)),
                 decode(LP_tran_type(i), 8, LP_current_clear_retail(i), 9, LP_current_simple_promo_retail(i), LP_current_selling_retail(i)),
                 decode(LP_tran_type(i), 8, LP_current_clear_uom(i), 9, LP_current_simple_promo_uom(i), LP_current_selling_uom(i)),
                 case 
                 when LP_action_date(i) < LP_vdate then (select greatest( LP_action_date(i), nvl( MIN(ph2.action_date), LP_action_date(i)))
                                                           from price_hist ph2
                                                          where ph2.item      = LP_item(i)
                                                            and ph2.loc       = LP_loc(i)
                                                            and ph2.tran_type = 0 )                
                 else LP_action_date(i)
                 end,
                 DECODE(LP_tran_type(i),11,NULL,LP_multi_units(i)),
                 DECODE(LP_tran_type(i),11,NULL,LP_multi_unit_retail(i)),
                 DECODE(LP_tran_type(i),11,NULL,LP_multi_selling_uom(i)),
                 SYSDATE);

  /* for inserting tran_type 9 when clearance reset is being processed and items are still on active promotion during that period */
     forall i in 1 .. LP_cr_count
   MERGE INTO price_hist ph
      USING (
             SELECT 9 promo_tran_type,
                    LP_prom_event(i) prom_event_temp,
                    LP_item(i) item_temp,
                    LP_price_chg_type(i) price_chg_type_temp,
                    LP_loc(i) loc_temp,
                    LP_loc_type(i) loc_type_temp,
                    LP_unit_cost(i) unit_cost_temp,
                    LP_current_std_promo_retail(i) current_std_promo_retail_temp,
                    LP_current_simple_promo_retail(i) current_sim_promo_retail_temp,
                    LP_current_simple_promo_uom(i) current_simple_promo_uom_temp,
                    LP_action_date(i) action_date_temp,
                    LP_multi_units(i) multi_units_temp,
                    LP_multi_unit_retail(i) multi_unit_retail_temp,
                    LP_multi_selling_uom(i) multi_selling_uom_temp,
                    LP_is_on_simple_promo(i) is_on_simple_promo_temp
               FROM dual
              WHERE LP_is_on_simple_promo(i) = 1
                AND LP_tran_type(i) = 4
                AND LP_price_chg_type(i) = RMSSUB_PRICECHANGE.CL_RESET
             )
         ON (    promo_tran_type = ph.tran_type
             and action_date_temp = ph.action_date
             and loc_temp = ph.loc
             and item_temp = ph.item
             )
         WHEN MATCHED THEN
         update
            set ph.event =  prom_event_temp,
                ph.unit_cost = unit_cost_temp,
                ph.unit_retail = current_std_promo_retail_temp,
                ph.selling_unit_retail = current_sim_promo_retail_temp,
                ph.selling_uom = current_simple_promo_uom_temp,
                ph.multi_units = multi_units_temp,
                ph.multi_unit_retail = multi_unit_retail_temp,
                ph.multi_selling_uom = multi_selling_uom_temp 
         WHEN NOT MATCHED THEN
         insert (tran_type,
                 reason,
                 event,
                 item,
                 loc,
                 loc_type,
                 unit_cost,
                 unit_retail,
                 selling_unit_retail,
                 selling_uom,
                 action_date,
                 multi_units,
                 multi_unit_retail,
                 multi_selling_uom,
                 post_date)
         values (promo_tran_type,
                 NULL,
                 prom_event_temp,
                 item_temp,
                 loc_temp,
                 loc_type_temp,
                 unit_cost_temp,
                 current_std_promo_retail_temp,
                 current_sim_promo_retail_temp,
                 current_simple_promo_uom_temp,
                 case
                 when action_date_temp < LP_vdate then (select greatest( action_date_temp, nvl( MIN(ph2.action_date), action_date_temp))
                                                           from price_hist ph2
                                                          where ph2.item      = item_temp
                                                            and ph2.loc       = loc_temp
                                                            and ph2.tran_type = 0 )
                 else action_date_temp
                 end,
                 multi_units_temp,
                 multi_unit_retail_temp,
                 multi_selling_uom_temp,
                 SYSDATE);   
   
   forall i in 1 .. LP_cr_count
      MERGE INTO price_hist ph
         USING dual
            ON (    LP_tran_type(i) = ph.tran_type
                and LP_action_date(i) = ph.action_date
                and LP_loc(i) = ph.loc
                and LP_item(i) = ph.item)
            WHEN NOT MATCHED THEN
            insert (tran_type,
                    reason,
                    event,
                    item,
                    loc,
                    loc_type,
                    unit_cost,
                    unit_retail,
                    selling_unit_retail,
                    selling_uom,
                    action_date,
                    multi_units,
                    multi_unit_retail,
                    multi_selling_uom,
                    post_date)
            values (DECODE(LP_tran_type(i),11,10,LP_tran_type(i)),
                    LP_reason(i),
                    LP_prom_event(i),
                    LP_item(i),
                    decode( decode(LP_tran_type(i), 8, 1, 0) +
                    decode(LP_is_on_clearance(i), 1, 1, 0) +
                    decode(LP_price_chg_type(i), RMSSUB_PRICECHANGE.PERM_PC, 1, 0), 3, -999, LP_loc(i)),
                    LP_loc_type(i),
                    LP_unit_cost(i),
                    NULL,
                    NULL,
                    NULL,
                    LP_action_date(i),
                    LP_multi_units(i),
                    LP_multi_unit_retail(i),
                    LP_multi_selling_uom(i),
                    SYSDATE)
               where LP_tran_type(i) = 11;
               
   /*for emergency price changes insert into the temp table which is used by ordupd.pc to update orders*/
   forall i in 1 .. LP_cr_count
      MERGE INTO emer_price_hist eph
         USING dual
            ON (    LP_tran_type(i) = eph.tran_type
                and LP_action_date(i) = eph.action_date
                and LP_loc(i) = eph.loc
                and LP_item(i) = eph.item
               )
            WHEN MATCHED THEN
            update
               set eph.unit_retail = decode(LP_tran_type(i), 8, LP_current_std_clear_retail(i), 9, LP_promo_retail(i), LP_current_standard_retail(i))
            WHEN NOT MATCHED THEN
            insert (tran_type,
                    item,
                    loc,
                    loc_type,
                    unit_retail,
                    action_date)
            values (LP_tran_type(i),
                    LP_item(i),
                    decode( decode(LP_tran_type(i), 8, 1, 0) +
                    decode(LP_is_on_clearance(i), 1, 1, 0) +
                    decode(LP_price_chg_type(i), RMSSUB_PRICECHANGE.PERM_PC, 1, 0),3, -999, LP_loc(i)),
                    LP_loc_type(i),
                    decode(LP_tran_type(i), 8, LP_current_std_clear_retail(i), 9, LP_promo_retail(i), LP_current_standard_retail(i)),
                    decode(LP_tran_type(i),4,decode(LP_price_chg_type(i), RMSSUB_PRICECHANGE.PROM_END,decode(LP_vdate+1,LP_action_date(i),LP_action_date(i),LP_vdate),LP_action_date(i)),LP_action_date(i))
                   )
               where LP_vdate = LP_action_date(i);
   --                 

   delete from price_hist WHERE loc = '-999';
   delete from emer_price_hist WHERE loc = '-999';
   
   --

   L_table := 'REPL_ITEM_LOC';

   open  C_LOCK_REPL;
   fetch C_LOCK_REPL BULK COLLECT into L_ril_rowid,
                                       L_repl_action_date;
   close C_LOCK_REPL;

   forall i in 1 .. L_ril_rowid.COUNT
      update repl_item_loc
         set deactivate_date = L_repl_action_date(i)
      where rowid = L_ril_rowid(i);


   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
--------------------------------------------------------------------------------

/* Formatted on 2007/02/26 17:36 (Formatter Plus v4.8.5) */
FUNCTION PROCESS_VFM (O_ERROR_MESSAGE IN OUT RTK_ERRORS.RTK_TEXT%TYPE, I_MESSAGE IN OBJ_PRICEEVENT_IL_TBL)
    RETURN BOOLEAN IS
    L_PROGRAM                        VARCHAR2 (50)                                     := 'RMSSUB_PRICECHANGE_UPDATE.PROCESS_VFM';
    L_MESSAGE                        OBJ_PRICEEVENT_IL_REC                             := NULL;
    L_VDATE                          DATE;
    L_REPORTING_DATE                 DATE;
    L_DEAL_ID                        DEAL_HEAD.DEAL_ID%TYPE;
    L_DEAL_DETAIL_ID                 DEAL_DETAIL.DEAL_DETAIL_ID%TYPE;
    L_VALID_DEAL                     VARCHAR2 (1);
    L_VALID_DETAIL                   VARCHAR2 (1);
    L_VALID_ITEMLOC                  VARCHAR2 (1);
    L_VALID_SUPPLIER                 VARCHAR2 (1);
    L_VALID_REPORTING_DATE           VARCHAR2 (1);
    L_ORIGIN_COUNTRY_ID              DEAL_ITEMLOC.ORIGIN_COUNTRY_ID%TYPE;
    L_DAIL_IDX                       NUMBER;
    L_ON_HAND_INV                    NUMBER;
    L_ACTUAL_INCOME                  NUMBER;
    L_PRICE_DIFF                     NUMBER;
    L_REF_NO_2                       TRAN_DATA.REF_NO_2%TYPE;
    L_TRAN_CODE                      TRAN_DATA.TRAN_CODE%TYPE;
    L_TOTAL_COST                     TRAN_DATA.TOTAL_COST%TYPE;
    --- Deal Head ---
    L_PARTNER_TYPE                   DEAL_HEAD.PARTNER_TYPE%TYPE;
    L_PARTNER_ID                     DEAL_HEAD.PARTNER_ID%TYPE;
    L_SUPPLIER                       DEAL_HEAD.SUPPLIER%TYPE;
    L_TYPE                           DEAL_HEAD.TYPE%TYPE;
    L_STATUS                         DEAL_HEAD.STATUS%TYPE;
    L_CURRENCY_CODE                  DEAL_HEAD.CURRENCY_CODE%TYPE;
    L_ACTIVE_DATE                    DEAL_HEAD.ACTIVE_DATE%TYPE;
    L_CLOSE_DATE                     DEAL_HEAD.CLOSE_DATE%TYPE;
    L_CREATE_ID                      DEAL_HEAD.CREATE_ID%TYPE;
    L_EXT_REF_NO                     DEAL_HEAD.EXT_REF_NO%TYPE;
    L_ORDER_NO                       DEAL_HEAD.ORDER_NO%TYPE;
    L_BILLING_TYPE                   DEAL_HEAD.BILLING_TYPE%TYPE;
    L_BILL_BACK_PERIOD               DEAL_HEAD.BILL_BACK_PERIOD%TYPE;
    L_DEAL_APPL_TIMING               DEAL_HEAD.DEAL_APPL_TIMING%TYPE;
    L_THRESHOLD_LIMIT_TYPE           DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE;
    L_THRESHOLD_LIMIT_UOM            DEAL_HEAD.THRESHOLD_LIMIT_UOM%TYPE;
    L_REBATE_IND                     DEAL_HEAD.REBATE_IND%TYPE;
    L_REBATE_CALC_TYPE               DEAL_HEAD.REBATE_CALC_TYPE%TYPE;
    L_GROWTH_REBATE_IND              DEAL_HEAD.GROWTH_REBATE_IND%TYPE;
    L_TRACK_PACK_LEVEL_IND           DEAL_HEAD.TRACK_PACK_LEVEL_IND%TYPE;
    L_HIST_COMP_START_DATE           DEAL_HEAD.HISTORICAL_COMP_START_DATE%TYPE;
    L_HIST_COMP_END_DATE             DEAL_HEAD.HISTORICAL_COMP_END_DATE%TYPE;
    L_REBATE_PURCH_SALES_IND         DEAL_HEAD.REBATE_PURCH_SALES_IND%TYPE;
    L_DEAL_REPORTING_LEVEL           DEAL_HEAD.DEAL_REPORTING_LEVEL%TYPE;
    L_BILL_BACK_METHOD               DEAL_HEAD.BILL_BACK_METHOD%TYPE;
    L_DEAL_INCOME_CALC               DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE;
    L_INVOICE_PROC_LOGIC             DEAL_HEAD.INVOICE_PROCESSING_LOGIC%TYPE;
    L_STOCK_LEDGER_IND               DEAL_HEAD.STOCK_LEDGER_IND%TYPE;
    L_INCLUDE_VAT_IND                DEAL_HEAD.INCLUDE_VAT_IND%TYPE;
    L_BILLING_PARTNER_TYPE           DEAL_HEAD.BILLING_PARTNER_TYPE%TYPE;
    L_BILLING_PARTNER_ID             DEAL_HEAD.BILLING_PARTNER_ID%TYPE;
    L_BILLING_SUPPLIER_ID            DEAL_HEAD.BILLING_SUPPLIER_ID%TYPE;
    L_GROWTH_RATE_TO_DATE            DEAL_HEAD.GROWTH_RATE_TO_DATE%TYPE;
    L_TURNOVER_TO_DATE               DEAL_HEAD.TURNOVER_TO_DATE%TYPE;
    L_ACTUAL_MONIES_EARNED_TO_DATE   DEAL_HEAD.ACTUAL_MONIES_EARNED_TO_DATE%TYPE;
    L_SECURITY_IND                   DEAL_HEAD.SECURITY_IND%TYPE;
    L_EST_NEXT_INVOICE_DATE          DEAL_HEAD.EST_NEXT_INVOICE_DATE%TYPE;
    L_LAST_INVOICE_DATE              DEAL_HEAD.LAST_INVOICE_DATE%TYPE;
    --- Deal Detail ---
    L_DEAL_COMP_TYPE                 DEAL_DETAIL.DEAL_COMP_TYPE%TYPE;
    L_APPLICATION_ORDER              DEAL_DETAIL.APPLICATION_ORDER%TYPE;
    L_COLLECT_START_DATE             DEAL_DETAIL.COLLECT_START_DATE%TYPE;
    L_COLLECT_END_DATE               DEAL_DETAIL.COLLECT_END_DATE%TYPE;
    L_COST_APPL_IND                  DEAL_DETAIL.COST_APPL_IND%TYPE;
    L_DEAL_CLASS                     DEAL_DETAIL.DEAL_CLASS%TYPE;
    L_TRAN_DISCOUNT_IND              DEAL_DETAIL.TRAN_DISCOUNT_IND%TYPE;
    L_CALC_TO_ZERO_IND               DEAL_DETAIL.CALC_TO_ZERO_IND%TYPE;
    L_TAL_FORECAST_UNITS             DEAL_DETAIL.TOTAL_FORECAST_UNITS%TYPE;
    L_TAL_FORECAST_REVENUE           DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE;
    L_TAL_BUDGET_TURNOVER            DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE;
    L_TAL_ACTUAL_FORECAST_TURNOVER   DEAL_DETAIL.TOTAL_ACTUAL_FORECAST_TURNOVER%TYPE;
    L_TAL_BASELINE_GROWTH_BUDGET     DEAL_DETAIL.TOTAL_BASELINE_GROWTH_BUDGET%TYPE;
    L_TAL_BASELINE_GROWTH_ACT_FOR    DEAL_DETAIL.TOTAL_BASELINE_GROWTH_ACT_FOR%TYPE;
    L_DEAL_ACTUALS_ITEM_LOC_REC      DEAL_ACTUALS_ITEM_LOC_REC;
    L_DEAL_ACTUALS_ITEM_LOC_TBL      DEAL_ACTUALS_ITEM_LOC_TBL                         := DEAL_ACTUALS_ITEM_LOC_TBL ();

    CURSOR C_VFM IS
        SELECT   PC.ITEM,
                 PC.LOC,
                 PC.LOC_TYPE,
                 PC.SELLING_UNIT_RETAIL,
                 PC.SELLING_UOM,
                 PC.MULTI_UNITS,
                 PC.MULTI_UNIT_RETAIL,
                 PC.MULTI_SELLING_UOM,
                 PC.PROM_EVENT,
                 PC.PRICE_CHG_TYPE,
                 PC.CURRENCY_CODE,
                 PC.OLD_SELLING_UNIT_RETAIL,
                 PC.OLD_SELLING_UOM,
                 PC.VENDOR_FUNDED_IND,
                 PC.FUNDING_TYPE,
                 PC.FUNDING_AMOUNT,
                 PC.FUNDING_AMOUNT_CURRENCY,
                 PC.FUNDING_PERCENT,
                 PC.DEAL_ID,
                 PC.DEAL_DETAIL_ID,
                 PC.DEPT,
                 PC.CLASS,
                 PC.SUBCLASS,
                 PC.PRICE_EVENT_ID,
                 PC.ORIG_EVENT_TYPE
            FROM TABLE (CAST (I_MESSAGE AS OBJ_PRICEEVENT_IL_TBL)) PC
           WHERE PRICE_CHG_TYPE IN (RMSSUB_PRICECHANGE.PERM_PC, RMSSUB_PRICECHANGE.CLEARANCE)
             AND VENDOR_FUNDED_IND = LP_VENDOR_FUNDED_PC
        ORDER BY DEAL_ID,
                 DEAL_DETAIL_ID;
BEGIN
    L_VDATE := GET_VDATE ();

    FOR REC IN C_VFM
    LOOP
        L_VALID_DEAL := 'N';
        L_VALID_DETAIL := 'N';
        L_VALID_ITEMLOC := 'N';
        L_VALID_REPORTING_DATE := 'N';
        IF L_DEAL_ID IS NULL OR L_DEAL_ID != REC.DEAL_ID THEN
            ---
            L_DEAL_ID := REC.DEAL_ID;
            L_DEAL_DETAIL_ID := REC.DEAL_DETAIL_ID;
            ---
            L_VALID_DEAL := 'Y';
            L_VALID_DETAIL := 'Y';
            ---
            IF NOT DEAL_ATTRIB_SQL.GET_HEADER_INFO (O_ERROR_MESSAGE,
                                                    L_PARTNER_TYPE,
                                                    L_PARTNER_ID,
                                                    L_SUPPLIER,
                                                    L_TYPE,
                                                    L_STATUS,
                                                    L_CURRENCY_CODE,
                                                    L_ACTIVE_DATE,
                                                    L_CLOSE_DATE,
                                                    L_CREATE_ID,
                                                    L_EXT_REF_NO,
                                                    L_ORDER_NO,
                                                    L_BILLING_TYPE,
                                                    L_BILL_BACK_PERIOD,
                                                    L_DEAL_APPL_TIMING,
                                                    L_THRESHOLD_LIMIT_TYPE,
                                                    L_THRESHOLD_LIMIT_UOM,
                                                    L_REBATE_IND,
                                                    L_REBATE_CALC_TYPE,
                                                    L_GROWTH_REBATE_IND,
                                                    L_HIST_COMP_START_DATE,
                                                    L_HIST_COMP_END_DATE,
                                                    L_REBATE_PURCH_SALES_IND,
                                                    L_DEAL_REPORTING_LEVEL,
                                                    L_BILL_BACK_METHOD,
                                                    L_DEAL_INCOME_CALC,
                                                    L_INVOICE_PROC_LOGIC,
                                                    L_STOCK_LEDGER_IND,
                                                    L_INCLUDE_VAT_IND,
                                                    L_BILLING_PARTNER_TYPE,
                                                    L_BILLING_PARTNER_ID,
                                                    L_BILLING_SUPPLIER_ID,
                                                    L_GROWTH_RATE_TO_DATE,
                                                    L_TURNOVER_TO_DATE,
                                                    L_ACTUAL_MONIES_EARNED_TO_DATE,
                                                    L_SECURITY_IND,
                                                    L_EST_NEXT_INVOICE_DATE,
                                                    L_LAST_INVOICE_DATE,
                                                    L_TRACK_PACK_LEVEL_IND,
                                                    L_DEAL_ID
                                                   ) THEN
                IF O_ERROR_MESSAGE = SQL_LIB.CREATE_MSG ('INVALID_DEAL_ID',
                                                         NULL,
                                                         NULL,
                                                         NULL
                                                        ) THEN
                    L_VALID_DEAL := 'N';
                    L_VALID_DETAIL := 'N';
                ELSE
                    RETURN FALSE;
                END IF;
            END IF;
            IF L_STATUS != 'A' OR (L_VDATE + 1) < L_ACTIVE_DATE OR L_VDATE > L_CLOSE_DATE THEN
                L_VALID_DEAL := 'N';
                L_VALID_DETAIL := 'N';
            END IF;
            IF L_VALID_DEAL = 'Y' THEN
                IF NOT DEAL_ATTRIB_SQL.DEAL_DETAIL_INFO (O_ERROR_MESSAGE,
                                                         L_DEAL_COMP_TYPE,
                                                         L_APPLICATION_ORDER,
                                                         L_COLLECT_START_DATE,
                                                         L_COLLECT_END_DATE,
                                                         L_COST_APPL_IND,
                                                         L_DEAL_CLASS,
                                                         L_TRAN_DISCOUNT_IND,
                                                         L_CALC_TO_ZERO_IND,
                                                         L_TAL_FORECAST_UNITS,
                                                         L_TAL_FORECAST_REVENUE,
                                                         L_TAL_BUDGET_TURNOVER,
                                                         L_TAL_ACTUAL_FORECAST_TURNOVER,
                                                         L_TAL_BASELINE_GROWTH_BUDGET,
                                                         L_TAL_BASELINE_GROWTH_ACT_FOR,
                                                         L_DEAL_ID,
                                                         L_DEAL_DETAIL_ID
                                                        ) THEN
                    IF O_ERROR_MESSAGE = SQL_LIB.CREATE_MSG ('INV_DEAL_COMP',
                                                             NULL,
                                                             NULL,
                                                             NULL
                                                            ) THEN
                        L_VALID_DETAIL := 'N';
                    ELSE
                        RETURN FALSE;
                    END IF;
                END IF;
            END IF;
        ELSE
            IF (L_DEAL_DETAIL_ID IS NULL OR L_DEAL_DETAIL_ID != REC.DEAL_DETAIL_ID) THEN
                ---
                IF L_VALID_DEAL = 'Y' THEN
                    ---
                    L_DEAL_DETAIL_ID := REC.DEAL_DETAIL_ID;
                    ---
                    IF NOT DEAL_ATTRIB_SQL.DEAL_DETAIL_INFO (O_ERROR_MESSAGE,
                                                             L_DEAL_COMP_TYPE,
                                                             L_APPLICATION_ORDER,
                                                             L_COLLECT_START_DATE,
                                                             L_COLLECT_END_DATE,
                                                             L_COST_APPL_IND,
                                                             L_DEAL_CLASS,
                                                             L_TRAN_DISCOUNT_IND,
                                                             L_CALC_TO_ZERO_IND,
                                                             L_TAL_FORECAST_UNITS,
                                                             L_TAL_FORECAST_REVENUE,
                                                             L_TAL_BUDGET_TURNOVER,
                                                             L_TAL_ACTUAL_FORECAST_TURNOVER,
                                                             L_TAL_BASELINE_GROWTH_BUDGET,
                                                             L_TAL_BASELINE_GROWTH_ACT_FOR,
                                                             L_DEAL_ID,
                                                             L_DEAL_DETAIL_ID
                                                            ) THEN
                        IF O_ERROR_MESSAGE = SQL_LIB.CREATE_MSG ('INV_DEAL_COMP',
                                                                 NULL,
                                                                 NULL,
                                                                 NULL
                                                                ) THEN
                            L_VALID_DETAIL := 'N';
                        ELSE
                            RETURN FALSE;
                        END IF;
                    END IF;
                ELSE
                    L_VALID_DETAIL := 'N';
                END IF;
            ---
            END IF;                                                       -- IF L_DEAL_DETAIL_ID IS NULL OR L_DEAL_DETAIL_ID != REC.DEAL_DETAIL_ID....
        END IF;                                                                                -- IF L_DEAL_ID IS NULL OR L_DEAL_ID != REC.DEAL_ID ...
        IF L_VALID_DEAL = 'Y' AND L_VALID_DETAIL = 'Y' THEN
            IF NOT CHECK_DEAL_ITEMLOC (O_ERROR_MESSAGE,
                                       L_ORIGIN_COUNTRY_ID,
                                       L_VALID_ITEMLOC,
                                       REC.DEAL_ID,
                                       REC.DEAL_DETAIL_ID,
                                       REC.ITEM,
                                       REC.LOC_TYPE,
                                       REC.LOC
                                      ) THEN
                RETURN FALSE;
            END IF;
        ELSE
            L_VALID_ITEMLOC := 'N';
        END IF;
        IF L_VALID_ITEMLOC = 'Y' THEN
            IF NOT CHECK_DEAL_SUPPLIER (O_ERROR_MESSAGE,
                                        L_VALID_SUPPLIER,
                                        REC.ITEM,
                                        L_PARTNER_TYPE,
                                        L_PARTNER_ID,
                                        L_SUPPLIER
                                       ) THEN
                RETURN FALSE;
            END IF;
        END IF;
        IF L_VALID_SUPPLIER = 'Y' THEN
            ---
            IF NOT CALC_REPORTING_DATE (O_ERROR_MESSAGE,
                                        L_REPORTING_DATE,
                                        L_DEAL_ID,
                                        L_DEAL_DETAIL_ID,
                                        L_ACTIVE_DATE,
                                        L_CLOSE_DATE,
                                        L_DEAL_REPORTING_LEVEL,
                                        L_VDATE
                                       ) THEN
                RETURN FALSE;
            END IF;
            ---
            IF NOT CALC_ON_HAND_INV (O_ERROR_MESSAGE,
                                     L_ON_HAND_INV,
                                     REC.ITEM,
                                     REC.LOC,
                                     REC.SELLING_UOM
                                    ) THEN
                RETURN FALSE;
            END IF;
            ---
            IF REC.FUNDING_TYPE = LP_AMOUNT_FUNDING THEN
                L_ACTUAL_INCOME := REC.FUNDING_AMOUNT * L_ON_HAND_INV;
            ELSE
                IF NOT CALC_PRICE_DIFF (O_ERROR_MESSAGE,
                                        L_PRICE_DIFF,
                                        REC.ITEM,
                                        REC.LOC,
                                        REC.SELLING_UOM,
                                        REC.SELLING_UNIT_RETAIL,
                                        REC.OLD_SELLING_UOM,
                                        REC.OLD_SELLING_UNIT_RETAIL
                                       ) THEN
                    RETURN FALSE;
                END IF;
                ---
                L_ACTUAL_INCOME := L_PRICE_DIFF * L_ON_HAND_INV * (REC.FUNDING_PERCENT / 100);
            END IF;
            ---
            L_DEAL_ACTUALS_ITEM_LOC_TBL.EXTEND ();
            L_DAIL_IDX := L_DEAL_ACTUALS_ITEM_LOC_TBL.COUNT;
            L_DEAL_ACTUALS_ITEM_LOC_REC :=
                DEAL_ACTUALS_ITEM_LOC_REC (L_DEAL_ID,
                                           L_DEAL_DETAIL_ID,
                                           L_REPORTING_DATE,
                                           REC.ITEM,
                                           REC.LOC,
                                           REC.LOC_TYPE,
                                           L_ON_HAND_INV,
                                           NULL,
                                           NULL,
                                           L_ACTUAL_INCOME
                                          );
            L_DEAL_ACTUALS_ITEM_LOC_TBL (L_DAIL_IDX) := L_DEAL_ACTUALS_ITEM_LOC_REC;
            --
            -- Prepare collections for bulk insert into Tran_data table
            --
            L_REF_NO_2 := NULL;
            L_TOTAL_COST := NULL;
            IF L_STOCK_LEDGER_IND = 'Y' THEN
                L_REF_NO_2 := 1;
            END IF;
            L_TRAN_CODE := 6;

            IF L_ACTUAL_INCOME > 0 and L_ON_HAND_INV > 0 THEN
               INSERT INTO RPM_EVENT_ITEMLOC_DEALS
                           (DEAL_ID,
                           DEAL_DETAIL_ID,
                           REPORTING_DATE,
                           ITEM,
                           LOCATION,
                           LOC_TYPE,
                           DEPT,
                           CLASS,
                           SUBCLASS,
                           TRAN_CODE,
                           TRAN_DATA_UNITS,
                           TOTAL_COST,
                           TRAN_DATA_REF_NO_2,
                           ACTUAL_TURNOVER_UNITS,
                           ACTUAL_TURNOVER_REVENUE,
                           ORDER_NO,
                           ACTUAL_INCOME,
                           LAST_EXECUTION_DATE,
                           ORIG_EVENT_TYPE,
                           ORIG_EVENT_ID
                           )
                    VALUES (L_DEAL_ID,             -- DEAL_ID
                           L_DEAL_DETAIL_ID,      -- DEAL_DETAIL_ID
                           L_REPORTING_DATE,      -- REPORTING_DATE
                           REC.ITEM,              -- ITEM
                           REC.LOC,               -- LOCATION
                           REC.LOC_TYPE,          -- LOC_TYPE
                           REC.DEPT,              -- DEPT
                           REC.CLASS,             -- CLASS
                           REC.SUBCLASS,          -- SUBCLASS
                           L_TRAN_CODE,           -- TRAN_CODE
                           L_ON_HAND_INV,         -- TRAN_DATA_UNITS
                           L_TOTAL_COST,          -- TOTAL_COST
                           L_REF_NO_2,            -- TRAN_DATA_REF_NO_2
                           L_ON_HAND_INV,         -- ACTUAL_TURNOVER_UNITS
                           NULL,                  -- ACTUAL_TURNOVER_REVENUE
                           NULL,                  -- ORDER_NO
                           L_ACTUAL_INCOME,       -- ACTUAL_INCOME
                           L_VDATE,               -- LAST_EXECUTION_DATE
                           REC.ORIG_EVENT_TYPE,   -- ORIG_EVENT_TYPE
                           REC.PRICE_EVENT_ID     -- ORIG_EVENT_ID
                           );
            END IF;
        END IF;
    END LOOP;

    RETURN TRUE;
EXCEPTION
    WHEN OTHERS THEN
        O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_PROGRAM,
                                               TO_CHAR (SQLCODE)
                                              );
        RETURN FALSE;
END PROCESS_VFM;


---------------------------------------------------

FUNCTION PROCESS_STAGED_DEALS (O_ERROR_MESSAGE IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
    RETURN BOOLEAN IS
BEGIN
        RETURN PROCESS_STAGED_DEALS(O_ERROR_MESSAGE,  new OBJ_PRICE_CHANGE_TBL());
END;

---------------------------------------------------

FUNCTION PROCESS_STAGED_DEALS (O_ERROR_MESSAGE   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_EVENTS_TBL      IN     OBJ_PRICE_CHANGE_TBL)
RETURN BOOLEAN IS

   L_PROGRAM               VARCHAR2 (50) := 'RMSSUB_PRICECHANGE_UPDATE.PROCESS_STAGED_DEALS';
   L_VDATE                 DATE;
   L_tran_inserts          NUMBER := 0;
   L_has_specific_events   NUMBER;
   ---
   L_tax_info_tbl          OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   L_tax_info_rec          OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();
   L_entity_type           VARCHAR2(6);
   L_actual_income         NUMBER(20,4) := 0;
   L_tax_amount            NUMBER(20,4) := 0;
   ---
   L_supplier              ORDHEAD.SUPPLIER%TYPE;
   cursor C_VFM (join_specific_events number) is
      select *
        from (select source.DEAL_ID,
                     source.DEAL_DETAIL_ID,
                     source.REPORTING_DATE,
                     source.ITEM,
                     source.LOCATION,
                     source.LOC_TYPE,
                     source.DEPT,
                     source.CLASS,
                     source.SUBCLASS,
                     source.TRAN_CODE,
                     source.TRAN_DATA_UNITS,
                     source.TOTAL_COST,
                     source.TRAN_DATA_REF_NO_2,
                     source.ACTUAL_TURNOVER_UNITS,
                     source.ACTUAL_TURNOVER_REVENUE,
                     source.ORDER_NO,
                     source.ACTUAL_INCOME,
                     source.LAST_EXECUTION_DATE
                from RPM_EVENT_ITEMLOC_DEALS source,
                     TABLE(CAST(I_events_tbl AS OBJ_PRICE_CHANGE_TBL)) sp_events
               where join_specific_events = 1
                          and source.orig_event_id = sp_events.price_change_id
                          and source.orig_event_type = sp_events.price_change_type
               UNION ALL
              select source.DEAL_ID,
                     source.DEAL_DETAIL_ID,
                     source.REPORTING_DATE,
                     source.ITEM,
                     source.LOCATION,
                     source.LOC_TYPE,
                     source.DEPT,
                     source.CLASS,
                     source.SUBCLASS,
                     source.TRAN_CODE,
                     source.TRAN_DATA_UNITS,
                     source.TOTAL_COST,
                     source.TRAN_DATA_REF_NO_2,
                     source.ACTUAL_TURNOVER_UNITS,
                     source.ACTUAL_TURNOVER_REVENUE,
                     source.ORDER_NO,
                     source.ACTUAL_INCOME,
                     source.LAST_EXECUTION_DATE
                from RPM_EVENT_ITEMLOC_DEALS source
               where join_specific_events = 0) t
            order by t.DEAL_ID,
                     t.DEAL_DETAIL_ID,
                     t.REPORTING_DATE;

   cursor C_GET_SUPPLIER(L_order_no RPM_EVENT_ITEMLOC_DEALS.ORDER_NO%TYPE) is
      select supplier
        from ordhead
       where order_no = L_order_no ;
BEGIN
   L_VDATE := GET_VDATE ();
   --

   if I_events_tbl is NULL or I_events_tbl.count = 0 then
       L_has_specific_events := 0;
   else
       L_has_specific_events := 1;
   end if;

   for REC IN C_VFM(L_has_specific_events)
   LOOP
      INSERT INTO temp_tran_data(item,
                                 dept,
                                 class,
                                 subclass,
                                 location,
                                 loc_type,
                                 tran_date,
                                 tran_code,
                                 units,
                                 total_retail,
                                 total_cost,
                                 pgm_name,
                                 timestamp,
                                 ref_no_1,
                                 ref_no_2)
                         VALUES (REC.ITEM,
                                 REC.DEPT,
                                 REC.CLASS,
                                 REC.SUBCLASS,
                                 REC.LOCATION,
                                 REC.LOC_TYPE,
                                 REC.REPORTING_DATE,
                                 REC.TRAN_CODE,
                                 REC.TRAN_DATA_UNITS,
                                 REC.ACTUAL_INCOME,
                                 REC.TOTAL_COST,
                                 L_PROGRAM,
                                 SYSDATE,
                                 REC.DEAL_ID,
                                 REC.TRAN_DATA_REF_NO_2);

      ---
      open C_GET_SUPPLIER(REC.order_no);
      fetch C_GET_SUPPLIER into L_supplier;
      close C_GET_SUPPLIER;     
      ---
      L_tax_info_rec := OBJ_TAX_INFO_REC();
      L_tax_info_rec.item               := REC.ITEM;
      L_tax_info_rec.merch_hier_value   := REC.DEPT;
      L_tax_info_rec.from_entity        := REC.LOCATION;
      ---
      if REC.LOC_TYPE = 'S' then
         L_tax_info_rec.from_entity_type :=  'ST';
      elsif REC.LOC_TYPE = 'W' then
         L_tax_info_rec.from_entity_type :=  'WH';
      elsif REC.LOC_TYPE = 'E' then
         L_tax_info_rec.from_entity_type :=  'P';
      end if;
      ---
      L_tax_info_rec.amount             := REC.ACTUAL_INCOME;
      L_tax_info_rec.cost_retail_ind    := 'R';
      L_tax_info_rec.tax_incl_ind       := 'Y';
      L_tax_info_rec.active_date        := L_VDATE;
      ---
      L_tax_info_rec.to_entity          := L_supplier;
      L_tax_info_rec.to_entity_type     := 'SU';
      L_tax_info_rec.tran_type          := 'DEALSPROC';
      L_tax_info_rec.tran_date          := L_VDATE;
      L_tax_info_rec.tran_id            := REC.deal_id;
      ---
      L_tax_info_tbl.DELETE();
      L_tax_info_tbl.EXTEND();
      L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
      ---
      if TAX_SQL.GET_TAX_INFO(O_error_message,
                              L_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
      ---
      L_actual_income := REC.ACTUAL_INCOME;

      if L_tax_info_tbl is not NULL and L_tax_info_tbl.count > 0 then
         L_tax_amount := L_tax_info_tbl(L_tax_info_tbl.count).tax_amount;
      end if;
      ---
      L_actual_income := L_actual_income - nvl(L_tax_amount,0);

      update RPM_EVENT_ITEMLOC_DEALS
         set actual_income = L_actual_income
       where deal_id = REC.DEAL_ID
         and deal_detail_id = REC.DEAL_DETAIL_ID
         and item = REC.ITEM
         and location = REC.LOCATION
         and loc_type = REC.LOC_TYPE;
   END LOOP;

   --
   -- Do Bulk Merge on DEAL_ACTUALS_ITEM_LOC --
   MERGE INTO DEAL_ACTUALS_ITEM_LOC DAIL
        USING (
               SELECT source.DEAL_ID,
                      source.DEAL_DETAIL_ID,
                      TRUNC(source.REPORTING_DATE) REPORTING_DATE,
                      source.ITEM,
                      source.LOCATION,
                      source.LOC_TYPE,
                      NVL(source.ORDER_NO,0) ORDER_NO,
                      SUM(source.ACTUAL_TURNOVER_UNITS) ACTUAL_TURNOVER_UNITS,
                      SUM(source.ACTUAL_INCOME) ACTUAL_INCOME,
                      SUM(source.ACTUAL_TURNOVER_REVENUE) ACTUAL_TURNOVER_REVENUE
                 FROM RPM_EVENT_ITEMLOC_DEALS source,
                      TABLE(CAST(I_EVENTS_TBL as OBJ_PRICE_CHANGE_TBL)) sp_events
                WHERE L_has_specific_events = 1
                  AND source.orig_event_id = sp_events.price_change_id
                  AND source.orig_event_type = sp_events.price_change_type
                GROUP BY source.DEAL_ID,
                         source.DEAL_DETAIL_ID,
                         TRUNC(source.REPORTING_DATE),
                         source.ITEM,
                         source.LOCATION,
                         source.LOC_TYPE,
                         NVL(source.ORDER_NO,0)
               UNION ALL
               SELECT source.DEAL_ID,
                      source.DEAL_DETAIL_ID,
                      TRUNC(source.REPORTING_DATE) REPORTING_DATE,
                      source.ITEM,
                      source.LOCATION,
                      source.LOC_TYPE,
                      NVL(source.ORDER_NO,0) ORDER_NO,
                      SUM(source.ACTUAL_TURNOVER_UNITS) ACTUAL_TURNOVER_UNITS,
                      SUM(source.ACTUAL_INCOME) ACTUAL_INCOME,
                      SUM(source.ACTUAL_TURNOVER_REVENUE) ACTUAL_TURNOVER_REVENUE
                 FROM RPM_EVENT_ITEMLOC_DEALS source
                WHERE L_has_specific_events = 0
                GROUP BY source.DEAL_ID,
                         source.DEAL_DETAIL_ID,
                         TRUNC(source.REPORTING_DATE),
                         source.ITEM,
                         source.LOCATION,
                         source.LOC_TYPE,
                         NVL(source.ORDER_NO,0)
              ) DAI
           ON (    DAI.DEAL_ID = DAIL.DEAL_ID
               AND DAI.DEAL_DETAIL_ID = DAIL.DEAL_DETAIL_ID
               AND DAI.REPORTING_DATE = DAIL.REPORTING_DATE
               AND DAI.ITEM = DAIL.ITEM
               AND DAI.LOCATION = DAIL.LOCATION
               AND DAI.LOC_TYPE = DAIL.LOC_TYPE
               AND NVL(DAI.ORDER_NO,0) = NVL(DAIL.ORDER_NO,0))
        WHEN MATCHED THEN
            UPDATE
               SET DAIL.ACTUAL_TURNOVER_UNITS = NVL (DAIL.ACTUAL_TURNOVER_UNITS, 0) + NVL (DAI.ACTUAL_TURNOVER_UNITS, 0),
                   DAIL.ACTUAL_INCOME = NVL (DAIL.ACTUAL_INCOME, 0) + NVL (DAI.ACTUAL_INCOME, 0)
        WHEN NOT MATCHED THEN
            INSERT (DAI_ID,
                    DEAL_ID,
                    DEAL_DETAIL_ID,
                    REPORTING_DATE,
                    ITEM,
                    LOCATION,
                    LOC_TYPE,
                    ACTUAL_TURNOVER_UNITS,
                    ACTUAL_TURNOVER_REVENUE,
                    ORDER_NO,
                    ACTUAL_INCOME)
            VALUES (DEAL_ACTUALS_ITEMLOC_SEQ.NEXTVAL,
                    DAI.DEAL_ID,
                    DAI.DEAL_DETAIL_ID,
                    DAI.REPORTING_DATE,
                    DAI.ITEM,
                    DAI.LOCATION,
                    DAI.LOC_TYPE,
                    DAI.ACTUAL_TURNOVER_UNITS,
                    DAI.ACTUAL_TURNOVER_REVENUE,
                    DAI.ORDER_NO,
                    DAI.ACTUAL_INCOME);

   --
   -- Do Bulk Merge on DEAL_ACTUALS_FORECAST --
   MERGE INTO DEAL_ACTUALS_FORECAST DAF
        USING (SELECT source.DEAL_ID,
                      source.DEAL_DETAIL_ID,
                      source.REPORTING_DATE,
                      SUM (NVL (source.ACTUAL_INCOME, 0)) AS SUM_ACTUAL_INCOME
                 FROM RPM_EVENT_ITEMLOC_DEALS source,
                      TABLE(CAST(I_EVENTS_TBL as OBJ_PRICE_CHANGE_TBL)) sp_events
                WHERE L_has_specific_events = 1
                  AND source.orig_event_id = sp_events.price_change_id
                  AND source.orig_event_type = sp_events.price_change_type
                GROUP BY DEAL_ID,
                         DEAL_DETAIL_ID,
                         REPORTING_DATE
                UNION ALL
               SELECT source.DEAL_ID,
                      source.DEAL_DETAIL_ID,
                      source.REPORTING_DATE,
                      SUM (NVL (source.ACTUAL_INCOME, 0)) AS SUM_ACTUAL_INCOME
                 FROM RPM_EVENT_ITEMLOC_DEALS source
                WHERE L_has_specific_events = 0
                GROUP BY DEAL_ID,
                         DEAL_DETAIL_ID,
                         REPORTING_DATE) DAIL
           ON (DAF.DEAL_ID = DAIL.DEAL_ID
               AND DAF.DEAL_DETAIL_ID = DAIL.DEAL_DETAIL_ID
               AND DAF.REPORTING_DATE = DAIL.REPORTING_DATE)
        WHEN MATCHED THEN
            UPDATE
               SET DAF.ACTUAL_FORECAST_INCOME       = NVL (DAF.ACTUAL_FORECAST_INCOME, 0) + DAIL.SUM_ACTUAL_INCOME,
                   DAF.ACTUAL_FORECAST_TREND_INCOME = NVL (DAF.ACTUAL_FORECAST_TREND_INCOME, 0) + DAIL.SUM_ACTUAL_INCOME,
                   DAF.ACTUAL_INCOME                = NVL (DAF.ACTUAL_INCOME, 0) + DAIL.SUM_ACTUAL_INCOME,
                   DAF.ACTUAL_FORECAST_IND          = 'A'
        WHEN NOT MATCHED THEN
            INSERT (DEAL_ID,
                    DEAL_DETAIL_ID,
                    REPORTING_DATE,
                    ACTUAL_FORECAST_IND,
                    BASELINE_TURNOVER,
                    BUDGET_TURNOVER,
                    BUDGET_INCOME,
                    ACTUAL_FORECAST_TURNOVER,
                    ACTUAL_FORECAST_INCOME,
                    ACTUAL_FORECAST_TREND_TURNOVER,
                    ACTUAL_FORECAST_TREND_INCOME,
                    ACTUAL_INCOME)
            VALUES (DAIL.DEAL_ID,
                    DAIL.DEAL_DETAIL_ID,
                    DAIL.REPORTING_DATE,
                    'A',
                    0,
                    0,
                    0,
                    0,
                    DAIL.SUM_ACTUAL_INCOME,
                    0,
                    DAIL.SUM_ACTUAL_INCOME,
                    DAIL.SUM_ACTUAL_INCOME);

   return TRUE;
EXCEPTION
    when OTHERS then
        O_ERROR_MESSAGE := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_PROGRAM,
                                               TO_CHAR (SQLCODE));
        return FALSE;
END PROCESS_STAGED_DEALS;



--------------------------------------------------------------------------------
--                             PRIVATE PROCEDURES                             --
--------------------------------------------------------------------------------

FUNCTION CHECK_REQUIRED_FIELDS(O_error_message      OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       OBJ_PRICEEVENT_IL_REC)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'RMSSUB_PRICECHANGE_UPDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item', NULL, NULL);
      return FALSE;
   end if;

   if I_message.loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'location', NULL, NULL);
      return FALSE;
   end if;

   if I_message.loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'location type', NULL, NULL);
      return FALSE;
   end if;

   if I_message.change_type is NULL then
      if I_message.currency_code is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'currency code', NULL, NULL);
         return FALSE;
      end if;
   end if;

   if I_message.loc_type NOT in ('S', 'W') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC_TYPE', NULL, NULL, NULL);
      return FALSE;
   end if;

   -- For 'PC', selling UOM and selling unit retail must either both be defined or both NULL
   -- Also, Multi units, multi selling UOM and multi-unit retail must be either all defined or all NULL
   -- For 'CL', 'CR', 'PS' and 'PE', both selling UOM and selling unit retail should be defined
   --
   if I_message.price_chg_type = RMSSUB_PRICECHANGE.PERM_PC then
      if (I_message.selling_uom is NULL and I_message.selling_unit_retail is NOT NULL) or
         (I_message.selling_uom is NOT NULL and I_message.selling_unit_retail is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SINGLE_RETAILS', NULL, NULL, NULL);
         return FALSE;
      end if;

      if NOT ((I_message.multi_units is NULL and
               I_message.multi_unit_retail is NULL and
               I_message.multi_selling_uom is NULL) or
              (I_message.multi_units is NOT NULL and
               I_message.multi_unit_retail is NOT NULL and
               I_message.multi_selling_uom is NOT NULL)) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_MULTI_RETAILS', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.multi_unit_retail is NOT NULL and I_message.multi_unit_retail < 0 then
         O_error_message := SQL_LIB.CREATE_MSG('UNIT_RTL_ZERO', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.multi_units is NOT NULL and I_message.multi_units <= 1 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VAL_MULTI_UNITS', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.multi_units != ROUND(I_message.multi_units, 0) then
         O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS_INTEGER', NULL, NULL, NULL);
         return FALSE;
      end if;

   else
      if ((I_message.change_type is NULL) or (I_message.change_type = LP_fixed_price)) then
         if I_message.selling_unit_retail is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'selling_unit_retail', NULL, NULL);
            return FALSE;
         end if;

         if I_message.selling_uom is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'selling_uom', NULL, NULL);
            return FALSE;
         end if;
      end if;
   end if;

   if I_message.selling_unit_retail is NOT NULL and I_message.selling_unit_retail < 0 then
      O_error_message := SQL_LIB.CREATE_MSG('UNIT_RTL_ZERO', NULL, NULL, NULL);
      return FALSE;
   end if;

   --
   -- Either single or multi must be defined unless the price-change/clearance is at the
   -- parent level and the the change type is "by-percent" or "by-amount"
   --
   if ((I_message.change_type is NULL) or (I_message.change_type = LP_fixed_price)) then
      if I_message.selling_uom is NULL and
         I_message.selling_unit_retail is NULL and
         I_message.multi_units is NULL and
         I_message.multi_unit_retail is NULL and
         I_message.multi_selling_uom is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SINGLE_OR_MULTI_REQ', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
--------------------------------------------------------------------------------

FUNCTION DETERMINE_TRAN_TYPE(O_error_message          OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tran_type           IN OUT PRICE_HIST.TRAN_TYPE%TYPE,
                             O_single_changed      IN OUT BOOLEAN,
                             O_multi_changed       IN OUT BOOLEAN,
                             I_price_chg_type      IN     VARCHAR2,
                             I_selling_unit_retail IN     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                             I_multi_unit_retail   IN     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                             I_change_type         IN     RPM_PRICE_CHANGE.CHANGE_TYPE%TYPE,
                             I_is_on_clearance     IN     VARCHAR2)
RETURN BOOLEAN IS

L_program   VARCHAR2(100) := 'RMSSUB_PRICECHANGE_UPDATE.DETERMINE_TRAN_TYPE';

BEGIN

   if I_price_chg_type in (RMSSUB_PRICECHANGE.PERM_PC, RMSSUB_PRICECHANGE.CL_RESET, RMSSUB_PRICECHANGE.PROM_END) then
      if ((I_selling_unit_retail is NOT NULL) or (I_change_type in (LP_by_percent, LP_by_amount))) then
         if I_is_on_clearance = 1 then
            O_tran_type     := 8;
            O_multi_changed := FALSE;
         else
            if I_multi_unit_retail is NOT NULL then
               O_tran_type     := 11;
               O_multi_changed := TRUE;
            else
               O_tran_type     := 4;
               O_multi_changed := FALSE;
            end if;
         end if;
         O_single_changed := TRUE;
      else
         if I_multi_unit_retail is NOT NULL then
            O_tran_type      := 10;
            O_single_changed := FALSE;
            O_multi_changed  := TRUE;
         end if;
      end if;
   elsif I_price_chg_type = RMSSUB_PRICECHANGE.CLEARANCE then
      O_single_changed := TRUE;
      O_tran_type := 8;
   elsif I_price_chg_type = RMSSUB_PRICECHANGE.PROM_ST then
      O_single_changed := TRUE;
      O_tran_type := 9;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DETERMINE_TRAN_TYPE;
--------------------------------------------------------------------------------

FUNCTION POPULATE_ORIG_RETAIL(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

L_program            VARCHAR2(100) := 'RMSSUB_PRICECHANGE_UPDATE.POPULATE_ORIG_RETAIL';

L_aort_item          ITEM_TBL := ITEM_TBL();
L_aort_loc           LOC_TBL := LOC_TBL();
L_aort_unit_retail   UNIT_RETAIL_TBL := UNIT_RETAIL_TBL();

L_fdoh_date          DATE;

cursor C_ORIG_RETAIL is
select aort.unit_retail
  from api_orig_retail_temp aort,
       api_pc_temp apt
 where aort.item (+) = apt.item
   and aort.loc (+) = apt.loc
order by apt.seq_no;

BEGIN

   if NOT GET_FDOH_DATE(O_error_message,
                        L_fdoh_date) then
      return FALSE;
   end if;

   delete GTT_NUM_NUM_STR_STR_DATE_DATE;

   forall i in 1..LP_cr_count
      insert into GTT_NUM_NUM_STR_STR_DATE_DATE
         (number_1, varchar2_1)
      values
         (LP_loc(i), LP_item(i));

   delete gtt_num_num_str_str_date_date
    where rowid in
         (select lead(rowid) over (partition by number_1, varchar2_1 order by null)
            from gtt_num_num_str_str_date_date );
            
   -- item_loc created before the beginning of period
   -- get the latest price change up to the beginning of period
   -- if non-exists, get the retail when new item_loc is created  

   merge into api_orig_retail_temp target
   using (select t1.item,
                 t1.loc,
                 t1.unit_retail
            from (select t.item,
                         t.loc,
                         t.unit_retail,
                         rank() over (partition by t.item, t.loc order by t.unit_retail) rank       
                    from (select /*+ leading(il) USE_NL(PH1, IL) */
                                 ph1.item,
                                 ph1.loc,
                                 ph1.unit_retail
                            from price_hist ph1,
                                 gtt_num_num_str_str_date_date il
                           where ph1.item = il.varchar2_1
                             and ph1.loc  = il.number_1
                             and ph1.tran_type = 0
                             and ph1.action_date >= L_fdoh_date
                           union all
                          select distinct inner.item,
                                 inner.loc,
                                 first_value (inner.unit_retail) over (partition by inner.item, inner.loc, inner.action_date order by inner.tran_type desc) unit_retail
                            from (select /*+ leading(il) USE_NL(PH3, IL) */
                                         item,
                                         loc,
                                         unit_retail,
                                         tran_type,
                                         action_date,
                                         max(action_date) over (partition by item, loc) max_action_date
                                    from price_hist ph3,
                                         gtt_num_num_str_str_date_date il
                                   where ph3.item = il.varchar2_1
                                     and ph3.loc = il.number_1
                                     and ph3.tran_type in (0, 4, 11)
                                     and ph3.action_date < L_fdoh_date) inner
                                   where inner.action_date = inner.max_action_date) t ) t1
            where t1.rank = 1) source
      on (    target.item = source.item  
          and target.loc  = source.loc)
     when not matched then
   insert (item, 
           loc,
           unit_retail)
   values (source.item,
           source.loc,
           source.unit_retail);                                    
   open C_ORIG_RETAIL;
   fetch C_ORIG_RETAIL bulk collect into LP_orig_retail;
   close C_ORIG_RETAIL;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_ORIG_RETAIL;
--------------------------------------------------------------------------------

FUNCTION GET_FDOH_DATE(O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_fdoh_date        OUT   DATE)
RETURN BOOLEAN IS

L_program       VARCHAR2(50) := 'RMSSUB_PRICECHANGE_UPDATE.GET_FDOH_DATE';
L_half_no       PERIOD.HALF_NO%TYPE := 0;
L_day           NUMBER(2);
L_month         NUMBER(2);
L_year          NUMBER(4);
L_cal           VARCHAR2(1);
L_return_code   VARCHAR2(10);
L_vdate         VARCHAR2(50) := to_char(GET_VDATE);

cursor C_HALF is
select half_no
  from period;

BEGIN

   if LP_fdoh_date_tbl.exists(L_vdate) then
      O_fdoh_date := LP_fdoh_date_tbl(L_vdate);
      return TRUE;
   end if;

   --

   open C_HALF;
   fetch C_HALF into L_half_no;
   close C_HALF;

   if LP_so.calendar_454_ind = 'C' then
      HALF_TO_CAL_FDOH(L_half_no,
                       L_day,
                       L_month,
                       L_year,
                       L_return_code,
                       O_error_message);

      if L_return_code = 'FALSE' then
         O_error_message := SQL_LIB.CREATE_MSG('ERR_DATE_CONV', NULL, NULL, NULL);
         return FALSE;
      else
         O_fdoh_date := TO_DATE(TO_CHAR(L_day,'09')||TO_CHAR(L_month,'09')||
                        TO_CHAR(L_year,'0999'),'DDMMYYYY');
      end if;
   else
      HALF_TO_454_FDOH(L_half_no,
                       L_day,
                       L_month,
                       L_year,
                       L_return_code,
                       O_error_message);

      if L_return_code = 'FALSE' then
         O_error_message := SQL_LIB.CREATE_MSG('ERR_DATE_CONV', NULL, NULL, NULL);
         return FALSE;
      else
         O_fdoh_date := TO_DATE(TO_CHAR(L_day,'09')||TO_CHAR(L_month,'09')||
                        TO_CHAR(L_year,'0999'),'DDMMYYYY');
      end if;
   end if;

   LP_fdoh_date_tbl(L_vdate) := O_fdoh_date;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_FDOH_DATE;
--------------------------------------------------------------------------------

FUNCTION CHECK_DEAL_ITEMLOC(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_origin_country_id IN OUT DEAL_ITEMLOC.ORIGIN_COUNTRY_ID%TYPE,
                            O_valid             IN OUT VARCHAR2,
                            I_deal_id           IN     DEAL_ITEMLOC.DEAL_ID%TYPE,
                            I_deal_detail_id    IN     DEAL_ITEMLOC.DEAL_DETAIL_ID%TYPE,
                            I_item              IN     DEAL_ITEMLOC.ITEM%TYPE,
                            I_loc_type          IN     DEAL_ITEMLOC.LOC_TYPE%TYPE,
                            I_location          IN     DEAL_ITEMLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS
   L_program          VARCHAR2(50)     := 'RMSSUB_PRICECHANGE_UPDATE.CHECK_DEAL_ITEMLOC';
   L_dummy            VARCHAR2(1);
   ---
   L_seq_no                DEAL_ITEMLOC.SEQ_NO%TYPE;
   L_merch_level           DEAL_ITEMLOC.MERCH_LEVEL%TYPE;
   L_division              DEAL_ITEMLOC.DIVISION%TYPE;
   L_group_no              DEAL_ITEMLOC.GROUP_NO%TYPE;
   L_dept                  DEAL_ITEMLOC.DEPT%TYPE;
   L_class                 DEAL_ITEMLOC.CLASS%TYPE;
   L_subclass              DEAL_ITEMLOC.SUBCLASS%TYPE;
   L_item_parent           DEAL_ITEMLOC.ITEM_PARENT%TYPE;
   L_diff_1                DEAL_ITEMLOC.DIFF_1%TYPE;
   L_diff_2                DEAL_ITEMLOC.DIFF_2%TYPE;
   L_diff_3                DEAL_ITEMLOC.DIFF_3%TYPE;
   L_diff_4                DEAL_ITEMLOC.DIFF_4%TYPE;
   L_org_level             DEAL_ITEMLOC.ORG_LEVEL%TYPE;
   L_chain                 DEAL_ITEMLOC.CHAIN%TYPE;
   L_area                  DEAL_ITEMLOC.AREA%TYPE;
   L_region                DEAL_ITEMLOC.REGION%TYPE;
   L_district              DEAL_ITEMLOC.DISTRICT%TYPE;
   L_location              DEAL_ITEMLOC.LOCATION%TYPE;
   L_loc_type              DEAL_ITEMLOC.LOC_TYPE%TYPE;
   L_item                  DEAL_ITEMLOC.ITEM%TYPE;
   L_excl_ind              DEAL_ITEMLOC.EXCL_IND%TYPE;
   L_origin_country_id     DEAL_ITEMLOC.ORIGIN_COUNTRY_ID%TYPE;
   ---
   cursor C_DEALDETAILTEMP is
   select 'X'
     from deal_detail_temp
    where deal_id = I_deal_id
      and deal_detail_id = I_deal_detail_id;

   cursor C_DEALITEMLOCTEMP is
   select origin_country_id
     from deal_itemloc_temp
    where deal_id = I_deal_id
      and deal_detail_id = I_deal_detail_id
      and item = I_item
      and loc_type = I_loc_type
      and location = I_location;

   cursor C_DEAL_ITEMLOC_INCLUDE is
      select seq_no,
             merch_level,
             division,
             group_no,
             dept,
             class,
             subclass,
             item_parent,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             org_level,
             chain,
             area,
             region,
             district,
             location,
             origin_country_id,
             loc_type,
             item,
             excl_ind
        from deal_itemloc
       where deal_id        = I_deal_id
         and deal_detail_id = I_deal_detail_id
         and excl_ind       = 'N'
    order by seq_no;

   cursor C_DEAL_ITEMLOC_EXCLUDE is
      select seq_no,
             merch_level,
             division,
             group_no,
             dept,
             class,
             subclass,
             item_parent,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             org_level,
             chain,
             area,
             region,
             district,
             location,
             loc_type,
             item,
             excl_ind
        from deal_itemloc
       where deal_id        = I_deal_id
         and deal_detail_id = I_deal_detail_id
         and excl_ind       = 'Y'
    order by seq_no;


BEGIN
   O_valid := 'N';

   open C_DEALDETAILTEMP;

   fetch C_DEALDETAILTEMP
    into L_dummy;

   if C_DEALDETAILTEMP%FOUND then
      open C_DEALITEMLOCTEMP;
     fetch C_DEALITEMLOCTEMP
       into O_origin_country_id;

      if C_DEALITEMLOCTEMP%FOUND then
         O_valid := 'Y';
      else
         O_valid := 'N';
      end if;

      close C_DEALITEMLOCTEMP;
      close C_DEALDETAILTEMP;
      return TRUE;
   end if;

   close C_DEALDETAILTEMP;

   insert into DEAL_DETAIL_TEMP
       (DEAL_ID,
        DEAL_DETAIL_ID)
   values
       (I_deal_id,
        I_deal_detail_id);

   for dil_rec in C_DEAL_ITEMLOC_INCLUDE LOOP
      --- Populate local variables.
      L_seq_no            := dil_rec.seq_no;
      L_merch_level       := dil_rec.merch_level;
      L_division          := dil_rec.division;
      L_group_no          := dil_rec.group_no;
      L_dept              := dil_rec.dept;
      L_class             := dil_rec.class;
      L_subclass          := dil_rec.subclass;
      L_item_parent       := dil_rec.item_parent;
      L_diff_1            := dil_rec.diff_1;
      L_diff_2            := dil_rec.diff_2;
      L_diff_3            := dil_rec.diff_3;
      L_diff_4            := dil_rec.diff_4;
      L_org_level         := dil_rec.org_level;
      L_chain             := dil_rec.chain;
      L_area              := dil_rec.area;
      L_region            := dil_rec.region;
      L_district          := dil_rec.district;
      L_location          := dil_rec.location;
      L_origin_country_id := dil_rec.origin_country_id;
      L_loc_type          := dil_rec.loc_type;
      L_item              := dil_rec.item;
      L_excl_ind          := dil_rec.excl_ind;

      --- Select items according to merch_level.
      if L_merch_level = 1 then
         --- Level 1 = All Items
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 2 then
         --- Level 2 = Division
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     deps de,
                     groups gr,
                     division di
               where im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and L_division         = di.division
                 and di.division        = gr.division
                 and gr.group_no        = de.group_no
                 and de.dept            = im.dept
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 3 then
         --- Level 3 = Group
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     deps de,
                     groups gr
               where im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and L_group_no         = gr.group_no
                 and gr.group_no        = de.group_no
                 and de.dept            = im.dept
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 4 then
         --- Level 4 = Dept
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and L_dept             = im.dept
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 5 then
         --- Level 5 = Class
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where L_dept             = im.dept
                 and L_class            = im.class
                 and im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 6 then
         --- Level 6 = Subclass
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where L_dept             = im.dept
                 and L_class            = im.class
                 and L_subclass         = im.subclass
                 and im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((L_loc_type is NOT NULL and st.store = L_location and  L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 7 then
         --- Level 7 = Item Parent
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent         = im.item_parent
                 and im.item_level         = im.tran_level
                 and im.status             = 'A'
                 and st.district           = d.district
                 and d.region              = r.region
                 and r.area                = a.area
                 and a.chain               = c.chain
                 and d.district            = nvl(L_district, d.district)
                 and r.region              = nvl(L_region,   r.region)
                 and a.area                = nvl(L_area,     a.area)
                 and c.chain               = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 8 then
         --- Level 8 = Item Parent/Diff 1
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent         = im.item_parent
                 and L_diff_1              = im.diff_1
                 and im.item_level         = im.tran_level
                 and im.status             = 'A'
                 and st.district           = d.district
                 and d.region              = r.region
                 and r.area                = a.area
                 and a.chain               = c.chain
                 and d.district            = nvl(L_district, d.district)
                 and r.region              = nvl(L_region,   r.region)
                 and a.area                = nvl(L_area,     a.area)
                 and c.chain               = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 9 then
         --- Level 9 = Parent/Diff 2
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent         = im.item_parent
                 and L_diff_2              = im.diff_2
                 and im.item_level         = im.tran_level
                 and im.status             = 'A'
                 and st.district           = d.district
                 and d.region              = r.region
                 and r.area                = a.area
                 and a.chain               = c.chain
                 and d.district            = nvl(L_district, d.district)
                 and r.region              = nvl(L_region,   r.region)
                 and a.area                = nvl(L_area,     a.area)
                 and c.chain               = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 10 then
         --- Level 10 = Parent/Diff 3
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent         = im.item_parent
                 and L_diff_3              = im.diff_3
                 and im.item_level         = im.tran_level
                 and im.status             = 'A'
                 and st.district           = d.district
                 and d.region              = r.region
                 and r.area                = a.area
                 and a.chain               = c.chain
                 and d.district            = nvl(L_district, d.district)
                 and r.region              = nvl(L_region,   r.region)
                 and a.area                = nvl(L_area,     a.area)
                 and c.chain               = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 11 then
         --- Level 11 = Parent/Diff 4
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                'S',
                     st.store,
                L_origin_country_id
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent         = im.item_parent
                 and L_diff_4              = im.diff_4
                 and im.item_level         = im.tran_level
                 and im.status             = 'A'
                 and st.district           = d.district
                 and d.region              = r.region
                 and r.area                = a.area
                 and a.chain               = c.chain
                 and d.district            = nvl(L_district, d.district)
                 and r.region              = nvl(L_region,   r.region)
                 and a.area                = nvl(L_area,     a.area)
                 and c.chain               = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      ------------------------------
      elsif L_merch_level = 12 then
         --- Level 12 = item
         insert into deal_itemloc_temp
                     (DEAL_ID,
                 DEAL_DETAIL_ID,
                 ITEM,
                 LOC_TYPE,
                 LOCATION,
                 ORIGIN_COUNTRY_ID)
              select distinct I_deal_id,
                     I_deal_detail_id,
                     L_item,
                'S',
                     st.store,
                L_origin_country_id
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL));
      end if;

      ------------------------------------
      --- Warehouse records.
      ------------------------------------
      if L_org_level is NULL or L_org_level = 5 then
         if L_merch_level = 1 then
            --- Level 1 = All Items
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from item_master im,
                        wh
                  where im.item_level = im.tran_level
                    and im.status     = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 2 then
            --- Level 2 = Division
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from item_master im,
                        wh,
                        deps de,
                        groups gr,
                        division di
                  where im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and L_division         = di.division
                    and di.division        = gr.division
                    and gr.group_no        = de.group_no
                    and de.dept            = im.dept
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 3 then
            --- Level 3 = Group
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from item_master im,
                        wh,
                        deps de,
                        groups gr
                  where im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and L_group_no         = gr.group_no
                    and gr.group_no        = de.group_no
                    and de.dept            = im.dept
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 4 then
            --- Level 4 = Dept
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from item_master im,
                        wh
                  where L_dept             = im.dept
                    and im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 5 then
            --- Level 5 = Class
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from item_master im,
                        wh
                  where L_dept             = im.dept
                    and L_class            = im.class
                    and im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 6 then
            --- Level 6 = Subclass
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from item_master im,
                        wh
                  where L_dept             = im.dept
                    and L_class            = im.class
                    and L_subclass         = im.subclass
                    and im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 7 then
            --- Level 7 = Parent
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from wh,
                        item_master im
                  where L_item_parent          = im.item_parent
                    and im.item_level          = im.tran_level
                    and im.status              = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 8 then
            --- Level 8 = Parent/Diff 1
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from wh,
                        item_master im
                  where L_item_parent             = im.item_parent
                    and L_diff_1                  = im.diff_1
                    and im.item_level             = im.tran_level
                    and im.status                 = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 9 then
            --- Level 9 = Parent/Diff 2
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from wh,
                        item_master im
                  where L_item_parent              = im.item_parent
                    and L_diff_2                   = im.diff_2
                    and im.item_level              = im.tran_level
                    and im.status                  = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 10 then
            --- Level 10 = Parent/Diff 3
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from wh,
                        item_master im
                  where L_item_parent              = im.item_parent
                    and L_diff_3                   = im.diff_3
                    and im.item_level              = im.tran_level
                    and im.status                  = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 11 then
            --- Level 11 = Parent/Diff 4
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from wh,
                        item_master im
                  where L_item_parent              = im.item_parent
                    and L_diff_4                   = im.diff_4
                    and im.item_level              = im.tran_level
                    and im.status                  = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         ------------------------------
         elsif L_merch_level = 12 then
            --- Level 12 = Item
            insert into deal_itemloc_temp
                       (DEAL_ID,
                      DEAL_DETAIL_ID,
                   ITEM,
                   LOC_TYPE,
                   LOCATION,
                        ORIGIN_COUNTRY_ID)
                 select distinct I_deal_id,
                        I_deal_detail_id,
                        L_item,
                       'W',
                        wh.wh,
                  L_origin_country_id
                   from wh
                  where ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL));
         end if;
      end if;
   END LOOP;  --- C_DEAL_ITEMLOC_INCLUDE loop.
   ---
   for dil_rec in C_DEAL_ITEMLOC_EXCLUDE LOOP
      --- Populate local variables.
      L_seq_no            := dil_rec.seq_no;
      L_merch_level       := dil_rec.merch_level;
      L_division          := dil_rec.division;
      L_group_no          := dil_rec.group_no;
      L_dept              := dil_rec.dept;
      L_class             := dil_rec.class;
      L_subclass          := dil_rec.subclass;
      L_item_parent       := dil_rec.item_parent;
      L_diff_1            := dil_rec.diff_1;
      L_diff_2            := dil_rec.diff_2;
      L_diff_3            := dil_rec.diff_3;
      L_diff_4            := dil_rec.diff_4;
      L_org_level         := dil_rec.org_level;
      L_chain             := dil_rec.chain;
      L_area              := dil_rec.area;
      L_region            := dil_rec.region;
      L_district          := dil_rec.district;
      L_location          := dil_rec.location;
      L_loc_type          := dil_rec.loc_type;
      L_item              := dil_rec.item;
      L_excl_ind          := dil_rec.excl_ind;

      --- Select items according to merch_level.
      if L_merch_level = 1 then
         --- Level 1 = All Items
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      ------------------------------
      elsif L_merch_level = 2 then
         --- Level 2 = Division
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     deps de,
                     groups gr,
                     division di
               where im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and L_division         = di.division
                 and di.division        = gr.division
                 and gr.group_no        = de.group_no
                 and de.dept            = im.dept
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      ------------------------------
      elsif L_merch_level = 3 then
         --- Level 3 = Group
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     deps de,
                     groups gr
               where im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and L_group_no         = gr.group_no
                 and gr.group_no        = de.group_no
                 and de.dept            = im.dept
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      ------------------------------
      elsif L_merch_level = 4 then
         --- Level 4 = Dept
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where L_dept             = im.dept
                 and im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      ------------------------------
      elsif L_merch_level = 5 then
         --- Level 5 = Class
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where L_dept             = im.dept
                 and L_class            = im.class
                 and im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      ------------------------------
      elsif L_merch_level = 6 then
         --- Level 6 = Sublass
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from item_master im,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where L_dept             = im.dept
                 and L_class            = im.class
                 and L_subclass         = im.subclass
                 and im.item_level      = im.tran_level
                 and im.status          = 'A'
                 and st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      ------------------------------
      elsif L_merch_level = 7 then
         --- Level 7 = Item Parent
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent          = im.item_parent
                 and im.item_level          = im.tran_level
                 and im.status              = 'A'
                 and st.district            = d.district
                 and d.region               = r.region
                 and r.area                 = a.area
                 and a.chain                = c.chain
                 and d.district             = nvl(L_district, d.district)
                 and r.region               = nvl(L_region,   r.region)
                 and a.area                 = nvl(L_area,     a.area)
                 and c.chain                = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      ------------------------------
      elsif L_merch_level = 8 then
         --- Level 8 = Parent/Diff 1
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent          = im.item_parent
                 and L_diff_1               = im.diff_1
                 and im.item_level          = im.tran_level
                 and im.status              = 'A'
                 and st.district            = d.district
                 and d.region               = r.region
                 and r.area                 = a.area
                 and a.chain                = c.chain
                 and d.district             = nvl(L_district, d.district)
                 and r.region               = nvl(L_region,   r.region)
                 and a.area                 = nvl(L_area,     a.area)
                 and c.chain                = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      ------------------------------
      elsif L_merch_level = 9 then
         --- Level 9 = Parent/Diff 2
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from deal_itemloc dil,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent          = im.item_parent
                 and L_diff_2               = im.diff_2
                 and im.item_level          = im.tran_level
                 and im.status              = 'A'
                 and st.district            = d.district
                 and d.region               = r.region
                 and r.area                 = a.area
                 and a.chain                = c.chain
                 and d.district             = nvl(dil.district, d.district)
                 and r.region               = nvl(dil.region,   r.region)
                 and a.area                 = nvl(dil.area,     a.area)
                 and c.chain                = nvl(dil.chain,    c.chain)
                 and ((st.store = dil.location and dil.loc_type  = 'S') or
                      (dil.location is NULL)));
      ------------------------------
      elsif L_merch_level = 10 then
         --- Level 10 = Parent/Diff 3
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     L_loc_type,
                     st.store
                from deal_itemloc dil,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent          = im.item_parent
                 and L_diff_3               = im.diff_3
                 and im.item_level          = im.tran_level
                 and im.status              = 'A'
                 and st.district            = d.district
                 and d.region               = r.region
                 and r.area                 = a.area
                 and a.chain                = c.chain
                 and d.district             = nvl(dil.district, d.district)
                 and r.region               = nvl(dil.region,   r.region)
                 and a.area                 = nvl(dil.area,     a.area)
                 and c.chain                = nvl(dil.chain,    c.chain)
                 and ((st.store = dil.location and dil.loc_type  = 'S') or
                      (dil.location is NULL)));
      ------------------------------
      elsif L_merch_level = 11 then
         --- Level 11 = Parent/Diff 4
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     im.item,
                     'S',
                     st.store
                from deal_itemloc dil,
                     chain c,
                     area a,
                     region r,
                     district d,
                     store st,
                     item_master im
               where L_item_parent          = im.item_parent
                 and L_diff_4               = im.diff_4
                 and im.item_level          = im.tran_level
                 and im.status              = 'A'
                 and st.district            = d.district
                 and d.region               = r.region
                 and r.area                 = a.area
                 and a.chain                = c.chain
                 and d.district             = nvl(dil.district, d.district)
                 and r.region               = nvl(dil.region,   r.region)
                 and a.area                 = nvl(dil.area,     a.area)
                 and c.chain                = nvl(dil.chain,    c.chain)
                 and ((st.store = dil.location and dil.loc_type  = 'S') or
                      (dil.location is NULL)));
      ------------------------------
      elsif L_merch_level = 12 then
         --- Level 12 = Item
         DELETE FROM deal_itemloc_temp
              where (deal_id,
                     deal_detail_id,
                     item,
                     loc_type,
                     location) in
             (select distinct I_deal_id,
                     I_deal_detail_id,
                     L_item,
                     'S',
                     st.store
                from chain c,
                     area a,
                     region r,
                     district d,
                     store st
               where st.district        = d.district
                 and d.region           = r.region
                 and r.area             = a.area
                 and a.chain            = c.chain
                 and d.district         = nvl(L_district, d.district)
                 and r.region           = nvl(L_region,   r.region)
                 and a.area             = nvl(L_area,     a.area)
                 and c.chain            = nvl(L_chain,    c.chain)
                 and ((st.store = L_location and L_loc_type  = 'S') or
                      (L_location is NULL)));
      end if;
      ------------------------------------
      --- Warehouse records.
      ------------------------------------
      if L_org_level is NULL or L_org_level = 5 then
         if L_merch_level = 1 then
            --- Level 1 = All Items
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh
                  where im.item_level = im.tran_level
                    and im.status     = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 2 then
            --- Level 2 = Division
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh,
                        deps de,
                        groups gr,
                        division di
                  where im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and L_division         = di.division
                    and di.division        = gr.division
                    and gr.group_no        = de.group_no
                    and de.dept            = im.dept
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 3 then
            --- Level 3 = Group
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh,
                        deps de,
                        groups gr
                  where im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and L_group_no         = gr.group_no
                    and gr.group_no        = de.group_no
                    and de.dept            = im.dept
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 4 then
            --- Level 4 = Dept
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh
                  where L_dept             = im.dept
                    and im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 5 then
            --- Level 5 = Class
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh
                  where L_dept             = im.dept
                    and L_class            = im.class
                    and im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 6 then
            --- Level 6 = Sublass
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh
                  where L_dept             = im.dept
                    and L_class            = im.class
                    and L_subclass         = im.subclass
                    and im.item_level      = im.tran_level
                    and im.status          = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 7 then
            --- Level 7 = Item Parent
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from wh,
                        item_master im
                  where L_item_parent             = im.item_parent
                    and im.item_level             = im.tran_level
                    and im.status                 = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 8 then
            --- Level 8 = Parent/Diff 1
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from wh,
                        item_master im
                  where L_item_parent          = im.item_parent
                    and L_diff_1               = im.diff_1
                    and im.item_level          = im.tran_level
                    and im.status              = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 9 then
            --- Level 9 - Parent/Diff 2
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh
                  where L_item_parent          = im.item_parent
                    and L_diff_2               = im.diff_2
                    and im.item_level          = im.tran_level
                    and im.status              = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 10 then
            --- Level 10 - Parent/Diff 3
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh
                  where L_item_parent          = im.item_parent
                    and L_diff_3               = im.diff_3
                    and im.item_level          = im.tran_level
                    and im.status              = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 11 then
            --- Level 11 - Parent/Diff 4
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        im.item,
                        'W',
                        wh.wh
                   from item_master im,
                        wh
                  where L_item_parent          = im.item_parent
                    and L_diff_4               = im.diff_4
                    and im.item_level          = im.tran_level
                    and im.status              = 'A'
                    and ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         ------------------------------
         elsif L_merch_level = 12 then
            --- Level 12 = Item
            DELETE FROM deal_itemloc_temp
                 where (deal_id,
                        deal_detail_id,
                        item,
                        loc_type,
                        location) in
                (select distinct I_deal_id,
                        I_deal_detail_id,
                        L_item,
                        'W',
                        wh.wh
                   from wh
                  where ((wh.wh = L_location and L_loc_type  = 'W') or
                         (L_location is NULL)));
         end if;
      end if;
   END LOOP;  -- C_DEAL_ITEMLOC_EXCLUDE loop.

   open C_DEALITEMLOCTEMP;
   fetch C_DEALITEMLOCTEMP
    into O_origin_country_id;

   if C_DEALITEMLOCTEMP%FOUND then
      O_valid := 'Y';
   else
      O_valid := 'N';
   end if;

   close C_DEALITEMLOCTEMP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DEAL_ITEMLOC;
--------------------------------------------------------------------------------

FUNCTION CHECK_DEAL_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT VARCHAR2,
                             I_item          IN     DEAL_ITEMLOC.ITEM%TYPE,
                             I_partner_type  IN     DEAL_HEAD.PARTNER_TYPE%TYPE,
                             I_partner_id    IN     DEAL_HEAD.PARTNER_ID%TYPE,
                             I_supplier      IN     DEAL_HEAD.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   L_program          VARCHAR2(50)     := 'RMSSUB_PRICECHANGE_UPDATE.CHECK_DEAL_SUPPLIER';
   L_dummy            VARCHAR2(1);

   cursor C_ISUPP is
   select 'X'
     from item_supplier isp,
          sups sp
    where isp.item = I_item
      and isp.supplier = sp.supplier
      and (sp.supplier = I_supplier
          or sp.supplier_parent = I_supplier);

   cursor C_ISUPP_COUNTRY is
   select 'X'
     from item_supp_country isc
    where isc.item = I_item
      and ((isc.SUPP_HIER_TYPE_1 = I_partner_type and isc.SUPP_HIER_LVL_1 = I_partner_id)
       or (isc.SUPP_HIER_TYPE_2 = I_partner_type and isc.SUPP_HIER_LVL_2 = I_partner_id)
        or (isc.SUPP_HIER_TYPE_3 = I_partner_type and isc.SUPP_HIER_LVL_3 = I_partner_id));

BEGIN
   O_valid := 'N';

   if I_partner_type = 'S' then
      open C_ISUPP;
      fetch C_ISUPP into L_dummy;

      if C_ISUPP%FOUND then
         O_valid := 'Y';
      end if;

      close C_ISUPP;
   else
      open C_ISUPP_COUNTRY;
      fetch C_ISUPP_COUNTRY into L_dummy;

      if C_ISUPP_COUNTRY%FOUND then
         O_valid := 'Y';
      end if;

      close C_ISUPP_COUNTRY;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DEAL_SUPPLIER;
--------------------------------------------------------------------------------

FUNCTION CALC_REPORTING_DATE(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_reporting_date       IN OUT DATE,
                             I_deal_id              IN     DEAL_ACTUALS_FORECAST.DEAL_ID%TYPE,
                             I_deal_detail_id       IN     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                             I_active_date          IN     DEAL_HEAD.ACTIVE_DATE%TYPE,
                             I_close_date           IN     DEAL_HEAD.CLOSE_DATE%TYPE,
                             I_deal_reporting_level IN     DEAL_HEAD.DEAL_REPORTING_LEVEL%TYPE,
                             I_vdate                IN     DATE)
   RETURN BOOLEAN IS
   L_program          VARCHAR2(50)     := 'RMSSUB_PRICECHANGE_UPDATE.CALC_REPORTING_DATE';
   L_last_rep_date    DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE := NULL;
   L_initial_rep_date DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE := NULL;
   L_next_rep_date    DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE := NULL;
   L_calc_rep_date    DEAL_ACTUALS_FORECAST.REPORTING_DATE%TYPE := NULL;

   cursor C_DEAL_ACTUALS is
   select min(reporting_date)
     from deal_actuals_forecast
    where deal_id        = I_deal_id
      and deal_detail_id = I_deal_detail_id
      and reporting_date >= I_vdate;

BEGIN

   --check if deal record already exists
   open C_DEAL_ACTUALS;
   fetch C_DEAL_ACTUALS into L_initial_rep_date;
   close C_DEAL_ACTUALS;

   --if no record exits then create a new inital record
   if L_initial_rep_date is NULL then
      if DEAL_FINANCE_SQL.CALC_INITIAL_INVOICE_DATE(O_error_message,
                                                    L_initial_rep_date,
                                                    I_active_date,
                                                    I_close_date,
                                                    I_deal_reporting_level) = FALSE then
         return FALSE;
      end if;

      if I_vdate <= L_initial_rep_date then
       O_reporting_date := L_initial_rep_date;
      else
         if I_deal_reporting_level in ( 'Q','H') then
            L_last_rep_date := L_initial_rep_date;
            L_calc_rep_date := add_months(L_initial_rep_date, 1);
         else
            L_last_rep_date := L_initial_rep_date;
            L_calc_rep_date := L_initial_rep_date + 1;
         end if;
         ---
         while L_last_rep_date <= I_close_date LOOP
            if L_calc_rep_date > I_close_date then
               L_calc_rep_date := I_close_date - 1;
            end if;
            ---
            --call function again to get the next reporting date,
            --this function accounts for gregorian and 454 calendars

            if DEAL_FINANCE_SQL.CALC_INITIAL_INVOICE_DATE(O_error_message,
                                                          L_next_rep_date,
                                                          L_calc_rep_date,
                                                          I_close_date,
                                                          I_deal_reporting_level) = FALSE then
               return FALSE;
            end if;
             --
            if I_vdate <= L_next_rep_date then
               O_reporting_date := L_next_rep_date;
               exit;
            end if;
            --
            L_last_rep_date := L_next_rep_date;
            --
            if I_deal_reporting_level in ( 'Q','H') then
               L_calc_rep_date := add_months(L_next_rep_date, 1);
            else
               L_calc_rep_date := L_next_rep_date + 1;
            end if;

         end LOOP;
     --
      end if;
   else
      O_reporting_date := L_initial_rep_date;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_REPORTING_DATE;
--------------------------------------------------------------------------------

FUNCTION CALC_ON_HAND_INV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_on_hand_inv   IN OUT NUMBER,
                          I_item          IN     DEAL_ITEMLOC.ITEM%TYPE,
                          I_location      IN     DEAL_ITEMLOC.LOCATION%TYPE,
                          I_selling_uom   IN     ITEM_MASTER.STANDARD_UOM%TYPE)
   RETURN BOOLEAN IS
   L_program          VARCHAR2(50)     := 'RMSSUB_PRICECHANGE_UPDATE.CALC_ON_HAND_INV';
   L_soh              NUMBER(12,4);
   L_soh_soum         NUMBER(12,4);

   cursor C_ITEM_LOC_SOH is
   select nvl(ils.STOCK_ON_HAND, 0) +
          nvl(ils.IN_TRANSIT_QTY, 0) +
          nvl(ils.PACK_COMP_SOH, 0) +
          nvl(ils.PACK_COMP_INTRAN, 0)
     from item_loc_soh ils
    where item = I_item
      and loc = I_location;

BEGIN

   open C_ITEM_LOC_SOH;
   fetch C_ITEM_LOC_SOH
    into L_soh;

   if C_ITEM_LOC_SOH%NOTFOUND then
      L_soh := 0;
   end if;

   close C_ITEM_LOC_SOH;

   if L_soh != 0 then
      if NOT UOM_SQL.CONVERT(O_error_message,
                             L_soh_soum,
                             I_selling_uom,
                             L_soh,
                             NULL,
                             I_item,
                             NULL,
                             NULL) then
         return FALSE;
      end if;
   else
      L_soh_soum := 0;
   end if;

   o_on_hand_inv := L_soh_soum;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_ON_HAND_INV;
--------------------------------------------------------------------------------

FUNCTION CALC_PRICE_DIFF(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_price_diff         IN OUT NUMBER,
                         I_item               IN     DEAL_ITEMLOC.ITEM%TYPE,
                         I_location           IN     DEAL_ITEMLOC.LOCATION%TYPE,
                         I_new_selling_uom    IN     ITEM_MASTER.STANDARD_UOM%TYPE,
                         I_new_selling_retail IN     NUMBER,
                         I_old_selling_uom    IN     ITEM_MASTER.STANDARD_UOM%TYPE,
                         I_old_selling_retail IN     NUMBER)
   RETURN BOOLEAN IS
   L_program          VARCHAR2(50)     := 'RMSSUB_PRICECHANGE_UPDATE.CALC_PRICE_DIFF';
   L_soh              NUMBER(12,4)  := 1;
   L_soh_soum         NUMBER(12,4);

BEGIN

   if I_new_selling_uom = I_old_selling_uom then
      L_soh_soum := 1;
   else
      if NOT UOM_SQL.CONVERT(O_error_message,
                             L_soh_soum,
                             I_new_selling_uom,
                             L_soh,
                             I_old_selling_uom,
                             I_item,
                             NULL,
                             NULL) then
         return FALSE;
      end if;
   end if;

   O_price_diff := I_old_selling_retail * L_soh_soum - I_new_selling_retail;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CALC_PRICE_DIFF;
--------------------------------------------------------------------------------

END RMSSUB_PRICECHANGE_UPDATE;
/