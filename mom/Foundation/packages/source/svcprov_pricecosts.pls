CREATE OR REPLACE PACKAGE SVCPROV_PRICECOST_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- Procedure Name : GET_PRICING_COST
-- Purpose:         The procedure calls the CORESVC_PRICECOST.GET_PRICING_COST to 
--                  process the pricing lookup message. In case of error from 
--                  the above call, the procedure calls PARSE_ERR_MSG to build 
--                  FailStatus_TBL object in the output RIB_ServiceOpStatus_REC 
--                  based on ERROR_TBL returned.
-- Called by:       Pricing Cost web service
--------------------------------------------------------------------------------
PROCEDURE GET_PRICING_COST(O_ServiceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                           O_business_object        OUT    "RIB_PrcCostColDesc_REC",
                           I_business_object        IN     "RIB_PrcCostColCriVo_REC");
                           
----------------------------------------------------------------------------------                           
END SVCPROV_PRICECOST_SQL;
/