
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEPOSIT_ITEM_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
--- Function Name:  VALIDATE_PACK
--- Purpose:        Validates that a deposit item complex contains the
---                 correct type and number of deposit items as components.
--- Author:         Faye Guerzon
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       I_item            IN       PACKITEM.PACK_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END DEPOSIT_ITEM_SQL;
/
