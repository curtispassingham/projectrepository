
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LOC_CLASSIFICATION_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------------
---Name:	CLEAR_LOC_CLSF
---Purpose:	Delete all associated classification information for the location or
---		location list for all classifications not in the input string.
--------------------------------------------------------------------------------------------
FUNCTION CLEAR_LOC_CLSF(O_error_message IN OUT VARCHAR2,
                        I_loc_list      IN     LOC_LIST_DETAIL.LOC_LIST%TYPE,
                        I_string        IN     VARCHAR2) 
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
---Name:	DELETE_LOC_CLSF
---Purpose:	Delete all associated classification information for the location or
---		location list and all classification records on loc_clsf_skulist;
--------------------------------------------------------------------------------------------
FUNCTION DELETE_LOC_CLSF(O_error_message IN OUT VARCHAR2,
                         I_location      IN     LOC_CLSF_HEAD.LOCATION%TYPE,
                         I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Name:         INSERT_LOC_CLSF_HEAD
--Purpose:      This function will update LOC_CLSF_HEAD if a record exists for a given
--              location and loc_type; if a record doesn't exist, this function
--              will insert a new record.
--------------------------------------------------------------------------------------------
FUNCTION INSERT_LOC_CLSF_HEAD(O_error_message       IN OUT VARCHAR2,
                              I_loc_list            IN     LOC_LIST_DETAIL.LOC_LIST%TYPE,
                              I_classification      IN     LOC_CLSF_HEAD.CLASSIFICATION%TYPE,
                              I_classification_type IN     LOC_CLSF_HEAD.CLASSIFICATION_TYPE%TYPE,
                              I_recalculate_ind     IN     LOC_CLSF_HEAD.RECALCULATE_IND%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--Name:         INSERT_LOC_CLSF_DETAIL
--Purpose:      This function will update LOC_CLSF_DETAIL if a record exists for a given
--              location list and class_level; if a record doesn't exist, this function
--              will insert a new record.
--------------------------------------------------------------------------------------------
FUNCTION INSERT_LOC_CLSF_DETAIL(O_error_message  IN OUT VARCHAR2,
                                I_loc_list       IN     LOC_LIST_DETAIL.LOC_LIST%TYPE,
                                I_class_level    IN     LOC_CLSF_DETAIL.CLASS_LEVEL%TYPE,
                                I_class_pct      IN     LOC_CLSF_DETAIL.CLASS_PCT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END;
/


