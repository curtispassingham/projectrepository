CREATE OR REPLACE PACKAGE ITEM_APPROVAL_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------
-- Name:  APPROVAL_CHECK
-- Purpose: this function checks to ensure
--          all the required information has been entered for a
--          given item.  If an item does not
--          have the required info a record is written to the
--          item_approval_error table and O_approved is set to FALSE.
-- This function will be public if the session is set plsql_ccflags = 'UTPLSQL:TRUE'
-- Compile private functions as public for unit testing
$if $$UTPLSQL=TRUE $then
-----------------------------------------------------------------------
FUNCTION APPROVAL_CHECK(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_approved             IN OUT   BOOLEAN,
                        I_skip_component_chk   IN       VARCHAR2,
                        I_parent_status        IN       ITEM_MASTER.STATUS%TYPE,
                        I_new_status           IN       ITEM_MASTER.STATUS%TYPE,
                        I_item                 IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
$end
-----------------------------------------------------------------------
-- Name:  PROCESS_ITEM
-- Purpose: this function is called to update the status of the inputted
--          item. An RPM event is created and records for item_approval_error 
--          are deleted, and item_master.status is updated to 'A'.  if the 
--          new status = 'S', item_master.status is updated to 'S'.  
--          If the I_single_record is Y, then the function
--          will be called for an item that is below the tran_level, so
--          the item_master record does not need to be locked (the form
--          will already be locking the record.
-----------------------------------------------------------------------
FUNCTION PROCESS_ITEM(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_new_status          IN     ITEM_MASTER.STATUS%TYPE,
                      I_single_record       IN     VARCHAR2,
                      I_item                IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Name:  ERRORS_EXIST
-- Purpose: this function checks if any errors exist on the
--         item_approval_error table for the given item.
-----------------------------------------------------------------------
FUNCTION ERRORS_EXIST(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exist                IN OUT BOOLEAN,
                      I_item                 IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------
--    Name: WORKSHEET
-- Purpose: Resets children/grandchildren status to 'W' and deletes
--          any records from the item_approval_error table for the
--          item, children and grandchildren.  O_worksheet will be
--          true if all children/grandchildren of I_item have been
--          sucessfully reset to 'W'.
-----------------------------------------------------------------------
FUNCTION WORKSHEET(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_children_worksheet  IN OUT  BOOLEAN,
                   I_item                IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Name:  APPROVE
-- Purpose: this function can be used to approve one item or all (grand/)children
--         items under a parent/grandparent item.  It calls approval_check
--         to ensure the item has all the required information associated
--         with it. if one child fails approval none of the children under
--         the inputted parent get approved.
-----------------------------------------------------------------------
FUNCTION APPROVE(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_item_approved      IN OUT  BOOLEAN,
                 O_children_approved  IN OUT  BOOLEAN,
                 I_appr_children_ind  IN      VARCHAR2,
                 I_item               IN      ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------
-- Name:  SUBMIT
-- Purpose: this function can be used to submit one item or all (grand/)children
--         items under a parent/grandparent item.  It calls approval_check
--         to ensure the item has all the required information associated
--         with it. if one child fails approval none of the children under
--         the inputted parent get approved.
-----------------------------------------------------------------------
FUNCTION SUBMIT(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                O_item_submitted      IN OUT  BOOLEAN,
                O_children_submitted  IN OUT  BOOLEAN,
                I_sub_children_ind    IN      VARCHAR2,
                I_item                IN      ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
-----------------------------------------------------------------------
-- Name:  INSERT_ERROR
-- Purpose: this function will insert the inputted error info on the
--         ITEM_APPROVAL_ERROR table.
-----------------------------------------------------------------------
FUNCTION INSERT_ERROR(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item            IN      ITEM_APPROVAL_ERROR.ITEM%TYPE,
                      I_error_key       IN      ITEM_APPROVAL_ERROR.ERROR_KEY%TYPE,
                      I_system_req_ind  IN      ITEM_APPROVAL_ERROR.SYSTEM_REQ_IND%TYPE,
                      I_override_ind    IN      ITEM_APPROVAL_ERROR.OVERRIDE_IND%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
--    Name: UPDATE_STATUS
-- Purpose: this function will update the status of the item on item master
-----------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_new_status      IN      ITEM_MASTER.STATUS%TYPE,
                       I_item            IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION SET_ALC_ITEM_TYPE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_items            IN     ITEM_TBL)
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION PERSIST_PROCESS_ITEM(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_new_status          IN     ITEM_MASTER.STATUS%TYPE,
                              I_single_record       IN     VARCHAR2,
                              I_item                IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
END;
/
