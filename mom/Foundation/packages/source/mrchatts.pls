CREATE OR REPLACE PACKAGE MERCH_ATTRIB_SQL AUTHID CURRENT_USER AS

------------------------------------------------------------
-- Function: DIVISION_NAME
-- Input:    division number
-- Output:   division name
-- Created:  12-Nov-96 Richard Stockton
------------------------------------------------------------

FUNCTION DIVISION_NAME( O_error_message   IN OUT   VARCHAR2,
            I_division     IN    NUMBER,
            O_division_name   IN OUT   VARCHAR2)
            RETURN BOOLEAN;
------------------------------------------------------------
-- Function: GROUP_NAME
-- Input:    division number
-- Output:   division name
-- Created:  12-Nov-96 Richard Stockton
------------------------------------------------------------

FUNCTION GROUP_NAME( O_error_message   IN OUT   VARCHAR2,
         I_group     IN    NUMBER,
         O_group_name   IN OUT   VARCHAR2)
         RETURN BOOLEAN;
------------------------------------------------------------
--Function: GET_GROUP_DIVISION
--Input:    group
--Output:   division name
--Created:  27-April-98 Erica Oesting
------------------------------------------------------------

FUNCTION GET_GROUP_DIVISION(O_error_message IN OUT VARCHAR2,
                            O_division      IN OUT groups.division%TYPE,
                            I_group         IN     groups.group_no%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------
-- Function Name: GET_MERCH_HIER_NAMES
-- Purpose      : This function return the department name, class
--                name, and subclass name, when given the
--                associated numbers.
-- Called by    : disppckh.fmb
-- Calls        : DEPT_ATTRIB_SQL.GET_NAME,
--                CLASS_ATTRIB_SQL.GET_NAME,
--                SUBCLASS_ATTRIB_SQL.GET_NAME
-- Input Values : O_error_message
--                O_dept_name,
--                O_class_name,
--                O_subclass_name,
--                I_dept,
--                I_class,
--                I_subclass
------------------------------------------------------------

   FUNCTION GET_MERCH_HIER_NAMES(O_error_message    IN OUT      VARCHAR2,
                                 O_dept_name        IN OUT      DEPS.DEPT_NAME%TYPE,
                                 O_class_name       IN OUT      CLASS.CLASS_NAME%TYPE,
                                 O_subclass_name    IN OUT      SUBCLASS.SUB_NAME%TYPE,
                                 I_dept             IN          DEPS.DEPT%TYPE,
                                 I_class            IN          CLASS.CLASS%TYPE,
                                 I_subclass         IN          SUBCLASS.SUBCLASS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------
--Function: CHECK_SUBCLASS_EXISTS
--Purpose : This function will check to see if a class and subclass
--          exists under the department.
--Input   : Department Number
--Output  : True or False
-------------------------------------------------------------

FUNCTION CHECK_SUBCLASS_EXISTS(O_error_message    IN OUT      VARCHAR2,
                               O_exists           IN OUT      BOOLEAN,
                               I_dept             IN          DEPS.DEPT%TYPE,
                               I_class            IN          CLASS.CLASS%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------
--Function: CHECK_SUBCLASS_ITEM_EXISTS
--Purpose : This function will check to see if an item 
--          exists under the department/class/subclass.
--Input   : Department Number
--Output  : True or False
-------------------------------------------------------------

FUNCTION CHECK_SUBCLASS_ITEM_EXISTS(O_error_message    IN OUT      VARCHAR2,
                                    O_exists           IN OUT      BOOLEAN,
                                    I_dept             IN          DEPS.DEPT%TYPE,
                                    I_class            IN          CLASS.CLASS%TYPE,
                                    I_subclass         IN          SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------
--Function: CHECK_CLASS_EXISTS
--Purpose : This function will check to see if a class 
--          exists under the department.
--Input   : Department Number
--Output  : True or False
-------------------------------------------------------------

FUNCTION CHECK_CLASS_EXISTS(O_error_message    IN OUT      VARCHAR2,
                            O_exists           IN OUT      BOOLEAN,
                            I_dept             IN          DEPS.DEPT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------
-- Function: GET_MERCH_HIER_LABEL
-- Purpose: Returns the labels for the levels of the merchandise hierarchy
-------------------------------------------------------------
FUNCTION GET_MERCH_HIER_LABEL(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_division        IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              O_group           IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              O_department      IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              O_class           IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              O_subclass        IN OUT  CODE_DETAIL.CODE_DESC%TYPE)
RETURN NUMBER;
-------------------------------------------------------------
END;
/
