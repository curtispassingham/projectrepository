CREATE OR REPLACE PACKAGE CORESVC_STORE_ADD_SQL AUTHID CURRENT_USER AS
  ------------------------------------------------------------------------------------------------
  -- Function Name: ADD_STORE
  -- Purpose      : This function contains the core logic for adding a new store to RMS.
  --                The process of adding a warehouse to RMS starts with store.fmb form. When user
  --                creates a new store using the form an entry is made to store_add table.
  --                The overall logic of the program is as follows.
  --1. Notify RPM
  -- a. Call PM_NOTIFY_API_SQL.NEW_LOCATION
  --2. Call L10N_FLEX_ATTRIB_SQL.ADD_STORE_ATTRIB and CFA_SQL.ADD_STORE_ATTRIB.
  --3. Insert cost zone
  -- a. Check if a store or wh exists for the new store's currency.
  --  If yes then new cost-zone need not be inserted.
  --  If no and if elc-ind is Y then new cost-zone needs to be inserted.
  -- b. For Corporate level zone group, add store to existing zone.
  -- c. For Location level zone group, add new zone for the store and add store to the new zone.
  -- d. For each zone level zone group, add store to the cost-zone mentioned.
  --  If no cost zone mentioned and new cost zone needs to be created (a),
  --  then create a cost zone also.
  --4. If like-store is mentioned and delivery schedule needs to be copied then
  -- copy source-delivery-schedule information (source_dlvry_sched/sched_exc/sched_days tables).
  --5. If like-store is mentioned and location close information needs to be copied then
  -- do so (company_closed_excep, location_closed).
  --6. Insert into store_hierarchy (by calling NEW_STORE)
  --7. Make stockledger entries (STKLEDGR_SQL.STOCK_LEDGER_INSERT)
  -- and copy district level location traits (LOC_TRAITS_SQL.NEW_ORG_HIER).
  --8. Copy wf_cost_relationship and deal_passthru data for the specified costing location.
  --9. If like-store is mentioned then perform like-store functionality.
  --10. Populate store_dept_area from store_dept_area_temp. Cleanup store_dept_area_temp.
  --11. Clean up  (delete store_add/ _l10n_ext/ _cfa_ext).
  ------------------------------------------------------------------------------------------------
  STATUS_01STOREADD      VARCHAR2(25):='01STOREADD'; 
  STATUS_02STOREADD_POST VARCHAR2(25):='02STOREADD_POST'; 
  STATUS_03LIKESTORE     VARCHAR2(25):='03LIKESTORE'; 
  ------------------------------------------------------------------------------------------------ 

  FUNCTION ADD_STORE(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_rms_async_id  IN RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
    RETURN BOOLEAN;
  ------------------------------------------------------------------------------------------------
  FUNCTION add_store_batch( 
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE) 
      RETURN BOOLEAN; 
  ------------------------------------------------------------------------------------------------ 
  PROCEDURE add_store_batch; 
  ------------------------------------------------------------------------------------------------ 
      FUNCTION like_store( 
      O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
      I_new_store          IN STORE_ADD.STORE%TYPE, 
      I_like_store         IN STORE_ADD.LIKE_STORE%TYPE, 
      I_new_store_currency IN STORE.CURRENCY_CODE%TYPE, 
      I_copy_repl_ind      IN VARCHAR2, 
      I_copy_clearance_ind IN VARCHAR2 ) 
      RETURN BOOLEAN; 
  ------------------------------------------------------------------------------------------------ 
  FUNCTION like_store_batch( 
      O_error_message IN OUT rtk_errors.rtk_text%type) 
      RETURN BOOLEAN; 
  ------------------------------------------------------------------------------------------------ 
  PROCEDURE like_store_batch; 
  ------------------------------------------------------------------------------------------------ 
  PROCEDURE wait4status( 
      I_status   IN store_add.process_status%type, 
      I_wait_sec IN NUMBER DEFAULT 30); 
  ------------------------------------------------------------------------------------------------ 
  FUNCTION get_likestore_pls_block( 
     I_dept               IN deps.dept%type, 
     I_commit_ctr         IN restart_control.commit_max_ctr%type, 
     I_new_store          IN store.store%type, 
     I_like_store         IN store_add.like_store%type, 
     I_new_store_currency IN store.currency_code%type, 
     I_copy_repl_ind      IN store_add.copy_repl_ind%type, 
     I_copy_clearance_ind IN store_add.copy_clearance_ind%type) 
     RETURN VARCHAR2; 
  ------------------------------------------------------------------------------------------------ 
  PROCEDURE LIKE_STORE_THREAD( 
     I_dept_thread        IN restart_program_status.thread_val%type, 
     I_commit_max_ctr     IN restart_control.commit_max_ctr%type, 
     I_new_store          IN STORE_ADD.STORE%TYPE, 
     I_like_store         IN STORE_ADD.LIKE_STORE%TYPE, 
     I_new_store_currency IN STORE.CURRENCY_CODE%TYPE, 
     I_copy_repl_ind      IN VARCHAR2, 
     I_copy_clearance_ind IN VARCHAR2 ); 
  ------------------------------------------------------------------------------------------------ 
  PROCEDURE set_process_mode( 
     I_mode IN process_config.process_mode%type); 
  ------------------------------------------------------------------------------------------------ 
  FUNCTION GET_PROCESS_MODE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                            O_Process_Mode       OUT    PROCESS_CONFIG.PROCESS_MODE%TYPE ) 
     RETURN BOOLEAN; 
  ---------------------------------------------------------------------------------------------- 

END CORESVC_STORE_ADD_SQL;
/
