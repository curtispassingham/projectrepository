CREATE OR REPLACE PACKAGE BODY RMSAIASUB_SUPPLIER_SQL AS
-------------------------------------------------------------------------------------------
--- Private function declarations
--- Private Function Name : INSERT_SUPPLIER
--- Purpose               : Inserts records for SUPPLIER into SUPS table.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_SUPPLIER (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_supplier_record   IN OUT   SUPP_ATTR_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Private Function Name : INSERT_SUPPLIER_SITES
--- Purpose               : Inserts records for SUPPLIER SITE into SUPS table.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_SUPPLIER_SITES (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_supplier_record   IN OUT   SUPP_ATTR_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Private Function Name : INSERT_ADDRESS
--- Purpose               : Inserts records for Address of SUPPLIER SITE
---                         into ADDR table.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_ADDRESS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_addr_record     IN OUT   SUPP_SITE_ADDR_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--- Private Function Name : INSERT_ORG_UNIT
--- Purpose               : Inserts records for SUPPLIER SITES + ORG UNIT
---                         into PARTNER_ORG_UNIT table.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_ORG_UNIT (O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_partner_org_unit_record   IN OUT   SUPP_SITE_OU_TBL)
RETURN BOOLEAN ;
-------------------------------------------------------------------------------------------
--- Public Function Definition
-------------------------------------------------------------------------------------------
FUNCTION PERSIST (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_supplier_record    IN OUT   SUPP_REC)
RETURN BOOLEAN IS

   L_program           VARCHAR2(255)     := 'RMSAIASUB_SUPPLIER_SQL.PERSIST';

BEGIN
   O_error_message := NULL;

   if I_supplier_record is NOT NULL then
      if I_supplier_record.supp_attr is NOT NULL then
         if INSERT_SUPPLIER (O_error_message,
                             I_supplier_record.supp_attr) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_supplier_record.supp_site_attr is NOT NULL then
         if INSERT_SUPPLIER_SITES (O_error_message,
                                   I_supplier_record.supp_site_attr) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_supplier_record.supp_site_addr is NOT NULL then
         if INSERT_ADDRESS (O_error_message,
                            I_supplier_record.supp_site_addr) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_supplier_record.supp_site_org_unit is NOT NULL then
         if INSERT_ORG_UNIT (O_error_message,
                             I_supplier_record.supp_site_org_unit) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END;
-------------------------------------------------------------------------------------------
--- Private function Definition
-------------------------------------------------------------------------------------------
FUNCTION INSERT_SUPPLIER (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_supplier_record   IN OUT   SUPP_ATTR_TBL)
RETURN BOOLEAN IS
   L_program             VARCHAR2(255)                                   := 'RMSAIASUB_SUPPLIER_SQL.INSERT_SUPPLIER';
   L_dept_level_orders   PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE := NULL;

   cursor C_DEPT_LEVEL_ORDERS is
      select dept_level_orders
        from procurement_unit_options;

BEGIN

   open C_DEPT_LEVEL_ORDERS;
   fetch C_DEPT_LEVEL_ORDERS into L_dept_level_orders;
   close C_DEPT_LEVEL_ORDERS;

   --- Insert the values in SUPS table from INPUT message
   MERGE into SUPS s
   USING TABLE(CAST(I_SUPPLIER_RECORD AS SUPP_ATTR_TBL)) SAT
   ON (sat.supplier_id = s.supplier)
   WHEN MATCHED then
      UPDATE SET s.supplier_parent          =   NULL,
                 s.sup_name                 =   sat.sup_name,
                 s.sup_name_secondary       =   sat.sup_name_secondary,
                 s.contact_name             =   NVL(sat.contact_name,s.contact_name),
                 s.contact_phone            =   NVL(sat.contact_phone,s.contact_phone),
                 s.contact_fax              =   NVL(sat.contact_fax,s.contact_fax),
                 s.contact_pager            =   NVL(sat.contact_pager,s.contact_pager),
                 s.sup_status               =   sat.sup_status,
                 s.qc_pct                   =   sat.qc_pct,
                 s.qc_freq                  =   sat.qc_freq,
                 s.vc_pct                   =   sat.vc_pct,
                 s.vc_freq                  =   sat.vc_freq,
                 s.currency_code            =   sat.currency_code,
                 s.lang                     =   sat.lang,
                 s.terms                    =   sat.terms,
                 s.freight_terms            =   sat.freight_terms,
                 s.ret_min_dol_amt          =   sat.ret_min_dol_amt,
                 s.ret_courier              =   sat.ret_courier,
                 s.handling_pct             =   sat.handling_pct,
                 s.edi_channel_id           =   sat.edi_channel_ind,
                 s.cost_chg_pct_var         =   sat.cost_chg_pct_var,
                 s.cost_chg_amt_var         =   sat.cost_chg_amt_var,
                 s.ship_method              =   NVL(sat.ship_method,s.ship_method),
                 s.payment_method           =   NVL(sat.payment_method,s.payment_method),
                 s.contact_telex            =   NVL(sat.contact_telex,s.contact_telex),
                 s.contact_email            =   NVL(sat.contact_email,s.contact_email),
                 s.invc_pay_loc             =   sat.invc_pay_loc,
                 s.invc_receive_loc         =   sat.invc_receive_loc,
                 s.comment_desc             =   sat.comment_desc,
                 s.default_item_lead_time   =   NVL(sat.default_item_lead_time,s.default_item_lead_time),
                 s.duns_number              =   NVL(sat.duns_number,s.duns_number),
                 s.duns_loc                 =   sat.duns_loc
   WHEN NOT MATCHED then
      INSERT (s.supplier,
              s.supplier_parent,
              s.sup_name,
              s.sup_name_secondary,
              s.contact_name,
              s.contact_phone,
              s.contact_fax,
              s.contact_pager,
              s.sup_status,
              s.qc_ind,
              s.qc_pct,
              s.qc_freq,
              s.vc_ind,
              s.vc_pct,
              s.vc_freq,
              s.currency_code,
              s.lang,
              s.terms,
              s.freight_terms,
              s.ret_allow_ind,
              s.ret_auth_req,
              s.ret_min_dol_amt,
              s.ret_courier,
              s.handling_pct,
              s.edi_po_ind,
              s.edi_po_chg,
              s.edi_po_confirm,
              s.edi_asn,
              s.edi_sales_rpt_freq,
              s.edi_supp_available_ind,
              s.edi_contract_ind,
              s.edi_invc_ind,
              s.edi_channel_id,
              s.cost_chg_pct_var,
              s.cost_chg_amt_var,
              s.replen_approval_ind,
              s.ship_method,
              s.payment_method,
              s.contact_telex,
              s.contact_email,
              s.settlement_code,
              s.pre_mark_ind,
              s.auto_appr_invc_ind,
              s.dbt_memo_code,
              s.freight_charge_ind,
              s.auto_appr_dbt_memo_ind,
              s.prepay_invc_ind,
              s.backorder_ind,
              s.vat_region,
              s.inv_mgmt_lvl,
              s.service_perf_req_ind,
              s.invc_pay_loc,
              s.invc_receive_loc,
              s.addinvc_gross_net,
              s.delivery_policy,
              s.comment_desc,
              s.default_item_lead_time,
              s.duns_number,
              s.duns_loc,
              s.bracket_costing_ind,
              s.vmi_order_status,
              s.dsd_ind,
              s.scale_aip_orders,
              s.sup_qty_level,
              s.external_ref_id)
      VALUES (sat.supplier_id,
              NULL,
              sat.sup_name,
              sat.sup_name_secondary,
              sat.contact_name,
              sat.contact_phone,
              sat.contact_fax,
              sat.contact_pager,
              sat.sup_status,
              sat.qc_ind,
              sat.qc_pct,
              sat.qc_freq,
              sat.vc_ind,
              sat.vc_pct,
              sat.vc_freq,
              sat.currency_code,
              sat.lang,
              sat.terms,
              sat.freight_terms,
              sat.ret_allow_ind,
              sat.ret_auth_req,
              sat.ret_min_dol_amt,
              sat.ret_courier,
              sat.handling_pct,
              sat.edi_po_ind,
              sat.edi_po_chg,
              sat.edi_po_confirm,
              sat.edi_asn,
              sat.edi_sales_rpt_freq,
              sat.edi_supp_available_ind,
              sat.edi_contract_ind,
              sat.edi_invc_ind,
              sat.edi_channel_ind,
              sat.cost_chg_pct_var,
              sat.cost_chg_amt_var,
              sat.replen_approval_ind,
              sat.ship_method,
              sat.payment_method,
              sat.contact_telex,
              sat.contact_email,
              sat.settlement_code,
              sat.pre_mark_ind,
              sat.auto_appr_invc_ind,
              sat.dbt_memo_code,
              sat.freight_charge_ind,
              sat.auto_appr_dbt_memo_ind,
              sat.prepay_invc_ind,
              sat.backorder_ind,
              sat.vat_region,
              decode(L_dept_level_orders, 'Y','D','S'),
              sat.service_perf_req_ind,
              sat.invc_pay_loc,
              sat.invc_receive_loc,
              sat.addinvc_gross_net,
              sat.delivery_policy,
              sat.comment_desc,
              sat.default_item_lead_time,
              sat.duns_number,
              sat.duns_loc,
              sat.bracket_costing_ind,
              sat.vmi_order_status,
              sat.dsd_ind,
              sat.scale_aip_orders,
              sat.sup_qty_level,
              sat.sup_xref_key);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END;
-------------------------------------------------------------------------------------------
--- Private Function Name : INSERT_SUPPLIER_SITES
--- Purpose               : Inserts records for SUPPLIER SITE into SUPS table.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_SUPPLIER_SITES (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_supplier_record   IN OUT   SUPP_ATTR_TBL)
RETURN BOOLEAN IS
   L_program             VARCHAR2(255)                                   := 'RMSAIASUB_SUPPLIER_SQL.INSERT_SUPPLIER_SITES';
   L_dept_level_orders   PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE := NULL;

   cursor C_DEPT_LEVEL_ORDERS is
      select dept_level_orders
        from procurement_unit_options;

BEGIN

   open C_DEPT_LEVEL_ORDERS;
   fetch C_DEPT_LEVEL_ORDERS into L_dept_level_orders;
   close C_DEPT_LEVEL_ORDERS;

   --- Insert the values in SUPS table from INPUT message
   MERGE into SUPS s
   USING TABLE(CAST(I_SUPPLIER_RECORD AS SUPP_ATTR_TBL)) SAT
   ON (sat.supplier_id = s.supplier)
   WHEN MATCHED then
      UPDATE SET s.supplier_parent          =   sat.supplier_parent,
                 s.sup_name                 =   sat.sup_name,
                 s.sup_name_secondary       =   sat.sup_name_secondary,
                 s.contact_name             =   NVL(sat.contact_name,s.contact_name),
                 s.contact_phone            =   NVL(sat.contact_phone,s.contact_phone),
                 s.contact_fax              =   NVL(sat.contact_fax,s.contact_fax),
                 s.contact_pager            =   NVL(sat.contact_pager,s.contact_pager),
                 s.sup_status               =   sat.sup_status,
                 s.qc_pct                   =   sat.qc_pct,
                 s.qc_freq                  =   sat.qc_freq,
                 s.vc_pct                   =   sat.vc_pct,
                 s.vc_freq                  =   sat.vc_freq,
                 s.currency_code            =   sat.currency_code,
                 s.lang                     =   sat.lang,
                 s.terms                    =   sat.terms,
                 s.freight_terms            =   sat.freight_terms,
                 s.ret_min_dol_amt          =   sat.ret_min_dol_amt,
                 s.ret_courier              =   sat.ret_courier,
                 s.handling_pct             =   sat.handling_pct,
                 s.edi_channel_id           =   sat.edi_channel_ind,
                 s.cost_chg_pct_var         =   sat.cost_chg_pct_var,
                 s.cost_chg_amt_var         =   sat.cost_chg_amt_var,
                 s.ship_method              =   NVL(sat.ship_method,s.ship_method),
                 s.payment_method           =   NVL(sat.payment_method,s.payment_method),
                 s.contact_telex            =   NVL(sat.contact_telex,s.contact_telex),
                 s.contact_email            =   NVL(sat.contact_email,s.contact_email),
                 s.invc_pay_loc             =   sat.invc_pay_loc,
                 s.invc_receive_loc         =   sat.invc_receive_loc,
                 s.comment_desc             =   sat.comment_desc,
                 s.default_item_lead_time   =   NVL(sat.default_item_lead_time,s.default_item_lead_time),
                 s.duns_number              =   NVL(sat.duns_number,s.duns_number),
                 s.duns_loc                 =   sat.duns_loc
   WHEN NOT MATCHED then
      INSERT (s.supplier,
              s.supplier_parent,
              s.sup_name,
              s.sup_name_secondary,
              s.contact_name,
              s.contact_phone,
              s.contact_fax,
              s.contact_pager,
              s.sup_status,
              s.qc_ind,
              s.qc_pct,
              s.qc_freq,
              s.vc_ind,
              s.vc_pct,
              s.vc_freq,
              s.currency_code,
              s.lang,
              s.terms,
              s.freight_terms,
              s.ret_allow_ind,
              s.ret_auth_req,
              s.ret_min_dol_amt,
              s.ret_courier,
              s.handling_pct,
              s.edi_po_ind,
              s.edi_po_chg,
              s.edi_po_confirm,
              s.edi_asn,
              s.edi_sales_rpt_freq,
              s.edi_supp_available_ind,
              s.edi_contract_ind,
              s.edi_invc_ind,
              s.edi_channel_id,
              s.cost_chg_pct_var,
              s.cost_chg_amt_var,
              s.replen_approval_ind,
              s.ship_method,
              s.payment_method,
              s.contact_telex,
              s.contact_email,
              s.settlement_code,
              s.pre_mark_ind,
              s.auto_appr_invc_ind,
              s.dbt_memo_code,
              s.freight_charge_ind,
              s.auto_appr_dbt_memo_ind,
              s.prepay_invc_ind,
              s.backorder_ind,
              s.vat_region,
              s.inv_mgmt_lvl,
              s.service_perf_req_ind,
              s.invc_pay_loc,
              s.invc_receive_loc,
              s.addinvc_gross_net,
              s.delivery_policy,
              s.comment_desc,
              s.default_item_lead_time,
              s.duns_number,
              s.duns_loc,
              s.bracket_costing_ind,
              s.vmi_order_status,
              s.dsd_ind,
              s.scale_aip_orders,
              s.sup_qty_level,
              s.external_ref_id)
      VALUES (sat.supplier_id,
              sat.supplier_parent,
              sat.sup_name,
              sat.sup_name_secondary,
              sat.contact_name,
              sat.contact_phone,
              sat.contact_fax,
              sat.contact_pager,
              sat.sup_status,
              sat.qc_ind,
              sat.qc_pct,
              sat.qc_freq,
              sat.vc_ind,
              sat.vc_pct,
              sat.vc_freq,
              sat.currency_code,
              sat.lang,
              sat.terms,
              sat.freight_terms,
              sat.ret_allow_ind,
              sat.ret_auth_req,
              sat.ret_min_dol_amt,
              sat.ret_courier,
              sat.handling_pct,
              sat.edi_po_ind,
              sat.edi_po_chg,
              sat.edi_po_confirm,
              sat.edi_asn,
              sat.edi_sales_rpt_freq,
              sat.edi_supp_available_ind,
              sat.edi_contract_ind,
              sat.edi_invc_ind,
              sat.edi_channel_ind,
              sat.cost_chg_pct_var,
              sat.cost_chg_amt_var,
              sat.replen_approval_ind,
              sat.ship_method,
              sat.payment_method,
              sat.contact_telex,
              sat.contact_email,
              sat.settlement_code,
              sat.pre_mark_ind,
              sat.auto_appr_invc_ind,
              sat.dbt_memo_code,
              sat.freight_charge_ind,
              sat.auto_appr_dbt_memo_ind,
              sat.prepay_invc_ind,
              sat.backorder_ind,
              sat.vat_region,
              decode(L_dept_level_orders, 'Y','D','S'),
              sat.service_perf_req_ind,
              sat.invc_pay_loc,
              sat.invc_receive_loc,
              sat.addinvc_gross_net,
              sat.delivery_policy,
              sat.comment_desc,
              sat.default_item_lead_time,
              sat.duns_number,
              sat.duns_loc,
              sat.bracket_costing_ind,
              sat.vmi_order_status,
              sat.dsd_ind,
              sat.scale_aip_orders,
              sat.sup_qty_level,
              sat.supsite_xref_key);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END;
-------------------------------------------------------------------------------------------
--- Private Function Name : INSERT_ADDRESS
--- Purpose               : Inserts records for SUPPLIER and SUPPLIER SITE
---                         into SUPS table.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_ADDRESS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_addr_record     IN OUT   SUPP_SITE_ADDR_TBL)
RETURN BOOLEAN IS
   L_program           VARCHAR2(255)     := 'RMSAIASUB_SUPPLIER_SQL.INSERT_ADDRESS';

BEGIN
   MERGE into addr a
   USING (select sup_addr.key_value_1,
                 sup_addr.addr_key,
                 sup_addr.addr_type,
                 sup_addr.primary_addr_ind,
                 sup_addr.add_1,
                 sup_addr.add_2,
                 sup_addr.add_3,
                 sup_addr.city,
                 sup_addr.state,
                 sup_addr.country_id,
                 sup_addr.post,
                 sup_addr.contact_name,
                 sup_addr.contact_phone,
                 sup_addr.contact_fax,
                 sup_addr.contact_email,
                 sup_addr.jurisdiction_code,
                 sup_addr.addr_xref_key,
                ((row_number() over (partition by sup_addr.key_value_1,sup_addr.addr_type order by c.seq_no desc)) + addr_temp.seq) new_seq_no
           from TABLE(CAST(I_addr_record AS SUPP_SITE_ADDR_TBL)) sup_addr,
                sups,
                addr c,
                (  select supadr.key_value_1,supadr.addr_type,NVL(max(b.seq_no),0) seq
                     from addr b,
                          TABLE(CAST(I_addr_record AS SUPP_SITE_ADDR_TBL)) supadr
                    where b.module(+)      = 'SUPP'
                      and b.key_value_1(+) = supadr.key_value_1
                      and b.addr_type  (+) = supadr.addr_type
                 group by supadr.key_value_1, supadr.addr_type
                )addr_temp 
          where sups.supplier         = sup_addr.key_value_1
            and addr_temp.key_value_1 = sup_addr.key_value_1
            and addr_temp.addr_type   = sup_addr.addr_type
            and c.addr_key(+)         = sup_addr.addr_key) sat
   ON ( a.addr_key = sat.addr_key)
   WHEN MATCHED then
   UPDATE SET a.primary_addr_ind    =   sat.primary_addr_ind,
              a.add_1               =   sat.add_1,
              a.add_2               =   sat.add_2,
              a.add_3               =   sat.add_3,
              a.city                =   sat.city,
              a.state               =   sat.state,
              a.country_id          =   sat.country_id,
              a.post                =   sat.post,
              a.contact_name        =   sat.contact_name,
              a.contact_phone       =   sat.contact_phone,
              a.contact_fax         =   sat.contact_fax,
              a.contact_email       =   sat.contact_email,
              a.jurisdiction_code   =   sat.jurisdiction_code
   WHEN NOT MATCHED then
   INSERT (a.key_value_1,
           a.addr_key,
           a.module,
           a.addr_type,
           a.seq_no,
           a.primary_addr_ind,
           a.add_1,
           a.add_2,
           a.add_3,
           a.city,
           a.state,
           a.country_id,
           a.post,
           a.contact_name,
           a.contact_phone,
           a.contact_fax,
           a.contact_email,
           a.jurisdiction_code,
           a.external_ref_id)
   VALUES (sat.key_value_1,
           sat.addr_key,
           'SUPP',
           sat.addr_type,
           sat.new_seq_no,
           sat.primary_addr_ind,
           sat.add_1,
           sat.add_2,
           sat.add_3,
           sat.city,
           sat.state,
           sat.country_id,
           sat.post,
           sat.contact_name,
           sat.contact_phone,
           sat.contact_fax,
           sat.contact_email,
           sat.jurisdiction_code,
           sat.addr_xref_key);


   -- Updating primary_addr_ind for one address each of all address_types to Y for whom all address have a value of 'N'
   UPDATE addr adr1
      SET adr1.primary_addr_ind = 'Y'
    WHERE adr1.module = 'SUPP'
      AND adr1.key_value_1 in (select TO_CHAR(key_value_1)
                                 from TABLE(CAST(I_addr_record AS SUPP_SITE_ADDR_TBL)))
      AND NOT EXISTS (select adr2.addr_type
                        from addr adr2
                       where adr2.primary_addr_ind = 'Y'
                         and adr2.module = 'SUPP'
                         and adr1.key_value_1 = adr2.key_value_1
                         and adr1.addr_type = adr2.addr_type)
      AND adr1.addr_key = (select min(addr_key)
                             from addr adr3
                            where adr3.module = 'SUPP'
                              and adr3.key_value_1 = adr1.key_value_1
                              and adr3.addr_type = adr1.addr_type);

   --Remove address types from the translation table.
   DELETE FROM addr_tl 
         WHERE addr_key in(select a.addr_key
                           from addr a
                           WHERE a.module = 'SUPP'
                             AND a.key_value_1 IN (SELECT distinct TO_CHAR(key_value_1)
                                                     FROM TABLE(CAST(I_addr_record AS SUPP_SITE_ADDR_TBL)))
                             AND a.addr_type in ('04','06')
                             AND NOT EXISTS (SELECT m.address_type
                                               FROM add_type_module m
                                              WHERE m.module        = 'SUPP'
                                                AND m.mandatory_ind = 'Y'
                                                AND m.address_type  = a.addr_type)
                             AND NOT EXISTS (SELECT input.addr_type
                                               FROM TABLE(CAST(I_addr_record AS SUPP_SITE_ADDR_TBL)) input
                                              WHERE TO_CHAR(input.key_value_1) = a.key_value_1
                                                AND input.addr_key             = a.addr_key
                                                AND input.addr_type            = a.addr_type));

   --Remove address types not in Input
   DELETE FROM addr a
         WHERE a.module = 'SUPP'
           AND a.key_value_1 IN (SELECT distinct TO_CHAR(key_value_1)
                                   FROM TABLE(CAST(I_addr_record AS SUPP_SITE_ADDR_TBL)))
           AND a.addr_type in ('04','06')
           AND NOT EXISTS (SELECT m.address_type
                             FROM add_type_module m
                            WHERE m.module        = 'SUPP'
                              AND m.mandatory_ind = 'Y'
                              AND m.address_type  = a.addr_type)
           AND NOT EXISTS (SELECT input.addr_type
                             FROM TABLE(CAST(I_addr_record AS SUPP_SITE_ADDR_TBL)) input
                            WHERE TO_CHAR(input.key_value_1) = a.key_value_1
                              AND input.addr_key             = a.addr_key
                              AND input.addr_type            = a.addr_type);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END;
-------------------------------------------------------------------------------------------
--- Private Function Name : INSERT_ADDRESS
--- Purpose               : Inserts records for SUPPLIER and SUPPLIER SITE
---                         into SUPS table.
-------------------------------------------------------------------------------------------
FUNCTION INSERT_ORG_UNIT (O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_partner_org_unit_record   IN OUT   SUPP_SITE_OU_TBL)
RETURN BOOLEAN IS
   L_program           VARCHAR2(255)     := 'RMSAIASUB_SUPPLIER_SQL.INSERT_ORG_UNIT';

BEGIN

   MERGE into partner_org_unit a
   USING (select sup_ou.supplier_site_id,
                 sup_ou.org_unit_id,
                 sup_ou.primary_pay_site
           from TABLE(CAST(I_partner_org_unit_record AS SUPP_SITE_OU_TBL)) sup_ou,
                sups, org_unit ou
          where sups.supplier = sup_ou.SUPPLIER_SITE_ID and
                ou.org_unit_id = sup_ou.org_unit_id ) sat
   ON (sat.supplier_site_id = a.partner and sat.org_unit_id = a.org_unit_id)
   WHEN MATCHED then
   UPDATE SET a.primary_pay_site  = sat.primary_pay_site
   WHEN NOT MATCHED then
   INSERT (a.partner,
           a.org_unit_id,
           a.partner_type,
           a.primary_pay_site)
   VALUES (sat.supplier_site_id,
           sat.org_unit_id,
           'U',
           sat.primary_pay_site);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END;
-------------------------------------------------------------------------------------------
END RMSAIASUB_SUPPLIER_SQL;
/