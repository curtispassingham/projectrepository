
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XITEMRCLS_VALIDATE AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message     IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_itemreclass_rec   OUT    NOCOPY   RECLASS_SQL.RECLASS_REC,
                       I_message           IN              "RIB_XItemRclsDesc_REC",
                       I_message_type      IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message     IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_itemreclass_rec   OUT    NOCOPY   RECLASS_SQL.RECLASS_REC,
                       I_message           IN              "RIB_XItemRclsRef_REC",
                       I_message_type      IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------

END RMSSUB_XITEMRCLS_VALIDATE;
/
