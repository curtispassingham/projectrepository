CREATE OR REPLACE PACKAGE RMSMFM_WH AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC VARIABLES
--------------------------------------------------------------------------------

FAMILY    CONSTANT RIB_SETTINGS.FAMILY%TYPE := 'WH';

HDR_ADD   CONSTANT VARCHAR2(15) := 'whcre';
HDR_UPD   CONSTANT VARCHAR2(15) := 'whmod';
HDR_DEL   CONSTANT VARCHAR2(15) := 'whdel';
DTL_ADD   CONSTANT VARCHAR2(15) := 'whdtlcre';
DTL_UPD   CONSTANT VARCHAR2(15) := 'whdtlmod';
DTL_DEL   CONSTANT VARCHAR2(15) := 'whdtldel';
--
WHA_ADD   CONSTANT VARCHAR2(15) := 'whaddcre';
WHA_UPD   CONSTANT VARCHAR2(15) := 'whaddmod';


--------------------------------------------------------------------------------
-- PUBLIC TYPES
--------------------------------------------------------------------------------

TYPE WH_KEY_REC IS RECORD
(
wh                 NUMBER,
addr_key           NUMBER,
wh_type            VARCHAR2(1),
pricing_loc        NUMBER,
pricing_loc_curr   VARCHAR2(3)
);


--------------------------------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg           OUT VARCHAR2,
                I_message_type     IN     VARCHAR2,
                I_wh_key_rec       IN     WH_KEY_REC,
                I_addr_publish_ind IN     ADDR.PUBLISH_IND%TYPE)
RETURN BOOLEAN;

PROCEDURE GETNXT(O_status_code     OUT VARCHAR2,
                 O_error_msg       OUT VARCHAR2,
                 O_message_type    OUT VARCHAR2,
                 O_message         OUT RIB_OBJECT,
                 O_bus_obj_id      OUT RIB_BUSOBJID_TBL,
                 O_routing_info    OUT RIB_ROUTINGINFO_TBL,
                 I_num_threads  IN     NUMBER DEFAULT 1,
                 I_thread_val   IN     NUMBER DEFAULT 1);

PROCEDURE PUB_RETRY(O_status_code     OUT VARCHAR2,
                    O_error_msg       OUT VARCHAR2,
                    O_message_type IN OUT VARCHAR2,
                    O_message         OUT RIB_OBJECT,
                    O_bus_obj_id   IN OUT RIB_BUSOBJID_TBL,
                    O_routing_info IN OUT RIB_ROUTINGINFO_TBL,
                    I_ref_object   IN     RIB_OBJECT);

END RMSMFM_WH;
/
