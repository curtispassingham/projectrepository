create or replace PACKAGE CORESVC_UOM_CLASS AUTHID CURRENT_USER  AS
   template_key                  CONSTANT VARCHAR2(255):='UOM_CLASS_DATA';
   action_new                             VARCHAR2(25)          := 'NEW';
   action_mod                             VARCHAR2(25)          := 'MOD';
   action_del                             VARCHAR2(25)          := 'DEL';
 
   UOM_X_CONVERSION_sheet                  VARCHAR2(255)         := 'UOM_X_CONVERSION';
   UOM_X_CONVERSION$Action                 NUMBER                :=1;
   UOM_X_CONVERSION$CONVERT_SQL            NUMBER                :=4;
   UOM_X_CONVERSION$TO_UOM_CLASS           NUMBER                :=3;
   UOM_X_CONVERSION$FRM_UOM_CLASS         NUMBER                :=2;
   
   UOM_CONVERSION_sheet                   VARCHAR2(255)         := 'UOM_CONVERSION';
   UOM_CONVERSION$Action                  NUMBER                :=1;
   UOM_CONVERSION$OPERATOR                NUMBER                :=5;
   UOM_CONVERSION$FACTOR                  NUMBER                :=4;
   UOM_CONVERSION$TO_UOM                  NUMBER                :=3;
   UOM_CONVERSION$FROM_UOM                NUMBER                :=2;
   
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
   
   template_category CODE_DETAIL.CODE%TYPE := 'RMSFND';
   TYPE UOM_CONVERSION_rec_tab IS TABLE OF UOM_CONVERSION%ROWTYPE;
   UOM_CLASS_TL_sheet                     VARCHAR2(255)         := 'UOM_CLASS_TL';
   UOM_CLASS_TL$Action                    NUMBER                :=1;
   UOM_CLASS_TL$UOM_DESC_TRANS            NUMBER                :=5;
   UOM_CLASS_TL$UOM_TRANS                 NUMBER                :=4;
   UOM_CLASS_TL$UOM                       NUMBER                :=3;
   UOM_CLASS_TL$LANG                      NUMBER                :=2;

   TYPE UOM_CLASS_TL_rec_tab IS TABLE OF UOM_CLASS_TL%ROWTYPE;
   UOM_CLASS_sheet                        VARCHAR2(255)         := 'UOM_CLASS';
   UOM_CLASS$Action                       NUMBER                :=1;
   UOM_CLASS$UOM_CLASS                    NUMBER                :=3;
   UOM_CLASS$UOM                          NUMBER                :=2;
   UOM_CLASS$UOM_DESC_TRANS               NUMBER                :=5;
   UOM_CLASS$UOM_TRANS                    NUMBER                :=4;

   TYPE UOM_CLASS_rec_tab IS TABLE OF UOM_CLASS%ROWTYPE;

   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;

   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER
                        )
   RETURN BOOLEAN;

   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
END CORESVC_UOM_CLASS;
/