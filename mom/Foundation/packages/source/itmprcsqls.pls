CREATE OR REPLACE PACKAGE ITEM_PRICING_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------
-- name: GET_BASE_RETAIL
-- purpose: this function is a wrapper to pricing_attrib_sql.get_base_zone_retail
-- it is only used by itemxformb.pls, rmsmfm_items_buildb.pls, and rmssub_xitemvalb.pls
-- due to scope, these functions cannot be changed to call pricing_attrib_sql
-- but any new function needing a base retail should NOT call this function.
-- it should call pricing_attrib_sql.
-------------------------------------------------------------------------
FUNCTION GET_BASE_RETAIL(
    O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_base_retail              IN OUT item_loc.unit_retail%TYPE,
    I_item                     IN     item_loc.item%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
END ITEM_PRICING_SQL;
/
