CREATE OR REPLACE PACKAGE BODY CORESVC_XPRICE_SQL AS

LP_BULK_FETCH_LIMIT       NUMBER := 1000;

-------------------------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_ITEM_LOC
   -- Purpose      : This function will have the query for inserting data into svc_pricing_event_temp 
   --                table from svc_pricing_event_head and svc_pricing_event_locs table
-------------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEM_LOC(O_error_message   IN OUT VARCHAR2)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_SOH_DETAILS
   -- Purpose      : This function will update SOH details from Item_loc_soh for the given record in 
   --                svc_pricing_event_temp table
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_SOH_DETAILS(O_error_message   IN OUT VARCHAR2,
                            I_thread_val      IN     NUMBER,
                            I_chunk_id        IN     NUMBER)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_LOC_CURRENCY
   -- Purpose      : This function will update location currency from Store or WH table
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_LOC_CURRENCY(O_error_message   IN OUT VARCHAR2,
                             I_thread_val      IN     NUMBER,
                             I_chunk_id        IN     NUMBER)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_PROMO_RETAIL
   -- Purpose      : This function will calculate promo_retail based on promo_selling_retail using
   --                Standard UOM and Promo_Selling_UOM
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PROMO_RETAIL(O_error_message   IN OUT         VARCHAR2,
                             O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_NEW_UNIT_RETAIL
   -- Purpose      : This function will calculate unit_retail based on selling_unit_retail using
   --                Standard UOM and Selling_UOM
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_NEW_UNIT_RETAIL(O_error_message   IN OUT VARCHAR2,
                                O_xprice_tbl      IN OUT CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_ORIG_UNIT_RETAIL
   -- Purpose      : This function will calculate orig_unit_retail based on retail in price_hist table.
   --                This value will be used during Tran_data Postings.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORIG_UNIT_RETAIL(O_error_message   IN OUT VARCHAR2,
                                 O_xprice_tbl      IN OUT CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CONVERT_UOM
   -- Purpose      : This function will be used to convert retail values from one UOM to another
-------------------------------------------------------------------------------------------------------
FUNCTION CONVERT_UOM(O_error_message      IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     O_to_unit_retail        OUT NOCOPY   ITEM_LOC.UNIT_RETAIL%TYPE,
                     I_to_uom             IN              UOM_CLASS.UOM%TYPE,
                     I_from_unit_retail   IN              ITEM_LOC.UNIT_RETAIL%TYPE,
                     I_from_uom           IN              UOM_CLASS.UOM%TYPE,
                     I_item               IN              ITEM_MASTER.ITEM%TYPE) 
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CONVERT_UOM
   -- Purpose      : This function calculates beginning of period date.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_FDOH_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_fdoh_date          OUT   DATE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CONVERT_UOM
   -- Purpose      : This function validates records after explosion in svc_pricing_event_temp table.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TEMP_DETAILS(O_error_message   IN OUT VARCHAR2,
                               I_thread_val      IN     NUMBER,
                               I_chunk_id        IN     NUMBER,
                               I_batch_ind       IN     VARCHAR2)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: PROCESS_BASE_RETAIL
   -- Purpose      : This function processes records of event_type 'BASE RETAIL'
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_BASE_RETAIL(O_error_message   IN OUT VARCHAR2,
                             I_thread_val      IN     NUMBER,
                             I_chunk_id        IN     NUMBER)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: PROCESS_REGCLR_EVENTS
   -- Purpose      : This function processes records of Clearance ,Clearance Resets and Regular price
   --                change event types.
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_REGCLR_EVENTS(O_error_message   IN OUT VARCHAR2,
                               O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: PROCESS_PROMO_EVENTS
   -- Purpose      : This function processes records of Promotion Start and Promotion End event types.
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PROMO_EVENTS(O_error_message   IN OUT VARCHAR2,
                              O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_ITEM_LOC
   -- Purpose      : This function updates item_loc records based on input collection 
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC(O_error_message   IN OUT         VARCHAR2,
                         O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_REPL_ITEM_LOC
   -- Purpose      : This function updates repl_item_loc records based on input collection.Called in 
   --                case of Clearance Price Events only.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_REPL_ITEM_LOC(O_error_message   IN OUT         VARCHAR2,
                              O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_TRAN_DATA
   -- Purpose      : This function inserts records into Tran data . It also populates sup_data table for
   --                Clearance Markdowns.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA(O_error_message   IN OUT VARCHAR2,
                          O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: MARK_DUPLICATES
   -- Purpose      : This function marks duplicate item loc records to be skipped . Lowest hierarchy
   --                level in item and org level is given priority.
-------------------------------------------------------------------------------------------------------
FUNCTION MARK_DUPLICATES(O_error_message   IN OUT VARCHAR2,
                         I_thread_val      IN     NUMBER,
                         I_chunk_id        IN     NUMBER)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_PRICE_HIST
   -- Purpose      : This function inserts records into Price_hist table based on input collection.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_PRICE_HIST(O_error_message   IN OUT VARCHAR2,
                           O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------

FUNCTION EXPLODE_DETAILS(O_error_message   IN OUT VARCHAR2,
                         I_process_id      IN     NUMBER)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.EXPLODE_DETAILS';
   HEAD_EXPL_EXC          EXCEPTION;
   --VAL_TEMP_EXC           EXCEPTION;
   
BEGIN
   BEGIN
       update svc_pricing_event_head 
         set process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
       where process_id = I_process_id
         and process_status = CORESVC_XPRICE_SQL.STATUS_NEW; 

      if EXPLODE_ITEM_LOC(O_error_message) = FALSE then
         raise HEAD_EXPL_EXC;
      end if;

      update svc_pricing_event_head 
         set process_status = CORESVC_XPRICE_SQL.STATUS_EXPLODED
       where process_id = I_process_id;

   EXCEPTION
      when HEAD_EXPL_EXC then
         update svc_pricing_event_head
            set process_status = CORESVC_XPRICE_SQL.STATUS_ERROR
          where process_id = I_process_id;
         return FALSE;    
   END;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_DETAILS;
-------------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_DETAILS(O_error_message   IN OUT VARCHAR2,
                         I_effective_date  IN     DATE)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.EXPLODE_DETAILS';
   HEAD_EXPL_EXC          EXCEPTION;
   
BEGIN
   update svc_pricing_event_head 
      set process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
    where effective_date = I_effective_date
      and process_status = CORESVC_XPRICE_SQL.STATUS_NEW; 

   if EXPLODE_ITEM_LOC(O_error_message) = FALSE then
      return FALSE;
   end if;

   update svc_pricing_event_head 
      set process_status = CORESVC_XPRICE_SQL.STATUS_EXPLODED
    where effective_date = I_effective_date;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_DETAILS;
-------------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEM_LOC(O_error_message   IN OUT VARCHAR2)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.EXPLODE_ITEM_LOC';
BEGIN
   insert 
       into svc_pricing_event_temp(process_id           ,
                                   thread_val           ,
                                   chunk_id             ,
                                   process_status       ,
                                   event_type           ,
                                   event_id             ,
                                   item_rank            ,
                                   item                 ,
                                   item_parent          ,
                                   item_grandparent     ,
                                   dept                 ,
                                   class                ,
                                   subclass             ,
                                   item_level           ,
                                   tran_level           ,
                                   status               ,
                                   pack_ind             ,
                                   sellable_ind         ,
                                   orderable_ind        ,
                                   catch_weight_ind     ,
                                   pack_type            ,
                                   standard_uom         ,
                                   hier_rank            ,
                                   location             ,
                                   loc_type             ,
                                   item_soh             ,
                                   effective_date       ,
                                   currency_code        ,
                                   orig_unit_retail     ,
                                   old_unit_retail      ,
                                   new_unit_retail      ,
                                   selling_unit_retail  ,
                                   selling_uom          ,
                                   multi_units          ,
                                   multi_unit_retail    ,
                                   multi_selling_uom    ,
                                   promo_selling_retail ,
                                   promo_selling_uom    ,
                                   il_promo_selling_retail,
                                   il_selling_unit_retail,
                                   il_selling_uom       ,
                                   primary_supp         ,
                                   clear_ind            ,
                                   loc_currency_code    ,
                                   event_similarity     ,
                                   price_hist_tran_type ,
                                   error_message)
                            select speh.process_id as PROCESS_ID,
                                   NULL as PROCESS_THREAD_VAL,
                                   NULL as PROCESS_CHUNK_ID,
                                   'N' as PROCESS_STATUS,
                                   speh.event_type as EVENT_TYPE,
                                   speh.event_id as EVENT_ID,
                                   CASE 
                                      when speh.item_level = im.tran_level then
                                         1
                                      when ((im.tran_level - speh.item_level) = 1 and speh.diff_id is not NULL) then
                                         2
                                      when ((im.tran_level - speh.item_level) = 1 and speh.diff_id is NULL) then
                                         3
                                      when ((im.tran_level - speh.item_level) = 2 and speh.diff_id is not NULL) then
                                         4
                                      when ((im.tran_level - speh.item_level) = 2 and speh.diff_id is NULL) then
                                         5
                                      else 
                                         6
                                   END as ITEM_RANK,
                                   im.item,
                                   im.item_parent,
                                   im.item_grandparent,
                                   im.dept,
                                   im.class,
                                   im.subclass,
                                   im.item_level,
                                   im.tran_level,
                                   im.status,
                                   im.pack_ind,
                                   im.sellable_ind,
                                   im.orderable_ind,
                                   im.catch_weight_ind,
                                   im.pack_type,
                                   im.standard_uom,
                                   DECODE(speh.hier_level,'S',1,
                                                          'DI',2,
                                                          'RE',3,
                                                          'AR',4,
                                                          'CH',5,
                                                          'W',1,
                                                           6) as HIER_RANK,
                                   il.loc as LOC,
                                   il.loc_type as LOC_TYPE,
                                   null as item_soh,
                                   speh.effective_date,
                                   speh.currency_code,
                                   null as Orig_unit_retail, -- From Price_hist
                                   il.Unit_retail as Old_unit_retail, -- from Item_loc
                                   null as New_unit_retail, -- Converted value in SUOM for speh.selling_unit_retail
                                   speh.selling_unit_retail,
                                   speh.selling_uom,
                                   speh.multi_units,
                                   speh.multi_unit_retail,
                                   speh.multi_selling_uom,
                                   speh.promo_selling_retail,
                                   speh.promo_selling_uom,
                                   il.promo_selling_retail as il_promo_selling_retail,
                                   il.selling_unit_retail as il_selling_unit_retail,
                                   il.selling_uom as il_selling_uom,
                                   il.primary_supp,
                                   il.clear_ind,
                                   null as loc_currency_code,
                                   DECODE(speh.event_type,'REG',1,
                                                          'CLRS',1,
                                                          'CLRE',1,
                                                          'PROMS',2,
                                                          'PROME',2,
                                                          3) as event_similarity,
                                   case 
                                      when speh.event_type = 'REG' and (speh.selling_unit_retail is not null and speh.multi_unit_retail is null) then
                                         4
                                      when speh.event_type = 'REG' and (speh.selling_unit_retail is null and speh.multi_unit_retail is not null) then
                                         10
                                      when speh.event_type = 'REG' and (speh.selling_unit_retail is not null and speh.multi_unit_retail is not null) then
                                         11
                                      when speh.event_type = 'CLRS' then
                                         8
                                      when speh.event_type = 'CLRE' then
                                         4
                                      when speh.event_type = 'PROMS' then
                                         9
                                      when speh.event_type = 'PROME' and il.clear_ind = 'Y' then
                                         8
                                      when speh.event_type = 'PROME' and il.clear_ind = 'N' then
                                         4
                                   end as Price_hist_tran_type,
                                   null as error_message
                              from svc_pricing_event_head speh,
                                   svc_pricing_event_locs spel,
                                   item_master im,
                                   store_hierarchy sh,
                                   item_loc il
                             where speh.process_id = spel.process_id
                               and speh.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
                               and im.item = il.item
                               and im.item_level = im.tran_level
                               and im.status = 'A'
                               and im.sellable_ind = 'Y'
                               and (  (il.loc_type = 'S' 
                                       and il.loc = sh.store) 
                                     or
                                      (il.loc_type = 'W' 
                                       and speh.hier_level = 'W')
                                    )
                               and (speh.item = im.item
                                    or speh.item = im.item_parent
                                    or speh.item = im.item_grandparent
                                    )
                               and (speh.diff_id is NULL
                                    or ( speh.diff_id = im.diff_1
                                      or speh.diff_id = im.diff_2
                                      or speh.diff_id = im.diff_3
                                      or speh.diff_id = im.diff_4)
                                    )
                                and (
                                       (speh.hier_level ='S' 
                                        and sh.store    = spel.hier_value)
                                    or (speh.hier_level ='DI' 
                                        and sh.district = spel.hier_value)
                                    or (speh.hier_level ='RE' 
                                        and sh.region   = spel.hier_value)
                                    or (speh.hier_level ='AR' 
                                        and sh.area     = spel.hier_value)
                                    or (speh.hier_level ='CH' 
                                        and sh.chain    = spel.hier_value)
                                    or (speh.hier_level ='W' 
                                        and il.loc = spel.hier_value)
                                    )
                         UNION 
                            select speh.process_id as PROCESS_ID,
                                   NULL as PROCESS_THREAD_VAL,
                                   NULL as PROCESS_CHUNK_ID,
                                   'N' as PROCESS_STATUS,
                                   speh.event_type as EVENT_TYPE,
                                   speh.event_id as EVENT_ID,
                                   CASE 
                                      when speh.item_level = im.tran_level then
                                         1
                                      when ((im.tran_level - speh.item_level) = 1 and speh.diff_id is not NULL) then
                                         2
                                      when ((im.tran_level - speh.item_level) = 1 and speh.diff_id is NULL) then
                                         3
                                      when ((im.tran_level - speh.item_level) = 2 and speh.diff_id is not NULL) then
                                         4
                                      when ((im.tran_level - speh.item_level) = 2 and speh.diff_id is NULL) then
                                         5
                                      else 
                                         6
                                   END as ITEM_RANK,
                                   im.item,
                                   im.item_parent,
                                   im.item_grandparent,
                                   im.dept,
                                   im.class,
                                   im.subclass,
                                   im.item_level,
                                   im.tran_level,
                                   im.status,
                                   im.pack_ind,
                                   im.sellable_ind,
                                   im.orderable_ind,
                                   im.catch_weight_ind,
                                   im.pack_type,
                                   im.standard_uom,
                                   7 as HIER_RANK,
                                   -1 as LOC,-- Loc is assigned as -1 for threading
                                   null as LOC_TYPE,
                                   null as item_soh,
                                   speh.effective_date,
                                   speh.currency_code,
                                   null as Orig_unit_retail, -- From Price_hist
                                   null as Old_unit_retail, -- from Item_loc
                                   null as New_unit_retail, -- Converted value in SUOM for speh.selling_unit_retail
                                   speh.selling_unit_retail,
                                   speh.selling_uom,
                                   speh.multi_units,
                                   speh.multi_unit_retail,
                                   speh.multi_selling_uom,
                                   speh.promo_selling_retail,
                                   speh.promo_selling_uom,
                                   null as il_promo_selling_retail,
                                   null as il_selling_unit_retail,
                                   null as il_selling_uom,
                                   null as primary_supp,
                                   null as clear_ind,
                                   null as loc_currency_code,
                                   3 as event_similarity,
                                   null as Price_hist_tran_type,
                                   null as error_message
                              from svc_pricing_event_head speh,
                                   item_master im
                             where speh.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
                               and speh.event_type = 'BASERT'
                               and im.item_level = im.tran_level
                               and im.status = 'A'
                               and im.sellable_ind = 'Y'
                               and (speh.item = im.item
                                    or speh.item = im.item_parent
                                    or speh.item = im.item_grandparent
                                    )
                               and (speh.diff_id is NULL
                                    or ( speh.diff_id = im.diff_1
                                      or speh.diff_id = im.diff_2
                                      or speh.diff_id = im.diff_3
                                      or speh.diff_id = im.diff_4)
                                    )
                               order by item,loc;
                               
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_ITEM_LOC;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_SOH_DETAILS(O_error_message   IN OUT VARCHAR2,
                            I_thread_val      IN     NUMBER,
                            I_chunk_id        IN     NUMBER)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.UPDATE_SOH_DETAILS';
BEGIN
   merge into svc_pricing_event_temp spet
      using (select ils.item,
                    ils.loc,
                    ils.loc_type,
                    (ils.stock_on_hand + ils.in_transit_qty +
                       NVL(ils.pack_comp_soh,0) + NVL(ils.pack_comp_intran,0)) item_soh
               from item_loc_soh ils
             ) input
         on (    spet.item           = input.item
             and spet.location       = input.loc
             and spet.loc_type       = input.loc_type
             and spet.thread_val     = I_thread_val
             and spet.chunk_id       = I_chunk_id
             and spet.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
             and spet.event_type in ('CLRS','CLRE','REG')
             )   
        when MATCHED then
      update 
         set spet.item_soh    = input.item_soh;
             
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_SOH_DETAILS;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_LOC_CURRENCY(O_error_message   IN OUT VARCHAR2,
                             I_thread_val      IN     NUMBER,
                             I_chunk_id        IN     NUMBER)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.UPDATE_LOC_CURRENCY';
BEGIN
   merge into svc_pricing_event_temp spet
      using (select spet2.item,
                    spet2.location,
                    spet2.loc_type,
                    spet2.loc_currency_code,
                    spet2.event_type,
                    wh.currency_code as in_currency_code 
               from svc_pricing_event_temp spet2,
                    wh wh
              where spet2.thread_val = I_thread_val
                and spet2.chunk_id   = I_chunk_id
                and spet2.event_type <> 'PROME'
                and spet2.loc_type   = 'W'
                and spet2.location   = wh.wh
                and spet2.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
               UNION
             select spet2.item,
                    spet2.location,
                    spet2.loc_type,
                    spet2.loc_currency_code,
                    spet2.event_type,
                    st.currency_code as in_currency_code 
               from svc_pricing_event_temp spet2,
                    store st
              where spet2.thread_val = I_thread_val
                and spet2.chunk_id   = I_chunk_id
                and spet2.event_type <> 'PROME'
                and spet2.loc_type   = 'S'
                and spet2.location   = st.store
                and spet2.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS) input
         on (    spet.thread_val     = I_thread_val
             and spet.chunk_id       = I_chunk_id
             and spet.event_type     = input.event_type
             and spet.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS 
             and spet.location       = input.location
             and spet.item           = input.item )
      when MATCHED then
       update
          set spet.loc_currency_code = input.in_currency_code;
             
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_LOC_CURRENCY;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_NEW_UNIT_RETAIL(O_error_message   IN OUT         VARCHAR2,
                                O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.UPDATE_NEW_UNIT_RETAIL';

BEGIN
   for i in 1..O_xprice_tbl.count loop
      if O_xprice_tbl(i).standard_uom <> O_xprice_tbl(i).selling_uom then
         if CONVERT_UOM(O_error_message,
                        O_xprice_tbl(i).new_unit_retail,
                        O_xprice_tbl(i).standard_uom,
                        O_xprice_tbl(i).selling_unit_retail,
                        O_xprice_tbl(i).selling_uom,
                        O_xprice_tbl(i).item) = FALSE then 
            return FALSE;
         end if;
      else
         O_xprice_tbl(i).new_unit_retail := O_xprice_tbl(i).selling_unit_retail;
      end if;
   end loop;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_NEW_UNIT_RETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PROMO_RETAIL(O_error_message   IN OUT         VARCHAR2,
                             O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.UPDATE_PROMO_RETAIL';

BEGIN
   for i in 1..O_xprice_tbl.count loop
      if (O_xprice_tbl(i).standard_uom <> O_xprice_tbl(i).promo_selling_uom) then
         if CONVERT_UOM(O_error_message,
                        O_xprice_tbl(i).promo_retail,
                        O_xprice_tbl(i).standard_uom,
                        O_xprice_tbl(i).promo_selling_retail,
                        O_xprice_tbl(i).promo_selling_uom,
                        O_xprice_tbl(i).item) = FALSE then 
            return FALSE;
         end if;
      else
         O_xprice_tbl(i).promo_retail := O_xprice_tbl(i).promo_selling_retail;
      end if;
   end loop;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_PROMO_RETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORIG_UNIT_RETAIL(O_error_message   IN OUT         VARCHAR2,
                                 O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.UPDATE_ORIG_UNIT_RETAIL';
   L_fdoh_date            DATE;
   L_item                 PRICE_HIST.ITEM%TYPE;
   L_loc                  PRICE_HIST.LOC%TYPE;
   
   cursor C_GET_ORIG_RETAIL is 
      select ph1.unit_retail 
        from price_hist ph1
       where ph1.item = L_item
         and ph1.loc = L_loc
         and ph1.tran_type = 0 
         and ph1.action_date > L_fdoh_date  -- item_loc created on or after the beginning of period;
                                             -- get the retail when new item_loc is created
      union all 
      select ph2.unit_retail
        from price_hist ph2
       where ph2.item = L_item
         and ph2.loc =  L_loc
         and ph2.tran_type in (0, 4, 11)
         and ph2.action_date = (select /*+ FIRST_ROWS */
                                       max(ph3.action_date)
                                  from price_hist ph3
                                 where ph3.item = ph2.item
                                   and ph3.loc = ph2.loc
                                   and ph3.tran_type in (0, 4, 11)
                                   and ph3.action_date <= L_fdoh_date
                                   and rownum >= 0)
         and ph2.action_date <= L_fdoh_date
       order by 1 ;  -- item_loc created before the beginning of period;
                                              -- get the latest price change up to the beginning of period;
                                              -- if non-exists, get the retail when new item_loc is created   
BEGIN
   -- get beginning of perod date 
   if not GET_FDOH_DATE(O_error_message,
                        L_fdoh_date) then
      return FALSE;
   end if;
   
   L_item := NULL;
   L_loc  := NULL;
   for i in 1..O_xprice_tbl.count loop
      L_item := O_xprice_tbl(i).item;
      L_loc  := O_xprice_tbl(i).location;
      open C_GET_ORIG_RETAIL;
      fetch C_GET_ORIG_RETAIL into O_xprice_tbl(i).orig_unit_retail;
      close C_GET_ORIG_RETAIL;
   end loop;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ORIG_UNIT_RETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_FDOH_DATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_fdoh_date          OUT   DATE)
return BOOLEAN IS

   L_program       VARCHAR2(50) := 'CORESVC_XPRICE_SQL.GET_FDOH_DATE';
   L_half_no       PERIOD.HALF_NO%TYPE := 0;
   L_day           NUMBER(2);
   L_month         NUMBER(2);
   L_year          NUMBER(4);
   L_cal           VARCHAR2(1);
   L_return_code   VARCHAR2(10);

   cursor C_HALF is
      select half_no
        from period;

   cursor C_CALENDAR is
      select 'x'
        from system_options
       where calendar_454_ind = 'C';

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_HALF', 'PERIOD', NULL);
   open C_HALF;

   SQL_LIB.SET_MARK('FETCH', 'C_HALF', 'PERIOD', NULL);
   fetch C_HALF into L_half_no;

   SQL_LIB.SET_MARK('CLOSE', 'C_HALF', 'PERIOD', NULL);
   close C_HALF;

   SQL_LIB.SET_MARK('OPEN', 'C_CALENDAR', 'SYSTEM_OPTIONS', NULL);
   open C_CALENDAR;

   SQL_LIB.SET_MARK('FETCH', 'C_CALENDAR', 'SYSTEM_OPTIONS', NULL);
   fetch C_CALENDAR into L_cal;

   if C_CALENDAR%FOUND then
      HALF_TO_CAL_FDOH(L_half_no,
                       L_day,
                       L_month,
                       L_year,
                       L_return_code,
                       O_error_message);

      if L_return_code = 'FALSE' then
         SQL_LIB.SET_MARK('CLOSE', 'C_CALENDAR', 'SYSTEM_OPTIONS', NULL);
         close C_CALENDAR;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_DATE_CONV', NULL, NULL, NULL);
         return FALSE;
      else
         O_fdoh_date := TO_DATE(TO_CHAR(L_day,'09')||TO_CHAR(L_month,'09')||
                        TO_CHAR(L_year,'0999'),'DDMMYYYY');
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_CALENDAR', 'SYSTEM_OPTIONS', NULL);
      close C_CALENDAR;
   else
      HALF_TO_454_FDOH(L_half_no,
                       L_day,
                       L_month,
                       L_year,
                       L_return_code,
                       O_error_message);

      if L_return_code = 'FALSE' then
         SQL_LIB.SET_MARK('CLOSE', 'C_CALENDAR', 'SYSTEM_OPTIONS', NULL);
         close C_CALENDAR;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_DATE_CONV', NULL, NULL, NULL);
         return FALSE;
      else
         O_fdoh_date := TO_DATE(TO_CHAR(L_day,'09')||TO_CHAR(L_month,'09')||
                        TO_CHAR(L_year,'0999'),'DDMMYYYY');
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_CALENDAR', 'SYSTEM_OPTIONS', NULL);
      close C_CALENDAR;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_FDOH_DATE;
-------------------------------------------------------------------------------------------------------
FUNCTION CONVERT_UOM(O_error_message      IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     O_to_unit_retail        OUT NOCOPY   ITEM_LOC.UNIT_RETAIL%TYPE,
                     I_to_uom             IN              UOM_CLASS.UOM%TYPE,
                     I_from_unit_retail   IN              ITEM_LOC.UNIT_RETAIL%TYPE,
                     I_from_uom           IN              UOM_CLASS.UOM%TYPE,
                     I_item               IN              ITEM_MASTER.ITEM%TYPE) 
return BOOLEAN is

   L_program     VARCHAR2(50) := 'CORESVC_XPRICE_SQL.CONVERT_UOM';
   L_same_class  BOOLEAN      := FALSE;
   
BEGIN

   if not UOM_SQL.CHECK_CLASS(O_error_message,
                              L_same_class,
                              I_to_uom,
                              I_from_uom) then
      return FALSE;
   end if;
   
   if L_same_class then 
      /* Note: from uom and to uom are passed in reversed order to return the converted unit_retail */
      if not UOM_SQL.CONVERT(O_error_message,
                             O_to_unit_retail,
                             I_from_uom,
                             I_from_unit_retail,
                             I_to_uom,
                             I_item,
                             NULL,
                             NULL) then
         return FALSE;
      end if;
   else 
      O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV', 
                                            I_to_uom, 
                                            I_from_uom,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CONVERT_UOM;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TEMP_DETAILS(O_error_message   IN OUT VARCHAR2,
                               I_thread_val      IN     NUMBER,
                               I_chunk_id        IN     NUMBER,
                               I_batch_ind       IN     VARCHAR2)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.VALIDATE_TEMP_DETAILS';
   L_merge_cnt            NUMBER := 0;
   
BEGIN

   merge into svc_pricing_event_temp spet
      using (select *
               from (select spet.process_id,
                            spet.location,
                            spet.item,
                            spet.rowid,
                            spet.thread_val,
                            spet.chunk_id,
                            -- validate that item is not on clearance for Regular Price Change
                            case
                               when spet.clear_ind = 'Y' and spet.event_type = 'REG' then
                                  SQL_LIB.GET_MESSAGE_TEXT('CLEAR_NO_PRICE_CHG', spet.item, spet.location, NULL)||';'
                            end ||
                            -- validate that location currency is same as currency being passed in
                            case
                               when (spet.currency_code is NOT NULL and (spet.loc_currency_code <> spet.currency_code)) then
                                  SQL_LIB.GET_MESSAGE_TEXT('LOC_CUR_NO_MATCH', NULL, NULL, NULL)||';'
                            end ||
                            -- validate that item is on clearance for Clearance Reset
                            case
                               when spet.clear_ind ='N' and spet.event_type = 'CLRE' then
                                  SQL_LIB.GET_MESSAGE_TEXT('ITEM_NO_CLEAR', spet.item, spet.location, NULL)||';'
                            end ||
                            -- validate that item has promo_selling_retail for Promotion End
                            case
                               when spet.il_promo_selling_retail is NULL and spet.event_type = 'PROME' then
                                  SQL_LIB.GET_MESSAGE_TEXT('ITEM_NO_PROM', spet.item, spet.location, NULL)||';'
                            end error_message
                       from svc_pricing_event_temp spet
                      where thread_val     = I_thread_val
                        and chunk_id       = I_chunk_id
                        and process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS)
              where error_message is NOT NULL) inner
      on (spet.rowid              = inner.rowid
          and spet.item           = inner.item
          and spet.location       = inner.location)
      when matched then
         update set spet.error_message      = substr(spet.error_message || inner.error_message, 1, 2000),
                    spet.process_status     = CORESVC_XPRICE_SQL.STATUS_ERROR;

   L_merge_cnt := SQL%ROWCOUNT;
   
   if I_batch_ind = 'N' then
      if L_merge_cnt > 0 then
         O_error_message := SQL_LIB.CREATE_MSG('PC_PROCESS_ERR',NULL,NULL,NULL);
         return FALSE; 
      end if;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_TEMP_DETAILS;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DETAILS(O_error_message   IN OUT VARCHAR2,
                         I_thread_val      IN     NUMBER,
                         I_chunk_id        IN     NUMBER,
                         I_batch_ind       IN     VARCHAR2 DEFAULT 'Y')
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.PROCESS_DETAILS';
   L_xprice_tbl           CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL;
   PROCESS_ERR            EXCEPTION;
   
   cursor C_GET_BASERT_RECS is 
      select *
        from svc_pricing_event_temp
       where thread_val     = I_thread_val
         and chunk_id       = I_chunk_id
         and process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
         and event_type     = 'BASERT'
         and rownum = 1;
         
   cursor C_GET_REG_RECS is 
      select *
        from svc_pricing_event_temp
       where thread_val     = I_thread_val
         and chunk_id       = I_chunk_id
         and process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
         and event_type     = 'REG';
         
   cursor C_GET_CLRS_RECS is 
      select *
        from svc_pricing_event_temp
       where thread_val     = I_thread_val
         and chunk_id       = I_chunk_id
         and process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS         
         and event_type     = 'CLRS';
         
   cursor C_GET_CLRE_RECS is 
      select *
        from svc_pricing_event_temp
       where thread_val     = I_thread_val
         and chunk_id       = I_chunk_id
         and process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
         and event_type     = 'CLRE';
         
   cursor C_GET_PROMS_RECS is 
      select *
        from svc_pricing_event_temp
       where thread_val     = I_thread_val
         and chunk_id       = I_chunk_id
         and process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
         and event_type     = 'PROMS';
         
   cursor C_GET_PROME_RECS is 
      select *
        from svc_pricing_event_temp
       where thread_val     = I_thread_val
         and chunk_id       = I_chunk_id
         and process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
         and event_type     = 'PROME';

BEGIN

   update svc_pricing_event_temp spet
      set process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
    where thread_val     = I_thread_val
      and chunk_id       = I_chunk_id
      and process_status = CORESVC_XPRICE_SQL.STATUS_NEW;
   
   -- Mark records to be ignored while processing
   if I_batch_ind = 'Y' then 
      if MARK_DUPLICATES(O_error_message,
                         I_thread_val,
                         I_chunk_id) = FALSE then
         return FALSE;
      end if;
   end if;
   -- Process all Base Retail updates
   L_xprice_tbl.DELETE();
   open C_GET_BASERT_RECS;
   fetch C_GET_BASERT_RECS BULK COLLECT into L_xprice_tbl;
   close C_GET_BASERT_RECS;
   if L_xprice_tbl.count > 0 then
      if PROCESS_BASE_RETAIL(O_error_message,
                             I_thread_val,
                             I_chunk_id) = FALSE then
         return FALSE;
      end if;
   end if;

   -- Update Loc currency in Temp Table
   if UPDATE_LOC_CURRENCY(O_error_message,
                          I_thread_val,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- Validate Records in Temp Table 
   if VALIDATE_TEMP_DETAILS(O_error_message,
                            I_thread_val,
                            I_chunk_id,
                            I_batch_ind) = FALSE then
      return FALSE;
   end if;

   -- Update SOH details for inserting into Tran_data
   if UPDATE_SOH_DETAILS(O_error_message,
                         I_thread_val,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   -- Regular Price Changes 
   L_xprice_tbl.DELETE();
   open C_GET_REG_RECS;
   LOOP
      fetch C_GET_REG_RECS BULK COLLECT into L_xprice_tbl LIMIT LP_BULK_FETCH_LIMIT;
      EXIT when L_xprice_tbl.count = 0;

      if PROCESS_REGCLR_EVENTS(O_error_message,
                               L_xprice_tbl) = FALSE then
         close C_GET_REG_RECS;
         return FALSE;
      end if;
   END LOOP;
   close C_GET_REG_RECS;
   
   -- Clearances
   L_xprice_tbl.DELETE();
   open C_GET_CLRS_RECS;
   LOOP
      fetch C_GET_CLRS_RECS BULK COLLECT into L_xprice_tbl LIMIT LP_BULK_FETCH_LIMIT;
      EXIT when L_xprice_tbl.count = 0;

      if PROCESS_REGCLR_EVENTS(O_error_message,
                               L_xprice_tbl) = FALSE then
         close C_GET_CLRS_RECS;
         return FALSE;
      end if;
   END LOOP;
   close C_GET_CLRS_RECS;
   
   --Clearance Reset
   L_xprice_tbl.DELETE();
   open C_GET_CLRE_RECS;
   LOOP
      fetch C_GET_CLRE_RECS BULK COLLECT into L_xprice_tbl LIMIT LP_BULK_FETCH_LIMIT;
      EXIT when L_xprice_tbl.count = 0;

      if PROCESS_REGCLR_EVENTS(O_error_message,
                               L_xprice_tbl) = FALSE then
         close C_GET_CLRE_RECS;
         return FALSE;
      end if;
   END LOOP;
   close C_GET_CLRE_RECS;
   
   --Promotion Start
   L_xprice_tbl.DELETE();
   open C_GET_PROMS_RECS;
   LOOP
      fetch C_GET_PROMS_RECS BULK COLLECT into L_xprice_tbl LIMIT LP_BULK_FETCH_LIMIT;
      EXIT when L_xprice_tbl.count = 0;

      if PROCESS_PROMO_EVENTS(O_error_message,
                              L_xprice_tbl) = FALSE then
         close C_GET_PROMS_RECS;
         return FALSE;
      end if;
   END LOOP;
   close C_GET_PROMS_RECS;
   
   --Promotion End
   L_xprice_tbl.DELETE();
   open C_GET_PROME_RECS;
   LOOP
      fetch C_GET_PROME_RECS BULK COLLECT into L_xprice_tbl LIMIT LP_BULK_FETCH_LIMIT;
      EXIT when L_xprice_tbl.count = 0;

      if PROCESS_PROMO_EVENTS(O_error_message,
                              L_xprice_tbl) = FALSE then
         close C_GET_PROME_RECS;
         return FALSE;
      end if;
   END LOOP;
   close C_GET_PROME_RECS;

   update svc_pricing_event_temp
      set process_status = CORESVC_XPRICE_SQL.STATUS_PROCESSED
    where thread_val     = I_thread_val
      and chunk_id       = I_chunk_id
      and process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS;

   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_BASERT_RECS%ISOPEN then
         close C_GET_BASERT_RECS;
      end if;
      if C_GET_REG_RECS%ISOPEN then
         close C_GET_REG_RECS;
      end if;
      if C_GET_CLRS_RECS%ISOPEN then
         close C_GET_CLRS_RECS;
      end if;
      if C_GET_CLRE_RECS%ISOPEN then
         close C_GET_CLRE_RECS;
      end if;
      if C_GET_PROMS_RECS%ISOPEN then
         close C_GET_PROMS_RECS;
      end if;
      if C_GET_PROME_RECS%ISOPEN then
         close C_GET_PROME_RECS;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_DETAILS;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DETAILS(O_error_message   IN OUT VARCHAR2,
                         I_process_id      IN     NUMBER)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.PROCESS_DETAILS';
   
BEGIN
   update svc_pricing_event_temp spet
      set thread_val = 1,
          chunk_id   = 1
    where process_id = I_process_id;
    
   if PROCESS_DETAILS(O_error_message,
                      1,
                      1,
                      'N') = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_DETAILS;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_BASE_RETAIL(O_error_message   IN OUT VARCHAR2,
                             I_thread_val      IN     NUMBER,
                             I_chunk_id        IN     NUMBER)
return BOOLEAN is 
   L_function              VARCHAR2(50) := 'CORESVC_XPRICE_SQL.PROCESS_BASE_RETAIL';
   L_table                 VARCHAR2(30) := null;
   
   --L_vdate                 DATE := GET_VDATE();
   ---
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE;
   L_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE;
   L_locked_acquired       VARCHAR2(1) := 'Y';
      
   cursor C_GET_LOCK_PARAM is
      select NVL(retry_lock_attempts,2),
             NVL(retry_wait_time,0)
        from rms_plsql_batch_config
       where program_name = 'CORESVC_XPRICE_SQL';
   
   cursor C_LOCK_ITEM_MASTER is 
     select 'x'
       from item_master im,
            svc_pricing_event_temp spet
      where spet.thread_val = I_thread_val
        and chunk_id        = I_chunk_id
        and spet.event_type = 'BASERT'
        and spet.item = im.item
        for update of im.curr_selling_unit_retail nowait;
   
BEGIN
   open C_GET_LOCK_PARAM;
   fetch C_GET_LOCK_PARAM into L_retry_lock_attempts,
                               L_retry_wait_time;
   close C_GET_LOCK_PARAM;
   --
   L_table := 'ITEM_MASTER';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   
   for i in 1..(L_retry_lock_attempts - 1) loop
      ---
      BEGIN
         open C_LOCK_ITEM_MASTER;
         close C_LOCK_ITEM_MASTER;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   end loop;
   
   if L_locked_acquired != 'Y' then
      open C_LOCK_ITEM_MASTER;
      close C_LOCK_ITEM_MASTER;
   else
      merge into item_master im
         using ( select item,
                        selling_unit_retail,
                        selling_uom,
                        event_rank
                  from  ( select item,
                                 selling_unit_retail,
                                 selling_uom,
                                 dense_rank() over (partition by item order by event_id desc) event_rank
                            from svc_pricing_event_temp spet
                           where spet.thread_val = I_thread_val
                             and spet.chunk_id   = I_chunk_id
                             and spet.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
                             and event_type = 'BASERT')
                   where event_rank = 1          
                ) inner
            on (im.item = inner.item)
            when matched then
               update set im.curr_selling_unit_retail = inner.selling_unit_retail,
                          im.curr_selling_uom         = inner.selling_uom;
   end if;
   
   -----------------------------------------------------
   -- Update the status for Temp table records
   update svc_pricing_event_temp spet
      set spet.process_status = CORESVC_XPRICE_SQL.STATUS_PROCESSED
    where spet.thread_val = I_thread_val
      and spet.chunk_id   = I_chunk_id
      and event_type = 'BASERT';
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
        
      return FALSE;
      
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_BASE_RETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_REGCLR_EVENTS(O_error_message   IN OUT VARCHAR2,
                               O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is   
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.PROCESS_REGCLR_EVENTS';

BEGIN
   -- Convert from Selling UOM to Standard UOM

   if UPDATE_NEW_UNIT_RETAIL(O_error_message,
                             O_xprice_tbl) = FALSE then
      return FALSE;
   end if;

   -- Get Orig Unit Retail Values from Price Hist for 
   -- inserting into Tran Data
   if UPDATE_ORIG_UNIT_RETAIL(O_error_message,
                              O_xprice_tbl) = FALSE then
      return FALSE;
   end if;

   -- For Clearance Events , Deactivate Replenishment
   if O_xprice_tbl(1).event_type = 'CLRS' then
      if UPDATE_REPL_ITEM_LOC(O_error_message,
                              O_xprice_tbl) = FALSE then
         return FALSE;
      end if; 
   end if;

   -- Update Item Loc Table
   if UPDATE_ITEM_LOC(O_error_message,
                      O_xprice_tbl) = FALSE then
      return FALSE;
   end if;

   -- Insert Into tran data 
   if INSERT_TRAN_DATA(O_error_message,
                       O_xprice_tbl) = FALSE then 
      return FALSE;
   end if;

   -- Insert into PRICE_HIST
   if INSERT_PRICE_HIST(O_error_message,
                        O_xprice_tbl) = FALSE then 
      return FALSE;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_REGCLR_EVENTS;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_PROMO_EVENTS(O_error_message   IN OUT VARCHAR2,
                              O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is   
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.PROCESS_PROMO_EVENTS';

BEGIN
   -- Convert Promo Retail from Selling UOM to Standard UOM
   if O_xprice_tbl(1).event_type = 'PROMS' then
      if UPDATE_PROMO_RETAIL(O_error_message,
                             O_xprice_tbl) = FALSE then
         return FALSE;
      end if;
   end if;
  
   -- Update Item Loc Table
   if UPDATE_ITEM_LOC(O_error_message,
                      O_xprice_tbl) = FALSE then
      return FALSE;
   end if;
   

   -- Insert into PRICE_HIST
   if INSERT_PRICE_HIST(O_error_message,
                        O_xprice_tbl) = FALSE then 
      return FALSE;
   end if;
   
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_PROMO_EVENTS;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC(O_error_message   IN OUT         VARCHAR2,
                         O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is 
   L_function              VARCHAR2(50) := 'CORESVC_XPRICE_SQL.UPDATE_ITEM_LOC';
   L_table                 VARCHAR2(30) := NULL;

   L_user                  VARCHAR2(20) := GET_USER;
   L_date                  DATE         := SYSDATE;
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE;
   L_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE;
   L_locked_acquired       VARCHAR2(1) := 'Y';
   L_err_rec               NUMBER;
   L_thread_val            SVC_PRICING_EVENT_TEMP.THREAD_VAL%TYPE;
   L_chunk_id              SVC_PRICING_EVENT_TEMP.CHUNK_ID%TYPE;
   L_event_type            SVC_PRICING_EVENT_TEMP.EVENT_TYPE%TYPE;
   
   cursor C_GET_LOCK_PARAM is
      select NVL(retry_lock_attempts,2),
             NVL(retry_wait_time,0)
        from rms_plsql_batch_config
       where program_name = 'CORESVC_XPRICE_SQL';
   
   cursor C_LOCK_ITEM_LOC is 
      select 'x'
        from item_loc il,
             svc_pricing_event_temp spet
       where spet.thread_val     = L_thread_val
         and spet.chunk_id       = L_chunk_id
         and spet.event_type     = L_event_type 
         and spet.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
         and spet.item           = il.item
         and spet.location       = il.loc
         for update of il.selling_unit_retail nowait;
BEGIN
   open C_GET_LOCK_PARAM;
   fetch C_GET_LOCK_PARAM into L_retry_lock_attempts,
                               L_retry_wait_time;
   close C_GET_LOCK_PARAM;
     
   L_table := 'ITEM_LOC';
   
   L_thread_val := O_xprice_tbl(1).thread_val;
   L_chunk_id   := O_xprice_tbl(1).chunk_id;
   L_event_type := O_xprice_tbl(1).event_type;
   
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   for i in 1..(L_retry_lock_attempts - 1) loop
      ---
      BEGIN
         open C_LOCK_ITEM_LOC;
         close C_LOCK_ITEM_LOC;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   end loop;
   
      if L_locked_acquired != 'Y' then
         open C_LOCK_ITEM_LOC;
         close C_LOCK_ITEM_LOC;
      else 
         forall i in INDICES of O_xprice_tbl
            update item_loc
               set unit_retail          = DECODE(L_event_type,'REG', NVL(O_xprice_tbl(i).new_unit_retail,unit_retail),
                                                              'CLRS',O_xprice_tbl(i).new_unit_retail,
                                                              'CLRE',O_xprice_tbl(i).new_unit_retail,
                                                                     unit_retail),
                   regular_unit_retail  = DECODE(L_event_type,'REG', NVL(O_xprice_tbl(i).new_unit_retail,regular_unit_retail),
                                                              'CLRS',O_xprice_tbl(i).new_unit_retail,
                                                              'CLRE',O_xprice_tbl(i).new_unit_retail,
                                                                     regular_unit_retail),
                   selling_unit_retail  = DECODE(L_event_type,'REG', NVL(O_xprice_tbl(i).selling_unit_retail,selling_unit_retail),
                                                              'CLRS',O_xprice_tbl(i).selling_unit_retail,
                                                              'CLRE',O_xprice_tbl(i).selling_unit_retail,
                                                                     selling_unit_retail),
                   selling_uom          = DECODE(L_event_type,'REG', NVL(O_xprice_tbl(i).selling_uom,selling_uom),
                                                              'CLRS',O_xprice_tbl(i).selling_uom,
                                                              'CLRE',O_xprice_tbl(i).selling_uom,
                                                                     selling_uom),
                   multi_units          = DECODE(L_event_type,'REG',NVL(O_xprice_tbl(i).multi_units,multi_units),
                                                                    multi_units),
                   multi_unit_retail    = DECODE(L_event_type,'REG',NVL(O_xprice_tbl(i).multi_unit_retail,multi_unit_retail),
                                                                    multi_unit_retail),
                   multi_selling_uom    = DECODE(L_event_type,'REG',NVL(O_xprice_tbl(i).multi_selling_uom,multi_selling_uom),
                                                                    multi_selling_uom),
                   clear_ind            = DECODE(L_event_type,'CLRS','Y',
                                                              'CLRE','N',
                                                              clear_ind),
                   promo_retail         = DECODE(L_event_type,'PROMS',O_xprice_tbl(i).promo_retail,
                                                              'PROME',NULL,
                                                              promo_retail),
                   promo_selling_retail = DECODE(L_event_type,'PROMS',O_xprice_tbl(i).promo_selling_retail,
                                                              'PROME',NULL,
                                                              promo_selling_retail),
                   promo_selling_uom    = DECODE(L_event_type,'PROMS',O_xprice_tbl(i).promo_selling_uom,
                                                              'PROME',NULL,
                                                              promo_selling_uom),
                   last_update_datetime = L_date,
                   last_update_id       = L_user
             where item       = O_xprice_tbl(i).item
               and loc        = O_xprice_tbl(i).location;
      end if;


   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ITEM_LOC;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_REPL_ITEM_LOC(O_error_message   IN OUT         VARCHAR2,
                              O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is 
   L_function              VARCHAR2(50) := 'CORESVC_XPRICE_SQL.UPDATE_REPL_ITEM_LOC';
   L_table                 VARCHAR2(30) := null;

   L_user                  VARCHAR2(20) := GET_USER;
   L_date                  DATE         := SYSDATE;
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_retry_lock_attempts   RMS_PLSQL_BATCH_CONFIG.RETRY_LOCK_ATTEMPTS%TYPE;
   L_retry_wait_time       RMS_PLSQL_BATCH_CONFIG.RETRY_WAIT_TIME%TYPE;
   L_locked_acquired       VARCHAR2(1) := 'Y';
   
   L_thread_val            SVC_PRICING_EVENT_TEMP.THREAD_VAL%TYPE;
   L_chunk_id              SVC_PRICING_EVENT_TEMP.CHUNK_ID%TYPE;
   L_event_type            SVC_PRICING_EVENT_TEMP.EVENT_TYPE%TYPE;
   
   cursor C_GET_LOCK_PARAM is
      select NVL(retry_lock_attempts,2),
             NVL(retry_wait_time,0)
        from rms_plsql_batch_config
       where program_name = 'CORESVC_XPRICE_SQL';
   
   cursor C_LOCK_REPL_ITEM_LOC is 
      select 'x'
        from repl_item_loc ril,
             svc_pricing_event_temp spet
       where spet.thread_val     = L_thread_val
         and spet.chunk_id       = L_chunk_id
         and spet.event_type     = L_event_type 
         and spet.process_status = CORESVC_XPRICE_SQL.STATUS_IN_PROGRESS
         and spet.item           = ril.item
         and spet.location       = ril.location
         for update of ril.item nowait;
BEGIN
   open C_GET_LOCK_PARAM;
   fetch C_GET_LOCK_PARAM into L_retry_lock_attempts,
                               L_retry_wait_time;
   close C_GET_LOCK_PARAM;
     
   L_table := 'REPL_ITEM_LOC';
   -- Retry the lock the maximum number of times defined in L_retry_lock_attempts
   L_thread_val   := O_xprice_tbl(1).thread_val;
   L_chunk_id     := O_xprice_tbl(1).chunk_id;   
   L_event_type   := O_xprice_tbl(1).event_type;   
   
   for i in 1..(L_retry_lock_attempts - 1) loop
      ---
      BEGIN
         open C_LOCK_REPL_ITEM_LOC;
         close C_LOCK_REPL_ITEM_LOC;
         L_locked_acquired := 'Y';
         exit;
         --
      EXCEPTION
         when RECORD_LOCKED then NULL;
      END;
      --
      dbms_lock.sleep(L_retry_wait_time);
   end loop;
   
   if L_locked_acquired != 'Y' then
      open C_LOCK_REPL_ITEM_LOC;
      close C_LOCK_REPL_ITEM_LOC;
   else 
      forall i in INDICES of O_xprice_tbl
         update repl_item_loc
            set deactivate_date = L_date
          where item = O_xprice_tbl(i).item
            and location = O_xprice_tbl(i).location;
   end if;


   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_REPL_ITEM_LOC;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_TRAN_DATA(O_error_message   IN OUT VARCHAR2,
                          O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is 
   L_function           VARCHAR2(50) := 'CORESVC_XPRICE_SQL.INSERT_TRAN_DATA';
   
   L_table              VARCHAR2(100) := NULL;
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   L_date               DATE          := get_vdate();
   L_mark_type          NUMBER        := NULL;
   L_mark_amt           NUMBER        := NULL;
   L_mark_type_2        NUMBER        := NULL;
   L_mark_amt_2         NUMBER        := NULL;
   L_total_amt          NUMBER(20,4)  := NULL;
   L_total_cost         NUMBER(20,4)  := NULL;
   
   L_average_weight     ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_nom_weight         ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   
   L_sup_data_rec       PRICING_SQL.SUP_DATA_REC;
   L_sup_data_count     NUMBER        := 0;
   L_sd_mark_type       NUMBER        := 10;
   
BEGIN
   -- Initialize sup_data collection
   L_sup_data_rec.depts             := DEPT_TBL();
   L_sup_data_rec.primary_suppliers := SUPPLIER_TBL();
   L_sup_data_rec.tran_types        := TRAN_TYPE_TBL();
   L_sup_data_rec.amounts           := UNIT_RETAIL_TBL();
   
   -- Begin Insert
   L_table := 'TRAN_DATA';

   if STKLEDGR_SQL.INIT_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;

   for i in O_xprice_tbl.FIRST..O_xprice_tbl.LAST loop
      if (O_xprice_tbl(i).new_unit_retail <> O_xprice_tbl(i).old_unit_retail and 
          O_xprice_tbl(i).item_soh > 0 and
          O_xprice_tbl(i).pack_ind = 'N') then
         if DETERMINE_MARK(O_xprice_tbl(i).orig_unit_retail,
                           O_xprice_tbl(i).old_unit_retail,
                           O_xprice_tbl(i).new_unit_retail,
                           --
                           L_mark_type,
                           L_mark_amt,
                           L_mark_type_2,
                           L_mark_amt_2,
                           O_error_message) = 'FALSE' then
            return FALSE;
         end if;
         
         if L_mark_type is NOT NULL and L_mark_type != 0 and L_mark_amt != 0 then
            if O_xprice_tbl(i).event_type = 'CLRS' then
               if L_mark_type = 13 then
                  L_mark_type := 16;
               end if;
               -- Build Records to be inserted into sup_data for Clearance Markdowns
               if O_xprice_tbl(i).primary_supp is not null then
                  L_sup_data_count := L_sup_data_count + 1;
                  L_sup_data_rec.depts.EXTEND();
                  L_sup_data_rec.primary_suppliers.EXTEND();
                  L_sup_data_rec.tran_types.EXTEND();
                  L_sup_data_rec.amounts.EXTEND();
                  L_sup_data_rec.depts(L_sup_data_count)             := O_xprice_tbl(i).dept;
                  L_sup_data_rec.primary_suppliers(L_sup_data_count) := O_xprice_tbl(i).primary_supp;
                  L_sup_data_rec.tran_types(L_sup_data_count)        := L_sd_mark_type;
                  L_sup_data_rec.amounts(L_sup_data_count)           := L_mark_amt;
                  
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_mark_amt,
                                          O_xprice_tbl(i).currency_code,
                                          NULL,
                                          L_sup_data_rec.amounts(L_sup_data_count),
                                          'R',
                                          NULL,
                                          NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;
         
            L_total_cost := NULL;
            if O_xprice_tbl(i).catch_weight_ind = 'Y' and O_xprice_tbl(i).standard_uom = 'EA' then
               if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                            L_average_weight,
                                                            O_xprice_tbl(i).item,
                                                            O_xprice_tbl(i).location,
                                                            O_xprice_tbl(i).loc_type) then
                  return FALSE;
               end if;

               if NOT ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                                   L_nom_weight,
                                                                   O_xprice_tbl(i).item) then
                  return FALSE;
               end if;

               if L_average_weight is NULL then
                  L_average_weight := L_nom_weight;
               end if;
               L_total_amt := ((L_mark_amt) * (O_xprice_tbl(i).item_soh) * (L_average_weight / L_nom_weight));
            else
               L_total_amt := ((L_mark_amt) * (O_xprice_tbl(i).item_soh));
            end if;
            
            if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                   O_xprice_tbl(i).item,
                                                   O_xprice_tbl(i).dept,
                                                   O_xprice_tbl(i).class,
                                                   O_xprice_tbl(i).subclass,
                                                   O_xprice_tbl(i).location,
                                                   O_xprice_tbl(i).loc_type,
                                                   L_date,
                                                   L_mark_type,
                                                   NULL,
                                                   O_xprice_tbl(i).item_soh,
                                                   L_total_cost,
                                                   L_total_amt,
                                                   O_xprice_tbl(i).event_id, -- Ref_No_1
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   O_xprice_tbl(i).old_unit_retail,
                                                   O_xprice_tbl(i).new_unit_retail,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_function,
                                                   --
                                                   NULL,
                                                   O_xprice_tbl(i).pack_ind,
                                                   O_xprice_tbl(i).sellable_ind,
                                                   O_xprice_tbl(i).orderable_ind,
                                                   O_xprice_tbl(i).pack_type) = FALSE then
               return FALSE;
            end if;
         end if;
         
         if L_mark_type_2 is NOT NULL and L_mark_type_2 != 0  and L_mark_amt_2 != 0 then
            if  O_xprice_tbl(i).event_type = 'CLRS' then
               if L_mark_type_2 = 13 then
                     L_mark_type_2 := 16;
               end if;
               -- Build Records to be inserted into sup_data for Clearance Markdowns
               if O_xprice_tbl(i).primary_supp is not null then
                  L_sup_data_count := L_sup_data_count + 1;
                  L_sup_data_rec.depts.EXTEND();
                  L_sup_data_rec.primary_suppliers.EXTEND();
                  L_sup_data_rec.tran_types.EXTEND();
                  L_sup_data_rec.amounts.EXTEND();
                  L_sup_data_rec.depts(L_sup_data_count)             := O_xprice_tbl(i).dept;
                  L_sup_data_rec.primary_suppliers(L_sup_data_count) := O_xprice_tbl(i).primary_supp;
                  L_sup_data_rec.tran_types(L_sup_data_count)        := L_sd_mark_type;
                  L_sup_data_rec.amounts(L_sup_data_count)           := L_mark_amt_2;
                  
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_mark_amt_2,
                                          O_xprice_tbl(i).currency_code,
                                          NULL,
                                          L_sup_data_rec.amounts(L_sup_data_count),
                                          'R',
                                          NULL,
                                          NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;

            L_total_cost := NULL;
            if O_xprice_tbl(i).catch_weight_ind = 'Y' and O_xprice_tbl(i).standard_uom = 'EA' then
               if NOT ITEMLOC_ATTRIB_SQL.GET_AVERAGE_WEIGHT(O_error_message,
                                                            L_average_weight,
                                                            O_xprice_tbl(i).item,
                                                            O_xprice_tbl(i).location,
                                                            O_xprice_tbl(i).loc_type) then
                  return FALSE;
               end if;

               if NOT ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                                   L_nom_weight,
                                                                   O_xprice_tbl(i).item) then
                  return FALSE;
               end if;

               if L_average_weight is NULL then
                  L_average_weight := L_nom_weight;
               end if;
               L_total_amt := ((L_mark_amt_2) * (O_xprice_tbl(i).item_soh) * (L_average_weight / L_nom_weight));
            else
               L_total_amt := ((L_mark_amt_2) * (O_xprice_tbl(i).item_soh));
            end if;
            
            if STKLEDGR_SQL.BUILD_TRAN_DATA_INSERT(O_error_message,
                                                   O_xprice_tbl(i).item,
                                                   O_xprice_tbl(i).dept,
                                                   O_xprice_tbl(i).class,
                                                   O_xprice_tbl(i).subclass,
                                                   O_xprice_tbl(i).location,
                                                   O_xprice_tbl(i).loc_type,
                                                   L_date,
                                                   L_mark_type_2,
                                                   NULL,
                                                   O_xprice_tbl(i).item_soh,
                                                   L_total_cost,
                                                   L_total_amt,
                                                   O_xprice_tbl(i).event_id,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   O_xprice_tbl(i).old_unit_retail,
                                                   O_xprice_tbl(i).new_unit_retail,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   L_function,
                                                   --
                                                   NULL,
                                                   O_xprice_tbl(i).pack_ind,
                                                   O_xprice_tbl(i).sellable_ind,
                                                   O_xprice_tbl(i).orderable_ind,
                                                   O_xprice_tbl(i).pack_type) = FALSE then
               return FALSE;
            end if;
         end if;--L_mark_type_2 is NOT NULL
         --
      end if;
   end loop;
   
   if STKLEDGR_SQL.FLUSH_TRAN_DATA_INSERT(O_error_message) = FALSE then
      return FALSE;
   end if;
   --
   L_table := 'SUP_DATA';
   
   forall i in 1..L_sup_data_count
      insert into sup_data (amount,
                            day_date,
                            dept,
                            supplier,
                            tran_type)
                   values  (L_sup_data_rec.amounts(i),
                            L_date,
                            L_sup_data_rec.depts(i),
                            L_sup_data_rec.primary_suppliers(i),
                            L_sup_data_rec.tran_types(i)
                            );
   --
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_TRAN_DATA;
-------------------------------------------------------------------------------------------------------
FUNCTION MARK_DUPLICATES(O_error_message   IN OUT VARCHAR2,
                         I_thread_val      IN     NUMBER,
                         I_chunk_id        IN     NUMBER)
return BOOLEAN is 
   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.MARK_DUPLICATES';

BEGIN
   update svc_pricing_event_temp spet
      set spet.process_status = CORESVC_XPRICE_SQL.STATUS_SKIPPED
    where exists(select 1
                   from (select rowid,
                                dense_rank() over (partition by item,location,event_similarity order by item_rank,hier_rank) as il_rank
                           from svc_pricing_event_temp spet
                          where thread_val = I_thread_val
                            and chunk_id   = I_chunk_id
                        ) inner
                  where inner.il_rank > 1
                    and inner.rowid  = spet.rowid
                );
                
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END MARK_DUPLICATES;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_PRICE_HIST(O_error_message   IN OUT VARCHAR2,
                           O_xprice_tbl      IN OUT NOCOPY  CORESVC_XPRICE_SQL.XPRICE_TEMP_TBL)
return BOOLEAN is 

   L_function             VARCHAR2(50) := 'CORESVC_XPRICE_SQL.INSERT_PRICE_HIST';
   L_date                 DATE := get_vdate();
  
BEGIN
   forall i in indices of O_xprice_tbl
      insert into price_hist (tran_type,
                              event,
                              item,
                              loc,
                              loc_type,
                              unit_retail,
                              selling_unit_retail,
                              selling_uom,
                              action_date,
                              multi_units,
                              multi_unit_retail,
                              multi_selling_uom,
                              post_date)
                      values (O_xprice_tbl(i).price_hist_tran_type,
                              O_xprice_tbl(i).event_id,
                              O_xprice_tbl(i).item,
                              O_xprice_tbl(i).location,
                              O_xprice_tbl(i).loc_type,
                              DECODE(O_xprice_tbl(i).event_type,'PROMS',O_xprice_tbl(i).promo_retail,
                                                                'PROME',O_xprice_tbl(i).old_unit_retail,
                                                                         O_xprice_tbl(i).new_unit_retail),
                              DECODE(O_xprice_tbl(i).event_type,'PROMS',O_xprice_tbl(i).promo_selling_retail,
                                                                'PROME',O_xprice_tbl(i).il_selling_unit_retail,
                                                                         O_xprice_tbl(i).selling_unit_retail),
                              DECODE(O_xprice_tbl(i).event_type,'PROMS',O_xprice_tbl(i).promo_selling_uom,
                                                                'PROME',O_xprice_tbl(i).il_selling_uom,
                                                                         O_xprice_tbl(i).selling_uom),
                              L_date,
                              O_xprice_tbl(i).multi_units,
                              O_xprice_tbl(i).multi_unit_retail,
                              O_xprice_tbl(i).multi_selling_uom,
                              L_date
                              );
   -- In case of emergency price changes, insert into the temp table which is 
   -- used by ordupd.pc to update orders
   if O_xprice_tbl(1).effective_date = L_date then
      forall i in indices of O_xprice_tbl
         insert into emer_price_hist (tran_type,
                                      item,
                                      loc,
                                      loc_type,
                                      unit_retail,
                                      action_date)
                              values (O_xprice_tbl(i).price_hist_tran_type,
                                      O_xprice_tbl(i).item,
                                      O_xprice_tbl(i).location,
                                      O_xprice_tbl(i).loc_type,
                                      DECODE(O_xprice_tbl(i).event_type,'PROMS',O_xprice_tbl(i).promo_retail,
                                                                        'PROME',O_xprice_tbl(i).old_unit_retail,
                                                                                 O_xprice_tbl(i).new_unit_retail),
                                      L_date);
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_PRICE_HIST;
-------------------------------------------------------------------------------------------------------
FUNCTION CHUNK_PRICE_EVENT(O_error_message   IN OUT   NOCOPY RTK_ERRORS.RTK_TEXT%TYPE)
return BOOLEAN IS

   L_program            VARCHAR2(64) := 'CORESVC_XPRICE_SQL.CHUNK_PRICE_EVENT';
   L_detail_count       NUMBER(10) := NULL;
   L_detail_chunk_count NUMBER(10) := NULL;
   L_max_chunk_size     RMS_PLSQL_BATCH_CONFIG.MAX_CHUNK_SIZE%TYPE := NULL;
   L_max_chunk          NUMBER(10) := NULL;
   L_loc_count          NUMBER;
   L_upd_loc_count      NUMBER;
   L_max_threads        NUMBER;
   L_rec_count          NUMBER;
   L_rec_chunk_count    NUMBER;
   L_thread_val         NUMBER;
   L_vdate              DATE := GET_VDATE + 1;
   

   cursor C_GET_MAX_CHUNK_SIZE is
      select max_chunk_size
        from rms_plsql_batch_config
       where program_name = 'CORESVC_XPRICE_SQL';

   cursor C_GET_MAX_THREADS is
      select max_concurrent_threads
        from rms_plsql_batch_config
       where program_name = 'CORESVC_XPRICE_SQL';

   cursor C_GET_DISTINCT_LOC is
      select distinct location
        from svc_pricing_event_temp
       where process_status     = 'N'
         and chunk_id IS NULL
         and effective_date = L_vdate;
         
   TYPE L_get_distinct_loc is TABLE OF C_GET_DISTINCT_LOC%ROWTYPE;
   L_loc             L_get_distinct_loc;

BEGIN

   open C_GET_MAX_CHUNK_SIZE;
   fetch C_GET_MAX_CHUNK_SIZE into L_max_chunk_size;
   close C_GET_MAX_CHUNK_SIZE;
   -------------------------------------------------------------------------------------
   -- Get the maximum threads of a pricing event transaction
   -------------------------------------------------------------------------------------
   open C_GET_MAX_THREADS;
   fetch C_GET_MAX_THREADS into L_max_threads;
   close C_GET_MAX_THREADS;

   -- Check if L_max_threads was retrieved
   if L_max_threads is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_THREADS_DEFINED',
                                            'CORESVC_XPRICE_SQL',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -------------------------------------------------------------------------------------
   -- Get the total number of distinct location
   -------------------------------------------------------------------------------------

   select count(distinct location) 
     into L_loc_count
     from svc_pricing_event_temp
    where process_status     = 'N'
      and thread_val IS NULL
      and location IS NOT NULL
      and effective_date = L_vdate;

   -- Check if no records were found
   if L_loc_count is NULL or L_loc_count = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS_FOUND',
                                            L_vdate,
                                            NULL,
                                            NULL);
      return FALSE;
    end if;

   -------------------------------------------------------------------------------------
   -- Assign the thread values to the location
   -------------------------------------------------------------------------------------
   L_thread_val := 1;
   open C_GET_DISTINCT_LOC;
   fetch C_GET_DISTINCT_LOC bulk collect into L_loc;
   close C_GET_DISTINCT_LOC;

   for i in 1..L_loc.COUNT LOOP
      if L_thread_val <= L_max_threads
      then
         update svc_pricing_event_temp
            set thread_val = L_thread_val
          where location = L_loc(i).location
            and effective_date = L_vdate
            and thread_val IS NULL
            and process_status = 'N';
         L_thread_val := L_thread_val + 1;
      else
         L_thread_val := 1;
         update svc_pricing_event_temp
            set thread_val = L_thread_val
          where location = L_loc(i).location
            and effective_date = L_vdate
            and thread_val IS NULL
            and process_status = 'N';
         L_thread_val := L_thread_val + 1;
      end if;
   END LOOP;

   -------------------------------------------------------------------------------------
   -- Get the number of location updated
   -------------------------------------------------------------------------------------
   
   -- Get the total number of distinct location after update
   select count(distinct location) 
     into L_upd_loc_count
     from svc_pricing_event_temp
    where process_status     = 'N'
      and thread_val IS NOT NULL
      and effective_date = L_vdate;

   -- Check for records that did not get threaded
   -- Return fatal if records are found
   if L_loc_count <> NVL(L_upd_loc_count, 0) then
      O_error_message := SQL_LIB.CREATE_MSG('PRCEVNT_THREAD_ERROR',
                                            '',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -------------------------------------------------------------------------------------
   -- Get the total number of records in svc_pricing_event_temp for status 'N'
   -------------------------------------------------------------------------------------
   select count(*) 
     into L_rec_count
     from svc_pricing_event_temp
    where process_status     = 'N'
      and chunk_id IS NULL
      and effective_date = L_vdate;

   -- Chunk the records on svc_pricing_event_temp
   merge into svc_pricing_event_temp target
   using (select distinct
                 location,
                 item,
                 CEIL(inner_set.rank/L_max_chunk_size) chunk_id
            from (select location,
                         item,
                         DENSE_RANK()
                            OVER(ORDER by item) rank
                    from svc_pricing_event_temp
                   where process_status     = 'N'
                     and effective_date     = L_vdate
                     and chunk_id IS NULL) inner_set
         ) source
      on (    target.location = source.location
          and target.item = source.item)
   when matched then
      update set target.chunk_id = source.chunk_id
   where process_status     = 'N'
     and effective_date     = L_vdate
     and thread_val is not null
     and chunk_id IS NULL;

   -- Get the number of rows in svc_pricing_event_temp after chunking
   select count(*) 
     into L_rec_chunk_count
     from svc_pricing_event_temp
    where process_status     = 'N'
      and chunk_id is NOT NULL
      and effective_date = L_vdate;

   -- Check for records that did not get chunked
   -- Return fatal if records are found
   if L_rec_count <> L_rec_chunk_count then
      O_error_message := SQL_LIB.CREATE_MSG('PRCEVNT_CHUNK_ERROR',
                                            '',
                                            NULL,
                                            NULL);
      -- return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHUNK_PRICE_EVENT;
-------------------------------------------------------------------------------------------------------
END CORESVC_XPRICE_SQL;
/