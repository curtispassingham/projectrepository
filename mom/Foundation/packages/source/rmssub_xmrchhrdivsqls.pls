
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRDIV_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION PERSIST_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE,
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION PERSIST_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRDIV_SQL;
/
