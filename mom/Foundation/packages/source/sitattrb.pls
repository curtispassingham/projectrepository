
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SIT_ATTRIB_SQL AS
----------------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message      IN OUT VARCHAR2,
                         O_itemloc_link_desc  IN OUT SIT_HEAD.ITEMLOC_LINK_DESC%TYPE,
                         O_itemlist           IN OUT SIT_HEAD.SKULIST%TYPE,
                         O_loc_list           IN OUT SIT_HEAD.LOC_LIST%TYPE,
                         I_itemloc_link_id    IN     SIT_HEAD.ITEMLOC_LINK_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_HEADER is
      select itemloc_link_desc,
             skulist,
             loc_list
        from sit_head
       where itemloc_link_id = I_itemloc_link_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_HEADER','SIT_HEAD',I_itemloc_link_id);
   open C_GET_HEADER;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_HEADER','SIT_HEAD',I_itemloc_link_id);
   fetch C_GET_HEADER into O_itemloc_link_desc, 
                           O_itemlist, 
                           O_loc_list;
   ---
   if C_GET_HEADER%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_ITEMLOC_LINK_ID',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_GET_HEADER','SIT_HEAD',I_itemloc_link_id);
      close C_GET_HEADER;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_HEADER','SIT_HEAD',I_itemloc_link_id);
   close C_GET_HEADER;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SIT_ATTRIB_SQL.GET_HEADER_INFO',
                                             to_char(SQLCODE));
   return FALSE;
END GET_HEADER_INFO;      
---------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_ITEMLOC_LINK_ID(O_error_message    IN OUT VARCHAR2,
                                  O_itemloc_link_id  IN OUT SIT_HEAD.ITEMLOC_LINK_ID%TYPE)
   RETURN BOOLEAN IS
   L_itemloc_link_sequence	SIT_HEAD.ITEMLOC_LINK_ID%TYPE;
   L_wrap_sequence_no         SIT_HEAD.ITEMLOC_LINK_ID%TYPE;
   L_first_time               VARCHAR2(3) := 'Y';
   L_dummy                    VARCHAR2(1);
   
   cursor C_SITHEAD_EXISTS is
      select 'x'
        from sit_head
       where itemloc_link_id = O_itemloc_link_id;

BEGIN
   LOOP
      select itemloc_link_sequence.NEXTVAL
        into L_itemloc_link_sequence
        from sys.dual;
      ---
      if L_first_time = 'Y' then
         L_wrap_sequence_no := L_itemloc_link_sequence;
         L_first_time := 'N';
      elsif L_itemloc_link_sequence = L_wrap_sequence_no then
         O_error_message := SQL_LIB.CREATE_MSG('NO_ITEMLOC_LINK_ID',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      O_itemloc_link_id := L_itemloc_link_sequence;
      ---
      SQL_LIB.SET_MARK('OPEN','C_SITHEAD_EXISTS','SIT_HEAD',to_char(O_itemloc_link_id));
      open C_SITHEAD_EXISTS;
      ---
      SQL_LIB.SET_MARK('FETCH','C_SITHEAD_EXISTS','SIT_HEAD',to_char(O_itemloc_link_id));
      fetch C_SITHEAD_EXISTS into L_dummy;
      ---
      if C_SITHEAD_EXISTS%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_SITHEAD_EXISTS','SIT_HEAD',to_char(O_itemloc_link_id));
         close C_SITHEAD_EXISTS;
         exit;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_SITHEAD_EXISTS','SIT_HEAD',to_char(O_itemloc_link_id));
      close C_SITHEAD_EXISTS;
      ---
   END LOOP;
   ---
   return TRUE;      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SIT_ATTRIB_SQL.GET_NEXT_ITEMLOC_LINK_ID',
                                            to_char(SQLCODE));
   return FALSE;
END GET_NEXT_ITEMLOC_LINK_ID;    
--------------------------------------------------------------------------------------------
FUNCTION LINK_EXISTS(O_error_message         IN OUT VARCHAR2,
                     O_exists                IN OUT BOOLEAN,
                     I_itemloc_link_id       IN     SIT_HEAD.ITEMLOC_LINK_ID%TYPE)
   RETURN BOOLEAN IS

   L_dummy      VARCHAR2(1);

   cursor C_EXISTS is 
      select 'x'
        from sit_head
       where itemloc_link_id = I_itemloc_link_id;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_EXISTS','SIT_HEAD',I_itemloc_link_id);
   open C_EXISTS;
   ---
   SQL_LIB.SET_MARK('FECTH','C_EXISTS','SIT_HEAD',I_itemloc_link_id);
   fetch C_EXISTS into L_dummy;
   ---
   if C_EXISTS%NOTFOUND then 
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','SIT_HEAD',I_itemloc_link_id);
   close C_EXISTS;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'SIT_ATTRIB_SQL.LINK_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END LINK_EXISTS;
--------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DATE(O_error_message    IN OUT VARCHAR2,
                       O_exists           IN OUT BOOLEAN,
                       I_itemloc_link_id  IN     SIT_DETAIL.ITEMLOC_LINK_ID%TYPE,
                       I_status_date      IN     SIT_DETAIL.STATUS_UPDATE_DATE%TYPE)
   RETURN BOOLEAN is

   L_program           VARCHAR2(50) := 'SIT_ATTRIB_SQL.VALIDATE_DATE';
   L_dummy             VARCHAR2(1);

   cursor C_CHECK_DATE is
   select 'x'
     from sit_detail
    where itemloc_link_id = I_itemloc_link_id
      and status_update_date = I_status_date;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_CHECK_DATE','SIT_DETAIL','ITEMLOC_LINK_ID: '||I_itemloc_link_id);
   open C_CHECK_DATE;
   ---
   SQL_LIB.SET_MARK('FETCH','C_CHECK_DATE','SIT_DETAIL','ITEMLOC_LINK_ID: '||I_itemloc_link_id);
   FETCH C_CHECK_DATE into L_dummy;
   ---
   if C_CHECK_DATE%FOUND then
      O_exists := TRUE;
   else 
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_DATE','SIT_DETAIL','ITEMLOC_LINK_ID: '||I_itemloc_link_id);
   close C_CHECK_DATE;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END VALIDATE_DATE;
------------------------------------------------------------------------------------------------
END SIT_ATTRIB_SQL;
/
