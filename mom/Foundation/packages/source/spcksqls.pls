CREATE OR REPLACE PACKAGE SIMPLE_PACK_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------
-- Function Name: BUILD_PACK
-- Purpose: This function is called by the Simple Pack Setup form and is used to build the
-- simple packs defined in the form. These packs are stored on the SIMPLE_PACK_TEMP table
-- and this function will read this table and based on the passed in copy indicators,
-- copy the appropriate component item information to the packs being built.
----------------------------------------------------------------------------------------
FUNCTION BUILD_PACK(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists              IN OUT   BOOLEAN,
                    O_exists_item         IN OUT   ITEM_MASTER.ITEM%TYPE,
                    I_item                IN       ITEM_MASTER.ITEM%TYPE,
                    I_supplier            IN       VARCHAR2,
                    I_wh                  IN       VARCHAR2,
                    I_store               IN       VARCHAR2,
                    I_expenses            IN       VARCHAR2,
                    I_hts                 IN       VARCHAR2,
                    I_uda                 IN       VARCHAR2,
                    I_ticket              IN       VARCHAR2,
                    I_seasons             IN       VARCHAR2,
                    I_docs                IN       VARCHAR2,
                    I_vat                 IN       VARCHAR2,
                    I_up_chrgs            IN       VARCHAR2,
                    I_curr_prim           IN       CURRENCIES.CURRENCY_CODE%TYPE,
                    I_item_xform_ind      IN       ITEM_MASTER.ITEM_XFORM_IND%TYPE,
                    I_inventory_ind       IN       ITEM_MASTER.INVENTORY_IND%TYPE,
                    I_internal_finisher   IN       VARCHAR2,
                    I_external_finisher   IN       VARCHAR2,
                    I_edi_ind             IN       VARCHAR2 DEFAULT 'N'
                    )
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: GET_RECORD_INFO
-- Purpose: This function is called by the Simple Pack Setup form and is used to retrieve
-- non base table field information for the Detail block. I_action determines if the
-- function is being called for a post-query or for another reason. The function will
-- retrieve different information depending on the action.
----------------------------------------------------------------------------------------
FUNCTION GET_RECORD_INFO(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_item_no_type_decode     IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_case_tare_type_decode   IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_pallet_tare_type_decode IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_supp_desc               IN OUT SUPS.SUP_NAME%TYPE,
                         O_origin_country_desc     IN OUT COUNTRY.COUNTRY_DESC%TYPE,
                         O_manu_country_desc       IN OUT COUNTRY.COUNTRY_DESC%TYPE,
                         O_curr_supp               IN OUT SUPS.CURRENCY_CODE%TYPE,
                         O_cost_prim               IN OUT SIMPLE_PACK_TEMP.UNIT_COST%TYPE,
                         O_retail_supp             IN OUT SIMPLE_PACK_TEMP.UNIT_RETAIL%TYPE,
                         O_markup                  IN OUT DEPS.BUD_MKUP%TYPE,
                         O_order_type_decode       IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_sale_type_decode        IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_tolerance_type_decode   IN OUT CODE_DETAIL.CODE_DESC%TYPE,
                         O_cost_UOM_unit_cost_prim IN OUT SIMPLE_PACK_TEMP.UNIT_COST%TYPE,
                         O_cost_UOM_unit_cost_supp IN OUT SIMPLE_PACK_TEMP.UNIT_COST%TYPE,
                         I_action                  IN     VARCHAR2,
                         I_item_no_type            IN     SIMPLE_PACK_TEMP.ITEM_NUMBER_TYPE%TYPE,
                         I_case_tare_type          IN     SIMPLE_PACK_TEMP.CASE_TARE_TYPE%TYPE,
                         I_pallet_tare_type        IN     SIMPLE_PACK_TEMP.PALLET_TARE_TYPE%TYPE,
                         I_supplier                IN     SIMPLE_PACK_TEMP.PRIMARY_SUPP%TYPE,
                         I_origin_country_id       IN     SIMPLE_PACK_TEMP.PRIMARY_CNTRY_ID%TYPE,
                         I_manu_country_id         IN     SIMPLE_PACK_TEMP.PRIMARY_MANU_CTRY_ID%TYPE,
                         I_cost_supp               IN     SIMPLE_PACK_TEMP.UNIT_COST%TYPE,
                         I_retail_prim             IN     SIMPLE_PACK_TEMP.UNIT_RETAIL%TYPE,
                         I_curr_prim               IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                         I_curr_supp               IN     SUPS.CURRENCY_CODE%TYPE,
                         I_markup_type             IN     DEPS.MARKUP_CALC_TYPE%TYPE,
                         I_dept                    IN     DEPS.DEPT%TYPE,
                         I_order_type              IN     SIMPLE_PACK_TEMP.ORDER_TYPE%TYPE,
                         I_sale_type               IN     SIMPLE_PACK_TEMP.SALE_TYPE%TYPE,
                         I_tolerance_type          IN     SIMPLE_PACK_TEMP.TOLERANCE_TYPE%TYPE,
                         I_case_weight_uom         IN     SIMPLE_PACK_TEMP.CASE_WEIGHT_UOM%TYPE,
                         I_case_weight             IN     SIMPLE_PACK_TEMP.CASE_WEIGHT%TYPE,
                         I_case_length_uom         IN     SIMPLE_PACK_TEMP.CASE_LWH_UOM%TYPE,
                         I_case_length             IN     SIMPLE_PACK_TEMP.CASE_LENGTH%TYPE,
                         I_case_liquid_volume_uom  IN     SIMPLE_PACK_TEMP.CASE_LIQUID_VOLUME_UOM%TYPE,
                         I_case_liquid_volume      IN     SIMPLE_PACK_TEMP.CASE_LIQUID_VOLUME%TYPE,
                         I_cost_uom                IN     UOM_CLASS.UOM%TYPE,
                         I_std_uom                 IN     UOM_CLASS.UOM%TYPE,
                         I_total_qty               IN     SIMPLE_PACK_TEMP.SUPP_PACK_SIZE%TYPE,
                         I_item                    IN     ITEM_SUPP_COUNTRY.ITEM%TYPE
                        )
RETURN BOOLEAN;

----------------------------------------------------------------------------------------
-- Function Name: CALCULATE_UNIT_COST
-- Thie function will carry out all neccessary conversions between UOMs and currencies to produce
-- a comprehensive set of unit costs.
----------------------------------------------------------------------------------------
FUNCTION CALCULATE_UNIT_COST ( O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE                     ,
                               O_cost_UOM_unit_cost_prim       IN OUT   SIMPLE_PACK_TEMP.UNIT_COST%TYPE              ,
                               O_standard_UOM_unit_cost_prim   IN OUT   SIMPLE_PACK_TEMP.UNIT_COST%TYPE              ,
                               IO_cost_UOM_unit_cost           IN OUT   SIMPLE_PACK_TEMP.UNIT_COST%TYPE              ,
                               IO_standard_UOM_unit_cost       IN OUT   SIMPLE_PACK_TEMP.UNIT_COST%TYPE              ,
                               I_cost_uom                      IN       UOM_CLASS.UOM%TYPE                           ,
                               I_std_uom                       IN       UOM_CLASS.UOM%TYPE                           ,
                               I_case_weight_uom               IN       SIMPLE_PACK_TEMP.CASE_WEIGHT_UOM%TYPE        ,
                               I_case_weight                   IN       SIMPLE_PACK_TEMP.CASE_WEIGHT%TYPE            ,
                               I_case_length_uom               IN       SIMPLE_PACK_TEMP.CASE_LWH_UOM%TYPE           ,
                               I_case_length                   IN       SIMPLE_PACK_TEMP.CASE_LENGTH%TYPE            ,
                               I_case_liquid_volume_uom        IN       SIMPLE_PACK_TEMP.CASE_LIQUID_VOLUME_UOM%TYPE ,
                               I_case_liquid_volume            IN       SIMPLE_PACK_TEMP.CASE_LIQUID_VOLUME%TYPE     ,
                               I_total_qty                     IN       SIMPLE_PACK_TEMP.SUPP_PACK_SIZE%TYPE         ,
                               I_primary_currency              IN       CURRENCIES.CURRENCY_CODE%TYPE                ,
                               I_supplier_currency             IN       CURRENCIES.CURRENCY_CODE%TYPE                ,
                               I_item                          IN       ITEM_SUPP_COUNTRY.ITEM%TYPE                  ,
                               I_supplier                      IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE              ,
                               I_origin_country_id             IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE
                             )
RETURN BOOLEAN;


----------------------------------------------------------------------------------------
-- Function Name: PACK_NUMBER_EXISTS
-- Purpose: This function is used to determine if the passed in item number exists on the
-- SIMPLE_PACK_TEMP table.
----------------------------------------------------------------------------------------
FUNCTION PACK_NUMBER_EXISTS(O_error_message IN OUT VARCHAR2,
                            O_exists        IN OUT BOOLEAN,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: RETRIEVE_SIMPLE_PACKS
-- Purpose: This function is called by the Simple Pack Setup form and is used to populate the
-- SIMPLE_PACK_TEMP table with the simple packs associated to the passed in item. The form is
-- based on this table.
----------------------------------------------------------------------------------------
FUNCTION RETRIEVE_SIMPLE_PACKS(O_error_message IN OUT VARCHAR2,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: UNBUILT_PACKS_EXIST
-- Purpose: This function is called by the Simple Pack Setup form and is used to determine
-- if any packs exist on the SIMPLE_PACK_TEMP table that have not yet been built.
----------------------------------------------------------------------------------------
FUNCTION UNBUILT_PACKS_EXIST(O_error_message IN OUT VARCHAR2,
                             O_exists        IN OUT BOOLEAN,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: GET_COPY_INFO
-- Purpose: This function is called by the Simple Pack Setup form and is used to determine
-- which attributes can be copied from the component item to the simple packs. Essentially,
-- the function determines if certain information exists for the component item and sets
-- an output parameter for each piece of information appropriately.
----------------------------------------------------------------------------------------
FUNCTION GET_COPY_INFO(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_default_tax_type     IN OUT   SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE,
                       O_elc_ind              IN OUT   SYSTEM_OPTIONS.ELC_IND%TYPE,
                       O_import_ind           IN OUT   SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                       O_whs_exist            IN OUT   BOOLEAN,
                       O_stores_exist         IN OUT   BOOLEAN,
                       O_expenses_exist       IN OUT   BOOLEAN,
                       O_hts_exist            IN OUT   BOOLEAN,
                       O_tax_exist            IN OUT   BOOLEAN,
                       O_udas_exist           IN OUT   BOOLEAN,
                       O_seasons_exist        IN OUT   BOOLEAN,
                       O_docs_exist           IN OUT   BOOLEAN,
                       O_up_chrgs_exist       IN OUT   BOOLEAN,
                       O_tickets_exist        IN OUT   BOOLEAN,
                       O_int_finisher_exist   IN OUT   BOOLEAN,
                       O_ext_finisher_exist   IN OUT   BOOLEAN,
                       I_item                 IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DELETE_SIMPLE_PACK_TEMP
-- Purpose: This function deletes records from the SIMPLE_PACK_TEMP table.
----------------------------------------------------------------------------------------
FUNCTION DELETE_SIMPLE_PACK_TEMP(O_error_message IN OUT VARCHAR2,
                                 I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function: ITEM_IN_SIMPLE_PACK
---  Purpose: Returns TRUE in O_exist if the given item exists in a simple pack.
----------------------------------------------------------------------------------------
FUNCTION ITEM_IN_SIMPLE_PACK(O_error_message   IN OUT VARCHAR2,
                             O_exist           IN OUT BOOLEAN,
                             I_item            IN     ITEM_MASTER.ITEM%TYPE,
                             I_simple_pack     IN     PACKITEM.PACK_NO%TYPE)

   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--- Function: CHECK_DUPLICATE_QTY
--- Purpose: This function would return TRUE if a record exists in the simple_pack_temp
--- table having the same component item and qty as the values passed to the function.
----------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE_QTY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_item            IN       PACKITEM.ITEM%TYPE,
                             I_qty             IN       PACKITEM.PACK_QTY%TYPE,
                             I_pack_no         IN       PACKITEM.PACK_NO%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CHECK_FS_COSTING_LOC_WH
-- Purpose: This function is called by the Simple Pack Setup form and is used to check whether
-- any of franchise store ranged to the item have warehouse location as costing location.
-- It will return TRUE if Costing location of franchise store ranged for the Item is Warehouse
-- else it returns FALSE.
----------------------------------------------------------------------------------------
FUNCTION CHECK_FS_COSTING_LOC_WH(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item                IN       PACKITEM.ITEM%TYPE)
  RETURN BOOLEAN;
----------------------------------------------------------------------------------------
END;
/
