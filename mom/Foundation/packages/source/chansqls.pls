
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE CHANNEL_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
--Function Name:   GET_CHANNEL_NAME
--Purpose      :   Retrieves the name of the channel from the channels table..
--------------------------------------------------------------------------------
FUNCTION GET_CHANNEL_NAME    (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_channel_name          IN OUT CHANNELS.CHANNEL_NAME%TYPE,
                              I_channel_id            IN     CHANNELS.CHANNEL_ID%TYPE)         
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   CHANNEL_ID_EXIST
--Purpose      :   This function checks if the channel id passed in exists on
--                 the channel table.                 
--------------------------------------------------------------------------------
FUNCTION CHANNEL_ID_EXIST     (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist                 IN OUT BOOLEAN,
                               I_channel_id            IN     CHANNELS.CHANNEL_ID%TYPE)      
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   GET_CHANNEL_WH_NAME
--Purpose      :   This function will retrieve the warehouse name for a warehouse and 
--                 check if it is within an entered channel. 
--------------------------------------------------------------------------------
FUNCTION GET_CHANNEL_WH_NAME  (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_WH_name               IN OUT WH.WH_NAME%TYPE,
                               I_WH                    IN     WH.WH%TYPE,     
                               I_channel_id            IN     CHANNELS.CHANNEL_ID%TYPE)         
   RETURN BOOLEAN;

--------------------------------------------------------------------------------
--Function Name:   DELETE_CHANNEL
--Purpose      :   This function will check to see if the selected channel is attached to any 
--                 stores or warehouses.  If the channel is attached to a location then O_exist will 
--                 set to true. If the channel is not attached to any location then the O_exist will be
--                 set to false and the channel will be deleted. 
--------------------------------------------------------------------------------
FUNCTION DELETE_CHANNEL       (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exist                 IN OUT BOOLEAN,
                               I_channel_id            IN     CHANNELS.CHANNEL_ID%TYPE)         
   RETURN BOOLEAN;


END CHANNEL_SQL;
/
