
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XDIFFGRP_VALIDATE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This overloaded private function will check all required fields for the create and
   --                modify messages to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XDiffGrpDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the delete message
   --                to ensure that diff_group_id is not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XDiffGrpRef_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_EXISTENCE
   -- Purpose      : This private function should check the existence of diff_group_head or
   --                diff_group_detail records based on the message type.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_XDiffGrpDesc_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_EXISTENCE
   -- Purpose      : This private function should check the existence of diff_group_head or
   --                diff_group_detail records based on the message type.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_XDiffGrpRef_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DELETE
   -- Purpose      : This private function checks if the diff_group_head and/or diff_group_detail is
   --                ok to be deleted.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XDiffGrpRef_REC",
                      I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will populate all the fields in the DIFF_GROUP_SQL.DIFF_GROUP_REC with
   --                the values from the create and modify RIB messages.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diffgrp_rec     OUT    NOCOPY   DIFF_GROUP_SQL.DIFF_GROUP_REC,
                         I_message         IN              "RIB_XDiffGrpDesc_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will populate all the fields in the DIFF_GROUP_SQL.DIFF_GROUP_REC with
   --                the values from the delete RIB message.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diffgrp_rec     OUT    NOCOPY   DIFF_GROUP_SQL.DIFF_GROUP_REC,
                         I_message         IN              "RIB_XDiffGrpRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diffgrp_rec     OUT    NOCOPY   DIFF_GROUP_SQL.DIFF_GROUP_REC,
                       I_message         IN              "RIB_XDiffGrpDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XDIFFGRP_VALIDATE.CHECK_MESSAGE';
   L_exists       BOOLEAN       := FALSE;

BEGIN
   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;

   if not CHECK_EXISTENCE(O_error_message,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_diffgrp_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diffgrp_rec     OUT    NOCOPY   DIFF_GROUP_SQL.DIFF_GROUP_REC,
                       I_message         IN              "RIB_XDiffGrpRef_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_VALIDATE.CHECK_MESSAGE';
   L_exists       BOOLEAN      := FALSE;

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;

   if not CHECK_EXISTENCE(O_error_message,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   if not CHECK_DELETE(O_error_message,
                       I_message,
                       I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_diffgrp_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XDiffGrpDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_VALIDATE.CHECK_REQUIRED_FIELDS';
   L_exists       BOOLEAN      := FALSE;
   L_count        NUMBER       := 0;

BEGIN

   if I_message.diff_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Diff Grp ID');
      return FALSE;
   end if;

   if I_message_type in (RMSSUB_XDIFFGRP.LP_cre_type, RMSSUB_XDIFFGRP.LP_mod_type) then
      if I_message.diff_group_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Diff Grp Type');
         return FALSE;
      end if;

      if I_message.diff_group_desc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Diff Grp Desc');
         return FALSE;
      end if;
   end if;

   if I_message_type not in (RMSSUB_XDIFFGRP.LP_mod_type) then
      if I_message.xdiffgrpdtl_tbl is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC');
         return FALSE;
      end if;

      FOR i in I_message.xdiffgrpdtl_tbl.first..I_message.xdiffgrpdtl_tbl.last LOOP
         -- check if detail table has at least 1 record and check if diff_id is not null.
         if I_message.xdiffgrpdtl_tbl(i).diff_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Diff id');
            return FALSE;
         end if;

         -- check if the diff_id of detail record exists in diff_ids table.
         if not DIFF_ID_SQL.DIFF_ID_EXISTS(O_error_message,
                                           L_exists,
                                           I_message.xdiffgrpdtl_tbl(i).diff_id) then
            return FALSE;
         end if;

         if not L_exists then
            O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID');
            return FALSE;
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XDiffGrpRef_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XDIFFGRP_VALIDATE.CHECK_REQUIRED_FIELDS';
   L_exists         BOOLEAN      := FALSE;
   L_count          NUMBER       := 0;
BEGIN

   if I_message.diff_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Diff Grp ID');
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XDIFFGRP.LP_dtl_del_type then
      if I_message.xdiffgrpdtlref_tbl is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC');
         return FALSE;
      end if;

      FOR i in I_message.xdiffgrpdtlref_tbl.first..I_message.xdiffgrpdtlref_tbl.last LOOP
         if I_message.xdiffgrpdtlref_tbl(i).diff_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Diff id');
            return FALSE;
         end if;

         -- check if the diff_id of detail record exists in diff_ids table.
         if not DIFF_ID_SQL.DIFF_ID_EXISTS(O_error_message,
                                           L_exists,
                                           I_message.xdiffgrpdtlref_tbl(i).diff_id) then
            return FALSE;
         end if;

         if not L_exists then
            O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID');
            return FALSE;
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_XDiffGrpDesc_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XDIFFGRP_VALIDATE.CHECK_EXISTENCE';
   L_exist           BOOLEAN      := FALSE;
   L_diff_type       DIFF_IDS.DIFF_TYPE%TYPE;
   L_diff_grp_type   DIFF_GROUP_HEAD.DIFF_TYPE%TYPE;

BEGIN
   -- Check for DIFF GROUP
   if not DIFF_GROUP_SQL.EXIST(O_error_message,
                               L_exist,
                               I_message.diff_group_id) then
      return FALSE;
   end if;

   if L_exist and I_message_type = RMSSUB_XDIFFGRP.LP_cre_type then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE_EXIST', 'Diff Group ID');
      return FALSE;
   end if;

   -- check if diff_group id already exists in diff_group_head table.
   L_exist := FALSE;    -- Reset exist flag
   if not DIFF_GROUP_SQL.DIFF_GROUP_EXISTS(O_error_message,
                                           L_exist,
                                           I_message.diff_group_id) then
      return FALSE;
   end if;

   if not L_exist and I_message_type != RMSSUB_XDIFFGRP.LP_cre_type then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Diff Group', I_message.diff_group_id);
      return FALSE;
   end if;

   -- Retrieve the diff type of the diff_group_id
   if I_message_type = RMSSUB_XDIFFGRP.LP_dtl_cre_type then
      if NOT DIFF_GROUP_SQL.GET_DIFF_GROUP_TYPE(O_error_message,
                                                L_diff_grp_type,
                                                I_message.diff_group_id) then
         return FALSE;
      end if;
   end if;

   -- Check for Detail
   if I_message_type != RMSSUB_XDIFFGRP.LP_mod_type then
      FOR i in I_message.xdiffgrpdtl_tbl.first..I_message.xdiffgrpdtl_tbl.last LOOP
         -- check if diff_group_id/diff_id already exists.
         L_exist := FALSE;    -- Reset exist flag
         if not DIFF_GROUP_SQL.DUP_DIFF_ID(O_error_message,
                                           L_exist,
                                           I_message.xdiffgrpdtl_tbl(i).diff_id,
                                           I_message.diff_group_id) then
            return FALSE;
         end if;
         ---
         if L_exist and I_message_type in (RMSSUB_XDIFFGRP.LP_cre_type, RMSSUB_XDIFFGRP.LP_dtl_cre_type) then
            O_error_message := SQL_LIB.CREATE_MSG('INV_MESSAGE_EXIST', 'Diff id');
            return FALSE;
         elsif not L_exist and I_message_type = RMSSUB_XDIFFGRP.LP_dtl_mod_type then
            O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Diff id', I_message.xdiffgrpdtl_tbl(i).diff_id);
            return FALSE;
         end if;

         -- Retrieve the diff type of diff ids in the detail node
         L_diff_type := NULL;
         if I_message_type in (RMSSUB_XDIFFGRP.LP_cre_type, RMSSUB_XDIFFGRP.LP_dtl_cre_type) then
            if NOT DIFF_GROUP_SQL.GET_DIFF_TYPE(O_error_message,
                                                L_diff_type,
                                                I_message.xdiffgrpdtl_tbl(i).diff_id) then
               return FALSE;
            end if;

            -- Check if diff_type for diff ids in the detail node matches the diff_group_id's diff type
            if I_message_type = RMSSUB_XDIFFGRP.LP_cre_type then
               if L_diff_type != I_message.diff_group_type then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Diff id', I_message.xdiffgrpdtl_tbl(i).diff_id);
                  return FALSE;
               end if;
            else
               if L_diff_type != L_diff_grp_type then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Diff id', I_message.xdiffgrpdtl_tbl(i).diff_id);
                  return FALSE;
               end if;
            end if;
            ---
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_EXISTENCE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EXISTENCE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_message         IN       "RIB_XDiffGrpRef_REC",
                         I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_VALIDATE.CHECK_EXISTENCE';
   L_exist        BOOLEAN      := FALSE;

BEGIN
   -- Check for DIFF GROUP
   if not DIFF_GROUP_SQL.DIFF_GROUP_EXISTS(O_error_message,
                                           L_exist,
                                           I_message.diff_group_id) then
      return FALSE;
   end if;

   if not L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Diff Group', I_message.diff_group_id);
      return FALSE;
   end if;

   -- Check for Detail
   if I_message_type = RMSSUB_XDIFFGRP.LP_dtl_del_type then
      FOR i in I_message.xdiffgrpdtlref_tbl.first..I_message.xdiffgrpdtlref_tbl.last LOOP
         L_exist := FALSE;    -- Reset exist flag
         ---
         if not  DIFF_GROUP_SQL.DUP_DIFF_ID(O_error_message,
                                            L_exist,
                                            I_message.xdiffgrpdtlref_tbl(i).diff_id,
                                            I_message.diff_group_id) then
            return FALSE;
         end if;
         ---
         if not L_exist then
            O_error_message := SQL_LIB.CREATE_MSG('INV_FIELD_VALUE', 'Diff id', I_message.xdiffgrpdtlref_tbl(i).diff_id);
            return FALSE;
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_EXISTENCE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XDiffGrpRef_REC",
                      I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_VALIDATE.CHECK_DELETE';
   L_exist        BOOLEAN      := FALSE;

BEGIN
   if I_message_type = RMSSUB_XDIFFGRP.LP_dtl_del_type then
      FOR i in I_message.xdiffgrpdtlref_tbl.first..I_message.xdiffgrpdtlref_tbl.last LOOP
         if not DIFF_GROUP_SQL.CHECK_DELETE_DETAILS(O_error_message,
                                                    L_exist,
                                                    I_message.xdiffgrpdtlref_tbl(i).diff_id,
                                                    I_message.diff_group_id) then
            return FALSE;
         end if;
         if L_exist then
            O_error_message := SQL_LIB.CREATE_MSG('ASSOC_DTLS_DELETE');
            return FALSE;
         end if;
      end LOOP;
   elsif I_message_type = RMSSUB_XDIFFGRP.LP_del_type then
      if not DIFF_GROUP_SQL.CHECK_DELETE(O_error_message,
                                         L_exist,
                                         I_message.diff_group_id) then
         return FALSE;
      end if;

      if L_exist then
         O_error_message := SQL_LIB.CREATE_MSG('ASSOC_DTLS_DELETE');
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DELETE;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diffgrp_rec     OUT    NOCOPY   DIFF_GROUP_SQL.DIFF_GROUP_REC,
                         I_message         IN              "RIB_XDiffGrpDesc_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_VALIDATE.POPULATE_RECORD';
   L_user         DIFF_GROUP_HEAD.LAST_UPDATE_ID%TYPE  := get_user;
   L_date         DIFF_GROUP_HEAD.CREATE_DATETIME%TYPE := SYSDATE;


BEGIN

   O_diffgrp_rec.diff_group_head_row.diff_group_id        := I_message.diff_group_id;
   O_diffgrp_rec.diff_group_head_row.diff_type            := I_message.diff_group_type;
   O_diffgrp_rec.diff_group_head_row.diff_group_desc      := I_message.diff_group_desc;
   O_diffgrp_rec.diff_group_head_row.create_datetime      := NVL(I_message.create_datetime, L_date);
   O_diffgrp_rec.diff_group_head_row.last_update_id       := L_user;
   O_diffgrp_rec.diff_group_head_row.last_update_datetime := L_date;
   ---
   if I_message_type <> RMSSUB_XDIFFGRP.LP_mod_type then

      -- initialize collection
      O_diffgrp_rec.diff_group_details := DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL();

      FOR i in I_message.xdiffgrpdtl_tbl.first..I_message.xdiffgrpdtl_tbl.last LOOP
         O_diffgrp_rec.diff_group_details.EXTEND;
         O_diffgrp_rec.diff_group_details(i).diff_id              := I_message.xdiffgrpdtl_tbl(i).diff_id;
         O_diffgrp_rec.diff_group_details(i).diff_group_id        := I_message.diff_group_id;
         O_diffgrp_rec.diff_group_details(i).display_seq          := I_message.xdiffgrpdtl_tbl(i).display_seq;
         O_diffgrp_rec.diff_group_details(i).create_datetime      := NVL(I_message.xdiffgrpdtl_tbl(i).create_datetime, L_date);
         O_diffgrp_rec.diff_group_details(i).last_update_id       := L_user;
         O_diffgrp_rec.diff_group_details(i).last_update_datetime := L_date;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_diffgrp_rec     OUT    NOCOPY   DIFF_GROUP_SQL.DIFF_GROUP_REC,
                         I_message         IN              "RIB_XDiffGrpRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XDIFFGRP_VALIDATE.POPULATE_RECORD';

BEGIN

   O_diffgrp_rec.diff_group_head_row.diff_group_id  :=  I_message.diff_group_id;

   ---
   if I_message_type = RMSSUB_XDIFFGRP.LP_dtl_del_type then

      -- initialize collection
      O_diffgrp_rec.diff_group_details := DIFF_GROUP_SQL.DIFF_GROUP_DETAIL_TBL();

      FOR i in I_message.xdiffgrpdtlref_tbl.first..I_message.xdiffgrpdtlref_tbl.last LOOP
         O_diffgrp_rec.diff_group_details.EXTEND();
         O_diffgrp_rec.diff_group_details(i).diff_group_id  :=  I_message.diff_group_id;
         O_diffgrp_rec.diff_group_details(i).diff_id        :=  I_message.xdiffgrpdtlref_tbl(i).diff_id;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XDIFFGRP_VALIDATE;
/
