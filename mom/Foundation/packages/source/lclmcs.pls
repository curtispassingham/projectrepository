
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LOCLIST_STORE_MC_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------

-- Name         : CHANGE_ATTRIBUTE
-- Purpose      : Loops through each store on the location list and updates the store attribute
--                if it is valid.  If the attribute is not valid, the user will be prompted with
--                a message asking if a report should be generated.
-- Created By   : Rebecca Dobosh APR-99
---------------------------------------------------------------------------------------------
FUNCTION CHANGE_ATTRIBUTE (O_error_message        IN OUT   VARCHAR2,
                           O_reject_report        IN OUT   VARCHAR2,
                           I_loc_list             IN       loc_list_head.loc_list%TYPE,
                           I_vat_region_cb        IN       VARCHAR2,
                           I_vat_region           IN       vat_region.vat_region%TYPE,
                           I_district_cb          IN       VARCHAR2,
                           I_district             IN       district.district%TYPE,
                           I_tsf_zone_cb          IN       VARCHAR2,
                           I_tsf_zone             IN       tsfzone.transfer_zone%TYPE,
                           I_store_format_cb      IN       VARCHAR2,
                           I_store_format         IN       store_format.store_format%TYPE,
                           I_mall_name_cb         IN       VARCHAR2,
                           I_mall_name            IN       store.mall_name%TYPE,
                           I_default_wh_cb        IN       VARCHAR2,
                           I_default_wh           IN       wh.wh%TYPE,
                           I_lang_cb              IN       VARCHAR2,
                           I_lang                 IN       lang.lang%TYPE,
                           I_store_class_cb       IN       VARCHAR2,
                           I_store_class          IN       store.store_class%TYPE,
                           I_total_square_ft_cb   IN       VARCHAR2,
                           I_total_square_ft      IN       store.total_square_ft%TYPE,
                           I_selling_square_ft_cb IN       VARCHAR2,
                           I_selling_square_ft    IN       store.selling_square_ft%TYPE,
                           I_store_open_date_cb   IN       VARCHAR2,
                           I_store_open_date      IN       store.store_open_date%TYPE,
                           I_store_close_date_cb  IN       VARCHAR2,
                           I_store_close_date     IN       store.store_close_date%TYPE,
                           I_acquired_date_cb     IN       VARCHAR2,
                           I_acquired_date        IN       store.acquired_date%TYPE,
                           I_user_id              IN       USER_USERS.USERNAME%TYPE,
                           I_auto_rcv_cb          IN       VARCHAR2,
                           I_auto_rcv             IN       STORE.AUTO_RCV%TYPE,
                           I_customer_ord_loc_cb  IN       VARCHAR2,
                           I_customer_ord_loc_ind IN       STORE.customer_order_loc_ind%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------

-- Name         : CHANGE_LOC_TRAIT
-- Purpose      : This function loops through all the stores on the inputted location list
--                and, according to the action_type on the MC_LOC_TRAIT_TEMP table, delete
--                or add records (if nec) to  the LOC_TRAIT_MATRIX table.  Then all records
--                for that location list are cleared from MC_LOC_TRAIT_TEMP.
-- Created By   : Rebecca Dobosh   APR-99
---------------------------------------------------------------------------------------------
FUNCTION CHANGE_LOC_TRAIT (O_error_message        IN OUT   VARCHAR2,
                           I_loc_list             IN       loc_list_head.loc_list%TYPE)
   RETURN BOOLEAN;


-------------------------------------------------------------------------------------------
-- Name       : DIFFERENT_COST_CURRENCY
-- Purpose    : This function takes the inputted I_loc_list, I_zone_group_id and the
--              new I_new_zone_id and checks if any location on the loc list has a different
--              currency that of the new zone_id (COST_ZONE).  If it does O_exists
--              returns TRUE and a list of locations is passed out.
-- Created By : Rebecca S. Dobosh   APR-99
-------------------------------------------------------------------
FUNCTION DIFFERENT_COST_CURRENCY(O_error_message       IN OUT VARCHAR2,
                                 O_exists             IN OUT BOOLEAN,
                                 O_location_list      IN OUT VARCHAR2,
                                 I_loc_list           IN     loc_list_head.loc_list%TYPE,
                                 I_zone_group_id      IN     cost_zone.zone_group_id%TYPE,
                                 I_new_zone_id        IN     cost_zone.zone_id%TYPE)
    RETURN BOOLEAN;

-------------------------------------------------------------------

-- Name       : UPDATE_COST_ZONE_GROUP_LOC
-- Purpose    : This function loops through all the locations on the location list that have the
--              same currency code as the new zone_id and updates the cost_zone_group_loc
--              table with the new cost zone_id for the zone_group_id/location.
-- Created By : Rebecca S. Dobosh   APR-99
-------------------------------------------------------------------
FUNCTION UPDATE_COST_ZONE_GROUP_LOC(O_error_message   IN OUT VARCHAR2,
                                    I_loc_list        IN     loc_list_head.loc_list%TYPE,
                                    I_zone_group_id   IN     cost_zone.zone_group_id%TYPE,
                                    I_new_zone_id     IN     cost_zone.zone_id%TYPE)
    RETURN BOOLEAN;

-------------------------------------------------------------------
END;
/


