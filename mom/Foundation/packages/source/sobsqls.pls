



CREATE OR REPLACE PACKAGE SET_OF_BOOKS_SQL AS
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------
--- Function Name:  GET_DESC
--- Purpose:        This Function will get the description for the Set of Books ID
---                 passed as input from FIF_GL_SETUP table
------------------------------------------------------------------------------
FUNCTION GET_DESC(O_Error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_set_of_books_desc  IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE,
                  I_Set_of_books_id    IN       FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  GET_SOB_LOC
--- Purpose:        This Function will get the Set of Books ID and its description
---                 for the Location passed as input from FIF_GL_SETUP, STORE
---                 WH, PARTNER and TSF_ENTITY_ORG_UNIT_SOB tables
------------------------------------------------------------------------------
FUNCTION GET_SOB_LOC(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_set_of_books_desc  IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE,
                     O_set_of_books_id    IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE,
                     I_location           IN       ITEM_LOC.LOC%TYPE,
                     I_location_type      IN       ITEM_LOC.LOC_TYPE%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  CHECK_SUPP_SINGLE_LOC
--- Purpose:        This Function will return True in O_exists if the Location/Location Type
---                 and the Supplier passed as input are attached to the Same Org Unit
------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_SINGLE_LOC(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists             IN OUT   BOOLEAN,
                               I_supplier           IN       SUPS.SUPPLIER%TYPE,
                               I_location           IN       ITEM_LOC.LOC%TYPE,
                               I_location_type      IN       ITEM_LOC.LOC_TYPE%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  CHECK_SUPP_MASS_LOC
--- Purpose:        This Function will return true in O_exists if Location/Location Type
---                 related to the item don't have a primary supplier associated
------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_MASS_LOC (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists             IN OUT   BOOLEAN,
                              I_item               IN       ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  GET_TSF_ENTITY
--- Purpose:        This Function will return the TSF_ENTITY_ID for the Org_unit_id
---                 and the Set_of_books_id passed as input.
------------------------------------------------------------------------------
FUNCTION GET_TSF_ENTITY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_tsf_entity_id   IN OUT   TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_org_unit_id     IN       ORG_UNIT.ORG_UNIT_ID%TYPE,
                        I_set_of_books_id IN       FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  GET_SOB_CURRENCY
--- Purpose:        This Function will return the Currency Code and Set_of_books_id 
---                 from FIF_GL_SETUP table for the Org_unit_id passed as input.
------------------------------------------------------------------------------
FUNCTION GET_SOB_CURRENCY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_currency_code    IN OUT   FIF_GL_SETUP.CURRENCY_CODE%TYPE,
                          O_set_of_books_id  IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE,
                          I_org_unit_id      IN       ORG_UNIT.ORG_UNIT_ID%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  CHECK_ORG_EXISTS
--- Purpose:        This Function will return true in O_exists if the input TSF_ENTITY_ID
---                 is associated to at-least one Org_unit_id in TSF_ENTITY_ORG_UNIT_SOB table
------------------------------------------------------------------------------
FUNCTION CHECK_ORG_EXISTS(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists        IN OUT   BOOLEAN,
                          I_tsf_entity_id IN       TSF_ENTITY.TSF_ENTITY_ID%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  CHECK_SUPP_ORG_UNIT_EXISTS
--- Purpose:        This Function will return True in O_exists if the input Supplier
---                 is associated to the input Org Unit in partner_org_unit table
---                 When input Org Unit is NULL, it returns TRUE if supplier is
---                 associated to any Org Unit
------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_ORG_UNIT_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists          IN OUT   BOOLEAN,
                                    I_supplier        IN       SUPS.SUPPLIER%TYPE,
                                    I_org_unit_id     IN       ORG_UNIT.ORG_UNIT_ID%TYPE DEFAULT NULL)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  CHECK_ALL_LOCS_SINGLE_SOB
--- Purpose:        This Function will return true if all the locations in
---                 table LOCATION_DIST_TEMP belongs to the same Set of Books ID
------------------------------------------------------------------------------
FUNCTION CHECK_ALL_LOCS_SINGLE_SOB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists          IN OUT   BOOLEAN,
                                   I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  REFRESH_MV_LOC_SOB
--- Purpose:        This Function will perform a complete refresh of the materialized view
---                 MV_LOC_SOB
------------------------------------------------------------------------------
FUNCTION REFRESH_MV_LOC_SOB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   return BOOLEAN;

------------------------------------------------------------------------------
--- Function Name:  GET_SINGLE_SOB_ID
--- Purpose:        This Function will return Set of Books ID and Set of books Desc 
---                 from FIF_GL_SETUP
------------------------------------------------------------------------------
FUNCTION GET_SINGLE_SOB_ID(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_set_of_books_desc  IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_DESC%TYPE,
                           O_Set_of_books_id    IN OUT   FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name:  GET_CURRENCY_CODE
--- Purpose:        This Function will return the Currency Code 
---                 from FIF_GL_SETUP table for the Set_of_books_id passed as input.
------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_currency_code    IN OUT   FIF_GL_SETUP.CURRENCY_CODE%TYPE,
                           I_set_of_books_id  IN       FIF_GL_SETUP.SET_OF_BOOKS_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name:  GET_PRIMARY_SUPP
--- Purpose:        This Function will return the first supplier with matching Org Unit
---                 as that of the Location passed as input
------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_SUPP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_supplier           IN OUT   SUPS.SUPPLIER%TYPE,
                          I_location           IN       ITEM_LOC.LOC%TYPE,
                          I_location_type      IN       ITEM_LOC.LOC_TYPE%TYPE,
                          I_item               IN       ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------
--- Function Name:  GET_TSF_ENTITY
--- Purpose:        This Function will return the TSF_ENTITY_ID for the Org_unit_id
---                 passed as input.
------------------------------------------------------------------------------
FUNCTION GET_TSF_ENTITY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_tsf_entity_id   IN OUT   TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_org_unit_id     IN       ORG_UNIT.ORG_UNIT_ID%TYPE)
                        
   return BOOLEAN;
------------------------------------------------------------------------------
END SET_OF_BOOKS_SQL;
/
