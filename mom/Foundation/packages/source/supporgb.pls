
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SUPPORG_SQL AS
--------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
--- Procedure Name: QUERY_PROCEDURE
--- Purpose:        This procedure will query PARTNER_ORG_UNIT table and populate the above declared 
---                 table of records that will be used as the 'base table' in the Supplier Org Unit form.
------------------------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                           I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                           I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE)
IS

   cursor C_PARTNER_ORG_UNIT_QRY is
      select pou.org_unit_id,
             ou.description,
             pou.primary_pay_site,
             'TRUE' return_code,
             NULL error_message
        from partner_org_unit pou,
             v_org_unit_tl ou
       where partner = I_partner
         and partner_type = I_partner_type
         and ou.org_unit_id = pou.org_unit_id;

BEGIN

   if I_PARTNER is NULL then
      IO_partner_org_unit_tbl(1).return_code   := 'FALSE';
      IO_partner_org_unit_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                            'I_partner',
                                                                            'SUPPORG_SQL.QUERY_PROCEDURE',
                                                                            'NULL');   
   elsif I_PARTNER_TYPE is NULL then
      IO_partner_org_unit_tbl(1).return_code   := 'FALSE';
      IO_partner_org_unit_tbl(1).error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                                            'I_partner_type',
                                                                            'SUPPORG_SQL.QUERY_PROCEDURE',
                                                                            'NULL');
   else
      open C_PARTNER_ORG_UNIT_QRY;
      fetch C_PARTNER_ORG_UNIT_QRY BULK COLLECT into IO_partner_org_unit_tbl;
      close C_PARTNER_ORG_UNIT_QRY;
   end if;
EXCEPTION
   when OTHERS then
      IO_partner_org_unit_tbl(1).return_code   := 'FALSE';
      IO_partner_org_unit_tbl(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                             SQLERRM,
                                                                             'SUPPORG_SQL.QUERY_PROCEDURE',
                                                                             to_char(SQLCODE));
END QUERY_PROCEDURE;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: DELETE_PROCEDURE
--- Purpose:        This procedure will delete the corresponding records from PARTNER_ORG_UNIT table 
---                 for the inputs I_PARTNER, I_PARTNER_TYPE  and the org_unit_id in the IN OUT table rec.
------------------------------------------------------------------------------------------------------------
PROCEDURE DELETE_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                            I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                            I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE)
IS
BEGIN
   for i in 1..IO_partner_org_unit_tbl.COUNT
   loop
      delete from partner_org_unit
            where org_unit_id    = IO_partner_org_unit_tbl(i).org_unit_id
              and partner        = I_partner
              and partner_type   = I_partner_type;
   end loop;
END DELETE_PROCEDURE;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: INSERT_PROCEDURE
--- Purpose:        This procedure will insert data in the PARTNER_ORG_UNIT table
---                 from the Supplier Org Unit form.
------------------------------------------------------------------------------------------------------------
PROCEDURE INSERT_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                            I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                            I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE)
IS
BEGIN
   for i in 1..IO_partner_org_unit_tbl.COUNT
   loop
      insert into partner_org_unit (partner,
                                    partner_type,
                                    org_unit_id,
                                    primary_pay_site)
                           values (I_partner,
                                   I_partner_type,
                                   IO_partner_org_unit_tbl(i).org_unit_id,
                                   IO_partner_org_unit_tbl(i).primary_pay_site);
   end loop;

END INSERT_PROCEDURE;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: LOCK_PROCEDURE
--- Purpose:        This procedure will lock the queried records in PARTNER_ORG_UNIT table
------------------------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                          I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                          I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE)
IS
   L_dummy_lock_fetch   VARCHAR2(1);
BEGIN

   for i in 1..IO_partner_org_unit_tbl.COUNT
   loop
      select 'x'
        into L_dummy_lock_fetch
        from partner_org_unit
       where org_unit_id    = IO_partner_org_unit_tbl(i).org_unit_id
         and partner        = I_partner
         and partner_type   = I_partner_type
         for update nowait;
   end loop;

END LOCK_PROCEDURE;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: UPDATE_PROCEDURE
--- Purpose:        This procedure will update the record in the PARTNER_ORG_UNIT table
---                 from the Supplier Org Unit form.
------------------------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE (IO_partner_org_unit_tbl IN OUT   SUPPORG_SQL.PARTNER_ORG_UNIT_TBL,
                            I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                            I_partner_type          IN       PARTNER_ORG_UNIT.PARTNER_TYPE%TYPE)
IS

BEGIN
   for i in 1..IO_partner_org_unit_tbl.COUNT
   loop
      update partner_org_unit
         set primary_pay_site = IO_partner_org_unit_tbl(i).primary_pay_site
       where org_unit_id    = IO_partner_org_unit_tbl(i).org_unit_id
         and partner        = I_partner
         and partner_type   = I_partner_type;
   end loop;
END UPDATE_PROCEDURE;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: CHECK_PPSITE_REMADD_EXIST
--- Purpose:        This function will check if a primary pay-site already is associated to the 
---                 Supplier-Org Unit combination. If yes sets O_exists to True else to False
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PPSITE_REMADD_EXIST (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists                IN OUT   BOOLEAN,
                                    IO_org_unit_id          IN OUT   PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE,
                                    I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE)
RETURN BOOLEAN IS
   L_org_unit_id   PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE;

   cursor C_GET_ORG_UNIT is
      select org_unit_id 
        from partner_org_unit pou
       where pou.partner = I_partner
         and pou.partner_type ='U';

   cursor C_GET_PRIM_PAY_SITE is
      select 'X'
        from partner_org_unit pou,
             sups su,
             sups su1
       where pou.partner = su1.supplier
         and pou.partner_type = 'U'
         and pou.primary_pay_site = 'Y'
         and pou.org_unit_id = L_org_unit_id
         and su.supplier_parent = su1.supplier_parent
         and su.supplier = I_partner
       UNION ALL
       select 'X'
         from addr
        where module = 'SUPP'
          and key_value_1 = I_partner
          and addr_type = '06';

   L_cursor_fetch_dummy   VARCHAR2(1);

BEGIN
    
   if IO_org_unit_id is NOT NULL then
      L_org_unit_id := IO_org_unit_id;

      O_exists := TRUE;
      open C_GET_PRIM_PAY_SITE;
      fetch C_GET_PRIM_PAY_SITE into L_cursor_fetch_dummy;

      if C_GET_PRIM_PAY_SITE%NOTFOUND then
         O_exists := FALSE;
      end if;

      close C_GET_PRIM_PAY_SITE;      
   else
      for i in C_GET_ORG_UNIT 
      loop
         L_org_unit_id := i.org_unit_id;

         O_exists := TRUE;
         open C_GET_PRIM_PAY_SITE;
         fetch C_GET_PRIM_PAY_SITE into L_cursor_fetch_dummy;

         if C_GET_PRIM_PAY_SITE%NOTFOUND then
            O_exists := FALSE;
            IO_org_unit_id := i.org_unit_id;
            exit;
         end if;

         close C_GET_PRIM_PAY_SITE;
      end loop;  
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SUPPORG_SQL.CHECK_PPSITE_REMADD_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_PPSITE_REMADD_EXIST;
------------------------------------------------------------------------------------------------------------
--- Procedure Name: CHECK_PRIM_PAYSITE_EXIST
--- Purpose:        This function will check if a primary pay-site already is associated to the 
---                 Supplier-Org Unit combination. If yes sets O_exists to True else to False
------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_PRIM_PAYSITE_EXIST (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists                IN OUT   BOOLEAN,
                                   I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                                   I_org_unit_id           IN       PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN IS
   CURSOR C_GET_PRIM_PAY_SITE IS
   select 'X'
     from partner_org_unit pou,
          sups su,
          sups su1
    where pou.partner = su.supplier
      and pou.partner_type = 'U'
      and pou.primary_pay_site = 'Y'
      and pou.org_unit_id = I_org_unit_id
      and su.supplier_parent = su1.supplier_parent
      and su1.supplier = I_partner
      and su.supplier <> I_partner;

   L_cursor_fetch_dummy   VARCHAR2(1);

BEGIN
   O_exists := TRUE;

   open C_GET_PRIM_PAY_SITE;
   fetch C_GET_PRIM_PAY_SITE into L_cursor_fetch_dummy;
   if C_GET_PRIM_PAY_SITE%NOTFOUND then
      O_exists := FALSE;
   end if;
   close C_GET_PRIM_PAY_SITE;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SUPPORG_SQL.CHECK_PRIM_PAYSITE_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_PRIM_PAYSITE_EXIST;

------------------------------------------------------------------------------------------------------------
--- Procedure Name: UPDATE_PRIM_PAYSITE_N
--- Purpose:        This function will set the primary pay-site to "N" for all the supplier sites related to the
---                 Parent supplier (for the i/p site) - Org Unit combination. 
------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIM_PAYSITE_N (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_partner               IN       PARTNER_ORG_UNIT.PARTNER%TYPE,
                                I_org_unit_id           IN       PARTNER_ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN IS

   CURSOR C_GET_PRIM_PAY_SITE IS
   select 'X'
     from partner_org_unit pou,
          sups su,
          sups su1
    where pou.partner = su.supplier
      and pou.partner_type = 'U'
      and pou.primary_pay_site = 'Y'
      and pou.org_unit_id = I_org_unit_id
      and su.supplier_parent = su1.supplier_parent
      and su1.supplier = I_partner
      and su.supplier <> I_partner
      for update of pou.primary_pay_site nowait;

BEGIN
   open C_GET_PRIM_PAY_SITE;
   close C_GET_PRIM_PAY_SITE;
   
   update partner_org_unit pou
      set primary_pay_site = 'N'
    where pou.partner in (select su.supplier
                            from sups su,
                                 sups su1
                           where su.supplier_parent = su1.supplier_parent
                             and su1.supplier = I_partner
                             and su.supplier <> I_partner)
      and pou.partner_type = 'U'
      and pou.primary_pay_site = 'Y'
      and pou.org_unit_id = I_org_unit_id;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SUPPORG_SQL.UPDATE_PRIM_PAYSITE_N',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_PRIM_PAYSITE_N;
------------------------------------------------------------------------------------------------------------
END SUPPORG_SQL;
/
