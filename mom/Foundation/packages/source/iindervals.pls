CREATE OR REPLACE PACKAGE ITEM_INDUCT_ERRVAL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_EXISTS(O_error_message  IN OUT   VARCHAR2,
                           IO_desc          IN OUT   VARCHAR2,
                           IO_exists        IN OUT   BOOLEAN,
                           I_process_id     IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                           I_item           IN       V_IIND_ERRORS_LIST.ITEM%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER_EXISTS(O_error_message           IN OUT   VARCHAR2,
                               IO_sup_name               IN OUT   VARCHAR2,
                               IO_exists                 IN OUT   BOOLEAN,
                               I_process_id              IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                               I_supplier                IN       V_SUPS.SUPPLIER%TYPE,
                               I_supplier_site           IN       V_SUPS.SUPPLIER%TYPE,
                               I_GV_supplier_sites_ind   IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER_SITE_EXISTS(O_error_message       IN OUT   VARCHAR2,
                                    IO_sup_name           IN OUT   VARCHAR2,
                                    IO_sup_parent         IN OUT   VARCHAR2,
                                    IO_sup_parent_name    IN OUT   VARCHAR2,
                                    IO_sup_exists         IN OUT   BOOLEAN,
                                    IO_sup_site_exists    IN OUT   BOOLEAN,
                                    I_process_id          IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                                    I_supplier            IN       V_IIND_ERRORS_LIST.SUPPLIER%TYPE,
                                    I_supplier_site       IN       V_IIND_ERRORS_LIST.SUPPLIER%TYPE,
                                    I_supplier_provided   IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_COUNTRY_EXISTS(O_error_message   IN OUT   VARCHAR2,
                              IO_country_desc   IN OUT   VARCHAR2,
                              IO_exists         IN OUT   BOOLEAN,
                              I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                              I_country_id      IN       V_IIND_ERRORS_LIST.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_TABLE_DESC_EXISTS(O_error_message   IN OUT   VARCHAR2,
                                 IO_exists         IN OUT   BOOLEAN,
                                 I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                                 I_wksht           IN       V_IIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ROW_EXISTS(O_error_message   IN OUT   VARCHAR2,
                          IO_exists         IN OUT   BOOLEAN,
                          I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                          I_row             IN       V_IIND_ERRORS_LIST.ROW_SEQ%TYPE,
                          I_wksht           IN       V_IIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_COLUMN_EXISTS(O_error_message   IN OUT   VARCHAR2,
                             IO_exists         IN OUT   BOOLEAN,
                             I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                             I_column          IN       V_IIND_ERRORS_LIST.COLUMN_DESC%TYPE,
                             I_wksht           IN       V_IIND_ERRORS_LIST.TABLE_DESC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION CHECK_ERR_MSG_EXISTS(O_error_message   IN OUT   VARCHAR2,
                              IO_exists         IN OUT   BOOLEAN,
                              I_process_id      IN       V_IIND_ERRORS_LIST.PROCESS_ID%TYPE,
                              I_error           IN       V_IIND_ERRORS_LIST.ERROR_MSG%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
FUNCTION GET_PROCESS_INFO(O_error_message    IN OUT   VARCHAR2,
                          IO_process_desc    IN OUT   V_SVC_PRC_TRACKER.PROCESS_DESC%TYPE,
                          IO_template_name   IN OUT   V_SVC_PRC_TRACKER.TEMPLATE_NAME%TYPE,
                          IO_action_date     IN OUT   V_SVC_PRC_TRACKER.ACTION_DATE%TYPE,
                          I_process_id       IN       V_SVC_PRC_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
END ITEM_INDUCT_ERRVAL;
/
