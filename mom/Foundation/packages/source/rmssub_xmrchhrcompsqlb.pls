
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRCOMP_SQL AS

-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_COMPANY(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_company_rec    IN      COMPHEAD%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_COMPANY(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_company_rec    IN      COMPHEAD%ROWTYPE)
   RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_company_rec     IN       COMPHEAD%ROWTYPE,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCOMP_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XMRCHHRCOMP.LP_cre_type then
      if CREATE_COMPANY(O_error_message,
                        I_company_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRCOMP.LP_mod_type then
      if MODIFY_COMPANY(O_error_message,
                        I_company_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_COMPANY(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_company_rec    IN       COMPHEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCOMP_SQL.CREATE_COMPANY';


BEGIN

   if not MERCH_SQL.INSERT_COMPANY(O_error_message,
                                   I_company_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;

END CREATE_COMPANY;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_COMPANY(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_company_rec    IN       COMPHEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCOMP_SQL.MODIFY_COMPANY';

BEGIN

   if not MERCH_SQL.UPDATE_COMPANY(O_error_message,
                                   I_company_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE;

END MODIFY_COMPANY;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRCOMP_SQL;
/