CREATE OR REPLACE PACKAGE PRICING_ATTRIB_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------
-- Name:       GET_BASE_ZONE_RETAIL
-- Purpose:    Passes out base retail in local currency
-- Created By: Peter Newburg, 04-SEP-97
--------------------------------------------------------------------
FUNCTION GET_BASE_ZONE_RETAIL(
    O_error_message            IN OUT VARCHAR2,
    O_standard_unit_retail_zon IN OUT item_loc.unit_retail%TYPE,
    O_standard_uom_zon         IN OUT item_master.standard_uom%TYPE,
    O_selling_unit_retail_zon  IN OUT item_loc.selling_unit_retail%TYPE,
    O_selling_uom_zon          IN OUT item_loc.selling_uom%TYPE,
    O_multi_units_zon          IN OUT item_loc.multi_units%TYPE,
    O_multi_unit_retail_zon    IN OUT item_loc.multi_unit_retail%TYPE,
    O_multi_selling_uom_zon    IN OUT item_loc.multi_selling_uom%TYPE,
    I_item                     IN     item_loc.item%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Name:       GET_BASE_ZONE_RETAIL
-- Purpose:    Overloaded version passes out base retail 
--             in system currency as well.
-- Created By: Peter Newburg, 04-SEP-97
--------------------------------------------------------------------
FUNCTION GET_BASE_ZONE_RETAIL(
    O_error_message            IN OUT VARCHAR2,
    O_standard_unit_retail_pri IN OUT item_loc.unit_retail%TYPE,
    O_standard_unit_retail_zon IN OUT item_loc.unit_retail%TYPE,
    O_standard_uom_zon         IN OUT item_master.standard_uom%TYPE,
    O_selling_unit_retail_zon  IN OUT item_loc.selling_unit_retail%TYPE,
    O_selling_uom_zon          IN OUT item_loc.selling_uom%TYPE,
    O_multi_units_zon          IN OUT item_loc.multi_units%TYPE,
    O_multi_unit_retail_zon    IN OUT item_loc.multi_unit_retail%TYPE,
    O_multi_selling_uom_zon    IN OUT item_loc.multi_selling_uom%TYPE,
    I_item                     IN     item_loc.item%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Name:       GET_BASE_ZONE_RETAIL
-- Purpose:    Overloaded version passes out standard retail, 
--             selling retail and multi unit retail in system currency.
--------------------------------------------------------------------
FUNCTION GET_BASE_ZONE_RETAIL(O_error_message            IN OUT VARCHAR2,
                              O_standard_unit_retail_pri IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_unit_retail_zon IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom_zon         IN OUT ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail_pri  IN OUT ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_unit_retail_zon  IN OUT ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom_zon          IN OUT ITEM_LOC.SELLING_UOM%TYPE,
                              O_multi_units_zon          IN OUT ITEM_LOC.MULTI_UNITS%TYPE,
                              O_multi_unit_retail_pri    IN OUT ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                              O_multi_unit_retail_zon    IN OUT ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                              O_multi_selling_uom_zon    IN OUT ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                              I_item                     IN     ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------
-- Name:        GET_RETAIL
-- Description: Get the unit retail from the sku_zone_price table
--              for a given item number and a location.
-- Created By:  Peter Newburg, 05-SEP-97
------------------------------------------------------------------------
FUNCTION GET_RETAIL(
      O_error_message             IN OUT VARCHAR2,
      O_standard_unit_retail_loc  IN OUT item_loc.unit_retail%TYPE,
      O_standard_uom_loc          IN OUT item_master.standard_uom%TYPE,
      O_selling_unit_retail_loc   IN OUT item_loc.selling_unit_retail%TYPE,
      O_selling_uom_loc           IN OUT item_loc.selling_uom%TYPE,
      O_multi_units_loc           IN OUT item_loc.multi_units%TYPE,
      O_multi_unit_retail_loc     IN OUT item_loc.multi_unit_retail%TYPE,
      O_multi_selling_uom_loc     IN OUT item_loc.multi_selling_uom%TYPE,
      I_item                      IN     item_loc.item%TYPE,
      I_loc_type                  IN     item_loc.loc_type%TYPE,
      I_location                  IN     item_loc.loc%TYPE)
RETURN BOOLEAN;

------------------------------------------------------------------------
-- Name:        GET_RETAIL
-- Description: Get the unit retail from the sku_zone_price table
--              for a given item number and a location.
-- Created By:  Peter Newburg, 05-SEP-97
------------------------------------------------------------------------
FUNCTION GET_RETAIL(O_error_message             IN OUT VARCHAR2,
                    O_standard_unit_retail_loc  IN OUT item_loc.unit_retail%TYPE,
                    O_selling_unit_retail_loc   IN OUT item_loc.selling_unit_retail%TYPE,
                    O_selling_uom_loc           IN OUT item_loc.selling_uom%TYPE,
                    O_multi_units_loc           IN OUT item_loc.multi_units%TYPE,
                    O_multi_unit_retail_loc     IN OUT item_loc.multi_unit_retail%TYPE,
                    O_multi_selling_uom_loc     IN OUT item_loc.multi_selling_uom%TYPE,
                    I_item                      IN     item_loc.item%TYPE,
                    I_loc_type                  IN     item_loc.loc_type%TYPE,
                    I_location                  IN     item_loc.loc%TYPE)
RETURN BOOLEAN;


------------------------------------------------------------------------
-- Name:        BUILD_PACK_COST
-- Description: Determines the total cost of a pack by determining
--              the total cost of the component items.
-- Variables:   IO_supplier:  The supplier to determine the cost for
--                            or if supplier is Null, this function
--                            will determine the pack cost for the
--                            primary supplier and return the primary
--                            supplier in this variiable.

-- Created By:  Steven B. Olson, 10-OCT-97
-- Modified on:  18-MAY-98,
-- Modification: Added the parameter I_origin_country to account for the
--               new item/supplier data model. Unit cost now kept at
--               Item/Supplier/Origin Country level instead of Item/Supplier level.
------------------------------------------------------------------------
FUNCTION BUILD_PACK_COST( O_error_message    IN OUT VARCHAR2,
                          O_unit_cost        IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          IO_supplier        IN OUT SUPS.SUPPLIER%TYPE,
                          I_origin_country   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                          I_pack_no          IN     ITEM_MASTER.ITEM%TYPE,
                          I_dlvry_country    IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN;

------------------------------------------------------------------------
-- Name:        BUILD_PACK_RETAIL
-- Description: Determines the total retail of a pack by determining
--              the total retail of the component items.  The retail is
--              retreived from the base price zone of the pack component.

-- Created By:  Steven B. Olson, 10-OCT-97
------------------------------------------------------------------------
FUNCTION BUILD_PACK_RETAIL( O_error_message    IN OUT VARCHAR2,
                            O_unit_retail      IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                            I_pack_no          IN     ITEM_MASTER.ITEM%TYPE,
                            I_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_location         IN     STORE.STORE%TYPE)
                            RETURN BOOLEAN;

-------------------------------------------------------------------------
--Function Name: GET_CURRENT_UNIT_RETAIL
--Purpose:       To get the current price for a given item at a given location including
--               clearances, promotions, promotions on top of clearances.  This funciton
--               will fail if the input values for I_item or I_location are null.  This
--               function will fail if the given item is a fashion style or pack item.
--               This function assumes the given location and item are valid.
--Created by:    Erica Oesting, 07-May-98.
-------------------------------------------------------------------------
FUNCTION GET_CURRENT_UNIT_RETAIL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_current_price   IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                  O_selling_uom     IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                                  I_location        IN       ITEM_LOC.LOC%TYPE,
                                  I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                                  I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
--Function Name: GET_EXTERNAL_FINISHER_RETAIL
--Purpose:      Get retail price for external finisher through pricing
--              location.
-------------------------------------------------------------------------
FUNCTION GET_EXTERNAL_FINISHER_RETAIL(O_error_message     IN OUT VARCHAR2,
                                      O_unit_retail       IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                      I_item              IN     ITEM_LOC.ITEM%TYPE,
                                      I_external_finisher IN     PARTNER.PARTNER_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
FUNCTION GET_CURR_UNIT_RETAIL(O_error_message            IN OUT VARCHAR2,
                              O_standard_unit_retail     IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom             IN OUT ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail      IN OUT ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom              IN OUT ITEM_LOC.SELLING_UOM%TYPE,
                              I_item                     IN     ITEM_LOC.ITEM%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------
END PRICING_ATTRIB_SQL;
/