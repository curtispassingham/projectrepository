
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEMLIST_COUNT_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------
--- Procedure:  COUNT_LIST
--- Purpose:	This function counts the number of items that are in an itemlist.
-----------------------------------------------------------------------------------
FUNCTION COUNT_LIST (I_itemlist         IN     SKULIST_HEAD.SKULIST%TYPE,
                     I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                     I_item             IN     ITEM_MASTER.ITEM%TYPE,
                     I_item_parent      IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                     I_item_grandparent IN     ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                     I_dept             IN     ITEM_MASTER.DEPT%TYPE,
                     I_class            IN     ITEM_MASTER.CLASS%TYPE,
                     I_subclass         IN     ITEM_MASTER.SUBCLASS%TYPE,
                     I_supplier         IN     SKULIST_CRITERIA.SUPPLIER%TYPE,                     
                     I_diff_1           IN     ITEM_MASTER.DIFF_1%TYPE,
                     I_diff_2           IN     ITEM_MASTER.DIFF_2%TYPE,
                     I_diff_3           IN     ITEM_MASTER.DIFF_3%TYPE,
                     I_diff_4           IN     ITEM_MASTER.DIFF_4%TYPE,
                     I_uda_id           IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                     I_uda_value        IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                     I_uda_max_date     IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                     I_uda_min_date     IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                     I_season_id        IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                     I_phase_id         IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                     I_item_level       IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                     O_no_items         IN OUT NUMBER,
                     O_error_message    IN OUT VARCHAR2) RETURN BOOLEAN;
-----------------------------------------------------------------------------------
--- Procedure:  COUNT_EXISTS
--- Purpose:	This function counts the number of items that are in an itemlist
---               and exist on the skulist_detail table.
-----------------------------------------------------------------------------------
FUNCTION COUNT_EXISTS (I_itemlist          IN     SKULIST_HEAD.SKULIST%TYPE,
                       I_pack_ind          IN     ITEM_MASTER.PACK_IND%TYPE,
                       I_item              IN     ITEM_MASTER.ITEM%TYPE,
                       I_item_parent       IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_item_grandparent  IN     ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                       I_dept              IN     ITEM_MASTER.DEPT%TYPE,
                       I_class             IN     ITEM_MASTER.CLASS%TYPE,
                       I_subclass          IN     ITEM_MASTER.SUBCLASS%TYPE,
                       I_supplier          IN     SKULIST_CRITERIA.SUPPLIER%TYPE,                       
                       I_diff_1            IN     ITEM_MASTER.DIFF_1%TYPE,
                       I_diff_2            IN     ITEM_MASTER.DIFF_2%TYPE,
                       I_diff_3            IN     ITEM_MASTER.DIFF_3%TYPE,
                       I_diff_4            IN     ITEM_MASTER.DIFF_4%TYPE,
                       I_uda_id            IN     SKULIST_CRITERIA.UDA_ID%TYPE,
                       I_uda_value         IN     SKULIST_CRITERIA.UDA_VALUE_LOV%TYPE,
                       I_uda_max_date      IN     SKULIST_CRITERIA.UDA_VALUE_MAX_DATE%TYPE,
                       I_uda_min_date      IN     SKULIST_CRITERIA.UDA_VALUE_MIN_DATE%TYPE,
                       I_season_id         IN     SKULIST_CRITERIA.SEASON_ID%TYPE,
                       I_phase_id          IN     SKULIST_CRITERIA.PHASE_ID%TYPE,
                       I_item_level        IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                       O_no_items          IN OUT NUMBER,
                       O_error_message     IN OUT VARCHAR2) RETURN BOOLEAN;
-----------------------------------------------------------------------------------
END;
/
