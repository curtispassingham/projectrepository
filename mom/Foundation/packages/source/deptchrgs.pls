
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEPT_CHARGE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name:  APPLY_CHARGES
--Purpose      :  This function will insert or update dept_chrg_head and 
--                dept_chrg_detail records. 

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION APPLY_CHARGES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_dept              IN     DEPS.DEPT%TYPE,
                       I_from_group_type   IN     CODE_DETAIL.CODE%TYPE,
                       I_from_group        IN     STORE.STORE%TYPE,
                       I_to_group_type     IN     CODE_DETAIL.CODE%TYPE,
                       I_to_group          IN     STORE.STORE%TYPE,
                       I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                       I_up_chrg_group     IN     ITEM_CHRG_DETAIL.UP_CHRG_GROUP%TYPE,
                       I_comp_rate         IN     ELC_COMP.COMP_RATE%TYPE,
                       I_per_count         IN     ELC_COMP.PER_COUNT%TYPE,
                       I_per_count_uom     IN     ELC_COMP.PER_COUNT_UOM%TYPE,
                       I_comp_currency     IN     ELC_COMP.COMP_CURRENCY%TYPE,
                       I_insert_update_del IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  DELETE_LOCS
--Purpose      :  This function will delete the passed in dept/from loc/to loc
--                combination from the dept_chrg_head table after first deleting
--                all associated components from the dept_chrg_detail table. 
-------------------------------------------------------------------------------
FUNCTION DELETE_LOCS(O_error_message   IN OUT VARCHAR2,
                     I_dept            IN     DEPS.DEPT%TYPE,
                     I_from_group_type IN     CODE_DETAIL.CODE%TYPE,
                     I_from_group      IN     STORE.STORE%TYPE,
                     I_to_group_type   IN     CODE_DETAIL.CODE%TYPE,
                     I_to_group        IN     STORE.STORE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CHARGES_EXIST
-- Purpose      : This function will check if up charges exist for the given 
--                dept/from location/to location combination. 
------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_dept          IN     DEPS.DEPT%TYPE,
                       I_from_loc      IN     ORDLOC.LOCATION%TYPE,
                       I_to_loc        IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
-- Function Name: CHARGES_EXIST
-- Purpose      : This function will check if up charges exist for the given dept. 
------------------------------------------------------------------------------------
FUNCTION CHARGES_EXIST(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_dept          IN     DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_HEADER_NO_DETAILS
-- Purpose      : Checks for Dept Charge Header records that do not have any 
--                associated Dept Charge Detail records.  Sets O_exists to TRUE 
--                if records found with no detail records.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_HEADER_NO_DETAILS(O_error_message IN OUT  VARCHAR2,
                                 O_exists        IN OUT  BOOLEAN,
                                 I_dept          IN     DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_HEADER
-- Purpose      : Deletes Dept Charge Header records that do not have any associated Dept 
--                Charge Detail records.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message IN OUT  VARCHAR2,
                       I_dept            IN     DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------                     
END DEPT_CHARGE_SQL;
/
