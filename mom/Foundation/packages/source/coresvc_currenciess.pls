CREATE OR REPLACE PACKAGE CORESVC_CURRENCIES AUTHID CURRENT_USER AS
   template_key              CONSTANT VARCHAR2(255)         := 'CURRENCIES_DATA';
   action_new                         VARCHAR2(25)          := 'NEW';
   action_mod                         VARCHAR2(25)          := 'MOD';
   action_del                         VARCHAR2(25)          := 'DEL';
   CURRENCIES_sheet                   VARCHAR2(255)         := 'CURRENCIES';
   CURRENCIES$ACTION                  NUMBER                :=1;
   CURRENCIES$CURRENCY_CODE           NUMBER                :=2;
   CURRENCIES$CURRENCY_DESC           NUMBER                :=3;
   CURRENCIES$CURRENCY_COST_FMT       NUMBER                :=4;
   CURRENCIES$CURRENCY_RTL_FMT        NUMBER                :=5;
   CURRENCIES$CURRENCY_COST_DEC       NUMBER                :=6;
   CURRENCIES$CURRENCY_RTL_DEC        NUMBER                :=7;
   CURRENCY_RATES_sheet               VARCHAR2(255)         := 'CURRENCY_RATES';
   CURRENCY_RATES$ACTION              NUMBER                :=1;
   CURRENCY_RATES$EXCHANGE_RATE       NUMBER                :=5;
   CURRENCY_RATES$EXCHANGE_TYPE       NUMBER                :=4;
   CURRENCY_RATES$EFFECTIVE_DATE      NUMBER                :=3;
   CURRENCY_RATES$CURRENCY_CODE       NUMBER                :=2;
   CURRENCIES_TL_sheet                VARCHAR2(255)         := 'CURRENCIES_TL';
   CURRENCIES_TL$ACTION               NUMBER                :=1;
   CURRENCIES_TL$LANG                 NUMBER                :=2;
   CURRENCIES_TL$CURRENCY_CODE        NUMBER                :=3;
   CURRENCIES_TL$CURRENCY_DESC        NUMBER                :=4;
   sheet_name_trans                S9T_PKG.TRANS_MAP_TYP;
   action_column                   VARCHAR2(255)         := 'ACTION';
   template_category               CODE_DETAIL.CODE%TYPE := 'RMSFND';
   TYPE currency_rates_rec_tab IS TABLE OF CURRENCY_RATES%ROWTYPE;
   TYPE currencies_rec_tab IS TABLE OF CURRENCIES%ROWTYPE;
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   FUNCTION process_s9t(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count    IN OUT   NUMBER,
                        I_file_id        IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id     IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name      IN       VARCHAR2)
   RETURN VARCHAR2;
END CORESVC_CURRENCIES;
/