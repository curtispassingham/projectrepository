
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRDEPT AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------
   LP_cre_type       VARCHAR2(15) := 'xmrchhrdeptcre';
   LP_mod_type       VARCHAR2(15) := 'xmrchhrdeptmod';
   LP_del_type       VARCHAR2(15) := 'xmrchhrdeptdel';

   LP_dtl_cre_type   VARCHAR2(15) := 'xmrchhrvatcre';
   LP_dtl_mod_type   VARCHAR2(15) := 'xmrchhrvatmod';

----------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2);
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRDEPT;
/
