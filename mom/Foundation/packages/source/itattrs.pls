CREATE OR REPLACE PACKAGE ITEM_ATTRIB_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
TYPE api_item_rec IS RECORD(item                 ITEM_MASTER.ITEM%TYPE,
                            dept                 ITEM_MASTER.DEPT%TYPE,
                            class                ITEM_MASTER.CLASS%TYPE,
                            subclass             ITEM_MASTER.SUBCLASS%TYPE,
                            item_level           ITEM_MASTER.ITEM_LEVEL%TYPE,
                            tran_level           ITEM_MASTER.TRAN_LEVEL%TYPE,
                            status               ITEM_MASTER.STATUS%TYPE,
                            pack_ind             ITEM_MASTER.PACK_IND%TYPE,
                            sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE,
                            orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE,
                            pack_type            ITEM_MASTER.PACK_TYPE%TYPE,
                            order_as_type        ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                            standard_uom         ITEM_MASTER.STANDARD_UOM%TYPE,
                            simple_pack_ind      ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                            contains_inner_ind   ITEM_MASTER.CONTAINS_INNER_IND%TYPE,
                            cost_zone_group_id   ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE,
                            item_parent          ITEM_MASTER.ITEM_PARENT%TYPE);
TYPE GV_EAN_TABLE_T IS TABLE OF ITEM_MASTER.ITEM%TYPE;
TYPE GV_item_table_t IS TABLE OF ITEM_MASTER.ITEM%TYPE;

---------------------------------------------------------------------------------------------
-- Function Name : GET_ITEM_TYPE
-- Purpose       : Decodes the flags on the specified item to give an item type.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_TYPE (O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_item_type_code         OUT  VARCHAR2                ,
                        I_item                IN      ITEM_MASTER.ITEM%TYPE    )
   RETURN BOOLEAN ;
---------------------------------------------------------------------------------------------
-- Function Name : NEXT_ITEM
-- Purpose       : This function will return the next item sequence.  If the check_digit_ind on
--                 system_options = 'Y', the number will be check digited.  If the indicator is
--                 no, the number will not be check digited.
-- Calls         : CHKDIG_ADD
---------------------------------------------------------------------------------------------

FUNCTION NEXT_ITEM (O_error_message    OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_item          IN OUT ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INV_STATUS_DESC
-- Purpose      : gets the inventory_status description for a given inv_status
-- Created      : 21-NOV-96 Richard Stockton
---------------------------------------------------------------------------------------------
FUNCTION INV_STATUS_DESC(O_error_message    IN OUT VARCHAR2,
                         I_inv_status       IN     NUMBER,
                         O_desc             IN OUT VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function Name:  GET_MERCH_HIER
--- Purpose:        To return the dept, class, and subclass for a sku.
--- Calls:          None.
---------------------------------------------------------------------------------------------
   FUNCTION GET_MERCH_HIER( O_error_message IN OUT VARCHAR2,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE,
                            O_dept          IN OUT ITEM_MASTER.DEPT%TYPE,
                            O_class         IN OUT ITEM_MASTER.CLASS%TYPE,
                            O_subclass      IN OUT ITEM_MASTER.SUBCLASS%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Name:       GET_BASE_COST_RETAIL
-- Purpose:    This function returns the base unit cost and
--             unit retail in primary currency for a given item number.
--             Depending on I_calc_type:
--             NULL        - both cost and retail are calculated and returned.
--             'C'        - cost is calculated and returned.
--             'R'        - retail is calculated and returned.
-- Created By: Dave Dederichs, 09-SEPT-97
--             (Note: This replaced the previous GET_PRICES function).
---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_COST_RETAIL(O_error_message             IN OUT    VARCHAR2,
                              O_unit_cost_prim            IN OUT  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              O_standard_unit_retail_prim IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom_prim         IN OUT  ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail_prim  IN OUT  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom_prim          IN OUT  ITEM_LOC.SELLING_UOM%TYPE,
                              I_item                      IN      ITEM_SUPPLIER.ITEM%TYPE,
                              I_calc_type                 IN      VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_BASE_COST_RETAIL(O_error_message             IN OUT  VARCHAR2,
                              O_unit_cost_prim            IN OUT  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              O_standard_unit_retail_prim IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom_prim         IN OUT  ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail_prim  IN OUT  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom_prim          IN OUT  ITEM_LOC.SELLING_UOM%TYPE,
                              I_item                      IN      ITEM_SUPPLIER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function:  CONSIGNMENT_ITEM
-- Purpose:   To check if the item is on consignment, this function will accept a
--            SKU and check against the sku_supplier table make the determination.
--            If the item is on consignment, then it will pass back TRUE in the
--            parameter O_consignment.
---------------------------------------------------------------------------------------------
FUNCTION CONSIGNMENT_ITEM(O_error_message IN OUT VARCHAR2,
                          I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                          O_consignment   IN OUT BOOLEAN)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- FUNCTION: GET_COST_ZONE_GROUP
-- PURPOSE : Retrieves an item's cost zone group.
-- CREATED : Arthur Palileo, 3-NOV-1997
---------------------------------------------------------------------------------------------
FUNCTION GET_COST_ZONE_GROUP(
                             O_error_message      IN OUT VARCHAR2,
                             O_cost_zone_group_ID IN OUT COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                             I_item               IN     ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- FUNCTION: IMPORT_ATTR_EXISTS
-- PURPOSE : Checks if import specific attributes exist for a given item
-- CALLS :   <none>
---------------------------------------------------------------------------------------------
FUNCTION IMPORT_ATTR_EXISTS(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- FUNCTION: ITEM_ELIGIBLE_EXISTS
-- PURPOSE : Determines if an item eligible relationship exists for a given item
-- CALLS :   <none>
---------------------------------------------------------------------------------------------
FUNCTION ITEM_ELIGIBLE_EXISTS(O_error_message  IN OUT VARCHAR2,
                              O_exists         IN OUT BOOLEAN,
                              I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- FUNCTION: SUPPLIER_EXISTS
-- PURPOSE : Determines if a supplier relationship exists for a given item
-- CALLS:    <none>
---------------------------------------------------------------------------------------------
FUNCTION SUPPLIER_EXISTS(O_error_message  IN OUT VARCHAR2,
                         O_exists         IN OUT BOOLEAN,
                         I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
-- Function: GET_COST_ZONE
-- Purpose : Retrieves the cost zone off of the cost zone group locations table
--            based on the zone_group, location, location_type, and item input
--            parameters
-- Calls  : ITEM_ATTRIB_SQL.GET_ZONE_GROUP
----------------------------------------------------------------------------------------
FUNCTION GET_COST_ZONE(O_error_message IN OUT VARCHAR2,
                       O_zone_id       IN OUT COST_ZONE.ZONE_ID%TYPE,
                       I_item          IN     ITEM_MASTER.ITEM%TYPE,
                       I_zone_group_id IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                       I_loc           IN     STORE.STORE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  GET_STANDARD_UOM
-- Purpose: Return the standard UOM, UOM conversion factor for the passed in
--          item.  If item_type is passed in NULL, it will fetch item_type (i.e.
--          system_ind).  If I_get_class_ind is passed in as 'Y', the UOM class
--          that the standard UOM belongs to will also be returned.
----------------------------------------------------------------------------------------------
FUNCTION GET_STANDARD_UOM(O_error_message               IN OUT VARCHAR2,
                          O_standard_uom                IN OUT UOM_CLASS.UOM%TYPE,
                          O_standard_class              IN OUT UOM_CLASS.UOM_CLASS%TYPE,
                          O_conv_factor                 IN OUT ITEM_MASTER.UOM_CONV_FACTOR%TYPE,
                          I_item                        IN     ITEM_MASTER.ITEM%TYPE,
                          I_get_class_ind               IN     VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Funtion Name:  GET_STORE_ORD_MULT_AND_UOM
-- Purpose: Return the store_order_multiple and standard UOM for the passed in
--          item.  If item_type is passed in NULL, it will fetch item_type (i.e.
--          system_ind).
----------------------------------------------------------------------------------------------
FUNCTION GET_STORE_ORD_MULT_AND_UOM(O_error_message   IN OUT VARCHAR2,
                                    O_store_ord_mult  IN OUT ITEM_MASTER.STORE_ORD_MULT%TYPE,
                                    O_standard_uom    IN OUT ITEM_MASTER.STANDARD_UOM%TYPE,
                                    I_item            IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name:   GET_WASTAGE
-- Purpose: This function retrieves the wastage type and the wastage percenctage
--          from the item_master table associated with an item (wastage is the portion
--          of an item that is lost between the time of purchase and the time of sale).
-----------------------------------------------------------------------------------------------
FUNCTION GET_WASTAGE(O_error_message     IN OUT VARCHAR2,
                     O_waste_type        IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                     O_waste_pct         IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                     O_default_waste_pct IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                     I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function Name:  UPDATE_DEFAULT_UOP
-- Purpose:        The function will update the default_uop, if the items standard uom is changed
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_DEFAULT_UOP(O_error_message     IN OUT VARCHAR2,
                            I_standard_uom      IN     ITEM_MASTER.STANDARD_UOM%TYPE,
                            I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: PURCHASE_TYPE
-- Purpose      : This function will check if an item belongs to a consignment dept.
-- Created      : 03-30-00 Andrew Cariveau
---------------------------------------------------------------------------------------------
FUNCTION PURCHASE_TYPE(O_error_message    IN OUT VARCHAR2,
                       O_purchase_type    IN OUT DEPS.PURCHASE_TYPE%TYPE,
                       I_item             IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
--- Function Name:  GET_DESC
--- Purpose:        To return the desc, status for a item.
--- Calls:          None.
---------------------------------------------------------------------------------------------
FUNCTION GET_DESC( O_error_message IN OUT VARCHAR2,
                   O_desc          IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                   I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function Name:  GET_DESC
--- Purpose:        To return the desc and status for a item.  This
---                 overloaded version of the function saves numerous call
---
--- Calls:          None
---------------------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message IN OUT VARCHAR2,
                  O_desc          IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_status        IN OUT ITEM_MASTER.STATUS%TYPE,
                  I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message IN OUT VARCHAR2,
                  O_desc          IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_status        IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_item_level    IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level    IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  I_item          IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_LEVELS - This function returns from item master the item level and transactional
--              level for the entered item.
---------------------------------------------------------------------------------------------
FUNCTION GET_LEVELS (O_error_message         IN OUT VARCHAR2,
                     O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                     O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                     I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_INFO - This function retrieve the following information from item master:
--            item description, item level, transaction level, status, pack indicator,
--            department, class, subclass, retail zone group, sellable indicator, orderable
--            indicator, pack type, wastage type.
--            The names of the department, class and subclass should then be retrieved using
--            DEPT_ATTRIB_SQL.GET_NAME, CLASS_ATTRIB_SQL.GET_NAME and
--            SUBCLASS_ATTRIB_SQL.GET_NAME.
---------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT VARCHAR2,
                  O_item_desc             IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT ITEM_MASTER.PACK_IND%TYPE,
                  O_dept                  IN OUT ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                  O_simple_pack_ind       IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                  O_waste_type            IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                  O_item_parent           IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_grandparent      IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                  O_short_desc            IN OUT ITEM_MASTER.SHORT_DESC%TYPE,
                  O_waste_pct             IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                  O_default_waste_pct     IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                  I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT VARCHAR2,
                  O_item_desc             IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT ITEM_MASTER.PACK_IND%TYPE,
                  O_dept                  IN OUT ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                  O_item_parent           IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_number_type      IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                  I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_INFO - This is similar to the GET_INFO above but it is overloaded to retrieve
--            a few more things: item_number_type, diff_1, diff_1_desc, diff_1_type,
--            diff_1_id_group_ind, diff_2, diff_2_desc, diff_2_type, diff_2_id_group_ind,
--            diff_3, diff_3_desc, diff_3_type, diff_3_id_group_ind, diff_4, diff_4_desc,
--            diff_4_type, diff_4_id_group_ind, order_as_type
---------------------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT VARCHAR2,
                  O_item_desc             IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT ITEM_MASTER.PACK_IND%TYPE,
                  O_dept                  IN OUT ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                  O_simple_pack_ind       IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                  O_waste_type            IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                  O_item_parent           IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_grandparent      IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                  O_short_desc            IN OUT ITEM_MASTER.SHORT_DESC%TYPE,
                  O_waste_pct             IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                  O_default_waste_pct     IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                  O_item_number_type      IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                  O_diff_1                IN OUT ITEM_MASTER.DIFF_1%TYPE,
                  O_diff_1_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_1_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_1_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_diff_2                IN OUT ITEM_MASTER.DIFF_2%TYPE,
                  O_diff_2_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_2_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_2_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_order_as_type         IN OUT ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                  O_format_id             IN OUT ITEM_MASTER.FORMAT_ID%TYPE,
                  O_prefix                IN OUT ITEM_MASTER.PREFIX%TYPE,
                  O_store_ord_mult        IN OUT ITEM_MASTER.STORE_ORD_MULT%TYPE,
                  O_contains_inner_ind    IN OUT ITEM_MASTER.CONTAINS_INNER_IND%TYPE,
                  I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

FUNCTION GET_INFO(O_error_message         IN OUT VARCHAR2,
                  O_item_desc             IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT ITEM_MASTER.PACK_IND%TYPE,
                  O_dept                  IN OUT ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                  O_simple_pack_ind       IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                  O_waste_type            IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                  O_item_parent           IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_grandparent      IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                  O_short_desc            IN OUT ITEM_MASTER.SHORT_DESC%TYPE,
                  O_waste_pct             IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                  O_default_waste_pct     IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                  O_item_number_type      IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                  O_diff_1                IN OUT ITEM_MASTER.DIFF_1%TYPE,
                  O_diff_1_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_1_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_1_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_diff_2                IN OUT ITEM_MASTER.DIFF_2%TYPE,
                  O_diff_2_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_2_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_2_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_diff_3                IN OUT ITEM_MASTER.DIFF_3%TYPE,
                  O_diff_3_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_3_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_3_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_diff_4                IN OUT ITEM_MASTER.DIFF_4%TYPE,
                  O_diff_4_desc           IN OUT V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                  O_diff_4_type           IN OUT V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE,
                  O_diff_4_id_group_ind   IN OUT V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
                  O_order_as_type         IN OUT ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                  O_format_id             IN OUT ITEM_MASTER.FORMAT_ID%TYPE,
                  O_prefix                IN OUT ITEM_MASTER.PREFIX%TYPE,
                  O_store_ord_mult        IN OUT ITEM_MASTER.STORE_ORD_MULT%TYPE,
                  O_contains_inner_ind    IN OUT ITEM_MASTER.CONTAINS_INNER_IND%TYPE,
                  I_item                  IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_PACK_INDS - This function retrieve from item master the entered item's pack
--                 indicator, sellable indicator, orderable indicator and pack type.
---------------------------------------------------------------------------------------------
FUNCTION GET_PACK_INDS (O_error_message  IN OUT VARCHAR2,
                        O_pack_ind       IN OUT ITEM_MASTER.PACK_IND%TYPE,
                        O_sellable_ind   IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                        O_orderable_ind  IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                        O_pack_type      IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_PACK_INDS - This is similar to the GET_PACK_INDS above but it is overloaded to retrieve
--                 the order_as_type of the pack as well.
---------------------------------------------------------------------------------------------
FUNCTION GET_PACK_INDS (O_error_message  IN OUT VARCHAR2,
                        O_pack_ind       IN OUT ITEM_MASTER.PACK_IND%TYPE,
                        O_sellable_ind   IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                        O_orderable_ind  IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                        O_pack_type      IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                        O_order_as_type  IN OUT ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_PACK_INDS - This is similar to the GET_PACK_INDS above but it is overloaded to retrieve
--                 the notional_pack_ind of the pack as well.
---------------------------------------------------------------------------------------------
FUNCTION GET_PACK_INDS (O_error_message      IN OUT VARCHAR2,
                        O_pack_ind           IN OUT ITEM_MASTER.PACK_IND%TYPE,
                        O_sellable_ind       IN OUT ITEM_MASTER.SELLABLE_IND%TYPE,
                        O_orderable_ind      IN OUT ITEM_MASTER.ORDERABLE_IND%TYPE,
                        O_pack_type          IN OUT ITEM_MASTER.PACK_TYPE%TYPE,
                        O_order_as_type      IN OUT ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                        O_notional_pack_ind  IN OUT ITEM_MASTER.NOTIONAL_PACK_IND%TYPE,
                        I_item               IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_PARENT_INFO - This function retrieve the entered item's parent and grandparent along
--                   with their descriptions.
--                   If the entered item doesn't have a parent or grandparent the function
--                   will return null for the relevant fields and return TRUE.
---------------------------------------------------------------------------------------------
FUNCTION GET_PARENT_INFO  (O_error_message     IN OUT VARCHAR2,
                           O_parent            IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                           O_parent_desc       IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                           O_grandparent       IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                           O_grandparent_desc  IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                           I_item              IN  ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- EXITS_AS_SUB_ITEM - This function check if the entered item exists as a substitute
--                     item for another item.
---------------------------------------------------------------------------------------------
FUNCTION EXISTS_AS_SUB_ITEM (O_error_message  IN OUT VARCHAR2,
                             O_exists         IN OUT BOOLEAN,
                             I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- UDA_EXIST - This function check if the entered item has a UDA of any type
--             (date, LOV or free form) attached.
---------------------------------------------------------------------------------------------
FUNCTION UDA_EXISTS (O_error_message  IN OUT VARCHAR2,
                     O_exists         IN OUT BOOLEAN,
                     I_item           IN    ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- TICKET_EXITS - This function check if the entered item has any tickets associated with it.
---------------------------------------------------------------------------------------------
FUNCTION TICKET_EXISTS (O_error_message  IN OUT VARCHAR2,
                        O_exists         IN OUT BOOLEAN,
                        I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_DIFFS -This new function will retrieve the diff_1, diff_2, diff_3, and diff_4
--            columns from item_master for an item.
---------------------------------------------------------------------------------------------
FUNCTION GET_DIFFS (O_error_message  IN OUT VARCHAR2,
                    O_diff_1         IN OUT ITEM_MASTER.DIFF_1%TYPE,
                    O_diff_2         IN OUT ITEM_MASTER.DIFF_2%TYPE,
                    O_diff_3         IN OUT ITEM_MASTER.DIFF_3%TYPE,
                    O_diff_4         IN OUT ITEM_MASTER.DIFF_4%TYPE,
                    I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- VALIDATE_ITEM_DIFF - This new function will replace the old
--                      FASHION_VALIDATE_SQL.VALIDATE_STYLE_COLOR function.
--                      This new function will validate if the diff_id/item combination
--                      exists.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_DIFF(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_diff_id        IN     DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- EXPENSE_EXISTS - This function will determine whether expenses exist for an item or not.
-- Created By: Chad Offerman  OCT 11, 00
---------------------------------------------------------------------------------------------
FUNCTION EXPENSE_EXISTS(O_error_message   IN OUT VARCHAR2,
                        O_exists          IN OUT BOOLEAN,
                        I_item            IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_CONST_DIMEN_IND - This new function will retrieve the constant dimension indicator if
--                       it is passed as NULL to itemsuppctry.fmb.
---------------------------------------------------------------------------------------------
FUNCTION GET_CONST_DIMEN_IND(O_error_message   IN OUT VARCHAR2,
                             O_const_dim_ind   IN OUT ITEM_MASTER.CONST_DIMEN_IND%TYPE,
                             I_item            IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- UNAV_INV_EXISTS - This function will return TRUE if any unavailable inventory records
--                   exist on inv_status_qty for the given item.
---------------------------------------------------------------------------------------------
FUNCTION UNAV_INV_EXISTS(O_error_message   IN OUT VARCHAR2,
                         O_exists          IN OUT BOOLEAN,
                         I_item            IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- CHECK_SUB_ITEM - This function checks if the entered item exists as a substitute
--                  item for another item or contains substitute items.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_SUB_ITEM(O_error_message  IN OUT  VARCHAR2,
                        O_exists         IN OUT  BOOLEAN,
                        O_item_or_sub    IN OUT  VARCHAR2,
                        I_item           IN      SUB_ITEMS_DETAIL.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_ITEM_NUMBER_TYPE - This function will return the item number type from item_master
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_NUMBER_TYPE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_item_number_type  IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                              I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_CHILD_ITEM_NUMBER_TYPE - This function will return the item number type from item_master
--                              for the children of the entered item.  If no children exists,
--                              O_exists will be FALSE.
---------------------------------------------------------------------------------------------
FUNCTION GET_CHILD_ITEM_NUMBER_TYPE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_item_number_type  IN OUT ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                                    O_exists            IN OUT BOOLEAN,
                                    I_item_parent       IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_PRIMARY_REF_ITEM - This function will the primary reference item for the entered item.
--                        If one doesn't exist, O_exists will return FALSE.
---------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_REF_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_primary_ref_item  IN OUT ITEM_MASTER.ITEM%TYPE,
                              O_exists            IN OUT BOOLEAN,
                              I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: OUTSTAND_ORDERS_EXIST
-- Purpose      : This function will check if an item or it's children exist on any
--                purchase orders.
---------------------------------------------------------------------------------------------
FUNCTION OUTSTAND_ORDERS_EXIST(O_error_message IN OUT VARCHAR2,
                               O_exists        IN OUT BOOLEAN,
                               I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_IND_ATTRIB
-- Purpose      : This function will retrieve all of the Item Indicator Attribute information
--                from ITEM_MASTER for a given item.
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_IND_ATTRIB(O_error_message            IN OUT VARCHAR2,
                             O_cost_zone_group_id       IN OUT ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE,
                             O_forecast_ind             IN OUT ITEM_MASTER.FORECAST_IND%TYPE,
                             O_merchandise_ind          IN OUT ITEM_MASTER.MERCHANDISE_IND%TYPE,
                             O_retail_label_type        IN OUT ITEM_MASTER.RETAIL_LABEL_TYPE%TYPE,
                             O_retail_label_value       IN OUT ITEM_MASTER.RETAIL_LABEL_VALUE%TYPE,
                             O_handling_temp            IN OUT ITEM_MASTER.HANDLING_TEMP%TYPE,
                             O_handling_sensitivity     IN OUT ITEM_MASTER.HANDLING_SENSITIVITY%TYPE,
                             O_catch_weight_ind         IN OUT ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                             O_waste_type               IN OUT ITEM_MASTER.WASTE_TYPE%TYPE,
                             O_waste_pct                IN OUT ITEM_MASTER.WASTE_PCT%TYPE,
                             O_default_waste_pct        IN OUT ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE,
                             O_package_size             IN OUT ITEM_MASTER.PACKAGE_SIZE%TYPE,
                             O_package_uom              IN OUT ITEM_MASTER.PACKAGE_UOM%TYPE,
                             O_check_uda_ind            IN OUT ITEM_MASTER.CHECK_UDA_IND%TYPE,
                             O_perishable_ind           IN OUT ITEM_MASTER.PERISHABLE_IND%TYPE,
                             O_comments                 IN OUT ITEM_MASTER.COMMENTS%TYPE,
                             O_brand_name               IN OUT ITEM_MASTER.BRAND_NAME%TYPE,
                             O_product_classification   IN OUT ITEM_MASTER.PRODUCT_CLASSIFICATION%TYPE,                             
                             I_item                     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_SIMPLE_PACK_IND - This function will return the simple pack_ind from ITEM_MASTER for
--                       the entered item.
---------------------------------------------------------------------------------------------
FUNCTION GET_SIMPLE_PACK_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_simple_pack_ind   IN OUT ITEM_MASTER.SIMPLE_PACK_IND%TYPE,
                             I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_ITEM_MASTER - This function will return a record containing every attribute from
--                   ITEM_MASTER for the entered item.  This function can be used for calling
--                   procedures that need just a few ITEM_MASTER attributes along with
--                   those calling procedures that need all of the attributes.
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_MASTER (O_error_message         OUT  VARCHAR2,
                          O_item_record           OUT  ITEM_MASTER%ROWTYPE,
                          I_item                  IN   ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- GET_TSF_FILL_SHIP_RECEIVED_QTY -- This function will return summaries of fill, transfer, shipped,
--                                   received and reconciled quantites for a transfter.
---------------------------------------------------------------------------------------------
FUNCTION GET_TSF_FILL_SHIP_RECEIVED_QTY (O_error_message           IN OUT VARCHAR2,
                                         O_fill_qty_suom           IN OUT TSFDETAIL.FILL_QTY%TYPE,
                                         O_tsf_qty_suom            IN OUT TSFDETAIL.TSF_QTY%TYPE,
                                         O_ship_qty_suom           IN OUT TSFDETAIL.SHIP_QTY%TYPE,
                                         O_received_qty            IN OUT TSFDETAIL.RECEIVED_QTY%TYPE,
                                         O_reconciled_qty          IN OUT TSFDETAIL.RECONCILED_QTY%TYPE,
                                         I_tsf_no                  IN     TSFDETAIL.TSF_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- GET_MERCH_HIER
--- Overloaded function which returns all merch levels: division, group_no,
--- dept, class, subclass
-------------------------------------------------------------------------------
FUNCTION GET_MERCH_HIER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_division       IN OUT  DIVISION.DIVISION%TYPE,
                        O_div_name       IN OUT  DIVISION.DIV_NAME%TYPE,
                        O_group_no       IN OUT  GROUPS.GROUP_NO%TYPE,
                        O_group_name     IN OUT  GROUPS.GROUP_NAME%TYPE,
                        O_dept           IN OUT  ITEM_MASTER.DEPT%TYPE,
                        O_dept_name      IN OUT  DEPS.DEPT_NAME%TYPE,
                        O_class          IN OUT  ITEM_MASTER.CLASS%TYPE,
                        O_class_name     IN OUT  CLASS.CLASS_NAME%TYPE,
                        O_subclass       IN OUT  ITEM_MASTER.SUBCLASS%TYPE,
                        O_sub_name       IN OUT  SUBCLASS.SUB_NAME%TYPE,
                        I_item           IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- GET_INFO
--- Overloaded function which returns all merch levels: division, group_no,
--- dept, class, subclass
-------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message         IN OUT  VARCHAR2,
                  O_item_desc             IN OUT  ITEM_MASTER.ITEM_DESC%TYPE,
                  O_item_level            IN OUT  ITEM_MASTER.ITEM_LEVEL%TYPE,
                  O_tran_level            IN OUT  ITEM_MASTER.TRAN_LEVEL%TYPE,
                  O_status                IN OUT  ITEM_MASTER.STATUS%TYPE,
                  O_pack_ind              IN OUT  ITEM_MASTER.PACK_IND%TYPE,
                  O_division              IN OUT  DIVISION.DIVISION%TYPE,
                  O_div_name              IN OUT  DIVISION.DIV_NAME%TYPE,
                  O_group_no              IN OUT  GROUPS.GROUP_NO%TYPE,
                  O_group_name            IN OUT  GROUPS.GROUP_NAME%TYPE,
                  O_dept                  IN OUT  ITEM_MASTER.DEPT%TYPE,
                  O_dept_name             IN OUT  DEPS.DEPT_NAME%TYPE,
                  O_class                 IN OUT  ITEM_MASTER.CLASS%TYPE,
                  O_class_name            IN OUT  CLASS.CLASS_NAME%TYPE,
                  O_subclass              IN OUT  ITEM_MASTER.SUBCLASS%TYPE,
                  O_subclass_name         IN OUT  SUBCLASS.SUB_NAME%TYPE,
                  O_sellable_ind          IN OUT  ITEM_MASTER.SELLABLE_IND%TYPE,
                  O_orderable_ind         IN OUT  ITEM_MASTER.ORDERABLE_IND%TYPE,
                  O_pack_type             IN OUT  ITEM_MASTER.PACK_TYPE%TYPE,
                  O_item_parent           IN OUT  ITEM_MASTER.ITEM_PARENT%TYPE,
                  O_item_number_type      IN OUT  ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                  I_item                  IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: GET_API_INFO
-- Purpose      : This function will retrieve all of the item information needed
--                for API processings from ITEM_MASTER for a given item.
-------------------------------------------------------------------------------
FUNCTION GET_API_INFO(O_error_message  IN OUT         VARCHAR2,
              O_api_item_rec      OUT NOCOPY    API_ITEM_REC,
                      I_item           IN             ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Function Name: ITEM_SUBQUERY
-- Purpose      : This function will build an item (sub)query based on the
--                input parameters for use with dynamic sql.  Using this
--          function will require knowing number of bind variables
--                and where they go in the query.
-------------------------------------------------------------------------------
FUNCTION ITEM_SUBQUERY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subquery          OUT VARCHAR2,
                       I_item_level     IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level     IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_diff_column    IN     VARCHAR2,
                       I_main_tab_item  IN     VARCHAR2,
                       I_item_bind_no   IN     VARCHAR,
                       I_diff_bind_no   IN     VARCHAR2,
                       I_include_parent IN     BOOLEAN)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ITEM_SUBQUERY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subquery          OUT VARCHAR2,
                       I_item_level     IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level     IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_diff_column    IN     VARCHAR2,
                       I_item_bind_no   IN     VARCHAR,
                       I_diff_bind_no   IN     VARCHAR2,
                       I_include_parent IN     BOOLEAN)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: PACK_SUBQUERY
-- Purpose      : This function will build a pack (sub)query for use with
--          dynamic sql.  This function will currently retrieve
--                orderable buyer packs and use an ITEM_TBL (database type)
--          to build query.
-------------------------------------------------------------------------------
FUNCTION PACK_SUBQUERY(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_subquery        OUT      VARCHAR2,
                       I_isc_exist       IN       BOOLEAN,
                       I_xpack_count     IN       NUMBER)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function Name: ITEM_DIFF_EXISTS
-- Purpose      : This function validates that the diff_id exists for a child item
--                and returns the diff_column name that holds the diff_id passed
--                into the function.
----------------------------------------------------------------------------
FUNCTION ITEM_DIFF_EXISTS(O_error_message   IN OUT   VARCHAR2,
                          O_exists          OUT      BOOLEAN,
                          O_diff_column     OUT      VARCHAR2,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE,
                          I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION GET_PACKS_FOR_ITEM(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                            O_pack_items      OUT    NOCOPY   ITEM_TBL,
                            I_item            IN              ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION BUILD_API_ITEM_TEMP(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                IN              ITEM_MASTER.ITEM%TYPE,
                             I_diff_id             IN              DIFF_IDS.DIFF_ID%TYPE,
                             I_tran_level          IN              ITEM_MASTER.TRAN_LEVEL%TYPE,
                             I_item_level          IN              ITEM_MASTER.ITEM_LEVEL%TYPE,
                             I_diff_column         IN              DIFF_IDS.DIFF_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------


-- Function Name: NEXT_EAN
-- Purpose      : This function will generate a new EAN13 number.
----------------------------------------------------------------------------
FUNCTION NEXT_EAN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_ean13           IN OUT   ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Function Name: DELETE_EAN
-- Purpose      : This function will delete all item children records from
--                the ITEM_SUPPLIER table of the parent item (of type EAN13)
--                passed in. It will also delete all rows from ITEM_MASTER
--                using the same criteria.
----------------------------------------------------------------------------
FUNCTION DELETE_EAN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_parent_item     IN       ITEM_MASTER.ITEM%TYPE,
                    I_supplier        IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_FIRST_EAN
-- Purpose:       This function will retrieve the lowest value of EAN13 Level 3 item from --                the item_master table for a parent item.
-- Created:       19-FEB-04 Lorelie C. Crisostomo
---------------------------------------------------------------------------------------------
FUNCTION GET_FIRST_EAN(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists             IN OUT   BOOLEAN,
                       O_item               IN OUT   ITEM_MASTER.ITEM%TYPE,
                       O_item_number_type   IN OUT   ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                       I_item_parent        IN       ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;


---------------------------------------------------------------------------------------------
-- Function Name: POPULATE_EAN_TABLE
-- Purpose:
-- Created:       24-FEB-04
---------------------------------------------------------------------------------------------
FUNCTION POPULATE_EAN_TABLE(O_error_message       IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                            O_item_master         IN OUT NOCOPY   GV_ean_table_t,
                            I_item_parent         IN              ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Purpose:       Function to retrieve all data for the post query of the
--                B_children block in module Itemchildrendiff.fmb
-- Created:       17-MAR-04
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEMCHILDRENDIFF_INFO(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_on_daily_purge     IN OUT  BOOLEAN,
                                   O_item_desc          IN OUT  ITEM_MASTER.ITEM_DESC%TYPE,
                                   O_unit_cost          IN OUT  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                   O_retail_price       IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                                   O_unit_cost_curr     IN OUT  SUPS.CURRENCY_CODE%TYPE,
                                   O_unit_retail_curr   IN OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                                   O_markup_percent     IN OUT  NUMBER,
                                   O_existing_vpn       IN OUT  ITEM_SUPPLIER.VPN%TYPE,
                                   O_diff_value         IN OUT  V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                                   O_ean                IN OUT  ITEM_MASTER.ITEM%TYPE,
                                   O_existing_ean_type  IN OUT  ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                                   O_supp_diff_1        IN OUT  ITEM_SUPPLIER.SUPP_DIFF_1%TYPE,
                                   O_supp_diff_2        IN OUT  ITEM_SUPPLIER.SUPP_DIFF_2%TYPE,
                                   O_supp_diff_3        IN OUT  ITEM_SUPPLIER.SUPP_DIFF_3%TYPE,
                                   O_supp_diff_4        IN OUT  ITEM_SUPPLIER.SUPP_DIFF_4%TYPE,
                                   I_item               IN      ITEM_MASTER.ITEM%TYPE,
                                   I_supplier           IN      SUPS.SUPPLIER%TYPE,
                                   I_retail_zone_id     IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE,
                                   I_dept               IN      DEPS.DEPT%TYPE,
                                   I_diff_id            IN      V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                                   I_elc_ind            IN      SYSTEM_OPTIONS.ELC_IND%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Purpose:       This function checks the contents items of a container item. If any of the
--                contents items are orderable then O_orderable is set to 'Y', otherwise it's
--                set to 'N'
-- Created:       15-JUN-04
---------------------------------------------------------------------------------------------
FUNCTION CONTENTS_ITEM_ORDERABLE(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_orderable         IN OUT  ITEM_MASTER.ORDERABLE_IND%TYPE,
                                 I_container_item    IN      ITEM_MASTER.CONTAINER_ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Purpose:       This function checks if the item is a container item. If item is a container
--                item, O_exists is set to 'Y' else it is set to 'N'.
-- Created:       29-JUL-04
---------------------------------------------------------------------------------------------
FUNCTION CONTENTS_ITEM_EXISTS (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists            IN OUT  VARCHAR2,
                               I_item              IN      ITEM_MASTER.CONTAINER_ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_CONTAINER_ITEM
-- Purpose:       This function will return the Deposit Container item for the given Content
--                item.
---------------------------------------------------------------------------------------------
FUNCTION GET_CONTAINER_ITEM(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_container_item   IN OUT   ITEM_MASTER.ITEM%TYPE,
                            I_item             IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: CHECK_DESC
-- Purpose:  Check if input field contains newline,semicolon,linefeed or pipe
--           character. If it does, this function returns TRUE.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DESC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_desc            IN       ITEM_MASTER.ITEM_DESC%TYPE,
                    O_exists             OUT   BOOLEAN)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_PRIMARY_REF_EAN
-- Purpose:       This function will retrieve the primary reference value of EAN13 Level 3
--                item from the item_master table for a parent item.
---------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_REF_EAN(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists             IN OUT   BOOLEAN,
                             O_item               IN OUT   ITEM_MASTER.ITEM%TYPE,
                             O_item_number_type   IN OUT   ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                             I_item_parent        IN       ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_ORDER_TYPE
-- Purpose:       This function will retrieve the order type from the
--                item_master table for a parent item.
---------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_TYPE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_order_type         IN OUT   ITEM_MASTER.ORDER_TYPE%TYPE,
                        I_item               IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHK_DIFF_AGGREGATE
-- Purpose:       This function will validate the diff aggregate entered for an item 
--                or dept or class or subclass and return diff desc aggregate if valid
---------------------------------------------------------------------------------------------
FUNCTION CHK_DIFF_AGGREGATE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_concatd_diff_desc  IN OUT VARCHAR2,
                            O_diff_1             IN OUT ITEM_MASTER.DIFF_1%TYPE,
                            O_diff_2             IN OUT ITEM_MASTER.DIFF_2%TYPE,
                            O_diff_3             IN OUT ITEM_MASTER.DIFF_3%TYPE,
                            O_diff_4             IN OUT ITEM_MASTER.DIFF_4%TYPE,
                            I_item               IN     ITEM_MASTER.ITEM%TYPE,
                            I_diff_aggr	         IN     VARCHAR2                            
                            )
  RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: GET_CATCH_WEIGHT_ATTRIB
-- Purpose:       This function will retrieve the catch weight attributes from item_master 
--                table for the passed in item.
---------------------------------------------------------------------------------------------
FUNCTION GET_CATCH_WEIGHT_ATTRIB(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_catch_weight_ind   IN OUT   ITEM_MASTER.CATCH_WEIGHT_IND%TYPE,
                                 I_item               IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION GET_CATCH_WEIGHT_UOM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_catch_weight_uom   IN OUT   ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE,
                              I_item               IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_STATUS_LOC
-- Purpose:       This function will check if the passed in item is in 'A'pproved status 
--                with ranging locations.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_STATUS_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   BOOLEAN,
                          I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name : GET_RECORD_COUNT_COST
-- Purpose       : This function will retrieve the record count from cost_change_temp
-------------------------------------------------------------------------------------------------------
FUNCTION GET_RECORD_COUNT_COST (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_count               IN OUT   NUMBER,
                                I_cost_change         IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                                I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_DIRECT_SHIP_IND
-- Purpose:       This function will retrieve the direct ship indicator for a given item and supplier.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_DIRECT_SHIP_IND(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_direct_ship_ind   IN OUT   ITEM_SUPPLIER.DIRECT_SHIP_IND%TYPE,
                             I_item              IN       ITEM_MASTER.ITEM%TYPE,
                             I_supplier          IN       SUPS.SUPPLIER%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_BRAND_DESC
-- Purpose:       This function will retrieve the brand description for the given brand name.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_BRAND_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_brand_desc        IN OUT   BRAND.BRAND_DESCRIPTION%TYPE,
                        I_brand_name        IN       BRAND.BRAND_NAME%TYPE)
   RETURN BOOLEAN;   
-------------------------------------------------------------------------------------------------------
FUNCTION GET_TRAN_CHILD_ITEMS(O_error_message  IN OUT  VARCHAR2,
                              O_item_tbl       IN OUT  ITEM_TBL,
                              I_item           IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: ITEM_CHILDREN
-- Purpose:       This function will retrieve the child items.
-------------------------------------------------------------------------------------------------------
FUNCTION ITEM_CHILDREN(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_item_table_t      OUT      GV_item_table_t,
                       I_item              IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;  
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_LEVEL_DESC
-- Purpose:       This function returns the translated item level description based on the pack
--                indicator, item_level and tran_level.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LEVEL_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_level_desc   IN OUT   CODE_DETAIL.CODE_DESC%TYPE,
                             I_pack_ind          IN       ITEM_MASTER.PACK_IND%TYPE,
                             I_item_level        IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                             I_tran_level        IN       ITEM_MASTER.TRAN_LEVEL%TYPE)
   RETURN BOOLEAN;  
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_LEVEL_DESC
-- Purpose:       This function returns the translated item level description based on the item's
--                item_level and tran_level and whether the item or its parent item is a pack or not.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LEVEL_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_item_level_desc   IN OUT   CODE_DETAIL.CODE_DESC%TYPE,
                             I_item              IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;  
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_TRAN_LEVEL_DESC
-- Purpose:       This function returns the translated transaction level description based on the pack
--                indicator and tran_level.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_TRAN_LEVEL_DESC(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tran_level_desc   IN OUT   CODE_DETAIL.CODE_DESC%TYPE,
                             I_pack_ind          IN       ITEM_MASTER.PACK_IND%TYPE,
                             I_tran_level        IN       ITEM_MASTER.TRAN_LEVEL%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_ITEM_MASTER_TL
-- Purpose:       This function deletes from the item_master_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_MASTER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_LEVEL_NAMES
-- Purpose:       Retrieves item level names
----------------------------------------------------------------------------------------------------------
FUNCTION GET_LEVEL_NAMES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_level_1_name      OUT  CODE_DETAIL.CODE_DESC%TYPE,
                         O_level_2_name      OUT  CODE_DETAIL.CODE_DESC%TYPE,
                         O_level_3_name      OUT  CODE_DETAIL.CODE_DESC%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END ITEM_ATTRIB_SQL;
/