CREATE OR REPLACE PACKAGE SUP_INV_MGMT_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------
-- DATA TYPE DECLARATION
--------------------------------------------------------

TYPE sup_inv_rectype IS RECORD
     (sup_dept_seq_no         SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE,
      loc_type                SUP_INV_MGMT.LOC_TYPE%TYPE,
      review_cycle            SUP_INV_MGMT.REVIEW_CYCLE%TYPE,
      repl_order_ctrl         SUP_INV_MGMT.repl_order_ctrl%TYPE,
      scale_cnstr_ind         SUP_INV_MGMT.SCALE_CNSTR_IND%TYPE,
      scale_cnstr_lvl         SUP_INV_MGMT.SCALE_CNSTR_LVL%TYPE,
      scale_cnstr_obj         SUP_INV_MGMT.SCALE_CNSTR_OBJ%TYPE,
      scale_cnstr_type1       SUP_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
      scale_cnstr_uom1        SUP_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
      scale_cnstr_curr1       SUP_INV_MGMT.SCALE_CNSTR_CURR1%TYPE,
      scale_cnstr_min_val1    SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
      scale_cnstr_max_val1    SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE,
      scale_cnstr_min_tol1    SUP_INV_MGMT.SCALE_CNSTR_MIN_TOL1%TYPE,
      scale_cnstr_max_tol1    SUP_INV_MGMT.SCALE_CNSTR_MAX_TOL1%TYPE,
      scale_cnstr_type2       SUP_INV_MGMT.SCALE_CNSTR_TYPE2%TYPE,
      scale_cnstr_uom2        SUP_INV_MGMT.SCALE_CNSTR_UOM2%TYPE,
      scale_cnstr_curr2       SUP_INV_MGMT.SCALE_CNSTR_CURR2%TYPE,
      scale_cnstr_min_val2    SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL2%TYPE,
      scale_cnstr_max_val2    SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL2%TYPE,
      scale_cnstr_min_tol2    SUP_INV_MGMT.SCALE_CNSTR_MIN_TOL2%TYPE,
      scale_cnstr_max_tol2    SUP_INV_MGMT.SCALE_CNSTR_MAX_TOL2%TYPE,
      min_cnstr_lvl           SUP_INV_MGMT.MIN_CNSTR_LVL%TYPE,
      min_cnstr_conj          SUP_INV_MGMT.MIN_CNSTR_CONJ%TYPE,
      min_cnstr_type1         SUP_INV_MGMT.MIN_CNSTR_TYPE1%TYPE,
      min_cnstr_uom1          SUP_INV_MGMT.MIN_CNSTR_UOM1%TYPE,
      min_cnstr_curr1         SUP_INV_MGMT.MIN_CNSTR_CURR1%TYPE,
      min_cnstr_val1          SUP_INV_MGMT.MIN_CNSTR_VAL1%TYPE,
      min_cnstr_type2         SUP_INV_MGMT.MIN_CNSTR_TYPE2%TYPE,
      min_cnstr_uom2          SUP_INV_MGMT.MIN_CNSTR_UOM2%TYPE,
      min_cnstr_curr2         SUP_INV_MGMT.MIN_CNSTR_CURR2%TYPE,
      min_cnstr_val2          SUP_INV_MGMT.MIN_CNSTR_VAL2%TYPE,
      truck_split_ind         SUP_INV_MGMT.TRUCK_SPLIT_IND%TYPE,
      truck_split_method      SUP_INV_MGMT.TRUCK_SPLIT_METHOD%TYPE,
      truck_cnstr_type1       SUP_INV_MGMT.TRUCK_CNSTR_TYPE1%TYPE,
      truck_cnstr_uom1        SUP_INV_MGMT.TRUCK_CNSTR_UOM1%TYPE,
      truck_cnstr_val1        SUP_INV_MGMT.TRUCK_CNSTR_VAL1%TYPE,
      truck_cnstr_tol1        SUP_INV_MGMT.TRUCK_CNSTR_TOL1%TYPE,
      truck_cnstr_type2       SUP_INV_MGMT.TRUCK_CNSTR_TYPE2%TYPE,
      truck_cnstr_uom2        SUP_INV_MGMT.TRUCK_CNSTR_UOM2%TYPE,
      truck_cnstr_val2        SUP_INV_MGMT.TRUCK_CNSTR_VAL2%TYPE,
      truck_cnstr_tol2        SUP_INV_MGMT.TRUCK_CNSTR_TOL2%TYPE,
      ltl_approval_ind        SUP_INV_MGMT.LTL_APPROVAL_IND%TYPE,
      due_ord_process_ind     SUP_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
      due_ord_lvl             SUP_INV_MGMT.DUE_ORD_LVL%TYPE,
      due_ord_serv_basis      SUP_INV_MGMT.DUE_ORD_SERV_BASIS%TYPE,
      non_due_ord_create_ind  SUP_INV_MGMT.NON_DUE_ORD_CREATE_IND%TYPE,
      mult_vehicle_ind        SUP_INV_MGMT.MULT_VEHICLE_IND%TYPE,
      single_loc_ind          SUP_INV_MGMT.SINGLE_LOC_IND%TYPE,
      round_lvl               SUP_INV_MGMT.ROUND_LVL%TYPE,
      round_to_inner_pct      SUP_INV_MGMT.ROUND_TO_INNER_PCT%TYPE,
      round_to_case_pct       SUP_INV_MGMT.ROUND_TO_CASE_PCT%TYPE,
      round_to_layer_pct      SUP_INV_MGMT.ROUND_TO_LAYER_PCT%TYPE,
      round_to_pallet_pct     SUP_INV_MGMT.ROUND_TO_PALLET_PCT%TYPE,
      ord_purge_ind           SUP_INV_MGMT.ORD_PURGE_IND%TYPE,
      pool_supplier           SUP_INV_MGMT.POOL_SUPPLIER%TYPE,
      pickup_loc              SUP_INV_MGMT.PICKUP_LOC%TYPE,
      purchase_type           SUP_INV_MGMT.PURCHASE_TYPE%TYPE,
      threshold_next_bracket  SUP_INV_MGMT.THRESHOLD_NEXT_BRACKET%TYPE,
      bracket_type1           SUP_INV_MGMT.BRACKET_TYPE1%TYPE,
      bracket_uom1            SUP_INV_MGMT.BRACKET_UOM1%TYPE,
      bracket_uom2            SUP_INV_MGMT.BRACKET_UOM2%TYPE,
      bracket_type2           SUP_INV_MGMT.BRACKET_TYPE2%TYPE,
      ib_ind                  SUP_INV_MGMT.IB_IND%TYPE,
      ib_order_ctrl           SUP_INV_MGMT.IB_ORDER_CTRL%TYPE);
---
ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error
---
TYPE sup_inv_mgmt_rec IS RECORD(sup_dept_seq_no          SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE,
                                supplier                 SUP_INV_MGMT.SUPPLIER%TYPE,
                                dept                     SUP_INV_MGMT.DEPT%TYPE,
                                location                 SUP_INV_MGMT.LOCATION%TYPE,
                                loc_type                 CODE_DETAIL.CODE%TYPE,
                                review_cycle             SUP_INV_MGMT.REVIEW_CYCLE%TYPE,
                                sunday_ind               VARCHAR2(1),
                                monday_ind               VARCHAR2(1),
                                tuesday_ind              VARCHAR2(1),
                                wednesday_ind            VARCHAR2(1),
                                thursday_ind             VARCHAR2(1),
                                friday_ind               VARCHAR2(1),
                                saturday_ind             VARCHAR2(1),
                                repl_order_ctrl          SUP_INV_MGMT.REPL_ORDER_CTRL%TYPE,
                                scale_cnstr_ind          SUP_INV_MGMT.SCALE_CNSTR_IND%TYPE,
                                scale_cnstr_lvl          SUP_INV_MGMT.SCALE_CNSTR_LVL%TYPE,
                                scale_cnstr_obj          SUP_INV_MGMT.SCALE_CNSTR_OBJ%TYPE,
                                scale_cnstr_type1        SUP_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                                scale_cnstr_uom1         SUP_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                                scale_cnstr_curr1        SUP_INV_MGMT.SCALE_CNSTR_CURR1%TYPE,
                                scale_cnstr_min_val1     SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
                                scale_cnstr_max_val1     SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE,
                                scale_cnstr_min_tol1     SUP_INV_MGMT.SCALE_CNSTR_MIN_TOL1%TYPE,
                                scale_cnstr_max_tol1     SUP_INV_MGMT.SCALE_CNSTR_MAX_TOL1%TYPE,
                                scale_cnstr_type2        SUP_INV_MGMT.SCALE_CNSTR_TYPE2%TYPE,
                                scale_cnstr_uom2         SUP_INV_MGMT.SCALE_CNSTR_UOM2%TYPE,
                                scale_cnstr_curr2        SUP_INV_MGMT.SCALE_CNSTR_CURR2%TYPE,
                                scale_cnstr_min_val2     SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL2%TYPE,
                                scale_cnstr_max_val2     SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL2%TYPE,
                                scale_cnstr_min_tol2     SUP_INV_MGMT.SCALE_CNSTR_MIN_TOL2%TYPE,
                                scale_cnstr_max_tol2     SUP_INV_MGMT.SCALE_CNSTR_MAX_TOL2%TYPE,
                                min_cnstr_lvl            SUP_INV_MGMT.MIN_CNSTR_LVL%TYPE,
                                min_cnstr_conj           SUP_INV_MGMT.MIN_CNSTR_CONJ%TYPE,
                                min_cnstr_type1          SUP_INV_MGMT.MIN_CNSTR_TYPE1%TYPE,
                                min_cnstr_uom1           SUP_INV_MGMT.MIN_CNSTR_UOM1%TYPE,
                                min_cnstr_curr1          SUP_INV_MGMT.MIN_CNSTR_CURR1%TYPE,
                                min_cnstr_val1           SUP_INV_MGMT.MIN_CNSTR_VAL1%TYPE,
                                min_cnstr_type2          SUP_INV_MGMT.MIN_CNSTR_TYPE2%TYPE,
                                min_cnstr_uom2           SUP_INV_MGMT.MIN_CNSTR_UOM2%TYPE,
                                min_cnstr_curr2          SUP_INV_MGMT.MIN_CNSTR_CURR2%TYPE,
                                min_cnstr_val2           SUP_INV_MGMT.MIN_CNSTR_VAL2%TYPE,
                                truck_split_ind          SUP_INV_MGMT.TRUCK_SPLIT_IND%TYPE,
                                truck_split_method       SUP_INV_MGMT.TRUCK_SPLIT_METHOD%TYPE,
                                truck_cnstr_type1        SUP_INV_MGMT.TRUCK_CNSTR_TYPE1%TYPE,
                                truck_cnstr_uom1         SUP_INV_MGMT.TRUCK_CNSTR_UOM1%TYPE,
                                truck_cnstr_val1         SUP_INV_MGMT.TRUCK_CNSTR_VAL1%TYPE,
                                truck_cnstr_tol1         SUP_INV_MGMT.TRUCK_CNSTR_TOL1%TYPE,
                                truck_cnstr_type2        SUP_INV_MGMT.TRUCK_CNSTR_TYPE2%TYPE,
                                truck_cnstr_uom2         SUP_INV_MGMT.TRUCK_CNSTR_UOM2%TYPE,
                                truck_cnstr_val2         SUP_INV_MGMT.TRUCK_CNSTR_VAL2%TYPE,
                                truck_cnstr_tol2         SUP_INV_MGMT.TRUCK_CNSTR_TOL2%TYPE,
                                ltl_approval_ind         SUP_INV_MGMT.LTL_APPROVAL_IND%TYPE,
                                due_ord_process_ind      SUP_INV_MGMT.DUE_ORD_PROCESS_IND%TYPE,
                                due_ord_lvl              SUP_INV_MGMT.DUE_ORD_LVL%TYPE,
                                due_ord_serv_basis       SUP_INV_MGMT.DUE_ORD_SERV_BASIS%TYPE,
                                non_due_ord_create_ind   SUP_INV_MGMT.NON_DUE_ORD_CREATE_IND%TYPE,
                                mult_vehicle_ind         SUP_INV_MGMT.MULT_VEHICLE_IND%TYPE,
                                single_loc_ind           SUP_INV_MGMT.SINGLE_LOC_IND%TYPE,
                                round_lvl                SUP_INV_MGMT.ROUND_LVL%TYPE,
                                round_to_inner_pct       SUP_INV_MGMT.ROUND_TO_INNER_PCT%TYPE,
                                round_to_case_pct        SUP_INV_MGMT.ROUND_TO_CASE_PCT%TYPE,
                                round_to_layer_pct       SUP_INV_MGMT.ROUND_TO_LAYER_PCT%TYPE,
                                round_to_pallet_pct      SUP_INV_MGMT.ROUND_TO_PALLET_PCT%TYPE,
                                ord_purge_ind            SUP_INV_MGMT.ORD_PURGE_IND%TYPE,
                                pool_supplier            SUP_INV_MGMT.POOL_SUPPLIER%TYPE,
                                pickup_loc               SUP_INV_MGMT.PICKUP_LOC%TYPE,
                                purchase_type            SUP_INV_MGMT.PURCHASE_TYPE%TYPE,
                                threshold_next_bracket   SUP_INV_MGMT.THRESHOLD_NEXT_BRACKET%TYPE,
                                bracket_type1            SUP_INV_MGMT.BRACKET_TYPE1%TYPE,
                                bracket_uom1             SUP_INV_MGMT.BRACKET_UOM1%TYPE,
                                bracket_type2            SUP_INV_MGMT.BRACKET_TYPE2%TYPE,
                                bracket_uom2             SUP_INV_MGMT.BRACKET_UOM2%TYPE,
                                ib_ind                   SUP_INV_MGMT.IB_IND%TYPE,
                                ib_order_ctrl            SUP_INV_MGMT.IB_ORDER_CTRL%TYPE,
                                error_message            RTK_ERRORS.RTK_TEXT%TYPE,
                                return_code              VARCHAR2(5));

TYPE sup_inv_mgmt_tbl IS TABLE OF sup_inv_mgmt_rec INDEX BY BINARY_INTEGER;
---

---------------------------------------------------------------------------------------------
--Function Name: GET_INV_MGMT_DATA
--Purpose      : This Function will return all currently existing columns on
--               the sup_inv_mgmt table. The query should default from the next
--               highest level if the location parameter is passed in NULL or
--               if there's no record for the passed in location.
--               The same defaulting logic should apply to department.
---------------------------------------------------------------------------------------------
FUNCTION GET_INV_MGMT_DATA(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_sup_inv_rec       IN OUT SUP_INV_RECTYPE,
                           I_supplier          IN     SUP_INV_MGMT.SUPPLIER%TYPE,
                           I_dept              IN     SUP_INV_MGMT.DEPT%TYPE,
                           I_location          IN     SUP_INV_MGMT.LOCATION%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: SUPP_REPL_INFO
--Purpose      : To retrieve the review cycle, replenishment order control and sup dept seq no from
--               the sup_inv_mgmt table.
---------------------------------------------------------------------------------------------
FUNCTION SUPP_REPL_INFO(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_review_cycle      IN OUT SUP_INV_MGMT.REVIEW_CYCLE%TYPE,
                        O_repl_order_ctrl   IN OUT SUP_INV_MGMT.REPL_ORDER_CTRL%TYPE,
                        O_sup_dept_seq_no   IN OUT SUP_INV_MGMT.SUP_DEPT_SEQ_NO%TYPE,
                        I_supplier          IN     SUP_INV_MGMT.SUPPLIER%TYPE,
                        I_dept              IN     SUP_INV_MGMT.DEPT%TYPE,
                        I_location          IN     SUP_INV_MGMT.LOCATION%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------
--- Function:  DEL_INV_MGMT
--- Purpose:   Deletes all records from the SUPS_INV_MGMT
---            table for a particular supplier.
---------------------------------------------------------------------
FUNCTION DEL_INV_MGMT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE)
         RETURN BOOLEAN;
---------------------------------------------------------------------
--- Function:  INV_MGMT_EXIST
--- Purpose:   Checks for the existence of supplier/dept/loc level
---            inventory management variables.
---------------------------------------------------------------------
FUNCTION INV_MGMT_EXIST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exist         IN OUT BOOLEAN,
                         I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE,
                         I_dept          IN     SUP_INV_MGMT.DEPT%TYPE,
                         I_loc           IN     SUP_INV_MGMT.LOCATION%TYPE)
         RETURN BOOLEAN;

---------------------------------------------------------------------
--- Function:  SUP_INV_MGMT_EXIST
--- Purpose:   Checks for the existence of supplier inventory management records.
---------------------------------------------------------------------
FUNCTION SUP_INV_MGMT_EXIST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist         IN OUT BOOLEAN,
                             I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE)
         RETURN BOOLEAN;
---------------------------------------------------------------------
--- Function:  GET_INV_MGMT_LEVEL
--- Purpose:   Retrieves the inventory management level for the supplier.
---------------------------------------------------------------------
FUNCTION GET_INV_MGMT_LEVEL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_inv_mgmt_lvl   IN OUT  SUPS.INV_MGMT_LVL%TYPE,
                            I_supplier       IN      SUPS.SUPPLIER%TYPE)
        RETURN BOOLEAN;
---------------------------------------------------------------------
--- Function:  GET_POOL_SUPPLIER
--- Purpose:   This function will return the pool supplier from the sup_inv_mgmt table
---            for the supplier, deparment and/or location entered.
---------------------------------------------------------------------------------------------
FUNCTION GET_POOL_SUPPLIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_pool_supplier   IN OUT   SUP_INV_MGMT.POOL_SUPPLIER%TYPE,
                           I_supplier        IN       SUP_INV_MGMT.SUPPLIER%TYPE,
                           I_dept            IN       SUP_INV_MGMT.DEPT%TYPE,
                           I_location        IN       SUP_INV_MGMT.LOCATION%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function:  GET_SCALING_CNSTR_INFO
--- Purpose:   This function will return scaling constraint information from the sup_inv_mgmt table
---            for the supplier, deparment and/or location entered.
---------------------------------------------------------------------------------------------
FUNCTION GET_SCALING_CNSTR_INFO(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_scale_cnstr_ind        IN OUT   SUP_INV_MGMT.SCALE_CNSTR_IND%TYPE,
                                O_scale_cnstr_lvl        IN OUT   SUP_INV_MGMT.SCALE_CNSTR_LVL%TYPE,
                                O_scale_cnstr_obj        IN OUT   SUP_INV_MGMT.SCALE_CNSTR_OBJ%TYPE,
                                O_scale_cnstr_type1      IN OUT   SUP_INV_MGMT.SCALE_CNSTR_TYPE1%TYPE,
                                O_scale_cnstr_uom1       IN OUT   SUP_INV_MGMT.SCALE_CNSTR_UOM1%TYPE,
                                O_scale_cnstr_curr1      IN OUT   SUP_INV_MGMT.SCALE_CNSTR_CURR1%TYPE,
                                O_scale_cnstr_min_val1   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL1%TYPE,
                                O_scale_cnstr_max_val1   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL1%TYPE,
                                O_scale_cnstr_min_tol1   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_TOL1%TYPE,
                                O_scale_cnstr_max_tol1   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_TOL1%TYPE,
                                O_scale_cnstr_type2      IN OUT   SUP_INV_MGMT.SCALE_CNSTR_TYPE2%TYPE,
                                O_scale_cnstr_uom2       IN OUT   SUP_INV_MGMT.SCALE_CNSTR_UOM2%TYPE,
                                O_scale_cnstr_curr2      IN OUT   SUP_INV_MGMT.SCALE_CNSTR_CURR2%TYPE,
                                O_scale_cnstr_min_val2   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_VAL2%TYPE,
                                O_scale_cnstr_max_val2   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_VAL2%TYPE,
                                O_scale_cnstr_min_tol2   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MIN_TOL2%TYPE,
                                O_scale_cnstr_max_tol2   IN OUT   SUP_INV_MGMT.SCALE_CNSTR_MAX_TOL2%TYPE,
                                I_supplier               IN       SUP_INV_MGMT.SUPPLIER%TYPE,
                                I_dept                   IN       SUP_INV_MGMT.DEPT%TYPE,
                                I_location               IN       SUP_INV_MGMT.LOCATION%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function:  GET_PURCHASE_PICKUP
--- Purpose:   Retrieves the purchase type and pick-up location for the
---            passed in supplier, department, location.
---------------------------------------------------------------------------------------------
FUNCTION GET_PURCHASE_PICKUP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_purchase_type IN OUT SUP_INV_MGMT.PURCHASE_TYPE%TYPE,
                             O_pickup_loc    IN OUT SUP_INV_MGMT.PICKUP_LOC%TYPE,
                             I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE,
                             I_dept          IN     SUP_INV_MGMT.DEPT%TYPE,
                             I_location      IN     SUP_INV_MGMT.LOCATION%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function:  CHECK_BRACKET_INFO
--- Purpose:   Checks to see if any bracket info exists in Sup_Inv_Mgmt table for
---            the passed in supplier.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_BRACKET_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function:  CLEAR_BRACKET_INFO
--- Purpose:   Clears bracket columns in all records for the passed in supplier.
---------------------------------------------------------------------------------------------
FUNCTION CLEAR_BRACKET_INFO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_supplier      IN     SUP_INV_MGMT.SUPPLIER%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--- Function:  QUERY_PROCEDURE
--- Purpose:   Forms the source for SUPINMGMT form
---------------------------------------------------------------------------------------------
PROCEDURE QUERY_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL,
                          I_supplier            IN       SUP_INV_MGMT.SUPPLIER%TYPE,
                          I_dept                IN       SUP_INV_MGMT.DEPT%TYPE,
                          I_location            IN       SUP_INV_MGMT.LOCATION%TYPE,
                          I_loc_type            IN       SUP_INV_MGMT.LOC_TYPE%TYPE,
                          I_default_from        IN       VARCHAR2);
---------------------------------------------------------------------------------------------
--- Function:  INSERT_PROCEDURE
--- Purpose:   Inserts records from SUPINMGMT form into Sup_inv_mgmt table.
---------------------------------------------------------------------------------------------
PROCEDURE INSERT_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL);
---------------------------------------------------------------------------------------------
--- Function:  DELETE_PROCEDURE
--- Purpose:   Deletes records from SUPINMGMT form in Sup_inv_mgmt table.
---------------------------------------------------------------------------------------------
PROCEDURE DELETE_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL);
---------------------------------------------------------------------------------------------
--- Function:  LOCK_PROCEDURE
--- Purpose:   Locks records from SUPINMGMT form in Sup_inv_mgmt table.
---------------------------------------------------------------------------------------------
PROCEDURE LOCK_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL);
---------------------------------------------------------------------------------------------
--- Function:  UPDATE_PROCEDURE
--- Purpose:   Updates records from SUPINMGMT form into Sup_inv_mgmt table.
---------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE(IO_sup_inv_mgmt_tbl   IN OUT   SUP_INV_MGMT_TBL);
---------------------------------------------------------------------------------------------
--- Function : INSERT_PROCEDURE_WRP
--- Purpose  : This is a wrapper function for INSERT_PROCEDURE that passes a record of 
---            database object type instead of a PL/SQL type to be called from Java wrappers. 
---------------------------------------------------------------------------------------------
PROCEDURE INSERT_PROCEDURE_WRP(IO_sup_inv_mgmt_tbl   IN OUT   WRP_SUP_INV_MGMT_TBL);
---------------------------------------------------------------------------------------------
--- Function : UPDATE_PROCEDURE_WRP
--- Purpose  : This is a wrapper function for UPDATE_PROCEDURE that passes a record of 
---            database object type instead of a PL/SQL type to be called from Java wrappers. 
---------------------------------------------------------------------------------------------
PROCEDURE UPDATE_PROCEDURE_WRP(IO_sup_inv_mgmt_tbl   IN OUT   WRP_SUP_INV_MGMT_TBL);
---------------------------------------------------------------------------------------------
END;
/
