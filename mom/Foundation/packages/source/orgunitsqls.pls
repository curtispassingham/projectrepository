
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORG_UNIT_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_desc                  IN OUT ORG_UNIT.DESCRIPTION%TYPE,
                  I_org_unit_id           IN     ORG_UNIT.ORG_UNIT_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CHECK_DUP_ORGUNIT(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid                 IN OUT BOOLEAN,
                           I_org_unit_id           IN     ORG_UNIT.ORG_UNIT_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_LOC_VENDOR_SITE_ASSOC(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_oracle_vendor_site_id  IN OUT ORG_UNIT_ADDR_SITE.ORACLE_VENDOR_SITE_ID%TYPE,
                                   I_supplier               IN     VARCHAR2,
                                   I_location               IN     ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_ORG_UNIT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_org_unit_record     IN OUT ORG_UNIT%ROWTYPE,
                      I_org_unit            IN     ORG_UNIT.ORG_UNIT_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CHECK_ORG_UNIT_ASSOC_EXISTS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists          IN OUT  BOOLEAN,
                                     I_org_unit_id     IN      ORG_UNIT.ORG_UNIT_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
END ORG_UNIT_SQL;
/
