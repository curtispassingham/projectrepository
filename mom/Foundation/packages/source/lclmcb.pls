



CREATE OR REPLACE PACKAGE BODY LOCLIST_STORE_MC_SQL AS
---------------------------------------------------------------------------------------------
FUNCTION CHANGE_ATTRIBUTE (O_error_message        IN OUT   VARCHAR2,
                           O_reject_report        IN OUT   VARCHAR2,
                           I_loc_list             IN       loc_list_head.loc_list%TYPE,
                           I_vat_region_cb        IN       VARCHAR2,
                           I_vat_region           IN       vat_region.vat_region%TYPE,
                           I_district_cb          IN       VARCHAR2,
                           I_district             IN       district.district%TYPE,
                           I_tsf_zone_cb          IN       VARCHAR2,
                           I_tsf_zone             IN       tsfzone.transfer_zone%TYPE,
                           I_store_format_cb      IN       VARCHAR2,
                           I_store_format         IN       store_format.store_format%TYPE,
                           I_mall_name_cb         IN       VARCHAR2,
                           I_mall_name            IN       store.mall_name%TYPE,
                           I_default_wh_cb        IN       VARCHAR2,
                           I_default_wh           IN       wh.wh%TYPE,
                           I_lang_cb              IN       VARCHAR2,
                           I_lang                 IN       lang.lang%TYPE,
                           I_store_class_cb       IN       VARCHAR2,
                           I_store_class          IN       store.store_class%TYPE,
                           I_total_square_ft_cb   IN       VARCHAR2,
                           I_total_square_ft      IN       store.total_square_ft%TYPE,
                           I_selling_square_ft_cb IN       VARCHAR2,
                           I_selling_square_ft    IN       store.selling_square_ft%TYPE,
                           I_store_open_date_cb   IN       VARCHAR2,
                           I_store_open_date      IN       store.store_open_date%TYPE,
                           I_store_close_date_cb  IN       VARCHAR2,
                           I_store_close_date     IN       store.store_close_date%TYPE,
                           I_acquired_date_cb     IN       VARCHAR2,
                           I_acquired_date        IN       store.acquired_date%TYPE,
                           I_user_id              IN       USER_USERS.USERNAME%TYPE,
                           I_auto_rcv_cb          IN       VARCHAR2,
                           I_auto_rcv             IN       STORE.AUTO_RCV%TYPE,
                           I_customer_ord_loc_cb  IN       VARCHAR2,
                           I_customer_ord_loc_ind IN       STORE.customer_order_loc_ind%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED     EXCEPTION;

   L_program            tran_data.pgm_name%TYPE := 'LOCLIST_STORE_MC_SQL.CHANGE_ATTRIBUTE';
   L_table              VARCHAR2(20) := 'STORE';
   L_store              store.store%TYPE;
   L_store_exists       BOOLEAN;
   L_open_date          store.store_open_date%TYPE;
   L_close_date         store.store_close_date%TYPE;
   L_store_type         store.store_type%TYPE; 

   cursor C_STORE is
      select lld.location,
             s.store_open_date,
             s.store_close_date,
             s.store_type
        from store s,
             loc_list_detail lld
       where s.store = lld.location
         and lld.loc_list = I_loc_list
         and lld.loc_type = 'S';

   cursor C_LOCK_STORE is
      select 'x'
        from store
       where store = L_store
         for update nowait;

BEGIN

   -- Ascertain that there are some stores on the loc_list
   if not LOCLIST_ATTRIBUTE_SQL.STORE_EXISTS(O_error_message,
                                             L_store_exists,
                                             I_loc_list) then
      return FALSE;
   end if;

   -- if not, display appropriate error message
   if L_store_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('LL_NO_STORES',
                                            NULL,
                                            NULL,
                                            NULL);

      return FALSE;
   end if;
   ---
   O_reject_report := 'FALSE';

   -- Loop through stores in location list, check to see
   -- if an invalid change is being requested, lock STORE table and
   -- update STORE attributes where update cb = 'Y'

   FOR rec in C_STORE LOOP

      L_store := rec.location;
      L_open_date := rec.store_open_date;
      L_close_date := rec.store_close_date;
      L_store_type :=rec.store_type;

      if I_store_close_date is NOT NULL then
         if L_open_date > I_store_close_date then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      NULL,
                                                      'S',
                                                      L_store,
                                                      'L',
                                                      'INVALID_STORE_CLOSE_DATE',
                                                      I_user_id,
                                                      I_store_close_date,
                                                      L_open_date,
                                                      NULL) = FALSE then
               return FALSE;
            end if;
            O_reject_report := 'TRUE';
            return TRUE;
         end if;
      end if;
      ---
      if L_close_date is NOT NULL then
         if L_close_date < I_store_open_date then
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      NULL,
                                                      'S',
                                                      L_store,
                                                      'L',
                                                      'INVALID_STORE_OPEN_DATE',
                                                      I_user_id,
                                                      I_store_open_date,
                                                      L_close_date,
                                                      NULL) = FALSE then
               return FALSE;
            end if;
            O_reject_report := 'TRUE';
            return TRUE;
         end if;
      end if;
      ---
      if O_reject_report = 'FALSE' then
         SQL_LIB.SET_MARK('UPDATE', NULL, 'store', 'store: '||to_char(L_store));
         update store
            set vat_region = DECODE(I_vat_region_cb, 'Y', I_vat_region, vat_region),
                  district = DECODE(I_district_cb, 'Y', I_district, district),
             transfer_zone = DECODE(I_tsf_zone_cb, 'Y', I_tsf_zone, transfer_zone),
              store_format = DECODE(I_store_format_cb, 'Y', I_store_format, store_format),
                 mall_name = DECODE(I_mall_name_cb, 'Y', I_mall_name, mall_name),
                default_wh = DECODE(I_default_wh_cb, 'Y', I_default_wh, default_wh),
                      lang = DECODE(I_lang_cb, 'Y', I_lang, lang),
               store_class = DECODE(I_store_class_cb, 'Y',DECODE(L_store_type, 'C',I_store_class,store_class), store_class),
           total_square_ft = DECODE(I_total_square_ft_cb, 'Y', I_total_square_ft, total_square_ft),
         selling_square_ft = DECODE(I_selling_square_ft_cb, 'Y', I_selling_square_ft, selling_square_ft),
           store_open_date = DECODE(I_store_open_date_cb, 'Y', I_store_open_date, store_open_date),
          store_close_date = DECODE(I_store_close_date_cb, 'Y', I_store_close_date, store_close_date),
             acquired_date = DECODE(I_acquired_date_cb, 'Y', I_acquired_date, acquired_date),
                  auto_rcv = DECODE(I_auto_rcv_cb,'Y',I_auto_rcv,auto_rcv),
    customer_order_loc_ind = DECODE(I_customer_ord_loc_cb,'Y',DECODE(L_store_type,'C',I_customer_ord_loc_ind,customer_order_loc_ind),customer_order_loc_ind )        
          where store = L_store;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                             L_table,
                                                             to_char(L_store),
                                                             NULL);

   when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
END CHANGE_ATTRIBUTE;
---------------------------------------------------------------------------------------------
FUNCTION CHANGE_LOC_TRAIT (O_error_message        IN OUT   VARCHAR2,
                           I_loc_list             IN       loc_list_head.loc_list%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED     EXCEPTION;

   L_matrix_rec_exists     VARCHAR2(1) := NULL;
   L_program               tran_data.pgm_name%TYPE := 'LOCLIST_STORE_MC_SQL.CHANGE_LOC_TRAIT';
   L_table                 VARCHAR2(20);
   L_store                 store.store%TYPE;
   L_delete_ind            VARCHAR2(1) := NULL;
   L_loc_trait             mc_loc_trait_temp.loc_trait%TYPE;

   cursor C_STORE is
      select location
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'S';

   cursor C_LOCK_LOC_TRAITS_MATRIX is
      select 'x'
        from loc_traits_matrix
       where store = L_store
         and loc_trait in ( select loc_trait
                              from mc_loc_trait_temp
                             where loc_list = I_loc_list
                               and action_type = 'D');

   cursor C_LOCK_MC_LOC_TRAIT_TEMP is
      select 'x'
        from mc_loc_trait_temp
       where loc_list = I_loc_list
         for update nowait;

BEGIN

   -- Loop through store on location list
   FOR store_rec in C_STORE LOOP
      L_store := store_rec.location;
      ---
      -- insert into loc_traits_matrix if action_type 'A'
      insert into loc_traits_matrix (loc_trait,
                                     store)
                              select loc_trait,
                                     L_store
                                from mc_loc_trait_temp mltt
                               where loc_list = I_loc_list
                                 and action_type = 'A'
                                 and NOT EXISTS (select 'x'
                                                   from loc_traits_matrix
                                                  where store = L_store
                                                    and loc_trait = mltt.loc_trait);
      -- delete if action_type = 'D'
      L_table := 'LOC_TRAITS_MATRIX';
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_LOC_TRAITS_MATRIX', 'loc_traits_matrix', 'store: '||to_char(L_store));
      open C_LOCK_LOC_TRAITS_MATRIX;
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_LOC_TRAITS_MATRIX', 'loc_traits_matrix', 'store: '||to_char(L_store));
      close C_LOCK_LOC_TRAITS_MATRIX;

      delete from loc_traits_matrix ltm
            where store = L_store
              and EXISTS (select loc_trait
                            from mc_loc_trait_temp
                           where loc_list = I_loc_list
                             and action_type = 'D'
                             and loc_trait = ltm.loc_trait);

   END LOOP;  -- end of store_rec loop

   L_table := 'MC_LOC_TRAIT_TEMP';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_MC_LOC_TRAIT_TEMP', 'mc_loc_trait_temp', 'loc_list: '||to_char(I_loc_list));
   open C_LOCK_MC_LOC_TRAIT_TEMP;
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_MC_LOC_TRAIT_TEMP', 'mc_loc_trait_temp', 'loc_list: '||to_char(I_loc_list));
   close C_LOCK_MC_LOC_TRAIT_TEMP;

   delete from mc_loc_trait_temp
         where loc_list = I_loc_list;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                             L_table,
                                                             to_char(I_loc_list),
                                                             to_char(L_store));
      RETURN FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END CHANGE_LOC_TRAIT;
-------------------------------------------------------------------


-------------------------------------------------------------------------------------------
FUNCTION DIFFERENT_COST_CURRENCY(O_error_message   IN OUT   VARCHAR2,
                                 O_exists          IN OUT   BOOLEAN,
                                 O_location_list   IN OUT   VARCHAR2,
                                 I_loc_list        IN       LOC_LIST_HEAD.LOC_LIST%TYPE,
                                 I_zone_group_id   IN       COST_ZONE.ZONE_GROUP_ID%TYPE,
                                 I_new_zone_id     IN       COST_ZONE.ZONE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         TRAN_DATA.PGM_NAME%TYPE := 'LOCLIST_STORE_MC_SQL.DIFFERENT_COST_CURRENCY';
   L_locs            VARCHAR2(400) := NULL;
   L_currency_code   COST_ZONE.CURRENCY_CODE%TYPE;

   cursor C_CURRENCY_CODE is
      select currency_code
        from cost_zone
       where zone_group_id = I_zone_group_id
         and zone_id = I_new_zone_id;

   cursor C_EXCLUDE_LOCS is
      select d.location
        from loc_list_detail d,
             store s
       where d.loc_list = I_loc_list
         and d.loc_type = 'S'
         and d.location = s.store
         and s.currency_code != L_currency_code
        union all
      select d.location
        from loc_list_detail d,
             wh
       where d.loc_list = I_loc_list
         and d.loc_type = 'W'
         and d.location = wh.wh
         and wh.currency_code != L_currency_code;

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CURRENCY_CODE',
                    'cost_zone',
                    'zone_group_id: '||to_char(I_zone_group_id)||
                    ', zone_id: '||to_char(I_new_zone_id));
   open C_CURRENCY_CODE;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CURRENCY_CODE',
                    'cost_zone',
                    'zone_group_id: '||to_char(I_zone_group_id)||
                    ', zone_id: '||to_char(I_new_zone_id));
   fetch C_CURRENCY_CODE into L_currency_code;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CURRENCY_CODE',
                    'cost_zone',
                    'zone_group_id: '||to_char(I_zone_group_id)||
                    ', zone_id: '||to_char(I_new_zone_id));
   close C_CURRENCY_CODE;
   ---
   -- string together a list of all the locations
   -- that are in a different currency code
   FOR locs_rec in C_EXCLUDE_LOCS LOOP
      L_locs := L_locs || locs_rec.location||', ';
      ---
      O_exists := TRUE;
   END LOOP;

   if O_exists = TRUE then
      -- remove final comma and pass out of function
      L_locs := SUBSTR(L_locs, 1, NVL(LENGTH(L_locs), 0)-2);
      O_location_list := L_locs;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;

END DIFFERENT_COST_CURRENCY;
--------------------------------------------------------------------------------------------
FUNCTION UPDATE_COST_ZONE_GROUP_LOC(O_error_message   IN OUT   VARCHAR2,
                                    I_loc_list        IN       LOC_LIST_HEAD.LOC_LIST%TYPE,
                                    I_zone_group_id   IN       COST_ZONE.ZONE_GROUP_ID%TYPE,
                                    I_new_zone_id     IN       COST_ZONE.ZONE_ID%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED    EXCEPTION;

   L_program                 TRAN_DATA.PGM_NAME%TYPE := 'LOCLIST_STORE_MC_SQL.UPDATE_COST_ZONE_GROUP_LOC';
   L_location                STORE.STORE%TYPE;
   L_currency_code           COST_ZONE.CURRENCY_CODE%TYPE;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_old_zone_id             COST_ZONE.ZONE_ID%TYPE;
   L_table                   VARCHAR2(30);

   cursor C_CURRENCY_CODE is
      select currency_code
        from cost_zone
       where zone_group_id = I_zone_group_id
         and zone_id = I_new_zone_id;

   cursor C_LOCATIONS is
      select d.location
        from loc_list_detail d,
             store s
       where d.loc_list = I_loc_list
         and d.loc_type = 'S'
         and d.location = s.store
         and s.currency_code = L_currency_code
       union all
      select d.location
        from loc_list_detail d,
             wh
       where d.loc_list = I_loc_list
         and d.loc_type = 'W'
         and d.location = wh.wh
         and wh.currency_code = L_currency_code
       union all
       select w.wh
         from wh w
        where exists (select 'x'
                        from loc_list_detail ld,
                             wh 
                       where ld.loc_list =I_loc_list
                         and ld.loc_type = 'W'
                         and ld.location = wh.wh
                         and wh.wh != wh.physical_wh
                         and wh.currency_code = L_currency_code
                         and w.physical_wh = wh.physical_wh)
          and not exists (select 'x'
                            from loc_list_detail ll
                           where ll.loc_list = I_loc_list
                             and ll.location = w.wh
                             and ll.loc_type = 'W')
          and w.physical_wh != w.wh;

   cursor C_LOCK_CZGL is
      select zone_id
        from cost_zone_group_loc czgl
       where czgl.zone_group_id = I_zone_group_id
         and czgl.location = L_location
         for update of zone_id nowait;

BEGIN

   -- fetch the currency_code for new cost_zone zone_id
   SQL_LIB.SET_MARK('OPEN',
                    'C_CURRENCY_CODE',
                    'cost_zone',
                    'zone_group_id: '||to_char(I_zone_group_id)||
                    ', zone_id: '||to_char(I_new_zone_id));
   open C_CURRENCY_CODE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CURRENCY_CODE',
                    'cost_zone',
                    'zone_group_id: '||to_char(I_zone_group_id)||
                    ', zone_id: '||to_char(I_new_zone_id));
   fetch C_CURRENCY_CODE into L_currency_code;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CURRENCY_CODE',
                    'cost_zone',
                    'zone_group_id: '||to_char(I_zone_group_id)||
                    ', zone_id: '||to_char(I_new_zone_id));
   close C_CURRENCY_CODE;
   ---

   FOR locs_rec in C_LOCATIONS LOOP
      L_location := locs_rec.location;
      ---
      L_table := 'COST_ZONE_GROUP_LOC';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_CZGL',
                       'cost_zone_group_loc',
                       'location: '||to_char(L_location)||
                       ', zone_group_id: '||to_char(I_zone_group_id));
      open C_LOCK_CZGL;
      
      SQL_LIB.SET_MARK('FETCH',
                       'C_LOCK_CZGL',
                       'cost_zone_group_loc',
                       'location: '||to_char(L_location)||
                       ', zone_group_id: '||to_char(I_zone_group_id));
      
      fetch C_LOCK_CZGL into L_old_zone_id;     

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_CZGL',
                       'cost_zone_group_loc',
                       'location: '||to_char(L_location)||
                       ', zone_group_id: '||to_char(I_zone_group_id));
      close C_LOCK_CZGL;
      ---
      update cost_zone_group_loc
         set zone_id = I_new_zone_id
       where zone_group_id = I_zone_group_id
         and location = L_location;
         
      if FUTURE_COST_EVENT_SQL.ADD_COST_ZONE_LOC_MOVE (O_error_message,
                                                       L_cost_event_process_id,
                                                       L_location,
                                                       I_zone_group_id,
                                                       L_old_zone_id,
                                                       I_new_zone_id,
                                                       USER) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                             L_table,
                                                             to_char(I_loc_list),
                                                             to_char(L_location));
      RETURN FALSE;

   when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
        return FALSE;

END UPDATE_COST_ZONE_GROUP_LOC;
---------------------------------------------------------------------------------------
END;
/



