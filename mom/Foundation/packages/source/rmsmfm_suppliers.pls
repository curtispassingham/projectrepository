CREATE OR REPLACE PACKAGE RMSMFM_SUPPLIER AUTHID CURRENT_USER AS

--------------------------------------------------------
-- DOC TYPE PARAMETERS
--------------------------------------------------------
VEND_DESC_MSG        CONSTANT VARCHAR2(15) := 'VendorDesc';
VEND_HDR_DESC_MSG    CONSTANT VARCHAR2(15) := 'VendorHdrDesc';
VEND_REF_MSG         CONSTANT VARCHAR2(15) := 'VendorRef';
VEND_ADDR_DESC_MSG   CONSTANT VARCHAR2(15) := 'VendorAddrDesc';
VEND_ADDR_REF_MSG    CONSTANT VARCHAR2(15) := 'VendorAddrRef';
VEND_OU_DESC_MSG     CONSTANT VARCHAR2(15) := 'VendorOUDesc';
VEND_OU_REF_MSG      CONSTANT VARCHAR2(15) := 'VendorOURef';

--------------------------------------------------------
-- MESSAGE TYPE PARAMETERS
--------------------------------------------------------
VEND_ADD   CONSTANT VARCHAR2(15) := 'VendorCre';
VEND_UPD   CONSTANT VARCHAR2(15) := 'VendorHdrMod';
VEND_DEL   CONSTANT VARCHAR2(15) := 'VendorDel';
ADDR_CRE   CONSTANT VARCHAR2(15) := 'VendorAddrCre';
ADDR_UPD   CONSTANT VARCHAR2(15) := 'VendorAddrMod';
ADDR_DEL   CONSTANT VARCHAR2(15) := 'VendorAddrDel';
OU_CRE     CONSTANT VARCHAR2(15) := 'VendorOUCre';
OU_DEL     CONSTANT VARCHAR2(15) := 'VendorOUDel';

--------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------
PROCEDURE ADDTOQ(I_message_type   IN  VARCHAR2,
                 I_supplier       IN  sups.supplier%TYPE,
                 I_addr_seq_no    IN  addr.seq_no%TYPE,
                 I_addr_type      IN  addr.addr_type%TYPE,
                 I_ret_allow_ind  IN  VARCHAR2,
                 I_org_unit_id    IN  VARCHAR2,
                 I_message        IN  CLOB,
                 O_status         OUT VARCHAR2,
                 O_text           OUT VARCHAR2);
----------------------------------------------------------
PROCEDURE GETNXT(O_status_code   OUT VARCHAR2,
                 O_error_msg     OUT VARCHAR2,
                 O_message_type  OUT VARCHAR2,
                 O_message       OUT CLOB,
                 O_supplier     OUT sups.supplier%TYPE,
                 O_addr_seq_no  OUT addr.seq_no%TYPE,
                 O_addr_type    OUT addr.addr_type%TYPE);
---------------------------------------------------------
END;
/
