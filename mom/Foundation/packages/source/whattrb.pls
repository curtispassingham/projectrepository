CREATE OR REPLACE PACKAGE BODY WH_ATTRIB_SQL AS
--------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_wh              IN       NUMBER,
                  O_wh_name         IN OUT   VARCHAR2)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)    := 'WH_ATTRIB_SQL.GET_NAME';

   cursor C_WH is
      select wh_name
        from v_wh_tl
       where wh = I_wh;
BEGIN
   open C_WH;
   fetch C_WH into O_wh_name;
   if C_WH%NOTFOUND then
      close C_WH;
      O_error_message := sql_lib.create_msg('INV_WH', null,null,null);
      RETURN FALSE;
   else
      close C_WH;
      RETURN TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             null);
      RETURN FALSE;
END GET_NAME;
--------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_currency_code   IN OUT   WH.CURRENCY_CODE%TYPE,
                           I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program     VARCHAR2(64) := 'WH_ATTRIB_SQL.GET_CURRENCY_CODE';
   ---
   cursor C_WH is
      select currency_code
        from wh
       where wh = I_wh;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_WH','WH','Warehouse: '||to_char(I_wh));
   open C_WH;
   SQL_LIB.SET_MARK('FETCH','C_WH','WH','Warehouse: '||to_char(I_wh));
   fetch C_WH into O_currency_code;
   if C_WH%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_WH','WH','Warehouse: '||to_char(I_wh));
      close C_WH;
      O_error_message := sql_lib.create_msg('INV_WH',
                                            null,null,null);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_WH','WH','Warehouse: '||to_char(I_wh));
   close C_WH;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_CURRENCY_CODE;
--------------------------------------------------------------------
FUNCTION GET_PWH_NAME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_wh              IN       WH.WH%TYPE,
                      O_wh_name         IN OUT   WH.WH_NAME%TYPE)
   RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.GET_PWH_NAME';
   ---
   cursor C_WH is
      select vwh.wh_name
        from wh wh,
             v_wh_tl vwh
       where wh.wh          = I_wh
         and wh.physical_wh = wh.wh
         and wh.wh          = vwh.wh;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_WH','wh','wh: ' || to_char(I_wh));
   open C_WH;
   ---
   SQL_LIB.SET_MARK('FETCH','C_WH','wh','wh: ' || to_char(I_wh));
   fetch C_WH into O_wh_name;
   ---
   if (C_WH%NOTFOUND) then
      SQL_LIB.SET_MARK('CLOSE','C_WH','wh','wh: ' || to_char(I_wh));
      close C_WH;
      O_error_message := sql_lib.create_msg('INV_PWH',
                                            null,
                                            null,
                                            null);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_WH','wh','wh: ' || to_char(I_wh));
   close C_WH;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;
END GET_PWH_NAME;
--------------------------------------------------------------------
FUNCTION CHECK_PWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_flag            IN OUT   BOOLEAN,
                   I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.CHECK_PWH';
   L_tmp VARCHAR2(1);
   ---
   cursor C_WH is
      select 'x'
        from wh
       where wh = I_wh
         and physical_wh = wh
         and rownum = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_WH','wh','wh: ' || to_char(I_wh));
   open C_WH;
   ---
   SQL_LIB.SET_MARK('FETCH','C_WH','wh','wh: ' || to_char(I_wh));
   fetch C_WH into L_tmp;
   ---
   if (C_WH%NOTFOUND) then
      SQL_LIB.SET_MARK('CLOSE','C_WH','wh','wh: ' || to_char(I_wh));
      close C_WH;
      O_flag := FALSE;
   else
      SQL_LIB.SET_MARK('CLOSE','C_WH','wh','wh: ' || to_char(I_wh));
      close C_WH;
      O_flag := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;
END CHECK_PWH;
-------------------------------------------------------------------
FUNCTION CHECK_VWH(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_flag                IN OUT   BOOLEAN,
                   I_wh                  IN       WH.WH%TYPE,
                   I_exclude_finishers   IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.CHECK_VWH';
   L_tmp     VARCHAR2(1);
   ---
   cursor C_WH_ALL is
      select 'X'
        from wh
       where wh = I_wh
         and stockholding_ind = 'Y'
         and rownum = 1;

   cursor C_WH is
      select 'X'
        from wh
       where wh = I_wh
         and stockholding_ind = 'Y'
         and finisher_ind = 'N'
         and rownum = 1;

BEGIN

   if I_exclude_finishers = 'Y' then

      open C_WH;
      fetch C_WH into L_tmp;
      ---
      if (C_WH%FOUND) then
         O_flag := TRUE;
      else
         O_flag := FALSE;
      end if;
      ---
      close C_WH;

   else

      open C_WH_ALL;
      fetch C_WH_ALL into L_tmp;
      ---
      if (C_WH_ALL%FOUND) then
         O_flag := TRUE;
      else
         O_flag := FALSE;
      end if;
      ---
      close C_WH_ALL;

   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;
END CHECK_VWH;
------------------------------------------------------------------------
FUNCTION GET_WH_INFO(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_wh_add1           IN OUT   ADDR.ADD_1%TYPE,
                     O_wh_add2           IN OUT   ADDR.ADD_2%TYPE,
                     O_wh_city           IN OUT   ADDR.CITY%TYPE,
                     O_state             IN OUT   ADDR.STATE%TYPE,
                     O_country_id        IN OUT   ADDR.COUNTRY_ID%TYPE,
                     O_wh_pcode          IN OUT   ADDR.POST%TYPE,
                     O_vat_region        IN OUT   WH.VAT_REGION%TYPE,
                     O_org_hier_type     IN OUT   WH.ORG_HIER_TYPE%TYPE,
                     O_org_hier_value    IN OUT   WH.ORG_HIER_VALUE%TYPE,
                     O_currency_code     IN OUT   WH.CURRENCY_CODE%TYPE,
                     O_break_pack_ind    IN OUT   WH.BREAK_PACK_IND%TYPE,
                     O_redist_wh_ind     IN OUT   WH.REDIST_WH_IND%TYPE,
                     O_delivery_policy   IN OUT   WH.DELIVERY_POLICY%TYPE,
                     O_email             IN OUT   WH.EMAIL%TYPE,
                     O_county            IN OUT   ADDR.COUNTY%TYPE,
                     O_duns_number       IN OUT   WH.DUNS_NUMBER%TYPE,
                     O_duns_loc          IN OUT   WH.DUNS_LOC%TYPE,
                     I_wh                IN       WH.WH%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.GET_WH_INFO';
   L_wh_rec   WH_RECTYPE;

BEGIN
   if GET_WH_INFO (O_error_message,
                   L_wh_rec,
                   I_wh) = FALSE then
      return FALSE;
   end if;

   O_wh_add1 := L_wh_rec.wh_add1;
   O_wh_add2 := L_wh_rec.wh_add2;
   O_wh_city := L_wh_rec.wh_city;
   O_state := L_wh_rec.state;
   O_country_id := L_wh_rec.country_id;
   O_wh_pcode := L_wh_rec.wh_pcode;
   O_vat_region := L_wh_rec.vat_region;
   O_org_hier_type := L_wh_rec.org_hier_type;
   O_org_hier_value := L_wh_rec.org_hier_value;
   O_currency_code := L_wh_rec.currency_code;
   O_break_pack_ind := L_wh_rec.break_pack_ind;
   O_redist_wh_ind := L_wh_rec.redist_wh_ind;
   O_delivery_policy := L_wh_rec.delivery_policy;
   O_email := L_wh_rec.email;
   O_county := L_wh_rec.county;
   O_duns_number := L_wh_rec.duns_number;
   O_duns_loc := L_wh_rec.duns_loc;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_WH_INFO;
--------------------------------------------------------------------
FUNCTION WH_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exist           IN OUT   BOOLEAN,
                  I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   CURSOR C_WH IS
      select 'X'
        from wh
       where wh.wh =   I_wh
         and rownum = 1
      union all
      select 'X'
        from store
       where store.store = I_wh
         and rownum = 1
      union all
      select 'X'
        from store_add
       where store_add.store = I_wh
         and rownum = 1;
   ---
   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.WH_EXIST';
   L_tmp     VARCHAR2(1);

BEGIN
   open C_WH;
   fetch C_WH into L_tmp;
   ---
   if (C_WH%FOUND) then
      close C_WH;
      O_exist := TRUE;
   else
      close C_WH;
      O_exist := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;
END WH_EXIST;
----------------------------------------------------------------
FUNCTION VWH_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exist           IN OUT   BOOLEAN,
                   I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

    CURSOR C_VWH IS
      SELECT 'x'
        FROM wh
       WHERE physical_wh = I_wh
         AND (stockholding_ind = 'Y' or
              (org_entity_type in ('M','X')))
         AND rownum = 1;
   ---
   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.VWH_EXIST';
   L_tmp     VARCHAR2(1);

BEGIN
   open C_VWH;
   fetch C_VWH into L_tmp;
   ---
   if (C_VWH%FOUND) then
      close C_VWH;
      O_exist := TRUE;
   else
      close C_VWH;
      O_exist := FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;
END VWH_EXIST;
-----------------------------------------------------------------------------
FUNCTION CANCEL_WH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)         := 'WH_ATTRIB_SQL.CANCEL_WH';
   L_wh                 WH.WH%TYPE;
   L_table              VARCHAR2(30)         := 'WH';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   --Initialize the attribute values for L10N_SQL
   L_procedure_key      VARCHAR2(30)         := 'DEL_ENT_TRIB_SUBS';
   L_country_id         VARCHAR2(3);
   L_source_entity      VARCHAR2(6)          := 'LOC';
   L_source_id          VARCHAR2(10)         := I_wh;
   L_source_type        VARCHAR2(6);
   L_item               VARCHAR2(25);
   L_exists_ind         VARCHAR2(1);

   cursor C_WH is
      select wh
        from wh
       where wh = I_wh
          or physical_wh = I_wh
       order BY wh desc;

   cursor C_LOCK_COST_ZONE_GROUP_LOC is
      select 'x'
        from cost_zone_group_loc
       where loc_type = 'W'
         and location = L_wh
          or zone_id  = L_wh
         for update nowait;

   cursor C_LOCK_SOURCE_DLVRY_SCHED_DAYS is
      select 'x'
        from source_dlvry_sched_days
       where loc_type = 'W'
         and location = L_wh
         for update nowait;

   cursor C_LOCK_SOURCE_DLVRY_SCHED_EXC is
      select 'x'
        from source_dlvry_sched_exc
       where loc_type = 'W'
         and location = L_wh
         for update nowait;

   cursor C_LOCK_SOURCE_DLVRY_SCHED is
      select 'x'
        from source_dlvry_sched
       where loc_type = 'W'
         and location = L_wh
         for update nowait;

   cursor C_LOCK_COMPANY_CLOSED_EXCEP is
      select 'x'
        from company_closed_excep
       where loc_type = 'W'
         and location = L_wh
         for update nowait;

   cursor C_LOCK_LOCATION_CLOSED_TL is
      select 'x'
        from location_closed_tl
       where location in (select location
                            from location_closed
                           where loc_type = 'W'
                             and location = L_wh)
         for update nowait;

   cursor C_LOCK_LOCATION_CLOSED is
      select 'x'
        from location_closed
       where loc_type = 'W'
         and location = L_wh
         for update nowait;

   cursor C_LOCK_COST_ZONE_TL is
      select 'x'
        from cost_zone_tl
       where zone_id = L_wh
         for update nowait;

   cursor C_LOCK_COST_ZONE is
      select 'x'
        from cost_zone
       where zone_id = L_wh
         for update nowait;

   cursor C_LOCK_STOCK_LEDGER_INSERTS is
      select 'x'
        from stock_ledger_inserts
       where location = L_wh
         for update nowait;

   cursor C_LOCK_WH_TL is
      select 'x'
        from wh_tl
       where wh in (select wh
                      from wh
                     where wh = I_wh
                        or physical_wh = I_wh)
         for update nowait;

   cursor C_LOCK_WH is
      select 'x'
        from wh
       where wh = I_wh
          or physical_wh = I_wh
         for update nowait;

   cursor C_LOCK_WH_ADD is
      select 'x'
        from wh_add
       where wh = L_wh
         for update nowait;

   cursor C_LOCK_ADDR_TL is
      select 'x'
        from addr_tl
       where addr_key in (select addr_key
                            from addr
                           where key_value_1 = TO_CHAR(L_wh)
                             and module = 'WH')
         for update nowait;
         
   cursor C_LOCK_ADDR is
      select 'x'
        from addr
       where key_value_1 = TO_CHAR(L_wh)
         and module = 'WH'
         for update nowait;
         
    cursor C_LOCK_ADDR_CFA_EXT is
       select 'x'
         from addr_cfa_ext
        where exists (select 'x'
                        from addr
                       where key_value_1 = TO_CHAR(L_wh)
                        and module      = 'WH')
          for update nowait;     
          
   cursor C_LOCK_WH_CFA_EXT is
      select 'x'
        from wh_cfa_ext
       where wh = L_wh 
         for update nowait;
BEGIN

   -- Call to the Generic Wrapper function L10N_SQL.CALL_EXEC_FUNC_FND
   if L10N_SQL.CALL_EXEC_FUNC_FND(O_error_message,
                                  L_exists_ind,
                                  L_procedure_key,
                                  L_country_id,
                                  L_source_entity,
                                  L_source_id,
                                  L_source_type,
                                  L_item) = FALSE then
      return FALSE;
   end if;

   FOR rec in C_WH loop
      L_wh := rec.wh;
      L_table := 'cost_zone_group_loc';
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_COST_ZONE_GROUP_LOC',
                       'cost_zone_group_loc',
                       'location: ' || to_char(L_wh));
      OPEN C_LOCK_COST_ZONE_GROUP_LOC;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_COST_ZONE_GROUP_LOC',
                       'cost_zone_group_loc',
                       'location: ' || to_char(L_wh));
      CLOSE C_LOCK_COST_ZONE_GROUP_LOC;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'cost_zone_group_loc',
                       'location: ' || to_char(L_wh));
      ---
      DELETE from cost_zone_group_loc
       WHERE loc_type = 'W'
         AND location = L_wh
          OR zone_id  = L_wh;
      ---

      L_table := 'source_dlvry_sched_days';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_SOURCE_DLVRY_SCHED_DAYS',
                       'source_dlvry_sched_days',
                       'location: ' || to_char(L_wh));
      OPEN C_LOCK_SOURCE_DLVRY_SCHED_DAYS;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_SOURCE_DLVRY_SCHED_DAYS',
                       'source_dlvry_sched_DAYS',
                       'location: ' || to_char(L_wh));
      CLOSE C_LOCK_SOURCE_DLVRY_SCHED_DAYS;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'source_dlvry_sched_days',
                       'location: ' || to_char(L_wh));
      ---
      DELETE from source_dlvry_sched_days
       WHERE loc_type = 'W'
         AND location = L_wh;

      L_table := 'source_dlvry_sched_exc';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_SOURCE_DLVRY_SCHED_EXC',
                       'source_dlvry_sched_exc',
                       'location: ' || to_char(L_wh));
      OPEN C_LOCK_SOURCE_DLVRY_SCHED_EXC;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_SOURCE_DLVRY_SCHED_EXC',
                       'source_dlvry_sched_exc',
                       'location: ' || to_char(L_wh));
      CLOSE C_LOCK_SOURCE_DLVRY_SCHED_EXC;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'source_dlvry_sched_exc',
                       'location: ' || to_char(L_wh));
      ---
      DELETE from source_dlvry_sched_exc
       WHERE loc_type = 'W'
         AND location = L_wh;

      L_table := 'source_dlvry_sched';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_SOURCE_DLVRY_SCHED',
                       'source_dlvry_sched',
                       'location: ' || to_char(L_wh));
      OPEN C_LOCK_SOURCE_DLVRY_SCHED;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_SOURCE_DLVRY_SCHED',
                       'source_dlvry_sched',
                       'location: ' || to_char(L_wh));
      CLOSE C_LOCK_SOURCE_DLVRY_SCHED;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'source_dlvry_sched',
                       'location: ' || to_char(L_wh));
      ---
      DELETE from source_dlvry_sched
       WHERE loc_type = 'W'
         AND location = L_wh;

      L_table := 'company_closde_exep';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_COMPANY_CLOSED_EXCEP',
                       'company_closed_exep',
                       'location: ' || to_char(L_wh));
      OPEN C_LOCK_COMPANY_CLOSED_EXCEP;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_COMPANY_CLOSED_EXCEP',
                       'company_closed_exep',
                       'location: ' || to_char(L_wh));
      CLOSE C_LOCK_COMPANY_CLOSED_EXCEP;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'company_closed_excep',
                       'location: ' || to_char(L_wh));
      ---
      DELETE from company_closed_excep
       WHERE loc_type = 'W'
         AND location = L_wh;

      L_table := 'location_closed_tl';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_LOCATION_CLOSED_TL',
                       'location_closed_tl',
                       'location: ' || to_char(L_wh));
      OPEN C_LOCK_LOCATION_CLOSED_TL;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_LOCATION_CLOSED_TL',
                       'location_closed_tl',
                       'location: ' || to_char(L_wh));
      CLOSE C_LOCK_LOCATION_CLOSED_TL;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'location_closed_tl',
                       'location: ' || to_char(L_wh));
      ---
      DELETE from location_closed_tl
       WHERE location in (select location
                            from location_closed
                           where loc_type = 'W'
                             AND location = L_wh);

      L_table := 'location_closed';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_LOCATION_CLOSED',
                       'location_closed',
                       'location: ' || to_char(L_wh));
      OPEN C_LOCK_LOCATION_CLOSED;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_LOCATION_CLOSED',
                       'location_closed',
                       'location: ' || to_char(L_wh));
      CLOSE C_LOCK_LOCATION_CLOSED;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'location_closed',
                       'location: ' || to_char(L_wh));
      ---
      DELETE from location_closed
       WHERE loc_type = 'W'
         AND location = L_wh;

      L_table := 'cost_zone_tl';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_COST_ZONE_TL',
                       'cost_zone_tl',
                       'Zone_id: ' || to_char(L_wh));
      OPEN C_LOCK_COST_ZONE_TL;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_COST_ZONE_TL',
                       'cost_zone_tl',
                       'Zone_id: ' || to_char(L_wh));
      CLOSE C_LOCK_COST_ZONE_TL;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'cost_zone_tl',
                       'zone_id: ' || to_char(L_wh));
      ---
      DELETE from cost_zone_tl
       WHERE zone_id = L_wh;

      L_table := 'cost_zone';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_COST_ZONE',
                       'cost_zone',
                       'Zone_id: ' || to_char(L_wh));
      OPEN C_LOCK_COST_ZONE;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_COST_ZONE',
                       'cost_zone',
                       'Zone_id: ' || to_char(L_wh));
      CLOSE C_LOCK_COST_ZONE;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'cost_zone',
                       'zone_id: ' || to_char(L_wh));
      ---
      DELETE from cost_zone
       WHERE zone_id = L_wh;

      L_table := 'stock_ledger_inserts';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_STOCK_LEDGER_INSERTS',
                       'stock_ledger_inserts',
                       'wh: ' || to_char(L_wh));
      OPEN C_LOCK_STOCK_LEDGER_INSERTS;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_STOCK_LEDGER_INSERTS',
                       'stock_ledger_inserts',
                       'wh: ' || to_char(L_wh));
      CLOSE C_LOCK_STOCK_LEDGER_INSERTS;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'stock_ledger_inserts',
                       'wh: ' || to_char(L_wh));
      ---
      DELETE from stock_ledger_inserts
       WHERE location = L_wh;
      ---
      L_table := 'ADDR_CFA_EXT';

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ADDR_CFA_EXT',
                       'ADDR',
                       'WH: '||to_char(L_wh));
      open C_LOCK_ADDR_CFA_EXT;
      SQL_LIB.SET_MARK('CLOSE',
                       'to_char(L_wh)',
                       'ADDR',
                       'WH: '||to_char(L_wh));
      close C_LOCK_ADDR_CFA_EXT;
 
      SQL_LIB.SET_MARK('DELETE',
                        NULL,
                       'ADDR',
                       'WH: '||to_char(L_wh));

      delete from addr_cfa_ext
            where addr_key in (select addr_key
                                 from addr
                                where key_value_1 = to_char(L_wh)
                                  and module      = 'WH');      
      ---
      L_table := 'addr_tl';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ADDR_TL',
                       'addr_tl',
                       'wh: ' || to_char(L_wh));
      OPEN C_LOCK_ADDR_TL;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ADDR_TL',
                       'addr_tl',
                       'wh: ' || to_char(L_wh));
      CLOSE C_LOCK_ADDR_TL;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'addr_tl',
                       'wh: ' || to_char(L_wh));
      ---
      DELETE from addr_tl
       WHERE addr_key in (select addr_key
                            from addr
                           where key_value_1 = TO_CHAR(L_wh)
                             and module = 'WH');

      L_table := 'addr';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ADDR',
                       'addr',
                       'wh: ' || to_char(L_wh));
      OPEN C_LOCK_ADDR;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ADDR',
                       'addr',
                       'wh: ' || to_char(L_wh));
      CLOSE C_LOCK_ADDR;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'addr',
                       'wh: ' || to_char(L_wh));
      ---
      DELETE from addr
       WHERE key_value_1 = TO_CHAR(L_wh)
         AND module = 'WH';

          --- delete current record from wh table
      L_table := 'wh_add';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_WH_ADD',
                       'wh_add',
                       'wh: ' || to_char(L_wh));
      OPEN C_LOCK_WH_ADD;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_WH_ADD',
                       'wh_add',
                       'wh: ' || to_char(L_wh));
      CLOSE C_LOCK_WH_ADD;
      ---
      SQL_LIB.SET_MARK('DELETE', null,
                       'wh_add',
                       'wh: ' || to_char(L_wh));
      ---
      DELETE from wh_add
       WHERE wh = L_wh;
          --- delete current record from wh_add table

      --Call to delete the warehouse attrib
      if WH_ATTRIB_SQL.DEL_WH_ATTRIB(O_error_message,
                                     L_wh) = FALSE then
         return FALSE;
      end if;
      
      -- Delete from cfa wh table before deleting from wh
      L_table := 'WH_CFA_EXT';

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_WH_CFA_EXT',
                       'WH_CFA_EXT',
                       'WH: '||L_wh);
      open C_LOCK_WH_CFA_EXT;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_WH_CFA_EXT',
                       'WH_CFA_EXT',
                       'WH: '||L_wh);
      close C_LOCK_WH_CFA_EXT;

      SQL_LIB.SET_MARK('DELETE',
                       'C_LOCK_WH_CFA_EXT',
                       'WH_CFA_EXT',
                       'WH: ' || L_wh);
      delete from wh_cfa_ext
            where wh= L_wh;       
   end loop;
   

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_WH_TL',
                    'wh_tl',
                    'wh: ' || to_char(I_wh));
   OPEN C_LOCK_WH_TL;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_WH_TL',
                    'wh_tl',
                    'wh: ' || to_char(I_wh));
   CLOSE C_LOCK_WH_TL;
   SQL_LIB.SET_MARK('DELETE', null,
                    'wh_tl',
                    'wh: ' || to_char(I_wh));

   delete from wh_tl
    where wh in (select wh 
                   from wh
                  where wh = I_wh
                     or physical_wh = I_wh);
    
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_wh',
                    'wh',
                    'wh: ' || to_char(I_wh));
   OPEN C_LOCK_wh;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_wh',
                    'wh',
                    'wh: ' || to_char(I_wh));
   CLOSE C_LOCK_wh;
   SQL_LIB.SET_MARK('DELETE', null,
                    'wh',
                    'wh: ' || to_char(I_wh));

   delete from wh
    where wh = I_wh
       or physical_wh = I_wh;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
   O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                         L_table,
                                         L_wh,
                                         NULL);
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;
END CANCEL_WH;
-----------------------------------------------------------------------------
FUNCTION GET_PWH_FOR_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_pwh             IN OUT   WH.PHYSICAL_WH%TYPE,
                         I_vwh             IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   CURSOR C_PWH IS
      SELECT physical_wh
        FROM wh
       WHERE wh = I_vwh;
   ---
   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.GET_PWH_FOR_VWH';

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_PWH', 'wh', 'wh: ' || to_char(I_vwh));
   open C_PWH;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_PWH', 'wh', 'wh: ' || to_char(I_vwh));
   fetch C_PWH into O_pwh;
   ---
   if (C_PWH%NOTFOUND) then
      SQL_LIB.SET_MARK('CLOSE', 'C_PWH', 'wh', 'wh: ' || to_char(I_vwh));
      close C_PWH;
      ---
      O_error_message := sql_lib.create_msg('INV_VWH', null, null, null);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_PWH', 'wh', 'wh: ' || to_char(I_vwh));
   close C_PWH;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;
END GET_PWH_FOR_VWH;
----------------------------------------------------------------------------------------
FUNCTION GET_WH_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_wh_rec          IN OUT   WH_RECTYPE,
                     I_wh              IN       WH.WH%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.GET_WH_INFO';


   cursor C_WH is
      select wh.wh_name,
             ad.add_1,
             ad.add_2,
             ad.city,
             ad.county,
             ad.state,
             ad.country_id,
             ad.post,
             wh.email,
             wh.vat_region,
             wh.org_hier_type,
             wh.org_hier_value,
             wh.currency_code,
             wh.physical_wh,
             wh.channel_id,
             wh.stockholding_ind,
             wh.break_pack_ind,
             ad.edi_addr_chg,
             wh.redist_wh_ind,
             wh.delivery_policy,
             wh.protected_ind,
             wh.forecast_wh_ind,
             wh.rounding_seq,
             wh.repl_ind,
             wh.repl_wh_link,
             wh.repl_src_ord,
             wh.ib_ind,
             wh.ib_wh_link,
             wh.auto_ib_clear,
             wh.duns_number,
             wh.duns_loc
        from wh wh,
             addr ad,
             add_type_module at
       where wh.wh = I_wh
         and wh.finisher_ind = 'N'
         and ad.key_value_1 = TO_CHAR(wh.physical_wh)
         and ad.module = 'WH'
         and ad.module = at.module
         and at.primary_ind = 'Y';

BEGIN
   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_wh',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_WH', 'WH', 'WH: ' || to_char(I_wh));
   open C_WH;
   SQL_LIB.SET_MARK('FETCH', 'C_WH', 'WH', 'WH: ' || to_char(I_wh));
   fetch C_WH into O_wh_rec.wh_name,
                   O_wh_rec.wh_add1,
                   O_wh_rec.wh_add2,
                   O_wh_rec.wh_city,
                   O_wh_rec.county,
                   O_wh_rec.state,
                   O_wh_rec.country_id,
                   O_wh_rec.wh_pcode,
                   O_wh_rec.email,
                   O_wh_rec.vat_region,
                   O_wh_rec.org_hier_type,
                   O_wh_rec.org_hier_value,
                   O_wh_rec.currency_code,
                   O_wh_rec.physical_wh,
                   O_wh_rec.channel_id,
                   O_wh_rec.stockholding_ind,
                   O_wh_rec.break_pack_ind,
                   O_wh_rec.edi_addr_chg,
                   O_wh_rec.redist_wh_ind,
                   O_wh_rec.delivery_policy,
                   O_wh_rec.protected_ind,
                   O_wh_rec.forecast_wh_ind,
                   O_wh_rec.rounding_seq,
                   O_wh_rec.repl_ind,
                   O_wh_rec.repl_wh_link,
                   O_wh_rec.repl_src_ord,
                   O_wh_rec.ib_ind,
                   O_wh_rec.ib_wh_link,
                   O_wh_rec.auto_ib_clear,
                   O_wh_rec.duns_number,
                   O_wh_rec.duns_loc;
   if C_WH%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_WH', 'WH', 'WH: ' || to_char(I_wh));
      close C_WH;
      O_error_message := SQL_LIB.CREATE_MSG('INV_WH',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_WH', 'WH', 'WH: ' || to_char(I_wh));
   close C_WH;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_WH_INFO;
----------------------------------------------------------------------------------------
FUNCTION GET_WH_MULTI_INFO(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_physical_wh        IN OUT   WH.PHYSICAL_WH%TYPE,
                           O_channel_id         IN OUT   WH.CHANNEL_ID%TYPE,
                           O_stockholding_ind   IN OUT   WH.STOCKHOLDING_IND%TYPE,
                           O_protected_ind      IN OUT   WH.PROTECTED_IND%TYPE,
                           O_forecast_wh_ind    IN OUT   WH.FORECAST_WH_IND%TYPE,
                           O_rounding_seq       IN OUT   WH.ROUNDING_SEQ%TYPE,
                           O_repl_ind           IN OUT   WH.REPL_IND%TYPE,
                           O_repl_wh_link       IN OUT   WH.REPL_WH_LINK%TYPE,
                           O_repl_src_ord       IN OUT   WH.REPL_SRC_ORD%TYPE,
                           O_ib_ind             IN OUT   WH.IB_IND%TYPE,
                           O_ib_wh_link         IN OUT   WH.IB_WH_LINK%TYPE,
                           O_auto_ib_clear      IN OUT   WH.AUTO_IB_CLEAR%TYPE,
                           I_wh                 IN       WH.WH%TYPE)
   return BOOLEAN is

   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.GET_WH_MULTI_INFO';

   L_wh_rec   WH_RECTYPE;

BEGIN
   if GET_WH_INFO (O_error_message,
                   L_wh_rec,
                   I_wh) = FALSE then
      return FALSE;
   end if;

   O_physical_wh := L_wh_rec.physical_wh;
   O_channel_id := L_wh_rec.channel_id;
   O_stockholding_ind := L_wh_rec.stockholding_ind;
   O_protected_ind := L_wh_rec.protected_ind;
   O_forecast_wh_ind := L_wh_rec.forecast_wh_ind;
   O_rounding_seq := L_wh_rec.rounding_seq;
   O_repl_ind := L_wh_rec.repl_ind;
   O_repl_wh_link := L_wh_rec.repl_wh_link;
   O_repl_src_ord := L_wh_rec.repl_src_ord;
   O_ib_ind := L_wh_rec.ib_ind;
   O_ib_wh_link := L_wh_rec.ib_wh_link;
   O_auto_ib_clear := L_wh_rec.auto_ib_clear;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_WH_MULTI_INFO;
-------------------------------------------------------------------------
FUNCTION VWH_EXISTS_IN_PWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_vwh             IN       WH.WH%TYPE,
                           I_physical_wh     IN       WH.PHYSICAL_WH%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'WH_ATTRIB_SQL.VWH_EXISTS_IN_PWH';
   L_exists     VARCHAR2(1)  := 'N';

   cursor C_EXISTS is
      select 'Y'
        from wh
       where wh               = I_vwh
         and physical_wh      = I_physical_wh
         and (stockholding_ind = 'Y'
              or (org_entity_type in ('M','X')))
         and rownum = 1;

BEGIN
   if I_vwh is NULL or I_physical_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','WH','Virtual Wh: '||to_char(I_vwh)||'Physical Wh: '||to_char(I_physical_wh));
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','WH','Virtual Wh: '||to_char(I_vwh)||'Physical Wh: '||to_char(I_physical_wh));
   fetch C_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','WH','Virtual Wh: '||to_char(I_vwh)||'Physical Wh: '||to_char(I_physical_wh));
   close C_EXISTS;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VWH_EXISTS_IN_PWH;
----------------------------------------------------------------------------------------
FUNCTION CHECK_REPL_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'WH_ATTRIB_SQL.CHECK_REPL_EXISTS';
   L_exists     VARCHAR2(1)  := 'N';

   cursor C_CHECK_REPL_ITEM_LOC is
       select 'Y'
         from dual
        where exists (select 1
                        from repl_item_loc
                       where (location = I_wh or
                              source_wh = I_wh));

   cursor C_CHECK_SUB_ITEMS_HEAD is
       select 'Y'
         from dual
        where exists (select 1
                        from sub_items_head
                       where location = I_wh);

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_REPL_ITEM_LOC','WH','Wh: '||to_char(I_wh));
   open C_CHECK_REPL_ITEM_LOC;

   SQL_LIB.SET_MARK('FETCH','C_CHECK_REPL_ITEM_LOC','WH','Wh: '||to_char(I_wh));
   fetch C_CHECK_REPL_ITEM_LOC into L_exists;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_REPL_ITEM_LOC','WH','Wh: '||to_char(I_wh));
   close C_CHECK_REPL_ITEM_LOC;
   ---
   if L_exists = 'N' then
      SQL_LIB.SET_MARK('OPEN','C_CHECK_SUB_ITEMS_HEAD','WH','Wh: '||to_char(I_wh));
      open C_CHECK_SUB_ITEMS_HEAD;

      SQL_LIB.SET_MARK('FETCH','C_CHECK_SUB_ITEMS_HEAD','WH','Wh: '||to_char(I_wh));
      fetch C_CHECK_SUB_ITEMS_HEAD into L_exists;

      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUB_ITEMS_HEAD','WH','Wh: '||to_char(I_wh));
      close C_CHECK_SUB_ITEMS_HEAD;
   end if;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_REPL_EXISTS;

----------------------------------------------------------------------------------------
FUNCTION UPDATE_ALL_WH_LINKS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_repl_wh_link    IN       WH.REPL_WH_LINK%TYPE,
                             I_ib_wh_link      IN       WH.REPL_WH_LINK%TYPE,
                             I_repl_ind        IN       WH.REPL_IND%TYPE,
                             I_ib_ind          IN       WH.IB_IND%TYPE)
   RETURN BOOLEAN IS

RECORD_LOCKED    EXCEPTION;
PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
L_program        VARCHAR2(50) := 'WH_ATTRIB_SQL.UPDATE_ALL_WH_LINKS';

   cursor C_LOCK_REPL_WH_LINK is
       select 'X'
         from wh
        where repl_wh_link = I_repl_wh_link
          for update nowait;

   cursor C_LOCK_IB_WH_LINK is
       select 'X'
         from wh
        where ib_wh_link = I_ib_wh_link
          for update nowait;

BEGIN
   if I_repl_ind = 'Y' then
      ---
      open C_LOCK_REPL_WH_LINK;
      close C_LOCK_REPL_WH_LINK;
      ---
      update wh
         set repl_wh_link = NULL
       where repl_wh_link = I_repl_wh_link;
   end if;
   ---
   if I_ib_ind = 'Y' then
      ---
      open C_LOCK_IB_WH_LINK;
      close C_LOCK_IB_WH_LINK;
      ---
      update wh
         set ib_wh_link = NULL
       where ib_wh_link = I_ib_wh_link;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'WH',
                                             NULL,
                                             NULL);

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_ALL_WH_LINKS;
----------------------------------------------------------------------------------------
FUNCTION VWH_EXISTS_IN_PWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           O_wh_name         IN OUT   WH.WH_NAME%TYPE,
                           I_vwh             IN       WH.WH%TYPE,
                           I_physical_wh     IN       WH.PHYSICAL_WH%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'WH_ATTRIB_SQL.VWH_EXISTS_IN_PWH';

BEGIN
   --- Call overloaded function to see if this vwh exists in the pwh.
   if VWH_EXISTS_IN_PWH(O_error_message,
                        O_exists,
                        I_vwh,
                        I_physical_wh) = FALSE then
      return FALSE;
   end if;

   --- If it does exist, return the name to the calling program.
   if O_exists then
      if GET_NAME(O_error_message,
                  I_vwh,
                  O_wh_name) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VWH_EXISTS_IN_PWH;
----------------------------------------------------------------------------------------
FUNCTION DELETE_PWH(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_physical_wh        IN       WH.PHYSICAL_WH%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50)                         := 'WH_ATTRIB_SQL.DELETE_PWH';
   L_primary_vwh       DAILY_PURGE.KEY_VALUE%TYPE           := NULL;
   L_exists            BOOLEAN;

   cursor C_GET_ALL_VWH is
      select wh
        from wh
       where physical_wh = I_physical_wh
         and wh != physical_wh;

   cursor C_GET_PRIMARY_VWH is
      select primary_vwh
        from wh
       where wh = I_physical_wh;

BEGIN
   --- This function will insert records on daily purge for each vwh in
   --- addition to the pwh.  The records will then be deleted in this order:
   ---    1) all non-primary vwh
   ---    2) primary_vwh
   ---    3) physical_wh
   --- If any of the vwh has associations (e.g., item_loc records), they will
   --- fail in VALIDATE_RECORDS_SQL.DEL_WH, and the pwh will fail because
   --- it has vwh remaining.

   if I_physical_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_physical_wh', L_program, NULL);
      return FALSE;
   end if;

   --- See if this wh is already on daily_purge.
   if DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                   L_exists,
                                   I_physical_wh,
                                   'WH') = FALSE then
      return FALSE;
   end if;
   if L_exists then
      return TRUE;
   end if;

   --- Loop through each vwh and write record to daily_purge with delete_order = 1.
   for rec in C_GET_ALL_VWH loop
      BEGIN
         insert into daily_purge
                     (key_value,
                      table_name,
                      delete_type,
                      delete_order)
              values (rec.wh,
                      'WH',
                      'D',
                      1);
      EXCEPTION
         --- If this record already exists, then update the delete value to 1
         --- to make sure the warehouses are deleted in the proper order.
         when DUP_VAL_ON_INDEX then
            update daily_purge
               set delete_order = 1
             where key_value = rec.wh
               and table_name = 'WH';
      END;
   end loop;

   --- Set the delete order = 2 for the primary vwh.
   open C_GET_PRIMARY_VWH;
   fetch C_GET_PRIMARY_VWH into L_primary_vwh;
   close C_GET_PRIMARY_VWH;
   ---
   if L_primary_vwh is NOT NULL then
      update daily_purge
         set delete_order = 2
       where table_name = 'WH'
         and key_value = L_primary_vwh;
   end if;

   --- Insert a record for the physical_wh with a delete order = 3.
   insert into daily_purge
               (key_value,
                table_name,
                delete_type,
                delete_order)
        values (I_physical_wh,
                'WH',
                'D',
                3);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_PWH;
----------------------------------------------------------------------------------------
FUNCTION DELETE_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists          IN OUT   BOOLEAN,
                    I_vwh             IN       WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'WH_ATTRIB_SQL.DELETE_VWH';
   L_exists          BOOLEAN;
   L_vwh_is_primary  VARCHAR2(1)  := 'N';
   L_delete_order    NUMBER(1);

   cursor C_CHECK_PRIMARY_VWH is
      select 'Y'
        from wh
       where primary_vwh = I_vwh;

BEGIN
   if I_vwh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_vwh', L_program, NULL);
      return FALSE;
   end if;

   --- See if this vwh is already on daily_purge.
   if DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                   L_exists,
                                   I_vwh,
                                   'WH') = FALSE then
      return FALSE;
   end if;
   if L_exists then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   --- Check if this is a primary_vwh.  Can only delete these by deleting the pwh.
   open C_CHECK_PRIMARY_VWH;
   fetch C_CHECK_PRIMARY_VWH into L_vwh_is_primary;
   close C_CHECK_PRIMARY_VWH;

   if L_vwh_is_primary = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_PRIMARY_VWH',NULL, NULL, NULL);
      return FALSE;
   end if;

   if O_exists = FALSE then
      insert into daily_purge
                  (key_value,
                   table_name,
                   delete_type,
                   delete_order)
           values (I_vwh,
                   'WH',
                   'D',
                   1);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_VWH;
----------------------------------------------------------------------------------------
FUNCTION WH_VALID_FOR_CURRENCY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid           IN OUT   BOOLEAN,
                               O_wh_name         IN OUT   WH.WH_NAME%TYPE,
                               I_currency_code   IN       WH.CURRENCY_CODE%TYPE,
                               I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR(50) := 'WH_ATTRIB_SQL.WH_VALID_FOR_CURRENCY';
   L_valid    VARCHAR2(1) := 'N';

   cursor C_VALIDATE_WH_CURRENCY is
      select 'Y'
        from wh
       where wh            = I_wh
         and currency_code = I_currency_code
         and wh            = physical_wh;

BEGIN
   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_wh', L_program, NULL);
      return FALSE;
   elsif I_currency_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_currency_code', L_program, NULL);
      return FALSE;
   end if;

   O_valid := FALSE;

   open C_VALIDATE_WH_CURRENCY;
   fetch C_VALIDATE_WH_CURRENCY into L_valid;
   close C_VALIDATE_WH_CURRENCY;

   if L_valid = 'Y' then
      if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                I_wh,
                                O_wh_name) = FALSE then
         return FALSE;
      end if;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END WH_VALID_FOR_CURRENCY;

-------------------------------------------------------------------------
FUNCTION CHECK_FINISHER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_flag            IN OUT   BOOLEAN,
                        O_vwh_name        IN OUT   WH.WH_NAME%TYPE,
                        I_vwh             IN       WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'WH_ATTRIB_SQL.CHECK_FINISHER';
   ---

   cursor C_FINISHER is
      select wh_name
        from wh
       where wh = I_vwh
         and finisher_ind = 'Y';

BEGIN

   open C_FINISHER;
   fetch C_FINISHER into O_vwh_name;
   ---
   if (C_FINISHER%FOUND) then
      O_flag := TRUE;
   else
      O_flag := FALSE;
   end if;
   ---
   close C_FINISHER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;
END CHECK_FINISHER;

-------------------------------------------------------------------------
FUNCTION GET_ROW (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exists          IN OUT   BOOLEAN,
                  O_wh_row          IN OUT   WH%ROWTYPE,
                  I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'WH_ATTRIB_SQL.GET_ROW';

   cursor C_GET_ROW is
      select *
        from wh
       where wh = I_wh;

BEGIN
   --- Check required input parameters
   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --- Initialize the output variable
   O_wh_row := NULL;

   --- Get the row from wh

   open C_GET_ROW;
   fetch C_GET_ROW into O_wh_row;
   close C_GET_ROW;

   O_exists := (O_wh_row.wh is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ROW;
------------------------------------------------------------------------
FUNCTION CHECK_VWHS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists             OUT   BOOLEAN,
                    I_wh_tbl          IN       LOC_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'WH_ATTRIB_SQL.CHECK_VWHS';

   L_count        NUMBER(25);

   cursor C_COUNT_VWHS is
      select count(*)
        from wh
       where wh in (select *
                      from TABLE(cast(I_wh_tbl AS loc_tbl)))
         and stockholding_ind = 'Y';

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT_VWHS',
                    'WH',
                    NULL);

   open C_COUNT_VWHS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT_VWHS',
                    'WH',
                    NULL);
   fetch C_COUNT_VWHS into L_count;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT_VWHS',
                    'WH',
                    NULL);
   close C_COUNT_VWHS;

   if L_count = I_wh_tbl.COUNT then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_VWHS;
------------------------------------------------------------------------
FUNCTION CHECK_PWHS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists             OUT   BOOLEAN,
                    I_wh_tbl          IN       LOC_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'WH_ATTRIB_SQL.CHECK_PWHS';

   L_count        NUMBER(25);

   cursor C_COUNT_PWHS is
      select count(*)
        from wh
       where wh in (select *
                      from TABLE(cast(I_wh_tbl AS loc_tbl)))
         and physical_wh = wh;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT_PWHS',
                    'WH',
                    NULL);

   open C_COUNT_PWHS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT_PWHS',
                    'WH',
                    NULL);
   fetch C_COUNT_PWHS into L_count;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT_PWHS',
                    'WH',
                    NULL);
   close C_COUNT_PWHS;

   if L_count = I_wh_tbl.COUNT then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_PWHS;
-------------------------------------------------------------------------------
FUNCTION CHECK_WH_CURRENCY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_whs             IN       LOC_TBL,
                           I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(50)  := 'WH_ATTRIB_SQL.CHECK_WH_CURRENCY';

   L_count              NUMBER;
   L_query              VARCHAR2(500);
   L_loc_bind_no        VARCHAR2(1) := '1';
   L_currency_bind_no   VARCHAR2(1) := '2';

BEGIN

   L_query := ' select count(*) '                                                            ||
              '   from wh '                                                                  ||
              '  where wh in (select * '                                                     ||
              '                 from TABLE (CAST(:' || L_loc_bind_no || ' AS LOC_TBL))) '   ||
              '    and currency_code = :' || L_currency_bind_no;

   EXECUTE IMMEDIATE L_query into L_count
      using I_whs,
            I_currency_code;

   if I_whs.count != L_count then
      O_error_message := SQL_LIB.CREATE_MSG('DIFF_CURR', I_currency_code, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_WH_CURRENCY;
-------------------------------------------------------------------------------
FUNCTION CHECK_WH_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_whs             IN       LOC_TBL,
                          I_country_id      IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50)  := 'WH_ATTRIB_SQL.CHECK_WH_COUNTRY';

   L_count                NUMBER;
   L_query                VARCHAR2(500);
   L_loc_bind_no          VARCHAR2(1) := '1';
   L_country_id_bind_no   VARCHAR2(1) := '2';

BEGIN

   L_query := ' select count(*) '                                                         ||
              '   from wh '                                                               ||
              '  where wh in (select * '                                                  ||
              '                from TABLE (CAST(:' || L_loc_bind_no || ' AS LOC_TBL))) ' ||
              '    and country_id = :' || L_country_id_bind_no;

   EXECUTE IMMEDIATE L_query into L_count
      using I_whs,
            I_country_id;

   if I_whs.count != L_count then
      O_error_message := SQL_LIB.CREATE_MSG('DIFF_COUNTRY', I_country_id, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_WH_COUNTRY;
----------------------------------------------------------------------------------
FUNCTION CHECK_ENT_ON_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   VARCHAR2,
                            I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'WH_ATTRIB_SQL.CHECK_ENT_ON_ORDER';

   L_ie_on_order_exists   VARCHAR2(1)  := NULL;

   cursor C_IE_ON_ORDER is
      select 'x'
        from ordhead
       where import_id = I_wh;

BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_IE_ON_ORDER',
                    'ORDHEAD',
                    'IMPORT ID: '||to_char(I_wh));
   open C_IE_ON_ORDER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_IE_ON_ORDER',
                    'ORDHEAD',
                    'IMPORT ID: '||to_char(I_wh));
   fetch C_IE_ON_ORDER into L_ie_on_order_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_IE_ON_ORDER',
                    'ORDHEAD',
                    'IMPORT ID: '||to_char(I_wh));
   close C_IE_ON_ORDER;

   if L_ie_on_order_exists is not NULL then
      O_exists := 'Y';
   else
      O_exists := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ENT_ON_ORDER;
------------------------------------------------------------------------
FUNCTION CHECK_ENT_ON_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   VARCHAR2,
                               I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) := 'WH_ATTRIB_SQL.CHECK_ENT_ON_SHIPMENT';

   L_ie_on_shipment_exists   VARCHAR2(1)  := NULL;

   cursor C_IE_ON_SHIPMENT is
      select 'x'
        from shipment sh,
             ordhead oh
       where sh.order_no = oh.order_no
         and oh.import_id = I_wh
         and rownum = 1;

BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_IE_ON_SHIPMENT',
                    'SHIPMENT',
                    'BILL TO LOC: '||to_char(I_wh));
   open C_IE_ON_SHIPMENT;

   SQL_LIB.SET_MARK('FETCH',
                    'C_IE_ON_SHIPMENT',
                    'SHIPMENT',
                    'BILL TO LOC: '||to_char(I_wh));
   fetch C_IE_ON_SHIPMENT into L_ie_on_shipment_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_IE_ON_SHIPMENT',
                    'SHIPMENT',
                    'BILL TO LOC: '||to_char(I_wh));
   close C_IE_ON_SHIPMENT;

   if L_ie_on_shipment_exists is not NULL then
      O_exists := 'Y';
   else
      O_exists := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ENT_ON_SHIPMENT;
------------------------------------------------------------------------
FUNCTION CHECK_ENT_ON_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   VARCHAR2,
                          I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'WH_ATTRIB_SQL.CHECK_ENT_ON_TSF';

   L_ie_on_sg_tsf_exists   VARCHAR2(1)  := NULL;

   cursor C_IE_ON_SG_TSF is
      select 'x'
        from tsfhead
       where tsf_type = 'SG'
         and from_loc = I_wh
         and rownum = 1;

BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_IE_ON_SG_TSF',
                    'TSFHEAD',
                    'FROM LOC: '||to_char(I_wh));
   open C_IE_ON_SG_TSF;

   SQL_LIB.SET_MARK('FETCH',
                    'C_IE_ON_SG_TSF',
                    'TSFHEAD',
                    'FROM LOC: '||to_char(I_wh));
   fetch C_IE_ON_SG_TSF into L_ie_on_sg_tsf_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_IE_ON_SG_TSF',
                    'TSFHEAD',
                    'FROM LOC: '||to_char(I_wh));
   close C_IE_ON_SG_TSF;

   if L_ie_on_sg_tsf_exists is not NULL then
      O_exists := 'Y';
   else
      O_exists := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ENT_ON_TSF;
------------------------------------------------------------------------
FUNCTION CHECK_ENT_ON_INV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   VARCHAR2,
                          I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'WH_ATTRIB_SQL.CHECK_ENT_ON_INV';

   L_ie_on_inv_exists   VARCHAR2(1)  := NULL;

   cursor C_IE_ON_INV_PO is
      select 'x'
        from ordhead oh,
             invc_xref ix
       where oh.order_no  = ix.order_no
         and oh.import_id = I_wh
         and rownum = 1;

   cursor C_IE_ON_INV_SHIPMENT is
      select 'Y'
        from shipment sm,
             ordhead oh,
             invc_xref ix
       where sm.shipment = ix.shipment
         and sm.order_no = oh.order_no
         and oh.import_id = I_wh
         and rownum = 1;

BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_IE_ON_INV_PO',
                    'ORDHEAD'||'INVC_XREF',
                    'IMPORT ID: '||to_char(I_wh));
   open C_IE_ON_INV_PO;

   SQL_LIB.SET_MARK('FETCH',
                    'C_IE_ON_INV_PO',
                    'ORDHEAD'||'INVC_XREF',
                    'IMPORT ID: '||to_char(I_wh));
   fetch C_IE_ON_INV_PO into L_ie_on_inv_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_IE_ON_INV_PO',
                    'ORDHEAD'||'INVC_XREF',
                    'IMPORT ID: '||to_char(I_wh));
   close C_IE_ON_INV_PO;

   if L_ie_on_inv_exists is not NULL then
      O_exists := 'Y';
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_IE_ON_INV_SHIPMENT',
                       'SHIPMENT'||'INVC_XREF',
                       'BILL TO LOC: '||to_char(I_wh));
      open C_IE_ON_INV_SHIPMENT;

      SQL_LIB.SET_MARK('FETCH',
                       'C_IE_ON_INV_SHIPMENT',
                       'SHIPMENT'||'INVC_XREF',
                       'BILL TO LOC: '||to_char(I_wh));
      fetch C_IE_ON_INV_SHIPMENT into L_ie_on_inv_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_IE_ON_INV_PO',
                       'SHIPMENT'||'INVC_XREF',
                       'BILL TO LOC: '||to_char(I_wh));
      close C_IE_ON_INV_SHIPMENT;

      if L_ie_on_inv_exists is not NULL then
         O_exists := 'Y';
      else
         O_exists := 'N';
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ENT_ON_INV;
------------------------------------------------------------------------
FUNCTION CHECK_SUPS_IMP_EXP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   VARCHAR2,
                            I_wh              IN       NUMBER)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)    := 'WH_ATTRIB_SQL.CHECK_SUPS_IMP_EXP';
   L_ie_sups    VARCHAR2(1)     := NULL;

   cursor C_SUPS_IMP_EXP is
      select 'x'
        from sups_imp_exp
       where import_id = I_wh
         and rownum = 1;
BEGIN

   if I_wh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_wh',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_SUPS_IMP_EXP',
                    'SUPS_IMP_EXP',
                    'IMPORT ID: '||to_char(I_wh));
   open C_SUPS_IMP_EXP;

   SQL_LIB.SET_MARK('FETCH',
                    'C_SUPS_IMP_EXP',
                    'SUPS_IMP_EXP',
                    'IMPORT ID: '||to_char(I_wh));
   fetch C_SUPS_IMP_EXP into L_ie_sups;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUPS_IMP_EXP',
                    'SUPS_IMP_EXP',
                    'IMPORT ID: '||to_char(I_wh));
   close C_SUPS_IMP_EXP;

   if L_ie_sups = 'x' then
      O_exists := 'Y';
   else
      O_exists := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             null);
      RETURN FALSE;
END CHECK_SUPS_IMP_EXP;
--------------------------------------------------------------------
FUNCTION DEL_WH_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)         := 'WH_ATTRIB_SQL.DEL_WH_ATTRIB';
   L_wh                 WH.WH%TYPE           := I_wh;
   L_table              VARCHAR2(30)         := 'WH';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_DEL_WH_ATTRIB IS
      SELECT 'X'
        FROM wh_l10n_ext
       WHERE wh = I_wh
         for update nowait;

BEGIN

   if L_wh is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'L_wh',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   L_table := 'WH_L10N_EXT';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_DEL_WH_ATTRIB',
                    'wh_l10n_ext',
                    'location: ' || to_char(L_wh));
   OPEN C_DEL_WH_ATTRIB;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_DEL_WH_ATTRIB',
                    'wh_l10n_ext',
                    'location: ' || to_char(L_wh));
   CLOSE C_DEL_WH_ATTRIB;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    'C_DEL_WH_ATTRIB',
                    'wh_l10n_ext',
                    'location: ' || to_char(L_wh));
   ---
   DELETE from wh_l10n_ext
    WHERE wh = L_wh;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_wh,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            L_program,
                                            null);
      return FALSE;

END DEL_WH_ATTRIB;
-----------------------------------------------------------------------------
FUNCTION LOCK_WH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_wh_id           IN       WH.WH%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'WH_ATTRIB_SQL.LOCK_WH';
   L_table         VARCHAR2(10);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_WH_ADD IS
      select 'x'
        from wh_add
       where wh = I_wh_id
         for update nowait;

   cursor C_LOCK_WH IS
      select 'x'
        from wh
       where wh = I_wh_id
         for update nowait;

BEGIN

   L_table := 'WH_ADD';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_WH_ADD',
                    L_table,
                    'wh: '||I_wh_id);
   open C_LOCK_WH_ADD;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_WH_ADD',
                    L_table,
                    'wh: '||I_wh_id);
   close C_LOCK_WH_ADD;
   ---
   L_table := 'WH';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_WH',
                    L_table,
                    'wh: '||I_wh_id);
   open C_LOCK_WH;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_WH',
                    L_table,
                    'wh: '||I_wh_id);
   close C_LOCK_WH;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            'wh: '||I_wh_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_WH;
-----------------------------------------------------------------------------
END WH_ATTRIB_SQL;
/
