CREATE OR REPLACE PACKAGE CORESVC_STORE_GRADE_GROUP AUTHID CURRENT_USER AS
----------------------------------------------------------------------------------------------
   template_key              CONSTANT VARCHAR2(255):= 'STORE_GRADE_GROUP_DATA';
   template_category         CODE_DETAIL.CODE%TYPE := 'RMSFND';
   action_new                VARCHAR2(25)          := 'NEW';
   action_mod                VARCHAR2(25)          := 'MOD';
   action_del                VARCHAR2(25)          := 'DEL';
   SG_sheet                  VARCHAR2(255)         := 'STORE_GRADE';
   SG$Action                 NUMBER                := 1;
   SG$COMMENTS               NUMBER                := 4;
   SG$STORE_GRADE            NUMBER                := 3;
   SG$STORE_GRADE_GROUP_ID   NUMBER                := 2;
   action_column             VARCHAR2(255)         := 'ACTION';
   TYPE SG_rec_tab IS TABLE OF STORE_GRADE%ROWTYPE;

   SGG_sheet                    VARCHAR2(255)      := 'STORE_GRADE_GROUP';
   SGG$Action                   NUMBER             := 1;
   SGG$BUYER                    NUMBER             := 4;
   SGG$STORE_GRADE_GROUP_DESC   NUMBER             := 3;
   SGG$STORE_GRADE_GROUP_ID     NUMBER             := 2;
   TYPE SGG_rec_tab IS TABLE OF STORE_GRADE_GROUP%ROWTYPE;

   SGS_sheet                  VARCHAR2(255)        := 'STORE_GRADE_STORE';
   SGS$Action                 NUMBER               := 1;
   SGS$STORE_GRADE            NUMBER               := 4;
   SGS$STORE                  NUMBER               := 3;
   SGS$STORE_GRADE_GROUP_ID   NUMBER               := 2;
   sheet_name_trans           S9T_PKG.trans_map_typ;
   TYPE SGS_rec_tab IS TABLE OF STORE_GRADE_STORE%ROWTYPE;
   
   SGG_TL_sheet                    VARCHAR2(255)      := 'STORE_GRADE_GROUP_TL';
   SGG_TL$Action                   NUMBER             := 1;
   SGG_TL$LANG                     NUMBER             := 2;
   SGG_TL$STORE_GRADE_GROUP_ID     NUMBER             := 3;
   SGG_TL$STORE_GRADE_GROUP_DESC   NUMBER             := 4;
   
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
return BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
return VARCHAR2;
----------------------------------------------------------------------------------------------
END CORESVC_STORE_GRADE_GROUP;
/