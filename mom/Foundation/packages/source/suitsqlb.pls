CREATE OR REPLACE PACKAGE BODY SUPP_ITEM_SQL AS
------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
-- Function Name: LOCK_ITEM_SUPPLIER
-- Purpose      : This function will lock the ITEM_SUPPLIER table for update or delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_table_state   IN OUT VARCHAR2,
                            I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                            I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
---------------------------------------------------------------------
FUNCTION ITEM_SUPP_COUNTRY_EXISTS (O_error_message  IN OUT VARCHAR2,
                                   O_exist          IN OUT BOOLEAN,
                                   I_item           IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                   I_supplier       IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                   I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   L_dummy       VARCHAR2(1);
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS';

   cursor C_ITEM_SUPP_COUNTRY is
      select 'x'
        from item_supp_country isc,
             sups s
       where isc.item              = I_item
         and (I_supplier is NULL
              or s.supplier = I_supplier
              or s.supplier_parent = I_supplier)
         and isc.supplier          = s.supplier
         and isc.origin_country_id = I_origin_country
         and rownum = 1;
BEGIN
   O_exist := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item
                    ||' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   open C_ITEM_SUPP_COUNTRY;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item
                    ||' supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   fetch C_ITEM_SUPP_COUNTRY into L_dummy;
   if C_ITEM_SUPP_COUNTRY%FOUND then
      O_exist := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item
                    ||' supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   close C_ITEM_SUPP_COUNTRY;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_SUPP_COUNTRY_EXISTS;
------------------------------------------------------------------------------
FUNCTION ITEM_PART_COUNTRY_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exist            IN OUT   BOOLEAN,
                                   I_item             IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                   I_partner_id       IN       PARTNER.PARTNER_ID%TYPE,
                                   I_origin_country   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                   I_partner_type     IN       PARTNER.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN IS
   L_partner_exists VARCHAR2(1);
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.ITEM_PART_COUNTRY_EXISTS';

   cursor C_CHECK_SUPP_HIER_LVL_1 is
      select 'Y'
        from item_supp_country
       where supp_hier_lvl_1   = I_partner_id
         and origin_country_id = I_origin_country;

   cursor C_CHECK_SUPP_HIER_LVL_2 is
      select 'Y'
        from item_supp_country
       where supp_hier_lvl_2   = I_partner_id
         and origin_country_id = I_origin_country;

   cursor C_CHECK_SUPP_HIER_LVL_3 is
      select 'Y'
        from item_supp_country
       where supp_hier_lvl_3   = I_partner_id
         and origin_country_id = I_origin_country;

BEGIN
   O_exist := FALSE;
   ---
   if I_partner_type = 'S1' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_SUPP_HIER_LVL_1','ITEM_SUPP_COUNTRY','supp_hier_lvl_1: '||I_partner_id);
      open  C_CHECK_SUPP_HIER_LVL_1;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_SUPP_HIER_LVL_1','ITEM_SUPP_COUNTRY','supp_hier_lvl_1: '||I_partner_id);
      fetch C_CHECK_SUPP_HIER_LVL_1 into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUPP_HIER_LVL_1','ITEM_SUPP_COUNTRY','supp_hier_lvl_1: '||I_partner_id);
      close C_CHECK_SUPP_HIER_LVL_1;
      ---
      if L_partner_exists = 'Y' then
         O_exist := TRUE;
      end if;
      ---
   elsif I_partner_type = 'S2' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_SUPP_HIER_LVL_2','ITEM_SUPP_COUNTRY','supp_hier_lvl_2: '||I_partner_id);
      open  C_CHECK_SUPP_HIER_LVL_2;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_SUPP_HIER_LVL_2','ITEM_SUPP_COUNTRY','supp_hier_lvl_2: '||I_partner_id);
      fetch C_CHECK_SUPP_HIER_LVL_2 into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUPP_HIER_LVL_2','ITEM_SUPP_COUNTRY','supp_hier_lvl_2: '||I_partner_id);
      close C_CHECK_SUPP_HIER_LVL_2;
      ---
      if L_partner_exists = 'Y' then
         O_exist := TRUE;
      end if;
      ---
   else    --I_partner_type = 'S3'
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_SUPP_HIER_LVL_3','ITEM_SUPP_COUNTRY','supp_hier_lvl_3: '||I_partner_id);
      open  C_CHECK_SUPP_HIER_LVL_3;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_SUPP_HIER_LVL_3','ITEM_SUPP_COUNTRY','supp_hier_lvl_3: '||I_partner_id);
      fetch C_CHECK_SUPP_HIER_LVL_3 into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUPP_HIER_LVL_3','ITEM_SUPP_COUNTRY','supp_hier_lvl_3: '||I_partner_id);
      close C_CHECK_SUPP_HIER_LVL_3;
      ---
      if L_partner_exists = 'Y' then
         O_exist := TRUE;
      end if;
      ---
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_PART_COUNTRY_EXISTS;
------------------------------------------------------------------------------
FUNCTION COUNTRY_EXISTS (O_error_message IN OUT VARCHAR2,
                         O_exist         IN OUT BOOLEAN,
                         I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                         I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   L_dummy       VARCHAR2(1);
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.COUNTRY_EXISTS';

   cursor C_ITEM_SUPP_COUNTRY is
      select 'x'
        from item_supp_country
       where item     = I_item
         and supplier = nvl(I_supplier, supplier);

BEGIN
   O_exist := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||
                    I_item||' Supplier: '||to_char(I_supplier));
   open C_ITEM_SUPP_COUNTRY;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||
                    I_item||' Supplier: '||to_char(I_supplier));
   fetch C_ITEM_SUPP_COUNTRY into L_dummy;
   if C_ITEM_SUPP_COUNTRY%FOUND then
      O_exist := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||
                    I_item||' Supplier: '||to_char(I_supplier));
   close C_ITEM_SUPP_COUNTRY;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END COUNTRY_EXISTS;
-----------------------------------------------------------------------
-- Description: Get unit cost from item/supplier/country table for a given
--              item and supplier numbers.
------------------------------------------------------------------------
FUNCTION GET_COST ( O_error_message        IN OUT VARCHAR2,
                    O_unit_cost            IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                    I_item                 IN     ITEM_SUPPLIER.ITEM%TYPE,
                    I_supplier             IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                    I_origin_country       IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                    I_location             IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                    I_delivery_country_id  IN     ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE DEFAULT NULL,
                    I_new_item_loc_ind     IN     VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'SUPP_ITEM_SQL.GET_COST';
   L_loc_exists           BOOLEAN      := FALSE;
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   ---
   cursor C_ITEM_COST_LOC is
      select unit_cost
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country
         and loc               = I_location;

   cursor C_ITEM_COST is
      select unit_cost
        from item_supp_country
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country;

   cursor C_ITEM_COST_ICH is
      select decode(nvl(ca.default_po_cost, 'BC'), 'BC', 
                    ich.base_cost, ich.negotiated_item_cost) unit_cost
        from item_cost_head ich,
             country_attrib ca 
       where ich.item                 = I_item
         and ich.supplier             = I_supplier
         and ich.origin_country_id    = I_origin_country
         and ca.country_id            = ich.origin_country_id
         and (ich.delivery_country_id = I_delivery_country_id
              or prim_dlvy_ctry_ind  = 'Y');
         
   cursor C_ITEM_COST_PRIM is
      select unit_cost
        from item_supp_country
       where supplier            = I_supplier
         and item                = I_item
         and primary_country_ind = 'Y';
         
   cursor C_ITEM_COST_PRIM_ICH is
      select decode(nvl(ca.default_po_cost, 'BC'), 'BC', 
                    ich.base_cost, ich.negotiated_item_cost) unit_cost
        from item_supp_country isc,
             item_cost_head    ich,
             country_attrib    ca
       where isc.supplier  = I_supplier
         and ich.supplier  = isc.supplier
         and isc.item      = I_item
         and ich.item      = isc.item
         and ich.origin_country_id = isc.origin_country_id
         and ich.origin_country_id = ca.country_id
         and isc.primary_country_ind = 'Y'         
         and (delivery_country_id = I_delivery_country_id
              or prim_dlvy_ctry_ind  = 'Y');         
       

BEGIN
   
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;     

   if I_location is not NULL then

      SQL_LIB.SET_MARK('OPEN',
                       'C_ITEM_COST_LOC',
                       'ITEM_SUPP_COUNTRY_LOC',
                       'Supplier: '||to_char(I_supplier)|| 'Item: ' || I_item);
      open  C_ITEM_COST_LOC;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ITEM_COST_LOC',
                       'ITEM_SUPP_COUNTRY_LOC',
                       'Supplier: ' || to_char(I_supplier) || 'Item: ' || I_item);
      fetch C_ITEM_COST_LOC into O_unit_cost;

      if O_unit_cost is NULL then
         L_loc_exists := FALSE;
      else
         L_loc_exists := TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_COST_LOC',
                       'ITEM_SUPP_COUNTRY_LOC',
                       'Supplier: ' || to_char(I_supplier) || 'Item: ' || I_item);
      close C_ITEM_COST_LOC;

   end if;

   if L_loc_exists = FALSE then
      if I_origin_country is not NULL then
         if (L_system_options_rec.default_tax_type in ('SVAT', 'SALES')) then
         
            SQL_LIB.SET_MARK('OPEN',
                             'C_ITEM_COST',
                             'ITEM_SUPP_COUNTRY',
                             NULL);
            open  C_ITEM_COST;
            SQL_LIB.SET_MARK('FETCH',
                             'C_ITEM_COST',
                             'ITEM_SUPP_COUNTRY',
                             NULL);
            fetch C_ITEM_COST into O_unit_cost;
            ---
            if C_ITEM_COST%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE','C_ITEM_COST','ITEM_SUPP_COUNTRY',NULL);
               close C_ITEM_COST;
               O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_SUPP_COUNTRY',
                                                      I_item,
                                                      to_char(I_supplier),
                                                      I_origin_country);
               return FALSE;
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_ITEM_COST',
                             'ITEM_SUPPLIER',
                             NULL);
            close C_ITEM_COST;
         elsif L_system_options_rec.default_tax_type = 'GTAX' then
            if I_new_item_loc_ind = 'Y' then
               SQL_LIB.SET_MARK('OPEN',
                                'C_ITEM_COST_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               open  C_ITEM_COST_ICH;
               SQL_LIB.SET_MARK('FETCH',
                                'C_ITEM_COST_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               fetch C_ITEM_COST_ICH into O_unit_cost;
               ---
               if C_ITEM_COST_ICH%NOTFOUND then
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_ITEM_COST',
                                   'ITEM_SUPP_COUNTRY',
                                   NULL);
                  open  C_ITEM_COST;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_ITEM_COST',
                                   'ITEM_SUPP_COUNTRY',
                                   NULL);
                  fetch C_ITEM_COST into O_unit_cost;
                  ---
                  if C_ITEM_COST%NOTFOUND then
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_ITEM_COST',
                                      'ITEM_SUPP_COUNTRY',
                                      NULL);
                     close C_ITEM_COST;
                     O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_SUPP_COUNTRY',
                                                            I_item,
                                                            to_char(I_supplier),
                                                            I_origin_country);
                     return FALSE;
                  end if;
                  ---
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_ITEM_COST',
                                   'ITEM_SUPPLIER',
                                   NULL);
                  close C_ITEM_COST;
               end if;
            else
               SQL_LIB.SET_MARK('OPEN',
                                'C_ITEM_COST_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               open  C_ITEM_COST_ICH;
               SQL_LIB.SET_MARK('FETCH',
                                'C_ITEM_COST_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               fetch C_ITEM_COST_ICH into O_unit_cost;
               ---
               if C_ITEM_COST_ICH%NOTFOUND then
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_ITEM_COST_ICH',
                                   'ITEM_COST_HEAD',
                                   NULL);
                  close C_ITEM_COST_ICH;
                  O_error_message := SQL_LIB.CREATE_MSG('ITEM_COST_HEAD',
                                                         I_item,
                                                         to_char(I_supplier),
                                                         I_origin_country);
                  return FALSE;
               end if;
               ---
               SQL_LIB.SET_MARK('CLOSE',
                                'C_ITEM_COST_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               close C_ITEM_COST_ICH;
            end if;
         end if;
      else
         if (L_system_options_rec.default_tax_type in ('SVAT', 'SALES')) then
            SQL_LIB.SET_MARK('OPEN',
                             'C_ITEM_COST_PRIM',
                             'ITEM_SUPP_COUNTRY',
                             'Supplier: '||to_char(I_supplier)|| 'Item: ' || I_item);
            open  C_ITEM_COST_PRIM;
            SQL_LIB.SET_MARK('FETCH',
                             'C_ITEM_COST_PRIM',
                             'ITEM_SUPP_COUNTRY',
                             'Supplier: ' || to_char(I_supplier) || 'Item: ' || I_item);
            fetch C_ITEM_COST_PRIM into O_unit_cost;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_ITEM_COST_PRIM',
                             'ITEM_SUPP_COUNTRY',
                             'Supplier: ' || to_char(I_supplier) || 'Item: ' || I_item);
            close C_ITEM_COST_PRIM;
         elsif L_system_options_rec.default_tax_type = 'GTAX' then
            if I_new_item_loc_ind = 'Y' then
               SQL_LIB.SET_MARK('OPEN',
                                'C_ITEM_COST_PRIM_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               open  C_ITEM_COST_PRIM_ICH;
               SQL_LIB.SET_MARK('FETCH',
                                'C_ITEM_COST_PRIM_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               fetch C_ITEM_COST_PRIM_ICH into O_unit_cost;
               ---
               if C_ITEM_COST_PRIM_ICH%NOTFOUND then
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_ITEM_COST_PRIM',
                                   'ITEM_SUPP_COUNTRY',
                                   'Supplier: '||to_char(I_supplier)|| 'Item: ' || I_item);
                  open  C_ITEM_COST_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_ITEM_COST_PRIM',
                                   'ITEM_SUPP_COUNTRY',
                                   'Supplier: ' || to_char(I_supplier) || 'Item: ' || I_item);
                  fetch C_ITEM_COST_PRIM into O_unit_cost;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_ITEM_COST_PRIM',
                                   'ITEM_SUPP_COUNTRY',
                                   'Supplier: ' || to_char(I_supplier) || 'Item: ' || I_item);
                  close C_ITEM_COST_PRIM;
               end if;
               ---
               SQL_LIB.SET_MARK('CLOSE',
                                'C_ITEM_COST_PRIM_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               close C_ITEM_COST_PRIM_ICH;
            else
               SQL_LIB.SET_MARK('OPEN',
                                'C_ITEM_COST_PRIM_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               open  C_ITEM_COST_PRIM_ICH;
               SQL_LIB.SET_MARK('FETCH',
                                'C_ITEM_COST_PRIM_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               fetch C_ITEM_COST_PRIM_ICH into O_unit_cost;
               ---
               SQL_LIB.SET_MARK('CLOSE',
                                'C_ITEM_COST_PRIM_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               close C_ITEM_COST_PRIM_ICH;
            end if;
         end if;
       end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_COST;
------------------------------------------------------------------------
-- Description: Get the primary supplier and unit cost from
--              item/supplier/country or item/supplier/country/loc
--          table for a given item number or item/location.
------------------------------------------------------------------------
FUNCTION GET_PRI_SUP_COST ( O_error_message IN OUT VARCHAR2,
                            O_supplier      IN OUT ITEM_SUPPLIER.SUPPLIER%TYPE,
                            O_unit_cost_sup IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                            I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                            I_location      IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                            I_retrieve_ebc  IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN is

   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.GET_PRI_SUP_COST';
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE;
   L_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_sellable_ind       ITEM_MASTER.SELLABLE_IND%TYPE;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE;
   QUICKEXIT     EXCEPTION;

   cursor C_ITEM_LOC_COST is
   select iscl.supplier,
          decode(I_retrieve_ebc, 'N', iscl.unit_cost, iscl.extended_base_cost)
     from item_loc il,
          item_supp_country_loc iscl
    where iscl.item = I_item
      and iscl.loc  = I_location
      and il.item = iscl.item
      and il.primary_supp = iscl.supplier
      and il.primary_cntry = iscl.origin_country_id
      and il.loc   = iscl.loc;

   cursor C_ITEM_COST is
      select supplier,
             decode(I_retrieve_ebc,'N', unit_cost, extended_base_cost)
        from item_supp_country
       where item = I_item
         and primary_supp_ind = 'Y'
         and primary_country_ind = 'Y';
   ---
BEGIN

   if not ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                            L_pack_ind,
                            L_sellable_ind,
                            L_orderable_ind,
                            L_pack_type,
                            I_item) then
      return FALSE;
   end if;

   O_unit_cost_sup := NULL;
   O_supplier := NULL;
   if L_pack_ind = 'Y' and L_orderable_ind = 'N' then
      raise QUICKEXIT;
   end if;

   if I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_COST','ITEM_SUPP_COUNTRY_LOC',
                'Location: '||to_char(I_location)|| 'Item: ' || I_item);
      open  C_ITEM_LOC_COST;
      SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_COST','ITEM_SUPP_COUNTRY_LOC',
                'location: '||to_char(I_location)|| 'Item: ' || I_item);
      fetch C_ITEM_LOC_COST into O_supplier,
                                 O_unit_cost_sup;
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_COST','ITEM_SUPP_COUNTRY_LOC',
                'location: '||to_char(I_location)|| 'Item: ' || I_item);
      close C_ITEM_LOC_COST;
   end if;

   if O_supplier is NULL then  -- if I_location was NULL, or the above cursor did
                               -- did not return a value, get the primiary supplier/cost
                               -- for the item.
       SQL_LIB.SET_MARK('OPEN','C_ITEM_COST','ITEM_SUPPLIER','Item: ' || I_item);
      open  C_ITEM_COST;
      SQL_LIB.SET_MARK('FETCH','C_ITEM_COST','ITEM_SUPPLIER','Item: ' || I_item);
      fetch C_ITEM_COST into O_supplier,
                             O_unit_cost_sup;
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_COST','ITEM_SUPPLIER','Item: ' || I_item);
      close C_ITEM_COST;
   end if;

   return TRUE;
EXCEPTION
   when QUICKEXIT then
      return TRUE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_PRI_SUP_COST;
--------------------------------------------------------------------------------
FUNCTION CHECK_COUNTRY_DELETE( O_error_message  IN OUT VARCHAR2,
                               O_exist          IN OUT BOOLEAN,
                               I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                               I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                               I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.CHECK_COUNTRY_DELETE';
   L_dummy       VARCHAR2(1);
   ---

   cursor C_ORDER_ITEMS_EXISTS is
      select 'x'
        from ordsku o,
             ordhead oh
       where o.order_no           = oh.order_no
         and oh.supplier          = I_supplier
         and o.origin_country_id  = I_origin_country
         and o.item               = I_item;
   ---
   cursor C_ORDLOC_WKSHT_EXISTS is
      select 'x'
        from ordloc_wksht ow,
             ordhead oh
       where ow.order_no          = oh.order_no
         and oh.supplier          = I_supplier
         and ow.origin_country_id = I_origin_country
         and ow.item              = I_item;

   ---
   cursor C_ITEM_SUPP_COUNTRY_EXISTS is
      select 'x'
        from item_supp_country
       where item                = I_item
         and supplier            = I_supplier
         and origin_country_id   = I_origin_country
         and primary_country_ind = 'Y';
   ---
   cursor C_PACK_ITEM_EXISTS is
      select 'x'
        from packitem p,
             item_supp_country ic
       where p.item               = I_item
         and p.pack_no            = ic.item
         and ic.supplier          = I_supplier
         and ic.origin_country_id = I_origin_country;
   ---
   cursor C_SUPP_COUNTRY_ON_REPL is
      select 'x'
        from repl_item_loc
       where item                  = I_item
         and primary_repl_supplier = I_supplier
         and origin_country_id     = I_origin_country;

   ---
   cursor C_REPL_SUPP_DIST is
      select 'x'
        from repl_item_loc_supp_dist
       where item                  = I_item
         and supplier              = I_supplier
         and origin_country_id     = I_origin_country;
   ---
   cursor C_ITEM_LOC_PRIMARY is
      select 'x'
        from item_loc
       where item          = I_item
         and primary_supp  = I_supplier
         and primary_cntry = I_origin_country;
BEGIN
   O_exist := FALSE;
   ---
   -- check the ordsku table for item/supplier/origin country combos
   ---

   SQL_LIB.SET_MARK('OPEN','C_ORDER_ITEMS_EXISTS','ORDSKU, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   open  C_ORDER_ITEMS_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_ORDER_ITEMS_EXISTS','ORDSKU, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   fetch C_ORDER_ITEMS_EXISTS into L_dummy;
   ---
   if C_ORDER_ITEMS_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ORDER_ITEMS_EXISTS','ORDSKU, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_ORDER_ITEMS_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_ON_ORDER', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ORDER_ITEMS_EXISTS','ORDSKU, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   close C_ORDER_ITEMS_EXISTS;
   ---
   -- Check the ordloc_wksht table for item/supplier/origin country combos
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDLOC_WKSHT_EXISTS','ORDLOC_WKSHT, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   open  C_ORDLOC_WKSHT_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_ORDLOC_WKSHT_EXISTS','ORDLOC_WKSHT, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   fetch C_ORDLOC_WKSHT_EXISTS into L_dummy;
   ---
   if C_ORDLOC_WKSHT_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_WKSHT_EXISTS','ORDLOC_WKSHT, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_ORDLOC_WKSHT_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_ON_ORDER', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_WKSHT_EXISTS','ORDLOC_WKSHT, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   close C_ORDLOC_WKSHT_EXISTS;

    -- Check if I_origin_country is being used for this SKU on repl_item_loc
    ---
   SQL_LIB.SET_MARK('OPEN', 'C_SUPP_COUNTRY_ON_REPL', 'REPL_ITEM_LOC',
                       'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
   open  C_SUPP_COUNTRY_ON_REPL;
   SQL_LIB.SET_MARK('FETCH', 'C_SUPP_COUNTRY_ON_REPL', 'REPL_ITEM_LOC',
                       'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
   fetch C_SUPP_COUNTRY_ON_REPL into L_dummy;
   if C_SUPP_COUNTRY_ON_REPL%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_COUNTRY_ON_REPL', 'REPL_ITEM_LOC',
                          'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
      close C_SUPP_COUNTRY_ON_REPL;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_COUNTRY_ON_REPL', 'REPL_ITEM_LOC',
                          'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
      close C_SUPP_COUNTRY_ON_REPL;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_COUNTRY_REPL', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   -- Check if I_origin_country is being used for this SKU on repl_item_loc_supp_dist
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_REPL_SUPP_DIST', 'REPL_ITEM_LOC_SUPP_DIST',
                       'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
   open  C_REPL_SUPP_DIST;
   SQL_LIB.SET_MARK('FETCH', 'C_REPL_SUPP_DIST', 'REPL_ITEM_LOC_SUPP_DIST',
                       'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
   fetch C_REPL_SUPP_DIST into L_dummy;
   if C_REPL_SUPP_DIST%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_REPL_SUPP_DIST', 'REPL_ITEM_LOC_SUPP_DIST',
                          'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
      close C_REPL_SUPP_DIST;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_REPL_SUPP_DIST', 'REPL_ITEM_LOC_SUPP_DIST',
                          'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
      close C_REPL_SUPP_DIST;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_COUNTRY_REPL_DIST', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   -- check the item_supp_country table for item/supplier/origin country combos
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPP_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   open  C_ITEM_SUPP_COUNTRY_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPP_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   fetch C_ITEM_SUPP_COUNTRY_EXISTS into L_dummy;
   ---
   if C_ITEM_SUPP_COUNTRY_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_ITEM_SUPP_COUNTRY_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_PRI_COUNTRY', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   close C_ITEM_SUPP_COUNTRY_EXISTS;
   ---
   -- Check if I_item is not a component of a pack that is currently associated with the
   -- Supplier/Origin Country being deleted
   ---
   ---
   SQL_LIB.SET_MARK('OPEN','C_PACK_ITEM_EXISTS','PACKITEM, ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   open  C_PACK_ITEM_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_PACK_ITEM_EXISTS','PACKITEM, ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   fetch C_PACK_ITEM_EXISTS into L_dummy;
   ---
   if C_PACK_ITEM_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_PACK_ITEM_EXISTS','PACKITEM, ITEM_SUPP_COUNTRY',
                          'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                          ||' Country: '|| I_origin_country);
      close C_PACK_ITEM_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_EXISTS_ON_PACK', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_PACK_ITEM_EXISTS','PACKITEM, ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   close C_PACK_ITEM_EXISTS;
   ---
   -- Check if the supplier/origin country are the primary on ITEM_LOC.
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_PRIMARY','ITEM_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   open  C_ITEM_LOC_PRIMARY;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_PRIMARY','ITEM_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   fetch C_ITEM_LOC_PRIMARY into L_dummy;
   ---
   if C_ITEM_LOC_PRIMARY%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_PRIMARY','ITEM_LOC',
                          'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                          ||' Country: '|| I_origin_country);
      close C_ITEM_LOC_PRIMARY;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_PRIM_COUNTRY_LOC', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_PRIMARY','ITEM_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   close C_ITEM_LOC_PRIMARY;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END CHECK_COUNTRY_DELETE;
--------------------------------------------------------------------------------
FUNCTION LOCK_COUNTRY_DELETE(O_error_message  IN OUT VARCHAR2,
                             I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                             I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                             I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(64)   := 'SUPP_ITEM_SQL.LOCK_COUNTRY_DELETE';
   L_table        VARCHAR2(30);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_COST_SUSP_SUP_DETAIL is
      select 'x'
        from cost_susp_sup_detail
       where supplier         = I_supplier
         and origin_country_id = I_origin_country
         and item = I_item
         for update nowait;
   ---
   cursor C_LOCK_COST_DETAIL_LOC is
      select 'x'
        from cost_susp_sup_detail_loc
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and item = I_item
         for update nowait;
   ---
   cursor C_LOCK_COST_SUSP_SUP_HEAD_CFA is
      select 'x'
        from cost_susp_sup_head_cfa_ext chcfa
         where exists (select 'x'
                         from cost_susp_sup_detail cd
                        where cd.supplier          = I_supplier
                          and cd.origin_country_id = I_origin_country
                          and cd.item              = I_item
                          and cd.cost_change       = chcfa.cost_change
                    union all
                       select 'x'
                         from cost_susp_sup_detail_loc cl
                        where cl.supplier          = I_supplier
                          and cl.origin_country_id = I_origin_country
                          and cl.item              = I_item
                          and cl.cost_change       = chcfa.cost_change)
           for update nowait;
   ---
   cursor C_LOCK_COST_SUSP_SUP_HEAD is
      select 'x'
        from cost_susp_sup_head ch
         where exists (select 'x'
                         from cost_susp_sup_detail cd
                        where cd.supplier          = I_supplier
                          and cd.origin_country_id = I_origin_country
                          and cd.item              = I_item
                          and cd.cost_change       = ch.cost_change
                    union all
                       select 'x'
                         from cost_susp_sup_detail_loc cl
                        where cl.supplier          = I_supplier
                          and cl.origin_country_id = I_origin_country
                          and cl.item              = I_item
                          and cl.cost_change       = ch.cost_change)
           for update nowait;
   ---
   cursor C_LOCK_ITEM_EXP_DETAIL is
      select 'x'
        from item_exp_detail ied
       where ied.supplier      = I_supplier
         and ied.item = I_item
         and ied.item_exp_seq in (select ieh.item_exp_seq
                                    from item_exp_head ieh
                                   where ieh.supplier          = I_supplier
                                     and ieh.origin_country_id = I_origin_country
                                     and ieh.item              = I_item)
         for update nowait;
   ---
   cursor C_LOCK_ITEM_EXP_HEAD is
      select 'x'
        from item_exp_head
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item = I_item
      for update nowait;
   ---
   cursor C_LOCK_DEAL_ITEMLOC_ITEM is
      select 'x'
        from deal_itemloc_item
       where origin_country_id = I_origin_country
         and item = I_item
         and deal_id in (select deal_id
                           from deal_head
                          where supplier = I_supplier)
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY_DIM is
      select 'x'
        from item_supp_country_dim
       where supplier = I_supplier
         and item     = I_item
         and origin_country = I_origin_country
         for update nowait;

  cursor C_LOCK_ITEM_SUPP_CTRY_BRACKET is
      select 'x'
        from item_supp_country_bracket_cost
       where supplier = I_supplier
         and item     = I_item
         and origin_country_id = I_origin_country
         for update nowait;

  cursor C_LOCK_ITM_SUPP_CTRY_LOC_CE is
      select 'x'
        from item_supp_country_loc_cfa_ext
       where supplier = I_supplier
         and item     = I_item
         and origin_country_id = I_origin_country
         for update nowait;

  cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
      select 'x'
        from item_supp_country_loc
       where supplier = I_supplier
         and item     = I_item
         and origin_country_id = I_origin_country
         for update nowait;
         
   cursor C_LOCK_REPL_ITEM_LOC_SUPP_DIST is
      select 'x'
        from repl_item_loc_supp_dist
       where supplier = I_supplier
         and item = I_item
         and origin_country_id = I_origin_country
         for update nowait;
  
   cursor C_LOCK_BUYER_WKSHT_MANUAL is
      select 'x'
        from repl_item_loc_supp_dist
       where supplier = I_supplier
         and item = I_item
         and origin_country_id = I_origin_country
         for update nowait;

BEGIN

   L_table := 'COST_SUSP_SUP_DETAIL';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COST_SUSP_DETAIL', 'COST_SUSP_SUP_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_COST_SUSP_SUP_DETAIL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COST_SUSP_DETAIL', 'COST_SUSP_SUP_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_COST_SUSP_SUP_DETAIL;
   ---
   L_table := 'COST_SUSP_SUP_DETAIL_LOC';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COST_DETAIL_LOC', 'COST_SUSP_SUP_DETAIL_LOC',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_COST_DETAIL_LOC;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COST_DETAIL_LOC', 'COST_SUSP_SUP_DETAIL_LOC',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_COST_DETAIL_LOC;
   ---
   L_table := 'COST_SUSP_SUP_HEAD_CFA_EXT';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COST_SUSP_HEAD_CFA_EXT', 'COST_SUSP_SUP_HEAD_CFA_EXT',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_COST_SUSP_SUP_HEAD_CFA;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COST_SUSP_HEAD_CFA_EXT', 'COST_SUSP_SUP_HEAD_CFA_EXT',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_COST_SUSP_SUP_HEAD_CFA;
   ---
   L_table := 'COST_SUSP_SUP_HEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COST_SUSP_HEAD', 'COST_SUSP_SUP_HEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_COST_SUSP_SUP_HEAD;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COST_SUSP_HEAD', 'COST_SUSP_SUP_HEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_COST_SUSP_SUP_HEAD;
   ---
   L_table := 'ITEM_EXP_DETAIL';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_EXP_DETAIL', 'ITEM_EXP_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_ITEM_EXP_DETAIL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_EXP_DETAIL', 'ITEM_EXP_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_ITEM_EXP_DETAIL;
   ---
   L_table := 'ITEM_EXP_HEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_EXP_HEAD', 'ITEM_EXP_HEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_ITEM_EXP_HEAD;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_EXP_HEAD', 'ITEM_EXP_HEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_ITEM_EXP_HEAD;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_CTRY_BRACKET',
                    'ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_ITEM_SUPP_CTRY_BRACKET;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_CTRY_BRACKET',
                    'ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_ITEM_SUPP_CTRY_BRACKET;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_LOC_CFA_EXT';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITM_SUPP_CTRY_LOC_CE','ITEM_SUPP_COUNTRY_LOC_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITM_SUPP_CTRY_LOC_CE;

   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITM_SUPP_CTRY_LOC_CE','ITEM_SUPP_COUNTRY_LOC_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITM_SUPP_CTRY_LOC_CE;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_LOC';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_LOC;

   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_LOC;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_DIM';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_DIM','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_DIM;

   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_DIM','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_DIM;
   ---
   L_table := 'DEAL_ITEMLOC_ITEM';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_DEAL_ITEMLOC_ITEM', 'DEAL_ITEMLOC_ITEM',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_DEAL_ITEMLOC_ITEM;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DEAL_ITEMLOC_ITEM', 'DEAL_ITEMLOC_ITEM',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_DEAL_ITEMLOC_ITEM;
   ---
   L_table := 'REPL_ITEM_LOC_SUPP_DIST';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_REPL_ITEM_LOC_SUPP_DIST','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_REPL_ITEM_LOC_SUPP_DIST','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
   ---
   L_table := 'BUYER_WKSHT_MANUAL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_BUYER_WKSHT_MANUAL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_BUYER_WKSHT_MANUAL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_BUYER_WKSHT_MANUAL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||
                       ' Country: '||I_origin_country);
   close C_LOCK_BUYER_WKSHT_MANUAL;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END LOCK_COUNTRY_DELETE;
--------------------------------------------------------------------------------
FUNCTION DELETE_COUNTRY_RELATIONS( O_error_message  IN OUT VARCHAR2,
                                   I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                   I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                   I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.DELETE_COUNTRY_RELATIONS';
   L_dummy       VARCHAR2(1);
   L_cost_change COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_sc_cost_event_tbl       OBJ_SC_COST_EVENT_TBL := OBJ_SC_COST_EVENT_TBL();
   ---
   ---
   cursor C_COST_CHANGE is
      select distinct cost_change
        from cost_susp_sup_detail
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and item              = I_item
   union all
      select distinct cost_change
        from cost_susp_sup_detail_loc
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and item              = I_item;
   ---
   cursor C_ITEM_SUPP_COUNTRY_LOC is
      select item,
             supplier,
             origin_country_id,
             loc
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country;
   ---
BEGIN
   if not LOCK_COUNTRY_DELETE(O_error_message ,
                              I_item,
                              I_supplier,
                              I_origin_country) then
      return FALSE;
   end if;
   --- Get all the locations for the item, supplier and the origin country.
   FOR C_rec in C_ITEM_SUPP_COUNTRY_LOC LOOP
      L_sc_cost_event_tbl.EXTEND;
      L_sc_cost_event_tbl(L_sc_cost_event_tbl.count)  := OBJ_SC_COST_EVENT_REC(C_rec.item,
                                                                               C_rec.loc,
                                                                               C_rec.supplier,
                                                                               C_rec.origin_country_id);
   END LOOP;
   ---
   -- Get the cost change associated with the item on cost_susp_sup_detail
   -- this will be used to delete from the header table
   ---
   FOR C_rec in C_COST_CHANGE LOOP
      L_cost_change := C_rec.cost_change;
      ---
      -- Delete all items with an item/supplier/origin country relationship
      -- for the item being deleted
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_DETAIL',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       ' Country: '||I_origin_country);
      delete from cost_susp_sup_detail
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and item              = I_item;
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_DETAIL_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       ' Country: '||I_origin_country);
      delete from cost_susp_sup_detail_loc
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and item              = I_item;
      ---
      -- if no more detail records exist for that cost change then delete from header table
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_HEAD_CFA_EXT',
                       'Cost_change: '||to_char(L_cost_change));
      delete from cost_susp_sup_head_cfa_ext
       where cost_change = L_cost_change
         and not exists (select 'x'
                           from cost_susp_sup_detail
                          where cost_change = L_cost_change
                      union all
                         select 'x'
                           from cost_susp_sup_detail_loc
                          where cost_change = L_cost_change);
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_HEAD',
                       'Cost_change: '||to_char(L_cost_change));
      delete from cost_susp_sup_head
       where cost_change = L_cost_change
         and not exists (select 'x'
                           from cost_susp_sup_detail
                          where cost_change = L_cost_change
                      union all
                         select 'x'
                           from cost_susp_sup_detail_loc
                          where cost_change = L_cost_change);
   END LOOP;
   ---
   -- Get the item exp seq no from item_exp_head to delete from detail
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_exp_detail ied
    where ied.supplier      = I_supplier
      and ied.item_exp_type = 'C'
      and ied.item = I_item
      and ied.item_exp_seq in (select ieh.item_exp_seq
                                 from item_exp_head ieh
                                where ieh.supplier          = I_supplier
                                  and ieh.origin_country_id = I_origin_country
                                  and ieh.item = I_item);
   ---
   -- Delete from item_exp_head
   --
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_exp_head
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item              = I_item;
   ---
   --- Delete from Item_Supp_Country_Bracket_Cost
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_supp_country_bracket_cost
    where supplier = I_supplier
      and origin_country_id = I_origin_country
      and item = I_item;
   ---
   --- Delete from Repl_Item_Loc_Supp_Dist
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from repl_item_loc_supp_dist
    where supplier = I_supplier
      and origin_country_id = I_origin_country
      and item = I_item;
   ---
   --- Delete from Buyer_Wksht_Manual
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from buyer_wksht_manual
    where supplier = I_supplier
      and origin_country_id = I_origin_country
      and item = I_item;
   ---
   if L_sc_cost_event_tbl.count > 0 then
      if not FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY (O_error_message,
                                                     L_cost_event_process_id,
                                                     future_cost_event_sql.remove_event,
                                                     L_sc_cost_event_tbl,
                                                     get_user) then
         return FALSE;
      end if;
   end if;
   ---
   --- Delete from ITEM_SUPP_COUNTRY_LOC_CFA_EXT
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_LOC_CFA_EXT', 'ITEM: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);

   delete from item_supp_country_loc_cfa_ext
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item              = I_item;
   
   ---
   --- Delete from Item_Supp_Country_Loc
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_supp_country_loc
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_supp_country_dim
    where supplier          = I_supplier
      and origin_country = I_origin_country
      and item = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','DEAL_ITEMLOC_ITEM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from deal_itemloc_item
    where origin_country_id = I_origin_country
      and item              = I_item
      and deal_id in (select deal_id
                        from deal_head
                       where supplier = I_supplier);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_COST_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from ITEM_COST_DETAIL
    where origin_country_id = I_origin_country
      and item              = I_item
      and supplier          = I_supplier;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_COST_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from ITEM_COST_HEAD
    where origin_country_id = I_origin_country
      and item              = I_item
      and supplier          = I_supplier;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END DELETE_COUNTRY_RELATIONS;
--------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER_DELETE(O_error_message  IN OUT VARCHAR2,
                               O_exist          IN OUT BOOLEAN,
                               I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                               I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   L_program       VARCHAR2(64)    := 'SUPP_ITEM_SQL.CHECK_SUPPLIER_DELETE';
   L_dummy         VARCHAR2(1);
   L_exist         VARCHAR2(1)                      := 'Y';
   L_contract_ind  SYSTEM_OPTIONS.CONTRACT_IND%TYPE;
   L_error_message VARCHAR2(255);
   L_pack_no       PACKITEM.PACK_NO%TYPE;
   ---
   cursor C_CHECK_SKU_PRIMARY is
      select 'x'
        from item_supplier isc,
             item_master im
       where isc.supplier         = I_supplier
         and isc.item             = I_item
         and isc.primary_supp_ind = 'Y'
         and isc.item             = im.item;
   ---
  cursor C_ORDER is
      select 'x'
        from ordsku os,
             ordhead oh
       where oh.supplier = I_supplier
         and oh.order_no = os.order_no
         and os.item     = I_item;
   ---
   cursor C_RTV is
      select 'x'
        from rtv_detail rd,
             rtv_head rh
       where rh.supplier     = I_supplier
         and rh.rtv_order_no = rd.rtv_order_no
         and rd.item         = I_item;
   ---
   cursor C_CONTRACT_COST is
      select 'x'
        from contract_cost cc,
             contract_header ch
       where ch.supplier    = I_supplier
         and ch.contract_no = cc.contract_no
         and cc.item        = I_item;
   ---
   cursor C_CONTRACT_DETAIL is
      select 'x'
        from contract_detail cd,
             contract_header ch
       where ch.supplier    = I_supplier
         and ch.contract_no = cd.contract_no
         and cd.item        = I_item;

   ---
   cursor C_CHECK_PACK_SUPP_EXISTS is
      select v.pack_no
        from item_supplier i,
             v_packsku_qty v
       where i.supplier  = I_supplier
         and v.pack_no   = i.item
         and v.item       = I_item;
   ---
   cursor C_CHECK_ITEM_LOC is
      select 'x'
        from item_loc
       where item         = I_item
         and primary_supp = I_supplier;
   ---
   cursor C_CHECK_REPL_ITEM_LOC is
      select 'x'
        from repl_item_loc
       where item                  = I_item
         and primary_repl_supplier = I_supplier;
   ---
   cursor C_CHECK_PACK_ITEM is
      select 'x'
        from packitem p,
             repl_item_loc r
       where p.pack_no               = I_item
         and r.item                  = p.item
         and r.primary_repl_supplier = I_supplier;

   ---
   cursor C_CHECK_REPL_SUPP_DIST is
      select 'x'
        from repl_item_loc_supp_dist
       where item                  = I_item
         and supplier              = I_supplier;

BEGIN
   O_exist := FALSE;
   ---
   -- If I_supplier is the primary supplier for I_item and I_item is at or
   -- above the transaction level then it cannot be deleted
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SKU_PRIMARY', 'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   open C_CHECK_SKU_PRIMARY;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SKU_PRIMARY', 'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   fetch C_CHECK_SKU_PRIMARY into L_dummy;
   if C_CHECK_SKU_PRIMARY%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SKU_PRIMARY', 'ITEM_SUPPLIER',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      close C_CHECK_SKU_PRIMARY;
      O_exist := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_PRI_SUP',
                                             NULL,
                                             NULL,
                                             NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SKU_PRIMARY', 'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_CHECK_SKU_PRIMARY;

   ---
   -- Check the purchasing module for item/supplier rel. on orders
   ---

   SQL_LIB.SET_MARK('OPEN', 'C_ORDER', 'ORDSKU, ORDHEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   open C_ORDER;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDER', 'ORDSKU, ORDHEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   fetch C_ORDER into L_dummy;
   if C_ORDER%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'ORDSKU, ORDHEAD',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      close C_ORDER;
      O_exist := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SKU_SUPP_ORD',
                                             NULL,
                                             NULL,
                                             NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'ORDSKU, ORDHEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_ORDER;
   ---
   -- Check the rtv module
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_RTV', 'RTV_HEAD, RTV_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   open C_RTV;
   SQL_LIB.SET_MARK('FETCH', 'C_RTV', 'RTV_HEAD, RTV_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   fetch C_RTV into L_dummy;
   if C_RTV%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_RTV', 'RTV_HEAD, RTV_DETAIL',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      close C_RTV;
      O_exist := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SKU_SUPP_RTV',
                                             NULL,
                                             NULL,
                                             NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_RTV', 'RTV_HEAD, RTV_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_RTV;
   ---
   -- Check system options for contracting indicator; If it is yes then
   -- Check the contracting module (cost and detail tables)
   ---
   if not SYSTEM_OPTIONS_SQL.GET_CONTRACT_IND(O_error_message,
                                              L_contract_ind) then
      return FALSE;
   end if;
   ---
   if L_contract_ind  = 'Y' then
      SQL_LIB.SET_MARK('OPEN', 'C_CONTRACT_COST', 'CONTRACT_HEADER, CONTRACT_COST',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      open C_CONTRACT_COST;
      SQL_LIB.SET_MARK('FETCH', 'C_CONTRACT_COST', 'CONTRACT_HEADER, CONTRACT_COST',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      fetch C_CONTRACT_COST into L_dummy;
      if C_CONTRACT_COST%FOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT_COST', 'CONTRACT_HEADER, CONTRACT_COST',
                          'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
         close C_CONTRACT_COST;
         O_exist := TRUE;
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SKU_SUPP_CONT',
                                                NULL,
                                                NULL,
                                                NULL);
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT_COST', 'CONTRACT_HEADER, CONTRACT_COST',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_CONTRACT_COST;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_CONTRACT_DETAIL', 'CONTRACT_HEADER, CONTRACT_DETAIL',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      open C_CONTRACT_DETAIL;
      SQL_LIB.SET_MARK('FETCH', 'C_CONTRACT_DETAIL', 'CONTRACT_HEADER, CONTRACT_DETAIL',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      fetch C_CONTRACT_DETAIL into L_dummy;
      if C_CONTRACT_DETAIL%FOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT_DETAIL', 'CONTRACT_HEADER, CONTRACT_DETAIL',
                          'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
         close C_CONTRACT_DETAIL;
         O_exist := TRUE;
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SKU_SUPP_CONT',
                                                NULL,
                                                NULL,
                                                NULL);
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT_DETAIL', 'CONTRACT_HEADER, CONTRACT_DETAIL',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_CONTRACT_DETAIL;
   end if;
   ---
   -- Check if supplier exists on the item location table

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ITEM_LOC', 'ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   open C_CHECK_ITEM_LOC;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ITEM_LOC', 'ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   fetch C_CHECK_ITEM_LOC into L_dummy;
   if C_CHECK_ITEM_LOC%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM_LOC', 'ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
      close C_CHECK_ITEM_LOC;
      O_exist         := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DELETE_PRIM_SUP_LOC',NULL, NULL, NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM_LOC', 'ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   close C_CHECK_ITEM_LOC;
   ---
   -- Check if supplier is on any replenishment tables
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_REPL_ITEM_LOC', 'REPL_ITEM_LOC',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   open C_CHECK_REPL_ITEM_LOC;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_REPL_ITEM_LOC', 'REPL_ITEM_LOC',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   fetch C_CHECK_REPL_ITEM_LOC into L_dummy;
   if C_CHECK_REPL_ITEM_LOC%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_REPL_ITEM_LOC', 'REPL_ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
      close C_CHECK_REPL_ITEM_LOC;
      O_exist         := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_PRIM_SUP_REPL',NULL, NULL, NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_REPL_ITEM_LOC', 'REPL_ITEM_LOC',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   close C_CHECK_REPL_ITEM_LOC;
   ---
   -- Check if the supplier is the primary replenishment supplier for the component item of packitem
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_PACK_ITEM', 'PACKITEM, REPL_ITEM_LOC',
                    'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   open C_CHECK_PACK_ITEM;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_PACK_ITEM', 'PACKITEM, REPL_ITEM_LOC',
                    'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   fetch C_CHECK_PACK_ITEM into L_dummy;
   if C_CHECK_PACK_ITEM%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_PACK_ITEM', 'PACKITEM, REPL_ITEM_LOC',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_CHECK_PACK_ITEM;
      O_exist         := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_PRIM_SUP_REPL',NULL,NULL,NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_PACK_ITEM', 'PACKITEM, REPL_ITEM_LOC',
                    'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   close C_CHECK_PACK_ITEM;
   ---
   -- Check if supplier is on repl_item_loc_supp_dist table
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_REPL_SUPP_DIST', 'REPL_ITEM_LOC_SUPP_DIST',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   open C_CHECK_REPL_SUPP_DIST;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_REPL_SUPP_DIST', 'REPL_ITEM_LOC_SUPP_DIST',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   fetch C_CHECK_REPL_SUPP_DIST into L_dummy;
   if C_CHECK_REPL_SUPP_DIST%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_REPL_SUPP_DIST', 'REPL_ITEM_LOC_SUPP_DIST',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
      close C_CHECK_REPL_SUPP_DIST;
      O_exist         := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_SUP_REPL_DIST',NULL, NULL, NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_REPL_SUPP_DIST', 'REPL_ITEM_LOC_SUPP_DIST',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   close C_CHECK_REPL_SUPP_DIST;
   ---
   -- Check if the item/supplier combo belongs in a pack that is supplied by I_supplier
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_PACK_SUPP_EXISTS', 'V_PACKSKU_QTY, ITEM_SUPPLIER',
                      'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   open C_CHECK_PACK_SUPP_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_PACK_SUPP_EXISTS', 'V_PACKSKU_QTY, ITEM_SUPPLIER',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   fetch C_CHECK_PACK_SUPP_EXISTS into L_pack_no;
   if C_CHECK_PACK_SUPP_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_PACK_SUPP_EXISTS', 'V_PACKSKU_QTY, ITEM_SUPPLIER',
                          'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_CHECK_PACK_SUPP_EXISTS;
      O_exist := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('PACK_EXISTS_FOR_SKU',
                                                L_pack_no,
                                                I_item,
                                                to_char(I_supplier));
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_PACK_SUPP_EXISTS', 'V_PACKSKU_QTY, ITEM_SUPPLIER',
                      'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   close C_CHECK_PACK_SUPP_EXISTS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_SUPPLIER_DELETE;
------------------------------------------------------------------------------
FUNCTION DELETE_SUPPLIER_RELATIONS( O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.DELETE_SUPPLIER_RELATIONS';
   L_dummy       VARCHAR2(1);
   L_cost_change COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_sc_cost_event_tbl       OBJ_SC_COST_EVENT_TBL := OBJ_SC_COST_EVENT_TBL();
   ---
   cursor C_COST_CHANGE is
      select distinct cost_change
        from cost_susp_sup_detail
       where supplier = I_supplier
         and item     = I_item
   union all
      select distinct cost_change
        from cost_susp_sup_detail_loc
       where supplier = I_supplier
         and item     = I_item;
         
   cursor C_ITEM_SUPP_COUNTRY_LOC is
      select item,
             supplier,
             origin_country_id,
             loc
        from item_supp_country_loc
       where item             = I_item
         and supplier          = I_supplier;

BEGIN
   if not LOCK_SUPPLIER_DELETE(O_error_message,
                               I_supplier,
                               I_item) then
      return FALSE;
   end if;
   ---
   ---
   FOR C_rec in C_ITEM_SUPP_COUNTRY_LOC LOOP
      L_sc_cost_event_tbl.EXTEND;
      L_sc_cost_event_tbl(L_sc_cost_event_tbl.count)  := OBJ_SC_COST_EVENT_REC(C_rec.item,
                                                                               C_rec.loc,
                                                                               C_rec.supplier,
                                                                               C_rec.origin_country_id);
   END LOOP;
   ---
   if L_sc_cost_event_tbl.count > 0 then
      if not FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY (O_error_message,
                                                     L_cost_event_process_id,
                                                     future_cost_event_sql.remove_event,
                                                     L_sc_cost_event_tbl,
                                                     get_user) then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','DEAL_ITEMLOC_ITEM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from deal_itemloc_item
    where item = I_item
      and deal_id in (select deal_id
                        from deal_head
                       where supplier = I_supplier);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','SUP_AVAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from sup_avail
    where supplier = I_supplier
      and item     = I_item;
   ---
   -- Get the cost change associated with the item on cost_susp_sup_detail
   -- this will be used to delete from the header table
   ---
   FOR C_rec in C_COST_CHANGE LOOP
      L_cost_change := C_rec.cost_change;
      ---
      ---
      -- Delete all Items with an item/supplier relationship for the style being deleted
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_DETAIL',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      delete from cost_susp_sup_detail
       where supplier = I_supplier
         and item     = I_item;
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_DETAIL_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      delete from cost_susp_sup_detail_LOC
       where supplier = I_supplier
         and item     = I_item;
      ---
      -- if no more detail records exist for that cost change then delete from header table
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_HEAD_CFA_EXT',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      delete from cost_susp_sup_head_cfa_ext
       where cost_change = L_cost_change
         and not exists (select 'x'
                           from cost_susp_sup_detail
                          where cost_change = L_cost_change
                      union all
                         select 'x'
                           from cost_susp_sup_detail_loc
                          where cost_change = L_cost_change);
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_HEAD',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      delete from cost_susp_sup_head
       where cost_change = L_cost_change
         and not exists (select 'x'
                           from cost_susp_sup_detail
                          where cost_change = L_cost_change
                      union all
                         select 'x'
                           from cost_susp_sup_detail_loc
                          where cost_change = L_cost_change);
   END LOOP;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_exp_detail
    where supplier = I_supplier
      and item    = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_EXP_HEAD', 'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_exp_head
    where supplier = I_supplier
      and item    = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_UOM', 'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_uom
    where supplier = I_supplier
      and item    = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_dim
    where supplier  = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_bracket_cost
    where supplier = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from repl_item_loc_supp_dist
    where supplier = I_supplier
      and item = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from buyer_wksht_manual
    where supplier = I_supplier
      and item = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE',NULL,'ITEM_SUPP_COUNTRY_LOC_CFA_EXT', 'ITEM: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_loc_cfa_ext
    where supplier  = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_loc
    where supplier  = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_MANU_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_manu_country
    where supplier  = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_COST_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_cost_detail
    where supplier  = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_COST_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_cost_head
    where supplier  = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_cfa_ext
    where supplier  = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country
    where supplier  = I_supplier
      and item     = I_item;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPPLIER_CFA_EXT','item: '||I_item||
                    ' supplier: '||I_supplier);
   delete from item_supplier_cfa_ext
    where item     = I_item
      and supplier = I_supplier;
   ---   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_SUPPLIER_RELATIONS;
--------------------------------------------------------------------------------
FUNCTION VALID_COUNTRY( O_error_message  IN OUT VARCHAR2,
                        O_valid          IN OUT BOOLEAN,
                        I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                        I_supplier       IN OUT ITEM_SUPPLIER.SUPPLIER%TYPE,
                        I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_dummy              VARCHAR2(1);
   L_item           ITEM_MASTER.ITEM%TYPE;
   L_exists             BOOLEAN;
   L_country_exist      BOOLEAN;
   L_program            VARCHAR2(64) := 'SUPP_ITEM_SQL.VALID_COUNTRY';
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE;
   L_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_sellable_ind       ITEM_MASTER.SELLABLE_IND%TYPE;

   cursor C_PACK_SKU is
      select v.item
        from v_packsku_qty v
       where I_item = v.pack_no;
   ---
   cursor C_PACKSKU_SUPP_EXISTS is
      select 'x'
        from item_supp_country ic
       where ic.item              = L_item
         and ic.supplier          = I_supplier
         and ic.origin_country_id = I_origin_country;
BEGIN
   O_valid   := TRUE;
   ---
   -- check if country is valid on the country table
   ---
   if not COUNTRY_VALIDATE_SQL.EXISTS_ON_TABLE(I_origin_country,
                                               'COUNTRY',
                                               O_error_message,
                                               L_exists) then
      return FALSE;
   end if;
   ---
   if L_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_COUNTRY',NULL,NULL,NULL);
      O_valid := FALSE;
      return TRUE;
   end if; -- L_exist = FALSE
   ---
   -- make sure country is not already associated with the given item/supplier
   ---
   if not ITEM_SUPP_COUNTRY_EXISTS( O_error_message,
                                    L_country_exist,
                                    I_item,
                                    I_supplier,
                                    I_origin_country) then
      return FALSE;
   end if;
   ---
   if L_country_exist = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_SUPP_COUNTRY_EXISTS',NULL,NULL,NULL);
      O_valid := FALSE;
      return TRUE;
   end if;
   ---
   if not ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                            L_pack_ind,
                            L_sellable_ind,
                            L_orderable_ind,
                            L_pack_type,
                            I_item) then
      return FALSE;
   end if;
   ---
   -- if the item is a pack, all components of the pack must
   -- at least have the given Supplier/Origin Country relationship
   -- as the pack
   ---
   if L_pack_ind = 'Y' then
      FOR C_rec in C_PACK_SKU LOOP
         L_item := C_rec.item;

         SQL_LIB.SET_MARK('OPEN', 'C_PACKSKU_SUPP_EXISTS', 'ITEM_SUPP_COUNTRY',
                         'Item: ' ||I_item||', Supplier: '||to_char(I_supplier)
                         ||', Country: '||I_origin_country);
         open C_PACKSKU_SUPP_EXISTS;
         SQL_LIB.SET_MARK('FETCH', 'C_PACKSKU_SUPP_EXISTS', 'ITEM_SUPP_COUNTRY',
                         'Item: ' ||I_item||', Supplier: '||to_char(I_supplier)
                         ||', Country: '||I_origin_country);
         fetch C_PACKSKU_SUPP_EXISTS into L_dummy;

         if C_PACKSKU_SUPP_EXISTS%NOTFOUND then
            SQL_LIB.SET_MARK('CLOSE', 'C_PACKSKU_SUPP_EXISTS', 'ITEM_SUPP_COUNTRY',
                         'Item: ' ||I_item||', Supplier: '||to_char(I_supplier)
                         ||', Country: '||I_origin_country);
            close C_PACKSKU_SUPP_EXISTS;
            O_error_message := SQL_LIB.CREATE_MSG('COUNTRY_NO_EXIST_COMP',
                                                 NULL,
                                                 NULL,
                                                 NULL);
            O_valid := FALSE;
            return TRUE;
         end if;
         SQL_LIB.SET_MARK('CLOSE', 'C_PACKSKU_SUPP_EXISTS', 'ITEM_SUPP_COUNTRY',
                         'Item: ' ||I_item||', Supplier: '||to_char(I_supplier)
                         ||', Country: '||I_origin_country);
         close C_PACKSKU_SUPP_EXISTS;
      END LOOP;
   end if; -- pack ind = 'Y'
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END VALID_COUNTRY;
--------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_INDICATORS( O_error_message  IN OUT VARCHAR2,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                    I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                    I_location       IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                                    I_default_ind    IN     VARCHAR2)
   RETURN BOOLEAN IS
   ---
   L_program  VARCHAR2(64) := 'SUPP_ITEM_SQL.UPDATE_PRIMARY_INDICATORS';
   L_table                 VARCHAR2(30);
   L_valid                 BOOLEAN;
   L_required_default_ind  VARCHAR2(1) := 'N';
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   --- Following cursor called when item/supplier/country or item/supplier are passed in
   --- and I_default_ind is 'Y'.
   cursor C_GET_CHILD_ITEMS is
      select im.item
        from item_master im
       where (im.item_parent = I_item or im.item_grandparent = I_item)
         and im.item_level  <= im.tran_level
         and exists( select 'x'
                       from item_supplier isp
                      where isp.item     = im.item
                        and isp.supplier = I_supplier );
   ---
   --- Following cursor called when item/supplier/country/location passed in---
   cursor C_LOCK_LOC_Y is
      select 'x'
        from item_supp_country_loc
       where primary_loc_ind     = 'Y'
         and origin_country_id   = I_origin_country
         and supplier            = I_supplier
         and loc               != I_location
         and item                = I_item
         for update nowait;
   ---
   --- Following cursor called when only item/supplier/country passed in---
   cursor C_LOCK_COUNTRY_Y is
      select 'x'
        from item_supp_country
       where primary_country_ind = 'Y'
         and origin_country_id   != I_origin_country
         and supplier            = I_supplier
         and item = I_item
         for update nowait;
   ---
   --- Following cursors when called only item/supplier passed in---
   cursor C_LOCK_SUPPLIER_Y is
      select 'x'
        from item_supplier
       where primary_supp_ind = 'Y'
         and supplier         != I_supplier
         and item = I_item
         for update nowait;
   ---
   cursor C_LOCK_COUNTRY_SUPP_Y is
      select 'x'
        from item_supp_country
       where primary_supp_ind = 'Y'
         and supplier         != I_supplier
         and item = I_item
         for update nowait;
   ---
   cursor C_LOCK_COUNTRY_SUPP_N is
      select 'x'
        from item_supp_country
       where supplier         = I_supplier
         and primary_supp_ind = 'N'
         and item = I_item
         for update nowait;
   ---
   --- Following cursor is called when only item/supplier are passed in to determine if
   --- defaulting the primary indicator is required.
   cursor CHILD_EXIST_FOR_TRAN_ITEM is
      select 'Y'
        from item_master im1
       where im1.item       = I_item
         and im1.tran_level = im1.item_level
         and exists( select 'x'
                       from item_master im2
                      where im2.item_parent = im1.item
                        and exists( select 'x'
                                      from item_supplier isp
                                     where isp.item     = im2.item
                                       and isp.supplier = I_supplier ));
 BEGIN

   if I_location is not NULL then
      -- Lock the item_supp_country_loc record, update the primary location ind to
      -- No for I_item with an location other than the input location

      L_table := 'ITEM_SUPP_COUNTRY_LOC';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_Y','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country
                 ||' Location: '|| I_Location);
      open C_LOCK_LOC_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_Y','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country
                 ||' Location: '|| I_Location);
      close C_LOCK_LOC_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC',
                       'Location: '|| I_Location);
      update item_supp_country_loc iscl1
         set iscl1.primary_loc_ind      = 'N',
             iscl1.last_update_id       = get_user,
             iscl1.last_update_datetime = sysdate
       where iscl1.primary_loc_ind      = 'Y'
         and iscl1.origin_country_id    = I_origin_country
         and iscl1.supplier             = I_supplier
         and iscl1.loc                 != I_location
         and iscl1.item                 = I_item
         and exists( select 'x'
                       from item_supp_country_loc iscl2
                      where iscl2.item              = iscl1.item
                        and iscl2.supplier          = iscl1.supplier
                        and iscl2.origin_country_id = iscl1.origin_country_id
                        and iscl2.loc               = I_location );

   elsif I_origin_country is not NULL and I_location is NULL then
      ---
      -- Verify that the origin country for the child item-supplier can become primary
      if I_default_ind = 'Y' then
         for rec in C_GET_CHILD_ITEMS LOOP
            if CHECK_NEW_PRIMARY_IND(O_error_message,
                                     L_valid,
                                     rec.item,
                                     I_supplier,
                                     I_origin_country) = FALSE then
               return FALSE;
            end if;
            if L_valid = FALSE then
               return FALSE;
            end if;
         end LOOP;
      end if;
      ---
      -- Lock the item_supp_country record, update the primary country ind to
      -- No for I_item with an origin
      -- country other than the input country
      ---
      L_table := 'ITEM_SUPP_COUNTRY';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_Y','ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      open C_LOCK_COUNTRY_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_Y','ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_LOCK_COUNTRY_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY',
                       'Country: '|| I_origin_country);
      update item_supp_country isc1
         set isc1.primary_country_ind  = 'N',
             isc1.last_update_id       = get_user,
             isc1.last_update_datetime = sysdate
       where isc1.primary_country_ind  = 'Y'
         and isc1.origin_country_id   != I_origin_country
         and isc1.supplier             = I_supplier
         and isc1.item                 = I_item
         and exists( select 'x'
                       from item_supp_country isc2
                      where isc2.item              = isc1.item
                        and isc2.supplier          = isc1.supplier
                        and isc2.origin_country_id = I_origin_country
                        and isc2.primary_country_ind = 'Y');
      ---
   else  --origin country is null for item/supplier combo, update supp ind
      ---
      -- Verify that the supplier for the child item can become primary
      if I_default_ind = 'Y' then
         for rec in C_GET_CHILD_ITEMS LOOP
            if CHECK_NEW_PRIMARY_IND(O_error_message,
                                     L_valid,
                                     rec.item,
                                     I_supplier,
                                     I_origin_country) = FALSE then
               return FALSE;
            end if;
            if L_valid = FALSE then
               return FALSE;
            end if;
         end LOOP;
      else
         -- Check if the current item is at the transaction level and has lower-level
         -- items associated with it. If so, these item-suppliers need to be updated.
         SQL_LIB.SET_MARK('OPEN','CHILD_EXIST_FOR_TRAN_ITEM','ITEM_MASTER, ITEM_SUPPLIER',
                          'Item: '|| I_item||', Supplier: '||to_char(I_supplier));
         open CHILD_EXIST_FOR_TRAN_ITEM;
         SQL_LIB.SET_MARK('FETCH','CHILD_EXIST_FOR_TRAN_ITEM','ITEM_MASTER, ITEM_SUPPLIER',
                          'Item: '|| I_item||', Supplier: '||to_char(I_supplier));
         fetch CHILD_EXIST_FOR_TRAN_ITEM into L_required_default_ind;
         SQL_LIB.SET_MARK('CLOSE','CHILD_EXIST_FOR_TRAN_ITEM','ITEM_MASTER, ITEM_SUPPLIER',
                          'Item: '|| I_item||', Supplier: '||to_char(I_supplier));
         close CHILD_EXIST_FOR_TRAN_ITEM;
      end if;
      ---
      -- Lock the item_supplier record and update the primary supplier ind to
      -- No for I_item where the supplier
      -- is not equal to the input supplier
      ---
      L_table := 'ITEM_SUPPLIER';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_SUPPLIER_Y','ITEM_SUPPLIER','Item: '|| I_item||
                       ' Supplier: '||to_char(I_supplier));
      open C_LOCK_SUPPLIER_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SUPPLIER_Y','ITEM_SUPPLIER','Item: '||I_item||
                       'Supplier: '||to_char(I_supplier));
      close C_LOCK_SUPPLIER_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPPLIER','Item: '||I_item||' Supplier: '||
                       to_char(I_supplier));
      update item_supplier is1
         set is1.primary_supp_ind     = 'N',
             is1.last_update_id       = get_user,
             is1.last_update_datetime = sysdate
       where is1.primary_supp_ind     = 'Y'
         and is1.supplier            != I_supplier
         and is1.item                 = I_item
         and exists( select 'x'
                       from item_supplier is2
                      where is2.item              = is1.item
                        and is2.supplier          = I_supplier);
      ---
      ---
      -- Lock the item_supp_country record and update the primary supplier ind to
      -- No for I_item where the supplier is
      -- not equal to the input supplier
      ---
      L_table := 'ITEM_SUPP_COUNTRY';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_SUPP_Y','ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      open C_LOCK_COUNTRY_SUPP_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_SUPP_Y','ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      close C_LOCK_COUNTRY_SUPP_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      update item_supp_country isc1
         set isc1.primary_supp_ind     = 'N',
             isc1.last_update_id       = get_user,
             isc1.last_update_datetime = sysdate
       where isc1.primary_supp_ind     = 'Y'
         and isc1.supplier            != I_supplier
         and isc1.item                 = I_item
         and exists( select 'x'
                       from item_supp_country isc2
                      where isc2.item              = isc1.item
                        and isc2.supplier          = I_supplier);
      ---
      -- Lock the item_supp_country record and update the primary supplier to Yes
      -- for I_item where the supplier is
      -- equal to the input supplier
      ---
      L_table := 'ITEM_SUPP_COUNTRY';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_SUPP_N','ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      open C_LOCK_COUNTRY_SUPP_N;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_SUPP_N','ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      close C_LOCK_COUNTRY_SUPP_N;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      update item_supp_country
         set primary_supp_ind     = 'Y',
             last_update_id       = get_user,
             last_update_datetime = sysdate
       where supplier             = I_supplier
         and primary_supp_ind     = 'N'
         and item                 = I_item;
      ---UPDATE PRIMARY SUPPLIER INFO FOR ALL CHILD RECORDS AT AND ABOVE THE TRANSACTION LEVEL
      if I_default_ind = 'Y' then
         if not UPDATE_PRIM_SUPP_FOR_CHILDREN(O_error_message,
                                I_item,
                                I_supplier) then
            return FALSE;
         end if;
      elsif L_required_default_ind = 'Y' then
         if not UPDATE_PRIMARY_CHILD_INDS(O_error_message,
                            I_item,
                            I_supplier,
                            I_origin_country,
                            I_location) then
            return FALSE;
         end if;
      end if;
      return TRUE;
   end if; -- I-location, I-origin_country checks
   ---
   --- UPDATE PRIMARY SUPPLIER, COUNTRY, OR LOC INFO FOR CHILD RECORDS AT AND ABOVE TRANSACTION LEVEL
   if I_default_ind = 'Y' then
      if not UPDATE_PRIMARY_CHILD_INDS(O_error_message,
                           I_item,
                           I_supplier,
                           I_origin_country,
                           I_location) then
         return FALSE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;
END UPDATE_PRIMARY_INDICATORS;
--------------------------------------------------------------------------------
-- Description:  Identifies if an item/supplier combination is present in the
--               item_supplier table.
--------------------------------------------------------------------------------
FUNCTION EXIST ( O_error_message IN OUT VARCHAR2,
                 O_exist         IN OUT BOOLEAN,
                 I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                 I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   ---
   L_dummy       VARCHAR2(1);
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.EXIST';

   cursor C_ITEM_SUPPLIER is
      select 'X'
        from item_supplier
       where item_supplier.item = I_item
         and item_supplier.supplier = I_supplier
         and rownum = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPPLIER','item_supplier','Item: '||
                    I_item||' supplier: '||to_char(I_supplier));
   open C_ITEM_SUPPLIER;

   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPPLIER','item_supplier','Item: '||
                    I_item||' supplier: '||to_char(I_supplier));
   fetch C_ITEM_SUPPLIER into L_dummy;

   if C_ITEM_SUPPLIER%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPPLIER','item_supplier','Item: '||
                    I_item||' supplier: '||to_char(I_supplier));
   close C_ITEM_SUPPLIER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END EXIST;
--------------------------------------------------------------------------------
FUNCTION UOM_EXISTS(O_error_message IN OUT VARCHAR2,
                    O_exists        IN OUT BOOLEAN,
                    I_item          IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                    I_supplier      IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                    I_uom           IN     UOM_CLASS.UOM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.UOM_EXISTS';
   L_dummy       VARCHAR2(1);
   ---
   cursor C_ITEM_SUPP_UOM is
      select 'x'
        from item_supp_uom
       where item     = I_item
         and supplier = I_supplier
         and uom      = nvl(I_uom,uom);
BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPP_UOM','ITEM_SUPP_UOM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' UOM: '||I_uom);
   open C_ITEM_SUPP_UOM;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPP_UOM','ITEM_SUPP_UOM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' UOM: '||I_uom);
   fetch C_ITEM_SUPP_UOM into L_dummy;
   if C_ITEM_SUPP_UOM%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_UOM','ITEM_SUPP_UOM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' UOM: '||I_uom);
   close C_ITEM_SUPP_UOM;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UOM_EXISTS;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER(O_error_message IN OUT VARCHAR2,
                           I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                           I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                           I_item          IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program          VARCHAR2(64) := 'SUPP_ITEM_SQL.VALIDATE_SUPPLIER';
   L_dummy        VARCHAR2(1);
   cursor C_ITEM_SUPPLIER is
     select 'x'
      from item_supplier
     where supplier = I_supplier
       and item     = I_item;

   cursor C_PACKITEM_SUPPLIER is
     select 'x'
       from v_packsku_qty v
      where v.pack_no  = I_item
       and v.item not in( select v.item
                            from item_supplier i, v_packsku_qty v
                           where i.supplier = I_supplier
                            and v.pack_no   = I_item
                            and v.item      = i.item);

   cursor C_SUBTRAN_ITEMS is
     select 'x'
       from item_master im
      where im.item = I_item
       and im.item_level > im.tran_level
       and not exists( select 'x'
                         from item_supplier isp
                        where isp.supplier = I_supplier
                         and isp.item      = im.item_parent );

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPPLIER','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_ITEM_SUPPLIER;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPPLIER','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   fetch C_ITEM_SUPPLIER into L_dummy;
   if C_ITEM_SUPPLIER%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPPLIER','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
      close C_ITEM_SUPPLIER;
      O_error_message := SQL_LIB.CREATE_MSG('ITEM_SUPP_EXIST', I_item, I_supplier, NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPPLIER','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_ITEM_SUPPLIER;

   if I_pack_ind = 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_PACKITEM_SUPPLIER','ITEM_SUPPLIER'||'V_PACKSKU_QTY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
      open C_PACKITEM_SUPPLIER;
      SQL_LIB.SET_MARK('FETCH','C_PACKITEM_SUPPLIER','ITEM_SUPPLIER'||'V_PACKSKU_QTY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
      fetch C_PACKITEM_SUPPLIER into L_dummy;
      if C_PACKITEM_SUPPLIER%FOUND then
         SQL_LIB.SET_MARK('CLOSE','C_PACKITEM_SUPPLIER','ITEM_SUPPLIER'||'V_PACKSKU_QTY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
         close C_PACKITEM_SUPPLIER;
         O_error_message := SQL_LIB.CREATE_MSG('SUPP_NO_EXIST_COMP', NULL, NULL, NULL);
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_PACKITEM_SUPPLIER','ITEM_SUPPLIER'||'V_PACKSKU_QTY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
      close C_PACKITEM_SUPPLIER;
   end if;

   ----
   --- Check sub transactional level item's parent has supplier

   SQL_LIB.SET_MARK('OPEN','C_SUBTRAN_ITEMS','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_SUBTRAN_ITEMS;
   SQL_LIB.SET_MARK('FETCH','C_SUBTRAN_ITEMS','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   fetch C_SUBTRAN_ITEMS into L_dummy;
   if C_SUBTRAN_ITEMS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_SUBTRAN_ITEMS','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
      close C_SUBTRAN_ITEMS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_TRAN_LEVEL_SUP', NULL, NULL, NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_SUBTRAN_ITEMS','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_SUBTRAN_ITEMS;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_SUPPLIER;
--------------------------------------------------------------------------------
FUNCTION LOCK_SUPPLIER_DELETE(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_supplier        IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                              I_item            IN     ITEM_SUPPLIER.ITEM%TYPE)
   return BOOLEAN IS
   L_program       VARCHAR2(64) := 'SUPP_ITEM_SQL.LOCK_SUPPLIER_DELETE';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_ITEM_EXP_DETAIL is
      select 'x'
        from item_exp_detail ied
       where ied.supplier = I_supplier
         and ied.item     = I_item
         for update nowait;
   ---
   cursor C_LOCK_ITEM_EXP_HEAD is
      select 'x'
        from item_exp_head
       where supplier = I_supplier
        and item      = I_item
        for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY_DIM is
      select 'x'
        from item_supp_country_dim
       where item     = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY_CE is
      select 'x'
        from item_supp_country_cfa_ext
       where item     = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY is
      select 'x'
        from item_supp_country
       where item     = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_CFA_EXT IS
      select 'x'
        from item_supplier_cfa_ext
       where item = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_MANU_COUNTRY is
      select 'x'
        from item_supp_manu_country
       where item     = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_CTRY_BRACKET is
      select 'x'
        from item_supp_country_bracket_cost
       where item     = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
      select 'x'
        from item_supp_country_loc
       where item     = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITM_SUPP_CTRY_LOC_CE is
      select 'x'
        from item_supp_country_loc_cfa_ext
       where item     = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_UOM is
      select 'x'
        from item_supp_uom
       where item     = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_COST_SUSP_SUP_DETAIL is
     select 'x'
       from cost_susp_sup_detail
      where supplier = I_supplier
        and item      = I_item
        for update nowait;
   ---
   cursor C_LOCK_COST_DETAIL_LOC is
     select 'x'
       from cost_susp_sup_detail_loc
      where supplier = I_supplier
        and item      = I_item
        for update nowait;
   ---
   cursor C_LOCK_COST_SUSP_SUP_HEAD_CFA is
      select 'x'
        from cost_susp_sup_head_cfa_ext chcfa
         where exists (select 'x'
                         from cost_susp_sup_detail cd
                        where cd.supplier    = I_supplier
                          and cd.item        = I_item
                          and cd.cost_change = chcfa.cost_change
                    union all
                       select 'x'
                         from cost_susp_sup_detail_loc cl
                        where cl.supplier    = I_supplier
                          and cl.item        = I_item
                          and cl.cost_change = chcfa.cost_change)
           for update nowait;
   ---
   cursor C_LOCK_COST_SUSP_SUP_HEAD is
      select 'x'
        from cost_susp_sup_head ch
         where exists (select 'x'
                         from cost_susp_sup_detail cd
                        where cd.supplier    = I_supplier
                          and cd.item        = I_item
                          and cd.cost_change = ch.cost_change
                    union all
                       select 'x'
                         from cost_susp_sup_detail_loc cl
                        where cl.supplier    = I_supplier
                          and cl.item        = I_item
                          and cl.cost_change = ch.cost_change)
           for update nowait;
   ---
   cursor C_LOCK_SUP_AVAIL is
     select 'x'
       from sup_avail
      where supplier = I_supplier
       and item      = I_item
       for update nowait;
   ---
   cursor C_LOCK_DEAL_ITEMLOC_ITEM is
      select 'x'
        from deal_itemloc_item
       where item = I_item
         and deal_id in (select deal_id
                           from deal_head
                          where supplier = I_supplier)
         for update nowait;
   ---
   cursor C_LOCK_REPL_ITEM_LOC_SUPP_DIST is
      select 'x'
        from repl_item_loc_supp_dist
       where item = I_item
         and supplier = I_supplier
         for update nowait;
   ---
   cursor C_LOCK_BUYER_WKSHT_MANUAL is
      select 'x'
        from buyer_wksht_manual
       where item = I_item
         and supplier = I_supplier
         for update nowait;
BEGIN
   L_table := 'ITEM_EXP_DETAIL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_EXP_DETAIL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_EXP_DETAIL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_EXP_DETAIL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_EXP_DETAIL;
   ---
   L_table := 'ITEM_EXP_HEAD';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_EXP_HEAD','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_EXP_HEAD;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_EXP_HEAD','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_EXP_HEAD;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_CFA_EXT';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_CE','ITEM_SUPP_COUNTRY_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_CE;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_CE','ITEM_SUPP_COUNTRY_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_CE;
   ---
   L_table := 'ITEM_SUPP_COUNTRY';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY;
   ---
   L_table := 'ITEM_SUPPLIER_CFA_EXT';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_SUPP_CFA_EXT ', 'ITEM_SUPPLIER_CFA_EXT','item: '||I_item||
                                                    ' supplier: '||I_supplier);
   open C_LOCK_ITEM_SUPP_CFA_EXT ;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_SUPP_CFA_EXT ', 'ITEM_SUPPLIER_CFA_EXT','item: '||I_item||
                                                    ' supplier: '||I_supplier);
   close C_LOCK_ITEM_SUPP_CFA_EXT ;
   ---
   L_table := 'ITEM_SUPP_MANU_COUNTRY';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_MANU_COUNTRY','ITEM_SUPP_MANU_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_MANU_COUNTRY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_MANU_COUNTRY','ITEM_SUPP_MANU_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_MANU_COUNTRY;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_CTRY_BRACKET',
                    'ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_CTRY_BRACKET;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_CTRY_BRACKET',
                    'ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_CTRY_BRACKET;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_LOC_CFA_EXT';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITM_SUPP_CTRY_LOC_CE','ITEM_SUPP_COUNTRY_LOC_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITM_SUPP_CTRY_LOC_CE;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITM_SUPP_CTRY_LOC_CE','ITEM_SUPP_COUNTRY_LOC_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITM_SUPP_CTRY_LOC_CE;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_LOC';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_LOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_LOC;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_DIM';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_DIM','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_DIM;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_DIM','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_DIM;
   ---
   L_table := 'ITEM_SUPP_UOM';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_UOM','ITEM_SUPP_UOM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_UOM;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_UOM','ITEM_SUPP_UOM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_UOM;
   ---
   L_table := 'COST_SUSP_SUP_DETAIL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_SUSP_SUP_DETAIL','COST_SUSP_SUP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_COST_SUSP_SUP_DETAIL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_SUSP_SUP_DETAIL','COST_SUSP_SUP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_COST_SUSP_SUP_DETAIL;
   ---
   L_table := 'COST_SUSP_SUP_DETAIL_LOC';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_DETAIL_LOC','COST_SUSP_SUP_DETAIL_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_COST_DETAIL_LOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_DETAIL_LOC','COST_SUSP_SUP_DETAIL_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_COST_DETAIL_LOC;
   ---
   L_table := 'COST_SUSP_SUP_HEAD_CFA_EXT';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_SUSP_SUP_HEAD_CFA','COST_SUSP_SUP_HEAD_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_COST_SUSP_SUP_HEAD_CFA;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_SUSP_SUP_HEAD_CFA','COST_SUSP_SUP_HEAD_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_COST_SUSP_SUP_HEAD_CFA;
   ---
   L_table := 'COST_SUSP_SUP_HEAD';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_SUSP_SUP_HEAD','COST_SUSP_SUP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_COST_SUSP_SUP_HEAD;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_SUSP_SUP_HEAD','COST_SUSP_SUP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_COST_SUSP_SUP_HEAD;
   ---
   L_table := 'SUP_AVAIL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SUP_AVAIL','SUP_AVAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_SUP_AVAIL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SUP_AVAIL','SUP_AVAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_SUP_AVAIL;
   ---
   L_table := 'DEAL_ITEMLOC_ITEM';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_DEAL_ITEMLOC_ITEM','DEAL_ITEMLOC_ITEM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_DEAL_ITEMLOC_ITEM;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_DEAL_ITEMLOC_ITEM','DEAL_ITEMLOC_ITEM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_DEAL_ITEMLOC_ITEM;
   ---
   L_table:='REPL_ITEM_LOC_SUPP_DIST';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_REPL_ITEM_LOC_SUPP_DIST','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_REPL_ITEM_LOC_SUPP_DIST','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
   ---
   L_table:='BUYER_WKSHT_MANUAL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_BUYER_WKSHT_MANUAL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_BUYER_WKSHT_MANUAL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_BUYER_WKSHT_MANUAL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_BUYER_WKSHT_MANUAL;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_SUPPLIER_DELETE;
--------------------------------------------------------------------------------
FUNCTION EXPENSES_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exists        IN OUT BOOLEAN,
                        I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                        I_item          IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'SUPP_ITEM_SQL.EXPENSES_EXIST';
   L_dummy        VARCHAR2(1);

   cursor C_ITEM_EXP_DETAIL is
      select 'x'
        from item_exp_detail ied
       where ied.supplier      = I_supplier
         and ied.item          = I_item
         and ied.item_exp_type = 'Z';

BEGIN
   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN','C_ITEM_EXP_DETAIL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_ITEM_EXP_DETAIL;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_EXP_DETAIL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   fetch C_ITEM_EXP_DETAIL into L_dummy;
   if C_ITEM_EXP_DETAIL%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_EXP_DETAIL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_ITEM_EXP_DETAIL;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPENSES_EXIST;
--------------------------------------------------------------------------------
FUNCTION UPDATE_INSERT_SUPP_TO_CHILDREN(O_error_message         IN OUT VARCHAR2,
                                        I_consignment_rate      IN     ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
                                        I_concession_rate       IN     ITEM_SUPPLIER.CONCESSION_RATE%TYPE,
                                        I_pallet_name           IN     ITEM_SUPPLIER.PALLET_NAME%TYPE,
                                        I_case_name             IN     ITEM_SUPPLIER.CASE_NAME%TYPE,
                                        I_inner_name            IN     ITEM_SUPPLIER.INNER_NAME%TYPE,
                                        I_supp_discontinue_date IN     ITEM_SUPPLIER.SUPP_DISCONTINUE_DATE%TYPE,
                                        I_direct_ship_ind       IN     ITEM_SUPPLIER.DIRECT_SHIP_IND%TYPE,
                                        I_supplier              IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_item                  IN     ITEM_SUPPLIER.ITEM%TYPE,
                                        I_update_only_ind       IN     VARCHAR2,
                                        I_primary_case_size     IN     ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE)
   return BOOLEAN is
   ---
   L_program      VARCHAR2(64) := 'SUPP_ITEM_SQL.UPDATE_INSERT_SUPP_TO_CHILDREN';
   L_table        VARCHAR2(30);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_ITEM_SUPP is
      select 'x'
        from item_supplier its, item_master im
       where (im.item_parent = I_item or
          im.item_grandparent = I_item)
         and its.item = im.item
         and its.supplier = I_supplier
       for update of its.supplier nowait;

BEGIN

  ---
  -- Update already existing child items to new parent values on item_supplier
  ---
  L_table := 'ITEM_SUPPLIER';
  SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP','ITEM_SUPPLIER','Item: '||I_item||
                   ' Supplier: '||to_char(I_supplier));
  open C_LOCK_ITEM_SUPP;

  SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP','ITEM_SUPPLIER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
  close C_LOCK_ITEM_SUPP;

   update item_supplier
      set consignment_rate      = I_consignment_rate,
          concession_rate       = I_concession_rate,
          pallet_name           = I_pallet_name,
          case_name             = I_case_name,
          inner_name            = I_inner_name,
          direct_ship_ind       = I_direct_ship_ind,
          supp_discontinue_date = I_supp_discontinue_date,
          last_update_id        = get_user,
          last_update_datetime  = sysdate,
          primary_case_size     = I_primary_case_size
    where supplier = I_supplier
      and item in (select its.item
                 from item_master im, item_supplier its
                    where (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and its.item = im.item
                      and its.supplier = I_supplier);
   ---
   if I_update_only_ind = 'N' then
      ---
      -- Insert lower level item's which do not have any suppliers.
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'item_supplier', NULL);
      ---
      insert into item_supplier ( item,
                                  supplier,
                                  primary_supp_ind,
                                  consignment_rate,
                                  concession_rate,
                                  pallet_name,
                                  case_name,
                                  inner_name,
                                  supp_discontinue_date,
                                  direct_ship_ind,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  primary_case_size)
                           select im.item,
                                  I_supplier,
                                  'Y',
                                  consignment_rate,
                                  concession_rate,
                                  pallet_name,
                                  case_name,
                                  inner_name,
                                  supp_discontinue_date,
                                  direct_ship_ind,
                                  sysdate,
                                  sysdate,
                                  get_user,
                                  I_primary_case_size
                             from item_master im, item_supplier its
                            where (im.item_parent      = I_item or
                                   im.item_grandparent = I_item)
                              and its.item = I_item
                              and its.supplier = I_supplier
                              and not exists (select 'x'
                                                from item_supplier
                                               where item_supplier.item = im.item);
      ---
      -- Insert lower level item's which have at least one supplier (have a primary_supp_ind = 'Y').
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'item_supplier', NULL);
      insert into item_supplier ( item,
                                  supplier,
                                  primary_supp_ind,
                                  consignment_rate,
                                  concession_rate,
                                  pallet_name,
                                  case_name,
                                  inner_name,
                                  supp_discontinue_date,
                                  direct_ship_ind,
                                  create_datetime,
                                  last_update_datetime,
                                  last_update_id,
                                  primary_case_size)
                           select im.item,
                                  I_supplier,
                                  'N',
                                  consignment_rate,
                                  concession_rate,
                                  pallet_name,
                                  case_name,
                                  inner_name,
                                  supp_discontinue_date,
                                  direct_ship_ind,
                                  sysdate,
                                  sysdate,
                                  get_user,
                                  I_primary_case_size
                             from item_master im, item_supplier its
                            where (im.item_parent      = I_item or
                                   im.item_grandparent = I_item)
                              and its.item     = I_item
                              and its.supplier = I_supplier
                              and exists (select 'x'
                                            from item_supplier
                                           where supplier          != I_supplier
                                             and item_supplier.item = im.item)
                              and not exists (select 'x'
                                                from item_supplier
                                               where item_supplier.item     = im.item
                                                 and item_supplier.supplier = I_supplier);
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_INSERT_SUPP_TO_CHILDREN;
--------------------------------------------------------------------------------
FUNCTION DELETE_SUPPLIER_FROM_CHILDREN(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                       I_item           IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program        VARCHAR2(64) := 'SUPP_ITEM_SQL.DELETE_SUPPLIER_FROM_CHILDREN';
   L_exists         BOOLEAN;
   L_child_item     ITEM_MASTER.ITEM%TYPE;
   L_table          VARCHAR2(30);
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ITEM_SUPPLIER_CFA_EXT is
      select 'x'
        from item_supplier_cfa_ext its, item_master im
       where supplier = I_supplier
         and (im.item_parent     = I_item
          or  im.item_grandparent = I_item)
         and its.item = im.item
         for update of its.item nowait;

   cursor C_LOCK_ITEM_SUPPLIER_TL is
      select 'x'
        from item_supplier_tl its, item_master im
       where supplier = I_supplier
         and (im.item_parent     = I_item
          or  im.item_grandparent = I_item)
         and its.item = im.item
         for update of its.item nowait;         

   cursor C_LOCK_ITEM_SUPPLIER is
      select 'x'
        from item_supplier its, item_master im
       where supplier = I_supplier
         and (im.item_parent     = I_item
          or  im.item_grandparent = I_item)
         and its.item = im.item
         for update of its.item nowait;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
----------------------INTERNAL FUNCTIONS FOR CHILD ITEMS------------------------
FUNCTION CHECK_SUPP_CHILDREN_DELETE(O_error_message  IN OUT VARCHAR2,
                                    O_exist          IN OUT BOOLEAN,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program       VARCHAR2(64)                     := 'SUPP_ITEM_SQL.CHECK_SUPP_CHILDREN_DELETE';
   L_dummy         VARCHAR2(1);
   L_exist         VARCHAR2(1)                      := 'Y';
   L_contract_ind  SYSTEM_OPTIONS.CONTRACT_IND%TYPE;
   L_error_message VARCHAR2(255);
   L_pack_no       PACKITEM.PACK_NO%TYPE;
   ---
   cursor C_CHECK_SKU_PRIMARY is
      select 'x'
        from item_supplier its, item_master im
       where supplier = I_supplier
         and (im.item_parent = I_item or
            im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
       and its.item = im.item
         and primary_supp_ind = 'Y';
   ---
   cursor C_CHECK_PACK_SUPP_EXISTS is
      select v.pack_no
        from item_supplier i,
             v_packsku_qty v
       where i.supplier  = I_supplier
         and v.pack_no   = i.item
         and v.item  in (select im.item
                   from item_supplier its, item_master im
                          where supplier = I_supplier
                        and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                        and item_level <= tran_level
                    and its.item = im.item);
   ---
   cursor C_CHECK_ITEM_LOC is
      select 'x'
        from item_loc il, item_master im
       where primary_supp = I_supplier
         and il. item = im.item
         and (im.item_parent = I_item or
            im.item_grandparent = I_item)
         and item_level <= tran_level;
   ---
   cursor C_CHECK_REPL_ITEM_LOC is
      select 'x'
        from repl_item_loc ril, item_master im
       where ril.primary_repl_supplier = I_supplier
         and ril.item = im.item
         and (im.item_parent = I_item or
            im.item_grandparent = I_item)
         and item_level <= tran_level;

   cursor C_ORDER is
      select 'x'
        from ordsku os,
             ordhead oh,
             item_master im
       where oh.supplier = I_supplier
         and oh.order_no = os.order_no
         and os.item     = im.item
         and (im.item_parent    = I_item
         or im.item_grandparent = I_item)
         and item_level        <= tran_level;
   ---
   cursor C_RTV is
      select 'x'
        from rtv_detail rd,
             rtv_head rh,
             item_master im
       where rh.supplier     = I_supplier
         and rh.rtv_order_no = rd.rtv_order_no
         and rd.item         = im.item
         and (im.item_parent    = I_item
         or im.item_grandparent = I_item)
         and item_level        <= tran_level;
   ---
   cursor C_CONTRACT_COST is
      select 'x'
        from contract_cost cc,
             contract_header ch
       where ch.supplier    = I_supplier
         and ch.contract_no = cc.contract_no
         and (cc.item_parent     = I_item
          or cc.item_grandparent = I_item);

   ---
   cursor C_CONTRACT_DETAIL is
      select 'x'
        from contract_detail cd,
             contract_header ch
       where ch.supplier    = I_supplier
         and ch.contract_no = cd.contract_no
         and (cd.item_parent     = I_item
          or cd.item_grandparent = I_item);

BEGIN
   O_exist := FALSE;
   ---
   -- If I_supplier is the primary supplier for a child of I_item and the child is at or
   -- above the transaction level, then it cannot be deleted
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SKU_PRIMARY', 'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   open C_CHECK_SKU_PRIMARY;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SKU_PRIMARY', 'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   fetch C_CHECK_SKU_PRIMARY into L_dummy;
   if C_CHECK_SKU_PRIMARY%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SKU_PRIMARY', 'ITEM_SUPPLIER',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      close C_CHECK_SKU_PRIMARY;
      O_exist := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_PRI_SUP',
                                             NULL,
                                             NULL,
                                             NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SKU_PRIMARY', 'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_CHECK_SKU_PRIMARY;

    ---
   -- Check if supplier is on any replenishment tables
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_REPL_ITEM_LOC', 'REPL_ITEM_LOC',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   open C_CHECK_REPL_ITEM_LOC;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_REPL_ITEM_LOC', 'REPL_ITEM_LOC',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   fetch C_CHECK_REPL_ITEM_LOC into L_dummy;
   if C_CHECK_REPL_ITEM_LOC%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_REPL_ITEM_LOC', 'REPL_ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
      close C_CHECK_REPL_ITEM_LOC;
      O_exist         := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_PRIM_SUP_CHILD_REP',NULL, NULL, NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_REPL_ITEM_LOC', 'REPL_ITEM_LOC',
                    'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   close C_CHECK_REPL_ITEM_LOC;
   -----
   ---
   -- Check if supplier exists on the item location table

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ITEM_LOC', 'ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   open C_CHECK_ITEM_LOC;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ITEM_LOC', 'ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   fetch C_CHECK_ITEM_LOC into L_dummy;
   if C_CHECK_ITEM_LOC%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM_LOC', 'ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
      close C_CHECK_ITEM_LOC;
      O_exist         := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_PRIM_SUP_CHILD_LOC',NULL, NULL, NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ITEM_LOC', 'ITEM_LOC',
                       'Item: '||I_item||'Supplier: '||to_char(I_supplier));
   close C_CHECK_ITEM_LOC;
   ---
   -- Check if the item/supplier combo belongs in a pack that is supplied by I_supplier
   ---

   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_PACK_SUPP_EXISTS', 'V_PACKSKU_QTY, ITEM_SUPPLIER',
                      'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   open C_CHECK_PACK_SUPP_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_PACK_SUPP_EXISTS', 'V_PACKSKU_QTY, ITEM_SUPPLIER',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   fetch C_CHECK_PACK_SUPP_EXISTS into L_pack_no;
   if C_CHECK_PACK_SUPP_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_PACK_SUPP_EXISTS', 'V_PACKSKU_QTY, ITEM_SUPPLIER',
                          'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_CHECK_PACK_SUPP_EXISTS;
      O_exist := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('PACK_EXISTS_FOR_SKU',
                                                L_pack_no,
                                                I_item,
                                                to_char(I_supplier));
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_PACK_SUPP_EXISTS', 'V_PACKSKU_QTY, ITEM_SUPPLIER',
                      'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
   close C_CHECK_PACK_SUPP_EXISTS;

   ---
   -- Check the purchasing module for item/supplier rel. on orders
   ---

   SQL_LIB.SET_MARK('OPEN', 'C_ORDER', 'ORDSKU, ORDHEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   open C_ORDER;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDER', 'ORDSKU, ORDHEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   fetch C_ORDER into L_dummy;
   if C_ORDER%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'ORDSKU, ORDHEAD',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      close C_ORDER;
      O_exist := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SKU_SUPP_ORD',
                                             NULL,
                                             NULL,
                                             NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'ORDSKU, ORDHEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_ORDER;
   ---
   -- Check the rtv module
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_RTV', 'RTV_HEAD, RTV_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   open C_RTV;
   SQL_LIB.SET_MARK('FETCH', 'C_RTV', 'RTV_HEAD, RTV_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   fetch C_RTV into L_dummy;
   if C_RTV%FOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_RTV', 'RTV_HEAD, RTV_DETAIL',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      close C_RTV;
      O_exist := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SKU_SUPP_RTV',
                                             NULL,
                                             NULL,
                                             NULL);
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_RTV', 'RTV_HEAD, RTV_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_RTV;
   ---
   -- Check system options for contracting indicator; If it is yes then
   -- Check the contracting module (cost and detail tables)
   ---
   if not SYSTEM_OPTIONS_SQL.GET_CONTRACT_IND(O_error_message,
                                              L_contract_ind) then
      return FALSE;
   end if;
   ---
   if L_contract_ind  = 'Y' then
      SQL_LIB.SET_MARK('OPEN', 'C_CONTRACT_COST', 'CONTRACT_HEADER, CONTRACT_COST',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      open C_CONTRACT_COST;
      SQL_LIB.SET_MARK('FETCH', 'C_CONTRACT_COST', 'CONTRACT_HEADER, CONTRACT_COST',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      fetch C_CONTRACT_COST into L_dummy;
      if C_CONTRACT_COST%FOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT_COST', 'CONTRACT_HEADER, CONTRACT_COST',
                          'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
         close C_CONTRACT_COST;
         O_exist := TRUE;
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SKU_SUPP_CONT',
                                                NULL,
                                                NULL,
                                                NULL);
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT_COST', 'CONTRACT_HEADER, CONTRACT_COST',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_CONTRACT_COST;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_CONTRACT_DETAIL', 'CONTRACT_HEADER, CONTRACT_DETAIL',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      open C_CONTRACT_DETAIL;
      SQL_LIB.SET_MARK('FETCH', 'C_CONTRACT_DETAIL', 'CONTRACT_HEADER, CONTRACT_DETAIL',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      fetch C_CONTRACT_DETAIL into L_dummy;
      if C_CONTRACT_DETAIL%FOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT_DETAIL', 'CONTRACT_HEADER, CONTRACT_DETAIL',
                          'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
         close C_CONTRACT_DETAIL;
         O_exist := TRUE;
         O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_SKU_SUPP_CONT',
                                                NULL,
                                                NULL,
                                                NULL);
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_CONTRACT_DETAIL', 'CONTRACT_HEADER, CONTRACT_DETAIL',
                       'Supplier: '||to_char(I_supplier)||' Item: '||I_item);
      close C_CONTRACT_DETAIL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_SUPP_CHILDREN_DELETE;
--------------------------------------------------------------------------------
FUNCTION LOCK_SUPP_CHILDREN_DELETE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                   I_item           IN     ITEM_SUPPLIER.ITEM%TYPE)
            RETURN BOOLEAN IS
 L_program          VARCHAR2(64) := 'SUPP_ITEM_SQL.LOCK_SUPP_CHILDREN_DELETE';
 L_table          VARCHAR2(30);
 RECORD_LOCKED      EXCEPTION;
 PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

  cursor C_LOCK_ITEM_EXP_DETAIL is
      select 'x'
        from item_exp_detail ied, item_master im
       where ied.supplier = I_supplier
         and ied.item  = im.item
         and (im.item_parent = I_item or
          im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of ied.supplier nowait;
  ---
  cursor C_LOCK_ITEM_EXP_HEAD is
     select 'x'
       from item_exp_head ieh, item_master im
      where ieh.supplier = I_supplier
        and ieh.item  = im.item
        and (im.item_parent = I_item or
           im.item_grandparent = I_item)
        and item_level <= tran_level
        for update of ieh.supplier nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY_DIM is
      select 'x'
        from item_supp_country_dim iscd, item_master im
       where supplier = I_supplier
         and iscd.item = im.item
         and (im.item_parent = I_item or
            im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of iscd.supplier nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY is
      select 'x'
        from item_supp_country isc, item_master im
       where supplier = I_supplier
       and isc.item = im.item
         and (im.item_parent = I_item or
            im.item_grandparent = I_item)
       and item_level <= tran_level
         for update of isc.supplier nowait;
  ---
   cursor C_LOCK_ITEM_SUPP_MANU_COUNTRY is
      select 'x'
        from item_supp_manu_country ismc, item_master im
       where supplier = I_supplier
       and ismc.item = im.item
         and (im.item_parent = I_item or
            im.item_grandparent = I_item)
       and item_level <= tran_level
         for update of ismc.supplier nowait;
  ---
  cursor C_LOCK_ITEM_SUPP_CTRY_BRACKET is
      select 'x'
        from item_supp_country_bracket_cost iscbc,
             item_master im
       where supplier = I_supplier
         and iscbc.item = im.item
         and (im.item_parent = I_item or
              im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of iscbc.supplier nowait;
  ---
  cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
      select 'x'
        from item_supp_country_loc iscl, item_master im
       where supplier   = I_supplier
         and iscl.item  = im.item
         and (im.item_parent = I_item or
            im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of iscl.supplier nowait;
  ---
  cursor C_LOCK_ITEM_SUPP_UOM is
      select 'x'
        from item_supp_uom isu, item_master im
       where isu.item  = im.item
         and supplier = I_supplier
         and (im.item_parent    = I_item
              or im.item_grandparent = I_item)
         and im.item_level     <= im.tran_level
       for update nowait;

  cursor C_LOCK_COST_SUSP_SUP_DETAIL is
     select 'x'
       from cost_susp_sup_detail cd, item_master im
      where supplier = I_supplier
        and cd.item  = im.item
        and (im.item_parent     = I_item
         or im.item_grandparent = I_item)
        and im.item_level      <= im.tran_level
        for update nowait;
  --
  cursor C_LOCK_COST_DETAIL_LOC is
     select 'x'
       from cost_susp_sup_detail_loc cdl, item_master im
      where supplier = I_supplier
        and cdl.item  = im.item
        and (im.item_parent     = I_item
         or im.item_grandparent = I_item)
        and im.item_level      <= im.tran_level
        for update nowait;
  --
  cursor C_LOCK_COST_SUSP_SUP_HEAD_CFA is
     select 'x'
       from cost_susp_sup_head_cfa_ext chcfa
      where exists (select 'x'
                      from cost_susp_sup_detail cd, item_master im
                     where cd.supplier = I_supplier
                       and cd.item  = im.item
                       and (im.item_parent        = I_item
                           or im.item_grandparent = I_item)
                       and im.item_level         <= im.tran_level
                       and cd.cost_change = chcfa.cost_change
                 union all
                    select 'x'
                      from cost_susp_sup_detail_loc cl, item_master im
                     where cl.supplier = I_supplier
                       and cl.item  = im.item
                       and (im.item_parent        = I_item
                           or im.item_grandparent = I_item)
                       and im.item_level         <= im.tran_level
                       and cl.cost_change = chcfa.cost_change)
         for update nowait;
  --
  cursor C_LOCK_COST_SUSP_SUP_HEAD is
     select 'x'
       from cost_susp_sup_head ch
      where exists (select 'x'
                      from cost_susp_sup_detail cd, item_master im
                     where cd.supplier = I_supplier
                       and cd.item  = im.item
                       and (im.item_parent        = I_item
                           or im.item_grandparent = I_item)
                       and im.item_level         <= im.tran_level
                       and cd.cost_change = ch.cost_change
                 union all
                    select 'x'
                      from cost_susp_sup_detail_loc cl, item_master im
                     where cl.supplier = I_supplier
                       and cl.item  = im.item
                       and (im.item_parent        = I_item
                           or im.item_grandparent = I_item)
                       and im.item_level         <= im.tran_level
                       and cl.cost_change = ch.cost_change)
         for update nowait;
  --
  cursor C_LOCK_SUP_AVAIL is
     select 'x'
       from sup_avail sa, item_master im
      where supplier = I_supplier
        and sa.item  = im.item
        and (im.item_parent     = I_item
             or im.item_grandparent = I_item)
        and im.item_level      <= im.tran_level
        for update nowait;
  --
  cursor C_LOCK_DEAL_ITEMLOC_ITEM is
     select 'x'
       from deal_itemloc_item
      where (item_parent     = I_item
          or item_grandparent = I_item)
        and deal_id in (select deal_id
                          from deal_head
                         where supplier = I_supplier)
        for update nowait;
  --
  cursor C_LOCK_DEAL_ITEMLOC_PARENT is
     select 'x'
       from deal_itemloc_parent_diff
      where (item_parent     = I_item
             or item_grandparent = I_item)
        and deal_id in (select deal_id
                          from deal_head
                         where supplier = I_supplier)
        for update nowait;
  --
  cursor C_LOCK_REPL_ITEM_LOC_SUPP_DIST is
     select 'x'
       from repl_item_loc_supp_dist rilsd, item_master im
      where supplier = I_supplier
        and rilsd.item = im.item
        and (im.item_parent = I_item or
             im.item_grandparent = I_item)
        and item_level <= tran_level
        for update nowait;
  --
  cursor C_LOCK_BUYER_WKSHT_MANUAL is
     select 'x'
       from buyer_wksht_manual bwm, item_master im
      where supplier = I_supplier
        and bwm.item = im.item
        and (im.item_parent = I_item or
             im.item_grandparent = I_item)
        and item_level <= tran_level
        for update nowait;
  --

BEGIN
   L_table := 'ITEM_EXP_DETAIL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_EXP_DETAIL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_EXP_DETAIL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_EXP_DETAIL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_EXP_DETAIL;
   ---
   L_table := 'ITEM_EXP_HEAD';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_EXP_HEAD','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_EXP_HEAD;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_EXP_HEAD','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_EXP_HEAD;
   ---
   L_table := 'ITEM_SUPP_COUNTRY';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY;
   ---
   L_table := 'ITEM_SUPP_MANU_COUNTRY';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_MANU_COUNTRY','ITEM_SUPP_MANU_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_MANU_COUNTRY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_MANU_COUNTRY','ITEM_SUPP_MANU_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_MANU_COUNTRY;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_CTRY_BRACKET',
                    'ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_CTRY_BRACKET;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_CTRY_BRACKET',
                    'ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_CTRY_BRACKET;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_LOC';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_LOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_LOC;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_DIM';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_DIM','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_DIM;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_DIM','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_DIM;
   ---
   L_table := 'ITEM_SUPP_UOM';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_UOM','ITEM_SUPP_UOM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_UOM;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_UOM','ITEM_SUPP_UOM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_UOM;
   ---
   L_table := 'COST_SUSP_SUP_DETAIL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_SUSP_SUP_DETAIL','COST_SUSP_SUP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_COST_SUSP_SUP_DETAIL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_SUSP_SUP_DETAIL','COST_SUSP_SUP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_COST_SUSP_SUP_DETAIL;
   ---
   L_table := 'COST_SUSP_SUP_DETAIL_LOC';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_DETAIL_LOC','COST_SUSP_SUP_DETAIL_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_COST_DETAIL_LOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_DETAIL_LOC','COST_SUSP_SUP_DETAIL_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_COST_DETAIL_LOC;
   ---
   L_table := 'COST_SUSP_SUP_HEAD_CFA_EXT';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_SUSP_SUP_HEAD_CFA','COST_SUSP_SUP_HEAD_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_COST_SUSP_SUP_HEAD_CFA;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_SUSP_SUP_HEAD_CFA','COST_SUSP_SUP_HEAD_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_COST_SUSP_SUP_HEAD_CFA;
   ---
   L_table := 'COST_SUSP_SUP_HEAD';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_COST_SUSP_SUP_HEAD','COST_SUSP_SUP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_COST_SUSP_SUP_HEAD;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_COST_SUSP_SUP_HEAD','COST_SUSP_SUP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_COST_SUSP_SUP_HEAD;
   ---
   L_table := 'SUP_AVAIL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_SUP_AVAIL','SUP_AVAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_SUP_AVAIL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_SUP_AVAIL','SUP_AVAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_SUP_AVAIL;
   ---
   L_table := 'DEAL_ITEMLOC_ITEM';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_DEAL_ITEMLOC_ITEM','DEAL_ITEMLOC_ITEM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_DEAL_ITEMLOC_ITEM;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_DEAL_ITEMLOC_ITEM','DEAL_ITEMLOC_ITEM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_DEAL_ITEMLOC_ITEM;
   ---
   L_table := 'DEAL_ITEMLOC_PARENT_DIFF';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_DEAL_ITEMLOC_PARENT','DEAL_ITEMLOC_PARENT_DIFF','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_DEAL_ITEMLOC_PARENT;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_DEAL_ITEMLOC_PARENT','DEAL_ITEMLOC_PARENT_DIFF','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_DEAL_ITEMLOC_PARENT;   
   ---
   L_table := 'REPL_ITEM_LOC_SUPP_DIST';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_REPL_ITEM_LOC_SUPP_DIST','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_REPL_ITEM_LOC_SUPP_DIST','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
   ---
   L_table := 'BUYER_WKSHT_MANUAL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_BUYER_WKSHT_MANUAL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_BUYER_WKSHT_MANUAL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_BUYER_WKSHT_MANUAL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_BUYER_WKSHT_MANUAL;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_SUPP_CHILDREN_DELETE;
------------------------------------------------------------------------------
FUNCTION DELETE_SUPP_CHILDREN_RELATIONS( O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                         I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                         I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.DELETE_SUPP_CHILDREN_RELATIONS';
   L_dummy       VARCHAR2(1);
   L_cost_change COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_sc_cost_event_tbl       OBJ_SC_COST_EVENT_TBL := OBJ_SC_COST_EVENT_TBL();
   ---
   cursor C_COST_CHANGE is
      select distinct cost_change
        from cost_susp_sup_detail cd, item_master im
       where supplier                = I_supplier
         and cd.item                 = im.item
         and (im.item_parent         = I_item
          or im.item_grandparent = I_item)
         and im.item_level          <= im.tran_level
   union all
      select distinct cost_change
        from cost_susp_sup_detail_loc cdl, item_master im
       where supplier                = I_supplier
         and cdl.item                = im.item
         and (im.item_parent         = I_item
          or im.item_grandparent = I_item)
         and im.item_level          <= im.tran_level;
   ---
   cursor C_ITEM_SUPP_COUNTRY_LOC is
      select iscl.item,
             iscl.supplier,
             iscl.origin_country_id,
             iscl.loc
        from item_supp_country_loc iscl, item_master im
       where iscl.supplier        = I_supplier
         and iscl.item            = im.item
         and (im.item_parent      = I_item
              or  im.item_grandparent = I_item);
         
BEGIN
   FOR C_rec in C_ITEM_SUPP_COUNTRY_LOC LOOP
        L_sc_cost_event_tbl.EXTEND;
        L_sc_cost_event_tbl(L_sc_cost_event_tbl.count)  := OBJ_SC_COST_EVENT_REC(C_rec.item,
                                                                                 C_rec.loc,
                                                                                 C_rec.supplier,
                                                                                 C_rec.origin_country_id);
   END LOOP;
   ---
   if L_sc_cost_event_tbl.count > 0 then
      if not FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY (O_error_message,
                                                     L_cost_event_process_id,
                                                     future_cost_event_sql.remove_event,
                                                     L_sc_cost_event_tbl,
                                                     get_user) then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','DEAL_ITEMLOC_ITEM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from deal_itemloc_item
      where (item_parent     = I_item
         or item_grandparent = I_item)
       and deal_id in (select deal_id
                         from deal_head
                        where supplier = I_supplier);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','DEAL_ITEMLOC_PARENT_DIFF', 'Item: '||I_item|| 
                    ' Supplier: '||to_char(I_supplier));                    
   delete from deal_itemloc_parent_diff
      where (item_parent     = I_item
             or item_grandparent = I_item)
        and deal_id in (select deal_id
                          from deal_head
                         where supplier = I_supplier);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','SUP_SKU_AVAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from sup_avail sa
    where supplier = I_supplier
      and exists (select 'x' from item_master im
                   where(item_parent     = I_item
                         or item_grandparent = I_item)
                     and item_level      <= tran_level
                     and sa.item          = im.item);
   ---
   -- Get the cost change associated with the item on cost_susp_sup_detail
   -- this will be used to delete from the header table
   ---
   FOR C_rec in C_COST_CHANGE LOOP
      L_cost_change := C_rec.cost_change;
      ---
      ---
      -- Delete all items with an item/supplier relationship for the item being deleted
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_DETAIL',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      delete from cost_susp_sup_detail cs
       where supplier = I_supplier
         and exists (select 'x' from item_master im
                        where(item_parent     = I_item
                          or item_grandparent = I_item)
                         and item_level      <= tran_level
                         and cs.item          = im.item);
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_DETAIL_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      delete from cost_susp_sup_detail_loc csl
       where supplier = I_supplier
         and exists (select 'x' from item_master im
                        where(item_parent     = I_item
                          or item_grandparent = I_item)
                         and item_level      <= tran_level
                         and csl.item         = im.item);
      ---
      -- if no more detail records exist for that cost change then delete from header table
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_HEAD_CFA_EXT',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      delete from cost_susp_sup_head_cfa_ext
       where cost_change = L_cost_change
         and not exists (select 'x'
                           from cost_susp_sup_detail
                          where cost_change = L_cost_change
                      union all
                         select 'x'
                           from cost_susp_sup_detail_loc
                          where cost_change = L_cost_change);
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_HEAD',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      delete from cost_susp_sup_head
       where cost_change = L_cost_change
         and not exists (select 'x'
                           from cost_susp_sup_detail
                          where cost_change = L_cost_change
                      union all
                         select 'x'
                           from cost_susp_sup_detail_loc
                          where cost_change = L_cost_change);
   END LOOP;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_exp_detail
    where supplier = I_supplier
      and item in (select im.item
                 from item_supplier its, item_master im
                    where supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                    and item_level <= tran_level
                and its.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_EXP_HEAD', 'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_exp_head
    where supplier = I_supplier
      and item  in (select im.item
                from item_supplier its, item_master im
                     where supplier = I_supplier
                     and (im.item_parent = I_item or
                        im.item_grandparent = I_item)
                     and item_level <= tran_level
                 and its.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_UOM', 'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_uom
    where supplier = I_supplier
      and item  in (select im.item
                      from item_supplier its, item_master im
                     where supplier = I_supplier
                       and (im.item_parent = I_item or
                            im.item_grandparent = I_item)
                       and item_level <= tran_level
                       and its.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_dim
    where supplier  = I_supplier
      and item  in (select im.item
                      from item_supp_country_dim iscd, item_master im
                     where supplier = I_supplier
                       and (im.item_parent = I_item or
                            im.item_grandparent = I_item)
                       and item_level <= tran_level
                       and iscd.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_bracket_cost
    where supplier  = I_supplier
      and item in (select im.item
                     from item_supp_country_bracket_cost iscbc, item_master im
                    where iscbc.supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and im.item_level <= im.tran_level
                      and iscbc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from repl_item_loc_supp_dist
    where supplier = I_supplier
      and item in (select im.item
                     from repl_item_loc_supp_dist rilsd, item_master im
                    where rilsd.supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and im.item_level <= im.tran_level
                      and rilsd.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from buyer_wksht_manual
    where supplier = I_supplier
      and item in (select im.item
                     from buyer_wksht_manual bwm, item_master im
                    where bwm.supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and im.item_level <= im.tran_level
                      and bwm.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_LOC_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_loc_cfa_ext isce
    where isce.supplier  = I_supplier
      and isce. item   in (select im.item
                             from item_supp_country_loc iscl, item_master im
                            where iscl.supplier = I_supplier
                              and (im.item_parent = I_item or
                                   im.item_grandparent = I_item)
                              and im.item_level <= im.tran_level
                              and iscl.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_loc
    where supplier  = I_supplier
      and item   in (select im.item
                       from item_supp_country_loc iscl, item_master im
                      where iscl.supplier = I_supplier
                        and (im.item_parent = I_item or
                             im.item_grandparent = I_item)
                        and im.item_level <= im.tran_level
                        and iscl.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_MANU_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_manu_country
    where supplier  = I_supplier
      and item in (select im.item
                     from item_supp_manu_country ismc, item_master im
                    where supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and item_level <= tran_level
                      and ismc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_COST_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_cost_detail
    where supplier  = I_supplier
      and item in (select im.item
                     from item_cost_detail icd, item_master im
                    where supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and item_level <= tran_level
                      and icd.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_COST_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_cost_head
    where supplier  = I_supplier
      and item in (select im.item
                     from item_cost_head ich, item_master im
                    where supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and item_level <= tran_level
                      and ich.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country_cfa_ext isce
    where isce.supplier  = I_supplier
      and isce.item in (select im.item
                          from item_supp_country isc, item_master im
                         where supplier = I_supplier
                           and (im.item_parent = I_item or
                                im.item_grandparent = I_item)
                           and item_level <= tran_level
                           and isc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   delete from item_supp_country
    where supplier  = I_supplier
      and item in (select im.item
                     from item_supp_country isc, item_master im
                    where supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and item_level <= tran_level
                      and isc.item = im.item);
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_SUPP_CHILDREN_RELATIONS;
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
-------------------------BEGIN DELETE FUNCTION--------------------------------------------
BEGIN
---call the checking, locking, and deleting internal functions.
   if not CHECK_SUPP_CHILDREN_DELETE(O_error_message,
                                     L_exists,
                                     I_item,
                                     I_supplier) then
      return FALSE;
   end if;
   if L_exists = TRUE then
      return FALSE;
   end if;
   ---
   if not LOCK_SUPP_CHILDREN_DELETE(O_error_message,
                                    I_supplier,
                                    I_item) then
      return FALSE;
   end if;
   if not DELETE_SUPP_CHILDREN_RELATIONS(O_error_message,
                                         I_item,
                                         I_supplier) then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPPLIER_CFA_EXT','item_supplier_cfa_ext','Item: '||I_item||
                    ' supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPPLIER_CFA_EXT;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPPLIER_CFA_EXT','item_supplier_cfa_ext','Item: '||I_item||
                    ' supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPPLIER_CFA_EXT;
   ---
   L_table := 'ITEM_SUPPLIER_CFA_EXT';
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPPLIER_CFA_EXT', NULL);
   delete from item_supplier_cfa_ext isce
    where isce.supplier = I_supplier
      and isce.item in (select im.item
                          from item_supplier its, item_master im
                         where supplier = I_supplier
                           and (im.item_parent = I_item or
                                im.item_grandparent = I_item)
                           and its.item = im.item);
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPPLIER_TL','item_supplier_tl','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPPLIER_TL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPPLIER_TL','item_supplier_tl','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPPLIER_TL;
   
   L_table := 'ITEM_SUPPLIER_TL';
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPPLIER_TL', NULL);
   delete from item_supplier_tl
    where supplier = I_supplier
      and item in (select im.item
                     from item_supplier its, item_master im
                    where supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and its.item = im.item);
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPPLIER','item_supplier','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPPLIER;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPPLIER','item_supplier','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPPLIER;
   ---
   L_table := 'ITEM_SUPPLIER';
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPPLIER', NULL);
   delete from item_supplier
    where supplier = I_supplier
      and item in (select im.item
                     from item_supplier its, item_master im
                    where supplier = I_supplier
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and its.item = im.item);

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_SUPPLIER_FROM_CHILDREN;
--------------------------------------------------------------------------------
FUNCTION UPDATE_PRIMARY_CHILD_INDS( O_error_message  IN OUT VARCHAR2,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                    I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                    I_location       IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(64)  := 'SUPP_ITEM_SQL.UPDATE_PRIMARY_CHILD_INDS';
   L_table        VARCHAR2(30);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   ---Following cursors called when item/supplier/country/location passed in---
   cursor C_LOCK_LOC_Y is
      select 'x'
        from item_supp_country_loc iscl, item_master im
       where primary_loc_ind   = 'Y'
         and origin_country_id = I_origin_country
         and supplier          = I_supplier
         and loc            != I_location
         and iscl.item       = im.item
         and (im.item_parent      = I_item or
            im.item_grandparent   = I_item)
         and item_level          <= tran_level
       for update of iscl.supplier nowait;
   ---
   cursor C_LOCK_LOC_N is
      select 'x'
        from item_supp_country_loc iscl, item_master im
       where primary_loc_ind   = 'N'
         and origin_country_id = I_origin_country
         and supplier          = I_supplier
         and loc               = I_location
         and iscl.item         = im.item
         and (im.item_parent     = I_item or
             im.item_grandparent = I_item)
         and item_level         <= tran_level
       for update of iscl.supplier nowait;

---Following cursors called when only item/supplier/country passed in---
   cursor C_LOCK_COUNTRY_Y is
      select 'x'
        from item_supp_country isc, item_master im
       where primary_country_ind = 'Y'
         and origin_country_id  != I_origin_country
         and supplier            = I_supplier
         and isc.item            = im.item
         and (im.item_parent     = I_item
          or im.item_grandparent = I_item)
         and item_level         <= tran_level
       for update of isc.supplier nowait;
   ---
   cursor C_LOCK_COUNTRY_N is
      select 'x'
        from item_supp_country isc, item_master im
       where primary_country_ind = 'N'
         and origin_country_id   = I_origin_country
         and supplier            = I_supplier
         and isc.item            = im.item
         and (im.item_parent     = I_item
          or im.item_grandparent = I_item)
         and item_level <= tran_level
       for update of isc.supplier nowait;
   ---
  ---Following cursors called when only item/supplier passed in---
   cursor C_LOCK_SUPPLIER_Y is
      select 'x'
        from item_supplier its, item_master im
       where primary_supp_ind     = 'Y'
         and supplier            != I_supplier
         and its.item             = im.item
         and (im.item_parent      = I_item
          or im.item_grandparent   = I_item)
        for update of its.supplier nowait;
   ---
   cursor C_LOCK_SUPPLIER_N is
      select 'x'
        from item_supplier its, item_master im
       where primary_supp_ind     = 'N'
         and supplier             = I_supplier
         and its.item             = im.item
         and (im.item_parent      = I_item
          or im.item_grandparent = I_item)
       for update of its.supplier nowait;
   ---
   cursor C_LOCK_COUNTRY_SUPP_Y is
      select 'x'
        from item_supp_country isc, item_master im
       where primary_supp_ind     = 'Y'
         and supplier            != I_supplier
         and isc.item             = im.item
         and (im.item_parent      = I_item
          or im.item_grandparent = I_item)
         and item_level         <= tran_level
       for update of isc.supplier nowait;
   ---
   cursor C_LOCK_COUNTRY_SUPP_N is
      select 'x'
        from item_supp_country isc, item_master im
       where supplier             = I_supplier
         and primary_supp_ind     = 'N'
         and isc.item             = im.item
       and (im.item_parent      = I_item
         or im.item_grandparent = I_item)
         and item_level        <= tran_level
       for update nowait;
BEGIN

   if I_location is not NULL then
      -- Lock the item_supp_country_loc record, update the primary location ind to
      -- No for I_item with an location other than the input location

      L_table := 'ITEM_SUPP_COUNTRY_LOC';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_Y','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country
                 ||' Location: '|| I_Location);
      open C_LOCK_LOC_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_Y','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country
                 ||' Location: '|| I_Location);
      close C_LOCK_LOC_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC',
                       'Location: '|| I_Location);

      update item_supp_country_loc iscl1
         set iscl1.primary_loc_ind      = 'N',
             iscl1.last_update_id       = get_user,
             iscl1.last_update_datetime = sysdate
       where iscl1.primary_loc_ind      = 'Y'
         and iscl1.origin_country_id    = I_origin_country
         and iscl1.supplier             = I_supplier
         and iscl1.loc                 != I_location
         and iscl1.item in (select im.item
                              from item_supp_country_loc iscl2, item_master im
                             where iscl2.supplier          = iscl1.supplier
                               and iscl2.origin_country_id = iscl1.origin_country_id
                               and iscl2.loc               = I_location
                               and (im.item_parent         = I_item or
                                    im.item_grandparent    = I_item)
                               and im.item_level          <= im.tran_level
                               and iscl2.item              = im.item );


      ---
      -- Lock the item_supp_country_loc record, update the primary location ind to
      -- Yes for I_item with an location equal to the input location
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_N','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country
                 ||' Location: '|| I_Location);
      open C_LOCK_LOC_N;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_N','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country
                 ||' Location: '|| I_Location);
      close C_LOCK_LOC_N;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC',
                       'Location: '|| I_Location);
      update item_supp_country_loc iscl1
         set iscl1.primary_loc_ind      = 'Y',
             iscl1.last_update_id       = get_user,
             iscl1.last_update_datetime = sysdate
       where iscl1.primary_loc_ind      = 'N'
         and iscl1.origin_country_id    = I_origin_country
         and iscl1.supplier             = I_supplier
         and iscl1.loc                  = I_location
         and iscl1.item in (select im.item
                              from item_supp_country_loc iscl2, item_master im
                             where iscl2.supplier             = iscl1.supplier
                               and iscl2.origin_country_id    = iscl1.origin_country_id
                               and iscl2.loc                  = iscl1.loc
                               and (im.item_parent            = I_item or
                                    im.item_grandparent       = I_item)
                               and im.item_level             <= im.tran_level
                               and iscl2.item                 = im.item);
   elsif I_origin_country is not NULL and I_location is NULL then
      ---
      -- Lock the item_supp_country record, update the primary country ind to
      -- No for I_item with an origin
      -- country other than the input country
      ---
      L_table := 'ITEM_SUPP_COUNTRY';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_Y','ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      open C_LOCK_COUNTRY_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_Y','ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_LOCK_COUNTRY_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY',
                       'Country: '|| I_origin_country);

      update item_supp_country isc1
         set isc1.primary_country_ind    = 'N',
             isc1.last_update_id         = get_user,
             isc1.last_update_datetime   = sysdate
       where isc1.primary_country_ind    = 'Y'
         and isc1.origin_country_id     != I_origin_country
         and isc1.supplier               = I_supplier
         and isc1.item in (select im.item
                             from item_supp_country isc2, item_master im
                            where isc2.supplier           = isc1.supplier
                              and isc2.origin_country_id  = I_origin_country
                              and (im.item_parent         = I_item or
                                   im.item_grandparent    = I_item)
                              and im.item_level          <= im.tran_level
                              and isc2.item               = im.item );

      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_N','ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      open C_LOCK_COUNTRY_N;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_N','ITEM_SUPP_COUNTRY','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_LOCK_COUNTRY_N;
      update item_supp_country isc1
         set isc1.primary_country_ind     = 'Y',
             isc1.last_update_id          = get_user,
             isc1.last_update_datetime    = sysdate
       where isc1.primary_country_ind     = 'N'
         and isc1.origin_country_id       = I_origin_country
         and isc1.supplier                = I_supplier
         and isc1.item in (select im.item
                             from item_supp_country isc2, item_master im
                            where isc2.supplier          = isc1.supplier
                              and isc2.origin_country_id = isc1.origin_country_id
                              and (im.item_parent        = I_item or
                                   im.item_grandparent   = I_item)
                              and im.item_level         <= im.tran_level
                              and isc2.item              = im.item);

   else  --origin country is null for item/supplier combo, update supp ind
      ---
      -- Lock the item_supplier record and update the primary supplier ind to
      -- No for I_item where the supplier
      -- is not equal to the input supplier
      ---
      L_table := 'ITEM_SUPPLIER';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_SUPPLIER_Y','ITEM_SUPPLIER','Item: '|| I_item||
                       ' Supplier: '||to_char(I_supplier));
      open C_LOCK_SUPPLIER_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SUPPLIER_Y','ITEM_SUPPLIER','Item: '||I_item||
                       'Supplier: '||to_char(I_supplier));
      close C_LOCK_SUPPLIER_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPPLIER','Item: '||I_item||' Supplier: '||
                       to_char(I_supplier));

      update item_supplier is1
         set is1.primary_supp_ind     = 'N',
             is1.last_update_id       = get_user,
             is1.last_update_datetime = sysdate
       where is1.primary_supp_ind     = 'Y'
         and is1.supplier            != I_supplier
         and is1.item in (select im.item
                            from item_supplier is2, item_master im
                           where is2.supplier         = I_supplier
                             and (im.item_parent      = I_item or
                                  im.item_grandparent = I_item)
                             and is2.item             = im.item );
      ---
      L_table := 'ITEM_SUPPLIER';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_SUPPLIER_N','ITEM_SUPPLIER',
                       'I_item: '|| I_item||' Supplier: '||to_char(I_supplier));
      open C_LOCK_SUPPLIER_N;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SUPPLIER_N','ITEM_SUPPLIER',
                       'I_item: '|| I_item||' Supplier: '||to_char(I_supplier));
      close C_LOCK_SUPPLIER_N;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPPLIER',
                       'I_item: '|| I_item||' Supplier: '||to_char(I_supplier));
      update item_supplier is1
         set is1.primary_supp_ind     = 'Y',
             is1.last_update_id       = get_user,
             is1.last_update_datetime = sysdate
       where is1.primary_supp_ind     = 'N'
         and is1.supplier             = I_supplier
         and is1.item in (select im.item
                            from item_supplier is2, item_master im
                           where is2.supplier         = is1.supplier
                             and (im.item_parent      = I_item or
                                  im.item_grandparent = I_item)
                             and is2.item             = im.item);
      ---
      -- Lock the item_supp_country record and update the primary supplier ind to
      -- No for I_item where the supplier is
      -- not equal to the input supplier
      ---
      L_table := 'ITEM_SUPP_COUNTRY';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_SUPP_Y','ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      open C_LOCK_COUNTRY_SUPP_Y;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_SUPP_Y','ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      close C_LOCK_COUNTRY_SUPP_Y;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);

      update item_supp_country isc1
         set isc1.primary_supp_ind     = 'N',
             isc1.last_update_id       = get_user,
             isc1.last_update_datetime = sysdate
       where isc1.primary_supp_ind     = 'Y'
         and isc1.supplier            != I_supplier
         and isc1.item in (select im.item
                             from item_supp_country isc2, item_master im
                            where isc2.supplier        = I_supplier
                              and (im.item_parent      = I_item or
                                   im.item_grandparent = I_item)
                              and im.item_level       <= im.tran_level
                              and isc2.item            = im.item );
      ---
      -- Lock the item_supp_country record and update the primary supplier to Yes
      -- for I_item where the supplier is
      -- equal to the input supplier
      ---
      L_table := 'ITEM_SUPP_COUNTRY';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_COUNTRY_SUPP_N','ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      open C_LOCK_COUNTRY_SUPP_N;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_COUNTRY_SUPP_N','ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      close C_LOCK_COUNTRY_SUPP_N;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY',
                       'I_item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       'Country: '|| I_origin_country);
      update item_supp_country isc1
         set isc1.primary_supp_ind     = 'Y',
             isc1.last_update_id       = get_user,
             isc1.last_update_datetime = sysdate
       where isc1.supplier             = I_supplier
         and isc1.primary_supp_ind     = 'N'
         and isc1.item in (select im.item
                             from item_supp_country isc2, item_master im
                            where isc2.supplier       = isc1.supplier
                             and (im.item_parent      = I_item or
                                  im.item_grandparent = I_item)
                             and im.item_level       <= im.tran_level
                             and isc2.item            = im.item);
   end if; -- origin country/location is not null
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;
END UPDATE_PRIMARY_CHILD_INDS;
--------------------------------------------------------------------------------
FUNCTION UPDATE_PRIM_SUPP_FOR_CHILDREN(O_error_message  IN OUT VARCHAR2,
                                       I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                       I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program   VARCHAR2(64)   := 'SUPP_ITEM_SQL.UPDATE_PRIM_SUPP_FOR_CHILDREN';
   L_exists    VARCHAR2(1)    := 'N';
   ---
   cursor C_COUNTRY_EXISTS is
     select 'Y'
       from item_master im
      where (im.item_parent   = I_item
       or im.item_grandparent = I_item)
       and im.item_level     <= im.tran_level
       and exists( select 'x'
                     from item_supplier isc
                    where isc.item     = im.item
                     and isc.supplier  = I_supplier)
                     and not exists( select 'x'
                                       from item_supp_country isc
                                      where isc.supplier = I_supplier
                                       and isc.item      = im.item);
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                       'I_item: '|| I_item||' Supplier: '||to_char(I_supplier));
   open C_COUNTRY_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                       'I_item: '|| I_item||' Supplier: '||to_char(I_supplier));
   fetch C_COUNTRY_EXISTS into L_exists;
   if C_COUNTRY_EXISTS%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                       'I_item: '|| I_item||' Supplier: '||to_char(I_supplier));
      close C_COUNTRY_EXISTS;
      ---
      if not UPDATE_PRIMARY_CHILD_INDS(O_error_message,
                           I_item,
                           I_supplier,
                           NULL,  --I_origin_country
                           NULL) then -- I_location
       return FALSE;
      end if;
      if not UPDATE_BASE_COST.CHANGE_SUPPLIER(O_error_message,
                                I_item,
                                I_supplier,
                                'Y') then
       return FALSE;
      end if;
   else ---Item child is not on item_supp_country for supplier.
      SQL_LIB.SET_MARK('CLOSE', 'C_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                       'I_item: '|| I_item||' Supplier: '||to_char(I_supplier));
      close C_COUNTRY_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_COUNTRY_INFO', NULL, NULL, NULL);
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_PRIM_SUPP_FOR_CHILDREN;
--------------------------------------------------------------------------------
FUNCTION CHECK_PRIM_SUPP_FOR_CHILDREN(O_error_message   IN OUT VARCHAR2,
                                      O_prim_supp_ind   IN OUT ITEM_SUPPLIER.PRIMARY_SUPP_IND%TYPE,
                                      I_supplier        IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                      I_item            IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(64) := 'SUPP_ITEM_SQL.CHECK_PRIM_SUPP_FOR_CHILDREN';
   L_dummy        VARCHAR2(1);
   L_child_item   ITEM_MASTER.ITEM%TYPE;

   cursor C_ITEM_CHILDREN is
     select 'Y'
       from item_master im, item_supplier its
      where (im.item_parent   = I_item
       or im.item_grandparent = I_item)
       and item_level        <= tran_level
       and its.supplier      != I_supplier
       and its.item           = im.item
       and primary_supp_ind   = 'Y';

BEGIN

   O_prim_supp_ind := 'N';

   SQL_LIB.SET_MARK('OPEN','C_ITEM_CHILDREN','item_supplier','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier));
   open C_ITEM_CHILDREN;

   SQL_LIB.SET_MARK('FETCH','C_ITEM_CHILDREN','item_supplier','Item: '||I_item||
                      ' supplier: '||to_char(I_supplier));
   fetch C_ITEM_CHILDREN into L_dummy;
   if C_ITEM_CHILDREN%FOUND then
      O_prim_supp_ind := 'Y';
   else
      O_prim_supp_ind := 'N';
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_ITEM_CHILDREN','item_supplier','Item: '||I_item||
                      ' supplier: '||to_char(I_supplier));
   close C_ITEM_CHILDREN;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_PRIM_SUPP_FOR_CHILDREN;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION DELETE_COUNTRY_FROM_CHILDREN(O_error_message   IN OUT VARCHAR2,
                                      I_item            IN     ITEM_SUPPLIER.ITEM%TYPE,
                                      I_supplier        IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                      I_origin_country  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(64) := 'SUPP_ITEM_SQL.DELETE_COUNTRY_FROM_CHILDREN';
   L_exists       BOOLEAN;
   L_child_item   ITEM_MASTER.ITEM%TYPE;
   L_table        VARCHAR2(30);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ITEM_SUPP_COUNTRY_CE is
      select 'x'
        from item_master im, item_supp_country_cfa_ext isc
       where supplier             = I_supplier
         and origin_country_id    = I_origin_country
         and isc.item             = im.item
         and (im.item_parent      = I_item or
              im.item_grandparent = I_item)
         and item_level          <= tran_level
         for update of isc.supplier nowait;

   cursor C_LOCK_ITEM_SUPP_COUNTRY is
      select 'x'
        from item_master im, item_supp_country isc
       where supplier             = I_supplier
         and origin_country_id    = I_origin_country
         and isc.item             = im.item
         and (im.item_parent      = I_item or
              im.item_grandparent = I_item)
         and item_level          <= tran_level
         for update of isc.supplier nowait;
--------------------------------------------------------------------------------
-------------------------INTERNAL FUNCTIONS-------------------------------------

FUNCTION CHECK_COUNTRY_CHILD_DELETE(O_error_message  IN OUT VARCHAR2,
                                    O_exist          IN OUT BOOLEAN,
                                    I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                    I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                    I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.CHECK_COUNTRY_CHILD_DELETE';
   L_dummy       VARCHAR2(1);
   ---

  cursor C_ORDER_ITEMS_EXISTS is
      select 'x'
        from ordsku o,
             ordhead oh,
             item_master im
       where o.order_no          = oh.order_no
         and oh.supplier         = I_supplier
         and o.origin_country_id = I_origin_country
         and o.item              = im.item
         and (im.item_parent     = I_item
          or im.item_grandparent = I_item)
         and im.item_level      <= im.tran_level;
   ---
   cursor C_ORDLOC_WKSHT_EXISTS is
      select 'x'
        from ordloc_wksht ow,
             ordhead oh
       where ow.order_no          = oh.order_no
         and oh.supplier          = I_supplier
         and ow.origin_country_id = I_origin_country
         and ow.item_parent      = I_item;

   ---
   cursor C_ITEM_SUPP_COUNTRY_EXISTS is
      select 'x'
        from item_supp_country isc, item_master im
       where supplier             = I_supplier
         and origin_country_id    = I_origin_country
         and primary_country_ind  = 'Y'
         and (im.item_parent      = I_item or
            im.item_grandparent = I_item)
         and item_level          <= tran_level
       and isc.item             = im.item;
   ---
   cursor C_PACK_ITEM_EXISTS is
      select 'x'
        from v_packsku_qty v,
             item_supp_country ic
       where v.pack_no            = ic.item
         and ic.supplier          = I_supplier
         and ic.origin_country_id = I_origin_country
       and v.item in (select im.item
                    from item_master im, item_supp_country isc
                 where supplier             = I_supplier
                   and origin_country_id    = I_origin_country
                   and (im.item_parent      = I_item or
                          im.item_grandparent = I_item)
                       and item_level          <= tran_level
                   and isc.item             = im.item);
   ---
   cursor C_SUPP_COUNTRY_ON_REPL is
      select 'x'
        from repl_item_loc ril, item_master im
       where primary_repl_supplier = I_supplier
         and origin_country_id     = I_origin_country
         and (im.item_parent       = I_item or
            im.item_grandparent  = I_item)
         and item_level           <= tran_level
       and ril.item              = im.item;
   ---
   cursor C_SUPP_COUNTRY_ON_ITEM_LOC is
      select 'x'
        from item_loc il, item_master im
       where primary_supp    = I_supplier
         and primary_cntry   = I_origin_country
         and (im.item_parent = I_item or
            im.item_grandparent = I_item)
         and item_level <= tran_level
       and il.item = im.item;

BEGIN
   O_exist := FALSE;
   ---
   -- check the ordsku table for item/supplier/origin country combos
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDER_ITEMS_EXISTS','ORDSKU, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   open  C_ORDER_ITEMS_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_ORDER_ITEMS_EXISTS','ORDSKU, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   fetch C_ORDER_ITEMS_EXISTS into L_dummy;
   ---
   if C_ORDER_ITEMS_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ORDER_ITEMS_EXISTS','ORDSKU, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_ORDER_ITEMS_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_ON_ORDER', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ORDER_ITEMS_EXISTS','ORDSKU, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   close C_ORDER_ITEMS_EXISTS;
   ---
   -- Check the ordloc_wksht table for item/supplier/origin country combos
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDLOC_WKSHT_EXISTS','ORDLOC_WKSHT, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   open  C_ORDLOC_WKSHT_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_ORDLOC_WKSHT_EXISTS','ORDLOC_WKSHT, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   fetch C_ORDLOC_WKSHT_EXISTS into L_dummy;
   ---
   if C_ORDLOC_WKSHT_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_WKSHT_EXISTS','ORDLOC_WKSHT, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_ORDLOC_WKSHT_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_ON_ORDER', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_WKSHT_EXISTS','ORDLOC_WKSHT, ORDHEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   close C_ORDLOC_WKSHT_EXISTS;

   ---
   -- Check if I_origin_country is being used for this item's children on repl_item_loc
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SUPP_COUNTRY_ON_REPL', 'REPL_ITEM_LOC',
                       'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
   open  C_SUPP_COUNTRY_ON_REPL;
   SQL_LIB.SET_MARK('FETCH', 'C_SUPP_COUNTRY_ON_REPL', 'REPL_ITEM_LOC',
                       'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
   fetch C_SUPP_COUNTRY_ON_REPL into L_dummy;
   if C_SUPP_COUNTRY_ON_REPL%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_COUNTRY_ON_REPL', 'REPL_ITEM_LOC',
                          'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
      close C_SUPP_COUNTRY_ON_REPL;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_COUNTRY_ON_REPL', 'REPL_ITEM_LOC',
                          'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
      close C_SUPP_COUNTRY_ON_REPL;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_REPL_COUNTRY_CHILD', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   -- Check if I_origin_country is being used for this item's children on item_loc
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_SUPP_COUNTRY_ON_ITEM_LOC', 'ITEM_LOC',
                       'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
   open  C_SUPP_COUNTRY_ON_ITEM_LOC;
   SQL_LIB.SET_MARK('FETCH', 'C_SUPP_COUNTRY_ON_ITEM_LOC', 'ITEM_LOC',
                       'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
   fetch C_SUPP_COUNTRY_ON_ITEM_LOC into L_dummy;
   if C_SUPP_COUNTRY_ON_ITEM_LOC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_COUNTRY_ON_ITEM_LOC', 'ITEM_LOC',
                          'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
      close C_SUPP_COUNTRY_ON_ITEM_LOC;
   else
      SQL_LIB.SET_MARK('CLOSE', 'C_SUPP_COUNTRY_ON_ITEM_LOC', 'ITEM_LOC',
                          'Item: ' || I_item || 'Origin Country: ' ||  I_origin_country);
      close C_SUPP_COUNTRY_ON_ITEM_LOC;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_PRIM_CNT_CHILD_LOC', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   -- check the item_supp_country table for item/supplier/origin country combos
   ---
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPP_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   open  C_ITEM_SUPP_COUNTRY_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPP_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   fetch C_ITEM_SUPP_COUNTRY_EXISTS into L_dummy;
   ---
   if C_ITEM_SUPP_COUNTRY_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
      close C_ITEM_SUPP_COUNTRY_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_PRI_COUNTRY', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY_EXISTS','ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country);
   close C_ITEM_SUPP_COUNTRY_EXISTS;
   ---
   -- Check if I_item is not a component of a pack that is currently associated with the
   -- Supplier/Origin Country being deleted
   ---
   ---
   SQL_LIB.SET_MARK('OPEN','C_PACK_ITEM_EXISTS','PACKITEM, ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   open  C_PACK_ITEM_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_PACK_ITEM_EXISTS','PACKITEM, ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   fetch C_PACK_ITEM_EXISTS into L_dummy;
   ---
   if C_PACK_ITEM_EXISTS%FOUND then
      SQL_LIB.SET_MARK('CLOSE','C_PACK_ITEM_EXISTS','PACKITEM, ITEM_SUPP_COUNTRY',
                          'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                          ||' Country: '|| I_origin_country);
      close C_PACK_ITEM_EXISTS;
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEL_EXISTS_ON_PACK', NULL, NULL, NULL);
      O_exist := TRUE;
      return TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_PACK_ITEM_EXISTS','PACKITEM, ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)
                       ||' Country: '|| I_origin_country);
   close C_PACK_ITEM_EXISTS;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END CHECK_COUNTRY_CHILD_DELETE;
--------------------------------------------------------------------------------
FUNCTION LOCK_COUNTRY_CHILD_DELETE(O_error_message  IN OUT VARCHAR2,
                                   I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                   I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                   I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(64)   := 'SUPP_ITEM_SQL.LOCK_COUNTRY_CHILD_DELETE';
   L_table        VARCHAR2(30);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_COST_SUSP_SUP_DETAIL is
      select 'x'
        from cost_susp_sup_detail cd, item_master im
       where supplier         = I_supplier
         and origin_country_id = I_origin_country
         and cd.item             = im.item
         and (im.item_parent     = I_item
            or im.item_grandparent = I_item)
         and im.item_level      <= im.tran_level
         for update nowait;
   ---
   cursor C_LOCK_COST_DETAIL_LOC is
      select 'x'
        from cost_susp_sup_detail_loc cdl, item_master im
       where supplier                = I_supplier
         and origin_country_id       = I_origin_country
         and cdl.item                = im.item
         and (im.item_parent         = I_item
            or im.item_grandparent = I_item)
         and im.item_level          <= im.tran_level
         for update nowait;
   ---
   cursor C_LOCK_COST_SUSP_SUP_HEAD_CFA is
      select 'x'
        from cost_susp_sup_head_cfa_ext chcfa
         where exists (select 'x'
                         from cost_susp_sup_detail cd, item_master im
                        where cd.supplier             = I_supplier
                          and cd.origin_country_id    = I_origin_country
                          and cd.item                 = im.item
                          and (im.item_parent         = I_item
                             or im.item_grandparent = I_item)
                          and im.item_level          <= im.tran_level
                          and cd.cost_change          = chcfa.cost_change
                        union all
                       select 'x'
                         from cost_susp_sup_detail_loc cl, item_master im
                        where cl.supplier             = I_supplier
                          and cl.origin_country_id    = I_origin_country
                          and cl.item                 = im.item
                          and (im.item_parent         = I_item
                             or im.item_grandparent = I_item)
                          and im.item_level          <= im.tran_level
                          and cl.cost_change          = chcfa.cost_change)
         for update nowait;
   ---
   cursor C_LOCK_COST_SUSP_SUP_HEAD is
      select 'x'
        from cost_susp_sup_head ch
         where exists (select 'x'
                         from cost_susp_sup_detail cd, item_master im
                        where cd.supplier             = I_supplier
                          and cd.origin_country_id    = I_origin_country
                          and cd.item                 = im.item
                          and (im.item_parent         = I_item
                             or im.item_grandparent = I_item)
                          and im.item_level          <= im.tran_level
                          and cd.cost_change          = ch.cost_change
                        union all
                       select 'x'
                         from cost_susp_sup_detail_loc cl, item_master im
                        where cl.supplier             = I_supplier
                          and cl.origin_country_id    = I_origin_country
                          and cl.item                 = im.item
                          and (im.item_parent         = I_item
                             or im.item_grandparent = I_item)
                          and im.item_level          <= im.tran_level
                          and cl.cost_change          = ch.cost_change)
         for update nowait;
   ---
   cursor C_LOCK_DEAL_ITEMLOC_ITEM is
      select 'x'
        from deal_itemloc_item
       where origin_country_id = I_origin_country
         and (item_parent     = I_item
          or item_grandparent = I_item)
         and deal_id in (select deal_id
                           from deal_head
                          where supplier = I_supplier)
         for update nowait;
   ---
   cursor C_LOCK_DEAL_ITEMLOC_PARENT is
      select 'x'
        from deal_itemloc_parent_diff
       where origin_country_id = I_origin_country
         and (item_parent     = I_item
          or item_grandparent = I_item)
         and deal_id in (select deal_id
                           from deal_head
                          where supplier = I_supplier)
         for update nowait;
   ---
   cursor C_LOCK_ITEM_EXP_DETAIL is
      select 'x'
        from item_exp_detail ied, item_master im
       where ied.supplier     = I_supplier
         and ied.item         = im.item
         and (im.item_parent  = I_item or
                im.item_grandparent = I_item)
         and item_level      <= tran_level
         and ied.item_exp_seq in (select ieh.item_exp_seq
                                    from item_exp_head ieh, item_master im
                                   where ieh.supplier          = I_supplier
                                    and ieh.origin_country_id  = I_origin_country
                                    and ieh.item               = im.item
                                    and (im.item_parent        = I_item or
                                         im.item_grandparent   = I_item)
                                    and item_level            <= tran_level)
         for update of ied.supplier nowait;
   ---
   cursor C_LOCK_ITEM_EXP_HEAD is
      select 'x'
        from item_exp_head ieh, item_master im
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and ieh.item = im.item
         and (im.item_parent = I_item or
              im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of ieh.supplier nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY_DIM is
      select 'x'
        from item_supp_country_dim iscd, item_master im
       where supplier = I_supplier
         and origin_country = I_origin_country
         and iscd.item           = im.item
         and (im.item_parent = I_item or
              im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of iscd.supplier nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_CTRY_BRACKET is
      select 'x'
        from item_supp_country_bracket_cost iscbc, item_master im
       where supplier           = I_supplier
         and origin_country_id  = I_origin_country
         and iscbc.item         = im.item
         and (im.item_parent = I_item or
              im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of iscbc.supplier nowait;
   ---
   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
      select 'x'
        from item_supp_country_loc iscl, item_master im
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and iscl.item         = im.item
         and (im.item_parent = I_item or
              im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of iscl.supplier nowait;
   ---
   cursor C_LOCK_REPL_ITEM_LOC_SUPP_DIST is
      select 'x'
        from repl_item_loc_supp_dist rilsd, item_master im
       where supplier = I_supplier
         and origin_country_id = I_origin_country
         and rilsd.item  = im.item
         and (im.item_parent = I_item or
              im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of rilsd.supplier nowait;
   ---
   cursor C_LOCK_BUYER_WKSHT_MANUAL is
      select 'x'
        from buyer_wksht_manual bwm, item_master im
       where supplier = I_supplier
         and origin_country_id = I_origin_country
         and bwm.item  = im.item
         and (im.item_parent = I_item or
              im.item_grandparent = I_item)
         and item_level <= tran_level
         for update of bwm.supplier nowait;
   ---
   cursor C_LOCK_ITEM_COST_DETAIL is
      select 'x' 
        from item_cost_detail icd, 
             item_master im
       where icd.supplier = I_supplier 
         and icd.origin_country_id = I_origin_country 
         and icd.item = im.item
         and (im.item_parent = I_item 
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
      for update of icd.supplier nowait;
   ---
   cursor C_LOCK_ITEM_COST_HEAD is
      select 'x' 
        from item_cost_head ich, 
             item_master im
       where ich.supplier = I_supplier 
         and ich.origin_country_id = I_origin_country 
         and ich.item = im.item
         and (im.item_parent = I_item 
              or im.item_grandparent = I_item)
         and im.item_level <= im.tran_level
         for update of ich.supplier nowait;
   ---

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COST_SUSP_DETAIL', 'COST_SUSP_SUP_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_COST_SUSP_SUP_DETAIL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COST_SUSP_DETAIL', 'COST_SUSP_SUP_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_COST_SUSP_SUP_DETAIL;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COST_DETAIL_LOC', 'COST_SUSP_SUP_DETAIL_LOC',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_COST_DETAIL_LOC;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COST_DETAIL_LOC', 'COST_SUSP_SUP_DETAIL_LOC',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_COST_DETAIL_LOC;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COST_SUSP_SUP_HEAD_CFA', 'COST_SUSP_SUP_HEAD_CFA_EXT',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_COST_SUSP_SUP_HEAD_CFA;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COST_SUSP_SUP_HEAD_CFA', 'COST_SUSP_SUP_HEAD_CFA_EXT',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_COST_SUSP_SUP_HEAD_CFA;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_COST_SUSP_HEAD', 'COST_SUSP_SUP_HEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_COST_SUSP_SUP_HEAD;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_COST_SUSP_HEAD', 'COST_SUSP_SUP_HEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_COST_SUSP_SUP_HEAD;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_DEAL_ITEMLOC_ITEM', 'DEAL_ITEMLOC_ITEM',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_DEAL_ITEMLOC_ITEM;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DEAL_ITEMLOC_ITEM', 'DEAL_ITEMLOC_ITEM',
                    'Sku or Style: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_DEAL_ITEMLOC_ITEM;
   ---
   SQL_LIB.SET_MARK('OPEN', 
                    'C_LOCK_DEAL_ITEMLOC_PARENT', 
                    'DEAL_ITEMLOC_PARENT_DIFF',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)|| ' Country: '||I_origin_country);
   open C_LOCK_DEAL_ITEMLOC_PARENT;
   SQL_LIB.SET_MARK('CLOSE', 
                    'C_LOCK_DEAL_ITEMLOC_PARENT', 
                    'DEAL_ITEMLOC_PARENT_DIFF',
                    'Sku or Style: '||I_item||' Supplier: '||to_char(I_supplier)|| ' Country: '||I_origin_country);
   close C_LOCK_DEAL_ITEMLOC_PARENT;
   ---
   L_table := 'ITEM_EXP_DETAIL';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_EXP_DETAIL', 'ITEM_EXP_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_ITEM_EXP_DETAIL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_EXP_DETAIL', 'ITEM_EXP_DETAIL',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_ITEM_EXP_DETAIL;
   ---
   L_table := 'ITEM_EXP_HEAD';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_EXP_HEAD', 'ITEM_EXP_HEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   open C_LOCK_ITEM_EXP_HEAD;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_EXP_HEAD', 'ITEM_EXP_HEAD',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   close C_LOCK_ITEM_EXP_HEAD;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_BRACKET_COST';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_CTRY_BRACKET',
                    'ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_CTRY_BRACKET;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_CTRY_BRACKET',
                    'ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_CTRY_BRACKET;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_LOC';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_LOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_LOC;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_DIM';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_DIM','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPP_COUNTRY_DIM;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_DIM','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPP_COUNTRY_DIM;
   ---
   L_table := 'REPL_ITEM_LOC_SUPP_DIST';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_REPL_ITEM_LOC_SUPP_DIST','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item|| 
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_REPL_ITEM_LOC_SUPP_DIST','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_REPL_ITEM_LOC_SUPP_DIST;
   ---
   L_table := 'BUYER_WKSHT_MANUAL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_BUYER_WKSHT_MANUAL','BUYER_WKSHT_MANUAL','Item: '||I_item|| 
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_BUYER_WKSHT_MANUAL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_BUYER_WKSHT_MANUAL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_BUYER_WKSHT_MANUAL;
   return TRUE;
   ---
   L_table := 'ITEM_COST_DETAIL';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_COST_DETAIL','ITEM_COST_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_COST_DETAIL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_COST_DETAIL','ITEM_COST_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_COST_DETAIL;         
   ---
   L_table := 'ITEM_COST_HEAD';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_COST_HEAD','ITEM_COST_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_COST_HEAD;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_COST_HEAD','ITEM_COST_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_COST_HEAD;
   ---
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END LOCK_COUNTRY_CHILD_DELETE;
--------------------------------------------------------------------------------
FUNCTION DELETE_COUNTRY_CHILD_RELATIONS(O_error_message  IN OUT VARCHAR2,
                                        I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                        I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                        I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.DELETE_COUNTRY_CHILD_RELATIONS';
   L_dummy       VARCHAR2(1);
   L_cost_change COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_sc_cost_event_tbl       OBJ_SC_COST_EVENT_TBL := OBJ_SC_COST_EVENT_TBL();
   ---
  cursor C_COST_CHANGE is
      select distinct cost_change
        from cost_susp_sup_detail cd, item_master im
       where supplier              = I_supplier
         and origin_country_id     = I_origin_country
         and cd.item               = im.item
         and (im.item_parent       = I_item
          or im.item_grandparent = I_item)
         and im.item_level        <= im.tran_level
   union all
      select distinct cost_change
        from cost_susp_sup_detail_loc cdl, item_master im
       where supplier              = I_supplier
         and origin_country_id     = I_origin_country
         and cdl.item               = im.item
         and (im.item_parent       = I_item
          or im.item_grandparent = I_item)
         and im.item_level        <= im.tran_level;
   ---
   cursor C_ITEM_SUPP_COUNTRY_LOC is
      select iscl.item,
             iscl.supplier,
             iscl.origin_country_id,
             iscl.loc
        from item_supp_country_loc iscl, item_master im
       where iscl.supplier          = I_supplier
         and iscl.origin_country_id = I_origin_country
         and iscl.item              = im.item
         and (im.item_parent        = I_item
             or im.item_grandparent   = I_item);
BEGIN
   ---
   FOR C_rec in C_ITEM_SUPP_COUNTRY_LOC LOOP
      L_sc_cost_event_tbl.EXTEND;
      L_sc_cost_event_tbl(L_sc_cost_event_tbl.count)  := OBJ_SC_COST_EVENT_REC(C_rec.item,
                                                                               C_rec.loc,
                                                                               C_rec.supplier,
                                                                               C_rec.origin_country_id);
   END LOOP;
   -- Get the cost change associated with the item on cost_susp_sup_detail
   -- this will be used to delete from the header table
   ---
   FOR C_rec in C_COST_CHANGE LOOP
      L_cost_change := C_rec.cost_change;
      ---
      -- Delete all skus with an item/supplier/origin country relationship
      -- for the style being deleted
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_DETAIL',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       ' Country: '||I_origin_country);
      delete from cost_susp_sup_detail cs
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and exists (select 'x' from item_master im
                        where (item_parent     = I_item
                               or item_grandparent = I_item)
                          and item_level      <= tran_level
                          and cs.item          = im.item);
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_DETAIL_LOC',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier)||
                       ' Country: '||I_origin_country);
      delete from cost_susp_sup_detail_loc csl
       where supplier          = I_supplier
         and origin_country_id = I_origin_country
         and exists (select 'x' from item_master im
                        where (item_parent     = I_item
                               or item_grandparent = I_item)
                          and item_level      <= tran_level
                          and csl.item         = im.item);
      ---
      -- if no more detail records exist for that cost change then delete from header table
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_HEAD_CFA_EXT',
                       'Cost_change: '||to_char(L_cost_change));
      delete from cost_susp_sup_head_cfa_ext
       where cost_change = L_cost_change
         and not exists (select 'x'
                           from cost_susp_sup_detail
                          where cost_change = L_cost_change
                      union all
                         select 'x'
                           from cost_susp_sup_detail_loc
                          where cost_change = L_cost_change);
      ---
      SQL_LIB.SET_MARK('DELETE','NULL','COST_SUSP_SUP_HEAD',
                       'Cost_change: '||to_char(L_cost_change));
      delete from cost_susp_sup_head
       where cost_change = L_cost_change
         and not exists (select 'x'
                           from cost_susp_sup_detail
                          where cost_change = L_cost_change
                      union all
                         select 'x'
                           from cost_susp_sup_detail_loc
                          where cost_change = L_cost_change);
   END LOOP;
   ---
   if L_sc_cost_event_tbl.count > 0 then
      if not FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY (O_error_message,
                                                     L_cost_event_process_id,
                                                     future_cost_event_sql.remove_event,
                                                     L_sc_cost_event_tbl,
                                                     get_user) then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','DEAL_ITEMLOC_ITEM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);

   delete from deal_itemloc_item
    where origin_country_id   = I_origin_country
      and (item_parent      = I_item
       or item_grandparent  = I_item)
      and deal_id in (select deal_id
                         from deal_head
                        where supplier = I_supplier);

   ---
   SQL_LIB.SET_MARK('DELETE','NULL', 'DEAL_ITEMLOC_PARENT_DIFF', 'Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from deal_itemloc_parent_diff
    where origin_country_id = I_origin_country
      and (item_parent      = I_item
           or item_grandparent  = I_item)
      and deal_id in (select deal_id
                        from deal_head
                       where supplier = I_supplier);
   ---
   -- Get the item exp seq no from item_exp_head to delete from detail
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_EXP_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_exp_detail ied
    where ied.supplier      = I_supplier
      and ied.item_exp_type = 'C'
      and ied.item in (select im.item
                    from item_master im, item_supp_country isc
                 where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                   and isc.item = im.item)

      and ied.item_exp_seq in (select ieh.item_exp_seq
                                 from item_exp_head ieh, item_master im
                                where ieh.supplier          = I_supplier
                                  and ieh.origin_country_id = I_origin_country
                                  and ieh.item              = im.item
                        and (im.item_parent = I_item or
                                 im.item_grandparent = I_item)
                            and item_level <= tran_level);
   ---
   -- Delete from item_exp_head
   --
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_EXP_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_exp_head
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
                    from item_master im, item_supp_country isc
                 where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                   and isc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_BRACKET_COST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_supp_country_bracket_cost
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
                     from item_master im,
                          item_supp_country isc
                    where supplier = I_supplier
                      and origin_country_id = I_origin_country
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and item_level <= tran_level
                      and isc.item = im.item);
   ---Delete from repl_item_loc_supp_dist
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','REPL_ITEM_LOC_SUPP_DIST','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   delete from repl_item_loc_supp_dist
    where supplier = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
                     from item_master im,
                          item_supp_country isc
                    where supplier = I_supplier
                      and origin_country_id = I_origin_country
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and item_level <= tran_level
                      and isc.item = im.item);
   --- Delete from buyer_wksht_manual
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','BUYER_WKSHT_MANUAL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||
                    ' Country: '||I_origin_country);
   delete from buyer_wksht_manual
    where supplier = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
                     from item_master im,
                          item_supp_country isc
                    where supplier = I_supplier
                      and origin_country_id = I_origin_country
                      and (im.item_parent = I_item or
                           im.item_grandparent = I_item)
                      and item_level <= tran_level
                      and isc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_LOC_CFA_EXT','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_supp_country_loc_cfa_ext
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
               from item_master im, item_supp_country isc
                   where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                   and isc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_supp_country_loc
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
               from item_master im, item_supp_country isc
                   where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                   and isc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_SUPP_COUNTRY_DIM','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_supp_country_dim
    where supplier          = I_supplier
      and origin_country = I_origin_country
      and item in (select im.item
                    from item_master im, item_supp_country isc
                 where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                   and isc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_COST_DETAIL','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_cost_detail
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
                    from item_master im, item_supp_country isc
                 where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                   and isc.item = im.item);
   ---
   SQL_LIB.SET_MARK('DELETE','NULL','ITEM_COST_HEAD','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   delete from item_cost_head
    where supplier          = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
                    from item_master im, item_supp_country isc
                 where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                and isc.item = im.item);
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END DELETE_COUNTRY_CHILD_RELATIONS;
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
-------------------------BEGIN DELETE_COUNTRY_FROM_CHILDREN FUNCTION----------------------
BEGIN
---call the checking, locking, and deleting internal functions.
   if not CHECK_COUNTRY_CHILD_DELETE(O_error_message,
                         L_exists,
                         I_item,
                         I_supplier,
                         I_origin_country) then
      return FALSE;
   end if;
   if L_exists = TRUE then
      return FALSE;
   end if;
   if not LOCK_COUNTRY_CHILD_DELETE(O_error_message,
                        I_item,
                        I_supplier,
                        I_origin_country) then
      return FALSE;
   end if;
   if not DELETE_COUNTRY_CHILD_RELATIONS(O_error_message,
                             I_item,
                             I_supplier,
                             I_origin_country) then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY_CE','ITEM_SUPP_COUNTRY_CFA_EXT','Item: '||I_item||
                    ' supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   open C_LOCK_ITEM_SUPP_COUNTRY_CE;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY_CE','ITEM_SUPP_COUNTRY_CFA_EXT','Item: '||I_item||
                    ' supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   close C_LOCK_ITEM_SUPP_COUNTRY_CE;
   ---
   L_table := 'ITEM_SUPP_COUNTRY_CFA_EXT';
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPP_COUNTRY_CFA_EXT', NULL);
   delete from item_supp_country_cfa_ext
    where supplier = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
                    from item_master im, item_supp_country isc
                 where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                   and isc.item = im.item);
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_SUPP_COUNTRY','item_supp_country','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   open C_LOCK_ITEM_SUPP_COUNTRY;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_SUPP_COUNTRY','item_supp_country','Item: '||I_item||
                       ' supplier: '||to_char(I_supplier)||' Country: '||I_origin_country);
   close C_LOCK_ITEM_SUPP_COUNTRY;
   ---
   L_table := 'ITEM_SUPP_COUNTRY';
   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPP_COUNTRY', NULL);
   delete from item_supp_country
    where supplier = I_supplier
      and origin_country_id = I_origin_country
      and item in (select im.item
                    from item_master im, item_supp_country isc
                 where supplier = I_supplier
                   and origin_country_id = I_origin_country
                   and (im.item_parent = I_item or
                          im.item_grandparent = I_item)
                       and item_level <= tran_level
                   and isc.item = im.item);
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_COUNTRY_FROM_CHILDREN;
-----------------------------------------------------------------------------------------
FUNCTION UPDATE_SUPP_TO_SUB_TRAN_CHILD(O_error_message     IN OUT VARCHAR2,
                                       I_consignment_rate  IN OUT ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
                                       I_concession_rate   IN OUT ITEM_SUPPLIER.CONCESSION_RATE%TYPE,
                                       I_pallet_name       IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                                       I_case_name         IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                                       I_inner_name        IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                                       I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                       I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                                       I_primary_case_size IN     ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE)
   return BOOLEAN is
   ---
   L_program      VARCHAR2(64) := 'SUPP_ITEM_SQL.UPDATE_SUPP_TO_SUB_TRAN_CHILD';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_ITEM_SUPPLIER is
      select 'x'
        from item_supplier isp
       where isp.supplier = I_supplier
         and exists( select 'x'
                       from item_master im
                      where im.item_parent = I_item
                        and im.item        = isp.item )
         for update nowait;
BEGIN
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_SUPPLIER',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_SUPPLIER',
                    'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   open C_LOCK_ITEM_SUPPLIER;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_SUPPLIER',
                    'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   close C_LOCK_ITEM_SUPPLIER;
   ---
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ITEM_SUPPLIER',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier));
   update item_supplier isp
      set isp.consignment_rate      = I_consignment_rate,
          isp.concession_rate       = I_concession_rate,
          isp.pallet_name           = I_pallet_name,
          isp.case_name             = I_case_name,
          isp.inner_name            = I_inner_name,
          isp.last_update_id        = get_user,
          isp.last_update_datetime  = sysdate,
          isp.primary_case_size     = I_primary_case_size
    where isp.supplier = I_supplier
      and exists( select 'x'
                    from item_master im
                   where im.item_parent = I_item
                     and im.item        = isp.item );
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPPLIER',
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_SUPP_TO_SUB_TRAN_CHILD;
-----------------------------------------------------------------------------------------
FUNCTION CNTRY_EXISTS_FOR_ALL_SUPPS( O_error_message  IN OUT VARCHAR2,
                                     O_all_exist      IN OUT BOOLEAN,
                                     O_supplier       IN OUT ITEM_SUPPLIER.SUPPLIER%TYPE,
                                     I_item           IN     ITEM_SUPPLIER.ITEM%TYPE)
   return BOOLEAN is
   ---
   L_program      VARCHAR2(64) := 'CNTRY_EXISTS_FOR_ALL_SUPPS';
   ---
   cursor C_GET_MISSING_ITEMSUPPCTRY is
      select isp.supplier
        from item_supplier isp
       where isp.item = I_item
         and not exists( select 'x'
                           from item_supp_country isc
                          where isc.item     = isp.item
                            and isc.supplier = isp.supplier );
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_MISSING_ITEMSUPPCTRY',
                    'ITEM_SUPPLIER, ITEM_SUPP_COUNTRY',
                    'Item: '||I_item);
   open C_GET_MISSING_ITEMSUPPCTRY;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_MISSING_ITEMSUPPCTRY',
                    'ITEM_SUPPLIER, ITEM_SUPP_COUNTRY',
                    'Item: '||I_item);
   fetch C_GET_MISSING_ITEMSUPPCTRY into O_supplier;
   if C_GET_MISSING_ITEMSUPPCTRY%NOTFOUND then
      O_all_exist := TRUE;
   else
      O_all_exist := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_MISSING_ITEMSUPPCTRY',
                    'ITEM_SUPPLIER, ITEM_SUPP_COUNTRY',
                    'Item: '||I_item);
   close C_GET_MISSING_ITEMSUPPCTRY;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CNTRY_EXISTS_FOR_ALL_SUPPS;
-----------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_ITSUPPCNTRY( O_error_message      IN OUT VARCHAR2,
                                  O_valid              IN OUT BOOLEAN,
                                  I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                  I_supplier           IN OUT ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                  I_origin_country_id  IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN is
   ---
   L_program      VARCHAR2(64) := 'ITSUPPCNTRY_FOR_CHILD_ITSUPP';
   L_child_item   ITEM_MASTER.ITEM%TYPE;
   L_exists       VARCHAR2(1);
   ---
   cursor C_GET_CHILD_ITEMS is
      select im.item
        from item_master im
       where (im.item_parent = I_item or im.item_grandparent = I_item)
         and im.item_level  <= im.tran_level;
   ---
   cursor C_CHECK_COMMON_ITEMSUPPCTRY is
      select 'x'
        from item_supp_country isc1
       where isc1.item     = L_child_item
         and isc1.supplier = I_supplier
         and isc1.origin_country_id = NVL(I_origin_country_id, isc1.origin_country_id)
         and exists( select 'x'
                       from item_supp_country isc2
                      where isc2.item              = I_item
                        and isc2.supplier          = isc1.supplier
                        and isc2.origin_country_id = isc1.origin_country_id );
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_SUPPLIER',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   for rec in C_GET_CHILD_ITEMS LOOP
      L_child_item := rec.item;
      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_COMMON_ITEMSUPPCTRY',
                       'ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||', Supplier: '||I_supplier||', Origin Country: '||I_origin_country_id);
      open C_CHECK_COMMON_ITEMSUPPCTRY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_COMMON_ITEMSUPPCTRY',
                       'ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||', Supplier: '||I_supplier||', Origin Country: '||I_origin_country_id);
      fetch C_CHECK_COMMON_ITEMSUPPCTRY into L_exists;
      if C_CHECK_COMMON_ITEMSUPPCTRY%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_COMMON_ITEMSUPPCTRY',
                          'ITEM_SUPP_COUNTRY',
                          'Item: '||I_item||', Supplier: '||I_supplier||', Origin Country: '||I_origin_country_id);
         close C_CHECK_COMMON_ITEMSUPPCTRY;
         O_valid := FALSE;
         return TRUE;
      else
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_COMMON_ITEMSUPPCTRY',
                          'ITEM_SUPP_COUNTRY',
                          'Item: '||I_item||', Supplier: '||I_supplier||', Origin Country: '||I_origin_country_id);
         close C_CHECK_COMMON_ITEMSUPPCTRY;
      end if;
   end LOOP;
   ---
   O_valid := TRUE;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_CHILD_ITSUPPCNTRY;
--------------------------------------------------------------------------------
FUNCTION CHECK_NEW_PRIMARY_IND( O_error_message      IN OUT VARCHAR2,
                                O_valid              IN OUT BOOLEAN,
                                I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                                I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                                I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN IS
   L_program         VARCHAR2(64)  := 'SUPP_ITEM_SQL.CHECK_NEW_PRIMARY_IND';
   L_country_exists  BOOLEAN;
   L_exists          VARCHAR2(1);
   ---
   cursor CHECK_STANDARD_UOM is
      select 'x'
        from item_master im
       where im.item = I_item
         and exists(select 'x'
                      from uom_class uc1
                     where uc1.uom       = im.standard_uom
                       and uc1.uom_class = 'QTY'
                       and rownum        = 1)
         and exists (select 'x'
                       from uom_class uc2,
                            item_loc il
                      where uc2.uom        = il.selling_uom
                        and il.item       = im.item
                        and uc2.uom_class != 'QTY'
                        and rownum         = 1)
         and rownum = 1;
   ---
   cursor CHECK_PRIMARY_SUPPLIER is
      select 'x'
        from item_supp_country isc
       where isc.item              = I_item
         and isc.supplier          = I_supplier
         and isc.origin_country_id = I_origin_country_id
         and isc.primary_supp_ind  = 'Y';
   ---
   cursor CHECK_CASE_DIMENSION is
      select 'x'
        from item_supp_country_dim iscd,
             item_supp_country isc
       where iscd.item                = I_item
         and iscd.item                = isc.item
         and iscd.supplier            = I_supplier
         and iscd.supplier            = isc.supplier
         and iscd.origin_country      = NVL(I_origin_country_id, iscd.origin_country)
         and iscd.origin_country      = isc.origin_country_id
         and (isc.primary_country_ind = 'Y' or I_origin_country_id is NOT NULL)
         and iscd.dim_object          = 'CA'; --- Case from DIMO code_type
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_SUPPLIER',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   --- Verify that the new primary item-supplier relationship has an origin country relationship.
   if I_origin_country_id is NULL then
      if COUNTRY_EXISTS(O_error_message,
                        L_country_exists,
                        I_item,
                        I_supplier) = FALSE then
         return FALSE;
      end if;
      if L_country_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('ADD_ITEM_SUPP_COUNTRY',
                                               NULL,
                                               NULL,
                                               NULL);
         O_valid := FALSE;
         return TRUE;
      end if;
   end if;
   ---
   --- Determine if there is a need to check for the CASE dimension.
   SQL_LIB.SET_MARK('OPEN',
                    'CHECK_STANDARD_UOM',
                    'ITEM_MASTER, UOM_CLASS, ITEM_LOC',
                    'Item: '||I_item);
   open CHECK_STANDARD_UOM;
   SQL_LIB.SET_MARK('FETCH',
                    'CHECK_STANDARD_UOM',
                    'ITEM_MASTER, UOM_CLASS, ITEM_LOC',
                    'Item: '||I_item);
   fetch CHECK_STANDARD_UOM into L_exists;
   if CHECK_STANDARD_UOM%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'CHECK_STANDARD_UOM',
                       'ITEM_MASTER, UOM_CLASS, ITEM_LOC',
                       'Item: '||I_item);
      close CHECK_STANDARD_UOM;
      O_valid := TRUE;
      return TRUE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'CHECK_STANDARD_UOM',
                       'ITEM_MASTER, UOM_CLASS, ITEM_LOC',
                       'Item: '||I_item);
      close CHECK_STANDARD_UOM;
   end if;
   ---
   --- If the origin country is specified, check if the specified supplier is primary.
   if I_origin_country_id is NOT NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'CHECK_PRIMARY_SUPPLIER',
                       'ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
      open CHECK_PRIMARY_SUPPLIER;
      SQL_LIB.SET_MARK('FETCH',
                       'CHECK_PRIMARY_SUPPLIER',
                       'ITEM_SUPP_COUNTRY',
                       'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
      fetch CHECK_PRIMARY_SUPPLIER into L_exists;
      if CHECK_PRIMARY_SUPPLIER%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'CHECK_PRIMARY_SUPPLIER',
                          'ITEM_SUPP_COUNTRY',
                          'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
         close CHECK_PRIMARY_SUPPLIER;
         O_valid := TRUE;
         return TRUE;
      else
         SQL_LIB.SET_MARK('CLOSE',
                          'CHECK_PRIMARY_SUPPLIER',
                          'ITEM_SUPP_COUNTRY',
                          'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
         close CHECK_PRIMARY_SUPPLIER;
      end if;
   end if;
   ---
   --- Verify that a case dimension object exists for the primary country and the item-supplier relationship.
   SQL_LIB.SET_MARK('OPEN',
                    'CHECK_CASE_DIMENSION',
                    'ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',
                    'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
   open CHECK_CASE_DIMENSION;
   SQL_LIB.SET_MARK('FETCH',
                    'CHECK_CASE_DIMENSION',
                    'ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',
                    'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
   fetch CHECK_CASE_DIMENSION into L_exists;
   if CHECK_CASE_DIMENSION%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'CHECK_CASE_DIMENSION',
                       'ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',
                       'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
      close CHECK_CASE_DIMENSION;
      if I_origin_country_id is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('PRIM_CNTRY_NEED_CASE_DIM',
                                               I_origin_country_id,
                                               I_item,
                                               to_char(I_supplier));
      else
         O_error_message := SQL_LIB.CREATE_MSG('PRIM_SUPP_NEED_CASE_DIM',
                                               to_char(I_supplier),
                                               I_item,
                                               NULL);
      end if;
      O_valid := FALSE;
      return TRUE;
   else
      SQL_LIB.SET_MARK('CLOSE',
                       'CHECK_CASE_DIMENSION',
                       'ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',
                       'Item: '||I_item||', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
      close CHECK_CASE_DIMENSION;
      O_valid := TRUE;
      return TRUE;
   end if;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_NEW_PRIMARY_IND;
--------------------------------------------------------------------------------
-- Function: EXIST (another EXIST function has an additional parameter I_supplier)
--  Purpose: Identifies the presence of an item (with any supplier) on the
--           item_supplier table.
--------------------------------------------------------------------------------
FUNCTION EXIST(O_error_message IN OUT VARCHAR2,
               O_exist         IN OUT BOOLEAN,
               I_item          IN     ITEM_SUPPLIER.ITEM%TYPE)
   return BOOLEAN IS
   ---
   L_dummy       VARCHAR2(1) := 'N';
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.EXIST';

   cursor C_ITEM_SUPPLIER is
      select 'Y'
        from item_supplier
       where item_supplier.item = I_item
         and rownum = 1;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPPLIER','item_supplier','Item: '||I_item);
   open C_ITEM_SUPPLIER;
   ---
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPPLIER','item_supplier','Item: '||I_item);
   fetch C_ITEM_SUPPLIER into L_dummy;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPPLIER','item_supplier','Item: '||I_item);
   close C_ITEM_SUPPLIER;
   ---
   if L_dummy = 'Y' then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END EXIST;
--------------------------------------------------------------------------------
FUNCTION INSERT_SUPP_TO_COMP_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_pack_no       IN     ITEM_MASTER.ITEM%TYPE,
                                  I_supplier      IN     SUPS.SUPPLIER%TYPE)

   RETURN BOOLEAN IS

   L_item    ITEM_MASTER.ITEM%TYPE;
   L_exists  BOOLEAN;
   L_prim    ITEM_SUPPLIER.PRIMARY_SUPP_IND%TYPE;

   cursor C_ITEM is
      select item
        from v_packsku_qty
       where pack_no = I_pack_no;

BEGIN

   open C_ITEM;
   fetch C_ITEM into L_item;
   close C_ITEM;

   if L_item is NOT NULL then
      if NOT SUPP_ITEM_SQL.EXIST(O_error_message,
                                 L_exists,
                                 L_item) then
         return FALSE;
      end if;

      if L_exists then
         L_prim := 'N';
      else
         L_prim := 'Y';
      end if;

      insert into item_supplier(item,
                                 supplier,
                                 primary_supp_ind,
                                 vpn,
                                 supp_label,
                                 consignment_rate,
                                 supp_diff_1,
                                 supp_diff_2,
                                 inner_name,
                                 case_name,
                                 pallet_name,
                                 supp_discontinue_date,
                                 direct_ship_ind,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id,
                                 primary_case_size)
                          select L_item,
                                 I_supplier,
                                 L_prim,
                                 NULL,
                                 NULL,
                                 consignment_rate,
                                 NULL,
                                 NULL,
                                 inner_name,
                                 case_name,
                                 pallet_name,
                                 NULL,
                                 direct_ship_ind,
                                 sysdate,
                                 sysdate,
                                 get_user,
                                 NULL
                            from item_supplier
                           where item = I_pack_no
                             and supplier = I_supplier;

       --Insert the children for component item
       insert into item_supplier(item,
                                 supplier,
                                 primary_supp_ind,
                                 vpn,
                                 supp_label,
                                 consignment_rate,
                                 supp_diff_1,
                                 supp_diff_2,
                                 inner_name,
                                 case_name,
                                 pallet_name,
                                 supp_discontinue_date,
                                 direct_ship_ind,
                                 create_datetime,
                                 last_update_datetime,
                                 last_update_id)
                          select im.item,
                                 I_supplier,
                                 L_prim,
                                 NULL,
                                 NULL,
                                 consignment_rate,
                                 NULL,
                                 NULL,
                                 inner_name,
                                 case_name,
                                 pallet_name,
                                 NULL,
                                 direct_ship_ind,
                                 sysdate,
                                 sysdate,
                                 get_user
                            from item_master im, item_supplier its
                           where (im.item_parent      = L_item or
                                  im.item_grandparent = L_item)
                             and its.item     = L_item
                             and its.supplier = I_supplier
                             and exists (select 'x'
                                           from item_supplier
                                          where supplier          != I_supplier
                                            and item_supplier.item = im.item)
                             and not exists (select 'x'
                                               from item_supplier
                                              where item_supplier.item     = im.item
                                                and item_supplier.supplier = I_supplier); 

   end if;
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'SUPP_ITEM_SQL.INSERT_SUPP_TO_COMP_ITEM',
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_SUPP_TO_COMP_ITEM;
--------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPPLIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item_supp_rec   IN       ITEM_SUPPLIER%ROWTYPE)
   RETURN BOOLEAN IS

   L_item      ITEM_MASTER.ITEM%TYPE;
   L_program   VARCHAR2(50) := 'SUPP_ITEM_SQL.INSERT_ITEM_SUPPLIER';

BEGIN

   SQL_LIB.SET_MARK('INSERT', 
                    NULL, 
                    'ITEM_SUPPLIER',
                    'item: '||I_item_supp_rec.item||
                    ' supplier: '||I_item_supp_rec.supplier);

   insert into item_supplier(item,
                             supplier,
                             primary_supp_ind,
                             vpn,
                             supp_label,
                             consignment_rate,
                             supp_diff_1,
                             supp_diff_2,
                             supp_diff_3,
                             supp_diff_4,
                             pallet_name,
                             case_name,
                             inner_name,
                             supp_discontinue_date,
                             direct_ship_ind,
                             create_datetime,
                             last_update_datetime,
                             last_update_id,
                             concession_rate,
                             primary_case_size)
                      values(I_item_supp_rec.item,
                             I_item_supp_rec.supplier,
                             I_item_supp_rec.primary_supp_ind,
                             I_item_supp_rec.vpn,
                             I_item_supp_rec.supp_label,
                             I_item_supp_rec.consignment_rate,
                             I_item_supp_rec.supp_diff_1,
                             I_item_supp_rec.supp_diff_2,
                             I_item_supp_rec.supp_diff_3,
                             I_item_supp_rec.supp_diff_4,
                             I_item_supp_rec.pallet_name,
                             I_item_supp_rec.case_name,
                             I_item_supp_rec.inner_name,
                             I_item_supp_rec.supp_discontinue_date,
                             I_item_supp_rec.direct_ship_ind,
                             sysdate,
                             sysdate,
                             get_user,
                             I_item_supp_rec.concession_rate,
                             I_item_supp_rec.primary_case_size);
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_ITEM_SUPPLIER;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item_supp_rec IN ITEM_SUPPLIER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'SUPP_ITEM_SQL.UPDATE_ITEM_SUPPLIER';
   L_table_state  VARCHAR2(10); --- Record may be locked when updating primary_supp_ind

BEGIN
   if not LOCK_ITEM_SUPPLIER(O_error_message,
                             L_table_state,
                     I_item_supp_rec.item,
                             I_item_supp_rec.supplier) then
      if L_table_state != 'LOCKED' then
         return FALSE;
      end if;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_SUPPLIER','item: '||I_item_supp_rec.item||
                                                    ' supplier: '||I_item_supp_rec.supplier);

   update item_supplier
      set vpn                   = I_item_supp_rec.vpn,
          supp_label            = I_item_supp_rec.supp_label,
          supp_discontinue_date = I_item_supp_rec.supp_discontinue_date,
          last_update_datetime  = sysdate,
          last_update_id        = I_item_supp_rec.last_update_id,
          primary_case_size     = I_item_supp_rec.primary_case_size,
          consignment_rate      = nvl(I_item_supp_rec.consignment_rate,consignment_rate),
          concession_rate       = nvl(I_item_supp_rec.concession_rate,concession_rate),
          supp_diff_1           = I_item_supp_rec.supp_diff_1,
          supp_diff_2           = I_item_supp_rec.supp_diff_2,
          supp_diff_3           = I_item_supp_rec.supp_diff_3,
          supp_diff_4           = I_item_supp_rec.supp_diff_4          
    where item                  = I_item_supp_rec.item
      and supplier              = I_item_supp_rec.supplier;
   ---
   if I_item_supp_rec.pallet_name is NOT NULL then
      update item_supplier
         set pallet_name = I_item_supp_rec.pallet_name
       where item        = I_item_supp_rec.item
         and supplier    = I_item_supp_rec.supplier;
   end if;
   ---
   if I_item_supp_rec.case_name is NOT NULL then
      update item_supplier
         set case_name = I_item_supp_rec.case_name
       where item      = I_item_supp_rec.item
         and supplier  = I_item_supp_rec.supplier;
   end if;
   ---
   if I_item_supp_rec.inner_name is NOT NULL then
      update item_supplier
         set inner_name = I_item_supp_rec.inner_name
       where item       = I_item_supp_rec.item
         and supplier   = I_item_supp_rec.supplier;
   end if;
   ---
   if I_item_supp_rec.direct_ship_ind is NOT NULL then
      update item_supplier
         set direct_ship_ind = I_item_supp_rec.direct_ship_ind
       where item            = I_item_supp_rec.item
         and supplier        = I_item_supp_rec.supplier;
   end if;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ITEM_SUPPLIER;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_YES_PRIM_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_item_supp_rec IN ITEM_SUPPLIER%ROWTYPE)
   RETURN BOOLEAN IS

   --- NOTE:  only used for XITEM subscription API after UPDATE_PRIMARY_INDICATORS is called
   L_program      VARCHAR2(50) := 'SUPP_ITEM_SQL.UPDATE_YES_PRIM_IND';
   L_table_state  VARCHAR2(10); --- Record may be locked when updating primary_supp_ind to N

BEGIN
   if not LOCK_ITEM_SUPPLIER(O_error_message,
                             L_table_state,
                     I_item_supp_rec.item,
                             I_item_supp_rec.supplier) then
      if L_table_state != 'LOCKED' then
         return FALSE;
      end if;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_SUPPLIER','item: '||I_item_supp_rec.item||
                                                    ' supplier: '||I_item_supp_rec.supplier);

   update item_supplier
      set  primary_supp_ind     = 'Y',
           last_update_datetime = sysdate,
           last_update_id       = I_item_supp_rec.last_update_id
    where item                  = I_item_supp_rec.item
      and supplier              = I_item_supp_rec.supplier;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_YES_PRIM_IND;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                              I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'SUPP_ITEM_SQL.DELETE_ITEM_SUPPLIER';
   L_table_state  VARCHAR2(10);
BEGIN
   
   if not LOCK_ITEM_SUPPLIER(O_error_message,
                             L_table_state,
                             I_item,
                             I_supplier) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPPLIER_CFA_EXT','item: '||I_item||
                    ' supplier: '||I_supplier);
   delete from item_supplier_cfa_ext
    where item     = I_item
      and supplier = I_supplier;

   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPPLIER_TL','item: '||I_item||
                    ' supplier: '||I_supplier);
   delete from item_supplier_tl
    where item     = I_item
      and supplier = I_supplier;

   SQL_LIB.SET_MARK('DELETE', NULL, 'ITEM_SUPPLIER','item: '||I_item||
                    ' supplier: '||I_supplier);
   delete from item_supplier
    where item     = I_item
      and supplier = I_supplier;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ITEM_SUPPLIER;
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_SUPPLIER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_table_state   IN OUT VARCHAR2,
                            I_item          IN     ITEM_SUPPLIER.ITEM%TYPE,
                            I_supplier      IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'SUPP_ITEM_SQL.LOCK_ITEM_SUPPLIER';
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);

   CURSOR C_LOCK_ITEM_SUPP_TL IS
      select 'x'
        from ITEM_SUPPLIER_TL
       where item = I_item
         and supplier = I_supplier
         for update nowait;
         
   CURSOR C_LOCK_ITEM_SUPP IS
      select 'x'
        from ITEM_SUPPLIER
       where item = I_item
         and supplier = I_supplier
         for update nowait;

   CURSOR C_LOCK_ITEM_SUPP_CFA_EXT IS
      select 'x'
        from ITEM_SUPPLIER_CFA_EXT
       where item = I_item
         and supplier = I_supplier
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_SUPP_TL', 'ITEM_SUPPLIER_TL','item: '||I_item||
                                                    ' supplier: '||I_supplier);
   open C_LOCK_ITEM_SUPP_TL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_SUPP_TL', 'ITEM_SUPPLIER_TL','item: '||I_item||
                                                    ' supplier: '||I_supplier);
   close C_LOCK_ITEM_SUPP_TL;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_SUPP', 'ITEM_SUPPLIER','item: '||I_item||
                                                    ' supplier: '||I_supplier);
   open C_LOCK_ITEM_SUPP;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_SUPP', 'ITEM_SUPPLIER','item: '||I_item||
                                                    ' supplier: '||I_supplier);
   close C_LOCK_ITEM_SUPP;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_SUPP_CFA_EXT ', 'ITEM_SUPPLIER_CFA_EXT','item: '||I_item||
                                                    ' supplier: '||I_supplier);
   open C_LOCK_ITEM_SUPP_CFA_EXT ;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_SUPP_CFA_EXT ', 'ITEM_SUPPLIER_CFA_EXT','item: '||I_item||
                                                    ' supplier: '||I_supplier);
   close C_LOCK_ITEM_SUPP_CFA_EXT ;
   ---
   O_table_state := 'LOCKED';

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPPLIER',
                                            I_item,
                                            to_char(I_supplier));
      O_table_state := 'LOCKED';
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_ITEM_SUPPLIER;
---------------------------------------------------------------------
FUNCTION VALID_DEAL_ITEM(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid           IN OUT BOOLEAN,
                         I_item            IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                         I_supplier        IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_country_id      IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_diff            IN     ITEM_MASTER.DIFF_1%TYPE,
                         I_merch_lvl       IN     CODE_DETAIL.CODE%TYPE,
                         I_partner_id      IN     PARTNER.PARTNER_ID%TYPE,
                         I_partner_type    IN     PARTNER.PARTNER_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'SUPP_ITEM_SQL.VALID_DEAL_ITEM';
   L_isc_exists  BOOLEAN;

   cursor C_GET_CHILDREN is
      select item
        from item_master
       where item_parent = I_item;

   cursor C_GET_ITEMDIFF is
      select item
        from item_master
       where item_parent = I_item
         and ((diff_1 = I_diff and I_merch_lvl = '8')
          or  (diff_2 = I_diff and I_merch_lvl = '9')
          or  (diff_3 = I_diff and I_merch_lvl = '10')
          or  (diff_4 = I_diff and I_merch_lvl = '11'));

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if (I_supplier is NULL and I_partner_id is NULL) then
      if I_supplier is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_supplier',
                                               L_program,
                                               NULL);
         return FALSE;
      else
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_partner_id',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---
   if I_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_valid := FALSE;

   if I_merch_lvl = '12' then
      if I_partner_type = 'S' then
         if SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                                   L_isc_exists,
                                                   I_item,
                                                   I_supplier,
                                                   I_country_id) = FALSE then
            return FALSE;
         end if;
      else
         if SUPP_ITEM_SQL.ITEM_PART_COUNTRY_EXISTS(O_error_message,
                                                   L_isc_exists,
                                                   I_item,
                                                   I_partner_id,
                                                   I_country_id,
                                                   I_partner_type) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if L_isc_exists then
         O_valid := TRUE;
      end if;
   elsif I_merch_lvl = '7' then
      FOR rec IN C_GET_CHILDREN LOOP
         if I_partner_type = 'S' then
            if SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                                      L_isc_exists,
                                                      rec.item,
                                                      I_supplier,
                                                      I_country_id) = FALSE then
               return FALSE;
            end if;
         else
            if SUPP_ITEM_SQL.ITEM_PART_COUNTRY_EXISTS(O_error_message,
                                                      L_isc_exists,
                                                      rec.item,
                                                      I_partner_id,
                                                      I_country_id,
                                                      I_partner_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_isc_exists then
            O_valid := TRUE;
            exit;
         end if;
      END LOOP;
   elsif I_merch_lvl in ('8','9','10','11') then
      if I_diff is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_diff',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      ---
      FOR rec IN C_GET_ITEMDIFF LOOP
         if I_partner_type = 'S' then
            if SUPP_ITEM_SQL.ITEM_SUPP_COUNTRY_EXISTS(O_error_message,
                                                      L_isc_exists,
                                                      rec.item,
                                                      I_supplier,
                                                      I_country_id) = FALSE then
               return FALSE;
            end if;
         else
            if SUPP_ITEM_SQL.ITEM_PART_COUNTRY_EXISTS(O_error_message,
                                                      L_isc_exists,
                                                      rec.item,
                                                      I_partner_id,
                                                      I_country_id,
                                                      I_partner_type) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if L_isc_exists then
            O_valid := TRUE;
            exit;
         end if;
      END LOOP;
   end if;
   ---
   if NOT L_isc_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_SUPP_CNTRY',
                                            NULL,
                                            NULL,
                                            NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALID_DEAL_ITEM;
----------------------------------------------------------------------------------
FUNCTION COST_CONTROL_LEVEL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_cost_control_level IN OUT VARCHAR2,
                            I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                            I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                            I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN is

   L_dummy       VARCHAR2(1);
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.COST_CONTROL_LEVEL';

   cursor C_COST_CONTROL_LEVEL is
      select 'x'
        from item_supp_country_loc iscl,
             item_supp_country isc
       where rownum = 1
         and iscl.unit_cost <> isc.unit_cost
         and isc.origin_country_id = iscl.origin_country_id
         and isc.supplier          = iscl.supplier
         and isc.item              = iscl.item
         and isc.origin_country_id = I_origin_country_id
         and isc.supplier          = I_supplier
         and isc.item              = I_item;

BEGIN
   O_cost_control_level := NULL;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_COST_CONTROL_LEVEL',
                    'ITEM_SUPP_COUNTRY_LOC',
                    'Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
   open C_COST_CONTROL_LEVEL;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_COST_CONTROL_LEVEL',
                    'ITEM_SUPP_COUNTRY_LOC',
                    'Item: '||I_item||' supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
   fetch C_COST_CONTROL_LEVEL into L_dummy;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COST_CONTROL_LEVEL',
                    'ITEM_SUPP_COUNTRY',
                    'Item: '||I_item||' supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
   close C_COST_CONTROL_LEVEL;
   ---
   if L_dummy is NOT NULL then
      O_cost_control_level := 'Item_Supp_Country_Loc';
   else
      O_cost_control_level := 'Item_Supp_Country';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END COST_CONTROL_LEVEL;
----------------------------------------------------------------------------------
FUNCTION NULLIFY_PRIMARY_CASE_SIZE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item               IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.NULLIFY_PRIMARY_CASE_SIZE';

BEGIN

   update item_supplier
      set primary_case_size    = NULL,
          last_update_datetime = sysdate,
          last_update_id       = get_user
    where item                 = I_item;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END NULLIFY_PRIMARY_CASE_SIZE;
----------------------------------------------------------------------------------
FUNCTION PRIMARY_CASE_SIZE_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exist         IN OUT BOOLEAN,
                                 I_item          IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN IS
   ---

   L_program             VARCHAR2(64) := 'SUPP_ITEM_SQL.PRIMARY_CASE_SIZE_EXIST';
   L_primary_case_size   ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE;

   cursor C_ITEM_SUPPLIER is
      select primary_case_size
        from item_supplier
       where item_supplier.item = I_item;

BEGIN

   O_exist := TRUE;

   FOR rec in C_ITEM_SUPPLIER LOOP
      if rec.primary_case_size is NULL then
         O_exist := FALSE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END PRIMARY_CASE_SIZE_EXIST;
--------------------------------------------------------------------------------
FUNCTION DEFAULT_CHILD_AIP_INFO(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                                I_aip_case_type      IN     ITEM_MASTER.AIP_CASE_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'SUPP_ITEM_SQL.DEFAULT_CHILD_AIP_INFO';
   L_item_parent         ITEM_MASTER.ITEM_PARENT%TYPE;
   L_item_gchild         ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_CHILD is
         select item
           from item_master
          where item_parent = I_item
          for update nowait;

   cursor C_GCHILD is
      select item
        from item_master
       where item_parent = L_item_parent
      and item_grandparent = I_item
      for update nowait;

BEGIN

   if I_aip_case_type = 'F' then
      FOR child in C_CHILD LOOP

         L_item_parent := child.item;

         update item_master
            set aip_case_type = I_aip_case_type
          where item = L_item_parent;

         update item_supplier
            set primary_case_size    = NULL,
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where item = L_item_parent;

         FOR gchild in C_GCHILD LOOP

            L_item_gchild := gchild.item;

            update item_master
               set aip_case_type = I_aip_case_type
             where item = L_item_gchild;

            update item_supplier
               set primary_case_size    = NULL,
                   last_update_datetime = sysdate,
                   last_update_id       = get_user
             where item = L_item_gchild;

         END LOOP;

      END LOOP;
   else
      FOR child in C_CHILD LOOP

         L_item_parent := child.item;

         update item_master
            set aip_case_type = I_aip_case_type
          where item = L_item_parent;

         FOR gchild in C_GCHILD LOOP

            L_item_gchild := gchild.item;

            update item_master
               set aip_case_type = I_aip_case_type
             where item = L_item_gchild;

         END LOOP;

      END LOOP;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('REC_LOCKED');
      return FALSE;   
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,to_char(SQLCODE));
      return FALSE;
END DEFAULT_CHILD_AIP_INFO;
--------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_DEAL_EXISTS(O_error_message  IN OUT VARCHAR2,
                                O_exist          IN OUT BOOLEAN,
                                I_item           IN     ITEM_SUPPLIER.ITEM%TYPE,
                                I_supplier       IN     ITEM_SUPPLIER.SUPPLIER%TYPE)
   RETURN BOOLEAN IS
   L_program              VARCHAR2(64)    := 'SUPP_ITEM_SQL.CHECK_SUPP_DEAL_EXISTS';
   L_supplier             SUPS.SUPPLIER%TYPE;
   L_item_level           ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level           ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_supplier_sites_ind   SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE;
   --- 
   cursor C_CHECK_SUP_SITES_DEAL is
   select distinct dh.supplier
     from deal_head dh,
          deal_item_loc_explode dile,
          item_master im,
          sups s
    where dh.deal_id = dile.deal_id
      and dile.supplier = s.supplier
      and (im.item = I_item
        or im.item_parent = I_item)
      and dile.item = im.item
      and dile.supplier = I_supplier
      and dh.status = 'A'
      and not exists (select 1
                        from item_supplier its,
                             sups s1
                       where its.item = I_item
                         and s.supplier_parent = s1.supplier_parent
                         and its.supplier = s1.supplier
                         and its.supplier <> I_supplier);
   ---
   cursor C_CHECK_SUPPLIER_DEAL is
   select distinct dile.supplier
     from deal_head dh,
          deal_item_loc_explode dile,
          item_master im,
          sups s
    where dh.deal_id = dile.deal_id
      and dile.supplier = s.supplier
      and (im.item = I_item
        or im.item_parent = I_item)
      and dile.item = im.item
      and dile.supplier = I_supplier
      and dh.status = 'A';
BEGIN
   O_exist := FALSE;
   ---
   -- Get the item level and transaction level of the item
   -- 
   if not ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                     L_item_level,
                                     L_tran_level,
                                     I_item) then
      return FALSE;
   end if;
   ---
   -- Check system options for supplier_sites_ind; If it is yes then
   -- Check for the deal based on the supplier sites else based on supplier
   ---
   if not SYSTEM_OPTIONS_SQL.GET_SUPPLIER_SITES_IND(O_error_message,
                                                    L_supplier_sites_ind) then
      return FALSE;
   end if;
   ---
   if L_supplier_sites_ind = 'Y' then
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SUP_SITES_DEAL','DEAL_HEAD,DEAL_ITEM_LOC_EXPLODE,ITEM_MASTER,SUPS',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      open C_CHECK_SUP_SITES_DEAL;
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SUP_SITES_DEAL','DEAL_HEAD,DEAL_ITEM_LOC_EXPLODE,ITEM_MASTER,SUPS,ITEM_SUPPLIER',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      fetch C_CHECK_SUP_SITES_DEAL into L_supplier;
      if C_CHECK_SUP_SITES_DEAL%FOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SUP_SITES_DEAL','DEAL_HEAD,DEAL_ITEM_LOC_EXPLODE,ITEM_MASTER,SUPS',
                          'Item: '||I_item||' Supplier: '||to_char(I_supplier));
         close C_CHECK_SUP_SITES_DEAL;
         O_exist := TRUE;
         if L_item_level < L_tran_level then
            O_error_message := SQL_LIB.CREATE_MSG('ACTIVE_CHILD_DEAL_EXISTS',
                                                   I_item,
                                                   to_char(L_supplier),
                                                   NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('ACTIVE_DEAL_EXISTS',
                                                   I_item,
                                                   to_char(L_supplier),
                                                   NULL);
         end if;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SUP_SITES_DEAL','DEAL_HEAD,DEAL_ITEM_LOC_EXPLODE,ITEM_MASTER,SUPS',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      close C_CHECK_SUP_SITES_DEAL;
   else
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SUP_SITES_DEAL','DEAL_HEAD,DEAL_ITEM_LOC_EXPLODE,ITEM_MASTER,SUPS',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      open C_CHECK_SUP_SITES_DEAL;
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SUP_SITES_DEAL','DEAL_HEAD,DEAL_ITEM_LOC_EXPLODE,ITEM_MASTER,SUPS',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      fetch C_CHECK_SUP_SITES_DEAL into L_supplier;
      if C_CHECK_SUP_SITES_DEAL%FOUND then
         SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SUP_SITES_DEAL','DEAL_HEAD,DEAL_ITEM_LOC_EXPLODE,ITEM_MASTER,SUPS',
                          'Item: '||I_item||' Supplier: '||to_char(I_supplier));
         close C_CHECK_SUP_SITES_DEAL;
         O_exist := TRUE;
         if L_item_level < L_tran_level then
            O_error_message := SQL_LIB.CREATE_MSG('ACTIVE_CHILD_DEAL_EXISTS',
                                                   I_item,
                                                   to_char(L_supplier),
                                                   NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('ACTIVE_DEAL_EXISTS',
                                                   I_item,
                                                   to_char(L_supplier),
                                                   NULL);
         end if;
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SUP_SITES_DEAL','DEAL_HEAD,DEAL_ITEM_LOC_EXPLODE,ITEM_MASTER,SUPS',
                       'Item: '||I_item||' Supplier: '||to_char(I_supplier));
      close C_CHECK_SUP_SITES_DEAL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_SUPP_DEAL_EXISTS;
--------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_SUPPLIER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item            IN       ITEM_SUPPLIER_TL.ITEM%TYPE,
                                 I_supplier        IN       ITEM_SUPPLIER_TL.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'SUPP_ITEM_SQL.DELETE_ITEM_SUPPLIER_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ITEM_SUPPLIER_TL is
      select 'x'
        from item_supplier_tl
        where item = I_item and supplier = I_supplier
        for update nowait;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
    if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'ITEM_SUPPLIER_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_SUPPLIER_TL',
                    L_table,
                    ' I_item '||I_item ||' I_supplier '||I_supplier);
   open C_LOCK_ITEM_SUPPLIER_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_SUPPLIER_TL',
                    L_table,
                    ' I_item '||I_item ||' I_supplier '||I_supplier);
   close C_LOCK_ITEM_SUPPLIER_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ITEM_SUPPLIER_TL',
                    ' I_item '||I_item ||' I_supplier '||I_supplier);
                    
   delete from item_supplier_tl
    where item = I_item 
    and supplier = I_supplier;
    
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_ITEM_SUPPLIER_TL;
--------------------------------------------------------------------------------
FUNCTION DEFAULT_PRIMARY_CASE_SIZE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item               IN     ITEM_SUPPLIER.ITEM%TYPE)
   RETURN BOOLEAN is
   L_program     VARCHAR2(64) := 'SUPP_ITEM_SQL.DEFAULT_PRIMARY_CASE_SIZE';
   L_default_uop ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE := NULL ;
   
   cursor C_DEFAULT_UOP is
      select isc.default_uop , its.supplier
        from item_supplier its, item_supp_country isc
       where its.supplier =isc.supplier(+)
         and its.item =isc.item(+)
         and its.item = I_item
         and isc.PRIMARY_COUNTRY_IND(+)='Y';

BEGIN

   FOR rec in C_DEFAULT_UOP LOOP
      if rec.default_uop is null then
         L_default_uop:='C';
      else
         L_default_uop:='S';
      end if;
      update item_supplier
         set primary_case_size    = L_default_uop,
             last_update_datetime = sysdate,
             last_update_id       = user
       where item                 = I_item
         and supplier             = rec.supplier;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END DEFAULT_PRIMARY_CASE_SIZE;
----------------------------------------------------------------------------------
END SUPP_ITEM_SQL;

/