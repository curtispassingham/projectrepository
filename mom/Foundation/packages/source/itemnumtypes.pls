
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_NUMBER_TYPE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------
FUNCTION VALIDATE_FORMAT(O_error_message   IN OUT VARCHAR2,
                         I_item_no         IN ITEM_MASTER.ITEM%TYPE,
                         I_item_type       IN ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------
FUNCTION CHECK_VPLU_FMT(O_error_message   IN OUT VARCHAR2,
                        I_item_no         IN ITEM_MASTER.ITEM%TYPE,
                        I_item_type       IN ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                        I_format_id       IN VAR_UPC_EAN.FORMAT_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------
FUNCTION CHECK_VPLU_FMT(O_error_message   IN OUT VARCHAR2,
                        I_plu_code        IN ITEM_MASTER.ITEM%TYPE,
                        I_prefix_code     IN VAR_UPC_EAN.DEFAULT_PREFIX%TYPE,
                        I_format_id       IN VAR_UPC_EAN.FORMAT_ID%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------
FUNCTION GET_NEXT(O_error_message   IN OUT VARCHAR2,
                  IO_item_no        IN OUT ITEM_MASTER.ITEM%TYPE,
                  I_item_type       IN ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE)
return BOOLEAN;
END ITEM_NUMBER_TYPE_SQL;
/


