create or replace PACKAGE BODY CORESVC_UOM_CLASS AS
   cursor C_SVC_UOM_X_CONVERSION(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_uom_x_conversion.rowid  AS pk_uom_x_conversion_rid,
             st.rowid AS st_rid,


             st.convert_sql,
             st.to_uom_class,
             st.from_uom_class,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_uom_x_conversion st,
             uom_x_conversion pk_uom_x_conversion,


             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.to_uom_class         = pk_uom_x_conversion.to_uom_class (+)
         and st.from_uom_class         = pk_uom_x_conversion.from_uom_class (+);




   cursor C_SVC_UOM_CONVERSION(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_uom_conversion.rowid  AS pk_uom_conversion_rid,
             st.rowid AS st_rid,
             uoc_umc_fk1.rowid    AS uoc_umc_fk1_rid,
             uoc_umc_fk2.rowid    AS uoc_umc_fk2_rid,
             uoc_umc_fk1.uom_class AS umc_to_class,
             uoc_umc_fk2.uom_class AS umc_from_class,
             st.operator,
             st.factor,
             st.to_uom,
             st.from_uom,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_uom_conversion st,
             uom_conversion pk_uom_conversion,
             uom_class uoc_umc_fk1,
             uom_class uoc_umc_fk2,
             dual
       where st.process_id       = I_process_id
         and st.chunk_id         = I_chunk_id
         and st.to_uom           = pk_uom_conversion.to_uom (+)
         and st.from_uom         = pk_uom_conversion.from_uom (+)
         and st.to_uom           = uoc_umc_fk1.uom (+)
         and st.from_uom         = uoc_umc_fk2.uom (+);

   cursor C_SVC_UOM_CLASS_TL(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_uom_class_tl.rowid  AS pk_uom_class_tl_rid,
             st.rowid AS st_rid,
             uct_uc_fk.rowid    AS uct_uc_fk_rid,
             cd_lang.rowid      AS cd_lang_rid,
             st.uom_desc_trans,
             st.uom_trans,
             st.uom,
             st.lang,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_uom_class_tl st,
             uom_class_tl pk_uom_class_tl,
             uom_class uct_uc_fk,
			 code_detail cd_lang,
             dual
       where st.process_id             = I_process_id
         and st.chunk_id               = I_chunk_id
         and st.uom                    = pk_uom_class_tl.uom (+)
         and st.lang                   = pk_uom_class_tl.lang (+)
         and st.uom                    = uct_uc_fk.uom (+)
         and st.lang                   = cd_lang.code (+)
         and cd_lang.code_type (+)     = 'LANG';


   cursor C_SVC_UOM_CLASS(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_uom_class.rowid  AS pk_uom_class_rid,
             st.rowid AS st_rid,
			       cd_uomc.rowid as cduomc_rid,
             st.uom_class,
             st.uom_desc_trans,
             st.uom_trans,
			       pk_uom_class.uom_class as uom_class_old,
             st.uom,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_uom_class st,
             uom_class pk_uom_class,
			 code_detail cd_uomc,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.uom         = pk_uom_class.uom (+)
		     and st.uom_class   = cd_uomc.code(+)
		     and cd_uomc.code_type(+) = 'UOMC';

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   LP_primary_lang    LANG.LANG%TYPE :=  LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;

   ----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E')IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
----------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   UOM_X_CONVERSION_cols s9t_pkg.names_map_typ;
   UOM_CONVERSION_cols s9t_pkg.names_map_typ;
   UOM_CLASS_TL_cols s9t_pkg.names_map_typ;
   UOM_CLASS_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                           :=s9t_pkg.get_sheet_names(I_file_id);
   UOM_X_CONVERSION_cols              :=s9t_pkg.get_col_names(I_file_id,UOM_X_CONVERSION_sheet);
   UOM_X_CONVERSION$Action            := UOM_X_CONVERSION_cols('ACTION');
   UOM_X_CONVERSION$CONVERT_SQL       := UOM_X_CONVERSION_cols('CONVERT_SQL');
   UOM_X_CONVERSION$TO_UOM_CLASS      := UOM_X_CONVERSION_cols('TO_UOM_CLASS');
   UOM_X_CONVERSION$FRM_UOM_CLASS     := UOM_X_CONVERSION_cols('FROM_UOM_CLASS');
   UOM_CONVERSION_cols                :=s9t_pkg.get_col_names(I_file_id,UOM_CONVERSION_sheet);
   UOM_CONVERSION$Action              := UOM_CONVERSION_cols('ACTION');
   UOM_CONVERSION$OPERATOR            := UOM_CONVERSION_cols('OPERATOR');
   UOM_CONVERSION$FACTOR              := UOM_CONVERSION_cols('FACTOR');
   UOM_CONVERSION$TO_UOM              := UOM_CONVERSION_cols('TO_UOM');
   UOM_CONVERSION$FROM_UOM            := UOM_CONVERSION_cols('FROM_UOM');
   UOM_CLASS_TL_cols                 := s9t_pkg.get_col_names(I_file_id,UOM_CLASS_TL_sheet);
   UOM_CLASS_TL$Action               := UOM_CLASS_TL_cols('ACTION');
   UOM_CLASS_TL$UOM_DESC_TRANS       := UOM_CLASS_TL_cols('UOM_DESC_TRANS');
   UOM_CLASS_TL$UOM_TRANS            := UOM_CLASS_TL_cols('UOM_TRANS');
   UOM_CLASS_TL$UOM                  := UOM_CLASS_TL_cols('UOM');
   UOM_CLASS_TL$LANG                 := UOM_CLASS_TL_cols('LANG');
   UOM_CLASS_cols                    := s9t_pkg.get_col_names(I_file_id,UOM_CLASS_sheet);
   UOM_CLASS$Action                  := UOM_CLASS_cols('ACTION');
   UOM_CLASS$UOM_CLASS               := UOM_CLASS_cols('UOM_CLASS');
   UOM_CLASS$UOM                     := UOM_CLASS_cols('UOM');
   UOM_CLASS$UOM_DESC_TRANS          := UOM_CLASS_cols('UOM_DESC_TRANS');
   UOM_CLASS$UOM_TRANS               := UOM_CLASS_cols('UOM_TRANS');
END POPULATE_NAMES;
--------------------------------------------------------------
PROCEDURE POPULATE_UOM_X_CONVERSION( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = UOM_X_CONVERSION_sheet )
   select s9t_row(s9t_cells(CORESVC_UOM_CLASS.action_mod,from_uom_class,to_uom_class,convert_sql
                           ))
     from uom_x_conversion ;
END POPULATE_UOM_X_CONVERSION;
--------------------------------------------------------------
PROCEDURE POPULATE_UOM_CONVERSION( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = UOM_CONVERSION_sheet )
   select s9t_row(s9t_cells(CORESVC_UOM_CLASS.action_mod,from_uom,to_uom,operator,factor

                           ))
     from uom_conversion ;
END POPULATE_UOM_CONVERSION;
-------------------------------------------------------------
PROCEDURE POPULATE_UOM_CLASS_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = UOM_CLASS_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_UOM_CLASS.action_mod ,uom,
                           lang,
                           uom_trans,
                           uom_desc_trans

                           ))
     from uom_class_tl
    where lang <> LP_primary_lang;
END POPULATE_UOM_CLASS_TL;
----------------------------------------------------------------
PROCEDURE POPULATE_UOM_CLASS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = UOM_CLASS_sheet )
   select s9t_row(s9t_cells(NULL ,
                           u.uom,
                           u.uom_class,
                           u1.uom_trans,
                           u1.uom_desc_trans
                           ))
     from uom_class u, uom_class_tl u1
     where u.uom = u1.uom
       and u1.lang = LP_primary_lang;

END POPULATE_UOM_CLASS;
------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;

   BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(UOM_X_CONVERSION_sheet);
   L_file.sheets(l_file.get_sheet_index(UOM_X_CONVERSION_sheet)).column_headers := s9t_cells( 'ACTION','FROM_UOM_CLASS','TO_UOM_CLASS','CONVERT_SQL');

   L_file.add_sheet(UOM_CONVERSION_sheet);
   L_file.sheets(l_file.get_sheet_index(UOM_CONVERSION_sheet)).column_headers := s9t_cells( 'ACTION','FROM_UOM','TO_UOM','OPERATOR','FACTOR'
 );
   L_file.add_sheet(UOM_CLASS_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(UOM_CLASS_TL_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'UOM'
                                                                                            ,'LANG'
                                                                                            ,'UOM_TRANS'
                                                                                            ,'UOM_DESC_TRANS');

   L_file.add_sheet(UOM_CLASS_sheet);
   L_file.sheets(l_file.get_sheet_index(UOM_CLASS_sheet)).column_headers := s9t_cells( 'ACTION','UOM','UOM_CLASS','UOM_TRANS','UOM_DESC_TRANS');

   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_UOM_CLASS.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_UOM_X_CONVERSION(O_file_id);
      POPULATE_UOM_CONVERSION(O_file_id);
      POPULATE_UOM_CLASS_TL(O_file_id);
      POPULATE_UOM_CLASS(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
---------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UOM_X_CONVERSION( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_UOM_X_CONVERSION.process_id%TYPE) IS
   TYPE svc_UOM_X_CONVERSION_col_typ IS TABLE OF SVC_UOM_X_CONVERSION%ROWTYPE;
   L_temp_rec SVC_UOM_X_CONVERSION%ROWTYPE;
   svc_UOM_X_CONVERSION_col svc_UOM_X_CONVERSION_col_typ :=NEW svc_UOM_X_CONVERSION_col_typ();
   L_process_id SVC_UOM_X_CONVERSION.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec  SVC_UOM_X_CONVERSION%ROWTYPE;
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
   L_pk_columns   VARCHAR2(255)  := 'Unit of measure Conversion of Different Class';

   cursor C_MANDATORY_IND is
      select
             CONVERT_SQL_mi,
             TO_UOM_CLASS_mi,
             FROM_UOM_CLASS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'UOM_X_CONVERSION'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'CONVERT_SQL' AS CONVERT_SQL,
                                         'TO_UOM_CLASS' AS TO_UOM_CLASS,
                                         'FROM_UOM_CLASS' AS FROM_UOM_CLASS,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       CONVERT_SQL_dv,
                       TO_UOM_CLASS_dv,
                       FROM_UOM_CLASS_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'UOM_X_CONVERSION'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'CONVERT_SQL' AS CONVERT_SQL,
                                                      'TO_UOM_CLASS' AS TO_UOM_CLASS,
                                                      'FROM_UOM_CLASS' AS FROM_UOM_CLASS,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.CONVERT_SQL := rec.CONVERT_SQL_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_X_CONVERSION ' ,
                            NULL,
                           'CONVERT_SQL ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TO_UOM_CLASS := rec.TO_UOM_CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_X_CONVERSION ' ,
                            NULL,
                           'TO_UOM_CLASS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FROM_UOM_CLASS := rec.FROM_UOM_CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_X_CONVERSION ' ,
                            NULL,
                           'FROM_UOM_CLASS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(UOM_X_CONVERSION$Action)      AS Action,
          r.get_cell(UOM_X_CONVERSION$CONVERT_SQL)              AS CONVERT_SQL,
          r.get_cell(UOM_X_CONVERSION$TO_UOM_CLASS)              AS TO_UOM_CLASS,
          r.get_cell(UOM_X_CONVERSION$FRM_UOM_CLASS)              AS FROM_UOM_CLASS,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(UOM_X_CONVERSION_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_X_CONVERSION_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CONVERT_SQL := rec.CONVERT_SQL;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_X_CONVERSION_sheet,
                            rec.row_seq,
                            'CONVERT_SQL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TO_UOM_CLASS := rec.TO_UOM_CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_X_CONVERSION_sheet,
                            rec.row_seq,
                            'TO_UOM_CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FROM_UOM_CLASS := rec.FROM_UOM_CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_X_CONVERSION_sheet,
                            rec.row_seq,
                            'FROM_UOM_CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_UOM_CLASS.action_new then
         L_temp_rec.CONVERT_SQL := NVL( L_temp_rec.CONVERT_SQL,L_default_rec.CONVERT_SQL);
         L_temp_rec.TO_UOM_CLASS := NVL( L_temp_rec.TO_UOM_CLASS,L_default_rec.TO_UOM_CLASS);
         L_temp_rec.FROM_UOM_CLASS := NVL( L_temp_rec.FROM_UOM_CLASS,L_default_rec.FROM_UOM_CLASS);
      end if;
      if not (
            L_temp_rec.TO_UOM_CLASS is NOT NULL and
            L_temp_rec.FROM_UOM_CLASS is NOT NULL and
            1 = 1
            )then
          WRITE_S9T_ERROR(I_file_id,
						  UOM_X_CONVERSION_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_UOM_X_CONVERSION_col.extend();
         svc_UOM_X_CONVERSION_col(svc_UOM_X_CONVERSION_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_UOM_X_CONVERSION_col.COUNT SAVE EXCEPTIONS
      merge into SVC_UOM_X_CONVERSION st
      using(select
                  (case
                   when l_mi_rec.CONVERT_SQL_mi    = 'N'
                    and svc_UOM_X_CONVERSION_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.CONVERT_SQL IS NULL
                   then mt.CONVERT_SQL
                   else s1.CONVERT_SQL
                   end) AS CONVERT_SQL,
                  (case
                   when l_mi_rec.TO_UOM_CLASS_mi    = 'N'
                    and svc_UOM_X_CONVERSION_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.TO_UOM_CLASS IS NULL
                   then mt.TO_UOM_CLASS
                   else s1.TO_UOM_CLASS
                   end) AS TO_UOM_CLASS,
                  (case
                   when l_mi_rec.FROM_UOM_CLASS_mi    = 'N'
                    and svc_UOM_X_CONVERSION_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.FROM_UOM_CLASS IS NULL
                   then mt.FROM_UOM_CLASS
                   else s1.FROM_UOM_CLASS
                   end) AS FROM_UOM_CLASS,
                  null as dummy
              from (select
                          svc_UOM_X_CONVERSION_col(i).CONVERT_SQL AS CONVERT_SQL,
                          svc_UOM_X_CONVERSION_col(i).TO_UOM_CLASS AS TO_UOM_CLASS,
                          svc_UOM_X_CONVERSION_col(i).FROM_UOM_CLASS AS FROM_UOM_CLASS,
                          null as dummy
                      from dual ) s1,
            UOM_X_CONVERSION mt
             where
                  mt.TO_UOM_CLASS (+)     = s1.TO_UOM_CLASS   and
                  mt.FROM_UOM_CLASS (+)     = s1.FROM_UOM_CLASS   and
                  1 = 1 )sq
                on (
                    st.TO_UOM_CLASS      = sq.TO_UOM_CLASS and
                    st.FROM_UOM_CLASS      = sq.FROM_UOM_CLASS and
                    svc_UOM_X_CONVERSION_col(i).ACTION IN (CORESVC_UOM_CLASS.action_mod,CORESVC_UOM_CLASS.action_del))
      when matched then
      update
         set process_id      = svc_UOM_X_CONVERSION_col(i).process_id ,
             chunk_id        = svc_UOM_X_CONVERSION_col(i).chunk_id ,
             row_seq         = svc_UOM_X_CONVERSION_col(i).row_seq ,
             action          = svc_UOM_X_CONVERSION_col(i).action ,
             process$status  = svc_UOM_X_CONVERSION_col(i).process$status ,
             convert_sql              = sq.convert_sql ,
             create_id       = svc_UOM_X_CONVERSION_col(i).create_id ,
             create_datetime = svc_UOM_X_CONVERSION_col(i).create_datetime ,
             last_upd_id     = svc_UOM_X_CONVERSION_col(i).last_upd_id ,
             last_upd_datetime = svc_UOM_X_CONVERSION_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             convert_sql ,
             to_uom_class ,
             from_uom_class ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_UOM_X_CONVERSION_col(i).process_id ,
             svc_UOM_X_CONVERSION_col(i).chunk_id ,
             svc_UOM_X_CONVERSION_col(i).row_seq ,
             svc_UOM_X_CONVERSION_col(i).action ,
             svc_UOM_X_CONVERSION_col(i).process$status ,
             sq.convert_sql ,
             sq.to_uom_class ,
             sq.from_uom_class ,
             svc_UOM_X_CONVERSION_col(i).create_id ,
             svc_UOM_X_CONVERSION_col(i).create_datetime ,
             svc_UOM_X_CONVERSION_col(i).last_upd_id ,
             svc_UOM_X_CONVERSION_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            UOM_X_CONVERSION_sheet,
                            svc_UOM_X_CONVERSION_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_UOM_X_CONVERSION;
-------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UOM_CONVERSION( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_UOM_CONVERSION.process_id%TYPE) IS
   TYPE svc_UOM_CONVERSION_col_typ IS TABLE OF SVC_UOM_CONVERSION%ROWTYPE;
   L_temp_rec SVC_UOM_CONVERSION%ROWTYPE;
   svc_UOM_CONVERSION_col svc_UOM_CONVERSION_col_typ :=NEW svc_UOM_CONVERSION_col_typ();
   L_process_id SVC_UOM_CONVERSION.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_UOM_CONVERSION%ROWTYPE;
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
   L_pk_columns   VARCHAR2(255)  := 'Unit of measure Conversion';

   cursor C_MANDATORY_IND is
      select
             OPERATOR_mi,
             FACTOR_mi,
             TO_UOM_mi,
             FROM_UOM_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'UOM_CONVERSION'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'OPERATOR' AS OPERATOR,
                                         'FACTOR' AS FACTOR,
                                         'TO_UOM' AS TO_UOM,
                                         'FROM_UOM' AS FROM_UOM,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       OPERATOR_dv,
                       FACTOR_dv,
                       TO_UOM_dv,
                       FROM_UOM_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'UOM_CONVERSION'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'OPERATOR' AS OPERATOR,
                                                      'FACTOR' AS FACTOR,
                                                      'TO_UOM' AS TO_UOM,
                                                      'FROM_UOM' AS FROM_UOM,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.OPERATOR := rec.OPERATOR_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CONVERSION ' ,
                            NULL,
                           'OPERATOR ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FACTOR := rec.FACTOR_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CONVERSION ' ,
                            NULL,
                           'FACTOR ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TO_UOM := rec.TO_UOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CONVERSION ' ,
                            NULL,
                           'TO_UOM ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FROM_UOM := rec.FROM_UOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CONVERSION ' ,
                            NULL,
                           'FROM_UOM ' ,
                            NULL,
                            'INV_DEFAULT');
     END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(UOM_CONVERSION$Action)      AS Action,
          r.get_cell(UOM_CONVERSION$OPERATOR)              AS OPERATOR,
          r.get_cell(UOM_CONVERSION$FACTOR)              AS FACTOR,
          r.get_cell(UOM_CONVERSION$TO_UOM)              AS TO_UOM,
          r.get_cell(UOM_CONVERSION$FROM_UOM)              AS FROM_UOM,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(UOM_CONVERSION_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CONVERSION_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.OPERATOR := rec.OPERATOR;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CONVERSION_sheet,
                            rec.row_seq,
                            'OPERATOR',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FACTOR := rec.FACTOR;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CONVERSION_sheet,
                            rec.row_seq,
                            'FACTOR',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TO_UOM := rec.TO_UOM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CONVERSION_sheet,
                            rec.row_seq,
                            'TO_UOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FROM_UOM := rec.FROM_UOM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CONVERSION_sheet,
                            rec.row_seq,
                            'FROM_UOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_UOM_CLASS.action_new then
         L_temp_rec.OPERATOR := NVL( L_temp_rec.OPERATOR,L_default_rec.OPERATOR);
         L_temp_rec.FACTOR := NVL( L_temp_rec.FACTOR,L_default_rec.FACTOR);
         L_temp_rec.TO_UOM := NVL( L_temp_rec.TO_UOM,L_default_rec.TO_UOM);
         L_temp_rec.FROM_UOM := NVL( L_temp_rec.FROM_UOM,L_default_rec.FROM_UOM);
      end if;
      if not (
            L_temp_rec.TO_UOM is NOT NULL and
            L_temp_rec.FROM_UOM is NOT NULL and
            1 = 1
            )then
          WRITE_S9T_ERROR(I_file_id,
						             UOM_CONVERSION_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_UOM_CONVERSION_col.extend();
         svc_UOM_CONVERSION_col(svc_UOM_CONVERSION_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_UOM_CONVERSION_col.COUNT SAVE EXCEPTIONS
      merge into SVC_UOM_CONVERSION st
      using(select
                  (case
                   when l_mi_rec.OPERATOR_mi    = 'N'
                    and svc_UOM_CONVERSION_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.OPERATOR IS NULL
                   then mt.OPERATOR
                   else s1.OPERATOR
                   end) AS OPERATOR,
                  (case
                   when l_mi_rec.FACTOR_mi    = 'N'
                    and svc_UOM_CONVERSION_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.FACTOR IS NULL
                   then mt.FACTOR
                   else s1.FACTOR
                   end) AS FACTOR,
                  (case
                   when l_mi_rec.TO_UOM_mi    = 'N'
                    and svc_UOM_CONVERSION_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.TO_UOM IS NULL
                   then mt.TO_UOM
                   else s1.TO_UOM
                   end) AS TO_UOM,
                  (case
                   when l_mi_rec.FROM_UOM_mi    = 'N'
                    and svc_UOM_CONVERSION_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.FROM_UOM IS NULL
                   then mt.FROM_UOM
                   else s1.FROM_UOM
                   end) AS FROM_UOM,
                  null as dummy
              from (select
                          svc_UOM_CONVERSION_col(i).OPERATOR AS OPERATOR,
                          svc_UOM_CONVERSION_col(i).FACTOR AS FACTOR,
                          svc_UOM_CONVERSION_col(i).TO_UOM AS TO_UOM,
                          svc_UOM_CONVERSION_col(i).FROM_UOM AS FROM_UOM,
                          null as dummy
                      from dual ) s1,
            UOM_CONVERSION mt
             where
                  mt.TO_UOM (+)     = s1.TO_UOM   and
                  mt.FROM_UOM (+)     = s1.FROM_UOM   and
                  1 = 1 )sq
                on (
                    st.TO_UOM      = sq.TO_UOM and
                    st.FROM_UOM      = sq.FROM_UOM and
                    svc_UOM_CONVERSION_col(i).ACTION IN (CORESVC_UOM_CLASS.action_mod,CORESVC_UOM_CLASS.action_del))
      when matched then
      update
         set process_id      = svc_UOM_CONVERSION_col(i).process_id ,
             chunk_id        = svc_UOM_CONVERSION_col(i).chunk_id ,
             row_seq         = svc_UOM_CONVERSION_col(i).row_seq ,
             action          = svc_UOM_CONVERSION_col(i).action ,
             process$status  = svc_UOM_CONVERSION_col(i).process$status ,
             factor              = sq.factor ,
             operator              = sq.operator ,
             create_id       = svc_UOM_CONVERSION_col(i).create_id ,
             create_datetime = svc_UOM_CONVERSION_col(i).create_datetime ,
             last_upd_id     = svc_UOM_CONVERSION_col(i).last_upd_id ,
             last_upd_datetime = svc_UOM_CONVERSION_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             operator ,
             factor ,
             to_uom ,
             from_uom ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_UOM_CONVERSION_col(i).process_id ,
             svc_UOM_CONVERSION_col(i).chunk_id ,
             svc_UOM_CONVERSION_col(i).row_seq ,
             svc_UOM_CONVERSION_col(i).action ,
             svc_UOM_CONVERSION_col(i).process$status ,
             sq.operator ,
             sq.factor ,
             sq.to_uom ,
             sq.from_uom ,
             svc_UOM_CONVERSION_col(i).create_id ,
             svc_UOM_CONVERSION_col(i).create_datetime ,
             svc_UOM_CONVERSION_col(i).last_upd_id ,
             svc_UOM_CONVERSION_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            UOM_CONVERSION_sheet,
                            svc_UOM_CONVERSION_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_UOM_CONVERSION;

------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UOM_CLASS_TL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_UOM_CLASS_TL.process_id%TYPE) IS
   TYPE svc_UOM_CLASS_TL_col_typ IS TABLE OF SVC_UOM_CLASS_TL%ROWTYPE;
   L_temp_rec SVC_UOM_CLASS_TL%ROWTYPE;
   svc_UOM_CLASS_TL_col svc_UOM_CLASS_TL_col_typ :=NEW svc_UOM_CLASS_TL_col_typ();
   L_process_id SVC_UOM_CLASS_TL.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_UOM_CLASS_TL%ROWTYPE;
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             UOM_DESC_TRANS_mi,
             UOM_TRANS_mi,
             UOM_mi,
             LANG_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'UOM_CLASS_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'UOM_DESC_TRANS' AS UOM_DESC_TRANS,
                                         'UOM_TRANS' AS UOM_TRANS,
                                         'UOM' AS UOM,
                                         'LANG' AS LANG,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns   VARCHAR2(255)  := 'Unit of measure Class, Language';

BEGIN
  -- Get default values.
   FOR rec IN (select
                       UOM_DESC_TRANS_dv,
                       UOM_TRANS_dv,
                       UOM_dv,
                       LANG_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'UOM_CLASS_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'UOM_DESC_TRANS' AS UOM_DESC_TRANS,
                                                      'UOM_TRANS' AS UOM_TRANS,
                                                      'UOM' AS UOM,
                                                      'LANG' AS LANG,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.UOM_DESC_TRANS := rec.UOM_DESC_TRANS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CLASS_TL ' ,
                            NULL,
                           'UOM_DESC_TRANS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UOM_TRANS := rec.UOM_TRANS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CLASS_TL ' ,
                            NULL,
                           'UOM_TRANS ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UOM := rec.UOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CLASS_TL ' ,
                            NULL,
                           'UOM ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CLASS_TL ' ,
                            NULL,
                           'LANG ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(UOM_CLASS_TL$Action)                      AS Action,
          r.get_cell(UOM_CLASS_TL$UOM_DESC_TRANS)              AS UOM_DESC_TRANS,
          r.get_cell(UOM_CLASS_TL$UOM_TRANS)                   AS UOM_TRANS,
          r.get_cell(UOM_CLASS_TL$UOM)                         AS UOM,
          r.get_cell(UOM_CLASS_TL$LANG)                        AS LANG,
          r.get_row_seq()                                      AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(UOM_CLASS_TL_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UOM_DESC_TRANS := rec.UOM_DESC_TRANS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_TL_sheet,
                            rec.row_seq,
                            'UOM_DESC_TRANS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UOM_TRANS := rec.UOM_TRANS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_TL_sheet,
                            rec.row_seq,
                            'UOM_TRANS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UOM := rec.UOM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_TL_sheet,
                            rec.row_seq,
                            'UOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_UOM_CLASS.action_new then
         L_temp_rec.UOM_DESC_TRANS := NVL( L_temp_rec.UOM_DESC_TRANS,L_default_rec.UOM_DESC_TRANS);
         L_temp_rec.UOM_TRANS := NVL( L_temp_rec.UOM_TRANS,L_default_rec.UOM_TRANS);
         L_temp_rec.UOM := NVL( L_temp_rec.UOM,L_default_rec.UOM);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,L_default_rec.LANG);
      end if;
      if not (
            L_temp_rec.UOM is NOT NULL and
            L_temp_rec.LANG is NOT NULL and
            1 = 1
            )then
          WRITE_S9T_ERROR(I_file_id,
						 UOM_CLASS_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;

      end if;
      if NOT L_error then
         svc_UOM_CLASS_TL_col.extend();
         svc_UOM_CLASS_TL_col(svc_UOM_CLASS_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_UOM_CLASS_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_UOM_CLASS_TL st
      using(select
                  (case
                   when l_mi_rec.UOM_DESC_TRANS_mi    = 'N'
                    and svc_UOM_CLASS_TL_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.UOM_DESC_TRANS IS NULL
                   then mt.UOM_DESC_TRANS
                   else s1.UOM_DESC_TRANS
                   end) AS UOM_DESC_TRANS,
                  (case
                   when l_mi_rec.UOM_TRANS_mi    = 'N'
                    and svc_UOM_CLASS_TL_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.UOM_TRANS IS NULL
                   then mt.UOM_TRANS
                   else s1.UOM_TRANS
                   end) AS UOM_TRANS,
                  (case
                   when l_mi_rec.UOM_mi    = 'N'
                    and svc_UOM_CLASS_TL_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.UOM IS NULL
                   then mt.UOM
                   else s1.UOM
                   end) AS UOM,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_UOM_CLASS_TL_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  null as dummy
              from (select
                          svc_UOM_CLASS_TL_col(i).UOM_DESC_TRANS AS UOM_DESC_TRANS,
                          svc_UOM_CLASS_TL_col(i).UOM_TRANS AS UOM_TRANS,
                          svc_UOM_CLASS_TL_col(i).UOM AS UOM,
                          svc_UOM_CLASS_TL_col(i).LANG AS LANG,
                          null as dummy
                      from dual ) s1,
            UOM_CLASS_TL mt
             where
                  mt.UOM (+)     = s1.UOM   and
                  mt.LANG (+)     = s1.LANG   and
                  1 = 1 )sq
                on (
                    st.UOM      = sq.UOM and
                    st.LANG      = sq.LANG and
                    svc_UOM_CLASS_TL_col(i).ACTION IN (CORESVC_UOM_CLASS.action_mod,CORESVC_UOM_CLASS.action_del))
      when matched then
      update
         set process_id      = svc_UOM_CLASS_TL_col(i).process_id ,
             chunk_id        = svc_UOM_CLASS_TL_col(i).chunk_id ,
             row_seq         = svc_UOM_CLASS_TL_col(i).row_seq ,
             action          = svc_UOM_CLASS_TL_col(i).action ,
             process$status  = svc_UOM_CLASS_TL_col(i).process$status ,
             uom_trans              = sq.uom_trans ,
             uom_desc_trans              = sq.uom_desc_trans ,
             create_id       = svc_UOM_CLASS_TL_col(i).create_id ,
             create_datetime = svc_UOM_CLASS_TL_col(i).create_datetime ,
             last_upd_id     = svc_UOM_CLASS_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_UOM_CLASS_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             uom_desc_trans ,
             uom_trans ,
             uom ,
             lang ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_UOM_CLASS_TL_col(i).process_id ,
             svc_UOM_CLASS_TL_col(i).chunk_id ,
             svc_UOM_CLASS_TL_col(i).row_seq ,
             svc_UOM_CLASS_TL_col(i).action ,
             svc_UOM_CLASS_TL_col(i).process$status ,
             sq.uom_desc_trans ,
             sq.uom_trans ,
             sq.uom ,
             sq.lang ,
             svc_UOM_CLASS_TL_col(i).create_id ,
             svc_UOM_CLASS_TL_col(i).create_datetime ,
             svc_UOM_CLASS_TL_col(i).last_upd_id ,
             svc_UOM_CLASS_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            UOM_CLASS_TL_sheet,
                            svc_UOM_CLASS_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_UOM_CLASS_TL;
--------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UOM_CLASS( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_UOM_CLASS.process_id%TYPE) IS
   TYPE svc_UOM_CLASS_col_typ IS TABLE OF SVC_UOM_CLASS%ROWTYPE;
   L_temp_rec SVC_UOM_CLASS%ROWTYPE;
   svc_UOM_CLASS_col svc_UOM_CLASS_col_typ :=NEW svc_UOM_CLASS_col_typ();
   L_process_id SVC_UOM_CLASS.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_UOM_CLASS%ROWTYPE;
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
           UOM_TRANS_mi,
	         UOM_desc_trans_mi,
             UOM_CLASS_mi,
             UOM_mi,

             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'UOM_CLASS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                               'UOM_TRANS' as UOM_TRANS,
					                     'UOM_DESC_TRANS' as UOM_desc_trans,
                                         'UOM_CLASS' AS UOM_CLASS,
                                         'UOM' AS UOM,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns   VARCHAR2(255)  := 'Unit of measure Class';

BEGIN
  -- Get default values.
   FOR rec IN (select
  UOM_DESC_TRANS_dv,UOM_TRANS_dv,
                       UOM_CLASS_dv,
                       UOM_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'UOM_CLASS'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                      'UOM_TRANS' as UOM_TRANS,
								                      'UOM_DESC_TRANS' as UOM_DESC_TRANS,
                                                      'UOM_CLASS' AS UOM_CLASS,
                                                      'UOM' AS UOM,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.UOM_CLASS := rec.UOM_CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CLASS ' ,
                            NULL,
                           'UOM_CLASS ' ,
                                                      NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UOM_TRANS := rec.UOM_TRANS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CLASS ' ,
                            NULL,
                           'UOM_TRANS' ,
                                                      NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.UOM_DESC_TRANS := rec.UOM_DESC_TRANS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CLASS ' ,
                            NULL,
                           'UOM_DESC_TRANS' ,
                                                      NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.UOM := rec.UOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UOM_CLASS ' ,
                            NULL,
                           'UOM ' ,
                           NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(UOM_CLASS$Action)      AS Action,
          r.get_cell(UOM_CLASS$UOM_CLASS)              AS UOM_CLASS,
          r.get_cell(UOM_CLASS$UOM)              AS UOM,
		  r.get_cell(UOM_CLASS$UOM_DESC_TRANS)              AS UOM_DESC_TRANS,
		  r.get_cell(UOM_CLASS$UOM_TRANS)              AS UOM_TRANS,

          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(UOM_CLASS_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UOM_CLASS := rec.UOM_CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_sheet,
                            rec.row_seq,
                            'UOM_CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UOM_DESC_TRANS := rec.UOM_DESC_TRANS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_sheet,
                            rec.row_seq,
                            'UOM_DESC_TRANS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UOM_TRANS := rec.UOM_TRANS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_sheet,
                            rec.row_seq,
                            'UOM_TRANS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.UOM := rec.UOM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
UOM_CLASS_sheet,
                            rec.row_seq,
                            'UOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_UOM_CLASS.action_new then
         L_temp_rec.UOM_CLASS := NVL( L_temp_rec.UOM_CLASS,L_default_rec.UOM_CLASS);
         L_temp_rec.UOM := NVL( L_temp_rec.UOM,L_default_rec.UOM);
    		 L_temp_rec.UOM_DESC_TRANS := NVL( L_temp_rec.UOM_DESC_TRANS,L_default_rec.UOM_DESC_TRANS);
         L_temp_rec.UOM_TRANS := NVL( L_temp_rec.UOM_TRANS,L_default_rec.UOM_TRANS);

      end if;
      if not (
            L_temp_rec.UOM is NOT NULL and
            1 = 1
            )then
          WRITE_S9T_ERROR(I_file_id,
						             UOM_CLASS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;

      end if;
      if NOT L_error then
         svc_UOM_CLASS_col.extend();
         svc_UOM_CLASS_col(svc_UOM_CLASS_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_UOM_CLASS_col.COUNT SAVE EXCEPTIONS
      merge into SVC_UOM_CLASS st
      using(select
                  (case
                   when l_mi_rec.UOM_TRANS_mi    = 'N'
                    and svc_UOM_CLASS_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.UOM_TRANS IS NULL
                   then mt.UOM_TRANS
                   else s1.UOM_TRANS
                   end) AS UOM_TRANS,
                  (case
                   when l_mi_rec.UOM_DESC_TRANS_mi    = 'N'
                    and svc_UOM_CLASS_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.UOM_DESC_TRANS IS NULL
                   then mt.UOM_DESC_TRANS
                   else s1.UOM_DESC_TRANS
                   end) AS UOM_DESC_TRANS,

            	  (case
                   when l_mi_rec.UOM_CLASS_mi    = 'N'
                    and svc_UOM_CLASS_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.UOM_CLASS IS NULL
                   then mt.UOM_CLASS
                   else s1.UOM_CLASS
                   end) AS UOM_CLASS,
                  (case
                   when l_mi_rec.UOM_mi    = 'N'
                    and svc_UOM_CLASS_col(i).action = CORESVC_UOM_CLASS.action_mod
                    and s1.UOM IS NULL
                   then mt.UOM
                   else s1.UOM
                   end) AS UOM,
                  null as dummy
              from (select
                          svc_UOM_CLASS_col(i).UOM_CLASS AS UOM_CLASS,
                          svc_UOM_CLASS_col(i).UOM AS UOM,
                          svc_UOM_CLASS_col(i).UOM_DESC_TRANS AS UOM_DESC_TRANS,
                          svc_UOM_CLASS_col(i).UOM_TRANS AS UOM_TRANS,
                          null as dummy
                      from dual ) s1,
            (select ut.*, utl.uom_desc_trans, utl.uom_trans
                         from UOM_CLASS ut, UOM_CLASS_TL utl
                        where ut.uom = utl.uom
                          and utl.lang = LP_primary_lang)  mt
             where
                  mt.UOM (+)     = s1.UOM   and
                  1 = 1 )sq
                on (
                    st.UOM      = sq.UOM and
                    svc_UOM_CLASS_col(i).ACTION IN (CORESVC_UOM_CLASS.action_mod,CORESVC_UOM_CLASS.action_del))
      when matched then
      update
         set process_id      = svc_UOM_CLASS_col(i).process_id ,
             chunk_id        = svc_UOM_CLASS_col(i).chunk_id ,
             row_seq         = svc_UOM_CLASS_col(i).row_seq ,
             action          = svc_UOM_CLASS_col(i).action ,
             process$status  = svc_UOM_CLASS_col(i).process$status ,
             uom_class       = sq.uom_class ,
             uom_desc_trans  = sq.uom_desc_trans ,
             uom_trans  = sq.uom_trans ,
             create_id       = svc_UOM_CLASS_col(i).create_id ,
             create_datetime = svc_UOM_CLASS_col(i).create_datetime ,
             last_upd_id     = svc_UOM_CLASS_col(i).last_upd_id ,
             last_upd_datetime = svc_UOM_CLASS_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             uom_class ,
			       uom_desc_trans,
             uom_trans,
             uom ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_UOM_CLASS_col(i).process_id ,
             svc_UOM_CLASS_col(i).chunk_id ,
             svc_UOM_CLASS_col(i).row_seq ,
             svc_UOM_CLASS_col(i).action ,
             svc_UOM_CLASS_col(i).process$status ,
             sq.uom_class ,
			       sq.uom_desc_trans,
             sq.uom_trans,
             sq.uom ,
             svc_UOM_CLASS_col(i).create_id ,
             svc_UOM_CLASS_col(i).create_datetime ,
             svc_UOM_CLASS_col(i).last_upd_id ,
             svc_UOM_CLASS_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            UOM_CLASS_sheet,
                            svc_UOM_CLASS_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_UOM_CLASS;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_UOM_CLASS.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_UOM_X_CONVERSION(I_file_id,I_process_id);
      PROCESS_S9T_UOM_CONVERSION(I_file_id,I_process_id);
      PROCESS_S9T_UOM_CLASS_TL(I_file_id,I_process_id);
      PROCESS_S9T_UOM_CLASS(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
 END PROCESS_S9T;
--------------------------------------------------------------------------------------

FUNCTION EXEC_UOM_X_CONVERSION_INS(  L_uom_x_conversion_temp_rec   IN   UOM_X_CONVERSION%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_X_CONVERSION_INS';
   L_table   VARCHAR2(255):= 'SVC_UOM_X_CONVERSION';
BEGIN
   insert
     into uom_x_conversion
   values L_uom_x_conversion_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_X_CONVERSION_INS;
------------------------------------------------------------------
FUNCTION EXEC_UOM_X_CONVERSION_UPD( L_uom_x_conversion_temp_rec   IN   UOM_X_CONVERSION%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_X_CONVERSION_UPD';
   L_table   VARCHAR2(255):= 'SVC_UOM_X_CONVERSION';
BEGIN
   update uom_x_conversion
      set row = L_uom_x_conversion_temp_rec
    where 1 = 1
      and to_uom_class = L_uom_x_conversion_temp_rec.to_uom_class
      and from_uom_class = L_uom_x_conversion_temp_rec.from_uom_class
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_X_CONVERSION_UPD;
---------------------------------------------------------------------
FUNCTION EXEC_UOM_X_CONVERSION_DEL(  L_uom_x_conversion_temp_rec   IN   UOM_X_CONVERSION%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_X_CONVERSION_DEL';
   L_table   VARCHAR2(255):= 'SVC_UOM_X_CONVERSION';
BEGIN
   delete
     from uom_x_conversion
    where 1 = 1
      and to_uom_class = L_uom_x_conversion_temp_rec.to_uom_class
      and from_uom_class = L_uom_x_conversion_temp_rec.from_uom_class
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_X_CONVERSION_DEL;
--------------------------------------------------------------------------
FUNCTION PROCESS_UOM_X_CONVERSION( I_process_id   IN   SVC_UOM_X_CONVERSION.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_UOM_X_CONVERSION.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_UOM_CLASS.PROCESS_UOM_X_CONVERSION';
   L_error_message VARCHAR2(600);
   L_UOM_X_CONVERSION_temp_rec UOM_X_CONVERSION%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_UOM_X_CONVERSION';
   L_exists varchar2(1):= null;
   cursor c_to_uom_class_exist(I_to_uom_class UOM_X_CONVERSION.TO_UOM_CLASS%TYPE) is
      select 'x'
        from  uom_class where uom_class=I_to_uom_class;

   cursor c_from_uom_class_exist(I_from_uom_class UOM_X_CONVERSION.FROM_UOM_CLASS%TYPE) is
      select 'x'
        from  uom_class where uom_class=I_from_uom_class;       
   
BEGIN
   FOR rec IN c_svc_UOM_X_CONVERSION(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action = action_new
         and rec.PK_UOM_X_CONVERSION_rid is NOT NULL then
          L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'UOM_X_CONVERSION',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FROM_UOM_CLASS,TO_UOM_CLASS',
                     L_error_message);
         L_error :=TRUE;
     end if;

	 if rec.action IN (action_mod,action_del)
         and rec.PK_UOM_X_CONVERSION_rid is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'UOM_X_CONVERSION',
                                                NULL,
                                                NULL);


         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FROM_UOM_CLASS,TO_UOM_CLASS',
                     L_error_message);
         L_error :=TRUE;
         L_error :=TRUE;
      end if;

	  if rec.action in (action_new,action_mod)
         and NOT(  rec.CONVERT_SQL  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CONVERT_SQL',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      L_exists:=null;
      open c_to_uom_class_exist(rec.to_uom_class);
      fetch c_to_uom_class_exist into L_exists;
      close c_to_uom_class_exist;

	  if rec.action in (action_new)
         and L_exists <> 'x' then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'UOM_CLASS',
                                                NULL,
                                                NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TO_UOM_CLASS',
                     L_error_message);
         L_error :=TRUE;
      end if;

      L_exists:=null;
      open c_from_uom_class_exist(rec.from_uom_class);
      fetch c_from_uom_class_exist into L_exists;
      close c_from_uom_class_exist;

	  if rec.action in (action_new)
         and L_exists <> 'x' then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'UOM_CLASS',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FROM_UOM_CLASS',
                     L_error_message);
         L_error :=TRUE;
      end if;

     if rec.action in (action_new)
         and rec.to_uom_class is not null
         and rec.from_uom_class is not null
         and nvl(rec.to_uom_class,'-1') = nvl(rec.from_uom_class,'-1') then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TO_UOM_CLASS,FROM_UOM_CLASS',
                     'UOM_CLASS_X_DIFF');
         L_error :=TRUE;

      end if;

      if NOT L_error then
         L_uom_x_conversion_temp_rec.from_uom_class              := rec.from_uom_class;
         L_uom_x_conversion_temp_rec.to_uom_class              := rec.to_uom_class;
         L_uom_x_conversion_temp_rec.convert_sql              := rec.convert_sql;
         if rec.action = action_new then
            if EXEC_UOM_X_CONVERSION_INS(   L_uom_x_conversion_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_UOM_X_CONVERSION_UPD( L_uom_x_conversion_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_UOM_X_CONVERSION_DEL( L_uom_x_conversion_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_uom_x_conversion st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_uom_x_conversion st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_uom_x_conversion st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_UOM_X_CONVERSION%ISOPEN then
	     close c_svc_UOM_X_CONVERSION;
	  end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UOM_X_CONVERSION;
---------------------------------------------------------------------------------------
FUNCTION EXEC_UOM_CONVERSION_INS(  L_uom_conversion_temp_rec   IN   UOM_CONVERSION%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CONVERSION_INS';
   L_table   VARCHAR2(255):= 'SVC_UOM_CONVERSION';
BEGIN
   insert
     into uom_conversion
   values L_uom_conversion_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CONVERSION_INS;
-----------------------------------------------------------------------------------------
FUNCTION EXEC_UOM_CONVERSION_UPD( L_uom_conversion_temp_rec   IN   UOM_CONVERSION%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CONVERSION_UPD';
   L_table   VARCHAR2(255):= 'SVC_UOM_CONVERSION';
BEGIN
   update uom_conversion
      set row = L_uom_conversion_temp_rec
    where 1 = 1
      and to_uom = L_uom_conversion_temp_rec.to_uom
      and from_uom = L_uom_conversion_temp_rec.from_uom
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CONVERSION_UPD;
-------------------------------------------------------------------------------------------
FUNCTION EXEC_UOM_CONVERSION_DEL(  L_uom_conversion_temp_rec   IN   UOM_CONVERSION%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CONVERSION_DEL';
   L_table   VARCHAR2(255):= 'SVC_UOM_CONVERSION';
BEGIN
   delete
     from uom_conversion
    where 1 = 1
      and to_uom = L_uom_conversion_temp_rec.to_uom
      and from_uom = L_uom_conversion_temp_rec.from_uom
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CONVERSION_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_UOM_CONVERSION( I_process_id   IN   SVC_UOM_CONVERSION.PROCESS_ID%TYPE,
                                 I_chunk_id     IN SVC_UOM_CONVERSION.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                   BOOLEAN;
   L_process_error           BOOLEAN      := FALSE;
   L_program                 VARCHAR2(255):='CORESVC_UOM_CLASS.PROCESS_UOM_CONVERSION';
   L_error_message           VARCHAR2(600);
   L_UOM_CONVERSION_temp_rec UOM_CONVERSION%ROWTYPE;
   L_table                   VARCHAR2(255):='SVC_UOM_CONVERSION';
BEGIN
   FOR rec IN c_svc_UOM_CONVERSION(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

	  if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.PK_UOM_CONVERSION_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'UOM_CONVERSION',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FROM_UOM,TO_UOM',
                    L_error_message);
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_UOM_CONVERSION_rid is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'UOM_CONVERSION',
                                                NULL,
                                                NULL);


         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FROM_UOM,TO_UOM',
                     L_error_message);
         L_error :=TRUE;
      end if;

	  if rec.action in (action_new)
	     and rec.uoc_umc_fk1_rid is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'UOM_CLASS',
                                                NULL,
                                                NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TO_UOM',
                     L_error_message);
         L_error :=TRUE;
      end if;

	  if rec.action in (action_new)
	     and rec.uoc_umc_fk2_rid is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'UOM_CLASS',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FROM_UOM',
                     L_error_message);
         L_error :=TRUE;
      end if;

      if rec.action in (action_new,action_mod)
	   and NOT(  rec.FACTOR  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FACTOR',
                     'ENTER_REC');
         L_error :=TRUE;
      end if;

      if rec.action in (action_new)
         and rec.to_uom is not null
         and rec.from_uom is not null
         and rec.umc_to_class is not null
         and rec.umc_from_class is not null
         and rec.umc_to_class <> rec.umc_from_class then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TO_UOM,FROM_UOM',
                     'UOM_CLASS_DIFF');
         L_error :=TRUE;

      end if;

	  if rec.action in (action_new,action_mod)
	   and NOT(  rec.OPERATOR  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OPERATOR',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;

	  if rec.action in (action_new,action_mod)
	   and rec.OPERATOR is not null
	   and NOT(  rec.OPERATOR IN  ( 'M','D' )  ) then
         L_error_message := SQL_LIB.CREATE_MSG('VALID_VALUE',
                                               'Operator',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OPERATOR',
                     L_error_message);
         L_error :=TRUE;
      end if;


      if NOT L_error then
         L_uom_conversion_temp_rec.operator            := rec.operator;
         L_uom_conversion_temp_rec.factor              := rec.factor;
         L_uom_conversion_temp_rec.to_uom              := rec.to_uom;
         L_uom_conversion_temp_rec.from_uom            := rec.from_uom;

		 if rec.action = action_new then
            if EXEC_UOM_CONVERSION_INS(   L_uom_conversion_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

		 if rec.action = action_mod then
            if EXEC_UOM_CONVERSION_UPD( L_uom_conversion_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

		 if rec.action = action_del then
            if EXEC_UOM_CONVERSION_DEL( L_uom_conversion_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_uom_conversion st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_uom_conversion st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_uom_conversion st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_UOM_CONVERSION%ISOPEN then
	     close c_svc_UOM_CONVERSION;
	  end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UOM_CONVERSION;
-----------------------------------------------------------------------
FUNCTION EXEC_UOM_CLASS_TL_INS(  L_uom_class_tl_temp_rec   IN   UOM_CLASS_TL%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CLASS_TL_INS';
   L_table   VARCHAR2(255):= 'SVC_UOM_CLASS_TL';
BEGIN
   insert
     into uom_class_tl
   values L_uom_class_tl_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CLASS_TL_INS;
------------------------------------------------------------------------------
FUNCTION EXEC_UOM_CLASS_TL_UPD( L_uom_class_tl_temp_rec   IN   UOM_CLASS_TL%ROWTYPE,
                                O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CLASS_TL_UPD';
   L_table   VARCHAR2(255):= 'SVC_UOM_CLASS_TL';
BEGIN
   update uom_class_tl
      set uom= L_uom_class_tl_temp_rec.uom,
          uom_desc_trans=L_uom_class_tl_temp_rec.uom_desc_trans,
          uom_trans  = L_uom_class_tl_temp_rec.uom_trans,
          last_update_datetime = sysdate,
          last_update_id = GET_USER
    where 1 = 1
      and uom = L_uom_class_tl_temp_rec.uom
      and lang = L_uom_class_tl_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CLASS_TL_UPD;
----------------------------------------------------------------------------
FUNCTION EXEC_UOM_CLASS_TL_DEL(  L_uom_class_tl_temp_rec   IN   UOM_CLASS_TL%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CLASS_TL_DEL';
   L_table   VARCHAR2(255):= 'SVC_UOM_CLASS_TL';
BEGIN
   delete
     from uom_class_tl
    where 1 = 1
      and uom = L_uom_class_tl_temp_rec.uom
      and lang = L_uom_class_tl_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CLASS_TL_DEL;
-------------------------------------------------------------------------------
FUNCTION PROCESS_UOM_CLASS_TL( I_process_id   IN   SVC_UOM_CLASS_TL.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_UOM_CLASS_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_UOM_CLASS.PROCESS_UOM_CLASS_TL';
   L_error_message VARCHAR2(600);
   L_UOM_CLASS_TL_temp_rec UOM_CLASS_TL%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_UOM_CLASS_TL';
BEGIN
   FOR rec IN c_svc_UOM_CLASS_TL(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

	  if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

	  if rec.action = action_new
         and rec.PK_UOM_CLASS_TL_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'UOM_CLASS_TL',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM,LANG',
                    L_error_message);
         L_error :=TRUE;
      end if;

	  if rec.action IN (action_mod,action_del)
         and rec.PK_UOM_CLASS_TL_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM,LANG',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      else
         if rec.action = action_del
            and rec.lang = LP_primary_lang then
             WRITE_ERROR(I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'LANG',
                        'CAN_NOT_DEL_PRIM_LANG'); -- New error
			 L_error :=TRUE;
	     end if;

	  end if;

      if rec.action in (action_new)
	     and rec.uct_uc_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM',
                    'INVALID_UOM');
         L_error :=TRUE;
      end if;

      if rec.action in (action_new)
         and rec.lang is NOT NULL
         and rec.cd_lang_rid is NULL then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                    'LANG_EXIST');
         L_error :=TRUE;
      end if;



      if rec.action in (action_new,action_mod)
    	   and NOT(  rec.UOM_DESC_TRANS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM_DESC_TRANS',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;

	  if rec.action in (action_new,action_mod)
	   and NOT(  rec.UOM_TRANS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM_TRANS',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;


	  if NOT L_error then
         L_uom_class_tl_temp_rec.reviewed_ind                := 'Y';
         L_uom_class_tl_temp_rec.orig_lang_ind               := 'N';
         L_uom_class_tl_temp_rec.uom_desc_trans              := rec.uom_desc_trans;
         L_uom_class_tl_temp_rec.uom_trans                   := rec.uom_trans;
         L_uom_class_tl_temp_rec.uom                         := rec.uom;
         L_uom_class_tl_temp_rec.lang                        := rec.lang;
         L_uom_class_tl_temp_rec.create_datetime             := SYSDATE;
         L_uom_class_tl_temp_rec.create_id                   := GET_USER;
		     L_uom_class_tl_temp_rec.last_update_id          := GET_USER;
         L_uom_class_tl_temp_rec.last_update_datetime        := SYSDATE;
         if rec.action = action_new then
            if EXEC_UOM_CLASS_TL_INS(   L_uom_class_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_UOM_CLASS_TL_UPD( L_uom_class_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_UOM_CLASS_TL_DEL( L_uom_class_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_uom_class_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_uom_class_tl st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_uom_class_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_UOM_CLASS_TL%ISOPEN then
		 close c_svc_UOM_CLASS_TL;
	  end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UOM_CLASS_TL;
---------------------------------------------------------------------------------------------
FUNCTION MERGE_UOM_CLASS_TL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_uom                      IN       UOM_CLASS.UOM%TYPE,
                            I_uom_desc_trans           IN       UOM_CLASS_TL.UOM_DESC_TRANS%TYPE,
                            I_uom_trans                IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                            I_lang                     IN       UOM_CLASS_TL.LANG%TYPE)
   RETURN BOOLEAN is
   L_program       VARCHAR2(61) := 'MERGE_UOM_CLASS_TL';
   L_table         VARCHAR2(30) := 'UOM_CLASS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_uom_class_exists    VARCHAR2(1)  := NULL;
   L_orig_lang_ind            VARCHAR2(1)  := 'N';
   L_reviewed_ind             VARCHAR2(1)  := 'N';

   cursor C_UOM_CLASS_EXIST is
      select 'x'
        from uom_class_tl
       where uom = I_uom
         and rownum = 1;

   cursor C_LOCK_UOM_CLASS_TL is
      select 'x'
        from uom_class_tl
       where uom = I_uom
         and lang   = I_lang
         for update nowait;

BEGIN

   -- Check first if the uom(regardless of language) already exists in the table.
   -- If it already exists, set orig_lang_ind = 'N'. Otherwise, set orig_lang_ind = 'Y'.
   -- Reviewed_ind is only set to 'N' for entries in the original language to indicate
   -- if the original description has changed and translation should be reviewed for accuracy.
   -- Both flags are only used for inserts, not updates.

   open C_UOM_CLASS_EXIST;
   fetch C_UOM_CLASS_EXIST into L_uom_class_exists;
   close C_UOM_CLASS_EXIST;

   if L_uom_class_exists is NULL then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind  := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind  := 'Y';
   end if;

   open C_LOCK_UOM_CLASS_TL;
   close C_LOCK_UOM_CLASS_TL;

   merge into uom_class_tl uctl
      using (select I_uom uom,
                    I_lang lang,
                    I_uom_trans uom_trans,
                    I_uom_desc_trans uom_desc_trans,
                    L_orig_lang_ind orig_lang_ind, --used for inserts only
                    L_reviewed_ind reviewed_ind,   --used for inserts only
                    user create_id,
                    sysdate create_datetime,
                    user last_update_id,
                    sysdate last_update_datetime
               from dual) use_this
         on (uctl.uom = use_this.uom and
             uctl.lang  = use_this.lang)
   when matched then
      update
         set uctl.uom_desc_trans = use_this.uom_desc_trans,
             uctl.uom_trans = use_this.uom_trans,
             uctl.reviewed_ind = decode(uctl.orig_lang_ind, 'Y', 'N', reviewed_ind), --when description is changed for the original language, set the entry for translation review
             uctl.last_update_id = use_this.last_update_id,
             uctl.last_update_datetime = use_this.last_update_datetime
   when NOT matched then
      insert (uom,
              lang,
              uom_trans,
              uom_desc_trans,
              orig_lang_ind,
              reviewed_ind,
              create_id,
              create_datetime,
              last_update_id,
              last_update_datetime)
      values (use_this.uom,
              use_this.lang,
              use_this.uom_trans,
              use_this.uom_desc_trans,
              use_this.orig_lang_ind,
              use_this.reviewed_ind,
              use_this.create_id,
              use_this.create_datetime,
              use_this.last_update_id,
              use_this.last_update_datetime);

   return TRUE;
EXCEPTION
   when RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_uom,
                                            I_LANG);
      return FALSE;
   when OTHERS then
      if C_LOCK_UOM_CLASS_TL%isopen then
	     close C_LOCK_UOM_CLASS_TL;
	  end if;
      if C_UOM_CLASS_EXIST%ISOPEN then
	     close C_UOM_CLASS_EXIST;
      end if;
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_UOM_CLASS_TL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_UOM_CLASS_VAL(I_rec             IN       C_SVC_UOM_CLASS%ROWTYPE,
                               O_error           IN OUT   BOOLEAN,
                               O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.PROCESS_UOM_CLASS_VAL';
cursor c_uom_exists(I_uom_class UOM_CLASS.UOM%TYPE)
is
	select 'x' from alloc_chrg
	 where per_count_uom=I_uom_class
	union
	select 'x' from alloc_chrg_temp
	 where per_count_uom=I_uom_class
	union
	select 'x' from ce_ord_item
	 where net_wt_uom=I_uom_class
	union
	select 'x' from ce_ord_item
	 where cubic_uom=I_uom_class
	union
	select 'x' from ce_ord_item
	 where GROSS_WT_UOM=I_uom_class
	union
	select 'x' from ce_ord_item
	 where CLEARED_QTY_UOM=I_uom_class
	union
	select 'x' from DEPT_CHRG_DETAIL
	 where per_count_uom=I_uom_class
	union
	select 'x' from DEAL_HEAD
	 where THRESHOLD_LIMIT_UOM=I_uom_class
	union
	select 'x' from EXP_PROF_DETAIL
	 where per_count_uom=I_uom_class
	union
	select 'x' from HTS
	 where UNITS_1=I_uom_class
	union
	select 'x' from HTS
	 where UNITS_2=I_uom_class
	union
	select 'x' from HTS
	 where UNITS_3=I_uom_class
	union
	select 'x' from ITEM_CHRG_DETAIL
	 where per_count_uom=I_uom_class
	union
	select 'x' from ITEM_EXP_DETAIL
	 where per_count_uom=I_uom_class
	union
	select 'x' from ITEM_MASTER
	 where CATCH_WEIGHT_UOM=I_uom_class
	union
	select 'x' from ITEM_MASTER
	 where PACKAGE_UOM=I_uom_class
	union
	select 'x' from ITEM_MASTER
	 where STANDARD_UOM=I_uom_class
	union
	select 'x' from ITEM_HTS_ASSESS
	 where per_count_uom=I_uom_class
	union
	select 'x' from ITEM_LOC_TRAITS
	 where FIXED_TARE_UOM=I_uom_class
	union
	select 'x' from ITEM_SUPP_COUNTRY
	 where COST_UOM=I_uom_class
	union
	select 'x' from ITEM_SUPP_COUNTRY_DIM
	 where LIQUID_VOLUME_UOM=I_uom_class
	union
	select 'x' from ITEM_SUPP_COUNTRY_DIM
	 where WEIGHT_UOM=I_uom_class
	union
	select 'x' from ITEM_SUPP_COUNTRY_DIM
	 where LWH_UOM=I_uom_class
	union
	select 'x' from ITEM_SUPP_UOM
	 where UOM=I_uom_class
	union
	select 'x' from ITEM_LOC
	 where SELLING_UOM=I_uom_class
	union
	select 'x' from MC_CHRG_DETAIL
	 where per_count_uom=I_uom_class
	union
	select 'x' from OBLIGATION_COMP
	 where per_count_uom=I_uom_class
	union
	select 'x' from OBLIGATION_COMP
	 where ALLOC_BASIS_UOM=I_uom_class
	union
	select 'x' from ORDCUST_DETAIL
	 where TRANSACTION_UOM=I_uom_class
	union
	select 'x' from ORDCUST_DETAIL
	 where STANDARD_UOM=I_uom_class
	union
	select 'x' from ORDSKU_HTS_ASSESS
	 where per_count_uom=I_uom_class
	union
	select 'x' from ORD_INV_MGMT
	 where MIN_CNSTR_UOM1=I_uom_class
	union
	select 'x' from ORD_INV_MGMT
	 where MIN_CNSTR_UOM2=I_uom_class
	union
	select 'x' from ORD_INV_MGMT
	 where TRUCK_CNSTR_UOM2=I_uom_class
	union
	select 'x' from ORD_INV_MGMT
	 where TRUCK_CNSTR_UOM1=I_uom_class
	union
	select 'x' from ORD_INV_MGMT
	 where SCALE_CNSTR_UOM2=I_uom_class
	union
	select 'x' from ORD_INV_MGMT
	 where SCALE_CNSTR_UOM2=I_uom_class
	union
	select 'x' from ORDLOC_EXP
	 where PER_COUNT_UOM=I_uom_class
	union
	select 'x' from SUP_INV_MGMT
	 where BRACKET_UOM2=I_uom_class
	union
	select 'x' from SUP_INV_MGMT
	 where BRACKET_UOM1=I_uom_class
	union
	select 'x' from SUP_INV_MGMT
	 where TRUCK_CNSTR_UOM1=I_uom_class
	union
	select 'x' from SUP_INV_MGMT
	 where SCALE_CNSTR_UOM1=I_uom_class
	union
	select 'x' from SUP_INV_MGMT
	 where SCALE_CNSTR_UOM2=I_uom_class
	union
	select 'x' from SUP_INV_MGMT
	 where TRUCK_CNSTR_UOM2=I_uom_class
	union
	select 'x' from SUP_INV_MGMT
	 where MIN_CNSTR_UOM2=I_uom_class
	union
	select 'x' from SUP_INV_MGMT
	 where MIN_CNSTR_UOM1=I_uom_class
	union
	select 'x' from SIMPLE_PACK_TEMP
	 where COST_UOM=I_uom_class
	union
	select 'x' from SIMPLE_PACK_TEMP
	 where CASE_WEIGHT_UOM=I_uom_class
	union
	select 'x' from SIMPLE_PACK_TEMP
	 where CASE_LIQUID_VOLUME_UOM=I_uom_class
	union
	select 'x' from SIMPLE_PACK_TEMP
	 where PALLET_LIQUID_VOLUME_UOM=I_uom_class
	union
	select 'x' from SIMPLE_PACK_TEMP
	 where PALLET_WEIGHT_UOM=I_uom_class
	union
	select 'x' from SIMPLE_PACK_TEMP
	 where PALLET_LWH_UOM=I_uom_class
	union
	select 'x' from SIMPLE_PACK_TEMP
	 where CASE_LWH_UOM=I_uom_class
	union
	select 'x' from TSFDETAIL_CHRG
	 where PER_COUNT_UOM=I_uom_class
	union
	select 'x' from TRANS_LIC_VISA
	 where NET_WEIGHT_UOM=I_uom_class
	union
	select 'x' from TRANS_LIC_VISA
	 where LICENSE_VISA_QTY_UOM=I_uom_class
	union
	select 'x' from TRANSPORTATION
	 where NET_WT_UOM=I_uom_class
	union
	select 'x' from TRANSPORTATION
	 where CUBIC_UOM=I_uom_class
	union
	select 'x' from TRANSPORTATION
	 where ITEM_QTY_UOM=I_uom_class
	union
	select 'x' from TRANSPORTATION
	 where GROSS_WT_UOM=I_uom_class
	union
	select 'x' from TRANSPORTATION
	 where CARTON_UOM=I_uom_class
	union
	select 'x' from TRANS_CLAIMS
	 where ITEM_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_DELIVERY
	 where ITEM_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_DELIVERY
	 where CARTON_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_PACKING
	 where CARTON_PACK_REC_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_PACKING
	 where ITEM_REC_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_PACKING
	 where CARTON_PACK_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_PACKING
	 where ITEM_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_PACKING
	 where CARTON_REC_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_PACKING
	 where CARTON_QTY_UOM=I_uom_class
	union
	select 'x' from TRANS_SKU
	 where QUANTITY_UOM=I_uom_class
	union
	select 'x' from UOM_CONVERSION
	 where FROM_UOM=I_uom_class
	union
	select 'x' from UOM_CONVERSION
	 where TO_UOM=I_uom_class
	union
	select 'x' from WH_DEPT
	 where COST_OUT_STORAGE_UOM=I_uom_class
	union
	select 'x' from WH_DEPT
	 where COST_WH_STORAGE_UOM=I_uom_class;

 L_exists varchar2(1);

BEGIN
 open c_uom_exists(I_rec.uom);
 fetch c_uom_exists into L_exists;
 close c_uom_exists;
 if I_rec.action in (action_del)
    and L_exists = 'x' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     'SVC_UOM_CLASS',
                     I_rec.row_seq,
                     'UOM',
                     'CHILD_EXISTS');
         O_error :=TRUE;
         L_exists:=null;
 end if;
 return TRUE;
EXCEPTION
   when OTHERS then
      if c_uom_exists%isopen then
	     close c_uom_exists;
	  end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UOM_CLASS_VAL;
--------------------------------------------------------------------------------
FUNCTION EXEC_UOM_CLASS_INS(  L_uom_class_temp_rec   IN       UOM_CLASS%ROWTYPE,
                              O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
							                I_rec                  IN       C_SVC_UOM_CLASS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CLASS_INS';
   L_table   VARCHAR2(255):= 'SVC_UOM_CLASS';
BEGIN
   SAVEPOINT UOM_CLASS_INS;

   insert
     into uom_class
   values L_uom_class_temp_rec;
   return TRUE;

   if MERGE_UOM_CLASS_TL(O_error_message,
                         I_rec.uom,
                         I_rec.uom_desc_trans,
                         I_rec.uom_trans,
                         LP_primary_lang) = FALSE then
      ROLLBACK TO SAVEPOINT UOM_CLASS_INS;
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CLASS_INS;
-----------------------------------------------------------------------------------
FUNCTION EXEC_UOM_CLASS_UPD( L_uom_class_temp_rec   IN   UOM_CLASS%ROWTYPE,
                             O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_rec                  IN       C_SVC_UOM_CLASS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CLASS_UPD';
   L_table   VARCHAR2(255):= 'SVC_UOM_CLASS';
   cursor C_LOCK_UOM_CLASS_UPD is
      select 'x'
        from UOM_CLASS
       where UOM_CLASS = L_uom_class_temp_rec.UOM
         for update nowait;

BEGIN
   open C_LOCK_UOM_CLASS_UPD;
   close C_LOCK_UOM_CLASS_UPD;

   if MERGE_UOM_CLASS_TL(O_error_message,
                         I_rec.uom,
                         I_rec.uom_desc_trans,
                         I_rec.uom_trans,
                         LP_primary_lang) = FALSE then
      ROLLBACK TO SAVEPOINT UOM_CLASS_INS;
      return FALSE;
   end if;

   update uom_class
      set row = L_uom_class_temp_rec
    where 1 = 1
      and uom = L_uom_class_temp_rec.uom;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_UOM_CLASS_UPD%isopen then
	     close C_LOCK_UOM_CLASS_UPD;
	  end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CLASS_UPD;
---------------------------------------------------------------------------------
FUNCTION EXEC_UOM_CLASS_DEL(  L_uom_class_temp_rec   IN   UOM_CLASS%ROWTYPE ,
                              O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_UOM_CLASS.EXEC_UOM_CLASS_DEL';
   L_table   VARCHAR2(255):= 'SVC_UOM_CLASS';
   cursor C_UOM_CLASS_LOCK is
      select 'X'
        from UOM_CLASS
       where 1 = 1
         and uom = L_uom_class_temp_rec.uom
         for update nowait;

   cursor C_UOM_CLASS_TL_LOCK is
      select 'X'
        from UOM_CLASS_tl
       where 1 = 1
         and uom = L_uom_class_temp_rec.uom
         for update nowait;
BEGIN
   open C_UOM_CLASS_LOCK;
   close C_UOM_CLASS_LOCK;

   open C_UOM_CLASS_TL_LOCK;
   close C_UOM_CLASS_TL_LOCK;

   delete
     from uom_class_tl
    where 1 = 1
      and uom = L_uom_class_temp_rec.uom;
   return TRUE;

   delete
     from uom_class
    where 1 = 1
      and uom = L_uom_class_temp_rec.uom;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_UOM_CLASS_LOCK%isopen then
	     close C_UOM_CLASS_LOCK;
	  end if;
      if C_UOM_CLASS_TL_LOCK%isopen then
	     close C_UOM_CLASS_TL_LOCK;
	  end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UOM_CLASS_DEL;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_UOM_CLASS( I_process_id   IN   SVC_UOM_CLASS.PROCESS_ID%TYPE,
                            I_chunk_id     IN SVC_UOM_CLASS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error               BOOLEAN;
   L_process_error       BOOLEAN := FALSE;
   L_program             VARCHAR2(255):='CORESVC_UOM_CLASS.PROCESS_UOM_CLASS';
   L_error_message       VARCHAR2(600);
   L_UOM_CLASS_temp_rec  UOM_CLASS%ROWTYPE;
   L_table               VARCHAR2(255)    :='SVC_UOM_CLASS';
BEGIN
   FOR rec IN c_svc_UOM_CLASS(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,
							   action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action in (action_new,action_mod)
       and NOT(  rec.UOM_DESC_TRANS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM_DESC_TRANS',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;

      if rec.action in (action_new,action_mod)
       and NOT(  rec.UOM_TRANS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM_TRANS',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_UOM_CLASS_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'UOM_CLASS',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM',
                    L_error_message);
         L_error :=TRUE;
      end if;

      if rec.action IN (action_del)
         and rec.PK_UOM_CLASS_rid is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'UOM_CLASS',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM',
                     L_error_message);
         L_error :=TRUE;
      end if;

      if rec.action in (action_mod)
	     and nvl(rec.uom_class,'-1') <> nvl(rec.uom_class_old,'-1') then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM_CLASS',
                     'FIELD_NO_MOD');
         L_error :=TRUE;
	  end if;
	  if rec.action in (action_new)
	   and rec.UOM_CLASS  IS NOT NULL
       and rec.cduomc_rid is null then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'CODE_DETAIL',
                                                NULL,
                                                NULL);

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM_CLASS',
                     L_error_message);
         L_error :=TRUE;

	end if;

    if rec.action in (action_new)
	   and NOT(  rec.UOM_CLASS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UOM_CLASS',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;

      if PROCESS_UOM_CLASS_VAL(rec,
                               L_error,
                               L_error_message) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     L_error_message);
         L_error :=TRUE;
      end if;

	  if NOT L_error then
         L_uom_class_temp_rec.uom_class              := rec.uom_class;
         L_uom_class_temp_rec.uom                    := rec.uom;
         if rec.action = action_new then
            if EXEC_UOM_CLASS_INS(   L_uom_class_temp_rec,
                                             L_error_message,rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_UOM_CLASS_UPD( L_uom_class_temp_rec,
                                             L_error_message,rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_UOM_CLASS_DEL( L_uom_class_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_uom_class st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_uom_class st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_uom_class st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_UOM_CLASS%isopen then
	     close c_svc_UOM_CLASS;
	  end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UOM_CLASS;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete
     from svc_uom_x_conversion
    where process_id=I_process_id;

   delete
     from svc_uom_conversion
    where process_id=I_process_id;

   delete
     from svc_uom_class_tl
    where process_id=I_process_id;

   delete
     from svc_uom_class
    where process_id=I_process_id;
END;

--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER
                  )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_UOM_CLASS.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';

BEGIN
   LP_errors_tab   := NEW errors_tab_typ();
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;

   if PROCESS_UOM_CLASS(I_process_id,
                        I_chunk_id)=FALSE then
      return FALSE;
   end if;

   commit;

   if PROCESS_UOM_CONVERSION(I_process_id,
                             I_chunk_id)=FALSE then
      return FALSE;
   end if;

   commit;

   if PROCESS_UOM_CLASS_TL(I_process_id,
                           I_chunk_id)=FALSE then
      return FALSE;
   end if;
   commit;
   if PROCESS_UOM_X_CONVERSION(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   commit;


   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

    open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);


   return TRUE;
EXCEPTION
   when OTHERS then
      if c_get_err_count%isopen then
	     close c_get_err_count;
	  end if;
      if c_get_warn_count%isopen then
	     close c_get_warn_count;
	  end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_UOM_CLASS;
/