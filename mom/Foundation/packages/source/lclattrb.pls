
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY LOCLIST_ATTRIBUTE_SQL AS

-----------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message IN OUT VARCHAR2,
                  O_loc_list_desc IN OUT LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                  I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN IS
   L_program        VARCHAR2(64) := 'LOCLIST_ATTRIBUTE_SQL.GET_DESC';
  
   cursor C_LOC_LIST is
      select loc_list_desc
        from v_loc_list_head_tl
       where loc_list = I_loc_list;

BEGIN
   open C_LOC_LIST;
   fetch C_LOC_LIST into O_loc_list_desc;
   if C_LOC_LIST%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_LOC_LIST', null, null, null);
      close C_LOC_LIST;
      return FALSE;
   else
      close C_LOC_LIST;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END GET_DESC;
-----------------------------------------------------------------------
FUNCTION GET_LOC_COUNT(O_error_message IN OUT VARCHAR2,
                       O_loc_count     IN OUT NUMBER,
                       I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN IS
   L_program        VARCHAR2(64) := 'LOCLIST_ATTRIBUTE_SQL.GET_LOC_COUNT';

   cursor C_GET_COUNT is
      select count(*)
        from loc_list_detail
       where loc_list = I_loc_list;

BEGIN
   open C_GET_COUNT;
   fetch C_GET_COUNT into O_loc_count;
   close C_GET_COUNT;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END GET_LOC_COUNT;

-----------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message     IN OUT VARCHAR2,
                         O_loc_list_desc     IN OUT LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                         O_create_date       IN OUT LOC_LIST_HEAD.CREATE_DATE%TYPE,
                         O_last_rebuild_date IN OUT LOC_LIST_HEAD.LAST_REBUILD_DATE%TYPE,
                         O_create_id         IN OUT LOC_LIST_HEAD.CREATE_ID%TYPE,
                         O_static_ind        IN OUT LOC_LIST_HEAD.STATIC_IND%TYPE,
                         O_batch_rebuild_ind IN OUT LOC_LIST_HEAD.BATCH_REBUILD_IND%TYPE,
                         O_source            IN OUT LOC_LIST_HEAD.SOURCE%TYPE,
                         O_external_ref_no   IN OUT LOC_LIST_HEAD.EXTERNAL_REF_NO%TYPE,
                         O_comment_desc      IN OUT LOC_LIST_HEAD.COMMENT_DESC%TYPE,
                         O_filter_org_id     IN OUT LOC_LIST_HEAD.FILTER_ORG_ID%TYPE,
                         I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN IS
   L_program        VARCHAR2(64) := 'LOCLIST_ATTRIBUTE_SQL.GET_HEADER_INFO';

   CURSOR C_GET_HEADER_INFO IS
     SELECT vll.loc_list_desc,
             ll.create_date,
             ll.last_rebuild_date,
             ll.create_id,
             ll.static_ind,
             ll.batch_rebuild_ind,
             ll.SOURCE,
             ll.external_ref_no,
             ll.comment_desc,
             ll.filter_org_id
        FROM loc_list_head ll,
             v_loc_list_head_tl vll
       WHERE vll.loc_list = ll.loc_list
         and ll.loc_list = I_loc_list;

BEGIN
   open C_GET_HEADER_INFO;
   fetch C_GET_HEADER_INFO into O_loc_list_desc,
                                O_create_date,
                                O_last_rebuild_date,
                                O_create_id,
                                O_static_ind,
                                O_batch_rebuild_ind,
                                O_source,
                                O_external_ref_no,
                                O_comment_desc,
                                O_filter_org_id;
   if C_GET_HEADER_INFO%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_LOC_LIST', null, null, null);
      close C_GET_HEADER_INFO;
      return FALSE;
   else
      close C_GET_HEADER_INFO;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', 
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END GET_HEADER_INFO;

-----------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message     IN OUT VARCHAR2,
                         O_loc_list_desc     IN OUT LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                         O_create_date       IN OUT LOC_LIST_HEAD.CREATE_DATE%TYPE,
                         O_last_rebuild_date IN OUT LOC_LIST_HEAD.LAST_REBUILD_DATE%TYPE,
                         O_create_id         IN OUT LOC_LIST_HEAD.CREATE_ID%TYPE,
                         O_static_ind        IN OUT LOC_LIST_HEAD.STATIC_IND%TYPE,
                         O_batch_rebuild_ind IN OUT LOC_LIST_HEAD.BATCH_REBUILD_IND%TYPE,
                         O_source            IN OUT LOC_LIST_HEAD.SOURCE%TYPE,
                         O_external_ref_no   IN OUT LOC_LIST_HEAD.EXTERNAL_REF_NO%TYPE,
                         O_comment_desc      IN OUT LOC_LIST_HEAD.COMMENT_DESC%TYPE,
                         I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN IS
 
  L_program        VARCHAR2(64) := 'LOCLIST_ATTRIBUTE_SQL.GET_HEADER_INFO';
  L_filter_org_id  LOC_LIST_HEAD.FILTER_ORG_ID%TYPE;

BEGIN

   if GET_HEADER_INFO(O_error_message,
                      O_loc_list_desc,
                      O_create_date,
                      O_last_rebuild_date,
                      O_create_id,
                      O_static_ind,
                      O_batch_rebuild_ind,
                      O_source,
                      O_external_ref_no,
                      O_comment_desc,
			    L_filter_org_id,
                      I_loc_list) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', 
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END GET_HEADER_INFO;

-----------------------------------------------------------------------
FUNCTION GET_SOURCE(O_error_message IN OUT VARCHAR2,
                    O_source        IN OUT LOC_LIST_HEAD.SOURCE%TYPE,
                    I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN IS
   L_program        VARCHAR2(64) := 'LOCLIST_ATTRIBUTE_SQL.GET_SOURCE';
   ---
   cursor C_GET_SOURCE is
      select source
        from loc_list_head
       where loc_list = I_loc_list;

BEGIN
   open C_GET_SOURCE;
   fetch C_GET_SOURCE into O_source;
   if C_GET_SOURCE%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_LOC_LIST', null, null, null);
      close C_GET_SOURCE;
      return FALSE;
   else
      close C_GET_SOURCE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR', 
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END GET_SOURCE;

-----------------------------------------------------------------------
FUNCTION STORE_EXISTS(O_error_message IN OUT VARCHAR2,
                      O_exist         IN OUT BOOLEAN,
                      I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN IS
   L_program        VARCHAR2(64):= 'LOCLIST_ATTRIBUTE_SQL.STORE_EXISTS';
   L_exist          VARCHAR2(1);

   cursor C_STORE_EXISTS is
      select 'x'
        from loc_list_detail
       where loc_list = I_loc_list
         and loc_type = 'S';

BEGIN
   open C_STORE_EXISTS;
   fetch C_STORE_EXISTS into L_exist;
   if C_STORE_EXISTS%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   ---
   close C_STORE_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END STORE_EXISTS;
-----------------------------------------------------------------------
FUNCTION WH_EXISTS(O_error_message     IN OUT VARCHAR2,
                   O_exist             IN OUT BOOLEAN,
                   I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE,
                   I_chk_stockhold_ind IN     VARCHAR2)

   RETURN BOOLEAN IS

   L_program        VARCHAR2(64):= 'LOCLIST_ATTRIBUTE_SQL.WH_EXISTS';
   L_exist          VARCHAR2(1);

   cursor C_WH_EXISTS is
      select 'x'
        from loc_list_detail lld
       where lld.loc_list         = I_loc_list
         and lld.loc_type         = 'W'
         and (I_chk_stockhold_ind = 'N'
          or (I_chk_stockhold_ind = 'Y'
              and exists (select 'x'
                            from wh
                           where wh.wh               = lld.location
                             and wh.stockholding_ind = 'Y')));

BEGIN

   open C_WH_EXISTS;
   fetch C_WH_EXISTS into L_exist;
   if C_WH_EXISTS%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   ---
   close C_WH_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END WH_EXISTS;
-----------------------------------------------------------------------
FUNCTION WH_EXISTS(O_error_message IN OUT VARCHAR2,
                   O_exist         IN OUT BOOLEAN,
                   I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(64):= 'LOCLIST_ATTRIBUTE_SQL.WH_EXISTS';

BEGIN

   if WH_EXISTS(O_error_message,
                O_exist,
                I_loc_list,
                'N') = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END WH_EXISTS;
-----------------------------------------------------------------------
FUNCTION CRITERIA_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exist         IN OUT BOOLEAN,
                        I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
         RETURN BOOLEAN IS
   L_program        VARCHAR2(64):= 'LOCLIST_ATTRIBUTE_SQL.CRITERIA_EXIST';
   L_exist          VARCHAR2(1);

   cursor C_CRIT_EXISTS is
      select 'x'
        from loc_list_criteria
       where loc_list = I_loc_list;

BEGIN
   open C_CRIT_EXISTS;
   fetch C_CRIT_EXISTS into L_exist;
   if C_CRIT_EXISTS%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   ---
   close C_CRIT_EXISTS;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END CRITERIA_EXIST;
------------------------------------------------------------------------------
FUNCTION GET_LAST_REBUILD_DATE(O_error_message     IN OUT VARCHAR2,
                               O_last_rebuild_date IN OUT LOC_LIST_HEAD.LAST_REBUILD_DATE%TYPE,
                               I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'LOCLIST_ATTRIBUTE_SQL.GET_LAST_REBUILD_DATE';
   L_last_rebuild_date   LOC_LIST_HEAD.LAST_REBUILD_DATE%TYPE;

   cursor C_REBUILD is
      select last_rebuild_date
        from loc_list_head
       where loc_list = I_loc_list;

BEGIN
   open C_REBUILD;
   fetch C_REBUILD into L_last_rebuild_date;
   close C_REBUILD;
   ---
   O_last_rebuild_date := L_last_rebuild_date;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END GET_LAST_REBUILD_DATE;
-----------------------------------------------------------------------
FUNCTION DELETE_LOC_LIST_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_loc_list        IN       LOC_LIST_HEAD_TL.LOC_LIST%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'LOCLIST_ATTRIBUTE_SQL.DELETE_LOC_LIST_HEAD_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_LOC_LIST_HEAD_TL is
      select 'x'
        from loc_list_head_tl
       where loc_list = I_loc_list
         for update nowait;
BEGIN

   if I_loc_list is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc_list',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'LOC_LIST_HEAD_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_LOC_LIST_HEAD_TL',
                    L_table,
                    'I_loc_list '||I_loc_list);
   open C_LOCK_LOC_LIST_HEAD_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_LOC_LIST_HEAD_TL',
                    L_table,
                    'I_loc_list'||I_loc_list);
   close C_LOCK_LOC_LIST_HEAD_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'I_loc_list '||I_loc_list);
   delete from loc_list_head_tl
         where loc_list = I_loc_list;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_LOC_LIST_HEAD_TL;
----------------------------------------------------------------------------------------------------------
END LOCLIST_ATTRIBUTE_SQL;
/ 
