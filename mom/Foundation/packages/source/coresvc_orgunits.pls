create or replace PACKAGE CORESVC_ORGUNIT AUTHID CURRENT_USER AS
   template_key                       CONSTANT VARCHAR2(255):='ORGUNIT_DATA';
   action_new                         VARCHAR2(25)          := 'NEW';
   action_mod                         VARCHAR2(25)          := 'MOD';
   action_del                         VARCHAR2(25)          := 'DEL';
   ORG_UNIT_TL_sheet                  VARCHAR2(255)         := 'ORG_UNIT_TL';
   ORG_UNIT_TL$Action                 NUMBER                :=1;
   ORG_UNIT_TL$LANG                   NUMBER                :=2;
   ORG_UNIT_TL$ORG_UNIT_ID            NUMBER                :=3;
   ORG_UNIT_TL$DESCRIPTION            NUMBER                :=4;
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column                      VARCHAR2(255)         := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE := 'RMSADM';
   TYPE ORG_UNIT_TL_rec_tab IS TABLE OF ORG_UNIT_TL%ROWTYPE;
   ORG_UNIT_sheet                     VARCHAR2(255)         := 'ORG_UNIT';
   ORG_UNIT$Action                    NUMBER                :=1;
   ORG_UNIT$ORG_UNIT_ID               NUMBER                :=2;
   ORG_UNIT$DESCRIPTION               NUMBER                :=3;
   ORG_UNIT$SET_OF_BOOKS_ID           NUMBER                :=4;
   TYPE ORG_UNIT_rec_tab IS TABLE OF ORG_UNIT%ROWTYPE;
   -------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   -------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
END CORESVC_ORGUNIT;
/