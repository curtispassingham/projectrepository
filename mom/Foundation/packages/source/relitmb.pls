CREATE OR REPLACE PACKAGE BODY RELATED_ITEM_SQL AS

LP_related_items_tbl RELATED_ITEMS_TBL; -- To explode the item or itemlist and insert into the related_item_detail table.
------------------------------------------------------------------
FUNCTION NEXT_REL_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_next_rel_id   IN OUT RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE)
RETURN BOOLEAN IS

  L_program VARCHAR2(64) := 'RELATED_ITEM_SQL.NEXT_REL_ID';
  
  cursor C_NEXT_REL_ID is
     select RELATIONSHIP_ID_SEQ.nextval 
       from sys.dual;
     
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_NEXT_REL_ID',NULL, NULL);
   open C_NEXT_REL_ID;
   
   SQL_LIB.SET_MARK('FETCH','C_NEXT_REL_ID',NULL, NULL); 
   fetch C_NEXT_REL_ID into O_next_rel_id;
   
   SQL_LIB.SET_MARK('CLOSE','C_NEXT_REL_ID',NULL, NULL);
   close C_NEXT_REL_ID;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_NEXT_REL_ID%ISOPEN then
         close C_NEXT_REL_ID;
      end if;
      --
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
      
END NEXT_REL_ID;
------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL_RECORDS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_relationship_id IN     RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64)  := 'RELATED_ITEM_SQL.DELETE_DETAIL_RECORDS';
   L_table   VARCHAR2(100) := 'RELATED_ITEM_DETAIL';
  
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
  
   cursor C_LOCK_DETAILS is
      select 'x'
        from related_item_detail 
       where relationship_id = I_relationship_id
         for update nowait;
BEGIN
   if I_relationship_id is NULL then
      O_error_message    := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_relationship_id',L_program,NULL);
      return FALSE;
   end if;
   
--- check for record-locking
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_DETAILS', L_table,'RELATIONSHIP_ID: '||I_relationship_id);
   open C_LOCK_DETAILS;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DETAILS', L_table,'RELATIONSHIP_ID: '||I_relationship_id);
   close C_LOCK_DETAILS;
---
   delete from related_item_detail 
    where relationship_id = I_relationship_id;
  
   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then 
      if C_LOCK_DETAILS%ISOPEN then
         close C_LOCK_DETAILS;
      end if;                  
      O_error_message := SQL_LIB.CREATE_MSG('DELRECSB_REC_LOC','RELATED_ITEM_DETAIL',NULL,NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,L_program,TO_CHAR(SQLCODE));
   return FALSE;
END DELETE_DETAIL_RECORDS;
-----------------------------------------------------------------------
FUNCTION MERGE_RELATED_ITEMS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_no_recs              IN OUT BOOLEAN,
                             I_main_item            IN     RELATED_ITEM_HEAD.ITEM%TYPE,
                             I_relationship_type    IN     RELATED_ITEM_HEAD.RELATIONSHIP_TYPE%TYPE,
                             I_relationship_id      IN     RELATED_ITEM_DETAIL.RELATIONSHIP_ID%TYPE,
                             I_priority             IN     RELATED_ITEM_DETAIL.PRIORITY%TYPE,
                             I_start_date           IN     RELATED_ITEM_DETAIL.START_DATE%TYPE,
                             I_end_date             IN     RELATED_ITEM_DETAIL.END_DATE%TYPE)
RETURN BOOLEAN IS
   
   L_program   VARCHAR2(64)   := 'RELATED_ITEM_SQL.INSERT_RELATED_ITEMS';
   L_error_msg VARCHAR2(2000) := NULL;

BEGIN
   
   if I_relationship_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_relationship_id',L_program,NULL);
      return FALSE;
   end if;
   
   if I_relationship_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_relationship_type',L_program,NULL);
      return FALSE;
   end if;
   
   if I_main_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_main_item',L_program,NULL);
      return FALSE;
   end if;
   --
   O_no_recs := FALSE;
   --
   if LP_related_items_tbl.COUNT = 0 then
      O_no_recs := TRUE; -- After all the validations , if there are no eligible records in the collection to process	
   end if;
--  
   if LP_related_items_tbl is NOT NULL and LP_related_items_tbl.COUNT > 0 then
      FOR i in LP_related_items_tbl.first..LP_related_items_tbl.last LOOP
          
         MERGE into related_item_detail rid USING
            (
		            select LP_related_items_tbl(i).item related_item,
	                     rih.item main_item ,
	                     rih.relationship_type,
	                     rih.relationship_id rel_id
	                from  related_item_head rih
	               where rih.item = I_main_item --MAIN_ITEM
	                 and rih.relationship_type = I_relationship_type  -- RELATIONSHIP_TYPE
	          )use_this
	          ON (    rid.related_item = use_this.related_item
	              and use_this.relationship_type =  (select relationship_type 
	                                                   from related_item_head 
	                                                  where relationship_id = rid.relationship_id)
	              and use_this.main_item = (select item 
	                                          from related_item_head 
	                                         where relationship_id = rid.relationship_id)
               )
	          WHEN MATCHED THEN UPDATE SET rid.priority   = I_priority,
	                                       rid.start_date = I_start_date,
	                                       rid.end_date   = I_end_date
	                                 where rid.relationship_id = use_this.rel_id
	          WHEN NOT MATCHED THEN insert (rid.relationship_id,
	                                        rid.related_item,   
	                                        rid.priority,
	                                        rid.start_date,
	                                        rid.end_date )
	                                values  (use_this.rel_id,
	                                         use_this.related_item,
	                                         I_priority,
	                                         I_start_date,
	                                         I_end_date)
                                  where use_this.rel_id = I_relationship_id; 
      END LOOP;
   
   end if;
   
   return TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.GET_MESSAGE_TEXT('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  return FALSE;
END MERGE_RELATED_ITEMS;
--------------------------------------------------------------------------------------------------
FUNCTION UPDATE_RELATED_ITEMS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item            IN RELATED_ITEM_DETAIL.RELATED_ITEM%TYPE,
                              I_relationship_id IN RELATED_ITEM_DETAIL.RELATIONSHIP_ID%TYPE,
                              I_priority        IN RELATED_ITEM_DETAIL.PRIORITY%TYPE,
                              I_start_date      IN RELATED_ITEM_DETAIL.START_DATE%TYPE,
                              I_end_date        IN RELATED_ITEM_DETAIL.END_DATE%TYPE )
  
RETURN BOOLEAN IS
   
   L_program   VARCHAR2(64)     := 'RELATED_ITEM_SQL.UPDATE_RELATED_ITEMS';
   L_error_msg VARCHAR2(2000)   :=  NULL;
   L_table     VARCHAR2(100)    := 'RELATED_ITEM_DETAIL';
  
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
    
   cursor C_LOCK_DETAILS is
      select 'x'
        from related_item_detail 
       where relationship_id   = I_relationship_id
         and related_item      = I_item
         and priority          = I_priority
         and start_date        = I_start_date
         and end_date          = I_end_date
      for update nowait;
BEGIN
   if I_relationship_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_relationship_id',L_program,NULL);
      return FALSE;
   end if;
   
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;
   
   --- check for record-locking
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_DETAILS', L_table,NULL);
   open C_LOCK_DETAILS;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DETAILS',L_table,NULL);
   close C_LOCK_DETAILS;
   
   update related_item_detail
      set priority        = I_priority,
          start_date      = I_start_date,
          end_date        = I_end_date
    where relationship_id = I_relationship_id
      and related_item    = I_item ;
  
   return TRUE;
  
EXCEPTION
when RECORD_LOCKED then
   if C_LOCK_DETAILS%ISOPEN then
         close C_LOCK_DETAILS;
   end if;  
      
   O_error_message := SQL_LIB.CREATE_MSG('DELRECSB_REC_LOC','RELATED_ITEM_DETAIL', NULL,NULL);
   return FALSE;
when OTHERS then
   O_error_message := SQL_LIB.GET_MESSAGE_TEXT('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  return FALSE;
END UPDATE_RELATED_ITEMS;
-----------------------------------------------------------------------------------------------------------
FUNCTION GET_STANDARD_UOM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_standard_uom   IN OUT UOM_CLASS.UOM%TYPE,
                          I_item           IN     ITEM_MASTER.ITEM%TYPE )
RETURN BOOLEAN IS

L_program         VARCHAR2(64) := 'RELATED_ITEM_SQL.GET_STANDARD_UOM';

cursor C_ITEM is
   select standard_uom
     from item_master
    where item = I_item;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   open C_ITEM;
   
   SQL_LIB.SET_MARK('FETCH', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   fetch C_ITEM into O_standard_uom;
   
   if C_ITEM%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
      close C_ITEM;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM',NULL,NULL,NULL);
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_ITEM','ITEM_MASTER','Item:'||I_item);
   close C_ITEM;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,L_program,to_char(SQLCODE));
      return FALSE;
END GET_STANDARD_UOM;
------------------------------------------------------------------------------------------------------------
FUNCTION BASIC_CHECKS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_sellable_count       IN OUT VARCHAR2,
                      O_Non_tran_lvl_Count   IN OUT VARCHAR2,
                      O_item_less_tran_Count IN OUT VARCHAR2,
                      O_Std_uom_Count        IN OUT VARCHAR2,
                      O_main_item_exists     IN OUT VARCHAR2,
                      O_pending_del_item     IN OUT VARCHAR2,
                      I_main_item            IN ITEM_MASTER.ITEM%TYPE,
                      I_main_item_std_uom    IN ITEM_MASTER.STANDARD_UOM%TYPE,
                      I_item_type            IN VARCHAR2,
                      I_item                 IN VARCHAR2,
                      I_relationship_type    IN RELATED_ITEM_HEAD.RELATIONSHIP_TYPE%TYPE )
RETURN BOOLEAN IS

L_program VARCHAR2(64) := 'RELATED_ITEM_SQL.BASIC_CHECKS';
  
cursor C_BASIC_CHECKS is
   WITH items AS
      (select item 
         from v_item_master vit 
        where item = I_item
          and I_item_type = 'SS'
       UNION
    	 select skd.item
    	   from skulist_detail skd,
    	        v_item_master vit
    	  where I_item_type = 'IL'
    	    and skd.skulist=I_item
    	    and vit.item =skd.item
        )
      select 
         max(case when sd.item = I_main_item 
                  then 'X' 
                  else null 
                  end) -- main item 
        ,max(case when im.sellable_ind='Y' and  sd.item <> I_main_item 
                  then 'X' 
                  else null 
                  end) -- Sellable items Count
        ,max(case when im.sellable_ind='Y' and  sd.item <> I_main_item and I_relationship_type='SUBS' and im.standard_uom <> I_main_item_std_uom 
                  then 'X' 
                  else null 
                  end) -- Std_Uom_Count
        ,max(case when im.sellable_ind='Y' AND sd.item <> I_main_item AND im.item_level != im.tran_level 
                  then 'X' 
                  else null 
                  end) -- Non_tran_lvl_Count
        ,max(case when im.sellable_ind='Y' AND sd.item <> I_main_item AND im.item_level <= im.tran_level 
                  then 'X' 
                  else null 
                  end) -- item_less_tran_Count -- This is to check if there are any items which are at or below transaction level.If no items found, then the processing will be stopped.
        from items sd,
              v_item_master im
     where sd.item =im.item;
 
 cursor C_DLY_PURGE is
   WITH items AS
        (select item 
           from v_item_master vit 
          where item = I_item
            and I_item_type = 'SS'
         UNION
      	 select skd.item
      	   from skulist_detail skd,
      	        v_item_master vit
      	  where I_item_type = 'IL'
      	    and skd.skulist=I_item
      	    and vit.item =skd.item
        )
    select max(case when (im.item= dp.key_value or im.item_parent= dp.key_value or im.item_grandparent= dp.key_value)
               then 'X'
               else null
               end)
    from items sd,
         daily_purge dp,
         v_item_master im
   where sd.item =im.item
     and(im.item = dp.key_value or im.item_parent= dp.key_value or im.item_grandparent=dp.key_value)
     and table_name  = 'ITEM_MASTER';
     
BEGIN
   if I_main_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_main_item',L_program,NULL);
      return FALSE;
   end if;
   
   if I_main_item_std_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_main_item_std_uom',L_program,NULL);
      return FALSE;
   end if;
   
   if I_item_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item_type',L_program,NULL);
      return FALSE;
   end if;
   
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;
   
   if I_relationship_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_relationship_type',L_program,NULL);
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('OPEN','C_BASIC_CHECKS',NULL,NULL);
   open C_BASIC_CHECKS;
   
   SQL_LIB.SET_MARK('FETCH','C_BASIC_CHECKS',NULL,NULL);
   fetch C_BASIC_CHECKS into O_main_item_exists,O_sellable_count,O_Std_uom_Count,O_Non_tran_lvl_Count,O_item_less_tran_Count;
  
   SQL_LIB.SET_MARK('CLOSE','C_BASIC_CHECKS',NULL,NULL);
   close C_BASIC_CHECKS;
   --
   SQL_LIB.SET_MARK('OPEN','C_DLY_PURGE',NULL,NULL);
   open C_DLY_PURGE;
   
   SQL_LIB.SET_MARK('FETCH','C_DLY_PURGE',NULL,NULL);
   fetch C_DLY_PURGE into O_pending_del_item;
   
   SQL_LIB.SET_MARK('CLOSE','C_DLY_PURGE',NULL,NULL);
   close C_DLY_PURGE;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_BASIC_CHECKS%ISOPEN then
         close C_BASIC_CHECKS;
      end if;
      
      if C_DLY_PURGE%ISOPEN then
         close C_DLY_PURGE;
      end if;
  
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, NULL);
  
   return FALSE;
END BASIC_CHECKS;
------------------------------------------------------------------------------------------------------------
FUNCTION GET_TRAN_LVL_ITEMS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_type          IN       VARCHAR2,
                            I_item               IN       VARCHAR2,
                            I_relationship_type  IN       RELATED_ITEM_HEAD.RELATIONSHIP_TYPE%TYPE,
                            I_tran_lvl_response  IN       VARCHAR2,
                            I_main_item          IN       ITEM_MASTER.ITEM%TYPE,	
                            I_main_std_uom       IN       ITEM_MASTER.STANDARD_UOM%TYPE)
RETURN BOOLEAN IS

L_program VARCHAR2(64) := 'RELATED_ITEM_SQL.GET_TRAN_LVL_ITEMS';

cursor C_TRAN_ITEMS is
   WITH items AS
      (select item,
              item_level,
              tran_level
         from skulist_detail
        where skulist   = I_item
          and I_item_type ='IL'
       UNION ALL
       select item,
              item_level,
              tran_level
        from item_master
       where item      = I_item
         and I_item_type = 'SS')
     select RELATED_ITEMS_REC(im.item,
                              im.item_level,
                              im.tran_level)                                                      
       from items sd,
            item_master im
      where im.sellable_ind = 'Y'
        and NOT EXISTS (select 'X' 
	                  from daily_purge
	                 where key_value in (select item 
	                                       from item_master 
	                                      where item= sd.item
	                                         or item_parent = sd.item
	                                         or item_grandparent = sd.item
	                                     )
	                            
                         and table_name = 'ITEM_MASTER') -- To exclude the item and children which are pending for deletion.
        and sd.item <>  I_main_item -- Main item should not be related to itself.
        and im.standard_uom = decode(I_relationship_type,'SUBS',I_main_std_uom,im.standard_uom)
        and ( 
             (sd.item_level  < sd.tran_level  AND I_tran_lvl_response = 'Y' AND im.item<> I_main_item and im.item_level=im.tran_level AND (sd.item = im.item_grandparent OR sd.item  = im.item_parent ) )
          OR (sd.item_level  = sd.tran_level  AND sd.item  = im.item )
        )
     ORDER BY im.item;

BEGIN
   if I_item_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item_type',L_program,NULL);
      return FALSE;
   end if;
   
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;
   
   if I_relationship_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_relationship_type',L_program,NULL);
      return FALSE;
   end if;
   
   if I_tran_lvl_response is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_tran_lvl_response',L_program,NULL);
      return FALSE;
   end if;
   
   if I_main_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_main_item',L_program,NULL);
      return FALSE;
   end if;
   
   if I_main_std_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_main_std_uom',L_program,NULL);
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('OPEN', 'C_TRAN_ITEMS',NULL,NULL);
   open C_TRAN_ITEMS;
   
   SQL_LIB.SET_MARK('FETCH', 'C_TRAN_ITEMS',NULL,NULL);
   fetch C_TRAN_ITEMS bulk collect into LP_related_items_tbl;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_TRAN_ITEMS',NULL,NULL);
   close C_TRAN_ITEMS;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      if C_TRAN_ITEMS%ISOPEN then
         close C_TRAN_ITEMS;
      end if;
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, NULL);
   
   return FALSE;
   
END GET_TRAN_LVL_ITEMS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHK_REL_ITEM_CONFLICT(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                               O_exists             IN OUT BOOLEAN,
                               I_main_item          IN     RELATED_ITEM_HEAD.ITEM%TYPE,
                               I_relationship_type  IN     RELATED_ITEM_HEAD.RELATIONSHIP_TYPE%TYPE)
RETURN BOOLEAN IS

L_dummy    VARCHAR2(1);
L_function VARCHAR2(64) := 'RELATED_ITEM_SQL.CHK_REL_ITEM_CONFLICT';

cursor C_CHECK_REL_CONFLICT is
   select 'x'
     from TABLE(CAST(LP_related_items_tbl AS RELATED_ITEMS_TBL)) rit,
          related_item_head rih,
          related_item_detail ril
    where rih.relationship_id   = ril.relationship_id
      and ril.related_item      = rit.item
      and rih.item              = I_main_item
      and rih.relationship_type = I_relationship_type;
BEGIN
   if I_main_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_main_item', L_function, 'NOT NULL');
      return FALSE;
   end if;
  
   if I_relationship_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM', 'I_relationship_type', L_function, 'NOT NULL');
      return FALSE;
   end if;
  ---
  O_exists := FALSE; -- default: the Related Item does not exist for the input Main item/Relationship type combination.
  ---
  SQL_LIB.SET_MARK('OPEN','C_CHECK_REL_CONFLICT','RELATED_ITEM_HEAD',NULL);
  open C_CHECK_REL_CONFLICT;
  
  SQL_LIB.SET_MARK('FETCH','C_CHECK_REL_CONFLICT','RELATED_ITEM_HEAD',NULL);
  fetch C_CHECK_REL_CONFLICT INTO L_dummy;
  if L_dummy is NOT NULL then
     O_exists := TRUE;
  end if;
  
  SQL_LIB.SET_MARK('CLOSE','C_CHECK_REL_CONFLICT','RELATED_ITEM_HEAD',NULL);
  close C_CHECK_REL_CONFLICT;

  return TRUE;
  
EXCEPTION
   when OTHERS then
      if C_CHECK_REL_CONFLICT%ISOPEN then
         close C_CHECK_REL_CONFLICT;
      end if;
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, 'RELATED_ITEM_SQL.CHK_REL_ITEM_CONFLICT', TO_CHAR(SQLCODE));
   
  return FALSE;
END CHK_REL_ITEM_CONFLICT;
--------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION CHK_REL_ITEM_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        IN OUT BOOLEAN,
                             I_item          IN RELATED_ITEM_HEAD.ITEM%TYPE)
RETURN BOOLEAN IS
   L_dummy             VARCHAR2(1);
   L_function          VARCHAR2(60) := 'RELATED_ITEM_SQL.CHK_REL_ITEM_EXISTS';
  
   
   cursor C_CHECK_RELATED_ITEM is
     select 'x' from related_item_head 
      where item = I_item;	

BEGIN
   O_exists := FALSE;
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_RELATED_ITEM', 'RELATED_ITEM_HEAD','ITEM:'||I_item);
   open C_CHECK_RELATED_ITEM;
   
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_RELATED_ITEM','RELATED_ITEM_HEAD','ITEM:'||I_item);
   fetch C_CHECK_RELATED_ITEM into L_dummy;
   
   if L_dummy is NOT NULL then
      O_exists := TRUE;
   end if;
   
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_RELATED_ITEM','RELATED_ITEM_HEAD','ITEM:'||I_item);
   close C_CHECK_RELATED_ITEM;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_CHECK_RELATED_ITEM%ISOPEN then
         close C_CHECK_RELATED_ITEM;
      end if;
	  --  
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM , L_function , TO_CHAR(SQLCODE));
      return FALSE;   

END CHK_REL_ITEM_EXISTS;
--------------------------------------------------------------------------------------------------------------------
FUNCTION COPY_LIKE_ITEM(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_new_item            IN       ITEM_MASTER.ITEM%TYPE,
                        I_existing_item       IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   
   L_program       VARCHAR2(50)    := 'RELATED_ITEM_SQL.COPY_LIKE_ITEM';
   
   cursor C_COPY_REL_ITEM_HEAD
   is
   select rih.relationship_id,
          rih.relationship_name, 
          rih.relationship_type, 
          rih.mandatory_ind
     from related_item_head rih
    where rih.item = i_existing_item;    

BEGIN

   for rec in C_COPY_REL_ITEM_HEAD loop
      
      SQL_LIB.SET_MARK('INSERT',NULL,'RELATED_ITEM_HEAD',NULL);
      insert into related_item_head(relationship_id,
                                    item,
                                    relationship_name,
                                    relationship_type,
                                    mandatory_ind)
                            values (relationship_id_seq.nextval,
                                    I_new_item,
                                    rec.relationship_name,
                                    rec.relationship_type,
                                    rec.mandatory_ind);
      
      SQL_LIB.SET_MARK('INSERT',NULL,'RELATED_ITEM_DETAIL',NULL);                              
      insert into related_item_detail(relationship_id,
                                      related_item,
                                      priority,
                                      start_date,
                                      end_date)
                               select relationship_id_seq.currval, 
                                      rid.related_item, 
                                      rid.priority, 
                                      rid.start_date, 
                                      rid.end_date 
                                 from related_item_detail rid 
                                where rid.relationship_id = rec.relationship_id;

   end loop;                              
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;   
END COPY_LIKE_ITEM;
-----------------------------------------------------------------------------------
FUNCTION APPROVAL_CHECK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_invalids_exist IN OUT VARCHAR2,
                        I_main_item      IN ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
  L_program VARCHAR2(50) := 'RELATED_ITEM_SQL.APPROVAL_CHECK';
  L_old_count number(10);
   
   cursor C_APPROVAL_CHECK is
     select RELATED_ITEMS_REC(rit.item, 
                              rit.item_level, 
                              rit.tran_level)
       from TABLE(CAST(LP_related_items_tbl AS RELATED_ITEMS_TBL)) rit,
            item_master ri,
            item_master mi
      where rit.item  = ri.item
        and mi.item   = I_main_item
        AND (
	      ri.status = 'A' -- Either related item is already approved
	      OR 
	      (
	       -- Main-item is not approved and related item is a sibling. In which case, both will get approved together
	         mi.status <> 'A'      
	         AND cardinality(item_tbl(ri.item,NVL(ri.item_parent,'!'),NVL(ri.item_grandparent,'@')) 
	                         multiset INTERSECT 
	                         item_tbl(mi.item,NVL(mi.item_parent,'#'),NVL(mi.item_grandparent,'$'))
	                        ) > 0
	      )
	   );
   
BEGIN
  L_old_count := LP_related_items_tbl.count();
  O_invalids_exist := 'N';
  
  SQL_LIB.SET_MARK('OPEN', 'C_APPROVAL_CHECK', NULL,NULL);
  open C_APPROVAL_CHECK;
  
  SQL_LIB.SET_MARK('FETCH', 'C_APPROVAL_CHECK', NULL,NULL);
  fetch C_APPROVAL_CHECK bulk collect into LP_related_items_tbl;
  
  if LP_related_items_tbl.count() <> L_old_count then
     O_invalids_exist := 'Y';
  end if;
  
  SQL_LIB.SET_MARK('CLOSE','C_APPROVAL_CHECK', NULL,NULL);
  close C_APPROVAL_CHECK;
  
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END APPROVAL_CHECK;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_RELATED_ITEM_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_relationship_id IN       RELATED_ITEM_HEAD_TL.RELATIONSHIP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(60) := 'RELATED_ITEM_SQL.DELETE_RELATED_ITEM_HEAD_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_RELATED_ITEM_HEAD_TL is
      select 'x'
        from related_item_head_tl
       where relationship_id = I_relationship_id
         for update nowait;
BEGIN

   if I_relationship_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_relationship_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'RELATED_ITEM_HEAD_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_RELATED_ITEM_HEAD_TL',
                    L_table,
                    'I_relationship_id '|| I_relationship_id);
   open C_LOCK_RELATED_ITEM_HEAD_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_RELATED_ITEM_HEAD_TL',
                    L_table,
                    'I_relationship_id '|| I_relationship_id);
   close C_LOCK_RELATED_ITEM_HEAD_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'RELATED_ITEM_HEAD_TL',
                    'I_relationship_id '|| I_relationship_id);
                    
   delete from related_item_head_tl
     where relationship_id = I_relationship_id;
    
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_RELATED_ITEM_HEAD_TL;
------------------------------------------------------------------------------------------------
END RELATED_ITEM_SQL;
/