create or replace PACKAGE CORESVC_S9T_LIST_VALS AUTHID CURRENT_USER AS
   template_key                CONSTANT VARCHAR2(255)         :='S9T_LIST_VALS_DATA';
   action_new                           VARCHAR2(25)          := 'NEW';
   action_mod                           VARCHAR2(25)          := 'MOD';
   action_del                           VARCHAR2(25)          := 'DEL';
   S9T_LIST_VALS_sheet                  VARCHAR2(255)         := 'S9T_LIST_VALS';
   S9T_LIST_VALS$Action                 NUMBER                :=1;
   S9T_LIST_VALS$CODE                   NUMBER                :=5;
   S9T_LIST_VALS$COLUMN_NAME            NUMBER                :=4;
   S9T_LIST_VALS$SHEET_NAME             NUMBER                :=3;
   S9T_LIST_VALS$TMPL_CATEGORY      NUMBER                :=2;
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE := 'RMSADM';

   TYPE S9T_LIST_VALS_rec_tab IS TABLE OF S9T_LIST_VALS%ROWTYPE;
   -----------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_COUNT     OUT      NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
END CORESVC_S9T_LIST_VALS;
/