CREATE OR REPLACE PACKAGE CORESVC_ITEM_LOC_RANGING_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------------
-- Function Name: CREATE_ITEM_LOC
-- Purpose      : This function contains the core logic for item-loc ranging. It is called by 
--                the de-queue notification process to perform asynchronous ranging of 
--                item-locations based on the data in the staging table.
------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEM_LOC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_async_id   IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ITEM_LOC
-- Purpose      : This function contains the core logic for item-loc update. It is called by 
--                the de-queue notification process to perform asynchronous update of 
--                item-locations based on the data in the staging table.
------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_async_id   IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function : CHECK_ASYNC_JOB_EXISTS
-- Purpose  : Check for any unprocessed or in-progress Async job for a item, its parent, 
--            grandparent or children if any.  
-- Return   : O_exists as TRUE and job id if a Asyncjob exists. else O_exist is set to FALSE
------------------------------------------------------------------------------------------------
FUNCTION CHECK_ASYNC_JOB_EXISTS(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists         IN OUT   BOOLEAN,
                                O_rms_async_id   IN OUT   RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                O_status         IN OUT   RMS_ASYNC_STATUS.STATUS%TYPE,
                                I_item           IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
END CORESVC_ITEM_LOC_RANGING_SQL;
/
