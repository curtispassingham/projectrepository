CREATE OR REPLACE PACKAGE SEASON_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-- Name:    NEXT_SEASON
-- Purpose:
-- Created:       23-SEP-97 Steven B. Olson
-------------------------------------------------------------------------------
FUNCTION NEXT_SEASON( O_error_message  IN OUT  VARCHAR2,
                      O_season_id      IN OUT  SEASONS.SEASON_ID%TYPE)
                      RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_SEASON_DESC
-- Purpose:       Retrieves the description associated with the Season.
-- Created:       23-SEP-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION GET_SEASON_DESC( O_error_message  IN OUT  VARCHAR2,
                          O_season_desc    IN OUT  SEASONS.SEASON_DESC%TYPE,
                          I_season_id      IN      SEASONS.SEASON_ID%TYPE)
                          RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_PHASE_DESC
-- Purpose:       Retrieves the description associated with the given Phase.
-- Created:       23-SEP-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION GET_PHASE_DESC( O_error_message  IN OUT  VARCHAR2,
                         O_phase_desc     IN OUT  PHASES.PHASE_DESC%TYPE,
                         I_season_id      IN      PHASES.SEASON_ID%TYPE,
                         I_phase_id       IN      PHASES.PHASE_ID%TYPE)
                         RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: ASSIGN_DEFAULTS
-- Purpose:       Assigns any Seasons and Phases that have been set as defaults at the Style
--                level to a given fashion item.
-- Created:       23-SEP-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION ASSIGN_DEFAULTS( O_error_message  IN OUT  VARCHAR2,
                          I_item           IN      ITEM_SEASONS.ITEM%TYPE)
                          RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: PERFORM_INSERT_DELETE
-- Purpose:       Performs the inserts/deletes for the item_season table.
--                It is used for SKU's, Styles, and mass maintenance with
--                Item Lists.
-- Variables:     I_item_type:  The type of item passed in.  Valid values
--                              are:
--                                     F     Fashion Style.
--                                     I     Any other item.
--                                     L     Item List.
--                I_action:     The action to be performed.  Valid values
--                              are:
--                                     D     Delete.
--                                     I     Insert.
-- Created:       23-SEP-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION PERFORM_MC( O_error_message       IN OUT  VARCHAR2,
                     O_reject              IN OUT  BOOLEAN,
                     I_itemlist            IN      SKULIST_DETAIL.SKULIST%TYPE,
                     I_new_season_id       IN      PHASES.SEASON_ID%TYPE,
                     I_new_phase_id        IN      PHASES.PHASE_ID%TYPE,
                     I_existing_season_id  IN      PHASES.SEASON_ID%TYPE,
                     I_existing_phase_id   IN      PHASES.PHASE_ID%TYPE,
                     I_action              IN      VARCHAR2)
                     RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_SEASON
-- Purpose:       Deterimines if a given Item has been associated with a
--                Season/Phase.
-- Created:       23-SEP-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION CHECK_SEASON( O_error_message  IN OUT  VARCHAR2,
                       O_exists         IN OUT  BOOLEAN,
                       I_item           IN      ITEM_SEASONS.ITEM%TYPE)
                             RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: SEASON_PHASE_EXISTS
-- Purpose:       Checks if a given SEASON or PHASE ID has been assigned
--                directly to an item.
-- Variable(s):   O_exists:  True if the given SEASON/PHASE ID has been
--                           assigned to an item.
-- Created:       02-OCT-97 Arthur D. Palileo
---------------------------------------------------------------------------------------------
FUNCTION SEASON_PHASE_EXISTS( O_error_message  IN OUT  VARCHAR2,
                              O_exists         IN OUT  BOOLEAN,
                              I_season_id      IN      PHASES.SEASON_ID%TYPE,
                              I_phase_id       IN      PHASES.PHASE_ID%TYPE)
                              RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CREATE_PHASE_ITEM_RELATION
-- Purpose:       Takes the inputted values and inserts a record into item_seasons if one
--                doesn't already exist
-- Created:       15-OCT-97 Andy Beger
---------------------------------------------------------------------------------------------
FUNCTION CREATE_PHASE_ITEM_RELATION(O_error_message        IN OUT VARCHAR2,
                                    O_item_season_seq_no   IN OUT item_seasons.item_season_seq_no%TYPE,
                                    I_season_id	           IN     phases.season_id%TYPE,
                                    I_phase_id             IN     phases.phase_id%TYPE,
                                    I_item                 IN     item_master.item%TYPE)
                                    RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: COPY_ITEM_SEASONS
-- Purpose:       Copies the seasons/phases from one item to another.
-- Created:       18-OCT-97 Derek Hoffman
---------------------------------------------------------------------------------------------
FUNCTION COPY_ITEM_SEASONS(O_error_message IN OUT VARCHAR2,
                           I_to_item       IN     item_master.item%TYPE,
                           I_from_item     IN     item_master.item%TYPE)
                           RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: APPLY_ITEM_DIFF
-- Purpose:       Accepts style, color, season, phase and action type and either adds or
--                overwrites season/phase records to the item_seasons table.
-- Created:       22-JUL-99 Randip Medhi
---------------------------------------------------------------------------------------------
FUNCTION APPLY_ITEM_DIFF(O_error_message  IN OUT VARCHAR2,
                           I_item         IN ITEM_SEASONS.ITEM%TYPE,
                           I_diff_id      IN ITEM_SEASONS.DIFF_ID%TYPE,
                           I_action	      IN VARCHAR2)
                           RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: NEXT_SEQ_NO
-- Purpose:       Produces the next sequence number to be used for new records on the
--                item_seasons table.
-- Created:       22-JUL-99 Randip Medhi
---------------------------------------------------------------------------------------------
FUNCTION NEXT_SEQ_NO(O_error_message         IN OUT VARCHAR2,
                     O_next_seq_no 	     OUT    item_seasons.item_season_seq_no%TYPE,
                     I_item                  IN     item_seasons.item%TYPE)
                     RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: ITEM_SEASON_PHASE_EXISTS
-- Purpose:       Determines if the passed season/phase combination is attached to an item in
--                item_seasons table.
-- Created:       11-AUG-99 Randip Medhi
--------------------------------------------------------------------------------------------
FUNCTION ITEM_SEASON_PHASE_EXISTS(O_error_message         IN OUT VARCHAR2,
                                  O_exists                IN OUT BOOLEAN,
                                  O_season_desc           IN OUT seasons.season_desc%TYPE,
                                  O_phase_desc            IN OUT phases.phase_desc%TYPE,
                                  I_season                IN     seasons.season_id%TYPE,
                                  I_phase                 IN     phases.phase_id%TYPE,
                                  I_item                  IN     item_master.item%TYPE,
                                  I_diff_id               IN     item_seasons.diff_id%TYPE)
                                  RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_PHASE_PERIOD
-- Purpose:       Retrieves the earliest start date and latest end date among the phases of a
--                season.
-- Created:       05-MAY-2006
----------------------------------------------------------------------------------------------
FUNCTION GET_PHASE_PERIOD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_start_date      IN OUT   PHASES.START_DATE%TYPE,
                          O_end_date        IN OUT   PHASES.END_DATE%TYPE,
                          I_season_id       IN       SEASONS.SEASON_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: CHECK_SUPER_USER
-- Purpose:       Determines whether user is a super user for security filtering purposes.
----------------------------------------------------------------------------------------------
FUNCTION CHECK_SUPER_USER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_valid         IN OUT BOOLEAN)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: CHECK_SEASON_PERIOD
-- Purpose:       Determines if a given season is valid on current date
-- Created:       03-MAR-2016
----------------------------------------------------------------------------------------------
FUNCTION CHECK_SEASON_PERIOD(O_error_message  IN OUT  VARCHAR2,
                             O_exists         IN OUT  VARCHAR2,
                             I_season         IN     seasons.season_id%TYPE,
                             I_phase          IN     phases.phase_id%TYPE)
    RETURN BOOLEAN;
 
 --Function Name: CHECK_SEASON_EXPIRED
 --Purpose:        Determine a given season is expired or not 
 ---------------------------------------------------------------------------------------------
FUNCTION CHECK_SEASON_EXPIRED(O_error_message IN OUT VARCHAR2,
                              O_exists        IN OUT VARCHAR2,
                              I_season        IN     seasons.season_id%TYPE)
   RETURN BOOLEAN;
END SEASON_SQL;
/



