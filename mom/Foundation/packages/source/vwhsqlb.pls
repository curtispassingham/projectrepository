



CREATE OR REPLACE PACKAGE BODY VWH_SQL AS
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--- Package level variable
--------------------------------------------------------------------------------
LP_system_options_row  SYSTEM_OPTIONS%ROWTYPE;


--------------------------------------------------------------------------------
FUNCTION INSERT_COST_ZONES_ID (O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_zone_records  IN OUT  BOOLEAN,
                               I_pwh                IN      WH.WH%TYPE,
                               I_vwh                IN      WH.WH%TYPE,
                               I_currency_code      IN      WH.CURRENCY_CODE%TYPE)
return BOOLEAN IS

   L_program_name      VARCHAR2(50) := 'VWH_SQL.INSERT_COST_ZONES_ID';
   L_zone_exists       VARCHAR2(1)              := 'N';
   L_zone_id           COST_ZONE.ZONE_ID%TYPE   := 0;
   L_warehouse         WH.WH%TYPE               := I_vwh;
   L_zone_group_id     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_insert_pwh        WH.PHYSICAL_WH%TYPE;

   cursor C_ZONE_GROUP is
      select zone_group_id
        from cost_zone
       where zone_id = I_pwh;

   cursor C_ZONE_GROUP_CORP is
      select czg.zone_group_id, cz.zone_id
        from cost_zone_group czg, cost_zone cz
       where czg.cost_level = 'C'
         and czg.zone_group_id = cz.zone_group_id;
BEGIN

   L_insert_pwh := I_pwh;
   ---

   O_cost_zone_records := FALSE;

   for C1 in C_ZONE_GROUP LOOP

      -- Insert into cost_zone_group_loc
      -- where zone_group is not Corporate zone group
      insert into cost_zone_group_loc (zone_group_id,
                                       location,
                                       loc_type,
                                       zone_id)
                               values (C1.zone_group_id,
                                       L_warehouse,
                                       'W',
                                       L_insert_pwh);

      O_cost_zone_records := TRUE;

   END LOOP;
   ---
   insert into cost_zone_group_loc(zone_group_id,
                                   location,
                                   loc_type,
                                   zone_id)
                            select czgl.zone_group_id,
                                   L_warehouse,
                                   'W',
                                   czgl.zone_id
                              from cost_zone_group_loc czgl, cost_zone_group czg
                             where czg.cost_level    = 'Z'
                               and czgl.location     = L_insert_pwh
                               and czg.zone_group_id = czgl.zone_group_id
                               and czgl.zone_group_id not in (select zone_group_id
                                                                from cost_zone_group_loc
                                                               where zone_id = L_insert_pwh
                                                                 and location = L_warehouse);

   open C_ZONE_GROUP_CORP;
   fetch C_ZONE_GROUP_CORP into L_zone_group_id, L_zone_id;
   ---
   if C_ZONE_GROUP_CORP%FOUND then
      insert into cost_zone_group_loc(zone_group_id,
                                      location,
                                      loc_type,
                                      zone_id)
                               values(L_zone_group_id,
                                      I_vwh,
                                      'W',
                                      L_zone_id);
   end if;
   ---
   close C_ZONE_GROUP_CORP;

   return TRUE;

EXCEPTION
   when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program_name,
                                          to_char(SQLCODE));
   return FALSE;
END INSERT_COST_ZONES_ID;
--------------------------------------------------------------------------------
FUNCTION PWH_ON_OPEN_STOCK_COUNT(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_stock_count_exists  IN OUT  BOOLEAN,
                                 I_pwh                 IN      WH.WH%TYPE,
                                 I_vwh                 IN      WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'VWH_SQL.PWH_ON_OPEN_STOCK_COUNT';
   L_lockout_date   DATE         := STKCOUNT_SQL.LOCKOUT_DAYS + GET_VDATE;
   L_exists         VARCHAR2(1)  := 'N';

   cursor C_PWH_EXISTS_SC is
      select 'Y'
        from stake_location sl,
             stake_head sh,
             wh w
       where sh.stocktake_date > L_lockout_date
         and sh.cycle_count = sl.cycle_count
         and sl.location = w.wh
         and w.physical_wh = I_pwh;

BEGIN

   --- Check for required input
   if I_pwh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pwh',
                                             L_program,
                                             NULL);
         return FALSE;
   elsif I_vwh is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_vwh',
                                             L_program,
                                             NULL);
         return FALSE;
   end if;

   O_stock_count_exists := FALSE;

   open  C_PWH_EXISTS_SC;
   fetch C_PWH_EXISTS_SC into L_exists;
   close C_PWH_EXISTS_SC;

   O_stock_count_exists := (L_exists = 'Y');

   if O_stock_count_exists then

      /* the grouping commands in 2 statments below will ensure */
      /* that only 1 record is inserted into stake_location     */
      /* and only 1 record is inserted into stake_sku_loc for   */
      /* each cycle_count/item combination                      */

      insert into stake_location(cycle_count,
                                 loc_type,
                                 location)
                          select sl.cycle_count,
                                 'W',
                                 I_vwh
                            from stake_head sh,
                                 stake_location sl,
                                 wh w
                           where sh.stocktake_date > L_lockout_date
                             and sh.cycle_count = sl.cycle_count
                             and sl.location = w.wh
                             and w.physical_wh = I_pwh
                        group by sl.cycle_count;

      insert into stake_sku_loc(cycle_count,
                                loc_type,
                                location,
                                item,
                                snapshot_on_hand_qty,
                                snapshot_in_transit_qty,
                                snapshot_unit_cost,
                                snapshot_unit_retail,
                                processed,
                                physical_count_qty,
                                pack_comp_qty,
                                dept ,
                                class,
                                subclass)
                         select sl.cycle_count,
                                'W',
                                I_vwh,
                                sl.item,
                                NULL,
                                NULL,
                                NULL,
                                sl.snapshot_unit_retail,
                                'N',
                                0,
                                0,
                                sl.dept,
                                sl.class,
                                sl.subclass
                           from stake_head sh,
                                stake_sku_loc sl,
                                wh w
                          where sh.stocktake_date > L_lockout_date
                            and sh.cycle_count = sl.cycle_count
                            and sl.location = w.wh
                            and w.physical_wh = I_pwh
                       group by sl.cycle_count,
                                sl.item,
                                sl.dept,
                                sl.class,
                                sl.subclass,
                                sl.snapshot_unit_retail;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
         return FALSE;
END PWH_ON_OPEN_STOCK_COUNT;

--------------------------------------------------------------------------------
FUNCTION VWH_FORM_REC(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_channel_desc           IN OUT  CHANNELS.CHANNEL_NAME%TYPE,
                      O_tsf_entity_desc        IN OUT  TSF_ENTITY.TSF_ENTITY_DESC%TYPE,
                      O_wh_name_tl             IN OUT  WH.WH_NAME%TYPE,
                      O_pricing_location       IN OUT  WH_ADD.PRICING_LOCATION%TYPE,
                      O_pricing_location_name  IN OUT  WH.WH_NAME%TYPE,
                      O_pricing_location_curr  IN OUT  WH.CURRENCY_CODE%TYPE,
                      I_channel_id             IN      CHANNELS.CHANNEL_ID%TYPE,
                      I_tsf_entity_id          IN      TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                      I_wh_name                IN      WH.WH_NAME%TYPE,
                      I_vwh                    IN      WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(61) := 'VWH_SQL.VWH_FORM_REC';
   L_invalid_param   VARCHAR2(20) := NULL;
   L_exists          BOOLEAN;
   L_tsf_entity_row  TSF_ENTITY%ROWTYPE;
   L_wh_row          WH%ROWTYPE;
   L_store_row       STORE%ROWTYPE;

   cursor C_GET_PRICING_LOCATION is
      select pricing_location
        from wh_add
       where wh = I_vwh;

BEGIN

   --- Check required input
   if I_vwh is NULL then
      L_invalid_param := 'I_vwh';
   elsif I_wh_name is NULL then
      L_invalid_param := 'I_wh_name';
   elsif I_channel_id is NULL then
      L_invalid_param := 'I_channel_id';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
         return FALSE;
   end if;


   --- Initialize all in/out parameters
   O_channel_desc           := NULL;
   O_tsf_entity_desc        := NULL;
   O_wh_name_tl             := NULL;
   O_pricing_location       := NULL;
   O_pricing_location_name  := NULL;
   O_pricing_location_curr  := NULL;


   --- Transfer entity desc
   if TSF_ENTITY_SQL.GET_ROW(O_error_message,
                             L_exists,
                             L_tsf_entity_row,
                             I_tsf_entity_id) = FALSE then
      return FALSE;
   end if;
   ---
   O_tsf_entity_desc := L_tsf_entity_row.tsf_entity_desc;

   --- Channel name
   if CHANNEL_SQL.GET_CHANNEL_NAME (O_error_message,
                                    O_channel_desc,
                                    I_channel_id) = FALSE then
      return FALSE;
   end if;

   --- Pricing location
   open C_GET_PRICING_LOCATION;
   fetch C_GET_PRICING_LOCATION into O_pricing_location;
   close C_GET_PRICING_LOCATION;

   --- If we have a pricing location then get the name and currency
   if O_pricing_location is NOT NULL then
      --- We don't know if the pricing location is a store or wh, so we need to try both
      if WH_ATTRIB_SQL.GET_ROW(O_error_message,
                               L_exists,
                               L_wh_row,
                               O_pricing_location) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists then
         O_pricing_location_name := L_wh_row.wh_name;
         O_pricing_location_curr := L_wh_row.currency_code;
      else
         --- If it isn't a wh then it must be a store
         if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                     L_exists,
                                     L_store_row,
                                     O_pricing_location) = FALSE then
            return FALSE;
         end if;
         ---
         O_pricing_location_name := L_store_row.store_name;
         O_pricing_location_curr := L_store_row.currency_code;
      end if;
   end if;

   O_wh_name_tl:=I_wh_name;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
         return FALSE;
END VWH_FORM_REC;

--------------------------------------------------------------------------------
FUNCTION GET_PRICING_LOC_INFO(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid             IN OUT  BOOLEAN,
                              O_pricing_loc_name  IN OUT  WH.WH_NAME%TYPE,
                              O_pricing_loc_curr  IN OUT  WH.CURRENCY_CODE%TYPE,
                              I_pricing_loc       IN OUT  WH.WH%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(61) := 'VWH_SQL.GET_PRICING_LOC_INFO';
   L_exists            BOOLEAN;
   L_wh_row            WH%ROWTYPE;
   L_store_row         STORE%ROWTYPE;
   L_name              WH.WH_NAME%TYPE;
   L_currency_code     WH.CURRENCY_CODE%TYPE;
   L_stockholding_ind  VARCHAR2(1);

BEGIN

   --- Check required input
   if I_pricing_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pricing_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --- Initialize in/out parameters
   O_valid             := TRUE;
   O_pricing_loc_name  := NULL;
   O_pricing_loc_curr  := NULL;

   --- We don't know if the location is a store or wh, so we need to try both
   if WH_ATTRIB_SQL.GET_ROW(O_error_message,
                            L_exists,
                            L_wh_row,
                            I_pricing_loc) = FALSE then
      return FALSE;
   end if;

   if L_exists then

      L_name             := L_wh_row.wh_name;
      L_stockholding_ind := L_wh_row.stockholding_ind;
      L_currency_code    := L_wh_row.currency_code;

   else -- L_exists = FALSE then

      --- If it isn't a wh then check for a store
      if STORE_ATTRIB_SQL.GET_ROW(O_error_message,
                                  L_exists,
                                  L_store_row,
                                  I_pricing_loc) = FALSE then
         return FALSE;
      end if;

      L_name             := L_store_row.store_name;
      L_stockholding_ind := L_store_row.stockholding_ind;
      L_currency_code    := L_store_row.currency_code;

   end if;

   --- If it isn't a wh or a store, or it isn't stockholding, then it is invalid
   if L_exists = FALSE
   or L_stockholding_ind = 'N' THEN
      O_valid := FALSE;
   else
      --- Pricing loc is valid so set the name and currency code
      O_pricing_loc_name := L_name;
      O_pricing_loc_curr := L_currency_code;
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRICING_LOC_INFO;

--------------------------------------------------------------------------------
FUNCTION NEW_VWH_INSERTS(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_cost_zone_records   IN OUT  BOOLEAN,
                         O_stock_count_exists  IN OUT  BOOLEAN,
                         I_pwh                 IN      WH.WH%TYPE,
                         I_new_vwh_table       IN      VWH_SQL.NEW_VWH_TABLE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'VWH_SQL.NEW_VWH_INSERTS';
   L_invalid_param  VARCHAR2(20) := NULL;
   L_pwh            WH.WH%TYPE   := I_pwh;
   L_vwh_rec        VWH_SQL.NEW_VWH_REC;
   L_index          BINARY_INTEGER;
   
   L_rms_async_id STORE_ADD.RMS_ASYNC_ID%TYPE;

BEGIN

   --- Check for required input
   if I_pwh is NULL then
      L_invalid_param := 'I_pwh';
   elsif I_new_vwh_table is NULL then
      L_invalid_param := 'I_new_vwh_table';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
         return FALSE;
   end if;

   --- Get the system options
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;

   L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
   L_index := I_new_vwh_table.FIRST;
   while L_index is NOT NULL loop

      L_vwh_rec := I_new_vwh_table(L_index);

      if LP_system_options_row.elc_ind = 'Y' then
         if VWH_SQL.INSERT_COST_ZONES_ID(O_error_message,
                                         O_cost_zone_records,
                                         L_pwh,
                                         L_vwh_rec.vwh,
                                         L_vwh_rec.currency_code) = FALSE then
            return FALSE;
         end if;
      end if;

      if VWH_SQL.PWH_ON_OPEN_STOCK_COUNT(O_error_message,
                                         O_stock_count_exists,
                                         L_pwh,
                                         L_vwh_rec.vwh) = FALSE then
         return FALSE;
      end if;
      
      if L_vwh_rec.pricing_location is NOT NULL then
         insert into wh_add(wh,
                            wh_currency,
                            pricing_location,
                            pricing_loc_curr,
                            rms_async_id)
                     values(L_vwh_rec.vwh,
                            L_vwh_rec.currency_code,
                            L_vwh_rec.pricing_location,
                            L_vwh_rec.pricing_loc_curr,
                            L_rms_async_id);
                            
      end if;

      L_index := I_new_vwh_table.NEXT(L_index);
   end LOOP;

  IF RMS_ASYNC_PROCESS_SQL.ENQUEUE_WH_ADD(O_error_message,L_rms_async_id)=FALSE THEN
    RETURN false;
  END IF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
         return FALSE;
END NEW_VWH_INSERTS;
--------------------------------------------------------------------------------
FUNCTION UPDATE_WH_ADD(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_vwh_table           IN      VWH_SQL.NEW_VWH_TABLE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'VWH_SQL.UPDATE_WH_ADD';
   L_invalid_param  VARCHAR2(20) := NULL;   
   L_vwh_rec        VWH_SQL.NEW_VWH_REC;
   L_index          BINARY_INTEGER;

BEGIN

   --- Check for required input
   if I_vwh_table is NULL then
      L_invalid_param := 'I_vwh_table';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
         return FALSE;
   end if;

   L_index := I_vwh_table.FIRST;
   while L_index is NOT NULL loop

      L_vwh_rec := I_vwh_table(L_index);
      
      update wh_add
         set pricing_location = L_vwh_rec.pricing_location,
             pricing_loc_curr = L_vwh_rec.pricing_loc_curr
       where wh = L_vwh_rec.vwh;            

      L_index := I_vwh_table.NEXT(L_index);
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
         return FALSE;
END UPDATE_WH_ADD;
--------------------------------------------------------------------------------

END VWH_SQL;
/
