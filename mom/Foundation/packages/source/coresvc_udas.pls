CREATE OR REPLACE PACKAGE CORESVC_UDA AUTHID CURRENT_USER AS
   template_key                      CONSTANT VARCHAR2(255):='UDA_DATA';
   action_new                        VARCHAR2(25)          :='NEW';
   action_mod                        VARCHAR2(25)          :='MOD';
   action_del                        VARCHAR2(25)          :='DEL';
   UDA_sheet                         VARCHAR2(255)         :='UDA';
   UDA$Action                        NUMBER                :=1;
   UDA$UDA_ID                        NUMBER                :=2;
   UDA$UDA_DESC                      NUMBER                :=3;
   UDA$DISPLAY_TYPE                  NUMBER                :=5;
   UDA$DATA_TYPE                     NUMBER                :=6;
   UDA$DATA_LENGTH                   NUMBER                :=7;
   UDA$SINGLE_VALUE_IND              NUMBER                :=8;
   UDA$FILTER_ORG_ID                 NUMBER                :=9;
   UDA$FILTER_MERCH_ID               NUMBER                :=10;
   UDA$FILTER_MERCH_ID_CLASS         NUMBER                :=11;
   UDA$FILTER_MERCH_ID_SUBCLASS      NUMBER                :=12;
                                     
   UDA_VALUES_sheet                  VARCHAR2(255)         :='UDV';
   UDA_VALUES$Action                 NUMBER                :=1;
   UDA_VALUES$UDA_ID                 NUMBER                :=2;
   UDA_VALUES$UDA_VALUE              NUMBER                :=3;    
   UDA_VALUES$UDA_VALUE_DESC         NUMBER                :=4; 
                                     
   UDA_TL_sheet                      VARCHAR2(255)         :='UDA_TL';
   UDA_TL$Action                     NUMBER                :=1;
   UDA_TL$LANG                       NUMBER                :=2;
   UDA_TL$UDA_ID                     NUMBER                :=3;
   UDA_TL$UDA_DESC                   NUMBER                :=4;
   
   UDA_VALUES_TL_sheet               VARCHAR2(255)         :='UDV_TL';
   UDA_VALUES_TL$Action              NUMBER                :=1;
   UDA_VALUES_TL$LANG                NUMBER                :=2;
   UDA_VALUES_TL$UDA_ID              NUMBER                :=3;
   UDA_VALUES_TL$UDA_VALUE           NUMBER                :=4;    
   UDA_VALUES_TL$UDA_VALUE_DESC      NUMBER                :=5; 
   
   UID_sheet           VARCHAR2(255)         :='UID';
   UID$Action          NUMBER                :=1;
   UID$UDA_ID          NUMBER                :=2;
   UID$UDA_VALUE_UPD   NUMBER                :=3;
   UID$DEPT            NUMBER                :=4;
   UID$CLASS           NUMBER                :=5;
   UID$SUBCLASS        NUMBER                :=6;
   UID$UDA_VALUE       NUMBER                :=7;
   UID$REQUIRED_IND    NUMBER                :=8;
   
   
   sheet_name_trans               S9T_PKG.trans_map_typ;
   action_column                  VARCHAR2(255)         :='ACTION';
   template_category              CODE_DETAIL.CODE%TYPE :='RMSITM';
   TYPE UDA_rec_tab IS TABLE OF UDA%ROWTYPE;
   TYPE UDA_VALUES_rec_tab IS TABLE OF UDA_VALUES%ROWTYPE;
-------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,   
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,   
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
-------------------------------------------------------------------------------
END CORESVC_UDA;
/