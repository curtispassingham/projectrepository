-- File Name : CORESVC_PO_TYPE_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_PO_TYPE AUTHID CURRENT_USER AS
   template_key           		CONSTANT VARCHAR2(255):='PO_TYPE_DATA';
   action_new             		VARCHAR2(25)          := 'NEW';
   action_mod             		VARCHAR2(25)          := 'MOD';
   action_del             		VARCHAR2(25)          := 'DEL';
   PO_TYPE_TL_sheet             VARCHAR2(255)         := 'PO_TYPE_TL';
   PO_TYPE_TL$Action           	NUMBER                :=1;
   PO_TYPE_TL$PO_TYPE_DESC      NUMBER                :=4;
   PO_TYPE_TL$PO_TYPE           NUMBER                :=3;
   PO_TYPE_TL$LANG            	NUMBER                :=2;
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    			VARCHAR2(255) := 'ACTION';
   template_category 			CODE_DETAIL.CODE%TYPE := 'RMSFND';
   TYPE PO_TYPE_TL_rec_tab IS TABLE OF PO_TYPE_TL%ROWTYPE;
   PO_TYPE_sheet             	VARCHAR2(255)         := 'PO_TYPE';
   PO_TYPE$Action           	NUMBER                :=1;
   PO_TYPE$PO_TYPE_DESC         NUMBER                :=3;
   PO_TYPE$PO_TYPE            	NUMBER                :=2;
   TYPE PO_TYPE_rec_tab IS TABLE OF PO_TYPE%ROWTYPE;
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   --------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   --------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
					O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER
                    )
   RETURN BOOLEAN;
   --------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
   -------------------------------------------------------------------------
END CORESVC_PO_TYPE;
/
