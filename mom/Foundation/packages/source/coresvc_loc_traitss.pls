-- File Name : coresvc_loc_traits_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_LOC_TRAITS AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------
   template_key                   CONSTANT VARCHAR2(255)         := 'LOC_TRAIT_DATA';
   template_category              CONSTANT VARCHAR2(255)         := 'RMSFND';
   action_new                              VARCHAR2(25)          := 'NEW';
   action_mod                              VARCHAR2(25)          := 'MOD';
   action_del                              VARCHAR2(25)          := 'DEL';
   LOC_TRAITS_sheet                        VARCHAR2(255)         := 'LOC_TRAITS';
   LOC_TRAITS$Action                       NUMBER                :=1;
   LOC_TRAITS$DESCRIPTION                  NUMBER                :=4;
   LOC_TRAITS$FILTER_ORG_ID                NUMBER                :=3;
   LOC_TRAITS$LOC_TRAIT                    NUMBER                :=2;
   
   LOC_TRAITS_TL_sheet                     VARCHAR2(255)         := 'LOC_TRAITS_TL';
   LOC_TRAITS_TL$Action                    NUMBER                :=1;
   LOC_TRAITS_TL$LANG                      NUMBER                :=2;
   LOC_TRAITS_TL$LOC_TRAIT                 NUMBER                :=3;
   LOC_TRAITS_TL$DESCRIPTION               NUMBER                :=4;
------------------------------------------------------------------------------------
TYPE LOC_TRAITS_REC_TAB IS TABLE OF LOC_TRAITS%ROWTYPE;
   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column    VARCHAR2(255) := 'ACTION';
------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN CHAR DEFAULT 'N')
   RETURN BOOLEAN;
-----------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN  OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count       OUT NUMBER,
                        I_file_id        IN      S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id     IN      NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   OUT NUMBER,
                    I_process_id    IN NUMBER,
                    I_chunk_id      IN NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)   --Changes
   RETURN VARCHAR2;
-----------------------------------------------------------------------
END CORESVC_LOC_TRAITS;
/
