CREATE OR REPLACE PACKAGE ITEM_VALIDATE_SQL AUTHID CURRENT_USER AS

-----------------------------------------------------------------------------------
--- Function Name:  EXIST 
--- Purpose:        Checks for the existence of an item.
--- Calls:          <none>
--- Created:        06-AUG-96 by Chad Whipple
-----------------------------------------------------------------------------------
   FUNCTION EXIST(O_error_message  IN OUT  VARCHAR2,
                  O_exist          IN OUT  BOOLEAN,
                  I_item           IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------------
--- Function Name:  CHECK_PARENT_CHILD
--- Purpose:        Checks the relationship between two given items.
--- Calls:          <none>
-----------------------------------------------------------------------------------
   FUNCTION CHECK_PARENT_CHILD(O_error_message  IN OUT  VARCHAR2,
                               O_parent         IN OUT  BOOLEAN,
                               O_grandparent    IN OUT  BOOLEAN,
                               I_ancestor       IN      ITEM_MASTER.ITEM%TYPE,
                               I_item           IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------------
--- Function Name:  CHILD_GCHILD_EXIST
--- Purpose:        Checks if any items exist with the entered item as a parent 
---                 or grandparent.
--- Calls:          <none>
-----------------------------------------------------------------------------------
   FUNCTION CHILD_GCHILD_EXIST(O_error_message  IN OUT  VARCHAR2,
                               O_child          IN OUT  BOOLEAN,
                               O_gchild         IN OUT  BOOLEAN,
                               I_item           IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------------
--- Function Name:  HIST_EXIST
--- Purpose:        Checks if the entered item has any records on the 
---                 item-location-history table.
--- Calls:          <none>
-----------------------------------------------------------------------------------
   FUNCTION HIST_EXIST(O_error_message  IN OUT  VARCHAR2,
                       O_exist          IN OUT  BOOLEAN,
                       I_item           IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------------
--- Function Name:  VPN_GET_ITEM
--- Purpose:        Takes a VPN, validates its existence on the 
---                 item-supplier table, and returns the associated item.
--- Calls:          <none>
--- Created:        28 July 97 by Jason Barber		
-----------------------------------------------------------------------------------
   FUNCTION VPN_GET_ITEM(O_error_message IN OUT  VARCHAR2,
                         O_item          IN OUT  ITEM_MASTER.ITEM%TYPE,
                         O_vpn_desc      IN OUT  ITEM_MASTER.DESC_UP%TYPE,
                         O_multiple      IN OUT  BOOLEAN,
                         O_exist         IN OUT  BOOLEAN,
                         I_vpn           IN      ITEM_SUPPLIER.VPN%TYPE) 
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
--- Function Name:  VPN_GET_ITEM_DESC
--- Purpose:        Takes a VPN and supplier, validates its existence on the 
---                 item-supplier table, and returns the associated item.		
-----------------------------------------------------------------------------------
   FUNCTION VPN_GET_ITEM_DESC(O_error_message IN OUT  VARCHAR2,
                              O_item          IN OUT  ITEM_MASTER.ITEM%TYPE,
                              O_vpn_desc      IN OUT  ITEM_MASTER.DESC_UP%TYPE,
                              O_exist         IN OUT  BOOLEAN,
                              I_vpn           IN      ITEM_SUPPLIER.VPN%TYPE,
                              I_supplier      IN      ITEM_SUPPLIER.SUPPLIER%TYPE) 
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
--- Function: VALID_CHILDREN_EXIST
--- Purpose:  Checks the existence of children of a particular item that are not
---           in deleted status.
--- Calls:    <none>
----------------------------------------------------------------------------------
FUNCTION VALID_CHILDREN_EXIST(O_error_message  IN OUT  VARCHAR2,
                              O_exist          IN OUT  BOOLEAN,
                              I_item           IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------
--- Function: CHILD_DIFF_EXISTS
--- Purpose:  Checks the existence of duplicate children of an item based on diffs.
--- Calls:    <none>
------------------------------------------------------------------------------------
FUNCTION CHILD_DIFF_EXISTS (O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT  BOOLEAN,
                            I_diff_1         IN     ITEM_MASTER.DIFF_1%TYPE,
                            I_diff_2         IN     ITEM_MASTER.DIFF_2%TYPE,
                            I_diff_3         IN     ITEM_MASTER.DIFF_3%TYPE,
                            I_diff_4         IN     ITEM_MASTER.DIFF_4%TYPE,
                            I_item           IN     ITEM_MASTER.ITEM%TYPE,
                            I_diff_cnt       IN     NUMBER)
RETURN BOOLEAN;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Function Name: CHECK_STATUS
-- Purpose      : Passes out flag to indicate if any level 2 items exist in worksheet status 
--                that match search criteria for cost changes, or if any exist in worksheet 
--                or submitted status for price changes. 
----------------------------------------------------------------------------------------------
FUNCTION CHECK_STATUS(O_error_message IN OUT VARCHAR2,
                      O_exists        IN OUT BOOLEAN,
                      I_change_type   IN     VARCHAR2,
                      I_supplier      IN     SUPS.SUPPLIER%TYPE,
                      I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                      I_diff_group_id IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                      I_diff_id       IN     DIFF_GROUP_DETAIL.DIFF_ID%TYPE)
RETURN BOOLEAN;   
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_RPM_ZONE
-- Purpose      : Checks the existence of zone_id and zone_desc in the GTT_ZONE_INFO table 
--                given a RPM display zone id. 
----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RPM_ZONE (O_error_message         IN OUT  VARCHAR2,
                            O_zone_id               IN OUT  PM_RETAIL_API_SQL.ZONE_ID%TYPE,
                            O_zone_desc             IN OUT  PM_RETAIL_API_SQL.ZONE_NAME%TYPE,
                            O_currency_code         IN OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                            O_exists                IN OUT  VARCHAR2,
                            I_rpm_display_zone_id   IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
-- Function Name: CHECK_CHILD_STATUS
-- Purpose      : Checks if an item has a child or grandchild in worksheet status.
----------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_STATUS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists             IN OUT   BOOLEAN,
                            I_item               IN       ITEM_MASTER.ITEM%TYPE,
                            I_chk_grndchld_ind   IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: CHECK_CHILD_SUBMIT_STATUS
-- Purpose      : Checks if an item has a child or grandchild in submitted status.
----------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_SUBMIT_STATUS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_exists             IN OUT   BOOLEAN,
                                   I_item               IN       ITEM_MASTER.ITEM%TYPE,
                                   I_chk_grndchld_ind   IN       VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
--- Function: ITEMLOC_FILTER_LIST
---  Purpose: Check whether the total number of records on the item_loc table for a
---           specific item is different from the one where the locations are from
---           v_store, v_wh, v_internal_finisher and v_external_finisher views for
---           the same item on item_loc.
----------------------------------------------------------------------------------------------
FUNCTION ITEMLOC_FILTER_LIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_diff_ind        IN OUT VARCHAR2,
                             I_item            IN     ITEM_LOC.ITEM%TYPE,
                             I_group_type      IN     CODE_DETAIL.CODE%TYPE,
                             I_group_id        IN     PARTNER.PARTNER_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END ITEM_VALIDATE_SQL;
/

