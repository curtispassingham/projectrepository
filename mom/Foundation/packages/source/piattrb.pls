CREATE OR REPLACE PACKAGE BODY PACKITEM_ATTRIB_SQL AS
--------------------------------------------------------------------
FUNCTION GET_ORDER_COMP_COST(O_error_message     IN OUT VARCHAR2,
                             I_pack_no           IN     PACKITEM.PACK_NO%TYPE,
                             I_order_cost        IN     NUMBER,
                             I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                             I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                             I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_location          IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                             O_order_cost        IN OUT NUMBER)
   RETURN BOOLEAN IS
   ---
   L_unit_cost  PRICE_HIST.UNIT_COST%TYPE    := 0;
   L_comp_cost  PRICE_HIST.UNIT_COST%TYPE    := 0;
   L_unit_qty   V_PACKSKU_QTY.QTY%TYPE       := 0;
   L_comp_qty   V_PACKSKU_QTY.QTY%TYPE       := 0;
   L_program    VARCHAR2(64)                 := 'PACKITEM_ATTRIB_SQL.GET_ORDER_COMP_COST';
   ---
   cursor C_GET_SUP_COST is
      select unit_cost,
             qty
        from item_supp_country its,
             v_packsku_qty vpq
       where origin_country_id = I_origin_country_id
         and supplier          = I_supplier
         and its.item          = I_item
         and its.item          = vpq.item;
   ---
   cursor C_GET_SUM_COMP_COST is
      select sum(unit_cost * vpq.qty),
             sum(vpq.qty)
        from item_supp_country its,
             v_packsku_qty vpq
       where its.origin_country_id = I_origin_country_id
         and its.supplier          = I_supplier
         and its.item              = vpq.item
         and vpq.pack_no           = I_pack_no;

   cursor C_GET_SUP_COST_LOC is
      select unit_cost,
             qty
        from item_supp_country_loc i,
             v_packsku_qty v
       where origin_country_id = I_origin_country_id
         and supplier          = I_supplier
         and i.item            = I_item
         and loc               = I_location
         and i.item            = v.item;
   ---
   cursor C_GET_SUM_COMP_COST_LOC is
      select SUM(i.unit_cost * v.qty),
             SUM(v.qty)
        from item_supp_country_loc i,
             v_packsku_qty v
       where i.origin_country_id = I_origin_country_id
         and i.supplier          = I_supplier
         and i.loc               = I_location
         and i.item              = v.item
         and v.pack_no           = I_pack_no;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_order_cost is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ORDER_COST',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_SUPPLIER',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_origin_country_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      -- fetch the unit cost for the item from item_supp_country
      SQL_LIB.SET_MARK('OPEN','C_GET_SUP_COST','ITEM_SUPP_COUNTRY',
                       'SUPPLIER: '||to_char(I_supplier)||' ITEM: '||I_item||' COUNTRY: '||I_origin_country_id);
      open C_GET_SUP_COST;
      SQL_LIB.SET_MARK('FETCH','C_GET_SUP_COST','ITEM_SUPP_COUNTRY',
                       'SUPPLIER: '||to_char(I_supplier)||' ITEM: '||I_item||' COUNTRY: '||I_origin_country_id);
      fetch C_GET_SUP_COST into L_unit_cost, L_unit_qty;
      if C_GET_SUP_COST%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_SUP_COST','ITEM_SUPP_COUNTRY',
                          'SUPPLIER: '||to_char(I_supplier)||' ITEM: '||I_item||' COUNTRY: '||I_origin_country_id);
         close C_GET_SUP_COST;
         O_error_message := SQL_LIB.CREATE_MSG('COST_NOT_FOUND', I_item, to_char(I_supplier),NULL);
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SUP_COST','ITEM_SUPP_COUNTRY',
                       'SUPPLIER: '||to_char(I_supplier)||' ITEM: '||I_item||' COUNTRY: '||I_origin_country_id);
      close C_GET_SUP_COST;
      ---
      -- retreive the sum of the component costs
      SQL_LIB.SET_MARK('OPEN','C_GET_SUM_COMP_COST','ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                       'SUPPLIER: '||to_char(I_supplier)||', PACK NO: '||I_pack_no||', COUNTRY: '||I_origin_country_id);
      open C_GET_SUM_COMP_COST;
      SQL_LIB.SET_MARK('FETCH','C_GET_SUM_COMP_COST','ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                       'SUPPLIER: '||to_char(I_supplier)||', PACK NO: '||I_pack_no||', COUNTRY: '||I_origin_country_id);
      fetch C_GET_SUM_COMP_COST into L_comp_cost, L_comp_qty;
      if C_GET_SUM_COMP_COST%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_SUM_COMP_COST','ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                       'SUPPLIER: '||to_char(I_supplier)||', PACK NO: '||I_pack_no||', COUNTRY: '||I_origin_country_id);
         close C_GET_SUM_COMP_COST;
         O_error_message := SQL_LIB.CREATE_MSG('NO_PACKITEM_COST_FOUND',
                                               I_pack_no,
                                               to_char(I_supplier),
                                               I_origin_country_id);
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SUM_COMP_COST','ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                       'SUPPLIER: '||to_char(I_supplier)||', PACK NO: '||I_pack_no||', COUNTRY: '||I_origin_country_id);
      close C_GET_SUM_COMP_COST;
   else
      -- fetch the unit cost for the item from item_supp_country
      SQL_LIB.SET_MARK('OPEN','C_GET_SUP_COST_LOC','ITEM_SUPP_COUNTRY_LOC',
                       'SUPPLIER: '||to_char(I_supplier)||' ITEM: '||I_item||' COUNTRY: '||I_origin_country_id);
      open C_GET_SUP_COST_LOC;
      SQL_LIB.SET_MARK('FETCH','C_GET_SUP_COST_LOC','ITEM_SUPP_COUNTRY_LOC',
                       'SUPPLIER: '||to_char(I_supplier)||' ITEM: '||I_item||' COUNTRY: '||I_origin_country_id);
      fetch C_GET_SUP_COST_LOC into L_unit_cost, L_unit_qty;
      if C_GET_SUP_COST_LOC%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_SUP_COST_LOC','ITEM_SUPP_COUNTRY_LOC',
                          'SUPPLIER: '||to_char(I_supplier)||' ITEM: '||I_item||' COUNTRY: '||I_origin_country_id);
         close C_GET_SUP_COST_LOC;
         O_error_message := SQL_LIB.CREATE_MSG('COST_NOT_FOUND', I_item, to_char(I_supplier),NULL);
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SUP_COST_LOC','ITEM_SUPP_COUNTRY_LOC',
                       'SUPPLIER: '||to_char(I_supplier)||' ITEM: '||I_item||' COUNTRY: '||I_origin_country_id);
      close C_GET_SUP_COST_LOC;
      ---
      -- retreive the sum of the component costs
      SQL_LIB.SET_MARK('OPEN','C_GET_SUM_COMP_COST_LOC','ITEM_SUPP_COUNTRY_LOC, V_PACKSKU_QTY',
                       'SUPPLIER: '||to_char(I_supplier)||', PACK NO: '||I_pack_no||', COUNTRY: '||I_origin_country_id);
      open C_GET_SUM_COMP_COST_LOC;
      SQL_LIB.SET_MARK('FETCH','C_GET_SUM_COMP_COST_LOC','ITEM_SUPP_COUNTRY_LOC, V_PACKSKU_QTY',
                       'SUPPLIER: '||to_char(I_supplier)||', PACK NO: '||I_pack_no||', COUNTRY: '||I_origin_country_id);
      fetch C_GET_SUM_COMP_COST_LOC into L_comp_cost, L_comp_qty;
      if C_GET_SUM_COMP_COST_LOC%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_SUM_COMP_COST_LOC','ITEM_SUPP_COUNTRY_LOC, V_PACKSKU_QTY',
                       'SUPPLIER: '||to_char(I_supplier)||', PACK NO: '||I_pack_no||', COUNTRY: '||I_origin_country_id);
         close C_GET_SUM_COMP_COST_LOC;
         O_error_message := SQL_LIB.CREATE_MSG('NO_PACKITEM_COST_FOUND',
                                               I_pack_no,
                                               to_char(I_supplier),
                                               I_origin_country_id);
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SUM_COMP_COST_LOC','ITEM_SUPP_COUNTRY_LOC, V_PACKSKU_QTY',
                       'SUPPLIER: '||to_char(I_supplier)||', PACK NO: '||I_pack_no||', COUNTRY: '||I_origin_country_id);
      close C_GET_SUM_COMP_COST_LOC;
   end if;
   -- find the order cost for the component item
   if L_comp_cost = 0 then
      O_order_cost := ((L_unit_qty * I_order_cost) / L_comp_qty);
   else
      O_order_cost := ((L_unit_cost * I_order_cost) / L_comp_cost);
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END GET_ORDER_COMP_COST;
--------------------------------------------------------------------
FUNCTION GET_ORDER_COMP_COST(O_error_message     IN OUT VARCHAR2,
                             I_pack_no           IN     PACKITEM.PACK_NO%TYPE,
                             I_order_cost        IN     NUMBER,
                             I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                             I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                             I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             O_order_cost        IN OUT NUMBER)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)                 := 'PACKITEM_ATTRIB_SQL.GET_ORDER_COMP_COST';

BEGIN
   if GET_ORDER_COMP_COST(O_error_message,
                          I_pack_no,
                          I_order_cost,
                          I_item,
                          I_supplier,
                          I_origin_country_id,
                          NULL,
                          O_order_cost) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END GET_ORDER_COMP_COST;
-----------------------------------------------------------------------
FUNCTION NEXT_PREV_PACKITEM(O_error_message  IN OUT VARCHAR2,
                            O_item           IN OUT ITEM_MASTER.ITEM%TYPE,
                            I_pack_no        IN     PACKITEM.PACK_NO%TYPE,
                            I_old_item       IN     ITEM_MASTER.ITEM%TYPE,
                            I_which_item     IN     VARCHAR2)
   RETURN BOOLEAN IS
   L_program       VARCHAR2(50) := 'PACKITEM_ATTRIB_SQL.NEXT_PREV_PACKITEM';
   L_combo_exists  NUMBER(1);

   cursor C_GET_FIRST is
      select item
        from v_packsku_qty
       where pack_no = I_pack_no
       order by item;

   cursor C_GET_NEXT is
      select item
        from v_packsku_qty
       where pack_no = I_pack_no
         and item    > I_old_item
       order by item;

   cursor C_GET_PREV is
      select item
        from v_packsku_qty
       where pack_no = I_pack_no
         and item    < I_old_item
       order by item desc;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_which_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_WHICH_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_which_item = 'FIRST' then
      SQL_LIB.SET_MARK('OPEN','C_GET_FIRST','V_PACKSKU_QTY',
                       'PACK_NO: ' || I_pack_no);
      open C_GET_FIRST;
      SQL_LIB.SET_MARK('FETCH','C_GET_FIRST','V_PACKSKU_QTY',
                       'PACK_NO: ' || I_pack_no);
      fetch C_GET_FIRST into O_item;
      if C_GET_FIRST%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_FIRST','V_PACKSKU_QTY',
                          'PACK_NO: ' || I_pack_no);
         close C_GET_FIRST;
         O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_PACKS',NULL,NULL,NULL);
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_FIRST','V_PACKSKU_QTY',
                       'PACK_NO: ' || I_pack_no);
      close C_GET_FIRST;
   elsif I_which_item = 'NEXT' then
      if I_old_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_OLD_ITEM',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_NEXT','V_PACKSKU_QTY',
                       'PACK_NO: ' || I_pack_no || ', OLD_ITEM: ' || I_old_item);
      open C_GET_NEXT;
      SQL_LIB.SET_MARK('FETCH','C_GET_NEXT','V_PACKSKU_QTY',
                       'PACK_NO: ' || I_pack_no || ', OLD_ITEM: ' || I_old_item);
      fetch C_GET_NEXT into O_item;
      if C_GET_NEXT%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT','V_PACKSKU_QTY',
                          'PACK_NO: ' || I_pack_no || ', OLD_ITEM: ' || I_old_item);
         close C_GET_NEXT;
         O_error_message := SQL_LIB.CREATE_MSG('LAST_REC_SKU',NULL,NULL,NULL);
         return FALSE;
      else
         SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT','V_PACKSKU_QTY',
                          'PACK_NO: ' || I_pack_no || ', OLD_ITEM: ' || I_old_item);
         close C_GET_NEXT;
      end if;
   elsif I_which_item = 'PREV' then
      if I_old_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'I_OLD_ITEM',
                                               'NULL',
                                               'NOT NULL');
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_PREV','V_PACKSKU_QTY',
                       'PACK_NO: ' || I_pack_no || ', OLD_ITEM: ' || I_old_item);
      open C_GET_PREV;
      SQL_LIB.SET_MARK('FETCH','C_GET_PREV','V_PACKSKU_QTY',
                       'PACK_NO: ' || I_pack_no || ', OLD_ITEM: ' || I_old_item);
      fetch C_GET_PREV into O_item;
      if C_GET_PREV%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_PREV','V_PACKSKU_QTY',
                          'PACK_NO: ' || I_pack_no || ', OLD_ITEM: ' || I_old_item);
         close C_GET_PREV;
         O_error_message := SQL_LIB.CREATE_MSG('FIRST_REC_SKU',NULL,NULL,NULL);
         return FALSE;
      else
         SQL_LIB.SET_MARK('CLOSE','C_GET_PREV','V_PACKSKU_QTY',
                          'PACK_NO: ' || I_pack_no || ', OLD_ITEM: ' || I_old_item);
         close C_GET_PREV;
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_PREV_PACKITEM;
-----------------------------------------------------------------------
FUNCTION GET_PARENT_TMPL_ID(O_error_message IN OUT VARCHAR2,
                            O_item_parent   IN OUT PACKITEM.ITEM_PARENT%TYPE,
                            O_pack_tmpl_id  IN OUT PACKITEM.PACK_TMPL_ID%TYPE,
                            I_pack_no       IN     PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN IS
   ---
   L_program      VARCHAR2(64) := 'PACKITEM_ATTRIB_SQL.GET_PARENT_TMPL_ID';
   ---
   cursor C_PARENT is
      select item_parent,
             pack_tmpl_id
        from packitem
       where pack_no = I_pack_no;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_PARENT',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_no);
   open C_PARENT;
   SQL_LIB.SET_MARK('FETCH',
                    'C_PARENT',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_no);
   fetch C_PARENT into O_item_parent, O_pack_tmpl_id;
   if C_PARENT%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_PARENT',
                       'PACKITEM',
                       'PACK_NO: '||I_pack_no);
      close C_PARENT;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PACK_NO',
                                            I_pack_no,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_PARENT',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_no);
   close C_PARENT;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
   return FALSE;
END GET_PARENT_TMPL_ID;
-----------------------------------------------------------------------
FUNCTION GET_ITEM_AND_QTY (O_error_message IN OUT VARCHAR2,
                           O_exists        IN OUT BOOLEAN,
                           O_item          IN OUT PACKITEM.ITEM%TYPE,
                           O_qty           IN OUT PACKITEM.PACK_QTY%TYPE,
                           I_pack_no       IN     PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN IS
   cursor C_GET_COMP_ITEM is
      select item,
             pack_qty
        from packitem
       where pack_no = I_pack_no;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_COMP_ITEM','PACKITEM','PACK NO:'||I_pack_no);
   open C_GET_COMP_ITEM;
   SQL_LIB.SET_MARK('FETCH','C_GET_COMP_ITEM','PACKITEM','PACK NO:'||I_pack_no);
   fetch C_GET_COMP_ITEM into O_item, O_qty;
   if C_GET_COMP_ITEM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ITEMS_IN_PACK', NULL, NULL, NULL);
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_COMP_ITEM','PACKITEM','PACK NO:'||I_pack_no);
   close C_GET_COMP_ITEM;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PACKITEM_ATTRIB_SQL.GET_ITEM_AND_QTY',
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEM_AND_QTY;
-----------------------------------------------------------------------------------
FUNCTION GET_NUMBER_OF_ITEMS (O_error_message IN OUT VARCHAR2,
                              O_no_of_items   IN OUT NUMBER,
                              I_pack_no       IN     PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN IS
   cursor C_GET_NUMBER_OF_ITEMS is
      select count(*)
        from packitem
       where pack_no = I_pack_no;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_NUMBER_OF_ITEMS','PACKITEM','PACK NO:'||I_pack_no);
   open C_GET_NUMBER_OF_ITEMS;
   SQL_LIB.SET_MARK('FETCH','C_GET_NUMBER_OF_ITEMS','PACKITEM','PACK NO:'||I_pack_no);
   fetch C_GET_NUMBER_OF_ITEMS into O_no_of_items;
   SQL_LIB.SET_MARK('CLOSE','C_GET_NUMBER_OF_ITEMS','PACKITEM','PACK NO:'||I_pack_no);
   close C_GET_NUMBER_OF_ITEMS;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PACKITEM_ATTRIB_SQL.GET_NUMBER_OF_ITEMS',
                                            to_char(SQLCODE));
      return FALSE;
END GET_NUMBER_OF_ITEMS;
---------------------------------------------------------------------------------------
FUNCTION GET_COMP_COSTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_comp_items      IN OUT   NOCOPY COMP_ITEM_TBL,
                        O_comp_qtys       IN OUT   NOCOPY COMP_QTY_TBL,
                        O_comp_costs      IN OUT   NOCOPY COMP_COST_TBL,
                        I_pack_no         IN       PACKITEM.PACK_NO%TYPE,
                        I_loc             IN       ITEM_LOC_SOH.LOC%TYPE)
RETURN BOOLEAN IS

   L_module   VARCHAR2(64) := 'PACKITEM_ATTRIB_SQL.GET_COMP_COSTS';

   cursor C_COMP_COST is
      select vpq.item,
             vpq.qty,
             ils.av_cost
        from v_packsku_qty vpq,
             item_loc_soh ils
       where vpq.pack_no = I_pack_no
         and ils.item = vpq.item
         and ils.loc = I_loc;

BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_module,
                                            'I_pack_no',
                                            'NULL');
      return FALSE;
   end if;

   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_module,
                                            'I_loc',
                                            'NULL');
      return FALSE;
   end if;

   open C_COMP_COST;
   fetch C_COMP_COST BULK COLLECT into O_comp_items,
                                       O_comp_qtys,
                                       O_comp_costs;
   close C_COMP_COST;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            to_char(SQLCODE));
      return FALSE;

END GET_COMP_COSTS;
-----------------------------------------------------------------------------------
FUNCTION GET_COMP_QTYS(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                       O_comp_items      IN OUT NOCOPY COMP_ITEM_TBL,
                       O_comp_qtys       IN OUT NOCOPY COMP_QTY_TBL,
                       I_pack_no         IN            PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN IS

   L_module   VARCHAR2(64) := 'PACKITEM_ATTRIB_SQL.GET_COMP_QTYS';

   cursor C_COMP_QTY is
   select vpq.item,
          vpq.qty
     from v_packsku_qty vpq,
          item_master im
    where vpq.pack_no = I_pack_no
      and vpq.item    = im.item
      and im.inventory_ind = 'Y';

BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                            L_module,
                                            'I_pack_no',
                                            'NULL');
      return FALSE;
   end if;

   open C_COMP_QTY;
   fetch C_COMP_QTY BULK COLLECT into O_comp_items,
                                      O_comp_qtys;
   close C_COMP_QTY;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_COMP_QTYS;
-----------------------------------------------------------------------------------
END PACKITEM_ATTRIB_SQL;
/
