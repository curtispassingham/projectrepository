CREATE OR REPLACE PACKAGE BODY RMSSUB_XITEMLOC AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHANGE_CASE
   -- Purpose      : This private function should convert specific char fields from the rib message to
   --                uppercase and convert the message type to lowercase.
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XItemLocDesc_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
   -- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                        IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause          IN     VARCHAR2,
                        I_program        IN     VARCHAR2);


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

PROCEDURE CONSUME(O_status_code   IN OUT VARCHAR2,
                  O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message       IN     RIB_OBJECT,
                  I_message_type  IN     VARCHAR2) IS

   L_program       VARCHAR2(50) := 'RMSSUB_XITEMLOC.CONSUME';
   L_message_type  VARCHAR2(15) := I_message_type;
   L_message       "RIB_XItemLocDesc_REC";
   L_enqueue_ind   VARCHAR2(1)    := 'N';
   PROGRAM_ERROR   EXCEPTION;

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is null then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', null, null,null);
      raise PROGRAM_ERROR;
   end if;
   if lower(L_message_type) in (ITEM_LOC_ADD, ITEM_LOC_UPD) then
      L_message := treat(I_message as "RIB_XItemLocDesc_REC");
      if L_message is NULL then
	   O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
	   raise PROGRAM_ERROR;
      end if;
   
      if CHANGE_CASE(O_error_message,
                     L_message,
                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   
      if RMSSUB_XITEMLOC_VALIDATE.CHECK_MESSAGE(O_error_message,
                                                L_message,
                                                L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   
      if RMSSUB_XITEMLOC_SQL.PERSIST(O_error_message,
                                     L_message,
                                     L_message_type) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE_TYPE',L_message_type, null, null);
      raise PROGRAM_ERROR;
   end if;
   
   return;

EXCEPTION
   when PROGRAM_ERROR then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_message,
                    API_LIBRARY.FATAL_ERROR,
                    L_program);
END CONSUME;
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_CASE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                     IO_message        IN OUT NOCOPY   "RIB_XItemLocDesc_REC",
                     IO_message_type   IN OUT NOCOPY   VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XITEMLOC.CHANGE_CASE';

BEGIN

   IO_message_type       := LOWER(IO_message_type);
   IO_message.hier_level := UPPER(IO_message.hier_level);

   if IO_message.xitemlocdtl_tbl is NOT NULL AND IO_message.xitemlocdtl_tbl.COUNT > 0 then
      FOR i in IO_message.xitemlocdtl_tbl.FIRST..IO_message.xitemlocdtl_tbl.LAST LOOP
         IO_message.xitemlocdtl_tbl(i).primary_cntry := UPPER(IO_message.xitemlocdtl_tbl(i).primary_cntry);
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHANGE_CASE;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE BODY
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code    IN OUT VARCHAR2,
                        IO_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause          IN     VARCHAR2,
                        I_program        IN     VARCHAR2) IS

L_program   VARCHAR2(50) := 'RMSSUB_XITEMLOC.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);
END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XITEMLOC;
/