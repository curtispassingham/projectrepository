CREATE OR REPLACE PACKAGE CORESVC_CUSTCREDITCHK AUTHID CURRENT_USER IS
-----------------------------------------------------------------------------
--Constant Variables.
ACTION_TYPE_UPDATE         CONSTANT VARCHAR2(6)  := 'update';

PROCESS_STATUS_ERROR       CONSTANT VARCHAR2(1)  := 'E';
PROCESS_STATUS_VALIDATED   CONSTANT VARCHAR2(1)  := 'V';
PROCESS_STATUS_NEW         CONSTANT VARCHAR2(1)  := 'N';
PROCESS_STATUS_COMPLETED   CONSTANT VARCHAR2(1)  := 'C';
-----------------------------------------------------------------------------
--FUNCTION NAME : CREATE_CREDIT_IND
--PURPOSE       : Called by SVCPROV_CUSTCREDITCHK  to process the input data.
-----------------------------------------------------------------------------
FUNCTION CREATE_CREDIT_IND(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id    IN      SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                           I_chunk_id      IN      SVC_CUSTCREDITCHK.CHUNK_ID%TYPE)
   RETURN BOOLEAN;

END CORESVC_CUSTCREDITCHK;
/