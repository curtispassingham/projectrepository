
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LOCLIST_VALIDATE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------------------
--Function Name:   VALIDATE_LOCLIST
--Purpose      :   Verifies that the passed in location list number exists on the
--                 LOC_LIST_HEAD table.
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCLIST(O_error_message   IN OUT VARCHAR2,
                          O_exists          IN OUT BOOLEAN,
                          I_loc_list        IN OUT LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
--Function Name:   VALIDATE_VALUE
--Purpose      :   For records to be saved to the LOC_LIST_CRITERIA table, validates that 
--                 the passed in parameter 'value' is valid for the given 'element' 
--                 (i.e. if the element is Store Number('SN') then the value parameter should be a 
--                 valid store number).
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_VALUE(O_error_message       IN OUT VARCHAR2,
                        I_element             IN     LOC_LIST_CRITERIA.ELEMENT%TYPE,
                        I_value               IN     LOC_LIST_CRITERIA.VALUE%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name:   DETAIL_EXISTS
-- Purpose      :   This function will check whether detail records exist on loc_list_detail table
--                  for a passed in location list.
--------------------------------------------------------------------------------------------------------
FUNCTION DETAIL_EXISTS(O_error_message     IN OUT VARCHAR2,
                       O_exists            IN OUT BOOLEAN,
                       I_loc_list          IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name:   CHECK_EDIT_EXISTS
-- Purpose      :   This function checks if the entered location list should be able to be
--                  modified by the current user.
--------------------------------------------------------------------------------------------------------
FUNCTION CHECK_EDIT_EXISTS(O_error_message      IN OUT  VARCHAR2,
                           O_exist              IN OUT  BOOLEAN,
                           I_loc_list           IN      LOC_LIST_HEAD.LOC_LIST%TYPE,
                           I_user_id            IN      LOC_LIST_HEAD.CREATE_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
END LOCLIST_VALIDATE_SQL;
/


