SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY PACKITEM_VALIDATE_SQL AS
-----------------------------------------------------------------------------
FUNCTION CHECK_DEPOSIT_COMPLEX (O_error_message   IN OUT VARCHAR2,
                                O_dep_complex_ind    OUT VARCHAR2,
                                I_item            IN     PACKITEM.PACK_NO%TYPE)
return BOOLEAN is


   L_program     VARCHAR2(64) := 'PACKITEM_VALIDATE_SQL.CHECK_DEPOSIT_COMPLEX';
   L_dep_exists    VARCHAR2(1) := 'N';

   cursor C_CHECK_DEP_ITEM is
      select 'Y'
        from item_master im,
             packitem p
       where p.pack_no = I_item
         and p.item    = im.item
         and im.deposit_item_type in ('E', 'A', 'Z');

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DEP_ITEM',
                    'PACKITEM, ITEM_MASTER',
                    NULL);
   open C_CHECK_DEP_ITEM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DEP_ITEM',
                    'PACKITEM, ITEM_MASTER',
                    NULL);
   fetch C_CHECK_DEP_ITEM into L_dep_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DEP_ITEM',
                    'PACKITEM, ITEM_MASTER',
                    NULL);
   close C_CHECK_DEP_ITEM;

   ---
   O_dep_complex_ind := L_dep_exists;
   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_DEPOSIT_COMPLEX;
-----------------------------------------------------------------------------

FUNCTION CHECK_FOR_SKUS(O_error_message   IN OUT VARCHAR2,
                        O_item_ind        IN OUT VARCHAR2,
                        I_pack_no         IN     PACKITEM.PACK_NO%TYPE)
   return BOOLEAN is

   L_program     VARCHAR2(64) := 'PACKITEM_VALIDATE_SQL.CHECK_FOR_SKUS';
   L_item_ind    VARCHAR2(1);
   ---
   cursor C_ITEMS is
      select 'x'
        from packitem
       where pack_no = I_pack_no;
BEGIN
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEMS',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_no);
   open C_ITEMS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEMS',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_no);
   fetch C_ITEMS into L_item_ind;
   if C_ITEMS%NOTFOUND then
      O_item_ind := 'N';
   else
      O_item_ind := 'Y';
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEMS',
                    'PACKITEM',
                    'PACK_NO: '||I_pack_no);
   close C_ITEMS;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   RETURN FALSE;
END CHECK_FOR_SKUS;
-----------------------------------------------------------------------------
FUNCTION CHECK_SKU(O_error_message  IN OUT  VARCHAR2,
                   I_item           IN      PACKITEM.ITEM%TYPE,
                   O_store_ind      IN OUT  BOOLEAN,
                   O_wh_ind         IN OUT  BOOLEAN)
   return BOOLEAN is
   ---
   L_program  VARCHAR2(60) := 'PACKITEM_VALIDATE_SQL.CHECK_SKU';
   L_fetch    VARCHAR2(1);
   ---
   cursor C_STORE is
      select 'x'
        from item_loc il
       where il.status in ('D', 'C')
         and il.item     = I_item
         and il.loc_type = 'S';
   ---
   cursor C_WH is
      select 'x'
        from item_loc il
       where il.status in ('D', 'C')
         and il.item     = I_item
         and il.loc_type = 'W';
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_STORE',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   open C_STORE;
   SQL_LIB.SET_MARK('FETCH',
                    'C_STORE',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   fetch C_STORE into L_fetch;
   if C_STORE%FOUND then
      O_store_ind := TRUE;
   else
      O_store_ind := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_STORE',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   close C_STORE;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_WH',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   open C_WH;
   SQL_LIB.SET_MARK('FETCH',
                    'C_WH',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   fetch C_WH into L_fetch;
   if C_WH%FOUND then
      O_wh_ind := TRUE;
   else
      O_wh_ind := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_WH',
                    'ITEM_LOC',
                    'ITEM: '||I_item);
   close C_WH;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_SKU;
-----------------------------------------------------------------------------
FUNCTION SIMPLE_PACK_EXISTS(O_error_message   IN OUT VARCHAR2,
                            O_exists          IN OUT BOOLEAN,
                            I_item            IN     PACKITEM.ITEM%TYPE,
                            I_pack_no         IN     PACKITEM.PACK_NO%TYPE)
   return BOOLEAN IS
   ---
   L_program      VARCHAR2(50) := 'PACKITEM_VALIDATE_SQL.SIMPLE_PACK_EXISTS';
   L_exists       VARCHAR2(1);
   ---
   cursor C_SIMPLE_PACK_EXISTS is
      select 'x'
        from packitem pi, item_master im
       where pi.pack_no         = im.item
         and pi.item            = I_item
         and im.simple_pack_ind = 'Y'
         and (pi.pack_no        != I_pack_no or I_pack_no is NULL);
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_ITEM',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_SIMPLE_PACK_EXISTS',
                    'PACKITEM, ITEM_MASTER',
                    'ITEM: '||I_item||' PACK_NO: '||I_pack_no);
   open C_SIMPLE_PACK_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_SIMPLE_PACK_EXISTS',
                    'PACKITEM, ITEM_MASTER',
                    'ITEM: '||I_item||' PACK_NO: '||I_pack_no);
   fetch C_SIMPLE_PACK_EXISTS into L_exists;
   if C_SIMPLE_PACK_EXISTS%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SIMPLE_PACK_EXISTS',
                    'PACKITEM, ITEM_MASTER',
                    'ITEM: '||I_item||' PACK_NO: '||I_pack_no);
   close C_SIMPLE_PACK_EXISTS;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SIMPLE_PACK_EXISTS;
-----------------------------------------------------------------------------

FUNCTION VALIDATE_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       I_item            IN       PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)    := 'PACKITEM_VALIDATE_SQL.VALIDATE_PACK';

   L_item_qty      PACKITEM.PACK_QTY%TYPE;
   L_qty_1         PACKITEM.PACK_QTY%TYPE;
   L_qty_2         PACKITEM.PACK_QTY%TYPE;
   L_qty_3         PACKITEM.PACK_QTY%TYPE;
   L_dep_type      ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_dep_1         ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_dep_2         ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_dep_3         ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_loop_ctr      NUMBER := 0;
   L_count_items   NUMBER(1);
   L_dep_exists    VARCHAR2(1);
   L_inv_exists    VARCHAR2(1);
   L_table         VARCHAR2(15);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   ---
   cursor C_COUNT_ITEMS is
      select COUNT(*)
        from packitem
       where pack_no = I_item;

   cursor C_GET_DEP_PACK is
      select im.deposit_item_type,
             pi.pack_qty
        from item_master im,
             packitem pi
       where pi.pack_no = I_item
         and pi.item    = im.item
         and im.deposit_item_type is not NULL;

   cursor C_CHECK_DEP_ITEM is
      select 'x'
        from item_master i,
             packitem p
       where p.pack_no = I_item
         and p.item    = i.item
         and i.deposit_item_type in ('E', 'A', 'Z')
         and rownum    = 1;

   cursor C_CHECK_INV_ITEM is
      select 'x'
        from item_master i,
             packitem p
       where p.pack_no           = I_item
         and (p.item             = i.item
              or p.item_parent   = i.item_parent)
         and i.inventory_ind     = 'Y'
         and rownum              = 1;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DEP_ITEM',
                    'PACKITEM, ITEM_MASTER',
                    NULL);
   open C_CHECK_DEP_ITEM;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DEP_ITEM',
                    'PACKITEM, ITEM_MASTER',
                    NULL);
   fetch C_CHECK_DEP_ITEM into L_dep_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DEP_ITEM',
                    'PACKITEM, ITEM_MASTER',
                    NULL);
   close C_CHECK_DEP_ITEM;

   ---
   if L_dep_exists = 'x' then

      -- item is a deposit item complex pack

      SQL_LIB.SET_MARK('OPEN',
                       'C_COUNT_ITEMS',
                       'PACKITEM',
                       NULL);
      open C_COUNT_ITEMS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_COUNT_ITEMS',
                       'PACKITEM',
                       NULL);
      fetch C_COUNT_ITEMS into L_count_items;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_COUNT_ITEMS',
                       'PACKITEM',
                       NULL);
      close C_COUNT_ITEMS;

      ---
      if L_count_items > 3 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PACK_COMP',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      elsif L_count_items = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PACKITEM_FUNC',
                                               L_program,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if L_count_items NOT in (2,3) then
         O_exists := FALSE;
         return TRUE;
      else

         O_exists := FALSE;

         for rec in C_GET_DEP_PACK LOOP

            -- reinitialize variables
            L_dep_type := NULL;
            L_item_qty := NULL;

            -- get the deposit_type and quantity of item
            L_dep_type := rec.deposit_item_type;
            L_item_qty := rec.pack_qty;

            L_loop_ctr := L_loop_ctr + 1;

            if ((L_loop_ctr = 2) and
                (L_count_items = 2)) or
               (L_loop_ctr = 3) then
               O_exists := TRUE;
            end if;

            ---
            if L_dep_type not in ('E', 'A', 'Z') then
               O_error_message := SQL_LIB.CREATE_MSG('INV_DEP_ITEMS',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            elsif L_loop_ctr = 1 then
               L_dep_1 := L_dep_type;
               L_qty_1 := L_item_qty;
            elsif L_loop_ctr = 2 then
               L_dep_2 := L_dep_type;
               L_qty_2 := L_item_qty;
            elsif L_loop_ctr = 3 then
               L_dep_3 := L_dep_type;
               L_qty_3 := L_item_qty;
            end if;

            -- if any of the deposit_item_type value repeats, o_exists is set to false
            -- if crate quantity is not 1 or content quantity is not equal to container quantity, o_exists is set to false
            if (L_loop_ctr = 2) and
               (L_count_items = 2) then
               if (L_dep_1 = L_dep_2) or
                  (L_dep_1 = 'Z') or
                  (L_dep_2 = 'Z') or
                  (L_qty_1 != L_qty_2) then
                  O_exists := FALSE;
                  return TRUE;
               end if;
            elsif (L_loop_ctr = 3) then
               if ((L_dep_1 = L_dep_2) or
                   (L_dep_1 = L_dep_3) or
                   (L_dep_2 = L_dep_3) or
                   ((L_dep_3 = 'Z') and
                    ((L_qty_1 != L_qty_2) or
                     (L_qty_3 != 1))) or
                   ((L_dep_2 = 'Z') and
                    ((L_qty_1 != L_qty_3) or
                     (L_qty_2 != 1))) or
                   ((L_dep_1 = 'Z') and
                    ((L_qty_2 != L_qty_3) or
                     (L_qty_1 != 1)))) then
                  O_exists := FALSE;
                  return TRUE;
               end if;
            elsif (L_loop_ctr = 2) then
               if ((L_dep_1 = L_dep_2) or
                   ((L_dep_1 != 'Z') and
                    (L_dep_2 != 'Z') and
                    (L_qty_1 != L_qty_2))) then
                  O_exists := FALSE;
                  return TRUE;
               end if;
            end if;

         END LOOP;
      end if;
   else

      -- item is a pack of regular items or pack items

      SQL_LIB.SET_MARK('OPEN',
                       'C_CHECK_INV_ITEM',
                       'PACKITEM, ITEM_MASTER',
                       'I_item: '|| I_item);
      open C_CHECK_INV_ITEM;

      SQL_LIB.SET_MARK('FETCH',
                       'C_CHECK_INV_ITEM',
                       'PACKITEM, ITEM_MASTER',
                       'I_item: '|| I_item);
      fetch C_CHECK_INV_ITEM into L_inv_exists;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_CHECK_INV_ITEM',
                       'PACKITEM, ITEM_MASTER',
                       'I_item: '|| I_item);
      close C_CHECK_INV_ITEM;

      if L_inv_exists is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PACK_COMP_INV',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      O_exists := (L_inv_exists is NOT NULL);

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END VALIDATE_PACK;

--------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATE_PACK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_pack_no         IN       PACKITEM.PACK_NO%TYPE,
                              I_item            IN       PACKITEM.ITEM%TYPE,
                              I_qty             IN       PACKITEM.PACK_QTY%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'PACKITEM_VALIDATE_SQL.CHECK_DUPLICATE_PACK';
   L_simple_pack_exists   VARCHAR2(1)  := NULL;
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_SIMPLE_PACK_EXISTS is
      select 'x'
        from packitem p,
             item_master im
       where im.item            = p.pack_no
         and im.simple_pack_ind = 'Y'
         and p.pack_no         != I_pack_no
         and p.item             = I_item
         and p.pack_qty         = I_qty
         and L_system_options_rec.aip_ind = 'Y';

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_qty is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_qty',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_SIMPLE_PACK_EXISTS',
                    'PACKITEM'||'ITEM_MASTER',
                    'Item: '||I_item||'Pack_no: '||I_pack_no);
   open C_SIMPLE_PACK_EXISTS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_SIMPLE_PACK_EXISTS',
                    'PACKITEM'||'ITEM_MASTER',
                    'Item: '||I_item||'Pack_no: '||I_pack_no);
   fetch C_SIMPLE_PACK_EXISTS into L_simple_pack_exists;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SIMPLE_PACK_EXISTS',
                    'PACKITEM'||'ITEM_MASTER',
                    'Item: '||I_item||'Pack_no: '||I_pack_no);
   close C_SIMPLE_PACK_EXISTS;
   if L_simple_pack_exists = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_DUPLICATE_PACK;
--------------------------------------------------------------------------------
END PACKITEM_VALIDATE_SQL;
/
