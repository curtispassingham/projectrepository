
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSMFM_SEEDDATA AS

------------------------------------------------------------------------------
/*** Private Program Declarations***/
------------------------------------------------------------------------------
PROCEDURE DO_GETNXT(O_status_code   OUT VARCHAR2,
                    O_error_msg     OUT VARCHAR2,
                    O_message_type  OUT CODES_MFQUEUE.MESSAGE_TYPE%TYPE,
                    O_message       IN OUT CLOB,
                    O_code_type     OUT CODES_MFQUEUE.CODE_TYPE%TYPE);
------------------------------------------------------------------------------
PROCEDURE DELETE_QUEUE_REC(O_status OUT VARCHAR2,
                           O_text   OUT VARCHAR2,
                           I_seq_no IN  CODES_MFQUEUE.SEQ_NO%TYPE);
------------------------------------------------------------------------------
           /*** Public Program Bodies ***/
------------------------------------------------------------------------------
PROCEDURE ADDTOQ(O_status         OUT VARCHAR2,
                 O_text           OUT VARCHAR2,
                 I_message_type   IN CODES_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_code_type      IN CODES_MFQUEUE.CODE_TYPE%TYPE,
                 I_message        IN OUT rib_sxw.SXWHandle)
IS
   L_message clob;

BEGIN
   O_status := API_CODES.SUCCESS;

   insert into codes_mfqueue(seq_no,
                             pub_status,
                             message_type,
                             code_type,
                             message)
                       values(codes_mfsequence.NEXTVAL,
                              'U',
                              I_message_type,
                              I_code_type,
                              empty_clob()) returning message into L_message;

   if not API_LIBRARY.WRITE_DOCUMENT_STR(O_status,
                                         O_text,
                                         L_message,
                                         I_message) then
      O_status := API_CODES.UNHANDLED_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SEEDDATA.ADDTOQ');
END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code   OUT VARCHAR2,
                 O_error_msg     OUT VARCHAR2,
                 O_message_type  OUT CODES_MFQUEUE.MESSAGE_TYPE%TYPE,
                 O_message       OUT nocopy CLOB,
                 O_code_type     OUT CODES_MFQUEUE.CODE_TYPE%TYPE)
                 
IS
   L_message clob;

BEGIN
   dbms_lob.createtemporary(L_message, true);
   
   do_getnxt(O_status_code,
             O_error_msg, 
             O_message_type,
             L_message,
             O_code_type);
   
   O_message := L_message;
   
EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SEEDDATA.GETNXT');
END GETNXT;
--------------------------------------------------------------------------------
PROCEDURE DO_GETNXT(O_status_code   OUT VARCHAR2,
                    O_error_msg     OUT VARCHAR2,
                    O_message_type  OUT CODES_MFQUEUE.MESSAGE_TYPE%TYPE,
                    O_message       IN OUT CLOB,
                    O_code_type     OUT CODES_MFQUEUE.CODE_TYPE%TYPE)
                 
IS
   cursor C_GET_MESSAGE is
      ---
      --- Get all of the unpublished code messages from the queue.
      ---
      select *
        from codes_mfqueue
       where pub_status = 'U'
       order by seq_no;
       
   L_queue_rec   CODES_MFQUEUE%ROWTYPE;

BEGIN
   open C_GET_MESSAGE;
      fetch C_GET_MESSAGE into L_queue_rec;

      if C_GET_MESSAGE%NOTFOUND then
         O_status_code := API_CODES.NO_MSG;
      else
         ---
         --- Publish code messages.
         ---
         O_message      := L_queue_rec.message;                 
         O_message_type := L_queue_rec.message_type;
         O_code_type    := L_queue_rec.code_type;

         DELETE_QUEUE_REC(O_status_code,
                          O_error_msg,
                          L_queue_rec.seq_no);
         O_status_code := API_CODES.SUCCESS;
      end if;
   close C_GET_MESSAGE;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SEEDDATA.DO_GETNXT');
END DO_GETNXT;
--------------------------------------------------------------------------------
           /*** Private Program Bodies***/
--------------------------------------------------------------------------------
PROCEDURE DELETE_QUEUE_REC(O_status OUT VARCHAR2,
                           O_text   OUT VARCHAR2,
                           I_seq_no IN  CODES_MFQUEUE.SEQ_NO%TYPE)
IS
BEGIN
   delete from codes_mfqueue
      where seq_no = I_seq_no;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status,
                                O_text,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_CODE.DELETE_QUEUE_REC');
END DELETE_QUEUE_REC;
--------------------------------------------------------------------------------
END RMSMFM_SEEDDATA;
/
