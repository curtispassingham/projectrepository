create or replace PACKAGE BODY PM_RETAIL_API_SQL IS

--------------------------------------------------------------------------------

-- The following types and variables are used to save the original retail values
-- returned by the pricing system in GET_ITEM_PRICING_INFO.  They are later
-- used in SET_ITEM_PRICING_INFO to determine if the values have changed.

TYPE ORIG_VALUES_REC IS RECORD
(unit_retail          ITEM_LOC.UNIT_RETAIL%TYPE,
 selling_unit_retail  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
 selling_uom          ITEM_LOC.SELLING_UOM%TYPE );

TYPE ORIG_VALUES_TBL IS TABLE OF ORIG_VALUES_REC INDEX BY BINARY_INTEGER;

GV_orig_values_table        ORIG_VALUES_TBL;
GV_empty_orig_values_table  ORIG_VALUES_TBL;

-- This variable is used to hold all zone/location records returned
-- by the pricing system.
GV_zone_locs_table          OBJ_ZONE_LOCS_TBL;

-- store the system options to use across all functions
GV_system_options_rec        SYSTEM_OPTIONS%ROWTYPE;
--------------------------------------------------------------------------------
-- this internal function will insert/update item_loc retails with the
-- RPM base retail for locations that exist in RMS but not in RPM

PROCEDURE INSERT_UPDATE_XTRA_RMS_LOCS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_success           IN OUT VARCHAR2,
                                      I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                      I_dept              IN     ITEM_MASTER.DEPT%TYPE,
                                      I_class             IN     ITEM_MASTER.CLASS%TYPE,
                                      I_subclass          IN     ITEM_MASTER.SUBCLASS%TYPE,
                                      I_pack_ind          IN     ITEM_MASTER.PACK_IND%TYPE,
                                      I_default_to_child  IN     VARCHAR2,
                                      I_base_unit_retail  IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                                      I_base_selling_ur   IN     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                      I_base_selling_uom  IN     ITEM_LOC.SELLING_UOM%TYPE,
                                      I_base_multi_units  IN     ITEM_LOC.MULTI_UNITS%TYPE,
                                      I_base_multi_ur     IN     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                      I_base_multi_s_uom  IN     ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                      I_base_currency_code IN    CURRENCIES.CURRENCY_CODE%TYPE
) IS

   L_program                 VARCHAR2(50) := 'PM_RETAIL_API_SQL.INSERT_UPDATE_XTRA_RMS_LOCS';
   L_loc_currency_code       CURRENCIES.CURRENCY_CODE%TYPE;
   L_unit_retail_loc         ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_ur_loc          ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_multi_ur_loc            ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_curr_exist        BOOLEAN;
   L_user                    VARCHAR2(50) := GET_USER;
   L_datetime                DATE := SYSDATE;


   cursor C_XTRA_RMS_LOCS is
     select i.loc,
            l.currency_code
         from item_loc i,
              (select store location,
                      currency_code currency_code
                 from store
               union all
               select wh location,
                      currency_code currency_code
                 from wh
              ) l
        where i.loc = l.location
          and I_item = i.item
      minus
      (select il.loc loc,
              l.currency_code
        from item_loc il,
            TABLE(CAST(GV_zone_locs_table AS OBJ_ZONE_LOCS_TBL)) zl,
            (select store location,
                    currency_code currency_code
               from store
             union all
             select wh location,
                    currency_code currency_code
               from wh
              ) l
       where item = I_item
         and zl.location_id = il.loc
         and il.loc = l.location);

BEGIN
   O_success := 'Y';

   --- find and insert the base retail values for all the RMS locations that don't exist in RPM
   for rec in C_XTRA_RMS_LOCS LOOP

      L_loc_currency_code := rec.currency_code;

      if I_base_currency_code != rec.currency_code then
         --- need to convert the unit, selling and multi unit retails
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 I_base_unit_retail,
                                 I_base_currency_code,
                                 L_loc_currency_code,
                                 L_unit_retail_loc,
                                 'R',
                                 NULL,
                                 NULL) = FALSE then
            O_success := 'N';
            return;
         end if;

         if CURRENCY_SQL.CONVERT(O_error_message,
                                 I_base_selling_ur,
                                 I_base_currency_code,
                                 L_loc_currency_code,
                                 L_selling_ur_loc,
                                 'R',
                                 NULL,
                                 NULL) = FALSE then
            O_success := 'N';
            return;
         end if;
         if I_base_multi_ur is not NULL then
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    I_base_multi_ur,
                                    I_base_currency_code,
                                    L_loc_currency_code,
                                    L_multi_ur_loc,
                                    'R',
                                    NULL,
                                    NULL) = FALSE then
               O_success := 'N';
               return;
            end if;
         end if;
      else
         --- the currencies are equal, no conversion necessary just set the local vars
         L_unit_retail_loc := I_base_unit_retail;
         L_selling_ur_loc  := I_base_selling_ur;
         L_multi_ur_loc    := I_base_multi_ur;
      end if;

      --- call function to update item_loc retails
      if SKU_RETAIL_SQL.UPDATE_RETAIL(O_error_message,
                                      I_item,
                                      NULL,  -- original  since these locs are unmapped in RPM, RPM did not send an original retail
                                      L_unit_retail_loc,  -- new
                                      NULL, -- original
                                      L_selling_ur_loc, -- new
                                      NULL,  -- original
                                      I_base_selling_uom,  -- new
                                      I_base_multi_units,
                                      L_multi_ur_loc,
                                      I_base_multi_s_uom,
                                      I_default_to_child,
                                      rec.loc, -- zone_id
                                      I_dept,
                                      I_class,
                                      I_subclass,
                                      NULL,  -- primary_currency, not used in function
                                      I_pack_ind) = FALSE then
         O_success := 'N';
         return;
      end if;
   end LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
      RETURN;
END INSERT_UPDATE_XTRA_RMS_LOCS;
--------------------------------------------------------------------------------------------------
FUNCTION GET_UNIT_RETAIL(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_regular_retail    IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                         O_promotion_retail  IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                         O_promotion_ind     IN OUT  BOOLEAN,
                         O_current_retail    IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                         O_clearance_ind     IN OUT  BOOLEAN,
                         I_item              IN      ITEM_MASTER.ITEM%TYPE,
                         I_location_type     IN      ITEM_LOC.LOC_TYPE%TYPE,
                         I_location          IN      ITEM_LOC.LOC%TYPE,
                         I_tran_date         IN      PRICE_HIST.ACTION_DATE%TYPE,
                         I_tran_type         IN      VARCHAR,
                         I_pack_process_ind  IN      BOOLEAN,
                         I_class_vat_ind     IN      BOOLEAN,
                         I_vat_rate          IN      NUMBER)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_UNIT_RETAIL';
   L_invalid_param  VARCHAR2(30) := NULL;
   L_found_retail   BOOLEAN := FALSE;

   L_tran_type PRICE_HIST.TRAN_TYPE%TYPE := NULL;
   L_unit_retail PRICE_HIST.UNIT_RETAIL%TYPE := NULL;
   L_action_date PRICE_HIST.ACTION_DATE%TYPE := NULL;

   L_first_iteration BOOLEAN := TRUE;
   L_vdate PERIOD.VDATE%TYPE := TO_CHAR(GET_VDATE);

   L_default_tax_type SYSTEM_OPTIONS.default_tax_type%TYPE;

   L_dept  DEPS.DEPT%TYPE := NULL;
   L_class CLASS.CLASS%TYPE := NULL;

   L_input_tran_type VARCHAR2(1) := I_tran_type;
   L_pack_process_ind BOOLEAN := I_pack_process_ind;

   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;

   cursor c_item is
      select im.dept,
             im.class
        from item_master im
       where im.item = I_item;

   cursor C_ALL_RETAIL is
      select tran_type,
             unit_retail,
             action_date
        from price_hist
       where item = I_item
         and loc = I_location
         and loc_type = I_location_type
         and action_date < (TO_DATE(GREATEST(NVL(I_tran_date, L_vdate), L_vdate)) + 1)
         and tran_type in (0, 4, 8, 9, 11)
      order by action_date DESC;

   cursor C_BASE_RETAIL is
      select unit_retail
        from price_hist
       where item = I_item
         and loc = I_location
         and loc_type = I_location_type
         and tran_type = 0;

BEGIN

   -- Begin parameter verificaton, function pre-processing
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_location is NULL then
      L_invalid_param := 'I_location';
   elsif I_location_type is NULL then
      L_invalid_param := 'I_location_type';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_default_tax_type := L_system_options_row.default_tax_type;

   if L_pack_process_ind IS NULL then
      L_pack_process_ind := FALSE;
   end if;

   if L_pack_process_ind = FALSE then
      O_promotion_ind := FALSE;
   end if;

   if L_input_tran_type IS NULL then
      L_input_tran_type := 'S';
   end if;

   -- Begin processing
   OPEN c_all_retail;
   WHILE L_found_retail = FALSE LOOP
      FETCH c_all_retail INTO L_tran_type,
                              L_unit_retail,
                              L_action_date;

      if c_all_retail%NOTFOUND then
         OPEN c_base_retail;
         FETCH c_base_retail INTO L_unit_retail;
         if c_base_retail%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_SKU_STAPLE',
                                                  I_item,
                                                  L_program,
                                                  NULL);
            return FALSE;
         end if;
         L_tran_type := 0;
         CLOSE c_base_retail;
      end if;

      -- If the action date returned by the cursor is less than or equal to the
      -- sale date, it is a valid retail value for the item.  If the tran_type
      -- is promotion, this is a sale transaction, and the item is not part of
      -- a pack, assign the value to O_promotion_retail and continue until a
      -- non-promotion retail value is retrieved.

      if L_first_iteration = TRUE OR
         L_action_date <= NVL(I_tran_date, L_vdate) then

         if L_tran_type = 9 AND
            L_pack_process_ind = FALSE AND
            O_promotion_ind = FALSE AND
            L_input_tran_type = 'S' AND
            L_action_date <= NVL(I_tran_date, L_vdate) then

            O_promotion_retail := L_unit_retail;
            O_promotion_ind := TRUE;
            L_first_iteration := FALSE;
         elsif L_tran_type != 9 AND L_action_date <= NVL(I_tran_date, L_vdate) then

            O_regular_retail := L_unit_retail;
            O_current_retail := L_unit_retail;
            L_found_retail := TRUE;
         end if;
      end if;
   END LOOP;
   CLOSE c_all_retail;

   open c_item;
   fetch c_item into L_dept, L_class;
   close c_item;

   if L_tran_type = 8 then
      O_clearance_ind := TRUE;
   end if;

   if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
              O_regular_retail,
              I_item, L_dept, L_class, I_location, I_location_type, L_vdate,
              O_regular_retail) = FALSE then
      return FALSE;
   end if;
   if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
              O_promotion_retail,
              I_item, L_dept, L_class, I_location, I_location_type, L_vdate,
              O_promotion_retail) = FALSE then
      return FALSE;
   end if;
   if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
              O_current_retail,
              I_item, L_dept, L_class, I_location, I_location_type, L_vdate,
              O_current_retail) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_UNIT_RETAIL;
--------------------------------------------------------------------------------
PROCEDURE GET_ITEM_PRICING_INFO
(O_item_pricing_table  IN OUT  PM_RETAIL_API_SQL.ITEM_PRICING_TABLE,
 I_item                IN      ITEM_MASTER.ITEM%TYPE,
 I_dept                IN      ITEM_MASTER.DEPT%TYPE,
 I_class               IN      ITEM_MASTER.CLASS%TYPE,
 I_subclass            IN      ITEM_MASTER.SUBCLASS%TYPE,
 I_cost_currency_code  IN      CURRENCIES.CURRENCY_CODE%TYPE,
 I_cost                IN      ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
 I_called_from_form    IN      VARCHAR2 DEFAULT 'N'
)
IS

   L_program                   VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_ITEM_PRICING_INFO';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_invalid_param             VARCHAR2(30) := NULL;
   L_item_parent               ITEM_MASTER.ITEM%TYPE;
   L_success                   VARCHAR2(1);

   L_budgeted_intake           DEPS.BUD_INT%TYPE;
   L_budgeted_markup           DEPS.BUD_MKUP%TYPE;
   L_primary_curr              CURRENCIES.CURRENCY_CODE%TYPE;
   L_euro_curr                 CURRENCIES.CURRENCY_CODE%TYPE;
   L_obj_item_pricing_table    OBJ_ITEM_PRICING_TBL;
   L_first                     NUMBER(10);
   L_last                      NUMBER(10);
   L_cost_sup                  ITEM_LOC_SOH.UNIT_COST%TYPE;  -- cost in supplier currency
   L_total_cost                ITEM_LOC_SOH.UNIT_COST%TYPE     := I_cost;
   L_cost_currency_code        CURRENCIES.CURRENCY_CODE%TYPE   := I_cost_currency_code;
   L_supp_currency_code        CURRENCIES.CURRENCY_CODE%TYPE;
   L_primary_sup               SUPS.SUPPLIER%TYPE;
   L_waste_pct                 ITEM_MASTER.WASTE_PCT%TYPE;
   L_waste_type                ITEM_MASTER.WASTE_TYPE%TYPE;
   L_default_waste_pct         ITEM_MASTER.WASTE_PCT%TYPE;
   L_total_exp                 NUMBER;
   L_exp_currency              CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_exp              CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty                 NUMBER;
   L_dty_currency              CURRENCIES.CURRENCY_CODE%TYPE;

   cursor C_PARENT is
      select item_parent
        from item_master
       where item = I_item;

   --- This cursor is used to do a BULK transfer of values from the object table
   --- returned by pricing into the the PL/SQL table used in the RMS form.
   --- For this to work the values and order of the 'select' items must be the
   --- same as the item_pricing_rec defined in the spec of PM_RETAIL_API_SQL.
   cursor C_OBJECT_RECS is
      select item,
             zone_group_id,  -- RPM
             zone_id,
             zone_display_id,
             zone_name,
             unit_retail,
             selling_unit_retail,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_selling_uom,
             base_retail_ind, -- RPM
             currency_code,
             standard_uom,
             ----
             NULL, -- selling_mark_up
             NULL, -- multi_selling_mark_up
             'N',  -- default_to_children
             --- The following values will be used to default the primary and
             --- euro retail values.  If we are not in a multi currency system
             --- or if the primary/euro currency is the same as the local currency
             --- then no conversion is necessary and these are the values
             --- which will be kept for the primary/euro values.  Populating them
             --- here saves a step later if we do not need to convert.
             unit_retail,           --- primary
             selling_unit_retail,   --- primary
             multi_unit_retail,     --- primary
             unit_retail,           --- euro
             selling_unit_retail,   --- euro
             multi_unit_retail,     --- euro,
             NULL,                  -- error message
             'TRUE',                -- return code
             area_diff_zone_ind
        from TABLE(cast(L_obj_item_pricing_table as OBJ_ITEM_PRICING_TBL))
    order by zone_display_id;

BEGIN
   --- Validate parameters
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_dept is NULL then
      L_invalid_param := 'I_dept';
   elsif I_class is NULL then
      L_invalid_param := 'I_class';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_subclass';
   elsif I_cost is NULL then
      L_invalid_param := 'I_cost';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      O_item_pricing_table(1).error_message := L_error_message;
      O_item_pricing_table(1).return_code := 'FALSE';
      return;
   end if;
   --- Make sure the global table is empty
   GV_orig_values_table := GV_empty_orig_values_table;

   --- Pricing system needs the item parent if it exists
   open  C_PARENT;
   fetch C_PARENT into L_item_parent;
   close C_PARENT;

    --- Get system options
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            GV_system_options_rec) = FALSE then
      O_item_pricing_table(1).error_message := L_error_message;
      O_item_pricing_table(1).return_code := 'FALSE';
      return;
   end if;

   L_primary_curr  := GV_system_options_rec.currency_code;

   if I_cost != 0 then  -- if the user hasn't entered supplier information, or the item is not orderable then
                        -- a 0 unit cost will be passed to rpm, and there is no point in figuring out wastage markups
      --- calculate wastage markups
      --- cost is now in the primary curr
      L_cost_currency_code := L_primary_curr;

      if ITEM_ATTRIB_SQL.GET_WASTAGE(L_error_message,
                                     L_waste_type,
                                     L_waste_pct,
                                     L_default_waste_pct,
                                     I_item) = FALSE then
         O_item_pricing_table(1).error_message := L_error_message;
         O_item_pricing_table(1).return_code := 'FALSE';
         return;
      end if;

      if L_waste_type in ('SP','SL') then
         L_waste_pct := nvl(L_waste_pct/100,0);
         L_total_cost := L_total_cost * (1 + L_waste_pct);
      end if;
   end if;

   --- Instantiate the local pricing table object
   L_obj_item_pricing_table := OBJ_ITEM_PRICING_TBL();
   --- Get the item/zone pricing information from pricing system
   MERCH_RETAIL_API_SQL.GET_ITEM_PRICING_INFO(L_error_message,
                                              L_success,
                                              L_obj_item_pricing_table,
                                              L_item_parent,
                                              I_item,
                                              I_dept,
                                              I_class,
                                              I_subclass,
                                              L_cost_currency_code,
                                              L_total_cost);
   if L_success = 'N' then
      O_item_pricing_table(1).error_message := L_error_message;
      O_item_pricing_table(1).return_code := 'FALSE';
      return;
   end if;

   --- Bulk copy values from the Oracle object into the PL/SQL table
   open  C_OBJECT_RECS;
   fetch C_OBJECT_RECS BULK COLLECT INTO O_item_pricing_table;
   close C_OBJECT_RECS;

   --- Set up local variables
   L_first         := O_item_pricing_table.FIRST;
   L_last          := O_item_pricing_table.LAST;
   L_euro_curr     := 'EUR';  -- currency codes are use standard abbreviations. need to
                              -- get EUR currency info, and there simply isn't a good way to
                              -- fetch the euro's currency code.

   --- Per record logic
   for i in L_first..L_last loop

      --- Save the original values from pricing so we can tell if they
      --- have changed later in SET_ITEM_PRICING_INFO
      GV_orig_values_table(i).unit_retail         := O_item_pricing_table(i).unit_retail;
      GV_orig_values_table(i).selling_unit_retail := O_item_pricing_table(i).selling_unit_retail;
      GV_orig_values_table(i).selling_uom         := O_item_pricing_table(i).selling_uom;
      --- Currency conversion if we are in a multi currency system
      if  GV_system_options_rec.multi_currency_ind = 'Y' then

         --- If local currency is different than primary then convert
         if L_primary_curr != O_item_pricing_table(i).currency_code then

            if CURRENCY_SQL.CONVERT(L_error_message,
                                    O_item_pricing_table(i).unit_retail,
                                    O_item_pricing_table(i).currency_code,
                                    L_primary_curr,
                                    O_item_pricing_table(i).unit_retail_prim,
                                    'R',
                                    NULL,
                                    NULL) = FALSE then
               O_item_pricing_table(1).error_message := L_error_message;
               O_item_pricing_table(1).return_code := 'FALSE';
               return;
            end if;

            if CURRENCY_SQL.CONVERT(L_error_message,
                                    O_item_pricing_table(i).selling_unit_retail,
                                    O_item_pricing_table(i).currency_code,
                                    L_primary_curr,
                                    O_item_pricing_table(i).selling_unit_retail_prim,
                                    'R',
                                    NULL,
                                    NULL) = FALSE then
               O_item_pricing_table(1).error_message := L_error_message;
               O_item_pricing_table(1).return_code := 'FALSE';
               return;
            end if;

            if O_item_pricing_table(i).multi_unit_retail is NOT NULL then
               if CURRENCY_SQL.CONVERT(L_error_message,
                                       O_item_pricing_table(i).multi_unit_retail,
                                       O_item_pricing_table(i).currency_code,
                                       L_primary_curr,
                                       O_item_pricing_table(i).multi_unit_retail_prim,
                                       'R',
                                       NULL,
                                       NULL) = FALSE then
                  O_item_pricing_table(1).error_message := L_error_message;
                  O_item_pricing_table(1).return_code := 'FALSE';
                  return;
               end if;
            end if;

         end if;

         --- If local currency is different than Euro then convert
         if L_euro_curr != O_item_pricing_table(i).currency_code then

            if CURRENCY_SQL.CONVERT(L_error_message,
                                    O_item_pricing_table(i).unit_retail,
                                    O_item_pricing_table(i).currency_code,
                                    L_euro_curr,
                                    O_item_pricing_table(i).unit_retail_euro,
                                    'R',
                                    NULL,
                                    NULL) = FALSE then
               O_item_pricing_table(1).error_message := L_error_message;
               O_item_pricing_table(1).return_code := 'FALSE';
               return;
            end if;

            if CURRENCY_SQL.CONVERT(L_error_message,
                                    O_item_pricing_table(i).selling_unit_retail,
                                    O_item_pricing_table(i).currency_code,
                                    L_euro_curr,
                                    O_item_pricing_table(i).selling_unit_retail_euro,
                                    'R',
                                    NULL,
                                    NULL) = FALSE then
               O_item_pricing_table(1).error_message := L_error_message;
               O_item_pricing_table(1).return_code := 'FALSE';
               return;
            end if;

            if O_item_pricing_table(i).multi_unit_retail is NOT NULL then
               if CURRENCY_SQL.CONVERT(L_error_message,
                                       O_item_pricing_table(i).multi_unit_retail,
                                       O_item_pricing_table(i).currency_code,
                                       L_euro_curr,
                                       O_item_pricing_table(i).multi_unit_retail_euro,
                                       'R',
                                       NULL,
                                       NULL) = FALSE then
                  O_item_pricing_table(1).error_message := L_error_message;
                  O_item_pricing_table(1).return_code := 'FALSE';
                  return;
               end if;
            end if;

         end if;

      else
         --- Not in a multi-currency system so copy the primary currency
         --- into the local currency code.
         O_item_pricing_table(i).currency_code := L_primary_curr;
      end if;
      ---
      if CURRENCY_SQL.ROUND_CURRENCY(L_error_message,
                                     O_item_pricing_table(i).selling_unit_retail,
                                     O_item_pricing_table(i).currency_code,
                                     'R') = FALSE then
         O_item_pricing_table(1).error_message := L_error_message;
         O_item_pricing_table(1).return_code := 'FALSE';
         return;
      end if;
      ---
      if CURRENCY_SQL.ROUND_CURRENCY(L_error_message,
                                     O_item_pricing_table(i).unit_retail,
                                     O_item_pricing_table(i).currency_code,
                                     'R') = FALSE then
         O_item_pricing_table(1).error_message := L_error_message;
         O_item_pricing_table(1).return_code := 'FALSE';
         return;
      end if;
   end loop;

   ------------------------------------------------------------------------
   -- Compute the markup if GET_ITEM_PRICING_INFO is called from a form
   -- and if the O_item_pricing_table collection has records.
   -- Currently only itemretail.fmb uses this logic.
   ---
   -- The markup calculation utilizes bulk processing and avoids having
   -- to do a row-by-row markup calculation in the calling form's
   -- POST-QUERY trigger.
   ---
   -- The markup result is returned in the selling_mark_up column of the
   -- O_item_pricing_table collection.
   ------------------------------------------------------------------------
   if I_called_from_form = 'Y' and O_item_pricing_table is not NULL and O_item_pricing_table.COUNT > 0 then
      -- Call markup calculation function
      if MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM_BULK(L_error_message,
                                                  O_item_pricing_table,
                                                  I_cost) = FALSE then  -- I_cost is the supplier cost without wastege
         O_item_pricing_table(1).error_message := L_error_message;
         O_item_pricing_table(1).return_code := 'FALSE';
         return;
      end if;
   end if;

EXCEPTION
   when OTHERS then
      O_item_pricing_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
      O_item_pricing_table(1).return_code := 'FALSE';
      return;
END GET_ITEM_PRICING_INFO;

--------------------------------------------------------------------------------
PROCEDURE GET_ZONE_LOCATIONS
(O_location_table      IN OUT  NOCOPY OBJ_RPM_LOC_TBL,
 I_rpm_zone_group_id   IN      PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE,
 I_zone_id             IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE
)
IS

   L_program        VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_ZONE_LOCATIONS';
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
   L_success        VARCHAR2(1);
   rpm_store_ind    CONSTANT BINARY_INTEGER := 0;
   rpm_wh_ind       CONSTANT BINARY_INTEGER := 2;

   cursor C_LOC_NAME is
      select OBJ_RPM_LOC_REC(store, store_name, NULL, NULL)
        from store,
             TABLE(cast(GV_zone_locs_table as OBJ_ZONE_LOCS_TBL)) zl
       where store = zl.location_id
         and zl.location_type = rpm_store_ind
       union all
      select OBJ_RPM_LOC_REC(wh, wh_name, NULL,NULL)
        from wh,
             TABLE(cast(GV_zone_locs_table as OBJ_ZONE_LOCS_TBL)) zl
       where wh = zl.location_id
         and zl.location_type = rpm_wh_ind;

BEGIN
   --- in the very rare case that RPM sends RMS a location that does not exist in RMS,
   ---  the location will be ignored.
   MERCH_RETAIL_API_SQL.GET_ZONE_LOCATIONS(L_error_message,
                                           L_success,
                                           GV_zone_locs_table,
                                           I_rpm_zone_group_id,
                                           I_zone_id);

   if L_success = 'N' then
      O_location_table(1).error_message := L_error_message;
      O_location_table(1).return_code := 'FALSE';
      return;
   end if;

   --- Select locations/descriptions for the zone_id specified
   --- and return this to the calling program
   open  C_LOC_NAME;
   fetch C_LOC_NAME BULK COLLECT INTO O_location_table;
   close C_LOC_NAME;

EXCEPTION
   when OTHERS then
     O_location_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
     O_location_table(1).return_code := 'FALSE';
     return;
END GET_ZONE_LOCATIONS;

--------------------------------------------------------------------------------
PROCEDURE GET_ZONE_LOCATIONS
(O_location_table      IN OUT  PM_RETAIL_API_SQL.LOCATION_TABLE,
 I_rpm_zone_group_id   IN      PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE,
 I_zone_id             IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE
)
IS

   L_program        VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_ZONE_LOCATIONS';
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
   L_success        VARCHAR2(1);
   rpm_store_ind    CONSTANT BINARY_INTEGER := 0;
   rpm_wh_ind       CONSTANT BINARY_INTEGER := 2;

   cursor C_LOC_NAME is
      select store location,
             store_name location_name,
             NULL error_message,
             NULL return_code
        from store,
             TABLE(cast(GV_zone_locs_table as OBJ_ZONE_LOCS_TBL)) zl
       where store = zl.location_id
         and zl.location_type = rpm_store_ind
       union all
      select wh location,
             wh_name location_name,
             NULL error_message,
             NULL return_code
        from wh,
             TABLE(cast(GV_zone_locs_table as OBJ_ZONE_LOCS_TBL)) zl
       where wh = zl.location_id
         and zl.location_type = rpm_wh_ind;

BEGIN
   --- in the very rare case that RPM sends RMS a location that does not exist in RMS,
   ---  the location will be ignored.
   MERCH_RETAIL_API_SQL.GET_ZONE_LOCATIONS(L_error_message,
                                           L_success,
                                           GV_zone_locs_table,
                                           I_rpm_zone_group_id,
                                           I_zone_id);

   if L_success = 'N' then
      O_location_table(1).error_message := L_error_message;
      O_location_table(1).return_code := 'FALSE';
      return;
   end if;

   --- Select locations/descriptions for the zone_id specified
   --- and return this to the calling program
   open  C_LOC_NAME;
   fetch C_LOC_NAME BULK COLLECT INTO O_location_table;
   close C_LOC_NAME;

EXCEPTION
   when OTHERS then
     O_location_table(1).error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
     O_location_table(1).return_code := 'FALSE';
     return;
END GET_ZONE_LOCATIONS;

--------------------------------------------------------------------------------
PROCEDURE SET_ITEM_PRICING_INFO
(I_item_pricing_table   IN      ITEM_PRICING_TABLE
)
IS

   L_program                 VARCHAR2(61) := 'PM_RETAIL_API_SQL.SET_ITEM_PRICING_INFO';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_success                 VARCHAR2(1);
   L_datetime                DATE;
   L_user                    VARCHAR2(30) := GET_USER;
   L_item                    ITEM_MASTER.ITEM%TYPE;
   L_item_level              ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level              ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_pack_ind                ITEM_MASTER.PACK_IND%TYPE;
   L_dept                    ITEM_MASTER.DEPT%TYPE;
   L_class                   ITEM_MASTER.CLASS%TYPE;
   L_subclass                ITEM_MASTER.SUBCLASS%TYPE;
   L_default_to_children     VARCHAR2(1);
   L_child_exists            BOOLEAN;
   L_grandchild_exists       BOOLEAN;
   L_new_child_default_ind   VARCHAR2(1);
   L_first                   NUMBER(10);
   L_last                    NUMBER(10);
   rpm_base_zone_ind         CONSTANT BINARY_INTEGER := 1;
   L_obj_item_pricing_table  OBJ_ITEM_PRICING_TBL;
   L_base_unit_retail        ITEM_LOC.UNIT_RETAIL%TYPE;
   L_base_selling_ur         ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_base_selling_uom        ITEM_LOC.SELLING_UOM%TYPE;
   L_base_multi_units        ITEM_LOC.MULTI_UNITS%TYPE;
   L_base_multi_ur           ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_base_multi_s_uom        ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_base_currency_code      CURRENCIES.CURRENCY_CODE%TYPE;
   L_loc_currency_code       CURRENCIES.CURRENCY_CODE%TYPE;
   L_unit_retail_loc         ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_ur_loc          ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_multi_ur_loc            ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_base_retail_ind         VARCHAR2(1);
   L_multi_curr_exist        BOOLEAN;
   L_default_standard_uom    SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE;


   cursor C_ITEM_INFO is
      select item_level,
             tran_level,
             pack_ind,
             dept,
             class,
             subclass
        from item_master
       where item = L_item;


   cursor C_LOCS_BY_ZONE (L_zone_id IN NUMBER) is
      select zl.location_id
        from TABLE(cast(GV_zone_locs_table as OBJ_ZONE_LOCS_TBL)) zl,
             item_loc il
       where zl.zone_id = L_zone_id
         and zl.location_id = il.loc
         and il.item = L_item;  -- only want to process records where there is an item-loc relationship

   cursor C_CHILDREN is
      select item child_item
        from item_master
       where item_parent = L_item
          or item_grandparent = L_item
         and tran_level >= item_level;

   cursor C_SYSTEM_OPTIONS is
      select default_standard_uom
        from system_options;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   open C_SYSTEM_OPTIONS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   fetch C_SYSTEM_OPTIONS into L_default_standard_uom;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   close C_SYSTEM_OPTIONS;

   if I_item_pricing_table is NULL or I_item_pricing_table.count = 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_pricing_table',
                                            L_program,
                                            NULL);
      raise_application_error(ERRNUM_INVALID_PARAM,L_error_message);
   end if;
   --- Set up local variables
   L_datetime            := SYSDATE;
   L_user                := GET_USER;
   L_item                := I_item_pricing_table(1).item;
   L_default_to_children := nvl(I_item_pricing_table(1).default_to_children,'N');
   L_first               := I_item_pricing_table.FIRST;
   L_last                := I_item_pricing_table.LAST;

   --- Get info for the item
   open  C_ITEM_INFO;
   fetch C_ITEM_INFO into L_item_level,
                          L_tran_level,
                          L_pack_ind,
                          L_dept,
                          L_class,
                          L_subclass;
   close C_ITEM_INFO;

   MERCH_RETAIL_API_SQL.GET_ZONE_LOCATIONS(L_error_message,
                                           L_success,
                                           GV_zone_locs_table,
                                           I_item_pricing_table(1).rpm_zone_group_id,  -- item has 1 rpm zone group id and
                                                                                       -- many zone ids
                                           NULL);
   if L_success = 'N' then
      L_error_message := SQL_LIB.CREATE_MSG('FAILED_CALL',
                                            'MERCH_RETAIL_API_SQ.GET_ZONE_LOCATIONS',
                                            L_error_message,
                                            NULL);
      raise_application_error(ERRNUM_PACKAGE_CALL,L_error_message);
   end if;

   L_item := I_item_pricing_table(1).item; -- There should only be one item in the passed object,
                                           -- so there's no reason to set this local var in the loop.

   for i in L_first..L_last loop
      ---
      if I_item_pricing_table(i).base_retail_ind = rpm_base_zone_ind then
         L_base_unit_retail := I_item_pricing_table(i).unit_retail;
         L_base_selling_ur  := I_item_pricing_table(i).selling_unit_retail;
         L_base_selling_uom := I_item_pricing_table(i).selling_uom;
         L_base_multi_units := I_item_pricing_table(i).multi_units;
         L_base_multi_ur    := I_item_pricing_table(i).multi_unit_retail;
         L_base_multi_s_uom := I_item_pricing_table(i).multi_selling_uom;
         L_base_currency_code   := I_item_pricing_table(i).currency_code;
      end if;


      for REC in C_LOCS_BY_ZONE(I_item_pricing_table(i).zone_id) LOOP
         ---
         if SKU_RETAIL_SQL.UPDATE_RETAIL(L_error_message,
                                         I_item_pricing_table(i).item,
                                         GV_orig_values_table(i).unit_retail,
                                         I_item_pricing_table(i).unit_retail,
                                         GV_orig_values_table(i).selling_unit_retail,
                                         I_item_pricing_table(i).selling_unit_retail,
                                         GV_orig_values_table(i).selling_uom,
                                         I_item_pricing_table(i).selling_uom,
                                         I_item_pricing_table(i).multi_units,
                                         I_item_pricing_table(i).multi_unit_retail,
                                         I_item_pricing_table(i).multi_selling_uom,
                                         L_default_to_children,
                                         rec.location_id,
                                         L_dept,
                                         L_class,
                                         L_subclass,
                                         NULL,  -- primary_currency, not used in function
                                         L_pack_ind) = FALSE then
            L_error_message := SQL_LIB.CREATE_MSG('FAILED_CALL',
                                                  'SKU_RETAIL_SQL.UPDATE_RETAIL',
                                                  L_error_message,
                                                  NULL);
            raise_application_error(ERRNUM_PACKAGE_CALL,L_error_message);
         end if;
      end LOOP;
      ---
   end LOOP;

   INSERT_UPDATE_XTRA_RMS_LOCS(L_error_message,
                               L_success,
                               L_item,
                               L_dept,
                               L_class,
                               L_subclass,
                               L_pack_ind,
                               L_default_to_children,
                               L_base_unit_retail,
                               L_base_selling_ur,
                               L_base_selling_uom,
                               L_base_multi_units,
                               L_base_multi_ur,
                               L_base_multi_s_uom,
                               L_base_currency_code);
   if L_success = 'N' then
      L_error_message := SQL_LIB.CREATE_MSG('FAILED_CALL',
                                            'PM_RETAIL_API_SQL.INSERT_UPDATE_XTRA_RMS_LOCS',
                                             L_error_message,
                                             NULL);
      raise_application_error(ERRNUM_PACKAGE_CALL,L_error_message);
   end if;

   --- Instantiate the local pricing table object
   L_obj_item_pricing_table := OBJ_ITEM_PRICING_TBL();
   L_obj_item_pricing_table.extend(I_item_pricing_Table.count);

   for i in 1..I_item_pricing_table.count LOOP
      L_obj_item_pricing_table(i) := OBJ_ITEM_PRICING_REC(I_item_pricing_table(i).item,
                                                          I_item_pricing_table(i).rpm_zone_group_id,
                                                          I_item_pricing_table(i).zone_id,
                                                          I_item_pricing_table(i).zone_display_id,
                                                          I_item_pricing_table(i).zone_description,
                                                          I_item_pricing_table(i).unit_retail,
                                                          I_item_pricing_table(i).selling_unit_retail,
                                                          NVL(I_item_pricing_table(i).selling_uom,L_default_standard_uom),
                                                          I_item_pricing_table(i).multi_units,
                                                          I_item_pricing_table(i).multi_unit_retail,
                                                          I_item_pricing_table(i).multi_selling_uom,
                                                          I_item_pricing_table(i).base_retail_ind,
                                                          I_item_pricing_table(i).currency_code,
                                                          NVL(I_item_pricing_table(i).standard_uom,L_default_standard_uom),
                                                          I_item_pricing_table(i).area_diff_zone_ind);

   end LOOP;

   MERCH_RETAIL_API_SQL.SET_ITEM_PRICING_INFO(L_error_message,
                                              L_success,
                                              L_obj_item_pricing_table);

   if L_success = 'N' then
      L_error_message := SQL_LIB.CREATE_MSG('FAILED_CALL',
                                            'MERCH_RETAIL_API_SQL.SET_ITEM_PRICING_INFO',
                                            L_error_message,
                                            NULL);
      raise_application_error(ERRNUM_PACKAGE_CALL,L_error_message);
   end if;

   if L_default_to_children = 'Y' then
      --- call the rpm update function for all the child items
      for rec in C_CHILDREN LOOP
         for i in 1..L_obj_item_pricing_table.count LOOP
            L_obj_item_pricing_table(i).item := rec.child_item;
         end LOOP;

         MERCH_RETAIL_API_SQL.SET_ITEM_PRICING_INFO(L_error_message,
                                                    L_success,
                                                    L_obj_item_pricing_table);

         if L_success = 'N' then
            L_error_message := SQL_LIB.CREATE_MSG('FAILED_CALL',
                                                  'MERCH_RETAIL_API_SQL.SET_ITEM_PRICING_INFO',
                                                  L_error_message,
                                                  NULL);
            raise_application_error(ERRNUM_PACKAGE_CALL,L_error_message);
         end if;
      end LOOP;
   end if;

   return;
EXCEPTION
   when ERROR_PACKAGE_CALL then
      raise;
   when ERROR_INVALID_PARAM then
      raise;
   when ERROR_OTHERS then
      raise;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      raise_application_error(ERRNUM_OTHERS,L_error_message);
END SET_ITEM_PRICING_INFO;

--------------------------------------------------------------------------------
FUNCTION SET_CHILD_ITEM_PRICING_INFO
(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
 I_parent_item               IN      ITEM_MASTER.ITEM%TYPE,
 I_zone_id                   IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE,
 I_child_item_pricing_table  IN      CHILD_ITEM_PRICING_TABLE
)
RETURN BOOLEAN IS

   L_program              VARCHAR2(61) := 'PM_RETAIL_API_SQL.SET_CHILD_ITEM_PRICING_INFO';
   L_success              VARCHAR2(1)  := 'Y';
   L_obj_child_price_tbl  OBJ_CHILD_ITEM_PRICING_TBL;
   L_rpm_ind              VARCHAR2(1):=NULL;
   -----
   cursor C_RPM_IND 
     IS 
   select RPM_IND from system_options; 
BEGIN
   open c_rpm_ind;
   fetch c_rpm_ind into L_rpm_ind;
   close c_rpm_ind;
   ---
   
   if L_rpm_ind = 'Y' then 
       
      if I_child_item_pricing_table is NULL or I_child_item_pricing_table.count = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_item_pricing_table',
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      --   
      L_obj_child_price_tbl := OBJ_CHILD_ITEM_PRICING_TBL();
      L_obj_child_price_tbl.extend(I_child_item_pricing_table.COUNT);

      for i in 1 .. I_child_item_pricing_table.COUNT LOOP
          L_obj_child_price_tbl(i) := OBJ_CHILD_ITEM_PRICING_REC(I_child_item_pricing_table(i).child_item,
                                                                 I_child_item_pricing_table(i).unit_retail,
                                                                 I_child_item_pricing_table(i).standard_uom,
                                                                 I_child_item_pricing_table(i).currency_code,
                                                                 I_child_item_pricing_table(i).selling_unit_retail,
                                                                 I_child_item_pricing_table(i).selling_uom);


      end LOOP;
      MERCH_RETAIL_API_SQL.SET_CHILD_ITEM_PRICING_INFO(O_error_message,
                                                       L_success,
                                                       I_parent_item,
                                                       I_zone_id,
                                                       L_obj_child_price_tbl);
      if L_success = 'N' then 
         return FALSE;
      end if;
   else 
      return TRUE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_CHILD_ITEM_PRICING_INFO;

--------------------------------------------------------------------------------
FUNCTION LOCK_PRICING_RECORDS
(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
 O_lock_success   IN OUT  BOOLEAN,
 I_item           IN      ITEM_MASTER.ITEM%TYPE
)
RETURN BOOLEAN IS
   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.LOCK_PRICING_RECORDS';
   L_success      VARCHAR2(1)  := 'Y';
   L_rpm_ind      varchar2(1):=NULL;
   cursor C_RPM_IND  is 
      select RPM_IND 
	from system_options;
	
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open c_rpm_ind;
   fetch c_rpm_ind into L_rpm_ind;
   close c_rpm_ind;     
   
   if L_rpm_ind = 'Y' then 
      MERCH_RETAIL_API_SQL.LOCK_PRICING_RECORDS(O_error_message,
                                                L_success,
                                                I_item);
      if L_success = 'N' then
         O_lock_success := FALSE;
         O_error_message := SQL_LIB.CREATE_MSG('FAILED_CALL',
                                               'MERCH_RETAIL_API_SQL.LOCK_PRICING_RECORDS',
                                                NULL,
                                                NULL);
         raise_application_error(ERRNUM_PACKAGE_CALL,O_error_message);
      end if;
      O_lock_success := TRUE;
   else                                           
      o_lock_success:=TRUE;
   end if  ;                                      
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END LOCK_PRICING_RECORDS;
--------------------------------------------------------------------------------
PROCEDURE APPLY_VAT(O_error_message                OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_success                      OUT  VARCHAR2,
                    IO_obj_item_pricing_table   IN OUT  NOCOPY OBJ_ITEM_PRICING_TBL)
IS

   L_program         VARCHAR2(61) := 'PM_RETAIL_API_SQL.APPLY_VAT';
   L_vdate           PERIOD.VDATE%TYPE := TO_CHAR(GET_VDATE);
   L_initial_count   NUMBER(10) := 0;
   L_tax_ind         VARCHAR2(1);
   L_item            ITEM_MASTER.ITEM%TYPE;

   L_obj_tax_calc_rec OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_obj_tax_calc_tbl OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();

   cursor C_CHECK_ITEM_VAT is
      select OIPT.ITEM
      from table(cast(IO_OBJ_ITEM_PRICING_TABLE as OBJ_ITEM_PRICING_TBL)) OIPT
      where rownum = 1;

   cursor C_CREATE_SVAT_TAX_CALC_TBL is
      select OBJ_TAX_CALC_REC(m.item,
                              m.pack_ind,
                              zone_locs.loc,  --I_from_entity
                              zone_locs.loc_type,  --I_from_entity_type
                              NULL,  --I_TO_ENTITY,
                              NULL,  --I_TO_ENTITY_TYPE,
                              NULL,  --I_EFFECTIVE_FROM_DATE,
                              m.unit_retail,  --I_AMOUNT,
                              zones.currency_code,  --I_AMOUNT_CURR,
                              'N',   --I_AMOUNT_TAX_INCL_IND,
                              NULL,  --I_origin_country_id
                              NULL,  --O_CUM_TAX_PCT,
                              NULL,  --O_CUM_TAX_VALUE,
                              NULL,  --O_TOTAL_TAX_AMOUNT,
                              NULL,  --O_TOTAL_TAX_AMOUNT_CURR,
                              NULL,  --O_TOTAL_RECOVER_AMOUNT,
                              NULL,  --O_TOTAL_RECOVER_AMOUNT_CURR,
                              NULL,  --O_TAX_DETAIL_TBL
                              'RPMRETAIL',  --I_TRAN_TYPE
                              L_vdate,  --I_TRAN_DATE
                              NULL,  --I_TRAN_ID
                              'R')   --I_COST_RETAIL_IND
        from retail_calc_temp m,
             rpm_zone zones,  -- zone_id is the primary key
             (select rzl.zone_id,
                     l.vat_region,
                     min(l.loc) loc,
                     l.loc_type
                from rpm_zone_location rzl,
                     (select store loc, 'ST' loc_type, vat_region
                        from store
                      union
                      select wh loc, 'WH' loc_type, vat_region
                        from wh) l
               where l.loc = rzl.location
               group by rzl.zone_id, vat_region, l.loc_type) zone_locs
       where m.zone_id = zones.zone_id
         and zones.zone_id = zone_locs.zone_id;

   cursor C_CREATE_GTAX_TAX_CALC_TBL is
     select OBJ_TAX_CALC_REC(m.item,
                             m.pack_ind,
                             rzl.location,        --I_from_entity
                             location.loc_type,   --I_from_entity_type
                             NULL,                --I_TO_ENTITY,
                             NULL,                --I_TO_ENTITY_TYPE,
                             L_vdate,             --I_EFFECTIVE_FROM_DATE,
                             m.unit_retail_prim,  --I_AMOUNT,
                             rz.currency_code,    --I_AMOUNT_CURR,
                             'Y',                 --I_AMOUNT_TAX_INCL_IND,
                             NULL,                --I_origin_country_id
                             NULL,                --O_CUM_TAX_PCT,
                             NULL,                --O_CUM_TAX_VALUE,
                             NULL,                --O_TOTAL_TAX_AMOUNT,
                             NULL,                --O_TOTAL_TAX_AMOUNT_CURR,
                             NULL,                --O_TOTAL_RECOVER_AMOUNT,
                             NULL,                --O_TOTAL_RECOVER_AMOUNT_CURR,
                             NULL)                --O_TAX_DETAIL_TBL
        from rpm_zone_location rzl,
             rpm_zone rz,
             (select store loc,
                     'ST' loc_type
                from store
               where store_type in ('C','F')
               union all
              select wh loc,
                     'WH' loc_type
                from wh) location,
             retail_calc_temp m
       where rzl.zone_id = m.zone_id
         and rzl.zone_id = rz.zone_id
         and rzl.location = location.loc;

BEGIN
   -- Check if IO_obj_item_pricing_table has records
   if IO_obj_item_pricing_table is NULL or IO_obj_item_pricing_table.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'IO_obj_item_pricing_table',
                                            L_program,
                                            NULL);
      O_success := 'N';
      return;
   end if;

   -- Store record count for checking after retail computations
   L_initial_count := IO_obj_item_pricing_table.COUNT;

   -- Retrieve system options
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            GV_system_options_rec) = FALSE then
      O_success := 'N';
      return;
   end if;

   -- Do not perform further processing if VAT is not enabled in the system
   if GV_system_options_rec.default_tax_type='SALES' then
      O_success := 'Y';
      return;
   end if;

   -- Getting ITEM from IO_OBJ_ITEM_PRICING_TABLE into L_item
   open  C_CHECK_ITEM_VAT;
   fetch C_CHECK_ITEM_VAT into L_item;
   close C_CHECK_ITEM_VAT;

   if TAX_SQL.GET_RETAIL_TAX_IND(O_error_message ,
                                 L_tax_ind ,
                                 L_item) = FALSE then
      O_success := 'N';
      return;
   end if;

   --Do not perform further processing if system's class_level_vat_ind is 'Y' and item class's CLASS_VAT_IND 'N'.
   if L_tax_ind = 'N' then
      O_success := 'Y';
      return;
   end if;

   -- Ensure retail_calc_temp table is empty prior to insert
   delete from retail_calc_temp;

   -- Insert contents of the IO_obj_item_pricing_table collection into the retail_calc_temp table
   FORALL i in IO_obj_item_pricing_table.FIRST..IO_obj_item_pricing_table.LAST
      insert into retail_calc_temp(item,
                                   rpm_zone_group_id,
                                   zone_id,
                                   zone_display_id,
                                   zone_description,
                                   loc_type,
                                   unit_retail,
                                   selling_unit_retail,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   total_cost,
                                   multi_selling_uom,
                                   base_retail_ind,
                                   currency_code,
                                   standard_uom,
                                   selling_mark_up,
                                   multi_selling_mark_up,
                                   default_to_children,
                                   unit_retail_prim,
                                   selling_unit_retail_prim,
                                   multi_unit_retail_prim,
                                   unit_retail_euro,
                                   selling_unit_retail_euro,
                                   multi_unit_retail_euro,
                                   error_message,
                                   return_code,
                                   area_diff_zone_ind,
                                   ---
                                   item_xform_ind,
                                   status,
                                   orderable_ind,
                                   sellable_ind,
                                   dept,
                                   class,
                                   pack_ind,
                                   pack_type,
                                   order_as_type,
                                   notional_pack_ind,
                                   waste_type,
                                   waste_pct,
                                   default_waste_pct,
                                   bud_int,
                                   bud_mkup,
                                   markup_calc_type,
                                   ---
                                   get_vat_ind,
                                   tax_ind,
                                   class_vat_ind,
                                   ---
                                   tax_rate,
                                   tax_amount)
      values(IO_obj_item_pricing_table(i).item,
             IO_obj_item_pricing_table(i).zone_group_id,
             IO_obj_item_pricing_table(i).zone_id,
             IO_obj_item_pricing_table(i).zone_display_id,
             IO_obj_item_pricing_table(i).zone_name,
             NULL, --'Z', -- loc_type - this is always 'Z'(zone) when called from itemretail.fmb
             IO_obj_item_pricing_table(i).unit_retail,
             IO_obj_item_pricing_table(i).selling_unit_retail,
             IO_obj_item_pricing_table(i).selling_uom,
             IO_obj_item_pricing_table(i).multi_units,
             IO_obj_item_pricing_table(i).multi_unit_retail,
             NULL, --NVL(I_total_cost, 0),  -- total_cost
             IO_obj_item_pricing_table(i).multi_selling_uom,
             IO_obj_item_pricing_table(i).base_retail_ind,
             IO_obj_item_pricing_table(i).currency_code,
             IO_obj_item_pricing_table(i).standard_uom,
             NULL,  -- selling_mark_up,
             NULL,  -- multi_selling_mark_up,
             NULL,  -- default_to_children,
             NULL,  -- unit_retail_prim,
             NULL,  -- selling_unit_retail_prim,
             NULL,  -- multi_unit_retail_prim,
             NULL,  -- unit_retail_euro,
             NULL,  -- selling_unit_retail_euro,
             NULL,  -- multi_unit_retail_euro,
             NULL,  -- error_message,
             NULL,  -- return_code,
             IO_obj_item_pricing_table(i).area_diff_zone_ind,
             ---
             NULL,  -- item_xform_ind
             NULL,  -- status
             NULL,  -- orderable_ind
             NULL,  -- sellable_ind
             NULL,  -- dept
             NULL,  -- class
             NULL,  -- pack_ind
             NULL,  -- pack_type
             NULL,  -- order_as_type
             NULL,  -- notional_pack_ind

             NULL,  -- waste_type
             NULL,  -- waste_pct
             NULL,  -- default_waste_pct
             NULL,  -- bud_int
             NULL,  -- bud_mkup
             NULL,  -- markup_calc_type
             ---
             NULL,  -- get_vat_ind
             NULL,  -- tax_ind
             NULL,  -- class_vat_ind
             ---
             NULL,  -- tax_rate
             NULL); -- tax_amount

   -- Retrieve pack_ind from itemmaster for records on retail_calc_temp.
   -- Merge the pack_ind back into the retail_calc_temp table.
   merge into retail_calc_temp mct
   using (
      select im.item,
             m.rpm_zone_group_id,
             m.zone_id,
             m.zone_display_id,
             im.pack_ind
        from retail_calc_temp m,
             item_master im
       where m.item   = im.item) inner
   on (    inner.item              = mct.item
       and inner.rpm_zone_group_id = mct.rpm_zone_group_id
       and inner.zone_id           = mct.zone_id
       and inner.zone_display_id   = mct.zone_display_id)
   when matched then
   update set mct.pack_ind = inner.pack_ind;


   if GV_system_options_rec.default_tax_type = TAX_SQL.LP_SVAT then
      -- Create the tax object to send to the tax engine.
      -- The cursor retrieves locations across the vat_regions associated with the item and its zones.
      -- These dataset is sent to the tax engine from tax computation.
      open C_CREATE_SVAT_TAX_CALC_TBL;
      fetch C_CREATE_SVAT_TAX_CALC_TBL bulk collect into L_obj_tax_calc_tbl;
      close C_CREATE_SVAT_TAX_CALC_TBL;

      if L_obj_tax_calc_tbl is not NULL and L_obj_tax_calc_tbl.COUNT > 0 then
         if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                    L_obj_tax_calc_tbl,
                                    TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
            O_success := 'N';
            return;
         end if;
      end if;

      merge into retail_calc_temp mct
      using (
         -- This statement performs the same calculation done in the SALES/SVAT branch in GET_ZONE_VAT_RATE.
         -- Any changes to that function should be applied here as well for consistency.
         select distinct
                m.item,
                m.zone_id,
                ROUND(m.unit_retail * (1 +
                                       (SUM(NVL(tax_result.o_cum_tax_pct,0) * z.count_vat ) OVER(PARTITION BY z.zone_id) /
                                        SUM(z.count_vat) OVER(PARTITION BY z.zone_id)) / 100), c.currency_rtl_dec) unit_retail,
                ROUND(m.selling_unit_retail * (1 +
                                       (SUM(NVL(tax_result.o_cum_tax_pct,0) * z.count_vat ) OVER(PARTITION BY z.zone_id) /
                                        SUM(z.count_vat) OVER(PARTITION BY z.zone_id)) / 100), c.currency_rtl_dec) selling_unit_retail
           from TABLE(CAST(L_obj_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_result,
                (select rzl.zone_id,
                        l.vat_region,
                        COUNT(*) count_vat,
                        min(l.loc) loc,
                        l.loc_type
                   from rpm_zone_location rzl,
                        (select store loc, 'ST' loc_type, vat_region
                           from store
                         union
                         select wh loc, 'WH' loc_type, vat_region
                           from wh) l
                  where l.loc = rzl.location
                  group by rzl.zone_id, vat_region, l.loc_type) z,
                retail_calc_temp m,
                currencies c
          where tax_result.I_from_entity = z.loc
            and z.zone_id         = m.zone_id
            and tax_result.I_item = m.item
            and m.currency_code   = c.currency_code) inner
      on (    inner.item    = mct.item
          and inner.zone_id = mct.zone_id)
      when matched then
      update set mct.unit_retail         = NVL(inner.unit_retail, 0),
                 mct.selling_unit_retail = NVL(inner.selling_unit_retail, 0);

      -- Reset table
      L_obj_tax_calc_tbl.DELETE;
   end if;


   if GV_system_options_rec.default_tax_type in (TAX_SQL.LP_GTAX) then
      -- Create the tax object to send to the tax engine.
      open C_CREATE_GTAX_TAX_CALC_TBL;
      fetch C_CREATE_GTAX_TAX_CALC_TBL bulk collect into L_obj_tax_calc_tbl;
      close C_CREATE_GTAX_TAX_CALC_TBL;

      -- Return an errors if the tax object was not created or is empty
      if L_obj_tax_calc_tbl is NULL or L_obj_tax_calc_tbl.COUNT = 0 then
         O_error_message := SQL_LIB.CREATE_MSG ('NO_LOCS_IN_RETAIL_ZONES',
                                                NULL,
                                                NULL,
                                                NULL);
         O_success := 'N';
         return;
      end if;

      -- Call the tax engine to compute tax for initial retail
      if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                 L_obj_tax_calc_tbl,
                                 TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
         O_success := 'N';
         return;
      end if;

      if L_obj_tax_calc_tbl is not NULL or L_obj_tax_calc_tbl.COUNT > 0 then
         merge into retail_calc_temp mc
         using (
            select mct.item,
                   mct.zone_id,
                   ROUND(((mct.unit_retail / (1 - result.tax_rate/100)) + result.tax_amount), c.currency_rtl_dec) unit_retail,
                   ROUND(((mct.selling_unit_retail / (1 - result.tax_rate/100)) + result.tax_amount), c.currency_rtl_dec) selling_unit_retail
              from retail_calc_temp mct,
                   (select m.item,
                           m.zone_id,
                           SUM(NVL(tax_result.o_cum_tax_pct,0)) / COUNT(*) as tax_rate,  -- average the cum_tax_pct
                           SUM(tax_result.o_cum_tax_value) / COUNT(*) as tax_amount -- average the cum_tax_valueyeah
                      from TABLE(CAST(L_obj_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_result,
                           rpm_zone_location rzl,
                           retail_calc_temp m
                     where tax_result.I_from_entity = rzl.location
                       and rzl.zone_id = m.zone_id
                     group by m.item,
                              m.zone_id) result,
                   currencies c
             where mct.item = result.item
               and mct.zone_id = result.zone_id
               and mct.currency_code = c.currency_code) inner
         on (    inner.item    = mc.item
             and inner.zone_id = mc.zone_id)
         when matched then
         update set mc.unit_retail         = NVL(inner.unit_retail, 0),
                    mc.selling_unit_retail = NVL(inner.selling_unit_retail, 0);
      end if;
   end if;

   -- At this point the retail_calc_temp table will have updated unit_retail and selling_unit_retail values
   -- Transfer contents of retail_calc_temp to IO_obj_item_pricing_table
   IO_obj_item_pricing_table.DELETE;
   select OBJ_ITEM_PRICING_REC(item,
                               rpm_zone_group_id,
                               zone_id,
                               zone_display_id,
                               zone_description,
                               unit_retail,
                               selling_unit_retail,
                               selling_uom,
                               multi_units,
                               multi_unit_retail,
                               multi_selling_uom,
                               base_retail_ind,
                               currency_code,
                               standard_uom,
                               area_diff_zone_ind)
   BULK COLLECT INTO IO_obj_item_pricing_table
     from retail_calc_temp;

   if IO_obj_item_pricing_table.COUNT != L_initial_count then
      O_error_message := SQL_LIB.CREATE_MSG('RETAIL_TAX_APPLY_ERROR',
                                            NULL,
                                            NULL,
                                            NULL);
      O_success := 'N';
      return;
   end if;

   delete from retail_calc_temp;

   O_success := 'Y';

   return;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END APPLY_VAT;
--------------------------------------------------------------------------------
FUNCTION MERCH_PRICING_DEFS_EXIST(O_error_message       IN OUT  VARCHAR2,
                                  O_success             IN OUT  VARCHAR2,
                                  I_dept                IN      DEPS.DEPT%TYPE,
                                  I_class               IN      CLASS.CLASS%TYPE,
                                  I_subclass            IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.MERCH_PRICING_DEFS_EXIST';
   L_rpm_ind      VARCHAR2(1)  :=  NULL;
   cursor C_RPM_IND is 
      select RPM_IND 
	 from system_options;
 
BEGIN
   ---
   open C_RPM_IND;
   fetch C_RPM_IND into L_rpm_ind;
   close C_RPM_IND;
   ---
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
    
   --
   if L_rpm_ind = 'Y' then   
      MERCH_RETAIL_API_SQL.MERCH_PRICING_DEFS_EXIST(O_error_message,
                                                    O_success,
                                                    I_dept,
                                                    I_class,
                                                    I_subclass);
      if O_success = 'N' then
         return FALSE;
      end if;
      return TRUE;
   else      
      O_success:='Y';   
      return TRUE;
   end if;                 
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MERCH_PRICING_DEFS_EXIST;
--------------------------------------------------------------------------------
FUNCTION GET_RPM_PRICING_WRAPPER(O_error_message          OUT  VARCHAR2,
                                 O_item_pricing_table     OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
                                 I_item_parent         IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                 I_item                IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                 I_dept                IN      ITEM_MASTER.DEPT%TYPE,
                                 I_class               IN      ITEM_MASTER.CLASS%TYPE,
                                 I_subclass            IN      ITEM_MASTER.SUBCLASS%TYPE,
                                 I_cost_currency_code  IN      STORE.CURRENCY_CODE%TYPE,
                                 I_cost                IN      NUMBER)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_RPM_PRICING_WRAPPER';
   L_success      VARCHAR2(30);
BEGIN

   MERCH_RETAIL_API_SQL.GET_ITEM_PRICING_INFO(O_error_message,
                                              L_success,
                                              O_item_pricing_table,
                                              I_item_parent,
                                              I_item,
                                              I_dept,
                                              I_class,
                                              I_subclass,
                                              I_cost_currency_code,
                                              I_cost);

   if L_success = 'N' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RPM_PRICING_WRAPPER;
--------------------------------------------------------------------------------
FUNCTION SET_RPM_PRICING_WRAPPER(O_error_message          OUT  VARCHAR2,
                                 I_item_pricing_table  IN      OBJ_ITEM_PRICING_TBL)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.SET_RPM_PRICING_WRAPPER';
   L_success      VARCHAR2(30);
   L_default_standard_uom    SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE;
   L_obj_item_pricing_table  OBJ_ITEM_PRICING_TBL;

   cursor C_SYSTEM_OPTIONS is
      select default_standard_uom
        from system_options;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   open  C_SYSTEM_OPTIONS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   fetch C_SYSTEM_OPTIONS into L_default_standard_uom;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   close C_SYSTEM_OPTIONS;

   --- Instantiate the local pricing table object
   L_obj_item_pricing_table := OBJ_ITEM_PRICING_TBL();
   L_obj_item_pricing_table.extend(I_item_pricing_Table.count);
   L_obj_item_pricing_table := I_item_pricing_table;

   for i in 1..I_item_pricing_table.count LOOP
      L_obj_item_pricing_table(i).selling_uom:=nvl(I_item_pricing_table(i).selling_uom,L_default_standard_uom);
      L_obj_item_pricing_table(i).standard_uom:=nvl(I_item_pricing_table(i).standard_uom,L_default_standard_uom);
   end LOOP;

   MERCH_RETAIL_API_SQL.SET_ITEM_PRICING_INFO(O_error_message,
                                              L_success,
                                              L_obj_item_pricing_table);

   if L_success = 'N' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_RPM_PRICING_WRAPPER;
--------------------------------------------------------------------------------
FUNCTION BUILD_PRICING_OBJECT_WRAPPER(O_error_message           OUT  VARCHAR2,
                                      O_item_pricing_table      OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
                                      I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                                      I_dept                 IN      DEPS.DEPT%TYPE,
                                      I_class                IN      CLASS.CLASS%TYPE,
                                      I_subclass             IN      SUBCLASS.SUBCLASS%TYPE,
                                      I_price_currency_code  IN      CURRENCIES.CURRENCY_CODE%TYPE,
                                      I_retail_price         IN      NUMBER)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.BUILD_PRICING_OBJECT_WRAPPER';
   L_success      VARCHAR2(30);
BEGIN

   MERCH_RETAIL_API_SQL.BUILD_ITEM_PRICING_OBJECT
             (O_error_message,
              L_success,
              O_item_pricing_table,
              I_item,
              I_dept,
              I_class,
              I_subclass,
              I_price_currency_code,
              I_retail_price
             );

   if L_success = 'N' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_PRICING_OBJECT_WRAPPER;
--------------------------------------------------------------------------------
FUNCTION CHECK_RETAIL_EXISTS (O_error_message     OUT  VARCHAR2,
                              O_exists            OUT  VARCHAR2,
                              I_item              IN   ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.CHECK_RETAIL_EXISTS';
   L_success VARCHAR2(1) := 'N';
   L_rpm_ind      varchar2(1):=NULL;
   --
   cursor C_RPM_IND 
     is
   select RPM_IND from system_options;
   --
   cursor C_RETAIL_EXISTS is
      select 'Y'
        from dual
       where exists (select im.item
                       from item_master im
                      where im.item = I_item
					    and im.curr_selling_unit_retail is not null
						and im.curr_selling_uom         is not null );
BEGIN
   open c_rpm_ind;
   fetch c_rpm_ind into L_rpm_ind;
   close c_rpm_ind;

   if L_rpm_ind = 'Y' then
      MERCH_RETAIL_API_SQL.CHECK_RETAIL_EXISTS( O_error_message,
                                                L_success,
                                                O_exists,
                                                I_item);
     
      if L_success = 'N' then
         return FALSE;
      end if;	  
   else
      O_exists := 'N';
	  open C_RETAIL_EXISTS;
      fetch C_RETAIL_EXISTS into O_exists;
      close C_RETAIL_EXISTS;
      L_success := 'Y';
   end if;  

   return TRUE;
	
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_RETAIL_EXISTS;
--------------------------------------------------------------------------------
FUNCTION COMPARE_RETAIL_DEF(O_error_message           OUT  VARCHAR2,
                            O_equal                   OUT  VARCHAR2,
                            I_new_dept             IN      DEPS.DEPT%TYPE,
                            I_new_class            IN      CLASS.CLASS%TYPE,
                            I_new_subclass         IN      SUBCLASS.SUBCLASS%TYPE,
                            I_old_dept             IN      DEPS.DEPT%TYPE,
                            I_old_class            IN      CLASS.CLASS%TYPE,
                            I_old_subclass         IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.COMPARE_RETAIL_DEF';
   L_success      VARCHAR2(30);
BEGIN

   MERCH_RETAIL_API_SQL.COMPARE_RETAIL_DEF(O_error_message,
                                           L_success,
                                           O_equal,
                                           I_new_dept,
                                           I_new_class,
                                           I_new_subclass,
                                           I_old_dept,
                                           I_old_class,
                                           I_old_subclass);

   if L_success = 'N' then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END COMPARE_RETAIL_DEF;
--------------------------------------------------------------------------------
FUNCTION DELETE_RPM_ITEM_ZONE_PRICE(O_error_message           OUT  VARCHAR2,
                                    I_item                 IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.DELETE_RPM_ITEM_ZONE_PRICE';
   L_success      VARCHAR2(30);
   L_rpm_ind      varchar2(1):=NULL;
   cursor C_RPM_IND is
      select RPM_IND 
	 from system_options;
BEGIN
   open C_RPM_IND;
   fetch C_RPM_IND into L_rpm_ind;
   close C_RPM_IND;
   
   if L_RPM_IND = 'Y' then  
      MERCH_RETAIL_API_SQL.DELETE_RPM_ITEM_ZONE_PRICE(O_error_message,
                                                      L_success,
                                                      I_item);
  
      if L_success = 'N' then
         return FALSE;
      end if;
	  return TRUE;
   else	 
      return TRUE;
   end if; 
  
--   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END DELETE_RPM_ITEM_ZONE_PRICE;
--------------------------------------------------------------------------------
FUNCTION GET_RPM_MARKUP_INFO(O_error_message        IN OUT  VARCHAR2,
                             O_markup_calc_type     IN OUT  DEPS.MARKUP_CALC_TYPE%TYPE,
                             O_markup_percent       IN OUT  NUMBER,
                             I_dept                 IN      DEPS.DEPT%TYPE,
                             I_class                IN      CLASS.CLASS%TYPE,
                             I_subclass             IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_RPM_MARKUP_INFO';
   L_zone_grp               PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE;
   L_rpm_calc_type          VARCHAR2(1);
   L_bud_mkup               DEPS.BUD_MKUP%TYPE;
   L_bud_int                DEPS.BUD_INT%TYPE;
   L_rpm_ind                varchar2(1):=NULL;
   L_currency_code          STORE.CURRENCY_CODE%TYPE;
   L_dept_calc_type         DEPS.MARKUP_CALC_TYPE%TYPE;
   cursor C_RPM_IND is 
     select RPM_IND 
	from system_options;
BEGIN
   --
   open c_rpm_ind;
   fetch c_rpm_ind into L_rpm_ind;
   close c_rpm_ind; 
   --
   if L_rpm_ind = 'N' then
      if DEPT_ATTRIB_SQL.GET_MARKUP(O_error_message,
                                    L_dept_calc_type,
                                    L_bud_int,
                                    L_bud_mkup,
                                    I_dept) = FALSE then
         return FALSE;
      end if;
	  
      if L_dept_calc_type = 'R' then
         L_rpm_calc_type:=1;
		 O_markup_percent:=L_bud_int;
      else
         L_rpm_calc_type:=0;
		 O_markup_percent:=L_bud_mkup;
      end if;
		  
   else 
       
       -- 	
       if MERCH_RETAIL_API_SQL.GET_MERCH_PRICING_DEFS(O_error_message,
                                                      I_dept,
                                                      I_class,
                                                      I_subclass,     
                                                      L_zone_grp, -- zone group
                                                      L_zone_grp, -- clearance zone group
                                                      L_rpm_calc_type,
                                                      O_markup_percent) = FALSE then      
          return FALSE;
       end if;
   end if;

   if L_rpm_calc_type = 0 then  -- Cost
      O_markup_calc_type := 'C';
   elsif L_rpm_calc_type = 1 then -- Retail
      O_markup_calc_type := 'R';
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_MKUP_CALC_TYPE',
                                            L_rpm_calc_type,
                                            NULL,
                                            NULL);
       return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RPM_MARKUP_INFO;
--------------------------------------------------------------------------------
FUNCTION GET_RPM_ITEM_ZONE_PRC_WRAPPER(O_error_message                IN OUT   VARCHAR2,
                                       I_item                         IN       RPM_ITEM_ZONE_PRICE.ITEM%TYPE,
                                       I_zone_id                      IN       RPM_ITEM_ZONE_PRICE.ZONE_ID%TYPE,
                                       O_standard_retail              IN OUT   RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                                       O_standard_retail_currency     IN OUT   RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL_CURRENCY%TYPE,
                                       O_standard_uom                 IN OUT   RPM_ITEM_ZONE_PRICE.STANDARD_UOM%TYPE,
                                       O_selling_retail               IN OUT   RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE,
                                       O_selling_retail_currency      IN OUT   RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                                       O_selling_uom                  IN OUT   RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                                       O_multi_units                  IN OUT   RPM_ITEM_ZONE_PRICE.MULTI_UNITS%TYPE,
                                       O_multi_unit_retail            IN OUT   RPM_ITEM_ZONE_PRICE.MULTI_UNIT_RETAIL%TYPE,
                                       O_multi_unit_retail_currency   IN OUT   RPM_ITEM_ZONE_PRICE.MULTI_UNIT_RETAIL_CURRENCY%TYPE,
                                       O_multi_selling_uom            IN OUT   RPM_ITEM_ZONE_PRICE.MULTI_SELLING_UOM%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_RPM_ITEM_ZONE_PRC_WRAPPER';
BEGIN

   if MERCH_RETAIL_API_SQL.GET_RPM_ITEM_ZONE_PRICE(O_error_message,
                                                   I_item,
                                                   I_zone_id,
                                                   O_standard_retail,
                                                   O_standard_retail_currency,
                                                   O_standard_uom,
                                                   O_selling_retail,
                                                   O_selling_retail_currency,
                                                   O_selling_uom,
                                                   O_multi_units,
                                                   O_multi_unit_retail,
                                                   O_multi_unit_retail_currency,
                                                   O_multi_selling_uom) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RPM_ITEM_ZONE_PRC_WRAPPER;
--------------------------------------------------------------------------------
FUNCTION SET_GTT_ZONE_INFO(O_error_message        IN OUT  VARCHAR2,
                           O_rpm_zone_group_id    IN OUT  RPM_ZONE.ZONE_GROUP_ID%TYPE,
                           O_rpm_base_zone        IN OUT  RPM_ZONE.ZONE_ID%TYPE,
                           O_rpm_display_zone_id  IN OUT  RPM_ZONE.ZONE_DISPLAY_ID%TYPE,
                           I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                           I_dept                 IN      DEPS.DEPT%TYPE,
                           I_class                IN      CLASS.CLASS%TYPE,
                           I_subclass             IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(61) := 'PM_RETAIL_API_SQL.SET_GTT_ZONE_INFO';
   L_success               VARCHAR2(30);
   L_dummy_currency_code   CURRENCIES.CURRENCY_CODE%TYPE := ' ';
   L_dummy_price           NUMBER := 0;
   L_item_pricing_table    OBJ_ITEM_PRICING_TBL;
   L_rpm_ind      varchar2(1):=NULL;
   cursor C_RPM_IND  is 
      select RPM_IND 
	     from system_options;
   cursor C_ZONE_INFO is
     select zone_group_id,
            zone_id,
            zone_display_id
       from gtt_zone_info
      where base_zone_ind = 1
        and rownum = 1;

BEGIN
   --
   open C_RPM_IND;
   fetch C_RPM_IND into L_rpm_ind;
   close C_RPM_IND;
   --
   if L_rpm_ind = 'Y' then 
      MERCH_RETAIL_API_SQL.BUILD_ITEM_PRICING_OBJECT(O_error_message,
                                                     L_success,
                                                     L_item_pricing_table,
                                                     I_item,
                                                     I_dept,
                                                     I_class,
                                                     I_subclass,
                                                     L_dummy_currency_code,
                                                     L_dummy_price);

      if L_success = 'N' then
         return FALSE;
      end if;
   
      --- Move stuff from pricing objects into the global temp tables
      insert into GTT_ZONE_INFO
                  (zone_group_id,
                   zone_id,
                   zone_display_id,
                   zone_desc,
                   currency_code,
                   base_zone_ind)
           select  zone_group_id,
                   zone_id,
                   zone_display_id,
                   zone_name,
                   currency_code,
                   base_retail_ind
             from TABLE(cast(L_item_pricing_table as OBJ_ITEM_PRICING_TBL));

      open C_ZONE_INFO;
      fetch C_ZONE_INFO into O_rpm_zone_group_id,
                             O_rpm_base_zone,
                             O_rpm_display_zone_id;
      close C_ZONE_INFO;
   else    
      O_rpm_zone_group_id   := NULL;
	  O_rpm_base_zone       := NULL;
	  O_rpm_display_zone_id := NULL;
   end if;   

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SET_GTT_ZONE_INFO;
--------------------------------------------------------------------------------
FUNCTION GET_BASE_RETAIL(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_selling_retail       IN OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                         O_selling_uom          IN OUT  ITEM_LOC.SELLING_UOM%TYPE,
                         O_multi_units          IN OUT  ITEM_LOC.MULTI_UNITS%TYPE,
                         O_multi_unit_retail    IN OUT  ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                         O_multi_selling_uom    IN OUT  ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                         O_currency_code        IN OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                         I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                         I_dept                 IN      DEPS.DEPT%TYPE,
                         I_class                IN      CLASS.CLASS%TYPE,
                         I_subclass             IN      SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_BASE_RETAIL';
   L_success               VARCHAR2(30);
   L_invalid_param         VARCHAR2(30) := NULL;
   L_rpm_base_zone         PM_RETAIL_API_SQL.ZONE_ID%TYPE;
   L_rpm_zone_group_id     PM_RETAIL_API_SQL.ZONE_ID%TYPE;
   L_locs_table            PM_RETAIL_API_SQL.LOCATION_TABLE;

   BEGIN
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_dept is NULL then
      L_invalid_param := 'I_dept';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_class';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_subclass';
   end if;

   if L_invalid_param is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('REQUIRED_INPUT_IS_NULL',
                                             L_invalid_param,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;


   if MERCH_RETAIL_API_SQL.GET_BASE_ZONE_RETAIL(O_error_message,
                                                L_rpm_zone_group_id,
                                                L_rpm_base_zone,
                                                O_selling_retail,
                                                O_selling_uom,
                                                O_multi_units,
                                                O_multi_unit_retail,
                                                O_multi_selling_uom,
                                                O_currency_code,
                                                I_item,
                                                I_dept,
                                                I_class,
                                                I_subclass) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_BASE_RETAIL;
-------------------------------------------------------------------------------------------
FUNCTION GET_NEW_ITEM_LOC_RETAIL(O_error_message        IN OUT  VARCHAR2,
                                 O_selling_retail       IN OUT  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                 O_selling_uom          IN OUT  ITEM_LOC.SELLING_UOM%TYPE,
                                 O_multi_units          IN OUT  ITEM_LOC.MULTI_UNITS%TYPE,
                                 O_multi_unit_retail    IN OUT  ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                 O_multi_selling_uom    IN OUT  ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                 I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                                 I_dept                 IN      DEPS.DEPT%TYPE,
                                 I_class                IN      CLASS.CLASS%TYPE,
                                 I_subclass             IN      SUBCLASS.SUBCLASS%TYPE,
                                 I_location             IN      ITEM_LOC.LOC%TYPE,
                                 I_loc_type             IN      ITEM_LOC.LOC_TYPE%TYPE,
                                 I_unit_cost            IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                 I_unit_cost_curr_code  IN      CURRENCIES.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(61) := 'PM_RETAIL_API_SQL.GET_NEW_ITEM_LOC_RETAIL';
   L_invalid_param         VARCHAR2(30) := NULL;
   L_currency_code         CURRENCIES.CURRENCY_CODE%TYPE;
   L_loc_currency_code     CURRENCIES.CURRENCY_CODE%TYPE;
   L_selling_retail        ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_multi_unit_retail     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_dummy                 BOOLEAN;
   L_unit_cost             ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := I_unit_cost;
   L_unit_cost_curr_code   CURRENCIES.CURRENCY_CODE%TYPE    := I_unit_cost_curr_code;
   L_supplier              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE  := NULL;
   L_regular_zone_group    RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE   := NULL;
   L_clearance_zone_group  RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE := NULL;
   L_markup_calc_type      RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE     := NULL;
   L_markup_percent        RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE       := NULL;
   L_loc_exists            VARCHAR2(1)                                    := NULL;
   L_retail_exists         VARCHAR2(1)                                    := 'N';
   L_rpm_ind               SYSTEM_OPTIONS.RPM_IND%TYPE;
   --
   cursor C_ITEM_COST is
      select extended_base_cost,
             supplier
        from item_supp_country
       where item = I_item
         and primary_supp_ind = 'Y'
         and primary_country_ind = 'Y';

   cursor C_GET_RPM_IND is
      select rpm_ind
        from system_options;

   cursor C_CHECK_ZONE_LOC_EXISTS is
      select 'x'
        from rpm_zone_location rzl,
             rpm_zone rz
       where rz.zone_id       = rzl.zone_id
         and rzl.location     = I_location
         and rz.zone_group_id = L_regular_zone_group;

BEGIN
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_dept is NULL then
      L_invalid_param := 'I_dept';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_class';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_subclass';
   elsif I_unit_cost is not NULL and I_unit_cost_curr_code is NULL then
      L_invalid_param := 'I_currency';
   end if;

   if L_invalid_param is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('REQUIRED_INPUT_IS_NULL',
                                             L_invalid_param,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   --

   if I_unit_cost is NULL and I_unit_cost_curr_code is NULL then
      -- Needed for location not belonging to primary zone
      SQL_LIB.SET_MARK('OPEN','C_ITEM_COST','ITEM_SUPPLIER','Item: ' || I_item);
      open  C_ITEM_COST;
      SQL_LIB.SET_MARK('FETCH','C_ITEM_COST','ITEM_SUPPLIER','Item: ' || I_ITEM);
      fetch C_ITEM_COST into L_unit_cost, L_supplier;
      ---
      if C_ITEM_COST%NOTFOUND then
         NULL;
      else
         -- Get the supplier currency as the unit cost is in supplier currency
         if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE (O_error_message,
                                               L_unit_cost_curr_code,
                                               L_supplier) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_COST','ITEM_SUPPLIER','Item: ' || I_item);
      close C_ITEM_COST;
   end if;

   open C_GET_RPM_IND;
   fetch C_GET_RPM_IND into L_rpm_ind;
   close C_GET_RPM_IND;

   if L_rpm_ind = 'Y' then

      -- Get Merch Retail Defs
      if MERCH_RETAIL_API_SQL.GET_MERCH_PRICING_DEFS (O_error_message,
                                                      I_dept,
                                                      I_class,
                                                      I_subclass,
                                                      L_regular_zone_group,
                                                      L_clearance_zone_group,
                                                      L_markup_calc_type,
                                                      L_markup_percent) = FALSE then
         return FALSE;
      end if;

      open C_CHECK_ZONE_LOC_EXISTS;
      fetch C_CHECK_ZONE_LOC_EXISTS into L_loc_exists;
      close C_CHECK_ZONE_LOC_EXISTS;

      if L_loc_exists is NULL then
         if CHECK_RETAIL_EXISTS (O_error_message,
                                 L_retail_exists,
                                 I_item) = FALSE then
            return FALSE;
         end if;
      end if;

      if L_loc_exists is NULL and L_retail_exists = 'Y' then
         -- If the location does not exist in RPM item's primary zone group zones, fetch base
         -- retail if retail is defined in RPM for the item.
         if GET_BASE_RETAIL (O_error_message,
                             L_selling_retail,
                             O_selling_uom,
                             O_multi_units,
                             L_multi_unit_retail,
                             O_multi_selling_uom,
                             L_currency_code,
                             I_item,
                             I_dept,
                             I_class,
                             I_subclass) = FALSE then
            return FALSE;
         end if;
      else
         if MERCH_RETAIL_API_SQL.GET_NON_RANGED_ITEM_RETAIL(O_error_message,
                                                            L_selling_retail,
                                                            O_selling_uom,
                                                            O_multi_units,
                                                            L_multi_unit_retail,
                                                            O_multi_selling_uom,
                                                            L_currency_code,
                                                            I_item,
                                                            I_dept,
                                                            I_class,
                                                            I_subclass,
                                                            I_location,
                                                            I_loc_type,
                                                            L_unit_cost,
                                                            L_unit_cost_curr_code) = 0 then
            return FALSE;
         end if;
      end if;
   else

      if RETAIL_API_SQL.CALC_RETAIL(O_error_message,
                                    L_selling_retail,
                                    O_selling_uom,
                                    O_multi_units,
                                    L_multi_unit_retail,
                                    O_multi_selling_uom,
                                    L_currency_code,
                                    I_item,
                                    I_dept,
                                    I_class,
                                    I_subclass,
                                    I_location,
                                    I_loc_type,
                                    L_unit_cost,
									L_unit_cost_curr_code) = FALSE then
         return FALSE;
      end if;
   end if;

   if LOCATION_ATTRIB_SQL.GET_LOCATION_CURRENCY(O_error_message,
                                                L_dummy, -- multi_curr_exist
                                                L_loc_currency_code,
                                                I_loc_type,
                                                I_location) = FALSE then
      return FALSE;
   end if;
   -- Convert the unit retail in location currency
   if L_loc_currency_code != L_currency_code then
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_selling_retail,
                              L_currency_code,
                              L_loc_currency_code,
                              O_selling_retail,
                              'R',
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;

      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_multi_unit_retail,
                              L_currency_code,
                              L_loc_currency_code,
                              O_multi_unit_retail,
                              'R',
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
   else
      O_selling_retail := L_selling_retail;
      O_multi_unit_retail   := L_multi_unit_retail;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_NEW_ITEM_LOC_RETAIL;
--------------------------------------------------------------------------------------------------
-- Any changes due to fixes or functional enhancements to this function should be applied
-- to the APPLY_VAT function in this package.
-- The APPLY_VAT function is a bulk implementation of this function.
--------------------------------------------------------------------------------------------------
FUNCTION GET_ZONE_VAT_RATE(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_vat_rate             IN OUT  VAT_CODE_RATES.VAT_RATE%TYPE,
                           O_tax_amount           IN OUT  GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE,
                           I_retail_zone_id       IN      RPM_ZONE.ZONE_ID%TYPE,
                           I_dept                 IN      ITEM_MASTER.DEPT%TYPE,
                           I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                           I_unit_retail          IN      ITEM_LOC.UNIT_RETAIL%TYPE DEFAULT NULL,
                           I_effective_date       IN      PERIOD.VDATE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61)                  := 'PM_RETAIL_API_SQL.GET_ZONE_VAT_RATE';

   L_tax_pct_sum    VAT_ITEM.VAT_RATE%TYPE              :=0;
   L_tax_amt_sum    ITEM_LOC.UNIT_RETAIL%TYPE           :=0;
   L_counter        NUMBER                              :=0;
   L_sum_count      NUMBER                              :=0;
   L_vat_rate       VAT_CODE_RATES.VAT_RATE%TYPE        :=0;
   L_tax_amount     GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE :=0;

   L_pack_ind       ITEM_MASTER.PACK_IND%TYPE;

   L_obj_tax_calc_rec OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_obj_tax_calc_tbl OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_vdate DATE := get_vdate();

   cursor C_GET_GTAX_BULK is
     select OBJ_TAX_CALC_REC(I_item,
                             L_pack_ind,
                             rzl.location,  --I_from_entity
                             location.loc_type,  --I_from_entity_type
                             NULL,  --I_TO_ENTITY,
                             NULL,  --I_TO_ENTITY_TYPE,
                             L_vdate,  --I_EFFECTIVE_FROM_DATE,
                             I_unit_retail,  --I_AMOUNT,
                             rz.currency_code,  --I_AMOUNT_CURR,
                             'Y',   --I_AMOUNT_TAX_INCL_IND,
                             NULL,  --I_origin_country_id
                             NULL,  --O_CUM_TAX_PCT,
                             NULL,  --O_CUM_TAX_VALUE,
                             NULL,  --O_TOTAL_TAX_AMOUNT,
                             NULL,  --O_TOTAL_TAX_AMOUNT_CURR,
                             NULL,  --O_TOTAL_RECOVER_AMOUNT,
                             NULL,  --O_TOTAL_RECOVER_AMOUNT_CURR,
                             NULL)  --O_TAX_DETAIL_TBL
        from rpm_zone_location rzl,
             rpm_zone rz,
             (select store loc,
                     'ST' loc_type
                from store
               where store_type in ('C','F')
               union all
              select wh loc,
                     'WH' loc_type
                from wh) location
       where rzl.zone_id = I_retail_zone_id
         and rzl.zone_id = rz.zone_id
         and rzl.location = location.loc;

   cursor C_GET_REGION is
      select /*+ INDEX(s) */ vat_region,
             count(*) count_vat,
             min(loc) loc,
             l.loc_type
        from rpm_zone_location rzl,
             (select store loc, 'ST' loc_type, vat_region
                from store
              union
              select wh loc, 'WH' loc_type, vat_region
                from wh) l
       where rzl.zone_id = I_retail_zone_id
         and l.loc     = rzl.location
       group by vat_region, l.loc_type;

   cursor C_IM is
      select pack_ind
        from item_master
      where  item = I_item;

BEGIN
   if I_retail_zone_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('REQUIRED_INPUT_IS_NULL',
                                             I_retail_zone_id,
                                             L_program,
                                             NULL);
      return FALSE;
   end If;

   open C_IM;
   fetch C_IM into L_pack_ind;
   close C_IM;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            GV_system_options_rec) = FALSE then
      return FALSE;
   end if;

   if GV_system_options_rec.default_tax_type = 'SVAT' then
      FOR rec in C_GET_REGION LOOP

         L_counter    := rec.count_vat;

         L_obj_tax_calc_rec := OBJ_TAX_CALC_REC();
         L_obj_tax_calc_tbl := OBJ_TAX_CALC_TBL();
         L_obj_tax_calc_rec.I_item := I_item;
         L_obj_tax_calc_rec.I_pack_ind := L_pack_ind;
         L_obj_tax_calc_rec.I_from_entity := rec.loc;
         L_obj_tax_calc_rec.I_from_entity_type := rec.loc_type;
         L_obj_tax_calc_rec.I_effective_from_date := I_effective_date;  --NULL
         L_obj_tax_calc_rec.I_amount := I_unit_retail;  --unit_retail
         L_obj_tax_calc_rec.I_amount_tax_incl_ind := 'N';
         ---
         L_obj_tax_calc_rec.I_tran_type := 'RPMRETAIL';
         L_obj_tax_calc_rec.I_tran_date := L_vdate;
         L_obj_tax_calc_rec.I_cost_retail_ind := 'R';
         ---
         L_obj_tax_calc_tbl.EXTEND;
         L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT) := L_obj_tax_calc_rec;

         if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                    L_obj_tax_calc_tbl,
                                    TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
            return FALSE;
         end if;

         -- multiply tax rate by number of locations in the same vat_region to derive the weighted average of tax rate
         L_tax_pct_sum := L_tax_pct_sum + (NVL(L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).o_cum_tax_pct,0) * L_counter);
         L_tax_amt_sum := L_tax_amt_sum + (NVL(L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).o_cum_tax_value, 0) * L_counter);
         L_sum_count    := L_sum_count + L_counter;

      end LOOP;

      if L_sum_count = 0 then
         O_vat_rate := 0;
         O_tax_amount := 0;
      else
         O_vat_rate   := L_tax_pct_sum / L_sum_count;
         O_tax_amount := L_tax_amt_sum / L_sum_count;
      end if;

   elsif GV_system_options_rec.default_tax_type = 'GTAX' then
      open C_GET_GTAX_BULK;
      fetch C_GET_GTAX_BULK bulk collect into L_obj_tax_calc_tbl;
      close C_GET_GTAX_BULK;

      if L_obj_tax_calc_tbl is NULL or L_obj_tax_calc_tbl.COUNT <= 0 then
         O_error_message := SQL_LIB.CREATE_MSG ('NO_LOC_IN_RETAIL_ZONE',
                                                I_retail_zone_id,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;

      if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                 L_obj_tax_calc_tbl,
                                 TAX_SQL.LP_CALL_TYPE_INIT_RETAIL) = FALSE then
         return FALSE;
      end if;

      if L_obj_tax_calc_tbl is NULL or L_obj_tax_calc_tbl.COUNT <= 0 then
         O_error_message := SQL_LIB.CREATE_MSG ('TAX_NOT_FOUND',
                                                I_item,
                                                I_retail_zone_id,
                                                NULL);
         return FALSE;
      end if;

      L_sum_count := L_obj_tax_calc_tbl.COUNT;

      if L_sum_count > 0 then
         L_vat_rate := 0;
         L_tax_amount := 0;
         for i in L_obj_tax_calc_tbl.FIRST..L_obj_tax_calc_tbl.LAST LOOP
            L_vat_rate   := L_vat_rate + NVL(L_obj_tax_calc_tbl(i).o_cum_tax_pct,0);
            L_tax_amount := L_tax_amount + L_obj_tax_calc_tbl(i).o_cum_tax_value;
         end LOOP;

         O_vat_rate   := L_vat_rate/ L_sum_count;
         O_tax_amount := L_tax_amount/ L_sum_count;
      else
         O_vat_rate := 0;
         O_tax_amount := 0;
      end if;
   end if;

   if O_vat_rate is NULL then
      O_vat_rate := 0;
      O_tax_amount := 0;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ZONE_VAT_RATE;
--------------------------------------------------------------------------------
FUNCTION GET_BASE_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_zone_id         IN OUT   RPM_ZONE.ZONE_ID%TYPE,
                       I_zone_group_id   IN       RPM_ZONE.ZONE_GROUP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(60) := 'PM_RETAIL_API_SQL.GET_BASE_ZONE';

   cursor C_ZONE_ID IS
      select zone_id
        from rpm_zone
       where zone_group_id = I_zone_group_id
         and base_ind      = 1;

BEGIN
   if I_zone_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_zone_group_id,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ZONE_ID',
                    'RPM_ZONE',
                    NULL);
   open C_ZONE_ID;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_ZONE_ID',
                    'RPM_ZONE',
                    NULL);
   fetch C_ZONE_ID into O_zone_id;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ZONE_ID',
                    'RPM_ZONE',
                    NULL);
   close C_ZONE_ID;
   ---
   if O_zone_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ZONE_GROUP',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_BASE_ZONE;
--------------------------------------------------------------------------------
--- GET_AREA_DIFF_PRICE
--- This function will return the secondary area retail price if the area differential
--- is defined for a department/zone combination.
--------------------------------------------------------------------------------
FUNCTION GET_AREA_DIFF_PRICE
(O_error_message          OUT  VARCHAR2,
 O_selling_retail         OUT  RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
 I_zone_group_id       IN      RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
 I_item                IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
 I_sec_zone_id         IN      RPM_ZONE.ZONE_ID%TYPE,
 I_sec_currency_code   IN      RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
 I_prm_zone_id         IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
 I_prm_selling_retail  IN      RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
 I_prm_selling_uom     IN      RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
 I_prm_currency_code   IN      RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE
)
RETURN BOOLEAN IS
   L_program               VARCHAR2(61)           := 'PM_RETAIL_API_SQL.GET_AREA_DIFF_PRICE';
   L_success               VARCHAR2(1);

BEGIN

   MERCH_RETAIL_API_SQL.GET_AREA_DIFF_PRICE(O_error_message,
                                            L_success,
                                            O_selling_retail,
                                            I_zone_group_id,
                                            I_item,
                                            I_sec_zone_id,
                                            I_sec_currency_code,
                                            I_prm_zone_id,
                                            I_prm_selling_retail,
                                            I_prm_selling_uom,
                                            I_prm_currency_code);

   if L_success != 'Y' then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_AREA_DIFF_PRICE;
--------------------------------------------------------------------------------
FUNCTION GET_PRICING_ZONE_ID (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_zone_id         IN OUT OBJ_ZONE_TBL,
                              O_exists          IN OUT BOOLEAN,
                              I_new_loc         IN     ITEM_LOC.LOC%TYPE,
                              I_new_loc_type    IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_zone_id         IN     ITEM_LOC.LOC%TYPE
                              )
   RETURN BOOLEAN IS
      L_program               VARCHAR2(61)           := 'PM_RETAIL_API_SQL.GET_PRICING_ZONE_ID';

BEGIN
   if MERCH_RETAIL_API_SQL.GET_PRICING_ZONE_ID (O_error_message,
                                                O_zone_id,
                                                I_new_loc,
                                                I_new_loc_type,
                                                I_zone_id) = FALSE then
      return FALSE;
   end if;
   if O_zone_id.COUNT > 0 then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   -- Insert all values from O_zone_id plsql table to global temp table
   if (I_new_loc is NULL and
      I_NEW_LOC_TYPE is NULL and
      I_zone_id is NULL) then
      insert into GTT_ZONE_INFO
                     (zone_id,
                      zone_desc)
              select zone_id,
                     zone_name
                from TABLE(cast(O_zone_id as OBJ_ZONE_TBL));
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PRICING_ZONE_ID;
--------------------------------------------------------------------------------
PROCEDURE SET_ITEM_PRICING_INFO_WRP(I_item_pricing_table IN WRP_ITEM_PRICING_TBL)
IS
   v_data   WRP_ITEM_PRICING_TBL := WRP_ITEM_PRICING_TBL() ;
   pTable   PM_RETAIL_API_SQL.ITEM_PRICING_TABLE;
   pRec     PM_RETAIL_API_SQL.ITEM_PRICING_REC ;
BEGIN
   V_DATA:= I_ITEM_PRICING_TABLE;
   for I IN 1 .. V_DATA.count
   loop
      pRec.item                     := v_data(i).item;
      pRec.rpm_zone_group_id        := v_data(i).rpm_zone_group_id;
      pRec.zone_id                  := v_data(i).zone_id;
      pRec.zone_display_id          := v_data(i).zone_display_id;
      pRec.zone_description         := v_data(i).zone_description;
      pRec.unit_retail              := v_data(i).unit_retail;
      pRec.selling_unit_retail      := v_data(i).selling_unit_retail;
      pRec.selling_uom              := v_data(i).selling_uom;
      pRec.multi_units              := v_data(i).multi_units;
      pRec.multi_unit_retail        := v_data(i).multi_unit_retail;
      pRec.multi_selling_uom        := v_data(i).multi_selling_uom;
      pRec.base_retail_ind          := v_data(i).base_retail_ind;
      pRec.currency_code            := v_data(i).currency_code;
      pRec.standard_uom             := v_data(i).standard_uom;
      pRec.selling_mark_up          := v_data(i).selling_mark_up;
      pRec.multi_selling_mark_up    := v_data(i).multi_selling_mark_up;
      pRec.default_to_children      := v_data(i).default_to_children;
      pRec.unit_retail_prim         := v_data(i).unit_retail_prim;
      pRec.selling_unit_retail_prim := v_data(i).selling_unit_retail_prim;
      pRec.multi_unit_retail_prim   := v_data(i).multi_unit_retail_prim;
      pRec.unit_retail_euro         := v_data(i).unit_retail_euro;
      pRec.selling_unit_retail_euro := v_data(i).selling_unit_retail_euro;
      pRec.multi_unit_retail_euro   := v_data(i).multi_unit_retail_euro;
      pRec.error_message            := v_data(i).error_message;
      pRec.return_code              := v_data(i).return_code;
      pRec.area_diff_zone_ind       := v_data(i).area_diff_zone_ind;
      pTable(i)                     := pRec;
   end loop;
   pm_retail_api_sql.set_item_pricing_info(pTable);
END SET_ITEM_PRICING_INFO_WRP;
----------------------------------------------------------------------------------------------
PROCEDURE GET_ZONE_LOCATIONS_WRP(
    O_location_table    IN OUT WRP_LOCATION_TBL,
    I_rpm_zone_group_id IN PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE,
    I_zone_id           IN PM_RETAIL_API_SQL.ZONE_ID%TYPE )
is
  pTable PM_RETAIL_API_SQL.LOCATION_TABLE;
  v_data WRP_LOCATION_TBL := WRP_LOCATION_TBL();
BEGIN
  pm_retail_api_sql.get_zone_locations(pTable,I_rpm_zone_group_id,I_zone_id);
  for i IN 1 .. pTable.count
  loop
    v_data.extend;
    v_data(i) := WRP_LOCATION_REC ( pTable(i).LOCATION_ID,
                                    pTable(i) .LOCATION_NAME,
                                    pTable(i) .ERROR_MESSAGE,
                                    pTable(i) .RETURN_CODE );
  end loop;
  O_location_table:=v_data;
END GET_ZONE_LOCATIONS_WRP;
----------------------------------------------------------------------------------------------------
PROCEDURE GET_ITEM_PRICING_INFO_WRP(O_item_pricing_table IN OUT WRP_ITEM_PRICING_TBL,
                                    I_item               IN     ITEM_MASTER.ITEM%TYPE,
                                    I_dept               IN     ITEM_MASTER.DEPT%TYPE,
                                    I_class              IN     ITEM_MASTER.CLASS%TYPE,
                                    I_subclass           IN     ITEM_MASTER.SUBCLASS%TYPE,
                                    I_cost_currency_code IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                    I_cost               IN     ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                                    I_called_from_form   IN     VARCHAR2 DEFAULT 'N' )
IS
  pTable PM_RETAIL_API_SQL.ITEM_PRICING_TABLE;
  v_data WRP_ITEM_PRICING_TBL;
BEGIN
  pm_retail_api_sql.get_item_pricing_info(pTable, I_item, I_dept, I_class, I_subclass, I_cost_currency_code, I_cost, I_called_from_form);
  v_data := WRP_ITEM_PRICING_TBL();
  for i IN 1 .. pTable.count
  loop
    v_data.EXTEND;
    v_data(i) := WRP_ITEM_PRICING_REC ( pTable(i).ITEM,
                                        pTable(i) .RPM_ZONE_GROUP_ID,
                                        pTable(i) .ZONE_ID,
                                        pTable(i) .ZONE_DISPLAY_ID,
                                        pTable(i) .ZONE_DESCRIPTION,
                                        pTable(i) .UNIT_RETAIL,
                                        pTable(i) .SELLING_UNIT_RETAIL,
                                        pTable(i) .SELLING_UOM,
                                        pTable(i) .MULTI_UNITS,
                                        pTable(i) .MULTI_UNIT_RETAIL,
                                        pTable(i) .MULTI_SELLING_UOM,
                                        pTable(i) .BASE_RETAIL_IND,
                                        pTable(i) .CURRENCY_CODE,
                                        pTable(i) .STANDARD_UOM,
                                        pTable(i) .SELLING_MARK_UP,
                                        pTable(i) .MULTI_SELLING_MARK_UP,
                                        pTable(i) .DEFAULT_TO_CHILDREN,
                                        pTable(i) .UNIT_RETAIL_PRIM,
                                        pTable(i) .SELLING_UNIT_RETAIL_PRIM,
                                        pTable(i) .MULTI_UNIT_RETAIL_PRIM,
                                        pTable(i) .UNIT_RETAIL_EURO,
                                        pTable(i) .SELLING_UNIT_RETAIL_EURO,
                                        pTable(i) .MULTI_UNIT_RETAIL_EURO,
                                        pTable(i) .ERROR_MESSAGE,
                                        pTable(i) .RETURN_CODE,
                                        pTable(i) .AREA_DIFF_ZONE_IND );
  end loop;
  O_item_pricing_table := v_data;
END GET_ITEM_PRICING_INFO_WRP;
--------------------------------------------------------------------------------------------
FUNCTION AREA_DIFF_RECALC(O_error_message          OUT  VARCHAR2,
                          I_item_pricing_table     IN   ITEM_PRICING_TABLE)
RETURN BOOLEAN IS
   
  L_program                  VARCHAR2(61) := 'PM_RETAIL_API_SQL.AREA_DIFF_RECALC';
  O_success                  VARCHAR2(30);
  L_item_pricing_area_diff   OBJ_ITEM_PRICING_TBL;
  L_obj_item_pricing_table   OBJ_ITEM_PRICING_TBL;
  L_default_standard_uom    SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE;
  cursor C_SYSTEM_OPTIONS is
      select default_standard_uom
        from system_options;
BEGIN                                 
  if I_item_pricing_table is NOT NULL and 
     I_item_pricing_table.count>0 then 
	 SQL_LIB.SET_MARK('OPEN',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
     open C_SYSTEM_OPTIONS;

    SQL_LIB.SET_MARK('FETCH',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
   fetch C_SYSTEM_OPTIONS into L_default_standard_uom;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    '');
  close C_SYSTEM_OPTIONS;
	 L_obj_item_pricing_table := OBJ_ITEM_PRICING_TBL();
     L_obj_item_pricing_table.extend(I_item_pricing_Table.count);

   for i in 1..I_item_pricing_table.count LOOP
      L_obj_item_pricing_table(i) := OBJ_ITEM_PRICING_REC(I_item_pricing_table(i).item,
                                                          I_item_pricing_table(i).rpm_zone_group_id,
                                                          I_item_pricing_table(i).zone_id,
                                                          I_item_pricing_table(i).zone_display_id,
                                                          I_item_pricing_table(i).zone_description,
                                                          I_item_pricing_table(i).unit_retail,
                                                          I_item_pricing_table(i).selling_unit_retail,
                                                          NVL(I_item_pricing_table(i).selling_uom,L_default_standard_uom) ,
                                                          I_item_pricing_table(i).multi_units,
                                                          I_item_pricing_table(i).multi_unit_retail,
                                                          I_item_pricing_table(i).multi_selling_uom,
                                                          I_item_pricing_table(i).base_retail_ind,
                                                          I_item_pricing_table(i).currency_code,
                                                          NVL(I_item_pricing_table(i).standard_uom,L_default_standard_uom) ,
                                                          I_item_pricing_table(i).area_diff_zone_ind);

   end LOOP;
      if MERCH_RETAIL_API_SQL.SET_AREA_DIFF_PRICE(O_error_message,
                                                  L_item_pricing_area_diff,
                                                  L_obj_item_pricing_table) = FALSE then
        return FALSE;
      end if;
    
      if L_item_pricing_area_diff is NOT NULL and
         L_item_pricing_area_diff.COUNT > 0 then
         MERCH_RETAIL_API_SQL.SET_ITEM_PRICING_INFO(O_error_message,
                                                    O_success,
                                                    L_item_pricing_area_diff);
         if O_success = 'N' then
            return FALSE;
         end if;
      end if;
  end if;

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END AREA_DIFF_RECALC;

END PM_RETAIL_API_SQL;
/