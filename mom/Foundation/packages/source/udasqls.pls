
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE UDA_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: NEXT_SEQUENCE_NO
-- Purpose:       Retrieves the next available sequence number.
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION NEXT_SEQUENCE_NO( O_error_message  IN OUT  VARCHAR2,
                           O_sequence_no    IN OUT  UDA_ITEM_DEFAULTS.SEQ_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:    NEXT_UDA
-- Purpose: This user-defined attribute number sequence generator will return to 
--          the calling program/procedure.  Upon success (TRUE) a UDA number.  
--          Upon failure (FALSE) an appropriate error message for display purposes by
--          the calling program/procedure.
-- Created By: Retek
-------------------------------------------------------------------------------
FUNCTION NEXT_UDA( O_error_message  IN OUT  VARCHAR2,
                   O_uda_id         IN OUT  UDA.UDA_ID%TYPE) 
                   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_UDA_DESC
-- Purpose:       Retrieves the description associated with the UDA.
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION GET_UDA_DESC( O_error_message  IN OUT  VARCHAR2,
                       O_uda_desc       IN OUT  UDA.UDA_DESC%TYPE,
                       I_uda_id         IN      UDA.UDA_ID%TYPE)
                       RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_VALUE_DESC
-- Purpose:       Retrieves the description associated with the UDA Value.
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION GET_VALUE_DESC( O_error_message   IN OUT  VARCHAR2,
                         O_uda_value_desc  IN OUT  UDA_VALUES.UDA_VALUE_DESC%TYPE,
                         I_uda_id          IN      UDA_VALUES.UDA_ID%TYPE,
                         I_uda_value       IN      UDA_VALUES.UDA_VALUE%TYPE)
                         RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_SINGLE_VAL_IND
-- Purpose:       Retrieves the single value indicator associated with the UDA Value.
-- Created By:    Retek
---------------------------------------------------------------------------------------------
FUNCTION GET_SINGLE_VAL_IND( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_single_val_ind   IN OUT   UDA.SINGLE_VALUE_IND%TYPE,
                             I_uda_id           IN       UDA.UDA_ID%TYPE,
                             I_display_type     IN       UDA.DISPLAY_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: RELATION_EXISTS
-- Purpose:       Checks if a given UDA or UDA Value has been assigned 
--                directly to an item.
-- Variable(s):   O_exists:  True if the given UDA/UDA Value has been
--                           assigned to an item.
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION RELATION_EXISTS( O_error_message  IN OUT  VARCHAR2,
                          O_exists         IN OUT  BOOLEAN, 
                          I_uda_id         IN      UDA_VALUES.UDA_ID%TYPE,
                          I_uda_value      IN      UDA_VALUES.UDA_VALUE%TYPE)
                          RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_SINGLE_UDA
-- Purpose:       Deterimines if a given UDA and hierarchy combination 
--                exists.  This is only called when a single associated
--                UDA Value can exist and the user is trying to create
--                a duplicate relationship.
-- Variable(s):   O_exists:  True if a given hierarchy and UDA combination
--                           already exists OR if the UDA/Item relation 
--                           already exists.
--                I_seq_no:  Is needed when a user changes the Required field.
--                           If Required is switched from 'Y' to 'N', this check
--                           needs to take place. 
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION CHECK_SINGLE_UDA( O_error_message  IN OUT  VARCHAR2,
                           O_exists         IN OUT  BOOLEAN, 
                           I_uda_id         IN      UDA.UDA_ID%TYPE,
                           I_seq_no         IN      UDA_ITEM_DEFAULTS.SEQ_NO%TYPE,
                           I_item           IN      UDA_ITEM_LOV.ITEM%TYPE,
                           I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                           I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                           I_subclass       IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                           RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_DEFAULT_NO_VALUE
-- Purpose:       Checks if any UDA's exist as a default for a given 
--                hierarchy without a default value.  The user should
--                be forced to remedy this if it is true.
-- Variable(s):   O_default_no_value:  True if a given hierarchy has a UDA as a
--                                     default and no default value exists.
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DEFAULT_NO_VALUE( O_error_message     IN OUT  VARCHAR2,
                                 O_default_no_value  IN OUT  BOOLEAN, 
                                 I_dept              IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                                 I_class             IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                                 I_subclass          IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                                 RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_ITEMS_NO_UDA
-- Purpose:       Deterimines if any items (SKU/Style) exist within a given 
--                hierarchy level for a UDA without a UDA attached. 
--                This is only checked when setting a UDA to be required.
-- Variable(s):   O_exists:  True if the item does NOT have a UDA attached.
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION CHECK_ITEMS_NO_UDA( O_error_message  IN OUT  VARCHAR2,
                             O_exists         IN OUT  BOOLEAN, 
                             I_uda_id         IN      UDA_ITEM_DEFAULTS.UDA_ID%TYPE,
                             I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                             I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                             I_subclass       IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                             RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: ASSIGN_DEFAULTS
-- Purpose:       Assigns any UDA's that have been set as defaults at some
--                level of the hierarchy to a given item.
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION ASSIGN_DEFAULTS( O_error_message  IN OUT  VARCHAR2, 
                          I_item           IN      UDA_ITEM_LOV.ITEM%TYPE,
                          I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                          I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                          I_subclass       IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                          RETURN BOOLEAN; 

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQUIRED_UDA
-- Purpose:       Deterimines if a given UDA has been set as required for a
--                given item.  If the item is NULL, this function determines
--                if the given hierarchy is set to Required at a higher level.
--                Since it is checking for higher levels this should never be
--                Called with a Null department OR class value.
-- Calls:         ITEM_ATTRIB_SQL.GET_MERCH_HIER
-- Variable(s):   O_required:  True if the UDA is required for the Item OR the
--                             UDA should be required for the given hierarchy.
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_UDA( O_error_message  IN OUT  VARCHAR2,
                             O_required       IN OUT  BOOLEAN, 
                             I_uda_id         IN      UDA_ITEM_LOV.UDA_ID%TYPE,
                             I_item           IN      UDA_ITEM_LOV.ITEM%TYPE,
                             I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                             I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                             I_subclass       IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                             RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQUIRED_HIER
-- Purpose:       Deterimines if a given UDA has NOT been set as required for any
--                level below the given hierarchy.
-- Variable(s):   O_not_required:  True if the UDA is NOT set as required for 
--                                 any level below the given hierarchy.
-- Created:       16-SEP-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_HIER( O_error_message  IN OUT  VARCHAR2,
                              O_not_required   IN OUT  BOOLEAN, 
                              I_uda_id         IN      UDA_ITEM_LOV.UDA_ID%TYPE,
                              I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                              I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE)
                              RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_REQUIRED_HIER
-- Purpose:       Sets a UDA as required for all levels below a given hierarchy.
-- Created:       16-SEP-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_REQUIRED_HIER( O_error_message  IN OUT  VARCHAR2,
                               I_uda_id         IN      UDA_ITEM_DEFAULTS.UDA_ID%TYPE,
                               I_dept           IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                               I_class          IN      UDA_ITEM_DEFAULTS.CLASS%TYPE)
                               RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: PERFORM_MC
-- Purpose:       Performs the logic associated with mass maintenance on 
--                UDA's through the use of item lists.
-- Calls:         ITEMLIST_MC_REJECTS_SQL.INSER_REJECTS
--                CHECK_REQUIRED_UDA
-- Variable(s):   O_reject:      True if any items on the item list were not
--                               updated due to errors.
--                I_maint_type:  Type of maintenance.  valid values are:
--                                    'Add':     Adds all items on list.
--                                    'Create':  Adds only items not already 
--                                               associated w/ UDA.
--                                    'Change':  Changes existing UDA Value
--                                               to new UDA Value.
--                                    'Delete':  Deletes the associated UDA Value.               
-- Created:       28-AUG-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION PERFORM_MC( O_error_message       IN OUT  VARCHAR2,
                     O_reject              IN OUT  BOOLEAN, 
                     I_itemlist            IN      SKULIST_DETAIL.SKULIST%TYPE,
                     I_maint_type          IN      VARCHAR2,
                     I_display_type        IN      UDA.DISPLAY_TYPE%TYPE,
                     I_uda_id              IN      UDA_VALUES.UDA_ID%TYPE,
                     I_new_uda_value       IN      VARCHAR2,
                     I_existing_uda_value  IN      VARCHAR2)
                     RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_REQD_NO_VALUE
-- Purpose:       Determines if an item's new hierarchy has any required UDA defaults
--                that the item is not currently associated with.
-- Variable(s):   O_reqd_no_value: 'N' if the item does NOT have a UDA default associated
--                with it.
-- Created:       9-OCT-97 Arthur D. Palileo
------------------------------------------------------------------------------------------
FUNCTION CHECK_REQD_NO_VALUE( O_error_message     IN OUT  VARCHAR2,
                              O_reqd_no_value     IN OUT  VARCHAR2,
                              I_item              IN      UDA_ITEM_LOV.ITEM%TYPE, 
                              I_dept              IN      UDA_ITEM_DEFAULTS.DEPT%TYPE,
                              I_class             IN      UDA_ITEM_DEFAULTS.CLASS%TYPE,
                              I_subclass          IN      UDA_ITEM_DEFAULTS.SUBCLASS%TYPE)
                              RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: COPY_DOWN_PARENT_UDA
-- Purpose:       Defaults uda info to child items.
------------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_UDA(O_error_message     IN OUT  VARCHAR2,
                              I_item              IN      UDA_ITEM_LOV.ITEM%TYPE)
                              RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_ITEMUDA_MERCH_HIER
--       Purpose: Validates whether or not the item and uda_id belongs to the same merchandise
--                hierarchy ID and bring back TRUE to O_valid if yes, otherwise return FALSE
--                to O_valid. 

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_ITEM_MASTER
-- which only returns data that the user has permission to access.  

FUNCTION VALIDATE_ITEMUDA_MERCH_HIER(O_error_message    IN OUT VARCHAR2,
                                     O_valid            IN OUT BOOLEAN,
                                     I_item             IN     V_ITEM_MASTER.ITEM%TYPE,
                                     I_uda_id           IN     UDA.UDA_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_UDA_INFO
--       Purpose: Bring back the whole UDA info from UDA table for the input uda_id.

FUNCTION GET_UDA_INFO(O_error_message    IN OUT VARCHAR2,
                      O_uda_row          IN OUT UDA%ROWTYPE,
                      I_uda_id           IN     UDA.UDA_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: CHECK_UDA
-- Purpose:       This function helps to restrict duplication of records in uda_item_lov,
--                uda_item_date and uda_item_ff. It checks any one table in each call and that
--                is determined by the existence of a non-null value in any one of the input
--                parameters I_uda_value(uda_item_lov),I_uda_date(uda_item_date) or I_uda_text
--                (uda_item_ff).
---------------------------------------------------------------------------------------------
FUNCTION CHECK_UDA(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exists        IN OUT  BOOLEAN,
                   I_item          IN      UDA_ITEM_LOV.ITEM%TYPE,
                   I_uda_id        IN      UDA.UDA_ID%TYPE,
                   I_uda_value     IN      UDA_ITEM_LOV.UDA_VALUE%TYPE,
                   I_uda_text      IN      UDA_ITEM_FF.UDA_TEXT%TYPE,
                   I_uda_date      IN      UDA_ITEM_DATE.UDA_DATE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function Name: INSERT_DEFAULTS
--      Purpose: Insert UDA defaults during data conversion.  Called from data conversion KSH scripts.

FUNCTION INSERT_DEFAULTS(O_error_message  IN OUT  VARCHAR2,
                         I_item_tbl       IN      ITEM_TBL,
                         I_dept_tbl       IN      DEPT_TBL,
                         I_class_tbl      IN      DEPT_TBL,
                         I_subclass_tbl   IN      DEPT_TBL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: CHECK_UDA_EXISTS
-- Purpose:       This function checks whether the item have any user defined attributes or not.
----------------------------------------------------------------------------------------------
FUNCTION CHECK_UDA_EXISTS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists        IN OUT  BOOLEAN,
                          I_item          IN      UDA_ITEM_LOV.ITEM%TYPE)
RETURN BOOLEAN;                   
----------------------------------------------------------------------------------------------
-- Function Name: DELETE_UDA_ITEM_FF_TL
-- Purpose:       This function deletes from the table UDA_ITEM_FF_TL
----------------------------------------------------------------------------------------------
FUNCTION DELETE_UDA_ITEM_FF_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item          IN       UDA_ITEM_FF_TL.ITEM%TYPE, 
                                 I_uda_id        IN       UDA_ITEM_FF_TL.UDA_ID%TYPE,
                                 I_uda_text      IN       UDA_ITEM_FF_TL.UDA_TEXT%TYPE)
RETURN BOOLEAN;
END UDA_SQL;
/

