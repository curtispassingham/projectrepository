
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRCLS_VALIDATE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the create and modify messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrClsDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the delete messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_CLASS_EXISTS
   -- Purpose      : This function will verify if the class record already exists.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XMrchHrClsDesc_REC",
                            I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_CLASS_EXISTS
   -- Purpose      : This function will verify if the class and any node being deleted already exists.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_DELETE
   -- Purpose      : This function will verify that the class can be deleted.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_DEFAULT_FIELDS
   -- Purpose      : This function will retrieve all values not defined in the message, but required
   --                for RMS integrity.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEFAULT_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_vat_ind        IN OUT NOCOPY   CLASS.CLASS_VAT_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB class object into a class record defined in
   --                the MERCH_SQL package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_class_rec       OUT    NOCOPY   CLASS%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrClsDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB class object into a class record defined in
   --                the MERCH_SQL package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_class_rec       OUT    NOCOPY   CLASS%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_class_rec       OUT    NOCOPY   CLASS%ROWTYPE,
                       I_message         IN              "RIB_XMrchHrClsDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XMRCHHRCLS_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if I_message_type != RMSSUB_XMRCHHRCLS.LP_cre_type then
      if not CHECK_CLASS_EXISTS(O_error_message,
                                I_message,
                                I_message_type) then
         return FALSE;
      end if;
   end if;

   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_class_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_class_rec       OUT    NOCOPY   CLASS%ROWTYPE,
                       I_message         IN              "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XMRCHHRCLS_VALIDATE.CHECK_MESSAGE';
   L_exist        VARCHAR2(1)   := 'N';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_CLASS_EXISTS(O_error_message,
                             I_message) then
      return FALSE;
   end if;

   if not CHECK_DELETE(O_error_message,
                       I_message) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_class_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
---------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrClsDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XMRCHHRCLS_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Class');
      return FALSE;
   end if;

   if I_message.class_name is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Class name');
      return FALSE;
   end if;

   if I_message.dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Department');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XMRCHHRCLS_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Department');
      return FALSE;
   end if;

   if I_message.class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Class');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XMrchHrClsDesc_REC",
                            I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCLS_VALIDATE.CHECK_CLASS_EXISTS';
   L_exist        BOOLEAN      := FALSE;
   L_table_name   VARCHAR2(10) := 'CLASS';
   L_key_value    DAILY_PURGE.KEY_VALUE%TYPE;

BEGIN
   L_key_value := substr(to_char(I_message.dept, '0999'),2,4)|| ';' ||
                  substr(to_char(I_message.class, '0999'),2,4);
                  
   -- Check for daily_purge existence
   if NOT DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                       L_exist,
                                       L_key_value,
                                       L_table_name) then
      return FALSE;
   end if;
   
   if L_exist then      
      O_error_message := SQL_LIB.CREATE_MSG('EXIST_IN_DAILY_PURGE',
                                            L_key_value,
                                            NULL,
                                            NULL);
      return FALSE;                                   
   end if;   

   -- Check for Class
   L_exist := FALSE;
   if not CLASS_VALIDATE_SQL.EXIST(O_error_message,
                                   I_message.dept,
                                   I_message.class,
                                   L_exist) then
      return FALSE;
   end if;

   if not L_exist and I_message_type = RMSSUB_XMRCHHRCLS.LP_mod_type then
      O_error_message := SQL_LIB.CREATE_MSG('CLASS_NO_EXIST_DEPT', I_message.class, I_message.dept);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CLASS_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_CLASS_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCLS_VALIDATE.CHECK_CLASS_EXISTS';
   L_exist        BOOLEAN      := FALSE;
   L_table_name   VARCHAR2(10) := 'CLASS';
   L_key_value    DAILY_PURGE.KEY_VALUE%TYPE;

BEGIN
   L_key_value := substr(to_char(I_message.dept, '0999'),2,4)|| ';' ||
                  substr(to_char(I_message.class, '0999'),2,4);
                  
   -- Check for daily_purge existence
   if NOT DAILY_PURGE_SQL.CHECK_EXISTS(O_error_message,
                                       L_exist,
                                       L_key_value,
                                       L_table_name) then
      return FALSE;
   end if;
   
   if L_exist then      
      O_error_message := SQL_LIB.CREATE_MSG('EXIST_IN_DAILY_PURGE',
                                            L_key_value,
                                            NULL,
                                            NULL);
      return FALSE;                                   
   end if;   

   -- Check for Class
   L_exist := FALSE;
   if not CLASS_VALIDATE_SQL.EXIST(O_error_message,
                                   I_message.dept,
                                   I_message.class,
                                   L_exist) then
      return FALSE;
   end if;

   if not L_exist then
      O_error_message := SQL_LIB.CREATE_MSG('CLASS_NO_EXIST_DEPT', I_message.class, I_message.dept);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CLASS_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message         IN       "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN IS

   L_program             VARCHAR2(60)  := 'RMSSUB_XMRCHHRCLS_VALIDATE.CHECK_DELETE';
   L_exist               VARCHAR2(1)   := 'N';
   L_class_exist         BOOLEAN       := FALSE;
   L_filter_merch_level  FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE := 'C';

BEGIN

   -- checks if the Merchandise Hierarchy class exists in one of the Data Element.
   if not FILTER_GROUP_HIER_SQL.VALIDATE_GROUP_MERCH(O_error_message,
                                                     L_class_exist,
                                                     L_filter_merch_level,
                                                     I_message.dept,
                                                     I_message.class,
                                                     NULL) then
      return FALSE;
   end if;

   if L_class_exist then
      O_error_message := SQL_LIB.CREATE_MSG('CNT_DEL_REC', 
                                            NULL, 
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DELETE;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_DEFAULT_FIELDS(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_vat_ind        IN OUT NOCOPY   CLASS.CLASS_VAT_IND%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XMRCHHRCLS_VALIDATE.POPULATE_DEFAULT_FIELDS';

BEGIN

   if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type IN ('SVAT','GTAX') and
      SYSTEM_OPTIONS_SQL.GP_system_options_row.class_level_vat_ind = 'N' then
      IO_vat_ind := 'Y';
   else
      IO_vat_ind := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_DEFAULT_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_class_rec       OUT    NOCOPY   CLASS%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrClsDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCLS_VALIDATE.POPULATE_RECORD';

BEGIN

   O_class_rec.dept              := I_message.dept;
   O_class_rec.class             := I_message.class;
   O_class_rec.class_name        := I_message.class_name;

   if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type = 'SALES' then
      O_class_rec.class_vat_ind  := 'N';

   elsif I_message.class_vat_ind is not NULL then
      O_class_rec.class_vat_ind  := I_message.class_vat_ind;

   elsif not POPULATE_DEFAULT_FIELDS(O_error_message,
                                     O_class_rec.class_vat_ind) then
         return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_class_rec       OUT    NOCOPY   CLASS%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrClsRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRCLS_VALIDATE.POPULATE_RECORD';

BEGIN

   O_class_rec.dept           := I_message.dept;
   O_class_rec.class          := I_message.class;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
---------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRCLS_VALIDATE;
/
