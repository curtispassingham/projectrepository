
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY STORE_GRADE_VALIDATE_SQL AS

--------------------------------------------------------------------
FUNCTION NEXT_STORE_GRADE_GROUP_NUMBER( O_error_message         IN OUT  VARCHAR2,
                                        O_store_grade_group_id  IN OUT  STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
                                        RETURN BOOLEAN IS

   L_wrap_sequence_number   STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);


   cursor C_GRADE_GROUP_EXISTS is
       select  'x'
         from  store_grade_group
         where store_grade_group_id = O_store_grade_group_id;

BEGIN
   LOOP
      SQL_LIB.SET_MARK('SELECT',
                       NULL,
                       'SYS.DUAL',
                       NULL);

      select STORE_GRADE_GROUP_SEQUENCE.NEXTVAL
        into O_store_grade_group_id
        from sys.dual;

      if L_first_time = 'Yes' then
          L_wrap_sequence_number := O_store_grade_group_id;
          L_first_time := 'No';
      elsif O_store_grade_group_id = L_wrap_sequence_number then
         O_error_message := 'NO_GRADE_GROUP_NUMBERS';
         RETURN FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN',
                       'C_GRADE_GROUP_EXISTS',
                       'STORE_GRADE_GROUP',
                       'STORE GRADE GROUP ID: ' || to_char(O_store_grade_group_id));

      open  C_GRADE_GROUP_EXISTS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GRADE_GROUP_EXISTS',
                       'STORE_GRADE_GROUP',
                       'STORE GRADE GROUP ID: ' || to_char(O_store_grade_group_id));

      fetch C_GRADE_GROUP_EXISTS into L_dummy;

      if C_GRADE_GROUP_EXISTS%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GRADE_GROUP_EXISTS',
                          'STORE_GRADE_GROUP',
                          'STORE GRADE GROUP ID: ' || to_char(O_store_grade_group_id));

          close C_GRADE_GROUP_EXISTS;
          RETURN TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GRADE_GROUP_EXISTS',
                       'STORE_GRADE_GROUP',
                       'STORE GRADE GROUP ID: ' || to_char(O_store_grade_group_id));

      close C_GRADE_GROUP_EXISTS;
   END LOOP;

   RETURN FALSE;

EXCEPTION
    WHEN OTHERS THEN
       O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                              SQLERRM,
                                              'STORE_GRADE_VALIDATE_SQL.NEXT_STORE_GRADE_GROUP_NUMBER',
                                              to_char(SQLCODE));

       RETURN FALSE;

END NEXT_STORE_GRADE_GROUP_NUMBER;

---------------------------------------------------------------------------------------------
FUNCTION GET_GROUP_DESC( O_error_message           IN OUT  VARCHAR2,
                         O_store_grade_group_desc  IN OUT  STORE_GRADE_GROUP.STORE_GRADE_GROUP_DESC%TYPE,
                         I_store_grade_group_id    IN      STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
                         RETURN BOOLEAN IS

   cursor C_GET_GRADE_GROUP_DESC is
      select store_grade_group_desc
        from v_store_grade_group_tl
       where store_grade_group_id = I_store_grade_group_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_GRADE_GROUP_DESC',
                    'V_STORE_GRADE_GROUP_TL',
                    'STORE GRADE GROUP ID: ' || to_char(I_store_grade_group_id));

   open  C_GET_GRADE_GROUP_DESC;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_GRADE_GROUP_DESC',
                    'V_STORE_GRADE_GROUP_TL',
                    'STORE GRADE GROUP ID: ' || to_char(I_store_grade_group_id));

   fetch C_GET_GRADE_GROUP_DESC into O_store_grade_group_desc;

   if C_GET_GRADE_GROUP_DESC%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_GRADE_GROUP_DESC',
                       'V_STORE_GRADE_GROUP_TL',
                       'STORE GRADE GROUP ID: ' || to_char(I_store_grade_group_id));

      close C_GET_GRADE_GROUP_DESC;
      O_error_message := SQL_LIB.CREATE_MSG( 'STORE_GRADE_GROUP_ERROR',
                                             to_char(I_store_grade_group_id),
                                             NULL,
                                             NULL);
      RETURN FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_GRADE_GROUP_DESC',
                    'V_STORE_GRADE_GROUP_TL',
                    'STORE GRADE GROUP ID: ' || to_char(I_store_grade_group_id));

   close C_GET_GRADE_GROUP_DESC;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'STORE_GRADE_VALIDATE_SQL.GET_GROUP_DESC',
                                             to_char(SQLCODE));
      RETURN FALSE;

END GET_GROUP_DESC;

---------------------------------------------------------------------------------------------
FUNCTION GRADE_EXISTS( O_error_message         IN OUT  VARCHAR2,
                       O_exists                IN OUT  BOOLEAN,
                       I_store_grade_group_id  IN      STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE,
                       I_store_grade           IN      STORE_GRADE.STORE_GRADE%TYPE)
                       RETURN BOOLEAN IS

   L_dummy  VARCHAR2(1);

   cursor C_CHECK_GRADE_EXISTS is
      select 'x'
        from store_grade
       where store_grade_group_id = I_store_grade_group_id
         and store_grade          = I_store_grade;

BEGIN


   open C_CHECK_GRADE_EXISTS;
   fetch C_CHECK_GRADE_EXISTS into L_dummy;

   if C_CHECK_GRADE_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   close C_CHECK_GRADE_EXISTS;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'STORE_GRADE_VALIDATE_SQL.GRADE_EXISTS',
                                             to_char(SQLCODE));
      RETURN FALSE;

END GRADE_EXISTS;
------------------------------------------------------------------------------------------
FUNCTION STORE_GRADE_STORE_FILTER_LIST(O_error_message         IN OUT  VARCHAR2,
                                       O_Partial_ind           IN OUT  VARCHAR2,
                                       I_store_grade_group_id  IN      STORE_GRADE_STORE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'STORE_GRADE_VALIDATE_SQL.STORE_GRADE_STORE_FILTER_LIST';

   cursor C_Partial_view is
     select 'Y'
       from store_grade_store
      where store_grade_group_id = I_store_grade_group_id
        and store not in (select store
                            from v_store);

   L_partial_ind         VARCHAR2(1) := 'N';

BEGIN
   ---
   if I_store_grade_group_id is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_grade_group_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_Partial_view;
   fetch C_Partial_view into L_partial_ind;
   close C_Partial_view;
   ---
   O_Partial_ind := L_partial_ind;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      RETURN FALSE;

END STORE_GRADE_STORE_FILTER_LIST;
------------------------------------------------------------------------------------------
FUNCTION INSERT_STORE_GRADE_TEMP(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_order_no               IN      STORE_GRADE_DIST_TEMP.ORDER_NO%TYPE,
                                 I_store_grade_group_id   IN      STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'STORE_GRADE_VALIDATE_SQL.INSERT_STORE_GRADE_TEMP';
   L_table        VARCHAR2(30);
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_STORE_GRADE_TEMP is
      select 'x'
        from store_grade_dist_temp
       where order_no = I_order_no
         for update nowait;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_store_grade_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_grade_group_id',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   L_table := 'STORE_GRADE_DIST_TEMP';

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_STORE_GRADE_TEMP',
                    'store_grade_dist_temp',
                    NULL);

   open  C_LOCK_STORE_GRADE_TEMP;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_STORE_GRADE_TEMP',
                    'store_grade_dist_temp',
                    NULL);

   close C_LOCK_STORE_GRADE_TEMP;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    'NULL',
                    'store_grade_dist_temp',
                    NULL);

   delete from store_grade_dist_temp
         where order_no = I_order_no;
   ---
   insert into store_grade_dist_temp(order_no,
                                     contract_no,
                                     store_grade_group_id,
                                     store_grade,
                                     dist_qty,
                                     dist_pct,
                                     dist_ratio)
                              select I_order_no,
                                     NULL,
                                     sg.store_grade_group_id,
                                     sg.store_grade,
                                     NULL,
                                     NULL,
                                     NULL
                                from store_grade sg
                               where sg.store_grade_group_id  = I_store_grade_group_id
                                 and exists (select 'x'
                                               from store_grade_store sgs
                                              where sgs.store_grade          = sg.store_grade
                                                and sgs.store_grade_group_id = sg.store_grade_group_id
                                                and rownum = 1);
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                            L_table,
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_STORE_GRADE_TEMP;
------------------------------------------------------------------------------------------
FUNCTION CHECK_STORE_GRADE_BUYER(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists                 IN OUT  VARCHAR2,
                                 I_buyer                  IN      BUYER.BUYER%TYPE,
                                 I_store_grade_group_id   IN      STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'STORE_GRADE_VALIDATE_SQL.CHECK_STORE_GRADE_BUYER';
   L_dummy     VARCHAR2(1)  := NULL;

   cursor C_CHECK_STRGRD_BUYER is
      select 'x'
        from store_grade_group
       where buyer                = I_buyer
         and store_grade_group_id = I_store_grade_group_id
         and rownum               = 1;

BEGIN

   if I_buyer is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_buyer',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_store_grade_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_grade_group_id',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_STRGRD_BUYER',
                    'store_grade_group',
                    NULL);
   open C_CHECK_STRGRD_BUYER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_STRGRD_BUYER',
                    'store_grade_group',
                    NULL);
   fetch C_CHECK_STRGRD_BUYER into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_STRGRD_BUYER',
                    'store_grade_group',
                    NULL);
   close C_CHECK_STRGRD_BUYER;

   if L_dummy is NULL then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CHECK_STORE_GRADE_BUYER;
------------------------------------------------------------------------------------------
FUNCTION CHECK_STORE_GRADE_STORE(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists                 IN OUT   VARCHAR2,
                                 I_store                  IN       STORE.STORE%TYPE,
                                 I_store_grade_group_id   IN       STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'STORE_GRADE_VALIDATE_SQL.CHECK_STORE_GRADE_STORE';
   L_dummy     VARCHAR2(1)  := NULL;

   cursor C_CHECK_STRGRD_STORE is
      select 'x'
        from store_grade_store
       where store                = I_store
         and store_grade_group_id = I_store_grade_group_id
         and rownum               = 1;

BEGIN

   if I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_store_grade_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_grade_group_id',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_STRGRD_STORE',
                    'STORE_GRADE_GROUP',
                    NULL);
   open C_CHECK_STRGRD_STORE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_STRGRD_STORE',
                    'STORE_GRADE_GROUP',
                    NULL);
   fetch C_CHECK_STRGRD_STORE into L_dummy;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_STRGRD_STORE',
                    'STORE_GRADE_GROUP',
                    NULL);
   close C_CHECK_STRGRD_STORE;

   if L_dummy is NULL then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CHECK_STORE_GRADE_STORE;
------------------------------------------------------------------------------------------
FUNCTION CREATE_STORE_GRADE_STORE(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists                 IN OUT   VARCHAR2,
                                  I_group_no               IN       VARCHAR2,
                                  I_group_type             IN       STORE_GRADE.STORE_GRADE%TYPE,
                                  I_store_grade            IN       STORE_GRADE.STORE_GRADE%TYPE,
                                  I_store_grade_group_id   IN       STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(60) := 'STORE_GRADE_VALIDATE_SQL.CREATE_STORE_GRADE_STORE';

   cursor C_EXPLODE_STORES is
      select distinct vs.store
        from loc_traits_matrix ltm,
             v_store vs, 
             district dis
       where ((I_group_type = 'A'
                  and exists (select 'x'
                                 from v_store s
                              where s.store_type in ('C','F')
                                and s.store = vs.store))
          or  (vs.store_class      = I_group_no
                  and I_group_type = 'C')
          or  (to_char(vs.district)         = I_group_no
                  and I_group_type = 'D')
          or  (to_char(ltm.loc_trait)       = I_group_no
                  and I_group_type = 'L')
          or  (to_char(dis.region)          = I_group_no
                  and I_group_type = 'R')
          or  (to_char(vs.store)            = I_group_no
                  and I_group_type = 'S')
          or  (to_char(vs.transfer_zone)    = I_group_no
                  and I_group_type = 'T')
          or  (I_group_type = 'LL'
                  and exists (select 'x'
                                from loc_list_detail lld
                               where lld.loc_type = 'S'
                                 and lld.location = vs.store
                                 and to_char(lld.loc_list) = I_group_no)))
         and vs.district = dis.district
         and ltm.store(+) = vs.store;

BEGIN

   if I_group_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_group_type',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_store_grade is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_grade',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_store_grade_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_grade_group_id',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   O_exists := 'N';
   ---
   FOR rec in C_EXPLODE_STORES LOOP
      BEGIN
         insert into store_grade_store(store, 
                                       store_grade,
                                       store_grade_group_id)
                               values (rec.store,
                                       I_store_grade,
                                       I_store_grade_group_id);
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            O_exists := 'Y';
      END;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CREATE_STORE_GRADE_STORE;
------------------------------------------------------------------------------------------
FUNCTION UPDATE_STORE_GRADE_STORE(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store                  IN       STORE.STORE%TYPE,
                                  I_store_grade            IN       STORE_GRADE.STORE_GRADE%TYPE,
                                  I_store_grade_group_id   IN       STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(60) := 'STORE_GRADE_VALIDATE_SQL.UPDATE_STORE_GRADE_STORE';
   RECORD_LOCKED              EXCEPTION;
   PRAGMA                     EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STORE_GRADE_STORE is
      select 'x' 
        from store_grade_store
       where store                = I_store
         and store_grade_group_id = I_store_grade_group_id
         for update of store nowait;

BEGIN

   if I_store_grade is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_grade',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_store_grade_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_store_grade_group_id',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_STORE_GRADE_STORE',
                    'STORE_GRADE_STORE',
                    NULL);
   open  C_LOCK_STORE_GRADE_STORE;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_STORE_GRADE_STORE',
                    'STORE_GRADE_STORE',
                    NULL);
   close C_LOCK_STORE_GRADE_STORE;

   update store_grade_store
      set store_grade          = I_store_grade
    where store                = I_store
      and store_grade_group_id = I_store_grade_group_id;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
       O_error_message := SQL_LIB.CREATE_MSG('STORE_GRADE_STORE_REC_LO2',
                                             I_store,
                                             I_store_grade_group_id,
                                             NULL);
       return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END UPDATE_STORE_GRADE_STORE;
------------------------------------------------------------------------------------------
END STORE_GRADE_VALIDATE_SQL;
/
