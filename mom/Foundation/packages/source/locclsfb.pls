
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY LOC_CLASSIFICATION_SQL AS
----------------------------------------------------------------------
   LP_table                VARCHAR2(30);
   LP_item                 VARCHAR2(30);
   RECORD_LOCKED 		EXCEPTION;
   PRAGMA			EXCEPTION_INIT(Record_Locked, -54);

----------------------------------------------------------------------
FUNCTION CLEAR_LOC_CLSF(O_error_message   IN OUT VARCHAR2,
                        I_loc_list        IN     LOC_LIST_DETAIL.LOC_LIST%TYPE,
                        I_string          IN     VARCHAR2)
   RETURN BOOLEAN is
   
   L_skulist  SKULIST_HEAD.SKULIST%TYPE := NULL;

   cursor C_GET_AND_LOCK_DETAIL_LIST is
      select location,
             class_level,
             skulist
        from loc_clsf_detail
       where exists (select 'x'
                       from loc_list_detail
                      where location = loc_clsf_detail.location
                        and loc_list = I_loc_list)
         and INSTR(I_string, class_level) = 0
         for update nowait;

   cursor C_LOCK_SKULIST_CRITERIA is
      select 'x'
        from skulist_criteria
       where skulist = L_skulist
         for update nowait;
 
   cursor C_LOCK_SKULIST_DETAIL is
      select 'x' 
        from skulist_detail
       where skulist = L_skulist
         for update nowait;
 
cursor C_LOCK_SKULIST_HEAD_TL is
      select 'x' 
        from skulist_head_tl
       where skulist = L_skulist 
         for update nowait;

   cursor C_LOCK_SKULIST_HEAD is
      select 'x' 
        from skulist_head 
       where skulist = L_skulist 
         for update nowait;
 
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_AND_LOCK_DETAIL_LIST',
                    'LOC_LIST_DETAIL', 'Location List: '||to_char(I_loc_list));
   FOR C_rec in C_GET_AND_LOCK_DETAIL_LIST LOOP
      L_skulist := C_rec.skulist;

      LP_table := 'LOC_CLSF_DETAIL';
      LP_item  := 'Location: '||to_char(C_rec.location);
      
  
      SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_HEAD_TL','SKULIST_HEAD_TL','Skulist: ' ||to_char(L_skulist));
      open C_LOCK_SKULIST_HEAD_TL;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD_TL','SKULIST_HEAD_TL','Skulist: ' ||to_char(L_skulist));
      close C_LOCK_SKULIST_HEAD_TL;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_HEAD','SKULIST_HEAD','Skulist: ' ||to_char(L_skulist));
      open C_LOCK_SKULIST_HEAD;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD','SKULIST_HEAD','Skulist: ' ||to_char(L_skulist));
      close C_LOCK_SKULIST_HEAD;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_DETAIL','SKULIST_DETAIL','Skulist: ' ||to_char(L_skulist));
      open C_LOCK_SKULIST_DETAIL;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_DETAIL','SKULIST_DETAIL','Skulist: ' ||to_char(L_skulist));
      close C_LOCK_SKULIST_DETAIL;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_CRITERIA','SKULIST_CRITERIA','Skulist: ' ||to_char(L_skulist));
      open C_LOCK_SKULIST_CRITERIA;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_CRITERIA','SKULIST_CRITERIA','Skulist: ' ||to_char(L_skulist));
      close C_LOCK_SKULIST_CRITERIA;


      SQL_LIB.SET_MARK('DELETE',NULL,'LOC_CLSF_DETAIL','Location: '||to_char(C_rec.location));    

      delete from loc_clsf_detail
       where location    = C_rec.location
         and class_level = C_rec.class_level;   

      LP_table := 'SKULIST_CRITERIA';
      LP_item  := 'Skulist: '||to_char(L_skulist);
 
      SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_CRITERIA','Skulist: '||to_char(L_skulist));

      delete from skulist_criteria 
       where skulist = L_skulist;

      LP_table := 'SKULIST_DETAIL';
      LP_item  := 'Skulist: '||to_char(L_skulist);

      SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_DETAIL','Skulist: '||to_char(L_skulist));

      delete from skulist_detail 
       where skulist = L_skulist;
      
      LP_table := 'SKULIST_HEAD_TL';
      LP_item  := 'Skulist: '||to_char(L_skulist);

      SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_HEAD_TL','Skulist: '||to_char(L_skulist));
      delete from skulist_head_tl
       where skulist = L_skulist;

      LP_table := 'SKULIST_HEAD';
      LP_item  := 'Skulist: '||to_char(L_skulist);

      SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_HEAD','Skulist: '||to_char(L_skulist));
      delete from skulist_head 
       where skulist = L_skulist;

   END LOOP;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_SKULIST_HEAD_TL%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD_TL','SKULIST_HEAD_TL','Skulist: '||to_char(L_skulist)); 
        close C_LOCK_SKULIST_HEAD_TL;
      end if;
      if C_LOCK_SKULIST_HEAD%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD','SKULIST_HEAD','Skulist: '||to_char(L_skulist)); 
        close C_LOCK_SKULIST_HEAD;
      end if;
      if C_LOCK_SKULIST_DETAIL%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_DETAIL','SKULIST_DETAIL','Skulist: '||to_char(L_skulist)); 
        close C_LOCK_SKULIST_DETAIL;
      end if;
      if C_LOCK_SKULIST_CRITERIA%ISOPEN then 
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_CRITERIA','SKULIST_CRITERIA','Skulist: ' ||to_char(L_skulist));
         close C_LOCK_SKULIST_CRITERIA;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            LP_item,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_CLASSIFICATION_SQL.CLEAR_LOC_CLSF',
                                            to_char(SQLCODE));
END CLEAR_LOC_CLSF;
----------------------------------------------------------------------
FUNCTION DELETE_LOC_CLSF(O_error_message IN OUT VARCHAR2,
                        I_location      IN     LOC_CLSF_HEAD.LOCATION%TYPE,
                        I_loc_list      IN     LOC_LIST_HEAD.LOC_LIST%TYPE)
   RETURN BOOLEAN IS

   L_class_level LOC_CLSF_DETAIL.CLASS_LEVEL%TYPE := NULL;
   L_skulist     SKULIST_HEAD.SKULIST%TYPE        := NULL;
   L_location    LOC_CLSF_DETAIL.LOCATION%TYPE    := NULL;
   L_loc_type    LOC_CLSF_DETAIL.LOCATION%TYPE    := NULL;


   cursor C_LOCK_HEAD_LIST is 
      select 'x' 
        from loc_clsf_head
       where exists(select 'x' 
                      from loc_list_detail 
                     where location = loc_clsf_head.location 
                       and loc_list = I_loc_list);


   cursor C_LOCK_HEAD_LOC is
    select 'x' 
      from loc_clsf_head
     where location = L_location
       for update nowait;

   cursor C_LOCK_DETAIL_LOC is
     select 'x'
       from loc_clsf_detail
      where location = L_location
        and class_level = L_class_level
        for update nowait;      

   cursor C_LOCK_SKULIST_CRITERIA is
      select 'x' 
        from skulist_criteria 
       where skulist = L_skulist
         for update nowait;

   cursor C_LOCK_SKULIST_DETAIL is
      select 'x' 
        from skulist_detail
       where skulist = L_skulist
         for update nowait;
   
   cursor C_LOCK_SKULIST_HEAD_TL is
      select 'x' 
        from skulist_head_tl
       where skulist = L_skulist 
         for update nowait;

   cursor C_LOCK_SKULIST_HEAD is
      select 'x' 
        from skulist_head 
       where skulist = L_skulist 
         for update nowait;

   cursor C_INFO_LIST_DETAIL is
      select lcd.skulist,
             lcd.location,
             lcd.class_level 
        from loc_clsf_detail lcd,
             loc_list_detail lld
       where lcd.location = lld.location
         and lcd.loc_type = lld.loc_type
         and lld.loc_list = I_loc_list;

   cursor C_INFO_LOC_DETAIL is
      select skulist, 
             location,
             class_level
        from loc_clsf_detail
       where location = I_location;

BEGIN

   if (I_location is NULL and I_loc_list is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC', NULL, NULL, NULL);
      return FALSE;
   elsif I_loc_list is not NULL then
      for C_rec in C_INFO_LIST_DETAIL LOOP
         L_skulist     := C_rec.skulist;
         L_location    := C_rec.location;     
         L_class_level := C_rec.class_level;
         SQL_LIB.SET_MARK('OPEN','C_LOCK_DETAIL_LOC','LOC_CLSF_DETAIL','Location : '|| to_char(L_location));
         open C_LOCK_DETAIL_LOC;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_DETAIL_LOC','LOC_CLSF_DETAIL','Location : '|| to_char(L_location));
         close C_LOCK_DETAIL_LOC;

         LP_item := 'Location: '||to_char(L_location);
         LP_table := 'LOC_CLSF_DETAIL';

         SQL_LIB.SET_MARK('DELETE',NULL,'LOC_CLSF_DETAIL','Location: '||to_char(L_location));
         delete from loc_clsf_detail 
          where class_level = L_class_level
            and location    = L_location;

         LP_table := 'SKULIST_CRITERIA';
         LP_item  := 'Skulist: '||to_char(L_skulist); 
   
         SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_CRITERIA','SKULIST_CRITERIA','Skulist: ' ||to_char(L_skulist));
         open C_LOCK_SKULIST_CRITERIA;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_CRITERIA','SKULIST_CRITERIA','Skulist: ' ||to_char(L_skulist));
         close C_LOCK_SKULIST_CRITERIA;
      
         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_CRITERIA','Skulist: '||to_char(L_skulist));
         delete from skulist_criteria
          where skulist = L_skulist;


         LP_table := 'SKULIST_DETAIL';

         SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_DETAIL','SKULIST_DETAIL','Skulist: ' ||to_char(L_skulist));
         open C_LOCK_SKULIST_DETAIL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_DETAIL','SKULIST_DETAIL','Skulist: ' ||to_char(L_skulist));
         close C_LOCK_SKULIST_DETAIL;
      
         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_DETAIL','Skulist: '||to_char(L_skulist));
         delete from skulist_detail
          where skulist = L_skulist;
 

         LP_table := 'SKULIST_HEAD_TL';

         SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_HEAD_TL','SKULIST_HEAD_TL','Skulist: ' ||to_char(L_skulist));
         open C_LOCK_SKULIST_HEAD_TL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD_TL','SKULIST_HEAD_TL','Skulist: ' ||to_char(L_skulist));
         close C_LOCK_SKULIST_HEAD_TL;
        
         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_HEAD_TL','Skulist: '||to_char(L_skulist));
         delete from skulist_head_tl
          where skulist = L_skulist;

         LP_table := 'SKULIST_HEAD';

         SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_HEAD','SKULIST_HEAD','Skulist: ' ||to_char(L_skulist));
         open C_LOCK_SKULIST_HEAD;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD','SKULIST_HEAD','Skulist: ' ||to_char(L_skulist));
         close C_LOCK_SKULIST_HEAD;
        
         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_HEAD','Skulist: '||to_char(L_skulist));
         delete from skulist_head
          where skulist = L_skulist;
      END LOOP;      

      LP_table := 'LOC_CLSF_HEAD';
      LP_item  := 'Loc_list: '||to_char(I_loc_list);

      SQL_LIB.SET_MARK('OPEN','C_LOCK_HEAD_LIST','LOC_CLSF_HEAD','Loc_list: '|| to_char(I_loc_list));
      open C_LOCK_HEAD_LIST;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_HEAD_LIST','LOC_CLSF_HEAD','Loc_list: '|| to_char(I_loc_list));
      close C_LOCK_HEAD_LIST;
      ---  
      SQL_LIB.SET_MARK('DELETE',NULL,'LOC_CLSF_HEAD','Loc_list: '|| to_char(I_loc_list));
      delete from loc_clsf_head
       where exists(select 'x'
                      from loc_list_detail
                     where location = loc_clsf_head.location
                       and loc_list = I_loc_list);

   elsif I_location is not NULL then
      
      FOR C_rec in C_INFO_LOC_DETAIL LOOP
         L_skulist     := C_rec.skulist;
         L_class_level := C_rec.class_level;
         L_location    := C_rec.location;

         LP_table := 'LOC_CLSF_DETAIL';
         LP_item := 'Location: '|| to_char(I_location); 

         SQL_LIB.SET_MARK('OPEN','C_LOCK_DETAIL_LOC','LOC_CLSF_DETAIL','Location: '||to_char(L_location));
         open C_LOCK_DETAIL_LOC;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_DETAIL_LOC','LOC_CLSF_DETAIL','Location: '||to_char(L_location));
         close C_LOCK_DETAIL_LOC;

  
         SQL_LIB.SET_MARK('DELETE',NULL,'LOC_CLSF_DETAIL','Location: '|| to_char(I_location));
         delete from loc_clsf_detail 
          where location    = L_location
            and class_level = L_class_level;

         LP_table := 'SKULIST_CRITERIA';
         LP_item  := 'Skulist: '||to_char(L_skulist); 
   
         SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_CRITERIA','SKULIST_CRITERIA','Skulist: ' ||to_char(L_skulist));
         open C_LOCK_SKULIST_CRITERIA;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_CRITERIA','SKULIST_CRITERIA','Skulist: ' ||to_char(L_skulist));
         close C_LOCK_SKULIST_CRITERIA;
      
         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_CRITERIA','Skulist: '||to_char(L_skulist));
         delete from skulist_criteria
          where skulist = L_skulist;
  
         LP_item := 'Skulist: '|| to_char(L_skulist);

         SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_DETAIL','SKULIST_DETAIL','Skulist: '|| to_char(L_skulist));
         open C_LOCK_SKULIST_DETAIL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_DETAIL','SKULIST_DETAIL','Skulist: '|| to_char(L_skulist));
         close C_LOCK_SKULIST_DETAIL;
 
         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_DETAIL','Skulist: '|| to_char(L_skulist));
         delete from skulist_detail 
          where skulist = L_skulist;
        
         LP_table := 'SKULIST_HEAD_TL';
  
         SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_HEAD_TL','SKULIST_HEAD_TL','Skulist: '|| to_char(L_skulist));
         open C_LOCK_SKULIST_HEAD_TL;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD_TL','SKULIST_HEAD_TL','Skulist: '|| to_char(L_skulist));
         close C_LOCK_SKULIST_HEAD_TL;

         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_HEAD_TL','Skulist: '|| to_char(L_skulist));
         delete from skulist_head_tl
          where skulist = L_skulist;

         LP_table := 'SKULIST_HEAD';
  
         SQL_LIB.SET_MARK('OPEN','C_LOCK_SKULIST_HEAD','SKULIST_HEAD','Skulist: '|| to_char(L_skulist));
         open C_LOCK_SKULIST_HEAD;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD','SKULIST_HEAD','Skulist: '|| to_char(L_skulist));
         close C_LOCK_SKULIST_HEAD;

         SQL_LIB.SET_MARK('DELETE',NULL,'SKULIST_HEAD','Skulist: '|| to_char(L_skulist));
         delete from skulist_head 
          where skulist = L_skulist;

      END LOOP;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_HEAD_LOC','LOC_CLSF_HEAD',NULL);
      open C_LOCK_HEAD_LOC;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_HEAD_LOC','LOC_CLSF_HEAD',NULL);
      close C_LOCK_HEAD_LOC;
      
      LP_table := 'LOC_CLSF_HEAD';
      SQL_LIB.SET_MARK('DELETE',NULL,'LOC_CLSF_HEAD','Location: '||to_char(I_location));
      delete from loc_clsf_head 
       where location = I_location;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_HEAD_LIST%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOC_HEAD_LIST','LOC_CLSF_HEAD','Loc_list: '|| to_char(I_loc_list));
         close C_LOCK_HEAD_LIST;
      end if;
      if C_LOCK_HEAD_LOC%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_HEAD_LOC','LOC_CLSF_HEAD','Location: '||to_char(I_location));
        close C_LOCK_HEAD_LOC;
      end if;
      if C_LOCK_DETAIL_LOC%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_DETAIL_LOC','LOC_CLSF_DETAIL','Location: '|| to_char(I_location)); 
        close C_LOCK_DETAIL_LOC;
      end if;
      if C_LOCK_SKULIST_CRITERIA%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_CRITERIA','SKULIST_CRITERIA','Skulist: '||to_char(L_skulist)); 
        close C_LOCK_SKULIST_CRITERIA;
      END IF;
      if C_LOCK_SKULIST_HEAD_TL%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD_TL','SKULIST_HEAD_TL','Skulist: '||to_char(L_skulist)); 
        close C_LOCK_SKULIST_HEAD_TL;
      end if;
      if C_LOCK_SKULIST_HEAD%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_HEAD','SKULIST_HEAD','Skulist: '||to_char(L_skulist)); 
        close C_LOCK_SKULIST_HEAD;
      end if;
      if C_LOCK_SKULIST_DETAIL%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_LOCK_SKULIST_DETAIL','SKULIST_DETAIL','Skulist: '||to_char(L_skulist)); 
        close C_LOCK_SKULIST_DETAIL;
      end if;
      if C_INFO_LIST_DETAIL%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_INFO_LIST_DETAIL','LOC_CLSF_DETAIL','Location: '||to_char(L_location)); 
        close C_INFO_LIST_DETAIL;
      end if;
      if C_INFO_LOC_DETAIL%ISOPEN then
        SQL_LIB.SET_MARK('CLOSE','C_INFO_LOC_DETAIL','LOC_CLSF_DETAIL','Location: '||to_char(L_location)); 
        close C_INFO_LOC_DETAIL;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            LP_table,
                                            LP_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_CLASSIFICATION_SQL.DELETE_LOC_CLSF',
                                            TO_CHAR(SQLCODE));
      return FALSE;   

END DELETE_LOC_CLSF;
------------------------------------------------------------------------------------
FUNCTION INSERT_LOC_CLSF_HEAD(O_error_message       IN OUT VARCHAR2,
                              I_loc_list            IN     LOC_LIST_DETAIL.LOC_LIST%TYPE,
                              I_classification      IN     LOC_CLSF_HEAD.CLASSIFICATION%TYPE,
                              I_classification_type IN     LOC_CLSF_HEAD.CLASSIFICATION_TYPE%TYPE,
                              I_recalculate_ind     IN     LOC_CLSF_HEAD.RECALCULATE_IND%TYPE)
   RETURN BOOLEAN is
  
   L_loc_type   loc_clsf_head.loc_type%TYPE;
   L_location   loc_clsf_head.location%TYPE;

   cursor C_LOCK_LOC_CLASS is
      select 'x' 
        from loc_clsf_head
       where location = L_location
         for update nowait;

   cursor C_LOCATIONS is
      select          lld.location,
                      lld.loc_type
        from          loc_list_detail lld
       where          lld.loc_list = I_loc_list
         and          lld.loc_type = 'S'
      UNION
      select distinct wh.wh,
                      lld.loc_type
        from          wh, 
                      loc_list_detail lld
       where          lld.loc_list = I_loc_list
         and          (wh.wh = lld.location OR lld.location = wh.physical_wh) 
         and		  wh.stockholding_ind = 'Y';


BEGIN
   FOR C_rec in C_LOCATIONS LOOP
      L_location := C_rec.location;
      L_loc_type := C_rec.loc_type;
      SQL_LIB.SET_MARK('OPEN','C_LOCK_LOC_CLASS','LOC_CLSF_HEAD','LOCATION: '||L_location);
      open C_LOCK_LOC_CLASS;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_CLASS','LOC_CLSF_HEAD','LOCATION: '||L_location);
      close C_LOCK_LOC_CLASS;
    
       update loc_clsf_head
          set classification       = I_classification,
              classification_type  = I_classification_type,
              recalculate_ind      = I_recalculate_ind
        where location = L_location;

      if SQL%NOTFOUND then

         insert into loc_clsf_head (loc_type,
                                    location,
                                    classification,
                                    classification_type,
                                    recalculate_ind)
              values (L_loc_type,
                      L_location,
                      I_classification,
                      I_classification_type,
                      I_recalculate_ind);
      end if; 
   END LOOP;
   
   return TRUE;
   
EXCEPTION
   when RECORD_LOCKED then
      if C_locations%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_CLASS','LOC_LIST_DETAIL','LOCATION: '||L_location);
         close C_locations;       
      end if;
      if C_lock_loc_class%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOC_CLASS','LOC_CLSF_HEAD','LOCATION: '||L_location);
         close C_lock_loc_class;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_location,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'LOC_CLASSIFICATION_SQL.INSERT_LOC_CLSF_HEAD',
                                            TO_CHAR(SQLCODE));
      return FALSE;   
         
END INSERT_LOC_CLSF_HEAD;
--------------------------------------------------------------------------------------
FUNCTION INSERT_LOC_CLSF_DETAIL(O_error_message    IN OUT VARCHAR2,
                                I_loc_list         IN     LOC_LIST_DETAIL.LOC_LIST%TYPE,
                                I_class_level      IN     LOC_CLSF_DETAIL.CLASS_LEVEL%TYPE,
                                I_class_pct        IN     LOC_CLSF_DETAIL.CLASS_PCT%TYPE) 
   RETURN BOOLEAN is

   L_loc_type    loc_clsf_detail.loc_type%TYPE;
   L_location    loc_clsf_detail.location%TYPE;

   cursor C_LOCK_CLSF_DETAIL is 
      select 'x' 
        from loc_clsf_detail lcd
       where lcd.location    = L_location
         and lcd.class_level = I_class_level 
         for update nowait;

   cursor C_GET_LOCATION is
      select          lld.location,
                      lld.loc_type
        from          loc_list_detail lld
       where          lld.loc_list = I_loc_list
         and          lld.loc_type = 'S'
      UNION
      select distinct wh.wh,
                      lld.loc_type
        from          wh, 
                      loc_list_detail lld
       where          lld.loc_list = I_loc_list
         and          (wh.wh = lld.location OR lld.location = wh.physical_wh) 
         and		  wh.stockholding_ind = 'Y'; 
         
BEGIN
   FOR C_rec in C_GET_LOCATION LOOP

      L_loc_type := C_rec.loc_type;
      L_location := C_rec.location;
 
      SQL_LIB.SET_MARK('OPEN','C_LOCK_CLSF_DETAIL','LOC_CLSF_DETAIL','Location: '||to_char(L_location));
      open C_LOCK_CLSF_DETAIL;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_CLSF_DETAIL','LOC_CLSF_DETAIL','Location: '||to_char(L_location));
      close C_LOCK_CLSF_DETAIL;

      update loc_clsf_detail
         set class_pct   = I_class_pct
       where location    = L_location
         and class_level = I_class_level;

      if SQL%NOTFOUND then
         insert into loc_clsf_detail (loc_type,
                                      location,
                                      class_level,
                                      class_pct,
                                      skulist)
              values(L_loc_type, 
                     L_location, 
                     I_class_level, 
                     I_class_pct,
                     NULL);
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_lock_clsf_detail%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_CLSF_DETAIL','LOC_CLSF_DETAIL','Location: '||to_char(L_location));
         close C_lock_clsf_detail;
      end if;
      if C_get_location%ISOPEN then
         SQL_LIB.SET_MARK('CLOSE','C_GET_LOCATION','LOC_LIST_HEAD','Location: '||to_char(L_location));
         close C_get_location;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                L_location,
                                                NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                SQLERRM,
                                                'LOCATION_CLASSIFICATION_SQL.INSERT_LOC_CLSF_DETAIL',
                                                to_char(SQLCODE));
      return FALSE;
END INSERT_LOC_CLSF_DETAIL;

---------------------------------------------------------------------------------------
END LOC_CLASSIFICATION_SQL;
/


