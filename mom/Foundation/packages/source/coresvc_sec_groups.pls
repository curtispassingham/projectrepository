CREATE OR REPLACE PACKAGE CORESVC_SEC_GROUP AUTHID CURRENT_USER AS
   template_key             CONSTANT VARCHAR2(255) :='SEC_GROUP_DATA';
   
   template_category        CONSTANT VARCHAR2(255) := 'RMSSEC';
   action_column            VARCHAR2(255)          := 'ACTION';
   
   action_new               VARCHAR2(25)           := 'NEW';
   action_mod               VARCHAR2(25)           := 'MOD';
   action_del               VARCHAR2(25)           := 'DEL';
                                                   
   SEC_GROUP_sheet          VARCHAR2(255)          := 'SEC_GROUP';
   SEC_GROUP$Action         NUMBER                 :=1;
   SEC_GROUP$GROUP_ID       NUMBER                 :=2;
   SEC_GROUP$GROUP_NAME     NUMBER                 :=3;
   SEC_GROUP$ROLE           NUMBER                 :=4;
   SEC_GROUP$COMMENTS       NUMBER                 :=5; 
                                                   
   SEC_GROUP_TL_sheet       VARCHAR2(255)          := 'SEC_GROUP_TL';
   SEC_GROUP_TL$Action      NUMBER                 :=1;
   SEC_GROUP_TL$LANG        NUMBER                 :=2;
   SEC_GROUP_TL$GROUP_ID    NUMBER                 :=3;
   SEC_GROUP_TL$GROUP_NAME  NUMBER                 :=4;
   sheet_name_trans S9T_PKG.trans_map_typ;
   
   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
END CORESVC_SEC_GROUP;
/
