
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEMLIST_MC_REJECTS_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
L_current_item  VARCHAR2(255);
-----------------------------------------------------------------------------
--- Function Name:   INSERT_REJECTS
--- Purpose:         This procedure will insert a record into the mc_rejections table.
--- Calls functions: GET_REJECT_REASONS
--- Created:         13-MAY-96 by Greg Berkhof
--- Modified:        29-JUL-96 by Matt Sniffen
-----------------------------------------------------------------------------
FUNCTION INSERT_REJECTS (O_error_message IN OUT VARCHAR2,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE,
                         I_locn_type     IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_locn          IN     ITEM_LOC.LOC%TYPE,
                     	 I_type          IN     VARCHAR2,
                     	 I_key           IN     VARCHAR2,
                         I_user_id       IN     USER_USERS.USERNAME%TYPE,
                         I_txt_1         IN     VARCHAR2 := NULL,
                      	 I_txt_2         IN     VARCHAR2 := NULL,
                     	 I_txt_3         IN     VARCHAR2 := NULL) 
return BOOLEAN;
----------------------------------------------------------------------------
--- Function Name: GET_REJECT_REASON
--- Purpose:       Retrieves the rejection reason and its substrings for the 
---                rejection message.
--- Called by:     INSERT_REJECTS
--- Created:       13-MAY-96 by Greg Burkhof
--- Modified:      29-JUL-96 by Matt Sniffen
----------------------------------------------------------------------------
FUNCTION GET_REJECT_REASON(O_error_message IN OUT VARCHAR2,
                           O_reason              IN OUT VARCHAR2,
                           I_key                 IN     VARCHAR2,
                           I_txt_1               IN     VARCHAR2 := null,
                           I_txt_2               IN     VARCHAR2 := null,
                           I_txt_3               IN     VARCHAR2 := null) 
return BOOLEAN;     
----------------------------------------------------------------------------
--- Function Name: DELETE_REJECTS
--- Purpose:       Deletes the rejects of a give type for the user.
----------------------------------------------------------------------------
FUNCTION DELETE_REJECTS(O_error_message   IN OUT   VARCHAR2,
                        I_change_type     IN       MC_REJECTIONS.CHANGE_TYPE%TYPE,
                        I_user            IN       VARCHAR2)
  RETURN BOOLEAN;
----------------------------------------------------------------------------
END ITEMLIST_MC_REJECTS_SQL;
/


