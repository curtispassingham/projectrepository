CREATE OR REPLACE PACKAGE SUPPLIER_XML AUTHID CURRENT_USER AS

-------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------
PROCEDURE BUILD_SUPPLIER(I_sups     IN    SUPS%ROWTYPE,
                         I_event    IN    VARCHAR2,
                         O_msg      OUT   CLOB,
                         O_status   OUT   VARCHAR2,
                         O_text     OUT   VARCHAR2);

PROCEDURE BUILD_ADDRESS(I_addr      IN    ADDR%ROWTYPE,
                        I_event     IN    VARCHAR2,
                        O_msg       OUT   CLOB,
                        O_status    OUT   VARCHAR2,
                        O_text      OUT   VARCHAR2);

PROCEDURE BUILD_ORG_UNIT(I_pou      IN    PARTNER_ORG_UNIT%ROWTYPE,
                         I_event    IN    VARCHAR2,
                         O_msg      OUT   CLOB,
                         O_status   OUT   VARCHAR2,
                         O_text     OUT   VARCHAR2);

FUNCTION GET_KEYS(O_error_message    OUT   VARCHAR2,
                  O_valid            OUT   BOOLEAN,
                  O_ret_allow_ind    OUT   VARCHAR2,
                  I_ret_allow_ind    IN    VARCHAR2,
                  I_supplier         IN    SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;

END;
/
