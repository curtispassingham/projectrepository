create or replace PACKAGE body coresvc_cflex
AS
PROCEDURE print_debug
IS
  l_index VARCHAR2(60);
BEGIN
  l_index       := cfa_sql.GP_key_value_tbl.first;
  WHILE l_index IS NOT NULL
  LOOP
    dbms_output.put_line('key::'||'index: '||l_index||' name : '||cfa_sql.GP_key_value_tbl(l_index).field_name||' val : '||cfa_sql.GP_key_value_tbl(l_index).field_value);
    l_index := cfa_sql.GP_key_value_tbl.next(l_index);
  END LOOP;
  l_index       := cfa_sql.GP_attrib_value_tbl.first;
  WHILE l_index IS NOT NULL
  LOOP
    dbms_output.put_line('val::'||'index: '||l_index||' name : '||cfa_sql.GP_attrib_value_tbl(l_index).field_name||' val : '||cfa_sql.GP_attrib_value_tbl(l_index).field_value);
    l_index := cfa_sql.GP_attrib_value_tbl.next(l_index);
  END LOOP;
END print_debug;
--------------------------------------------------------------------------------
FUNCTION add_item_cflex_sheets(
    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file              IN NUMBER,
    I_process_id        IN NUMBER,
    I_template_key      IN VARCHAR2,
    I_template_only_ind IN CHAR DEFAULT 'N')
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.ADD_ITEM_CFLEX_SHEETS';
  L_sql     VARCHAR2(4000);
BEGIN
  FOR rec IN
  (SELECT gsl.label,
    gs.group_set_view_name,
    ext.base_rms_table,
    ext.custom_ext_table
  FROM cfa_ext_entity ext,
    cfa_attrib_group_set gs,
    cfa_attrib_group_set_labels gsl
  WHERE gs.group_set_view_name IN
    (SELECT wksht_key
    FROM s9t_tmpl_wksht_def
    WHERE template_key = I_template_key
    AND mandatory      = 'Y'
    )
  AND gs.ext_entity_id                      = ext.ext_entity_id
  AND gsl.group_set_id                      = gs.group_set_id
  AND gsl.lang                              = get_user_lang
  AND is_valid_view(gs.group_set_view_name) = 'Y'
  )
  LOOP
    L_sql := 'select tbl.* from '                                                                                                      --
    || rec.group_set_view_name                                                                                                         --
    ||' tbl WHERE '||item_induct_sql.get_item_col(rec.base_rms_table)||' IN (SELECT item FROM svc_item_search_temp WHERE process_id = '--
    ||I_process_id                                                                                                                     --
    ||' ) '                                                                                                                            --
    ||' and '''||I_template_only_ind||''' = ''N''';
    s9t_pkg.add_sheet_for_sql(I_file,rec.group_set_view_name,L_sql);
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END add_item_cflex_sheets;
--------------------------------------------------------------------------------
FUNCTION add_cflex_sheets(
    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file              IN NUMBER,
    I_template_key      IN VARCHAR2,
    I_template_only_ind IN CHAR DEFAULT 'N')
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.add_cflex_sheets';
  L_sql     VARCHAR2(4000);
BEGIN
   
   FOR rec IN
  (SELECT gsl.label,
    gs.group_set_view_name,
    ext.base_rms_table,
    ext.custom_ext_table
  FROM cfa_ext_entity ext,
    cfa_attrib_group_set gs,
    cfa_attrib_group_set_labels gsl
  WHERE gs.group_set_view_name IN
    (SELECT wksht_key
    FROM s9t_tmpl_wksht_def
    WHERE template_key = I_template_key
    AND mandatory      = 'Y'
    )
  AND gs.ext_entity_id                      = ext.ext_entity_id
  AND gsl.group_set_id                      = gs.group_set_id
  AND gsl.lang                              = get_user_lang
  AND is_valid_view(gs.group_set_view_name) = 'Y'
  )
  LOOP
    L_sql := 'select tbl.* from '
    || rec.group_set_view_name
    ||' tbl ';
    s9t_pkg.add_sheet_for_sql(I_file,rec.group_set_view_name,L_sql);
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END add_cflex_sheets;
--------------------------------------------------------------------------------
FUNCTION add_order_cflex_sheets(
    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file              IN NUMBER,
    I_process_id        IN NUMBER,
    I_template_key      IN VARCHAR2,
    I_template_only_ind IN CHAR DEFAULT 'N')
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.ADD_ORDER_CFLEX_SHEETS';
  L_sql     VARCHAR2(4000);
BEGIN
  FOR rec IN
  (SELECT gsl.label,
          gs.group_set_view_name,
          ext.base_rms_table,
          ext.custom_ext_table
     FROM cfa_ext_entity ext,
          cfa_attrib_group_set gs,
          cfa_attrib_group_set_labels gsl
    WHERE gs.group_set_view_name IN
          (SELECT wksht_key
             FROM s9t_tmpl_wksht_def
            WHERE template_key = I_template_key
              AND mandatory    = 'Y')
      AND gs.ext_entity_id                      = ext.ext_entity_id
      AND gsl.group_set_id                      = gs.group_set_id
      AND gsl.lang                              = get_user_lang
      AND is_valid_view(gs.group_set_view_name) = 'Y'
  )
  LOOP
    L_sql := 'select tbl.* from '                                                                                                      --
    || rec.group_set_view_name                                                                                                         --
    ||' tbl WHERE order_no'||' IN (SELECT order_no FROM svc_po_search_temp WHERE process_id = '--
    ||I_process_id                                                                                                                     --
    ||' ) '                                                                                                                            --
    ||' and '''||I_template_only_ind||''' = ''N''';

    s9t_pkg.add_sheet_for_sql(I_file,rec.group_set_view_name,L_sql);
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END add_order_cflex_sheets;
--------------------------------------------------------------------------------
FUNCTION add_cost_chng_cflex_sheets(
    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file              IN NUMBER,
    I_process_id        IN NUMBER,
    I_template_key      IN VARCHAR2,
    I_template_only_ind IN CHAR DEFAULT 'N')
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.ADD_COST_CHNG_CFLEX_SHEETS';
  L_sql     VARCHAR2(4000);
BEGIN
  FOR rec IN
  (SELECT gsl.label,
          gs.group_set_view_name,
          ext.base_rms_table,
          ext.custom_ext_table
     FROM cfa_ext_entity ext,
          cfa_attrib_group_set gs,
          cfa_attrib_group_set_labels gsl
    WHERE gs.group_set_view_name IN
          (SELECT wksht_key
             FROM s9t_tmpl_wksht_def
            WHERE template_key = I_template_key
              AND mandatory    = 'Y')
      AND gs.ext_entity_id                      = ext.ext_entity_id
      AND gsl.group_set_id                      = gs.group_set_id
      AND gsl.lang                              = get_user_lang
      AND is_valid_view(gs.group_set_view_name) = 'Y'
  )
  LOOP
    L_sql := 'select tbl.* from '                                                                                                      --
    || rec.group_set_view_name                                                                                                         --
    ||' tbl WHERE cost_change'||' IN (SELECT cost_change FROM svc_cost_chg_search_temp WHERE process_id = '                            --
    ||I_process_id                                                                                                                     --
    ||' ) '                                                                                                                            --
    ||' and '''||I_template_only_ind||''' = ''N''';

    s9t_pkg.add_sheet_for_sql(I_file,rec.group_set_view_name,L_sql);
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END add_cost_chng_cflex_sheets;
--------------------------------------------------------------------------------
FUNCTION process_s9t_sheets(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file          IN NUMBER,
    I_process_id    IN NUMBER,
    I_template_key  IN VARCHAR2 )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.PROCESS_S9T_ITEM';
BEGIN
  FOR rec IN
  ( WITH group_sets AS
  (SELECT gsl.label,
    gs.group_set_view_name,
    gs.group_set_id,
    ext.custom_ext_table
  FROM cfa_ext_entity ext,
    cfa_attrib_group_set gs,
    cfa_attrib_group_set_labels gsl
  WHERE gs.group_set_view_name IN
    (SELECT wksht_key
    FROM s9t_tmpl_wksht_def
    WHERE template_key = I_template_key
    AND mandatory      = 'Y'
    )
  AND gs.ext_entity_id                      = ext.ext_entity_id
  AND gsl.group_set_id                      = gs.group_set_id
  AND gsl.lang                              = get_user_lang
  AND is_valid_view(gs.group_set_view_name) = 'Y'
  ),
  sheets AS
  (SELECT column_value AS sheet_name
  FROM TABLE
    (SELECT sf.s9t_file_obj.sheet_names
    FROM s9t_folder sf
    WHERE file_id = I_file
    )
  ),
  sheets_keys AS
  (SELECT sh.sheet_name,
    NVL(wd.wksht_key,sh.sheet_name) AS wksht_key
  FROM sheets sh,
    v_s9t_tmpl_wksht_def wd
  WHERE template_key (+) = I_template_key
  AND sh.sheet_name      = wd.wksht_name(+)
  )
SELECT *
FROM group_sets gs,
  sheets_keys sh
WHERE gs.group_set_view_name = sh.wksht_key
  )
  LOOP
    IF PROCESS_S9T_CFA(O_error_message,I_file,I_process_id,rec.sheet_name,rec.group_set_view_name)=false THEN
      RETURN false;
    END IF;
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END process_s9t_sheets;
--------------------------------------------------------------------------------
FUNCTION process_s9t_sheets_adminapi(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file          IN NUMBER,
    I_process_id    IN NUMBER,
    I_template_key  IN VARCHAR2 )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.PROCESS_S9T_SHEETS_ADMINAPI';
BEGIN
  FOR rec IN
  ( WITH group_sets AS
  (SELECT gsl.label,
    gs.group_set_view_name,
    gs.group_set_id,
    ext.custom_ext_table
  FROM cfa_ext_entity ext,
    cfa_attrib_group_set gs,
    cfa_attrib_group_set_labels gsl
  WHERE gs.group_set_view_name IN
    (SELECT wksht_key
    FROM s9t_tmpl_wksht_def
    WHERE template_key = I_template_key
    AND mandatory      = 'Y'
    )
  AND gs.ext_entity_id                      = ext.ext_entity_id
  AND gsl.group_set_id                      = gs.group_set_id
  AND gsl.lang                              = get_user_lang
  AND is_valid_view(gs.group_set_view_name) = 'Y'
  ),
  sheets AS
  (SELECT column_value AS sheet_name
  FROM TABLE
    (SELECT sf.s9t_file_obj.sheet_names
    FROM s9t_folder sf
    WHERE file_id = I_file
    )
  ),
  sheets_keys AS
  (SELECT sh.sheet_name,
    NVL(wd.wksht_key,sh.sheet_name) AS wksht_key
  FROM sheets sh,
    v_s9t_tmpl_wksht_def wd
  WHERE template_key (+) = I_template_key
  AND sh.sheet_name      = wd.wksht_name(+)
  )
SELECT *
FROM group_sets gs,
  sheets_keys sh
WHERE gs.group_set_view_name = sh.wksht_key
  )
  LOOP
    IF PROCESS_S9T_CFA_ADMINAPI(O_error_message,I_file,I_process_id,rec.sheet_name,rec.group_set_view_name)=false THEN
      RETURN false;
    END IF;
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END process_s9t_sheets_adminapi;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T_CFA(
    O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file                IN NUMBER,
    I_process_id          IN NUMBER,
    I_sheet_name          IN VARCHAR2,
    I_group_set_view_name IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.PROCESS_S9T_CFA';
  
  CURSOR c_get_keys
  IS
    SELECT ck.base_rms_table,
      CAST(collect(key_col
    ORDER BY ck.key_number) AS s9t_cells) AS key_cols_tab
    FROM cfa_ext_entity ext,
      cfa_attrib_group_set gs,
      cfa_ext_entity_key ck
    WHERE gs.group_set_view_name = I_group_set_view_name
    AND gs.ext_entity_id         = ext.ext_entity_id
    AND ck.base_rms_table        = ext.base_rms_table
    GROUP BY ck.base_rms_table;
  l_base_table VARCHAR2(255);
  l_keys_tab s9t_cells;
  l_keys_col key_val_pairs;
  l_attrs_col key_val_pairs;
BEGIN
  OPEN c_get_keys;
  FETCH c_get_keys
  INTO l_base_table,
    l_keys_tab;
  CLOSE c_get_keys;
  FOR rec IN
  (SELECT tb.column_value AS pairs
  FROM TABLE(s9t_pkg.get_key_val_pairs_tab(I_file,I_sheet_name)) tb
  )
  LOOP
    l_keys_col  := NEW key_val_pairs();
    l_attrs_col := rec.pairs;
    FOR i IN 1..l_keys_tab.count
    LOOP
      l_keys_col.extend;
      l_keys_col(l_keys_col.count) := NEW key_value_pair(l_keys_tab(i),key_val_pairs_pkg.get_attr(rec.pairs,l_keys_tab(i)));
      l_attrs_col                  := key_val_pairs_pkg.delete_key(l_attrs_col,l_keys_tab(i));
    END LOOP;
    BEGIN
      DELETE
        FROM svc_cfa_ext
       WHERE key_val_pairs_pkg.get_map(keys_col)=key_val_pairs_pkg.get_map(l_keys_col)
         AND process_id                          <> I_process_id;
      --
      INSERT
      INTO svc_cfa_ext
        (
          process_id,
          row_seq,
          action,
          base_rms_table,
          group_set_view_name,
          keys_col,
          attrs_col,
          create_id,
          create_datetime
        )
        VALUES
        (
          I_process_id,
          key_val_pairs_pkg.get_attr(rec.pairs,'ROW_SEQ'),
          key_val_pairs_pkg.get_attr(rec.pairs,'ACTION'),
          l_base_table,
          I_group_set_view_name,
          l_keys_col,
          l_attrs_col,
          get_user,
          sysdate
        );
    EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      s9t_pkg.write_s9t_error(I_file,I_group_set_view_name,key_val_pairs_pkg.get_attr(rec.pairs,'ROW_SEQ'),NULL,NULL,'DUP_REC_EXISTS');
    END;
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END PROCESS_S9T_CFA;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T_CFA_ADMINAPI(
    O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file                IN NUMBER,
    I_process_id          IN NUMBER,
    I_sheet_name          IN VARCHAR2,
    I_group_set_view_name IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.PROCESS_S9T_CFA_ADMINAPI';
  CURSOR c_get_keys
  IS
    SELECT ck.base_rms_table,
      CAST(collect(key_col
    ORDER BY ck.key_number) AS s9t_cells) AS key_cols_tab
    FROM cfa_ext_entity ext,
      cfa_attrib_group_set gs,
      cfa_ext_entity_key ck
    WHERE gs.group_set_view_name = I_group_set_view_name
    AND gs.ext_entity_id         = ext.ext_entity_id
    AND ck.base_rms_table        = ext.base_rms_table
    GROUP BY ck.base_rms_table;
  l_base_table VARCHAR2(255);
  l_keys_tab s9t_cells;
  l_keys_col key_val_pairs;
  l_attrs_col key_val_pairs;
BEGIN
  OPEN c_get_keys;
  FETCH c_get_keys
  INTO l_base_table,
    l_keys_tab;
  CLOSE c_get_keys;
  FOR rec IN
  (SELECT tb.column_value AS pairs
  FROM TABLE(s9t_pkg.get_key_val_pairs_tab(I_file,I_sheet_name)) tb
  )
  LOOP
    l_keys_col  := NEW key_val_pairs();
    l_attrs_col := rec.pairs;
    FOR i IN 1..l_keys_tab.count
    LOOP
      l_keys_col.extend;
      l_keys_col(l_keys_col.count) := NEW key_value_pair(l_keys_tab(i),UPPER(key_val_pairs_pkg.get_attr(rec.pairs,l_keys_tab(i))));
      l_attrs_col                  := key_val_pairs_pkg.delete_key(l_attrs_col,l_keys_tab(i));
    END LOOP;
    BEGIN
      DELETE
      FROM svc_cfa_ext
      WHERE key_val_pairs_pkg.get_map(keys_col)=key_val_pairs_pkg.get_map(l_keys_col)
      AND process_id                          <> I_process_id;
      --
      INSERT
      INTO svc_cfa_ext
        (
          PROCESS_ID,
          ROW_SEQ,
          ACTION,
          base_rms_table,
          group_set_view_name,
          keys_col,
          attrs_col
        )
        VALUES
        (
          I_process_id,
          key_val_pairs_pkg.get_attr(rec.pairs,'ROW_SEQ'),
          key_val_pairs_pkg.get_attr(rec.pairs,'ACTION'),
          l_base_table,
          I_group_set_view_name,
          l_keys_col,
          l_attrs_col
        );
    EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      s9t_pkg.write_s9t_error(I_file,I_group_set_view_name,key_val_pairs_pkg.get_attr(rec.pairs,'ROW_SEQ'),NULL,NULL,'DUP_REC_EXISTS');
    END;
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END PROCESS_S9T_CFA_ADMINAPI;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CFA
  (
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_err_tab OUT typ_err_tab,
    I_process_id IN NUMBER,
    I_chunk_id   IN NUMBER
  )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.PROCESS_CFA';
  CURSOR c_recs
  IS
    SELECT sc.PROCESS_ID ,
           sc.CHUNK_ID ,
           sc.ROW_SEQ ,
           sc.ACTION ,
           sc.BASE_RMS_TABLE ,
           sc.GROUP_SET_VIEW_NAME ,
           sc.KEYS_COL ,
           sc.ATTRS_COL ,
           gs.group_set_id,
           gs.default_func,
           sf.template_key,
           sc.rowid AS rid
      FROM svc_cfa_ext sc,
           cfa_attrib_group_set gs,
           svc_process_tracker sp,
           s9t_folder sf
     WHERE sc.process_id          = I_process_id
       AND chunk_id               = I_chunk_id
       AND sp.process_id          = I_process_id
       AND sp.file_id             = sf.file_id (+)
       AND sc.group_set_view_name = gs.group_set_view_name
       AND sc.process$status      = 'N';
  L_set_fields_tbl CFA_SQL.TYP_VALUE_TBL;
  L_err_field VARCHAR2(4000);
  ex_custom   EXCEPTION;
BEGIN
  O_err_tab := NEW typ_err_tab();
  FOR rec IN c_recs
  LOOP
    DECLARE
      l_err_attrib    VARCHAR2(255):=NULL;
      l_temp_attr_var VARCHAR2(4000);
    BEGIN
      GV_base_table := rec.BASE_RMS_TABLE;
      
      /*
      IF nvl(key_val_pairs_pkg.get_attr(rec. ATTRS_COL, 'ACTION'), 'X') <> 'MOD' THEN
         l_err_attrib := 'ACTION';
         O_error_message := 'INV_ACT';
         raise ex_custom;
      END IF;
      */
      
      FOR i IN 1..rec.KEYS_COL.count
      LOOP
        IF i <= 20 THEN
          EXECUTE immediate 'begin coresvc_cflex.GV_key_name_'||i||' := :1; coresvc_cflex.GV_key_value_'||i||' := :2; end;' USING rec.KEYS_COL(i).a_key,
          rec.KEYS_COL(i).a_val;
        END IF;
      END LOOP;
      IF CFA_SQL.BUILD_KEY_TBL(--
        O_error_message,       --
        GV_base_table,         --
        GV_key_name_1,         --
        GV_key_value_1,        --
        GV_key_name_2,         --
        GV_key_value_2,        --
        GV_key_name_3,         --
        GV_key_value_3,        --
        GV_key_name_4,         --
        GV_key_value_4,        --
        GV_key_name_5,         --
        GV_key_value_5,        --
        GV_key_name_6,         --
        GV_key_value_6,        --
        GV_key_name_7,         --
        GV_key_value_7,        --
        GV_key_name_8,         --
        GV_key_value_8,        --
        GV_key_name_9,         --
        GV_key_value_9,        --
        GV_key_name_10,        --
        GV_key_value_10,       --
        GV_key_name_11,        --
        GV_key_value_11,       --
        GV_key_name_12,        --
        GV_key_value_12,       --
        GV_key_name_13,        --
        GV_key_value_13,       --
        GV_key_name_14,        --
        GV_key_value_14,       --
        GV_key_name_15,        --
        GV_key_value_15,       --
        GV_key_name_16,        --
        GV_key_value_16,       --
        GV_key_name_17,        --
        GV_key_value_17,       --
        GV_key_name_18,        --
        GV_key_value_18,       --
        GV_key_name_19,        --
        GV_key_value_19,       --
        GV_key_name_20,        --
        GV_key_value_20        --
        ) = FALSE THEN
        raise ex_custom;
      END IF;
      ---
      IF CFA_VALIDATE_SQL.QUALIFY_GROUP_SET(O_error_message, rec.group_set_id) = FALSE THEN
        raise ex_custom;
      END IF;
      --
      IF NOT CFA_SQL.INITIALIZE(--
        O_error_message,        --
        rec.BASE_RMS_TABLE,     --
        rec.group_set_id,       --
        get_user_lang,          --
        'N'                     --simulate indicator
        ) THEN
        raise ex_custom;
      END IF;
      --
      -- Fetch Data subset from extension table
      IF NOT CFA_SQL.BUILD_ATTRIB_TBL(--
        O_error_message,              --
        rec.BASE_RMS_TABLE,           --
        rec.group_set_id,             --
        'N'                           --simulate ind
        ) THEN
        raise ex_custom;
      END IF;
      --
      print_debug;
      -- Set user defined default values for the attributes
      IF NOT CFA_VALIDATE_SQL.DEFAULT_GROUP_SET(--
        O_error_message,                        --
        rec.BASE_RMS_TABLE,                     --
        rec.group_set_id                        --
        ) THEN
        raise ex_custom;
      END IF;
      FOR attr_rec IN
      (SELECT cg.group_id,
        ca.view_col_name,
        attr.a_val,
        lead(cg.group_id) over (order by cg.group_id) AS next_gid
      FROM TABLE ( rec.attrs_col ) attr,
        cfa_attrib ca,
        cfa_attrib_group cg
      WHERE cg.group_set_id = rec.group_set_id
      AND ca.group_id       = cg.group_id
      AND attr.a_key        = ca.view_col_name
      ORDER BY cg.group_id
      )
      LOOP
        IF attr_rec.a_val IS NULL AND rec.default_func IS NULL AND is_new_record(rec.group_set_view_name,rec.keys_col) = 'Y' THEN
          l_temp_attr_var := get_tmpl_default(rec.template_key,rec.group_set_view_name,attr_rec.view_col_name);
        ELSE
          l_temp_attr_var := attr_rec.a_val ;
        END IF;
        IF CFA_VALIDATE_SQL.VALIDATE_ATTRIB(--
          O_error_message                   --
          ,L_set_fields_tbl                 --
          ,attr_rec.group_id                --
          ,attr_rec.view_col_name           --
          ,l_temp_attr_var                  --
          )             =false THEN
          l_err_attrib := attr_rec.view_col_name;
          raise ex_custom;
        END IF;
        --
        IF NVL(attr_rec.next_gid,-1) <> attr_rec.group_id THEN
          IF NOT CFA_VALIDATE_SQL.VALIDATE_GROUP(O_error_message, L_err_field, attr_rec.group_id) THEN
            raise ex_custom;
          END IF;
        END IF;
        --
      END LOOP;
      --
      IF NOT CFA_VALIDATE_SQL.VALIDATE_GROUP_SET(O_error_message, rec.group_set_id) THEN
        raise ex_custom;
      END IF;
      --
      IF NOT CFA_SQL.PERSIST_DATA(O_error_message) THEN
        raise ex_custom;
      END IF;
      UPDATE svc_cfa_ext 
         SET process$status = 'P',
             last_upd_id = get_user,
             last_upd_datetime = sysdate             
       WHERE rowid = rec.rid;
    EXCEPTION
    WHEN ex_custom THEN
      IF O_error_message LIKE '%ORA-02291%' THEN
        O_error_message := 'CFA_NO_BASE_REC';
      END IF;
      IF O_error_message LIKE '%ORA-01830%' THEN
        O_error_message := 'IIND-ORA--1830';
      END IF; 
      IF O_error_message LIKE '%ORA-01839%' THEN
        O_error_message := 'IIND-ORA--1839';
      END IF;
      IF O_error_message LIKE '%ORA-01841%' THEN
        O_error_message := 'IIND-ORA--1841';
      END IF;           
      IF O_error_message LIKE '%ORA-01843%' THEN
        O_error_message := 'IIND-ORA--1843';
      END IF;      
      IF O_error_message LIKE '%ORA-01847%' THEN
        O_error_message := 'IIND-ORA--1847';
      END IF;
      IF O_error_message LIKE '%ORA-01858%' THEN
        O_error_message := 'IIND-ORA--1858';
      END IF;
      IF O_error_message LIKE '%ORA-01861%' THEN
        O_error_message := 'IIND-ORA--1861';
      END IF;   
      IF O_error_message LIKE '%ORA-06502%' THEN
        O_error_message := 'IIND-ORA--6502';
      END IF;
      O_err_tab.extend;
      O_err_tab(O_err_tab.count).rid       :=rec.rid;
      O_err_tab(O_err_tab.count).err_msg   :=O_error_message;
      O_err_tab(O_err_tab.count).view_name := rec.GROUP_SET_VIEW_NAME;
      O_err_tab(O_err_tab.count).row_seq   := rec.row_seq;
      O_err_tab(O_err_tab.count).attrib    := l_err_attrib;
      UPDATE svc_cfa_ext
         SET process$status = 'E',
             last_upd_id = get_user,
             last_upd_datetime = sysdate
       WHERE rowid          = rec.rid;
    END;
    --print_debug;
  END LOOP;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END PROCESS_CFA;
--------------------------------------------------------------------------------
FUNCTION item_svc2s9t(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file          IN NUMBER,
    I_process_id    IN NUMBER,
    I_template_key  IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.item_svc2s9t';
BEGIN
  IF add_item_cflex_sheets(--
    O_error_message,       --
    I_file,                --
    I_process_id,          --
    I_template_key,        --
    'Y'                    -- Template only ind, Just to populate blank sheet. Rows will be added later
    )=false THEN
    RETURN false;
  END IF;
  FOR rec IN
  (SELECT gs.group_set_view_name,
    ext.base_rms_table,
    ext.custom_ext_table
  FROM cfa_ext_entity ext,
    cfa_attrib_group_set gs,
    cfa_attrib_group_set_labels gsl
  WHERE gs.group_set_view_name IN
    (SELECT wksht_key
    FROM s9t_tmpl_wksht_def
    WHERE template_key = I_template_key
    AND mandatory      = 'Y'
    )
  AND gs.ext_entity_id = ext.ext_entity_id
  AND gsl.group_set_id = gs.group_set_id
  AND gsl.lang         = get_user_lang
  )
  LOOP
    INSERT
    INTO TABLE
      (SELECT ss.s9t_rows
        FROM s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss
        WHERE sf.file_id  = I_file
        AND ss.sheet_name = rec.group_set_view_name
      )
  WITH cols AS
    (SELECT 1    AS col_group_seq,
      key_col    AS col,
      NULL       AS col_seq1,
      key_number AS col_seq2
    FROM cfa_ext_entity_key ck
    WHERE ck.base_rms_table = rec.base_rms_table
    UNION
    SELECT 2         AS col_group_seq,
      view_col_name  AS col,
      cg.display_seq AS col_seq1,
      ca.display_seq AS col_seq2
    FROM cfa_attrib ca,
      cfa_attrib_group cg,
      cfa_attrib_group_set gs
    WHERE gs.group_set_view_name = rec.group_set_view_name
    AND cg.group_set_id          = gs.group_set_id
    AND ca.group_id              = cg.group_id
    ),
    recs AS
    (SELECT sc.rowid AS sc_rid,
      sc.row_seq,
      CAST(collect(key_val_pairs_pkg.get_attr(DECODE(col_group_seq,1,keys_col,attrs_col),col)
    ORDER BY col_group_seq,col_seq1,col_seq2) AS s9t_cells) AS the_attrs
    FROM cols,
      svc_cfa_ext sc
    WHERE group_set_view_name                                                                 = rec.group_set_view_name
    AND key_val_pairs_pkg.get_attr(keys_col,item_induct_sql.get_item_col(sc.base_rms_table)) IN
      (SELECT item FROM svc_item_search_temp WHERE process_id = I_process_id
      )
    GROUP BY sc.rowid,
      sc.row_seq
    )
  SELECT s9t_row(the_attrs,row_seq) AS srow FROM recs;
END LOOP;
RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END item_svc2s9t;
--------------------------------------------------------------------------------
FUNCTION order_svc2s9t(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file          IN NUMBER,
    I_process_id    IN NUMBER,
    I_template_key  IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.order_svc2s9t';
BEGIN
  IF add_order_cflex_sheets(--
    O_error_message,       --
    I_file,                --
    I_process_id,          --
    I_template_key,        --
    'Y'                    -- Template only ind, Just to populate blank sheet. Rows will be added later
    )=false THEN
    RETURN false;
  END IF;
  FOR rec IN
  (SELECT gs.group_set_view_name,
    ext.base_rms_table,
    ext.custom_ext_table
  FROM cfa_ext_entity ext,
    cfa_attrib_group_set gs,
    cfa_attrib_group_set_labels gsl
  WHERE gs.group_set_view_name IN
    (SELECT wksht_key
    FROM s9t_tmpl_wksht_def
    WHERE template_key = I_template_key
    AND mandatory      = 'Y'
    )
  AND gs.ext_entity_id = ext.ext_entity_id
  AND gsl.group_set_id = gs.group_set_id
  AND gsl.lang         = get_user_lang
  )
  LOOP
    INSERT
    INTO TABLE
      (SELECT ss.s9t_rows
        FROM s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss
        WHERE sf.file_id  = I_file
        AND ss.sheet_name = rec.group_set_view_name
      )
  WITH cols AS
    (SELECT 1    AS col_group_seq,
      key_col    AS col,
      NULL       AS col_seq1,
      key_number AS col_seq2
    FROM cfa_ext_entity_key ck
    WHERE ck.base_rms_table = rec.base_rms_table
    UNION
    SELECT 2         AS col_group_seq,
      view_col_name  AS col,
      cg.display_seq AS col_seq1,
      ca.display_seq AS col_seq2
    FROM cfa_attrib ca,
      cfa_attrib_group cg,
      cfa_attrib_group_set gs
    WHERE gs.group_set_view_name = rec.group_set_view_name
    AND cg.group_set_id          = gs.group_set_id
    AND ca.group_id              = cg.group_id
    ),
    recs AS
    (SELECT sc.rowid AS sc_rid,
      sc.row_seq,
      CAST(collect(key_val_pairs_pkg.get_attr(DECODE(col_group_seq,1,keys_col,attrs_col),col)
    ORDER BY col_group_seq,col_seq1,col_seq2) AS s9t_cells) AS the_attrs
    FROM cols,
      svc_cfa_ext sc
    WHERE sc.group_set_view_name  = rec.group_set_view_name
    AND key_val_pairs_pkg.get_attr(keys_col,(SELECT key_col FROM cfa_ext_entity_key ck 
                                             WHERE ck.base_rms_table = rec.base_rms_table)) IN
      (SELECT order_no FROM svc_po_search_temp WHERE process_id = I_process_id
      )
    GROUP BY sc.rowid,
      sc.row_seq
    )
  SELECT s9t_row(the_attrs,row_seq) AS srow FROM recs;
END LOOP;
RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END order_svc2s9t;
--------------------------------------------------------------------------------
FUNCTION COST_CHANGE_SVC2S9T(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file          IN NUMBER,
    I_process_id    IN NUMBER,
    I_template_key  IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'CORESVC_CFLEX.COST_CHANGE_SVC2S9T';
BEGIN
  IF add_cost_chng_cflex_sheets(--
    O_error_message,       --
    I_file,                --
    I_process_id,          --
    I_template_key,        --
    'Y'                    -- Template only ind, Just to populate blank sheet. Rows will be added later
    )=false THEN
    RETURN false;
  END IF;
  FOR rec IN
  (SELECT gs.group_set_view_name,
    ext.base_rms_table,
    ext.custom_ext_table
  FROM cfa_ext_entity ext,
    cfa_attrib_group_set gs,
    cfa_attrib_group_set_labels gsl
  WHERE gs.group_set_view_name IN
    (SELECT wksht_key
    FROM s9t_tmpl_wksht_def
    WHERE template_key = I_template_key
    AND mandatory      = 'Y'
    )
  AND gs.ext_entity_id = ext.ext_entity_id
  AND gsl.group_set_id = gs.group_set_id
  AND gsl.lang         = get_user_lang
  )
  LOOP
    INSERT
    INTO TABLE
      (SELECT ss.s9t_rows
        FROM s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss
        WHERE sf.file_id  = I_file
        AND ss.sheet_name = rec.group_set_view_name
      )
  WITH cols AS
    (SELECT 1    AS col_group_seq,
      key_col    AS col,
      NULL       AS col_seq1,
      key_number AS col_seq2
    FROM cfa_ext_entity_key ck
    WHERE ck.base_rms_table = rec.base_rms_table
    UNION
    SELECT 2         AS col_group_seq,
      view_col_name  AS col,
      cg.display_seq AS col_seq1,
      ca.display_seq AS col_seq2
    FROM cfa_attrib ca,
      cfa_attrib_group cg,
      cfa_attrib_group_set gs
    WHERE gs.group_set_view_name = rec.group_set_view_name
    AND cg.group_set_id          = gs.group_set_id
    AND ca.group_id              = cg.group_id
    ),
    recs AS
    (SELECT sc.rowid AS sc_rid,
      sc.row_seq,
      CAST(collect(key_val_pairs_pkg.get_attr(DECODE(col_group_seq,1,keys_col,attrs_col),col)
    ORDER BY col_group_seq,col_seq1,col_seq2) AS s9t_cells) AS the_attrs
    FROM cols,
      svc_cfa_ext sc
    WHERE sc.group_set_view_name = rec.group_set_view_name
    AND key_val_pairs_pkg.get_attr(keys_col,DECODE(col_group_seq,1,cols.col))IN
      (SELECT cost_change FROM svc_cost_chg_search_temp WHERE process_id = I_process_id
      )
    GROUP BY sc.rowid,
      sc.row_seq
    )
  SELECT s9t_row(the_attrs,row_seq) AS srow FROM recs;
END LOOP;
RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END COST_CHANGE_SVC2S9T;
--------------------------------------------------------------------------------
FUNCTION is_valid_view(
    I_view_name IN VARCHAR2)
  RETURN CHAR
IS
  invalid_view EXCEPTION;
  PRAGMA EXCEPTION_INIT (invalid_view, -44002);
  l_str VARCHAR2(255);
BEGIN
  l_str := DBMS_ASSERT.SQL_OBJECT_NAME(I_view_name);
  RETURN 'Y';
EXCEPTION
WHEN invalid_view THEN
  RETURN 'N';
END is_valid_view;
--------------------------------------------------------------------------------
FUNCTION is_new_record(
    I_view_name IN VARCHAR2,
    I_keys      IN key_val_pairs)
  RETURN CHAR
IS
  l_sql VARCHAR2(32000);
TYPE CurTyp
IS
  REF
  CURSOR;
    c_check CurTyp;
    l_new_rec VARCHAR2(1):='Y';
  BEGIN
    l_sql := 'select ''N'' from '||I_view_name||' where 1 =1 ';
    FOR i IN 1..I_keys.count
    LOOP
      l_sql := l_sql ||' and '||I_keys(i).a_key||' = '''||key_val_pairs_pkg.get_attr(I_keys,I_keys(i).a_key)||'''';
    END LOOP;
    OPEN c_check FOR l_sql;
    FETCH c_check INTO l_new_rec;
  CLOSE c_check;
  RETURN l_new_rec;
END is_new_record;
--------------------------------------------------------------------------------
FUNCTION get_tmpl_default(
    I_template_key IN VARCHAR2,
    I_wksht_key    IN VARCHAR2,
    I_col_key      IN VARCHAR2)
  RETURN VARCHAR2
IS
  CURSOR c_get
  IS
    SELECT default_value
    FROM s9t_tmpl_cols_def
    WHERE template_key = I_template_key
    AND wksht_key      = I_wksht_key
    AND column_key     = I_col_key;
  O_default_value s9t_tmpl_cols_def.default_value%type;
BEGIN
  OPEN c_get;
  FETCH c_get INTO O_default_value;
  CLOSE c_get;
  RETURN O_default_value;
END get_tmpl_default;
--------------------------------------------------------------------------------
END coresvc_cflex;
/