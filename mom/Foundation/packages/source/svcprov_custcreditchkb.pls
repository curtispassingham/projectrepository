CREATE OR REPLACE PACKAGE BODY SVCPROV_CUSTCREDITCHK IS
--------------------------------------------------------------------------------
--FUNCTION POP_TABLES : This function populates the data from the input object.
--                      No data validations are done here. Validations are done
--                      in the core service package. 
--------------------------------------------------------------------------------
FUNCTION POP_TABLES (O_error_message      IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                     O_process_id         IN OUT      SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                     O_chunk_id           IN OUT      SVC_CUSTCREDITCHK.CHUNK_ID%TYPE,
                     I_businessObject     IN          "RIB_CustCreditChkCol_REC")
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--FUNCTION CLEANUP_TABLES : This function deletes the data from the staging table
--                          after a message is successfully consumed.
--------------------------------------------------------------------------------
FUNCTION CLEANUP_TABLES(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id            IN     SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                        I_chunk_id              IN     SVC_CUSTCREDITCHK.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--FUNCTION PARSE_ERROR_MSG : This function is used to parse the error messages
--                           in case of validation failures or other unhandled
--                           exceptions.
--------------------------------------------------------------------------------
FUNCTION PARSE_ERROR_MSG (O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_serviceOperationStatus    IN OUT   "RIB_ServiceOpStatus_REC",
                          I_process_id                IN       SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                          I_chunk_id                  IN       SVC_CUSTCREDITCHK.CHUNK_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE CREATE_CREDIT_IND (O_serviceOperationStatus    IN OUT "RIB_ServiceOpStatus_REC",
                            I_businessObject             IN     "RIB_CustCreditChkCol_REC")
IS

   L_chunk_id                     SVC_CUSTCREDITCHK.CHUNK_ID%TYPE        := 1;
   L_program                      VARCHAR2(64)                           := 'SVCPROV_CUSTCREDITCHK.CREATE_CREDIT_IND';
   L_process_id                   SVC_CUSTCREDITCHK.PROCESS_ID%TYPE;
   L_error_message                RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   --check input object contains data
   if I_businessObject is NULL or
      I_businessObject.CustCreditChkDesc_TBL is NULL or 
      I_businessObject.CustCreditChkDesc_TBL.count <= 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   if I_businessObject.CustCreditChkDesc_TBL.COUNT <> I_businessObject.collection_size then
      L_error_message := SQL_LIB.GET_MESSAGE_TEXT('INV_CUSTCREDIT_COL_COUNT', 
                                                  NULL,
                                                  NULL,
                                                  NULL);
      raise PROGRAM_ERROR;
   end if;

   if SVCPROV_CUSTCREDITCHK.POP_TABLES(L_error_message,
                                       L_process_id,
                                       L_chunk_id,
                                       I_businessObject) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if CORESVC_CUSTCREDITCHK.CREATE_CREDIT_IND(L_error_message,
                                              L_process_id,
                                              L_chunk_id) = FALSE then
      if L_error_message is null then
         if SVCPROV_CUSTCREDITCHK.PARSE_ERROR_MSG(L_error_message,
                                                  O_serviceOperationStatus,
                                                  L_process_id,
                                                  L_chunk_id) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         return;
      end if;
      raise PROGRAM_ERROR;
   end if;

   if SVCPROV_CUSTCREDITCHK.CLEANUP_TABLES(L_error_message,
                                           L_process_id,
                                           L_chunk_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   return;
EXCEPTION

   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
      return;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
      return;
END CREATE_CREDIT_IND ;
--------------------------------------------------------------------------------
FUNCTION POP_TABLES (O_error_message      IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                     O_process_id         IN OUT      SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                     O_chunk_id           IN OUT      SVC_CUSTCREDITCHK.CHUNK_ID%TYPE,
                     I_businessObject     IN          "RIB_CustCreditChkCol_REC")
RETURN BOOLEAN IS

   L_program                      VARCHAR2(64)                           := 'SVCPROV_CUSTCREDITCHK.POP_TABLES';
   L_user                         SVCPROV_CONTEXT.USER_NAME%TYPE         := get_user;
   L_CustCreditChkDesc_TBL        "RIB_CustCreditChkDesc_TBL";

   TYPE wf_customer_id_TBL        is table of wf_customer.wf_customer_id%TYPE;
   TYPE wf_customer_group_id_TBL  is table of wf_customer.wf_customer_group_id%TYPE;
   TYPE credit_ind_TBL            is table of wf_customer.credit_ind%TYPE;
   
   L_wf_customer_id_TBL           wf_customer_id_TBL;
   L_wf_customer_group_id_TBL     wf_customer_group_id_TBL;
   L_credit_ind_TBL               credit_ind_TBL;
             
   cursor C_GET_DISTINCT_DATA is
      select distinct wf_customer_id,
                      wf_customer_group_id,
                      credit_ind
        from TABLE(CAST(I_businessObject.CustCreditChkDesc_TBL as "RIB_CustCreditChkDesc_TBL"));

BEGIN

   O_process_id := SVC_CUSTCREDITCHK_PROC_SEQ.NEXTVAL;
   O_chunk_id   := 1;


   --Removing duplicates, if any.
   open C_GET_DISTINCT_DATA;
   fetch C_GET_DISTINCT_DATA BULK COLLECT into L_wf_customer_id_TBL,
                                               L_wf_customer_group_id_TBL,
                                               L_credit_ind_TBL;
   close C_GET_DISTINCT_DATA;
   
   FORALL i in L_wf_customer_id_TBL.FIRST .. L_wf_customer_id_TBL.LAST
      
      insert into svc_custcreditchk(process_id,
                                    chunk_id,
                                    process_status,
                                    custcreditchk_id,
                                    action_type,
                                    wf_customer_id,
                                    wf_customer_group_id,
                                    credit_ind,
                                    create_datetime,
                                    last_update_datetime,
                                    create_id,
                                    last_update_id)
                             values (O_process_id,
                                     O_chunk_id,
                                     CORESVC_CUSTCREDITCHK.PROCESS_STATUS_NEW,
                                     SVC_CUSTCREDITCHK_ID_SEQ.NEXTVAL,
                                     CORESVC_CUSTCREDITCHK.ACTION_TYPE_UPDATE,
                                     L_wf_customer_id_TBL(i),
                                     L_wf_customer_group_id_TBL(i),
                                     NVL(UPPER(L_credit_ind_TBL(i)),'X'),
                                     SYSDATE,
                                     SYSDATE,
                                     L_user,
                                     L_user);   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POP_TABLES;
--------------------------------------------------------------------------------
FUNCTION CLEANUP_TABLES(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id            IN     SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                        I_chunk_id              IN     SVC_CUSTCREDITCHK.PROCESS_ID%TYPE)
RETURN BOOLEAN IS
   
   L_program                                VARCHAR2(64)                   := 'SVCPROV_CUSTCREDITCHK.CLEANUP_TABLES';
   L_table                                  VARCHAR2(30);

   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_CUSTCREDITCHK is
      select 'x'
        from svc_custcreditchk
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_COMPLETED;
BEGIN

   L_table := 'SVC_CUSTCREDITCHK';
   open C_LOCK_SVC_CUSTCREDITCHK;
   close C_LOCK_SVC_CUSTCREDITCHK;

   delete 
     from svc_custcreditchk
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status = CORESVC_CUSTCREDITCHK.PROCESS_STATUS_COMPLETED;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
         return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CLEANUP_TABLES;

FUNCTION PARSE_ERROR_MSG (O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_serviceOperationStatus    IN OUT   "RIB_ServiceOpStatus_REC",
                          I_process_id                IN       SVC_CUSTCREDITCHK.PROCESS_ID%TYPE,
                          I_chunk_id                  IN       SVC_CUSTCREDITCHK.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                           VARCHAR2(64)            := 'SVCPROV_CUSTCREDITCHK.PARSE_ERROR_MSG';

BEGIN

   if SVCPROV_UTILITY.PARSE_STAGED_ERR_MSG (O_error_message,
                                            O_serviceOperationStatus.FailStatus_TBL,
                                            'SVC_CUSTCREDITCHK',
                                            I_process_id)  = FALSE then
      return FALSE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PARSE_ERROR_MSG;
END SVCPROV_CUSTCREDITCHK;
/