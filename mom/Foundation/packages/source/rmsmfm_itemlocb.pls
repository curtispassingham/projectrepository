CREATE OR REPLACE PACKAGE BODY RMSMFM_ITEMLOC AS

TYPE rowid_TBL      is table of ROWID INDEX BY BINARY_INTEGER;

LP_max_details_to_publish     NUMBER := NULL;
LP_num_threads                NUMBER := NULL;
LP_minutes_time_lag           NUMBER := NULL;

-- depending upon the message type, there will come a point in the publication process
-- when error retry will no longer be possible.  this variable will track whether or
-- not that point has been reached.
LP_error_status varchar2(1):=NULL;

PROGRAM_ERROR EXCEPTION;

            /*** Private Functions/Procedures ***/

--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_message         OUT        VARCHAR2,
                              O_message           IN  OUT nocopy RIB_OBJECT,
                              O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id        IN  OUT nocopy RIB_BUSOBJID_TBL,
                              O_message_type      IN  OUT        VARCHAR2,
                              I_item              IN             ITEMLOC_MFQUEUE.ITEM%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                               O_ilphys_tbl     IN OUT nocopy "RIB_ItemLocPhys_TBL",
                               O_routing_info   IN OUT nocopy RIB_ROUTINGINFO_TBL,
                               I_message_type   IN     ITEMLOC_MFQUEUE.MESSAGE_TYPE%TYPE,
                               I_item           IN     ITEMLOC_MFQUEUE.ITEM%TYPE,
                               I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_DELETE_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                                      O_ilphys_tbl     IN OUT nocopy "RIB_ItemLocPhysRef_TBL",
                                      O_routing_info   IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                      I_message_type   IN     ITEMLOC_MFQUEUE.MESSAGE_TYPE%TYPE,
                                      I_item           IN     ITEMLOC_MFQUEUE.ITEM%TYPE,
                                      I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION ROUTING_INFO_ADD(O_error_message         OUT VARCHAR2,
                          O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                          I_physical_loc      IN      ITEMLOC_MFQUEUE.PHYSICAL_LOC%TYPE,
                          I_loc_type          IN      ITEMLOC_MFQUEUE.LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code       IN OUT         VARCHAR2,
                        O_error_message     IN OUT         VARCHAR2,
                        O_message           IN OUT  nocopy RIB_OBJECT,
                        O_bus_obj_id        IN OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info      IN OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_item              IN             ITEMLOC_MFQUEUE.ITEM%TYPE,
                        I_physical_loc      IN             ITEMLOC_MFQUEUE.PHYSICAL_LOC%TYPE,
                        I_loc               IN             ITEMLOC_MFQUEUE.LOC%TYPE,
                        I_seq_no            IN             ITEMLOC_MFQUEUE.SEQ_NO%TYPE);
--------------------------------------------------------------------------------

           /*** Public Program Bodies***/

FUNCTION ADDTOQ(O_error_message          OUT  VARCHAR2,
                I_message_type           IN   ITEMLOC_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_itemloc_record         IN   ITEM_LOC%ROWTYPE,
                I_returnable_ind         IN   ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE,
                I_prim_repl_supplier     IN   REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                I_repl_method            IN   REPL_ITEM_LOC.REPL_METHOD%TYPE,
                I_reject_store_ord_ind   IN   REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE,
                I_next_delivery_date     IN   REPL_ITEM_LOC.NEXT_DELIVERY_DATE%TYPE,
                I_mult_runs_per_day_ind  IN   REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE)
RETURN BOOLEAN IS

   L_module             VARCHAR2(64) := 'RMSMFM_ITEMLOC.ADDTOQ';
   L_status_code        VARCHAR2(1)  := NULL;
   L_item_hash          NUMBER := NULL;

   L_physical_loc       ITEMLOC_MFQUEUE.LOC%TYPE := NULL;
   L_returnable_ind     ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE;
   L_traits_exists      VARCHAR2(1)              := NULL;
   
   L_dummy              varchar2(1):= NULL;
   L_dummy_mfq          varchar2(1):= NULL;
   
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);
   cursor C_GET_RET_IND is
      select returnable_ind 
        from item_loc_traits
       where item = I_itemloc_record.item
         and loc  = I_itemloc_record.loc;
         
   cursor C_ITEM_PUBLISHED is
      select published 
        from item_pub_info
       where item = I_itemloc_record.item;
       
   cursor C_ITEMLOC_MFQUEUE is
      select 'x'
        from itemloc_mfqueue mf
       where mf.item = I_itemloc_record.item 
         and mf.loc = I_itemloc_record.loc
         and mf.loc_type = I_itemloc_record.loc_type;

   cursor C_LOCK_ITEMLOC_MFQUEUE is
      select 'x' 
        from itemloc_mfqueue mf
       where mf.item = I_itemloc_record.item
         and mf.loc = I_itemloc_record.loc
         and mf.loc_type = I_itemloc_record.loc_type
		 
         for update nowait;      
BEGIN
   
   if I_message_type = ITEMLOC_DEL then
      -- If deleting an item that has never been published, do NOT add the message to the queue.
      -- Check for ITEMLOC_MFQUEUE records if not deleted in DELETE_ITEM_RECORDS_SQL.DEL_ITEM.
      --
      -- If deleting an item that has already been published, add the message to the queue.
      -- ITEM_PUB_INFO would still exist, but all other ITEMLOC_MFQUEUE 
      -- records would have been deleted in DELETE_ITEM_RECORDS_SQL.DEL_ITEM.
      --
      open C_ITEM_PUBLISHED;
      fetch C_ITEM_PUBLISHED into L_dummy;
      close C_ITEM_PUBLISHED;
      if L_dummy = 'N' or L_dummy is NULL then
         open C_ITEMLOC_MFQUEUE;
         fetch C_ITEMLOC_MFQUEUE into L_dummy_mfq;
         close C_ITEMLOC_MFQUEUE;
         if L_dummy_mfq is NOT NULL then
            open C_LOCK_ITEMLOC_MFQUEUE;
            close C_LOCK_ITEMLOC_MFQUEUE;
            delete from itemloc_mfqueue
             where item = I_itemloc_record.item
               and loc = I_itemloc_record.loc
               and loc_type = I_itemloc_record.loc_type;
         end if;
          return TRUE;
       end if;
   end if;
   
   if LP_num_threads is NULL then
      API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                   O_error_message,
                                   LP_max_details_to_publish,
                                   LP_num_threads,
                                   LP_minutes_time_lag,
                                   FAMILY);
      if L_status_code = API_CODES.UNHANDLED_ERROR then
         return FALSE;
      end if;
   end if;

   ---
   if HASH_ITEM(O_error_message,
                L_item_hash,
                I_itemloc_record.item) = false then
      return FALSE;
   end if;
   ---
   if I_itemloc_record.loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_physical_loc,
                                       I_itemloc_record.loc) = FALSE then
         return FALSE;
      end if;
   else
      L_physical_loc := I_itemloc_record.loc;
   end if;
   ---
   L_returnable_ind := I_returnable_ind;
   
   if L_returnable_ind is NULL and I_message_type = RMSMFM_ITEMLOC.ITEMLOC_UPD then
      
      if I_itemloc_record.loc_type = 'W' then
         if ITEM_LOC_TRAITS_SQL.DERIVE_RETURNABLE_IND(O_error_message,
				                                              L_returnable_ind,
				                                              I_itemloc_record.item,
				                                              I_itemloc_record.loc) = FALSE then
				    return FALSE;
				 end if;
      else
			   SQL_LIB.SET_MARK('OPEN', 'C_GET_RET_IND', 'ITEM_LOC_TRAITS', NULL);
         open C_GET_RET_IND;
       
         SQL_LIB.SET_MARK('FETCH', 'C_GET_RET_IND', 'ITEM_LOC_TRAITS', NULL);
         fetch C_GET_RET_IND into L_returnable_ind;
       
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_RET_IND', 'ITEM_LOC_TRAITS', NULL);
         close C_GET_RET_IND;
	 		end if;			       
   end if;
   ---
   insert into itemloc_mfqueue(seq_no,
                               pub_status,
                               message_type,
                               item,
                               physical_loc,
                               loc,
                               loc_type,
                               local_item_desc,
                               local_short_desc,
                               status,
                               primary_supp,
                               primary_cntry,
                               receive_as_type,
                               taxable_ind,
                               source_method,
                               source_wh,
                               primary_repl_supplier,
                               repl_method,
                               reject_store_order_ind,
                               next_delivery_date,
                               thread_no,
                               family,
                               custom_message_type,
                               transaction_time_stamp,
                               store_price_ind,
                               unit_retail,
                               selling_unit_retail,
                               selling_uom,
                               mult_runs_per_day_ind,
                               uin_type,
                               uin_label,
                               capture_time,
                               ext_uin_ind,
                               ranged_ind,
                               returnable_ind)
                        values(itemloc_mfsequence.NEXTVAL,
                               'U',
                               I_message_type,
                               I_itemloc_record.item,
                               L_physical_loc,
                               I_itemloc_record.loc,
                               I_itemloc_record.loc_type,
                               I_itemloc_record.local_item_desc,
                               I_itemloc_record.local_short_desc,
                               I_itemloc_record.status,
                               I_itemloc_record.primary_supp,
                               I_itemloc_record.primary_cntry,
                               I_itemloc_record.receive_as_type,
                               I_itemloc_record.taxable_ind,
                               I_itemloc_record.source_method,
                               I_itemloc_record.source_wh,
                               I_prim_repl_supplier,
                               I_repl_method,
                               I_reject_store_ord_ind,
                               I_next_delivery_date,
                               mod(L_item_hash,LP_num_threads) + 1,
                               FAMILY,
                               'N',
                               sysdate,
                               I_itemloc_record.store_price_ind,
                               I_itemloc_record.unit_retail,
                               I_itemloc_record.selling_unit_retail,
                               I_itemloc_record.selling_uom,
                               I_mult_runs_per_day_ind,
                               I_itemloc_record.uin_type,
                               I_itemloc_record.uin_label,
                               I_itemloc_record.capture_time,
                               nvl(I_itemloc_record.ext_uin_ind,'N'),
                               I_itemloc_record.ranged_ind,
                               L_returnable_ind);
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_ITEMLOC_MFQUEUE%ISOPEN then
         close C_LOCK_ITEMLOC_MFQUEUE;
      end if;      
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                             'ITEMLOC_MFQUEUE',
                                                              I_itemloc_record.item,
                                                              I_itemloc_record.loc);
      
   when OTHERS then
      if C_LOCK_ITEMLOC_MFQUEUE%ISOPEN then
         close C_LOCK_ITEMLOC_MFQUEUE;
      end if;
      
      if C_ITEM_PUBLISHED%ISOPEN then
         close C_ITEM_PUBLISHED;
      end if;
      
      if C_ITEMLOC_MFQUEUE%ISOPEN then
         close C_ITEMLOC_MFQUEUE;
      end if;
   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT     VARCHAR2,
                 O_error_msg        OUT     VARCHAR2,
                 O_message_type     OUT     VARCHAR2,
                 O_message          OUT     RIB_OBJECT,
                 O_bus_obj_id       OUT     RIB_BUSOBJID_TBL,
                 O_routing_info     OUT     RIB_ROUTINGINFO_TBL,
                 I_num_threads      IN      NUMBER DEFAULT 1,
                 I_thread_val       IN      NUMBER DEFAULT 1)
IS

   L_module           VARCHAR2(64) := 'RMSMFM_ITEMLOC.GETNXT';
   L_item             ITEMLOC_MFQUEUE.ITEM%TYPE := NULL;
   L_physical_loc     ITEMLOC_MFQUEUE.PHYSICAL_LOC%TYPE := NULL;
   L_loc              ITEMLOC_MFQUEUE.LOC%TYPE := NULL;
   L_seq_no           ITEMLOC_MFQUEUE.SEQ_NO%TYPE:=NULL;
   L_hospital_found   VARCHAR2(1) := 'N';

   ---
   -- DRIVING CURSOR - selects ONE message from the queue
   ---
   cursor C_QUEUE is
      select /*+ FIRST_ROWS */
             item,
             physical_loc,
             loc,
             message_type,
             seq_no
        from (select q.item,
                     q.physical_loc,
                     q.loc,
                     q.message_type,
                     q.seq_no
                from itemloc_mfqueue q
               where q.pub_status = 'U'
                 and q.thread_no = I_thread_val
                 and not exists (select 'x'
                                   from item_pub_info ipi
                                  where ipi.item = q.item
                                    and ipi.published = 'N'
                                    and rownum = 1)
               order by q.seq_no)
       where rownum = 1;

   cursor C_CHECK_FOR_HOSPITAL_MSGS is
      select 'Y'
        from itemloc_mfqueue
       where item = L_item
         and loc = L_loc
         and pub_status = 'H';

BEGIN

   -- status of 'H'ospital
   LP_error_status := API_CODES.HOSPITAL;

   L_item   := NULL;
   L_seq_no := NULL;

   O_message       := NULL;
   ---
   open C_QUEUE;
   fetch C_QUEUE into L_item,
                      L_physical_loc,
                      L_loc,
                      O_message_type,
                      L_seq_no;
   close C_QUEUE;

   if L_item is NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   open  C_CHECK_FOR_HOSPITAL_MSGS;
   fetch C_CHECK_FOR_HOSPITAL_MSGS into L_hospital_found;
   close C_CHECK_FOR_HOSPITAL_MSGS;

   if L_hospital_found = 'Y' then
      O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                        NULL,
                                        NULL,
                                        NULL);
      raise PROGRAM_ERROR;
   end if;

   if PROCESS_QUEUE_RECORD(O_error_msg,
                           O_message,
                           O_routing_info,
                           O_bus_obj_id,
                           O_message_type,
                           L_item) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_item);
   end if;

EXCEPTION
   when OTHERS then
      if O_error_msg is NULL then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      end if;

      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_item,
                    L_physical_loc,
                    L_loc,
                    L_seq_no);
END GETNXT;
--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code         OUT      VARCHAR2,
                    O_error_msg           OUT      VARCHAR2,
                    O_message_type    IN  OUT      VARCHAR2,
                    O_message             OUT      RIB_OBJECT,
                    O_bus_obj_id      IN  OUT      RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT      RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN           RIB_OBJECT)
IS

   L_module          VARCHAR2(64) := 'RMSMFM_ITEMLOC.PUB_RETRY';

   L_item         ITEMLOC_MFQUEUE.ITEM%TYPE := NULL;
   L_physical_loc ITEMLOC_MFQUEUE.PHYSICAL_LOC%TYPE := NULL;
   L_loc          ITEMLOC_MFQUEUE.LOC%TYPE := NULL;
   L_seq_no       ITEMLOC_MFQUEUE.SEQ_NO%TYPE:=NULL;
   L_rowid        ROWID:=NULL;

   cursor C_RETRY_QUEUE is
      select q.item,
             q.physical_loc,
             q.loc,
             q.message_type,
             q.rowid
        from itemloc_mfqueue q
       where q.seq_no = L_seq_no;

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;


   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;

   O_message := null;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_item,
                            L_physical_loc,
                            L_loc,
                            O_message_type,
                            L_rowid;
   close C_RETRY_QUEUE;

   if L_item IS NULL then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;

   if PROCESS_QUEUE_RECORD(O_error_msg,
                           O_message,
                           O_routing_info,
                           O_bus_obj_id,
                           O_message_type,
                           L_item) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if O_message IS NULL then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_item);
   end if;

EXCEPTION
   when OTHERS then
      if O_error_msg is NULL then
         O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      end if;

      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_item,
                    L_physical_loc,
                    L_loc,
                    L_seq_no);
END PUB_RETRY;
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_RECORD(O_error_message         OUT        VARCHAR2,
                              O_message           IN  OUT nocopy RIB_OBJECT,
                              O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id        IN  OUT nocopy RIB_BUSOBJID_TBL,
                              O_message_type      IN  OUT        VARCHAR2,
                              I_item              IN             ITEMLOC_MFQUEUE.ITEM%TYPE)
RETURN BOOLEAN IS

   L_module        VARCHAR2(64) := 'RMSMFM_ITEMLOC.PROCESS_QUEUE_RECORD';
   L_status_code   VARCHAR2(1) := NULL;

   L_itemlocdesc_rec "RIB_ItemLocDesc_REC" := NULL;
   L_itemlocref_rec  "RIB_ItemLocRef_REC" := NULL;

   L_max_details        rib_settings.max_details_to_publish%TYPE:=NULL;
   L_num_threads        rib_settings.num_threads%TYPE:=NULL;
   L_minutes_time_lag   rib_settings.minutes_time_lag%TYPE:=NULL;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_message,
                                L_max_details,
                                L_num_threads,
                                L_minutes_time_lag,
                                FAMILY);   --- I_family
   if L_status_code = API_CODES.UNHANDLED_ERROR then
      return FALSE;
   end if;

   if O_message_type in (ITEMLOC_ADD, ITEMLOC_UPD, REPL_UPD) then
      L_itemlocdesc_rec := "RIB_ItemLocDesc_REC"(0,
                                               I_item,
                                               NULL);
      if BUILD_DETAIL_OBJECTS(O_error_message,
                              L_itemlocdesc_rec.itemlocphys_tbl,
                              O_routing_info,
                              O_message_type,
                              I_item,
                              L_max_details) = FALSE then
         return FALSE;
      end if;

      O_message := L_itemlocdesc_rec;

   else
      L_itemlocref_rec := "RIB_ItemLocRef_REC"(0,
                                             I_item,
                                             NULL);

      if BUILD_DETAIL_DELETE_OBJECTS(O_error_message,
                                     L_itemlocref_rec.itemlocphysref_tbl,
                                     O_routing_info,
                                     O_message_type,
                                     I_item,
                                     L_max_details) = FALSE then
         return FALSE;
      end if;

      O_message := L_itemlocref_rec;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_RECORD;
------------------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                               O_ilphys_tbl     IN OUT nocopy "RIB_ItemLocPhys_TBL",
                               O_routing_info   IN OUT nocopy RIB_ROUTINGINFO_TBL,
                               I_message_type   IN     ITEMLOC_MFQUEUE.MESSAGE_TYPE%TYPE,
                               I_item           IN     ITEMLOC_MFQUEUE.ITEM%TYPE,
                               I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_module         VARCHAR2(64) := 'RMSMFM_ITEMLOC.BUILD_DETAIL_OBJECTS';

   L_itemlocphys_rec   "RIB_ItemLocPhys_REC" := NULL;
   L_itemlocvirt_rec   "RIB_ItemLocVirt_REC" := NULL;
   L_itemlocvirtrepl_rec "RIB_ItemLocVirtRepl_REC" := NULL;

   L_itemlocphys_index BINARY_INTEGER := NULL;
   L_itemlocvirt_index BINARY_INTEGER := NULL;

   L_mfqueue_rowid     rowid_TBL;
   L_mfqueue_size      BINARY_INTEGER := 0;

   L_purchase_type     VARCHAR2(1);

   L_details_processed NUMBER := 0;

   cursor C_ITEMLOC_DETAILS is
      select q.message_type,
             q.physical_loc,
             q.loc,
             q.loc_type,
             q.local_item_desc,
             q.local_short_desc,
             q.status,
             q.primary_supp,
             q.primary_cntry,
             q.receive_as_type,
             q.taxable_ind,
             q.source_method,
             q.source_wh,
             q.primary_repl_supplier,
             q.repl_method,
             q.reject_store_order_ind,
             q.next_delivery_date,
             q.seq_no,
             q.rowid,
             q.store_price_ind,
             q.unit_retail,
             q.selling_unit_retail,
             q.selling_uom,
             q.mult_runs_per_day_ind,
             q.uin_type,
             q.uin_label,
             q.capture_time,
             q.ext_uin_ind,
             q.ranged_ind,
             q.returnable_ind,
             s.stockholding_ind stockholding_ind,
             s.store_type store_type
        from itemloc_mfqueue q,
             store s
       where q.item = I_item
         and q.loc  = s.store(+)
       order by q.seq_no;

   cursor C_ITEMLOC_DETAILS_AU is
      select q.message_type,
             q.physical_loc,
             q.loc,
             q.loc_type,
             il.local_item_desc,
             il.local_short_desc,
             il.status,
             il.primary_supp,
             il.primary_cntry,
             il.receive_as_type,
             il.taxable_ind,
             il.source_method,
             il.source_wh,
             q.primary_repl_supplier,
             q.repl_method,
             q.reject_store_order_ind,
             q.next_delivery_date,
             q.seq_no,
             q.rowid,
             il.store_price_ind,
             il.unit_retail,
             il.selling_unit_retail,
             il.selling_uom,
             q.mult_runs_per_day_ind,
             q.uin_type,
             q.uin_label,
             q.capture_time,
             q.ext_uin_ind,
             q.ranged_ind,
             q.returnable_ind,
             s.stockholding_ind stockholding_ind,
             s.store_type store_type
        from item_loc il,
             itemloc_mfqueue q,
             store s
       where il.item = q.item
         and il.loc  = q.loc
         and q.item  = I_item
         and q.loc   = s.store(+)
       order by q.seq_no;


   TYPE ITEMLOC_CURSOR_TBL is TABLE OF C_ITEMLOC_DETAILS%ROWTYPE
      INDEX BY BINARY_INTEGER;

   L_itemloc_cursor_tbl  ITEMLOC_CURSOR_TBL;


   cursor C_GET_PURCHASE_TYPE is
      select DECODE(d.purchase_type,0,'N',1,'S',2,'C',NULL)
        from item_master im,
             deps d
       where im.item = I_item
         and d.dept = im.dept;

BEGIN

   L_itemlocphys_rec := "RIB_ItemLocPhys_REC"(0, NULL, NULL, NULL);
   O_ilphys_tbl := "RIB_ItemLocPhys_TBL"();
   L_itemlocvirt_rec := "RIB_ItemLocVirt_REC"(0,
                                            NULL, NULL, NULL, NULL, NULL,
                                            NULL, NULL, NULL, NULL, NULL,
                                            NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL);

   L_itemlocvirtrepl_rec := "RIB_ItemLocVirtRepl_REC"(0,
                                                    NULL, NULL, NULL, NULL, NULL, NULL);

   if I_message_type in (ITEMLOC_ADD, ITEMLOC_UPD) then
      open C_ITEMLOC_DETAILS_AU;
      fetch C_ITEMLOC_DETAILS_AU BULK COLLECT INTO L_itemloc_cursor_tbl
         LIMIT I_max_details;
      close C_ITEMLOC_DETAILS_AU;
   else
      open C_ITEMLOC_DETAILS;
      fetch C_ITEMLOC_DETAILS BULK COLLECT INTO L_itemloc_cursor_tbl
         LIMIT I_max_details;
      close C_ITEMLOC_DETAILS;
   end if;

   FOR i IN 1..L_itemloc_cursor_tbl.COUNT LOOP  --- outer loop

      if I_message_type != L_itemloc_cursor_tbl(i).message_type then
         EXIT;
      end if;

      L_itemlocphys_index := NULL;
      L_itemlocvirt_index := NULL;

      ----
      --- if the location for the current queue record is a warehouse,
      ---
      ----
      if L_itemloc_cursor_tbl(i).loc_type = 'W' then
         FOR j in 1..O_ilphys_tbl.COUNT LOOP
            if O_ilphys_tbl(j).physical_loc = L_itemloc_cursor_tbl(i).physical_loc then
               L_itemlocphys_index := j;
               EXIT;
            end if;
         end LOOP;
      end if;

      if L_itemlocphys_index is null then
         O_ilphys_tbl.EXTEND;
         L_itemlocphys_index := O_ilphys_tbl.COUNT;

         O_ilphys_tbl(L_itemlocphys_index) := L_itemlocphys_rec;
         O_ilphys_tbl(L_itemlocphys_index).physical_loc     := L_itemloc_cursor_tbl(i).physical_loc;
         O_ilphys_tbl(L_itemlocphys_index).returnable_ind   := L_itemloc_cursor_tbl(i).returnable_ind;

         O_ilphys_tbl(L_itemlocphys_index).loc_type         := L_itemloc_cursor_tbl(i).loc_type;
         O_ilphys_tbl(L_itemlocphys_index).stockholding_ind := L_itemloc_cursor_tbl(i).stockholding_ind;
         O_ilphys_tbl(L_itemlocphys_index).store_type       := L_itemloc_cursor_tbl(i).store_type;

         if I_message_type = REPL_UPD then
            O_ilphys_tbl(L_itemlocphys_index).itemlocvirtrepl_tbl := "RIB_ItemLocVirtRepl_TBL"();
         else
            O_ilphys_tbl(L_itemlocphys_index).itemlocvirt_tbl := "RIB_ItemLocVirt_TBL"();
         end if;

         ------ add location to routing info
         if ROUTING_INFO_ADD(O_error_msg,
                             O_routing_info,
                             L_itemloc_cursor_tbl(i).physical_loc,
                             L_itemloc_cursor_tbl(i).loc_type) = FALSE then
            return FALSE;
         end if;
      end if;

      if I_message_type = REPL_UPD then
         L_itemlocvirtrepl_rec.loc := L_itemloc_cursor_tbl(i).loc;

         L_itemlocvirtrepl_rec.loc_type := L_itemloc_cursor_tbl(i).loc_type;
         L_itemlocvirtrepl_rec.primary_repl_supplier := L_itemloc_cursor_tbl(i).primary_repl_supplier;
         L_itemlocvirtrepl_rec.repl_method := L_itemloc_cursor_tbl(i).repl_method;
         L_itemlocvirtrepl_rec.reject_store_order_ind := L_itemloc_cursor_tbl(i).reject_store_order_ind;
         L_itemlocvirtrepl_rec.next_delivery_date := L_itemloc_cursor_tbl(i).next_delivery_date;

         O_ilphys_tbl(L_itemlocphys_index).itemlocvirtrepl_tbl.EXTEND;
         L_itemlocvirt_index := O_ilphys_tbl(L_itemlocphys_index).itemlocvirtrepl_tbl.COUNT;
         L_itemlocvirtrepl_rec.multiple_runs_per_day_ind := L_itemloc_cursor_tbl(i).mult_runs_per_day_ind;


         O_ilphys_tbl(L_itemlocphys_index).itemlocvirtrepl_tbl(L_itemlocvirt_index) := L_itemlocvirtrepl_rec;

      else

         L_itemlocvirt_rec.loc := L_itemloc_cursor_tbl(i).loc;

         L_itemlocvirt_rec.loc_type := L_itemloc_cursor_tbl(i).loc_type;
         L_itemlocvirt_rec.local_item_desc := L_itemloc_cursor_tbl(i).local_item_desc;
         L_itemlocvirt_rec.local_short_desc := L_itemloc_cursor_tbl(i).local_short_desc;
         L_itemlocvirt_rec.status := L_itemloc_cursor_tbl(i).status;
         L_itemlocvirt_rec.primary_supp := L_itemloc_cursor_tbl(i).primary_supp;
         L_itemlocvirt_rec.primary_cntry := L_itemloc_cursor_tbl(i).primary_cntry;
         L_itemlocvirt_rec.receive_as_type := L_itemloc_cursor_tbl(i).receive_as_type;
         L_itemlocvirt_rec.taxable_ind := L_itemloc_cursor_tbl(i).taxable_ind;
         L_itemlocvirt_rec.source_method := L_itemloc_cursor_tbl(i).source_method;
         L_itemlocvirt_rec.source_wh := L_itemloc_cursor_tbl(i).source_wh;
         L_itemlocvirt_rec.store_price_ind := L_itemloc_cursor_tbl(i).store_price_ind;
         L_itemlocvirt_rec.unit_retail := L_itemloc_cursor_tbl(i).unit_retail;
         L_itemlocvirt_rec.selling_unit_retail := L_itemloc_cursor_tbl(i).selling_unit_retail;
         L_itemlocvirt_rec.selling_uom := L_itemloc_cursor_tbl(i).selling_uom;
         L_itemlocvirt_rec.uin_type := L_itemloc_cursor_tbl(i).uin_type;
         L_itemlocvirt_rec.uin_label := L_itemloc_cursor_tbl(i).uin_label;
         L_itemlocvirt_rec.capture_time := L_itemloc_cursor_tbl(i).capture_time;
         L_itemlocvirt_rec.ext_uin_ind := L_itemloc_cursor_tbl(i).ext_uin_ind;
         L_itemlocvirt_rec.ranged_ind  := L_itemloc_cursor_tbl(i).ranged_ind;

         ---
         open C_GET_PURCHASE_TYPE;
         fetch C_GET_PURCHASE_TYPE into L_purchase_type;
         close C_GET_PURCHASE_TYPE;
         --
         L_itemlocvirt_rec.purchase_type := L_purchase_type;
         ---
         O_ilphys_tbl(L_itemlocphys_index).itemlocvirt_tbl.EXTEND;
         L_itemlocvirt_index := O_ilphys_tbl(L_itemlocphys_index).itemlocvirt_tbl.COUNT;

         O_ilphys_tbl(L_itemlocphys_index).itemlocvirt_tbl(L_itemlocvirt_index) := L_itemlocvirt_rec;

      end if;

      L_mfqueue_size := L_mfqueue_size + 1;
      L_mfqueue_rowid(L_mfqueue_size) := L_itemloc_cursor_tbl(i).rowid;

      L_details_processed := L_details_processed + 1;

   END LOOP;

   -- if no data found in cursor, raise error
   if L_details_processed = 0 then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_IL_DETAIL_PUB',
                                        I_item, NULL, NULL);
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_mfqueue_size > 0 then
      FORALL i IN 1..L_mfqueue_size
         delete from itemloc_mfqueue where rowid = L_mfqueue_rowid(i);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ITEMLOC_DETAILS%ISOPEN then
         close C_ITEMLOC_DETAILS;
      end if;
      if C_ITEMLOC_DETAILS_AU%ISOPEN then
         close C_ITEMLOC_DETAILS_AU;
      end if;
      if C_GET_PURCHASE_TYPE%ISOPEN then
         close C_GET_PURCHASE_TYPE;
      end if;

      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_OBJECTS;
------------------------------------------------------------------------------------------
FUNCTION BUILD_DETAIL_DELETE_OBJECTS( O_error_msg      IN OUT VARCHAR2,
                                      O_ilphys_tbl     IN OUT nocopy "RIB_ItemLocPhysRef_TBL",
                                      O_routing_info   IN OUT nocopy RIB_ROUTINGINFO_TBL,
                                      I_message_type   IN     ITEMLOC_MFQUEUE.MESSAGE_TYPE%TYPE,
                                      I_item           IN     ITEMLOC_MFQUEUE.ITEM%TYPE,
                                      I_max_details    IN     rib_settings.max_details_to_publish%TYPE)
RETURN BOOLEAN IS

   L_module         VARCHAR2(64) := 'RMSMFM_ITEMLOC.BUILD_DETAIL_DELETE_OBJECTS';

   L_store_type        STORE.STORE_TYPE%TYPE;
   L_stockholding_ind  VARCHAR2(1) := NULL;

   L_itemlocphys_rec   "RIB_ItemLocPhysRef_REC" := NULL;
   L_itemlocvirt_rec   "RIB_ItemLocVirtRef_REC" := NULL;
   L_itemlocvirt_tbl   "RIB_ItemLocVirtRef_TBL" := NULL;

   L_itemlocphys_index BINARY_INTEGER := NULL;
   L_itemlocvirt_index BINARY_INTEGER := NULL;

   L_mfqueue_rowid     rowid_TBL;
   L_mfqueue_size      BINARY_INTEGER := 0;

   L_details_processed NUMBER := 0;

   cursor C_ITEMLOC_DETAILS is
      select q.message_type,
             q.physical_loc,
             q.loc,
             q.loc_type,
             q.local_item_desc,
             q.local_short_desc,
             q.status,
             q.primary_supp,
             q.primary_cntry,
             q.receive_as_type,
             q.taxable_ind,
             q.source_method,
             q.source_wh,
             q.primary_repl_supplier,
             q.repl_method,
             q.reject_store_order_ind,
             q.next_delivery_date,
             q.seq_no,
             q.rowid,
             s.stockholding_ind stockholding_ind,
             s.store_type store_type
        from itemloc_mfqueue q,
             store s
       where q.item = I_item
         and q.loc  = s.store(+)
       order by q.seq_no;

BEGIN

   if I_message_type = ITEMLOC_DEL then
      L_itemlocphys_rec := "RIB_ItemLocPhysRef_REC"(0, NULL, NULL, NULL, NULL, NULL);
      O_ilphys_tbl := "RIB_ItemLocPhysRef_TBL"();

      L_itemlocvirt_rec := "RIB_ItemLocVirtRef_REC"(0, NULL);
      L_itemlocvirt_tbl := "RIB_ItemLocVirtRef_TBL"();
   end if;

   FOR rec IN C_ITEMLOC_DETAILS LOOP  --- outer loop

      L_itemlocphys_index := NULL;
      L_itemlocvirt_index := NULL;

      if (I_message_type != rec.message_type) or
         (L_details_processed >= I_max_details) then
         EXIT;
      end if;

      FOR i in 1..O_ilphys_tbl.COUNT LOOP    ----- loop through itemlocphys_tbl (inner loop)

         if O_ilphys_tbl(i).physical_loc = rec.physical_loc then
            L_itemlocphys_index := i;
            EXIT;
         end if;

      end LOOP;

      if L_itemlocphys_index is null then
         O_ilphys_tbl.EXTEND;
         L_itemlocphys_index := O_ilphys_tbl.COUNT;

         O_ilphys_tbl(L_itemlocphys_index)                  := L_itemlocphys_rec;
         O_ilphys_tbl(L_itemlocphys_index).physical_loc     := rec.physical_loc;
         O_ilphys_tbl(L_itemlocphys_index).loc_type         := rec.loc_type;
         O_ilphys_tbl(L_itemlocphys_index).stockholding_ind := rec.stockholding_ind;
         O_ilphys_tbl(L_itemlocphys_index).store_type       := rec.store_type;

         O_ilphys_tbl(L_itemlocphys_index).itemlocvirtref_tbl := L_itemlocvirt_tbl;

         ------ add location to routing info
         if ROUTING_INFO_ADD(O_error_msg,
                             O_routing_info,
                             rec.physical_loc,
                             rec.loc_type) = FALSE then
            return FALSE;
         end if;
      end if;

      L_itemlocvirt_rec.loc := rec.loc;

      O_ilphys_tbl(L_itemlocphys_index).itemlocvirtref_tbl.EXTEND;
      L_itemlocvirt_index := O_ilphys_tbl(L_itemlocphys_index).itemlocvirtref_tbl.COUNT;
      O_ilphys_tbl(L_itemlocphys_index).itemlocvirtref_tbl(L_itemlocvirt_index) := L_itemlocvirt_rec;

      L_mfqueue_size := L_mfqueue_size + 1;
      L_mfqueue_rowid(L_mfqueue_size) := rec.rowid;

      L_details_processed := L_details_processed + 1;

   END LOOP;

   -- if no data found in cursor, raise error
   if L_details_processed = 0 then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_IL_DETAIL_PUB',
                                        I_item, NULL, NULL);
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_mfqueue_size > 0 then
      FORALL i IN 1..L_mfqueue_size
         delete from itemloc_mfqueue where rowid = L_mfqueue_rowid(i);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_module,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_DETAIL_DELETE_OBJECTS;
--------------------------------------------------------------------------------
FUNCTION ROUTING_INFO_ADD(O_error_message         OUT VARCHAR2,
                          O_routing_info      IN  OUT nocopy RIB_ROUTINGINFO_TBL,
                          I_physical_loc      IN      ITEMLOC_MFQUEUE.PHYSICAL_LOC%TYPE,
                          I_loc_type          IN      ITEMLOC_MFQUEUE.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_module   VARCHAR2(64) := 'RMSMFM_ITEMLOC.ROUTING_INFO_ADD';

   L_rib_routing_rec    RIB_ROUTINGINFO_REC := NULL;

BEGIN

   if O_routing_info is NULL then
      O_routing_info := RIB_ROUTINGINFO_TBL();
   end if;

   L_rib_routing_rec := RIB_ROUTINGINFO_REC('to_phys_loc', I_physical_loc,
                                            'to_phys_loc_type', I_loc_type,
                                            null,null);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ROUTING_INFO_ADD;
---------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code       IN OUT         VARCHAR2,
                        O_error_message     IN OUT         VARCHAR2,
                        O_message           IN OUT  nocopy RIB_OBJECT,
                        O_bus_obj_id        IN OUT  nocopy RIB_BUSOBJID_TBL,
                        O_routing_info      IN OUT  nocopy RIB_ROUTINGINFO_TBL,
                        I_item              IN             ITEMLOC_MFQUEUE.ITEM%TYPE,
                        I_physical_loc      IN             ITEMLOC_MFQUEUE.PHYSICAL_LOC%TYPE,
                        I_loc               IN             ITEMLOC_MFQUEUE.LOC%TYPE,
                        I_seq_no            IN             ITEMLOC_MFQUEUE.SEQ_NO%TYPE)
IS

   L_module              VARCHAR2(64) := 'RMSMFM_ITEMLOC.HANDLE_ERRORS';
   L_error_type          VARCHAR2(5) := NULL;
   L_loc_type            VARCHAR2(1);
   L_store_type          VARCHAR2(1);
   L_stockholding_ind    VARCHAR2(1);

   L_itemlocphysref_rec  "RIB_ItemLocPhysRef_REC" :=NULL;
   L_itemlocphysref_tbl  "RIB_ItemLocPhysRef_TBL" :=NULL;

   L_itemlocvirtref_rec  "RIB_ItemLocVirtRef_REC" :=NULL;
   L_itemlocvirtref_tbl  "RIB_ItemLocVirtRef_TBL" :=NULL;

   cursor C_GET_LOC_TYPE is
      select loc_type
        from itemloc_mfqueue
       where loc = I_loc;

   cursor C_GET_STORE_INFO(I_store STORE.STORE%TYPE) is
      select store_type, stockholding_ind
        from store
       where store = I_store;

   cursor C_GET_STCKHLDNG_IND_WH(I_wh WH.WH%TYPE) is
      select stockholding_ind
        from wh
       where wh = I_wh;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then

      O_bus_obj_id    := RIB_BUSOBJID_TBL(I_item);
      O_routing_info  := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no', I_seq_no,null,null,null,null));

      L_itemlocvirtref_rec := "RIB_ItemLocVirtRef_REC"(0,
                                                     I_loc);
      L_itemlocvirtref_tbl := "RIB_ItemLocVirtRef_TBL"(L_itemlocvirtref_rec);
      ---
      open C_GET_LOC_TYPE;
      fetch C_GET_LOC_TYPE into L_loc_type;
      if C_GET_LOC_TYPE%NOTFOUND then
         close C_GET_LOC_TYPE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_GET_LOC_TYPE;

      if L_loc_type = 'S' then
         open C_GET_STORE_INFO(I_loc);
         fetch C_GET_STORE_INFO into L_store_type, L_stockholding_ind;
         if C_GET_STORE_INFO%NOTFOUND then
            close C_GET_STORE_INFO;
            O_status_code := API_CODES.NO_MSG;
            return;
         end if;
         close C_GET_STORE_INFO;
      else
         L_store_type := NULL;
         open C_GET_STCKHLDNG_IND_WH(I_loc);
         fetch C_GET_STCKHLDNG_IND_WH into L_stockholding_ind;
         if C_GET_STCKHLDNG_IND_WH%NOTFOUND then
            close C_GET_STCKHLDNG_IND_WH;
            O_status_code := API_CODES.NO_MSG;
            return;
         end if;
         close C_GET_STCKHLDNG_IND_WH;
      end if;

      L_itemlocphysref_rec := "RIB_ItemLocPhysRef_REC"(0,
                                                     I_physical_loc,
                                                     L_loc_type,
                                                     L_store_type,
                                                     L_stockholding_ind,
                                                     L_itemlocvirtref_tbl);
      L_itemlocphysref_tbl := "RIB_ItemLocPhysRef_TBL"(L_itemlocphysref_rec);
      ---
      O_message := "RIB_ItemLocRef_REC"(0,
                                      I_item,
                                      L_itemlocphysref_tbl);

      update itemloc_mfqueue
         set pub_status = LP_error_status
       where seq_no    = I_seq_no;

   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type,
                   O_error_message);

EXCEPTION
   when OTHERS then
      O_status_code   := API_CODES.UNHANDLED_ERROR;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_module,
                                            TO_CHAR(SQLCODE));
END HANDLE_ERRORS;
------------------------------------------------------------------------------------------
END RMSMFM_ITEMLOC;
/

