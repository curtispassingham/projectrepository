CREATE OR REPLACE PACKAGE ITEM_LOC_TRAITS_SQL AUTHID CURRENT_USER AS

TYPE MULTI_LOC_REC is RECORD(location           MC_LOCATION_TEMP.LOCATION%TYPE,
                             loc_type           MC_LOCATION_TEMP.LOC_TYPE%TYPE);

TYPE MULTI_LOC_TBL is TABLE of MULTI_LOC_REC
   INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------------
-- Function : GET_VALUES
-- Purpose  : This function returns all of the values in the item location
--            traits table, specified by the item and location passed in.
-- Calls    : None
-------------------------------------------------------------------------------
FUNCTION GET_VALUES(O_error_message                 IN OUT  VARCHAR2,
                    O_exists                        IN OUT  BOOLEAN,
                    O_launch_date                   IN OUT  ITEM_LOC_TRAITS.LAUNCH_DATE%TYPE,
                    O_qty_key_options               IN OUT  ITEM_LOC_TRAITS.QTY_KEY_OPTIONS%TYPE,
                    O_manual_price_entry            IN OUT  ITEM_LOC_TRAITS.MANUAL_PRICE_ENTRY%TYPE,
                    O_deposit_code                  IN OUT  ITEM_LOC_TRAITS.DEPOSIT_CODE%TYPE,
                    O_food_stamp_ind                IN OUT  ITEM_LOC_TRAITS.FOOD_STAMP_IND%TYPE,
                    O_wic_ind                       IN OUT  ITEM_LOC_TRAITS.WIC_IND%TYPE,
                    O_proportional_tare_pct         IN OUT  ITEM_LOC_TRAITS.PROPORTIONAL_TARE_PCT%TYPE,
                    O_fixed_tare_value              IN OUT  ITEM_LOC_TRAITS.FIXED_TARE_VALUE%TYPE,
                    O_fixed_tare_uom                IN OUT  ITEM_LOC_TRAITS.FIXED_TARE_UOM%TYPE,
                    O_reward_eligible_ind           IN OUT  ITEM_LOC_TRAITS.REWARD_ELIGIBLE_IND%TYPE,
                    O_natl_brand_comp_item          IN OUT  ITEM_LOC_TRAITS.NATL_BRAND_COMP_ITEM%TYPE,
                    O_return_policy                 IN OUT  ITEM_LOC_TRAITS.RETURN_POLICY%TYPE,
                    O_stop_sale_ind                 IN OUT  ITEM_LOC_TRAITS.STOP_SALE_IND%TYPE,
                    O_elect_mtk_clubs               IN OUT  ITEM_LOC_TRAITS.ELECT_MTK_CLUBS%TYPE,
                    O_report_code                   IN OUT  ITEM_LOC_TRAITS.REPORT_CODE%TYPE,
                    O_req_shelf_life_on_selection   IN OUT  ITEM_LOC_TRAITS.REQ_SHELF_LIFE_ON_SELECTION%TYPE,
                    O_req_shelf_life_on_receipt	    IN OUT  ITEM_LOC_TRAITS.REQ_SHELF_LIFE_ON_RECEIPT%TYPE,
                    O_ib_shelf_life                 IN OUT  ITEM_LOC_TRAITS.IB_SHELF_LIFE%TYPE,
                    O_store_reorderable_ind         IN OUT  ITEM_LOC_TRAITS.STORE_REORDERABLE_IND%TYPE,
                    O_rack_size                     IN OUT  ITEM_LOC_TRAITS.RACK_SIZE%TYPE,
                    O_full_pallet_item              IN OUT  ITEM_LOC_TRAITS.FULL_PALLET_ITEM%TYPE,
                    O_in_store_market_basket        IN OUT  ITEM_LOC_TRAITS.IN_STORE_MARKET_BASKET%TYPE,
                    O_storage_location              IN OUT  ITEM_LOC_TRAITS.STORAGE_LOCATION%TYPE,
                    O_alt_storage_location          IN OUT  ITEM_LOC_TRAITS.ALT_STORAGE_LOCATION%TYPE,
                    O_returnable_ind                IN OUT  ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE,
                    O_refundable_ind                IN OUT  ITEM_LOC_TRAITS.REFUNDABLE_IND%TYPE,
                    O_back_order_ind                IN OUT  ITEM_LOC_TRAITS.BACK_ORDER_IND%TYPE,
                    I_item                          IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                    I_location                      IN      VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : APPLY
-- Purpose  : This function inserts records into the mc_location_temp table
--            that do not already exist on the table.  The only locations
--            inserted are those associated with the inputted item in the
--            item_loc table.
-- Calls    : None

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
-------------------------------------------------------------------------------
FUNCTION APPLY (O_error_message IN OUT  VARCHAR2,
                O_insert_ind    IN OUT  VARCHAR2,
                I_loc           IN      VARCHAR2,
                I_loc_type      IN      CODE_DETAIL.CODE_TYPE%TYPE,
                I_item          IN      ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : DELETE
-- Purpose  : This function deletes records from the mc_location_temp table.
-- Calls    : None
-------------------------------------------------------------------------------
FUNCTION DELETE (O_error_message    IN OUT  VARCHAR2,
                 I_loc              IN      VARCHAR2,
                 I_loc_type         IN      CODE_DETAIL.CODE_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : INSERT_UPDATE_SINGLE_LOC
-- Purpose  : This function will either insert into or update the item/location
--            traits table.  If the item/location combination already exists,
--            it is updated.  If not, it is inserted.
-- Calls    : None
-------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_SINGLE_LOC(O_error_message               IN OUT  VARCHAR2,
                                  I_launch_date                 IN      ITEM_LOC_TRAITS.LAUNCH_DATE%TYPE,
                                  I_qty_key_options             IN      ITEM_LOC_TRAITS.QTY_KEY_OPTIONS%TYPE,
                                  I_manual_price_entry          IN      ITEM_LOC_TRAITS.MANUAL_PRICE_ENTRY%TYPE,
                                  I_deposit_code                IN      ITEM_LOC_TRAITS.DEPOSIT_CODE%TYPE,
                                  I_food_stamp_ind              IN      ITEM_LOC_TRAITS.FOOD_STAMP_IND%TYPE,
                                  I_wic_ind                     IN      ITEM_LOC_TRAITS.WIC_IND%TYPE,
                                  I_proportional_tare_pct       IN      ITEM_LOC_TRAITS.PROPORTIONAL_TARE_PCT%TYPE,
                                  I_fixed_tare_value            IN      ITEM_LOC_TRAITS.FIXED_TARE_VALUE%TYPE,
                                  I_fixed_tare_uom              IN      ITEM_LOC_TRAITS.FIXED_TARE_UOM%TYPE,
                                  I_reward_eligible_ind         IN      ITEM_LOC_TRAITS.REWARD_ELIGIBLE_IND%TYPE,
                                  I_natl_brand_comp_item        IN      ITEM_LOC_TRAITS.NATL_BRAND_COMP_ITEM%TYPE,
                                  I_return_policy               IN      ITEM_LOC_TRAITS.RETURN_POLICY%TYPE,
                                  I_stop_sale_ind               IN      ITEM_LOC_TRAITS.STOP_SALE_IND%TYPE,
                                  I_elect_mtk_clubs             IN      ITEM_LOC_TRAITS.ELECT_MTK_CLUBS%TYPE,
                                  I_report_code                 IN      ITEM_LOC_TRAITS.REPORT_CODE%TYPE,
                                  I_req_shelf_life_on_selection IN      ITEM_LOC_TRAITS.REQ_SHELF_LIFE_ON_SELECTION%TYPE,
                                  I_req_shelf_life_on_receipt   IN      ITEM_LOC_TRAITS.REQ_SHELF_LIFE_ON_RECEIPT%TYPE,
                                  I_ib_shelf_life               IN      ITEM_LOC_TRAITS.IB_SHELF_LIFE%TYPE,
                                  I_store_reorderable_ind       IN      ITEM_LOC_TRAITS.STORE_REORDERABLE_IND%TYPE,
                                  I_rack_size                   IN      ITEM_LOC_TRAITS.RACK_SIZE%TYPE,
                                  I_full_pallet_item            IN      ITEM_LOC_TRAITS.FULL_PALLET_ITEM%TYPE,
                                  I_in_store_market_basket      IN      ITEM_LOC_TRAITS.IN_STORE_MARKET_BASKET%TYPE,
                                  I_storage_location            IN      ITEM_LOC_TRAITS.STORAGE_LOCATION%TYPE,
                                  I_alt_storage_location        IN      ITEM_LOC_TRAITS.ALT_STORAGE_LOCATION%TYPE,
                                  I_returnable_ind              IN      ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE,
                                  I_refundable_ind              IN      ITEM_LOC_TRAITS.REFUNDABLE_IND%TYPE,
                                  I_back_order_ind              IN      ITEM_LOC_TRAITS.BACK_ORDER_IND%TYPE,
                                  I_exists                      IN      BOOLEAN,
                                  I_item                        IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                                  I_location                    IN      VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : INSERT_UPDATE_MULTI_LOC
-- Purpose  : For each location in the mc_location_temp table, this function
--            will either insert into or update the item/location traits table.
--            If the item/location combination already exists, it is updated.
--            If not, it is inserted.
-- Calls    : None
-------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_MULTI_LOC(O_error_message                        IN OUT  VARCHAR2,
                                 I_update_launch_date                   IN      VARCHAR2,
                                 I_launch_date                          IN      ITEM_LOC_TRAITS.LAUNCH_DATE%TYPE,
                                 I_update_qty_key_options               IN      VARCHAR2,
                                 I_qty_key_options                      IN      ITEM_LOC_TRAITS.QTY_KEY_OPTIONS%TYPE,
                                 I_update_manual_price_entry            IN      VARCHAR2,
                                 I_manual_price_entry                   IN      ITEM_LOC_TRAITS.MANUAL_PRICE_ENTRY%TYPE,
                                 I_update_deposit_code                  IN      VARCHAR2,
                                 I_deposit_code                         IN      ITEM_LOC_TRAITS.DEPOSIT_CODE%TYPE,
                                 I_update_food_stamp_ind                IN      VARCHAR2,
                                 I_food_stamp_ind                       IN      ITEM_LOC_TRAITS.FOOD_STAMP_IND%TYPE,
                                 I_update_wic_ind                       IN      VARCHAR2,
                                 I_wic_ind                              IN      ITEM_LOC_TRAITS.WIC_IND%TYPE,
                                 I_update_proportional_tare_pct         IN      VARCHAR2,
                                 I_proportional_tare_pct                IN      ITEM_LOC_TRAITS.PROPORTIONAL_TARE_PCT%TYPE,
                                 I_update_fixed_tare_value              IN      VARCHAR2,
                                 I_fixed_tare_value                     IN      ITEM_LOC_TRAITS.FIXED_TARE_VALUE%TYPE,
                                 I_update_fixed_tare_uom                IN      VARCHAR2,
                                 I_fixed_tare_uom                       IN      ITEM_LOC_TRAITS.FIXED_TARE_UOM%TYPE,
                                 I_update_reward_eligible_ind           IN      VARCHAR2,
                                 I_reward_eligible_ind                  IN      ITEM_LOC_TRAITS.REWARD_ELIGIBLE_IND%TYPE,
                                 I_update_natl_brand_comp_item          IN      VARCHAR2,
                                 I_natl_brand_comp_item                 IN      ITEM_LOC_TRAITS.NATL_BRAND_COMP_ITEM%TYPE,
                                 I_update_return_policy                 IN      VARCHAR2,
                                 I_return_policy                        IN      ITEM_LOC_TRAITS.RETURN_POLICY%TYPE,
                                 I_update_stop_sale_ind                 IN      VARCHAR2,
                                 I_stop_sale_ind                        IN      ITEM_LOC_TRAITS.STOP_SALE_IND%TYPE,
                                 I_update_elect_mtk_clubs               IN      VARCHAR2,
                                 I_elect_mtk_clubs                      IN      ITEM_LOC_TRAITS.ELECT_MTK_CLUBS%TYPE,
                                 I_update_report_code                   IN      VARCHAR2,
                                 I_report_code                          IN      ITEM_LOC_TRAITS.REPORT_CODE%TYPE,
                                 I_upd_req_shelf_life_on_select         IN      VARCHAR2,
                                 I_req_shelf_life_on_selection          IN      ITEM_LOC_TRAITS.REQ_SHELF_LIFE_ON_SELECTION%TYPE,
                                 I_upd_req_shelf_life_on_rcpt           IN      VARCHAR2,
                                 I_req_shelf_life_on_receipt            IN      ITEM_LOC_TRAITS.REQ_SHELF_LIFE_ON_RECEIPT%TYPE,
                                 I_upd_ib_shelf_life                    IN      VARCHAR2,
                                 I_ib_shelf_life                        IN      ITEM_LOC_TRAITS.IB_SHELF_LIFE%TYPE, 
                                 I_upd_store_reorderable_ind            IN      VARCHAR2,
                                 I_store_reorderable_ind                IN      ITEM_LOC_TRAITS.STORE_REORDERABLE_IND%TYPE,
                                 I_update_rack_size                     IN      VARCHAR2,
                                 I_rack_size                            IN      ITEM_LOC_TRAITS.RACK_SIZE%TYPE,
                                 I_update_full_pallet_item              IN      VARCHAR2,
                                 I_full_pallet_item                     IN      ITEM_LOC_TRAITS.FULL_PALLET_ITEM%TYPE,
                                 I_upd_in_store_market_basket           IN      VARCHAR2,
                                 I_in_store_market_basket               IN      ITEM_LOC_TRAITS.IN_STORE_MARKET_BASKET%TYPE,
                                 I_update_storage_location              IN      VARCHAR2,
                                 I_storage_location                     IN      ITEM_LOC_TRAITS.STORAGE_LOCATION%TYPE,
                                 I_update_alt_storage_location          IN      VARCHAR2,
                                 I_alt_storage_location                 IN      ITEM_LOC_TRAITS.ALT_STORAGE_LOCATION%TYPE,
                                 I_update_returnable_ind                IN      VARCHAR2,
                                 I_returnable_ind                       IN      ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE,
                                 I_update_refundable_ind                IN      VARCHAR2,
                                 I_refundable_ind                       IN      ITEM_LOC_TRAITS.REFUNDABLE_IND%TYPE,
                                 I_update_back_order_ind                IN      VARCHAR2,
                                 I_back_order_ind                       IN      ITEM_LOC_TRAITS.BACK_ORDER_IND%TYPE,
                                 I_item                                 IN      ITEM_LOC_TRAITS.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : COPY_DOWN_PARENT_SINGLE_LOC
-- Purpose  : This function will default identical item/location trait records
--            down to the children and grandchildren levels for the inputted
--            item and location.  The defaulting will go down as far as the
--            transaction level.
-- Calls    : None
-------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_SINGLE_LOC (O_error_message   IN OUT   VARCHAR2,
                                      I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                      I_loc             IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : COPY_DOWN_PARENT_MULTI_LOC
-- Purpose  : This function will default identical item/location trait records
--            down to the children and grandchildren levels for the inputted
--            item.  The only locations defaulted will be those in the table
--            mc_location_temp.  The defaulting will go down as far as the
--            transaction level.
-- Calls    : None
-------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_MULTI_LOC (O_error_message   IN OUT   VARCHAR2,
                                     I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION DERIVE_RETURNABLE_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_derived_ret_ind      IN OUT  ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE,
                               I_item                 IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                               I_location             IN      VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_RETURNABLE_LOCS(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                 IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                             O_multi_loc_tbl        IN OUT  ITEM_LOC_TRAITS_SQL.MULTI_LOC_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION GET_RET_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_ret_ind              IN OUT  ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE,
                     I_item                 IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                     I_location             IN      VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PUBLISH_RET_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item                 IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                         I_multi_loc_tbl        IN      ITEM_LOC_TRAITS_SQL.MULTI_LOC_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION CHECK_RETURNABLE_LOC(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists               IN OUT  BOOLEAN,
                              I_item                 IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                              I_location             IN      VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION PUBLISH_RET_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item                 IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                         I_loc                  IN      ITEM_LOC_TRAITS.LOC%TYPE,
                         I_ret_ind              IN      ITEM_LOC_TRAITS.RETURNABLE_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : GET_RETURNABLE_LOCS_WRP
-- Purpose  : This is a wrapper function for GET_RETURNABLE_LOCS that returns a database
--            type object instead of a PLSQL type to be called from Java wrappers.
-------------------------------------------------------------------------------
FUNCTION GET_RETURNABLE_LOCS_WRP(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_item                 IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                                 O_multi_loc_tbl        IN OUT  WRP_LOCN_LOC_TYPE_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function : PUBLISH_RET_IND_WRP
-- Purpose  : This is a wrapper function for PUBLISH_RET_IND that returns a database
--            type object instead of a PLSQL type to be called from Java wrappers.
-------------------------------------------------------------------------------
FUNCTION PUBLISH_RET_IND_WRP(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                 IN      ITEM_LOC_TRAITS.ITEM%TYPE,
                             I_multi_loc_tbl        IN      WRP_LOCN_LOC_TYPE_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END;
/