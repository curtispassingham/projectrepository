
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHR AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2);
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC PROCEDURE
-------------------------------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code     IN OUT   VARCHAR2,
                  O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message         IN       RIB_OBJECT,
                  I_message_type    IN       VARCHAR2) IS

   PROGRAM_ERROR    EXCEPTION;

   L_program               VARCHAR2(50) := 'RMSSUB_XMRCHHR.CONSUME';
   L_message_type          VARCHAR2(15) := LOWER(I_message_type);

BEGIN

   O_STATUS_CODE := API_CODES.SUCCESS;

   -- perform common api initialization tasks
   if API_LIBRARY.INIT(O_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if I_message is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', NULL, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;
 
   if L_message_type in (RMSSUB_XMRCHHRCOMP.LP_cre_type,
                         RMSSUB_XMRCHHRCOMP.LP_mod_type) then
                         
      RMSSUB_XMRCHHRCOMP.CONSUME(O_status_code,
      			             	 O_error_message,
                                 I_message,
                                 I_message_type);
                                 
   elsif L_message_type in (RMSSUB_XMRCHHRDIV.LP_cre_type,
                            RMSSUB_XMRCHHRDIV.LP_mod_type,
                            RMSSUB_XMRCHHRDIV.LP_del_type) then
                            
      RMSSUB_XMRCHHRDIV.CONSUME(O_status_code,
      	                        O_error_message,
                                I_message,
                                I_message_type);
                                
   elsif L_message_type in (RMSSUB_XMRCHHRGRP.LP_cre_type,
                            RMSSUB_XMRCHHRGRP.LP_mod_type,
                            RMSSUB_XMRCHHRGRP.LP_del_type) then
                            
      RMSSUB_XMRCHHRGRP.CONSUME(O_status_code,
      				            O_error_message,
                                I_message,
                                I_message_type);
                                
   elsif L_message_type in (RMSSUB_XMRCHHRDEPT.LP_cre_type,
                            RMSSUB_XMRCHHRDEPT.LP_mod_type,
                            RMSSUB_XMRCHHRDEPT.LP_del_type,
		                    RMSSUB_XMRCHHRDEPT.LP_dtl_cre_type, 
		                    RMSSUB_XMRCHHRDEPT.LP_dtl_mod_type) then
		                    
      RMSSUB_XMRCHHRDEPT.CONSUME(O_status_code,
      				             O_error_message,
                                 I_message,
                                 I_message_type);
                                 
   elsif L_message_type in (RMSSUB_XMRCHHRCLS.LP_cre_type,
                            RMSSUB_XMRCHHRCLS.LP_mod_type,
                            RMSSUB_XMRCHHRCLS.LP_del_type) then
                            
      RMSSUB_XMRCHHRCLS.CONSUME(O_status_code,
      	 		                O_error_message,
                                I_message,
                                I_message_type);
                                
  elsif L_message_type in (RMSSUB_XMRCHHRSCLS.LP_cre_type,
                           RMSSUB_XMRCHHRSCLS.LP_mod_type,
                           RMSSUB_XMRCHHRSCLS.LP_del_type) then
                           
      RMSSUB_XMRCHHRSCLS.CONSUME(O_status_code,
      				             O_error_message,
                                 I_message,
                                 I_message_type);
  else
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MSG_TYPE', NVL(I_message_type, 'NULL'), NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   return;

EXCEPTION
   when PROGRAM_ERROR then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);
   when OTHERS then
       HANDLE_ERRORS(O_status_code,
                     O_error_message,
                     API_LIBRARY.FATAL_ERROR,
                     L_program);

END CONSUME;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE PROCEDURE BODY
-------------------------------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_status_code      IN OUT   VARCHAR2,
                        IO_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cause            IN       VARCHAR2,
                        I_program          IN       VARCHAR2) IS

   L_program VARCHAR2(50) := 'RMSSUB_XMRCHHR.HANDLE_ERRORS';

BEGIN

   API_LIBRARY.HANDLE_ERRORS(O_status_code,
                             IO_error_message,
                             I_cause,
                             I_program);

EXCEPTION
   when OTHERS then
      IO_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));

      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                IO_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                L_program);

END HANDLE_ERRORS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHR;
/
