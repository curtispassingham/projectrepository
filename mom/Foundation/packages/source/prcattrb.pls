CREATE OR REPLACE PACKAGE BODY PRICING_ATTRIB_SQL AS

------------------------------------------------------------------------
-- Description: Get the base zone and retail information
--              from the sku_zone_price table for a given item number.
------------------------------------------------------------------------
FUNCTION GET_BASE_ZONE_RETAIL(O_error_message            IN OUT VARCHAR2,
                              O_standard_unit_retail_zon IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom_zon         IN OUT ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail_zon  IN OUT ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom_zon          IN OUT ITEM_LOC.SELLING_UOM%TYPE,
                              O_multi_units_zon          IN OUT ITEM_LOC.MULTI_UNITS%TYPE,
                              O_multi_unit_retail_zon    IN OUT ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                              O_multi_selling_uom_zon    IN OUT ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                              I_item                     IN     ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'PRICING_ATTRIB_SQL.GET_BASE_ZONE_RETAIL';
   L_zone_currency      CURRENCIES.CURRENCY_CODE%TYPE;

   L_item               ITEM_MASTER%ROWTYPE ;
   L_qty                NUMBER;
   L_rpm_ind            VARCHAR2(1);

BEGIN

   select rpm_ind
     into L_rpm_ind
     from system_options;

   if I_item is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_rpm_ind = 'Y' then
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item,
                                          I_item) = FALSE then
         return FALSE;
      end if;
      
      O_standard_uom_zon := L_item.standard_uom;
      ---
      
      if PM_RETAIL_API_SQL.GET_BASE_RETAIL(O_error_message,
                                           O_selling_unit_retail_zon,
                                           O_selling_uom_zon,
                                           O_multi_units_zon,
                                           O_multi_unit_retail_zon,
                                           O_multi_selling_uom_zon,
                                           L_zone_currency,
                                           I_item,
                                           L_item.dept,
                                           L_item.class,
                                           L_item.subclass) = FALSE then
         return FALSE;
      end if;
      
      if O_standard_uom_zon = O_selling_uom_zon then
         O_standard_unit_retail_zon := O_selling_unit_retail_zon;
      else
         if UOM_SQL.CONVERT(O_error_message,
                            L_qty,
                            O_standard_uom_zon,
                            1,
                            O_selling_uom_zon,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV', O_standard_uom_zon, O_selling_uom_zon, NULL);
            return FALSE;
         end if;
         ---
         O_standard_unit_retail_zon := O_selling_unit_retail_zon / L_qty; -- the selling unit retail in standard UOM
      end if;
   else
      if GET_CURR_UNIT_RETAIL(O_error_message,
                              O_standard_unit_retail_zon,
                              O_standard_uom_zon,
                              O_selling_unit_retail_zon,
                              O_selling_uom_zon,
                              I_item) = FALSE then
         return FALSE;
      end if;
   end if;

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_BASE_ZONE_RETAIL;
-----------------------------------------------------------------------
FUNCTION GET_BASE_ZONE_RETAIL(O_error_message            IN OUT VARCHAR2,
                              O_standard_unit_retail_pri IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_unit_retail_zon IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom_zon         IN OUT ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail_zon  IN OUT ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom_zon          IN OUT ITEM_LOC.SELLING_UOM%TYPE,
                              O_multi_units_zon          IN OUT ITEM_LOC.MULTI_UNITS%TYPE,
                              O_multi_unit_retail_zon    IN OUT ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                              O_multi_selling_uom_zon    IN OUT ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                              I_item                     IN     ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'PRICING_ATTRIB_SQL.GET_BASE_ZONE_RETAIL';

   L_zone_currency      CURRENCIES.CURRENCY_CODE%TYPE;
   L_prim_currency      CURRENCIES.CURRENCY_CODE%TYPE;
   L_item               ITEM_MASTER%ROWTYPE ;
   L_qty                NUMBER;
   L_rpm_ind            VARCHAR2(1);

BEGIN

   select rpm_ind
     into L_rpm_ind
     from system_options;

   if I_item is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_rpm_ind = 'Y' then
      if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                              L_prim_currency) then
         return FALSE;
      end if;
      
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item,
                                          I_item) = FALSE then
         return FALSE;
      end if;
      
      O_standard_uom_zon := L_item.standard_uom;
      
      --This base retail will be in the local currency of the zone.
      
      if PM_RETAIL_API_SQL.GET_BASE_RETAIL(O_error_message,
                                           O_selling_unit_retail_zon,
                                           O_selling_uom_zon,
                                           O_multi_units_zon,
                                           O_multi_unit_retail_zon,
                                           O_multi_selling_uom_zon,
                                           L_zone_currency,
                                           I_item,
                                           L_item.dept,
                                           L_item.class,
                                           L_item.subclass) = FALSE then
         return FALSE;
      end if;
      
      if O_standard_uom_zon = O_selling_uom_zon then
         O_standard_unit_retail_zon := O_selling_unit_retail_zon;
      else
         if UOM_SQL.CONVERT(O_error_message,
                            L_qty,
                            O_standard_uom_zon,
                            1,
                            O_selling_uom_zon,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV', O_standard_uom_zon, O_selling_uom_zon, NULL);
            return FALSE;
         end if;
         ---
         O_standard_unit_retail_zon := O_selling_unit_retail_zon / L_qty; -- the selling unit retail in standard UOM
      end if;
      
      if L_zone_currency = L_prim_currency then
         O_standard_unit_retail_pri:= O_standard_unit_retail_zon;
      else
         ---convert the local currency value to the primary currency value
         ---
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_standard_unit_retail_zon,
                                 L_zone_currency,
                                 L_prim_currency,
                                 O_standard_unit_retail_pri,
                                 'R',
                                 NULL,
                                 NULL) = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   else
      if GET_CURR_UNIT_RETAIL(O_error_message,
                              O_standard_unit_retail_pri,
                              O_standard_uom_zon,
                              O_selling_unit_retail_zon,
                              O_selling_uom_zon,
                              I_item) = FALSE then
         return FALSE;
      end if;
      O_standard_unit_retail_zon := O_standard_unit_retail_pri;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_BASE_ZONE_RETAIL;
------------------------------------------------------------------------
FUNCTION GET_BASE_ZONE_RETAIL(O_error_message            IN OUT VARCHAR2,
                              O_standard_unit_retail_pri IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_unit_retail_zon IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom_zon         IN OUT ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail_pri  IN OUT ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_unit_retail_zon  IN OUT ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom_zon          IN OUT ITEM_LOC.SELLING_UOM%TYPE,
                              O_multi_units_zon          IN OUT ITEM_LOC.MULTI_UNITS%TYPE,
                              O_multi_unit_retail_pri    IN OUT ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                              O_multi_unit_retail_zon    IN OUT ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                              O_multi_selling_uom_zon    IN OUT ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                              I_item                     IN     ITEM_LOC.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'PRICING_ATTRIB_SQL.GET_BASE_ZONE_RETAIL';

   L_zone_currency      CURRENCIES.CURRENCY_CODE%TYPE;
   L_prim_currency      CURRENCIES.CURRENCY_CODE%TYPE;
   L_item               ITEM_MASTER%ROWTYPE ;   
   L_qty                NUMBER;
   L_rpm_ind            VARCHAR2(1);

BEGIN

   select rpm_ind
     into L_rpm_ind
     from system_options;

   if I_item is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if L_rpm_ind = 'Y' then
      if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                              L_prim_currency) then
         return FALSE;
      end if;
      
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item,
                                          I_item) = FALSE then
         return FALSE;
      end if;
      
      O_standard_uom_zon := L_item.standard_uom;
      
      --This base retail will be in the local currency of the zone.
      
      if PM_RETAIL_API_SQL.GET_BASE_RETAIL(O_error_message,
                                           O_selling_unit_retail_zon,
                                           O_selling_uom_zon,
                                           O_multi_units_zon,
                                           O_multi_unit_retail_zon,
                                           O_multi_selling_uom_zon,
                                           L_zone_currency,
                                           I_item,
                                           L_item.dept,
                                           L_item.class,
                                           L_item.subclass) = FALSE then
         return FALSE;
      end if;   
      
      if O_standard_uom_zon = O_selling_uom_zon then
         O_standard_unit_retail_zon := O_selling_unit_retail_zon;
      else
         if UOM_SQL.CONVERT(O_error_message,
                            L_qty,
                            O_standard_uom_zon,
                            1,
                            O_selling_uom_zon,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            return FALSE;
         end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV', O_standard_uom_zon, O_selling_uom_zon, NULL);
            return FALSE;
         end if;
         ---
         O_standard_unit_retail_zon := O_selling_unit_retail_zon / L_qty; -- the selling unit retail in standard UOM
      end if;
      
      if L_zone_currency = L_prim_currency then
         O_selling_unit_retail_pri := O_selling_unit_retail_zon;
         O_multi_unit_retail_pri   := O_multi_unit_retail_zon;
         O_standard_unit_retail_pri:= O_standard_unit_retail_zon;
      else
         ---convert the local currency value to the primary currency value
         ---
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_selling_unit_retail_zon,
                                 L_zone_currency,
                                 L_prim_currency,
                                 O_selling_unit_retail_pri,
                                 'R',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 'N') = FALSE then
            return FALSE;
         end if;
         
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_multi_unit_retail_zon,
                                 L_zone_currency,
                                 L_prim_currency,
                                 O_multi_unit_retail_pri,
                                 'R',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 'N') = FALSE then
            return FALSE;
         end if;
         
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 O_standard_unit_retail_zon,
                                 L_zone_currency,
                                 L_prim_currency,
                                 O_standard_unit_retail_pri,
                                 'R',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 'N') = FALSE then
            return FALSE;
         end if;
         ---
      end if;
   else
      if GET_CURR_UNIT_RETAIL(O_error_message,
                              O_standard_unit_retail_pri,
                              O_standard_uom_zon,
                              O_selling_unit_retail_pri,
                              O_selling_uom_zon,
                              I_item) = FALSE then
         return FALSE;
      end if;
      O_standard_unit_retail_zon := O_standard_unit_retail_pri;
      O_selling_unit_retail_zon := O_selling_unit_retail_pri;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_BASE_ZONE_RETAIL;
------------------------------------------------------------------------
-- Description: Get the Retail and UOM values from the item_loc table
--               for a given item number and a location, or query RPM
--               for the retail for a non-ranged location.
------------------------------------------------------------------------
FUNCTION GET_RETAIL(O_error_message             IN OUT VARCHAR2,
                    O_standard_unit_retail_loc  IN OUT item_loc.unit_retail%TYPE,
                    O_standard_uom_loc          IN OUT item_master.standard_uom%TYPE,
                    O_selling_unit_retail_loc   IN OUT item_loc.selling_unit_retail%TYPE,
                    O_selling_uom_loc           IN OUT item_loc.selling_uom%TYPE,
                    O_multi_units_loc           IN OUT item_loc.multi_units%TYPE,
                    O_multi_unit_retail_loc     IN OUT item_loc.multi_unit_retail%TYPE,
                    O_multi_selling_uom_loc     IN OUT item_loc.multi_selling_uom%TYPE,
                    I_item                      IN     item_loc.item%TYPE,
                    I_loc_type                  IN     item_loc.loc_type%TYPE,
                    I_location                  IN     item_loc.loc%TYPE)
RETURN BOOLEAN IS

   L_program                     VARCHAR2(64) := 'PRICING_ATTRIB_SQL.GET_RETAIL';

   L_supplier                    SUPS.SUPPLIER%TYPE;
   L_unit_cost                   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_currency_code               CURRENCIES.CURRENCY_CODE%TYPE;
   L_item                        ITEM_MASTER%ROWTYPE ;
   L_loc_type                    ITEM_LOC.LOC_TYPE%TYPE;

   cursor C_GET_INFO is
      select il.unit_retail,
             im.standard_uom,
             il.selling_unit_retail,
             il.selling_uom,
             il.multi_units,
             il.multi_unit_retail,
             il.multi_selling_uom
        from item_loc il,
             item_master im
       where il.item = I_item
         and im.item = il.item
         and il.loc = I_location
         and ((im.pack_ind = 'Y' and im.sellable_ind = 'Y') or
               im.pack_ind = 'N');

BEGIN
   if I_location is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   O_standard_unit_retail_loc := NULL;
   O_standard_uom_loc := NULL;
   O_selling_unit_retail_loc := NULL;
   O_selling_uom_loc := NULL;
   O_multi_units_loc := NULL;
   O_multi_unit_retail_loc := NULL;
   O_multi_selling_uom_loc := NULL;
   
   if I_loc_type = 'I' then
      L_loc_type := 'W';
   else
      L_loc_type := I_loc_type;
   end if;
   ---
   if L_loc_type in ('S','W') then
      -- Location is a store/warehouse
      SQL_LIB.SET_MARK('OPEN','C_GET_INFO','ITEM_LOC', NULL);
      open C_GET_INFO;
      SQL_LIB.SET_MARK('FETCH','C_GET_INFO','ITEM_LOC', NULL);
      fetch C_GET_INFO into O_standard_unit_retail_loc,
                            O_standard_uom_loc,
                            O_selling_unit_retail_loc,
                            O_selling_uom_loc,
                            O_multi_units_loc,
                            O_multi_unit_retail_loc,
                            O_multi_selling_uom_loc;
      ---

      if C_GET_INFO%NOTFOUND 
         or (O_standard_unit_retail_loc is NULL and O_selling_unit_retail_loc is NULL) then

         if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                             L_item,
                                             I_item) = FALSE then
            return FALSE;
         end if;

         if PM_RETAIL_API_SQL.GET_NEW_ITEM_LOC_RETAIL(O_error_message,
                                                      O_selling_unit_retail_loc,
                                                      O_selling_uom_loc,
                                                      O_multi_units_loc,
                                                      O_multi_unit_retail_loc,
                                                      O_multi_selling_uom_loc,
                                                      I_item,
                                                      L_item.dept,
                                                      L_item.class,
                                                      L_item.subclass,
                                                      I_location,
                                                      L_loc_type,
                                                      NULL,
                                                      NULL) = FALSE then
            return FALSE;
         end if;

         O_standard_unit_retail_loc     := O_selling_unit_retail_loc;
         O_standard_uom_loc := L_item.standard_uom;

      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_INFO','ITEM_LOC', NULL);
      close C_GET_INFO;
      ---
   else  -- loc_type = 'E'
      ---
      if GET_EXTERNAL_FINISHER_RETAIL(O_error_message,
                                      O_standard_unit_retail_loc,
                                      I_item,
                                      I_location) = FALSE then
         return FALSE;
      end if;
      ---
   end if;

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_RETAIL;
------------------------------------------------------------------------
-- Description: Get the Retail and UOM values from the item_loc table
--               for a given item number and a location.
------------------------------------------------------------------------
FUNCTION GET_RETAIL(O_error_message             IN OUT VARCHAR2,
                    O_standard_unit_retail_loc  IN OUT item_loc.unit_retail%TYPE,
                    O_selling_unit_retail_loc   IN OUT item_loc.selling_unit_retail%TYPE,
                    O_selling_uom_loc           IN OUT item_loc.selling_uom%TYPE,
                    O_multi_units_loc           IN OUT item_loc.multi_units%TYPE,
                    O_multi_unit_retail_loc     IN OUT item_loc.multi_unit_retail%TYPE,
                    O_multi_selling_uom_loc     IN OUT item_loc.multi_selling_uom%TYPE,
                    I_item                      IN     item_loc.item%TYPE,
                    I_loc_type                  IN     item_loc.loc_type%TYPE,
                    I_location                  IN     item_loc.loc%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64) := 'PRICING_ATTRIB_SQL.GET_RETAIL';

   L_standard_retail_currency    currencies.currency_code%TYPE;
   L_standard_uom                ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_retail_currency     currencies.currency_code%TYPE;
   L_multi_unit_retail_currency  currencies.currency_code%TYPE;
   L_supplier                    SUPS.SUPPLIER%TYPE;
   L_unit_cost                   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;

   L_item                        ITEM_MASTER%ROWTYPE ;

   cursor C_GET_INFO is
      select unit_retail,
             selling_unit_retail,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_selling_uom
        from item_loc
       where item = I_item
         and loc = I_location;

BEGIN

   if I_location is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   O_standard_unit_retail_loc   := NULL;
   O_selling_unit_retail_loc    := NULL;
   O_selling_uom_loc            := NULL;
   O_multi_units_loc            := NULL;
   O_multi_unit_retail_loc      := NULL;
   O_multi_selling_uom_loc      := NULL;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_INFO','ITEM_LOC', NULL);
   open C_GET_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_INFO','ITEM_LOC',NULL);
   fetch C_GET_INFO into O_standard_unit_retail_loc,
                         O_selling_unit_retail_loc,
                         O_selling_uom_loc,
                         O_multi_units_loc,
                         O_multi_unit_retail_loc,
                         O_multi_selling_uom_loc;
      ---
      if C_GET_INFO%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_INFO','ITEM_LOC', NULL);
         close C_GET_INFO;
         if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                             L_item,
                                             I_item) = FALSE then
            return FALSE;
         end if;
         if PM_RETAIL_API_SQL.GET_NEW_ITEM_LOC_RETAIL(O_error_message,
                                                      O_selling_unit_retail_loc,
                                                      O_selling_uom_loc,
                                                      O_multi_units_loc,
                                                      O_multi_unit_retail_loc,
                                                      O_multi_selling_uom_loc,
                                                      I_item,
                                                      L_item.dept,
                                                      L_item.class,
                                                      L_item.subclass,
                                                      I_location,
                                                      I_loc_type,
                                                      NULL,
                                                      NULL) = FALSE then
            return FALSE;
         end if;

         O_standard_unit_retail_loc     := O_selling_unit_retail_loc;

      else
         SQL_LIB.SET_MARK('CLOSE','C_GET_INFO','ITEM_LOC', NULL);
         close C_GET_INFO;
      end if;
      ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_RETAIL;
-----------------------------------------------------------------------------
FUNCTION BUILD_PACK_COST(O_error_message    IN OUT VARCHAR2,
                         O_unit_cost        IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                         IO_supplier        IN OUT SUPS.SUPPLIER%TYPE,
                         I_origin_country   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_pack_no          IN     ITEM_MASTER.ITEM%TYPE,
                         I_dlvry_country    IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
                         RETURN BOOLEAN IS

   L_program        VARCHAR2(64) := 'PRICING_ATTRIB_SQL.BUILD_PACK_COST';
   -- Not used for processing.  Just used for function call.
   L_unit_cost_sup  ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_cnt            NUMBER;
   L_dlvry_country  ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_base_country_id  ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE;
   L_default_tax_type SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;
  
   cursor C_GET_SYSTEM_OPTION is
         select base_country_id ,
                default_tax_type
         from system_options   ;
              
   cursor C_GET_DLVRY_COUNTRY is
      select ich.delivery_country_id
        from item_cost_head ich
         where ich.item                      = I_pack_no 
         and ich.supplier                    = IO_supplier
         and ich.prim_dlvy_ctry_ind          ='Y'
         and ich.origin_country_id           = I_origin_country;
   
                 
    cursor C_SUM_PACK_COST is
      select sum(i.unit_cost * v.qty)
        from item_supp_country i,
             v_packsku_qty v
       where v.pack_no                       = I_pack_no
         and v.item                          = i.item
         and i.origin_country_id             = I_origin_country
         and i.supplier                      = IO_supplier
         and L_default_tax_type              in ('SVAT', 'SALES')
      union all
       select sum(decode(c.item_cost_tax_incl_ind, 'N', ich.base_cost * v.qty, ich.negotiated_item_cost * v.qty)) 
        from item_supp_country i,
             item_cost_head ich ,
             v_packsku_qty v,
             country_attrib c
       where v.pack_no                       = I_pack_no
         and v.item                          = i.item
         and i.origin_country_id             = I_origin_country
         and i.supplier                      = IO_supplier
         and ich.item                        = v.item 
         and ich.supplier                    = IO_supplier
         and ich.origin_country_id           = I_origin_country
         and ich.delivery_country_id         = L_dlvry_country
         and c.country_id                    = L_dlvry_country
         and L_default_tax_type              = 'GTAX'
      order by 1;

BEGIN

   if I_origin_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   O_unit_cost := 0;
   ---
   if IO_supplier is NULL then
     if not SUPP_ITEM_SQL.GET_PRI_SUP_COST(O_error_message,
                                           IO_supplier,
                                           L_unit_cost_sup,
                                           I_pack_no,
                             NULL) then
         return FALSE;
      end if;
   end if;
   ---
   open C_GET_SYSTEM_OPTION;
   fetch C_GET_SYSTEM_OPTION into L_base_country_id,L_default_tax_type;
   close C_GET_SYSTEM_OPTION;
   --
   open C_GET_DLVRY_COUNTRY;
   fetch C_GET_DLVRY_COUNTRY into L_dlvry_country;
   close C_GET_DLVRY_COUNTRY;
   ---
   if L_dlvry_country is NULL then
      L_dlvry_country := L_base_country_id;
   end if;
   if I_dlvry_country is not NULL then
      L_dlvry_country := I_dlvry_country;
   end if;
   ---
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_SUM_PACK_COST',
                    'ITEM_SUPP_COUNTRY_LOC, V_PACKSKU_QTY',
                    'Pack No: '    || I_pack_no ||
                    ', Supplier: ' || to_char(IO_supplier) ||
                    ', Country: ' || I_origin_country);
   ---
   open C_SUM_PACK_COST;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUM_PACK_COST',
                    'ITEM_SUPP_COUNTRY_LOC, V_PACKSKU_QTY',
                    'Pack No: '    || I_pack_no ||
                    ', Supplier: ' || to_char(IO_supplier) ||
                    ', Country: ' || I_origin_country);
   ---
   fetch C_SUM_PACK_COST into O_unit_cost;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUM_PACK_COST',
                    'ITEM_SUPP_COUNTRY_LOC, V_PACKSKU_QTY',
                    'Pack No: '    || I_pack_no ||
                    ', Supplier: ' || to_char(IO_supplier) ||
                    ', Country: ' || I_origin_country);
   ---
   close C_SUM_PACK_COST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_PACK_COST;

-----------------------------------------------------------------------------
FUNCTION BUILD_PACK_RETAIL(O_error_message    IN OUT VARCHAR2,
                           O_unit_retail      IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                           I_pack_no          IN     ITEM_MASTER.ITEM%TYPE,
                           I_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                           I_location         IN     STORE.STORE%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(64) := 'PRICING_ATTRIB_SQL.BUILD_PACK_RETAIL';
   L_comp_qty                  V_PACKSKU_QTY.QTY%TYPE;

   L_pack_unit_retail          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_pack_loc_exists           BOOLEAN;

   L_unit_retail               ITEM_LOC.UNIT_RETAIL%TYPE;
   L_selling_unit_retail_zon   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_zon           ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_zon           ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_zon     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_zon     ITEM_LOC.MULTI_SELLING_UOM%TYPE;

   L_item                      ITEM_MASTER%ROWTYPE ;


   cursor C_SUM_PACK_RETAIL is
      select sum(i.unit_retail * v.qty)
        from item_loc i,
             v_packsku_qty v
       where v.pack_no = I_pack_no
         and v.item = i.item
         and i.loc = I_location;

   cursor C_SUM_PACK_QTY is
      select item,
             sum(v.qty) qty
        from v_packsku_qty v
       where v.pack_no = I_pack_no
        group by item;

BEGIN
   ---
   if I_location is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_pack_no is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_pack_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if I_loc_type is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_loc_type',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   -- At external finisher location, pack's retail should always get at component level.
   if I_loc_type = 'E' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_unit_retail := 0;
   ---
   --- Check to see if the pack is ranged to this location because one of it's components could
   --- be ranged to this location but not the pack
   if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                        I_pack_no,
                                        I_location,
                                        L_pack_loc_exists) = FALSE then
      return FALSE;
   end if;

   if L_pack_loc_exists then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_SUM_PACK_RETAIL',
                       'ITEM_LOC, V_PACKSKU_QTY',
                       'Pack No: '    || I_pack_no ||
                       ', Store: ' || to_char(I_location));

      ---
      open C_SUM_PACK_RETAIL;
      ---
      SQL_LIB.SET_MARK('FETCH',
                       'C_SUM_PACK_RETAIL',
                       'ITEM_LOC, V_PACKSKU_QTY',
                       'Pack No: '    || I_pack_no ||
                       ', Store: ' || to_char(I_location));

      ---
      fetch C_SUM_PACK_RETAIL into L_pack_unit_retail;

   end if;

   if L_pack_loc_exists = FALSE  or
      L_pack_unit_retail is NULL then

         L_pack_unit_retail := 0;

         SQL_LIB.SET_MARK('OPEN',
                          'C_SUM_PACK_QTY',
                          'V_PACKSKU_QTY',
                          'Pack No: '    || I_pack_no);
         ---
         FOR irec IN C_SUM_PACK_QTY LOOP

            if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                                L_item,
                                                irec.item) = FALSE then
               return FALSE;
            end if;
            if  L_item.sellable_ind='Y' then 
               if PM_RETAIL_API_SQL.GET_NEW_ITEM_LOC_RETAIL(O_error_message,
                                                            L_selling_unit_retail_zon,
                                                            L_selling_uom_zon,
                                                            L_multi_units_zon,
                                                            L_multi_unit_retail_zon,
                                                            L_multi_selling_uom_zon,
                                                            irec.item,
                                                            L_item.dept,
                                                            L_item.class,
                                                            L_item.subclass,
                                                            I_location,
                                                            I_loc_type,
                                                            NULL,
                                                            NULL) = FALSE then
                  return FALSE;
               end if;
                  L_unit_retail := L_selling_unit_retail_zon;
                  L_pack_unit_retail := L_pack_unit_retail + (L_unit_retail * irec.qty);
            end if;
         END LOOP;
      ---
   end if;
   ---

   if C_SUM_PACK_RETAIL%ISOPEN then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SUM_PACK_RETAIL',
                       'ITEM_LOC, V_PACKSKU_QTY',
                       'Pack No: '    || I_pack_no ||
                       ', Store: ' || to_char(I_location));
      close C_SUM_PACK_RETAIL;
   end if;

   ---
   O_unit_retail := L_pack_unit_retail;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BUILD_PACK_RETAIL;
-----------------------------------------------------------------------------
FUNCTION GET_CURRENT_UNIT_RETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_current_price   IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 O_selling_uom     IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                                 I_location        IN       ITEM_LOC.LOC%TYPE,
                                 I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS


   L_regular_retail    ITEM_LOC.UNIT_RETAIL%TYPE    := NULL;
   L_promotion_retail  ITEM_LOC.UNIT_RETAIL%TYPE    := NULL;
   L_promotion_ind     BOOLEAN                      := NULL;
   L_current_retail    ITEM_LOC.UNIT_RETAIL%TYPE    := NULL;
   L_clearance_ind     BOOLEAN                      := NULL;
   L_loc_type          ITEM_LOC.LOC_TYPE%TYPE       := NULL;
   L_tran_date         PRICE_HIST.ACTION_DATE%TYPE  := NULL;
   L_tran_type         PRICE_HIST.TRAN_TYPE%TYPE    := NULL;
   L_pack_process_ind  BOOLEAN                      := NULL;
   L_class_vat_ind     BOOLEAN                      := NULL;
   L_vat_rate          NUMBER                       := NULL;

BEGIN
   ---
   if I_location is NULL or I_item is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_loc_type is NULL then
      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_loc_type,
                                      I_location) = FALSE then
         return FALSE;
      end if;
   else
      L_loc_type := I_loc_type;
   end if;
   ---

   if PM_RETAIL_API_SQL.GET_UNIT_RETAIL(O_error_message,
                                        L_regular_retail,
                                        L_promotion_retail,
                                        L_promotion_ind,
                                        L_current_retail,
                                        L_clearance_ind,
                                        I_item,
                                        L_loc_type,
                                        I_location,
                                        L_tran_date,
                                        L_tran_type,
                                        L_pack_process_ind,
                                        L_class_vat_ind,
                                        L_vat_rate) = FALSE then
      return FALSE;
   end if;
   ---

   if L_promotion_ind = TRUE then
      O_current_price := L_promotion_retail;
   else
      O_current_price := L_current_retail;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'PRICING_ATTRIB_SQL.GET_CURRENT_UNIT_RETAIL',
                                            to_char(SQLCODE));
      return FALSE;

END GET_CURRENT_UNIT_RETAIL;
----------------------------------------------------------------------------------------
FUNCTION GET_EXTERNAL_FINISHER_RETAIL(O_error_message            IN OUT VARCHAR2,
                                      O_unit_retail              IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                      I_item                     IN     ITEM_LOC.ITEM%TYPE,
                                      I_external_finisher        IN     PARTNER.PARTNER_ID%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(64) := 'PRICING_ATTRIB_SQL.GET_EXTERNAL_FINISHER_RETAIL';
   L_unit_retail               ITEM_LOC.UNIT_RETAIL%TYPE;
   L_unit_retail_pri           ITEM_LOC.UNIT_RETAIL%TYPE;
   L_unit_retail_zon           ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_zon          ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_pri   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;   
   L_selling_unit_retail_zon   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_zon           ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_zon           ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_pri     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_unit_retail_zon     ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_zon     ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_prim_currency             CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_out              CURRENCIES.CURRENCY_CODE%TYPE;
   L_item                      ITEM_MASTER%ROWTYPE;

   cursor C_ITEM_LOC_RETAIL is
      select i.finisher_av_retail
        from item_loc_soh i
       where i.item = I_item
         and i.loc  = I_external_finisher;

BEGIN

   if I_external_finisher is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_external_finisher',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ITEM_LOC_RETAIL',
                    'ITEM_LOC_SOH',
                    NULL);
   open C_ITEM_LOC_RETAIL;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITEM_LOC_RETAIL',
                    'ITEM_LOC_SOH',
                    NULL);
   fetch C_ITEM_LOC_RETAIL into L_unit_retail;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITEM_LOC_RETAIL',
                    'ITEM_LOC_SOH',
                    NULL);
   close C_ITEM_LOC_RETAIL;
   ---
   if L_unit_retail is NULL then
      ---
      if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                              L_prim_currency) then
      return FALSE;
      end if;
      ---   
      if GET_BASE_ZONE_RETAIL(O_error_message,
                              L_unit_retail_pri,
                              L_unit_retail_zon,
                              L_standard_uom_zon,
                              L_selling_unit_retail_pri,
                              L_selling_unit_retail_zon,
                              L_selling_uom_zon,
                              L_multi_units_zon,
                              L_multi_unit_retail_pri,
                              L_multi_unit_retail_zon,
                              L_multi_selling_uom_zon,
                              I_item) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                   I_external_finisher,
                                   'E',
                                   NULL,
                                   L_currency_out) = FALSE then
            return FALSE;
      end if;
      
      if CURRENCY_SQL.CONVERT(O_error_message,
                              L_unit_retail_pri,
                              L_prim_currency,
                              L_currency_out,
                              O_unit_retail,
                              'R',
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              'N') = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if O_unit_retail is NULL then
      O_unit_retail := L_unit_retail;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program,TO_CHAR(SQLCODE));
      return FALSE;
END GET_EXTERNAL_FINISHER_RETAIL;
----------------------------------------------------------------------------------------
FUNCTION GET_CURR_UNIT_RETAIL(O_error_message            IN OUT VARCHAR2,
                              O_standard_unit_retail     IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_standard_uom             IN OUT ITEM_MASTER.STANDARD_UOM%TYPE,
                              O_selling_unit_retail      IN OUT ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom              IN OUT ITEM_LOC.SELLING_UOM%TYPE,
                              I_item                     IN     ITEM_LOC.ITEM%TYPE)
return BOOLEAN IS
   L_program  VARCHAR2(50) := 'PRICING_ATTRIB_SQL.GET_CURR_UNIT_RETAIL';
   L_item_record              ITEM_MASTER%ROWTYPE;
   L_convert_retail           ITEM_MASTER.ORIGINAL_RETAIL%TYPE;
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
      return FALSE;
   end if;
   
   if L_item_record.CURR_SELLING_UOM <> L_item_record.STANDARD_UOM then
      if UOM_SQL.CONVERT(O_error_message,
                         L_convert_retail,
                         L_item_record.standard_uom,
                         1,
                         L_item_record.curr_selling_uom,
                         I_item,
                         NULL,
                         NULL,
                         NULL) = FALSE then
         return FALSE;
      else if L_convert_retail=0 then 
              O_error_message:=SQL_LIB.create_msg('NO_DIM_CONV', L_item_record.standard_uom,L_item_record.curr_selling_uom, NULL);
              L_item_record.curr_selling_unit_retail := 0;
           else
              L_item_record.curr_selling_unit_retail := L_item_record.curr_selling_unit_retail / L_convert_retail;
           end if;
      end if;
   else
      L_convert_retail := L_item_record.curr_selling_unit_retail;
   end if;

   O_standard_unit_retail := L_convert_retail;
   O_standard_uom := L_item_record.standard_uom;
   O_selling_unit_retail := L_item_record.curr_selling_unit_retail;
   O_selling_uom := L_item_record.curr_selling_uom;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_CURR_UNIT_RETAIL;
----------------------------------------------------------------------------------------
END PRICING_ATTRIB_SQL;
/
