SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY STORE_LIST_VALIDATE_SQL AS
   --
   -- this functions calls one of the other functions
   -- in this package, depending on the group indicator
   --



   FUNCTION GROUPS(IO_message    IN OUT VARCHAR2,
                   I_group_ind   IN     VARCHAR2,
                   I_value       IN     VARCHAR2,
                   IO_value_desc IN OUT VARCHAR2,
                   IO_exist_ind  IN OUT BOOLEAN)



            return BOOLEAN is

      QUICK_EXIT  EXCEPTION;
      L_program    VARCHAR2(60) := 'STORE_LIST_VALIDATE_SQL.GROUPS';

   BEGIN
      if I_group_ind = 'S' then
         if STORES(IO_message,
                   I_value,
                   IO_value_desc,
                   IO_exist_ind) = FALSE then
            raise QUICK_EXIT;
         end if;
      elsif I_group_ind = 'C' then
         if STORE_CLASS(IO_message,
                        I_value,
                        IO_value_desc,
                        IO_exist_ind) = FALSE then
            raise QUICK_EXIT;
         end if;
      elsif I_group_ind = 'D' then
         if DISTRICTS(IO_message,
                      I_value,
                      IO_value_desc,
                      IO_exist_ind) = FALSE then
            raise QUICK_EXIT;
         end if;
      elsif I_group_ind = 'R' then
         if REGIONS(IO_message,
                    I_value,
                    IO_value_desc,
                    IO_exist_ind) = FALSE then
            raise QUICK_EXIT;
         end if;
      elsif I_group_ind = 'A' then
         if AREA(IO_message,
                        I_value,
                        IO_value_desc,
                        IO_exist_ind) = FALSE then
            raise QUICK_EXIT;
         end if;
      elsif I_group_ind = 'T' then
         if TZONE(IO_message,
                  I_value,
                  IO_value_desc,
                  IO_exist_ind) = FALSE then
            raise QUICK_EXIT;
         end if;
      elsif I_group_ind = 'L' then
         if VAL_LOC_TRAITS(IO_message,
                           I_value,
                           IO_value_desc,
                           IO_exist_ind) = FALSE then
            raise QUICK_EXIT;
         end if;
      elsif I_group_ind = 'LLS' or I_group_ind = 'LLW' then
         if LOCLIST_ATTRIBUTE_SQL.GET_DESC(IO_message,
                                           IO_value_desc,
                                           to_number(I_value)) = FALSE then
            IO_exist_ind := FALSE;
            return FALSE;  -- took out raising of QUICK EXIT as the above package
                           -- returns FALSE if the loc_list # is invalid, unlike the other
                           -- package in this function which return the IO_exist_ind.
         else
            IO_exist_ind := TRUE;
         end if;
      elsif I_group_ind = 'W' then

         if WH_ATTRIB_SQL.GET_NAME(IO_message,
                                   to_number(I_value),
                                   IO_value_desc) = FALSE then
            IO_exist_ind := FALSE;
            return FALSE;
         else
            IO_exist_ind := TRUE;
         end if;
      end if;

      return TRUE;

   EXCEPTION
      when QUICK_EXIT then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                      IO_message,
                      L_program,
                      null);

         return FALSE;
      when VALUE_ERROR then
         IO_message := SQL_LIB.CREATE_MSG('ENTER_NUM',
                                          NULL,
                                          NULL,
                                          NULL);
         return FALSE;
      when OTHERS then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                      SQLERRM,
                      L_program,
                      null);

         return FALSE;
   END GROUPS;

---------------------------------------------------------------------
   FUNCTION STORES(IO_message      IN OUT VARCHAR2,
                   I_store         IN     VARCHAR2,
                   IO_store_name   IN OUT VARCHAR2,
                   IO_exist_ind    IN OUT BOOLEAN)
            return BOOLEAN is

      L_program     VARCHAR2(60) := 'STORE_LIST_VALIDATE_SQL.STORES';
      cursor C_STORE is
         select store_name
           from v_store_tl
          where to_char(store) = I_store;

   BEGIN
      open C_STORE;
      fetch C_STORE into IO_store_name;
      if C_STORE%NOTFOUND then
         IO_exist_ind := FALSE;
         IO_message := SQL_LIB.CREATE_MSG('INV_STORE', NULL, NULL, NULL);
      else
         IO_exist_ind := TRUE;
      end if;
      close C_STORE;

      return TRUE;

   EXCEPTION
      when OTHERS then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                      SQLERRM,
                      L_program,
                      null);

         return FALSE;

   END STORES;
---------------------------------------------------------------------
   FUNCTION AREA(IO_message      IN OUT VARCHAR2,
                   I_area         IN     VARCHAR2,
                   IO_area_name   IN OUT VARCHAR2,
                   IO_exist_ind    IN OUT BOOLEAN)
            return BOOLEAN is

      L_program     VARCHAR2(60) := 'STORE_LIST_VALIDATE_SQL.AREA';
      cursor C_AREA is
         select area_name
           from v_area_tl
          where to_char(area) = I_area;

   BEGIN
      open C_AREA;
      fetch C_AREA into IO_area_name;
      if C_AREA%NOTFOUND then
         IO_exist_ind := FALSE;
         IO_message := SQL_LIB.CREATE_MSG('INV_AREA', NULL, NULL, NULL);
      else
         IO_exist_ind := TRUE;
      end if;
      close C_AREA;

      return TRUE;

   EXCEPTION
      when OTHERS then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                      SQLERRM,
                      L_program,
                      null);

         return FALSE;

   END AREA;
----------------------------------------------------------------------
   FUNCTION STORE_CLASS(IO_message          IN OUT VARCHAR2,
                        I_store_class       IN     VARCHAR2,
                        IO_store_class_desc IN OUT VARCHAR2,
                        IO_exist_ind        IN OUT BOOLEAN)
            return BOOLEAN is
      L_dummy    VARCHAR2(1) := NULL;
      L_program   VARCHAR2(60) := 'STORE_LIST_VALIDATE_SQL.STORE_CLASS';

      cursor C_STORE_CLASS is
         select code_desc
           from v_code_detail_tl
          where code = I_store_class
            and code_type = 'CSTR';

   BEGIN
      open C_STORE_CLASS;
      fetch C_STORE_CLASS into IO_store_class_desc;
      if C_STORE_CLASS%NOTFOUND then
         IO_exist_ind := FALSE;
         IO_message := SQL_LIB.CREATE_MSG('INV_STORE_CLASS', NULL, NULL, NULL);
      else
         IO_exist_ind := TRUE;
      end if;
      close C_STORE_CLASS;

      return TRUE;

   EXCEPTION
      when OTHERS then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                      SQLERRM,
                      L_program,
                      null);
         return FALSE;

   END STORE_CLASS;

-----------------------------------------------------------------------
   FUNCTION DISTRICTS(IO_message      IN OUT VARCHAR2,
                     I_district      IN     VARCHAR2,
                     IO_description  IN OUT VARCHAR2,
                     IO_exist_ind    IN OUT BOOLEAN)
            return BOOLEAN is

     L_program      VARCHAR2(60) := 'STORE_LIST_VALIDATE_SQL.DISTRICTS';
      cursor C_DISTRICT is
         select district_name
           from v_district_tl
          where to_char(district) = I_district;

   BEGIN
      open C_DISTRICT;
      fetch C_DISTRICT into IO_description;
      if C_DISTRICT%NOTFOUND then
         IO_exist_ind := FALSE;
         IO_message := SQL_LIB.CREATE_MSG('INV_DISTRICT', NULL, NULL, NULL);
      else
         IO_exist_ind := TRUE;
      end if;
      close C_DISTRICT;

      return TRUE;

   EXCEPTION
      when OTHERS then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                    SQLERRM,
                    L_program,
                    null);
         return FALSE;

   END DISTRICTS;
-------------------------------------------------------------------------
   FUNCTION REGIONS(IO_message       IN OUT VARCHAR2,
                   I_region         IN     VARCHAR2,
                   IO_description   IN OUT VARCHAR2,
                   IO_exist_ind     IN OUT BOOLEAN)
            return BOOLEAN is

      L_program      VARCHAR2(60) := 'STORE_LIST_VALIDATE_SQL.REGIONS';
      cursor C_REGION is
         select region_name
           from v_region_tl
          where to_char(region) = I_region;

   BEGIN
      open C_REGION;
      fetch C_REGION into IO_description;
      if C_REGION%NOTFOUND then
         IO_exist_ind := FALSE;
         IO_message := SQL_LIB.CREATE_MSG('INV_REGION', NULL, NULL, NULL);
      else
         IO_exist_ind := TRUE;
      end if;
      close C_REGION;

      return TRUE;

   EXCEPTION
      when OTHERS then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                    SQLERRM,
                    L_program,
                    null);
         return FALSE;

   END REGIONS;
----------------------------------------------------------------------------
   FUNCTION TZONE(IO_message       IN OUT VARCHAR2,
                  I_tzone          IN     VARCHAR2,
                  IO_description   IN OUT VARCHAR2,
                  IO_exist_ind     IN OUT BOOLEAN)
            return BOOLEAN is

      L_program     VARCHAR2(60) := 'STORE_LIST_VALIDATE_SQL.TZONE';
      cursor C_TZONE is
         select description
           from v_tsfzone_tl
          where to_char(transfer_zone) = I_tzone;

   BEGIN
      open C_TZONE;
      fetch C_TZONE into IO_description;
      if C_TZONE%NOTFOUND then
         IO_exist_ind := FALSE;
         IO_message := SQL_LIB.CREATE_MSG('INV_TRAN_ZONE', NULL, NULL, NULL);
      else
         IO_exist_ind := TRUE;
      end if;
      close C_TZONE;

      return TRUE;

   EXCEPTION
      when OTHERS then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                    SQLERRM,
                    L_program,
                    null);
         return FALSE;

   END TZONE;
-----------------------------------------------------------------------------
   FUNCTION VAL_LOC_TRAITS(IO_message       IN OUT VARCHAR2,
                           I_trait          IN     VARCHAR2,
                           IO_description   IN OUT VARCHAR2,
                           IO_exist_ind     IN OUT BOOLEAN)
            return BOOLEAN is

      L_program     VARCHAR2(60)  := 'STORE_LIST_VALIDATE_SQL.VAL_LOC_TRAITS';
      cursor C_LOC_TRAITS is
         select description
           from v_loc_traits_tl
          where to_char(loc_trait) = I_trait;

   BEGIN
      open C_LOC_TRAITS;
      fetch C_LOC_TRAITS into IO_description;
      if C_LOC_TRAITS%NOTFOUND then
         IO_exist_ind := FALSE;
         IO_message := SQL_LIB.CREATE_MSG('INV_LOC_TRAIT', NULL, NULL, NULL);
      else
         IO_exist_ind := TRUE;
      end if;
      close C_LOC_TRAITS;

      return TRUE;

   EXCEPTION
      when OTHERS then
         IO_message := sql_lib.create_msg('PACKAGE_ERROR',
                    SQLERRM,
                    L_program,
                    null);
         return FALSE;

   END VAL_LOC_TRAITS;
----------------------------------------------------------------------------
FUNCTION LOCATIONS_EXIST(O_message     IN OUT VARCHAR2,
                         O_exists      IN OUT BOOLEAN,
                         I_group_ind   IN     VARCHAR2,
                         I_value       IN     NUMBER,
                         I_item        IN     ITEM_MASTER.ITEM%TYPE)
            return BOOLEAN is


   L_program         VARCHAR2(60) := 'STORE_LIST_VALIDATE_SQL.LOCATIONS_EXIST';
   L_dummy           NUMBER(1);
   QUICK_EXIT        EXCEPTION;

   cursor C_DISTRICT is
      select 1
        from store s
       where s.district = I_value
         and ROWNUM = 1;

   cursor C_REGION is
      select 1
        from store s,
             district d
       where s.district = d.district
         and d.region   = I_value
         and ROWNUM     = 1;

   cursor C_AREA is
      select 1
        from area a,
             store s,
             district d,
             region r
       where s.district = d.district
         and r.region   = d.region
         and r.area     = I_value
         and ROWNUM     = 1;

   cursor C_LOC_TRAITS is
      select 1
        from loc_traits_matrix
       where loc_traits_matrix.loc_trait = I_value
         and ROWNUM     = 1;

   cursor C_DEFAULT_WH is
      select 1
        from store s
       where s.default_wh =  I_value
         and ROWNUM     = 1;
   cursor C_ITEM_STORE is
      select 1
        from item_loc i
       where i.item = I_item
         and i.loc = I_value
         and i.loc_type = 'S'
         and ROWNUM     = 1;

   cursor C_ITEM_WH is
      select 1
        from wh w,
             item_loc i
       where i.item = I_item
         and w.wh = i.loc
         and w.wh = I_value
         and w.stockholding_ind = 'Y'
         and i.loc_type = 'W'
         and ROWNUM     = 1;
   cursor C_ITEM_FINISHER is
      select 1
        from wh w,
             item_loc i
       where i.item = I_item
         and w.wh = i.loc
         and w.wh = I_value
         and w.stockholding_ind = 'Y'
         and w.finisher_ind = 'Y'
         and i.loc_type = 'W'
         and ROWNUM     = 1;

BEGIN
   if I_group_ind = 'S' then
      open C_ITEM_STORE;
      fetch C_ITEM_STORE into L_dummy;
      if C_ITEM_STORE%NOTFOUND then
         close C_ITEM_STORE;
         O_message := SQL_LIB.CREATE_MSG('NO_ITEM_AT_LOC', NULL, NULL, NULL);
         raise QUICK_EXIT;
      end if;
      close C_ITEM_STORE;
   elsif I_group_ind = 'D' then
      open C_DISTRICT;
      fetch C_DISTRICT into L_dummy;
      if C_DISTRICT%NOTFOUND then
         close C_DISTRICT;
         O_message := SQL_LIB.CREATE_MSG('NO_LOCNS', NULL, NULL, NULL);
         raise QUICK_EXIT;
      end if;
      close C_DISTRICT;
   elsif I_group_ind = 'R' then
      open C_REGION;
      fetch C_REGION into L_dummy;
      if C_REGION%NOTFOUND then
         close C_REGION;
         O_message := SQL_LIB.CREATE_MSG('NO_LOCNS', NULL, NULL, NULL);
         raise QUICK_EXIT;
      end if;
      close C_REGION;
   elsif I_group_ind = 'A' then
      open C_AREA;
      fetch C_AREA into L_dummy;
      if C_AREA%NOTFOUND then
         close C_AREA;
         O_message := SQL_LIB.CREATE_MSG('NO_LOCNS', NULL, NULL, NULL);
         raise QUICK_EXIT;
      end if;
      close C_AREA;
   elsif I_group_ind = 'L' then
      open C_LOC_TRAITS;
      fetch C_LOC_TRAITS into L_dummy;
      if C_LOC_TRAITS%NOTFOUND then
         close C_LOC_TRAITS;
         O_message := SQL_LIB.CREATE_MSG('NO_LOCNS', NULL, NULL, NULL);
         raise QUICK_EXIT;
      end if;
      close C_LOC_TRAITS;
   elsif I_group_ind = 'W' then
      open C_ITEM_WH;
      fetch C_ITEM_WH into L_dummy;
      if C_ITEM_WH%NOTFOUND then
         close C_ITEM_WH;
         O_message := SQL_LIB.CREATE_MSG('NO_ITEM_AT_LOC', NULL, NULL, NULL);
         raise QUICK_EXIT;
      end if;
      close C_ITEM_WH;
   elsif I_group_ind = 'I' then
      open C_ITEM_FINISHER;
      fetch C_ITEM_FINISHER into L_dummy;
      if C_ITEM_FINISHER%NOTFOUND then
         close C_ITEM_FINISHER;
         O_message := SQL_LIB.CREATE_MSG('NO_ITEM_AT_LOC', NULL, NULL, NULL);
         raise QUICK_EXIT;
      end if;
      close C_ITEM_FINISHER;
   elsif I_group_ind = 'DW' then
      open C_DEFAULT_WH;
      fetch C_DEFAULT_WH into L_dummy;
      if C_DEFAULT_WH%NOTFOUND then
         close C_DEFAULT_WH;
         O_message := SQL_LIB.CREATE_MSG('NO_LOCNS', NULL, NULL, NULL);
         raise QUICK_EXIT;
      end if;
      close C_DEFAULT_WH;
   end if;
   ---
   return TRUE;
EXCEPTION
   when QUICK_EXIT then
      O_exists  := FALSE;
      return TRUE;
   when OTHERS then
      O_message := sql_lib.create_msg('PACKAGE_ERROR',
                                      SQLERRM,
                                      L_program,
                                      null);

         return FALSE;
END LOCATIONS_EXIST;

END;
/

