-- File Name : CORESVC_STORE_FORMAT_body.pls
CREATE OR REPLACE PACKAGE BODY CORESVC_STORE_FORMAT AS
   cursor C_SVC_STORE_FORMAT(I_process_id NUMBER,
                             I_chunk_id   NUMBER) is
      select pk_store_format.rowid       as pk_store_format_rid,
             pk_store_format.format_name as base_format_name,
             st.format_name,
             st.store_format,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)            as action,
             st.process$status
        from svc_store_format  st,
             store_format      pk_store_format,
             dual
       where st.process_id   = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.store_format = pk_store_format.store_format (+);

   TYPE errors_tab_typ is TABLE OF svc_admin_upld_er%ROWTYPE;

   LP_errors_tab errors_tab_typ;

   TYPE s9t_errors_tab_typ is TABLE OF S9T_ERRORS%ROWTYPE;

   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type STORE_FORMAT_TAB IS TABLE OF STORE_FORMAT_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
end GET_SHEET_NAME_TRANS;
----------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES( I_file_id NUMBER ) IS
   L_sheets              S9T_PKG.names_map_typ;
   STORE_FORMAT_cols     S9T_PKG.names_map_typ;
   STORE_FORMAT_TL_cols  S9T_PKG.names_map_typ;
BEGIN
   L_sheets                  := S9T_PKG.get_sheet_names(I_file_id);
   STORE_FORMAT_cols         := S9T_PKG.get_col_names(I_file_id,STORE_FORMAT_sheet);
   STORE_FORMAT$Action       := STORE_FORMAT_cols('ACTION');
   STORE_FORMAT$STORE_FORMAT := STORE_FORMAT_cols('STORE_FORMAT');
   STORE_FORMAT$FORMAT_NAME  := STORE_FORMAT_cols('FORMAT_NAME');

   STORE_FORMAT_TL_cols         := S9T_PKG.get_col_names(I_file_id,STORE_FORMAT_TL_SHEET);
   STORE_FORMAT_TL$Action       := STORE_FORMAT_TL_cols('ACTION');
   STORE_FORMAT_TL$LANG         := STORE_FORMAT_TL_cols('LANG');
   STORE_FORMAT_TL$STORE_FORMAT := STORE_FORMAT_TL_cols('STORE_FORMAT');
   STORE_FORMAT_TL$FORMAT_NAME  := STORE_FORMAT_TL_cols('FORMAT_NAME');
END POPULATE_NAMES;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_STORE_FORMAT( I_file_id IN NUMBER ) IS
BEGIN
   insert into TABLE
     (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id    = I_file_id
         and ss.sheet_name = STORE_FORMAT_sheet)
   select s9t_row(s9t_cells(CORESVC_STORE_FORMAT.action_mod,
                            STORE_FORMAT,
                            FORMAT_NAME))
     from STORE_FORMAT;
END POPULATE_STORE_FORMAT;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_STORE_FORMAT_TL( I_file_id IN NUMBER ) IS
BEGIN
   insert into TABLE
     (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id    = I_file_id
         and ss.sheet_name = STORE_FORMAT_TL_SHEET)
   select s9t_row(s9t_cells(CORESVC_STORE_FORMAT.action_mod,
                            LANG,
                            STORE_FORMAT,
                            FORMAT_NAME))
     from STORE_FORMAT_TL;
END POPULATE_STORE_FORMAT_TL;
-------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT NUMBER) IS
   L_file        s9t_file;
   L_file_name   s9t_folder.file_name%type;
BEGIN
   L_file              := NEW S9T_FILE();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;


   L_file.user_lang :=GET_USER_LANG;

   L_file.add_sheet(STORE_FORMAT_sheet);
   L_file.sheets(L_file.get_sheet_index(STORE_FORMAT_sheet)).column_headers := S9T_CELLS('ACTION',
                                                                                         'STORE_FORMAT',
                                                                                         'FORMAT_NAME');
   s9t_pkg.save_obj(L_file);

   L_file.add_sheet(STORE_FORMAT_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(STORE_FORMAT_TL_SHEET)).column_headers := S9T_CELLS('ACTION',
                                                                                            'LANG',
                                                                                            'STORE_FORMAT',
                                                                                            'FORMAT_NAME');
   s9t_pkg.save_obj(L_file);
END INIT_S9T;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message      IN OUT  rtk_errors.rtk_text%type,
                    O_file_id            IN OUT  s9t_folder.file_id%type,
                    I_template_only_ind  IN      CHAR                     DEFAULT 'N')
RETURN BOOLEAN IS
   L_file      s9t_file;
   L_program   VARCHAR2(255):='CORESVC_STORE_FORMAT.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_STORE_FORMAT(O_file_id);
      POPULATE_STORE_FORMAT_TL(O_file_id);
      Commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
  when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
     return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_STORE_FORMAT( I_file_id    IN s9t_folder.file_id%TYPE,
                                    I_process_id IN SVC_STORE_FORMAT.process_id%TYPE ) IS
   TYPE svc_STORE_FORMAT_col_typ is TABLE OF SVC_STORE_FORMAT%ROWTYPE;

   L_error                 BOOLEAN                  := FALSE;
   L_temp_rec              SVC_STORE_FORMAT%ROWTYPE;
   L_default_rec           SVC_STORE_FORMAT%ROWTYPE;
   L_process_id            SVC_STORE_FORMAT.process_id%TYPE;
   svc_store_format_col    SVC_STORE_FORMAT_COL_TYP := NEW SVC_STORE_FORMAT_COL_TYP();

   cursor c_mandatory_ind is
      select format_name_mi,
             store_format_mi,
             1               as   dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = CORESVC_STORE_FORMAT.template_key
                 and wksht_key      = 'STORE_FORMAT')
               PIVOT (MAX(mandatory)  as mi FOR (column_key) IN (
                       'FORMAT_NAME'  as FORMAT_NAME,
                       'STORE_FORMAT' as STORE_FORMAT,
                                null  as dummy));
   L_mi_rec c_mandatory_ind%rowtype;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_STORE_FORMAT';
   L_pk_columns    VARCHAR2(255)  := 'Number';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   FOR rec in
   (select format_name_dv,
           store_format_dv,
           null            as dummy
      from (select column_key,
                   default_value
              from s9t_tmpl_cols_def
             where template_key  = CORESVC_STORE_FORMAT.template_key
               and wksht_key                      = 'STORE_FORMAT')
            PIVOT (MAX(default_value) as dv FOR (column_key) in ('FORMAT_NAME'  as FORMAT_NAME,
                                                                 'STORE_FORMAT' as STORE_FORMAT,
                                                                  NULL          as dummy))) LOOP
   BEGIN
      L_default_rec.FORMAT_NAME  := rec.FORMAT_NAME_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'STORE_FORMAT',
                          NULL,
                         'FORMAT_NAME',
                          NULL,
                         'INV_DEFAULT');
   END;

   BEGIN
   L_default_rec.STORE_FORMAT    := rec.STORE_FORMAT_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'STORE_FORMAT',
                          NULL,
                         'STORE_FORMAT',
                          NULL,
                         'INV_DEFAULT');
   END;

   END LOOP;

   open c_mandatory_ind;
   fetch c_mandatory_ind
   into l_mi_rec;
   close c_mandatory_ind;
   FOR rec IN
    (select r.get_cell(STORE_FORMAT$Action)        as ACTION,
            r.get_cell(STORE_FORMAT$FORMAT_NAME)   as FORMAT_NAME,
            r.get_cell(STORE_FORMAT$STORE_FORMAT)  as STORE_FORMAT,
            r.get_row_seq()                        as row_seq
       from s9t_folder sf,
            TABLE(sf.s9t_file_obj.sheets) ss,
            TABLE(ss.s9t_rows) r
      where sf.file_id              = I_file_id
        and ss.sheet_name           = sheet_name_trans(STORE_FORMAT_sheet)) LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          STORE_FORMAT_sheet,
                          rec.row_seq,
                          action_column,
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.FORMAT_NAME := rec.FORMAT_NAME;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          STORE_FORMAT_sheet,
                          rec.row_seq,
                         'FORMAT_NAME',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.STORE_FORMAT := rec.STORE_FORMAT;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          STORE_FORMAT_sheet,
                          rec.row_seq,
                         'STORE_FORMAT',
                          SQLCODE,
                          SQLERRM);
         L_error := TRUE;
   END;

   if rec.action = CORESVC_STORE_FORMAT.action_new then
      L_temp_rec.FORMAT_NAME  := NVL(L_temp_rec.FORMAT_NAME,
                                     L_default_rec.FORMAT_NAME);
      L_temp_rec.STORE_FORMAT := NVL(L_temp_rec.STORE_FORMAT,
                                     L_default_rec.STORE_FORMAT);
   end if;

   If NOT ( L_temp_rec.STORE_FORMAT is not null and 1 = 1 ) then
      WRITE_S9T_ERROR( I_file_id,
                       STORE_FORMAT_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
      L_error := TRUE;
   end if;

   if NOT L_error then
      svc_STORE_FORMAT_col.extend();
      svc_STORE_FORMAT_col(svc_STORE_FORMAT_col.COUNT()):=L_temp_rec;
   end if;

   END LOOP;

   BEGIN
   forall i IN 1..svc_STORE_FORMAT_col.count SAVE EXCEPTIONS
   merge INTO SVC_STORE_FORMAT st
   using (select (case
                     when l_mi_rec.format_name_mi           = 'N' and
                          svc_store_format_col(i).action    = coresvc_item.action_mod and
                          s1.format_name   is NULL then
                          mt.format_name
                     else s1.format_name
                     end) as format_name,
                 (case
                     when l_mi_rec.store_format_mi          = 'N' and
                          svc_store_format_col(i).action    = coresvc_item.action_mod and
                          s1.store_format  is null then
                          mt.store_format
                     else s1.store_format
                     end) as store_format,
                     null as dummy
            from (select svc_store_format_col(i).format_name  as format_name,
                         svc_store_format_col(i).store_format as store_format,
                         null                                 as dummy
                    from dual) s1, store_format mt
                   where mt.store_format (+) = s1.store_format
                     and                   1 = 1 ) sq
      ON (       st.store_format = sq.store_format and
                 svc_STORE_FORMAT_col(i).action in (CORESVC_STORE_FORMAT.action_mod,CORESVC_STORE_FORMAT.action_del))

      when matched then
         update
            set process_id        = svc_store_format_col(i).process_id ,
                chunk_id          = svc_store_format_col(i).chunk_id ,
                row_seq           = svc_store_format_col(i).row_seq ,
                action            = svc_STORE_FORMAT_col(i).ACTION,
                process$status    = svc_store_format_col(i).process$status ,
                format_name       = sq.format_name ,
                create_id         = svc_store_format_col(i).create_id ,
                create_datetime   = svc_store_format_col(i).create_datetime ,
                last_upd_id       = svc_store_format_col(i).last_upd_id ,
                last_upd_datetime = svc_store_format_col(i).last_upd_datetime
      when not matched then
         insert( process_id ,
                 chunk_id ,
                 row_seq ,
                 action ,
                 process$status ,
                 format_name ,
                 store_format ,
                 create_id ,
                 create_datetime ,
                 last_upd_id ,
                 last_upd_datetime )
         values( svc_STORE_FORMAT_col(i).PROCESS_ID ,
                 svc_STORE_FORMAT_col(i).CHUNK_ID ,
                 svc_STORE_FORMAT_col(i).ROW_SEQ ,
                 svc_STORE_FORMAT_col(i).ACTION ,
                 svc_STORE_FORMAT_col(i).PROCESS$STATUS ,
                 sq.FORMAT_NAME ,
                 sq.STORE_FORMAT ,
                 svc_STORE_FORMAT_col(i).CREATE_ID ,
                 svc_STORE_FORMAT_col(i).CREATE_DATETIME ,
                 svc_STORE_FORMAT_col(i).LAST_UPD_ID ,
                 svc_STORE_FORMAT_col(i).LAST_UPD_DATETIME );
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
         WRITE_S9T_ERROR( I_file_id,
                          STORE_FORMAT_sheet,
                          svc_STORE_FORMAT_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
   END;
END PROCESS_S9T_STORE_FORMAT;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_STORE_FORMAT_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id   IN   SVC_STORE_FORMAT_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_STORE_FORMAT_TL_COL_TYP IS TABLE OF SVC_STORE_FORMAT_TL%ROWTYPE;
   L_temp_rec                SVC_STORE_FORMAT_TL%ROWTYPE;
   SVC_STORE_FORMAT_TL_COL   SVC_STORE_FORMAT_TL_COL_TYP := NEW SVC_STORE_FORMAT_TL_COL_TYP();
   L_process_id              SVC_STORE_FORMAT_TL.PROCESS_ID%TYPE;
   L_error                   BOOLEAN := FALSE;
   L_default_rec             SVC_STORE_FORMAT_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select format_name_mi,
             store_format_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'STORE_FORMAT_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('FORMAT_NAME'  as format_name,
                                       'STORE_FORMAT' as store_format,
                                       'LANG'         as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_STORE_FORMAT_TL';
   L_pk_columns    VARCHAR2(255)  := 'Store Format, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select format_name_dv,
                      store_format_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'STORE_FORMAT_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('FORMAT_NAME'   as format_name,
                                                'STORE_FORMAT'  as store_format,
                                                'LANG'          as lang)))
   LOOP
      BEGIN
         L_default_rec.format_name := rec.format_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            STORE_FORMAT_TL_SHEET ,
                            NULL,
                           'FORMAT_NAME' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.store_format := rec.store_format_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           STORE_FORMAT_TL_SHEET ,
                            NULL,
                           'STORE_FORMAT' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            STORE_FORMAT_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(store_format_tl$action))        as action,
                      r.get_cell(store_format_tl$format_name)          as format_name,
                      r.get_cell(store_format_tl$store_format)         as store_format,
                      r.get_cell(store_format_tl$lang)                 as lang,
                      r.get_row_seq()                                  as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(STORE_FORMAT_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            store_format_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.format_name := rec.format_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            STORE_FORMAT_TL_SHEET,
                            rec.row_seq,
                            'FORMAT_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.store_format := rec.store_format;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            STORE_FORMAT_TL_SHEET,
                            rec.row_seq,
                            'STORE_FORMAT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            STORE_FORMAT_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_store_format.action_new then
         L_temp_rec.format_name  := NVL( L_temp_rec.format_name,L_default_rec.format_name);
         L_temp_rec.store_format := NVL( L_temp_rec.store_format,L_default_rec.store_format);
         L_temp_rec.lang         := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.store_format is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         STORE_FORMAT_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_STORE_FORMAT_TL_col.extend();
         SVC_STORE_FORMAT_TL_col(SVC_STORE_FORMAT_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_STORE_FORMAT_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_STORE_FORMAT_TL st
      using(select
                  (case
                   when l_mi_rec.format_name_mi = 'N'
                    and SVC_STORE_FORMAT_TL_col(i).action = coresvc_store_format.action_mod
                    and s1.format_name IS NULL then
                        mt.format_name
                   else s1.format_name
                   end) as format_name,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_STORE_FORMAT_TL_col(i).action = coresvc_store_format.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.store_format_mi = 'N'
                    and SVC_STORE_FORMAT_TL_col(i).action = coresvc_store_format.action_mod
                    and s1.store_format IS NULL then
                        mt.store_format
                   else s1.store_format
                   end) as store_format
              from (select SVC_STORE_FORMAT_TL_col(i).format_name  as format_name,
                           SVC_STORE_FORMAT_TL_col(i).store_format as store_format,
                           SVC_STORE_FORMAT_TL_col(i).lang         as lang
                      from dual) s1,
                   store_format_tl mt
             where mt.store_format (+) = s1.store_format
               and mt.lang (+)       = s1.lang) sq
                on (st.store_format = sq.store_format and
                    st.lang = sq.lang and
                    SVC_STORE_FORMAT_TL_col(i).ACTION IN (CORESVC_store_format.action_mod,CORESVC_store_format.action_del))
      when matched then
      update
         set process_id        = SVC_STORE_FORMAT_TL_col(i).process_id ,
             chunk_id          = SVC_STORE_FORMAT_TL_col(i).chunk_id ,
             row_seq           = SVC_STORE_FORMAT_TL_col(i).row_seq ,
             action            = SVC_STORE_FORMAT_TL_col(i).action ,
             process$status    = SVC_STORE_FORMAT_TL_col(i).process$status ,
             format_name = sq.format_name
      when not matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             format_name ,
             store_format ,
             lang)
      values(SVC_STORE_FORMAT_TL_col(i).process_id ,
             SVC_STORE_FORMAT_TL_col(i).chunk_id ,
             SVC_STORE_FORMAT_TL_col(i).row_seq ,
             SVC_STORE_FORMAT_TL_col(i).action ,
             SVC_STORE_FORMAT_TL_col(i).process$status ,
             sq.format_name ,
             sq.store_format ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            STORE_FORMAT_TL_SHEET,
                            SVC_STORE_FORMAT_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_STORE_FORMAT_TL;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T (O_error_message IN OUT  rtk_errors.rtk_text%type ,
                      O_error_count   IN OUT  NUMBER,
                      I_file_id       IN      s9t_folder.file_id%type,
                      I_process_id    IN      NUMBER )
RETURN BOOLEAN IS
   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR          EXCEPTION;
   PRAGMA            EXCEPTION_INIT(MAX_CHAR, -01706);

   L_program         VARCHAR2(255) := 'CORESVC_STORE_FORMAT.PROCESS_S9T';
   L_file            s9t_file;
   L_sheets          S9T_PKG.names_map_typ;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
BEGIN
   commit;
   s9t_pkg.ods2obj(I_file_id);
   commit;

   L_file := s9t_pkg.get_obj(I_file_id);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE   then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = FALSE   then
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.sheet_trans(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_STORE_FORMAT(I_file_id,
                               I_process_id);
      PROCESS_S9T_STORE_FORMAT_TL(I_file_id,
                                  I_process_id);
   end if;

   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
   insert into s9t_errors
        values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   commit;

   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values lp_s9t_errors_tab(i);
         update svc_process_tracker
            set status  = 'PE',
                file_id = I_file_id
          where process_id = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_STORE_FORMAT_INS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_ins_tab        IN      STORE_FORMAT%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_STORE_FORMAT.EXEC_STORE_FORMAT_INS';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_STORE_FORMAT';
BEGIN

     insert into STORE_FORMAT
          values I_ins_tab;
   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_STORE_FORMAT_INS;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_STORE_FORMAT_UPD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_upd_tab        IN      STORE_FORMAT%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_STORE_FORMAT.EXEC_STORE_FORMAT_UPD';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_STORE_FORMAT';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_STORE_FORMAT_LOCK is
      select 'X'
        from STORE_FORMAT
       where STORE_FORMAT = I_upd_tab.store_format
       for update nowait;
BEGIN

   open C_STORE_FORMAT_LOCK;
   close C_STORE_FORMAT_LOCK;

      update STORE_FORMAT
         set row          = I_upd_tab
       where STORE_FORMAT = I_upd_tab.store_format;


   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'STORE_FORMAT',
                                                                I_upd_tab.store_format,
                                                                NULL);

      return FALSE;

   when OTHERS then
      if C_STORE_FORMAT_LOCK%ISOPEN then
         close C_STORE_FORMAT_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      close C_STORE_FORMAT_LOCK;
      return FALSE;
END EXEC_STORE_FORMAT_UPD;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_STORE_FORMAT_DEL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_del_tab       IN     STORE_FORMAT%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_STORE_FORMAT.EXEC_STORE_FORMAT_DEL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_STORE_FORMAT';

   cursor C_STORE_FORMAT_LOCK is
      select 'X'
        from store_format
       where STORE_FORMAT = I_del_tab.store_format
       for update nowait;

   cursor C_STORE_FORMAT_TL_LOCK is
      select 'X'
        from store_format_tl
       where store_format = I_del_tab.store_format
       for update nowait;
BEGIN
   open C_STORE_FORMAT_TL_LOCK;
   close C_STORE_FORMAT_TL_LOCK;

   delete from store_format_tl
    where store_format = i_del_tab.store_format;

   open C_STORE_FORMAT_LOCK;
   close C_STORE_FORMAT_LOCK;

   delete from store_format
    where store_format = i_del_tab.store_format;


   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'STORE_FORMAT',
                                                                I_del_tab.store_format,
                                                                NULL);

      return FALSE;

   when OTHERS then
      if C_STORE_FORMAT_LOCK%ISOPEN then
         close C_STORE_FORMAT_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      close C_STORE_FORMAT_LOCK;
      return FALSE;
END EXEC_STORE_FORMAT_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_STORE_FORMAT_TL_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_store_format_tl_ins_tab    IN       STORE_FORMAT_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_STORE_FORMAT.EXEC_STORE_FORMAT_TL_INS';

BEGIN
   if I_store_format_tl_ins_tab is NOT NULL and I_store_format_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_store_format_tl_ins_tab.COUNT()
         insert into store_format_tl
              values I_store_format_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_STORE_FORMAT_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_STORE_FORMAT_TL_UPD(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store_format_tl_upd_tab   IN       STORE_FORMAT_TAB,
                                  I_stfmt_tl_upd_rst          IN       ROW_SEQ_TAB,
                                  I_process_id                IN       SVC_STORE_FORMAT_TL.PROCESS_ID%TYPE,
                                  I_chunk_id                  IN       SVC_STORE_FORMAT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_STORE_FORMAT.EXEC_STORE_FORMAT_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'STORE_FORMAT_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_STORE_FORMAT_TL_UPD(I_store_format  STORE_FORMAT_TL.store_format%TYPE,
                                     I_lang          STORE_FORMAT_TL.LANG%TYPE) is
      select 'x'
        from store_format_tl
       where store_format = I_store_format
         and lang = I_lang
         for update nowait;

BEGIN
   if I_store_format_tl_upd_tab is NOT NULL and I_store_format_tl_upd_tab.count > 0 then
      for i in I_store_format_tl_upd_tab.FIRST..I_store_format_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_store_format_tl_upd_tab(i).lang);
            L_key_val2 := 'Store Format: '||to_char(I_store_format_tl_upd_tab(i).store_format);
            open C_LOCK_STORE_FORMAT_TL_UPD(I_store_format_tl_upd_tab(i).store_format,
                                            I_store_format_tl_upd_tab(i).lang);
            close C_LOCK_STORE_FORMAT_TL_UPD;

            update store_format_tl
               set format_name = I_store_format_tl_upd_tab(i).format_name,
                   last_update_id = I_store_format_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_store_format_tl_upd_tab(i).last_update_datetime
             where lang = I_store_format_tl_upd_tab(i).lang
               and store_format = I_store_format_tl_upd_tab(i).store_format;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_STORE_FORMAT_TL',
                           I_stfmt_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_STORE_FORMAT_TL_UPD%ISOPEN then
         close C_LOCK_STORE_FORMAT_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_STORE_FORMAT_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_STORE_FORMAT_TL_DEL(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store_format_tl_del_tab   IN       STORE_FORMAT_TAB,
                                  I_stfmt_tl_del_rst          IN       ROW_SEQ_TAB,
                                  I_process_id                IN       SVC_STORE_FORMAT_TL.PROCESS_ID%TYPE,
                                  I_chunk_id                  IN       SVC_STORE_FORMAT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_STORE_FORMAT.EXEC_STORE_FORMAT_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'STORE_FORMAT_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_STORE_FORMAT_TL_DEL(I_store_format  STORE_FORMAT_TL.STORE_FORMAT%TYPE,
                                     I_lang          STORE_FORMAT_TL.LANG%TYPE) is
      select 'x'
        from store_format_tl
       where store_format = I_store_format
         and lang = I_lang
         for update nowait;

BEGIN
   if I_store_format_tl_del_tab is NOT NULL and I_store_format_tl_del_tab.count > 0 then
      for i in I_store_format_tl_del_tab.FIRST..I_store_format_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_store_format_tl_del_tab(i).lang);
            L_key_val2 := 'Store Format: '||to_char(I_store_format_tl_del_tab(i).store_format);
            open C_LOCK_STORE_FORMAT_TL_DEL(I_store_format_tl_del_tab(i).store_format,
                                            I_store_format_tl_del_tab(i).lang);
            close C_LOCK_STORE_FORMAT_TL_DEL;
            
            delete store_format_tl
             where lang = I_store_format_tl_del_tab(i).lang
               and store_format = I_store_format_tl_del_tab(i).store_format;
            
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_STORE_FORMAT_TL',
                           I_stfmt_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_STORE_FORMAT_TL_DEL%ISOPEN then
         close C_LOCK_STORE_FORMAT_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_STORE_FORMAT_TL_DEL;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_STORE_FORMAT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error          IN OUT  BOOLEAN,
                                  I_rec            IN      C_SVC_STORE_FORMAT%ROWTYPE )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                      :='CORESVC_STORE_FORMAT.PROCESS_VAL_STORE_FORMAT';
   L_temp            VARCHAR2(1)                       := NULL;
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_STORE_FORMAT';

   cursor C_CHECK_STORE is
      select 'x'
        from store
       where store_format = I_rec.store_format;

   cursor C_CHECK_STORE_ADD is
      select 'x'
        from store_add
       where store_format = I_rec.store_format;

BEGIN
   if I_rec.action = action_del
      and I_rec.STORE_FORMAT  is NOT NULL
      and I_rec.PK_STORE_FORMAT_rid is NOT NULL   then

      open  C_CHECK_STORE;
      fetch C_CHECK_STORE into L_temp;
      close C_CHECK_STORE;

      if L_temp is NOT NULL then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'STORE_FORMAT',
                     'STR_FRMT_ASSO_NO_DEL');
         O_error :=TRUE;
      end if;

      L_temp := NULL;

      open  C_CHECK_STORE_ADD;
      fetch C_CHECK_STORE_ADD into L_temp;
      close C_CHECK_STORE_ADD;

      if L_temp is NOT NULL then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'STORE_FORMAT',
                     'STR_FMT_ASSO_PEND_NO_DEL');
         O_error :=TRUE;
      end if;

   end if;

   if ((I_rec.action = action_new
      and I_rec.STORE_FORMAT is NOT NULL
      and I_rec.PK_STORE_FORMAT_rid is NULL )
      or (I_rec.action = action_mod
      and I_rec.STORE_FORMAT is NOT NULL
      and I_rec.PK_STORE_FORMAT_rid is NOT NULL )) then

          if I_rec.store_format <= 0   then
             WRITE_ERROR( I_rec.process_id,
                          svc_admin_upld_er_seq.NEXTVAL,
                          I_rec.chunk_id,
                          L_table,
                          I_rec.row_seq,
                         'STORE_FORMAT',
                         'STR_FORMAT_GREAT_0');
             O_error :=TRUE;
          elsif I_rec.store_format > 9999   then
                WRITE_ERROR( I_rec.process_id,
                             svc_admin_upld_er_seq.NEXTVAL,
                             I_rec.chunk_id,
                             L_table,
                             I_rec.row_seq,
                            'STORE_FORMAT',
                            'ISC_BETWEEN_ONE_9999');
                O_error :=TRUE;
          end if;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_STORE_FORMAT;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_STORE_FORMAT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id     IN      SVC_STORE_FORMAT.PROCESS_ID%TYPE,
                              I_chunk_id       IN      SVC_STORE_FORMAT.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                      :='CORESVC_STORE_FORMAT.PROCESS_STORE_FORMAT';
   L_error                  BOOLEAN;
   L_store_format_temp_rec  STORE_FORMAT%ROWTYPE;
   L_process_error          BOOLEAN                           := FALSE;
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_STORE_FORMAT';
BEGIN
   FOR rec IN C_SVC_STORE_FORMAT(I_process_id,I_chunk_id)
   LOOP
      L_error := False;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del)   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_STORE_FORMAT_rid is NOT NULL   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      'STORE_FORMAT',
                     'DUP_STORE_FORMAT');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.store_format  is NOT NULL
         and rec.PK_STORE_FORMAT_rid is NULL   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'STORE_FORMAT',
                     'NO_RECORD');
         L_error :=TRUE;
      end if;

      if NOT( rec.store_format is NOT NULL )   then
              WRITE_ERROR( I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                          'STORE_FORMAT',
                          'ENTER_NUM');
              L_error :=TRUE;
      end if;

      if (rec.action = action_new
         or (rec.action = action_mod
         and NVL(rec.base_format_name,'-2') <> NVL(rec.format_name,'-1') ))   then
             if NOT( rec.FORMAT_NAME IS NOT NULL )   then
                     WRITE_ERROR( I_process_id,
                                  svc_admin_upld_er_seq.NEXTVAL,
                                  I_chunk_id,
                                  L_table,
                                  rec.row_seq,
                                 'FORMAT_NAME',
                                 'ENTER_DESC');
                     L_error :=TRUE;
             end if;
      end if;

      if PROCESS_VAL_STORE_FORMAT(O_error_message,
                                  L_error,
                                  rec ) = FALSE   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_store_format_temp_rec.format_name     := rec.format_name;
         L_store_format_temp_rec.store_format    := rec.store_format;
         L_store_format_temp_rec.create_id       := GET_USER;
         L_store_format_temp_rec.create_datetime := SYSDATE;

         if(rec.action = action_new ) then
            if(EXEC_STORE_FORMAT_INS(O_error_message,
                                     L_store_format_temp_rec ) = FALSE )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if(rec.action = action_mod ) then
            if(EXEC_STORE_FORMAT_UPD(O_error_message,
                                     L_store_format_temp_rec ) = FALSE )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if(rec.action = action_del ) then
            if(EXEC_STORE_FORMAT_DEL(O_error_message,
                                     L_store_format_temp_rec ) = FALSE )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;

   END LOOP;

return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_STORE_FORMAT;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_STORE_FORMAT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN       SVC_STORE_FORMAT_TL.PROCESS_ID%TYPE,
                                 I_chunk_id        IN       SVC_STORE_FORMAT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                     VARCHAR2(64) := 'CORESVC_STORE_FORMAT.PROCESS_STORE_FORMAT_TL';
   L_error_message               RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'STORE_FORMAT_TL';
   L_base_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'STORE_FORMAT';
   L_table                       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_STORE_FORMAT_TL';
   L_error                       BOOLEAN := FALSE;
   L_process_error               BOOLEAN := FALSE;
   L_store_format_TL_temp_rec    STORE_FORMAT_TL%ROWTYPE;
   L_stfmt_tl_upd_rst            ROW_SEQ_TAB;
   L_stfmt_tl_del_rst            ROW_SEQ_TAB;

   cursor C_SVC_STORE_FORMAT_TL(I_process_id NUMBER,
                                I_chunk_id   NUMBER) is
      select pk_store_format_tl.rowid  as pk_store_format_tl_rid,
             fk_store_format.rowid     as fk_store_format_rid,
             fk_lang.rowid             as fk_lang_rid,
             st.lang,
             st.store_format,
             st.format_name,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_store_format_tl  st,
             store_format         fk_store_format,
             store_format_tl      pk_store_format_tl,
             lang                 fk_lang
       where st.process_id   =  I_process_id
         and st.chunk_id     =  I_chunk_id
         and st.store_format =  fk_store_format.store_format (+)
         and st.lang         =  pk_store_format_tl.lang (+)
         and st.store_format =  pk_store_format_tl.store_format (+)
         and st.lang         =  fk_lang.lang (+);

   TYPE SVC_STORE_FORMAT_TL is TABLE OF C_SVC_STORE_FORMAT_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_store_format_tab        SVC_STORE_FORMAT_TL;

   L_store_format_TL_ins_tab         STORE_FORMAT_TAB         := NEW STORE_FORMAT_TAB();
   L_store_format_TL_upd_tab         STORE_FORMAT_TAB         := NEW STORE_FORMAT_TAB();
   L_store_format_TL_del_tab         STORE_FORMAT_TAB         := NEW STORE_FORMAT_TAB();

BEGIN
   if C_SVC_STORE_FORMAT_TL%ISOPEN then
      close C_SVC_STORE_FORMAT_TL;
   end if;

   open C_SVC_STORE_FORMAT_TL(I_process_id,
                              I_chunk_id);
   LOOP
      fetch C_SVC_STORE_FORMAT_TL bulk collect into L_svc_store_format_tab limit LP_bulk_fetch_limit;
      if L_svc_store_format_tab.COUNT > 0 then
         FOR i in L_svc_store_format_tab.FIRST..L_svc_store_format_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_store_format_tab(i).action is NULL
               or L_svc_store_format_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_store_format_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_store_format_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_store_format_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_store_format_tab(i).action = action_new
               and L_svc_store_format_tab(i).pk_store_format_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_store_format_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_store_format_tab(i).action IN (action_mod, action_del)
               and L_svc_store_format_tab(i).lang is NOT NULL
               and L_svc_store_format_tab(i).store_format is NOT NULL
               and L_svc_store_format_tab(i).pk_store_format_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_store_format_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_store_format_tab(i).action = action_new
               and L_svc_store_format_tab(i).store_format is NOT NULL
               and L_svc_store_format_tab(i).fk_store_format_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_store_format_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_store_format_tab(i).action = action_new
               and L_svc_store_format_tab(i).lang is NOT NULL
               and L_svc_store_format_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_store_format_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_store_format_tab(i).action in (action_new, action_mod) then
               if L_svc_store_format_tab(i).format_name is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_store_format_tab(i).row_seq,
                              'FORMAT_NAME',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_store_format_tab(i).store_format is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_store_format_tab(i).row_seq,
                           'STORE_FORMAT',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_store_format_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_store_format_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_store_format_TL_temp_rec.lang := L_svc_store_format_tab(i).lang;
               L_store_format_TL_temp_rec.store_format := L_svc_store_format_tab(i).store_format;
               L_store_format_TL_temp_rec.format_name := L_svc_store_format_tab(i).format_name;
               L_store_format_TL_temp_rec.create_datetime := SYSDATE;
               L_store_format_TL_temp_rec.create_id := GET_USER;
               L_store_format_TL_temp_rec.last_update_datetime := SYSDATE;
               L_store_format_TL_temp_rec.last_update_id := GET_USER;

               if L_svc_store_format_tab(i).action = action_new then
                  L_store_format_TL_ins_tab.extend;
                  L_store_format_TL_ins_tab(L_store_format_TL_ins_tab.count()) := L_store_format_TL_temp_rec;
               end if;

               if L_svc_store_format_tab(i).action = action_mod then
                  L_store_format_TL_upd_tab.extend;
                  L_store_format_TL_upd_tab(L_store_format_TL_upd_tab.count()) := L_store_format_TL_temp_rec;
                  L_stfmt_tl_upd_rst(L_store_format_TL_upd_tab.count()) := L_svc_store_format_tab(i).row_seq;
               end if;

               if L_svc_store_format_tab(i).action = action_del then
                  L_store_format_TL_del_tab.extend;
                  L_store_format_TL_del_tab(L_store_format_TL_del_tab.count()) := L_store_format_TL_temp_rec;
                  L_stfmt_tl_del_rst(L_store_format_TL_del_tab.count()) := L_svc_store_format_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_STORE_FORMAT_TL%NOTFOUND;
   END LOOP;
   close C_SVC_STORE_FORMAT_TL;

   if EXEC_STORE_FORMAT_TL_INS(O_error_message,
                               L_store_format_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_STORE_FORMAT_TL_UPD(O_error_message,
                               L_store_format_TL_upd_tab,
                               L_stfmt_tl_upd_rst,
                               I_process_id,
                               I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_STORE_FORMAT_TL_DEL(O_error_message,
                               L_store_format_TL_del_tab,
                               L_stfmt_tl_del_rst,
                               I_process_id,
                               I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_STORE_FORMAT_TL;
----------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_STORE_FORMAT.PROCESS_ID%TYPE) IS

BEGIN
   delete
     from svc_store_format_tl
    where process_id= I_process_id;

   delete
     from svc_store_format
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;
------------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count   IN OUT  NUMBER,
                 I_process_id    IN      NUMBER,
                 I_chunk_id      IN      NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64):='CORESVC_STORE_FORMAT.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';


BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_STORE_FORMAT(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_STORE_FORMAT_TL(O_error_message,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_STORE_FORMAT;
------------------------------------------------------------------------------------------------------
/
