CREATE OR REPLACE PACKAGE CORESVC_ITEM AUTHID CURRENT_USER AS
   action_new   VARCHAR2(25)   := 'NEW';
   action_mod   VARCHAR2(25)   := 'MOD';
   action_del   VARCHAR2(25)   := 'DEL';

   Type ICD_rec_tab IS TABLE OF ITEM_COST_DETAIL%rowtype;

   Type ICH_rec_tab IS TABLE OF ITEM_COST_HEAD%rowtype;

   Type IC_rec_tab IS TABLE OF ITEM_COUNTRY%rowtype;

   Type IC_L10_rec_tab IS TABLE OF ITEM_COUNTRY_L10N_EXT%rowtype;

   Type IM_rec_tab IS TABLE OF ITEM_MASTER%rowtype;

   Type IMTL_rec_tab IS TABLE OF ITEM_MASTER_TL%rowtype;

   Type IM_CFA_rec_tab IS TABLE OF ITEM_MASTER_CFA_EXT%rowtype;

   Type IS_rec_tab IS TABLE OF ITEM_SUPPLIER%rowtype;

   Type ISTL_rec_tab IS TABLE OF ITEM_SUPPLIER_TL%rowtype;

   Type IS_CFA_rec_tab IS TABLE OF ITEM_SUPPLIER_CFA_EXT%rowtype;

   Type ISC_rec_tab IS TABLE OF ITEM_SUPP_COUNTRY%rowtype;

   Type ISC_CFA_rec_tab IS TABLE OF ITEM_SUPP_COUNTRY_CFA_EXT%rowtype;

   Type ISCD_rec_tab IS TABLE OF ITEM_SUPP_COUNTRY_DIM%rowtype;

   Type ISMC_rec_tab IS TABLE OF ITEM_SUPP_MANU_COUNTRY%rowtype;

   Type ISU_rec_tab IS TABLE OF ITEM_SUPP_UOM%rowtype;

   Type IXD_rec_tab IS TABLE OF ITEM_XFORM_DETAIL%rowtype;

   Type IXH_rec_tab IS TABLE OF ITEM_XFORM_HEAD%rowtype;

   Type IXHTL_rec_tab IS TABLE OF ITEM_XFORM_HEAD_TL%rowtype;

   Type PI_rec_tab IS TABLE OF PACKITEM%rowtype;

   Type PI_breakout_tab IS TABLE OF PACKITEM_BREAKOUT%rowtype;

   Type IZP_rec_tab IS TABLE OF RPM_ITEM_ZONE_PRICE%rowtype;

   Type SEASON_rec_tab IS TABLE OF ITEM_SEASONS%rowtype;

   Type UID_rec_tab IS TABLE OF UDA_ITEM_DATE%rowtype;

   Type UIF_rec_tab IS TABLE OF UDA_ITEM_FF%rowtype;

   Type UIL_rec_tab IS TABLE OF UDA_ITEM_LOV%rowtype;

   Type VI_rec_tab IS TABLE OF VAT_ITEM%rowtype;

   Type IIM_rec_tab IS TABLE OF ITEM_IMAGE%rowtype;

   Type IIMTL_rec_tab IS TABLE OF ITEM_IMAGE_TL%rowtype;

FUNCTION PROCESS_CHUNK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id      IN       NUMBER,
                       I_chunk_id        IN       NUMBER,
                       O_error_count     OUT      NUMBER)
   RETURN BOOLEAN;


FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_process_id      IN       NUMBER)
   RETURN BOOLEAN;

FUNCTION PURGE_SVC_ITEM_TABLES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   
FUNCTION COMPLETE_SVC_IM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_process_id      IN       NUMBER)
   RETURN BOOLEAN;

END CORESVC_ITEM;
/