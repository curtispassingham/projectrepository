
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY PACKITEM_BREAK_SQL AS
------------------------------------------------------------------
FUNCTION BREAK_TO_COMPONENTS( O_error_message  IN OUT VARCHAR2,
                              I_pack_no        IN     PACKITEM.PACK_NO%TYPE,
                              I_loc            IN     ITEM_LOC.LOC%TYPE,
                              I_break_qty      IN     NUMBER,
                              I_break_to_level IN     VARCHAR2)
   return BOOLEAN is
   ---
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);
   L_table           VARCHAR2(50);
   L_component_item  PACKITEM.ITEM%TYPE                   := NULL;
   L_qty             PACKITEM_BREAKOUT.PACK_ITEM_QTY%TYPE := NULL;
   L_pack_ind        ITEM_MASTER.PACK_IND%TYPE := NULL;
   ---
   cursor C_LOCK_ITEM_LOC_SOH is
      select 'x'
        from item_loc_soh ils
       where ils.loc  = I_loc
         and (ils.item = I_pack_no
              and exists (select 'x'
                            from v_packsku_qty vpq
                           where vpq.pack_no = I_pack_no
                             and vpq.item    = ils.item
                             and I_break_to_level = 'I')
              or exists (select 'x'
                           from packitem pi
                          where pi.pack_no = I_pack_no
                            and pi.item   = ils.item
                            and I_break_to_level = 'N'));
   ---
   cursor C_LOCK_ITEM_LOC is
      select 'x'
        from item_loc il
       where il.loc = I_loc
              and exists (select 'x'
                            from v_packsku_qty vpq
                           where vpq.item = il.item
                             and I_break_to_level = 'I')
              or exists (select 'x'
                           from packitem pi
                          where pi.item   = il.item
                            and I_break_to_level = 'N');
   ---
   cursor C_GET_COMPONENT_ITEMS is
      select vpq.item,
             vpq.qty
         from v_packsku_qty vpq,
              item_master im
        where vpq.pack_no = I_pack_no
          and vpq.item = im.item
          and im.inventory_ind = 'Y'
          and exists(select 'x'
                       from item_loc il
                      where il.loc  = I_loc
                        and il.item = vpq.item);
   ---
   cursor C_GET_PACK_COMP_ITEMS is
      select pi.item,
             pi.pack_qty,
             im1.pack_ind
         from packitem pi,
              item_master im1
        where pi.pack_no = I_pack_no
          and im1.item = pi.item
          and im1.inventory_ind = 'Y'
          and exists ( select 'x'
                          from item_loc il,
                               item_master im2
                         where il.loc  = I_loc
                           and il.item = pi.item
                           and im2.item = pi.item );

BEGIN
   -- Check if pack_no was passed in
   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_PACK_NO',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   -- Check if I_loc was passed in
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_LOC',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   -- Set I_break_qty to -1 if <= 0
   if NVL(I_break_qty, -1) <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_BREAK_QTY',
                                            NVL(I_break_qty, 'NULL'),
                                            '> 0');
      return FALSE;
   end if;
   -- Check if valid I_break_level value passed in
   if I_break_to_level not in ('I','N') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_BREAK_TO_LEVEL',
                                            NVL(I_break_to_level, 'NULL'),
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   L_table    := 'ITEM_LOC_SOH';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    'PACK_NO: '||I_pack_no||', LOC: '||I_loc);
   open C_LOCK_ITEM_LOC_SOH;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_LOC_SOH',
                    'ITEM_LOC_SOH',
                    'PACK_NO: '||I_pack_no||', LOC: '||I_loc);
   close C_LOCK_ITEM_LOC_SOH;
   ---
   L_table    := 'ITEM_LOC';
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    'PACK_NO: '||I_pack_no||', LOC: '||I_loc);
   open C_LOCK_ITEM_LOC;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ITEM_LOC',
                    'ITEM_LOC',
                    'PACK_NO: '||I_pack_no||', LOC: '||I_loc);
   close C_LOCK_ITEM_LOC;

   ---
   /* Update the stock on hand for the pack */
   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ITEM_LOC_SOH',
                    'PACK_NO: '||I_pack_no||', LOC: '||I_loc);
   update item_loc_soh ils
      set ils.stock_on_hand         = ils.stock_on_hand - I_break_qty,
          ils.last_update_id        = get_user,
          ils.last_update_datetime  = sysdate,
          ils.soh_update_datetime   = sysdate
    where ils.loc  = I_loc
      and ils.item = I_pack_no;
   ---
   /* Process pack component items */
   if I_break_to_level = 'I' then
      FOR rec in C_GET_COMPONENT_ITEMS LOOP
         L_component_item := rec.item;
         L_qty            := rec.qty;
         L_pack_ind       := 'N';
         ---
         /* Update the stock on hand for the pack component items */
         SQL_LIB.SET_MARK('UPDATE',
                          NULL,
                          'ITEM_LOC_SOH',
                          'PACK_NO: '||I_pack_no||', ITEM: '||L_component_item||', LOC: '||I_loc);
         update item_loc_soh ils
            set ils.stock_on_hand = ils.stock_on_hand + (I_break_qty * L_qty),
                ils.pack_comp_soh = decode(L_pack_ind, 'N', (ils.pack_comp_soh - (I_break_qty * L_qty)), ils.pack_comp_soh),
                ils.last_update_id       = get_user,
                ils.last_update_datetime = sysdate,
                ils.soh_update_datetime  = sysdate
          where ils.loc  = I_loc
            and ils.item = L_component_item;
         ---
      end LOOP;
   end if;
   ---
   if I_break_to_level = 'N' then
      FOR rec1 in C_GET_PACK_COMP_ITEMS LOOP
         L_component_item := rec1.item;
         L_qty            := rec1.pack_qty;
         L_pack_ind       := rec1.pack_ind;
         ---
         /* Update the stock on hand for the pack component items */
        
           if L_pack_ind = 'N' then
            SQL_LIB.SET_MARK('UPDATE',
                             NULL,
                             'ITEM_LOC_SOH',
                             'PACK_NO: '||I_pack_no||', ITEM: '||L_component_item||', LOC: '||I_loc);
            update item_loc_soh ils
               set ils.stock_on_hand = ils.stock_on_hand + (I_break_qty * L_qty),
                   ils.pack_comp_soh = ils.pack_comp_soh - (I_break_qty * L_qty),
                   ils.last_update_id       = get_user,
                   ils.last_update_datetime = sysdate,
                   ils.soh_update_datetime  = sysdate
             where ils.loc  = I_loc
               and ils.item = L_component_item;
         end if;
         if L_pack_ind = 'Y' then
            SQL_LIB.SET_MARK('UPDATE',
                             NULL,
                             'ITEM_LOC_SOH',
                             'PACK_NO: '||I_pack_no||', ITEM: '||L_component_item||', LOC: '||I_loc);
            update item_loc_soh ils
               set ils.stock_on_hand = ils.stock_on_hand + (I_break_qty * L_qty),
                   ils.last_update_id       = get_user,
                   ils.last_update_datetime = sysdate,
                   ils.soh_update_datetime  = sysdate
             where ils.loc  = I_loc
               and ils.item = L_component_item;
        end if;
      end LOOP;
   end if;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'PACK_NO: '||I_pack_no,
                                            'LOC: '||I_loc);
         return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'BREAK_TO_COMPONENTS',
                                            SQLCODE);
      return FALSE;
END BREAK_TO_COMPONENTS;
------------------------------------------------------------------
END;
/


