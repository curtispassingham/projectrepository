CREATE OR REPLACE PACKAGE RMSMFM_PARTNER AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- PUBLIC VARIABLES
--------------------------------------------------------------------------------

FAMILY    CONSTANT RIB_SETTINGS.FAMILY%TYPE := 'PARTNER';

HDR_ADD   CONSTANT VARCHAR2(15) := 'partnercre';
HDR_UPD   CONSTANT VARCHAR2(15) := 'partnermod';
HDR_DEL   CONSTANT VARCHAR2(15) := 'partnerdel';
DTL_ADD   CONSTANT VARCHAR2(15) := 'partnerdtlcre';
DTL_UPD   CONSTANT VARCHAR2(15) := 'partnerdtlmod';
DTL_DEL   CONSTANT VARCHAR2(15) := 'partnerdtldel';


--------------------------------------------------------------------------------
-- PUBLIC TYPES
--------------------------------------------------------------------------------

TYPE PARTNER_KEY_REC IS RECORD
(
partner_type   PARTNER.PARTNER_TYPE%TYPE,
partner_id     PARTNER.PARTNER_ID%TYPE,
addr_key       ADDR.ADDR_KEY%TYPE
);


--------------------------------------------------------------------------------
-- PUBLIC PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg          OUT VARCHAR2,
                I_message_type    IN     VARCHAR2,
                I_partner_key_rec IN     PARTNER_KEY_REC,
                I_publish_ind     IN     ADDR.PUBLISH_IND%TYPE)
RETURN BOOLEAN;

PROCEDURE GETNXT(O_status_code     OUT VARCHAR2,
                 O_error_msg       OUT VARCHAR2,
                 O_message_type    OUT VARCHAR2,
                 O_message         OUT RIB_OBJECT,
                 O_bus_obj_id      OUT RIB_BUSOBJID_TBL,
                 O_routing_info    OUT RIB_ROUTINGINFO_TBL,
                 I_num_threads  IN     NUMBER DEFAULT 1,
                 I_thread_val   IN     NUMBER DEFAULT 1);

PROCEDURE PUB_RETRY(O_status_code     OUT VARCHAR2,
                    O_error_msg       OUT VARCHAR2,
                    O_message_type IN OUT VARCHAR2,
                    O_message         OUT RIB_OBJECT,
                    O_bus_obj_id   IN OUT RIB_BUSOBJID_TBL,
                    O_routing_info IN OUT RIB_ROUTINGINFO_TBL,
                    I_ref_object   IN     RIB_OBJECT);

END RMSMFM_PARTNER;
/
