CREATE OR REPLACE PACKAGE BODY CORESVC_ITEM_LOC_RANGING_SQL AS
------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEM_LOC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_async_id   IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'CORESVC_ITEM_LOC_RANGING_SQL.CREATE_ITEM_LOC';
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   L_input                NEW_ITEM_LOC_SQL.NIL_INPUT_TBL;
   L_null_supp_loc_exists BOOLEAN;
   L_table                VARCHAR2(30);
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_ITEMLOC_REC is
      select item,
             item_parent,
             item_grandparent,
             short_desc,
             dept,
             class,
             subclass,
             item_level,
             tran_level,
             item_status,
             waste_type,
             sellable_ind,
             orderable_ind,
             pack_ind,
             pack_type,
             item_desc,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             loc,
             loc_type,
             daily_waste_pct,
             unit_cost_loc,
             unit_retail_loc,
             selling_unit_retail_loc,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_selling_uom,
             item_loc_status,
             taxable_ind,
             ti,
             hi,
             store_ord_mult,
             meas_of_each,
             meas_of_price,
             uom_of_price,
             primary_variant,
             primary_supp,
             primary_cntry,
             local_item_desc,
             local_short_desc,
             primary_cost_pack,
             receive_as_type,
             store_price_ind,
             uin_type,
             uin_label,
             capture_time,
             ext_uin_ind,
             source_method,
             source_wh,
             inbound_handling_days,
             CURRENCY_CODE,
             like_store,
             default_to_children,
             NULL, --class_vat_ind,
             hier_level,
             hier_num_value,
             hier_char_value,
             costing_loc,
             costing_loc_type,
             ranged_ind,
             NULL,
             NULL
        from svc_item_loc_ranging
       where rms_async_id = I_rms_async_id;

   cursor C_LOCK_SIL is
      select 'x'
        from svc_item_loc_ranging
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_LOCK_SRL is
      select 'x'
        from svc_item_loc_ranging_locs
       where rms_async_id = I_rms_async_id
         for update nowait;   
         
BEGIN

   if I_rms_async_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_rms_async_id,
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
 
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;

   open C_GET_ITEMLOC_REC;
   fetch C_GET_ITEMLOC_REC BULK COLLECT into L_input;
   close C_GET_ITEMLOC_REC;
      
   if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC(O_error_message,
                                    L_input,
                                    'Y') = FALSE then
      return FALSE;
   end if;
   ---
   FOR L_indx IN L_input.FIRST .. L_input.LAST LOOP
      if L_input(L_indx).loc_type in ('W','PW') and L_input(L_indx).receive_as_type is not null then
         if ITEM_LOC_SQL.UPDATE_RECV_AS_TYPE(O_error_message,
                                             L_input(L_indx).item,
                                             L_input(L_indx).loc,
                                             L_input(L_indx).receive_as_type) = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      if ITEM_LOC_SQL.DEL_GTAX_ITEM_ROLLUP(O_error_message,
                                           L_input(L_indx).item) = FALSE then
         return FALSE;
      end if;
      ---
      if L_input(L_indx).orderable_ind = 'Y' and L_input(L_indx).item_status = 'A' and L_system_options_rec.org_unit_ind = 'Y' then
         if SET_OF_BOOKS_SQL.CHECK_SUPP_MASS_LOC(O_error_message,
                                                 L_null_supp_loc_exists,
                                                 L_input(L_indx).item) = FALSE then
            return FALSE;
         end if;
         --
         if L_null_supp_loc_exists = TRUE then
            O_error_message := SQL_LIB.CREATE_MSG('NULL_PRIM_SUPP',
                                                  NULL, 
                                                  NULL, 
                                                  NULL);
            return FALSE;
         end if;
      end if;
   END LOOP;
   
   L_table := 'SVC_ITEM_LOC_RANGING_LOCS';
   open C_LOCK_SRL;
   close C_LOCK_SRL;
   
   delete from SVC_ITEM_LOC_RANGING_LOCS
    where rms_async_id = I_rms_async_id;
    
   L_table := 'SVC_ITEM_LOC_RANGING';
   open C_LOCK_SIL;
   close C_LOCK_SIL;

   delete from SVC_ITEM_LOC_RANGING
    where rms_async_id = I_rms_async_id;
      
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_ITEM_LOC;
------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_LOC(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rms_async_id   IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) := 'CORESVC_ITEM_LOC_RANGING_SQL.UPDATE_ITEM_LOC';
   L_itemloc_rec             SVC_ITEM_LOC_RANGING%ROWTYPE;
   L_date                    PERIOD.VDATE%TYPE := get_vdate;
   L_default_to_children     BOOLEAN;
   L_system_options_rec      SYSTEM_OPTIONS%ROWTYPE;
   L_null_supp_loc_exists    BOOLEAN;
   L_primary_cost_pack       ITEM_LOC.PRIMARY_COST_PACK%TYPE;
   L_stockholding_store      BOOLEAN;
   L_store_type              STORE.STORE_TYPE%TYPE;
   L_store_name              STORE.STORE_NAME%TYPE;
   L_index                   NUMBER := 1;
   L_pp_cost_event_tbl       OBJ_PP_COST_EVENT_TBL := OBJ_PP_COST_EVENT_TBL(OBJ_PP_COST_EVENT_REC(NULL,NULL,NULL));
   L_username                USERS.USER_NAME%TYPE  := GET_USER;
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_table                   VARCHAR2(30);
   L_old_costing_loc         ITEM_LOC.COSTING_LOC%TYPE; 
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(RECORD_LOCKED, -54);   
   cursor C_GET_ITEMLOC_REC is
      select *
        from svc_item_loc_ranging
       where rms_async_id = I_rms_async_id;

   cursor C_GET_PRIMARY_COST_PACK is
      select primary_cost_pack
        from item_loc
       where item = L_itemloc_rec.item
         and loc = L_itemloc_rec.loc_group_value;

   cursor C_LOCK_SIL is
      select 'x'
        from svc_item_loc_ranging
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_LOCK_SRL is
      select 'x'
        from svc_item_loc_ranging_locs
       where rms_async_id = I_rms_async_id
         for update nowait;
         
   cursor C_GET_COSTING_LOC is
      select costing_loc
        from item_loc
       where item = L_itemloc_rec.item
         and loc = L_itemloc_rec.loc_group_value;          

BEGIN
   if I_rms_async_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rms_async_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;

   open C_GET_ITEMLOC_REC;
   fetch C_GET_ITEMLOC_REC into L_itemloc_rec;
   close C_GET_ITEMLOC_REC;

   open C_GET_PRIMARY_COST_PACK;
   fetch C_GET_PRIMARY_COST_PACK into L_primary_cost_pack;
   close C_GET_PRIMARY_COST_PACK;
   
   if L_itemloc_rec.default_to_children = 'Y' then
      L_default_to_children := TRUE;
   else
      L_default_to_children := FALSE;
   end if;
   
   open C_GET_COSTING_LOC;
   fetch C_GET_COSTING_LOC into L_old_costing_loc;
   close C_GET_COSTING_LOC; 
      
   if ITEM_LOC_SQL.UPDATE_ITEM_LOC(O_error_message,
                                   L_itemloc_rec.item,
                                   L_itemloc_rec.item_status,
                                   L_itemloc_rec.item_level,
                                   L_itemloc_rec.tran_level,
                                   to_number(L_itemloc_rec.loc_group_value),
                                   L_itemloc_rec.loc_group_type,
                                   L_itemloc_rec.primary_supp,
                                   L_itemloc_rec.primary_cntry,
                                   L_itemloc_rec.item_loc_status,
                                   L_itemloc_rec.local_item_desc,
                                   L_itemloc_rec.local_short_desc,
                                   L_itemloc_rec.primary_variant,
                                   L_itemloc_rec.unit_retail_loc,
                                   L_itemloc_rec.ti,
                                   L_itemloc_rec.hi,
                                   L_itemloc_rec.store_ord_mult,
                                   L_itemloc_rec.daily_waste_pct,
                                   L_itemloc_rec.taxable_ind,
                                   L_itemloc_rec.meas_of_each,
                                   L_itemloc_rec.meas_of_price,
                                   L_itemloc_rec.uom_of_price,
                                   L_itemloc_rec.selling_unit_retail_loc,
                                   L_itemloc_rec.selling_uom,
                                   L_itemloc_rec.primary_cost_pack,
                                   L_itemloc_rec.default_to_children,
                                   L_itemloc_rec.receive_as_type,
                                   L_itemloc_rec.ranged_ind,
                                   L_itemloc_rec.inbound_handling_days,
                                   L_itemloc_rec.store_price_ind,
                                   L_itemloc_rec.source_method,
                                   L_itemloc_rec.source_wh,
                                   L_itemloc_rec.multi_units,
                                   L_itemloc_rec.multi_unit_retail,
                                   L_itemloc_rec.multi_selling_uom,
                                   L_itemloc_rec.uin_type,
                                   L_itemloc_rec.uin_label,
                                   L_itemloc_rec.capture_time,
                                   L_itemloc_rec.ext_uin_ind,
                                   L_itemloc_rec.costing_loc,
                                   L_itemloc_rec.costing_loc_type) = FALSE then                                   
         return FALSE;
   end if;

   if L_itemloc_rec.loc_group_type = 'S' then
      if FILTER_LOV_VALIDATE_SQL.STOCKHOLDING_STORE(O_error_message,
                                                    L_stockholding_store,
                                                    L_store_name,
                                                    L_store_type,
                                                    L_itemloc_rec.loc_group_value) = FALSE then
         return FALSE;
      end if;
   end if;

   if ((L_itemloc_rec.item is NOT NULL) and
       (NVL(L_itemloc_rec.primary_cost_pack, 'x') != NVL(L_primary_cost_pack, 'x')) and
        NVL(L_store_type, 'x')  not in ('W','F')) then

      L_pp_cost_event_tbl(L_index).item                := L_itemloc_rec.item;
      L_pp_cost_event_tbl(L_index).location            := L_itemloc_rec.loc_group_value;
      L_pp_cost_event_tbl(L_index).pack_no             := L_itemloc_rec.primary_cost_pack;

      if FUTURE_COST_EVENT_SQL.ADD_PRIMARY_PACK_COST_CHG(O_error_message,
                                                         L_cost_event_process_id,
                                                         L_pp_cost_event_tbl,
                                                         L_username) = FALSE then
         return FALSE;
      end if;
   end if;

   if L_old_costing_loc != L_itemloc_rec.costing_loc then   
      if L_default_to_children then
      
         if FUTURE_COST_EVENT_SQL.MOD_FRAN_COSTING_LOC_CH(O_error_message,
                                                          L_cost_event_process_id,
                                                          L_itemloc_rec.item,
                                                          L_itemloc_rec.loc_group_value,
                                                          L_itemloc_rec.costing_loc,
                                                          L_date) = FALSE then 
            return FALSE;
         end if;
      else
         if FUTURE_COST_EVENT_SQL.MOD_FRAN_COSTING_LOC(O_error_message,
                                                       L_cost_event_process_id,
                                                       L_itemloc_rec.item,
                                                       L_itemloc_rec.loc_group_value,
                                                       L_itemloc_rec.costing_loc,
                                                       L_date) = FALSE then 
            return FALSE;
         end if;   
      end if;       
   end if;

   if L_itemloc_rec.loc_group_type = 'W' and L_itemloc_rec.receive_as_type is not null then
      if ITEM_LOC_SQL.UPDATE_RECV_AS_TYPE(O_error_message,
                                          L_itemloc_rec.item,
                                          L_itemloc_rec.loc_group_value,
                                          L_itemloc_rec.receive_as_type) = FALSE then
         return FALSE;
      end if;
   end if;

   if ITEM_LOC_SQL.DEL_GTAX_ITEM_ROLLUP(O_error_message,
                                        L_itemloc_rec.item) = FALSE then
      return FALSE;
   end if;

   if L_itemloc_rec.orderable_ind = 'Y' and L_itemloc_rec.item_status = 'A' and L_system_options_rec.org_unit_ind = 'Y' then
      if SET_OF_BOOKS_SQL.CHECK_SUPP_MASS_LOC(O_error_message,
                                              L_null_supp_loc_exists,
                                              L_itemloc_rec.item) = FALSE then
         return FALSE;
      end if;

      if L_null_supp_loc_exists = TRUE then
         O_error_message := SQL_LIB.CREATE_MSG('NULL_PRIM_SUPP',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   L_table := 'SVC_ITEM_LOC_RANGING_LOCS';
   open C_LOCK_SRL;
   close C_LOCK_SRL;
   
   delete from SVC_ITEM_LOC_RANGING_LOCS
    where rms_async_id = I_rms_async_id;
    
   L_table := 'SVC_ITEM_LOC_RANGING';
   open C_LOCK_SIL;
   close C_LOCK_SIL;

   delete from SVC_ITEM_LOC_RANGING
    where rms_async_id = I_rms_async_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ITEM_LOC;
------------------------------------------------------------------------------------------------
-- Function : CHECK_ASYNC_JOB_EXISTS
-- Purpose  : Check for any unprocessed or in-progress Async job for a item, its parent, 
--            grandparent or children if any.  
-- Return   : O_exists as TRUE and job id if a Asyncjob exists. else O_exist is set to FALSE
------------------------------------------------------------------------------------------------
FUNCTION CHECK_ASYNC_JOB_EXISTS(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists         IN OUT   BOOLEAN,
                                O_rms_async_id   IN OUT   RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                O_status         IN OUT   RMS_ASYNC_STATUS.STATUS%TYPE,
                                I_item           IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) := 'CORESVC_ITEM_LOC_RANGING_SQL.CHECK_ASYNC_JOB_EXISTS';
   
   
   cursor C_GET_ASYNC_JOB is
     select sv.rms_async_id,
            ra.status
       from svc_item_loc_ranging sv,
            rms_async_status ra,
            item_master im
      where (   im.item = sv.item
             or (   (   im.item_parent = sv.item 
                     or im.item_grandparent = sv.item)
                 and sv.default_to_children = 'Y')
             or im.item = sv.item_parent
             or im.item = sv.item_grandparent)
        and im.item = I_item
        and sv.rms_async_id = ra.rms_async_id 
        and ra.status NOT IN ('E', 'S')
        and rownum  = 1;
             
BEGIN
   
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 
                                            'I_item', 
                                            NULL, 
                                            NULL);
      return FALSE;
   end if;
   
   --- get the Async Job id of any pending Async ranging 
   open C_GET_ASYNC_JOB;
   fetch C_GET_ASYNC_JOB into O_rms_async_id,
                              O_status;
   if C_GET_ASYNC_JOB%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_GET_ASYNC_JOB;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_ASYNC_JOB_EXISTS;
------------------------------------------------------------------------------------------------                              
END CORESVC_ITEM_LOC_RANGING_SQL;
/
