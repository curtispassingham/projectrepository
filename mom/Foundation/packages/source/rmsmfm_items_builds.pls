CREATE OR REPLACE PACKAGE RMSMFM_ITEMS_BUILD AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------------------
FUNCTION BUILD_MESSAGE(O_error_message    IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                       O_message          IN OUT NOCOPY RIB_OBJECT,
                       O_rowids_rec       IN OUT NOCOPY RMSMFM_ITEMS.ITEM_ROWID_REC,
                       O_record_exists    IN OUT        BOOLEAN,
                       I_message_type     IN            VARCHAR2,
                       I_tran_level_ind   IN            ITEM_PUB_INFO.TRAN_LEVEL_IND%TYPE,
                       I_queue_rec        IN            ITEM_MFQUEUE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
FUNCTION BUILD_DELETE_MESSAGE(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                              O_message         IN OUT NOCOPY RIB_OBJECT,
                              O_create_rowid    IN OUT        ROWID,
                              O_create_seq_no   IN OUT        ITEM_MFQUEUE.SEQ_NO%TYPE,
                              I_queue_rec       IN            ITEM_MFQUEUE%ROWTYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END;
/