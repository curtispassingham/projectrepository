CREATE OR REPLACE PACKAGE CORESVC_SEC_GROUP_LOC_MATRIX AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------
   template_key                     CONSTANT VARCHAR2(255):='SEC_GROUP_LOC_MATRIX_DATA';
   action_new                       VARCHAR2(25)          := 'NEW';
   action_mod                       VARCHAR2(25)          := 'MOD';
   action_del                       VARCHAR2(25)          := 'DEL';
   SEC_GRP_LC_MTRX_sheet             VARCHAR2(255)         := 'SEC_GROUP_LOC_MATRIX';
   SEC_GRP_LC_MTRX$Action            NUMBER                :=1;
   SEC_GRP_LC_MTRX$UPDATE_IND        NUMBER                :=9;
   SEC_GRP_LC_MTRX$SELECT_IND        NUMBER                :=8;
   SEC_GRP_LC_MTRX$WH                NUMBER                :=7;
   SEC_GRP_LC_MTRX$STORE             NUMBER                :=6;
   SEC_GRP_LC_MTRX$DISTRICT          NUMBER                :=5;
   SEC_GRP_LC_MTRX$REGION            NUMBER                :=4;
   SEC_GRP_LC_MTRX$GROUP_ID          NUMBER                :=3;
   SEC_GRP_LC_MTRX$COLUMN_CODE       NUMBER                :=2;
-----------------------------------------------------------------------------------
   sheet_name_trans                       S9T_PKG.trans_map_typ;
   action_column                          VARCHAR2(255) := 'ACTION';
   template_category                      CODE_DETAIL.CODE%TYPE :='RMSADM';
-----------------------------------------------------------------------------------
   TYPE SEC_GROUP_LOC_MATRIX_rec_tab IS TABLE OF SEC_GROUP_LOC_MATRIX%ROWTYPE;
-----------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
------------------------------------------------------------------------------------
END CORESVC_SEC_GROUP_LOC_MATRIX;
/
