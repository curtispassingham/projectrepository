
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY MERCHANT_SQL AS
--------------------------------------------------------------------
FUNCTION GET_MERCH_NAME(O_error_message   IN OUT   VARCHAR2,
                        O_merch_name      IN OUT   MERCHANT.MERCH_NAME%TYPE,
                        I_merchant        IN       MERCHANT.MERCH%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MERCHANT_SQL.GET_MERCH_NAME';

   cursor C_MERCH is
      select merch_name
        from merchant
       where merch = I_merchant;

BEGIN
   open C_MERCH;
   fetch C_MERCH into O_merch_name;
   if C_MERCH%NOTFOUND then
      O_error_message := 'INV_MERCH';
      close C_MERCH;
      return FALSE;
   end if;
   close C_MERCH;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_MERCH_NAME;
--------------------------------------------------------------------------   
END MERCHANT_SQL;
/
