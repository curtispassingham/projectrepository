CREATE OR REPLACE PACKAGE RELATED_ITEM_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------
-- Function: NEXT_REL_ID
-- Purpose: This function generates the next relationship_id 
--          using the sequence RELATIONSHIP_ID_SEQ.
-----------------------------------------------------------------
FUNCTION NEXT_REL_ID(O_error_message     IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                     O_next_rel_id       IN OUT    RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE)

RETURN BOOLEAN;
----------------------------------------------------------------
--Function: DELETE_DETAIL_RECORDS
--Purpose: This function will delete the related item detail records if any, 
--         when attempting to delete the master relationship record.
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_DETAIL_RECORDS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_relationship_id   IN     RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function: MERGE_RELATED_ITEMS
--Purpose: This function will insert the related item details in the related_item_detail 
--         table after all the validations. If for any main item/relationship type ,
--         the related item already exists then that record will be updated.
------------------------------------------------------------------------------------------------------
FUNCTION MERGE_RELATED_ITEMS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_no_recs              IN OUT BOOLEAN,
                             I_main_item            IN     RELATED_ITEM_HEAD.ITEM%TYPE,
                             I_relationship_type    IN     RELATED_ITEM_HEAD.RELATIONSHIP_TYPE%TYPE,
                             I_relationship_id      IN     RELATED_ITEM_DETAIL.RELATIONSHIP_ID%TYPE,
                             I_priority             IN     RELATED_ITEM_DETAIL.PRIORITY%TYPE,
                             I_start_date           IN     RELATED_ITEM_DETAIL.START_DATE%TYPE,
                             I_end_date             IN     RELATED_ITEM_DETAIL.END_DATE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
--Function: UPDATE_RELATED_ITEMS
--Purpose: This function will update the existing related items.
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_RELATED_ITEMS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_item               IN       RELATED_ITEM_DETAIL.RELATED_ITEM%TYPE,
                              I_relationship_id    IN       RELATED_ITEM_DETAIL.RELATIONSHIP_ID%TYPE,
                              I_priority           IN       RELATED_ITEM_DETAIL.PRIORITY%TYPE,
                              I_start_date         IN       RELATED_ITEM_DETAIL.START_DATE%TYPE,
                              I_end_date           IN       RELATED_ITEM_DETAIL.END_DATE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
--Function: GET_STANDARD_UOM
--Purpose: This function will fetch the standard UOM of the main item.
------------------------------------------------------------------------------------------------------
FUNCTION GET_STANDARD_UOM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_standard_uom   IN OUT UOM_CLASS.UOM%TYPE,
                          I_item           IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN ;
------------------------------------------------------------------------------------------------------
--Function: BASIC_CHECKS
--Purpose: This function will return few counts required by the
--         relateditems form to perform some basic validations.
------------------------------------------------------------------------------------------------------
FUNCTION BASIC_CHECKS(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_sellable_count       IN OUT VARCHAR2,
                      O_Non_tran_lvl_Count   IN OUT VARCHAR2,
                      O_item_less_tran_Count IN OUT VARCHAR2,
                      O_Std_uom_Count        IN OUT VARCHAR2,
                      O_main_item_exists     IN OUT VARCHAR2,
                      O_pending_del_item     IN OUT VARCHAR2,
                      I_main_item            IN ITEM_MASTER.ITEM%TYPE,
                      I_main_item_std_uom    IN ITEM_MASTER.STANDARD_UOM%TYPE,
                      I_item_type            IN VARCHAR2,
                      I_item                 IN VARCHAR2,
                      I_relationship_type    IN RELATED_ITEM_HEAD.RELATIONSHIP_TYPE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--Function: GET_TRAN_LVL_ITEMS
--Purpose: This function will fetch the tran level items and also 
--         the tran level children of the items that are above tran level.
-----------------------------------------------------------------------------------------------
FUNCTION GET_TRAN_LVL_ITEMS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_type          IN       VARCHAR2,
                            I_item               IN       VARCHAR2,
                            I_relationship_type  IN       RELATED_ITEM_HEAD.RELATIONSHIP_TYPE%TYPE,
                            I_tran_lvl_response  IN       VARCHAR2,
                            I_main_item          IN       ITEM_MASTER.ITEM%TYPE,	
                            I_main_std_uom       IN       ITEM_MASTER.STANDARD_UOM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
--Function: CHK_REL_ITEM_CONFLICT
--Purpose: This function will check if there is a conflict for 
--         main item/related items/relationship type combination.
---------------------------------------------------------------------------------------------
FUNCTION CHK_REL_ITEM_CONFLICT(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists             IN OUT  BOOLEAN,
                               I_main_item          IN      RELATED_ITEM_HEAD.ITEM%TYPE,
                               I_relationship_type  IN      RELATED_ITEM_HEAD.RELATIONSHIP_TYPE%TYPE)
                     
RETURN BOOLEAN;  
-------------------------------------------------------------------------------------------------
-- Function: CHK_REL_ITEM_EXISTS
-- Purpose: The function checks if there exists related item for the main item.
---------------------------------------------------------------------------------------

FUNCTION CHK_REL_ITEM_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                             O_exists        IN OUT BOOLEAN,
                             I_item          IN RELATED_ITEM_HEAD.ITEM%TYPE)
	
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: COPY_LIKE_ITEM
-- Purpose: 
---------------------------------------------------------------------------------------------------
FUNCTION COPY_LIKE_ITEM(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_new_item                IN       ITEM_MASTER.ITEM%TYPE,
                        I_existing_item           IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------
-- Function: APPROVAL_CHECK
-- Purpose: This function checks to make sure that the either the related item should be
-- in approved status or it should be a sku of the main-item, in which case both main-item
-- and related item can get approved together.
---------------------------------------------------------------------------------------------------
FUNCTION APPROVAL_CHECK(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_invalids_exist IN OUT VARCHAR2,
                        I_main_item      IN ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function: DELETE_RELATED_ITEM_HEAD_TL
-- Purpose: This function deletes from RELATED_ITEM_HEAD_TL table.
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_RELATED_ITEM_HEAD_TL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_relationship_id   IN       RELATED_ITEM_HEAD_TL.RELATIONSHIP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
END RELATED_ITEM_SQL;

/