
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE UPC_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Function:  Next_var_upc_item
-- Purpose:   Called for generating the next unique variable UPC item code.
--            The item code will be five digits long and will be used to
--            uniquely identify each variable weight UPC.
-- Calls:     var_upc_ean_sequence
-- Created:   Yumen Li Mar. 3, 1999
-------------------------------------------------------------------------------
FUNCTION NEXT_VAR_UPC_ITEM(O_error_message   IN OUT VARCHAR2,
                           O_upc_item_code   IN OUT NUMBER)
RETURN BOOLEAN;
END;
/


