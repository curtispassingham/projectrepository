
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XDIFFGRP_VALIDATE AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diffgrp_rec     OUT    NOCOPY   DIFF_GROUP_SQL.DIFF_GROUP_REC,
                       I_message         IN              "RIB_XDiffGrpDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_diffgrp_rec     OUT    NOCOPY   DIFF_GROUP_SQL.DIFF_GROUP_REC,
                       I_message         IN              "RIB_XDiffGrpRef_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XDIFFGRP_VALIDATE;
/
