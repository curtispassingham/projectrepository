CREATE OR REPLACE PACKAGE CORESVC_SEC_USER_GROUP AUTHID CURRENT_USER AS
-----------------------------------------------------------------------
   template_key              CONSTANT VARCHAR2(255) :='SEC_USER_GROUP_DATA';
   template_category         CONSTANT VARCHAR2(255) := 'RMSSEC';   
   
   action_new                VARCHAR2(25)          := 'NEW';
   action_mod                VARCHAR2(25)          := 'MOD';
   action_del                VARCHAR2(25)          := 'DEL';
   
   SEC_USER_GROUP_sheet      VARCHAR2(255)         := 'SEC_USER_GROUP';
   SEC_USER_GROUP$Action     NUMBER                := 1;
   SEC_USER_GROUP$GROUP_ID   NUMBER                := 2;
   SEC_USER_GROUP$USER_SEQ   NUMBER                := 3;
   
   
   sheet_name_trans          S9T_PKG.trans_map_typ;
   action_column             VARCHAR2(255) := 'ACTION';
   
   TYPE SEC_USER_GROUP_rec_tab IS TABLE OF SEC_USER_GROUP%ROWTYPE;
-------------------------------------------------------------------------   
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
-------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER
                        )
   RETURN BOOLEAN;
-------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER
                    )
   RETURN BOOLEAN;
-------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;
-------------------------------------------------------------------------   
END CORESVC_SEC_USER_GROUP;
/
