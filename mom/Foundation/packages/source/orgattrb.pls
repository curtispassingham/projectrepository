
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORGANIZATION_ATTRIB_SQL AS

------------------------------------------------------------
FUNCTION GET_NAME (O_error_message  IN OUT  VARCHAR2,
                   I_org_hier_type  IN  NUMBER,
                   I_org_hier_value IN      NUMBER,
                   O_org_hier_desc  IN OUT  VARCHAR2)
    RETURN BOOLEAN IS
 
   L_error_message VARCHAR2(255) := NULL;
   L_program       VARCHAR2(64)  := 'ORGANIZATION_ATTRIB_SQL.GET_NAME';

   cursor C_COMPANY is
      select co_name
        from v_comphead_tl
       where company = I_org_hier_value;

   cursor C_CHAIN is
     select chain_name
       from v_chain_tl
      where chain = I_org_hier_value;

   cursor C_AREA is
      select area_name
        from v_area_tl
       where area = I_org_hier_value;

   cursor C_REGION is
      select region_name
        from v_region_tl
       where region = I_org_hier_value;

   cursor C_DISTRICT is
      select district_name
        from v_district_tl
       where district = I_org_hier_value;

   
BEGIN
   if I_org_hier_type = 1 then
      sql_lib.set_mark('OPEN', 'C_company', 'v_comphead_tl', NULL);
      open C_COMPANY;
      sql_lib.set_mark('FETCH', 'C_company', 'v_comphead_tl', NULL);
      fetch C_COMPANY into O_org_hier_desc;
      if C_COMPANY%NOTFOUND then
          O_error_message := sql_lib.create_msg('ERR_RET_COMP', 
                                                 NULL, NULL, NULL);
          close C_COMPANY;
          RETURN FALSE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_company', 'v_comphead_tl', NULL);
      close C_COMPANY;
   elsif I_org_hier_type = 10 then
      sql_lib.set_mark('OPEN', 'C_chain', 'v_chain_tl', NULL);
      open C_CHAIN;
      sql_lib.set_mark('FETCH', 'C_chain', 'v_chain_tl', NULL);
      fetch C_CHAIN into O_org_hier_desc;
      if C_CHAIN%NOTFOUND then
          O_error_message := sql_lib.create_msg('INV_CHAIN',
                                                 NULL, NULL, NULL);
          close C_CHAIN;
          RETURN FALSE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_chain', 'v_chain_tl', NULL);
      close C_CHAIN;
   elsif I_org_hier_type = 20 then
      sql_lib.set_mark('OPEN', 'C_area', 'v_area_tl', NULL);
      open C_area;
      sql_lib.set_mark('FETCH', 'C_area', 'v_area_tl', NULL);
      fetch C_area into O_org_hier_desc;
      if C_area%NOTFOUND then
          O_error_message := sql_lib.create_msg('INV_AREA',
                                                 NULL, NULL, NULL);
          close C_AREA;
          RETURN FALSE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_area', 'v_area_tl', NULL);
      close C_AREA;
   elsif I_org_hier_type = 30 then
      sql_lib.set_mark('OPEN', 'C_region', 'v_region_tl', NULL);
      open C_REGION;
      sql_lib.set_mark('FETCH', 'C_region', 'v_region_tl', NULL);
      fetch C_REGION into O_org_hier_desc;
      if C_REGION%NOTFOUND then
          O_error_message := sql_lib.create_msg('INV_REGION',
                                                 NULL, NULL, NULL);
          close C_REGION;
          RETURN FALSE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_region', 'v_region_tl', NULL);
      close C_REGION;
   elsif I_org_hier_type = 40 then
      sql_lib.set_mark('OPEN', 'C_district', 'v_district_tl', NULL);
      open C_DISTRICT;
      sql_lib.set_mark('FETCH', 'C_district', 'v_district_tl', NULL);
      fetch C_DISTRICT into O_org_hier_desc;
      if C_DISTRICT%NOTFOUND then
          O_error_message := sql_lib.create_msg('INV_DISTRICT',
                                                 NULL, NULL, NULL);
          close C_DISTRICT;
          RETURN FALSE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_district', 'v_district_tl', NULL);
      close C_DISTRICT;
   elsif I_org_hier_type = 50  then
      if STORE_ATTRIB_SQL.GET_NAME(L_error_message,
                                   I_org_hier_value,
                                   O_org_hier_desc) = FALSE then
         O_error_message := L_error_message;
         RETURN FALSE;
      end if;
   end if;
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                         SQLERRM,
                          L_program,
                                 to_char(SQLCODE));
    RETURN FALSE;

END GET_NAME;
-------------------------------------------------------------------
FUNCTION COMPANY_NAME ( O_error_message IN OUT  VARCHAR2,
                I_company       IN      NUMBER,
                O_company_name  IN OUT  VARCHAR2)

RETURN BOOLEAN IS

   cursor C_company is
      select co_name
        from v_comphead_tl
       where company = I_company;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_company', 'v_comphead_tl', 'company: '||to_char(I_company));
   open C_company;
   SQL_LIB.SET_MARK('OPEN' , 'C_company', 'v_comphead_tl', 'company: '||to_char(I_company));
   fetch C_COMPANY into O_company_name;
   if C_company%NOTFOUND then
      O_error_message := sql_lib.create_msg('ERR_RET_COMP', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('OPEN' , 'C_company', 'v_comphead_tl', 'company: '||to_char(I_company));
      close C_COMPANY;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN' , 'C_company', 'v_comphead_tl', 'company: '||to_char(I_company));
   close C_COMPANY;
   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(    'PACKAGE_ERROR', 
                                SQLERRM, 
                                'ORGANIZATION_ATTRIB_SQL.COMPANY_NAME', 
                                to_char(SQLCODE));
   RETURN FALSE;

END COMPANY_NAME;
-------------------------------------------------------------------
FUNCTION CHAIN_NAME (   O_error_message IN OUT  VARCHAR2,
                I_chain     IN      NUMBER,
                O_chain_name    IN OUT  VARCHAR2)

RETURN BOOLEAN IS

   cursor C_chain is
      select chain_name
        from v_chain_tl
       where chain = I_chain;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_chain', 'v_chain_tl', 'chain: '||to_char(I_chain));
   open C_chain;
   SQL_LIB.SET_MARK('OPEN' , 'C_chain', 'v_chain_tl', 'chain: '||to_char(I_chain));
   fetch C_CHAIN into O_chain_name;
   if C_chain%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_CHAIN', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('OPEN' , 'C_chain', 'v_chain_tl', 'chain: '||to_char(I_chain));
      close C_CHAIN;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN' , 'C_chain', 'v_chain_tl', 'chain: '||to_char(I_chain));
   close C_CHAIN;
   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(    'PACKAGE_ERROR', 
                                SQLERRM, 
                                'ORGANIZATION_ATTRIB_SQL.CHAIN_NAME', 
                                to_char(SQLCODE));
   RETURN FALSE;

END CHAIN_NAME;
-------------------------------------------------------------------
FUNCTION AREA_NAME (O_error_message IN OUT  VARCHAR2,
            I_area      IN  NUMBER,
            O_area_name         IN OUT VARCHAR2)

RETURN BOOLEAN IS

   cursor C_area is
      select area_name
        from v_area_tl
       where area = I_area;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_area', 'v_area_tl', 'area: '||to_char(I_area));
   open C_area;
   SQL_LIB.SET_MARK('OPEN' , 'C_area', 'v_area_tl', 'area: '||to_char(I_area));
   fetch C_AREA into O_area_name;
   if C_area%NOTFOUND then
      O_error_message := sql_lib.create_msg('ERR_RET_AREA', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('OPEN' , 'C_area', 'v_area_tl', 'area: '||to_char(I_area));
      close C_AREA;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN' , 'C_area', 'v_area_tl', 'area: '||to_char(I_area));
   close C_AREA;
   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(    'PACKAGE_ERROR', 
                                SQLERRM, 
                                'ORGANIZATION_ATTRIB_SQL.AREA_NAME', 
                                to_char(SQLCODE));
   RETURN FALSE;

END AREA_NAME;

-------------------------------------------------------------------
FUNCTION REGION_NAME (  O_error_message IN OUT  VARCHAR2,
                I_region        IN      NUMBER,
                O_region_name   IN OUT  VARCHAR2)

RETURN BOOLEAN IS

   cursor C_region is
      select region_name
        from v_region_tl
       where region = I_region;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_region', 'v_region_tl', 'region: '||to_char(I_region));
   open C_region;
   SQL_LIB.SET_MARK('OPEN' , 'C_region', 'v_region_tl', 'region: '||to_char(I_region));
   fetch C_REGION into O_region_name;
   if C_region%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_REGION', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('OPEN' , 'C_region', 'v_region_tl', 'region: '||to_char(I_region));
      close C_REGION;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN' , 'C_region', 'v_region_tl', 'region: '||to_char(I_region));
   close C_REGION;

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(    'PACKAGE_ERROR', 
                                SQLERRM, 
                                'ORGANIZATION_ATTRIB_SQL.REGION_NAME', 
                                to_char(SQLCODE));
   RETURN FALSE;

END REGION_NAME;

-------------------------------------------------------------------
FUNCTION DISTRICT_NAME (O_error_message IN OUT  VARCHAR2,
                I_district      IN      NUMBER,
                O_district_name IN OUT  VARCHAR2)

RETURN BOOLEAN IS

   cursor C_DISTRICT is
      select district_name
        from v_district_tl
       where district = I_district;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_DISTRICT', 'v_district_tl', 'District: '||to_char(I_district));
   open C_DISTRICT;
   SQL_LIB.SET_MARK('OPEN' , 'C_DISTRICT', 'v_district_tl', 'District: '||to_char(I_district));
   fetch C_DISTRICT into O_district_name;
   if C_DISTRICT%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_DISTRICT_NUM', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('OPEN' , 'C_DISTRICT', 'v_district_tl', 'District: '||to_char(I_district));
      close C_DISTRICT;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN' , 'C_DISTRICT', 'v_district_tl', 'District: '||to_char(I_district));
   close C_DISTRICT;


   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG(    'PACKAGE_ERROR', 
                                SQLERRM, 
                                'ORGANIZATION_ATTRIB_SQL.DISTRICT_NAME', 
                                to_char(SQLCODE));
   RETURN FALSE;

END DISTRICT_NAME;

-----------------------------------------------------------------------------
FUNCTION GET_DISTRICT_REGION(O_error_message IN OUT VARCHAR2,
                             O_region        IN OUT district.region%TYPE,
                             I_district      IN     district.district%TYPE)
RETURN BOOLEAN IS
   
   cursor C_REGION is
      select region
        from district
       where district = I_district;

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_REGION','DISTRICT','DISTRICT'||I_district);
   open C_REGION; 
   SQL_LIB.SET_MARK('FETCH','C_REGION','DISTRICT','DISTRICT'||I_district);
   fetch C_REGION into O_region;
   ---
   if C_REGION%NOTFOUND then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRICT_NUM',
                                          I_district,
                                          NULL,
                                          NULL);

      SQL_LIB.SET_MARK('CLOSE','C_REGION','DISTRICT','DISTRICT'||I_district);
      close C_REGION;
      ---
      return FALSE;
   end if; 
   ---
   SQL_LIB.SET_MARK('CLOSE','C_REGION','DISTRICT','DISTRICT'||I_district);
   close C_REGION;
   ---

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                              SQLERRM, 
                              'ORGANIZATION_ATTRIB_SQL.GET_DISTRICT_REGION', 
                              to_char(SQLCODE));
   RETURN FALSE;
   
END GET_DISTRICT_REGION; 
------------------------------------------------------------------------------
FUNCTION GET_REGION_AREA (O_error_message IN OUT   VARCHAR2,
                          O_desc      IN OUT   area.area_name%TYPE,
                          O_area      IN OUT   area.area%TYPE,
                          I_region        IN       region.region%TYPE)
RETURN BOOLEAN IS

   l_area_name    area.area_name%TYPE := null;
   
   cursor C_REGION_AREA is
      select area
        from region
       where region = I_region;

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_REGION_AREA','region','region:'||I_region);
   open C_REGION_AREA; 
   SQL_LIB.SET_MARK('FETCH','C_REGION_AREA','region','region:'||I_region);
   fetch C_REGION_AREA into O_area;
   ---
   if C_REGION_AREA%NOTFOUND then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_REGION_NUM',
                                            I_region,
                                            NULL,
                                            NULL);

      SQL_LIB.SET_MARK('CLOSE','C_REGION_AREA','region','region:'||I_region);
      close C_REGION_AREA;
      ---
      return FALSE;
   end if; 
   ---
   SQL_LIB.SET_MARK('CLOSE','C_REGION_AREA','region','region:'||I_region);
   close C_REGION_AREA;
   ---
   if AREA_NAME(O_error_message,
                O_area,
                l_area_name) = FALSE then
      return FALSE;
   end if;
   ---
   O_desc := l_area_name;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                        SQLERRM, 
                            'ORGANIZATION_ATTRIB_SQL.GET_REGION_AREA', 
                        to_char(SQLCODE));
   RETURN FALSE;
   
END GET_REGION_AREA; 
------------------------------------------------------------------------------
FUNCTION GET_AREA_CHAIN (O_error_message IN OUT   VARCHAR2,
                         O_desc          IN OUT   chain.chain_name%TYPE,
                         O_chain         IN OUT   chain.chain%TYPE,
                         I_area          IN       area.area%TYPE)
RETURN BOOLEAN IS

   l_chain_name    chain.chain_name%TYPE := null;
   
   cursor C_AREA_CHAIN is
      select chain
        from area
       where area = I_area;

BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN','C_AREA_CHAIN','area','area:'||I_area);
   open C_AREA_CHAIN; 
   SQL_LIB.SET_MARK('FETCH','C_AREA_CHAIN','area','area:'||I_area);
   fetch C_AREA_CHAIN into O_chain;
   ---
   if C_AREA_CHAIN%NOTFOUND then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INV_AREA_NUM',
                                            I_area,
                                            NULL,
                                            NULL);

      SQL_LIB.SET_MARK('CLOSE','C_AREA_CHAIN','area','area:'||I_area);
      close C_AREA_CHAIN;
      ---
      return FALSE;
   end if; 
   ---
   SQL_LIB.SET_MARK('CLOSE','C_AREA_CHAIN','area','area:'||I_area);
   close C_AREA_CHAIN;
   ---
   if CHAIN_NAME(O_error_message,
                 O_chain,
                 l_chain_name) = FALSE then
      return FALSE;
   end if;
   ---
   O_desc := l_chain_name;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                        SQLERRM, 
                            'ORGANIZATION_ATTRIB_SQL.GET_AREA_CHAIN', 
                        to_char(SQLCODE));
   RETURN FALSE;
   
END GET_AREA_CHAIN; 
------------------------------------------------------------------------------
FUNCTION GET_PARENT(O_error_message    IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                    O_parent_id        OUT         NUMBER,
                    I_hier_level       IN          VARCHAR2,
                    I_hier_value       IN          NUMBER)
   RETURN BOOLEAN IS
   
   L_program    VARCHAR2(50)  :=  'ORGANIZATION_ATTRIB_SQL.GET_PARENT';
    
   cursor C_REGION_PARENT is
      select r.area
        from region r
       where r.region = I_hier_value; 
         
   cursor C_DISTRICT_PARENT is 
      select d.region
        from district d
       where d.district = I_hier_value;

BEGIN

   if UPPER(I_hier_level) = 'RE' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_REGION_PARENT','region','region:'||I_hier_value);
      open C_REGION_PARENT; 
      SQL_LIB.SET_MARK('FETCH','C_REGION_PARENT','region','region:'||I_hier_value);
      fetch C_REGION_PARENT into O_parent_id;
      SQL_LIB.SET_MARK('CLOSE','C_REGION_PARENT','region','region:'||I_hier_value);
      close C_REGION_PARENT;
      ---
   elsif UPPER(I_hier_level) = 'DI' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_DISTRICT_PARENT','district','district:'||I_hier_value);
      open C_DISTRICT_PARENT; 
      SQL_LIB.SET_MARK('FETCH','C_DISTRICT_PARENT','district','district:'||I_hier_value);
      fetch C_DISTRICT_PARENT into O_parent_id;
      SQL_LIB.SET_MARK('CLOSE','C_DISTRICT_PARENT','district','district:'||I_hier_value);
      close C_DISTRICT_PARENT;
      ---
   end if;

   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            to_char(SQLCODE));
   RETURN FALSE;
END GET_PARENT;
-------------------------------------------------------------
FUNCTION GET_ORG_HIER_LABEL(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_company         IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                            O_chain           IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                            O_area            IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                            O_region          IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                            O_district        IN OUT  CODE_DETAIL.CODE_DESC%TYPE)
RETURN NUMBER IS
   L_program   VARCHAR2(64) := 'ORGANIZATION_ATTRIB_SQL.GET_ORG_HIER_LABEL';

BEGIN
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','COMP',O_company) = FALSE then
      return 0;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','CHAIN',O_chain) = FALSE then
      return 0;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','AREA',O_area) = FALSE then
      return 0;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','REGION',O_region) = FALSE then
      return 0;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','DIS',O_district) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return 0;
END GET_ORG_HIER_LABEL;
-------------------------------------------------------------
END ORGANIZATION_ATTRIB_SQL;
/
