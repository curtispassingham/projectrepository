CREATE OR REPLACE PACKAGE BODY CORESVC_PRICECOST_SQL AS

-------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION 
--  Function Name:   VALIDATE_INPUT
--  Purpose      :This function will validate the input table type for the pricing cost records.
--                For all the errors it will update the error_msg column with the error details.
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_INPUT(O_error_message   OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_tbl       OUT    SVCPROV_UTILITY.ERROR_TBL,
                        I_pricecost_inp   IN     OBJ_INPUT_PRICECOST_TBL)
 RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION 
--  Function Name:   POPULATE_COST_STORE
--  Purpose      :This function will get the pricing_cost for store.
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_COST_STORE(O_error_message       OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_tbl           OUT     SVCPROV_UTILITY.ERROR_TBL,
                             O_output_store        OUT     "RIB_PrcCostDesc_TBL",
                             I_input_tbl           IN      OBJ_INPUT_PRICECOST_TBL)
 RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--- PRIVATE FUNCTION 
--  Function Name:   POPULATE_COST_WH
--  Purpose      :This function will get the pricing cost for wh.
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_COST_WH(O_error_message     OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_tbl         OUT      SVCPROV_UTILITY.ERROR_TBL,
                          O_output_wh         OUT      "RIB_PrcCostDesc_TBL",
                          I_input_tbl         IN       OBJ_INPUT_PRICECOST_TBL)
 RETURN BOOLEAN;                    
------------------------------------------------------------------------------------------------------                    
FUNCTION GET_PRICING_COST(O_error_message     OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_business_object   OUT   "RIB_PrcCostColDesc_REC",
                          O_error_tbl         OUT   SVCPROV_UTILITY.ERROR_TBL,
                          I_business_object   IN    "RIB_PrcCostColCriVo_REC")
 RETURN BOOLEAN IS
  
   L_program                  VARCHAR2(64)     := 'CORESVC_PRICECOST_SQL.GET_PRICING_COST';
   L_vdate                    PERIOD.VDATE%TYPE := GET_VDATE;
   L_location                 NUMBER;
   L_index                    NUMBER := 1;
   L_pricecost_input_tbl      OBJ_INPUT_PRICECOST_TBL := OBJ_INPUT_PRICECOST_TBL();
   L_outptut_store_tbl        "RIB_PrcCostDesc_TBL" := "RIB_PrcCostDesc_TBL"();
   L_outptut_wh_tbl           "RIB_PrcCostDesc_TBL" := "RIB_PrcCostDesc_TBL"();
   L_PrcCostDescOut_tbl       "RIB_PrcCostDesc_TBL" := "RIB_PrcCostDesc_TBL"();
   L_price_error_tbl          SVCPROV_UTILITY.ERROR_TBL := SVCPROV_UTILITY.ERROR_TBL();
   L_exist                    BOOLEAN :=  FALSE;
   L_index_loc_type           NUMBER := 1;     

BEGIN
   O_error_tbl := SVCPROV_UTILITY.ERROR_TBL();
   if I_business_object.PRCCOSTCRIVO_TBL is NULL or I_business_object.PRCCOSTCRIVO_TBL.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_prccostcrivo_obj',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
  
   -- RIB object data will be populated in a table type.    
   for i in I_business_object.PRCCOSTCRIVO_TBL.FIRST..I_business_object.PRCCOSTCRIVO_TBL.LAST LOOP        
      L_pricecost_input_tbl.EXTEND;
      L_pricecost_input_tbl(L_index) := OBJ_INPUT_PRICECOST_REC(I_business_object.PrcCostCriVo_TBL(i).item,
                                                                I_business_object.PrcCostCriVo_TBL(i).source_location,
                                                                I_business_object.PrcCostCriVo_TBL(i).source_location_country,
                                                                I_business_object.PrcCostCriVo_TBL(i).fulfill_location,
                                                                I_business_object.PrcCostCriVo_TBL(i).loc_type,
                                                                I_business_object.PrcCostCriVo_TBL(i).channel_id,
                                                                I_business_object.PrcCostCriVo_TBL(i).active_date);
       L_index := L_index + 1;
   end LOOP;
  
   -- Remove any duplicates 
   select SET(L_pricecost_input_tbl) into L_pricecost_input_tbl from dual;
 
   --validate the input
   if VALIDATE_INPUT(O_error_message,
                     O_error_tbl,
                     L_pricecost_input_tbl) = FALSE then
      return FALSE; 
   end if;
   
   --Getting price cost if the location is "S"tore type.
   for i in L_pricecost_input_tbl.FIRST..L_pricecost_input_tbl.LAST loop
         if L_pricecost_input_tbl(i).loc_type = 'S' then
            L_exist := TRUE;
         end if;   
         exit when L_pricecost_input_tbl(i).loc_type = 'S';
   end loop;
   if L_exist then
      if POPULATE_COST_STORE(O_error_message,
                             O_error_tbl,
                             L_outptut_store_tbl,
                             L_pricecost_input_tbl) = FALSE then
         return FALSE;                          
      end if;   
   end if;  
 
   L_exist := FALSE;
   
   --Getting price cost if the location is "W"arehouse type.
   for i in L_pricecost_input_tbl.FIRST..L_pricecost_input_tbl.LAST loop
      if L_pricecost_input_tbl(i).loc_type = 'W' then
         L_exist := TRUE;
      end if;   
      exit when L_pricecost_input_tbl(i).loc_type = 'W';
   end loop;
  
   if L_exist then
      if POPULATE_COST_WH(O_error_message,
                          O_error_tbl,
                          L_outptut_wh_tbl,
                          L_pricecost_input_tbl) = FALSE then
         return FALSE;                          
      end if;   
   end if;

   L_PrcCostDescOut_tbl := L_outptut_store_tbl MULTISET UNION L_outptut_wh_tbl;  
   
   O_business_object   :=   "RIB_PrcCostColDesc_REC"(0,
                                                     L_PrcCostDescOut_tbl,
                                                     L_PrcCostDescOut_tbl.count);
                                                     
   --Check if the pricing cost null then return false
   if O_business_object.PrcCostDesc_TBL.COUNT > 0 and O_business_object.PrcCostDesc_TBL is NOT NULL then
      for i in O_business_object.PrcCostDesc_TBL.FIRST..O_business_object.PrcCostDesc_TBL.LAST LOOP
         if O_business_object.PrcCostDesc_TBL(i).pricing_cost = 0 then
            O_error_tbl.EXTEND();
            O_error_tbl(i):= O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('PRICE_COST_NOT_EXIST',
                                                                         O_business_object.PrcCostDesc_TBL(i).item,
                                                                         O_business_object.PrcCostDesc_TBL(i).fulfill_location,
                                                                         NULL)||';';

           
         end if;
      END LOOP;
      if O_error_tbl.COUNT > 0 and O_error_tbl is NOT NULL then
         return FALSE;
      end if;  
   end if; 
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRICING_COST;
-------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_INPUT(O_error_message   OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                        O_error_tbl       OUT    SVCPROV_UTILITY.ERROR_TBL,
                        I_pricecost_inp   IN     OBJ_INPUT_PRICECOST_TBL)
 RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'CORESVC_PRICECOST_SQL.VALIDATE_INPUT';

   cursor C_VALIDATE is 
      select input.item                           item,
             im.item                              valid_item,
             input.source_location                input_supplier,
             sup.supplier                         valid_supplier,
             input.source_location_country        input_country,
             cntry.country_id                     valid_country,
             input.loc_type                       loc_type,
             input.channel_id                     input_channel_id,
             ch.channel_id                        valid_channel_id,
             input.fulfill_location               loc,
             im.status                            status,
             im.item_level                        item_level,
             im.tran_level                        tran_level,
             s.store                              valid_store,
             s.channel_id                         store_channel_id,
             NVL(s.customer_order_loc_ind,'N')    customer_order_loc_ind,              
             w.physical_wh                        valid_warehouse,
             (select 'Y' 
                from wh w2 
               where w2.physical_wh = input.fulfill_location 
                 and input.loc_type = 'W' 
                 and w2.channel_id = input.channel_id
                 and rownum = 1)  valid_wh_ind
        from TABLE(CAST(I_pricecost_inp as OBJ_INPUT_PRICECOST_TBL))  input,
             item_master im,
             country cntry,
             sups sup,
             store s,
             channels ch,
             (select physical_wh
                from wh
              group by physical_wh) w
       where input.item = im.item(+)
         and input.channel_id = ch.channel_id(+)
         and input.fulfill_location = s.store(+)
         and input.fulfill_location = w.physical_wh(+)
         and input.source_location_country = cntry.country_id(+)
         and input.source_location = sup.supplier(+)
         and (im.item is NULL
              or im.status <> 'A'
              or im.item_level < im.tran_level
              or NVL(input.loc_type,'X') not in ('S','W')
              or (input.channel_id is not NULL and ch.channel_id is NULL)
              or (NVL(input.loc_type,'X') = 'S' and not exists (select 'X'
                                                                from store st
                                                               where st.store = input.fulfill_location
                                                                 and st.store_type = 'C'
                                                                 and st.customer_order_loc_ind = 'Y'
                                                                 and (st.channel_id is NULL
                                                                      or input.channel_id is NULL
                                                                      or NVL(st.channel_id,-9999) = NVL(input.channel_id,-9999))))
              or (NVL(input.loc_type,'X') = 'W' and not exists (select 'X'
                                                                  from wh wh
                                                                 where wh.physical_wh = input.fulfill_location
                                                                   and wh.channel_id = input.channel_id
                                                                   and wh.channel_id is not NULL
                                                                   and input.channel_id is not NULL)));
  
   TYPE INVALID_TBL IS TABLE OF C_VALIDATE%ROWTYPE INDEX BY BINARY_INTEGER;
   L_inv_invalid_tbl INVALID_TBL;                                                                 

BEGIN
   
   -- Initialize the output error collection. 
   O_error_tbl := SVCPROV_UTILITY.ERROR_TBL();
     
   open  C_VALIDATE;
   fetch C_VALIDATE BULK COLLECT INTO L_inv_invalid_tbl;
   close C_VALIDATE;
   -- if the cursor returns no value, then the data has passed validation with no errors.
   if L_inv_invalid_tbl.COUNT > 0 and L_inv_invalid_tbl is not NULL then
      
      for i in L_inv_invalid_tbl.FIRST..L_inv_invalid_tbl.LAST LOOP
          O_error_tbl.EXTEND;
          -- Input item exists
          if (L_inv_invalid_tbl(i).valid_item is NULL) then
             O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_ITEM_NOT_EXIST',
                                                                          L_inv_invalid_tbl(i).item,
                                                                          NULL,
                                                                          NULL)||';';   
          else
             --Item is not an approved
             if L_inv_invalid_tbl(i).status <> 'A'  then
                O_error_tbl(i) := O_error_tbl(i) ||SQL_LIB.GET_MESSAGE_TEXT('INV_ITEM_NOT_APPROVED',
                                                                            L_inv_invalid_tbl(i).item,
                                                                            NULL,
                                                                            NULL)||';';
             end if; 
             if L_inv_invalid_tbl(i).item_level < L_inv_invalid_tbl(i).tran_level then
                O_error_tbl(i) := O_error_tbl(i) ||SQL_LIB.GET_MESSAGE_TEXT('NO_PARENT_ITEM',
                                                                            L_inv_invalid_tbl(i).item,
                                                                            NULL,
                                                                            NULL)||';';
             end if; 
          end if;
          --Source Location is invalid
          if L_inv_invalid_tbl(i).input_supplier is not NULL and L_inv_invalid_tbl(i).valid_supplier is NULL then
             O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INV_SUP',
                                                                          TO_CHAR(L_inv_invalid_tbl(i).input_supplier),
                                                                          NULL,
                                                                          NULL)||';';
          end if;
          --Source Location Country is invalid
          if L_inv_invalid_tbl(i).input_country is not NULL and L_inv_invalid_tbl(i).valid_country is NULL then
             O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INV_COUNTRY',
                                                                          TO_CHAR(L_inv_invalid_tbl(i).input_country),
                                                                          NULL,
                                                                          NULL)||';';
         end if;
         --Channel_id is invalid
         if L_inv_invalid_tbl(i).input_channel_id is not NULL and L_inv_invalid_tbl(i).valid_channel_id is NULL then
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INV_CHANNEL_ID',
                                                                         TO_CHAR(L_inv_invalid_tbl(i).input_channel_id),
                                                                         NULL,
                                                                         NULL)||';';
         end if;
          
         --Loc_type is invalid
         if L_inv_invalid_tbl(i).loc_type not in ('S','W') then 
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_INVALID_LOC_TYPE',
                                                                         L_inv_invalid_tbl(i).loc_type,
                                                                         NULL,
                                                                         NULL)||';';
         end if;
         -- Store specific validation
         if L_inv_invalid_tbl(i).loc_type = 'S' then 
            --Store location is invalid
            if L_inv_invalid_tbl(i).valid_store is NULL or L_inv_invalid_tbl(i).customer_order_loc_ind <> 'Y' then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_STORE_NOT_EXIST',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            NULL,
                                                                            NULL)||';';
            end if;
            if L_inv_invalid_tbl(i).valid_store is not NULL
               and L_inv_invalid_tbl(i).input_channel_id is not NULL
               and L_inv_invalid_tbl(i).valid_channel_id is not NULL
               and NVL(L_inv_invalid_tbl(i).input_channel_id,-1) <> NVL(L_inv_invalid_tbl(i).store_channel_id,-1)then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_STORE_CHANNEL',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            L_inv_invalid_tbl(i).input_channel_id,
                                                                            NULL)||';';
            end if;  
         end if; -- Location type is store
         if L_inv_invalid_tbl(i).loc_type = 'W' then 
           
            -- Channel id should be present
            if L_inv_invalid_tbl(i).input_channel_id is NULL then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('CHANNEL_ID_NULL',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            NULL,
                                                                            NULL)||';';
            end if;
      
            --Warehouse location is invalid
            if L_inv_invalid_tbl(i).valid_warehouse is NULL then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_WH_NOT_EXIST',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            NULL,
                                                                            NULL)||';';
            end if;   
            -- No virtual warehouse exists for the physical warehouse/channel combination
            if L_inv_invalid_tbl(i).valid_warehouse is not NULL
               and L_inv_invalid_tbl(i).input_channel_id is not NULL
               and L_inv_invalid_tbl(i).valid_channel_id is not NULL
               and NVL(L_inv_invalid_tbl(i).valid_wh_ind,'N') <> 'Y' then
               O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('INVAVAIL_INVALID_WH',
                                                                            TO_CHAR(L_inv_invalid_tbl(i).loc),
                                                                            L_inv_invalid_tbl(i).input_channel_id,
                                                                            NULL)||';';
            end if;
         end if; -- Location type is Warehouse
      end LOOP;
      if O_error_tbl is NOT NULL and O_error_tbl.COUNT > 0 then
         return FALSE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_VALIDATE%ISOPEN then
         close C_VALIDATE;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      
      return FALSE;   
     
END VALIDATE_INPUT;
-------------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_COST_STORE(O_error_message       OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error_tbl           OUT     SVCPROV_UTILITY.ERROR_TBL,
                             O_output_store        OUT     "RIB_PrcCostDesc_TBL",
                             I_input_tbl           IN      OBJ_INPUT_PRICECOST_TBL)
 RETURN BOOLEAN IS
 
   L_program                VARCHAR2(50) := 'CORESVC_PRICECOST_SQL.POPULATE_COST_STORE';
   
   cursor C_RECORD_NOT_EXIST is
      select item,
             fulfill_location
        from TABLE(CAST(I_input_tbl as OBJ_INPUT_PRICECOST_TBL))  input_tbl
       where not exists (select 1 
                           from future_cost fc
                          where item = input_tbl.item
                            and fc.supplier =  NVL(input_tbl.source_location,decode(primary_supp_country_ind,'Y',supplier))
                            and fc.origin_country_id = NVL(input_tbl.source_location_country,decode(primary_supp_country_ind,'Y',origin_country_id))
                            and fc.location = input_tbl.fulfill_location
                            and fc.loc_type = input_tbl.loc_type
                            and fc.active_date = (select max(active_date) 
                                                    from future_cost 
                                                   where item = input_tbl.item 
                                                     and location = input_tbl.fulfill_location
                                                     and active_date <= NVL(input_tbl.active_date,get_vdate)))
         and input_tbl.loc_type = 'S';
   
   cursor C_GET_COST_STORE is
      select "RIB_PrcCostDesc_REC"(0,
                                   input_tbl.item,
                                   input_tbl.source_location,
                                   input_tbl.source_location_country,
                                   input_tbl.fulfill_location,
                                   input_tbl.loc_type,
                                   input_tbl.channel_id,
                                   input_tbl.active_date,
                                   nvl(fc.pricing_cost,0))
                              from TABLE(CAST(I_input_tbl as OBJ_INPUT_PRICECOST_TBL))  input_tbl, 
                                   future_cost fc
                             where fc.item = input_tbl.item
                               and fc.supplier =  NVL(input_tbl.source_location,decode(primary_supp_country_ind,'Y',supplier))
                               and fc.origin_country_id = NVL(input_tbl.source_location_country,decode(primary_supp_country_ind,'Y',origin_country_id))
                               and fc.location = input_tbl.fulfill_location
                               and fc.loc_type = input_tbl.loc_type
                               and input_tbl.loc_type = 'S'
                               and fc.active_date = (select max(active_date) 
                                                       from future_cost 
                                                      where item = input_tbl.item 
                                                        and location = input_tbl.fulfill_location
                                                        and active_date <= NVL(input_tbl.active_date,get_vdate));
   
   TYPE INVALID_TBL IS TABLE OF C_RECORD_NOT_EXIST%ROWTYPE INDEX BY BINARY_INTEGER;
   L_invalid_tbl INVALID_TBL;
   

BEGIN
   O_error_tbl := SVCPROV_UTILITY.ERROR_TBL();
  
   open C_RECORD_NOT_EXIST;
   fetch C_RECORD_NOT_EXIST BULK COLLECT into L_invalid_tbl;
   close C_RECORD_NOT_EXIST;
 
   if L_invalid_tbl.COUNT > 0  and L_invalid_tbl is NOT NULL then
      for i in L_invalid_tbl.FIRST..L_invalid_tbl.LAST LOOP
         O_error_tbl.EXTEND();
         O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('PRICE_COST_NOT_EXIST',
                                                                      L_invalid_tbl(i).item,
                                                                      L_invalid_tbl(i).fulfill_location,
                                                                      NULL)||';';
      END LOOP;
   end if;
   if O_error_tbl.COUNT > 0 and O_error_tbl is NOT NULL then
      return FALSE;
   end if;   
   
   open C_get_cost_store;
   fetch C_get_cost_store BULK COLLECT into O_output_store;
   close C_get_cost_store; 
   
   return TRUE;
    

EXCEPTION
   when OTHERS then
      if C_GET_COST_STORE%ISOPEN then
         close C_GET_COST_STORE;
      end if;
       if C_RECORD_NOT_EXIST%ISOPEN then
          close C_RECORD_NOT_EXIST;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));

      return FALSE; 
END  POPULATE_COST_STORE;
----------------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_COST_WH(O_error_message     OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error_tbl         OUT      SVCPROV_UTILITY.ERROR_TBL,
                          O_output_wh         OUT      "RIB_PrcCostDesc_TBL",
                          I_input_tbl         IN       OBJ_INPUT_PRICECOST_TBL)
 RETURN BOOLEAN IS
 
   L_program                VARCHAR2(50)             := 'CORESVC_PRICECOST_SQL.POPULATE_COST_WH';
   L_wh_pricecost_tbl       OBJ_WH_PRICECOST_TBL     := OBJ_WH_PRICECOST_TBL();
   L_output_single_wh       "RIB_PrcCostDesc_TBL"    := "RIB_PrcCostDesc_TBL"();
   L_output_multiple_wh     "RIB_PrcCostDesc_TBL"    := "RIB_PrcCostDesc_TBL"();
   L_wh_exist               BOOLEAN := FALSE;
 
   cursor C_WH_COUNT is
      select  OBJ_WH_PRICECOST_REC(item,
                                   source_location,
                                   source_location_country,
                                   physical_wh,
                                   vwh,
                                   loc_type,
                                   channel_id,
                                   active_date,
                                   wh_count
                                  ) from(select input_tbl.item,
                                                input_tbl.source_location,
                                                input_tbl.source_location_country,
                                                input_tbl.fulfill_location physical_wh,
                                                wh.wh vwh,
                                                input_tbl.loc_type,
                                                input_tbl.channel_id ,
                                                input_tbl.active_date,
                                                count(*) over (partition by wh.physical_wh) wh_count
                                           from TABLE(CAST(I_input_tbl as OBJ_INPUT_PRICECOST_TBL))  input_tbl, 
                                                wh wh
                                          where wh.physical_wh(+) = input_tbl.fulfill_location
                                            and input_tbl.loc_type = 'W'
                                            and wh.customer_order_loc_ind(+) = 'Y'
                                            and wh.channel_id(+) = input_tbl.channel_id);                                            
                                            


   cursor C_NOT_EXIST_SINGLE_WH is 
      select item,
             physical_wh
        from TABLE(CAST(L_wh_pricecost_tbl as OBJ_WH_PRICECOST_TBL)) wh_tbl
       where not exists(select 1
                          from future_cost fc
                         where item =  wh_tbl.item
                           and location = wh_tbl.vwh
                           and fc.item = wh_tbl.item
                           and fc.supplier =  NVL(wh_tbl.source_location,decode(primary_supp_country_ind,'Y',supplier))
                           and fc.origin_country_id = NVL(wh_tbl.source_location_country,decode(primary_supp_country_ind,'Y',origin_country_id))
                           and fc.location = wh_tbl.vwh
                           and fc.loc_type = wh_tbl.loc_type
                           and fc.active_date = (select max(active_date) 
                                                   from future_cost 
                                                  where item = wh_tbl.item 
                                                    and location = wh_tbl.vwh
                                                    and active_date <= NVL(wh_tbl.active_date,get_vdate)))
         and wh_tbl.loc_type = 'W'
         and wh_tbl.wh_count =  1;                                                
   
   cursor C_COST_SINGLE_WH is
      select "RIB_PrcCostDesc_REC"(0,
                                   wh_tbl.item,
                                   wh_tbl.source_location,
                                   wh_tbl.source_location_country,
                                   wh_tbl.physical_wh,
                                   wh_tbl.loc_type,
                                   wh_tbl.channel_id,
                                   wh_tbl.active_date,
                                   nvl(fc.pricing_cost,0))
                              from TABLE(CAST(L_wh_pricecost_tbl as OBJ_WH_PRICECOST_TBL)) wh_tbl,
                                   future_cost fc
                             where fc.item = wh_tbl.item
                               and fc.supplier =  NVL(wh_tbl.source_location,decode(primary_supp_country_ind,'Y',supplier))
                               and fc.origin_country_id = NVL(wh_tbl.source_location_country,decode(primary_supp_country_ind,'Y',origin_country_id))
                               and fc.location = wh_tbl.vwh
                               and fc.loc_type = wh_tbl.loc_type
                               and wh_tbl.loc_type = 'W'
                               and wh_tbl.wh_count =  1
                               and fc.active_date = (select max(active_date) 
                                                       from future_cost 
                                                      where item = wh_tbl.item 
                                                        and location = wh_tbl.vwh
                                                        and active_date <= NVL(wh_tbl.active_date,get_vdate));
   cursor C_NOT_EXIST_MULTIP_WH is
   with wh_tbl as
   (
     select item,
            source_location,
            source_location_country,
            fulfill_location,
            loc_type,
            channel_id,
            active_date,
            wh_count,
            vwh
      from (
            select distinct input_tbl.item,
                            input_tbl.source_location,
                            input_tbl.source_location_country,
                            input_tbl.physical_wh fulfill_location,
                            input_tbl.loc_type,
                            input_tbl.channel_id,
                            input_tbl.active_date,
                            input_tbl.wh_count,
                            wh1.wh vwh,
                            RANK() OVER (PARTITION BY wh1.physical_wh,
                                                      wh1.channel_id
                                             ORDER BY wh1.protected_ind asc,
                                                 decode(wh2.primary_vwh,wh1.wh,1,0) desc,
                                                        wh1.wh asc) wh_rank
                       from TABLE(CAST(L_wh_pricecost_tbl as OBJ_WH_PRICECOST_TBL)) input_tbl,
                            wh wh1,
                            wh wh2
                      where wh1.customer_order_loc_ind = 'Y'      
                        and wh2.wh = wh1.physical_wh
                        and wh1.physical_wh =  input_tbl.physical_wh
                       and wh1.channel_id = input_tbl.channel_id
          )
    where wh_rank = 1
   )
   select item,
          fulfill_location
     from wh_tbl wh_tbl
    where NOT EXISTS(select 1
                       from future_cost fc
                      where fc.item = wh_tbl.item
                        and fc.supplier =  NVL(wh_tbl.source_location,decode(primary_supp_country_ind,'Y',supplier))
                        and fc.origin_country_id = NVL(wh_tbl.source_location_country,decode(primary_supp_country_ind,'Y',origin_country_id))
                        and fc.location = wh_tbl.vwh
                        and fc.loc_type = wh_tbl.loc_type
                        and fc.active_date = (select max(active_date) 
                                                     from future_cost 
                                                    where item = wh_tbl.item 
                                                      and location = wh_tbl.vwh
                                                      and active_date <= NVL(wh_tbl.active_date,get_vdate)) )
      and wh_tbl.loc_type = 'W'
      and wh_tbl.wh_count >  1;
                                                                               
   cursor C_COST_MULT_WH is   
   with wh_tbl as
   (
     select item,
            source_location,
            source_location_country,
            fulfill_location,
            loc_type,
            channel_id,
            active_date,
            wh_count,
            vwh
      from (
            select distinct input_tbl.item,
                   input_tbl.source_location,
                   input_tbl.source_location_country,
                   input_tbl.physical_wh fulfill_location,
                   input_tbl.loc_type,
                   input_tbl.channel_id,
                   input_tbl.active_date,
                   input_tbl.wh_count,
                   wh1.wh vwh,
                   RANK() OVER (PARTITION BY wh1.physical_wh,
                                             wh1.channel_id
                                    ORDER BY wh1.protected_ind asc,
                                             decode(wh2.primary_vwh,wh1.wh,1,0) desc,
                                             wh1.wh asc) wh_rank
              from TABLE(CAST(L_wh_pricecost_tbl as OBJ_WH_PRICECOST_TBL)) input_tbl,
                   wh wh1,
                   wh wh2
             where wh1.customer_order_loc_ind = 'Y'      
               and wh2.wh = wh1.physical_wh
               and wh1.physical_wh =  input_tbl.physical_wh
               and wh1.channel_id = input_tbl.channel_id
           )
    where wh_rank = 1                                           
   )
   select  "RIB_PrcCostDesc_REC"(0,
                                 wh_tbl.item,
                                 wh_tbl.source_location,
                                 wh_tbl.source_location_country,
                                 wh_tbl.fulfill_location,
                                 wh_tbl.loc_type,
                                 wh_tbl.channel_id,
                                 wh_tbl.active_date,
                                 nvl(fc.pricing_cost,0))
                            from wh_tbl,
                                 future_cost fc
                           where fc.item = wh_tbl.item
                             and fc.supplier =  NVL(wh_tbl.source_location,decode(primary_supp_country_ind,'Y',supplier))
                             and fc.origin_country_id = NVL(wh_tbl.source_location_country,decode(primary_supp_country_ind,'Y',origin_country_id))
                             and fc.location = wh_tbl.vwh
                             and fc.loc_type = wh_tbl.loc_type
                             and wh_tbl.loc_type = 'W'
                             and wh_tbl.wh_count >  1
                             and fc.active_date = (select max(active_date) 
                                                     from future_cost 
                                                    where item = wh_tbl.item 
                                                      and location = wh_tbl.vwh
                                                      and active_date <= NVL(wh_tbl.active_date,get_vdate)); 
   
   TYPE INVALID_TBL IS TABLE OF C_NOT_EXIST_SINGLE_WH%ROWTYPE INDEX BY BINARY_INTEGER;
   L_invalid_tbl        INVALID_TBL;
   
   TYPE INVALID_TBL_WH IS TABLE OF C_NOT_EXIST_MULTIP_WH%ROWTYPE INDEX BY BINARY_INTEGER;
   L_invalid_tbl_wh     INVALID_TBL_WH;  
                                                   
                                   

BEGIN
 
   open C_WH_COUNT;
   fetch C_WH_COUNT BULK COLLECT into L_wh_pricecost_tbl;
   close C_WH_COUNT;

   O_error_tbl := SVCPROV_UTILITY.ERROR_TBL();
   
   --if the virtual wh is not exist for the physical_which is customer orderbale
  
   if L_wh_pricecost_tbl.COUNT > 0 and L_wh_pricecost_tbl is not NULL then
      for i in L_wh_pricecost_tbl.FIRST..L_wh_pricecost_tbl.LAST LOOP
         if L_wh_pricecost_tbl(i).vwh is NULL then 
            O_error_tbl.EXTEND();
            O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('PRICE_COST_NOT_EXIST',
                                                                         L_wh_pricecost_tbl(i).item,
                                                                         L_wh_pricecost_tbl(i).physical_wh,
                                                                         NULL)||';';
            return FALSE;
         end if; 
      END LOOP;
   end if;
   if O_error_tbl.COUNT > 0 and O_error_tbl is NOT NULL then
      return FALSE;
   end if;   
   
   --if only one vwh exist
   open C_NOT_EXIST_SINGLE_WH;
   fetch C_NOT_EXIST_SINGLE_WH BULK COLLECT into L_invalid_tbl;
   close C_NOT_EXIST_SINGLE_WH; 
  
   if L_invalid_tbl.COUNT > 0 and L_invalid_tbl is NOT NULL then
      for i in L_invalid_tbl.FIRST..L_invalid_tbl.LAST LOOP
         O_error_tbl.EXTEND();
         O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('PRICE_COST_NOT_EXIST',
                                                                      L_invalid_tbl(i).item,
                                                                      L_invalid_tbl(i).physical_wh,
                                                                      NULL)||';';   
                                                                   
      END LOOP; 
      if O_error_tbl.COUNT > 0 and O_error_tbl is NOT NULL then
         return FALSE;
      end if; 
   end if;
   
   open C_COST_SINGLE_WH;
   fetch C_COST_SINGLE_WH BULK COLLECT into L_output_single_wh;
   close C_COST_SINGLE_WH;
   ---
   --if multiple vwh exist
   open C_NOT_EXIST_MULTIP_WH;
   fetch C_NOT_EXIST_MULTIP_WH BULK COLLECT into L_invalid_tbl_wh;
   close C_NOT_EXIST_MULTIP_WH; 
  
   if L_invalid_tbl_wh.COUNT > 0 and L_invalid_tbl_wh is NOT  NULL then
      for i in L_invalid_tbl_wh.FIRST..L_invalid_tbl_wh.LAST LOOP
         O_error_tbl.EXTEND();
         O_error_tbl(i) := O_error_tbl(i) || SQL_LIB.GET_MESSAGE_TEXT('PRICE_COST_NOT_EXIST',
                                                                      L_invalid_tbl_wh(i).item,
                                                                      L_invalid_tbl_wh(i).fulfill_location,
                                                                      NULL)||';';   
      END LOOP;
      if O_error_tbl.COUNT > 0 and O_error_tbl is NOT NULL then
         return FALSE;
      end if;
   end if; 
   open C_COST_MULT_WH;
   fetch C_COST_MULT_WH BULK COLLECT into L_output_multiple_wh;
   close C_COST_MULT_WH;
   
   O_output_wh := L_output_single_wh MULTISET UNION L_output_multiple_wh;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_WH_COUNT%ISOPEN then
         close C_WH_COUNT;
      end if;
      if C_COST_SINGLE_WH%ISOPEN then
         close C_COST_SINGLE_WH;
      end if;
      if C_COST_MULT_WH%ISOPEN then
         close C_COST_MULT_WH;
      end if;
      if C_NOT_EXIST_SINGLE_WH%ISOPEN then
         close C_NOT_EXIST_SINGLE_WH;
      end if;
      if C_NOT_EXIST_MULTIP_WH%ISOPEN then
         close C_NOT_EXIST_MULTIP_WH;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      
      return FALSE;  
END  POPULATE_COST_WH;
----------------------------------------------------------------------------------------------------------------
END CORESVC_PRICECOST_SQL;
/

