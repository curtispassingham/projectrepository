
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY WAREHOUSE_VALIDATE_SQL AS
--------------------------------------------------------------------
FUNCTION EXIST(	O_error_message	IN OUT	VARCHAR2,
		I_wh		IN	NUMBER,
		O_exist		IN OUT	BOOLEAN)
	RETURN BOOLEAN IS

   L_dummy	VARCHAR2(1);

   cursor C_EXIST is
	select 'x'
	from WH
	where WH = I_wh;

BEGIN
   open C_EXIST;
   fetch C_EXIST into L_dummy;  
   if C_EXIST%NOTFOUND then
	O_exist := FALSE;
	O_error_message := sql_lib.create_msg('INV_WH',
						null,null,null);
   else
	O_exist := TRUE;
   end if;
   close C_EXIST;

   return TRUE;

EXCEPTION
   when OTHERS then
	O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
						SQLERRM,
						'WAREHOUSE_VALIDATE_SQL.EXIST',
						to_char(SQLCODE));
	return FALSE;
END EXIST;
---------------------------------------------------------------------
END WAREHOUSE_VALIDATE_SQL;
/


