
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SYSTEM_CALENDAR_SQL AUTHID CURRENT_USER AS


---------------------------------------------------------------------------
-- Function Name: GET_HALF_NO
-- Purpose  : This function gets the half number from the period table.
-- Called by   : 
-- Calls : <none>
-- Input Value : O_error_message,
--               O_half_no
--
-- Created  : 31-JUL-96 by Matt Sniffen
---------------------------------------------------------------------------
FUNCTION GET_HALF_NO (O_error_message   IN OUT VARCHAR2,
                      O_half_no     IN OUT NUMBER)
   return BOOLEAN;

---------------------------------------------------------------------------
-- Function Name: GET_CAL_OPTS
-- Purpose  : This function gets calendar_454_ind from the system_options 
--                table.
-- Called by   : 
-- Calls : <none>
-- Input Values   : O_error_message,
--      O_cal_ind,
--                O_start_of_half_month,
--                O_start_last_year 
--
-- Created  : 31-JUL-96 by Matt Sniffen
---------------------------------------------------------------------------
FUNCTION GET_CAL_OPTS ( O_error_message         IN OUT VARCHAR2,
                        O_cal_ind         IN OUT VARCHAR2,
                        O_start_of_half_month IN OUT NUMBER,
                        O_start_last_year     IN OUT NUMBER)
   return BOOLEAN;


END SYSTEM_CALENDAR_SQL;
/


