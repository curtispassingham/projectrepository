CREATE OR REPLACE PACKAGE LOCATION_ATTRIB_SQL AUTHID CURRENT_USER AS

TYPE locs_table_type IS
   TABLE OF store.store%TYPE INDEX BY BINARY_INTEGER;
--------------------------------------------------------------------------------------------
--Function Name: GET_TYPE
--Purpose:       This function will return the location type (S or W) for a given location.
--Calls:         None
--Created:       07-MAY-98, by Sonia Wong
--------------------------------------------------------------------------------------------
FUNCTION GET_TYPE(O_error_message IN OUT    VARCHAR2,
                  O_loc_type      IN OUT    VARCHAR2,
                  I_location      IN        STORE.STORE%TYPE)
                  return BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: GET_NAME
--Purpose:       This function will return the description of a location within the system.
--Calls:         LOCATION_ATTRIB_SQL.GET_TYPE, STORE_ATTRIB_SQL.GET_NAME,
--               WH_ATTRIB_SQL.GET_NAME
--Created:       12-MAY-98, by Sonia Wong
--------------------------------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message IN OUT   VARCHAR2,
                  O_loc_name      IN OUT   VARCHAR2,
                  I_location      IN       VARCHAR2,
                  I_loc_type      IN       VARCHAR2)
                  return BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: BUILD_GROUP_TYPE_WHERE_CLAUSE
--Purpose:       This function will dynamically build a where clause based on a location
--               group type and a group value.
--Calls:         locclose.fmb
--Created:       18-JAN-00, by Yumen Li

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------------------
FUNCTION BUILD_GROUP_TYPE_WHERE_CLAUSE(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_where_clause  IN OUT   VARCHAR2,
                                       I_group_type    IN       VARCHAR2,
                                       I_group_value   IN       VARCHAR2)
                  return BOOLEAN;

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_EXTERNAL_FINISHER V_INTERNAL_FINISHER V_STORE V_WH
-- which only returns data that the user has permission to access.  

FUNCTION BUILD_GROUP_TYPE_WHERE_CLAUSE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_where_clause    IN OUT   VARCHAR2,
                                       I_group_type      IN       VARCHAR2,
                                       I_group_value     IN       VARCHAR2,
                                       I_loc_or_location IN       VARCHAR2)
                 return BOOLEAN;
--------------------------------------------------------------------------------------------
--Function Name: BUILD_GROUP_TYPE_VIRTUAL_WHERE
--Purpose:       This function will dynamically build a where clause based on a location
--               group type and a group value.
--Calls:         locclose.fmb,  cmclexcp.fmb
--Created:       14-FEB-01, by Luan Nguyen

-- note: 
-- Only call this function with online forms to control what data the user can
-- see or use and do not call the function from batch.  This function retrieves
-- data from:
--    V_WH
-- which only returns data that the user has permission to access.  
--------------------------------------------------------------------------------------------
FUNCTION BUILD_GROUP_TYPE_VIRTUAL_WHERE(O_error_message   IN OUT   VARCHAR2,
                                        O_where_clause    IN OUT   VARCHAR2,
                                        I_loc_or_location IN       VARCHAR2,
                                        I_group_type      IN       VARCHAR2,
                                        I_group_value     IN       VARCHAR2)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION GET_LOCATION_CURRENCY(O_error_message         OUT VARCHAR2,
                               O_multi_currency_exists OUT BOOLEAN,
                               O_currency_code         OUT currencies.currency_code%TYPE,
                               I_group_type            IN  code_detail.code%TYPE,
                               I_group_value           IN  VARCHAR2)
return BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION LOC_CURRENCY_EXISTS(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        OUT BOOLEAN,
                             I_group_type    IN  code_detail.code%TYPE,
                             I_group_value   IN  VARCHAR2,
                             I_currency_code IN  store.currency_code%TYPE)
return BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: UPDATE_VWH
-- Purpose      : This function will update the warehouse address and
--                warehouse indicators for all virtual warehouse that
--                attached to the physical warehouse
-- Created by   : Luan Nguyen, February 2001 for Multi Channel

FUNCTION UPDATE_VWH(O_error_message   IN  OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_wh              IN      WH.WH%TYPE,    
                    I_email           IN      WH.EMAIL%TYPE,
                    I_vat_region      IN      WH.VAT_REGION%TYPE,
                    I_org_hier_type   IN      WH.ORG_HIER_TYPE%TYPE,
                    I_org_hier_value  IN      WH.ORG_HIER_VALUE%TYPE,
                    I_currency_code   IN      WH.CURRENCY_CODE%TYPE,
                    I_break_pack_ind  IN      WH.BREAK_PACK_IND%TYPE,
                    I_redist_wh_ind   IN      WH.REDIST_WH_IND%TYPE,
                    I_delivery_policy IN      WH.DELIVERY_POLICY%TYPE,
                    I_duns_number     IN      WH.DUNS_NUMBER%TYPE,
                    I_duns_loc                IN   WH.DUNS_LOC%TYPE,
                    I_inbound_handling_days   IN   WH.INBOUND_HANDLING_DAYS%TYPE
                    )
   RETURN BOOLEAN;
------------------------------------------------------------------------
-- Function Name: check_stockholding
-- Purpose      : call check_stockholding below
-- Created by   : Luan Nguyen, February 2001 for Multi Channel

FUNCTION check_stockholding(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_stockhold_ind IN OUT WH.STOCKHOLDING_IND%TYPE,
                            I_loc           IN     VARCHAR2,
                            I_loc_type      IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Function Name: check_stockholding
-- Purpose      : This will check if the location is stockholding
--                If loc type is a store then check store else check
--                warehouse if it is a warehouse
-- Created by   : Luan Nguyen, February 2001 for Multi Channel

FUNCTION check_stockholding(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_stockhold_ind IN OUT WH.STOCKHOLDING_IND%TYPE,
                            O_loc_name      IN OUT VARCHAR2,
                            I_loc           IN     VARCHAR2,
                            I_loc_type      IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Function Name:  check_item_loc_hist
-- Purpose      :  This will check if the location exists on the
--                 item_loc_hist table
-- Created by   :  Ladi Okuneye, June 2001 for Multi Channel

FUNCTION check_item_loc_hist(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_loc_name          IN OUT WH.WH_NAME%TYPE,
                             I_loc               IN VARCHAR2,
                             I_loc_type          IN VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name: get_entity
-- Purpose      : This will get the TSF Entity Id assigned to the store, warehouse,
--                or external finisher, depending on the loc type.

FUNCTION GET_ENTITY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_entity_id       IN OUT TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                    O_entity_name     IN OUT TSF_ENTITY.TSF_ENTITY_DESC%TYPE,
                    I_loc             IN     VARCHAR2,
                    I_loc_type        IN     VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name: get_finisher_name
-- Purpose      : For finisher type of internal finisher this will check that the
--                finisher is a valid virtual warehouse [and that it is specified
--                as a finisher].  For finisher type of external finisher this will
--                check that the finisher is a valid partner with partner type of
--                external finisher.

FUNCTION GET_FINISHER_NAME(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists        IN OUT BOOLEAN,
                           O_finisher_name IN OUT PARTNER.PARTNER_DESC%TYPE,
                           I_finisher      IN     VARCHAR2,
                           I_finisher_type IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Function Name:  id_exists
-- Purpose      :  This will check if the location id exists on the
--                 store, store_add, wh and partner tables
-- Created by   :  Rochelle Paz, June 2003 for GAP Mods

FUNCTION ID_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists           IN OUT   BOOLEAN,
                    I_location_id      IN       NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Function Name: check_tsf_entity_change
-- Purpose      : This function will check if a location/external finisher is
--                associated with pending transfers, orders or allocations.
--                The function also check if a location/external finisher has
--                stock on hand or quantities expected/being transferred.

FUNCTION CHECK_TSF_ENTITY_CHANGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_ok_to_change  IN OUT BOOLEAN,
                                 I_location_id   IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name: CHECK_CLOSE DATE
-- Purpose      : This function will check for a location warehouse or store is closed or not
FUNCTION CHECK_CLOSE_DATE(I_store_wh        IN LOCATION_CLOSED.LOCATION%TYPE,
                          I_loc_type        IN LOCATION_CLOSED.LOC_TYPE%TYPE,
                          I_date            IN DATE,
                          O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_open_close_ind  IN OUT VARCHAR2)
RETURN BOOLEAN ;
--------------------------------------------------------------------------
-- Function Name: CHECK_STORE_WH
-- Purpose      : This function will check if there exists any Store or Warehouse.
--                When not a single store/wh is found, O_exists returns FALSE.
FUNCTION CHECK_STORE_WH(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists          IN OUT BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function Name: GET_LOC_COUNTRY
-- Purpose      : This function will retrive the Country for the Location passed in
FUNCTION GET_LOC_COUNTRY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_country_id      IN OUT COUNTRY.COUNTRY_ID%TYPE,
                         I_store_wh        IN     LOCATION_CLOSED.LOCATION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
END LOCATION_ATTRIB_SQL;
/
