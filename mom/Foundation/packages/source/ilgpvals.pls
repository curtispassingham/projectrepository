CREATE OR REPLACE PACKAGE ITEM_LOC_GROUP_VALIDATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------
-- Name: GROUPS
-- Description:  This functions calls the validation for
--               a location group type depending on the
--               passed in group type 
-- Called Functions: STORES
--                   STORE_CLASS
--                   DISTRICTS
--                   REGIONS
--                   PROMOZONE
--                   TZONE
--                   VAL_LOC_TRAITS
--                   DEFAULT_WH
--                   WAREHOUSE
--                   ZONE_GROUP
--                   AREAS
--  Created by:  Greg Nathe 
--  Date:        1-APR-97 
---------------------------------------------------
   FUNCTION GROUP_TYPE (O_error_message     IN OUT VARCHAR2,
                        I_group_type        IN     VARCHAR2,
                        I_item_level        IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                        I_tran_level        IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                        I_item              IN     ITEM_MASTER.ITEM%TYPE,
                        I_value             IN     VARCHAR2,
                        O_value_desc        IN OUT VARCHAR2,
                        O_exist_ind         IN OUT BOOLEAN)
   return BOOLEAN;
-------------------------------------------------
-- Name: STORES_REPL
-- Description:  This function validates the existence
--               of a passed in store and item combination
-- Called Functions: none
-- Created by:  Greg Nathe 
-- Date:        1-APR-97 
---------------------------------------------------
   FUNCTION STORES_REPL(O_error_message     IN OUT VARCHAR2,
                   I_item_level             IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                   I_tran_level             IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                   I_item                   IN     ITEM_MASTER.ITEM%TYPE,
                   I_value                  IN     STORE.STORE%TYPE,
                   O_value_desc             IN OUT STORE.STORE_NAME%TYPE,
                   O_exist_ind              IN OUT BOOLEAN)
   return BOOLEAN;
-------------------------------------------------
-- Name: STORE_CLASS
-- Description:  This functions validates the existence of
--               a passed in store class and item combination
-- Called Functions: none
-- Created by:  Greg Nathe 
-- Date:        1-APR-97 
---------------------------------------------------
   FUNCTION STORE_CLASS(O_error_message      IN OUT VARCHAR2,
                        I_item_level         IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                        I_tran_level         IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                        I_item               IN     ITEM_MASTER.ITEM%TYPE,
                        I_value              IN     VARCHAR2,
                        O_value_desc         IN OUT VARCHAR2,
                        O_exist_ind          IN OUT BOOLEAN)
   return BOOLEAN;
-------------------------------------------------
-- Name: DISTRICTS
-- Description:  This functions validates the existence of
--               a passed in district and item combination
-- Called Functions: none
-- Created by:  Greg Nathe 
-- Date:        1-APR-97 
---------------------------------------------------
   FUNCTION DISTRICTS(O_error_message      IN OUT VARCHAR2,
                      I_item_level         IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                      I_tran_level         IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                      I_item               IN     ITEM_MASTER.ITEM%TYPE,
                      I_value              IN     DISTRICT.DISTRICT%TYPE,
                      O_value_desc         IN OUT DISTRICT.DISTRICT_NAME%TYPE,
                      O_exist_ind          IN OUT BOOLEAN)
   return BOOLEAN;
-------------------------------------------------
-- Name: REGIONS
-- Description:  This functions validates the existence of
--               a passed in region and item combination
-- Called Functions: none
-- Created by:  Greg Nathe 
-- Date:        1-APR-97 
---------------------------------------------------
   FUNCTION REGIONS(O_error_message        IN OUT VARCHAR2,
                    I_item_level           IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                    I_tran_level           IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                    I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                    I_value                IN     REGION.REGION%TYPE,
                    O_value_desc           IN OUT REGION.REGION_NAME%TYPE,
                    O_exist_ind            IN OUT BOOLEAN)
   return BOOLEAN;
-------------------------------------------------


-------------------------------------------------
-- Name: TZONE
-- Description:  This functions validates the existence of
--               a passed in transfer zone and item combination
-- Called Functions: none
-- Created by:  Greg Nathe 
-- Date:        1-APR-97 
---------------------------------------------------
   FUNCTION TZONE(O_error_message       IN OUT VARCHAR2,
                  I_item_level          IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                  I_tran_level          IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                  I_item                IN     ITEM_MASTER.ITEM%TYPE,
                  I_value               IN     VARCHAR2,
                  O_value_desc          IN OUT VARCHAR2,
                  O_exist_ind           IN OUT BOOLEAN)
   return BOOLEAN;
-------------------------------------------------
-- Name: VAL_LOC_TRAITS
-- Description:  This functions validates the existence of
--               a passed in location trait and item combination
-- Called Functions: none
-- Created by:  Greg Nathe 
-- Date:        1-APR-97 
---------------------------------------------------
   FUNCTION VAL_LOC_TRAITS(O_error_message       IN OUT VARCHAR2,
                           I_item_level          IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                           I_tran_level          IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                           I_item                IN     ITEM_MASTER.ITEM%TYPE,
                           I_value               IN     VARCHAR2,
                           O_value_desc          IN OUT VARCHAR2,
                           O_exist_ind           IN OUT BOOLEAN)
   return BOOLEAN;
-------------------------------------------------
-- Name: DEFAULT_WH 
-- Description:  This functions validates the existence of
--               a passed in default wh and item combination
-- Called Functions: none
-- Created by:  Greg Nathe 
-- Date:        1-APR-97 
---------------------------------------------------
   FUNCTION DEFAULT_WAREHOUSE (O_error_message       IN OUT VARCHAR2,
                               I_item_level          IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                               I_tran_level          IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                               I_item                IN     ITEM_MASTER.ITEM%TYPE,
                               I_value               IN     VARCHAR2,
                               O_value_desc          IN OUT VARCHAR2,
                               O_exist_ind           IN OUT BOOLEAN)
   return BOOLEAN;
-------------------------------------------------
-- Name: WAREHOUSE 
-- Description:  This functions validates the existence of
--               a passed in warehouse and item combination
-- Called Functions: none
-- Created by:  Greg Nathe 
-- Date:        1-APR-97 
---------------------------------------------------
   FUNCTION WAREHOUSE (O_error_message       IN OUT VARCHAR2,
                       I_item_level          IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level          IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_item                IN     ITEM_MASTER.ITEM%TYPE,
                       I_value               IN     VARCHAR2,
                       O_value_desc          IN OUT VARCHAR2,
                       O_exist_ind           IN OUT BOOLEAN)
   return BOOLEAN;
---------------------------------------------------
-- Name: LOC_LIST_WH 
-- Description: This function will validate that the location
--              list entered has at least one warehouse that
--              is stockholding 
-- Created by:  Luan Nguyen 
-- Date:        February 2001 
---------------------------------------------------
   FUNCTION LOC_LIST_WH (O_error_message IN OUT VARCHAR2,
                         I_item_level    IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                         I_tran_level    IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE,
                         I_value         IN     VARCHAR2,
                         O_value_desc    IN OUT VARCHAR2,
                         O_exist_ind     IN OUT BOOLEAN)
   return BOOLEAN;
---------------------------------------------------
-- Name: LOC_LIST_ST 
-- Description: This function will validate that the location
--              list entered has at least one store that is 
--              stockholding 
-- Created by:  Luan Nguyen 
-- Date:        February 2001 
---------------------------------------------------
   FUNCTION LOC_LIST_ST (O_error_message IN OUT VARCHAR2,
                         I_item_level    IN     ITEM_MASTER.ITEM_LEVEL%TYPE,
                         I_tran_level    IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE,
                         I_value         IN     VARCHAR2,
                         O_value_desc    IN OUT VARCHAR2,
                         O_exist_ind     IN OUT BOOLEAN)
   return BOOLEAN;
---------------------------------------------------
-- Name: AREAS
-- Description:  This functions validates the existence of
--               a passed in area and item combination
-- Called Functions: none
-- Created by:  Richmond Gocheco 
-- Date:        December 8, 2004
---------------------------------------------------
   FUNCTION AREAS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_item_level      IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                  I_tran_level      IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                  I_item            IN       ITEM_MASTER.ITEM%TYPE,
                  I_value           IN       AREA.AREA%TYPE,
                  O_value_desc      IN OUT   AREA.AREA_NAME%TYPE,
                  O_exist_ind       IN OUT   BOOLEAN)
   return BOOLEAN;
-------------------------------------------------
FUNCTION GROUP_TYPE_HIER (O_error_message     IN OUT VARCHAR2,
                           I_group_type        IN     VARCHAR2,
                           I_dept              IN     ITEM_MASTER.DEPT%TYPE,
                           I_class             IN     ITEM_MASTER.CLASS%TYPE,
                           I_subclass          IN     ITEM_MASTER.SUBCLASS%TYPE,
                           I_value             IN     VARCHAR2,
                           O_value_desc        IN OUT VARCHAR2,
                           O_exist_ind         IN OUT BOOLEAN)
return BOOLEAN;     
--------------------------------------------------------------------------
END;
/
