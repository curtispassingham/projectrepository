-- File Name : CORESVC_OUTLOC_SPEC.PLS
CREATE OR REPLACE PACKAGE CORESVC_OUTLOC AUTHID CURRENT_USER AS
   template_key        CONSTANT VARCHAR2(255) := 'OUTLOC_DATA';
   template_category   CONSTANT VARCHAR2(255) := 'RMSFND';
   action_new                   VARCHAR2(25)  := 'NEW';
   action_mod                   VARCHAR2(25)  := 'MOD';
   action_del                   VARCHAR2(25)  := 'DEL';
   OUTLOC_sheet                 VARCHAR2(255) := 'OUTLOC';
   OUTLOC$Action                NUMBER        := 1;
   OUTLOC$OUTLOC_TYPE           NUMBER        := 2;
   OUTLOC$OUTLOC_ID             NUMBER        := 3;
   OUTLOC$OUTLOC_DESC           NUMBER        := 4;
   OUTLOC$OUTLOC_CURRENCY       NUMBER        := 5;
   OUTLOC$OUTLOC_ADD1           NUMBER        := 6;
   OUTLOC$OUTLOC_ADD2           NUMBER        := 7;
   OUTLOC$OUTLOC_CITY           NUMBER        := 8;
   OUTLOC$OUTLOC_STATE          NUMBER        := 9;
   OUTLOC$OUTLOC_COUNTRY_ID     NUMBER        :=10;
   OUTLOC$OUTLOC_POST           NUMBER        :=11;
   OUTLOC$OUTLOC_VAT_REGION     NUMBER        :=12;
   OUTLOC$CONTACT_NAME          NUMBER        :=13;
   OUTLOC$CONTACT_PHONE         NUMBER        :=14;
   OUTLOC$CONTACT_FAX           NUMBER        :=15;
   OUTLOC$CONTACT_TELEX         NUMBER        :=16;
   OUTLOC$CONTACT_EMAIL         NUMBER        :=17;
   OUTLOC$PRIMARY_IND           NUMBER        :=18;
   OUTLOC$OUTLOC_NAME_SEC       NUMBER        :=19;
   OUTLOC$OUTLOC_JUR_CODE       NUMBER        :=20;
   
   OUTLOC_TL_sheet                 VARCHAR2(255) := 'OUTLOC_TL';
   OUTLOC_TL$Action                NUMBER        := 1;
   OUTLOC_TL$LANG                  NUMBER        := 2;
   OUTLOC_TL$OUTLOC_TYPE           NUMBER        := 3;
   OUTLOC_TL$OUTLOC_ID             NUMBER        := 4;
   OUTLOC_TL$OUTLOC_DESC           NUMBER        := 5;
   OUTLOC_TL$OUTLOC_ADD1           NUMBER        := 6;
   OUTLOC_TL$OUTLOC_ADD2           NUMBER        := 7;
   OUTLOC_TL$OUTLOC_CITY           NUMBER        := 8;
   OUTLOC_TL$CONTACT_NAME          NUMBER        := 9;
   OUTLOC_TL$OUTLOC_NAME_SEC       NUMBER        :=10;

   sheet_name_trans             S9T_PKG.trans_map_typ;
   action_column                VARCHAR2(255) := 'ACTION';
   TYPE OUTLOC_rec_tab IS TABLE OF OUTLOC%ROWTYPE;
--------------------------------------------------------------------------------------------------------
   FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT s9t_folder.file_id%TYPE,
                       I_template_only_ind IN     CHAR DEFAULT 'N')
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count   IN OUT NUMBER,
                        I_file_id       IN     s9t_folder.file_id%TYPE,
                        I_process_id    IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count   IN OUT NUMBER,
                    I_process_id    IN     NUMBER,
                    I_chunk_id      IN     NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
   RETURN VARCHAR2;
--------------------------------------------------------------------------------------------------------
END CORESVC_OUTLOC;
/
