
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PM_DEALS_API_SQL AUTHID CURRENT_USER IS
--------------------------------------------------------------------------------
PROCEDURE CREATE_DEAL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_return_code    IN OUT  BOOLEAN,
                      O_deal_ids_table IN OUT  DEAL_IDS_TBL,
                      I_deal_head_rec  IN OUT  DEAL_HEAD_REC); -- this is declared as in out to allow values to be updated before
                                                               -- calling new_deal_comp
--------------------------------------------------------------------------------
PROCEDURE NEW_DEAL_COMP(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_return_code        IN OUT  BOOLEAN,
                        O_deal_ids_table     IN OUT  DEAL_IDS_TBL,
                        I_deal_detail_table  IN      DEAL_DETAIL_TBL);
--------------------------------------------------------------------------------
END PM_DEALS_API_SQL;
/
