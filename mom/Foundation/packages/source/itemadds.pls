CREATE OR REPLACE PACKAGE ITEM_ADD_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
TYPE item_temp_rectype IS RECORD
(
 item_number_type   ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
 auto_create        VARCHAR2(1),
 item_parent        ITEM_MASTER.ITEM_PARENT%TYPE,
 parent_desc        ITEM_MASTER.ITEM_DESC%TYPE,
 parent_diff1       ITEM_MASTER.DIFF_1%TYPE,
 parent_diff2       ITEM_MASTER.DIFF_2%TYPE,
 parent_diff3       ITEM_MASTER.DIFF_3%TYPE,
 parent_diff4       ITEM_MASTER.DIFF_4%TYPE);

TYPE diff_range_rectype IS RECORD
(
diff_range1_group1  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range1_group2  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range2_group1  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range2_group2  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
multi_range1        DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
multi_range2        DIFF_RANGE_HEAD.DIFF_RANGE%TYPE
);

TYPE diff_mixed_rectype IS RECORD
(
diff_range_group1   DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range_group2   DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
diff_range_group3   DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
multi_range         DIFF_RANGE_HEAD.DIFF_RANGE%TYPE,
diff1_used          VARCHAR2(1),
diff2_used          VARCHAR2(1),
diff3_used          VARCHAR2(1),
diff4_used          VARCHAR2(1)
);
-------------------------------------------------------------------------------
--Function Name: INSERT_ITEM
--Purpose      : Inserts the item from the item_add_temp table to the item_master table.
-------------------------------------------------------------------------------
FUNCTION INSERT_ITEM(O_error_message        IN OUT VARCHAR2,
                     I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                     I_item_number_type     IN     ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                     I_format_id            IN     ITEM_MASTER.FORMAT_ID%TYPE,
                     I_prefix               IN     ITEM_MASTER.PREFIX%TYPE,
                     I_tran_level           IN     ITEM_MASTER.TRAN_LEVEL%TYPE,
                     I_item_desc            IN     ITEM_MASTER.ITEM_DESC%TYPE,
                     I_short_desc           IN     ITEM_MASTER.SHORT_DESC%TYPE,
                     I_dept                 IN     DEPS.DEPT%TYPE,
                     I_class                IN     CLASS.CLASS%TYPE,
                     I_subclass             IN     SUBCLASS.SUBCLASS%TYPE,                     
                     I_cost_zone_group_id   IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                     I_diff_1               IN     DIFF_IDS.DIFF_ID%TYPE,
                     I_diff_2               IN     DIFF_IDS.DIFF_ID%TYPE,
                     I_diff_3               IN     DIFF_IDS.DIFF_ID%TYPE,
                     I_diff_4               IN     DIFF_IDS.DIFF_ID%TYPE,
                     I_standard_uom         IN     UOM_CLASS.UOM%TYPE,
                     I_uom_conv_factor      IN     ITEM_MASTER.UOM_CONV_FACTOR%TYPE,
                     I_package_size         IN     ITEM_MASTER.PACKAGE_SIZE%TYPE,
                     I_package_uom          IN     ITEM_MASTER.PACKAGE_UOM%TYPE,
                     I_merchandise_ind      IN     ITEM_MASTER.MERCHANDISE_IND%TYPE,
                     I_store_ord_mult       IN     ITEM_MASTER.STORE_ORD_MULT%TYPE,
                     I_forecast_ind         IN     ITEM_MASTER.FORECAST_IND%TYPE,
                     I_original_retail      IN     ITEM_MASTER.ORIGINAL_RETAIL%TYPE,
                     I_mfg_rec_retail       IN     ITEM_MASTER.MFG_REC_RETAIL%TYPE,
                     I_retail_label_type    IN     ITEM_MASTER.RETAIL_LABEL_TYPE%TYPE,
                     I_retail_label_value   IN     ITEM_MASTER.RETAIL_LABEL_TYPE%TYPE)

   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPP_CTRY(O_error_message          IN OUT   VARCHAR2,
                               I_item                   IN       ITEM_SUPPLIER.ITEM%TYPE,
                               I_supplier               IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                               I_origin_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                               I_vpn                    IN       ITEM_SUPPLIER.VPN%TYPE,
                               I_consignment_rate       IN       ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
                               I_pallet_name            IN       ITEM_SUPPLIER.PALLET_NAME%TYPE,
                               I_case_name              IN       ITEM_SUPPLIER.CASE_NAME%TYPE,
                               I_inner_name             IN       ITEM_SUPPLIER.INNER_NAME%TYPE,
                               I_unit_cost              IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                               I_lead_time              IN       ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                               I_supp_pack_size         IN       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                               I_inner_pack_size        IN       ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                               I_round_lvl              IN       ITEM_SUPP_COUNTRY.ROUND_LVL%TYPE,
                               I_round_to_inner_pct     IN       ITEM_SUPP_COUNTRY.ROUND_TO_INNER_PCT%TYPE,
                               I_round_to_case_pct      IN       ITEM_SUPP_COUNTRY.ROUND_TO_CASE_PCT%TYPE,
                               I_round_to_layer_pct     IN       ITEM_SUPP_COUNTRY.ROUND_TO_LAYER_PCT%TYPE,
                               I_round_to_pallet_pct    IN       ITEM_SUPP_COUNTRY.ROUND_TO_PALLET_PCT%TYPE,
                               I_packing_method         IN       ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE,
                               I_default_uop            IN       ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE,
                               I_ti                     IN       ITEM_SUPP_COUNTRY.TI%TYPE,
                               I_hi                     IN       ITEM_SUPP_COUNTRY.HI%TYPE,
                               I_length                 IN       ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                               I_width                  IN       ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE,
                               I_height                 IN       ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                               I_lwh_uom                IN       UOM_CLASS.UOM%TYPE,
                               I_weight                 IN       ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                               I_weight_uom             IN       UOM_CLASS.UOM%TYPE,
                               I_primary_case_size      IN       ITEM_SUPPLIER.PRIMARY_CASE_SIZE%TYPE,
                               I_negotiated_item_cost   IN       ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE,
                               I_extended_base_cost     IN       ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE,
                               I_inclusive_cost         IN       ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE,
                               I_base_cost              IN       ITEM_SUPP_COUNTRY.BASE_COST%TYPE,
                               I_tolerance_type         IN       ITEM_SUPP_COUNTRY.TOLERANCE_TYPE%TYPE DEFAULT NULL,
                               I_max_tolerance          IN       ITEM_SUPP_COUNTRY.MAX_TOLERANCE%TYPE DEFAULT NULL,
                               I_min_tolerance          IN       ITEM_SUPP_COUNTRY.MIN_TOLERANCE%TYPE DEFAULT NULL,
                               I_cost_uom               IN       ITEM_SUPP_COUNTRY.COST_UOM%TYPE DEFAULT NULL)

RETURN BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION GET_ITEM_SUPP_CTRY_INFO(O_error_message        IN OUT VARCHAR2,
                                 O_vpn                  IN OUT item_supplier.vpn%TYPE,
                                 O_consignment_rate     IN OUT item_supplier.consignment_rate%TYPE,
                                 O_pallet_name          IN OUT item_suppLIER.pallet_name%TYPE,
                                 O_case_name            IN OUT item_suppLIER.case_name%TYPE,
                                 O_inner_name           IN OUT item_suppLIER.inner_name%TYPE,
                                 O_supp_pack_size       IN OUT item_supp_country.supp_pack_size%TYPE,
                                 O_inner_pack_size      IN OUT item_supp_country.inner_pack_size%TYPE,
                                 O_packing_method       IN OUT ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE,
                                 O_ti                   IN OUT ITEM_SUPP_COUNTRY.TI%TYPE,
                                 O_hi                   IN OUT ITEM_SUPP_COUNTRY.HI%TYPE,
                                 O_length               IN OUT item_supp_country_DIM.LENGTH%TYPE,
                                 O_width                IN OUT item_supp_country_DIM.WIDTH%TYPE,
                                 O_height               IN OUT item_supp_country_DIM.HEIGHT%TYPE,
                                 O_lwh_uom              IN OUT uom_class.uom%TYPE,
                                 O_weight               IN OUT item_supp_country_DIM.WEIGHT%TYPE,
                                 O_weight_uom           IN OUT uom_class.uom%TYPE,
                                 I_item                 IN     item_supplier.item%TYPE,
                                 I_supplier             IN     item_supplier.supplier%TYPE,
                                 I_origin_country_id    IN     item_supp_country.origin_country_id%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
FUNCTION POST_QUERY(O_error_message        IN OUT VARCHAR2,
                    O_supplier             IN OUT SUPS.SUPPLIER%TYPE,
                    O_origin_country_id    IN OUT COUNTRY.COUNTRY_ID%TYPE,
                    O_manu_country_id      IN OUT COUNTRY.COUNTRY_ID%TYPE,
                    O_vpn                  IN OUT ITEM_SUPPLIER.VPN%TYPE,
                    O_consignment_rate     IN OUT ITEM_SUPPLIER.CONSIGNMENT_RATE%TYPE,
                    O_pallet_name          IN OUT ITEM_SUPPLIER.PALLET_NAME%TYPE,
                    O_case_name            IN OUT ITEM_SUPPLIER.CASE_NAME%TYPE,
                    O_inner_name           IN OUT ITEM_SUPPLIER.INNER_NAME%TYPE,
                    O_supp_pack_size       IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                    O_inner_pack_size      IN OUT ITEM_SUPP_COUNTRY.INNER_PACK_SIZE%TYPE,
                    O_packing_method       IN OUT ITEM_SUPP_COUNTRY.PACKING_METHOD%TYPE,
                    O_ti                   IN OUT ITEM_SUPP_COUNTRY.TI%TYPE,
                    O_hi                   IN OUT ITEM_SUPP_COUNTRY.HI%TYPE,
                    O_length               IN OUT ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                    O_width                IN OUT ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE,
                    O_height               IN OUT ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                    O_lwh_uom              IN OUT UOM_CLASS.UOM%TYPE,
                    O_weight               IN OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                    O_weight_uom           IN OUT UOM_CLASS.UOM%TYPE,
                    I_item                 IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
END ITEM_ADD_SQL;
/
