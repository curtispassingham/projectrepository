
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFF_TYPE_XML AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
FUNCTION BUILD_MESSAGE(O_status       	     		OUT VARCHAR2,
                       O_text         	   	  	OUT VARCHAR2,
                       O_diff_type_msg 	            OUT rib_sxw.SXWHandle,
                       I_diff_type_rec           	IN  DIFF_TYPE%ROWTYPE,
                       I_action_type  	     		IN  VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END DIFF_TYPE_XML;
/
