
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE POS_VALIDATION_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
---
---  FUNCTION:  STORE
---  Purpose:   Selects only stores with the currency passed in from one of
---             the POS forms.
---  Called By: when-validate-item of B_store_apply.TI_location in posstore.fmb
---
--------------------------------------------------------------------------------
FUNCTION STORE(O_error_message   IN OUT  VARCHAR2,
               O_exists          IN OUT  VARCHAR2,
               O_store_name      IN OUT  VARCHAR2,
               I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
               I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
               I_store           IN      POS_STORE.STORE%TYPE)
   return BOOLEAN;
--------------------------------------------------------------------------------
---
---  FUNCTION:  DISTRICT
---  Purpose:   Selects only districts with the currency passed in from one of
---             the POS forms.
---  Called By: when-validate-item of B_store_apply.TI_location in posstore.fmb
---
--------------------------------------------------------------------------------
FUNCTION DISTRICT (O_error_message   IN OUT  VARCHAR2,
                   O_exists          IN OUT  VARCHAR2,
                   O_district_name   IN OUT  VARCHAR2,
                   I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
                   I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
                   I_district        IN      NUMBER) 
   return BOOLEAN;

--------------------------------------------------------------------------------
---
---  FUNCTION:  REGION
---  Purpose:   Selects only regions with the currency passed in from one of
---             the POS forms.
---  Called By: when-validate-item of B_store_apply.TI_location in posstore.fmb
---
--------------------------------------------------------------------------------
FUNCTION REGION (O_error_message   IN OUT  VARCHAR2,
                 O_exists          IN OUT  VARCHAR2,
                 O_region_name     IN OUT  VARCHAR2,
                 I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
                 I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
                 I_region          IN      NUMBER) 
   return BOOLEAN;

--------------------------------------------------------------------------------
---
---  FUNCTION:  AREA
---  Purpose:   Selects only areas with the currency passed in from one of
---             the POS forms.
---  Called By: when-validate-item of B_store_apply.TI_location in posstore.fmb
---
--------------------------------------------------------------------------------
FUNCTION AREA (O_error_message   IN OUT  VARCHAR2,
               O_exists          IN OUT  VARCHAR2,
               O_area_name       IN OUT  VARCHAR2,
               I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
               I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
               I_area            IN      NUMBER) 
   return BOOLEAN;

--------------------------------------------------------------------------------
---
---  FUNCTION:  LOC_LIST
---  Purpose:   Selects only location lists with a single currency that 
---             agrees with the currency passed in from one of the POS forms.
---  Called By: when-validate-item of B_store_apply.TI_location in posstore.fmb
---
--------------------------------------------------------------------------------
FUNCTION LOC_LIST (O_error_message   IN OUT  VARCHAR2,
                   O_exists          IN OUT  VARCHAR2,
                   O_loc_list_desc   IN OUT  LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                   I_pos_config_type IN      POS_STORE.POS_CONFIG_TYPE%TYPE,
                   I_pos_config_id   IN      POS_STORE.POS_CONFIG_ID%TYPE,
                   I_loc_list        IN      LOC_LIST_HEAD.LOC_LIST%TYPE) 
   return BOOLEAN;
--------------------------------------------------------------------------------                                                                                                                                
END POS_VALIDATION_SQL;

/


