CREATE OR REPLACE PACKAGE CORESVC_PRICECOST_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------------------------------
-- Function Name : GET_PRICING_COST
-- Purpose       : This function gets the pricing cost from FUTURE_COST table depending on the 
--                 item,supplier,location,origin_country_id and active_date input.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_PRICING_COST(O_error_message   OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_business_object OUT "RIB_PrcCostColDesc_REC",
                          O_error_tbl       OUT SVCPROV_UTILITY.ERROR_TBL,
                          I_business_object IN  "RIB_PrcCostColCriVo_REC")
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END CORESVC_PRICECOST_SQL;
/