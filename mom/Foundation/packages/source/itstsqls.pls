
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_STATUS_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--- Package Variables
-------------------------------------------------------------------------------
--- Function Name CHECK_ITEM
--- Purpose       This function will make certain that items are not
---               currently on active order or that there are
---               no pending price or cost changes for these items before
---               allowing a status change.
--------------------------------------------------------------------------------


FUNCTION CHECK_ITEM (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists          IN OUT VARCHAR2,
                     I_item            IN     ITEM_MASTER.ITEM%TYPE,
                     I_store           IN     ITEM_LOC.LOC%TYPE,
                     I_wh              IN     ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--- Function Name UPDATE_ITEM_STATUS
--- Purpose       This function will update all item at a selected store or 
---               warehouse to the same status.  It will also update taxable
---               indicator for items at a selected store.  
---               However, previously deleted items are not modified.
---               Items changed to Delete status are inserted into daily_purge.
--- Calls         For tax changes, tran type 26 is used.

--------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_STATUS (O_error_message   IN OUT VARCHAR2,
                             I_item            IN     ITEM_MASTER.ITEM%TYPE,
                             I_location        IN     ITEM_LOC.LOC%TYPE,
                             I_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_status          IN     ITEM_LOC.STATUS%TYPE,
                             I_taxable_ind     IN     ITEM_LOC.TAXABLE_IND%TYPE,
                             I_supplier        IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                             I_chg_status      IN     VARCHAR2,
                             I_chg_tax         IN     VARCHAR2,
                             I_chg_supplier    IN     VARCHAR2)
                             RETURN BOOLEAN;
--------------------------------------------------------------------------------
END ITEM_STATUS_SQL;
/


