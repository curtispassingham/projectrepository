
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE STORE_GRADE_VALIDATE_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-- Name:          NEXT_STORE_GRADE_GROUP_NUMBER
-- Purpose:       Generates the next Store Grade Group sequence number.
-- Created:       02-OCT-97 Steven B. Olson
-------------------------------------------------------------------------------
FUNCTION NEXT_STORE_GRADE_GROUP_NUMBER( O_error_message         IN OUT  VARCHAR2,
                                        O_store_grade_group_id  IN OUT  STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
                                        RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_GROUP_DESC
-- Purpose:       Retrieves the description associated with the Store Grade Group.
-- Created:       02-OCT-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION GET_GROUP_DESC( O_error_message           IN OUT  VARCHAR2,
                         O_store_grade_group_desc  IN OUT  STORE_GRADE_GROUP.STORE_GRADE_GROUP_DESC%TYPE,
                         I_store_grade_group_id    IN      STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
                         RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GRADE_EXISTS
-- Purpose:       Checks to see if the Store Grade exists for the Store Grade Group.
-- Created:       02-OCT-97 Steven B. Olson
---------------------------------------------------------------------------------------------
FUNCTION GRADE_EXISTS( O_error_message         IN OUT  VARCHAR2,
                       O_exists                IN OUT  BOOLEAN,
                       I_store_grade_group_id  IN      STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE,
                       I_store_grade           IN      STORE_GRADE.STORE_GRADE%TYPE)
                       RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: STORE_GRADE_STORE_FILTER_LIST
--       Purpose: This function checks to see whether the user has visibility to see all the
--                stores within the store grade group id.
FUNCTION STORE_GRADE_STORE_FILTER_LIST(O_error_message         IN OUT  VARCHAR2,
                                       O_Partial_ind           IN OUT  VARCHAR2,
                                       I_store_grade_group_id  IN      STORE_GRADE_STORE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:          INSERT_STORE_GRADE_TEMP
-- Purpose:       This function will insert Store Grade into the store_grade_dist_temp table
-------------------------------------------------------------------------------
FUNCTION INSERT_STORE_GRADE_TEMP(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_order_no               IN      STORE_GRADE_DIST_TEMP.ORDER_NO%TYPE,
                                 I_store_grade_group_id   IN      STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:          CHECK_STORE_GRADE_BUYER
-- Purpose:       This function will check that the Store Grade exists for a given Buyer
-------------------------------------------------------------------------------
FUNCTION CHECK_STORE_GRADE_BUYER(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists                 IN OUT  VARCHAR2,
                                 I_buyer                  IN      BUYER.BUYER%TYPE,
                                 I_store_grade_group_id   IN      STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:          CHECK_STORE_GRADE_STORE
-- Purpose:       This function will check if the store exists for a given Store Grade.
-------------------------------------------------------------------------------
FUNCTION CHECK_STORE_GRADE_STORE(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_exists                 IN OUT   VARCHAR2,
                                 I_store                  IN       STORE.STORE%TYPE,
                                 I_store_grade_group_id   IN       STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:          CREATE_STORE_GRADE_STORE
-- Purpose:       This function will insert records into the store_grade_store table for a given
--                group_no, group_type, store_grade and store_grade_group_id.
-------------------------------------------------------------------------------
FUNCTION CREATE_STORE_GRADE_STORE(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists                 IN OUT   VARCHAR2,
                                  I_group_no               IN       VARCHAR2,
                                  I_group_type             IN       STORE_GRADE.STORE_GRADE%TYPE,
                                  I_store_grade            IN       STORE_GRADE.STORE_GRADE%TYPE,
                                  I_store_grade_group_id   IN       STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:          UPDATE_STORE_GRADE_STORE
-- Purpose:       This function will update the store_grade_store table for a given
--                group_no, group_type, store_grade and store_grade_group_id.
-------------------------------------------------------------------------------
FUNCTION UPDATE_STORE_GRADE_STORE(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_store                  IN       STORE.STORE%TYPE,
                                  I_store_grade            IN       STORE_GRADE.STORE_GRADE%TYPE,
                                  I_store_grade_group_id   IN       STORE_GRADE.STORE_GRADE_GROUP_ID%TYPE)
RETURN BOOLEAN;

END STORE_GRADE_VALIDATE_SQL;
/
