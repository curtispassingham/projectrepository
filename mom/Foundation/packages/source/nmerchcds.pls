CREATE OR REPLACE PACKAGE NON_MERCH_CODE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------------------------------
--Function Name:   MERGE_NON_MERCH_CODE_HEAD_TL
--Purpose:         This function accepts non_merch_code,lang and non_merch_code_desc as input. 
--                 It updates NON_MERCH_CODE_HEAD_TL table if non merch code is already present 
--                 otherwise inserts a new record into the table. It returs TRUE if insert/update 
--                 is successful otherwise returns FALSE.
--Created:         AUG-14    Anandan Muthukannan
-----------------------------------------------------------------------------------------------------
FUNCTION MERGE_NON_MERCH_CODE_HEAD_TL( O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_non_merch_code      IN     NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE%TYPE,
                                       I_lang                IN     NON_MERCH_CODE_HEAD_TL.LANG%TYPE,
                                       I_non_merch_code_desc IN     NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE_DESC%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   DEL_NON_MERCH_CODE_HEAD_TL
--Purpose:         This function accepts non_merch_code as input.
--                 It deletes entry associated with non_merch_code from  NON_MERCH_CODE_HEAD_TL table.
--                 It returs TRUE if delete operation is successful otherwise returns FALSE.
--Created:         AUG-14    Anandan Muthukannan
-----------------------------------------------------------------------------------------------------
FUNCTION DEL_NON_MERCH_CODE_HEAD_TL( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_non_merch_code  IN     NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE%TYPE )
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
--Function Name:   DEL_NON_MERCH_CODE_COMP
--Purpose:         This function accepts non_merch_code as input.
--                 It deletes entry associated with non_merch_code from  NON_MERCH_CODE_COMP table.
--                 It returs TRUE if delete operation is successful otherwise returns FALSE.
--Created:         AUG-14    Anandan Muthukannan
-----------------------------------------------------------------------------------------------------
FUNCTION DEL_NON_MERCH_CODE_COMP( O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_non_merch_code  IN     NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE%TYPE )
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
END NON_MERCH_CODE_SQL;
/
