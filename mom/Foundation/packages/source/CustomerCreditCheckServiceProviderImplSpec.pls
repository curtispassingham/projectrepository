/******************************************************************************
* Service Name     : CustomerCreditCheckService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/CustomerCreditCheckService/v1
* Description      : $service.documentation
*
*******************************************************************************/
CREATE OR REPLACE PACKAGE CUSTOMERCREDITCHECKSERVICEPROV AUTHID CURRENT_USER AS


/******************************************************************************
 *
 * Operation       : updateCustCredit
 * Description     : updates the customer credits in RMS for a franchise customer  
                        for requests from an external financial system.
             
 * 
 * Input           : "RIB_CustCreditChkCol_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/CustCreditChkColDesc/v1
 * Description     :  CustCreditChkColDesc object holds multiple franchise customers credit information
                    update requests from financials. 
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : ${operation.output.documentation}
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
                    message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
                    "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE updateCustCredit(
                          I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject          IN  "RIB_CustCreditChkCol_REC",
                          O_serviceOperationStatus  OUT "RIB_ServiceOpStatus_REC",
                          O_businessObject          OUT "RIB_InvocationSuccess_REC"
                          );
/******************************************************************************/



END CustomerCreditCheckServiceProv;
/
 