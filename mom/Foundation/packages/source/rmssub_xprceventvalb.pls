CREATE OR REPLACE PACKAGE BODY RMSSUB_XPRCEVENT_VALIDATE AS
-------------------------------------------------------------------------------------------------------
LP_diff_column       DIFF_IDS.DIFF_ID%TYPE := NULL;
LP_base_retail      CODE_DETAIL.CODE%TYPE := 'BASERT';
LP_hier_values       LOC_TBL;
LP_wh               CONSTANT   VARCHAR2(2) :='W';
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPEC
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for modify messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XPrcEventDesc_REC")
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: VALIDATE_MESSAGE
   -- Purpose      : This function will perform basic validations on the message
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: VALIDATE_LOCATIONS
   -- Purpose      : This function will validate that the XPriceChgHrDtl node and XPriceChgExcSt
   --                nodes in the message are valid.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATIONS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: BUILD_HIER_VALUE_TBL
   -- Purpose      : This function will loop through tge XpriceChgHrDtl node of the price change message
   --                to get the list of organization hierarchy values and put them in a global variable.
   --                It will also remove any duplicates.
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_HIER_VALUE_TBL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                              I_message         IN              "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message      IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       I_message            IN              "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'RMSSUB_XPRCEVENT_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not VALIDATE_MESSAGE(O_error_message,
                           I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
---------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'RMSSUB_XPRCEVENT_VALIDATE.CHECK_REQUIRED_FIELDS';

   L_multi_units_round NUMBER;

BEGIN

   if I_message.event_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'event id', NULL, NULL);
      return FALSE;
   end if;

   if I_message.event_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'event type', NULL, NULL);
      return FALSE;
   end if;

   if I_message.item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item', NULL, NULL);
      return FALSE;
   end if;

   if I_message.item_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item level', NULL, NULL);
      return FALSE;
   end if;

   if I_message.event_type = LP_base_retail and I_message.hier_level is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL', 'hierarchy level', NULL, NULL);
      return FALSE;
   end if;

   if I_message.event_type <> LP_base_retail and I_message.hier_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'hierarchy level', NULL, NULL);
      return FALSE;
   end if;

   if I_message.currency_code is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'currency code', NULL, NULL);
      return FALSE;
   end if;

   if I_message.event_type in (LP_base_retail, 'REG', 'CLRS', 'CLRE') then
      if I_message.selling_unit_retail is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'Selling Retail', NULL, NULL);
         return FALSE;
      end if;

      -- selling UOM and selling unit retail must be either both defined or both null
      if (I_message.selling_uom is NULL and I_message.selling_unit_retail is NOT NULL) or
         (I_message.selling_uom is NOT NULL and I_message.selling_unit_retail is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SINGLE_RETAILS', NULL, NULL, NULL);
         return FALSE;
      end if;

     if I_message.selling_unit_retail is NOT NULL and I_message.selling_unit_retail < 0 then
        O_error_message := SQL_LIB.CREATE_MSG('UNIT_RTL_ZERO', NULL, NULL, NULL);
        return FALSE;
      end if;
   end if;

   if I_message.event_type in ('REG') and I_message.multi_units is not NULL then
  
     -- multi units, multi selling UOM and multi-unit retail must be either all defined or all null
      if NOT ((I_message.multi_units is NULL and
               I_message.multi_unit_retail is NULL and
               I_message.multi_selling_uom is NULL) or
              (I_message.multi_units is NOT NULL and
               I_message.multi_unit_retail is NOT NULL and
               I_message.multi_selling_uom is NOT NULL)) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_MULTI_RETAILS', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.multi_unit_retail is NOT NULL and I_message.multi_unit_retail < 0 then
         O_error_message := SQL_LIB.CREATE_MSG('UNIT_RTL_ZERO', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.multi_units is NOT NULL and I_message.multi_units <= 1 then
         O_error_message := SQL_LIB.CREATE_MSG('INV_VAL_MULTI_UNITS', NULL, NULL, NULL);
         return FALSE;
      end if;

      L_multi_units_round := round(I_message.multi_units,0);
      if I_message.multi_units != L_multi_units_round then
         O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS_INTEGER', NULL, NULL, NULL);
         return FALSE;
      end if;

      -- either single or multi must be defined
      if I_message.selling_uom is NULL and
         I_message.selling_unit_retail is NULL and
         I_message.multi_units is NULL and
         I_message.multi_unit_retail is NULL and
         I_message.multi_selling_uom is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('SINGLE_OR_MULTI_REQ', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   if I_message.event_type in ('PROMS') then
      if I_message.promo_unit_retail is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'promo retail', NULL, NULL);
         return FALSE;
      end if;

      -- promp selling UOM and promo selling retail must be either both defined or both null
      if (I_message.promo_selling_uom is NULL and I_message.promo_unit_retail is NOT NULL) or
         (I_message.promo_selling_uom is NOT NULL and I_message.promo_unit_retail is NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PROMO_RETAIL', NULL, NULL, NULL);
         return FALSE;
      end if;

      if I_message.promo_unit_retail is NOT NULL and I_message.promo_unit_retail < 0 then
         O_error_message := SQL_LIB.CREATE_MSG('UNIT_RTL_ZERO', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XPRCEVENT_VALIDATE.VALIDATE_MESSAGE';
   L_loc          LOC_TBL;
   L_exists       BOOLEAN;
   L_valid        BOOLEAN;
   L_exist        VARCHAR2(1);

   cursor C_CODE_DETAIL is
      select 'x'
        from code_detail cd
       where code_type = 'PCET'
         and code = I_message.event_type
         and rownum = 1;
BEGIN

   SQL_LIB.SET_MARK('OPEN','C_CODE_DETAIL','CODE_DETAIL','Code_type: '||I_message.event_type );
   open C_CODE_DETAIL;
   SQL_LIB.SET_MARK('FETCH','C_CODE_DETAIL','CODE_DETAIL','Code_type: '||I_message.event_type );
   fetch C_CODE_DETAIL into L_exist;

   if C_CODE_DETAIL%FOUND then
      L_exists := TRUE;
   else
      L_exists := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_CODE_DETAIL','CODE_DETAIL','Code_type: '||I_message.event_type );
   close C_CODE_DETAIL;

   if not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CODE_TYPE', I_message.event_type, NULL, NULL);
      return FALSE;
   end if;

   if I_message.hier_level is NOT NULL then
      if I_message.hier_level not in (ORGANIZATION_SQL.LP_chain,
                                      ORGANIZATION_SQL.LP_area,
                                      ORGANIZATION_SQL.LP_region,
                                      ORGANIZATION_SQL.LP_district,
                                      ORGANIZATION_SQL.LP_store,
                                      LP_wh) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_LEVEL');
         return FALSE;
      end if;
   end if;

   if not CURRENCY_SQL.EXIST(O_error_message,
                             I_message.currency_code,
                             L_exists) then
      return FALSE;
   end if;

   if not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURR_CODE', NULL, NULL, NULL);
      return FALSE;
   end if;

   if I_message.selling_uom is NOT NULL then
      if not UOM_SQL.VALID_STANDARD_UOM(O_error_message,
                                        L_valid,
                                        I_message.selling_uom) then
         return FALSE;
      end if;

      if not L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_UOM', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   if I_message.multi_selling_uom is NOT NULL then
      if not UOM_SQL.VALID_STANDARD_UOM(O_error_message,
                                        L_valid,
                                        I_message.multi_selling_uom) then
         return FALSE;
      end if;

      if not L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_UOM', NULL, NULL, NULL);
         return FALSE;
      end if;

   end if;

   if I_message.promo_selling_uom is NOT NULL then
      if not UOM_SQL.VALID_STANDARD_UOM(O_error_message,
                                        L_valid,
                                        I_message.promo_selling_uom) then
         return FALSE;
      end if;

      if not L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_UOM', NULL, NULL, NULL);
         return FALSE;
      end if;

   end if;

   if I_message.diff_id is not NULL then
      if not ITEM_ATTRIB_SQL.ITEM_DIFF_EXISTS(O_error_message,
                                              L_exists,
                                              LP_diff_column,
                                              I_message.item,
                                              I_message.diff_id) then
         return FALSE;
      end if;

      if not L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STYLE_SIZE', NULL, NULL, NULL);
         return FALSE;
      end if;

   end if;

  if I_message.event_type <> LP_base_retail and 
            I_message.hier_level is not NULL and I_message.XPrcEventDtl_TBL.count > 0 then
     if not VALIDATE_LOCATIONS(O_error_message,
                               I_message) then
       return FALSE;
     end if;
  end if;  

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATIONS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_message         IN       "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)  := 'RMSSUB_XPRCEVENT_VALIDATE.VALIDATE_LOCATIONS';

   L_exists         BOOLEAN;

BEGIN

  if not BUILD_HIER_VALUE_TBL(O_error_message,
                              I_message) then
      return FALSE;
   end if;

   -- store and warehouses are validated along with currency and country
   if I_message.hier_level = ORGANIZATION_SQL.LP_store then
      if not STORE_VALIDATE_SQL.VALIDATE_STORES(O_error_message,
                                                LP_hier_values,
                                                I_message.currency_code,
                                                null) then
         return FALSE;
      end if;
   elsif I_message.hier_level = LP_wh then
      for i in  LP_hier_values.first..LP_hier_values.last loop
         if not WH_ATTRIB_SQL.WH_EXIST(O_error_message,
                                       L_exists,
                                       LP_hier_values(i)) then
            return FALSE;
         end if;

         if not L_exists then
            O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_ID',NULL, NULL, NULL);
            return FALSE;
         end if;
      end loop;
      return TRUE;
   else
      if not ORGANIZATION_SQL.VALIDATE_IDS(O_error_message,
                                           L_exists,
                                           I_message.hier_level,
                                           LP_hier_values) then
         return FALSE;
      end if;

      if not L_exists then
         O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_ID', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_LOCATIONS;
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_HIER_VALUE_TBL(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                              I_message         IN              "RIB_XPrcEventDesc_REC")
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'RMSSUB_XPRCEVENT_VALIDATE.BUILD_HIER_VALUE_TBL';
   L_duplicate   BOOLEAN := FALSE;

BEGIN

   -- This function will put the hier values on the message into a global variable.
   -- It will also remove any duplicates.
   LP_hier_values := LOC_TBL();

   FOR i in I_message.XPrcEventDtl_TBL.FIRST..I_message.XPrcEventDtl_TBL.LAST LOOP
      -- check for duplicates
      L_duplicate := FALSE;
      FOR j in 1..LP_hier_values.COUNT LOOP
         if I_message.XPrcEventDtl_TBL(i).hier_value = LP_hier_values(j) then
            L_duplicate := TRUE;
            exit;
         end if;
      END LOOP;

      -- only add to table if not already there
      if L_duplicate = FALSE then
         LP_hier_values.EXTEND();
         LP_hier_values(LP_hier_values.COUNT) := I_message.XPrcEventDtl_TBL(i).hier_value;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_HIER_VALUE_TBL;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XPRCEVENT_VALIDATE;
/
