
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFFGRPHDR_XML AUTHID CURRENT_USER AS

--------------------------------------------------------
FUNCTION BUILD_MESSAGE(O_status          OUT VARCHAR2,
                       O_text            OUT VARCHAR2,
                       O_message         OUT CLOB,
                       I_record          IN  DIFF_GROUP_HEAD%ROWTYPE,
                       I_action_type     IN  VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
END DIFFGRPHDR_XML;
/
