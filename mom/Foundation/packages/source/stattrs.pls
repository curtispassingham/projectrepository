CREATE OR REPLACE PACKAGE STORE_ATTRIB_SQL AUTHID CURRENT_USER AS

TYPE STORE_RECTYPE IS RECORD(
   STORE_NAME       STORE.STORE_NAME%TYPE,
   STORE_ADD1       ADDR.ADD_1%TYPE,
   STORE_ADD2       ADDR.ADD_2%TYPE,
   STORE_CITY       ADDR.CITY%TYPE,
   STATE            ADDR.STATE%TYPE,
   COUNTRY_ID       ADDR.COUNTRY_ID%TYPE,
   STORE_PCODE      ADDR.POST%TYPE,
   FAX_NUMBER       STORE.FAX_NUMBER%TYPE,
   PHONE_NUMBER     STORE.PHONE_NUMBER%TYPE,
   STORE_TYPE       STORE.STORE_TYPE%TYPE,
   STOCKHOLDING_IND STORE.STOCKHOLDING_IND%TYPE
);

----------------------------------------------------------------------
-- Function Name: active
-- Purpose  : To check it the given date is within
--         the open and close date of the store.
----------------------------------------------------------------------
FUNCTION ACTIVE(I_store           IN      NUMBER,
                I_date            IN      DATE,
                O_open_close_ind  IN OUT  VARCHAR2,
                O_error_message   IN OUT  VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------
--- Function Name:  GET_NAME
--- Purpose:        Fetches the name of the store from store table.
--------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message IN OUT VARCHAR2,
                  I_store         IN     NUMBER,
                  O_store_name    IN OUT VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function: MALL_NAME-
-- Purpose: Gets the mall name of a given store
--------------------------------------------------------------------
FUNCTION MALL_NAME(O_error_message  IN OUT  VARCHAR2,
                   I_store          IN      NUMBER,
                   O_desc           IN OUT  VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function: STORE_NAME10
-- Purpose: Gets the 10 character name of a given store
--------------------------------------------------------------------
FUNCTION STORE_NAME10(O_error_message  IN OUT  VARCHAR2,
                      I_store          IN      NUMBER,
                      O_desc           IN OUT  VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function: FORMAT_NAME
-- Purpose: Gets the format name of a given store format
--------------------------------------------------------------------
FUNCTION FORMAT_NAME(O_error_message  IN OUT  VARCHAR2,
                     I_format         IN      NUMBER,
                     O_desc           IN OUT  VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------
--Function Name: GET_TSF_ZONE
--Purpose:       This function will return the transfer zone for a given
--               store. If the store is not in a transfer zone, the function
--               will return a Boolean indicator.
--------------------------------------------------------------------
FUNCTION GET_TSF_ZONE(O_error_message  IN OUT  VARCHAR2,
                      O_tsf_zone       IN OUT  store.transfer_zone%TYPE,
                      O_store_in_zone  IN OUT  BOOLEAN,
                      I_store          IN      store.store%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------
--Function Name : GET_INFO
--Purpose:        This functon will retrieve location information about
--                a store.
----------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_store_name      IN OUT   STORE.STORE_NAME%TYPE,   
                   O_store_add1      IN OUT   ADDR.ADD_1%TYPE,          
                   O_store_add2      IN OUT   ADDR.ADD_2%TYPE,          
                   O_store_city      IN OUT   ADDR.CITY%TYPE,          
                   O_state           IN OUT   ADDR.STATE%TYPE,         
                   O_country_id      IN OUT   ADDR.COUNTRY_ID%TYPE,    
                   O_store_pcode     IN OUT   ADDR.POST%TYPE,          
                   O_fax_number      IN OUT   STORE.FAX_NUMBER%TYPE,   
                   O_phone_number    IN OUT   STORE.PHONE_NUMBER%TYPE, 
                   I_store           IN       STORE.STORE%TYPE)  
RETURN BOOLEAN;

--------------------------------------------------------------------
-- Function Name: GET_CURRENCY_CODE
-- Purpose      : This function will be used to determine the currency
--                for the given store
--------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE(O_error_message  IN OUT VARCHAR2,
                           O_currency_code  IN OUT STORE.CURRENCY_CODE%TYPE,
                           I_store          IN     STORE.STORE%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------
-- Name:    GET_STORE_DISTRICT
-- Purpose: This function will take a store as an input parameter and will
--          then return the district that the store is within in organizational
--          hierarchy.
--------------------------------------------------------------------------
FUNCTION GET_STORE_DISTRICT(O_error_message  IN OUT  VARCHAR2,
                            O_district       IN OUT  store.district%TYPE,
                            I_store          IN      store.store%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Name :   STORE_CLASS_EXIST
-- Purpose: This function will take a store class as input and will return true
--          if the store class exists on the Store table, false if not.
---------------------------------------------------------------------------
FUNCTION STORE_CLASS_EXIST(O_error_message  IN OUT  VARCHAR2,
                           O_exists         IN OUT  BOOLEAN,
                           I_store_class    IN      STORE.STORE_CLASS%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------
-- Name: STORE_VALID_FOR_CURRENCY
-- Purpose: Checks to see that a given store/currency combination exists.  If so,
--          returns O_valid = TRUE and the store name.
---------------------------------------------------------------------------------
FUNCTION STORE_VALID_FOR_CURRENCY(O_error_message  IN OUT  VARCHAR2,
                                  O_valid          IN OUT  BOOLEAN,
                                  O_store_name     IN OUT  STORE.STORE_NAME%TYPE,
                                  I_currency_code  IN      STORE.CURRENCY_CODE%TYPE,
                                  I_store          IN      STORE.STORE%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------
-- Name: GET_STORE_CLOSE_DATE
-- Purpose: This function will return the store_close_date for a given store.
---------------------------------------------------------------------------------
FUNCTION GET_STORE_CLOSE_DATE(O_error_message  IN OUT  VARCHAR2,
                              O_close_date     IN OUT  STORE.STORE_CLOSE_DATE%TYPE,
                              I_store          IN      STORE.STORE%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------
-- Name: VALIDATE_HIERARCHY
-- Purpose: This function will take a store and location hierarchy values as input
--          and will return true if the store exists in that hierarchy chain, and
--          will return false if not.
---------------------------------------------------------------------------------
FUNCTION VALIDATE_HIERARCHY(O_error_message  IN OUT  VARCHAR2,
                            I_store          IN      NUMBER,
                            I_district       IN      NUMBER,
                            I_region         IN      NUMBER,
                            I_area           IN      NUMBER,
                            I_chain          IN      NUMBER)
RETURN BOOLEAN;

---------------------------------------------------------------------------------
-- Name: VALIDATE_WALKTHROUGH
-- Purpose: This function verifies that the walk through store is a stockholding
-- store on the store table.  This function also calls STORE_ATTRIB_SQL.GET_NAME
-- and STORE_ATTRIB_SQL.CHECK_DUP_WALKTHROUGH.
---------------------------------------------------------------------------------
FUNCTION VALIDATE_WALKTHROUGH(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid                IN OUT  BOOLEAN,
                              O_store_name           IN OUT  STORE.STORE_NAME%TYPE,
                              I_store                IN      STORE.STORE%TYPE,
                              I_walk_through_store   IN      WALK_THROUGH_STORE.WALK_THROUGH_STORE%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------
-- Name: CHECK_DUP_WALKTHROUGH
-- Purpose: This function verifies that the walk through store store does not
-- already exist for the header store.  This also verifies that the walk through
-- store is not the same as the header store.
---------------------------------------------------------------------------------
FUNCTION CHECK_DUP_WALKTHROUGH(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_duplicate            IN OUT  BOOLEAN,
                               I_store                IN      STORE.STORE%TYPE,
                               I_walk_through_store   IN      WALK_THROUGH_STORE.WALK_THROUGH_STORE%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------
-- Name: CHECK_WALKTHROUGH_EXISTS
-- Purpose: This function verifies that the walk through stores
-- exist for the header store.
---------------------------------------------------------------------------------
FUNCTION CHECK_WALKTHROUGH_EXISTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists         IN OUT  BOOLEAN,
                                  I_store          IN      STORE.STORE%TYPE)

RETURN BOOLEAN;

---------------------------------------------------------------------------------
-- GET_ROW
-- Validates the store and returns the entire store row if it exists.
---------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists         IN OUT  BOOLEAN,
                 O_store_row      IN OUT  STORE%ROWTYPE,
                 I_store          IN      STORE.STORE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
--- Function Name:  GET_COUNTRY_ID
--- Purpose:        Fetches the country_id of the store from addr table.
---------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_country_id      IN OUT   ADDR.COUNTRY_ID%TYPE,
                        I_store           IN       STORE.STORE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
--- Function Name:  GET_STOCKHOLDING_IND
--- Purpose:        Fetches the stockholding indicator of the store from 
---                 store table.
---------------------------------------------------------------------------------
FUNCTION GET_STOCKHOLDING_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_stkhldg_ind   OUT    STORE.STOCKHOLDING_IND%TYPE,
                              I_store         IN     STORE.STORE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
--Function Name : GET_INFO
--Purpose:        This new overloaded functon will return a record type containing 
--                location information for an input store.
---------------------------------------------------------------------------------
FUNCTION GET_INFO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_store_rec       IN OUT   STORE_RECTYPE,
                   I_store           IN       STORE.STORE%TYPE)  
RETURN BOOLEAN;
---------------------------------------------------------------------------------
END STORE_ATTRIB_SQL;
/
