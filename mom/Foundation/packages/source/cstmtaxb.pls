create or replace PACKAGE BODY CUSTOM_TAX_SQL AS
---
-----------------------------------------------------------------------------------------------------
FUNCTION CALC_CTAX_COST_TAX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_tax_calc_tbl IN OUT NOCOPY OBJ_TAX_CALC_TBL)
RETURN BOOLEAN IS 
   L_program                VARCHAR2(255) := 'CUSTOM_TAX_SQL.CALC_CTAX_COST_TAX';

BEGIN
   RETURN TRUE;
END CALC_CTAX_COST_TAX;
-------------------------------------------------------------------------------------------------------
FUNCTION CALC_CTAX_RETAIL_TAX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_tax_calc_tbl IN OUT NOCOPY OBJ_TAX_CALC_TBL)
RETURN BOOLEAN IS
   L_program                VARCHAR2(255) := 'CUSTOM_TAX_SQL.CALC_CTAX_RETAIL_TAX';
   
BEGIN
   RETURN TRUE;
END CALC_CTAX_RETAIL_TAX;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_CTAX_RETAIL_TAX(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_tax_add_remove_tbl IN OUT NOCOPY OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN IS

BEGIN
   RETURN TRUE;
END ADD_CTAX_RETAIL_TAX;
--------------------------------------------------------------------------------------------------------
---
END CUSTOM_TAX_SQL;
/