
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRSCLS_SQL AS
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_SUBCLASS
   -- Purpose      :  Inserts a record on the subclass table
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_SUBCLASS
   -- Purpose      :  Updates a record on the subclass table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_SUBCLASS
   -- Purpose      :  Deletes a record on the subclass table
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_subclass_rec    IN       SUBCLASS%ROWTYPE,
                 I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XMRCHHRSCLS.LP_cre_type then
      if CREATE_SUBCLASS(O_error_message,
                         I_subclass_rec) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XMRCHHRSCLS.LP_mod_type then
      if MODIFY_SUBCLASS(O_error_message,
                         I_subclass_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_SQL.PERSIST';

BEGIN

   if not DELETE_SUBCLASS(O_error_message,
                      I_subclass_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_SQL.CREATE_SUBCLASS';
   L_type_code VARCHAR2(1)  := 'B';

BEGIN

   if not MERCH_SQL.INSERT_SUBCLASS(O_error_message,
                                    I_subclass_rec) then
      return FALSE;
   end if;

   if STKLEDGR_SQL.STOCK_LEDGER_INSERT(L_type_code,
                                       I_subclass_rec.dept,
                                       I_subclass_rec.class,
                                       I_subclass_rec.subclass,
                                       NULL,
                                       O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_SUBCLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_SQL.MODIFY_SUBCLASS';

BEGIN

   if not MERCH_SQL.MODIFY_SUBCLASS(O_error_message,
                                    I_subclass_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_SUBCLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_SQL.DELETE_SUBCLASS';

BEGIN

   if not MERCH_SQL.DELETE_SUBCLASS(O_error_message,
                                    I_subclass_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_SUBCLASS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRSCLS_SQL;
/
