
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE FILTER_GROUP_HIER_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Name:    CHECK_DUP_GROUP_MERCH
-- Purpose: This function will check for duplicate values exists. 
----------------------------------------------------------------------------
FUNCTION CHECK_DUP_GROUP_MERCH(O_error_message            IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_dup_exists               IN OUT  BOOLEAN,
                               I_sec_group_id             IN      FILTER_GROUP_MERCH.SEC_GROUP_ID%TYPE,
                               I_filter_merch_level       IN      FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                               I_filter_merch_id          IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                               I_filter_merch_id_class    IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                               I_filter_merch_id_subclass IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)                               
RETURN BOOLEAN;
 
--------------------------------------------------------------------------
-- Name:    CHECK_DUP_GROUP_ORG
-- Purpose: This function will check for duplicate values exists. 
----------------------------------------------------------------------------
FUNCTION CHECK_DUP_GROUP_ORG(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_dup_exists       IN OUT  BOOLEAN,
                             I_sec_group_id     IN      FILTER_GROUP_ORG.SEC_GROUP_ID%TYPE,
                             I_filter_org_level IN      FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                             I_filter_org_id    IN      FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    CHECK_GROUP_MERCH
-- Purpose: This function takes in a group_id as input, then returns back a
--          value of TRUE to the calling module if their are any merch children 
--          for that group in the filter_group_merch table, or else it returns 
--          a value of FALSE if there are no merch children for that group.
-- Created By: Jason Herzog 05-05-03 (Logic Information Systems)
----------------------------------------------------------------------------
FUNCTION CHECK_GROUP_MERCH  (O_error_message IN OUT VARCHAR2,
                             O_exist         IN OUT BOOLEAN,
                             I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------
-- Name:    CHECK_GROUP_ORG
-- Purpose: This function takes in a group_id as input, then returns back a
--          value of TRUE to the calling module if their are any org children 
--          for that group in the filter_group_org table, or else it returns 
--          a value of FALSE if there are no org children for that group.
-- Created By: Jason Herzog 05-05-03
----------------------------------------------------------------------------
FUNCTION CHECK_GROUP_ORG  (O_error_message IN OUT VARCHAR2,
                           O_exist         IN OUT BOOLEAN,
                           I_group_id      IN     sec_group.group_id%TYPE)
return BOOLEAN;

----------------------------------------------------------------------------
-- Name:    DELETE_GROUP_MERCH
-- Purpose: This function deletes Merchandise Hierarchy Assignment to User Group.
----------------------------------------------------------------------------
FUNCTION DELETE_GROUP_MERCH(O_error_message            IN OUT VARCHAR2,
                            I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                            I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                            I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                            I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)
return BOOLEAN;
----------------------------------------------------------------------------
-- Name:    DELETE_GROUP_ORG
-- Purpose: This function deletes Organization Hierarchy Assignment to User Group.
----------------------------------------------------------------------------
FUNCTION DELETE_GROUP_ORG(O_error_message    IN OUT VARCHAR2,
                          I_filter_org_level IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                          I_filter_org_id    IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_GROUP_ORG
-- Purpose: This function will check if the Organization Hierarchy exists
--          in one of the Data Element.
--          If exists then O_exist = TRUE and O_error_message indicate
--          the Data Element and Hierarchy Level.
----------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_ORG(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist               IN OUT BOOLEAN,
                            I_filter_org_level    IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                            I_filter_org_id       IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_GROUP_MERCH
-- Purpose: This function will check if the Merchandise Hierarchy exists
--          in one of the Data Element.
--          If exists then O_exist = TRUE and O_error_message indicate
--          the Data Element and Hierarchy Level.
----------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_MERCH(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist                    IN OUT BOOLEAN,
                              I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                              I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                              I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                              I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_GROUP_MERCH
-- Purpose: This function will check if the Merchandise Hierarchy exists
--          under the specified user.
----------------------------------------------------------------------------
FUNCTION VALIDATE_GROUP_MERCH(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exist               IN OUT  BOOLEAN,
                              I_filter_merch_level  IN      FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                              I_filter_merch_id     IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                              I_user_id             IN      SEC_USER.DATABASE_USER_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_DIFF_GROUP_HEAD
-- Purpose: This function will check if the Org or Merch Hierarchy value
--          exists in DIFF_GROUP_HEAD table.
----------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF_GROUP_HEAD(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                                  O_exist                    IN OUT BOOLEAN,
                                  I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                                  I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                                  I_filter_org_level         IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                                  I_filter_org_id            IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE,
                                  I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                                  I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)                                  
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_LOC_LIST_HEAD
-- Purpose: This function will check if the Org Hierarchy value
--          exists in LOC_LIST_HEAD table.
----------------------------------------------------------------------------
FUNCTION VALIDATE_LOC_LIST_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                                O_exist               IN OUT BOOLEAN,
                                I_filter_org_level    IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                                I_filter_org_id       IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_LOC_TRAITS
-- Purpose: This function will check if the Org Hierarchy value
--          exists in LOC_TRAITS table.
----------------------------------------------------------------------------
FUNCTION VALIDATE_LOC_TRAITS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                             O_exist               IN OUT BOOLEAN,
                             I_filter_org_level    IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                             I_filter_org_id       IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_SKULIST_HEAD
-- Purpose: This function will check if the Org or Merch Hierarchy value
--          exists in SKULIST_HEAD table.
----------------------------------------------------------------------------
FUNCTION VALIDATE_SKULIST_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                               O_exist               IN OUT BOOLEAN,
                               I_filter_org_level    IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                               I_filter_org_id       IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_SEASONS
-- Purpose: This function will check if the Org or Merch Hierarchy value
--          exists in SEASONS table.
----------------------------------------------------------------------------
FUNCTION VALIDATE_SEASONS(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                          O_exist                    IN OUT BOOLEAN,
                          I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                          I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                          I_filter_org_level         IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                          I_filter_org_id            IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE,
                          I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                          I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)                           
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_TICKET_TYPE_HEAD
-- Purpose: This function will check if the Org or Merch Hierarchy value
--          exists in TICKET_TYPE_HEAD table.
----------------------------------------------------------------------------
FUNCTION VALIDATE_TICKET_TYPE_HEAD(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                                   O_exist                    IN OUT BOOLEAN,
                                   I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                                   I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                                   I_filter_org_level         IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                                   I_filter_org_id            IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE,
                                   I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                                   I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Name:    VALIDATE_UDA
-- Purpose: This function will check if the Org or Merch Hierarchy value
--          exists in UDA table.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_UDA(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                      O_exist                    IN OUT BOOLEAN,
                      I_filter_merch_level       IN     FILTER_GROUP_MERCH.FILTER_MERCH_LEVEL%TYPE,
                      I_filter_merch_id          IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID%TYPE,
                      I_filter_org_level         IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                      I_filter_org_id            IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE,
                      I_filter_merch_id_class    IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                      I_filter_merch_id_subclass IN     FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)                    
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION LOCK_FILTER_GROUP_RECORD(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE, 
                                  O_locked_ind     IN OUT  BOOLEAN,
                                  I_merch_org      IN      VARCHAR2,
                                  I_sec_group_id   IN      FILTER_GROUP_MERCH.SEC_GROUP_ID%TYPE,
                                  I_filter_level   IN      FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                                  I_filter_id      IN      NUMBER,
                                  I_class          IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID_CLASS%TYPE,
                                  I_subclass       IN      FILTER_GROUP_MERCH.FILTER_MERCH_ID_SUBCLASS%TYPE)                                  
RETURN BOOLEAN;
--------------------------------------------------------------------------------
END FILTER_GROUP_HIER_SQL;
/
