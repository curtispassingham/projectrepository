CREATE OR REPLACE PACKAGE RETAIL_API_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------------------
-- Function: GET_DEPS_MARKUP_INFO
-- Purpose : Fetches markup_calc_type and markup_percent info from deps table.
---------------------------------------------------------------------------------------------------------
FUNCTION GET_DEPS_MARKUP_INFO(O_error_message     IN OUT  VARCHAR2,
                              O_markup_calc_type  IN OUT  DEPS.MARKUP_CALC_TYPE%TYPE,
                              O_markup_percent    IN OUT  NUMBER,
                              I_dept              IN      DEPS.DEPT%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function: GET_SUPP_VAT_RATE
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_VAT_RATE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_vat_rate       IN OUT  VAT_CODE_RATES.VAT_RATE%TYPE,
                           O_tax_amount     IN OUT  GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE,
                           I_supplier       IN      SUPS.SUPPLIER%TYPE,
                           I_dept           IN      ITEM_MASTER.DEPT%TYPE,
                           I_item           IN      ITEM_MASTER.ITEM%TYPE,
                           I_unit_retail    IN      ITEM_LOC.UNIT_RETAIL%TYPE DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function: CALC_RETAIL
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION CALC_RETAIL(O_error_message         OUT  VARCHAR2,
                     O_selling_retail        OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                     O_selling_uom           OUT  ITEM_LOC.SELLING_UOM%TYPE,
                     O_multi_units           OUT  ITEM_LOC.MULTI_UNITS%TYPE,
                     O_multi_unit_retail     OUT  ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                     O_multi_selling_uom     OUT  ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                     O_currency_code         OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                     I_item               IN      ITEM_MASTER.ITEM%TYPE,
                     I_dept               IN      ITEM_MASTER.DEPT%TYPE,
                     I_class              IN      ITEM_MASTER.CLASS%TYPE,
                     I_subclass           IN      ITEM_MASTER.SUBCLASS%TYPE,
                     I_location           IN      ITEM_LOC.LOC%TYPE,
                     I_loc_type           IN      ITEM_LOC.LOC_TYPE%TYPE,
                     I_unit_cost          IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  DEFAULT NULL,
                     I_currency           IN      SUPS.CURRENCY_CODE%TYPE           DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function: CALCULATE_RETAIL_FROM_MKUP
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION CALCULATE_RETAIL_FROM_MKUP(O_error_message           OUT  VARCHAR2,
                                    O_selling_retail          OUT  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                    O_selling_uom             OUT  ITEM_LOC.SELLING_UOM%TYPE,
                                    O_selling_retail_curr     OUT  CURRENCIES.CURRENCY_CODE%TYPE,
                                    I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                                    I_dept                 IN      ITEM_MASTER.DEPT%TYPE,
                                    I_class                IN      ITEM_MASTER.CLASS%TYPE,
                                    I_markup_calc_type     IN      VARCHAR2,
                                    I_markup_percent       IN      NUMBER,
                                    I_location             IN      ITEM_LOC.LOC%TYPE,
                                    I_loc_type             IN      ITEM_LOC.LOC_TYPE%TYPE,
                                    I_loc_currency         IN      STORE.CURRENCY_CODE%TYPE,
                                    I_unit_cost            IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  DEFAULT NULL,
                                    I_supp_currency        IN      SUPS.CURRENCY_CODE%TYPE           DEFAULT NULL)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function: CALC_RETAIL_FROM_MARKUP
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_FROM_MARKUP(O_error_message        OUT  VARCHAR2,
                                 O_unit_retail          OUT  ITEM_LOC.UNIT_RETAIL%TYPE,
                                 I_markup_percent    IN      NUMBER,
                                 I_markup_calc_type  IN      VARCHAR2,
                                 I_unit_cost         IN      ITEM_LOC_SOH.UNIT_COST%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function: ADD_TAX
-- Purpose :
---------------------------------------------------------------------------------------------------------
FUNCTION ADD_TAX(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item             IN     ITEM_MASTER.ITEM%TYPE,
                 I_dept             IN     ITEM_MASTER.DEPT%TYPE,
                 I_class            IN     ITEM_MASTER.CLASS%TYPE,
                 I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                 I_from_loc         IN     ITEM_LOC.LOC%TYPE,
                 I_from_entity_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                 I_from_loc_curr    IN     CURRENCIES.CURRENCY_CODE%TYPE,
                 I_date             IN     DATE,
                 IO_retail          IN OUT ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN;

END RETAIL_API_SQL;
/