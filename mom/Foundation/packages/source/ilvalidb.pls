



CREATE OR REPLACE PACKAGE BODY ITEMLIST_VALIDATE_SQL AS

-------------------------------------------------------------------
FUNCTION EXIST(	O_error_message	    IN OUT    VARCHAR2,
		I_skulist	    IN        NUMBER,
		O_exist		    IN OUT    BOOLEAN) RETURN BOOLEAN IS

   L_dummy	VARCHAR2(1);

   cursor C_EXIST is
	select 'x'
	from skulist_head
	where skulist = I_skulist;

BEGIN
   open C_EXIST;
   fetch C_EXIST into L_dummy;
   if C_EXIST%NOTFOUND then
	O_error_message := sql_lib.create_msg('INV_SKU_LIST',
						null,null,null);
	O_exist := FALSE;
   else
	O_exist := TRUE;
   end if;

   close C_EXIST;
   return TRUE;

EXCEPTION
   when OTHERS then
	O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                               SQLERRM,
                                               'ITEMLIST_VALIDATE_SQL.EXIST',
                                               to_char(SQLCODE));
	return FALSE;
END EXIST;
---------------------------------------------------------------------
FUNCTION CHECK_EDIT_EXISTS(O_error_message   IN OUT	VARCHAR2,
		           O_exist	     IN OUT	BOOLEAN,
                           I_itemlist        IN      SKULIST_HEAD.SKULIST%TYPE,
                           I_user_id         IN      SKULIST_HEAD.CREATE_ID%TYPE) RETURN BOOLEAN IS
   L_dummy VARCHAR2(1);

    cursor C_EDIT_EXIST is
    select 'Y'
      from skulist_head
     where skulist = I_itemlist
       and (create_id = I_user_id
        or user_security_ind = 'N');

BEGIN
   if I_itemlist is NULL or I_user_id is NULL then
      O_error_message := sql_lib.create_msg ('INVALID_PARM',
                                             NULL,
                                             NULL,
                                             NULL);   
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_EDIT_EXIST', 'SKULIST_HEAD', 'ITEMLIST_ID: '||I_itemlist);
   open C_EDIT_EXIST;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_EDIT_EXIST', 'SKULIST_HEAD', 'ITEMLIST_ID: '||I_itemlist);                         
   fetch C_EDIT_EXIST into L_dummy;
   ---
   if C_EDIT_EXIST%NOTFOUND then
      O_exist := FALSE;
   else
      O_exist := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_EDIT_EXIST', 'SKULIST_HEAD', 'ITEMLIST_ID: '||I_itemlist);
   close C_EDIT_EXIST;
   ---
   return TRUE;
EXCEPTION 
   when OTHERS then
      O_error_message := sql_lib.create_msg ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ITEMLIST_VALIDATE_SQL.CHECK_EDIT_EXISTS',
                                             to_char(SQLCODE));
	return FALSE;
END CHECK_EDIT_EXISTS;
-------------------------------------------------------------------
FUNCTION SUPPLIER_EXISTS(O_error_message    IN OUT VARCHAR2,
                         O_item_supp_exists IN OUT BOOLEAN,
                         I_skulist          IN     SKULIST_HEAD.SKULIST%TYPE,
                         I_supplier         IN     ITEM_SUPPLIER.SUPPLIER%TYPE) RETURN BOOLEAN IS
   L_dummy VARCHAR2(1);
   L_program VARCHAR2(64):= 'ITEMLIST_VALIDATE_SQL.SUPPLIER_EXISTS';
   
   cursor C_SUPPLIER_EXISTS is
      select 'x'
        from v_sups s,
             item_supplier is1,
             skulist_detail sd,
             v_skulist_head sh
       where sh.skulist = I_skulist
         and sh.skulist = sd.skulist
         and sd.item    = is1.item
         and is1.supplier = s.supplier
         and is1.supplier = I_supplier
         and rownum < 2;
         
BEGIN
   if I_skulist is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_skulist',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN', 
                    'C_SUPPLIER_EXISTS', 
                    'V_SKULIST_HEAD,SKULIST_DETAIL,ITEM_SUPPLIER,SUPS',
                    'ITEMLIST_ID: '||I_skulist||' SUPPLIER: '||I_supplier);
   open C_SUPPLIER_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUPPLIER_EXISTS',
                    'V_SKULIST_HEAD,SKULIST_DETAIL,ITEM_SUPPLIER,SUPS',
                    'ITEMLIST_ID: '||I_skulist||' SUPPLIER: '||I_supplier);                       
   fetch C_SUPPLIER_EXISTS into L_dummy;
   ---
   if C_SUPPLIER_EXISTS%FOUND THEN
      O_item_supp_exists := TRUE;
   else
      O_item_supp_exists := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUPPLIER_EXISTS', 
                    'V_SKULIST_HEAD,SKULIST_DETAIL,ITEM_SUPPLIER,SUPS',
                    'ITEMLIST_ID: '||I_skulist||' SUPPLIER: '||I_supplier);                       
   close C_SUPPLIER_EXISTS;
     
   return TRUE;
EXCEPTION 
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END SUPPLIER_EXISTS;
---------------------------------------------------------------------

END ITEMLIST_VALIDATE_SQL;
/


