
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE VWH_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
--- Global types
--------------------------------------------------------------------------------

--- These will be used by the vwh form.  When new vwh are added, they will be
--- collected in the table.  Upon exiting the form the new vwh table will be
--- sent to the NEW_VWH_INSERTS function.
TYPE new_vwh_rec is RECORD(vwh               WH.WH%TYPE,
                           currency_code     WH.CURRENCY_CODE%TYPE,
                           pricing_location  WH_ADD.PRICING_LOCATION%TYPE,
                           pricing_loc_curr  WH_ADD.PRICING_LOC_CURR%TYPE);

TYPE new_vwh_table is TABLE of new_vwh_rec INDEX BY BINARY_INTEGER;

--------------------------------------------------------------------------------
-- INSERT_COST_ZONES_ID
-- Insert cost zones for Virtual Warehouses.
--------------------------------------------------------------------------------
FUNCTION INSERT_COST_ZONES_ID (O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_zone_records  IN OUT  BOOLEAN,
                               I_pwh                IN      WH.WH%TYPE,
                               I_vwh                IN      WH.WH%TYPE,
                               I_currency_code      IN      WH.CURRENCY_CODE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- PWH_ON_STOCK_COUNT
-- Determines if the passed in physical warehouse is on any stock counts by
-- searching for it's virtual warehouses on the stock count location table.
--------------------------------------------------------------------------------
FUNCTION PWH_ON_OPEN_STOCK_COUNT(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_stock_count_exists  IN OUT  BOOLEAN,
                                 I_pwh                 IN      WH.WH%TYPE,
                                 I_vwh                 IN      WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--- VWH_FORM_REC
--- Returns info for each record in the form.  Use this in the post-query
--- instead of making several database calls for each record.
--------------------------------------------------------------------------------
FUNCTION VWH_FORM_REC(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_channel_desc           IN OUT  CHANNELS.CHANNEL_NAME%TYPE,
                      O_tsf_entity_desc        IN OUT  TSF_ENTITY.TSF_ENTITY_DESC%TYPE,
                      O_wh_name_tl             IN OUT  WH.WH_NAME%TYPE,
                      O_pricing_location       IN OUT  WH_ADD.PRICING_LOCATION%TYPE,
                      O_pricing_location_name  IN OUT  WH.WH_NAME%TYPE,
                      O_pricing_location_curr  IN OUT  WH.CURRENCY_CODE%TYPE,
                      I_channel_id             IN      CHANNELS.CHANNEL_ID%TYPE,
                      I_tsf_entity_id          IN      TSF_ENTITY.TSF_ENTITY_ID%TYPE,
                      I_wh_name                IN      WH.WH_NAME%TYPE,
                      I_vwh                    IN      WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- GET_PRICING_LOC_INFO
-- Returns the pricing loc description and currency code when a valid pricing
-- location is found.  Returns O_valid = FALSE if a valid pricing location
-- is not found.
--------------------------------------------------------------------------------
FUNCTION GET_PRICING_LOC_INFO(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid             IN OUT  BOOLEAN,
                              O_pricing_loc_name  IN OUT  WH.WH_NAME%TYPE,
                              O_pricing_loc_curr  IN OUT  WH.CURRENCY_CODE%TYPE,
                              I_pricing_loc       IN OUT  WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- NEW_VWH_INSERTS
-- Calls functions and inserts records for new vwh records.
--------------------------------------------------------------------------------
FUNCTION NEW_VWH_INSERTS(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_cost_zone_records   IN OUT  BOOLEAN,
                         O_stock_count_exists  IN OUT  BOOLEAN,
                         I_pwh                 IN      WH.WH%TYPE,
                         I_new_vwh_table       IN      VWH_SQL.NEW_VWH_TABLE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- UPDATE_WH_ADD
-- Updates the wh_add table
--------------------------------------------------------------------------------
FUNCTION UPDATE_WH_ADD(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_vwh_table           IN      VWH_SQL.NEW_VWH_TABLE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
END VWH_SQL;
/
