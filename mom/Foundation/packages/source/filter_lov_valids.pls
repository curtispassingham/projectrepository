CREATE OR REPLACE PACKAGE FILTER_LOV_VALIDATE_SQL AUTHID CURRENT_USER AS
----------------------------------------------------------------------------
-- Name:    VALIDATE_CHAIN
-- Purpose: This function will check if the user has visibility to the
--          entered chain using V_CHAIN view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_chain_name    IN OUT V_CHAIN.CHAIN_NAME%TYPE,
                        I_chain         IN     V_CHAIN.CHAIN%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_AREA
-- Purpose: This function will check if the user has visibility to the
--          entered area using V_AREA view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_area_name     IN OUT V_AREA.AREA_NAME%TYPE,
                       I_chain         IN     V_CHAIN.CHAIN%TYPE,
                       I_area          IN     V_AREA.AREA%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_area_name     IN OUT V_AREA.AREA_NAME%TYPE,
                       I_area          IN     V_AREA.AREA%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_area_name     IN OUT V_AREA.AREA_NAME%TYPE,
                       I_area          IN     V_AREA.AREA%TYPE,
                       I_org_level     IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                       I_org_id        IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_REGION
-- Purpose: This function will check if the user has visibility to the
--          entered region using V_REGION view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_region_name   IN OUT V_REGION.REGION_NAME%TYPE,
                         I_chain         IN     V_CHAIN.CHAIN%TYPE,
                         I_area          IN     V_AREA.AREA%TYPE,
                         I_region        IN     V_REGION.REGION%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_look_down_ind IN OUT VARCHAR2,
                         O_region_name   IN OUT V_REGION.REGION_NAME%TYPE,
                         I_region        IN     V_REGION.REGION%TYPE,
                         I_org_level     IN     FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE,
                         I_org_id        IN     FILTER_GROUP_ORG.FILTER_ORG_ID%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_region_name   IN OUT V_REGION.REGION_NAME%TYPE,
                         I_region        IN     V_REGION.REGION%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_DISTRICT
-- Purpose: This function will check if the user has visibility to the
--          entered district using V_DISTRICT view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_district_name IN OUT V_DISTRICT.DISTRICT_NAME%TYPE,
                           I_chain         IN     V_CHAIN.CHAIN%TYPE,
                           I_area          IN     V_AREA.AREA%TYPE,
                           I_region        IN     V_REGION.REGION%TYPE,
                           I_district      IN     V_DISTRICT.DISTRICT%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_district_name IN OUT V_DISTRICT.DISTRICT_NAME%TYPE,
                           I_district      IN     V_DISTRICT.DISTRICT%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_STORE
-- Purpose: This function will check if the user has visibility to the
--          entered store using V_STORE view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_STORE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_store         IN OUT V_STORE%ROWTYPE,
                        I_chain         IN     V_CHAIN.CHAIN%TYPE,
                        I_area          IN     V_AREA.AREA%TYPE,
                        I_region        IN     V_REGION.REGION%TYPE,
                        I_district      IN     V_DISTRICT.DISTRICT%TYPE,
                        I_store         IN     V_STORE.STORE%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_STORE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_store_name    IN OUT V_STORE.STORE_NAME%TYPE,
                        I_chain         IN     V_CHAIN.CHAIN%TYPE,
                        I_area          IN     V_AREA.AREA%TYPE,
                        I_region        IN     V_REGION.REGION%TYPE,
                        I_district      IN     V_DISTRICT.DISTRICT%TYPE,
                        I_store         IN     V_STORE.STORE%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_STORE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_store_name    IN OUT V_STORE.STORE_NAME%TYPE,
                        I_store         IN     V_STORE.STORE%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_STORE (O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid            IN OUT BOOLEAN,
                         I_store            IN     V_STORE.STORE%TYPE,
                         I_total_id         IN     SA_TOTAL_LOC_TRAIT.TOTAL_ID%TYPE,
                         I_set_of_books_id  IN     MV_LOC_SOB.SET_OF_BOOKS_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_WH
-- Purpose: This function will check if the user has visibility to the
--          entered warehouse using V_WH view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_WH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_valid         IN OUT BOOLEAN,
                     O_wh            IN OUT V_WH%ROWTYPE,
                     I_wh            IN     V_WH.WH%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_PHYSICAL_WH
-- Purpose: This function will check if the user has visibility to the
--          entered physical warehouse using V_PHYSICAL_WH view.
--          if the user has access to ALL virtual warehouses and internal
--          finishers within that physical warehouse O_full_ind = 'Y'.
--          else O_full_ind = 'N'
--          O_valid will be TRUE for partial or full access.
----------------------------------------------------------------------------
FUNCTION VALIDATE_PHYSICAL_WH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid         IN OUT BOOLEAN,
                              O_full_ind      IN OUT VARCHAR2,
                              O_wh            IN OUT V_PHYSICAL_WH%ROWTYPE,
                              I_wh            IN     V_PHYSICAL_WH.WH%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_LOCATION
-- Purpose: This function will check if the user has visibility to the
--          entered store using V_STORE view or warehouse using V_WH view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid            IN OUT BOOLEAN,
                           O_loc_name         IN OUT V_STORE.STORE_NAME%TYPE,
                           O_stockholding_ind IN OUT V_STORE.STOCKHOLDING_IND%TYPE,
                           I_location         IN     V_STORE.STORE%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_COSTING_LOCATION
-- Purpose: This function will check if the entered costing location(store/wh)
--          is valid or not.
----------------------------------------------------------------------------
FUNCTION VALIDATE_COSTING_LOCATION(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_valid            IN OUT BOOLEAN,
                                   O_loc_name         IN OUT V_STORE.STORE_NAME%TYPE,
                                   O_loc_type         IN OUT V_STORE.STORE_TYPE%TYPE,                                   
                                   I_location         IN     V_STORE.STORE%TYPE)
 
RETURN BOOLEAN;

--------------------------------------------------------------------------
-- Name:    VALIDATE_DIVISION
-- Purpose: This function will check if the user has visibility to the
--          entered division using V_DIVISION view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_DIVISION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_div_name      IN OUT V_DIVISION.DIV_NAME%TYPE,
                           I_division      IN     V_DIVISION.DIVISION%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_GROUPS
-- Purpose: This function will check if the user has visibility to the
--          entered group_no using V_GROUPS view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_GROUPS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_group_name    IN OUT V_GROUPS.GROUP_NAME%TYPE,
                         I_division      IN     V_DIVISION.DIVISION%TYPE,
                         I_group_no      IN     V_GROUPS.GROUP_NO%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_GROUPS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_group_name    IN OUT V_GROUPS.GROUP_NAME%TYPE,
                         I_group_no      IN     V_GROUPS.GROUP_NO%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_DEPS
-- Purpose: This function will check if the user has visibility to the
--          entered dept using V_DEPS view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_DEPS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_dept_name     IN OUT V_DEPS.DEPT_NAME%TYPE,
                       I_division      IN     V_DIVISION.DIVISION%TYPE,
                       I_group_no      IN     V_GROUPS.GROUP_NO%TYPE,
                       I_dept          IN     V_DEPS.DEPT%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_DEPS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid         IN OUT BOOLEAN,
                       O_dept_name     IN OUT V_DEPS.DEPT_NAME%TYPE,
                       I_dept          IN     V_DEPS.DEPT%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_DEPS_INV
-- Purpose: This function will check if the user has visibility to the
--          entered dept using V_DEPS view and also if the dept can be used
--          for inventory related function
----------------------------------------------------------------------------
FUNCTION VALIDATE_DEPS_INV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_dept_name     IN OUT V_DEPS.DEPT_NAME%TYPE,
                           I_dept          IN     V_DEPS.DEPT%TYPE)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
-- Name:    VALIDATE_CLASS
-- Purpose: This function will check if the user has visibility to the
--          entered class using V_CLASS view
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid         IN OUT BOOLEAN,
                        O_class_name    IN OUT V_CLASS.CLASS_NAME%TYPE,
                        I_dept          IN     V_CLASS.DEPT%TYPE,
                        I_class         IN     V_CLASS.CLASS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Name:    VALIDATE_SUBCLASS
-- Purpose: This function will check if the user has visibility to the
--          entered subclass using V_SUBCLASS view
--------------------------------------------------------------------------------
FUNCTION VALIDATE_SUBCLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_subclass_name IN OUT V_SUBCLASS.SUB_NAME%TYPE,
                           I_dept          IN     V_SUBCLASS.DEPT%TYPE,
                           I_class         IN     V_SUBCLASS.CLASS%TYPE,
                           I_subclass      IN     V_SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_ITEM_MASTER
-- Purpose: This function will check if the user has visibility to the
--          entered item using V_ITEM_MASTER view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_MASTER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid         IN OUT BOOLEAN,
                              O_item_master   IN OUT V_ITEM_MASTER%ROWTYPE,
                              I_division      IN     V_ITEM_MASTER.DIVISION%TYPE,
                              I_group_no      IN     V_ITEM_MASTER.GROUP_NO%TYPE,
                              I_dept          IN     V_ITEM_MASTER.DEPT%TYPE,
                              I_class         IN     V_ITEM_MASTER.CLASS%TYPE,
                              I_subclass      IN     V_ITEM_MASTER.SUBCLASS%TYPE,
                              I_item          IN     V_ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_ITEM_MASTER
-- Purpose: This function will check if the user has visibility to the
--          entered item using V_ITEM_MASTER view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_ITEM_MASTER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid         IN OUT BOOLEAN,
                              O_item_master   IN OUT V_ITEM_MASTER%ROWTYPE,
                              I_item          IN     V_ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_DIFF_GROUP_HEAD
-- Purpose: This function will check if the user has visibility to the
--          entered diff_group_id using V_DIFF_GROUP_HEAD view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF_GROUP_HEAD(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_valid           IN OUT BOOLEAN,
                                  O_diff_group_desc IN OUT V_DIFF_GROUP_HEAD.DIFF_GROUP_DESC%TYPE,
                                  I_diff_type       IN     V_DIFF_GROUP_HEAD.DIFF_TYPE%TYPE,
                                  I_diff_group_id   IN     V_DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_LOC_LIST_HEAD
-- Purpose: This function will check if the user has visibility to the
--          entered loc_list using V_LOC_LIST_HEAD view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_LOC_LIST_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid         IN OUT BOOLEAN,
                                O_loc_list_desc IN OUT V_LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                                I_loc_list      IN     V_LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_LOC_TRAITS
-- Purpose: This function will check if the user has visibility to the
--          entered loc_trait using V_LOC_TRAITS view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_LOC_TRAITS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT BOOLEAN,
                             O_description   IN OUT V_LOC_TRAITS.DESCRIPTION%TYPE,
                             I_loc_trait     IN     V_LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_SKULIST_HEAD
-- Purpose: This function will check if the user has visibility to the
--          entered skulist using V_SKULIST_HEAD view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_SKULIST_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid         IN OUT BOOLEAN,
                               O_skulist_desc  IN OUT V_SKULIST_HEAD.SKULIST_DESC%TYPE,
                               I_skulist       IN     V_SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN;

FUNCTION VALIDATE_SKULIST_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid         IN OUT BOOLEAN,
                               O_skulist_head  IN OUT V_SKULIST_HEAD%ROWTYPE,
                               I_skulist       IN     V_SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_SEASONS
-- Purpose: This function will check if the user has visibility to the
--          entered season_id using V_SEASONS view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_SEASONS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_valid         IN OUT BOOLEAN,
                          O_season_desc   IN OUT V_SEASONS.SEASON_DESC%TYPE,
                          I_season_id     IN     V_SEASONS.SEASON_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_TICKET_TYPE_HEAD
-- Purpose: This function will check if the user has visibility to the
--          entered ticket_type_id using V_TICKET_TYPE_HEAD view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_TICKET_TYPE_HEAD(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_valid            IN OUT BOOLEAN,
                                   O_ticket_type_desc IN OUT V_TICKET_TYPE_HEAD.TICKET_TYPE_DESC%TYPE,
                                   I_ticket_type_id   IN     V_TICKET_TYPE_HEAD.TICKET_TYPE_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_UDA
-- Purpose: This function will check if the user has visibility to the
--          entered uda_id using V_UDA view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_UDA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_valid         IN OUT BOOLEAN,
                      O_uda           IN OUT V_UDA%ROWTYPE,
                      O_uda_desc      IN OUT V_UDA.UDA_DESC%TYPE,
                      I_uda_id        IN     V_UDA.UDA_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_UDA
-- Purpose: This function will check if the user has visibility to the
--          entered uda_id using V_UDA view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_UDA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_valid         IN OUT BOOLEAN,
                      O_uda_desc      IN OUT V_UDA.UDA_DESC%TYPE,
                      I_uda_id        IN     V_UDA.UDA_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_TRANSFER_STORE
-- Purpose: This function will check if the user has visibility to the
--          entered store using V_TRANSFER_FROM_STORE or V_TRANSFER_TO_STORE
--          views, depending upon the value of I_to_from_ind.
----------------------------------------------------------------------------
FUNCTION VALIDATE_TRANSFER_STORE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid          IN OUT BOOLEAN,
                                 O_store_name     IN OUT V_TRANSFER_FROM_STORE.STORE_NAME%TYPE,
                                 IO_tsf_entity_id IN OUT V_TRANSFER_FROM_STORE.TSF_ENTITY_ID%TYPE,
                                 I_store          IN     V_TRANSFER_FROM_STORE.STORE%TYPE,
                                 I_to_from_ind    IN     VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Name:    VALIDATE_TRANSFER_WH
-- Purpose: This function will check if the user has visibility to the
--          entered pwh or vwh using the V_TRANSFER_FROM_WH or V_TRANSFER_TO_WH view.
--          Which view is used is dictated by the to_from paramter.
----------------------------------------------------------------------------
FUNCTION VALIDATE_TRANSFER_WH(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid          IN OUT BOOLEAN,
                              O_wh_name        IN OUT V_TRANSFER_FROM_WH.WH_NAME%TYPE,
                              IO_tsf_entity_id IN OUT V_TRANSFER_FROM_WH.TSF_ENTITY_ID%TYPE,
                              I_wh             IN     V_TRANSFER_FROM_WH.WH%TYPE,
                              I_pwh_vwh_ind    IN     VARCHAR2,
                              I_to_from_ind    IN     VARCHAR2)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Name:    VALIDATE_LOCATION_TYPE
-- Purpose: This function will check I_loc_type and call validate
--          function for the value.  Valid location types are:  D-Division,
--          R-Region, A-Area, S-store, L-Location Trait, LL-Location Lists,
--          LLW-Location Lists - WH, LLS-Location Lists - Store,
--          C-Store Class, P-Promotion Zone, T-Transfer Zone, Z-Price Zone ID,
--          W-Warehouse, PW-Physical Warehouse, DW-Default Warehouse,
--          I-internal Finisher, E-External Finisher, M-Importer, X-Exporter
--
--          IO_second_value - used for zone_group_id when validating Price Zone
--                            and tsf_entity_id when validating Finishers
--                            Finisher validation will also assign the TSF_ENTITY_ID
--                            to this variable if it is passed null.
--                            Store and WH validation will assign the STOCKHOLDING_IND
----------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION_TYPE(O_message       IN OUT VARCHAR2,
                                O_valid         IN OUT BOOLEAN,
                                O_value_desc    IN OUT VARCHAR2,
                                IO_second_value IN OUT VARCHAR2,
                                I_loc_type      IN     VARCHAR2,
                                I_value         IN     VARCHAR2)
         return BOOLEAN;
----------------------------------------------------------------------------
-- Name:    VALIDATE_MERCH_LEVEL
-- Purpose: This function will check I_merch_level_ind and call validate
--          function for the value in I_merch_level_ind (ex, 'D' will validate
--          Division, 'G' will validate Group, etc.).
----------------------------------------------------------------------------
FUNCTION VALIDATE_MERCH_LEVEL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid           IN OUT BOOLEAN,
                              O_name            IN OUT VARCHAR2,
                              I_merch_level_ind IN     VARCHAR2,
                              I_merch_level_id  IN     NUMBER,
                              I_class           IN     V_CLASS.CLASS%TYPE,
                              I_subclass        IN     V_SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    VALIDATE_ORG_LEVEL
-- Purpose: This function will check I_org_level_ind and call validate
--          function for the value in I_org_level_ind (eg, 'C' will validate
--          Chain, 'R' will validate Region, etc.).
----------------------------------------------------------------------------
FUNCTION VALIDATE_ORG_LEVEL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT  BOOLEAN,
                            O_name           IN OUT  VARCHAR2,
                            I_org_level_ind  IN      VARCHAR2,
                            I_org_level_id   IN      NUMBER)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Name:    VALIDATE_STORE_CLASS
-- Purpose: This function will check if the user has visibility to at least
--          one store within the given store class.
----------------------------------------------------------------------------
FUNCTION VALIDATE_STORE_CLASS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid            IN OUT  BOOLEAN,
                              O_store_class_desc IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              I_store_class      IN      STORE.STORE_CLASS%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Name:    VALIDATE_SHIPMENT
-- Purpose: This function will check if the user has visibility to the
--          entered shipment which is be limited by the user's association
--          to the Merchandise or Organization Hierarchies and the Products
--          and Locations on SHIPSKU table
---------------------------------------------------------------------------------
FUNCTION VALIDATE_SHIPMENT(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid           IN OUT  BOOLEAN,
                           O_shipment        IN OUT  V_SHIPMENT%ROWTYPE,
                           I_shipment        IN      SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Name:    VALIDATE_ORDER
-- Purpose: This function will check if the user has visibility to the specified
--          order number using V_ORDHEAD view.
----------------------------------------------------------------------------
FUNCTION VALIDATE_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_valid          IN OUT  BOOLEAN,
                        O_ordhead        IN OUT  V_ORDHEAD%ROWTYPE,
                        I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- VALIDATE_RTV_ORDER_NO
-- Check to see if RTV_ORDER_NO exists on the V_RTV_HEAD view, which may be limited by items
-- and/or locations based on the org and merch hierarchy settings.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RTV_ORDER_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid          IN OUT  BOOLEAN,
                               O_rtv_head_row   IN OUT  V_RTV_HEAD%ROWTYPE,
                               I_rtv_order_no   IN      RTV_HEAD.RTV_ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_tsfhead_row   IN OUT V_TSFHEAD%ROWTYPE,
                         I_tsf_no        IN     V_TSFHEAD.TSF_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
FUNCTION VALIDATE_PACK_TMPL_HEAD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid          IN OUT BOOLEAN,
                                 O_pack_tmpl_head IN OUT V_PACK_TMPL_HEAD%ROWTYPE,
                                 I_pack_tmpl_id   IN     V_PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF_RATIO_ID(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT BOOLEAN,
                                O_diff_ratio_head IN OUT V_DIFF_RATIO_HEAD%ROWTYPE,
                                I_diff_ratio_id   IN     V_DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------


-----------------------------------------------------------------------------
-- Name:    VALIDATE_TSF_ZONE
-- Purpose: This function will check if the user has visibility to the specified
--          transfer zone.
----------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_ZONE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid         IN OUT BOOLEAN,
                            O_tsf_desc      IN OUT TSFZONE.DESCRIPTION%TYPE,
                            I_tsf_zone      IN     TSFZONE.TRANSFER_ZONE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Name:    VALIDATE_INTERNAL_FINISHER
-- Purpose: This function validates Internal Finishers (ie warehouses)
----------------------------------------------------------------------------

FUNCTION VALIDATE_INTERNAL_FINISHER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_valid            IN OUT BOOLEAN,
                                    O_finisher_desc    IN OUT V_INTERNAL_FINISHER.FINISHER_DESC%TYPE,
                                    IO_tsf_entity_id   IN OUT V_INTERNAL_FINISHER.TSF_ENTITY_ID%TYPE,
                                    I_finisher         IN     V_INTERNAL_FINISHER.FINISHER_ID%TYPE)

RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Name:    VALIDATE_EXTERNAL_FINISHER
-- Purpose: This function validates External Finishers (ie partners)
----------------------------------------------------------------------------
FUNCTION VALIDATE_EXTERNAL_FINISHER(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_valid            IN OUT BOOLEAN,
                                    O_finisher_desc    IN OUT V_EXTERNAL_FINISHER.FINISHER_DESC%TYPE,
                                    IO_tsf_entity_id   IN OUT V_EXTERNAL_FINISHER.TSF_ENTITY_ID%TYPE,
                                    I_finisher         IN     V_EXTERNAL_FINISHER.FINISHER_ID%TYPE)

RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Name:    VALIDATE_TSF_ENTITY
-- Purpose: This function validates Transfer Entities.
----------------------------------------------------------------------------
FUNCTION VALIDATE_TSF_ENTITY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid           IN OUT BOOLEAN,
                             O_tsf_entity_row  IN OUT V_TSF_ENTITY%ROWTYPE,
                             I_tsf_entity_id   IN     V_TSF_ENTITY.TSF_ENTITY_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------
-- Name:    VALIDATE_SHIP_ORDER
-- Purpose: This function will check if the user has visibility to the specified
--          order number using V_SHIPMENT view for orders associated with shipments.
----------------------------------------------------------------------------
FUNCTION VALIDATE_SHIP_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid          IN OUT  BOOLEAN,
                             O_ordhead        IN OUT  ORDHEAD.ORDER_NO%TYPE,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_CONTRACT
-- Purpose: This function will check if the user has visibility to the specified
--          contract number using V_CONTRACT_HEADER view.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CONTRACT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid            IN OUT  BOOLEAN,
                           O_contract_header  IN OUT  V_CONTRACT_HEADER%ROWTYPE,
                           I_contract_no      IN      CONTRACT_HEADER.CONTRACT_NO%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Name:     VALIDATE_COUPON_ID
-- Purpose:  This function will check if the user has visibility to the specified
--           coupon ID using v_pos_coupon_head view for coupon id's associated
--           with merchandise or organizational hierarchy.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COUPON_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid           IN OUT   BOOLEAN,
                            I_coupon_id       IN       POS_COUPON_HEAD.COUPON_ID%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Name:     VALIDATE_CE_ID
-- Purpose:  This function will check if the user has visibility to the specified
--           Customs Entry Ref. ID using v_ce_head view for CE ID's associated
--           with merchandise or organizational hierarchy.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CE_ID (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid           IN OUT   BOOLEAN,
                         O_ce_head         IN OUT   CE_HEAD%ROWTYPE,
                         I_ce_id           IN       CE_HEAD.CE_ID%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
-- Name:    MANUAL_LOC_TRAITS
-- Purpose: This function will check if the user has visibility to the
--          entered loc_trait using V_LOC_TRAITS view.
----------------------------------------------------------------------------
FUNCTION MANUAL_LOC_TRAITS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid           IN OUT   BOOLEAN,
                           O_description     IN OUT   V_LOC_TRAITS.DESCRIPTION%TYPE,
                           I_loc_trait       IN       V_LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------
FUNCTION VALIDATE_MRT_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid_mrt      IN OUT  BOOLEAN,
                         O_mrt_row        IN OUT  V_MRT_ITEM%ROWTYPE,
                         I_mrt_no         IN      MRT.MRT_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------

FUNCTION MANUAL_COMP_STORE(O_error_message   IN OUT   VARCHAR2,
                           O_valid           IN OUT   BOOLEAN,
                           O_store_name      IN OUT   V_COMP_STORE.STORE_NAME%TYPE,
                           O_competitor      IN OUT   COMPETITOR.COMPETITOR%TYPE,
                           O_comp_name       IN OUT   COMPETITOR.COMP_NAME%TYPE,
                           O_currency        IN OUT   V_COMP_STORE.CURRENCY_CODE%TYPE,
                           I_comp_store      IN       V_COMP_STORE.STORE%TYPE)

RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MRT_ITEM(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid_mrt      IN OUT  BOOLEAN,
                         O_mrt_row        IN OUT  V_MRT_ITEM%ROWTYPE,
                         I_mrt_no         IN      V_MRT_ITEM.MRT_NO%TYPE ,
                         I_item           IN      V_MRT_ITEM.ITEM%TYPE )
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MRT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_valid_mrt      IN OUT  BOOLEAN,
                      I_mrt_no         IN      V_MRT_ITEM.MRT_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ENTRY_NO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid           IN OUT   BOOLEAN,
                           O_ce_head         IN OUT   CE_HEAD%ROWTYPE,
                           I_entry_no        IN       CE_HEAD.ENTRY_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COST_CHANGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid           IN OUT   BOOLEAN,
                              I_cost_change     IN       COST_SUSP_SUP_DETAIL.COST_CHANGE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TRANPO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid          IN OUT  BOOLEAN,
                         I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_SEC_WH
-- Purpose: This function will check if the user has visibility to the
--          entered physical warehouse according to the privileges of its
--          virtual warehouses using v_sec_wh view.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SEC_WH(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_wh            IN OUT V_SEC_WH%ROWTYPE,
                         I_wh            IN     V_WH.WH%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_CARTON
-- Purpose: This function will validate the entered carton id and determine
--          if the user should be able to access the record.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CARTON(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         I_carton_id     IN     SHIPSKU.CARTON%TYPE,
                         I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_ALLOC_NO
-- Purpose: This function will validate the entered allocation number and
--          determine if the user should be able to access the record.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ALLOC_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           O_alloc_header  IN OUT ALLOC_HEADER%ROWTYPE,
                           I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_RECV_TSF
-- Purpose: This function will validate the entered transfer number and
--          determine if the user should be able to access the record.  The transfer number
--          must be in a valid shipment for receiving.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RECV_TSF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid         IN OUT BOOLEAN,
                           I_tsf_no        IN     TSFHEAD.TSF_NO%TYPE,
                           I_ctn_ind       IN     VARCHAR2,
                           I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_RECV_ALLOC
-- Purpose: This function will validate the entered allocation number and
--          determine if the user should be able to access the record. The allocation number
--          must be in a valid shipment for receiving.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RECV_ALLOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT BOOLEAN,
                             I_alloc_no      IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_ctn_ind       IN     VARCHAR2,
                             I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_BOL_NO
-- Purpose: This function will validate the bol number and
--          determine if the user should be able to access the record.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_BOL_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid         IN OUT BOOLEAN,
                         O_bol_shipment  IN OUT BOL_SHIPMENT%ROWTYPE,
                         I_bol_no        IN     SHIPMENT.BOL_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_RECV_ORDER
-- Purpose: This function will validate the entered order number and
--          determine if the user should be able to access the record. The order number
--          must be in a valid shipment for receiving.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_RECV_ORDER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT BOOLEAN,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                             I_ctn_ind       IN     VARCHAR2,
                             I_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_SEC_ORDER
-- Purpose: This function will check if the user has visibility to the
--          entered order number according to the privileges of its
--          location using v_sec_ordhead view.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SEC_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT  BOOLEAN,
                            O_ordhead        IN OUT  V_SEC_ORDHEAD%ROWTYPE,
                            I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    COMPANY_STORE
-- Purpose: This function will check if the Store being passed is a Company Store or not.
-----------------------------------------------------------------------------------------------
FUNCTION COMPANY_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_company_store   IN OUT   BOOLEAN,
                       O_store_name      IN OUT   STORE.STORE_NAME%TYPE,
                       I_store           IN       STORE.STORE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    WS_FR_STORE
-- Purpose: This function will check if the Store being passed is
--          a Wholesale / Franchise Store or not.
-----------------------------------------------------------------------------------------------
FUNCTION WS_FR_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_ws_fr_store     IN OUT   BOOLEAN,
                     O_wf_store_name   IN OUT   STORE.STORE_NAME%TYPE,
                     I_wf_store        IN       STORE.STORE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    LOC_LIST_HEAD_TYPE
-- Purpose: This function will retrieve the location list type
--          of a given location list id.
-----------------------------------------------------------------------------------------------
FUNCTION LOC_LIST_HEAD_TYPE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_loc_list_desc        IN OUT   LOC_LIST_HEAD.LOC_LIST_DESC%TYPE,
                            I_loc_list             IN       LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    STOCKHOLDING_STORE
-- Purpose: This function will check if the Store being passed
--          is a Stockholding Store or not.
-----------------------------------------------------------------------------------------------
FUNCTION STOCKHOLDING_STORE(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_stockholding_store   IN OUT   BOOLEAN,
                            O_store_name           IN OUT   STORE.STORE_NAME%TYPE,
                            O_store_type           IN OUT   STORE.STORE_TYPE%TYPE,
                            I_store                IN       STORE.STORE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    REGION_STORE_TYPES
-- Purpose: This function will check if the Region being passed contains
--          Wholesale/Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
FUNCTION REGION_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_ws_fr_store     IN OUT   BOOLEAN,
                            O_company_store   IN OUT   BOOLEAN,
                            I_region          IN       REGION.REGION%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    DISTRICT_STORE_TYPES
-- Purpose: This function will check if the District being passed contains
--          Wholesale/Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
FUNCTION DISTRICT_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_ws_fr_store     IN OUT   BOOLEAN,
                              O_company_store   IN OUT   BOOLEAN,
                              I_district        IN       DISTRICT.DISTRICT%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Name:    AREA_STORE_TYPES
-- Purpose: This function will check if the Area being passed contains
--          Wholesale/Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
FUNCTION AREA_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_ws_fr_store     IN OUT   BOOLEAN,
                          O_company_store   IN OUT   BOOLEAN,
                          I_area            IN       V_AREA.AREA%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Name:    LOC_LIST_STORE_TYPES
-- Purpose: This function will check if the Location list being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
FUNCTION LOC_LIST_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_fr_store        IN OUT   BOOLEAN,
                              O_company_store   IN OUT   BOOLEAN,
                              I_loc_list        IN       LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    LOC_LIST_FR_NONSKHD_TYPES
-- Purpose: This function will check if the Location list being passed contains
--      Non stockholding Franchise Stores.
---------------------------------------------------------------------------------------------
FUNCTION LOC_LIST_FR_NONSKHD_TYPES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_fr_nonskhd_store    IN OUT   BOOLEAN,
                                   I_loc_list            IN       LOC_LIST_HEAD.LOC_LIST%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    TSF_ZONE_STORE_TYPES
-- Purpose: This function will check if the Transfer Zone being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
FUNCTION TSF_ZONE_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_fr_store        IN OUT   BOOLEAN,
                              O_company_store   IN OUT   BOOLEAN,
                              I_tsf_zone        IN       TSFZONE.TRANSFER_ZONE%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Name:    STORE_CLASS_STORE_TYPES
-- Purpose: This function will check if the Store Class being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION STORE_CLASS_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_fr_store        IN OUT   BOOLEAN,
                                 O_company_store   IN OUT   BOOLEAN,
                                 I_store_class     IN       STORE.STORE_CLASS%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    LOC_TRAITS_STORE_TYPES
-- Purpose: This function will check if the Location trait being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION LOC_TRAITS_STORE_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_fr_store          IN OUT   BOOLEAN,
                                O_company_store     IN OUT   BOOLEAN,
                                I_loc_trait         IN       LOC_TRAITS.LOC_TRAIT%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Name:    DEFAULT_WH_STORE_TYPES
-- Purpose: This function will check if the Default wh being passed contains
--          Franchise Stores and/or Company Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION DEFAULT_WH_STORE_TYPES(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_fr_store          IN OUT   BOOLEAN,
                                O_company_store     IN OUT   BOOLEAN,
                                I_default_wh        IN       STORE.DEFAULT_WH%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Name:    ALL_STORE_TYPES
-- Purpose: This function will check if the Store table contains both
--          Company and Franchise Stores.
-----------------------------------------------------------------------------------------------
                              
FUNCTION ALL_STORE_TYPES (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_fr_store        IN OUT   BOOLEAN,
                          O_company_store   IN OUT   BOOLEAN)
                              
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_CLOSED_STORE
-- Purpose: This function will check if the Store location being passed contains
--          close date less than the current vdate.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_CLOSED_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_closed          IN OUT   BOOLEAN,
                               I_store           IN       V_STORE.STORE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_IMP_EXP
-- Purpose: This function will check if the Importer/Exporter location being passed
--           is valid or not
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_IMP_EXP(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_valid           IN OUT BOOLEAN,
                          O_imp_exp_desc    IN OUT WH.WH_NAME%TYPE,
                          I_loc_type        IN     VARCHAR2,
                          I_value           IN     WH.WH%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    SA_STORE
-- Purpose: This function will check if the Store being passed is a
--          valid Sales Audit Store or not.
-----------------------------------------------------------------------------------------------
FUNCTION SA_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_sa_store        IN OUT   BOOLEAN,
                  O_store_name      IN OUT   STORE.STORE_NAME%TYPE,
                  O_store_type      IN OUT   STORE.STORE_TYPE%TYPE,
                  I_store           IN       STORE.STORE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_SUPPLIER
-- Purpose: This function will check if the user has visibility to the entered Supplier using
--          V_SUPS view .
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPPLIER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid           IN OUT   BOOLEAN,
                           O_sups_row        IN OUT   V_SUPS%ROWTYPE,
                           I_supplier        IN       V_SUPS.SUPPLIER%TYPE,
                           I_financial_ind   IN       VARCHAR2 DEFAULT 'Y')
return BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    SUPPLIER_EXTERNAL_ID
-- Purpose: This function called from supfind form will check if the user has
--          visibility to the entered Supplier using V_SUPS view .Inactive suppliers are
--          visible to the user if the financial_ind in system_options table is 'N'.
-----------------------------------------------------------------------------------------------
FUNCTION SUPPLIER_EXTERNAL_ID(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_valid           IN OUT BOOLEAN,
                              O_sups_row        IN OUT V_SUPS%ROWTYPE,
                              I_external_ref_id IN     SUPS.EXTERNAL_REF_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:    VALIDATE_SEC_MASTER_PO
-- Purpose: This function will check if the user has visibility to the entered master order
--          number according to the privileges of its location using v_sec_ordhead view.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_SEC_MASTER_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists          IN OUT   BOOLEAN,
                                I_master_po       IN       ORDHEAD.MASTER_PO_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END FILTER_LOV_VALIDATE_SQL;
/
