WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE LIKE_ITEM_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
FUNCTION LIKE_ITEM(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_supplier_ind            IN OUT   VARCHAR2,
                   O_price_ind               IN OUT   VARCHAR2,
                   O_store_ind               IN OUT   VARCHAR2,
                   O_wh_ind                  IN OUT   VARCHAR2,
                   O_repl_ind                IN OUT   VARCHAR2,
                   O_uda_ind                 IN OUT   VARCHAR2,
                   O_seasons_ind             IN OUT   VARCHAR2,
                   O_ticket_ind              IN OUT   VARCHAR2,
                   O_req_doc_ind             IN OUT   VARCHAR2,
                   O_hts_ind                 IN OUT   VARCHAR2,
                   O_tax_code_ind            IN OUT   VARCHAR2,
                   O_children_ind            IN OUT   VARCHAR2,

                   O_internal_finisher_ind   IN OUT   VARCHAR2,
                   O_external_finisher_ind   IN OUT   VARCHAR2,

                   I_item                    IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION LIKE_ITEM_INSERT(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_new_item            IN       ITEM_MASTER.ITEM%TYPE,
                          I_existing_item       IN       ITEM_MASTER.ITEM%TYPE,
                          I_item_desc           IN       ITEM_MASTER.ITEM_DESC%TYPE,
                          I_number_type         IN       ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                          I_format_id           IN       ITEM_MASTER.FORMAT_ID%TYPE,
                          I_prefix              IN       ITEM_MASTER.PREFIX%TYPE,
                          I_diff_1              IN       ITEM_MASTER.DIFF_1%TYPE,
                          I_diff_2              IN       ITEM_MASTER.DIFF_2%TYPE,
                          I_diff_3              IN       ITEM_MASTER.DIFF_3%TYPE,
                          I_diff_4              IN       ITEM_MASTER.DIFF_4%TYPE,
                          I_children_ind        IN       VARCHAR2,
                          I_supplier_ind        IN       VARCHAR2,
                          I_price_ind           IN       VARCHAR2,
                          I_store_ind           IN       VARCHAR2,
                          I_wh_ind              IN       VARCHAR2,
                          I_repl_ind            IN       VARCHAR2,
                          I_uda_ind             IN       VARCHAR2,
                          I_seasons_ind         IN       VARCHAR2,
                          I_ticket_ind          IN       VARCHAR2,
                          I_req_doc_ind         IN       VARCHAR2,
                          I_hts_ind             IN       VARCHAR2,
                          I_internal_finisher   IN       VARCHAR2,
                          I_external_finisher   IN       VARCHAR2)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION ITEM_MASTER_DETAIL_INSERT(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_new_item            IN       ITEM_MASTER.ITEM%TYPE,
                                   I_existing_item       IN       ITEM_MASTER.ITEM%TYPE,
                                   I_item_desc           IN       ITEM_MASTER.ITEM_DESC%TYPE,
                                   I_number_type         IN       ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE,
                                   I_children_ind        IN       VARCHAR2,
                                   I_supplier_ind        IN       VARCHAR2,
                                   I_price_ind           IN       VARCHAR2,
                                   I_store_ind           IN       VARCHAR2,
                                   I_wh_ind              IN       VARCHAR2,
                                   I_repl_ind            IN       VARCHAR2,
                                   I_uda_ind             IN       VARCHAR2,
                                   I_seasons_ind         IN       VARCHAR2,
                                   I_ticket_ind          IN       VARCHAR2,
                                   I_req_doc_ind         IN       VARCHAR2,
                                   I_hts_ind             IN       VARCHAR2,
                                   I_internal_finisher   IN       VARCHAR2,
                                   I_external_finisher   IN       VARCHAR2)

   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION CHECK_DUPL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_flag          IN OUT VARCHAR2,
                    I_item          IN     ITEM_MASTER.ITEM%TYPE,
                    I_diff_1        IN     ITEM_MASTER.DIFF_1%TYPE,
                    I_diff_2        IN     ITEM_MASTER.DIFF_2%TYPE,
                    I_diff_3        IN     ITEM_MASTER.DIFF_3%TYPE,
                    I_diff_4        IN     ITEM_MASTER.DIFF_4%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
END LIKE_ITEM_SQL;
/
