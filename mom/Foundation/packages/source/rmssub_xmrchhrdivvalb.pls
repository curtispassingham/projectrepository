
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRDIV_VALIDATE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will verify all required fields for the create and modify messages.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrDivDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will verify all required fields for the delete message.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrDivRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: DIVISION_EXISTS
   -- Purpose      : This function will check if the division number received is legitimate.
-------------------------------------------------------------------------------------------------------
FUNCTION DIVISION_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division        IN       DIVISION.DIVISION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_TOTAL_MARKET_AMT
   -- Purpose      : This function will check if the total market amount received is at least 1000, only
   --                if it is null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TOTAL_MARKET_AMT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_total_mkt_amt   IN       DIVISION.TOTAL_MARKET_AMT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert RIB division object into a division record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_division_rec    OUT    NOCOPY   DIVISION%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrDivDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert RIB division object into a division record.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_division_rec    OUT    NOCOPY   DIVISION%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrDivRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_division_rec    OUT    NOCOPY   DIVISION%ROWTYPE,
                       I_message         IN              "RIB_XMrchHrDivDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XMRCHHRDIV_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not CHECK_TOTAL_MARKET_AMT(O_error_message,
                                 I_message.total_market_amt) then
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XMRCHHRDIV.LP_mod_type then
      if not DIVISION_EXISTS(O_error_message,
                             I_message.division) then
         return FALSE;
      end if;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_division_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_division_rec    OUT    NOCOPY   DIVISION%ROWTYPE,
                       I_message         IN              "RIB_XMrchHrDivRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not DIVISION_EXISTS(O_error_message,
                          I_message.division) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_division_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrDivDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.division is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'division', NULL, NULL);
      return FALSE;
   end if;

   if I_message.div_name is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'div_name', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrDivRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.division is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'division', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION DIVISION_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division        IN       DIVISION.DIVISION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_VALIDATE.DIVISION_EXISTS';
   L_exists       BOOLEAN      := FALSE;

BEGIN

   if not MERCH_VALIDATE_SQL.DIVISION_EXIST(O_error_message,
                                            L_exists,
                                            I_division) then
      return FALSE;
   end if;

   if not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MERCH_DIV', I_division);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DIVISION_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TOTAL_MARKET_AMT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_total_mkt_amt   IN       DIVISION.TOTAL_MARKET_AMT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_VALIDATE.CHECK_TOTAL_MARKET_AMT';

BEGIN

   if NVL(I_total_mkt_amt, 1000) < 1000 then
      O_error_message := SQL_LIB.CREATE_MSG('INV_MARKET_AMT');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_TOTAL_MARKET_AMT;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_division_rec    OUT    NOCOPY   DIVISION%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrDivDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_VALIDATE.POPULATE_RECORD';

BEGIN

   O_division_rec.division         := I_message.division;
   O_division_rec.div_name         := I_message.div_name;
   O_division_rec.buyer            := I_message.buyer;
   O_division_rec.merch            := I_message.merch;
   O_division_rec.total_market_amt := I_message.total_market_amt;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_division_rec    OUT    NOCOPY   DIVISION%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrDivRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRDIV_VALIDATE.POPULATE_RECORD';

BEGIN

   O_division_rec.division := I_message.division;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRDIV_VALIDATE;
/
