CREATE OR REPLACE PACKAGE DATA_EXPORT_SQL AUTHID CURRENT_USER AS

/* Organizational Hierarchy and Stores */
COMP_ADD    CONSTANT  VARCHAR2(64) := 'compcre';
COMP_UPD    CONSTANT  VARCHAR2(64) := 'compmod';

CHAIN_ADD   CONSTANT  VARCHAR2(64) := 'chaincre';
CHAIN_UPD   CONSTANT  VARCHAR2(64) := 'chainmod';
CHAIN_DEL   CONSTANT  VARCHAR2(64) := 'chaindel';

AREA_ADD    CONSTANT  VARCHAR2(64) := 'areacre';
AREA_UPD    CONSTANT  VARCHAR2(64) := 'areamod';
AREA_DEL    CONSTANT  VARCHAR2(64) := 'areadel';

REG_ADD     CONSTANT  VARCHAR2(64) := 'regioncre';
REG_UPD     CONSTANT  VARCHAR2(64) := 'regionmod';
REG_DEL     CONSTANT  VARCHAR2(64) := 'regiondel';

DIST_ADD    CONSTANT  VARCHAR2(64) := 'districtcre';
DIST_UPD    CONSTANT  VARCHAR2(64) := 'districtmod';
DIST_DEL    CONSTANT  VARCHAR2(64) := 'districtdel';

ST_ADD      CONSTANT  VARCHAR2(64) := 'storecre';
ST_UPD      CONSTANT  VARCHAR2(64) := 'storemod';
ST_DEL      CONSTANT  VARCHAR2(64) := 'storedel';

STDTL_ADD   CONSTANT  VARCHAR2(64) := 'storedtlcre';
STDTL_UPD   CONSTANT  VARCHAR2(64) := 'storedtlmod';
STDTL_DEL   CONSTANT  VARCHAR2(64) := 'storedtldel';

WH_ADD      CONSTANT  VARCHAR2(64) := 'whcre';
WH_UPD      CONSTANT  VARCHAR2(64) := 'whmod';
WH_DEL      CONSTANT  VARCHAR2(64) := 'whdel';

/* Merchandise Hierarchy */
DIV_CRE        CONSTANT  VARCHAR2(64)  := 'divisioncre';
DIV_UPD        CONSTANT  VARCHAR2(64)  := 'divisionmod';
DIV_DEL        CONSTANT  VARCHAR2(64)  := 'divisiondel';

GRP_CRE        CONSTANT  VARCHAR2(64)  := 'groupcre';
GRP_UPD        CONSTANT  VARCHAR2(64)  := 'groupmod';
GRP_DEL        CONSTANT  VARCHAR2(64)  := 'groupdel';
               
DEPT_CRE       CONSTANT  VARCHAR2(64)  := 'deptcre';
DEPT_UPD       CONSTANT  VARCHAR2(64)  := 'deptmod';
DEPT_DEL       CONSTANT  VARCHAR2(64)  := 'deptdel';

CLASS_CRE      CONSTANT  VARCHAR2(64)  := 'classcre';
CLASS_UPD      CONSTANT  VARCHAR2(64)  := 'classmod';
CLASS_DEL      CONSTANT  VARCHAR2(64)  := 'classdel';
               
SUBCLASS_CRE   CONSTANT  VARCHAR2(64)  := 'subclasscre';
SUBCLASS_UPD   CONSTANT  VARCHAR2(64)  := 'subclassmod';
SUBCLASS_DEL   CONSTANT  VARCHAR2(64)  := 'subclassdel';

/* Differentiators */
DIFF_CRE       CONSTANT  VARCHAR2(64)  := 'diffcre';
DIFF_UPD       CONSTANT  VARCHAR2(64)  := 'diffmod';
DIFF_DEL       CONSTANT  VARCHAR2(64)  := 'diffdel';

/* Differentiator Groups */
DIFFGRPHDR_CRE       CONSTANT  VARCHAR2(64)  := 'diffgrphdrcre';
DIFFGRPHDR_UPD       CONSTANT  VARCHAR2(64)  := 'diffgrphdrmod';
DIFFGRPHDR_DEL       CONSTANT  VARCHAR2(64)  := 'diffgrphdrdel';

DIFFGRPDTL_CRE       CONSTANT  VARCHAR2(64)  := 'diffgrpdtlcre';
DIFFGRPDTL_UPD       CONSTANT  VARCHAR2(64)  := 'diffgrpdtlmod';
DIFFGRPDTL_DEL       CONSTANT  VARCHAR2(64)  := 'diffgrpdtldel';

/* Item */
ITEM_CRE             CONSTANT  VARCHAR2(64)  := 'itemhdrcre';
ITEM_UPD             CONSTANT  VARCHAR2(64)  := 'itemhdrmod';
ITEM_DEL             CONSTANT  VARCHAR2(64)  := 'itemhdrdel';
ITEM_UPC_CRE         CONSTANT  VARCHAR2(64)  := 'itemupccre'; 
ITEM_UPC_DEL        CONSTANT  VARCHAR2(64)   := 'itemupcdel'; 

/* Itemloc */
ITLOC_CRE         CONSTANT  VARCHAR2(64)  := 'itemloccre';
ITLOC_UPD         CONSTANT  VARCHAR2(64)  := 'itemlocmod';
ITLOC_DEL         CONSTANT  VARCHAR2(64)  := 'itemlocdel';

/* VatItem */
VATITM_CRE        CONSTANT  VARCHAR2(64)  := 'vatitemcre';
VATITM_UPD        CONSTANT  VARCHAR2(64)  := 'vatitemmod';
VATITM_DEL        CONSTANT  VARCHAR2(64)  := 'vatitemdel';

/* Vat */
VAT_CRE           CONSTANT  VARCHAR2(64)  := 'vatcre';
VAT_UPD           CONSTANT  VARCHAR2(64)  := 'vatmod';
VAT_DEL           CONSTANT  VARCHAR2(64)  := 'vatdel';

/* Related Item */
RELITMHDR_CRE     CONSTANT  VARCHAR2(64)  := 'relitemheadcre';
RELITMHDR_UPD     CONSTANT  VARCHAR2(64)  := 'relitemheadmod';
RELITMHDR_DEL     CONSTANT  VARCHAR2(64)  := 'relitemheaddel';

RELITMDTL_CRE     CONSTANT  VARCHAR2(64)  := 'relitemdetcre';
RELITMDTL_UPD     CONSTANT  VARCHAR2(64)  := 'relitemdetmod';
RELITMDTL_DEL     CONSTANT  VARCHAR2(64)  := 'relitemdetdel';
----------------------------------------------------------------------------------------------
-- Name:          INS_MERCHHIER_EXPORT_STG
-- Purpose:       Insert records in the MERCHHIER_EXPORT_STG table
--                which will be used for data extraction of merchandise hierarchy.
-----------------------------------------------------------------------------------------------
FUNCTION INS_MERCHHIER_EXPORT_STG(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_action_type    IN       MERCHHIER_EXPORT_STG.ACTION_TYPE%TYPE,
                                  I_division       IN       DIVISION.DIVISION%TYPE,
                                  I_group_no       IN       GROUPS.GROUP_NO%TYPE,
                                  I_dept           IN       DEPS.DEPT%TYPE,
                                  I_class_id       IN       CLASS.CLASS_ID%TYPE,
                                  I_subclass_id    IN       SUBCLASS.SUBCLASS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:          INS_ORGHIER_EXPORT_STG
-- Purpose:       Insert records in the ORGHIER_EXPORT_STG table
--                which will be used for data extraction of organizational hierarchy.
-----------------------------------------------------------------------------------------------
FUNCTION INS_ORGHIER_EXPORT_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_action_type    IN       ORGHIER_EXPORT_STG.ACTION_TYPE%TYPE,
                                 I_comp           IN       COMPHEAD.COMPANY%TYPE,
                                 I_chain          IN       CHAIN.CHAIN%TYPE,
                                 I_area           IN       AREA.AREA%TYPE,
                                 I_region         IN       REGION.REGION%TYPE,
                                 I_district       IN       DISTRICT.DISTRICT%TYPE,
                                 I_store          IN       STORE.STORE%TYPE,
                                 I_wh             IN       WH.WH%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:          INS_STORE_EXPORT_STG
-- Purpose:       Insert records in the STORE_EXPORT_STG table
--                which will be used for data extraction.
-----------------------------------------------------------------------------------------------
FUNCTION INS_STORE_EXPORT_STG(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_action_type        IN       STORE_EXPORT_STG.ACTION_TYPE%TYPE,
                              I_store              IN       STORE.STORE%TYPE,
                              I_addr_key           IN       ADDR.ADDR_KEY%TYPE,
                              I_addr_type          IN       ADDR.ADDR_TYPE%TYPE,
                              I_primary_addr_ind   IN       ADDR.PRIMARY_ADDR_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Name:          INS_DIFFS_EXPORT_STG 
-- Purpose:       Insert records in the DIFFS_EXPORT_STG table
--                which will be used for data extraction of differentiators.
-----------------------------------------------------------------------------------------------
FUNCTION INS_DIFFS_EXPORT_STG (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_action_type     IN       DIFFS_EXPORT_STG.ACTION_TYPE%TYPE,
                               I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:          INS_DIFFGRP_EXPORT_STG  
-- Purpose:       Insert records in the DIFFGRP_EXPORT_STG table
--                which will be used for data extraction of differentiator groups.
-----------------------------------------------------------------------------------------------
FUNCTION INS_DIFFGRP_EXPORT_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_action_type     IN       DIFFGRP_EXPORT_STG.ACTION_TYPE%TYPE,
                                I_diff_group_id   IN       DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                                I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:          INS_ITEM_EXPORT_STG   
-- Purpose:       Insert records in the ITEM_EXPORT_STG table
--                which will be used for data extraction of item/item-vat/item-store.
-----------------------------------------------------------------------------------------------
FUNCTION INS_ITEM_EXPORT_STG(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_action_type        IN       ITEM_EXPORT_STG.ACTION_TYPE%TYPE,
                             I_item               IN       ITEM_MASTER.ITEM%TYPE,
                             I_item_parent        IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                             I_item_grandparent   IN       ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                             I_item_level         IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                             I_tran_level         IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                             I_vat_region         IN       VAT_REGION.VAT_REGION%TYPE,
                             I_vat_code           IN       VAT_CODES.VAT_CODE%TYPE,
                             I_vat_type           IN       VAT_ITEM.VAT_TYPE%TYPE,
                             I_merchandise_ind    IN       ITEM_MASTER.MERCHANDISE_IND%TYPE,
                             I_vat_active_date    IN       VAT_ITEM.ACTIVE_DATE%TYPE,
                             I_loc                IN       ITEM_LOC.LOC%TYPE,
                             I_loc_type           IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:          INS_VAT_EXPORT_STG   
-- Purpose:       Insert records in the ITEM_EXPORT_STG table
--                which will be used for data extraction of item/item-vat/item-store.
-----------------------------------------------------------------------------------------------
FUNCTION INS_VAT_EXPORT_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_action_type     IN       VAT_EXPORT_STG.ACTION_TYPE%TYPE,
                            I_vat_region      IN       VAT_REGION.VAT_REGION%TYPE,
                            I_vat_code        IN       VAT_CODES.VAT_CODE%TYPE,
                            I_active_date     IN       VAT_CODE_RATES.ACTIVE_DATE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:          INS_RELITEM_EXPORT_STG   
-- Purpose:       Insert records in the RELITEM_EXPORT_STG table
--                which will be used for data extraction of related item.
-----------------------------------------------------------------------------------------------
FUNCTION INS_RELITEM_EXPORT_STG(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_action_type       IN       VAT_EXPORT_STG.ACTION_TYPE%TYPE,
                                I_relationship_id   IN       RELATED_ITEM_HEAD.RELATIONSHIP_ID%TYPE,
                                I_item              IN       RELATED_ITEM_HEAD.ITEM%TYPE,
                                I_related_item      IN       RELATED_ITEM_DETAIL.RELATED_ITEM%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Name:          PURGE_STG   
-- Purpose:       Purge all the export stage table.
-----------------------------------------------------------------------------------------------
FUNCTION PURGE_STG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END DATA_EXPORT_SQL;
/
