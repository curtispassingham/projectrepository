CREATE OR REPLACE PACKAGE BODY MERCH_ATTRIB_SQL AS

-----------------------------------------------------------------------

FUNCTION DIVISION_NAME( O_error_message   IN OUT   VARCHAR2,
            I_division     IN    NUMBER,
            O_division_name   IN OUT   VARCHAR2)

RETURN BOOLEAN IS

   cursor C_DIVISION is
   select div_name
     from v_division_tl
    where division = I_division;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_DIVISION', 'V_DIVISION_TL', to_char(I_division));
   open C_DIVISION;
   SQL_LIB.SET_MARK('FETCH' , 'C_DIVISION', 'V_DIVISION_TL', to_char(I_division));
   fetch C_DIVISION into O_division_name;
   if C_DIVISION%notfound then
      O_error_message := sql_lib.create_msg('ENTER_DIVISION',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_DIVISION', 'V_DIVISION_TL', to_char(I_division));
      close C_DIVISION;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_DIVISION', 'V_DIVISION_TL', to_char(I_division));
   close C_DIVISION;

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                        SQLERRM,
                        'MERCH_ATTRIB_SQL.DIVISION_NAME',
                        to_char(SQLCODE));
   RETURN FALSE;

END DIVISION_NAME;
-----------------------------------------------------------------------

FUNCTION GROUP_NAME( O_error_message   IN OUT   VARCHAR2,
            I_group     IN    NUMBER,
            O_group_name   IN OUT   VARCHAR2)

RETURN BOOLEAN IS

   cursor C_GROUP is
   select group_name
     from v_groups_tl
    where group_no = I_group;

BEGIN

   SQL_LIB.SET_MARK('OPEN' , 'C_GROUP', 'V_GROUP_TL', to_char(I_group));
   open C_GROUP;
   SQL_LIB.SET_MARK('FETCH' , 'C_GROUP', 'V_GROUP_TL', to_char(I_group));
   fetch C_GROUP into O_group_name;
   if C_GROUP%notfound then
      O_error_message := sql_lib.create_msg('INV_GROUP',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE' , 'C_GROUP', 'V_GROUP_TL', to_char(I_group));
      close C_GROUP;
      RETURN FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE' , 'C_GROUP', 'V_GROUP_TL', to_char(I_group));
   close C_GROUP;

   RETURN TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                        SQLERRM,
                        'MERCH_ATTRIB_SQL.GROUP_NAME',
                        to_char(SQLCODE));
   RETURN FALSE;

END GROUP_NAME;

-------------------------------------------------------------------

FUNCTION GET_GROUP_DIVISION(O_error_message IN OUT VARCHAR2,
                            O_division      IN OUT groups.division%TYPE,
                            I_group         IN     groups.group_no%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_GROUP_HIER is
      select division
        from groups
       where group_no = I_group;

BEGIN
   if I_group is NULL then
      O_error_message := sql_lib.create_msg('INVALID_PARM','I_group','NULL', 'NOT NULL');
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_GROUP_HIER',
                    'GROUPS',
                    'GROUP NUMBER:'||to_char(I_group));

   open C_GET_GROUP_HIER;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_GROUP_HIER',
                    'GROUPS',
                    'GROUP NUMBER:'||to_char(I_group));

   fetch C_GET_GROUP_HIER into O_division;

   if C_GET_GROUP_HIER%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_GROUP_HIER',
                       'GROUPS',
                       'GROUP NUMBER:'||to_char(I_group));
      close C_GET_GROUP_HIER;
      O_error_message := sql_lib.create_msg('INV_GROUP',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_GROUP_HIER',
                    'GROUPS',
                    'GROUP NUMBER:'||to_char(I_group));

   close C_GET_GROUP_HIER;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                             'MERCH_ATTRIB_SQL.GET_GROUP_DIVISION',
                    to_char(SQLCODE));

      return FALSE;

END GET_GROUP_DIVISION;
-------------------------------------------------------------------------------
FUNCTION GET_MERCH_HIER_NAMES(O_error_message    IN OUT      VARCHAR2,
                                 O_dept_name        IN OUT      DEPS.DEPT_NAME%TYPE,
                                 O_class_name       IN OUT      CLASS.CLASS_NAME%TYPE,
                                 O_subclass_name    IN OUT      SUBCLASS.SUB_NAME%TYPE,
                                 I_dept             IN          DEPS.DEPT%TYPE,
                                 I_class            IN          CLASS.CLASS%TYPE,
                                 I_subclass         IN          SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN IS

      L_program VARCHAR2(64)    := 'MERCH_ATTRIB_SQL.GET_MERCH_HIER_NAMES';

   BEGIN
      ---
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  I_dept,
                                  O_dept_name) = FALSE then
         RETURN FALSE;
      end if;
      ---
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   I_dept,
                                   I_class,
                                   O_class_name) = FALSE then
         RETURN FALSE;
      end if;
      ---
      if SUBCLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                      I_dept,
                                      I_class,
                                      I_subclass,
                                      O_subclass_name) = FALSE then
         RETURN FALSE;
      end if;
      ---
      RETURN TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                        SQLERRM,
                        L_program,
                        null);
     RETURN FALSE;

   END GET_MERCH_HIER_NAMES;
------------------------------------------------------------------------------
FUNCTION CHECK_SUBCLASS_EXISTS(O_error_message    IN OUT      VARCHAR2,
                               O_exists           IN OUT      BOOLEAN,
                               I_dept             IN          DEPS.DEPT%TYPE,
                               I_class            IN          CLASS.CLASS%TYPE DEFAULT NULL)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MERCH_ATTRIB_SQL.CHECK_SUBCLASS_EXISTS';
   L_dummy     VARCHAR2(1);

   cursor C_SUB_EXIST is
      select 'Y'
        from subclass
       where dept = I_dept
         and class = NVL(I_class, class)
         and rownum = 1;

BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_SUB_EXIST','MERCH_ATTRIB_SQL',NULL);
   open C_SUB_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_SUB_EXIST','MERCH_ATTRIB_SQL',NULL);
   fetch C_SUB_EXIST into L_dummy;
   SQL_LIB.SET_MARK('CLOSE','C_SUB_EXIST','MERCH_ATTRIB_SQL',NULL);
   close C_SUB_EXIST;
   ---
   if L_dummy = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
   RETURN FALSE;

END CHECK_SUBCLASS_EXISTS;
------------------------------------------------------------------------------
FUNCTION CHECK_SUBCLASS_ITEM_EXISTS(O_error_message    IN OUT      VARCHAR2,
                                    O_exists           IN OUT      BOOLEAN,
                                    I_dept             IN          DEPS.DEPT%TYPE,
                                    I_class            IN          CLASS.CLASS%TYPE,
                                    I_subclass         IN          SUBCLASS.SUBCLASS%TYPE)
RETURN BOOLEAN  IS

   L_program   VARCHAR2(64) := 'MERCH_ATTRIB_SQL.CHECK_SUBCLASS_ITEM_EXISTS';
   L_dummy     VARCHAR2(1);

   cursor C_ITEM_EXIST is
      select 'Y'
        from item_master
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass
         and rownum = 1;

BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_subclass',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_ITEM_EXIST;
   fetch C_ITEM_EXIST into L_dummy;
   close C_ITEM_EXIST;
   ---
   if L_dummy = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;

END CHECK_SUBCLASS_ITEM_EXISTS;
------------------------------------------------------------------------------
FUNCTION CHECK_CLASS_EXISTS(O_error_message    IN OUT      VARCHAR2,
                            O_exists           IN OUT      BOOLEAN,
                            I_dept             IN          DEPS.DEPT%TYPE)

RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'MERCH_ATTRIB_SQL.CHECK_CLASS_EXISTS';
   L_dummy     VARCHAR2(1);

   cursor C_CLASS_EXIST is
      select 'Y'
        from class
       where dept = I_dept
         and rownum = 1;

BEGIN
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_CLASS_EXIST;
   fetch C_CLASS_EXIST into L_dummy;
   close C_CLASS_EXIST;
   ---
   if L_dummy = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return FALSE;
END CHECK_CLASS_EXISTS;
-------------------------------------------------------------
FUNCTION GET_MERCH_HIER_LABEL(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_division        IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              O_group           IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              O_department      IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              O_class           IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                              O_subclass        IN OUT  CODE_DETAIL.CODE_DESC%TYPE)
RETURN NUMBER IS
   L_program   VARCHAR2(64) := 'MERCH_ATTRIB_SQL.GET_MERCH_HIER_LABEL';

BEGIN
   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','DIV',O_division) = FALSE then
      return 0;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','GROUP',O_group) = FALSE then
      return 0;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','DEP',O_department) = FALSE then
      return 0;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','CLS',O_class) = FALSE then
      return 0;
   end if;

   if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,'LABL','SCLS',O_subclass) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            null);
      return 0;
END GET_MERCH_HIER_LABEL;
-------------------------------------------------------------
END MERCH_ATTRIB_SQL;
/
