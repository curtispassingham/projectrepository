CREATE OR REPLACE PACKAGE CORESVC_COSTCHG AUTHID CURRENT_USER AS

   action_new   VARCHAR2(25)   := 'NEW';
   action_mod   VARCHAR2(25)   := 'MOD';
   action_del   VARCHAR2(25)   := 'DEL';

   TYPE CSH_REC_TAB IS TABLE OF COST_SUSP_SUP_HEAD%ROWTYPE;
   TYPE CSDL_REC_TAB IS TABLE OF SVC_COST_SUSP_SUP_DETAIL_LOC%ROWTYPE;
   TYPE CSD_REC_TAB IS TABLE OF SVC_COST_SUSP_SUP_DETAIL%ROWTYPE;

   FUNCTION PROCESS_CHUNK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN       NUMBER,
                          I_chunk_id        IN       NUMBER,
                          O_error_count     OUT      NUMBER)
       RETURN BOOLEAN;

   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_process_id      IN       NUMBER)
      RETURN BOOLEAN;
      
   FUNCTION COMPLETE_SVC_CSH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       NUMBER)
      RETURN BOOLEAN;

END CORESVC_COSTCHG;
/
