CREATE OR REPLACE PACKAGE TAX_SQL AUTHID CURRENT_USER AS

LP_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
LP_SVAT                 VARCHAR2(6)              := 'SVAT';
LP_GTAX                 VARCHAR2(6)              := 'GTAX';
LP_SALES                VARCHAR2(6)              := 'SALES';
LP_dept_merch           NUMBER(2)                := 4;

--call type constants
LP_CALL_TYPE_NIL         VARCHAR2(20)             := 'NIL';  -- new item loc
LP_CALL_TYPE_RPM_NIL     VARCHAR2(20)             := 'RPM_NIL';  -- RPM new item loc
LP_CALL_TYPE_INIT_RETAIL VARCHAR2(20)             := 'INIT_RETAIL';  -- initial retail
LP_CALL_TYPE_DEFAULT     VARCHAR2(20)             := 'DEFAULT';  -- default

--tax type constants
LP_TAX_TYPE_COST         VARCHAR2(20)             := 'C';
LP_TAX_TYPE_RETAIL       VARCHAR2(20)             := 'R';
LP_TAX_TYPE_BOTH         VARCHAR2(20)             := 'B';

--Add/remove constants
LP_ADD_TAX               VARCHAR2(1)             := 'A';
LP_REMOVE_TAX            VARCHAR2(1)             := 'R';

------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_REGION_DESC
-- Purpose: This function uses a TAX region number to return the description, but instead of passing in a single value,
--           a collection or ABO containing the TAX/VAT region will be used to return a set of description.
-- Required Input: Tax_Region
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_REGION_DESC(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_CODE_DESC
-- Purpose: - This function will be used to retrieve TAX_CODE description for a given collection of TAX_CODES.
-- Required Input: Tax_Code
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_CODE_DESC(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_CODE_DESC_WRP
-- Purpose: - This function will be used to retrieve TAX_CODE description for a given collection of TAX_CODES.
--            Returns integer inplace of boolean to make it easy to call from Java code.
-- Required Input: Tax_Code
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_CODE_DESC_WRP(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN integer;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_CODE
-- Purpose: This function will be used to retrieve Tax information for a given tax_code.
-- Required Input: Tax Code. If this function receives only an active date, It will retrieve all
--                 records from the vat_code_rates table with that date.
--                 If no active date is passed, it will retrieve the record with minimum active date.
----------------------------------------------------------------------------------------
FUNCTION GET_TAX_CODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_CODE_WRP
-- Purpose: This function will be used to retrieve Tax information for a given tax_code.
-- Required Input: Tax Code. If this function receives only an active date, It will retrieve all
--                 records from the vat_code_rates table with that date.
--                 If no active date is passed, it will retrieve the record with minimum active date.
--                 Returns Integer instead of boolean to make it easy to call from Java code.
----------------------------------------------------------------------------------------
FUNCTION GET_TAX_CODE_WRP(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN integer;
----------------------------------------------------------------------------------------
-- Function Name:  GET_ZERO_RATE_TAX_CODE
-- Purpose: Use a rate = 0 to return a tax code
----------------------------------------------------------------------------------------
FUNCTION GET_ZERO_RATE_TAX_CODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_ENTITY_TAX_REGION
-- Purpose: This function will be used to retrieve TAX_REGION information for a selected entity.
-- Required Input: Entity_id, Entity_type.
------------------------------------------------------------------------------------------------------
FUNCTION GET_ENTITY_TAX_REGION(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_INVC_TAX_REGION
-- Purpose: This function will return tax region for invoice matching.
------------------------------------------------------------------------------------------------------
FUNCTION GET_INVC_TAX_REGION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tax_region      IN OUT   VAT_REGION.VAT_REGION%TYPE,
                             I_invc_id         IN       INVC_HEAD.INVC_ID%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                             I_order_billtoloc IN       SHIPMENT.BILL_TO_LOC%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
-- Function Name: TAX_DEPT_REGION_EXISTS
-- Purpose: This function will be used to check if tax information for all regions
--          already exists for the given department.
-------------------------------------------------------------------------------------------
FUNCTION TAX_DEPT_REGION_EXISTS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: GET_TAX_INFO
-- Purpose: This function will be used to retrieve TAX item relationship.
-- Required Input: Item, Item_Parent or Dept. This will perform in different ways, depending on the
--                 following scenario of inputs.
--                 1. Dept is not null and Item and Item_parent is null - This will default all the
--                    tax information of the department into all items in that department.
--                 2. Dept and Item is not null and Item_parent is null - This will default tax
--                    information of the item to that of the departmet.
--                 3. Dept is null and Item and Item_parent is not null - This will default tax
--                    information of the first item (item) to that of second item (item_parent).
--                 4. Dept and Item is null and Item_parent is not null - This will default tax
--                    information of the child and grandchild items to that of
--                    its parent/grandparent (item_parent).
--                 5. Dept and Item_parent is null anf Item is not null - This will just retrieve
--                    tax_information of the item (item) from the vat_item table.
----------------------------------------------------------------------------------------
FUNCTION GET_TAX_INFO(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: ITEMLIST_TAX_EXPLODE
-- Purpose: This new function will explode the tax details of an item list to the attached items.
----------------------------------------------------------------------------------------
FUNCTION ITEMLIST_TAX_EXPLODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              O_run_report      IN OUT            BOOLEAN,
                              IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DELETE_TAX_ITEM
-- Purpose: - This function will be used to delete tax information from the vat_item table.
----------------------------------------------------------------------------------------
FUNCTION DELETE_TAX_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DELETE_TAX_DEPS
-- Purpose: If SVAT or SALES, this function will be used to delete tax information from the vat_deps table.
----------------------------------------------------------------------------------------
FUNCTION DELETE_TAX_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_UPDATE_TAX_SKU
-- Purpose: This function will be used insert and update records in the vat_item table.
----------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_TAX_SKU(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               O_run_report      IN OUT            BOOLEAN,
                               IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_UPDATE_TAX_DEPS
-- Purpose: This function will be used to insert and update records in the vat_deps table.
----------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_TAX_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DELETE_ITEM_DEPS_TAXRATE
-- Purpose: If SVAT or SALES, this function will be used to check if a vat code exists in the vat item table.
----------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_DEPS_TAXRATE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: DELETE_ITEM_DEPS_TAXCODE
-- Purpose: This function will be used to check if the vat_code will be used in the future.
------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_DEPS_TAXCODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function Name: POST_TAX_AMOUNT
-- Purpose: This fucntion will compute for the tax amount, which will be used for tran data posting.
------------------------------------------------------------------------------------------
FUNCTION POST_TAX_AMOUNT(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: CALC_COST_TAX
-- Purpose: This function will retrieve the purchase tax for each record in the collection.
-- In a SVAT and SALES scenario, it will query tax from the VAT tables. In a GTAX scenario,
-- it calls external tax engine to retrieve tax information.
------------------------------------------------------------------------------------------------------
FUNCTION CALC_COST_TAX(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_tax_calc_tbl       IN OUT   NOCOPY   OBJ_TAX_CALC_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: CALC_RETAIL_TAX
-- Purpose: This function will retrieve the retail sales tax for each record in the collection.
-- In a SVAT and SALES scenario, it will query tax from the VAT tables. In a GTAX scenario,
-- it either queries GTAX_ITEM_ROLLUP table or calls external tax engine depending on the call type.
------------------------------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_calc_tbl   IN OUT   NOCOPY   OBJ_TAX_CALC_TBL,
                         I_call_type       IN                VARCHAR2 DEFAULT LP_CALL_TYPE_DEFAULT)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_INCLUSIVE_RETAIL
-- Purpose: This function will return tax inclusive retail for an item/location/date.
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INCLUSIVE_RETAIL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_amount              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                  I_dept                IN       ITEM_MASTER.DEPT%TYPE,
                                  I_class               IN       ITEM_MASTER.CLASS%TYPE,
                                  I_location            IN       ITEM_LOC.LOC%TYPE,
                                  I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                                  I_effective_from_date IN       DATE,
                                  I_amount              IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT 1)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_EXCLUSIVE_RETAIL
-- Purpose: This function will return tax exclusive retail for an item/location/date.
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_EXCLUSIVE_RETAIL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_amount              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                  I_dept                IN       ITEM_MASTER.DEPT%TYPE,
                                  I_class               IN       ITEM_MASTER.CLASS%TYPE,
                                  I_location            IN       ITEM_LOC.LOC%TYPE,
                                  I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                                  I_effective_from_date IN       DATE,
                                  I_amount              IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT 1)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_INCLUSIVE_RETAIL
-- Purpose: This function will return tax inclusive retail for each record in the collection.
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INCLUSIVE_RETAIL(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_EXCLUSIVE_RETAIL
-- Purpose: This function will return tax exclusive retail for each record in the collection.
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_EXCLUSIVE_RETAIL(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_RATE_BY_CODE
-- Purpose: This function will return tax rates for tax codes in the collection.
-- Not supported for GTAX.
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_RATE_BY_CODE(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_tax_code_rate_tbl    IN OUT   NOCOPY   OBJ_TAX_CODE_RATE_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_TAX_RATE_BY_CODE_WRP
-- Purpose: This function will return tax rates for tax codes in the collection.
-- Not supported for GTAX.
-- Returns integer instead of boolean to make it easy to call from Java code.
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_RATE_BY_CODE_WRP(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_code_rate_tbl    IN OUT   NOCOPY   OBJ_TAX_CODE_RATE_TBL)
RETURN integer;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_DEFAULT_TAX_REGION_RATE
-- Purpose: This function will return cumulative tax rate for item's default tax region.
------------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_TAX_REGION_RATE(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_cum_tax_pct          IN OUT  VAT_ITEM.VAT_RATE%TYPE,
                                     I_item                 IN      VAT_ITEM.ITEM%TYPE,
                                     I_cost_retail_ind      IN      VAT_ITEM.VAT_TYPE%TYPE,
                                     I_effective_from_date  IN      VAT_ITEM.ACTIVE_DATE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: GET_RETAIL_TAX_IND
-- Purpose: This function will determine whether taxes needed to be added to Retail or not for GTAX
-- , SVAT and SALES tax.
------------------------------------------------------------------------------------------------------
FUNCTION GET_RETAIL_TAX_IND (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tax_ind         IN OUT   VARCHAR2,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: ADD_RETAIL_TAX
-- Purpose: This function will add taxes to Retail when Item is initially setup when taxes needed to be added.
--
------------------------------------------------------------------------------------------------------
FUNCTION ADD_RETAIL_TAX (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_amount              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_item                IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_ind            IN       ITEM_MASTER.PACK_IND%TYPE,
                         I_dept                IN       ITEM_MASTER.DEPT%TYPE,
                         I_class               IN       ITEM_MASTER.CLASS%TYPE,
                         I_location            IN       ITEM_LOC.LOC%TYPE,
                         I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_effective_from_date IN       DATE,
                         I_amount              IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_currency_code       IN       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                         I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT 1,
                         I_call_type           IN       VARCHAR2 DEFAULT LP_CALL_TYPE_NIL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: DEL_VAT_CODES_CFA_EXT
-- Purpose: This function will delete the entries in vat_code_cfa_ext tables for a corresponding delete
--          in the vat_codes table
-------------------------------------------------------------------------------------------------------

FUNCTION DEL_VAT_CODES_EXT (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_vat_code        IN      VAT_CODES.VAT_CODE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name : CHECK_DUP_VAT_CODE
-- Purpose       : This function checks whether the entered vat code is already present in the VAT_CODES table or not.
--
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_VAT_CODE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        IN OUT BOOLEAN,
                             I_vat_code      IN     VAT_CODES.VAT_CODE%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_TAX_INFO_EXIST
-- Purpose: This function checks whether the entered item has tax records.
-- Required Input: Item
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TAX_INFO_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists           OUT BOOLEAN,
                              I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_AND_INSERT_UPDATE_TAX_SKU
-- Purpose: This function get tax info by calling GET_TAX_INFO then call INSERT_UPDATE_TAX_SKU.
-- Required Input: Item, Merch_hier_level, Merch_hier_value, Cost_retail_ind,
--                 and Active_date
-------------------------------------------------------------------------------------------------------
FUNCTION GET_AND_INSERT_UPDATE_TAX_SKU(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                       I_merch_hier_level IN     NUMBER,
                                       I_merch_hier_value IN     NUMBER,
                                       I_cost_retail_ind  IN     VARCHAR2,
                                       I_active_date      IN     DATE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Name:    GET_TAX_RATE_INFO
-- Purpose: This function is used get the tax.
-------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_RATE_INFO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_tax_rate        IN OUT   VAT_CODE_RATES.VAT_RATE%TYPE,
                            O_tax_code        IN OUT   VAT_CODES.VAT_CODE%TYPE,
                            O_exempt_ind      IN OUT   VARCHAR2,
                            I_item            IN       ITEM_LOC_SOH.ITEM%TYPE,
                            I_from_loc_type   IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                            I_from_loc        IN       ITEM_LOC_SOH.LOC%TYPE,
                            I_to_loc_type     IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                            I_to_loc          IN       ITEM_LOC_SOH.LOC%TYPE,
                            I_cost_retail_ind IN       VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Name:    FETCH_VAT_APPL_TYPE
-- Purpose: This function is used to fetch vat_calc_type.
-------------------------------------------------------------------------------------------------------
FUNCTION FETCH_VAT_APPL_TYPE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_vat_region      IN       STORE.VAT_REGION%TYPE,
                              O_vat_appl_type   OUT      VAT_REGION.VAT_CALC_TYPE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END;
/