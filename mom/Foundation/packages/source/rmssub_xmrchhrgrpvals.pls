
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSSUB_XMRCHHRGRP_VALIDATE AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                       O_group_rec       OUT    NOCOPY    GROUPS%ROWTYPE,
                       I_message         IN               "RIB_XMrchHrGrpDesc_REC",
                       I_message_type    IN               VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                       O_group_rec       OUT    NOCOPY    GROUPS%ROWTYPE,
                       I_message         IN               "RIB_XMrchHrGrpRef_REC")
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XMRCHHRGRP_VALIDATE;
/
