
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ITEM_XFORM_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: NEXT_ITEM_XFORM_HEAD_ID
-- Purpose:       Supplies the next available transformation head sequence number.
-- Created:       09-FEB-04 Lorelie C. Crisostomo
---------------------------------------------------------------------------------------------
FUNCTION NEXT_ITEM_XFORM_HEAD_ID(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_item_xform_head_id   IN OUT   ITEM_XFORM_HEAD.ITEM_XFORM_HEAD_ID%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: NEXT_ITEM_XFORM_DETAIL_ID
-- Purpose:       Retrieves the next available transformation detail sequence number.
-- Created:       09-FEB-04 Lorelie C. Crisostomo
---------------------------------------------------------------------------------------------
FUNCTION NEXT_ITEM_XFORM_DETAIL_ID(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_item_xform_detail_id   IN OUT   ITEM_XFORM_DETAIL.ITEM_XFORM_DETAIL_ID%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CHECK_MULTI_PARENTS
-- Purpose:       Returns TRUE if an item has multiparents and FALSE if it does not.
-- Created:       11-FEB-04 Lorelie C. Crisostomo
---------------------------------------------------------------------------------------------
FUNCTION CHECK_MULTI_PARENTS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_multi_parent_ind   IN OUT   BOOLEAN,
                             I_item               IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_HEAD_ITEM
-- Purpose:       Retrieves the head item for a specific Item Transformation detail record.
-- Created:       17-FEB-04 Faye Guerzon
---------------------------------------------------------------------------------------------
FUNCTION GET_HEAD_ITEM(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_head_item              IN OUT   ITEM_XFORM_HEAD.HEAD_ITEM%TYPE,
                       I_item_xform_detail_id   IN       ITEM_XFORM_DETAIL.ITEM_XFORM_DETAIL_ID%TYPE)
   RETURN BOOLEAN;


---------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------

-- Function Name: FILTER_DETAIL_LIST
-- Purpose      : This function provides 'filter view' security to the item transformation 
--                forms (itemxform.fmb and itxformyld.fmb).
----------------------------------------------------------------------------------------------


FUNCTION FILTER_DETAIL_LIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_partial_ind     IN OUT   VARCHAR2,
                            I_item_xform_id   IN       ITEM_XFORM_DETAIL.ITEM_XFORM_HEAD_ID%TYPE,
                            I_item            IN       ITEM_XFORM_DETAIL.DETAIL_ITEM%TYPE,
                            I_filter_type     IN       VARCHAR2)
   RETURN BOOLEAN;  

----------------------------------------------------------------------------------------------

-- Function Name: CALCULATE_RETAIL
-- Purpose      : This function calculates the unit retail for an orderable only item, using 
--                retail prices of the associated sellable only items in the item 
--                transformation details for the orderable only item.
----------------------------------------------------------------------------------------------


FUNCTION CALCULATE_RETAIL (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_item            IN       ITEM_MASTER.ITEM%TYPE,
                           I_location        IN       ITEM_LOC.LOC%TYPE,
                           O_unit_retail     IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE)
   RETURN BOOLEAN;  
---------------------------------------------------------------------------------------------
-- Function Name: CALCULATE_COST
-- Purpose:       Obtains the unit cost for a sellable only item using the cost prices of 
--                the associated orderable only items..
-- Created:       24-FEB-04 Des McIntosh
---------------------------------------------------------------------------------------------
FUNCTION CALCULATE_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item                  IN OUT   ITEM_MASTER.ITEM%TYPE,
                        I_location              IN       ITEM_LOC.LOC%TYPE,
                        O_unit_cost             OUT      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- Function Name: GET_SUM_OF_YIELD_PERCENTAGES
-- Purpose      : Returns the sum of yield percentages from ITEM_XFORM_DETAIL for an input
--                Detail Item.
-- Created      : 26-FEB-04 Carl Hothersall
----------------------------------------------------------------------------------------------
FUNCTION GET_SUM_OF_YIELD_PERCENTAGES(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_sum_of_yield_percentages IN OUT ITEM_XFORM_DETAIL.YIELD_FROM_HEAD_ITEM_PCT%TYPE,
                                      I_detail_item              IN     ITEM_XFORM_DETAIL.DETAIL_ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: ITEM_XFORM_DETAIL_EXISTS
-- Purpose      : Checks if a row exists on ITEM_XFORM_DETAIL for the input Item Xform Head Id.
--              : and Detail Item.
-- Created      : 26-FEB-04 Carl Hothersall
----------------------------------------------------------------------------------------------
FUNCTION ITEM_XFORM_DETAIL_EXISTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_exists             IN OUT BOOLEAN,
                                  I_item_xform_head_id IN     ITEM_XFORM_DETAIL.ITEM_XFORM_HEAD_ID%TYPE,
                                  I_detail_item        IN     ITEM_XFORM_DETAIL.DETAIL_ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
-- Function Name: COUNT_XFORM_DETAIL_RECS
-- Purpose      : Returns a count of the number of records in the item xform detail table for a
--              : specific detail item. Called from the itemxform form.
-- Created      : 03-MAR-04 Cathal Crawford
----------------------------------------------------------------------------------------------
FUNCTION COUNT_XFORM_DETAIL_RECS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_count              IN OUT NUMBER,
                                 I_detail_item        IN     ITEM_XFORM_DETAIL.DETAIL_ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: SET_YIELD_PCT_NULL
-- Purpose      : Sets the yield percentage to null where the item xform detail is the last record
--              : after the second last has been deleted
-- Created      : 03-MAR-04 Cathal Crawford
----------------------------------------------------------------------------------------------
FUNCTION SET_YIELD_PCT_NULL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_detail_item        IN     ITEM_XFORM_DETAIL.DETAIL_ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: ITEM_XFORM_HEAD_EXISTS
-- Purpose      : Checks if a row exists on ITEM_XFORM_HEAD for the input Item.
-- Created      : 10-Mar-04 Rob McDonagh
----------------------------------------------------------------------------------------------
FUNCTION ITEM_XFORM_HEAD_EXISTS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists             IN OUT BOOLEAN,
                                I_head_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
-- Function Name: RTV_ORDERABLE_ITEM_INFO
-- Purpose      : Supplies a table of orderable only items that contain the input sellable items.
--                For each sellable item in the table, if any of the orderable items associated
--                with the sellable item are on the RTV, the orderable item(s)
--                on the RTV will be used. 
----------------------------------------------------------------------------------------------
FUNCTION RTV_ORDERABLE_ITEM_INFO(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_orderable_TBL   IN OUT NOCOPY BTS_ORDITEM_QTY_TBL,
                                 I_sell_item_tbl   IN            ITEM_TBL,
                                 I_sell_qty_tbl    IN            QTY_TBL,
                                 I_rtv_order_no    IN            RTV_HEAD.RTV_ORDER_NO%TYPE,
                                 I_ext_ref_no      IN            RTV_HEAD.EXT_REF_NO%TYPE,
                                 I_location        IN            ITEM_LOC.LOC%TYPE,
                                 I_inv_status_tbl  IN            INV_STATUS_TBL,
                                 I_reason_tbl      IN            RTV_SQL.REASON_TBL)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: ALLOC_ORDERABLE_ITEM_INFO
-- Purpose      : Supplies a table of orderable only items that contain the input sellable items.
--                For each sellable item in the table, if any of the orderable items associated
--                with the sellable item are on the allocation, the orderable item(s)
--                on the allocation will be used. 
----------------------------------------------------------------------------------------------
FUNCTION ALLOC_ORDERABLE_ITEM_INFO(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_orderable_TBL   IN OUT NOCOPY BTS_ORDITEM_QTY_TBL,
                                   I_sell_item_tbl   IN            ITEM_TBL,
                                   I_sell_qty_tbl    IN            QTY_TBL,
                                   I_alloc_no        IN            ALLOC_HEADER.ALLOC_NO%TYPE,
                                   I_to_loc          IN            ALLOC_DETAIL.TO_LOC%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: TSF_ORDERABLE_ITEM_INFO
-- Purpose      : Supplies a table of orderable only items that contain the input sellable items.
--                For each sellable item in the table, if any of the orderable items associated
--                with the sellable item are on the transfer, the orderable item(s)
--                on the transfer will be used. 
----------------------------------------------------------------------------------------------
FUNCTION TSF_ORDERABLE_ITEM_INFO(O_error_message   IN OUT        RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_orderable_TBL   IN OUT NOCOPY BTS_ORDITEM_QTY_TBL,
                                 I_sell_item_tbl   IN            ITEM_TBL,
                                 I_sell_qty_tbl    IN            QTY_TBL,
                                 I_inv_status_tbl  IN            INV_STATUS_TBL,
                                 I_tsf_no          IN            TSFDETAIL.TSF_NO%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: ORDERABLE_ITEM_INFO
-- Purpose:       Supplies a table of orderable only items that contain the sellable items
--                from the input item table and the computed quantities associated with
--                the orderable items based on the input quantity of the sellable items.
-- Created:       19-APR-2004
---------------------------------------------------------------------------------------------
FUNCTION ORDERABLE_ITEM_INFO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_orderable_TBL   OUT    BTS_ORDITEM_QTY_TBL,
                             I_sell_item_tbl   IN     ITEM_TBL,
                             I_sell_qty_tbl    IN     QTY_TBL)
RETURN BOOLEAN;

----------------------------------------------------------------------------------------------
-- Function Name: ITEM_XFORM_HEAD_DETAIL_COUNT
-- Purpose      : Gets detail item for a given head item and checks if the detail item has
--                multiparents.
-- Created      : 25-Jun-04
----------------------------------------------------------------------------------------------
FUNCTION XFORM_HEAD_DETAIL_COUNT(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_multi_parent_ind           IN OUT   BOOLEAN,
                                 O_sum_of_yield_percentages   IN OUT   ITEM_XFORM_DETAIL.YIELD_FROM_HEAD_ITEM_PCT%TYPE,
                                 I_head_item                  IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: DELETE_ITEM_XFORM_HEAD_TL
-- Purpose      : Deletes from ITEM_XFORM_HEAD_TL table.
-- Created      : 4-April-2016
----------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_XFORM_HEAD_TL(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item_xform_head_id          IN       ITEM_XFORM_HEAD_TL.ITEM_XFORM_HEAD_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
END ITEM_XFORM_SQL;
/
