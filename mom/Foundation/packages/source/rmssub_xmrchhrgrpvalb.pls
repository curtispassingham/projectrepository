
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRGRP_VALIDATE AS

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields in the incoming message.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrGrpDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields in the incoming message.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrGrpRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: GROUP_EXISTS
   -- Purpose      : This function will check if the group number received is legitimate.
-------------------------------------------------------------------------------------------------------
FUNCTION GROUP_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_no        IN       GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB group object into a group record defined in the
   --                MERCH_SQL package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_group_rec       OUT    NOCOPY   GROUPS%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrGrpDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB group object into a group record defined in the
   --                MERCH_SQL package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_group_rec       OUT    NOCOPY   GROUPS%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrGrpRef_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_group_rec       OUT    NOCOPY   GROUPS%ROWTYPE,
                       I_message         IN              "RIB_XMrchHrGrpDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XMRCHHRGRP_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XMRCHHRGRP.LP_mod_type then
      if not GROUP_EXISTS(O_error_message,
                          I_message.group_no) then
         return FALSE;
      end if;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_group_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_group_rec       OUT    NOCOPY   GROUPS%ROWTYPE,
                       I_message         IN              "RIB_XMrchHrGrpRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message) then
      return FALSE;
   end if;

   if not GROUP_EXISTS(O_error_message,
                       I_message.group_no) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_group_rec,
                          I_message) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrGrpDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN
   if I_message.group_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'group_no', NULL, NULL);
      return FALSE;
   end if;

   if I_message.group_name is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'group_name', NULL, NULL);
      return FALSE;
   end if;

   if I_message.division is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'division', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XMrchHrGrpRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.group_no is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'group_no', NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
-------------------------------------------------------------------------------------------------------
FUNCTION GROUP_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_no        IN       GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_VALIDATE.GROUP_EXISTS';
   L_exists       BOOLEAN      := FALSE;

BEGIN

   if not MERCH_VALIDATE_SQL.GROUP_EXIST(O_error_message,
                                         L_exists,
                                         I_group_no) then
      return FALSE;
   end if;

   if not L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('INV_GROUP');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GROUP_EXISTS;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_group_rec       OUT    NOCOPY   GROUPS%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrGrpDesc_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_VALIDATE.POPULATE_RECORD';

BEGIN

   O_group_rec.group_no   := I_message.group_no;
   O_group_rec.group_name := I_message.group_name;
   O_group_rec.buyer      := I_message.buyer;
   O_group_rec.merch      := I_message.merch;
   O_group_rec.division   := I_message.division;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_group_rec       OUT    NOCOPY   GROUPS%ROWTYPE,
                         I_message         IN              "RIB_XMrchHrGrpRef_REC")
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XMRCHHRGRP_VALIDATE.POPULATE_RECORD';

BEGIN

    O_group_rec.group_no := I_message.group_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRGRP_VALIDATE;
/
