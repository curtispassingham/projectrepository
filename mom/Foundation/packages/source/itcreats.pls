CREATE OR REPLACE PACKAGE ITEM_CREATE_SQL AUTHID CURRENT_USER AS

-------------------------------------------------------------------------------
-- Function: TRAN_CHILDREN
-- Purpose : This function should automatically create item records for items 
--           on the item temp table. 
-------------------------------------------------------------------------------
FUNCTION TRAN_CHILDREN(O_error_message          IN OUT VARCHAR2,
                       I_item_master_insert     IN     VARCHAR2,
                       I_auto_approve_child_ind IN     VARCHAR2,
                       I_svc_izp_exists         IN     VARCHAR2 DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: INSERT_ITEM_MASTER
-- Purpose : This function will insert records into the item master table for 
--           items on the item temp table.
-------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_MASTER(O_error_message          IN OUT VARCHAR2,
                            I_auto_approve_child_ind IN     VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: INSERT_ITEM_SUPPLIER
-- Purpose : This function will insert records into the item supplier table for 
--           the selected item.
-------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPPLIER(O_error_message IN OUT VARCHAR2,
                              I_item          IN     ITEM_MASTER.ITEM%TYPE,
                              I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: SUB_TRAN_INSERT_ITEM_SUPPLIER
-- Purpose:  This function will insert the item_temp items into item_supplier for the
--           item parent that's passed in, using the parent's item_supplier attribs.
------------------------------------------------------------------------------
FUNCTION SUB_TRAN_INSERT_ITEM_SUPPLIER(O_error_message IN OUT VARCHAR2,
                                       I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: SUB_TRAN_INSERT_ITEM_COUNTRY
-- Purpose:  This function will insert the item_temp items into item_country for the
--           item parent that's passed in, using the parent's item_country attribs.
------------------------------------------------------------------------------
FUNCTION SUB_TRAN_INSERT_ITEM_COUNTRY(O_error_message IN OUT VARCHAR2,
                                      I_item_parent   IN     ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: INSERT_ITEM_MASTER
-- Purpose:  This function does a simple one row insert into item_master.
--------------------------------------------------------------------------
FUNCTION INSERT_ITEM_MASTER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec      IN     ITEM_MASTER%ROWTYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function: MASS_UPDATE_CHILDREN
-- Purpose:  This function manages the filtering functionality for the itemchildrendiff form.
--           This function will update the ITEM_MASTER, ITEM_SUPPLER, ITEM_SUPP_COUNTRY 
--           and ITEM_LOCATION tables for the item and attributes passed in.
--------------------------------------------------------------------------

FUNCTION MASS_UPDATE_CHILDREN(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_supplier          IN      SUPS.SUPPLIER%TYPE,
                              I_item_parent       IN      ITEM_MASTER.ITEM_PARENT%TYPE,
                              I_rpm_zone_group_id IN      PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE,
                              I_rpm_zone_id       IN      PM_RETAIL_API_SQL.ZONE_ID%TYPE,
                              I_rpm_currency_code IN      CURRENCIES.CURRENCY_CODE%TYPE,
                              I_diff_group_id     IN      DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                              I_diff_id           IN      DIFF_GROUP_DETAIL.DIFF_ID%TYPE,
                              I_supp_diff_id      IN      DIFF_GROUP_DETAIL.DIFF_ID%TYPE,
                              I_unit_retail       IN      ITEM_LOC.UNIT_RETAIL%TYPE,
                              I_unit_cost         IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              I_generate_ean      IN      BOOLEAN,
                              I_auto_approve      IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function: MASS_UPDATE_CHILDREN
-- Purpose:  This function manages the filering functionality for the itemchildrendiff form.
--           This overload for the function will update the VPN column of the item_supplier
--           table.
--------------------------------------------------------------------------
FUNCTION MASS_UPDATE_CHILDREN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_supplier      IN     SUPS.SUPPLIER%TYPE,
                              I_item          IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                              I_vpn           IN     ITEM_SUPPLIER.VPN%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------

-- Function: TRAN_CHILDREN_LEVEL3
-- Purpose:  This function inserts into the item_temp table to reserve the
--           a record for new level 3 items.
--------------------------------------------------------------------------
FUNCTION TRAN_CHILDREN_LEVEL3 ( O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                 IN      ITEM_TEMP.ITEM%TYPE,
                                I_item_number_type     IN      ITEM_TEMP.ITEM_NUMBER_TYPE%TYPE,
                                I_item_level           IN      ITEM_TEMP.ITEM_LEVEL%TYPE,
                                I_item_desc            IN      ITEM_TEMP.ITEM_DESC%TYPE,
                                I_diff_1               IN      ITEM_TEMP.DIFF_1%TYPE,
                                I_diff_2               IN      ITEM_TEMP.DIFF_2%TYPE,
                                I_diff_3               IN      ITEM_TEMP.DIFF_3%TYPE,
                                I_diff_4               IN      ITEM_TEMP.DIFF_4%TYPE,
                                I_existing_item_parent IN      ITEM_TEMP.EXISTING_ITEM_PARENT%TYPE, 
                                I_auto_approve         IN      VARCHAR2, 
                                I_status               IN      ITEM_MASTER.STATUS%TYPE
                                )
RETURN BOOLEAN;

------------------------------------------------------------------------------
-- Function: INSERT_ITEM_COUNTRY
-- Purpose : This function will insert records into the item country and item country extension tables for 
--           the selected item.
-------------------------------------------------------------------------------
FUNCTION INSERT_ITEM_COUNTRY(O_error_message   IN OUT   VARCHAR2,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
END ITEM_CREATE_SQL;
/