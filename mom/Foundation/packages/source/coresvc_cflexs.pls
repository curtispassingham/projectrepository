CREATE OR REPLACE PACKAGE CORESVC_CFLEX AUTHID CURRENT_USER
AS
  --------------------------------------------------------------------------------
  GV_group_set_id_1 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_2 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_3 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_4 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_5 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_6 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_7 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_8 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_9 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_group_set_id_10 CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
  GV_base_table CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE;
  GV_key_name_1 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_1 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_2 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_2 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_3 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_3 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_4 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_4 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_5 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_5 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_6 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_6 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_7 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_7 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_8 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_8 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_9 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_9 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_10 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_10 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_11 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_11 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_12 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_12 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_13 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_13 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_14 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_14 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_15 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_15 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_16 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_16 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_17 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_17 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_18 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_18 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_19 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_19 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  GV_key_name_20 CFA_SQL.GP_FIELD_ITEM%TYPE DEFAULT NULL;
  GV_key_value_20 CFA_SQL.GP_FIELD_VALUE%TYPE DEFAULT NULL;
  --------------------------------------------------------------------------------
Type typ_err_rec
IS
  record
  (
    rid ROWID ,
    view_name VARCHAR2(255),
    attrib    VARCHAR2(255),
    row_seq   NUMBER,
    err_msg RTK_ERRORS.RTK_TEXT%TYPE );
Type typ_err_tab
IS
  TABLE OF typ_err_rec;
  --------------------------------------------------------------------------------
  FUNCTION add_item_cflex_sheets(
      O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file              IN NUMBER,
      I_process_id        IN NUMBER,
      I_template_key      IN VARCHAR2,
      I_template_only_ind IN CHAR DEFAULT 'N')
    RETURN BOOLEAN;
    --------------------------------------------------------------------------------
  FUNCTION add_cflex_sheets(
      O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file              IN NUMBER,
      I_template_key      IN VARCHAR2,
      I_template_only_ind IN CHAR DEFAULT 'N')
    RETURN BOOLEAN;
  -------------------------------------------------------------------------------- 
  FUNCTION add_order_cflex_sheets(
      O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file              IN NUMBER,
      I_process_id        IN NUMBER,
      I_template_key      IN VARCHAR2,
      I_template_only_ind IN CHAR DEFAULT 'N')
    RETURN BOOLEAN;
    --------------------------------------------------------------------------------
   FUNCTION add_cost_chng_cflex_sheets(
      O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file              IN NUMBER,
      I_process_id        IN NUMBER,
      I_template_key      IN VARCHAR2,
      I_template_only_ind IN CHAR DEFAULT 'N')
    RETURN BOOLEAN;
    --------------------------------------------------------------------------------    
  FUNCTION process_s9t_sheets(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file          IN NUMBER,
      I_process_id    IN NUMBER,
      I_template_key  IN VARCHAR2 )
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION process_s9t_sheets_adminapi(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file          IN NUMBER,
      I_process_id    IN NUMBER,
      I_template_key  IN VARCHAR2 )
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION process_s9t_cfa(
      O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file                IN NUMBER,
      I_process_id          IN NUMBER,
      I_sheet_name          IN VARCHAR2,
      I_group_set_view_name IN VARCHAR2)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION process_s9t_cfa_adminapi(
      O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file                IN NUMBER,
      I_process_id          IN NUMBER,
      I_sheet_name          IN VARCHAR2,
      I_group_set_view_name IN VARCHAR2)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION process_cfa(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      O_err_tab OUT typ_err_tab,
      I_process_id IN NUMBER,
      I_chunk_id   IN NUMBER)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION item_svc2s9t(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file          IN NUMBER,
      I_process_id    IN NUMBER,
      I_template_key  IN VARCHAR2)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION order_svc2s9t(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file          IN NUMBER,
      I_process_id    IN NUMBER,
      I_template_key  IN VARCHAR2)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
    FUNCTION cost_change_svc2s9t(
      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
      I_file          IN NUMBER,
      I_process_id    IN NUMBER,
      I_template_key  IN VARCHAR2)
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  FUNCTION is_valid_view(
      I_view_name IN VARCHAR2)
    RETURN CHAR;
  --------------------------------------------------------------------------------
  FUNCTION is_new_record(
      I_view_name IN VARCHAR2,
      I_keys      IN key_val_pairs)
    RETURN CHAR;
  --------------------------------------------------------------------------------
  FUNCTION get_tmpl_default(
      I_template_key IN VARCHAR2,
      I_wksht_key    IN VARCHAR2,
      I_col_key       IN VARCHAR2)
    RETURN VARCHAR2;
  --------------------------------------------------------------------------------
END coresvc_cflex;
/