
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE MCITEM_CHRG_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Filename: mcitmchs/b.pls
-- Purpose : This package will insert or delete item up charge records in 
--           ITEM_CHRG_HEAD, ITEM_CHRG_DETAIL, MC_CHRG_HEAD and MC_CHRG_DETAIL
--           tables
-- Author:  Janeth Ocfemia (Accenture - ERC)
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Function Name: EXPLODE_ITEM_LIST_AND_SAVE
-- Purpose:       Function will insert or delete item up charge records
--                into ITEM_CHRG_HEAD and ITEM_CHRG_DETAIL from MC_CHRG_HEAD
--                and MC_CHRG_DETAIL tables. Then delete records in MC_CHRG_HEAD
--                and MC_CHRG_DETAIL after inserts have been executed
-------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEM_LIST_AND_SAVE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_count           IN OUT   VARCHAR2,
                                    I_item_list       IN       MC_CHRG_HEAD.ITEM_LIST%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
END;
/
