CREATE OR REPLACE PACKAGE WH_ATTRIB_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------
--- Record declaration for fully overloaded GET_WH_INFO function
--------------------------------------------------------------------
TYPE wh_rectype IS RECORD
   (wh_name            WH.WH_NAME%TYPE,
    wh_add1            ADDR.ADD_1%TYPE,
    wh_add2            ADDR.ADD_2%TYPE,
    wh_city            ADDR.CITY%TYPE,
    county             ADDR.COUNTY%TYPE,
    state              ADDR.STATE%TYPE,
    country_id         ADDR.COUNTRY_ID%TYPE,
    wh_pcode           ADDR.POST%TYPE,
    email              WH.EMAIL%TYPE,
    vat_region         WH.VAT_REGION%TYPE,
    org_hier_type      WH.ORG_HIER_TYPE%TYPE,
    org_hier_value     WH.ORG_HIER_VALUE%TYPE,
    currency_code      WH.CURRENCY_CODE%TYPE,
    physical_wh        WH.PHYSICAL_WH%TYPE,
    channel_id         WH.CHANNEL_ID%TYPE,
    stockholding_ind   WH.STOCKHOLDING_IND%TYPE,
    break_pack_ind     WH.BREAK_PACK_IND%TYPE,
    edi_addr_chg       ADDR.EDI_ADDR_CHG%TYPE,
    redist_wh_ind      WH.REDIST_WH_IND%TYPE,
    delivery_policy    WH.DELIVERY_POLICY%TYPE,
    protected_ind      WH.PROTECTED_IND%TYPE,
    forecast_wh_ind    WH.FORECAST_WH_IND%TYPE,
    rounding_seq       WH.ROUNDING_SEQ%TYPE,
    repl_ind           WH.REPL_IND%TYPE,
    repl_wh_link       WH.REPL_WH_LINK%TYPE,
    repl_src_ord       WH.REPL_SRC_ORD%TYPE,
    ib_ind             WH.IB_IND%TYPE,
    ib_wh_link         WH.IB_WH_LINK%TYPE,
    auto_ib_clear      WH.AUTO_IB_CLEAR%TYPE,
    duns_number        WH.DUNS_NUMBER%TYPE,
    duns_loc           WH.DUNS_LOC%TYPE);

--------------------------------------------------------------------------------
--- Function Name:  GET_NAME
--- Purpose:        Fetches the name of the WH from WH table.
--------------------------------------------------------------------------------
FUNCTION GET_NAME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_wh              IN       NUMBER,
                  O_wh_name         IN OUT   VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: GET_CURRENCY_CODE
-- Purpose      : This function will be used to determine the currency
--                for the given warehouse
--------------------------------------------------------------------------------
FUNCTION GET_CURRENCY_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_currency_code   IN OUT   WH.CURRENCY_CODE%TYPE,
                           I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: GET_PWH_NAME
-- Purpose      : This will be used to en validating a physical wareshouse
--                number and getting the warehouse name for that warehouse
--------------------------------------------------------------------------------
FUNCTION GET_PWH_NAME(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_wh              IN       WH.WH%TYPE,
                      O_wh_name         IN OUT   WH.WH_NAME%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: CHECK_PWH
-- Purpose      : This function will check to insure the selected warehouse
--                is a physical warehouse
--------------------------------------------------------------------------------
FUNCTION CHECK_PWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_flag            IN OUT   BOOLEAN,
                   I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: CHECK_VWH
-- Purpose      : This function will check to insure the selected warehouse
--                is a virtual warehouse
--------------------------------------------------------------------------------
FUNCTION CHECK_VWH(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_flag                IN OUT   BOOLEAN,
                   I_wh                  IN       WH.WH%TYPE,
                   I_exclude_finishers   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: WH_EXIST
-- Purpose      : This function will check if the selected warehouse
--                number already exists on the  table
--------------------------------------------------------------------------------
FUNCTION WH_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exist           IN OUT   BOOLEAN,
                  I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: VWH_EXIST
-- Purpose      : This function will check if the selected warehouse
--                number already exists on the  table
--------------------------------------------------------------------------------
FUNCTION VWH_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_exist           IN OUT   BOOLEAN,
                   I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: GET_WH_INFO
-- Purpose      : This will be used to get the address information
--                for the physical wareshouse
--------------------------------------------------------------------------------
FUNCTION GET_WH_INFO(
                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_wh_add1           IN OUT   ADDR.ADD_1%TYPE,
                     O_wh_add2           IN OUT   ADDR.ADD_2%TYPE,
                     O_wh_city           IN OUT   ADDR.CITY%TYPE,
                     O_state             IN OUT   ADDR.STATE%TYPE,
                     O_country_id        IN OUT   ADDR.COUNTRY_ID%TYPE,
                     O_wh_pcode          IN OUT   ADDR.POST%TYPE,
                     O_vat_region        IN OUT   WH.VAT_REGION%TYPE,
                     O_org_hier_type     IN OUT   WH.ORG_HIER_TYPE%TYPE,
                     O_org_hier_value    IN OUT   WH.ORG_HIER_VALUE%TYPE,
                     O_currency_code     IN OUT   WH.CURRENCY_CODE%TYPE,
                     O_break_pack_ind    IN OUT   WH.BREAK_PACK_IND%TYPE,
                     O_redist_wh_ind     IN OUT   WH.REDIST_WH_IND%TYPE,
                     O_delivery_policy   IN OUT   WH.DELIVERY_POLICY%TYPE,
                     O_email             IN OUT   WH.EMAIL%TYPE,
                     O_county            IN OUT   ADDR.COUNTY%TYPE,
                     O_duns_number       IN OUT   WH.DUNS_NUMBER%TYPE,
                     O_duns_loc          IN OUT   WH.DUNS_LOC%TYPE,
                     I_wh                IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: CANCEL_WH
-- Purpose      : This function will cancel out of warehouse
--------------------------------------------------------------------------------
FUNCTION CANCEL_WH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: GET_PWH_FOR_VWH
-- Purpose      : This function will return physical warehouse
--------------------------------------------------------------------------------
FUNCTION GET_PWH_FOR_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_pwh             IN OUT   WH.PHYSICAL_WH%TYPE,
                         I_vwh             IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: GET_WH_INFO
-- Purpose      : This function is the fully overloaded GET_WH_INFO function.
--                It retrieves warehouse information.
--------------------------------------------------------------------------------
FUNCTION GET_WH_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_wh_rec          IN OUT   WH_RECTYPE,
                     I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: GET_WH_MULTI_INFO
-- Purpose      : This function retrieves multichannel warehouse info.
--------------------------------------------------------------------------------
FUNCTION GET_WH_MULTI_INFO(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_physical_wh        IN OUT   WH.PHYSICAL_WH%TYPE,
                           O_channel_id         IN OUT   WH.CHANNEL_ID%TYPE,
                           O_stockholding_ind   IN OUT   WH.STOCKHOLDING_IND%TYPE,
                           O_protected_ind      IN OUT   WH.PROTECTED_IND%TYPE,
                           O_forecast_wh_ind    IN OUT   WH.FORECAST_WH_IND%TYPE,
                           O_rounding_seq       IN OUT   WH.ROUNDING_SEQ%TYPE,
                           O_repl_ind           IN OUT   WH.REPL_IND%TYPE,
                           O_repl_wh_link       IN OUT   WH.REPL_WH_LINK%TYPE,
                           O_repl_src_ord       IN OUT   WH.REPL_SRC_ORD%TYPE,
                           O_ib_ind             IN OUT   WH.IB_IND%TYPE,
                           O_ib_wh_link         IN OUT   WH.IB_WH_LINK%TYPE,
                           O_auto_ib_clear      IN OUT   WH.AUTO_IB_CLEAR%TYPE,
                           I_wh                 IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name: VWH_EXISTS_IN_PWH
-- Purpose      : This function will validate whether or not a given
--                virtual wh exists within the given physical warehouse .
--------------------------------------------------------------------------------
FUNCTION VWH_EXISTS_IN_PWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_vwh             IN       WH.WH%TYPE,
                           I_physical_wh     IN       WH.PHYSICAL_WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--- Function Name:  CHECK_REPL_EXISTS
--- Purpose:        Fetches the name of the WH from WH table.
---
--------------------------------------------------------------------------------
FUNCTION CHECK_REPL_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--- Function Name:  UPDATE_ALL_WH_LINKS
--- Purpose:        Update warehouse and investment buy links on WH table.
--------------------------------------------------------------------------------
FUNCTION UPDATE_ALL_WH_LINKS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_repl_wh_link    IN       WH.REPL_WH_LINK%TYPE,
                             I_ib_wh_link      IN       WH.REPL_WH_LINK%TYPE,
                             I_repl_ind        IN       WH.REPL_IND%TYPE,
                             I_ib_ind          IN       WH.IB_IND%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--    Name: VWH_EXISTS_IN_PWH
-- Purpose: This function will validate whether or not a given
--          virtual wh exists within the given physical warehouse,
--          and retrieve the name if it does exist.  This is
--          an overloaded function created so 2 calls won't have to
--          be made (1 to validate and 1 to get the name if valid).
--------------------------------------------------------------------------------
FUNCTION VWH_EXISTS_IN_PWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists          IN OUT   BOOLEAN,
                           O_wh_name         IN OUT   WH.WH_NAME%TYPE,
                           I_vwh             IN       WH.WH%TYPE,
                           I_physical_wh     IN       WH.PHYSICAL_WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--    Name: DELETE_PWH
-- Purpose: This function will insert records on daily purge for each vwh in
--          addition to the pwh.  The records will then be deleted in this order:
--             1) all non-primary vwh
--             2) primary_vwh
--             3) physical_wh
--          If any of the vwh has associations (e.g., item_loc records), they will
--          fail in VALIDATE_RECORDS_SQL.DEL_WH, and the pwh will fail because
--          it has vwh remaining.
--------------------------------------------------------------------------------
FUNCTION DELETE_PWH(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_physical_wh        IN       WH.PHYSICAL_WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--    Name: DELETE_VWH
-- Purpose: This function will write a record to daily_purge for the
--          virtual warehouse passed in.  If it is a primary_vwh, it will
--          set the delete order = 2, otherwise delete_order = 1.
--------------------------------------------------------------------------------
FUNCTION DELETE_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists          IN OUT   BOOLEAN,
                    I_vwh             IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--    Name: WH_VALID_FOR_CURRENCY
-- Purpose: Checks to see that a given wh/currency combination exists.  If so,
--          returns O_wh_is_valid = TRUE and the wh name.
--------------------------------------------------------------------------------
FUNCTION WH_VALID_FOR_CURRENCY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_valid           IN OUT   BOOLEAN,
                               O_wh_name         IN OUT   WH.WH_NAME%TYPE,
                               I_currency_code   IN       WH.CURRENCY_CODE%TYPE,
                               I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--    Name: CHECK_FINISHER
-- Purpose: Checks to see if the virtual warehouse entered is a finisher.
--          If so, O_flag = TRUE and the Virtual wh name is returned.
--------------------------------------------------------------------------------
FUNCTION CHECK_FINISHER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_flag            IN OUT   BOOLEAN,
                        O_vwh_name        IN OUT   WH.WH_NAME%TYPE,
                        I_vwh             IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--    Name: GET_ROW
-- Purpose: Checks for wh existence and information retrieval.
--------------------------------------------------------------------------------
FUNCTION GET_ROW (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exists          IN OUT   BOOLEAN,
                  O_wh_row          IN OUT   WH%ROWTYPE,
                  I_wh              IN       WH.WH%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--    Name: CHECK_VWHS
-- Purpose: Checks if the whs and vwhs in the populated TBL are valid.
--------------------------------------------------------------------------------
FUNCTION CHECK_VWHS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists             OUT   BOOLEAN,
                    I_wh_tbl          IN       LOC_TBL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
--    Name: CHECK_PWHS
-- Purpose: Checks if the pwhs in the populated TBL are valid.
--------------------------------------------------------------------------------
FUNCTION CHECK_PWHS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists             OUT   BOOLEAN,
                    I_wh_tbl          IN       LOC_TBL)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_WH_CURRENCY
-- Purpose      : This function will check if all warehouses have the same
--                currecncy code as specified. I_whs contains a distinct
--                set of warehouses.
--------------------------------------------------------------------------------
FUNCTION CHECK_WH_CURRENCY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_whs             IN       LOC_TBL,
                           I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_WH_COUNTRY
-- Purpose      : This function will check if all warehosues have the same
--                country id as specified. I_whs contains a distinct set of
--                warehouses.
--------------------------------------------------------------------------------
FUNCTION CHECK_WH_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_whs             IN       LOC_TBL,
                          I_country_id      IN       COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_ENT_ON_ORDER
-- Purpose      :This funciton will check if there is an order assigned to default 
--               importer/exporter location I_wh.
--------------------------------------------------------------------------------
FUNCTION CHECK_ENT_ON_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   VARCHAR2,
                            I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_ENT_ON_SHIPMENT
-- Purpose      :This funciton will check if there is a shipment assigned to default 
--               importer/exporter location I_wh.
--------------------------------------------------------------------------------
FUNCTION CHECK_ENT_ON_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   VARCHAR2,
                               I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_ENT_ON_TSF
-- Purpose      :This funciton will check if there is a intercompany book transfer 
--               assigned to default importer/exporter location I_wh.
--------------------------------------------------------------------------------
FUNCTION CHECK_ENT_ON_TSF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   VARCHAR2,
                          I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CHECK_ENT_ON_INV
-- Purpose      :This funciton will check if there is an invoice record assigned 
--               to default importer/exporter location I_wh.
--------------------------------------------------------------------------------
FUNCTION CHECK_ENT_ON_INV(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT   VARCHAR2,
                          I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--- Function Name:  CHECK_SUPS_IMP_EXP
--- Purpose      :  This function will check if an importer/exporter location is
---                 attached to a supplier site.
--------------------------------------------------------------------------------
FUNCTION CHECK_SUPS_IMP_EXP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   VARCHAR2,
                            I_wh              IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
--- Function Name:  DEL_WH_ATTRIB
--- Purpose      :  This function will delete from the WH_L10N_EXT table
---                 for the given warehouse.
--------------------------------------------------------------------------------
FUNCTION DEL_WH_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_wh              IN       WH.WH%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION LOCK_WH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_wh_id           IN       WH.WH%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
END WH_ATTRIB_SQL;
/
