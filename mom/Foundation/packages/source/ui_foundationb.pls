CREATE OR REPLACE PACKAGE Body FOUNDATION_SQL IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : poscoup (Form Module)
  -- Source Object            : PROCEDURE  -> P_GET_COUPON_DETAIL
  -- NEW ARGUMENTS (17)
  -- GLOBAL        :GLOBAL.PRIM_LANG....................... -> G_PRIM_LANG
  -- GLOBAL        :GLOBAL.USER_LANG....................... -> G_USER_LANG
  -- ITEM          :B_SEARCH.TI_CLASS...................... -> I_TI_CLASS
  -- ITEM          :B_SEARCH.TI_CLASS_NAME................. -> I_TI_CLASS_NAME
  -- ITEM          :B_SEARCH.TI_COUPON_DESC................ -> I_TI_COUPON_DESC
  -- ITEM          :B_SEARCH.TI_COUPON_ID.................. -> I_TI_COUPON_ID
  -- ITEM          :B_SEARCH.TI_DEPT....................... -> I_TI_DEPT
  -- ITEM          :B_SEARCH.TI_DEPT_NAME.................. -> I_TI_DEPT_NAME
  -- ITEM          :B_SEARCH.TI_EFFECTIVE_DATE_FROM........ -> I_TI_EFFECTIVE_DATE_FROM
  -- ITEM          :B_SEARCH.TI_EXPIRATION_DATE_FROM....... -> I_TI_EXPIRATION_DATE_FROM
  -- ITEM          :B_SEARCH.TI_GROUP...................... -> I_TI_GROUP
  -- ITEM          :B_SEARCH.TI_GROUP_NAME................. -> I_TI_GROUP_NAME
  -- ITEM          :B_SEARCH.TI_ITEM....................... -> I_TI_ITEM
  -- ITEM          :B_SEARCH.TI_ITEM_NAME.................. -> I_TI_ITEM_NAME
  -- ITEM          :B_SEARCH.TI_MODIFY_ID.................. -> I_TI_MODIFY_ID
  -- ITEM          :B_SEARCH.TI_SUBCLASS................... -> I_TI_SUBCLASS
  -- ITEM          :B_SEARCH.TI_SUBCLASS_NAME.............. -> I_TI_SUBCLASS_NAME

  ---------------------------------------------------------------------------------------------
FUNCTION GET_COUPON_DETAIL(O_error_message           IN OUT VARCHAR2,
                           G_PRIM_LANG               IN     VARCHAR2,
                           G_USER_LANG               IN     VARCHAR2,
                           I_TI_CLASS                IN OUT NUMBER,
                           I_TI_CLASS_NAME           IN OUT VARCHAR2,
                           I_TI_COUPON_DESC          IN OUT VARCHAR2,
                           I_TI_COUPON_ID            IN     NUMBER,
                           I_TI_DEPT                 IN OUT NUMBER,
                           I_TI_DEPT_NAME            IN OUT VARCHAR2,
                           I_TI_EFFECTIVE_DATE_FROM  IN OUT DATE,
                           I_TI_EXPIRATION_DATE_FROM IN OUT DATE,
                           I_TI_GROUP                IN OUT NUMBER,
                           I_TI_GROUP_NAME           IN OUT VARCHAR2,
                           I_TI_ITEM                 IN OUT VARCHAR2,
                           I_TI_ITEM_NAME            IN OUT VARCHAR2,
                           I_TI_MODIFY_ID            IN OUT VARCHAR2,
                           I_TI_SUBCLASS             IN OUT NUMBER,
                           I_TI_SUBCLASS_NAME        IN OUT VARCHAR2)
  return BOOLEAN
is
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE := NULL;
  L_program         VARCHAR2(64)                   := 'FOUNDATION_SQL.GET_COUPON_DETAIL';
  ---
  cursor C_DETAILS is
     select vpc.coupon_desc,
            pc.effective_date,
            pc.expiration_date,
            pc.modify_id
       from v_pos_coupon_head_tl vpc,
            pos_coupon_head pc
      where vpc.coupon_id = I_TI_COUPON_ID 
        and vpc.coupon_id=pc.coupon_id;
  ---
  cursor C_GROUP is
     select group_no,
            dept,
            class,
            subclass,
            item
       from pos_merch_criteria
      where pos_config_id = I_TI_COUPON_ID
        and pos_config_type = 'COUP';
  ---
BEGIN
  open C_DETAILS;
  fetch C_DETAILS
  into I_TI_COUPON_DESC,
    I_TI_EFFECTIVE_DATE_FROM,
    I_TI_EXPIRATION_DATE_FROM,
    I_TI_MODIFY_ID;
  close C_DETAILS;
  ---
  open C_GROUP;
  fetch C_GROUP
  into I_TI_GROUP,
    I_TI_DEPT,
    I_TI_CLASS,
    I_TI_SUBCLASS,
    I_TI_ITEM;
  close C_GROUP;
  ---
  if I_TI_GROUP is NOT NULL then
    if MERCH_ATTRIB_SQL.GROUP_NAME (L_error_message,
                                    I_TI_GROUP,
                                    I_TI_GROUP_NAME) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
  end if;
  ---
  if I_TI_DEPT is NOT NULL then
    if DEPT_ATTRIB_SQL.GET_NAME (L_error_message,
                                 I_TI_DEPT,
                                 I_TI_DEPT_NAME) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
  end if;
  ---
  if I_TI_CLASS is NOT NULL then
    if CLASS_ATTRIB_SQL.GET_NAME (L_error_message,
                                  I_TI_DEPT,
                                  I_TI_CLASS,
                                  I_TI_CLASS_NAME) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
  end if;
  ---
  if I_TI_SUBCLASS is NOT NULL then
    if SUBCLASS_ATTRIB_SQL.GET_NAME (L_error_message,
                                     I_TI_DEPT,
                                     I_TI_CLASS,
                                     I_TI_SUBCLASS,
                                     I_TI_SUBCLASS_NAME) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
  end if;
  ---
  if I_TI_ITEM is NOT NULL then
    if ITEM_ATTRIB_SQL.GET_DESC (L_error_message,
                                 I_TI_ITEM_NAME,
                                 I_TI_ITEM) = FALSE then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
  end if;
  ---
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : poscoup (Form Module)
-- Source Object            : PROCEDURE  -> P_RECORD_LOCKING
-- NEW ARGUMENTS (2)
-- ITEM          :B_MV_POS_COUPON_HEAD.COUPON_ID......... -> I_COUPON_ID01
-- ITEM          :B_POS_COUPON_HEAD.COUPON_ID............ -> I_COUPON_ID
---------------------------------------------------------------------------------------------
FUNCTION RECORD_LOCKING_POSCOUP(O_error_message  IN OUT   VARCHAR2,
                                I_COUPON_ID01    IN       NUMBER,
                                I_COUPON_ID      IN       NUMBER)
  return BOOLEAN
is
  L_program       VARCHAR2(64) := 'FOUNDATION_SQL.RECORD_LOCKING_POSCOUP';
  L_table         VARCHAR2(30);
  Record_Locked   EXCEPTION;
  PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
  ---
  cursor C_LOCK_POS_STORE is
     select 'x'
       from pos_store
      where pos_config_id = I_COUPON_ID
        and pos_config_type = 'COUP' FOR update nowait;
  cursor C_LOCK_POS_MERCH_CRITERIA is
     select 'x'
       from pos_merch_criteria
      where pos_config_id = I_COUPON_ID01
        and pos_config_type = 'COUP' FOR update nowait;
BEGIN
  L_table := 'POS_STORE';
  open C_LOCK_POS_STORE;
  close C_LOCK_POS_STORE;
  ---
  L_table := 'POS_MERCH_CRITERIA';
  open C_LOCK_POS_MERCH_CRITERIA;
  close C_LOCK_POS_MERCH_CRITERIA;
  return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ERR_LOCK_REC',
                                            NULL,
                                            NULL,
                                            NULL);
  return FALSE;
when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : poscoup (Form Module)
-- Source Object            : PROCEDURE  -> P_GET_TOTAL_ITEMS
-- NEW ARGUMENTS (2)
-- ITEM          :B_POS_COUPON_HEAD.COUPON_ID............ -> I_COUPON_ID
-- ITEM          :B_POS_COUPON_HEAD.TI_TOTAL_ITEMS....... -> I_TI_TOTAL_ITEMS
---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_ITEMS_POSCOUP(O_error_message    IN OUT   VARCHAR2,
                                 I_COUPON_ID        IN       NUMBER,
                                 I_TI_TOTAL_ITEMS   IN OUT   NUMBER)
  return BOOLEAN
is
  L_total_items NUMBER;
  L_program     VARCHAR2(64) := 'FOUNDATION_SQL.GET_TOTAL_ITEMS_POSCOUP';
  ---
  cursor C_TOTAL_ITEMS is
     select COUNT(DISTINCT im.item)
       from item_master im,
            deps d,
            pos_merch_criteria pm
      where pm.pos_config_id = I_COUPON_ID
        and pm.pos_config_type = 'COUP'
        and NVL(pm.status, '*') != 'D'
        and im.status = 'A'
        and ( d.group_no = pm.group_no
        and d.dept = im.dept
        and ( (pm.dept is NULL)
         or ( im.dept = pm.dept
        and pm.class is NULL)
         or ( pm.dept = im.dept
        and pm.class = im.class
        and pm.subclass is NULL)
         or ( pm.dept = im.dept
        and pm.class = im.class
        and pm.subclass = im.subclass
        and pm.item is NULL)
         or ( pm.dept = im.dept
        and pm.class = im.class
        and pm.subclass = im.subclass
        and pm.item = im.item
       and pm.exclude_ind = 'N'))
       and NOT EXISTS
         (select 'x'
            from pos_merch_criteria pmc
           where pmc.pos_config_id = I_COUPON_ID
             and pmc.pos_config_type = 'COUP'
             and NVL(pmc.status, '*') != 'D'
             and item = im.item
             and pmc.exclude_ind = 'Y'));
  BEGIN
    open C_TOTAL_ITEMS;
    fetch C_TOTAL_ITEMS into L_total_items;
    close C_TOTAL_ITEMS;
    ---
    if L_total_items   is NULL then
      I_TI_TOTAL_ITEMS := 0;
    ELSE
      I_TI_TOTAL_ITEMS := L_total_items;
    end if;
    return TRUE;
  EXCEPTION
     when OTHERS then
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
    return FALSE;
  end;
  ---------------------------------------------------------------------------------------------
  -- Module                   : poscoup (Form Module)
  -- Source Object            : PROCEDURE  -> P_GET_TOTAL_STORES
  -- NEW ARGUMENTS (2)
  -- ITEM          :B_POS_COUPON_HEAD.COUPON_ID............ -> I_COUPON_ID
  -- ITEM          :B_POS_COUPON_HEAD.TI_TOTAL_STORES...... -> I_TI_TOTAL_STORES
  ---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_STORES(O_error_message     IN OUT   VARCHAR2,
                          I_COUPON_ID         IN       NUMBER,
                          I_TI_TOTAL_STORES   IN OUT   NUMBER)
  return BOOLEAN
is
  L_total_sites NUMBER;
  L_program     VARCHAR2(64) := 'FOUNDATION_SQL.GET_TOTAL_STORES';
  cursor C_GET_TOTAL_SITES is
     select COUNT(store)
       from pos_store
      where pos_config_type = 'COUP'
        and pos_config_id = I_COUPON_ID
        and (status is NULL
         or status != 'D');
BEGIN
  open C_GET_TOTAL_SITES;
  fetch C_GET_TOTAL_SITES into L_total_sites;
  close C_GET_TOTAL_SITES;
  I_TI_TOTAL_STORES := L_total_sites;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : posmcrit (Form Module)
-- Source Object            : PROCEDURE  -> P_GET_ITEM_DETAIL
-- NEW ARGUMENTS (15)
-- ITEM          :B_APPLY_EXCLUSIONS.TI_ITEM............. -> I_TI_ITEM01
-- ITEM          :B_APPLY_EXCLUSIONS.TI_ITEM_DESC........ -> I_TI_ITEM_DESC01
-- ITEM          :B_APPLY_INCLUSIONS.TI_CLASS............ -> I_TI_CLASS
-- ITEM          :B_APPLY_INCLUSIONS.TI_CLASS_DESC....... -> I_TI_CLASS_DESC
-- ITEM          :B_APPLY_INCLUSIONS.TI_DEPT............. -> I_TI_DEPT
-- ITEM          :B_APPLY_INCLUSIONS.TI_DEPT_DESC........ -> I_TI_DEPT_DESC
-- ITEM          :B_APPLY_INCLUSIONS.TI_GROUP............ -> I_TI_GROUP
-- ITEM          :B_APPLY_INCLUSIONS.TI_GROUP_DESC....... -> I_TI_GROUP_DESC
-- ITEM          :B_APPLY_INCLUSIONS.TI_ITEM............. -> I_TI_ITEM
-- ITEM          :B_APPLY_INCLUSIONS.TI_ITEM_DESC........ -> I_TI_ITEM_DESC
-- ITEM          :B_APPLY_INCLUSIONS.TI_SUBCLASS......... -> I_TI_SUBCLASS
-- ITEM          :B_APPLY_INCLUSIONS.TI_SUBCLASS_DESC.... -> I_TI_SUBCLASS_DESC
-- Pack.Spec     INTERNAL_VARIABLES.GP_CLASS............. -> P_IntrnlVrblsGpClss
-- Pack.Spec     INTERNAL_VARIABLES.GP_DEPT_VALUE........ -> P_IntrnlVrblsGpDptVle
-- SYSTEM        :SYSTEM.CURRENT_BLOCK................... -> SYSTEM_CURRENT_BLOCK
---------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_DETAIL(O_error_message         IN OUT   VARCHAR2,
                         I_TI_ITEM01             IN       VARCHAR2,
                         I_TI_ITEM_DESC01        IN       VARCHAR2,
                         I_TI_CLASS              IN OUT   NUMBER,
                         I_TI_CLASS_DESC         IN OUT   VARCHAR2,
                         I_TI_DEPT               IN OUT   NUMBER,
                         I_TI_DEPT_DESC          IN OUT   VARCHAR2,
                         I_TI_GROUP              IN OUT   NUMBER,
                         I_TI_GROUP_DESC         IN OUT   VARCHAR2,
                         I_TI_ITEM               IN       VARCHAR2,
                         I_TI_ITEM_DESC          IN       VARCHAR2,
                         I_TI_SUBCLASS           IN OUT   NUMBER,
                         I_TI_SUBCLASS_DESC      IN OUT   VARCHAR2,
                         P_IntrnlVrblsGpClss     IN OUT   NUMBER,
                         P_IntrnlVrblsGpDptVle   IN OUT   NUMBER,
                         SYSTEM_CURRENT_BLOCK    IN       VARCHAR2)
  return BOOLEAN
is
  L_item                ITEM_MASTER.ITEM%TYPE;
  L_program             VARCHAR2(64) := 'FOUNDATION_SQL.GET_ITEM_DETAIL';
  ---
  L_error_message       VARCHAR2(255);
  L_item_desc           ITEM_MASTER.ITEM_DESC%TYPE;
  L_item_level          ITEM_MASTER.ITEM_LEVEL%TYPE;
  L_tran_level          ITEM_MASTER.TRAN_LEVEL%TYPE;
  L_status              ITEM_MASTER.STATUS%TYPE;
  L_pack_ind            ITEM_MASTER.PACK_IND%TYPE;
  L_dept                ITEM_MASTER.DEPT%TYPE;
  L_class               ITEM_MASTER.CLASS%TYPE;
  L_subclass            ITEM_MASTER.SUBCLASS%TYPE;
  L_sellable_ind        ITEM_MASTER.SELLABLE_IND%TYPE;
  L_orderable_ind       ITEM_MASTER.ORDERABLE_IND%TYPE;
  L_pack_type           ITEM_MASTER.PACK_TYPE%TYPE;
  L_simple_pack_ind     ITEM_MASTER.SIMPLE_PACK_IND%TYPE;
  L_waste_type          ITEM_MASTER.WASTE_TYPE%TYPE;
  L_item_parent         ITEM_MASTER.ITEM_PARENT%TYPE;
  L_item_grandparent    ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
  L_short_desc          ITEM_MASTER.SHORT_DESC%TYPE;
  L_waste_pct           ITEM_MASTER.WASTE_PCT%TYPE;
  L_default_waste_pct   ITEM_MASTER.DEFAULT_WASTE_PCT%TYPE;
  ---
  cursor C_GET_GROUP is
     select g.group_no,
            g.group_name
       from groups g,
            deps d
      where g.group_no = d.group_no
        and d.dept = I_TI_DEPT;
BEGIN
  if SYSTEM_CURRENT_BLOCK = 'B_APPLY_INCLUSIONS' then
    L_item := I_TI_ITEM;
    L_item_desc := I_TI_ITEM_DESC;
     elsif SYSTEM_CURRENT_BLOCK = 'B_APPLY_EXCLUSIONS' then
       L_item := I_TI_ITEM01;
       L_item_desc := I_TI_ITEM_DESC01;
  end if;
  ---
  if ITEM_ATTRIB_SQL.GET_INFO(L_error_message,
                              L_item_desc,
                              L_item_level,
                              L_tran_level,
                              L_status,
                              L_pack_ind,
                              I_TI_DEPT,
                              I_TI_DEPT_DESC,
                              I_TI_CLASS,
                              I_TI_CLASS_DESC,
                              I_TI_SUBCLASS,
                              I_TI_SUBCLASS_DESC,
                              L_sellable_ind,
                              L_orderable_ind,
                              L_pack_type,
                              L_simple_pack_ind,
                              L_waste_type,
                              L_item_parent,
                              L_item_grandparent,
                              L_short_desc,
                              L_waste_pct,
                              L_default_waste_pct,
                              L_item ) = FALSE then

     O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                          NULL,
                                          NULL,
                                          NULL);
    return FALSE;
  end if;
  ---
  open C_GET_GROUP;
  fetch C_GET_GROUP into I_TI_GROUP, I_TI_GROUP_DESC;
  close C_GET_GROUP;
  P_IntrnlVrblsGpDptVle := I_TI_DEPT;
  P_IntrnlVrblsGpClss   := I_TI_CLASS;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : posmcrit (Form Module)
-- Source Object            : PROCEDURE  -> P_RECORD_LOCKING
-- NEW ARGUMENTS (2)
-- PARAMETER     :PARAMETER.PM_POS_CONFIG_ID............. -> P_PM_POS_CONFIG_ID
-- PARAMETER     :PARAMETER.PM_POS_CONFIG_TYPE........... -> P_PM_POS_CONFIG_TYPE
---------------------------------------------------------------------------------------------
FUNCTION RECORD_LOCKING_POSMCRIT(O_error_message        IN OUT   VARCHAR2,
                                 P_PM_POS_CONFIG_ID     IN       NUMBER,
                                 P_PM_POS_CONFIG_TYPE   IN       VARCHAR2)
  return BOOLEAN
is
  L_program       VARCHAR2(64) := 'FOUNDATION_SQL.RECORD_LOCKING_POSMCRIT';
  L_table         VARCHAR2(30);
  Record_Locked   EXCEPTION;
  PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
  ---
  cursor C_LOCK_POS_MERCH_CRITERIA is
     select 'x'
       from pos_merch_criteria
      where pos_config_id = P_PM_POS_CONFIG_ID
        and pos_config_type = P_PM_POS_CONFIG_TYPE FOR update nowait;
BEGIN
  L_table := 'POS_MERCH_CRITERIA';
  open C_LOCK_POS_MERCH_CRITERIA;
  close C_LOCK_POS_MERCH_CRITERIA;
  return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(P_PM_POS_CONFIG_ID),
                                            'POS Merchandise Criteria');
  return FALSE;
when OTHERS then
   O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : pospres (Form Module)
-- Source Object            : FUNCTION   -> F_CHECK_DUPLICATES
-- NEW ARGUMENTS (4)
-- ITEM          :B_APPLY.LI_AFTER_TIME.................. -> I_LI_AFTER_TIME
-- ITEM          :B_APPLY.LI_BEFORE_TIME................. -> I_LI_BEFORE_TIME
-- ITEM          :B_APPLY.TI_DATE........................ -> I_TI_DATE
-- ITEM          :B_APPLY_HEAD.TI_POS_PROD_REST_ID....... -> I_TI_POS_PROD_REST_ID
---------------------------------------------------------------------------------------------
FUNCTION CHECK_DUPLICATES(O_error_message         IN OUT   VARCHAR2,
                          O_ret                      OUT   VARCHAR2,
                          I_LI_AFTER_TIME         IN       VARCHAR2,
                          I_LI_BEFORE_TIME        IN       VARCHAR2,
                          I_TI_DATE               IN       DATE,
                          I_TI_POS_PROD_REST_ID   IN       NUMBER,
                          DAY_NO                           VARCHAR2)
  return BOOLEAN
is
  L_exist     VARCHAR2(1)  := 'N';
  L_program   VARCHAR2(64) := 'FOUNDATION_SQL.CHECK_DUPLICATES';
  ---
  cursor C_CHECK_EXIST is
     select 'Y'
       from pos_day_time_date
      where pos_prod_rest_id = I_TI_POS_PROD_REST_ID
        and NVL(DAY,0) = NVL(DAY_NO,0)
        and NVL(before_time,-1) = NVL(I_LI_BEFORE_TIME,-1)
        and NVL(after_time, -1) = NVL(I_LI_AFTER_TIME,-1)
        and NVL(pos_restrict_date,to_date('01-01-1900','DD-MM-YYYY')) = NVL(I_TI_DATE,to_date('01-01-1900','DD-MM-YYYY'));
BEGIN
  open C_CHECK_EXIST;
  fetch C_CHECK_EXIST into L_exist;
  close C_CHECK_EXIST;
  ---
  O_ret := L_exist;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : pospres (Form Module)
-- Source Object            : PROCEDURE  -> P_GET_TOTAL_ITEMS
-- NEW ARGUMENTS (2)
-- ITEM          :B_POS_PROD_REST_HEAD.POS_PROD_REST_ID.. -> I_POS_PROD_REST_ID
-- ITEM          :B_POS_PROD_REST_HEAD.TI_TOTAL_ITEMS.... -> I_TI_TOTAL_ITEMS
---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_ITEMS_POSPRES(O_error_message      IN OUT   VARCHAR2,
                                 I_POS_PROD_REST_ID   IN       NUMBER,
                                 I_TI_TOTAL_ITEMS     IN OUT   NUMBER)
  return BOOLEAN
is
  L_total_items    NUMBER;
  L_program        VARCHAR2(64) := 'FOUNDATION_SQL.GET_TOTAL_ITEMS_POSPRES';
  ---
  cursor C_TOTAL_ITEMS is
     select COUNT(DISTINCT im.item)
       from item_master im,
            deps d,
            pos_merch_criteria pm
      where pm.pos_config_id = I_POS_PROD_REST_ID
        and pm.pos_config_type = 'PRES'
        and im.status = 'A'
        and im.item_level = im.tran_level
        and ( d.group_no = pm.group_no
        and d.dept = im.dept
        and ( (pm.dept is NULL)
         or ( im.dept = pm.dept
        and pm.class is NULL)
         or ( pm.dept = im.dept
        and pm.class = im.class
        and pm.subclass is NULL)
         or ( pm.dept = im.dept
        and pm.class = im.class
        and pm.subclass = im.subclass
        and pm.item is NULL)
         or ( pm.dept = im.dept
        and pm.class = im.class
        and pm.subclass = im.subclass
        and pm.item = im.item
        and pm.exclude_ind = 'N'))
        and NOT EXISTS
          (select 'x'
             from pos_merch_criteria pmc
            where pmc.pos_config_id = I_POS_PROD_REST_ID
              and pmc.pos_config_type = 'PRES'
              and item = im.item
              and pmc.exclude_ind = 'Y'));
  BEGIN
    open C_TOTAL_ITEMS;
    fetch C_TOTAL_ITEMS into L_total_items;
    close C_TOTAL_ITEMS;
    ---
    if L_total_items   is NULL then
      I_TI_TOTAL_ITEMS := 0;
    ELSE
      I_TI_TOTAL_ITEMS := L_total_items;
    end if;
    return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
    return FALSE;
  end;
  ---------------------------------------------------------------------------------------------
  -- Module                   : pospres (Form Module)
  -- Source Object            : PROCEDURE  -> P_GET_TOTAL_SITES
  -- NEW ARGUMENTS (2)
  -- ITEM          :B_POS_PROD_REST_HEAD.POS_PROD_REST_ID.. -> I_POS_PROD_REST_ID
  -- ITEM          :B_POS_PROD_REST_HEAD.TI_TOTAL_STORES... -> I_TI_TOTAL_STORES
  ---------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_SITES(O_error_message      IN OUT   VARCHAR2,
                         I_POS_PROD_REST_ID   IN       NUMBER,
                         I_TI_TOTAL_STORES    IN OUT   NUMBER)
  return BOOLEAN
is
  L_program       VARCHAR2(64) := 'FOUNDATION_SQL.GET_TOTAL_SITES';
  L_store_count   NUMBER       := 0;
  cursor C_STORES is
     select COUNT(store)
       from pos_store
      where pos_config_type = 'PRES'
        and pos_config_id = I_POS_PROD_REST_ID
        and (status is NULL
         or status != 'D');
BEGIN
  open C_STORES;
  fetch C_STORES into L_store_count;
  close C_STORES;
  ---
  I_TI_TOTAL_STORES := L_store_count;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : posstore (Form Module)
-- Source Object            : PROCEDURE  -> P_COUNT_STORES
-- NEW ARGUMENTS (6)
-- ITEM          :B_POS_STORE.CB_PARTIAL_VIEW............ -> I_CB_PARTIAL_VIEW
-- ITEM          :B_POS_STORE.TI_TOTAL_STORES............ -> I_TI_TOTAL_STORES
-- PARAMETER     :PARAMETER.PM_POS_CONFIG_ID............. -> P_PM_POS_CONFIG_ID
-- PARAMETER     :PARAMETER.PM_POS_CONFIG_TYPE........... -> P_PM_POS_CONFIG_TYPE
-- Pack.Spec     INTERNAL_VARIABLES.GP_FORM_STARTUP...... -> P_IntrnlVrblsGpFrmStrtp
-- Pack.Spec     INTERNAL_VARIABLES.GP_STORE_COUNT....... -> P_IntrnlVrblsGpStreCnt

---------------------------------------------------------------------------------------------
FUNCTION COUNT_STORES(O_error_message           IN OUT   VARCHAR2,
                      I_CB_PARTIAL_VIEW         IN OUT   VARCHAR2,
                      I_TI_TOTAL_STORES         IN OUT   NUMBER,
                      P_PM_POS_CONFIG_ID        IN       NUMBER,
                      P_PM_POS_CONFIG_TYPE      IN       VARCHAR2,
                      P_IntrnlVrblsGpFrmStrtp   IN OUT   VARCHAR2,
                      P_IntrnlVrblsGpStreCnt    IN OUT   NUMBER)
  return BOOLEAN
is
  L_program         VARCHAR2(64) := 'FOUNDATION_SQL.COUNT_STORES';
  L_stores          NUMBER       := 0;
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  cursor C_GET_STORES is
     select COUNT(store)
       from pos_store
      where pos_config_id = P_PM_POS_CONFIG_ID
        and pos_config_type = P_PM_POS_CONFIG_TYPE
        and (status is NULL
         or status  != 'D');
BEGIN
  open C_GET_STORES;
  fetch C_GET_STORES into L_stores;
  I_TI_TOTAL_STORES := L_stores;
  close C_GET_STORES;
  if P_IntrnlVrblsGpFrmStrtp = 'Y' then
    P_IntrnlVrblsGpStreCnt  := L_stores;
    P_IntrnlVrblsGpFrmStrtp := 'N';
  end if;
  if NOT POS_CONFIG_SQL.POS_STORE_FILTER_LIST(L_error_message,
                                              I_CB_PARTIAL_VIEW,
                                              L_stores,
                                              P_PM_POS_CONFIG_TYPE,
                                              P_PM_POS_CONFIG_ID) then
    O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                          NULL,
                                          NULL,
                                          NULL);
    return FALSE;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : rolepriv (Form Module)
-- Source Object            : PROCEDURE  -> FORM_VALIDATION.ROLE
-- NEW ARGUMENTS (1)
-- ITEM          :B_ROLE.ROLE............................ -> I_ROLE
---------------------------------------------------------------------------------------------
FUNCTION ROLE(O_error_message   IN OUT   VARCHAR2,
              I_ROLE            IN       VARCHAR2)
  return BOOLEAN
is
  L_program   VARCHAR2(64) := 'FOUNDATION_SQL.ROLE';
  L_role      DBA_ROLES.ROLE%TYPE;
  cursor C_ROLE is
     select role
       from dba_roles DBA
      where dba.role = I_ROLE
        and NOT EXISTS
           (select 'X'
              from rtk_role_privs rrp
             where rrp.role = dba.role);
BEGIN
  if I_ROLE is NOT NULL then
    open C_ROLE;
    fetch C_ROLE into L_role;
    if C_ROLE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('ROLE_INVALID',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_ROLE;
      return FALSE;
    end if;
    close C_ROLE;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end ROLE;
end FOUNDATION_SQL;
/
