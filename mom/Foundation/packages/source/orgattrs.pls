
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORGANIZATION_ATTRIB_SQL AUTHID CURRENT_USER AS
  
----------------------------------------------------------------
-- Function Name: GET_NAME 
-- Purpose  :
-- Input Values : I_org_hier_type   --an IN parameter
--              : I_org_hier_value  --an IN parameter
--      : O_org_hier_desc   --an IN OUT parameter 
-- Created  : 09-Oct-96 Carrie Kavanaugh 
----------------------------------------------------------------
FUNCTION GET_NAME (O_error_message  IN OUT  VARCHAR2,
           I_org_hier_type      IN      NUMBER,
                   I_org_hier_value     IN      NUMBER,
                   O_org_hier_desc  IN OUT  VARCHAR2)
    RETURN BOOLEAN;

----------------------------------------------------------------
-- Function: COMPANY_NAME
-- Accepts a company number and returns the company description
-- Created: 18-NOV-96 Richard Stockton
----------------------------------------------------------------

FUNCTION COMPANY_NAME ( O_error_message IN OUT  VARCHAR2,
                I_company       IN      NUMBER,
                O_company_name  IN OUT  VARCHAR2)
                RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: CHAIN_NAME
-- Accepts a chain number and returns the chain description
-- Created: 18-NOV-96 Richard Stockton
----------------------------------------------------------------

FUNCTION CHAIN_NAME (   O_error_message IN OUT  VARCHAR2,
                I_chain     IN      NUMBER,
                O_chain_name    IN OUT  VARCHAR2)
                RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: AREA_NAME
-- Accepts an area number and returns the area description
-- Created: 18-NOV-96 Richard Stockton
----------------------------------------------------------------
FUNCTION AREA_NAME (    O_error_message IN OUT  VARCHAR2,
                I_area      IN      NUMBER,
                O_area_name     IN OUT  VARCHAR2)
                RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: REGION_NAME
-- Accepts a region number and returns the region description
-- Created: 18-NOV-96 Richard Stockton
----------------------------------------------------------------
FUNCTION REGION_NAME (  O_error_message IN OUT  VARCHAR2,
                I_region        IN      NUMBER,
                O_region_name   IN OUT  VARCHAR2)
                RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: DISTRICT_NAME
-- Accepts a district number and returns the district description
-- Created: 18-NOV-96 Richard Stockton
----------------------------------------------------------------
FUNCTION DISTRICT_NAME (O_error_message IN OUT  VARCHAR2,
            I_district  IN NUMBER,
            O_district_name IN OUT  VARCHAR2)
            RETURN BOOLEAN;
----------------------------------------------------------------
-- Name:    GET_DISTRICT_REGION
-- Purpose: This function will take a district as an input parameter 
--          and will then return the region that the district is within 
--          in organizational hierarchy.
-- Created By: Jeff Fang 06-10-99
----------------------------------------------------------------
FUNCTION GET_DISTRICT_REGION(O_error_message IN OUT VARCHAR2,
                             O_region        IN OUT district.region%TYPE,
                             I_district      IN     district.district%TYPE)
RETURN BOOLEAN; 
----------------------------------------------------------------
-- Function: GET_REGION_AREA
-- Purpose:  This function will retrieve the code and description
--           of area for the passed in region. 
-- Created:  27-Jul-99 Fang Sheng
----------------------------------------------------------------
FUNCTION GET_REGION_AREA (O_error_message IN OUT   VARCHAR2,
                          O_desc      IN OUT   area.area_name%TYPE,
                          O_area      IN OUT   area.area%TYPE,
                          I_region        IN       region.region%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: GET_AREA_CHAIN
-- Purpose:  This function will retrieve the code and description
--           of chain for the passed in area. 
-- Created:  27-Jul-99 Fang Sheng
----------------------------------------------------------------
FUNCTION GET_AREA_CHAIN (O_error_message IN OUT   VARCHAR2,
                         O_desc          IN OUT   chain.chain_name%TYPE,
                         O_chain         IN OUT   chain.chain%TYPE,
                         I_area          IN       area.area%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: GET_PARENT
-- Accepts hier_level and hier_value then returns the parent id
-- Created: Aug-14/2003   Asher Ledesma
----------------------------------------------------------------

FUNCTION GET_PARENT (O_error_message    IN OUT      RTK_ERRORS.RTK_TEXT%TYPE,
                     O_parent_id        OUT         NUMBER,
                     I_hier_level       IN          VARCHAR2,
                     I_hier_value       IN          NUMBER)
RETURN BOOLEAN;
----------------------------------------------------------------
-- Function: GET_ORG_HIER_LABEL
-- Returns the labels for the levels of the organization hierarchy
----------------------------------------------------------------
FUNCTION GET_ORG_HIER_LABEL(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_company         IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                            O_chain           IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                            O_area            IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                            O_region          IN OUT  CODE_DETAIL.CODE_DESC%TYPE,
                            O_district        IN OUT  CODE_DETAIL.CODE_DESC%TYPE)
RETURN NUMBER;
----------------------------------------------------------------
END ORGANIZATION_ATTRIB_SQL;
/



