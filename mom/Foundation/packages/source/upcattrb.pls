
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY UPC_SQL AS 
-------------------------------------------------------------------------------
-- Function:  Next_var_upc_item
-- Purpose:   Called for generating the next unique variable UPC item code.
--            The item code will be five digits long and will be used to
--            uniquely identify each variable weight UPC.
-- Calls:     var_upc_ean_sequence
-- Created:   Yumen Li Mar. 3, 1999
-------------------------------------------------------------------------------
FUNCTION NEXT_VAR_UPC_ITEM(O_error_message   IN OUT VARCHAR2,
                           O_upc_item_code   IN OUT NUMBER) RETURN BOOLEAN IS
   cursor C_UPC_SEQ is
      select var_upc_ean_sequence.NEXTVAL
        from dual;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_UPC_SEQ','dual',NULL);
   open C_UPC_SEQ;

   SQL_LIB.SET_MARK('FETCH','C_UPC_SEQ','dual',NULL);
   fetch C_UPC_SEQ into O_upc_item_code;

   SQL_LIB.SET_MARK('CLOSE','C_UPC_SEQ','dual',NULL);
   close C_UPC_SEQ;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                            'UPC_SQL.NEXT_VAR_UPC_ITEM', 
                                             TO_CHAR(SQLCODE));
      return FALSE;

END NEXT_VAR_UPC_ITEM;
END UPC_SQL; 
/ 


