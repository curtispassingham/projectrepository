
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFFID_XML AUTHID CURRENT_USER AS

--------------------------------------------------------
FUNCTION BUILD_MESSAGE(O_status          OUT VARCHAR2,
                       O_text            OUT VARCHAR2,
                       O_message         OUT CLOB,
                       I_diffid_rec      IN  DIFF_IDS%ROWTYPE,
                       I_action_type     IN  VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
END DIFFID_XML;
/
