
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_MERCHHIER AUTHID CURRENT_USER AS

FAMILY         CONSTANT  RIB_SETTINGS.FAMILY%TYPE := 'merchhier';

DIV_ADD        CONSTANT  VARCHAR2(64) := 'divisioncre';
DIV_UPD        CONSTANT  VARCHAR2(64) := 'divisionmod';
DIV_DEL        CONSTANT  VARCHAR2(64) := 'divisiondel';

GRP_ADD        CONSTANT  VARCHAR2(64) := 'groupcre';
GRP_UPD        CONSTANT  VARCHAR2(64) := 'groupmod';
GRP_DEL        CONSTANT  VARCHAR2(64) := 'groupdel';

DEP_ADD        CONSTANT  VARCHAR2(64) := 'deptcre';
DEP_UPD        CONSTANT  VARCHAR2(64) := 'deptmod';
DEP_DEL        CONSTANT  VARCHAR2(64) := 'deptdel';

CLS_ADD        CONSTANT  VARCHAR2(64) := 'classcre';
CLS_UPD        CONSTANT  VARCHAR2(64) := 'classmod';
CLS_DEL        CONSTANT  VARCHAR2(64) := 'classdel';

SUB_ADD        CONSTANT  VARCHAR2(64) := 'subclasscre';
SUB_UPD        CONSTANT  VARCHAR2(64) := 'subclassmod';
SUB_DEL        CONSTANT  VARCHAR2(64) := 'subclassdel';

--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_msg             OUT     VARCHAR2,
                I_message_type          IN      MERCHHIER_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_division              IN      DIVISION.DIVISION%TYPE,
                I_division_rec          IN      DIVISION%ROWTYPE,
                I_group_no              IN      GROUPS.GROUP_NO%TYPE,
                I_groups_rec            IN      GROUPS%ROWTYPE,
                I_dept                  IN      DEPS.DEPT%TYPE,
                I_deps_rec              IN      DEPS%ROWTYPE,
                I_class                 IN      CLASS.CLASS%TYPE,
                I_class_rec             IN      CLASS%ROWTYPE,
                I_subclass              IN      SUBCLASS.SUBCLASS%TYPE,
                I_subclass_rec          IN      SUBCLASS%ROWTYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code      OUT  VARCHAR2,
                 O_error_msg        OUT  VARCHAR2,
                 O_message_type     OUT  VARCHAR2,
                 O_message          OUT  RIB_OBJECT,
                 O_bus_obj_id       OUT  RIB_BUSOBJID_TBL,
                 O_routing_info     OUT  RIB_ROUTINGINFO_TBL,
                 I_num_threads      IN   NUMBER DEFAULT 1,
                 I_thread_val       IN   NUMBER DEFAULT 1);
--------------------------------------------------------------------------------                
PROCEDURE PUB_RETRY(O_status_code         OUT   VARCHAR2,
                    O_error_msg           OUT   VARCHAR2,
                    O_message_type    IN  OUT   VARCHAR2,
                    O_message             OUT   RIB_OBJECT,
                    O_bus_obj_id      IN  OUT   RIB_BUSOBJID_TBL,
                    O_routing_info    IN  OUT   RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT      IN        RIB_OBJECT);
--------------------------------------------------------------------------------
END RMSMFM_MERCHHIER;
/
