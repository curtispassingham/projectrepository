
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE MERCH_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------
--VARIABLES
---------------------------------------------------------------------
   TYPE vat_dept_detail_tbl IS TABLE OF VAT_DEPS%ROWTYPE;
   TYPE dept_rec            IS RECORD(dept_row   DEPS%ROWTYPE,
                                   vat_detail VAT_DEPT_DETAIL_TBL);

   LP_division      VARCHAR2(15) := 'Division';
---------------------------------------------------------------------
   -- Function    : INSERT_COMPANY
   -- Purpose     : Takes in a company table record and inserts all of
   --               its contents into the COMPANY table.
   -- Created by  : Asher Ledesma 08/20/2003
---------------------------------------------------------------------
FUNCTION INSERT_COMPANY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_company_rec     IN       COMPHEAD%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : UPDATE_COMPANY
   -- Purpose     : Takes in a company table record and updates all of
   --               its contents into the COMPANY table.
   -- Created by  : Asher Ledesma 08/20/2003
---------------------------------------------------------------------
FUNCTION UPDATE_COMPANY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_company_rec     IN       COMPHEAD%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : INSERT_DIVISION
   -- Purpose     : Takes in a division table record and inserts all of
   --               its contents into the DIVISION table.
---------------------------------------------------------------------
FUNCTION INSERT_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : MODIFY_DIVISION
   -- Purpose     : Takes in a division table record and updates all of
   --               its contents into the DIVISION table.
---------------------------------------------------------------------
FUNCTION MODIFY_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_DIVISION
   -- Purpose     : Takes in a division table record and deletes all of
   --               its contents into the DIVISION table.
---------------------------------------------------------------------
FUNCTION DELETE_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : INSERT_GROUP
   -- Purpose     : Takes in a group table record and inserts all of
   --               its contents into the GROUPS table.
---------------------------------------------------------------------

FUNCTION INSERT_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : MODIFY_GROUP
   -- Purpose     : Updates the GROUPS table with the values from the
   --               group table record
---------------------------------------------------------------------
FUNCTION MODIFY_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_GROUP
   -- Purpose     : Deletes a particular group from the GROUPS table
---------------------------------------------------------------------
FUNCTION DELETE_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : INSERT_DEPT
   -- Purpose     : Takes in a department record and inserts all of
   --               its contents into the deps table.
   -- Created     : 30-Aug-2003 by Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION INSERT_DEPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_dept_rec        IN       MERCH_SQL.DEPT_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : MODIFY_DEPT
   -- Purpose     : Takes in a department record and updates the deps table.
   -- Created     : 30-Aug-2003 by Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION MODIFY_DEPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_dept_rec        IN       DEPT_REC)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_DEPT
   -- Purpose     : Takes in a department record and inserts all of
   --               its contents into the daily_purge table.
   -- Created     : 30-Aug-2003 by Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION DELETE_DEPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_dept            IN       DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : INSERT_CLASS
   -- Purpose     : Takes in a class table record and inserts all of
   --               its contents into the class table.
   -- Created     : 20-Aug-2003 by Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION INSERT_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : MODIFY_CLASS
   -- Purpose     : Updates the class table with the values from the
   --               class table record
   -- Created     : 20-Aug-2003 by Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION MODIFY_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_CLASS
   -- Purpose     : Takes in a class table record and inserts all of
   --               its contents into the daily_purge table.
   -- Created     : 30-Aug-2003 by Eldonelle Dolloso
---------------------------------------------------------------------
FUNCTION DELETE_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : INSERT_SUBCLASS
   -- Purpose     : Takes in a subclass table record and inserts all of
   --               its contents into the SUBCLASS table.
   -- Created     : 30-Aug-2003 by Gerson Berea
---------------------------------------------------------------------
FUNCTION INSERT_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : MODIFY_SUBCLASS
   -- Purpose     : Updates the SUBCLASS table with the values from the
   --               SUBCLASS table record
   -- Created     : 30-Aug-2003 by Gerson Berea
---------------------------------------------------------------------
FUNCTION MODIFY_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function    : DELETE_SUBCLASS
   -- Purpose     : Deletes a particular SUBCLASS from the SUBCLASS table
   -- Created     : 30-Aug-2003 by Gerson Berea
---------------------------------------------------------------------
FUNCTION DELETE_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------
   -- Function Name: NEXT_DEPT_NUMBER
   -- Purpose      : Supplies the next available department number.   
---------------------------------------------------------------------
FUNCTION NEXT_DEPT_NUMBER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_dept_number     IN OUT   DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function Name: NEXT_SUBCLASS_NUMBER
   -- Purpose      : Supplies the next available subclass number for the
   --                class-dept combination.
---------------------------------------------------------------------
FUNCTION NEXT_SUBCLASS_NUMBER(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_subclass_number IN OUT  SUBCLASS.SUBCLASS%TYPE,
                              I_dept_number     IN      DEPS.DEPT%TYPE,
                              I_class_number    IN      CLASS.CLASS%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
   -- Function Name: NEXT_CLASS_NUMBER
   -- Purpose      : Supplies the next available subclass number for a
   --                dept.
---------------------------------------------------------------------
FUNCTION NEXT_CLASS_NUMBER(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_class_number    IN OUT  CLASS.CLASS%TYPE,
                           I_dept_number     IN      DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
  -- Function Name: DELETE_DIVISION_TL
  -- Purpose      :  Deletes from the division_tl table
---------------------------------------------------------------------
FUNCTION DELETE_DIVISION_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_division        IN       DIVISION_TL.DIVISION%TYPE)
   RETURN BOOLEAN;
   
-----------------------------------------------------------------------------
  -- Function Name: DELETE_GROUP_TL
  -- Purpose      :  Deletes  from the groups_tl table
---------------------------------------------------------------------
FUNCTION DELETE_GROUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_group_no        IN       GROUPS_TL.GROUP_NO%TYPE)
   RETURN BOOLEAN;
   
-----------------------------------------------------------------------------
  -- Function Name: DELETE_SUBCLASS_TL
  -- Purpose      :  Deletes from the SUBCLASS_TL table
---------------------------------------------------------------------
FUNCTION DELETE_SUBCLASS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_dept            IN       SUBCLASS_TL.DEPT%TYPE,
                            I_class           IN       SUBCLASS_TL.CLASS%TYPE,
                            I_subclass        IN       SUBCLASS_TL.SUBCLASS%TYPE )
RETURN BOOLEAN;
-----------------------------------------------------------------------------

  -- Function Name: DELETE_CLASS_TL
  -- Purpose      :  Deletes from the CLASS_TL table
---------------------------------------------------------------------
FUNCTION DELETE_CLASS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_dept            IN CLASS_TL.DEPT%TYPE,
                         I_class           IN  CLASS_TL.CLASS%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_DEPS
   -- Purpose      : This function will lock the DEPS table for update.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_DEPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_dept            IN       DEPS.DEPT%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
END MERCH_SQL;
/
