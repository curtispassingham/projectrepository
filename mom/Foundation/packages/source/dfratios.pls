
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DIFF_RATIO_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------
--Function Name : NEXT_SIZE_RATIO_ID
--Purpose       : This package will be used to obtain the next_diff_ratio_id.
--Called By     : diff_ratio_head 
--Return Values : diff_ratio_id, return_code
--Created By    : Sara Isbell
--                    03-OCT-97
-- Modification : Renamed to NEXT_DIFF_RATIO_ID
--
FUNCTION NEXT_DIFF_RATIO_ID (O_error_message IN OUT VARCHAR2,
                             O_diff_ratio_id IN OUT DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE) RETURN BOOLEAN;                              
------------------------------------------------------------------------------
-- Function Name : GET_DIFF_RATIO_DESC
-- Purpose       : Retrieves the diff ratio description for the diff 
--                 ratio id passed in and translates it.
-- Called By     : SZRTFIND.FMB
-- Input Value   : diff_ratio_id
-- Return Value  : diff_ratio_dsc
-- Created by    : Sara Isbell
--                     03-OCT-97
--
FUNCTION GET_DIFF_RATIO_DESC (O_ERROR_MESSAGE   IN OUT VARCHAR2,
                              O_DIFF_RATIO_DESC IN OUT DIFF_RATIO_HEAD.DESCRIPTION%TYPE,
                              I_DIFF_RATIO_ID   IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE) return BOOLEAN;                          
------------------------------------------------------------------------------                            
-- Function Name : DIFF_RATIO_EXISTS
-- Purpose       : Checks if the diff ratio passed in exists on the 
--                 diff_ratio_head table
-- Called By     : SZRTFIND.FMB
-- Input Value   : diff_ratio_id
-- Return Value  : found
-- Created by    : Sara Isbell
--                     03-OCT-97                            
--
FUNCTION DIFF_RATIO_EXISTS (O_ERROR_MESSAGE  IN OUT VARCHAR2,
                            O_FOUND          IN OUT BOOLEAN,
                            I_DIFF_RATIO_ID  IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE) return BOOLEAN;                            
------------------------------------------------------------------------------                             
-- Function Name : CREATE_LIKE_DIFF_RATIO
-- Purpose       : Creates new_diff_ratio_head, and diff_ratio_detail records 
--                 for the like_diff_ratio_id pased in.
-- Called By     : SZRTFIND.FMB
-- Input Value   : new_diff_ratio_id, like_diff_ratio_id
-- Created by    : Sara Isbell
--                     03-OCT-97                            
--
FUNCTION CREATE_LIKE_DIFF_RATIO (O_ERROR_MESSAGE        IN OUT VARCHAR2,
                                 I_NEW_DIFF_RATIO_ID    IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                                 I_NEW_DIFF_RATIO_DESC  IN     DIFF_RATIO_HEAD.DESCRIPTION%TYPE,
                                 I_LIKE_DIFF_RATIO_ID   IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE) return BOOLEAN;
------------------------------------------------------------------------------                          
-- Function Name : CREATE_LIKE_STORE_DETAIL
-- Purpose       : Creates new_diff_ratio_detail records for the diff_ratio_id 
--                 and new and like stores passed in.
-- Called By     : SZRTFIND.FMB
-- Input Value   : diff_ratio_id, new_store, like_store
-- Created by    : Sara Isbell
--                     03-OCT-97                            
--
FUNCTION CREATE_LIKE_STORE_DETAIL (O_ERROR_MESSAGE  IN OUT VARCHAR2,
                                   I_DIFF_RATIO_ID  IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                                   I_NEW_STORE      IN     STORE.STORE%TYPE,
                                   I_LIKE_STORE     IN     STORE.STORE%TYPE) return BOOLEAN;
------------------------------------------------------------------------------ 
-- Function Name : DELETE_STORE_DETAIL
-- Purpose       : Deletes detail records off the diff_ratio_detail table for a 
--                 specified diff ratio.
-- Called By     : SZRTFIND.FMB
-- Input Value   : diff_ratio_id
-- Created by    : Jason Barber
--                     18-OCT-97                            
--
FUNCTION DELETE_STORE_DETAIL (O_ERROR_MESSAGE  IN OUT VARCHAR2,
                              I_DIFF_RATIO_ID  IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE) return BOOLEAN;
------------------------------------------------------------------------------ 
-- Function Name : SIMILAR_DIFF_RATIO
-- Purpose       : Checks if a system generated diff ratio passed in has the same dept, class, subclass   
--                 and diff groups as another diff ratio on the diff_ratio_head table
--
FUNCTION SIMILAR_DIFF_RATIO(O_ERROR_MESSAGE  IN OUT VARCHAR2,
                            O_FOUND          IN OUT BOOLEAN,
                            I_DEPT           IN     DIFF_RATIO_HEAD.DEPT%TYPE,
                            I_CLASS          IN     DIFF_RATIO_HEAD.CLASS%TYPE,
                            I_SUBCLASS       IN     DIFF_RATIO_HEAD.SUBCLASS%TYPE,
                            I_DIFF_GROUP_1   IN     DIFF_RATIO_HEAD.DIFF_GROUP_1%TYPE,
                            I_DIFF_GROUP_2   IN     DIFF_RATIO_HEAD.DIFF_GROUP_2%TYPE,
                            I_DIFF_GROUP_3   IN     DIFF_RATIO_HEAD.DIFF_GROUP_3%TYPE,
                            I_SYSTEM_GEN_IND IN     DIFF_RATIO_HEAD.SYSTEM_GEN_IND%TYPE) return BOOLEAN; 
------------------------------------------------------------------------------ 
--Function Name : DETAIL_EXISTS
--Purpose       : Check to see if detail records exists on the diff_ratio_detail
--                for a specific diff ratio
--Called by     : SZRHEAD.FMB
--Input Value   : diff_ratio_id
--Created by    : Hien Tran April 4, 2000
--
FUNCTION DETAIL_EXISTS (O_ERROR_MESSAGE  IN OUT VARCHAR2,
                        O_EXISTS         IN OUT VARCHAR2, 
                        I_DIFF_RATIO_ID  IN  DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE) 
   return BOOLEAN;
------------------------------------------------------------------------------
--Function Name : GET_GROUPS_AND_DESC
--Purpose       : 
--Called by     : 
--Input Value   : 
--
FUNCTION GET_GROUPS_AND_DESC(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_ratio_desc       IN OUT DIFF_RATIO_HEAD.DESCRIPTION%TYPE,
                             O_diff_group_1     IN OUT DIFF_RATIO_HEAD.DIFF_GROUP_1%TYPE,
                             O_diff_group_2     IN OUT DIFF_RATIO_HEAD.DIFF_GROUP_1%TYPE,
                             O_diff_group_3     IN OUT DIFF_RATIO_HEAD.DIFF_GROUP_1%TYPE,
                             I_ratio_id         IN     DIFF_RATIO_DETAIL.DIFF_RATIO_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_DIFF_RATIO_HEAD_TL
-- Purpose:       This function deletes records from the diff_ratio_head_tl table for given diff_ratio_id .
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIFF_RATIO_HEAD_TL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_diff_ratio_id IN       DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END DIFF_RATIO_SQL;
/

