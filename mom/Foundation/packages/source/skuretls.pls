SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SKU_RETAIL_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------
-- Name: SKU_RETAIL_SQL
-------------------------------------------------------------------------------------------
-- Function: UPDATE_RETAIL
-- This function is called from the PM_RETAIL_API_SQL package when unit_retail is
-- changed in the itemretail.fmb form, and when the following conditions are met:
--     no reciepts of the item
--     no approved orders for the item.
-- The item can be a staple sku, style, or sellabe pack.
------------------------------------------------------------------------------------------
FUNCTION UPDATE_RETAIL(O_error_message        IN OUT   VARCHAR2,
                       I_item                 IN       ITEM_MASTER.item%TYPE,
                       I_old_unit_retail      IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                       I_new_unit_retail      IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                       I_old_selling_retail   IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                       I_new_selling_retail   IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                       I_old_selling_uom      IN       ITEM_LOC.SELLING_UOM%TYPE,
                       I_new_selling_uom      IN       ITEM_LOC.SELLING_UOM%TYPE,
                       I_multi_units          IN       ITEM_LOC.MULTI_UNITS%TYPE,
                       I_multi_unit_retail    IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                       I_multi_selling_uom    IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                       I_default_down_ind     IN       VARCHAR2,
                       I_location             IN       ITEM_LOC.LOC%TYPE,
                       I_dept                 IN       DEPS.DEPT%TYPE,
                       I_class                IN       CLASS.CLASS%TYPE,
                       I_subclass             IN       SUBCLASS.SUBCLASS%TYPE,
                       I_primary_curr         IN       CURRENCIES.CURRENCY_CODE%TYPE,
                       I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------
END SKU_RETAIL_SQL;
/