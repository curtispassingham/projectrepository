
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE RMSMFM_DIFFID AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
PROCEDURE ADDTOQ(O_status       OUT VARCHAR2,
                 O_text         OUT VARCHAR2,
                 I_message_type IN  DIFFID_MFQUEUE.MESSAGE_TYPE%TYPE,
                 I_diff_id      IN  DIFFID_MFQUEUE.DIFF_ID%TYPE,
                 I_message      IN  CLOB);
--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code   OUT  VARCHAR2,
                 O_error_msg     OUT  VARCHAR2,
                 O_message_type  OUT  VARCHAR2,
                 O_message       OUT  CLOB,
                 O_diff_id       OUT  DIFFGRP_MFQUEUE.DIFF_ID%TYPE);
--------------------------------------------------------------------------------
END RMSMFM_DIFFID;
/
