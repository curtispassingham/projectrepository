-- File Name : CORESVC_PO_TYPE_body.pls
CREATE OR REPLACE PACKAGE BODY CORESVC_PO_TYPE AS
   cursor C_SVC_PO_TYPE_TL(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_po_type_tl.rowid  AS pk_po_type_tl_rid,
             st.rowid AS st_rid,
             ptyt_pty_fk.rowid    AS ptyt_pty_fk_rid,
             st.po_type_desc,
             st.po_type,
             st.lang,
			 cd_lang.rowid as cd_lang_rid,
			 st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_po_type_tl st,
             po_type_tl pk_po_type_tl,
             po_type ptyt_pty_fk,
			 code_detail cd_lang,
			 dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.po_type         = pk_po_type_tl.po_type (+)
         and st.lang         = pk_po_type_tl.lang (+)
         and st.po_type        = ptyt_pty_fk.po_type (+)
		 and st.lang                      = cd_lang.code (+)
         and cd_lang.code_type (+)        = 'LANG'

;
   cursor C_SVC_PO_TYPE(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_po_type.rowid  AS pk_po_type_rid,
             st.rowid AS st_rid,
             st.po_type_desc,
             st.po_type,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_po_type st,
             po_type pk_po_type,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.po_type         = pk_po_type.po_type (+)
;
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E')IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
---------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   PO_TYPE_TL_cols s9t_pkg.names_map_typ;
   PO_TYPE_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                     :=s9t_pkg.get_sheet_names(I_file_id);
   PO_TYPE_TL_cols              :=s9t_pkg.get_col_names(I_file_id,PO_TYPE_TL_sheet);
   PO_TYPE_TL$Action            := PO_TYPE_TL_cols('ACTION');
   PO_TYPE_TL$PO_TYPE_DESC      := PO_TYPE_TL_cols('PO_TYPE_DESC');
   PO_TYPE_TL$PO_TYPE           := PO_TYPE_TL_cols('PO_TYPE');
   PO_TYPE_TL$LANG              := PO_TYPE_TL_cols('LANG');
   PO_TYPE_cols              	:=s9t_pkg.get_col_names(I_file_id,PO_TYPE_sheet);
   PO_TYPE$Action            	:= PO_TYPE_cols('ACTION');
   PO_TYPE$PO_TYPE_DESC         := PO_TYPE_cols('PO_TYPE_DESC');
   PO_TYPE$PO_TYPE              := PO_TYPE_cols('PO_TYPE');
END POPULATE_NAMES;
------------------------------------------------------------------------------
PROCEDURE POPULATE_PO_TYPE_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = PO_TYPE_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_PO_TYPE.action_mod ,
                           po_type_desc,
                           po_type,
                           lang
                           ))
     from po_type_tl ;
END POPULATE_PO_TYPE_TL;
------------------------------------------------------------------------------
PROCEDURE POPULATE_PO_TYPE( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = PO_TYPE_sheet )
   select s9t_row(s9t_cells(CORESVC_PO_TYPE.action_mod ,
                           po_type_desc,
                           po_type
                           ))
     from po_type ;
END POPULATE_PO_TYPE;
-----------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
   cursor C_USER_LANG is
      select lang
        from user_attrib
       where user_id = get_user;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(PO_TYPE_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(PO_TYPE_TL_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'PO_TYPE_DESC'
                                                                                            ,'PO_TYPE'
                                                                                            ,'LANG');
   L_file.add_sheet(PO_TYPE_sheet);
   L_file.sheets(l_file.get_sheet_index(PO_TYPE_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'PO_TYPE_DESC'
                                                                                            ,'PO_TYPE'
);
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
---------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_PO_TYPE.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;   
   if I_template_only_ind = 'N' then
      POPULATE_PO_TYPE_TL(O_file_id);
      POPULATE_PO_TYPE(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-----------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_PO_TYPE_TL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                  I_process_id IN   SVC_PO_TYPE_TL.process_id%TYPE) IS
   TYPE svc_PO_TYPE_TL_col_typ IS TABLE OF SVC_PO_TYPE_TL%ROWTYPE;
   L_temp_rec SVC_PO_TYPE_TL%ROWTYPE;
   svc_PO_TYPE_TL_col svc_PO_TYPE_TL_col_typ :=NEW svc_PO_TYPE_TL_col_typ();
   L_process_id SVC_PO_TYPE_TL.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_PO_TYPE_TL%ROWTYPE;
   O_error_message varchar2(500);
         L_pk_columns    VARCHAR2(255)  := ' PO_Type, Lang';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             PO_TYPE_DESC_mi,
             PO_TYPE_mi,
             LANG_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'PO_TYPE_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'PO_TYPE_DESC' AS PO_TYPE_DESC,
                                         'PO_TYPE' AS PO_TYPE,
                                         'LANG' AS LANG,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       PO_TYPE_DESC_dv,
                       PO_TYPE_dv,
                       LANG_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'PO_TYPE_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'PO_TYPE_DESC' AS PO_TYPE_DESC,
                                                      'PO_TYPE' AS PO_TYPE,
                                                      'LANG' AS LANG,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.PO_TYPE_DESC := rec.PO_TYPE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_TYPE_TL ' ,
                            NULL,
                           'PO_TYPE_DESC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.PO_TYPE := rec.PO_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_TYPE_TL ' ,
                            NULL,
                           'PO_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_TYPE_TL ' ,
                            NULL,
                           'LANG ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(PO_TYPE_TL$Action)      AS Action,
          r.get_cell(PO_TYPE_TL$PO_TYPE_DESC)              AS PO_TYPE_DESC,
          r.get_cell(PO_TYPE_TL$PO_TYPE)              AS PO_TYPE,
          r.get_cell(PO_TYPE_TL$LANG)              AS LANG,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(PO_TYPE_TL_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_TYPE_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.PO_TYPE_DESC := rec.PO_TYPE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_TYPE_TL_sheet,
                            rec.row_seq,
                            'PO_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.PO_TYPE := rec.PO_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_TYPE_TL_sheet,
                            rec.row_seq,
                            'PO_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_TYPE_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_PO_TYPE.action_new then
         L_temp_rec.PO_TYPE_DESC := NVL( L_temp_rec.PO_TYPE_DESC,L_default_rec.PO_TYPE_DESC);
         L_temp_rec.PO_TYPE := NVL( L_temp_rec.PO_TYPE,L_default_rec.PO_TYPE);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,L_default_rec.LANG);
      end if;
      if not (
            L_temp_rec.PO_TYPE is NOT NULL and
            L_temp_rec.LANG is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
PO_TYPE_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                            SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_PO_TYPE_TL_col.extend();
         svc_PO_TYPE_TL_col(svc_PO_TYPE_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_PO_TYPE_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_PO_TYPE_TL st
      using(select
                  (case
                   when l_mi_rec.PO_TYPE_DESC_mi    = 'N'
                    and svc_PO_TYPE_TL_col(i).action = CORESVC_PO_TYPE.action_mod
                    and s1.PO_TYPE_DESC IS NULL
                   then mt.PO_TYPE_DESC
                   else s1.PO_TYPE_DESC
                   end) AS PO_TYPE_DESC,
                  (case
                   when l_mi_rec.PO_TYPE_mi    = 'N'
                    and svc_PO_TYPE_TL_col(i).action = CORESVC_PO_TYPE.action_mod
                    and s1.PO_TYPE IS NULL
                   then mt.PO_TYPE
                   else s1.PO_TYPE
                   end) AS PO_TYPE,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_PO_TYPE_TL_col(i).action = CORESVC_PO_TYPE.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  null as dummy
              from (select
                          svc_PO_TYPE_TL_col(i).PO_TYPE_DESC AS PO_TYPE_DESC,
                          svc_PO_TYPE_TL_col(i).PO_TYPE AS PO_TYPE,
                          svc_PO_TYPE_TL_col(i).LANG AS LANG,
                          null as dummy
                      from dual ) s1,
            PO_TYPE_TL mt
             where
                  mt.PO_TYPE (+)     = s1.PO_TYPE   and
                  mt.LANG (+)     = s1.LANG   and
                  1 = 1 )sq
                on (
                    st.PO_TYPE      = sq.PO_TYPE and
                    st.LANG      = sq.LANG and
                    svc_PO_TYPE_TL_col(i).ACTION IN (CORESVC_PO_TYPE.action_mod,CORESVC_PO_TYPE.action_del))
      when matched then
      update
         set process_id      = svc_PO_TYPE_TL_col(i).process_id ,
             chunk_id        = svc_PO_TYPE_TL_col(i).chunk_id ,
             row_seq         = svc_PO_TYPE_TL_col(i).row_seq ,
             action          = svc_PO_TYPE_TL_col(i).action ,
             process$status  = svc_PO_TYPE_TL_col(i).process$status ,
             po_type_desc              = sq.po_type_desc ,
             create_id       = svc_PO_TYPE_TL_col(i).create_id ,
             create_datetime = svc_PO_TYPE_TL_col(i).create_datetime ,
             last_upd_id     = svc_PO_TYPE_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_PO_TYPE_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             po_type_desc ,
             po_type ,
             lang ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_PO_TYPE_TL_col(i).process_id ,
             svc_PO_TYPE_TL_col(i).chunk_id ,
             svc_PO_TYPE_TL_col(i).row_seq ,
             svc_PO_TYPE_TL_col(i).action ,
             svc_PO_TYPE_TL_col(i).process$status ,
             sq.po_type_desc ,
             sq.po_type ,
             sq.lang ,
             svc_PO_TYPE_TL_col(i).create_id ,
             svc_PO_TYPE_TL_col(i).create_datetime ,
             svc_PO_TYPE_TL_col(i).last_upd_id ,
             svc_PO_TYPE_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            PO_TYPE_TL_sheet,
                            svc_PO_TYPE_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
             
         when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              null,
                                              TO_CHAR(SQLCODE));
   END;
END PROCESS_S9T_PO_TYPE_TL;
-----------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_PO_TYPE( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_PO_TYPE.process_id%TYPE) IS
   TYPE svc_PO_TYPE_col_typ IS TABLE OF SVC_PO_TYPE%ROWTYPE;
   L_temp_rec SVC_PO_TYPE%ROWTYPE;
   svc_PO_TYPE_col svc_PO_TYPE_col_typ :=NEW svc_PO_TYPE_col_typ();
   L_process_id SVC_PO_TYPE.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_PO_TYPE%ROWTYPE;
   O_error_message varchar2(500);
      L_pk_columns    VARCHAR2(255)  := 'PO Type';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   
   cursor C_MANDATORY_IND is
      select
             PO_TYPE_DESC_mi,
             PO_TYPE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'PO_TYPE'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'PO_TYPE_DESC' AS PO_TYPE_DESC,
                                         'PO_TYPE' AS PO_TYPE,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       PO_TYPE_DESC_dv,
                       PO_TYPE_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'PO_TYPE'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'PO_TYPE_DESC' AS PO_TYPE_DESC,
                                                      'PO_TYPE' AS PO_TYPE,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.PO_TYPE_DESC := rec.PO_TYPE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_TYPE ' ,
                            NULL,
                           'PO_TYPE_DESC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.PO_TYPE := rec.PO_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_TYPE ' ,
                            NULL,
                           'PO_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(PO_TYPE$Action)      AS Action,
          r.get_cell(PO_TYPE$PO_TYPE_DESC)              AS PO_TYPE_DESC,
          r.get_cell(PO_TYPE$PO_TYPE)              AS PO_TYPE,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(PO_TYPE_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_TYPE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.PO_TYPE_DESC := rec.PO_TYPE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_TYPE_sheet,
                            rec.row_seq,
                            'PO_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.PO_TYPE := rec.PO_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_TYPE_sheet,
                            rec.row_seq,
                            'PO_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_PO_TYPE.action_new then
         L_temp_rec.PO_TYPE_DESC := NVL( L_temp_rec.PO_TYPE_DESC,L_default_rec.PO_TYPE_DESC);
         L_temp_rec.PO_TYPE := NVL( L_temp_rec.PO_TYPE,L_default_rec.PO_TYPE);
      end if;
      if not (
            L_temp_rec.PO_TYPE is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
PO_TYPE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                            SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_PO_TYPE_col.extend();
         svc_PO_TYPE_col(svc_PO_TYPE_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_PO_TYPE_col.COUNT SAVE EXCEPTIONS
      merge into SVC_PO_TYPE st
      using(select
                  (case
                   when l_mi_rec.PO_TYPE_DESC_mi    = 'N'
                    and svc_PO_TYPE_col(i).action = CORESVC_PO_TYPE.action_mod
                    and s1.PO_TYPE_DESC IS NULL
                   then mt.PO_TYPE_DESC
                   else s1.PO_TYPE_DESC
                   end) AS PO_TYPE_DESC,
                  (case
                   when l_mi_rec.PO_TYPE_mi    = 'N'
                    and svc_PO_TYPE_col(i).action = CORESVC_PO_TYPE.action_mod
                    and s1.PO_TYPE IS NULL
                   then mt.PO_TYPE
                   else s1.PO_TYPE
                   end) AS PO_TYPE,
                  null as dummy
              from (select
                          svc_PO_TYPE_col(i).PO_TYPE_DESC AS PO_TYPE_DESC,
                          svc_PO_TYPE_col(i).PO_TYPE AS PO_TYPE,
                          null as dummy
                      from dual ) s1,
            PO_TYPE mt
             where
                  mt.PO_TYPE (+)     = s1.PO_TYPE   and
                  1 = 1 )sq
                on (
                    st.PO_TYPE      = sq.PO_TYPE and
                    svc_PO_TYPE_col(i).ACTION IN (CORESVC_PO_TYPE.action_mod,CORESVC_PO_TYPE.action_del))
      when matched then
      update
         set process_id      = svc_PO_TYPE_col(i).process_id ,
             chunk_id        = svc_PO_TYPE_col(i).chunk_id ,
             row_seq         = svc_PO_TYPE_col(i).row_seq ,
             action          = svc_PO_TYPE_col(i).action ,
             process$status  = svc_PO_TYPE_col(i).process$status ,
             po_type_desc              = sq.po_type_desc ,
             create_id       = svc_PO_TYPE_col(i).create_id ,
             create_datetime = svc_PO_TYPE_col(i).create_datetime ,
             last_upd_id     = svc_PO_TYPE_col(i).last_upd_id ,
             last_upd_datetime = svc_PO_TYPE_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             po_type_desc ,
             po_type ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_PO_TYPE_col(i).process_id ,
             svc_PO_TYPE_col(i).chunk_id ,
             svc_PO_TYPE_col(i).row_seq ,
             svc_PO_TYPE_col(i).action ,
             svc_PO_TYPE_col(i).process$status ,
             sq.po_type_desc ,
             sq.po_type ,
             svc_PO_TYPE_col(i).create_id ,
             svc_PO_TYPE_col(i).create_datetime ,
             svc_PO_TYPE_col(i).last_upd_id ,
             svc_PO_TYPE_col(i).last_upd_datetime );
Exception             
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            PO_TYPE_sheet,
                            svc_PO_TYPE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              null,
                                              TO_CHAR(SQLCODE));
 
   END;
END PROCESS_S9T_PO_TYPE;
----------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_PO_TYPE.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_PO_TYPE_TL(I_file_id,I_process_id);
      PROCESS_S9T_PO_TYPE(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION EXEC_PO_TYPE_TL_INS(  L_po_type_tl_temp_rec   IN   PO_TYPE_TL%ROWTYPE,
                               O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_PO_TYPE.EXEC_PO_TYPE_TL_INS';
   L_table   VARCHAR2(255):= 'SVC_PO_TYPE_TL';
BEGIN
   insert
     into po_type_tl
   values L_po_type_tl_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_PO_TYPE_TL_INS;
------------------------------------------------------------------------------------------
FUNCTION EXEC_PO_TYPE_TL_UPD( L_po_type_tl_temp_rec   IN   PO_TYPE_TL%ROWTYPE,
                              O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_PO_TYPE.EXEC_PO_TYPE_TL_UPD';
   L_table   VARCHAR2(255):= 'SVC_PO_TYPE_TL';
BEGIN
   update po_type_tl
      set row = L_po_type_tl_temp_rec
    where 1 = 1
      and po_type = L_po_type_tl_temp_rec.po_type
      and lang = L_po_type_tl_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_PO_TYPE_TL_UPD;
-------------------------------------------------------------------------------------------
FUNCTION EXEC_PO_TYPE_TL_DEL(  L_po_type_tl_temp_rec   IN   PO_TYPE_TL%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_PO_TYPE.EXEC_PO_TYPE_TL_DEL';
   L_table   VARCHAR2(255):= 'SVC_PO_TYPE_TL';
BEGIN
   delete
     from po_type_tl
    where 1 = 1
      and po_type = L_po_type_tl_temp_rec.po_type
      and lang = L_po_type_tl_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_PO_TYPE_TL_DEL;
---------------------------------------------------------------------------------
FUNCTION PROCESS_PO_TYPE_TL( I_process_id   IN   SVC_PO_TYPE_TL.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_PO_TYPE_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_PO_TYPE.PROCESS_PO_TYPE_TL';
   L_error_message VARCHAR2(600);
   L_PO_TYPE_TL_temp_rec PO_TYPE_TL%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_PO_TYPE_TL';
BEGIN
   FOR rec IN c_svc_PO_TYPE_TL(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.lang = LP_primary_lang then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                     'ERR_PRIM_LANG',
                     'W');
        L_error:=true;      
      end if;

      if rec.action = action_new
         and rec.PK_PO_TYPE_TL_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'PO_TYPE_TL',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'PO_TYPE,LANG',
                    L_error_message);
         L_error :=TRUE;
      end if;
      
	  if rec.action IN (action_mod,action_del)
         and rec.PK_PO_TYPE_TL_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'PO_TYPE,LANG',
                    'NO_RECORD_UPD_DEL');

         L_error :=TRUE;
      end if;
       if rec.po_type is not null 
	     and rec.ptyt_pty_fk_rid is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('INV_FIELD',
                                               'PO_TYPE_TL',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'PO_TYPE',
                     L_error_message);
         L_error :=TRUE;
      end if;
    if rec.action in (action_new,action_mod)
	   and NOT(  rec.PO_TYPE_DESC  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'PO_TYPE_DESC',
                    'FIELD_IS_NULL');
         L_error :=TRUE;
      end if;
     if rec.action in (action_new)
	     and rec.lang is NOT NULL
         and rec.cd_lang_rid is NULL then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                    'LANG_EXIST');
         L_error :=TRUE;
      end if;
 	  
      if NOT L_error then
         L_po_type_tl_temp_rec.lang              := rec.lang;
         L_po_type_tl_temp_rec.po_type              := rec.po_type;
         L_po_type_tl_temp_rec.po_type_desc              := rec.po_type_desc;
         L_po_type_tl_temp_rec.create_id         := GET_USER;
		 L_po_type_tl_temp_rec.last_update_id         := GET_USER;
         L_po_type_tl_temp_rec.create_datetime         := SYSDATE;
         L_po_type_tl_temp_rec.last_update_datetime         := SYSDATE;
         if rec.action = action_new then
            if EXEC_PO_TYPE_TL_INS(   L_po_type_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_PO_TYPE_TL_UPD( L_po_type_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_PO_TYPE_TL_DEL( L_po_type_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_po_type_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_po_type_tl st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_po_type_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_PO_TYPE_TL;
FUNCTION EXEC_PO_TYPE_INS(  L_po_type_temp_rec   IN   PO_TYPE%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_PO_TYPE.EXEC_PO_TYPE_INS';
   L_table   VARCHAR2(255):= 'SVC_PO_TYPE';
BEGIN
   insert
     into po_type
   values L_po_type_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_PO_TYPE_INS;
FUNCTION EXEC_PO_TYPE_UPD( L_po_type_temp_rec   IN   PO_TYPE%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_PO_TYPE.EXEC_PO_TYPE_UPD';
   L_table   VARCHAR2(255):= 'SVC_PO_TYPE';
BEGIN
   update po_type
      set row = L_po_type_temp_rec
    where 1 = 1
      and po_type = L_po_type_temp_rec.po_type
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_PO_TYPE_UPD;
FUNCTION EXEC_PO_TYPE_DEL(  L_po_type_temp_rec   IN   PO_TYPE%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_PO_TYPE.EXEC_PO_TYPE_DEL';
   L_table   VARCHAR2(255):= 'SVC_PO_TYPE';
BEGIN
   delete
     from po_type_tl
    where 1 = 1
      and po_type = L_po_type_temp_rec.po_type
;

   delete
     from po_type
    where 1 = 1
      and po_type = L_po_type_temp_rec.po_type
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_PO_TYPE_DEL;
FUNCTION PROCESS_PO_TYPE( I_process_id   IN   SVC_PO_TYPE.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_PO_TYPE.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_PO_TYPE.PROCESS_PO_TYPE';
   L_error_message VARCHAR2(600);
   L_PO_TYPE_temp_rec PO_TYPE%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_PO_TYPE';
   L_exist varchar2(1);
   cursor c_data_exist (I_po_type po_type.po_type%type) is
      select 'x' from 
         ordhead where po_type=I_po_type
	  union all
	  select 'x' 
      from alloc_header where po_type=I_po_type;
BEGIN
   FOR rec IN c_svc_PO_TYPE(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action = action_new
         and rec.PK_PO_TYPE_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'PO_TYPE',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'PO_TYPE',
                    L_error_message);
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_PO_TYPE_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'PO_TYPE',
                    'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;
    if rec.action in (action_new,action_mod)
	   and NOT(  rec.PO_TYPE_DESC  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'PO_TYPE_DESC',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
	  if rec.action in (action_del) then
	     open c_data_exist(rec.po_type);
		 fetch c_data_exist into L_exist;
		 close c_data_exist;
		 if L_exist = 'x' then
		 	 L_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_REC',
												   'ORD_HEAD,ALLOC_HEADER',
													NULL,
													NULL);
			 WRITE_ERROR(I_process_id,
						 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
						 I_chunk_id,
						 L_table,
						 rec.row_seq,
						 'PO_TYPE',
						 L_error_message);
			 L_error :=TRUE;
         end if;         
		 
	  end if;	 
      if NOT L_error then
         L_po_type_temp_rec.po_type              := rec.po_type;
         L_po_type_temp_rec.po_type_desc              := rec.po_type_desc;
         L_po_type_temp_rec.create_id         := USER;
         L_po_type_temp_rec.create_datetime         := SYSDATE;
         if rec.action = action_new then
            if EXEC_PO_TYPE_INS(   L_po_type_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_PO_TYPE_UPD( L_po_type_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_PO_TYPE_DEL( L_po_type_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_po_type st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_po_type st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_po_type st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_PO_TYPE;
-------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete 
     from svc_po_type_tl
    where process_id=I_process_id;

   delete 
     from svc_po_type
    where process_id=I_process_id;
END;
-------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
				  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER
                  )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_USER_ATTRIB.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;   
   if PROCESS_PO_TYPE(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   commit;
   if PROCESS_PO_TYPE_TL(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   commit;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;


   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_PO_TYPE;
/
