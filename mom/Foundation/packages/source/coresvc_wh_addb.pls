CREATE OR REPLACE PACKAGE BODY CORESVC_WH_ADD_SQL AS
  ------------------------------------------------------------------------------------------------
  -- Function Name: ADD_WH
  -- Purpose      : This function contains the core logic for adding a new warehouse to RMS.
  --                The process of adding a warehouse to RMS starts with warehse.fmb form. When user
  --                creates a new wh using the form an entry is made to wh as well as wh_add table.
  --                As part of warehouse creation process, RPM tables need to be updated. So, this
  --                function calls PM_NOTIFY_API_SQL.NEW_LOCATION to create pricing records.
  --                After completion of the process, it deletes the record from wh_add table.
  ------------------------------------------------------------------------------------------------
FUNCTION ADD_WH(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_rms_async_id  IN RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
  RETURN BOOLEAN
IS
  L_program       VARCHAR2(64) := 'CORESVC_WH_ADD_SQL.ADD_WH';
  L_MVs_refreshed BOOLEAN      := FALSE;
BEGIN
  FOR rec IN
  ( WITH locs AS
  ( SELECT store AS loc FROM store
  UNION
  SELECT wh AS loc FROM wh
  )
SELECT wa.wh,
  wa.pricing_location,
  wh.stockholding_ind,
  wh.finisher_ind,
  locs.loc,
  wa.rowid
FROM wh_add wa,
  wh,
  locs
WHERE wa.rms_async_id   = I_rms_async_id
AND wa.wh               = wh.wh (+)
AND wa.pricing_location = locs.loc (+)
ORDER BY wa.wh
  )
  LOOP
    IF rec.stockholding_ind = 'Y' AND rec.finisher_ind = 'N' AND rec.pricing_location IS NOT NULL AND rec.loc IS NOT NULL THEN
      -- create pricing records in RPM tables.
      IF PM_NOTIFY_API_SQL.NEW_LOCATION(O_error_message, rec.wh, 'W', rec.pricing_location) = FALSE THEN
        RETURN false;
      END IF;
    END IF;
    -- Delete wh_add records.
    DELETE FROM wh_add WHERE wh_add.rowid = rec.rowid;
    -- Refresh the MVs. This will automatically commit changes done so far
    --
    IF SET_OF_BOOKS_SQL.REFRESH_MV_LOC_SOB(O_error_message) = FALSE THEN
      RETURN FALSE;
    END IF;
    IF L10N_SQL.REFRESH_MV_L10N_ENTITY(O_error_message) = FALSE THEN
      RETURN FALSE;
    END IF;
    IF ADDRESS_SQL.REFRESH_MV_LOC_PRIM_ADDR(O_error_message) = FALSE THEN
      RETURN FALSE;
    END IF;
    -- The save point named ASYNC was issued at the very beginning in NOTIFY_STORE_ADD
    -- Since MV refresh has committed the data hence the save point has been lost.
    -- reissue save point so that WRITE_ERROR function does not fail while trying to rollback to it.
    DBMS_STANDARD.SAVEPOINT('ASYNC'); 
    L_MVs_refreshed := TRUE;
    if STKLEDGR_SQL.STOCK_LEDGER_INSERT('W',
                                        NULL,
                                        NULL,
                                        NULL,
                                        rec.wh,
                                        O_error_message) = FALSE then
       return FALSE;
    end if;

  END LOOP;
  -- this is to handle the scenario when record got deleted from WH_ADD and
  -- during MV refresh, the deletion got commited but MV refresh failed.
  -- In that case, during re-try there won't be any record in WH_ADD for the async ID passed
  -- so, loop will not run. So, in that case, only MVs need to be refreshed.
  IF NOT L_MVs_refreshed THEN
    IF SET_OF_BOOKS_SQL.REFRESH_MV_LOC_SOB(O_error_message) = FALSE THEN
      RETURN FALSE;
    END IF;
    IF L10N_SQL.REFRESH_MV_L10N_ENTITY(O_error_message) = FALSE THEN
      RETURN FALSE;
    END IF;
    IF ADDRESS_SQL.REFRESH_MV_LOC_PRIM_ADDR(O_error_message) = FALSE THEN
      RETURN FALSE;
    END IF;
    -- The save point named ASYNC was issued at the very beginning in NOTIFY_STORE_ADD
    -- Since MV refresh has committed the data hence the save point has been lost.
    -- reissue save point so that WRITE_ERROR function does not fail while trying to rollback to it.
    DBMS_STANDARD.SAVEPOINT('ASYNC'); 
  END IF;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END Add_wh;
------------------------------------------------------------------------------------------------
END CORESVC_WH_ADD_SQL;
/
