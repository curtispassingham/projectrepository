CREATE OR REPLACE PACKAGE ITEM_SUPP_COUNTRY_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name: GET_PRIMARY_COUNTRY
--Purpose      : Get the primary country for the input item-supplier combination.
--               If there is no primary country, the exists output variable will
--               indicate this.
-------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_COUNTRY(O_error_message     IN OUT VARCHAR2,
                             O_exists            IN OUT BOOLEAN,
                             O_origin_country_id IN OUT ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                             I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                             I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
--Function Name: ALL_PACK_COMPONENTS_EXIST
--Purpose      : Check if all pack components exist on the item_supp_country table.
----------------------------------------------------------------------------------------
FUNCTION ALL_PACK_COMPONENTS_EXIST(O_error_message     IN OUT VARCHAR2,
                                   O_exists            IN OUT BOOLEAN,
                                   I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                   I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                   I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: UPDATE_CONST_DIMENSIONS
-- Purpose      : Lock and update all of the item-supplier dimension records based
--                on the input variables.  This function will update for only a specific
--                dimension if the supplier, origin country, dimension object and action
--                indicator variables have input values.  The dimension object, origin country
--                and action will be known when changing dimensions for the primary supplier
--                in the item-supplier-origin country form.
----------------------------------------------------------------------------------------
FUNCTION UPDATE_CONST_DIMENSIONS(O_error_message     IN OUT VARCHAR2,
                                 I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                 I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                 I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                                 I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE,
                                 I_default_children  IN     VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_CONST_DIMENSIONS
-- Purpose      : Insert constant dimension objects for the input item-supplier-origin country
--                combination based on the dimension objects associated with the primary
--                supplier and primary country for the input item.
----------------------------------------------------------------------------------------
FUNCTION INSERT_CONST_DIMENSIONS(O_error_message     IN OUT VARCHAR2,
                                 I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                 I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                 I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_DIMENSION_TO_CHILDREN
-- Purpose      : Default the input dimension to all child items for the input
--                item-supplier-origin country combination that do not already have
--                the dimension object.
----------------------------------------------------------------------------------------
FUNCTION INSERT_DIMENSION_TO_CHILDREN(O_error_message     IN OUT VARCHAR2,
                                      I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                      I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                      I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                                      I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: DELETE_DIMENSION_TO_CHILDREN
-- Purpose      : Default the input dimension deletion to all child items for the input
--                item-supplier-origin country combination that for the dimension object.
----------------------------------------------------------------------------------------
FUNCTION DELETE_DIMENSION_TO_CHILDREN(O_error_message     IN OUT VARCHAR2,
                                      I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                      I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                      I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                                      I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_COUNTRY_TO_CHILDREN
-- Purpose      : Default the input origin country to all child items for the input
--                item-supplier-origin country combination that do not already have
--                the dimension object.
----------------------------------------------------------------------------------------
FUNCTION INSERT_COUNTRY_TO_CHILDREN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                    I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                    I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                    I_primary_country_ind IN     VARCHAR2,
                                    I_replace_ind         IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: INSERT_COUNTRY_IND_TO_CHILDREN
-- Purpose      : This function will be called by INSERT_COUNTRY_TO_CHILDREN function.
--                It will default the input origin country to all child items for the input
--                item-supplier-origin country combination that do not already have
--                the dimension object and moreover it will set primary country indicator
--                according to the replace indicator, for the children who already have different
--                origin countries.
----------------------------------------------------------------------------------------
FUNCTION INSERT_COUNTRY_IND_TO_CHILDREN(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                        I_child_item          IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                        I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                        I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                        I_primary_country_ind IN     VARCHAR2,
                                        I_update_ind          IN     VARCHAR2,
                                        I_insert_ind          IN     VARCHAR2,
                                        I_bracket_ind         IN     VARCHAR2,
                                        I_inv_mgmt_level      IN     VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: CHECK_PRIMARY_TO_CHILDREN
-- Purpose      : Determine if there are origin countries already existing for
--                child items which are designated as primary origin countries not
--                equal to the origin country input for the parent item.  If there
--                are records which exist, then the user must decide whether to override
--                the primary origin country for the child or to insert the new origin
--                country as non-primary.
----------------------------------------------------------------------------------------
FUNCTION CHECK_PRIMARY_TO_CHILDREN(O_error_message            IN OUT VARCHAR2,
                                   O_child_primary_exists_ind IN OUT VARCHAR2,
                                   I_item                     IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                   I_supplier                 IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                   I_origin_country_id        IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: UPDATE_DIMENSION_TO_CHILDREN
-- Purpose      : Default the input origin country to all child items for the input
--                item-supplier-origin country combination that already have
--                the dimension object.
----------------------------------------------------------------------------------------
FUNCTION UPDATE_DIMENSION_TO_CHILDREN(O_error_message     IN OUT VARCHAR2,
                                      I_item              IN     ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                                      I_supplier          IN     ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                                      I_origin_country_id IN     ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                                      I_dim_object        IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE,
                                      I_insert            IN     VARCHAR2)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function name: FIRST_SUPPLIER
-- Purpose      : Called when adding the first supplier to a new item.
-----------------------------------------------------------------------------------------
FUNCTION FIRST_SUPPLIER(O_error_message      IN OUT VARCHAR2,
                        I_item               IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier           IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_unit_cost          IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                        I_ti                 IN     ITEM_SUPP_COUNTRY.TI%TYPE,
                        I_hi                 IN     ITEM_SUPP_COUNTRY.HI%TYPE,
                        I_source             IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function name: GET_PRIM_TI_HI
-- Purpose      : Called from itemloc.fmb when defaulting TI-HI information to a new location.
-----------------------------------------------------------------------------------------
FUNCTION GET_TI_HI(O_error_message OUT VARCHAR2,
                   O_ti            OUT item_supp_country.ti%TYPE,
                   O_hi            OUT item_supp_country.hi%TYPE,
                   I_item          IN item_master.item%TYPE)
  RETURN BOOLEAN;

FUNCTION GET_TI_HI(O_error_message OUT VARCHAR2,
                   O_ti            OUT item_supp_country.ti%TYPE,
                   O_hi            OUT item_supp_country.hi%TYPE,
                   I_item          IN  item_master.item%TYPE,
                   I_supplier      IN  sups.supplier%TYPE)
   RETURN BOOLEAN;

FUNCTION GET_TI_HI(O_error_message OUT VARCHAR2,
                   O_ti            OUT item_supp_country.ti%TYPE,
                   O_hi            OUT item_supp_country.hi%TYPE,
                   I_item          IN  item_master.item%TYPE,
                   I_supplier      IN  sups.supplier%TYPE,
                   I_country       IN  country.country_id%TYPE)
    RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function name: EXPENSES_EXIST
-- Purpose      : Called when entering itemexp.fmb from itemsuppctry.fmb in view mode.
-----------------------------------------------------------------------------------------
FUNCTION EXPENSES_EXIST(O_error_message     IN OUT VARCHAR2,
                        O_exists            IN OUT BOOLEAN,
                        I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
-- Function name: GET_PRIM_SUPP_CNTRY
-- Purpose      : Called from itemloc.fmb to retrieve primary supplier and country for item.
-----------------------------------------------------------------------------------------
FUNCTION GET_PRIM_SUPP_CNTRY(O_error_message OUT VARCHAR2,
                             O_prim_supp     OUT sups.supplier%TYPE,
                             O_prim_cntry    OUT country.country_id%TYPE,
                             I_item          IN  item_master.item%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function name: DEFAULT_PRIM_CASE_SIZE
-- Purpose      : Called from itemsuppctry.fmb to default case size information
--                for non-primary supplier and non-primary country for current item.
-----------------------------------------------------------------------------------------
FUNCTION DEFAULT_PRIM_CASE_SIZE(O_error_message      IN OUT VARCHAR2,
                                O_supp_pack_size     IN OUT item_supp_country.supp_pack_size%TYPE,
                                O_inner_pack_size    IN OUT item_supp_country.inner_pack_size%TYPE,
                                I_item               IN     item_supp_country.item%TYPE)

  RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function name: DEFAULT_PRIM_CASE_DIMENSIONS
-- Purpose      : Called from itemsuppctry.fmb to default dimension information
--                for non-primary supplier and non-primary country for current item.  Defaulting
--                currently only occurs for case dimension objects.
-----------------------------------------------------------------------------------------
FUNCTION DEFAULT_PRIM_CASE_DIMENSIONS(O_error_message       IN OUT VARCHAR2,
                                      I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                      I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                      I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                      O_exists              IN OUT BOOLEAN,
                                      O_dim_object          IN OUT ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE,
                                      O_presentation_method IN OUT ITEM_SUPP_COUNTRY_DIM.PRESENTATION_METHOD%TYPE,
                                      O_length              IN OUT ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE,
                                      O_width               IN OUT ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE,
                                      O_height              IN OUT ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE,
                                      O_lwh_uom             IN OUT ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                                      O_weight              IN OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE,
                                      O_net_weight          IN OUT ITEM_SUPP_COUNTRY_DIM.NET_WEIGHT%TYPE,
                                      O_weight_uom          IN OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                                      O_liquid_volume       IN OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE,
                                      O_liquid_volume_uom   IN OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE,
                                      O_stat_cube           IN OUT ITEM_SUPP_COUNTRY_DIM.STAT_CUBE%TYPE,
                                      O_tare_weight         IN OUT ITEM_SUPP_COUNTRY_DIM.TARE_WEIGHT%TYPE,
                                      O_tare_type           IN OUT ITEM_SUPP_COUNTRY_DIM.TARE_TYPE%TYPE)

  RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function name: CHECK_CASE_DIMENSION
-- Purpose      : Called from itemsuppctry.fmb.  Ensures that case dimensions are not deleted when
--                they are needed for unit of measure requirements.
-----------------------------------------------------------------------------------------
FUNCTION CHECK_CASE_DIMENSION(O_error_message      IN OUT VARCHAR2,
                              O_exists             IN OUT BOOLEAN,
                              I_item               IN     ITEM_SUPPLIER.ITEM%TYPE,
                              I_supplier           IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                              I_origin_country_id  IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)

return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function name: GET_COUNTRY_PARAMETERS
-- Purpose      : Returns the primary country indicator and case size.  If other parameters
--                are required in the future, they should be added on to this function.
-----------------------------------------------------------------------------------------
FUNCTION GET_COUNTRY_PARAMETERS(O_error_message     IN OUT VARCHAR2,
                                O_primary_ind       IN OUT ITEM_SUPP_COUNTRY.PRIMARY_COUNTRY_IND%TYPE,
                                O_supp_pack_size    IN OUT ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                                I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- Function name: UPDATE_COUNTRY_TO_CHILDREN
-- Purpose      : Update child records when origin country detail records are updated.
-----------------------------------------------------------------------------------------
FUNCTION UPDATE_COUNTRY_TO_CHILDREN(O_error_message       IN OUT VARCHAR2,
                                    I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                    I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                    I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                    I_edit_cost           IN     VARCHAR2)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function name: GET_UNIT_COST
-- Purpose      : If unit_cost is updated on item_supp_country_loc, then this function
--                will be called in order to determine if the cost is different or not
--                from that on item_supp_country.
-----------------------------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message OUT VARCHAR2,
                       O_unit_cost     OUT item_supp_country.unit_cost%TYPE,
                       I_item          IN  item_master.item%TYPE,
                       I_supplier      IN  sups.supplier%TYPE,
                       I_origin_country_id   IN  country.country_id%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function name: DELETE_CONST_DIMENSIONS
-- Purpose      : Delete dimensions for non-primary origin countries and/or suppliers if
--                the constant dimension indicator is turned on.  The dimension object will
--                be passed in as NULL if the user is changin primary countries and thus may
--                need to delete all former constant dimensions.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_CONST_DIMENSIONS(O_error_message       OUT VARCHAR2,
                                 I_item                IN  item_master.item%TYPE,
                                 I_supplier            IN  sups.supplier%TYPE,
                                 I_origin_country_id   IN  country.country_id%TYPE,
                                 I_dim_object          IN  item_supp_country_dim.dim_object%TYPE,
                                 I_delete_children     IN  VARCHAR2)
return BOOLEAN;
------------------------------------------------------------------------------------------

--Function Name: CHECK_CHILD_PRIMARY_EXISTS
--Purpose      : Check if any primary country exists for child items.
--------------------------------------------------------------------------------------------
FUNCTION CHECK_CHILD_PRIMARY_EXISTS(O_error_message            IN OUT VARCHAR2,
                                    O_exists                   IN OUT VARCHAR2,
                                    I_item                     IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                    I_supplier                 IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name: GET_DFLT_RND_LVL_ITEM
-- Purpose      : This function is used to fetch Rounding info. from the Sup. Inv. Mgmt. table,
--                for defaulting purposes.  If Sup. Inv. Mgmt. info. isn't present, System defaults
--                are employed.
--
--                The Rounding info. is determined as follows:
--                   If Location is passed in and Inv. Mgmt. Lvl. is Supp./Loc. or Supp./Dep./Loc,
--                   fetch Sup. Inv. Mgmt.-level values for the passed in Supp., Loc. and Dept. (if any).
--
--                   If Location is passed in and Inv. Mgmt. Lvl. is Supp. or Supp./Dep., or SIM-level info.
--                   isn't found as above, fetch Item Supp. Country-level values for the passed-in Item,
--                   Supplier and Origin Country.
--
--                   If Location isn't passed in and Inv. Mgmt. Lvl. is Supp./Dep. or Supp./Dep./Loc.,
--                   fetch SIM-level values for the passed-in Supplier and Department.
--
--                   If Location isn't passed in and Inv. Mgmt. Lvl. is Supp./Loc. or Supp., or SIM-level
--                   values aren't found as above, fetch SIM-level values for the passed-in Supplier.
--
--                   If values aren't found as above, fetch System-level values.
--
---------------------------------------------------------------------------------------------
FUNCTION GET_DFLT_RND_LVL_ITEM (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_round_level     IN OUT  SUP_INV_MGMT.ROUND_LVL%TYPE,
                                O_to_inner_pct    IN OUT  SUP_INV_MGMT.ROUND_TO_INNER_PCT%TYPE,
                                O_to_case_pct     IN OUT  SUP_INV_MGMT.ROUND_TO_CASE_PCT%TYPE,
                                O_to_layer_pct    IN OUT  SUP_INV_MGMT.ROUND_TO_LAYER_PCT%TYPE,
                                O_to_pallet_pct   IN OUT  SUP_INV_MGMT.ROUND_TO_PALLET_PCT%TYPE,
                                I_item            IN      ITEM_MASTER.ITEM%TYPE,
                                I_supplier        IN      SUP_INV_MGMT.SUPPLIER%TYPE,
                                I_dept            IN      SUP_INV_MGMT.DEPT%TYPE,
                                I_origin_country  IN      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                I_location        IN      ITEM_LOC.LOC%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_COUNTRY_TO_COMP_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_pack_no           IN     ITEM_MASTER.ITEM%TYPE,
                                     I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                     I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                     I_pack_cost         IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                     I_ti                IN     ITEM_SUPP_COUNTRY.TI%TYPE,
                                     I_hi                IN     ITEM_SUPP_COUNTRY.HI%TYPE,
                                     I_uom               IN     ITEM_SUPP_COUNTRY.COST_UOM%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_EXP_TO_COMP_ITEM(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_pack_no           IN     ITEM_MASTER.ITEM%TYPE,
                                 I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                 I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_UOP(O_error_message     IN OUT VARCHAR2,
                         O_default_uop       IN OUT ITEM_SUPP_COUNTRY.DEFAULT_UOP%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------------
--Function Name: INSERT_ITEM_SUPP_COUNTRY
--Purpose      : Does a 1 row insert into item_supp_country.
---------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPP_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_isc_rec       IN ITEM_SUPP_COUNTRY%ROWTYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function: UPDATE_YES_PRIM_IND
--  Purpose: Updates primary_country_ind to Y for 1 row in the item_supp_country table.
--           NOTE:  only used for XITEM subscription API after UPDATE_PRIMARY_INDICATORS is called
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_YES_PRIM_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_isc_rec       IN ITEM_SUPP_COUNTRY%ROWTYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------
--Function Name: DELETE_ITEM_SUPP_COUNTRY
--Purpose      : Deletes 1 row from item_supp_country.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_SUPP_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_item          IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                  I_supplier      IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                  I_country       IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION CONVERT_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_unit_cost          IN OUT   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                      I_item                IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                      I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                      I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                      I_cost_type           IN       VARCHAR2,
                      I_from_uom             IN      UOM_CONVERSION.FROM_UOM%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_ROW
-- Purpose      : Retrieves the item_supplier_country row for a specific item-supplier-country
--                combination.
----------------------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists               IN OUT   BOOLEAN,
                 O_item_supp_country    IN OUT   ITEM_SUPP_COUNTRY%ROWTYPE,
                 I_item                 IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                 I_supplier             IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                 I_origin_country_id    IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: GET_PRIMARY_LOC_ROW
-- Purpose      : Gets the primary row of catch weight item records in item_supp_country_dim
-- that meet the specified requirements.
----------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_LOC_ROW(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists                  IN OUT   BOOLEAN,
                             O_item_supp_country_dim   IN OUT   ITEM_SUPP_COUNTRY_DIM%ROWTYPE,
                             I_item                    IN       ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                             I_loc                     IN       ITEM_LOC.LOC%TYPE,
                             I_dim_object              IN       ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
FUNCTION CONVERT_COST(I_unit_cost           IN   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                      I_item                IN   ITEM_SUPP_COUNTRY.ITEM%TYPE,
                      I_supplier            IN   ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                      I_origin_country_id   IN   ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                      I_cost_type           IN   VARCHAR2)
   RETURN NUMBER;
------------------------------------------------------------------------------------------
--Function Name: GET_COST_UOM
--Purpose      : Retrieves the cost unit of measure (cuom) from item_supp_country
--               for the primary supplier/primary country.  Used by various catch
--               weight processing functions.
----------------------------------------------------------------------------------
FUNCTION GET_COST_UOM(O_error_message IN OUT VARCHAR2,
                      O_cuom          IN OUT ITEM_SUPP_COUNTRY.COST_UOM%TYPE,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name: CONVERT_COST
-- Purpose      : An overloaded function that converts a passed in cost (whether unit or case)
--                from the standard UOM to the cost UOM or vise versa.
--                This function will first determine if the cost and the standard UOM belong
--                to the same class, in which case, the UOM_SQL.WITHIN_CLASS function will be
--                used to retrieve the conversion factor.  Otherwise, the retrieval of the
--                conversion factor will involve converting the standard UOM to the supplier
--                pack size, then to the dimension UOM and then finally to the cost UOM.
-- Called by    : ITEMSUPPCTRY.FMB
----------------------------------------------------------------------------------------------
FUNCTION CONVERT_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_unit_cost          IN OUT   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                      I_item                IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                      I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                      I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                      I_cost_type           IN       VARCHAR2,
                      I_from_uom            IN       UOM_CONVERSION.FROM_UOM%TYPE DEFAULT NULL,
                      I_cost_uom            IN       ITEM_SUPP_COUNTRY.COST_UOM%TYPE,
                      I_supp_pack_size      IN       ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--Function Name: INSERT_ISC_DIM
--Purpose      : Inserts 1 row into item_supp_country_dim.
---------------------------------------------------------------------------------------------------
FUNCTION INSERT_ISC_DIM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_iscd_rec        IN       ITEM_SUPP_COUNTRY_DIM%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--Function Name: UPDATE_ISC_DIM
--Purpose      : Updates 1 row on item_supp_country_dim.
---------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ISC_DIM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_iscd_rec        IN       ITEM_SUPP_COUNTRY_DIM%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
--Function Name: DELETE_ISC_DIM
--Purpose      : Deletes 1 row from item_supp_country_dim.
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_ISC_DIM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item            IN       ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE,
                        I_supplier        IN       ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE,
                        I_country         IN       ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE,
                        I_dim_object      IN       ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function: UPDATE_ISC
--  Purpose: Updates the fields in the item_supp_country table for the given item/supplier/country.
--           NOTE:  only used for XITEM subscription API
---------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_isc_rec         IN       ITEM_SUPP_COUNTRY%ROWTYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function Name: LOCK_ITEM_SUPP_COUNTRY
-- Purpose      : This function will lock the ITEM_SUPP_COUNTRY table for update or delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_SUPP_COUNTRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item          IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                I_supplier      IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                I_country       IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name : GET_AVERAGE_WEIGHT
-- Purpose       : This function has been added as part of ADF conversion to move logic from forms to 
--                 package. This is called from item location screen to calculate average weight for 
--                 catchweight items at warehouse location. 
------------------------------------------------------------------------------------------------------
FUNCTION GET_AVERAGE_WEIGHT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_average_weight      IN OUT ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE,
                            I_item                IN     ITEM_LOC.ITEM%TYPE,
                            I_loc                 IN     ITEM_LOC.LOC%TYPE,
                            I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                            I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                            I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,                            
                            I_dim_object          IN     ITEM_SUPP_COUNTRY_DIM.DIM_OBJECT%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
END ITEM_SUPP_COUNTRY_SQL;
/
