CREATE OR REPLACE PACKAGE BODY ORGANIZATION_SQL AS
---------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Function Name: LOCK_CHAIN_TL
-- Purpose      : This function will lock the CHAIN_TL table for delete.
---------------------------------------------------------------------

FUNCTION LOCK_CHAIN_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_chain         IN     CHAIN.CHAIN%TYPE)
  RETURN BOOLEAN;

---------------------------------------------------------------------
-- Function Name: LOCK_CHAIN
-- Purpose      : This function will lock the CHAIN table for update or delete.
---------------------------------------------------------------------

FUNCTION LOCK_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_chain         IN     CHAIN.CHAIN%TYPE)
  RETURN BOOLEAN;

---------------------------------------------------------------------
-- Function Name: LOCK_AREA_TL
-- Purpose      : This function will lock the AREA_TL table for delete.
---------------------------------------------------------------------

FUNCTION LOCK_AREA_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_area          IN     AREA.AREA%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------
-- Function Name: LOCK_AREA
-- Purpose      : This function will lock the AREA table for update or delete.
---------------------------------------------------------------------

FUNCTION LOCK_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_area          IN     AREA.AREA%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------
-- Function Name: LOCK_REGION_TL
-- Purpose      : This function will lock the REGION_TL table for delete.
---------------------------------------------------------------------

FUNCTION LOCK_REGION_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_region        IN     REGION.REGION%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------
-- Function Name: LOCK_REGION
-- Purpose      : This function will lock the REGION table for update or delete.
---------------------------------------------------------------------

FUNCTION LOCK_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_region        IN     REGION.REGION%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: LOCK_DISTRICT_TL
-- Purpose      : This function will lock the DISTRICT_TL table for delete.
---------------------------------------------------------------------

FUNCTION LOCK_DISTRICT_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_district      IN     DISTRICT.DISTRICT%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------
-- Function Name: LOCK_DISTRICT
-- Purpose      : This function will lock the DISTRICT table for update or delete.
---------------------------------------------------------------------

FUNCTION LOCK_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_district      IN     DISTRICT.DISTRICT%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------
-- Function Name: STORE_COUNTRY_SUBQUERY
-- Purpose      : This function will add the country id to a store
--                dynamic query.  It will create a subquery that references 
--                the ADDR and ADD_TYPE_MODULE tables.
---------------------------------------------------------------------

FUNCTION STORE_COUNTRY_SUBQUERY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_subquery           IN OUT VARCHAR2,
                                I_country_bind_no     IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------


---------------------------------------------------------------------
-- PUBLIC FUNCTIONS
---------------------------------------------------------------------
------------------------------------------------------------------------------------------------
FUNCTION ORG_HIER_SUBQUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_subquery        OUT    VARCHAR2,
                           I_bind_no         IN     VARCHAR2,
                           I_hier_level      IN     VARCHAR2,
                           I_hier_tbl        IN     LOC_TBL,
                           I_main_query_tab  IN     VARCHAR2,
                           I_bind_no_sup     IN     VARCHAR2   DEFAULT NULL)
RETURN  BOOLEAN IS

   L_function        VARCHAR2(60) := 'ORGANIZATION_SQL.ORG_HIER_SUBQUERY';
   L_hier_value_cnt  NUMBER := 0;

   L_subquery        VARCHAR2(2000) := ' select s.store from ';

BEGIN

   if I_hier_tbl is NULL then
      L_hier_value_cnt := 1;
   else
      L_hier_value_cnt := I_hier_tbl.COUNT;
   end if;

   if I_hier_level = ORGANIZATION_SQL.LP_chain then
      if L_hier_value_cnt = 1 then
         L_subquery := L_subquery ||   ' area a, region r, district d, store s ' ||
                                     ' where a.area = r.area ' ||
                                     '   and r.region = d.region  ' ||
                                     '   and d.district = s.district ' ||
                                     '   and a.chain = :'||I_bind_no;
      else
         L_subquery := L_subquery || ' area a, region r, district d, store s, ' ||
                                     ' TABLE (cast(:'||I_bind_no ||' AS loc_tbl)) xloc ' ||
                                     ' where a.area = r.area ' ||
                                     '   and r.region = d.region  ' ||
                                     '   and d.district = s.district ' ||
                                     '   and a.chain = value(xloc)';
      end if;
      --
      if I_main_query_tab IS NOT NULL then
         L_subquery := L_subquery || '   and s.store = '||I_main_query_tab || ' ';
      end if;

   elsif I_hier_level = ORGANIZATION_SQL.LP_area then
      if L_hier_value_cnt = 1 then
         L_subquery := L_subquery || ' region r, district d, store s ' ||
                                     '  where r.region = d.region  ' ||
                                     '    and d.district = s.district ' ||
                                     '    and r.area = :'||I_bind_no;
      else
         L_subquery := L_subquery || ' region r, district d, store s, ' ||
                                     ' TABLE (cast(:'||I_bind_no ||' AS loc_tbl)) xloc ' ||
                                     ' where r.region = d.region  ' ||
                                     '   and d.district = s.district ' ||
                                     '   and r.area = value(xloc)';
      end if;
      --
      if I_main_query_tab IS NOT NULL then
         L_subquery := L_subquery || '  and s.store = '||I_main_query_tab || ' ';
      end if;

   elsif I_hier_level = ORGANIZATION_SQL.LP_region then
      if L_hier_value_cnt = 1 then
         L_subquery := L_subquery || ' district d, store s ' ||
                                     '  where d.district = s.district ' ||
                                     '   and d.region = :'||I_bind_no;
      else
         L_subquery := L_subquery || ' district d, store s, ' ||
                                     ' TABLE (cast(:'||I_bind_no ||' AS loc_tbl)) xloc ' ||
                                     ' where d.district = s.district ' ||
                                     '   and d.region = value(xloc)';
      end if;
      --
      if I_main_query_tab IS NOT NULL then
         L_subquery := L_subquery || '  and s.store = '||I_main_query_tab || ' ';
      end if;

   elsif I_hier_level = ORGANIZATION_SQL.LP_district then
      if L_hier_value_cnt = 1 then
         L_subquery := L_subquery || ' store s ' ||
                                     ' where s.district = :'||I_bind_no;
      else
         L_subquery := L_subquery || ' store s, ' ||
                                     ' TABLE (cast(:'||I_bind_no ||' AS loc_tbl)) xloc ' ||
                                     ' where s.district = value(xloc)';
      end if;
      --
      if I_main_query_tab IS NOT NULL then
         L_subquery := L_subquery || '  and s.store = '||I_main_query_tab || ' ';
      end if;

   elsif I_hier_level = ORGANIZATION_SQL.LP_store then
      if L_hier_value_cnt = 1 then
         L_subquery := L_subquery || '  store s ' ||
                                     '  where s.store = :'||I_bind_no;
      else
         L_subquery := L_subquery || '  store s, ' ||
                                     ' TABLE (cast(:'||I_bind_no ||' AS loc_tbl)) xloc ' ||
                                     '  where s.store = value(xloc)';
      end if;
      --
      if I_main_query_tab IS NOT NULL then
         L_subquery := L_subquery || '  and s.store = '||I_main_query_tab || ' ';
      end if;
   end if;
   ---
   if I_bind_no_sup is NOT NULL then
      L_subquery := L_subquery || '  and exists (select 1 from partner_org_unit pou '||
                                                 'where pou.partner = :'||I_bind_no_sup||
                                                 '  and pou.partner_type in (''S'',''U'')'||
                                                 '  and ((pou.org_unit_id = s.org_unit_id) or'||
                                                 '       exists (select ''x'' '||
                                                 '                 from sups_imp_exp sie'||
                                                 '                where sie.supplier = pou.partner'||
                                                 '                  and rownum = 1))) ';
   end if;
   --
   O_subquery := L_subquery;

   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ORG_HIER_SUBQUERY;
------------------------------------------------------------------------------------------------
FUNCTION ORG_HIER_SUBQUERY(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_subquery           OUT    VARCHAR2,
                           I_bind_no            IN     VARCHAR2,
                           I_hier_level         IN     VARCHAR2,
                           I_hier_tbl           IN     LOC_TBL,
                           I_main_query_tab     IN     VARCHAR2,
                           I_currency_bind_no   IN     VARCHAR2,
                           I_country_bind_no    IN     VARCHAR2,
                           I_store_list_bind_no IN     VARCHAR2)
RETURN  BOOLEAN IS

   L_function        VARCHAR2(60) := 'ORGANIZATION_SQL.ORG_HIER_SUBQUERY';
   L_hier_value_cnt  NUMBER := 0;

   L_subquery        VARCHAR2(2000);

BEGIN

   if ORG_HIER_SUBQUERY(O_error_message,
                        L_subquery,
                        I_bind_no,
                        I_hier_level,
                        I_hier_tbl,
                        I_main_query_tab) = FALSE then
      return FALSE;
   end if;

   if I_currency_bind_no is NOT NULL then
      L_subquery := L_subquery || '  and s.currency_code = :'||I_currency_bind_no || ' ';
   end if;
   --
   if I_country_bind_no is NOT NULL then
      if STORE_COUNTRY_SUBQUERY(O_error_message,
                                L_subquery,
                                I_country_bind_no) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if I_store_list_bind_no is NOT NULL then
      L_subquery := L_subquery || '  and s.store not in '||
                                  '(select * from TABLE (cast(:'||I_store_list_bind_no ||' AS loc_tbl))) ';
   end if;
   --
   -- order by stores for performance
   L_subquery := L_subquery || ' order by s.store ';

   O_subquery := L_subquery;

   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ORG_HIER_SUBQUERY;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.INSERT_CHAIN';

BEGIN
   SQL_LIB.SET_MARK('INSERT', NULL, 'CHAIN', 'chain: '||I_orghier_rec.hier_value);
   insert into chain(chain,
                     chain_name,
                     mgr_name,
                     currency_code)
              values(I_orghier_rec.hier_value,
                     I_orghier_rec.hier_desc,
                     I_orghier_rec.mgr_name,
                     I_orghier_rec.currency_code);
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_INSERT_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_CHAIN;
---------------------------------------------------------------------
FUNCTION UPDATE_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.UPDATE_CHAIN';

BEGIN
   if not LOCK_CHAIN(O_error_message,
                     I_orghier_rec.hier_value) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'CHAIN', 'chain: '||I_orghier_rec.hier_value);
   update chain
      set chain_name = I_orghier_rec.hier_desc,
          mgr_name = I_orghier_rec.mgr_name,
          currency_code = I_orghier_rec.currency_code
    where chain = I_orghier_rec.hier_value;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_CHAIN;
---------------------------------------------------------------------
FUNCTION DELETE_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.DELETE_CHAIN';

BEGIN
   if not LOCK_CHAIN_TL(O_error_message,
                     I_orghier_rec.hier_value) then
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('DELETE', NULL, 'CHAIN_TL', 'chain: '||I_orghier_rec.hier_value);
   delete from chain_tl
    where chain = I_orghier_rec.hier_value;
   ---
   if not LOCK_CHAIN(O_error_message,
                     I_orghier_rec.hier_value) then
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('DELETE', NULL, 'CHAIN', 'chain: '||I_orghier_rec.hier_value);
   delete from chain
    where chain = I_orghier_rec.hier_value;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_CHAIN;
--------------------------------------------------------------------
FUNCTION INSERT_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.INSERT_AREA';

BEGIN
   SQL_LIB.SET_MARK('INSERT', NULL, 'AREA', 'area: '||I_orghier_rec.hier_value);
   insert into area(area,
                    area_name,
                    chain,
                    mgr_name,
                    currency_code)
             values(I_orghier_rec.hier_value,
                    I_orghier_rec.hier_desc,
                    I_orghier_rec.parent_id,
                    I_orghier_rec.mgr_name,
                    I_orghier_rec.currency_code);
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_INSERT_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_AREA;
---------------------------------------------------------------------
FUNCTION UPDATE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.UPDATE_AREA';

BEGIN
   if not LOCK_AREA(O_error_message,
                    I_orghier_rec.hier_value) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'AREA', 'area: '||I_orghier_rec.hier_value);
   update area
      set area_name = I_orghier_rec.hier_desc,
          chain = I_orghier_rec.parent_id,
          mgr_name = I_orghier_rec.mgr_name,
          currency_code = I_orghier_rec.currency_code
    where area = I_orghier_rec.hier_value;

   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   if NOT LOCK_STORE_HIERARCHY(O_error_message,
                               LP_area,
                               I_orghier_rec.hier_value) then
      return FALSE;
   end if;
   ---
   update store_hierarchy
      set chain = I_orghier_rec.parent_id
    where area = I_orghier_rec.hier_value
      and chain != I_orghier_rec.parent_id;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_AREA;
---------------------------------------------------------------------
FUNCTION DELETE_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.DELETE_AREA';

BEGIN

   if not LOCK_AREA_TL(O_error_message,
                       I_orghier_rec.hier_value) then
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('DELETE', NULL, 'AREA_TL', 'area: '||I_orghier_rec.hier_value);
   delete from area_tl
    where area = I_orghier_rec.hier_value;
   ---
   
   if not LOCK_AREA(O_error_message,
                    I_orghier_rec.hier_value) then
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('DELETE', NULL, 'AREA', 'area: '||I_orghier_rec.hier_value);
   delete from area
    where area = I_orghier_rec.hier_value;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_AREA;
--------------------------------------------------------------------
   -- Function    : INSERT_REGION
   -- Purpose     : Takes in a region table record and inserts all of
   --               its contents into the REGION table.
---------------------------------------------------------------------
FUNCTION INSERT_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.INSERT_REGION';

BEGIN
   SQL_LIB.SET_MARK('INSERT', NULL, 'REGION', 'region: '||I_orghier_rec.hier_value);
   insert into region(region,
                      region_name,
                      area,
                      mgr_name,
                      currency_code)
               values(I_orghier_rec.hier_value,
                      I_orghier_rec.Hier_desc,
                      I_orghier_rec.Parent_id,
                      I_orghier_rec.Mgr_name,
                      I_orghier_rec.Currency_code);
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_INSERT_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_REGION;
---------------------------------------------------------------------
FUNCTION UPDATE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.UPDATE_REGION';

BEGIN
   if not LOCK_REGION(O_error_message,
                      I_orghier_rec.hier_value) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'REGION', 'region: '||I_orghier_rec.hier_value);
   update region
      set region_name = I_orghier_rec.hier_desc,
          area = I_orghier_rec.parent_id,
          mgr_name = I_orghier_rec.mgr_name,
          currency_code = I_orghier_rec.currency_code
    where region = I_orghier_rec.hier_value;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   if NOT LOCK_STORE_HIERARCHY(O_error_message,
                               LP_region,
                               I_orghier_rec.hier_value) then
      return FALSE;
   end if;
   ---
   update store_hierarchy
      set area   = I_orghier_rec.parent_id,
          chain  = (select chain
                      from area
                     where area = I_orghier_rec.parent_id)
    where region = I_orghier_rec.hier_value
      and area != I_orghier_rec.parent_id;

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_REGION;
---------------------------------------------------------------------
FUNCTION DELETE_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.DELETE_REGION';

BEGIN

   if not LOCK_REGION_TL(O_error_message,
                      I_orghier_rec.hier_value) then
      return FALSE;
   end if;
    
   SQL_LIB.SET_MARK('DELETE', NULL, 'REGION_TL', 'region: '||I_orghier_rec.hier_value);
   delete from region_tl
    where region = I_orghier_rec.hier_value;
   ---
   if not LOCK_REGION(O_error_message,
                      I_orghier_rec.hier_value) then
      return FALSE;
   end if;
    
   SQL_LIB.SET_MARK('DELETE', NULL, 'REGION', 'region: '||I_orghier_rec.hier_value);
   delete from region
    where region = I_orghier_rec.hier_value;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_REGION;
--------------------------------------------------------------------
   -- Function    : INSERT_DISTRICT
   -- Purpose     : Takes in a district table record and inserts all of
   --               its contents into the DISTRICT table.
---------------------------------------------------------------------
FUNCTION INSERT_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.INSERT_DISTRICT';

BEGIN
   SQL_LIB.SET_MARK('INSERT', NULL, 'DISTRICT', 'district: '||I_orghier_rec.hier_value);
   insert into district(district,
                        district_name,
                        region,
                        mgr_name,
                        currency_code)
                 values(I_orghier_rec.hier_value,
                        I_orghier_rec.Hier_desc,
                        I_orghier_rec.Parent_id,
                        I_orghier_rec.Mgr_name,
                        I_orghier_rec.Currency_code);
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_INSERT_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_DISTRICT;
---------------------------------------------------------------------
FUNCTION UPDATE_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.UPDATE_DISTRICT';

BEGIN
   if not LOCK_DISTRICT(O_error_message,
                        I_orghier_rec.hier_value) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'DISTRICT', 'district: '||I_orghier_rec.hier_value);
   update district
      set district_name = I_orghier_rec.Hier_desc,
          region = I_orghier_rec.Parent_id,
          mgr_name = I_orghier_rec.Mgr_name,
          currency_code = I_orghier_rec.Currency_code
    where district = I_orghier_rec.hier_value;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   if NOT LOCK_STORE_HIERARCHY(O_error_message,
                               LP_district,
                               I_orghier_rec.hier_value) then
      return FALSE;
   end if;
   ---
   update store_hierarchy
      set region = I_orghier_rec.parent_id,
          area   = (select area
                      from region
                     where region = I_orghier_rec.parent_id),
          chain  = (select area.chain
                      from area,
                           region
                       where area.area = region.area
                         and region.region = I_orghier_rec.parent_id)
    where district = I_orghier_rec.hier_value
      and region != I_orghier_rec.parent_id;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_DISTRICT;
---------------------------------------------------------------------
FUNCTION DELETE_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_orghier_rec   IN     ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.DELETE_DISTRICT';

BEGIN

   if not LOCK_DISTRICT_TL(O_error_message,
                        I_orghier_rec.hier_value) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE', NULL, 'DISTRICT_TL', 'district: '||I_orghier_rec.hier_value);
   delete from district_tl
    where district = I_orghier_rec.hier_value;
   ---
   if not LOCK_DISTRICT(O_error_message,
                        I_orghier_rec.hier_value) then
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('DELETE', NULL, 'DISTRICT', 'district: '||I_orghier_rec.hier_value);
   delete from district
    where district = I_orghier_rec.hier_value;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DISTRICT;
---------------------------------------------------------------------
FUNCTION LOCK_STORE_HIERARCHY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hier_level    IN     VARCHAR2,
                              I_hier_value    IN     NUMBER)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_STORE_HIERARCHY';
   L_table        VARCHAR2(20) := 'STORE_HIERARCHY';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_STORE_HIERARCHY IS
      select 'x'
        from store_hierarchy
       where DECODE(I_hier_level,
             ORGANIZATION_SQL.LP_area, area,
             ORGANIZATION_SQL.LP_region, region,
             ORGANIZATION_SQL.LP_district, district,
             ORGANIZATION_SQL.LP_store, store) = I_hier_value
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_STORE_HIERARCHY',
                    'STORE_HIERARCHY',
                    'Hier Level: ' || I_hier_level || 'Hier Value: '|| I_hier_value);
   open C_LOCK_STORE_HIERARCHY;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_STORE_HIERARCHY',
                    'STORE_HIERARCHY',
                    'Hier Level: ' || I_hier_level || ' Hier Value: '|| I_hier_value);
   close C_LOCK_STORE_HIERARCHY;

   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'Hier Level: ' || I_hier_level ,
                                            'Hier Value: ' || I_hier_value);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_STORE_HIERARCHY;
---------------------------------------------------------------------
FUNCTION LOCK_CHAIN_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_chain         IN     CHAIN.CHAIN%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_CHAIN_TL';
   L_table        VARCHAR2(50)  := 'CHAIN_TL';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_CHAIN_TL IS
      select 'x'
        from chain_tl
       where chain = I_chain
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_CHAIN_TL', 'CHAIN_TL', 'chain: '||I_chain);
   open C_LOCK_CHAIN_TL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_CHAIN_TL', 'CHAIN_TL', 'chain: '||I_chain);
   close C_LOCK_CHAIN_TL;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'chain ' || I_chain,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_CHAIN_TL;
---------------------------------------------------------------------
FUNCTION LOCK_CHAIN(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_chain         IN     CHAIN.CHAIN%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_CHAIN';
   L_table        VARCHAR2(10)  := 'CHAIN';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_CHAIN IS
      select 'x'
        from chain
       where chain = I_chain
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_CHAIN', 'CHAIN', 'chain: '||I_chain);
   open C_LOCK_CHAIN;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_CHAIN', 'CHAIN', 'chain: '||I_chain);
   close C_LOCK_CHAIN;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'chain ' || I_chain,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_CHAIN;
---------------------------------------------------------------------
FUNCTION LOCK_AREA_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_area          IN     AREA.AREA%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_AREA_TL';
   L_table        VARCHAR2(50)  := 'AREA_tl';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_AREA_TL IS
      select 'x'
        from area_tl
       where area = I_area
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_AREA_TL', 'AREA_TL', 'area: '||I_area);
   open C_LOCK_AREA_TL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_AREA_TL', 'AREA_TL', 'area: '||I_area);
   close C_LOCK_AREA_TL;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'area ' || I_area,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_AREA_TL;
---------------------------------------------------------------------
FUNCTION LOCK_AREA(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_area          IN     AREA.AREA%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_AREA';
   L_table        VARCHAR2(10)  := 'AREA';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_AREA IS
      select 'x'
        from area
       where area = I_area
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_AREA', 'AREA', 'area: '||I_area);
   open C_LOCK_AREA;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_AREA', 'AREA', 'area: '||I_area);
   close C_LOCK_AREA;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'area ' || I_area,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_AREA;
---------------------------------------------------------------------
FUNCTION LOCK_REGION_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_region        IN     REGION.REGION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_REGION_TL';
   L_table        VARCHAR2(50)  := 'REGION_TL';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_REGION_TL IS
      select 'x'
        from region_tl
       where region = I_region
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_REGION_TL', 'REGION_TL', 'region: '||I_region);
   open C_LOCK_REGION_TL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_REGION_TL', 'REGION_TL', 'region: '||I_region);
   close C_LOCK_REGION_TL;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'region ' || I_region,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_REGION_TL;
---------------------------------------------------------------------
FUNCTION LOCK_REGION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_region        IN     REGION.REGION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_REGION';
   L_table        VARCHAR2(10)  := 'REGION';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_REGION IS
      select 'x'
        from REGION
       where region = I_region
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_REGION', 'REGION', 'region: '||I_region);
   open C_LOCK_REGION;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_REGION', 'REGION', 'region: '||I_region);
   close C_LOCK_REGION;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'region ' || I_region,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_REGION;
---------------------------------------------------------------------
FUNCTION LOCK_DISTRICT_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_district      IN     DISTRICT.DISTRICT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_DISTRICT_TL';
   L_table        VARCHAR2(50) := 'DISTRICT_TL';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_DISTRICT_TL IS
      select 'x'
        from district_tl
       where district = I_district
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_DISTRICT_TL', 'DISTRICT_TL', 'district: '||I_district);
   open C_LOCK_DISTRICT_TL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DISTRICT_TL', 'DISTRICT_TL', 'district: '||I_district);
   close C_LOCK_DISTRICT_TL;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'district ' || I_district,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_DISTRICT_TL;
---------------------------------------------------------------------
FUNCTION LOCK_DISTRICT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_district      IN     DISTRICT.DISTRICT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.LOCK_DISTRICT';
   L_table        VARCHAR2(10)  := 'DISTRICT';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_DISTRICT IS
      select 'x'
        from district
       where district = I_district
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_DISTRICT', 'DISTRCIT', 'district: '||I_district);
   open C_LOCK_DISTRICT;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_DISTRICT', 'DISTRICT', 'district: '||I_district);
   close C_LOCK_DISTRICT;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'district ' || I_district,
                                            NULL);
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_DISTRICT;
---------------------------------------------------------------------
FUNCTION VALIDATE_IDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists             OUT   BOOLEAN,
                      I_hier_level      IN       VARCHAR2,
                      I_hier_tbl        IN       LOC_TBL)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'ORGANIZATION_SQL.VALIDATE_IDS';

   L_hier_table_name    VARCHAR2(10);
   L_hier_column_name   VARCHAR2(10);
   L_count              NUMBER(25);
   L_query              VARCHAR2(1000);

BEGIN

   if I_hier_level = LP_chain then
      L_hier_table_name  := 'chain';
      L_hier_column_name := 'chain';
   elsif I_hier_level = LP_area then
      L_hier_table_name  := 'area';
      L_hier_column_name := 'area';
   elsif I_hier_level = LP_region then
      L_hier_table_name  := 'region';
      L_hier_column_name := 'region';
   elsif I_hier_level = LP_district then
      L_hier_table_name  := 'district';
      L_hier_column_name := 'district';
   elsif I_hier_level = LP_store then
      L_hier_table_name  := 'store';
      L_hier_column_name := 'store';
   end if;

   L_query := 'select count(*)' ||
               ' from ' || L_hier_table_name ||
              ' where ' || L_hier_column_name  || ' in (select * ' ||
                                                 ' from TABLE(cast(:1 AS loc_tbl)))';

   EXECUTE IMMEDIATE L_query
      INTO L_count
     USING I_hier_tbl;

   if L_count = I_hier_tbl.COUNT then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END VALIDATE_IDS;
------------------------------------------------------------------------------------------------
FUNCTION WH_COUNTRY_SUBQUERY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_subquery           IN OUT VARCHAR2,
                             I_country_bind_no     IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'ORGANIZATION_SQL.WH_COUNTRY_SUBQUERY';
         
BEGIN

   IO_subquery := IO_subquery||' and exists (select ''x'' '||
                               '               from addr, '||
                               '                    add_type_module atm '||
                               '              where addr.country_id = :'||I_country_bind_no||
                               '                and addr.module = ''WH'' '||
                               '                and addr.primary_addr_ind = ''Y'' '||
                               '                and addr.key_value_1 = wh.physical_wh '||
                               '                and addr.addr_type = atm.address_type '||
                               '                and addr.module = atm.module '||
                               '                and atm.primary_ind = ''Y'' ) ';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WH_COUNTRY_SUBQUERY;
-------------------------------------------------------------------------------
FUNCTION STORE_COUNTRY_SUBQUERY(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_subquery           IN OUT VARCHAR2,
                                I_country_bind_no     IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'ORGANIZATION_SQL.STORE_COUNTRY_SUBQUERY';
         
BEGIN

   IO_subquery := IO_subquery||' and exists (select ''x'' '||
                               '               from addr, '||
                               '                    add_type_module atm '||
                               '              where addr.country_id = :'||I_country_bind_no||
                               '                and addr.module = ''ST'' '||
                               '                and addr.primary_addr_ind = ''Y'' '||
                               '                and addr.key_value_1 = to_char(s.store) '||
                               '                and addr.addr_type = atm.address_type '||
                               '                and addr.module = atm.module '||
                               '                and atm.primary_ind = ''Y'' ) ';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STORE_COUNTRY_SUBQUERY;
--------------------------------------------------------------------
FUNCTION CHECK_STORE_ADD_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_district        IN       DISTRICT.DISTRICT%TYPE,
                                O_exists          IN OUT   BOOLEAN)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORGANIZATION_SQL.CHECK_STORE_ADD_EXISTS';
   L_exists    VARCHAR2(1)  := NULL;

   cursor C_CHK_STOREADD_DISTRICT is
      select 'x'
        from store_add
       where district = I_district;

BEGIN

   open C_CHK_STOREADD_DISTRICT;
   fetch C_CHK_STOREADD_DISTRICT into L_exists;
   close C_CHK_STOREADD_DISTRICT;

   if L_exists is NOT NULL then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_STORE_ADD_EXISTS;
--------------------------------------------------------------------
FUNCTION DELETE_AREA_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_area            IN       AREA_TL.AREA%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'ORGANIZATION_SQL.DELETE_AREA_TL';
   L_table         VARCHAR2(30) := 'AREA_TL';


   BEGIN

   if I_area is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_area',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if not LOCK_AREA_TL(O_error_message,
                     I_area ) then
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'I_area '||I_area);
   delete from area_tl
         where area = I_area;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_AREA_TL;
--------------------------------------------------------------------
FUNCTION DELETE_CHAIN_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_chain           IN       CHAIN_TL.CHAIN%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'ORGANIZATION_SQL.DELETE_CHAIN_TL';
   L_table         VARCHAR2(30) :='CHAIN_TL';

   BEGIN

   if I_chain is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chain',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if not LOCK_CHAIN_TL(O_error_message,
                     I_chain ) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'I_chain '||I_chain);
   delete from chain_tl
         where chain = I_chain;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_CHAIN_TL;
--------------------------------------------------------------------
FUNCTION DELETE_DISTRICT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_district        IN       DISTRICT_TL.DISTRICT%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'ORGANIZATION_SQL.DELETE_DISTRICT_TL';
   L_table         VARCHAR2(30) := 'DISTRICT_TL';

   BEGIN

   if I_district is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_district',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if not LOCK_DISTRICT_TL(O_error_message,
                           I_district ) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'I_district '||I_district);
   delete from district_tl
         where district = I_district;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DISTRICT_TL;
--------------------------------------------------------------------
FUNCTION DELETE_REGION_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_region          IN       REGION_TL.REGION%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'ORGANIZATION_SQL.DELETE_REGION_TL';
   L_table         VARCHAR2(25) := 'REGION_TL';

   BEGIN

   if I_region is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_region',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if not LOCK_REGION_TL(O_error_message,
                     I_region ) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'I_region '||I_region);
   delete from region_tl
         where region = I_region;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_REGION_TL;
--------------------------------------------------------------------
END ORGANIZATION_SQL;
/
