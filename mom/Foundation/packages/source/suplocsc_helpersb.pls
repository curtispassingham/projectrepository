CREATE OR REPLACE PACKAGE BODY SUPLOCSC_HELPERS AS 

FUNCTION POST_QUERY(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_offset_day      IN OUT SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE,
					O_offset_day_desc IN OUT VARCHAR2,
                    O_delivery_cycle  IN OUT SOURCE_DLVRY_SCHED.delivery_cycle%TYPE,
					O_delivery_cycle_desc IN OUT VARCHAR2,
                    O_start_date      IN OUT SOURCE_DLVRY_SCHED.start_date%TYPE,
					O_loc_desc        IN OUT   VARCHAR2,                                    
			        I_day			  IN     SOURCE_DLVRY_SCHED_DAYS.DAY%TYPE,
                    I_source      	  IN     SOURCE_DLVRY_SCHED_EXC.SOURCE%TYPE,
				    I_source_type     IN     SOURCE_DLVRY_SCHED_EXC.SOURCE_TYPE%TYPE,
                    I_location        IN      repl_day.location%TYPE,
					I_loc_type        IN       VARCHAR2) 
	RETURN NUMBER IS 	
	
BEGIN
   if REPLENISHMENT_DAYS_SQL.CONVERT_TO_OFFSET_DAY(O_error_message,
   	                                               O_offset_day,
   	                                               I_day) = FALSE then
		return 0;
   end if; 

   if LANGUAGE_SQL.GET_CODE_DESC (O_error_message,
	            	              'DAYS',
                                  O_offset_day,
			                      O_offset_day_desc) = FALSE then
		return 0;
   end if; 

   if SOURCE_DLVRY_SCHED_SQL.GET_DELIVERY_CYCLE(O_error_message,
                                                O_delivery_cycle,
                                                O_start_date,
                                                I_source,
                                                I_source_type,
                                                I_location) = FALSE then                              
      return 0;
   end if;
   
   if LANGUAGE_SQL.GET_CODE_DESC (O_error_message,
	            	              'RPRC',
                                  O_delivery_cycle,
			                      O_delivery_cycle_desc) = FALSE then
      return 0;
   end if; 

   if LOCATION_ATTRIB_SQL.GET_NAME(O_error_message,
    	                           O_loc_desc,
	                               I_location,
	                               I_loc_type) = FALSE then
       return 0;
   end if;

	return 1;

EXCEPTION
   when OTHERS then
	    O_error_message := SQLERRM;
	    return 0;
	 
END POST_QUERY;

END SUPLOCSC_HELPERS;
/
