
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSMFM_SEEDOBJ AS

--------------------------------------------------------------------------------
LP_error_status   VARCHAR2(1) := NULL;

PROGRAM_ERROR   EXCEPTION;
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_REC_COUNTRY(O_error_msg       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_message         IN OUT   nocopy RIB_OBJECT,
                                   O_bus_obj_id      IN OUT   nocopy RIB_BUSOBJID_TBL,
                                   O_message_type    IN OUT   VARCHAR2,
                                   I_queue_rec       IN       SEEDOBJ_MFQUEUE%ROWTYPE,
                                   I_rowid           IN       ROWID)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_REC_CURRRATE(O_error_msg       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_message         IN OUT   nocopy RIB_OBJECT,
                                    O_bus_obj_id      IN OUT   nocopy RIB_BUSOBJID_TBL,
                                    O_message_type    IN OUT   VARCHAR2,
                                    I_queue_rec       IN       SEEDOBJ_MFQUEUE%ROWTYPE,
                                    I_rowid           IN       ROWID)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_message         IN OUT   RIB_OBJECT,
                        O_status_code     IN OUT   VARCHAR2,
                        O_error_msg       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message_type    IN OUT   VARCHAR2,
                        O_bus_obj_id      IN OUT   RIB_BUSOBJID_TBL,
                        O_routing_info    IN OUT   RIB_ROUTINGINFO_TBL,
                        I_seq_no          IN       SEEDOBJ_MFQUEUE.SEQ_NO%TYPE,
                        I_seed_id         IN       SEEDOBJ_MFQUEUE.COUNTRY_ID%TYPE);

--------------------------------------------------------------------------------
FUNCTION ADDTOQ(O_error_message    IN OUT   VARCHAR2,
                I_message_type     IN       SEEDOBJ_MFQUEUE.MESSAGE_TYPE%TYPE,
                I_country_id       IN       SEEDOBJ_MFQUEUE.COUNTRY_ID%TYPE,
                I_currency_code    IN       SEEDOBJ_MFQUEUE.CURRENCY_CODE%TYPE,
                I_country_desc     IN       SEEDOBJ_MFQUEUE.COUNTRY_DESC%TYPE,
                I_effective_date   IN       SEEDOBJ_MFQUEUE.EFFECTIVE_DATE%TYPE,
                I_exchange_type    IN       SEEDOBJ_MFQUEUE.EXCHANGE_TYPE%TYPE,
                I_exchange_rate    IN       SEEDOBJ_MFQUEUE.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSMFM_SEEDOBJ.ADDTOQ';

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                    'NULL',
                    'SEEDOBJ_MFQUEUE',
                    NULL);

   insert into SEEDOBJ_MFQUEUE(seq_no,
                               country_id,
                               currency_code,
                               country_desc,
                               effective_date,
                               exchange_type,
                               exchange_rate,
                               message_type,
                               thread_no,
                               family,
                               custom_message_type,
                               pub_status,
                               transaction_number,
                               transaction_time_stamp)
                        values(SEEDOBJ_MFSEQUENCE.NEXTVAL,
                               I_country_id,
                               I_currency_code,
                               I_country_desc,
                               I_effective_date,
                               I_exchange_type,
                               I_exchange_rate,
                               I_message_type,
                               1,
                               RMSMFM_SEEDOBJ.FAMILY,
                               'N',
                               'U',
                               SEEDOBJ_MFSEQUENCE.NEXTVAL,
                               SYSDATE);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADDTOQ;

--------------------------------------------------------------------------------
PROCEDURE GETNXT(O_status_code    IN OUT   VARCHAR2,
                 O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_message_type   IN OUT   VARCHAR2,
                 O_message        IN OUT   RIB_OBJECT,
                 O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                 O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                 I_num_threads    IN       NUMBER DEFAULT 1,
                 I_thread_val     IN       NUMBER DEFAULT 1)
   IS

   L_seed_row      SEEDOBJ_MFQUEUE%ROWTYPE;
   L_rowid         ROWID;
   L_hospital      VARCHAR2(1) := 'N';
   L_seed_id       VARCHAR2(3);

   cursor C_QUEUE is
      select seq_no,
             country_id,
             currency_code,
             country_desc,
             effective_date,
             exchange_type,
             exchange_rate,
             message_type,
             rowid
        from seedobj_mfqueue q
       where q.seq_no = (select min(q2.seq_no)
                           from seedobj_mfqueue q2
                          where q2.thread_no = I_thread_val
                            and q2.pub_status = 'U')
         and q.thread_no = I_thread_val;

   cursor C_HOSP_CRT is
      select 'Y'
        from seedobj_mfqueue
       where currency_code = L_seed_id
         and pub_status = API_CODES.HOSPITAL;

   cursor C_HOSP_CNT is
      select 'Y'
        from seedobj_mfqueue
       where country_id = L_seed_id
         and pub_status = API_CODES.HOSPITAL;


BEGIN

   LP_error_status := API_CODES.HOSPITAL;

   O_status_code  := API_CODES.NO_MSG;
   O_error_msg    := NULL;
   O_message_type := NULL;
   O_message      := NULL;
   O_bus_obj_id   := NULL;
   O_routing_info := RIB_ROUTINGINFO_TBL();

   SQL_LIB.SET_MARK('OPEN',
                    'C_QUEUE',
                    'SEEDOBJ_MFQUEUE',
                    NULL);
   open C_QUEUE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_QUEUE',
                    'SEEDOBJ_MFQUEUE',
                    NULL);
   fetch C_QUEUE into L_seed_row.seq_no,
                      L_seed_row.country_id,
                      L_seed_row.currency_code,
                      L_seed_row.country_desc,
                      L_seed_row.effective_date,
                      L_seed_row.exchange_type,
                      L_seed_row.exchange_rate,
                      O_message_type,
                      L_rowid;

   if C_QUEUE%NOTFOUND then
      close C_QUEUE;
      return;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_QUEUE',
                    'SEEDOBJ_MFQUEUE',
                    NULL);
   close C_QUEUE;
   
   --Value to pass to HANDLE ERRORS in case of an error
   if L_seed_row.currency_code is not NULL then
      L_seed_id := L_seed_row.currency_code;

      SQL_LIB.SET_MARK('OPEN',
                       'C_HOSP_CRT',
                       'SEEDOBJ_MFQUEUE',
                       NULL);
      open C_HOSP_CRT;

      SQL_LIB.SET_MARK('FETCH',
                       'C_HOSP_CRT',
                       'SEEDOBJ_MFQUEUE',
                       NULL);
      fetch C_HOSP_CRT into L_hospital;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_HOSP_CRT',
                       'SEEDOBJ_MFQUEUE',
                       NULL);
      close C_HOSP_CRT;

   else
      L_seed_id := L_seed_row.country_id;

      SQL_LIB.SET_MARK('OPEN',
                       'C_HOSP_CNT',
                       'SEEDOBJ_MFQUEUE',
                       NULL);
      open C_HOSP_CNT;

      SQL_LIB.SET_MARK('FETCH',
                       'C_HOSP_CNT',
                       'SEEDOBJ_MFQUEUE',
                       NULL);
      fetch C_HOSP_CNT into L_hospital;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_HOSP_CNT',
                       'SEEDOBJ_MFQUEUE',
                       NULL);
      close C_HOSP_CNT;

   end if;

   if L_hospital = 'Y' then
      O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',
                                        NULL,
                                        NULL,
                                        NULL);
      raise PROGRAM_ERROR;
   end if;

   if lower(O_message_type) in (RMSMFM_SEEDOBJ.COUNTRY_ADD,
                                RMSMFM_SEEDOBJ.COUNTRY_DEL,
                                RMSMFM_SEEDOBJ.COUNTRY_UPD) then
                                
      if PROCESS_QUEUE_REC_COUNTRY(O_error_msg,
                                   O_message,
                                   O_bus_obj_id,
                                   O_message_type,
                                   L_seed_row,
                                   L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      
   elsif lower(O_message_type) in (RMSMFM_SEEDOBJ.CURR_ADD,
                                   RMSMFM_SEEDOBJ.CURR_UPD) then
                                   
      if PROCESS_QUEUE_REC_CURRRATE(O_error_msg,
                                    O_message,
                                    O_bus_obj_id,
                                    O_message_type,
                                    L_seed_row,
                                    L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;                                   
          
   end if;

   if L_seed_row.country_id is not NULL then
   
      if O_message IS NOT NULL then
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id  := RIB_BUSOBJID_TBL(L_seed_row.country_id);
      end if;
   
   else
   
      if O_message IS NOT NULL then
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id  := RIB_BUSOBJID_TBL(L_seed_row.currency_code);
      end if;         
   
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_message,
                    O_status_code,
                    O_error_msg,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_seed_row.seq_no,
                    L_seed_id);
END GETNXT;

--------------------------------------------------------------------------------
PROCEDURE PUB_RETRY(O_status_code    IN OUT   VARCHAR2,
                    O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_message        IN OUT   RIB_OBJECT,
                    O_message_type   IN OUT   VARCHAR2,
                    O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                    O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                    I_REF_OBJECT     IN       RIB_OBJECT)
   IS

   L_seq_no        SEEDOBJ_MFQUEUE.SEQ_NO%TYPE   := NULL;
   L_seed_row      SEEDOBJ_MFQUEUE%ROWTYPE;
   L_rowid         ROWID;
   L_hospital      VARCHAR2(1) := 'N';
   L_seed_id       VARCHAR2(3);

   cursor C_RETRY_QUEUE is
      select seq_no,
             country_id,
             currency_code,
             country_desc,
             effective_date,
             exchange_type,
             exchange_rate,
             message_type,
             rowid
        from seedobj_mfqueue q
       where q.seq_no = L_seq_no;

BEGIN

   LP_error_status := API_CODES.HOSPITAL;

   O_status_code  := API_CODES.NO_MSG;
   O_error_msg    := NULL;
   O_message_type := NULL;
   O_message      := NULL;
   O_bus_obj_id   := NULL;

   L_seq_no := O_routing_info(1).value;
   O_routing_info := RIB_ROUTINGINFO_TBL();

   SQL_LIB.SET_MARK('OPEN',
                    'C_RETRY_QUEUE',
                    'SEEDOBJ_MFQUEUE',
                    NULL);
   open C_RETRY_QUEUE;

   SQL_LIB.SET_MARK('FETCH',
                    'C_RETRY_QUEUE',
                    'SEEDOBJ_MFQUEUE',
                    NULL);
   fetch C_RETRY_QUEUE into L_seed_row.seq_no,
                            L_seed_row.country_id,
                            L_seed_row.currency_code,
                            L_seed_row.country_desc,
                            L_seed_row.effective_date,
                            L_seed_row.exchange_type,
                            L_seed_row.exchange_rate,
                            O_message_type,
                            L_rowid;

   if C_RETRY_QUEUE%NOTFOUND then
      close C_RETRY_QUEUE;
      return;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_RETRY_QUEUE',
                    'SEEDOBJ_MFQUEUE',
                    NULL);
   close C_RETRY_QUEUE;
   
   --Value to pass to HANDLE ERRORS in case of an error
   if L_seed_row.currency_code is not NULL then
      L_seed_id := L_seed_row.currency_code;
   else
      L_seed_id := L_seed_row.country_id;
   end if;             

   if lower(O_message_type) in (RMSMFM_SEEDOBJ.COUNTRY_ADD,
                                RMSMFM_SEEDOBJ.COUNTRY_DEL,
                                RMSMFM_SEEDOBJ.COUNTRY_UPD) then
                                
      if PROCESS_QUEUE_REC_COUNTRY(O_error_msg,
                                   O_message,
                                   O_bus_obj_id,
                                   O_message_type,
                                   L_seed_row,
                                   L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      
   elsif lower(O_message_type) in (RMSMFM_SEEDOBJ.CURR_ADD,
                                   RMSMFM_SEEDOBJ.CURR_UPD) then
                                   
      if PROCESS_QUEUE_REC_CURRRATE(O_error_msg,
                                    O_message,
                                    O_bus_obj_id,
                                    O_message_type,
                                    L_seed_row,
                                    L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;                                   
          
   end if;

   if L_seed_row.country_id is not NULL then
   
      if O_message IS NOT NULL then
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id  := RIB_BUSOBJID_TBL(L_seed_row.country_id);
      end if;
   
   else
   
      if O_message IS NOT NULL then
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id  := RIB_BUSOBJID_TBL(L_seed_row.currency_code);
      end if;         
   
   end if;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_message,
                    O_status_code,
                    O_error_msg,
                    O_message_type,
                    O_bus_obj_id,
                    O_routing_info,
                    L_seed_row.seq_no,
                    L_seed_id);
END PUB_RETRY;

--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_REC_COUNTRY(O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_message        IN OUT   nocopy RIB_OBJECT,
                                   O_bus_obj_id     IN OUT   nocopy RIB_BUSOBJID_TBL,
                                   O_message_type   IN OUT   VARCHAR2,
                                   I_queue_rec      IN       SEEDOBJ_MFQUEUE%ROWTYPE,
                                   I_rowid          IN       ROWID)
RETURN BOOLEAN IS

   L_program           VARCHAR2(50)         := 'RMSMFM_SEEDOBJ.PROCESS_QUEUE_REC_COUNTRY';
   L_countrydesc_rec   "RIB_CountryDesc_REC"  := NULL;
   L_countryref_rec    "RIB_CountryRef_REC"   := NULL;

BEGIN

   if lower(O_message_type) in (RMSMFM_SEEDOBJ.COUNTRY_ADD,
                                          RMSMFM_SEEDOBJ.COUNTRY_UPD) then

      L_countrydesc_rec := "RIB_CountryDesc_REC"(0,
                                               I_queue_rec.country_id,
                                               I_queue_rec.country_desc);
      O_message := L_countrydesc_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      delete
        from seedobj_mfqueue
       where rowid = I_rowid;

   elsif lower(O_message_type) = RMSMFM_SEEDOBJ.COUNTRY_DEL then

      L_countryref_rec := "RIB_CountryRef_REC"(0,
                                             I_queue_rec.country_id);

      O_message := L_countryref_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;

      delete
        from seedobj_mfqueue
       where rowid = I_rowid;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_REC_COUNTRY;

--------------------------------------------------------------------------------
FUNCTION PROCESS_QUEUE_REC_CURRRATE(O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_message        IN OUT   nocopy RIB_OBJECT,
                                    O_bus_obj_id     IN OUT   nocopy RIB_BUSOBJID_TBL,
                                    O_message_type   IN OUT   VARCHAR2,
                                    I_queue_rec      IN       SEEDOBJ_MFQUEUE%ROWTYPE,
                                    I_rowid          IN       ROWID)
RETURN BOOLEAN IS
   
   L_program            VARCHAR2(50)                        := 'RMSMFM_SEED0BJ.PROCESS_QUEUE_REC_CURRRATE';
   L_currratedesc_rec   "RIB_CurrRateDesc_REC"                := NULL;
   L_currency_code      SEEDOBJ_MFQUEUE.CURRENCY_CODE%TYPE  := NULL;

BEGIN

      L_currency_code := I_queue_rec.currency_code;

      if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_msg,
                                          L_currency_code) = FALSE then
         return FALSE;
      end if;

      L_currratedesc_rec := "RIB_CurrRateDesc_REC"(0,
                                                 I_queue_rec.currency_code,
                                                 L_currency_code,
                                                 I_queue_rec.effective_date,
                                                 I_queue_rec.exchange_rate,
                                                 I_queue_rec.exchange_type);
      O_message := L_currratedesc_rec;

      LP_error_status := API_CODES.UNHANDLED_ERROR;
      
      delete
        from seedobj_mfqueue
       where rowid = I_rowid;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_REC_CURRRATE;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(O_message        IN OUT   RIB_OBJECT,
                        O_status_code    IN OUT   VARCHAR2,
                        O_error_msg      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_message_type   IN OUT   VARCHAR2,
                        O_bus_obj_id     IN OUT   RIB_BUSOBJID_TBL,
                        O_routing_info   IN OUT   RIB_ROUTINGINFO_TBL,
                        I_seq_no         IN       SEEDOBJ_MFQUEUE.SEQ_NO%TYPE,
                        I_seed_id        IN       SEEDOBJ_MFQUEUE.COUNTRY_ID%TYPE)
   IS

   L_error_type        VARCHAR2(5)           := NULL;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then

      
      O_bus_obj_id   := RIB_BUSOBJID_TBL(I_seed_id);
      O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                I_seq_no,
                                                                NULL,
                                                                NULL,
                                                                NULL,
                                                                NULL));
      update seedobj_mfqueue
         set pub_status = LP_error_status
       where seq_no = I_seq_no;

   end if;

   SQL_LIB.API_MSG(L_error_type,
                   O_error_msg);

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(O_status_code,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_SEEDOBJ');
END HANDLE_ERRORS;
--------------------------------------------------------------------------------
END RMSMFM_SEEDOBJ;
/
