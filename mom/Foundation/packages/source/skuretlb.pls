CREATE OR REPLACE PACKAGE BODY SKU_RETAIL_SQL AS
------------------------------------------------------------------------------------------
--- internal function called by update_retail
FUNCTION ITEM_WH_PROCESSING (O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                             I_new_unit_retail      IN      ITEM_LOC.UNIT_RETAIL%TYPE,
                             I_old_unit_retail      IN      ITEM_LOC.UNIT_RETAIL%TYPE,
                             I_new_selling_retail   IN      ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                             I_new_selling_uom      IN      ITEM_LOC.SELLING_UOM%TYPE,
                             I_default_down_ind     IN      VARCHAR2,
                             I_location             IN      ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_loc                  ITEM_LOC.LOC%TYPE;
   L_local_currency       WH.CURRENCY_CODE%TYPE;
   L_local_retail         ITEM_LOC.UNIT_RETAIL%TYPE:=0;
   L_selling_retail       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE:=0;
   L_zone_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   L_program              VARCHAR2(60)                  := 'SKU_RETAIL_SQL.ITEM_WH_PROCESSING';
   L_table                VARCHAR2(60)                  := 'ITEM_LOC';
   L_new_unit_retail      ITEM_LOC.UNIT_RETAIL%TYPE:=0;
   L_selling_unit_retail  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE:=0;
   L_selling_uom          ITEM_LOC.SELLING_UOM%TYPE;

   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);
   ---

   cursor C_ITEM_WH_1 is
   select il.loc,
          wh.currency_code local_curr,
          il.unit_retail,
          il.selling_unit_retail,
          il.selling_uom,
          im.status
     from item_loc il, wh, item_master im
    where il.item = im.item
      and il.loc = I_location
      and im.item = I_item
      and il.loc = wh.wh
      and il.loc_type = 'W'
    order by wh.currency_code
      for update of il.item nowait;

   cursor C_ITEM_WH_2 is
   select il.loc,
          wh.currency_code local_curr,
          il.unit_retail,
          il.selling_unit_retail,
          il.selling_uom,
          im.status
     from item_loc il, wh, item_master im
    where il.item = im.item
     and il.loc = I_location
      and (I_default_down_ind = 'Y'
           and im.item_parent = I_item
           and im.item_level <= im.tran_level)
      and il.loc = wh.wh
      and il.loc_type = 'W'
      order by wh.currency_code
      for update of il.item nowait;

   cursor C_ITEM_WH_3 is
   select il.loc,
          wh.currency_code local_curr,
          il.unit_retail,
          il.selling_unit_retail,
          il.selling_uom,
          im.status
     from item_loc il, wh, item_master im
    where il.item = im.item
      and il.loc = I_location
      and (I_default_down_ind = 'Y'
               and im.item_grandparent = I_item
               and im.item_level <= im.tran_level)
      and il.loc = wh.wh
      and il.loc_type = 'W'
      order by wh.currency_code
      for update of il.item nowait;
   ---
BEGIN
   if I_location is NULL then
     O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;
   -- For each child item
   ---
   FOR rec in C_ITEM_WH_1 LOOP
       L_loc                 := rec.loc;
       
       -- Update item location retail
       update item_loc
          set unit_retail = I_new_unit_retail,
              selling_unit_retail = I_new_selling_retail,
              selling_uom = I_new_selling_uom,
              regular_unit_retail = I_new_unit_retail,
              last_update_datetime = sysdate,
              last_update_id = get_user
        where current of C_ITEM_WH_1;
   END LOOP;
   ---
   FOR rec2 in C_ITEM_WH_2 LOOP
       L_loc                 := rec2.loc;
       
       -- Update item location retail       ---
       update item_loc
          set unit_retail = I_new_unit_retail,
              selling_unit_retail = I_new_selling_retail,
              selling_uom = I_new_selling_uom,
              regular_unit_retail = I_new_unit_retail,
              last_update_datetime = sysdate,
              last_update_id = get_user
        where current of C_ITEM_WH_2;
   END LOOP;
   ---
   FOR rec3 in C_ITEM_WH_3 LOOP
       L_loc                 := rec3.loc;
       
       ---Update item location retail       ---
       update item_loc
          set unit_retail = I_new_unit_retail,
              selling_unit_retail = I_new_selling_retail,
              selling_uom = I_new_selling_uom,
              regular_unit_retail = I_new_unit_retail,
              last_update_datetime = sysdate,
              last_update_id = get_user
        where current of C_ITEM_WH_3;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_item,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
      return FALSE;
END ITEM_WH_PROCESSING;
-------------------------------------------------------------------------------------------

--- internal function, called by update_retail.
FUNCTION ITEM_STORE_PROCESSING (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                                I_location             IN       ITEM_LOC.LOC%TYPE,
                                I_old_unit_retail      IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                I_new_unit_retail      IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                I_old_selling_retail   IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                I_new_selling_retail   IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                I_old_selling_uom      IN       ITEM_LOC.SELLING_UOM%TYPE,
                                I_new_selling_uom      IN       ITEM_LOC.SELLING_UOM%TYPE,
                                I_multi_units          IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                I_multi_unit_retail    IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                I_multi_selling_uom    IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                I_default_down_ind     IN       VARCHAR2,
                                I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE)

RETURN BOOLEAN IS

   L_program                   VARCHAR2(60) := 'SKU_RETAIL_SQL.ITEM_STORE_PROCESSING';
   L_table                     VARCHAR2(60) := 'ITEM_LOC';
   
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ITEM_STORE_1 is
   select il.loc, il.item
     from item_loc il, item_master im
    where il.item = im.item
      and im.status != 'A'
      and im.item = I_item
      and il.loc = I_location
      and loc_type = 'S'
      for update of il.item nowait;

   cursor C_ITEM_STORE_2 is
   select il.loc, il.item
     from item_loc il, item_master im
    where il.item = im.item
      and im.status != 'A'
      and (I_default_down_ind = 'Y'
          and im.item_parent = I_item
          and item_level <= tran_level)
      and il.loc = I_location
      and loc_type = 'S'
      for update of il.item nowait;

   cursor C_ITEM_STORE_3 is
   select il.loc, il.item
     from item_loc il, item_master im
    where il.item = im.item
      and im.status != 'A'
      and (I_default_down_ind = 'Y'
           and im.item_grandparent = I_item
           and im.item_level <= im.tran_level)
      and il.loc = I_location
      and loc_type = 'S'
      for update of il.item nowait;

---
BEGIN
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   -- Process one store at a time
   for rec in C_ITEM_STORE_1 loop
   ---
      update item_loc
         set unit_retail =  I_new_unit_retail,
             selling_unit_retail = I_new_selling_retail,
             selling_uom = I_new_selling_uom,
             regular_unit_retail = I_new_unit_retail,
             multi_units = I_multi_units,
             multi_unit_retail = I_multi_unit_retail,
             multi_selling_uom = I_multi_selling_uom,
             last_update_datetime = sysdate,
             last_update_id = get_user
       where current of C_item_store_1;
   END LOOP;
   ---
   for rec2 in C_ITEM_STORE_2 loop
   ---
      update item_loc
         set unit_retail =  I_new_unit_retail,
             selling_unit_retail = I_new_selling_retail,
             selling_uom = I_new_selling_uom,
             regular_unit_retail = I_new_unit_retail,
             multi_units = I_multi_units,
             multi_unit_retail = I_multi_unit_retail,
             multi_selling_uom = I_multi_selling_uom,
             last_update_datetime = sysdate,
             last_update_id = get_user
       where current of C_item_store_2;
   END LOOP;
   ---
   for rec3 in C_ITEM_STORE_3 loop
      ---
      update item_loc
         set unit_retail =  I_new_unit_retail,
             selling_unit_retail = I_new_selling_retail,
             selling_uom = I_new_selling_uom,
             regular_unit_retail = I_new_unit_retail,
             multi_units = I_multi_units,
             multi_unit_retail = I_multi_unit_retail,
             multi_selling_uom = I_multi_selling_uom,
             last_update_datetime = sysdate,
             last_update_id = get_user
       where current of C_item_store_3;
   END LOOP;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            I_location);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
      return FALSE;
END ITEM_STORE_PROCESSING;
-------------------------------------------------------------------------------------
--- public function
FUNCTION UPDATE_RETAIL(O_error_message        IN OUT   VARCHAR2,
                       I_item                 IN       ITEM_MASTER.item%TYPE,
                       I_old_unit_retail      IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                       I_new_unit_retail      IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                       I_old_selling_retail   IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                       I_new_selling_retail   IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                       I_old_selling_uom      IN       ITEM_LOC.SELLING_UOM%TYPE,
                       I_new_selling_uom      IN       ITEM_LOC.SELLING_UOM%TYPE,
                       I_multi_units          IN       ITEM_LOC.MULTI_UNITS%TYPE,
                       I_multi_unit_retail    IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                       I_multi_selling_uom    IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                       I_default_down_ind     IN       VARCHAR2,
                       I_location             IN       ITEM_LOC.LOC%TYPE,
                       I_dept                 IN       DEPS.DEPT%TYPE,
                       I_class                IN       CLASS.CLASS%TYPE,
                       I_subclass             IN       SUBCLASS.SUBCLASS%TYPE,
                       I_primary_curr         IN       CURRENCIES.CURRENCY_CODE%TYPE,
                       I_pack_ind             IN       ITEM_MASTER.PACK_IND%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(255)   := 'SKU_RETAIL_SQL.UPDATE_RETAIL';

BEGIN

  if ITEM_STORE_PROCESSING(O_error_message,
                            I_item,
                            I_location,
                            I_old_unit_retail,
                            I_new_unit_retail,
                            I_old_selling_retail,
                            I_new_selling_retail,
                            I_old_selling_uom,
                            I_new_selling_uom,
                            I_multi_units,
                            I_multi_unit_retail,
                            I_multi_selling_uom,
                            I_default_down_ind,
                            I_pack_ind) = FALSE then
      return FALSE;
   end if;
   ---
   if I_pack_ind = 'N' then
      if ITEM_WH_PROCESSING(O_error_message,
                            I_item,
                            I_new_unit_retail,
                            I_old_unit_retail,
                            I_new_selling_retail,
                            I_new_selling_uom,
                            I_default_down_ind,
                            I_location) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_RETAIL;
--------------------------------------------------------------------------------------------------------
END SKU_RETAIL_SQL;
/