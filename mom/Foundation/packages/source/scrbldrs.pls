
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE SCRIPTBUILDER AUTHID CURRENT_USER AS
------------------------------------------------------------------------
--Function Name:  PREPARE_CURSOR                                               
--Purpose      :  This function will begin by using the table ALL_TAB_COLUMNS to 
--                loop through and fetch a list of all the column names that are
--                on a given table for a given owner.  This will be done using a
--                cursor defined as:  C_COLUMNS which will be reused later to 
--                reloop through.
--Date Created :  7-Apr-97
--Created by   :  Catherine Miller
---------------------------------------------------------------------------
FUNCTION PREPARE_CURSOR(I_table_name             IN VARCHAR2,
                        I_owner                  IN VARCHAR2,
                        I_where_clause           IN VARCHAR2,
                        O_column_names           IN OUT VARCHAR2,
                        O_num_columns            IN OUT NUMBER,
                        IO_error_message         IN OUT VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION FETCH_NEXT_ROW(O_done                   IN OUT BOOLEAN,
                        IO_error_message         IN OUT VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION FETCH_NEXT_COLUMN_VALUE(I_col_num        IN     NUMBER,
                                 O_column_value   IN OUT VARCHAR2,
                                 IO_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN;

END SCRIPTBUILDER;
/
