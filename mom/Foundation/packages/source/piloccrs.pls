
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PACKITEM_LOC_CREATE_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Name:     CHECK_SKU_STATUS
-- Purpose:  This function checks the existence of all
--           item-locaction status types for the component items
--           in a pack.
--
FUNCTION CHECK_SKU_STATUS(O_error_message  IN OUT VARCHAR2,
                          O_inactive_ind   IN OUT BOOLEAN,
                          O_del_disc_ind   IN OUT BOOLEAN,
                          I_pack_no        IN     ITEM_MASTER.ITEM%TYPE,
                          I_loc            IN     ITEM_LOC.LOC%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
-- Name:     UPDATE_SKU_STATUS
-- Purpose:  This function checks and updates all the 
--           item-location status types for the component items
--           in a pack, excluding component items which are inner
--           packs and the component items of those inner packs. 
-- 
FUNCTION UPDATE_SKU_STATUS(O_error_message  IN OUT VARCHAR2,
                           I_pack_no        IN     ITEM_MASTER.ITEM%TYPE,
                           I_loc            IN     ITEM_LOC.LOC%TYPE,
                           I_status         IN     ITEM_LOC.STATUS%TYPE)
   return BOOLEAN; 
-------------------------------------------------------------------------------
END;
/


