
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XMRCHHRRCLS_SQL AS
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_RECLASS
   -- Purpose      :  Inserts a record on the PEND_MERCH_HIER table
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  MODIFY_RECLASS
   -- Purpose      :  Updates a record on the PEND_MERCH_HIER table
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DELETE_RECLASS
   -- Purpose      :  Deletes a record on the PEND_MERCH_HIER table
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE,
                 I_message_type          IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRRCLS_SQL.PERSIST';

BEGIN

   if I_message_type = RMSSUB_XMRCHHRRCLS.LP_cre_type then
      if CREATE_RECLASS(O_error_message,
                        I_pend_merch_hier_rec) = FALSE then
         return FALSE;
      end if;

   elsif I_message_type = RMSSUB_XMRCHHRRCLS.LP_mod_type then
      if MODIFY_RECLASS(O_error_message,
                        I_pend_merch_hier_rec) = FALSE then
         return FALSE;
      end if;

   elsif I_message_type = RMSSUB_XMRCHHRRCLS.LP_del_type then
      if DELETE_RECLASS(O_error_message,
                        I_pend_merch_hier_rec) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_SQL.CREATE_RECLASS';

BEGIN

   if not MERCH_RECLASS_SQL.INSERT_RECLASS(O_error_message,
                                           I_pend_merch_hier_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END CREATE_RECLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_SQL.MODIFY_RECLASS';

BEGIN

   if not MERCH_RECLASS_SQL.UPDATE_RECLASS(O_error_message,
                                           I_pend_merch_hier_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END MODIFY_RECLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_RECLASS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pend_merch_hier_rec   IN       PEND_MERCH_HIER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XMRCHHRSCLS_SQL.DELETE_RECLASS';

BEGIN

   if not MERCH_RECLASS_SQL.DELETE_RECLASS(O_error_message,
                                           I_pend_merch_hier_rec) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DELETE_RECLASS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XMRCHHRRCLS_SQL;
/
