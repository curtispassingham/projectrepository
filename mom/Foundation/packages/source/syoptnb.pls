CREATE OR REPLACE PACKAGE BODY SYSTEM_OPTIONS_SQL AS
-------------------------------------------------------------------------------
   LP_tsf_force_close_ind   SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE := NULL;
   LP_nwp_ind               SYSTEM_OPTIONS.NWP_IND%TYPE;
   LP_min_cum_markon_pct    SYSTEM_OPTIONS.MIN_CUM_MARKON_PCT%TYPE;
   LP_max_cum_markon_pct    SYSTEM_OPTIONS.MAX_CUM_MARKON_PCT%TYPE;
   LP_cum_markon_ind        BOOLEAN := FALSE;

FUNCTION POPULATE_SYSTEM_OPTIONS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS';

   cursor C_SYSTEM_OPTIONS is
      select *
        from system_options;

BEGIN

   GP_system_options_row := NULL;

   open  C_SYSTEM_OPTIONS;
   fetch C_SYSTEM_OPTIONS into GP_system_options_row;
   close C_SYSTEM_OPTIONS;

   -- Check a required field, if it is null then there was a problem
   -- reading from the table.
   if GP_system_options_row.base_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CURSOR',
                                            'C_SYSTEM_OPTIONS',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   GP_options_populated := TRUE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_SYSTEM_OPTIONS;
-------------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_system_options_row     OUT  SYSTEM_OPTIONS%ROWTYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_system_options_row := GP_system_options_row;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SYSTEM_OPTIONS;
-------------------------------------------------------------------------------
FUNCTION GET_STKLDGR_LEVEL(O_error_message                 IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_stock_ledger_time_level_code     OUT  SYSTEM_OPTIONS.STOCK_LEDGER_TIME_LEVEL_CODE%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'SYSTEM_OPTIONS_SQL.GET_STKLDGER_LEVEL';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_stock_ledger_time_level_code := GP_system_options_row.stock_ledger_time_level_code;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STKLDGR_LEVEL;
-------------------------------------------------------------------------------
FUNCTION GET_AIP_IND(O_aip_ind           OUT  SYSTEM_OPTIONS.AIP_IND%TYPE,
                     O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_AIP_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_aip_ind := GP_system_options_row.aip_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_AIP_IND;
-------------------------------------------------------------------------------
FUNCTION MULTI_CURRENCY_IND(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_multi_currency_ind     OUT  SYSTEM_OPTIONS.MULTI_CURRENCY_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.MULTI_CURRENCY_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_multi_currency_ind := GP_system_options_row.multi_currency_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MULTI_CURRENCY_IND;
-------------------------------------------------------------------------------
FUNCTION CURRENCY_CODE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_currency_code     OUT  SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.CURRENCY_CODE';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_currency_code := GP_system_options_row.currency_code;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CURRENCY_CODE;
-------------------------------------------------------------------------------
FUNCTION CONSOLIDATION_IND(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_consolidation_ind     OUT  SYSTEM_OPTIONS.CONSOLIDATION_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.CONSOLIDATION_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_consolidation_ind := GP_system_options_row.consolidation_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONSOLIDATION_IND;
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
FUNCTION STD_AV_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_std_av_ind        OUT  SYSTEM_OPTIONS.STD_AV_IND%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.STD_AV_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_std_av_ind := GP_system_options_row.std_av_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STD_AV_IND;
-------------------------------------------------------------------------------
FUNCTION GET_DEPT_LEVEL_TSF(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_dept_level_tsf    OUT  SYSTEM_OPTIONS.DEPT_LEVEL_TRANSFERS%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_DEPT_LEVEL_TSF';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_dept_level_tsf := GP_system_options_row.dept_level_transfers;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEPT_LEVEL_TSF;
-------------------------------------------------------------------------------
FUNCTION GET_FINANCIAL_AP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_financial_ap      OUT  SYSTEM_OPTIONS.FINANCIAL_AP%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)  :=  'SYSTEM_OPTIONS.GET_FINANCIAL_AP';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_financial_ap := GP_system_options_row.financial_ap;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_FINANCIAL_AP;
-------------------------------------------------------------------------------
FUNCTION GET_IMPORT_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_import_ind        OUT  SYSTEM_OPTIONS.IMPORT_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS.GET_IMPORT_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_import_ind := GP_system_options_row.import_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_IMPORT_IND;
-------------------------------------------------------------------------------
FUNCTION GET_ELC_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     O_elc_ind           OUT  SYSTEM_OPTIONS.ELC_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS.GET_ELC_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_elc_ind := GP_system_options_row.elc_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ELC_IND;
-------------------------------------------------------------------------------
FUNCTION GET_IMPORT_ELC_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_import_ind        OUT  SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                            O_elc_ind           OUT  SYSTEM_OPTIONS.ELC_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS.GET_IMPORT_ELC_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_import_ind := GP_system_options_row.import_ind;
   O_elc_ind    := GP_system_options_row.elc_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_IMPORT_ELC_IND;
-------------------------------------------------------------------------------
FUNCTION GET_CONTRACT_IND(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_contract_ind      OUT  SYSTEM_OPTIONS.CONTRACT_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS.GET_CONTRACT_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_contract_ind := GP_system_options_row.contract_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CONTRACT_IND;
-------------------------------------------------------------------------------
FUNCTION GET_IMAGE_PATH(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_image_path        OUT  SYSTEM_OPTIONS.IMAGE_PATH%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_IMAGE_PATH';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_image_path := GP_system_options_row.image_path;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_IMAGE_PATH;
-------------------------------------------------------------------------------
FUNCTION GET_LATEST_SHIP_DAYS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_latest_ship_days    OUT  SYSTEM_OPTIONS.LATEST_SHIP_DAYS%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_LATEST_SHIP_DAYS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_latest_ship_days := GP_system_options_row.latest_ship_days;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LATEST_SHIP_DAYS;
-------------------------------------------------------------------------------
FUNCTION GET_TITLE_PASS(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_fob_title_pass          OUT  SYSTEM_OPTIONS.FOB_TITLE_PASS%TYPE,
                        O_fob_title_pass_desc     OUT  SYSTEM_OPTIONS.FOB_TITLE_PASS_DESC%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_TITLE_PASS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_fob_title_pass      := GP_system_options_row.fob_title_pass;
   O_fob_title_pass_desc := GP_system_options_row.fob_title_pass_desc;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TITLE_PASS;
-------------------------------------------------------------------------------
FUNCTION GET_BASE_COUNTRY(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_base_country_id     OUT  SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS.GET_BASE_COUNTRY';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_base_country_id := GP_system_options_row.base_country_id;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_BASE_COUNTRY;
-------------------------------------------------------------------------------
FUNCTION GET_ALL_DEFAULT_UOM(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_default_standard_uom       OUT  SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE,
                             O_default_dimension_uom      OUT  SYSTEM_OPTIONS.DEFAULT_DIMENSION_UOM%TYPE,
                             O_default_weight_uom         OUT  SYSTEM_OPTIONS.DEFAULT_WEIGHT_UOM%TYPE,
                             O_default_packing_method     OUT  SYSTEM_OPTIONS.DEFAULT_PACKING_METHOD%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_ALL_DEFAULT_UOM';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_default_standard_uom   := GP_system_options_row.default_standard_uom;
   O_default_dimension_uom  := GP_system_options_row.default_dimension_uom;
   O_default_weight_uom     := GP_system_options_row.default_weight_uom;
   O_default_packing_method := GP_system_options_row.default_packing_method;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ALL_DEFAULT_UOM;
-------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ROUNDING(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_round_to_inner_pct      OUT  SYSTEM_OPTIONS.ROUND_TO_INNER_PCT%TYPE,
                              O_round_to_case_pct       OUT  SYSTEM_OPTIONS.ROUND_TO_CASE_PCT%TYPE,
                              O_round_to_layer_pct      OUT  SYSTEM_OPTIONS.ROUND_TO_LAYER_PCT%TYPE,
                              O_round_to_pallet_pct     OUT  SYSTEM_OPTIONS.ROUND_TO_PALLET_PCT%TYPE,
                              O_round_lvl               OUT  SYSTEM_OPTIONS.ROUND_LVL%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_DEFAULT_ROUNDING';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_round_to_inner_pct  := GP_system_options_row.round_to_inner_pct;
   O_round_to_case_pct   := GP_system_options_row.round_to_case_pct;
   O_round_to_layer_pct  := GP_system_options_row.round_to_layer_pct;
   O_round_to_pallet_pct := GP_system_options_row.round_to_pallet_pct;
   O_round_lvl           := GP_system_options_row.round_lvl;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_ROUNDING;
-------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_UOM_AND_NAMES(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_default_standard_uom       OUT  SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE,
                                   O_default_dimension_uom      OUT  SYSTEM_OPTIONS.DEFAULT_DIMENSION_UOM%TYPE,
                                   O_default_weight_uom         OUT  SYSTEM_OPTIONS.DEFAULT_WEIGHT_UOM%TYPE,
                                   O_default_packing_method     OUT  SYSTEM_OPTIONS.DEFAULT_PACKING_METHOD%TYPE,
                                   O_default_pallet_name        OUT  SYSTEM_OPTIONS.DEFAULT_PALLET_NAME%TYPE,
                                   O_default_case_name          OUT  SYSTEM_OPTIONS.DEFAULT_CASE_NAME%TYPE,
                                   O_default_inner_name         OUT  SYSTEM_OPTIONS.DEFAULT_INNER_NAME%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_DEFAULT_UOM_AND_NAMES';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_default_standard_uom   := GP_system_options_row.default_standard_uom;
   O_default_dimension_uom  := GP_system_options_row.default_dimension_uom;
   O_default_weight_uom     := GP_system_options_row.default_weight_uom;
   O_default_packing_method := GP_system_options_row.default_packing_method;
   O_default_pallet_name    := GP_system_options_row.default_pallet_name;
   O_default_case_name      := GP_system_options_row.default_case_name;
   O_default_inner_name     := GP_system_options_row.default_inner_name;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_UOM_AND_NAMES;
-------------------------------------------------------------------------------
FUNCTION GET_LC_EXP_DAYS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_lc_exp_days       OUT  SYSTEM_OPTIONS.LC_EXP_DAYS%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS.GET_LC_EXP_DAYS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_lc_exp_days := GP_system_options_row.lc_exp_days;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END GET_LC_EXP_DAYS;
-------------------------------------------------------------------------------
FUNCTION GET_LC_DEFAULTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_lc_form_type      OUT  SYSTEM_OPTIONS.LC_FORM_TYPE%TYPE,
                         O_lc_type           OUT  SYSTEM_OPTIONS.LC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS.GET_LC_DEFAULTS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_lc_form_type := GP_system_options_row.lc_form_type;
   O_lc_type      := GP_system_options_row.lc_type;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LC_DEFAULTS;
-------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_UOP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_default_uop       OUT  SYSTEM_OPTIONS.DEFAULT_UOP%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS_SQL.GET_DEFAULT_UOP';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_default_uop := GP_system_options_row.default_uop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_UOP;
-------------------------------------------------------------------------------
FUNCTION GET_ROUND_LVL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_round_lvl         OUT  SYSTEM_OPTIONS.ROUND_LVL%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS_SQL.GET_ROUND_LVL';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_round_lvl := GP_system_options_row.round_lvl;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ROUND_LVL;
-------------------------------------------------------------------------------
FUNCTION GET_STAKE_REVIEW_DAYS(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_stake_review_days     OUT  SYSTEM_OPTIONS.STAKE_REVIEW_DAYS%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS.GET_STAKE_REVIEW_DAYS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_stake_review_days := GP_system_options_row.stake_review_days;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STAKE_REVIEW_DAYS;
--------------------------------------------------------------------------------------
FUNCTION GET_SA_IND(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_sales_audit_ind     OUT  SYSTEM_OPTIONS.SALES_AUDIT_IND%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS.GET_SA_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_sales_audit_ind := GP_system_options_row.sales_audit_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_SA_IND;
-------------------------------------------------------------------------------
FUNCTION GET_DEAL_LEAD_DAYS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_deal_lead_days     OUT  SYSTEM_OPTIONS.DEAL_LEAD_DAYS%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS.GET_DEAL_LEAD_DAYS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_deal_lead_days := GP_system_options_row.deal_lead_days;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEAL_LEAD_DAYS;
-------------------------------------------------------------------------------

FUNCTION GET_UPDATE_HTS_INDS(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_update_item_hts_ind      OUT  SYSTEM_OPTIONS.UPDATE_ITEM_HTS_IND%TYPE,
                             O_update_order_hts_ind     OUT  SYSTEM_OPTIONS.UPDATE_ORDER_HTS_IND%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS_SQL.GET_UPDATE_HTS_INDS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_update_item_hts_ind  := GP_system_options_row.update_item_hts_ind;
   O_update_order_hts_ind := GP_system_options_row.update_order_hts_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_UPDATE_HTS_INDS;
-------------------------------------------------------------------------------
FUNCTION GET_SCHEDULE_IND(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_loc_dlvry_ind        OUT  SYSTEM_OPTIONS.LOC_DLVRY_IND%TYPE,
                          O_loc_activity_ind     OUT  SYSTEM_OPTIONS.LOC_ACTIVITY_IND%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(62) := 'SYSTEM_OPTIONS_SQL.GET_SCHEDULE_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_loc_dlvry_ind    := GP_system_options_row.loc_dlvry_ind;
   O_loc_activity_ind := GP_system_options_row.loc_activity_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_SCHEDULE_IND;
-------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ORDER_TYPE(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_default_order_type        OUT  SYSTEM_OPTIONS.DEFAULT_ORDER_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)    := 'SYSTEM_OPTIONS_SQL.GET_DEFAULT_ORDER_TYPE';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_default_order_type := GP_system_options_row.default_order_type;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_ORDER_TYPE;
-------------------------------------------------------------------------------
FUNCTION GET_ELC_VAT_STD_AV(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_elc_ind           OUT  SYSTEM_OPTIONS.ELC_IND%TYPE,
                            O_std_av_ind        OUT  SYSTEM_OPTIONS.STD_AV_IND%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)    := 'SYSTEM_OPTIONS_SQL.GET_ELC_VAT_STD_AV';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_elc_ind    := GP_system_options_row.elc_ind;
   O_std_av_ind := GP_system_options_row.std_av_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ELC_VAT_STD_AV;
-------------------------------------------------------------------------------
FUNCTION GET_ITEM_MASTER_OPTIONS(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_import_ind                 OUT  SYSTEM_OPTIONS.IMPORT_IND%TYPE,
                                 O_elc_ind                    OUT  SYSTEM_OPTIONS.ELC_IND%TYPE,
                                 O_multi_currency_ind         OUT  SYSTEM_OPTIONS.MULTI_CURRENCY_IND%TYPE,
                                 O_currency_code              OUT  SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                                 O_forecast_ind               OUT  SYSTEM_OPTIONS.FORECAST_IND%TYPE,
                                 O_default_standard_uom       OUT  SYSTEM_OPTIONS.DEFAULT_STANDARD_UOM%TYPE,
                                 O_auto_approve_child_ind     OUT  SYSTEM_OPTIONS.AUTO_APPROVE_CHILD_IND%TYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)    := 'SYSTEM_OPTIONS_SQL.GET_ITEM_MASTER_OPTIONS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   
   O_import_ind             := GP_system_options_row.import_ind;
   O_elc_ind                := GP_system_options_row.elc_ind;
   O_multi_currency_ind     := GP_system_options_row.multi_currency_ind;
   O_currency_code          := GP_system_options_row.currency_code;
   O_forecast_ind           := GP_system_options_row.forecast_ind;
   O_default_standard_uom   := GP_system_options_row.default_standard_uom;
   O_auto_approve_child_ind := GP_system_options_row.auto_approve_child_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEM_MASTER_OPTIONS;
-------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_OPTIONS(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_multi_currency_ind           OUT  SYSTEM_OPTIONS.MULTI_CURRENCY_IND%TYPE,
                             O_currency_code                OUT  SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                             O_stkldgr_vat_incl_retl_ind    OUT  SYSTEM_OPTIONS.STKLDGR_VAT_INCL_RETL_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_ITEMLOC_OPTIONS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_multi_currency_ind        := GP_system_options_row.multi_currency_ind;
   O_currency_code             := GP_system_options_row.currency_code;
   O_stkldgr_vat_incl_retl_ind := GP_system_options_row.stkldgr_vat_incl_retl_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEMLOC_OPTIONS;
-------------------------------------------------------------------------------
FUNCTION GET_HELP_INDS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_sales_audit_ind     OUT  SYSTEM_OPTIONS.SALES_AUDIT_IND%TYPE,
                       O_import_ind          OUT  SYSTEM_OPTIONS.IMPORT_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_HELP_INDS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_sales_audit_ind := GP_system_options_row.sales_audit_ind;
   O_import_ind      := GP_system_options_row.import_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_HELP_INDS;
-------------------------------------------------------------------------------
FUNCTION SOFT_CONTRACT_IND(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_soft_contract_ind     OUT  SYSTEM_OPTIONS.SOFT_CONTRACT_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.SOFT_CONTRACT_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_soft_contract_ind := GP_system_options_row.soft_contract_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END SOFT_CONTRACT_IND;
-------------------------------------------------------------------------------
FUNCTION GET_BRACKET_COST_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_bracket_costing_ind     OUT  SYSTEM_OPTIONS.BRACKET_COSTING_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_BRACKET_COST_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_bracket_costing_ind := GP_system_options_row.bracket_costing_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_BRACKET_COST_IND;
-------------------------------------------------------------------------------
FUNCTION GET_IB_DEFAULTS(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_look_ahead_days           OUT  SYSTEM_OPTIONS.LOOK_AHEAD_DAYS%TYPE,
                         O_cost_wh_storage_meas      OUT  SYSTEM_OPTIONS.COST_WH_STORAGE_MEAS%TYPE,
                         O_cost_wh_storage           OUT  SYSTEM_OPTIONS.COST_WH_STORAGE%TYPE,
                         O_cost_wh_storage_uom       OUT  SYSTEM_OPTIONS.COST_WH_STORAGE_UOM%TYPE,
                         O_cost_out_storage_meas     OUT  SYSTEM_OPTIONS.COST_OUT_STORAGE_MEAS%TYPE,
                         O_cost_out_storage          OUT  SYSTEM_OPTIONS.COST_OUT_STORAGE%TYPE,
                         O_cost_out_storage_uom      OUT  SYSTEM_OPTIONS.COST_OUT_STORAGE_UOM%TYPE,
                         O_cost_level                OUT  SYSTEM_OPTIONS.COST_LEVEL%TYPE,
                         O_storage_type              OUT  SYSTEM_OPTIONS.STORAGE_TYPE%TYPE,
                         O_max_weeks_supply          OUT  SYSTEM_OPTIONS.MAX_WEEKS_SUPPLY%TYPE,
                         O_target_roi                OUT  SYSTEM_OPTIONS.TARGET_ROI%TYPE,
                         O_cost_money                OUT  SYSTEM_OPTIONS.COST_MONEY%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_IB_DEFAULTS';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_look_ahead_days       := GP_system_options_row.look_ahead_days;
   O_cost_wh_storage_meas  := GP_system_options_row.cost_wh_storage_meas;
   O_cost_wh_storage       := GP_system_options_row.cost_wh_storage;
   O_cost_wh_storage_uom   := GP_system_options_row.cost_wh_storage_uom;
   O_cost_out_storage_meas := GP_system_options_row.cost_out_storage_meas;
   O_cost_out_storage      := GP_system_options_row.cost_out_storage;
   O_cost_out_storage_uom  := GP_system_options_row.cost_out_storage_uom;
   O_cost_level            := GP_system_options_row.cost_level;
   O_storage_type          := GP_system_options_row.storage_type;
   O_max_weeks_supply      := GP_system_options_row.max_weeks_supply;
   O_target_roi            := GP_system_options_row.target_roi;
   O_cost_money            := GP_system_options_row.cost_money;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_IB_DEFAULTS;
-------------------------------------------------------------------------------
FUNCTION GET_WH_CROSS_LINK_IND(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_wh_cross_link_ind     OUT  SYSTEM_OPTIONS.WH_CROSS_LINK_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.WH_CROSS_LINK_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_wh_cross_link_ind := GP_system_options_row.wh_cross_link_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_WH_CROSS_LINK_IND;
-------------------------------------------------------------------------------
FUNCTION GET_REJECT_STORE_ORD_IND(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_reject_store_ord_ind     OUT  SYSTEM_OPTIONS.REJECT_STORE_ORD_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.REJECT_STORE_ORD_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_reject_store_ord_ind := GP_system_options_row.reject_store_ord_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_REJECT_STORE_ORD_IND;
-------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ALLOC_CHRG_IND(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_default_alloc_chrg_ind     OUT  SYSTEM_OPTIONS.DEFAULT_ALLOC_CHRG_IND%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'SYSTEM_OPTIONS_SQL.GET_DEFAULT_ALLOC_CHRG_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_default_alloc_chrg_ind := GP_system_options_row.default_alloc_chrg_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_ALLOC_CHRG_IND;
-------------------------------------------------------------------------------
FUNCTION GET_CLASS_LEVEL_VAT_IND(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_class_level_vat_ind     OUT  SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE)
RETURN BOOLEAN is

   L_program        VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_CLASS_LEVEL_VAT_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_class_level_vat_ind := GP_system_options_row.class_level_vat_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CLASS_LEVEL_VAT_IND;
-------------------------------------------------------------------------------
FUNCTION GET_STKLDGR_VAT_INCL_RETL_IND(O_error_message              IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_stkldgr_vat_incl_retl_ind  IN OUT  SYSTEM_OPTIONS.STKLDGR_VAT_INCL_RETL_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_STKLDGR_VAT_INCL_RETL_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_stkldgr_vat_incl_retl_ind := GP_system_options_row.stkldgr_vat_incl_retl_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STKLDGR_VAT_INCL_RETL_IND;
-------------------------------------------------------------------------------
FUNCTION GET_CYCLE_COUNT_LAG_DAYS(O_error_message         IN OUT VARCHAR2,
                                  O_cycle_count_lag_days  IN OUT SYSTEM_OPTIONS.CYCLE_COUNT_LAG_DAYS%TYPE)
RETURN BOOLEAN IS
   LP_cycle_count_lag_days   SYSTEM_OPTIONS.CYCLE_COUNT_LAG_DAYS%TYPE;
   L_program                 VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_CYCLE_COUNT_LAG_DAYS';

   cursor C_CYCLE_COUNT_LAG_DAYS is
      select cycle_count_lag_days
        from system_options;
BEGIN

   if LP_cycle_count_lag_days is NULL then

      SQL_LIB.SET_MARK('OPEN', 'C_CYCLE_COUNT_LAG_DAYS', 'system_options', NULL);
      open C_CYCLE_COUNT_LAG_DAYS;
      SQL_LIB.SET_MARK('FETCH', 'C_CYCLE_COUNT_LAG_DAYS', 'system_options', NULL);
      fetch C_CYCLE_COUNT_LAG_DAYS into LP_cycle_count_lag_days;
      SQL_LIB.SET_MARK('CLOSE', 'C_CYCLE_COUNT_LAG_DAYS', 'system_options', NULL);
      close C_CYCLE_COUNT_LAG_DAYS;

   end if;

   O_cycle_count_lag_days := LP_cycle_count_lag_days;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CYCLE_COUNT_LAG_DAYS;
-------------------------------------------------------------------------------
FUNCTION GET_GROCERY_ITEMS_IND(O_error_message     IN OUT VARCHAR2,
                               O_grocery_items_ind IN OUT SYSTEM_OPTIONS.GROCERY_ITEMS_IND%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_GROCERY_ITEMS_IND';

   cursor C_GROCERY_ITEMS_IND is
      select grocery_items_ind
        from system_options;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GROCERY_ITEMS_IND', 'system_options', NULL);
   open C_GROCERY_ITEMS_IND;
   SQL_LIB.SET_MARK('FETCH', 'C_GROCERY_ITEMS_IND', 'system_options', NULL);
   fetch C_GROCERY_ITEMS_IND into O_grocery_items_ind;
   SQL_LIB.SET_MARK('CLOSE', 'C_GROCERY_ITEMS_IND', 'system_options', NULL);
   close C_GROCERY_ITEMS_IND;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_GROCERY_ITEMS_IND;
-------------------------------------------------------------------------------
FUNCTION GET_NWP_IND(O_error_message  IN OUT VARCHAR2,
                     O_nwp_ind        IN OUT SYSTEM_OPTIONS.NWP_IND%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_NWP_IND';

   cursor C_NWP_IND is
      select NVL(nwp_ind, 'N')
        from system_options;
BEGIN

   if LP_nwp_ind is NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_NWP_IND', 'system_options', NULL);
      open C_NWP_IND;
      SQL_LIB.SET_MARK('FETCH', 'C_NWP_IND', 'system_options', NULL);
      fetch C_NWP_IND into LP_nwp_ind;
      SQL_LIB.SET_MARK('CLOSE', 'C_NWP_IND', 'system_options', NULL);
      close C_NWP_IND;
   end if;
   ---
   O_nwp_ind := LP_nwp_ind;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NWP_IND;
-------------------------------------------------------------------------------
FUNCTION GET_DATA_LEVEL_SECURITY_IND(O_error_message            IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_data_level_security_ind     OUT  SYSTEM_OPTIONS.data_level_security_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_DATA_LEVEL_SECURITY_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_data_level_security_ind := GP_system_options_row.data_level_security_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DATA_LEVEL_SECURITY_IND;
-------------------------------------------------------------------------------
FUNCTION GET_WRONG_ST_RECEIPT_IND(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_wrong_st_receipt_ind     OUT  SYSTEM_OPTIONS.WRONG_ST_RECEIPT_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_WRONG_ST_RECEIPT_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_wrong_st_receipt_ind := GP_system_options_row.wrong_st_receipt_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_WRONG_ST_RECEIPT_IND;
-------------------------------------------------------------------------------
FUNCTION CHECK_RECORD_LOCKED(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS
   L_program        VARCHAR2(50)              := 'SYSTEM_OPTIONS_SQL.CHECK_RECORD_LOCKED';
   ---
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_SYSTEM_OPTIONS is
      select 'x'
        from system_config_options sy
            ,security_config_options sc
            ,functional_config_options fc
            ,default_func_config_options df
            ,ui_config_options uc
            ,default_ui_config_options du
            ,purge_config_options pu
            ,localization_config_options lo
            ,foundation_unit_options fo
            ,procurement_unit_options po
            ,inv_move_unit_options im
            ,inv_track_unit_options it
            ,financial_unit_options fi
            ,rtm_unit_options ru
       where rownum = 1
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    NULL);
   open C_LOCK_SYSTEM_OPTIONS;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    NULL);
   close C_LOCK_SYSTEM_OPTIONS;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED_VIEW',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_RECORD_LOCKED;
-------------------------------------------------------------------------------
FUNCTION GET_TSF_FORCE_CLOSE_IND(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_tsf_force_close_ind   IN OUT   SYSTEM_OPTIONS.TSF_FORCE_CLOSE_IND%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_TSF_FORCE_CLOSE_IND';

   cursor C_TSF_FORCE_CLOSE_IND is
      select tsf_force_close_ind
        from system_options;
BEGIN

   if LP_tsf_force_close_ind is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_TSF_FORCE_CLOSE_IND',
                       'system_options',
                       NULL);
      open C_TSF_FORCE_CLOSE_IND;

      SQL_LIB.SET_MARK('FETCH',
                       'C_TSF_FORCE_CLOSE_IND',
                       'system_options',
                       NULL);
      fetch C_TSF_FORCE_CLOSE_IND into LP_tsf_force_close_ind;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_TSF_FORCE_CLOSE_IND',
                       'system_options',
                       NULL);
      close C_TSF_FORCE_CLOSE_IND;
   end if;
   ---
   O_tsf_force_close_ind := LP_tsf_force_close_ind;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_TSF_FORCE_CLOSE_IND;
--------------------------------------------------------------------------------
FUNCTION GET_MIN_MAX_CMO(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_min_cum_markon_pct   IN OUT   SYSTEM_OPTIONS.MIN_CUM_MARKON_PCT%TYPE,
                         O_max_cum_markon_pct   IN OUT   SYSTEM_OPTIONS.MAX_CUM_MARKON_PCT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64)  := 'SYSTEM_OPTIONS_SQL.GET_MIN_MAX_CMO';

   cursor C_MIN_MAX_CMO is
      select min_cum_markon_pct,
             max_cum_markon_pct
        from system_options;
BEGIN

   if not LP_cum_markon_ind then
      SQL_LIB.SET_MARK('OPEN',
                       'C_MIN_MAX_CMO',
                       'system_options',
                       NULL);
      open C_MIN_MAX_CMO;

      SQL_LIB.SET_MARK('FETCH',
                       'C_MIN_MAX_CMO',
                       'system_options',
                       NULL);
      fetch C_MIN_MAX_CMO into LP_min_cum_markon_pct,
                               LP_max_cum_markon_pct;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_MIN_MAX_CMO',
                       'system_options',
                       NULL);
      close C_MIN_MAX_CMO;

      LP_cum_markon_ind := TRUE;
   end if;
   ---
   O_min_cum_markon_pct := LP_min_cum_markon_pct;
   O_max_cum_markon_pct := LP_max_cum_markon_pct;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_MIN_MAX_CMO;
-------------------------------------------------------------------------------
FUNCTION GET_STORE_PACK_COMP_RCV_IND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_store_pack_comp_rcv_ind IN OUT SYSTEM_OPTIONS.STORE_PACK_COMP_RCV_IND%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_STORE_PACK_COMP_RCV_IND';


BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_store_pack_comp_rcv_ind := GP_system_options_row.store_pack_comp_rcv_ind;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_STORE_PACK_COMP_RCV_IND;
-------------------------------------------------------------------------------
FUNCTION GET_DUPLICATE_RECEIVING_IND(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_duplicate_receiving_ind IN OUT SYSTEM_OPTIONS.DUPLICATE_RECEIVING_IND%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_DUPLICATE_RECEIVING_IND';


BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_duplicate_receiving_ind := GP_system_options_row.duplicate_receiving_ind;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_DUPLICATE_RECEIVING_IND;
-------------------------------------------------------------------------------
FUNCTION GET_INCREASE_TSF_QTY_IND(O_error_message           IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_increase_tsf_qty_ind    IN OUT    SYSTEM_OPTIONS.INCREASE_TSF_QTY_IND%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_INCREASE_TSF_QTY_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_increase_tsf_qty_ind := GP_system_options_row.increase_tsf_qty_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_INCREASE_TSF_QTY_IND;
--------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER_SITES_IND(O_error_message           IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                O_supplier_sites_ind      IN OUT    SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_SUPPLIER_SITES_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_supplier_sites_ind := GP_system_options_row.supplier_sites_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_SUPPLIER_SITES_IND;
--------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_TAX_TYPE(O_error_message           IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                              O_default_tax_type        IN OUT    SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE';
   
   cursor C_DEF_TAX is
   select default_tax_type
     from system_options;

BEGIN

   if GP_options_populated = FALSE then
      open C_DEF_TAX;
      fetch C_DEF_TAX into O_default_tax_type;
      close C_DEF_TAX;
   else
      O_default_tax_type := GP_system_options_row.default_tax_type;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_TAX_TYPE;
--------------------------------------------------------------------------------
FUNCTION GET_OTB_SYSTEM_IND(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_otb_system_ind      OUT   SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'SYSTEM_OPTIONS_SQL.OTB_SYSTEM_IND';

BEGIN

   if GP_options_populated = FALSE then
      if POPULATE_SYSTEM_OPTIONS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   O_otb_system_ind := GP_system_options_row.otb_system_ind;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_OTB_SYSTEM_IND;
--------------------------------------------------------------------------------
END SYSTEM_OPTIONS_SQL;
/