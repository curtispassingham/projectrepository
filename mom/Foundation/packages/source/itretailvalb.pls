create or replace
PACKAGE BODY ITEM_RETAIL_VALIDATION_SQL AS
--------------------------------------------------------------------------------
--   PRIVATE  FUNCTIONS
--------------------------------------------------------------------------------------
FUNCTION CALC_LANDED_COST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_landed_cost_prim  IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                          I_zone_id           IN       COST_ZONE.ZONE_ID%TYPE,
                          I_item              IN       ITEM_MASTER.ITEM%TYPE,
                          I_rpm_zone_group_id IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------
FUNCTION CALC_LANDED_COST(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_landed_cost_prim  IN OUT   ITEM_LOC_SOH.UNIT_COST%TYPE,
                          I_zone_id           IN       COST_ZONE.ZONE_ID%TYPE,
                          I_item              IN       ITEM_MASTER.ITEM%TYPE,
                          I_rpm_zone_group_id IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE) 
    RETURN BOOLEAN IS
   L_program              VARCHAR2(61)                   := 'ITEM_RETAIL_VALIDATION_SQL.CALC_LANDED_COST';
   L_error_message        VARCHAR2(255);
   L_pack_ind             ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type            VARCHAR2(1);
   L_cost_zone_group_id   COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_cost_zone_id         COST_ZONE.ZONE_ID%TYPE;
   L_cost_zone_desc       COST_ZONE.DESCRIPTION%TYPE;
   L_total_exp            ORDLOC_EXP.EST_EXP_VALUE%TYPE;
   L_exp_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_exp    CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_total_dty            ORDLOC_EXP.EST_EXP_VALUE%TYPE;
   L_dty_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_location_table       OBJ_RPM_LOC_TBL;
   L_location             COST_ZONE_GROUP_LOC.LOCATION%TYPE;

   
BEGIN
   ---
   if I_item is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_zone_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_zone_id', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_PACK_INDS(L_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      O_error_message := L_error_message;
      return false;
   end if;

   if L_orderable_ind = 'N' or
      (L_orderable_ind = 'Y' and L_pack_type = 'V') then
      return true;
   end if;
   ---
   PM_RETAIL_API_SQL.GET_ZONE_LOCATIONS(L_location_table,
                                        I_rpm_zone_group_id,
                                        I_zone_id);
   
   if ITEM_ATTRIB_SQL.GET_COST_ZONE_GROUP(L_error_message,
                                          L_cost_zone_group_id,
                                          I_item) = FALSE then
      O_error_message := L_error_message;
      return false;
   end if;
   
   if L_location_table is NULL or L_location_table.COUNT = 0 then 
      L_location := NULL;
   else
      L_location := L_location_table(L_location_table.COUNT).location_id;
   end if;
   
   if COST_ZONE_SQL.GET_FIRST_COST_ZONE_ID(L_error_message,
                                           L_cost_zone_id,
                                           L_cost_zone_desc,
                                           L_cost_zone_group_id,
                                           L_location) = FALSE then
      O_error_message := L_error_message;
      return false;
   end if;
   ---
   if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                               O_landed_cost_prim,
                               L_total_exp,
                               L_exp_currency,
                               L_exchange_rate_exp,
                               L_total_dty,
                               L_dty_currency,
                               NULL,
                               I_item,
                               NULL,
                               L_cost_zone_id,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL) = FALSE then
        O_error_message := L_error_message;
     return false;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CALC_LANDED_COST;
---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_SELLING_UNIT_RETAIL(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_selling_mark_up               IN OUT   RETAIL_CALC_TEMP.SELLING_MARK_UP%TYPE,
                                 O_selling_unit_retail_prim      IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 O_unit_retail                   IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 O_selling_unit_retail           IN OUT   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                 O_unit_retail_euro              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 O_unit_retail_prim              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                 I_zone_id                       IN       COST_ZONE.ZONE_ID%TYPE,
                                 I_unit_cost_prm                 IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                 I_standard_uom                  IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                                 I_selling_uom                   IN       ITEM_LOC.SELLING_UOM%TYPE,
                                 I_item                          IN       ITEM_MASTER.ITEM%TYPE,
                                 I_currency_code                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_currency_prim                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_currency_euro                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_elc_ind                       IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                                 I_rpm_zone_group_id             IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE,
                                 I_multi_selling_uom             IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                 I_multi_unit_retail             IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                 I_multi_units                   IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                 I_standard_uom_trans            IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                                 I_block_query_executed          IN       VARCHAR2,
                                 I_markup_calc_type              IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                                 I_dept                          IN       DEPS.DEPT%TYPE)
   RETURN BOOLEAN IS
   
   L_program               VARCHAR2(61)                              := 'ITEM_RETAIL_VALIDATION_SQL.VAL_SELLING_UNIT_RETAIL';
   L_GV_block_query_executed BOOLEAN;
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
   L_markup_calc_type      DEPS.MARKUP_CALC_TYPE%TYPE                := I_markup_calc_type;
   L_location              PM_RETAIL_API_SQL.ZONE_ID%TYPE            := I_zone_id;
   L_selling_unit_retail   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE         := O_selling_unit_retail;
   L_unit_cost_prm         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE          := I_unit_cost_prm;
   L_currency_rtl_dec      CURRENCIES.CURRENCY_RTL_DEC%TYPE;
   L_dumb1                 CURRENCIES.CURRENCY_RTL_FMT%TYPE;
   L_dumb2                 CURRENCIES.CURRENCY_COST_FMT%TYPE;
   L_dumb3                 CURRENCIES.CURRENCY_COST_DEC%TYPE;
   L_loc_type              VARCHAR2(1)  := 'Z';
   L_return_code           VARCHAR2(5);
   L_qty                   NUMBER;
   L_retail                NUMBER;
   L_dec_place             NUMBER;
   L_dept                  NUMBER(4)    := I_dept;
   L_markup                NUMBER(12,4);

BEGIN

   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_zone_id is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_zone_id', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_elc_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_elc_ind', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   
   if I_block_query_executed ='Y' then
      L_GV_block_query_executed := TRUE;
   else
      L_GV_block_query_executed := FALSE;
   end if;
   if L_selling_unit_retail is NULL then
      O_selling_mark_up := NULL;
      O_selling_unit_retail_prim := NULL;
      O_unit_retail := NULL;
   elsif L_selling_unit_retail < 0 then
      O_selling_unit_retail := NULL;
      O_selling_unit_retail_prim := NULL;
      O_selling_mark_up := NULL;
      return false;
   else
      if CURRENCY_SQL.GET_FORMAT(L_error_message,
                                 I_currency_code,
                                 L_dumb1,
                                 L_currency_rtl_dec,
                                 L_dumb2,
                                 L_dumb3) = FALSE then
         O_error_message := L_error_message;
         return false;
      end if;
      ---
      L_selling_unit_retail := to_char(L_selling_unit_retail);
      ---
      if L_dec_place != 0 then
         if L_currency_rtl_dec < NVL(LENGTH(substr(L_selling_unit_retail, L_dec_place + 1)), 0) then
            O_error_message := SQL_LIB.CREATE_MSG('INV_RTL_DEC',
                                                   to_char(L_currency_rtl_dec),
                                                   NULL,
                                                   NULL);
            return false;
         end if;
      end if;
      ---
      --- primary currency information ---
      if not CURRENCY_SQL.CONVERT(L_error_message,
                                  O_selling_unit_retail,
                                  I_currency_code,
                                  I_currency_prim,
                                  O_selling_unit_retail_prim,
                                  'R',
                                  NULL,
                                  NULL)then
         O_selling_unit_retail_prim := NULL;
         O_error_message := L_error_message;
         return false;
      end if;
      ---
      --- Euro currency information ---
      if not CURRENCY_SQL.CONVERT(L_error_message,
                                  O_selling_unit_retail,
                                  I_currency_code,
                                  I_currency_euro,
                                  O_unit_retail_euro,
                                  'R',
                                  NULL,
                                  NULL) then
         O_unit_retail_euro := NULL;
         O_error_message := L_error_message;
         return false;
      end if;
      ---
     if I_elc_ind = 'Y' and I_unit_cost_prm != 0 then
        if not CALC_LANDED_COST(L_error_message,
                                L_unit_cost_prm,
                                I_zone_id,
                                I_item,
                                I_rpm_zone_group_id) then 

           O_error_message := L_error_message;
           return false;
        end if; 
     end if;
      ---
      if O_selling_unit_retail is not NULL and
         I_selling_uom is not NULL then
         if UOM_SQL.CONVERT(L_error_message,
                            L_qty,
                            I_standard_uom_trans,
                            1,
                            I_selling_uom,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                NULL,
                                                NULL,
                                                NULL);
            return false;
         end if;
         ---
         O_unit_retail := O_selling_unit_retail / L_qty;
         ---
         if not CURRENCY_SQL.ROUND_CURRENCY(L_error_message,
                                           O_unit_retail,
                                           I_currency_code,
                                           'R') then
           O_error_message := L_error_message;
            return false;
         end if;
         ---
      end if;
      ---
      --- primary currency information ---
      if not CURRENCY_SQL.CONVERT(L_error_message,
                                  O_unit_retail,
                                  I_currency_code,
                                  I_currency_prim,
                                  O_unit_retail_prim,
                                  'R',
                                  NULL,
                                  NULL)then
         O_unit_retail_prim := NULL;
         O_error_message := L_error_message;
         return false;
      end if;
      ---
      --- Euro currency information ---
      if not CURRENCY_SQL.CONVERT(L_error_message,
                                  O_unit_retail,
                                  I_currency_code,
                                  I_currency_euro,
                                  O_unit_retail_euro,
                                  'R',
                                  NULL,
                                  NULL) then
         O_unit_retail_euro := NULL;
         O_error_message := L_error_message;
         return false;
      end if;
      ---
      if I_multi_units is not NULL and
         I_multi_unit_retail is not NULL and
         I_multi_selling_uom is not NULL then
         if UOM_SQL.CONVERT(L_error_message,
                            L_qty,
                            I_multi_selling_uom,
                            I_multi_unit_retail,
                            I_selling_uom,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return false;
         end if;
         ---
         L_retail := L_qty / I_multi_units;
         ---
         if L_retail > O_selling_unit_retail then
            O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            O_selling_unit_retail := NULL;
            return false;
         end if;
         ---
         if L_qty < O_selling_unit_retail then
            O_error_message := SQL_LIB.CREATE_MSG('MULTI_RETAIL_LESS_SINGLE',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            O_selling_unit_retail := NULL;
            return false;
         end if;
      end if;
      ---
      if I_unit_cost_prm is not NULL and
         -- suppress call to compute markup percent if already populated by Execute_Query
         L_GV_block_query_executed = TRUE then
         if not MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM(L_error_message,
                                                    L_markup,
                                                    'Y',
                                                    L_markup_calc_type,
                                                    L_unit_cost_prm,
                                                    O_unit_retail_prim,
                                                    I_item,
                                                    NULL,
                                                    L_loc_type,
                                                    L_location,
                                                    NULL,
                                                    NULL,
                                                    NULL) then
            O_error_message := L_error_message;
         return false;
         end if;
      end if;
      ---
      O_selling_mark_up := L_markup;
   end if;
   
   if O_unit_retail_prim is NOT NULL then
      if O_unit_retail_prim = 0 then
         O_selling_mark_up := 0;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VAL_SELLING_UNIT_RETAIL;
-------------------------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_MULTI_UNITS (O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_multi_selling_uom              IN OUT   ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                          O_multi_selling_uom_trans        IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                          O_multi_units                    IN OUT   ITEM_LOC.MULTI_UNITS%TYPE,
                          O_multi_selling_mark_up          IN OUT   RETAIL_CALC_TEMP.MULTI_SELLING_MARK_UP%TYPE,
                          O_multi_unit_retail_prim         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                          O_multi_unit_retail_euro         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                          O_selling_unit_retail_prim       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                          O_selling_unit_retail_euro       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                          O_multi_unit_retail              IN OUT   ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                          I_unit_cost_prm                  IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          I_markup_calc_type               IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                          I_selling_uom                    IN       ITEM_LOC.SELLING_UOM%TYPE,
                          I_zone_id                        IN       COST_ZONE.ZONE_ID%TYPE,
                          I_selling_uom_trans              IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                          I_elc_ind                        IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                          I_multi_unit_retail_prim         IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                          I_item                           IN       ITEM_MASTER.ITEM%TYPE,
                          I_rpm_zone_group_id              IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE,
                          I_standard_uom_trans             IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                          I_selling_unit_retail            IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                          I_standard_uom                   IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                          I_unit_retail                    IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                          I_currency_code                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                          I_currency_prim                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                          I_currency_euro                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                          I_block_query_executed           IN       VARCHAR2)
   RETURN BOOLEAN IS
   L_program               VARCHAR2(61)                              := 'ITEM_RETAIL_VALIDATION_SQL.VAL_MULTI_UNITS';
   L_GV_block_query_executed BOOLEAN;
   L_retail          ITEM_LOC.UNIT_RETAIL%TYPE;
   L_retail_prm      ITEM_LOC.UNIT_RETAIL%TYPE;
   L_cost            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE     := I_unit_cost_prm;
   L_markup          NUMBER(12,4);
   L_markup_type     DEPS.MARKUP_CALC_TYPE%TYPE := I_markup_calc_type;
   L_return_code     VARCHAR2(5);
   L_error_message   VARCHAR2(250);
   L_loc_type        VARCHAR2(1);
   L_location        ORDLOC.LOCATION%TYPE;
   L_qty             NUMBER;

BEGIN
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_zone_id is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_zone_id', 
                                             L_program, 
                                             NULL);
   elsif I_elc_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_elc_ind', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_block_query_executed ='Y' then
      L_GV_block_query_executed := TRUE;
   else
      L_GV_block_query_executed := FALSE;
   end if;
   L_loc_type := 'Z';
   L_location := I_zone_id;
   ---
   if O_multi_units is not NULL then
      if O_multi_selling_uom is NULL or O_multi_selling_uom_trans is NOT NULL then
         O_multi_selling_uom := I_selling_uom;
         O_multi_selling_uom_trans := I_selling_uom_trans;
      end if;
      O_multi_units := round(O_multi_units,0);
      if O_multi_units < 2 then
         O_error_message := SQL_LIB.CREATE_MSG('GREATER_1',
                                               NULL,
                                               NULL,
                                               NULL);
         O_multi_units := NULL;
         O_multi_selling_mark_up := NULL;
         return false;
      elsif O_multi_unit_retail_prim is not NULL then
         if I_elc_ind = 'Y' and I_unit_cost_prm != 0 then
            if not CALC_LANDED_COST(L_error_message,
                                    L_cost,
                                    I_zone_id,
                                    I_item,
                                    I_rpm_zone_group_id) then 
               O_error_message := L_error_message;
               return false;
            end if; 
         end if;
         ---
         if O_multi_units is not NULL and O_multi_unit_retail is not NULL
            and O_multi_selling_uom is not NULL then
            if UOM_SQL.CONVERT(L_error_message,
                               L_qty,
                               O_multi_selling_uom,
                               O_multi_unit_retail,
                               I_selling_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            if L_qty = 0 then
               O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                               NULL,
                                               NULL,
                                               NULL);
               return false;
            end if;
            ---
            L_retail := L_qty / O_multi_units;
            ---
            if L_retail > I_selling_unit_retail then
               O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS',
                                                      NULL,
                                                      NULL,
                                                      NULL);
                  O_multi_units := NULL;
               return false;
            end if;
            ---
            if L_qty < I_selling_unit_retail then
               O_error_message := SQL_LIB.CREATE_MSG('MULTI_RETAIL_LESS_SINGLE',
                                                      NULL,
                                                      NULL,
                                                      NULL);
               O_multi_units := NULL;
               return false;
            end if;
            ---
            if I_standard_uom != I_selling_uom then
                 L_retail := I_unit_retail;
            end if;
            --- primary currency information ---
            if not CURRENCY_SQL.CONVERT(L_error_message,
                                        O_multi_unit_retail,
                                        I_currency_code,
                                        I_currency_prim,
                                        O_multi_unit_retail_prim,
                                        'R',
                                        NULL,
                                        NULL)then
               O_selling_unit_retail_prim := NULL;
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            --- Euro currency information ---
            if not CURRENCY_SQL.CONVERT(L_error_message,
                                        O_multi_unit_retail,
                                        I_currency_code,
                                        I_currency_euro,
                                        O_multi_unit_retail_euro,
                                        'R',
                                        NULL,
                                        NULL) then
               O_selling_unit_retail_euro := NULL;
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            --- primary currency information ---
            if not CURRENCY_SQL.CONVERT(L_error_message,
                                        L_retail,
                                        I_currency_code,
                                        I_currency_prim,
                                        L_retail_prm,
                                        'R',
                                        NULL,
                                        NULL)then
               L_retail_prm := NULL;
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            ---
            if I_unit_cost_prm is not NULL and
               -- suppress call to compute markup percent if already populated by Execute_Query
               L_GV_block_query_executed = TRUE then
               if NOT MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM(L_error_message,
                                                          L_markup,
                                                          'Y',
                                                          L_markup_type,
                                                          L_cost,
                                                          L_retail_prm,
                                                          I_item,
                                                          NULL,
                                                          L_loc_type,
                                                          L_location,
                                                          NULL) then
                  O_error_message := L_error_message;
                  return false;
               end if;
            end if;
            O_multi_selling_mark_up := L_markup;
         end if;
      end if;
   else
      O_multi_selling_mark_up := NULL;
      O_multi_selling_uom := NULL;
      O_multi_unit_retail := NULL;
   end if;

   if L_retail_prm is NOT NULL then
      if L_retail_prm = 0 then
         O_multi_selling_mark_up := 0;
      end if;
   end if;
   return true;
  EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VAL_MULTI_UNITS;
-----------------------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_SELLING_MARK_UP (O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_selling_mark_up                IN OUT   RETAIL_CALC_TEMP.SELLING_MARK_UP%TYPE,
                              O_old_mkup                       IN OUT   NUMBER,
                              O_unit_retail_prim               IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_selling_uom_trans              IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                              O_selling_unit_retail_prim       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_selling_unit_retail            IN OUT   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                              O_selling_uom                    IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                              O_multi_unit_retail_prim         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_multi_unit_retail_euro         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              O_unit_retail                    IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                              I_unit_cost_prm                  IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              I_markup_calc_type               IN       DEPS.MARKUP_CALC_TYPE%TYPE, 
                              I_zone_id                        IN       COST_ZONE.ZONE_ID%TYPE,
                              I_standard_uom                   IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                              I_standard_uom_trans             IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                              I_elc_ind                        IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                              I_item                           IN       ITEM_MASTER.ITEM%TYPE,
                              I_rpm_zone_group_id              IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE,
                              I_currency_code                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                              I_dept                           IN       DEPS.DEPT%TYPE,
                              I_currency_prim                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                              I_multi_units                    IN       ITEM_LOC.MULTI_UNITS%TYPE,
                              I_multi_unit_retail              IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                              I_multi_selling_uom              IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                              I_currency_euro                  IN       CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN IS 
   
   L_program            VARCHAR2(61)                     := 'ITEM_RETAIL_VALIDATION_SQL.VAL_SELLING_MARK_UP';
   L_markup             NUMBER(12,4)                     := O_selling_mark_up;
   L_retail             ITEM_LOC.UNIT_RETAIL%TYPE        := O_unit_retail;
   L_cost               ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := I_unit_cost_prm;
   L_markup_type        DEPS.MARKUP_CALC_TYPE%TYPE       := I_markup_calc_type;
   L_return_code        VARCHAR2(5);
   L_error_message      VARCHAR2(250);
   L_loc_type           VARCHAR2(1)                      := 'Z';
   L_location           ORDLOC.LOCATION%TYPE             := I_zone_id;
   L_unit_cost_prm      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := I_unit_cost_prm;
   L_qty                NUMBER;

BEGIN
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_zone_id is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_zone_id', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_elc_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_elc_ind', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   if L_markup is NOT NULL then
      if O_selling_uom is NULL or O_selling_uom_trans is NULL then
         O_selling_uom := I_standard_uom;
         O_selling_uom_trans := I_standard_uom_trans;
      end if;
      ---
      if nvl(O_old_mkup,0) != O_selling_mark_up then
         if I_elc_ind = 'Y' then
            if not CALC_LANDED_COST(L_error_message,
                                    L_unit_cost_prm,
                                    I_zone_id,
                                    I_item,
                                    I_rpm_zone_group_id) then 
               O_error_message := L_error_message;
               return false;
            end if; 
         end if;
         ---
         if I_unit_cost_prm is NOT NULL then
            if MARKUP_SQL.CALC_RETAIL_FROM_MARKUP(L_error_message,
                                                  L_retail,
                                                  L_markup,
                                                  L_markup_type,
                                                  L_unit_cost_prm,
                                                  I_item,
                                                  L_loc_type,
                                                  L_location,
                                                  I_dept,
                                                  NULL,
                                                  I_currency_code,
                                                  NULL,
                                                  NULL) = FALSE then
               O_error_message := L_error_message;
               O_unit_retail      := NULL;
               O_unit_retail_prim := NULL;
               O_selling_mark_up  := NULL;
               return false;
            elsif L_retail > 9999999999999999 or L_retail < -100 then
               O_error_message := SQL_LIB.CREATE_MSG('RETAIL_OUT_OF_RANGE',
                                                      NULL,
                                                      NULL,
                                                      NULL);
               O_unit_retail      := NULL;
               O_unit_retail_prim := NULL;
               O_selling_mark_up  := NULL;
               O_old_mkup     := -101;
               return false;
            end if;
         end if;
         ---
         O_unit_retail_prim := L_retail;
         ---
         if O_selling_unit_retail is not NULL
            and O_selling_uom is not NULL then
            if UOM_SQL.CONVERT(L_error_message,
                               O_selling_unit_retail_prim,
                               I_standard_uom,
                               O_unit_retail_prim,
                               O_selling_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            if O_selling_unit_retail_prim = 0 and O_unit_retail_prim!= 0 then
               O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                      NULL,
                                                      NULL,
                                                      NULL);
               return false;
            end if;
            ---
            if not CURRENCY_SQL.ROUND_CURRENCY(L_error_message,
                                              O_selling_unit_retail_prim,
                                              I_currency_code,
                                              'R') then
                O_error_message := L_error_message;
                return false;
            end if;
            ---
         end if;
         --- Convert from primary to local, bringing back 'smart' retail if necessary
         if NOT CURRENCY_SQL.CONVERT(L_error_message,
                                     O_unit_retail_prim,
                                     I_currency_prim,
                                     I_currency_code,
                                     O_unit_retail,
                                     'P', --- 'P' stands for PSMART retail
                                     NULL,
                                     NULL)then
            O_unit_retail_prim := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if NOT CURRENCY_SQL.CONVERT(L_error_message,
                                     O_selling_unit_retail_prim,
                                     I_currency_prim,
                                     I_currency_code,
                                     O_selling_unit_retail,
                                     'P', --- 'P' stands for PSMART retail
                                     NULL,
                                     NULL)then
            O_selling_unit_retail_prim := NULL;
            O_error_message := L_error_message;
            return false;
         end if;

         --- Re-convert to primary to calculate markup using 'smart' retail
         if NOT CURRENCY_SQL.CONVERT(L_error_message,
                                     O_unit_retail,
                                     I_currency_code,
                                     I_currency_prim,
                                     O_unit_retail_prim,
                                     'R',
                                     NULL,
                                     NULL)then
            O_unit_retail_prim := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if NOT CURRENCY_SQL.CONVERT(L_error_message,
                                     O_selling_unit_retail,
                                     I_currency_code,
                                     I_currency_prim,
                                     O_selling_unit_retail_prim,
                                     'R',
                                     NULL,
                                     NULL)then
            O_selling_unit_retail_prim := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if I_multi_units is NOT NULL and I_multi_unit_retail is NOT NULL and
            I_multi_selling_uom is NOT NULL then
            if UOM_SQL.CONVERT(L_error_message,
                               L_qty,
                               I_multi_selling_uom,
                               I_multi_unit_retail,
                               O_selling_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            if L_qty = 0 then
               O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                               NULL,
                                               NULL,
                                               NULL);
               return false;
            end if;
            ---
            L_retail := L_qty / I_multi_units;
            ---
            if L_retail > O_selling_unit_retail then
               O_error_message := SQL_LIB.CREATE_MSG('S_GREATER_M_MARKUP',
                                               NULL,
                                               NULL,
                                               NULL);
               O_unit_retail := NULL;
               O_unit_retail_prim := NULL;
               O_selling_unit_retail := NULL;
               O_selling_unit_retail_prim := NULL;
               O_selling_mark_up := NULL;
               O_old_mkup := 0;
               return false;
            end if;
            ---
         end if;
         --- primary currency information ---
         if NOT CURRENCY_SQL.CONVERT(L_error_message,
                                     I_multi_unit_retail,
                                     I_currency_code,
                                     I_currency_prim,
                                     O_multi_unit_retail_prim,
                                     'R',
                                     NULL,
                                     NULL)then
            O_multi_unit_retail_prim := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         --- Euro currency information ---
         if NOT CURRENCY_SQL.CONVERT(L_error_message,
                                     I_multi_unit_retail,
                                     I_currency_code,
                                     I_currency_euro,
                                     O_multi_unit_retail_euro,
                                     'R',
                                     NULL,
                                     NULL) then
            O_multi_unit_retail_euro := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         --- Call CALC_MARKUP_PERCENT to re-calculate the markup based on the rounded
         --- new retail price.
         if I_unit_cost_prm is NOT NULL then
            if NOT MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM(L_error_message,
                                                       L_markup,
                                                      'Y',
                                                       L_markup_type,
                                                       L_cost,
                                                       O_unit_retail_prim,
                                                       I_item,
                                                       NULL,
                                                       L_loc_type,
                                                       L_location,
                                                       NULL) then
               O_error_message := L_error_message;
               return false;
            end if;
            O_selling_mark_up := L_markup;
         end if;
      ---
      end if; --- old_markup != new_markup
   ---
   else
      O_unit_retail              := NULL;
      O_unit_retail_prim         := NULL;
      O_selling_unit_retail      := NULL;
      O_selling_unit_retail_prim := NULL;
      O_selling_mark_up          := NULL;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VAL_SELLING_MARK_UP;
---------------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_MULTI_UNIT_RETAIL( O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_multi_unit_retail_prim         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_multi_unit_retail_euro         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_unit_retail_euro               IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_multi_selling_mark_up          IN OUT   RETAIL_CALC_TEMP.MULTI_SELLING_MARK_UP%TYPE,
                                O_multi_unit_retail              IN OUT   ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                I_unit_cost_prm                  IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                I_markup_calc_type               IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                                I_zone_id                        IN       COST_ZONE.ZONE_ID%TYPE,
                                I_currency_code                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_currency_prim                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_multi_units                    IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                I_currency_euro                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_multi_selling_uom              IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                I_selling_uom                    IN       ITEM_LOC.SELLING_UOM%TYPE,
                                I_item                           IN       ITEM_MASTER.ITEM%TYPE,
                                I_standard_uom                   IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                                I_standard_uom_trans             IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                                I_selling_unit_retail            IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                I_elc_ind                        IN       SYSTEM_OPTIONS.ELC_IND%TYPE,
                                I_rpm_zone_group_id              IN       RETAIL_CALC_TEMP.RPM_ZONE_GROUP_ID%TYPE,
                                I_block_query_executed           IN       VARCHAR2)
RETURN BOOLEAN IS 
   L_program            VARCHAR2(61)                     := 'ITEM_RETAIL_VALIDATION_SQL.VAL_MULTI_UNIT_RETAIL';                        
   L_retail             ITEM_LOC.UNIT_RETAIL%TYPE;
   L_retail_convert     ITEM_LOC.UNIT_RETAIL%TYPE;
   L_retail_prm         ITEM_LOC.UNIT_RETAIL%TYPE;
   L_cost               ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := I_unit_cost_prm;
   L_markup_type        DEPS.MARKUP_CALC_TYPE%TYPE := I_markup_calc_type;
   L_markup             NUMBER(12,4);
   L_return_code        VARCHAR2(5);
   L_error_message      VARCHAR2(250);
   L_loc_type           VARCHAR2(1);
   L_location           ORDLOC.LOCATION%TYPE;
   L_qty                NUMBER;
   L_unit_retail        NUMBER;
   L_m_unit_retail      ITEM_LOC.UNIT_RETAIL%TYPE := to_char(O_multi_unit_retail);
   L_currency_rtl_dec   CURRENCIES.CURRENCY_RTL_DEC%TYPE;
   L_dumb1              CURRENCIES.CURRENCY_RTL_FMT%TYPE;
   L_dumb2              CURRENCIES.CURRENCY_COST_FMT%TYPE;
   L_dumb3              CURRENCIES.CURRENCY_COST_DEC%TYPE;
   L_dec_place          NUMBER;
   L_GV_block_query_executed BOOLEAN;

BEGIN
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_zone_id is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_zone_id', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_elc_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_elc_ind', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_block_query_executed ='Y' then
      L_GV_block_query_executed := TRUE;
   else
      L_GV_block_query_executed := FALSE;
   end if;
   L_loc_type := 'Z';
   L_location := I_zone_id;
   ---
   if O_multi_unit_retail is not NULL then
      ---
      if O_multi_unit_retail >= 0 then
         if CURRENCY_SQL.GET_FORMAT(L_error_message,
                                    I_currency_code,
                                    L_dumb1,
                                    L_currency_rtl_dec,
                                    L_dumb2,
                                    L_dumb3) = FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         --- Primary currency information ---
         if not CURRENCY_SQL.CONVERT(L_error_message,
                                     O_multi_unit_retail,
                                     I_currency_code,
                                     I_currency_prim,
                                     O_multi_unit_retail_prim,
                                     'R',
                                     NULL,
                                     NULL)then
            O_multi_unit_retail_prim := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         --- Euro currency information ---
         if not CURRENCY_SQL.CONVERT(L_error_message,
                                     O_multi_unit_retail,
                                     I_currency_code,
                                     I_currency_euro,
                                     O_multi_unit_retail_euro,
                                     'R',
                                     NULL,
                                     NULL) then
            O_unit_retail_euro := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if I_multi_units is not NULL and O_multi_unit_retail is not NULL
            and I_multi_selling_uom is not NULL then
            if UOM_SQL.CONVERT(L_error_message,
                               L_qty,
                               I_multi_selling_uom,
                               O_multi_unit_retail,
                               I_selling_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            if L_qty = 0 then
               O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                      NULL,
                                                      NULL,
                                                      NULL);
               return false;
            end if;
            ---
            L_retail := L_qty / I_multi_units;
            ---
            if L_retail > I_selling_unit_retail then
               O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS',
                                               NULL,
                                               NULL,
                                               NULL);
                 O_multi_unit_retail := NULL;
                 return false;
            end if;
            ---
            if L_qty < I_selling_unit_retail then
               O_error_message := SQL_LIB.CREATE_MSG('MULTI_RETAIL_LESS_SINGLE',
                                                      NULL,
                                                      NULL,
                                                      NULL);
               O_multi_unit_retail := NULL;
               return false;
            end if;
            ---
            if I_multi_units is not NULL and O_multi_unit_retail is not NULL
               and I_multi_selling_uom is not NULL then
               --Convert the retail calculated from the multi UOM back to the standard UOM
               if UOM_SQL.CONVERT(L_error_message,
                                  L_retail_convert,
                                  I_selling_uom,
                                  L_retail,
                                  I_standard_uom,
                                  I_item,
                                  NULL,
                                  NULL) = FALSE then
                  O_error_message := L_error_message;
                  return false;
               end if;
            ---
            end if;
            if I_elc_ind = 'Y' and I_unit_cost_prm != 0 then
               if not CALC_LANDED_COST(L_error_message,
                                       L_cost,
                                       I_zone_id, 
                                       I_item,
                                       I_rpm_zone_group_id) then
                   O_error_message := L_error_message;
                   return false;
               end if; 
            end if;
            ---
            --- primary currency information ---
            if not CURRENCY_SQL.CONVERT(L_error_message,
                                       O_multi_unit_retail,
                                       I_currency_code,
                                       I_currency_prim,
                                       O_multi_unit_retail_prim,
                                       'R',
                                       NULL,
                                       NULL)then
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            --- Euro currency information ---
            if not CURRENCY_SQL.CONVERT(L_error_message,
                                        O_multi_unit_retail,
                                        I_currency_code,
                                        I_currency_euro,
                                        O_multi_unit_retail_euro,
                                        'R',
                                        NULL,
                                        NULL) then
               O_multi_unit_retail_euro := NULL;
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            --Convert the retail convert to the standard UOM to the primary.
            --- primary currency information ---
            if not CURRENCY_SQL.CONVERT(L_error_message,
                                        L_retail_convert,
                                        I_currency_code,
                                        I_currency_prim,
                                        L_retail_prm,
                                        'R',
                                        NULL,
                                        NULL)then
               L_retail_prm := NULL;
               O_error_message := L_error_message;
               return false;
            end if;
            ---
            if I_unit_cost_prm is not NULL and
               -- suppress call to compute markup percent if already populated by Execute_Query
               L_GV_block_query_executed = TRUE then
               if NOT MARKUP_SQL.calc_markup_percent_item(L_error_message,
                                                          L_markup,
                                                         'Y',
                                                          L_markup_type,
                                                          L_cost,
                                                          L_retail_prm,
                                                          I_item,
                                                          NULL,
                                                          L_loc_type,
                                                          L_location,
                                                          NULL) then
                  O_error_message := L_error_message;
                  return false;
               end if;
               ---
               O_multi_selling_mark_up := L_markup;
            end if;
         end if;
      else
         O_error_message := SQL_LIB.CREATE_MSG('GREATER_0',
                                               NULL,
                                               NULL,
                                               NULL);
         return false;
      end if;
   else
      O_multi_selling_mark_up := NULL;
      O_multi_unit_retail_prim := NULL;
   end if;
   
   if L_retail_prm is NOT NULL then
      if L_retail_prm = 0 then
         O_multi_selling_mark_up := 0;
      end if;
   end if;
return true;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VAL_MULTI_UNIT_RETAIL;
------------------------------------------------------------------------------------------------------------
FUNCTION VAL_SELLING_UOM_TRANS (O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_uom                           IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                                O_unit_retail                   IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_unit_retail_prim              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_unit_retail_euro              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                O_selling_uom_trans             IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                                O_selling_uom                   IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                                O_selling_mark_up               IN OUT   RETAIL_CALC_TEMP.SELLING_MARK_UP%TYPE,
                                I_standard_uom_trans            IN       UOM_CLASS.UOM%TYPE,
                                I_zone_id                       IN       COST_ZONE.ZONE_ID%TYPE,
                                I_selling_unit_retail           IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                I_currency_code                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_markup_calc_type              IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                                I_unit_cost_prm                 IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                I_item                          IN       ITEM_MASTER.ITEM%TYPE,
                                I_standard_uom                  IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                                I_currency_prim                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_currency_euro                 IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                I_multi_unit_retail             IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                I_multi_units                   IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                I_multi_selling_uom             IN       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                I_block_query_executed          IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program          VARCHAR2(61)                   := 'ITEM_RETAIL_VALIDATION_SQL.VAL_SELLING_UOM_TRANS';
   L_error_message    VARCHAR2(255) := NULL;
   L_uom_desc         UOM_CLASS_TL.UOM_DESC_TRANS%TYPE;
   L_valid            BOOLEAN;
   L_qty              NUMBER;
   L_retail           ITEM_LOC.UNIT_RETAIL%TYPE;
   L_markup           NUMBER(12,4)                     := O_selling_mark_up;
   L_markup_type      DEPS.MARKUP_CALC_TYPE%TYPE       := I_markup_calc_type;
   L_return_code      VARCHAR2(5);
   L_loc_type         VARCHAR2(1)                      := 'Z';
   L_location         ORDLOC.LOCATION%TYPE             := I_zone_id;
   L_unit_cost_prm    ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := I_unit_cost_prm;
   L_GV_block_query_executed BOOLEAN;

BEGIN
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_zone_id is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_zone_id', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;   
   ---
   if I_block_query_executed ='Y' then
      L_GV_block_query_executed := TRUE;
   else
      L_GV_block_query_executed := FALSE;
   end if;
   if O_selling_uom is NOT NULL then
      ---
      if not UOM_SQL.VALID_STANDARD_UOM(L_error_message,
                                        L_valid,
                                        O_selling_uom) then
         O_error_message := L_error_message;
         return false;
      end if;
      ---
      if not L_valid then
         O_error_message := SQL_LIB.CREATE_MSG('INV_UOM_FOR_STANDARD',
                                               NULL,
                                               NULL,
                                               NULL);
         return false;
      end if;
      ---
      if UOM_SQL.GET_DESC(L_error_message,
                          L_uom_desc,
                          O_selling_uom) = FALSE then
         O_error_message := L_error_message;
         return false;
      end if;
      ---
      if I_selling_unit_retail is not NULL
          and O_selling_uom is not NULL then
         if UOM_SQL.CONVERT(L_error_message,
                            L_qty,
                            I_standard_uom,
                            1,
                            O_selling_uom,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CASE_CONV',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return false;
         end if;
         ---
         O_unit_retail := I_selling_unit_retail / L_qty;
         ---
         if not CURRENCY_SQL.ROUND_CURRENCY(L_error_message,
                                            O_unit_retail,
                                            I_currency_code,
                                            'R') then
            O_error_message := L_error_message;
            return false;
         end if;
         ---
      end if;
      --- primary currency information ---
      if not CURRENCY_SQL.CONVERT(L_error_message,
                                  O_unit_retail,
                                  I_currency_code,
                                  I_currency_prim,
                                  O_unit_retail_prim,
                                  'R',
                                  NULL,
                                  NULL)then
         O_unit_retail_prim := NULL;
         O_error_message := L_error_message;
         return false;
      end if;
      ---
      --- Euro currency information ---
      if not CURRENCY_SQL.CONVERT(L_error_message,
                                  O_unit_retail,
                                  I_currency_code,
                                  I_currency_euro,
                                  O_unit_retail_euro,
                                  'R',
                                  NULL,
                                  NULL) then
         O_unit_retail_euro := NULL;
         O_error_message := L_error_message;
         return false;
      end if;
      if I_multi_units is not NULL and I_multi_unit_retail is not NULL
         and I_multi_selling_uom is not NULL then
         if UOM_SQL.CONVERT(L_error_message,
                            L_qty,
                            I_multi_selling_uom,
                            I_multi_unit_retail,
                            O_selling_uom,
                            I_item,
                            NULL,
                            NULL) = FALSE then
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return false;
         end if;
         ---
         L_retail := L_qty / I_multi_units;
         ---
         if L_retail > I_selling_unit_retail then
            O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            O_selling_uom := NULL;
            O_selling_uom_trans := NULL;
            return false;
         end if;
         ---
         if L_qty < I_selling_unit_retail then
            O_error_message := SQL_LIB.CREATE_MSG('MULTI_RETAIL_LESS_SINGLE',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            O_selling_uom := NULL;
            O_selling_uom_trans := NULL;
            return false;
         end if;
      end if;
      ---
      if I_unit_cost_prm is not NULL and
         -- suppress call to compute markup percent if already populated by Execute_Query
         L_GV_block_query_executed = TRUE then
         if not MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM
                                     (L_error_message,
                                      L_markup,
                                     'Y',
                                      L_markup_type,
                                      L_unit_cost_prm,
                                      O_unit_retail_prim,
                                      I_item,
                                      NULL,
                                      L_loc_type,
                                      L_location,
                                      NULL,
                                      NULL,
                                      NULL) then
            O_error_message := L_error_message;
            return false;
         end if;
      end if;
      ---
      O_selling_mark_up := L_markup;
   end if;
   
   if O_unit_retail_prim is NOT NULL then
      if O_unit_retail_prim = 0 then
         O_selling_mark_up := 0;
      end if;
   end if; 
return true;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VAL_SELLING_UOM_TRANS;
--------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_MULTI_SELLING_UOM_TRANS(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_multi_selling_uom              IN OUT   ITEM_LOC.MULTI_SELLING_UOM%TYPE,
                                     O_multi_selling_uom_trans        IN OUT   UOM_CLASS_TL.UOM_TRANS%TYPE,
                                     O_multi_unit_retail_prim         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                     O_selling_unit_retail_prim       IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                     O_multi_unit_retail_euro         IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                     O_multi_selling_mark_up          IN OUT   RETAIL_CALC_TEMP.MULTI_SELLING_MARK_UP%TYPE,
                                     O_selling_unit_retail_euro       IN OUT   ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                     I_markup_calc_type               IN       DEPS.MARKUP_CALC_TYPE%TYPE,
                                     I_zone_id                        IN       COST_ZONE.ZONE_ID%TYPE,
                                     I_unit_cost_prm                  IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                     I_multi_units                    IN       ITEM_LOC.MULTI_UNITS%TYPE,
                                     I_multi_unit_retail              IN       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
                                     I_item                           IN       ITEM_MASTER.ITEM%TYPE,
                                     I_selling_uom                    IN       ITEM_LOC.SELLING_UOM%TYPE,
                                     I_standard_uom_trans             IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                                     I_standard_uom                   IN       ITEM_MASTER.STANDARD_UOM%TYPE,
                                     I_selling_unit_retail            IN       ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
                                     I_unit_retail                    IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                     I_currency_code                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                     I_currency_prim                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                     I_currency_euro                  IN       CURRENCIES.CURRENCY_CODE%TYPE,
                                     I_selling_uom_trans              IN       UOM_CLASS_TL.UOM_TRANS%TYPE,
                                     I_block_query_executed           IN       VARCHAR2)

RETURN BOOLEAN IS
   L_program          VARCHAR2(61)                   := 'ITEM_RETAIL_VALIDATION_SQL.VAL_MULTI_SELLING_UOM_TRANS';
   L_error_message    VARCHAR2(255) := NULL;
   L_uom_desc         UOM_CLASS_TL.UOM_DESC_TRANS%TYPE;
   L_valid            BOOLEAN;
   L_qty              NUMBER;
   L_retail           ITEM_LOC.UNIT_RETAIL%TYPE;
   L_retail_prm       ITEM_LOC.UNIT_RETAIL%TYPE;
   L_markup           NUMBER(12,4)                      := O_multi_selling_mark_up;
   L_markup_type      DEPS.MARKUP_CALC_TYPE%TYPE        := I_markup_calc_type;
   L_return_code      VARCHAR2(5);                      
   L_loc_type         VARCHAR2(1)                       := 'Z';
   L_location         ORDLOC.LOCATION%TYPE              := I_zone_id;
   L_unit_cost_prm    ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  := I_unit_cost_prm;
   L_GV_block_query_executed BOOLEAN;
   
BEGIN
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item', 
                                             L_program, 
                                             NULL);
      return FALSE;
   elsif I_zone_id is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_zone_id', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_block_query_executed ='Y' then
      L_GV_block_query_executed := TRUE;
   else
      L_GV_block_query_executed := FALSE;
   end if;
   ---
   if O_multi_selling_uom is NOT NULL then
      if not UOM_SQL.VALID_STANDARD_UOM(L_error_message,
                                        L_valid,
                                        O_multi_selling_uom) then
         O_error_message := L_error_message;
         return false;
      end if;
      ---
      if not L_valid then
        O_error_message := SQL_LIB.CREATE_MSG('INV_UOM_FOR_STANDARD',
                                               NULL,
                                               NULL,
                                               NULL);
         return false;
      end if;
      ---
      if UOM_SQL.GET_DESC(L_error_message,
                          L_uom_desc,
                          O_multi_selling_uom) = FALSE then
         O_error_message := L_error_message;
         return false;
      end if;
      ---
       if I_multi_units is not NULL and I_multi_unit_retail is not NULL
          and O_multi_selling_uom is not NULL then
          if UOM_SQL.CONVERT(L_error_message,
                               L_qty,
                               O_multi_selling_uom,
                               I_multi_unit_retail,
                               I_selling_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
             O_error_message := L_error_message;
             return false;
          end if;
         ---
         if L_qty = 0 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return false;
         end if;
         ---
         L_retail := L_qty / I_multi_units;
         ---
         if L_retail > I_selling_unit_retail then
            O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            O_multi_selling_uom := NULL;
            O_multi_selling_uom_trans := NULL;
            return false;
         end if;
         ---
         if L_qty < I_selling_unit_retail then
            O_error_message := SQL_LIB.CREATE_MSG('MULTI_RETAIL_LESS_SINGLE',
                                                     NULL,
                                                     NULL,
                                                     NULL);
            O_multi_selling_uom := NULL;
            O_multi_selling_uom_trans := NULL;
            return false;
         end if;
         ---
         if I_standard_uom != I_selling_uom then
            L_retail := I_unit_retail;
         end if;
         --- primary currency information ---
         if not CURRENCY_SQL.CONVERT(L_error_message,
                                  I_multi_unit_retail,
                                  I_currency_code,
                                  I_currency_prim,
                                  O_multi_unit_retail_prim,
                                  'R',
                                  NULL,
                                  NULL) then
            O_selling_unit_retail_prim := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         --- Euro currency information ---
         if not CURRENCY_SQL.CONVERT(L_error_message,
                                     I_multi_unit_retail,
                                     I_currency_code,
                                     I_currency_euro,
                                     O_multi_unit_retail_euro,
                                     'R',
                                     NULL,
                                     NULL) then
            O_selling_unit_retail_euro := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         --- primary currency information ---
         if not CURRENCY_SQL.CONVERT(L_error_message,
                                     L_retail,
                                     I_currency_code,
                                     I_currency_prim,
                                     L_retail_prm,
                                     'R',
                                     NULL,
                                     NULL)then
            O_selling_unit_retail_prim := NULL;
            O_error_message := L_error_message;
            return false;
         end if;
         ---
         if I_unit_cost_prm is not NULL and
            -- suppress call to compute markup percent if already populated by Execute_Query
            L_GV_block_query_executed = TRUE then
            if not MARKUP_SQL.CALC_MARKUP_PERCENT_ITEM
                                     (L_error_message,
                                      L_markup,
                                     'Y',
                                      L_markup_type,
                                      L_unit_cost_prm,
                                      L_retail_prm,
                                      I_item,
                                      NULL,
                                      L_loc_type,
                                      L_location,
                                      NULL,
                                      NULL,
                                      NULL) then
               O_error_message := L_error_message;
               return false;
            end if;
         end if;
         ---
         O_multi_selling_mark_up := L_markup;
      end if;
      ---
   end if;

   if L_retail_prm is NOT NULL then
      if L_retail_prm = 0 then
         O_multi_selling_mark_up := 0;
      end if;
   end if;
   return true;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VAL_MULTI_SELLING_UOM_TRANS;
--------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION VAL_PRE_FORM  (O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_selling_uom                    IN OUT   ITEM_LOC.SELLING_UOM%TYPE,
                        O_standard_uom                   IN OUT   ITEM_MASTER.STANDARD_UOM%TYPE,
                        O_elc_ind                        IN OUT   SYSTEM_OPTIONS.ELC_IND%TYPE,
                        O_unit_cost_prm                  IN OUT   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                        I_item                           IN       ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN IS
   L_program              VARCHAR2(61)                   := 'ITEM_RETAIL_VALIDATION_SQL.VAL_PRE_FORM';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_unit_cost_prm        ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_unit_cost            ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_supp_unit_cost       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_unit_retail_prm      ITEM_LOC.UNIT_RETAIL%TYPE;
   L_supplier             SUPS.SUPPLIER%TYPE := NULL;
   L_item_cost_info_rec   ITEM_COST_SQL.ITEM_COST_INFO_REC;
   L_origin_country_id    ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE := NULL;
   L_currency_code         SUPS.CURRENCY_CODE%TYPE;
   L_extended_base_cost   ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE;
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   L_dummy_varchar        VARCHAR2(25);
   L_dummy_number         NUMBER; 
   L_pack_ind             ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type            ITEM_MASTER.PACK_TYPE%TYPE;
   L_supp_exists          BOOLEAN;
 
BEGIN
   ---
   if I_item is NULL then 
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 
                                            'I_item', 
                                             L_program, 
                                             NULL);
      return FALSE;
   end if;
   ---
   if O_unit_cost_prm is NULL then
      if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(L_error_message,
                                              L_unit_cost_prm,
                                              L_unit_retail_prm,
                                              O_standard_uom,
                                              L_unit_retail_prm,
                                              O_selling_uom,
                                              I_item,
                                              'C') = FALSE then
         O_error_message := L_error_message;
         return false;
      end if;
    
      if ITEM_ATTRIB_SQL.GET_PACK_INDS(L_error_message,
                                       L_pack_ind,
                                       L_sellable_ind,
                                       L_orderable_ind,
                                       L_pack_type,
                                       I_item) = FALSE then
         O_error_message := L_error_message;
         return false;
      end if;
      if ITEM_ATTRIB_SQL.SUPPLIER_EXISTS(L_error_message,
                                         L_supp_exists,
                                         I_item) = FALSE then
         O_error_message := L_error_message;
         return false;
      end if;  

      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(l_error_message,
                                            L_system_options_rec) = FALSE then
         O_error_message := L_error_message;
         return false;
      end if;
      O_elc_ind := L_system_options_rec.elc_ind; 
      -- Get item costing info for gtax/svat if supplier information has been entered for the item. (no supplier means no cost)

      if L_system_options_rec.default_tax_type in ('GTAX','SVAT')  then
           if L_orderable_ind = 'Y' or (L_orderable_ind = 'N' and L_supp_exists = TRUE) then 
              if ITEM_COST_SQL.GET_COST_INFO(L_error_message,
                                             L_item_cost_info_rec,
                                             I_item,
                                             L_supplier,
                                             L_origin_country_id) = FALSE then
                 O_error_message := L_error_message;
                 return false;
              end if;
              L_supp_unit_cost := NVL(L_item_cost_info_rec.extended_base_cost,0);
              if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(L_error_message,
                                                   L_currency_code,
                                                   L_item_cost_info_rec.supplier)= FALSE then
                 O_error_message := L_error_message;
                 return false;
              end if;  
              if CURRENCY_SQL.CONVERT(L_error_message,
                                      L_item_cost_info_rec.extended_base_cost,
                                      L_currency_code,
                                      L_system_options_rec.currency_code,
                                      L_extended_base_cost,
                                      'N',
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL) = FALSE then
                 O_error_message := L_error_message;
                 return false;
              end if;
              L_unit_cost := nvl(L_extended_base_cost,0);
           else
              L_unit_cost := 0;
           end if;   
      else -- default tax type 'SALES 
         L_unit_cost := nvl(L_unit_cost_prm,0);
         L_supp_unit_cost := L_unit_cost; 
      end if;
      if L_system_options_rec.elc_ind = 'N' then
         O_unit_cost_prm := nvl(L_unit_cost,0);
      else -- When ELC is on
         if L_orderable_ind = 'Y' then
            if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                                  O_unit_cost_prm,
                                  L_dummy_number,  -- O_toal_exp
                                  L_dummy_varchar, -- O_exp_currency
                                  L_dummy_number,  -- O_exchange_rate_exp
                                  L_dummy_number,  -- O_total_dty
                                  L_dummy_varchar, -- O_dty_currency
                                  NULL,            -- I_order_no
                                  I_item,
                                  NULL,            -- I_comp_sku
                                  NULL,            -- I_zone_id
                                  NULL,            -- I_location
                                  NULL,            -- I_supplier
                                  NULL,            -- I_origin_country_id
                                  NULL,            -- I_import_country_id
                                  L_supp_unit_cost) = FALSE then
               O_error_message := L_error_message;
               return false;
            end if;
         else
            if nvl(O_unit_cost_prm,0) = 0 then
               O_unit_cost_prm := nvl(L_unit_cost,0);
            end if;
         end if;
      end if;
   end if;
   return true;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VAL_PRE_FORM;
-------------------------------------------------------------------------------------------------------------------------
END ITEM_RETAIL_VALIDATION_SQL;
/