CREATE OR REPLACE PACKAGE VAT_SQL AUTHID CURRENT_USER AS

-- Section commented out since the tables, views required are not ready.
---------------------------------------------------------------------------------
-- Function Name:  GET_VAT_RATE
-- Purpose:        Retrieve vat_rate
-- Calls:
-- CREATED:        29-AUG-97
-- Comments: To retrieve a vat rate you will need to enter one of the following
--           combinations:  1. a vat code
--                          2. a vat region, vat type and a dept or sku,
--                          3. a loc, loc type, vat type and a dept or sku.
--           if no active date is entered, the current active rate will be retrieved.
--           VAT type is either 'C'ost or 'R'etail, a type of 'B' can not be entered.
--           For buyer pack items, blended rate of its component skus will be returned.
--           The code returned for a buyer pack item is for a component of the pack, not
--           the pack as a whole. Vendor pack rates will be processed as a staple sku.
---------------------------------------------------------------------------------

FUNCTION GET_VAT_RATE(O_error_message IN OUT  VARCHAR2,
                      IO_vat_region   IN OUT  VAT_REGION.VAT_REGION%TYPE,
                      IO_vat_code     IN OUT  VAT_CODE_RATES.VAT_CODE%TYPE,
                      O_vat_rate      IN OUT  VAT_CODE_RATES.VAT_RATE%TYPE,
                      I_item          IN      ITEM_MASTER.ITEM%TYPE,
                      I_dept          IN      DEPS.DEPT%TYPE,
                      I_loc_type      IN      COST_ZONE_GROUP_LOC.LOC_TYPE%TYPE,
                      I_location      IN      COST_ZONE_GROUP_LOC.LOCATION%TYPE,
                      I_active_date   IN      VAT_CODE_RATES.ACTIVE_DATE%TYPE,
                      I_vat_type      IN      VAT_item.VAT_TYPE%TYPE,
                      I_store_ind     IN      VARCHAR2)

     RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION GET_VAT_RATE(O_error_message IN OUT  VARCHAR2,
                      IO_vat_region   IN OUT  vat_region.vat_region%TYPE,
                      IO_vat_code     IN OUT  vat_code_rates.vat_code%TYPE,
                      O_vat_rate      IN OUT  vat_code_rates.vat_rate%TYPE,
                      I_item          IN      item_master.item%TYPE,
                      I_dept          IN      deps.dept%TYPE,
                      I_loc_type      IN      cost_zone_group_loc.loc_type%TYPE,
                      I_location      IN      cost_zone_group_loc.location%TYPE,
                      I_active_date   IN      vat_code_rates.active_date%TYPE,
                      I_vat_type      IN      vat_item.vat_type%TYPE)
    RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- FUNCTION: VAT_CODE_DESC
-- PURPOSE: Use a VAT code to return the description
-- CREATED: 23-AUG-97 Retek
----------------------------------------------------------------------------------------
FUNCTION VAT_CODE_DESC(O_error_message    IN OUT VARCHAR2,
                       I_vat_code       IN     vat_codes.vat_code%TYPE,
                       O_vat_desc        IN OUT VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- FUNCTION: VAT_REGION_DESC
-- PURPOSE: Use a VAT region number to return the description
-- CREATED: 18-NOV-96 Richard Stockton
-----------------------------------------------------------------------------------------

FUNCTION VAT_REGION_DESC(O_error_message    IN OUT VARCHAR2,
                         I_vat_region    IN    vat_item.vat_region%TYPE,
                         O_vat_desc        IN OUT VARCHAR2)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- FUNCTION: VAT_SKU_EXIST
-- PURPOSE: Determines if a record exists on VAT SKU with the entered characteristics.
-- CREATED: 12-NOV-98 Mike Foley
-----------------------------------------------------------------------------------------
FUNCTION VAT_SKU_EXIST(O_error_message IN OUT VARCHAR2,
                       O_exists        IN OUT BOOLEAN,
                       I_item          IN     item_master.item%TYPE,
                       I_vat_region    IN     vat_item.vat_region%TYPE,
                       I_active_date   IN     vat_item.active_date%TYPE,
                       I_vat_type      IN     vat_item.vat_type%TYPE ) return BOOLEAN;
-----------------------------------------------------------------------------------
-- FUNCTION: VAT_DEPT_EXIST
-- PURPOSE: Determines if a record exists on VAT Dept with the entered characteristics.
-- CREATED: 12-NOV-98 Mike Foley
-----------------------------------------------------------------------------------------
FUNCTION VAT_DEPT_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exists        IN OUT BOOLEAN,
                        I_dept          IN     vat_deps.dept%TYPE,
                        I_vat_region    IN     vat_deps.vat_region%TYPE,
                        I_vat_type      IN     vat_deps.vat_type%TYPE ) return BOOLEAN;
-----------------------------------------------------------------------------------
-- FUNCTION: VAT_CODE_EXIST
-- PURPOSE: Determines if a the entered VAT code exists on VAT_codes.
-- CREATED: 12-NOV-98 Mike Foley
-----------------------------------------------------------------------------------------
FUNCTION VAT_CODE_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exists        IN OUT BOOLEAN,
                        I_vat_code      IN     vat_codes.vat_code%TYPE ) return BOOLEAN;
-----------------------------------------------------------------------------------
-- FUNCTION: DATE_CODE_EXIST
-- PURPOSE: Determines if a the entered VAT code/active date combination exists on VAT_code.
-- CREATED: 12-NOV-98 Mike Foley
-----------------------------------------------------------------------------------------
FUNCTION DATE_CODE_EXIST(O_error_message IN OUT VARCHAR2,
                        O_exists        IN OUT BOOLEAN,
                        I_active_date   IN     vat_code_rates.active_date%TYPE,
                        I_vat_code      IN     vat_codes.vat_code%TYPE ) return BOOLEAN;
-----------------------------------------------------------------------------------
-- FUNCTION: VAT_RATE_EXIST
-- PURPOSE: Determines if a the entered VAT rate exists on VAT_codes.
-----------------------------------------------------------------------------------------
FUNCTION VAT_RATE_EXIST(O_error_message IN OUT VARCHAR2,
                        I_vat_code      IN     VAT_CODES.VAT_CODE%TYPE ) return BOOLEAN;
-----------------------------------------------------------------------------------
-- FUNCTION: VERIFY_VAT_TYPE
-- PURPOSE: Determines if a the entered dept/vat region has the necessary VAT types.
-----------------------------------------------------------------------------------------
FUNCTION VERIFY_VAT_TYPE(O_error_message IN OUT VARCHAR2,
                         I_dept          IN     VAT_DEPS.DEPT%TYPE,
                         I_vat_region    IN     VAT_DEPS.VAT_REGION%TYPE) return BOOLEAN;
-----------------------------------------------------------------------------------
FUNCTION GET_MIN_CODE_ACTIVE_DATE(O_error_message IN OUT VARCHAR2,
                                  O_min_date      IN OUT VAT_CODE_RATES.ACTIVE_DATE%TYPE,
                                  I_vat_code      IN     VAT_CODE_RATES.VAT_CODE%TYPE) return BOOLEAN;
----------------------------------------------------------------------------------------
FUNCTION VAT_EXISTS_LOC(O_error_message   IN OUT   VARCHAR2,
                        O_exists          IN OUT   BOOLEAN,
                        I_item            IN       item_master.item%type,
                        I_location        IN       store.store%type,
                        I_location_type   IN       VARCHAR2,
                        I_date            IN       DATE) RETURN BOOLEAN;
-----------------------------------------------------------------------------------
-- FUNCTION: VERIFY_VAT_REGION
-- PURPOSE: Determines if a the entered dept has the necessary VAT REGIONS.
-----------------------------------------------------------------------------------------
FUNCTION VERIFY_VAT_REGION(O_error_message   IN OUT   VARCHAR2,
                           I_dept            IN       VAT_DEPS.DEPT%TYPE) return BOOLEAN;
-----------------------------------------------------------------------------------
-- FUNCTION: VATRATE_ITEM_DEPS_DELETION
-- PURPOSE: Determines if a vat code or vat rate deletion has an item relationship.
-----------------------------------------------------------------------------------------
FUNCTION VATRATE_ITEM_DEPS_DELETION(O_error_message   IN OUT   VARCHAR2,
                                    I_vat_rate        IN       VAT_ITEM.VAT_RATE%TYPE,
                                    I_vat_code        IN       VAT_DEPS.VAT_CODE%TYPE,
                                    I_active_date     IN       VAT_ITEM.ACTIVE_DATE%TYPE) return BOOLEAN;
-----------------------------------------------------------------------------------
-- FUNCTION: VATCODE_ITEM_DEPS_DELETION
-- PURPOSE: Determines if a vat code deletion has item dept relationship.
-----------------------------------------------------------------------------------------
FUNCTION VATCODE_ITEM_DEPS_DELETION(O_error_message   IN OUT   VARCHAR2,
                                    I_vat_item_code   IN       VAT_ITEM.VAT_CODE%TYPE,
                                    I_vat_deps_code   IN       VAT_DEPS.VAT_CODE%TYPE) return BOOLEAN;
-----------------------------------------------------------------------------------------
   -- Function    : UPDATE_VAT_DEPT
   -- Purpose     : This function will update vat records in the vat_deps table.
   -- Created     : 30-Aug-2003 by Eldonelle Dolloso
-----------------------------------------------------------------------------------------
FUNCTION UPDATE_VAT_DEPT(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_dept_detail_tbl   IN       MERCH_SQL.VAT_DEPT_DETAIL_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
   -- Function    : DELETE_VAT_ITEM
   -- Purpose     : This function deletes 1 row in the vat_item table.
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_VAT_ITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item          IN     VAT_ITEM.ITEM%TYPE,
                         I_vat_region    IN     VAT_ITEM.VAT_REGION%TYPE,
                         I_type          IN     VAT_ITEM.VAT_TYPE%TYPE,
                         I_active_date   IN     DATE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- FUNCTION: GET_VAT_CODE
-- PURPOSE: Use a VAT rate to return the vat code
-- CREATED: 19-SEP-03 Retek
----------------------------------------------------------------------------------------
FUNCTION GET_VAT_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_vat_code        IN OUT   VAT_CODE_RATES.VAT_CODE%TYPE,
                      I_vat_rate        IN       VAT_CODE_RATES.VAT_RATE%TYPE,
                      I_vat_date        IN       VAT_CODE_RATES.ACTIVE_DATE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- FUNCTION: GET_ZERO_RATE_VAT_CODE
-- PURPOSE: Use a VAT rate = 0 to return the vat code
----------------------------------------------------------------------------------------
FUNCTION GET_ZERO_RATE_VAT_CODE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_vat_code        IN OUT   VAT_CODE_RATES.VAT_CODE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC_VAT_BULK(O_error_message    OUT VARCHAR2,
                          IO_ild_vat_tbl  IN OUT OBJ_ITEM_LOCATION_DATE_VAT_TBL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
END;
/