
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORGANIZATION_VALIDATE_SQL AS

------------------------------------------------------------
FUNCTION EXIST (O_error_message      IN OUT  VARCHAR2,
                I_org_hier_type      IN      NUMBER,
                I_org_hier_value     IN      NUMBER,
                O_exists             IN OUT  BOOLEAN)
        RETURN BOOLEAN IS

   L_temp     STORE.STORE%TYPE;
   L_program  VARCHAR2(64) := 'ORGANIZATION_VALIDATE_SQL.EXIST';
   L_cursor   VARCHAR2(20) := NULL;

   cursor C_COMPANY is
      select comphead.company
        from comphead
       where comphead.company = I_org_hier_value;

   cursor C_CHAIN is
     select chain.chain
       from chain
      where chain.chain = I_org_hier_value;

   cursor C_AREA is
      select area.area
        from area
       where area.area = I_org_hier_value;

   cursor C_REGION is
      select region.region
        from region
       where region.region = I_org_hier_value;

   cursor C_DISTRICT is
      select district.district
        from district
       where district.district = I_org_hier_value;

   cursor C_STORE is
      select store.store
        from store
       where store.store = I_org_hier_value;

BEGIN
   if I_org_hier_type = 1 then
      sql_lib.set_mark('OPEN', 'C_company', 'company', NULL);
      open C_COMPANY;
      sql_lib.set_mark('FETCH', 'C_company', 'company', NULL);
      fetch C_COMPANY into L_temp;
      if C_COMPANY%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_company', 'company', NULL);
      close C_COMPANY;
   elsif I_org_hier_type = 10 then
      sql_lib.set_mark('OPEN', 'C_chain', 'chain', NULL);
      open C_CHAIN;
      sql_lib.set_mark('FETCH', 'C_chain', 'chain', NULL);
      fetch C_CHAIN into L_temp;
      if C_CHAIN%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_chain', 'chain', NULL);
      close C_CHAIN;
   elsif I_org_hier_type = 20 then
      sql_lib.set_mark('OPEN', 'C_area', 'area', NULL);
      open C_AREA;
      sql_lib.set_mark('FETCH', 'C_area', 'area', NULL);
      fetch C_AREA into L_temp;
      if C_AREA%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_area', 'area', NULL);
      close C_AREA;
   elsif I_org_hier_type = 30 then
      sql_lib.set_mark('OPEN', 'C_region', 'region', NULL);
      open C_REGION;
      sql_lib.set_mark('FETCH', 'C_region', 'region', NULL);
      fetch C_REGION into L_temp;
      if C_REGION%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_region', 'region', NULL);
      close C_REGION;
   elsif I_org_hier_type = 40 then
      sql_lib.set_mark('OPEN', 'C_district', 'district', NULL);
      open C_DISTRICT;
      sql_lib.set_mark('FETCH', 'C_district', 'district', NULL);
      fetch C_DISTRICT into L_temp;
      if C_DISTRICT%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_district', 'district', NULL);
      close C_DISTRICT;
   elsif I_org_hier_type = 50 then
      sql_lib.set_mark('OPEN', 'C_store', 'store', NULL);
      open C_STORE;
      sql_lib.set_mark('FETCH', 'C_store', 'store', NULL);
      fetch C_STORE into L_temp;
      if C_STORE%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_store', 'store', NULL);
      close C_STORE;
   end if;


   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
        RETURN FALSE;

END EXIST;
-------------------------------------------------------------------
------------------------------------------------------------
FUNCTION EXIST (O_error_message      IN OUT  VARCHAR2,
                I_org_hier_type      IN      NUMBER,
                I_org_hier_value     IN      NUMBER,
                O_exists             IN OUT  BOOLEAN,
                I_message_type       IN      VARCHAR2)
        RETURN BOOLEAN IS

   L_temp     STORE.STORE%TYPE;
   L_program  VARCHAR2(64) := 'ORGANIZATION_VALIDATE_SQL.EXIST';
   L_cursor   VARCHAR2(20) := NULL;
   L_group_org NUMBER (1);
   L_exists    NUMBER (1);

   cursor C_COMPANY is
      select comphead.company
        from comphead
       where comphead.company = I_org_hier_value;

   cursor C_CHAIN is
     select chain.chain
       from chain
      where chain.chain = I_org_hier_value;

   cursor C_AREA is
      select area.area
        from area
       where area.area = I_org_hier_value;

   cursor C_REGION is
      select region.region
        from region
       where region.region = I_org_hier_value;

   cursor C_DISTRICT is
      select district.district
        from district
       where district.district = I_org_hier_value;

   cursor C_STORE is
      select store.store
        from store
       where store.store = I_org_hier_value;


   cursor C_AREA_CHAIN is
      select area.area
        from area
       where area.chain = I_org_hier_value and rownum = 1;

   cursor C_DEAL_ITEMLOC is
      select 'x' CHAIN_EXISTS
        from DEAL_ITEMLOC
       where chain = I_org_hier_value and rownum = 1;

   cursor C_ORDLOC_DISCOUNT_BUILD is
      select 'x' CHAIN_EXISTS
        from Ordloc_Discount_Build
       where chain = I_org_hier_value and rownum = 1;

BEGIN
   if I_org_hier_type = 1 then
      sql_lib.set_mark('OPEN', 'C_company', 'company', NULL);
      open C_COMPANY;
      sql_lib.set_mark('FETCH', 'C_company', 'company', NULL);
      fetch C_COMPANY into L_temp;
      if C_COMPANY%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_company', 'company', NULL);
      close C_COMPANY;

   elsif I_org_hier_type = 10 then

      sql_lib.set_mark('OPEN', 'C_chain', 'chain', NULL);
      open C_CHAIN;
      sql_lib.set_mark('FETCH', 'C_chain', 'chain', NULL);
      fetch C_CHAIN into L_temp;
      if C_CHAIN%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_chain', 'chain', NULL);
      close C_CHAIN;

      IF NOT O_exists THEN
        O_error_message := 'INV_FIELD_VALUE';
        RETURN TRUE;
      END IF;

      sql_lib.set_mark('OPEN', 'C_AREA_CHAIN', 'area', NULL);
      open C_AREA_CHAIN;
      sql_lib.set_mark('FETCH', 'C_AREA_CHAIN', 'area', NULL);
      fetch C_AREA_CHAIN into L_temp;
      if C_AREA_CHAIN%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_AREA_CHAIN', 'area', NULL);
      close C_AREA_CHAIN;

      if O_exists then
        RETURN TRUE;
      end if;

      sql_lib.set_mark('OPEN', 'C_DEAL_ITEMLOC', 'chain', NULL);
      open C_DEAL_ITEMLOC;
      sql_lib.set_mark('FETCH', 'C_DEAL_ITEMLOC', 'chain', NULL);
      fetch C_DEAL_ITEMLOC into L_temp;
      if C_DEAL_ITEMLOC%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_DEAL_ITEMLOC', 'chain', NULL);
      close C_DEAL_ITEMLOC;

      if O_exists then
        RETURN TRUE;
      end if;

      sql_lib.set_mark('OPEN', 'C_ORDLOC_DISCOUNT_BUILD', 'chain', NULL);
      open C_ORDLOC_DISCOUNT_BUILD;
      sql_lib.set_mark('FETCH', 'C_ORDLOC_DISCOUNT_BUILD', 'chain', NULL);
      fetch C_ORDLOC_DISCOUNT_BUILD into L_temp;
      if C_ORDLOC_DISCOUNT_BUILD%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_ORDLOC_DISCOUNT_BUILD', 'chain', NULL);
      close C_ORDLOC_DISCOUNT_BUILD;

      if O_exists then
        RETURN TRUE;
      end if;

   elsif I_org_hier_type = 20 then
      sql_lib.set_mark('OPEN', 'C_area', 'area', NULL);
      open C_AREA;
      sql_lib.set_mark('FETCH', 'C_area', 'area', NULL);
      fetch C_AREA into L_temp;
      if C_AREA%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_area', 'area', NULL);
      close C_AREA;
   elsif I_org_hier_type = 30 then
      sql_lib.set_mark('OPEN', 'C_region', 'region', NULL);
      open C_REGION;
      sql_lib.set_mark('FETCH', 'C_region', 'region', NULL);
      fetch C_REGION into L_temp;
      if C_REGION%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_region', 'region', NULL);
      close C_REGION;
   elsif I_org_hier_type = 40 then
      sql_lib.set_mark('OPEN', 'C_district', 'district', NULL);
      open C_DISTRICT;
      sql_lib.set_mark('FETCH', 'C_district', 'district', NULL);
      fetch C_DISTRICT into L_temp;
      if C_DISTRICT%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_district', 'district', NULL);
      close C_DISTRICT;
   elsif I_org_hier_type = 50 then
      sql_lib.set_mark('OPEN', 'C_store', 'store', NULL);
      open C_STORE;
      sql_lib.set_mark('FETCH', 'C_store', 'store', NULL);
      fetch C_STORE into L_temp;
      if C_STORE%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      sql_lib.set_mark('CLOSE', 'C_store', 'store', NULL);
      close C_STORE;
   end if;


   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
        RETURN FALSE;

END EXIST;


-------------------------------------------------------------------
END ORGANIZATION_VALIDATE_SQL;
/


