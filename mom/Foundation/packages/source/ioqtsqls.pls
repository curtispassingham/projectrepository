CREATE OR REPLACE PACKAGE ITEMLOC_QUANTITY_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------

-- Function Name: GET_LOC_FUTURE_AVAIL
-- Purpose:       To retrieve the total available stock quantity at
--                a loc for replenishment calculations.
--
--  O_available := (O_stock_on_hand + O_pack_comp_soh +
--                  O_in_transit_qty + O_pack_comp_intran +
--                  O_tsf_expected_qty + O_pack_comp_exp +
--                  O_on_order + O_alloc_in)
--                 - (O_rtv_qty + O_alloc_out +
--                    O_tsf_reserved_qty + O_pack_comp_resv +
--                    O_non_sellable_qty + L_co_tsf_out);
--
-- Calls:         GET_ITEM_LOC_QTYS
--                GET_ON_ORDER
--                GET_ALLOC_IN
--                GET_ALLOC_OUT
FUNCTION GET_LOC_FUTURE_AVAIL
        (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
         O_available             IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_stock_on_hand         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_soh         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_in_transit_qty        IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_intran      IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_tsf_expected_qty      IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_exp         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_on_order              IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_alloc_in              IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_rtv_qty               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_alloc_out             IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_tsf_reserved_qty      IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_resv        IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_non_sellable_qty      IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_customer_backorder    IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_cust_back   IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         I_item                  IN       ITEM_LOC.ITEM%TYPE,
         I_loc                   IN       ITEM_LOC.LOC%TYPE,
         I_loc_type              IN       ITEM_LOC.LOC_TYPE%TYPE,
         I_date                  IN       DATE,
         I_all_orders            IN       VARCHAR2,
         I_lost_sales_ind        IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_LOC_FUTURE_AVAIL
-- Purpose:       To retrieve the total available stock quantity at
--                a loc for replenishment calculations.
--
--  O_available := (O_stock_on_hand + O_pack_comp_soh +
--                  O_in_transit_qty + O_pack_comp_intran +
--                  O_tsf_expected_qty + O_pack_comp_exp +
--                  O_on_order + O_alloc_in)
--                 - (O_rtv_qty + O_alloc_out +
--                    O_tsf_reserved_qty + O_pack_comp_resv +
--                    O_non_sellable_qty + L_co_tsf_out);
--
-- Calls:         GET_ITEM_LOC_QTYS
--                GET_ON_ORDER
--                GET_ALLOC_IN
--                GET_ALLOC_OUT
FUNCTION GET_LOC_FUTURE_AVAIL
        (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
         O_available             IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_stock_on_hand         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_soh         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_in_transit_qty        IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_intran      IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_tsf_expected_qty      IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_exp         IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_on_order              IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_alloc_in              IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_rtv_qty               IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_alloc_out             IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_tsf_reserved_qty      IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_resv        IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_non_sellable_qty      IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_customer_backorder    IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_cust_back   IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         I_item                  IN       ITEM_LOC.ITEM%TYPE,
         I_loc                   IN       ITEM_LOC.LOC%TYPE,
         I_loc_type              IN       ITEM_LOC.LOC_TYPE%TYPE,
         I_date                  IN       DATE,
         I_all_orders            IN       VARCHAR2,
         I_order_point           IN       REPL_RESULTS.ORDER_POINT%TYPE,
         I_repl_ind              IN       VARCHAR2,
         I_lost_sales_ind        IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Function Name: GET_LOC_FUTURE_AVAIL
-- Purpose:       Overloads the other GET_LOC_FUTURE_AVAIL function
--                which returns extra parameters.

FUNCTION GET_LOC_FUTURE_AVAIL
        (O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
         O_available             IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_customer_backorder    IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         O_pack_comp_cust_back   IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         I_item                  IN       ITEM_LOC.ITEM%TYPE,
         I_loc                   IN       ITEM_LOC.LOC%TYPE,
         I_loc_type              IN       ITEM_LOC.LOC_TYPE%TYPE,
         I_date                  IN       DATE,
         I_all_orders            IN       VARCHAR2,
         I_lost_sales_ind        IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Function Name: GET_LOC_FUTURE_AVAIL 
-- Purpose: Overloads the other GET_LOC_FUTURE_AVAIL functions 
--          to include_pack_component indicator.  This function is called
--          by the allocation product. 
--          When the include_pack_comp indicator is 'Y" or null, then 
--          the future available quantity will include all inventory including 
--          the component quantities within the pack
-----------------------------------------------------------------------------
FUNCTION GET_LOC_FUTURE_AVAIL
        (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
         O_available           IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         I_item                IN       ITEM_LOC.ITEM%TYPE,
         I_loc                 IN       ITEM_LOC.LOC%TYPE,
         I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
         I_date                IN       DATE,
         I_all_orders          IN       VARCHAR2,
         I_include_pack_comp   IN       VARCHAR2,
         I_lost_sales_ind      IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Function Name: GET_LOC_CURRENT_AVAIL
-- Purpose:       To retrieve the total current available stock qty
--                (i.e. future items like orders, allocs, etc. not
--                included)
--
--                For a specific location the loc and loc_type should be inputed.
--                For all warehouses the loc should be sent as null and loc_type should be 'W'.
--                For all stores loc should be sent as null and loc_type should be send as 'S'.
--                For all locations, loc and loc_type should be sent as null.
--
-- Calls:         GET_ITEM_LOC_QTYS
FUNCTION GET_LOC_CURRENT_AVAIL
        (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
         O_available       IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         I_item            IN       ITEM_LOC.ITEM%TYPE,
         I_loc             IN       ITEM_LOC.LOC%TYPE,
         I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_STOCK_ON_HAND
-- Purpose:       To retrieve the total stock on hand for an item
--                at a specific location.
--
--                For a specific location the loc and loc_type should be inputed.
--                For all warehouses the loc should be sent as null and loc_type should be 'W'.
--                For all stores loc should be sent as null and loc_type should be send as 'S'.
--                For all locations, loc and loc_type should be sent as null.
-- Calls:         GET_ITEM_LOC_QTYS

FUNCTION GET_STOCK_ON_HAND
        (O_error_message  IN OUT VARCHAR2,
         O_quantity       IN OUT item_loc_soh.stock_on_hand%TYPE,
         I_item           IN     item_loc.item%TYPE,
         I_loc            IN     item_loc.loc%TYPE,
         I_loc_type       IN     item_loc.loc_type%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_ITEM_STOCK
-- Purpose:       Retrieves total stock on hand, in transit qty,
--                tsf reserved qty, tsf expected qty, rtv qty,
--                and non sellable qty from item/loc tables
--                for all stores, all wh's, all external finisher's or all loc's.
--
--                For all warehouses the loc_type should be 'W'.
--                For all stores the loc_type should be send as 'S'.
--                For all external finishers the loc_type should be sent as 'E'.
--                For all locations loc_type should be sent as null.
-- Calls:         GET_ITEM_LOC_QTYS
FUNCTION GET_ITEM_STOCK
        (O_error_message            IN OUT  VARCHAR2,
         O_soh_all_store            IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_soh_all_wh               IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_soh_all_i                IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_soh_all_e                IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_comp_soh_all_wh          IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_comp_soh_all_i           IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_intran_all_store         IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_intran_all_wh            IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_intran_all_i             IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_intran_all_ext_fin       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_comp_intran_all_wh       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_comp_intran_all_i        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_resv_all_store           IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_resv_all_wh              IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_resv_all_i               IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_resv_all_ext_fin         IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_comp_resv_all_wh         IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_comp_resv_all_i          IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_exp_all_store            IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_exp_all_wh               IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_exp_all_i                IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_exp_all_ext_fin          IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_comp_exp_all_wh          IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_comp_exp_all_i           IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_non_sell_all_store       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_non_sell_all_wh          IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_non_sell_all_i           IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_non_sell_all_ext_fin     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_rtv_all_store            IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_rtv_all_wh               IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_rtv_all_i                IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_rtv_all_ext_fin          IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_customer_resv            IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_cust_resv      IN OUT  item_loc_soh.stock_on_hand%TYPE,
         I_item                     IN      item_loc.item%TYPE,
         I_loc_type                 IN      item_loc.loc_type%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_ITEM_LOC_QTYS
-- Purpose:       Retrieves stock on hand, in transit qty,
--                tsf reserved qty, tsf expected qty, rtv qty,
--                non sellable qty, customer reserve, customer backorder,
--                pack component customer reserve and pack component customer backorder
--                from item/loc tables for one loc, all stores, all wh's and all loc's.
--
--                For a specific location the loc and loc_type should be inputed.
--                For all warehouses the loc should be sent as null and loc_type should be 'W'.
--                For all stores loc should be sent as null and loc_type should be send as 'S'.
--                For all locations, loc and loc_type should be sent as null.
-- Calls:         GET_PL_TRANSFER
FUNCTION GET_ITEM_LOC_QTYS
        (O_error_message        IN OUT  VARCHAR2,
         O_stock_on_hand        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_soh        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_in_transit_qty       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_intran     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_tsf_reserved_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_resv       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_tsf_expected_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_exp        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_rtv_qty              IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_non_sellable_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_customer_resv        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_customer_backorder   IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_cust_resv  IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_cust_back  IN OUT  item_loc_soh.stock_on_hand%TYPE,
         I_item                 IN      item_loc.item%TYPE,
         I_loc                  IN      item_loc.loc%TYPE,
         I_loc_type             IN      item_loc.loc_type%TYPE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_ONLINE_STOCK
-- Purpose:       To retrieve online stock quantities including
--                on order, allocs in (tsfalloc and PO allocs)
--                allocs out (tsfalloc and PO allocs),
--                and quality control RTVs.
-- Calls:         GET_ITEM_LOC_QTYS
FUNCTION GET_ONLINE_STOCK
                (I_item             IN     ITEM_LOC.ITEM%TYPE,
                 I_loc              IN     ITEM_LOC.LOC%TYPE,
                 I_loc_type         IN     ITEM_LOC.LOC_TYPE%TYPE,
                 I_date             IN     DATE,
                 I_all_orders       IN     VARCHAR2,
                 O_on_order         IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                 O_in_allocs        IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                 O_out_allocs       IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                 O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_ON_ORDER
-- Purpose:      This function will retrieve the total stock on order for
--               an item for a store or a warehouse. For a specific store
--               or warehouse, inputs should be the location and location
--               type. For all stores or warehouses, inputs should be NULL
--               for location and have a location type specified.  For all
--               locations, inputs should be NULL for both location and
--               location type. Valid orders include ones with a status of
--               Approved. Orders included can be restricted by a date if
--               a date is input. Use NULL for I_date if no date restrictions
--               required. Orders can be restricted to include only those orders
--               with an include_on_order_ind = 'Y' on ordhead. Use N for
--               I_all_orders in this case. To include all orders, use Y for
--               I_all_orders.

FUNCTION GET_ON_ORDER(I_item           IN     ITEM_LOC.ITEM%TYPE,
                      I_loc            IN     ITEM_LOC.LOC%TYPE,
                      I_loc_type       IN     ITEM_LOC.LOC_TYPE%TYPE,
                      I_date           IN     DATE,
                      I_all_orders     IN     VARCHAR2,
                      O_quantity       IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                      O_pack_quantity  IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                      O_error_message  IN OUT VARCHAR2,
                      I_lost_sales_ind IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_ALLOC_IN
-- Purpose:       This function will retrieve the total stock on order
--                allocations coming into a location for an item for a
--                store or warehouse. For a specific store or warehouse,
--                inputs should have a value for the location and location
--                type. For all stores or all warehouses, inputs should not
--                have a value for the location but have a value for the
--                location type. For all locations, inputs not have a value
--                for location or location type. Valid allocations include
--                ones with a status of Approved (A) or Extracted (E) and
--                ones tied to an order. Allocations can be restricted by a
--                date if a date is inputted. Use NULL for I_date if no date
--                restrictions are required. Allocations can be restricted to
--                include only those orders with include_on_order_ind = 'Y' on
--                ordhead. Use NULL for I_all_orders in this case. To include
--                allocations tied to all orders, use Y for I_all_orders.

FUNCTION GET_ALLOC_IN(I_item          IN     ITEM_LOC.ITEM%TYPE,
                      I_loc           IN     ITEM_LOC.LOC%TYPE,
                      I_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                      I_date          IN     DATE,
                      I_all_orders    IN     VARCHAR2,
                      O_quantity      IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                      O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_ALLOC_OUT
-- Purpose:       This function will retrieve the total stock on order allocations
--                going out of a location for an item for a wh. Since allocations
--                cannot be sourced from a store, the location type always assumed
--                to be 'W' (warehouse). For a specified warehouse, inputs should
--                be the location. For all warehouses, inputs should be NULL for
--                location. Valid allocations include ones with a status of Approved
--                (A) or Extracted (E) and ones tied to an order. Allocations can be
--                restricted by a date if a date is input. Use NULL for I_date if
--                no date  restrictions required. Allocs can be restricted to include
--                only those orders that have an include_on_order_ind = 'Y'. Use N for
--                I_all_orders in this case. To include allocs tied to all orders, use
--                Y for I_all_orders.

FUNCTION GET_ALLOC_OUT(I_item          IN     ITEM_LOC.ITEM%TYPE,
                       I_loc           IN     ITEM_LOC.LOC%TYPE,
                       I_date          IN     DATE,
                       O_quantity      IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                       O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

-- Function Name: GET_PL_TRANSFER
-- Purpose:       To calculate the quantity of stock that will be available
--                on the input date from transfers of type PL

FUNCTION GET_PL_TRANSFER(O_error_message IN OUT VARCHAR2,
                         O_item_qty      IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         O_pack_qty      IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                         I_item          IN     ITEM_LOC.ITEM%TYPE,
                         I_loc           IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_date          IN     DATE) RETURN BOOLEAN;
-----------------------------------------------------------------------------
--Function Name:  GET_TOTAL_DIST_QTY
--Purpose:        To sum the dist qty's that are present on allocation tied to
--                orders.  To be used be the GET_LOC_FUTURE_AVAIL, and GET_LOC
--                CURRENT_AVAIL functions.

FUNCTION GET_TOTAL_DIST_QTY(O_error_message   IN OUT VARCHAR2,
                      O_total_dist_qty  IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                      I_item            IN     ITEM_LOC_SOH.ITEM%TYPE,
                      I_loc             IN     ITEM_LOC.LOC%TYPE,
                      I_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                      I_date            IN     DATE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
-- Function Name: GET_TRAN_ITEM_LOC_QTYS
-- Purpose:       Retrieves stock on hand, in transit qty,
--                tsf reserved qty, tsf expected qty, rtv qty,
--                non sellable qty, customer reserve, customer backorder,
--                pack component customer reserve and pack component customer backorder
--                from item/loc tables for a tran-level item at one loc, all stores, all wh's and all loc's.
--
--                For a specific location the loc and loc_type should be inputed.
--                For all warehouses the loc should be sent as null and loc_type should be 'W'.
--                For all stores loc should be sent as null and loc_type should be send as 'S'.
--                For all locations, loc and loc_type should be sent as null.
-- Calls:         GET_PL_TRANSFER
FUNCTION GET_TRAN_ITEM_LOC_QTYS
        (O_error_message        IN OUT  VARCHAR2,
         O_stock_on_hand        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_soh        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_in_transit_qty       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_intran     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_tsf_reserved_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_resv       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_tsf_expected_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_exp        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_rtv_qty              IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_non_sellable_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_customer_resv        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_customer_backorder   IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_cust_resv  IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_cust_back  IN OUT  item_loc_soh.stock_on_hand%TYPE,
         I_item                 IN      item_loc.item%TYPE,
         I_loc                  IN      item_loc.loc%TYPE,
         I_loc_type             IN      item_loc.loc_type%TYPE)
RETURN BOOLEAN;

------------------------------------------------------------------------------------------------
FUNCTION GET_LOC_CURRENT_AVAIL
        (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
         O_available       IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
         I_item            IN       ITEM_LOC.ITEM%TYPE,
         I_loc             IN       ITEM_LOC.LOC%TYPE,
         I_loc_type        IN       ITEM_LOC.LOC_TYPE%TYPE,
         I_repl_ind        IN       VARCHAR2)
RETURN BOOLEAN;            

------------------------------------------------------------------------------------------------
FUNCTION GET_ALLOC_IN(I_item          IN     ITEM_LOC.ITEM%TYPE,
                      I_loc           IN     ITEM_LOC.LOC%TYPE,
                      I_loc_type      IN     ITEM_LOC.LOC_TYPE%TYPE,
                      I_date          IN     DATE,
                      I_all_orders    IN     VARCHAR2,
                      I_repl_ind      IN     VARCHAR2,
                      O_quantity      IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                      O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_DIST_QTY (O_error_message   IN OUT VARCHAR2,
                             O_total_dist_qty  IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_item            IN     ITEM_LOC_SOH.ITEM%TYPE,
                             I_loc             IN     ITEM_LOC.LOC%TYPE,
                             I_loc_type        IN     ITEM_LOC.LOC_TYPE%TYPE,
                             I_date            IN     DATE,
                             I_repl_ind        IN     VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
FUNCTION GET_FUTURE_EXPECTED(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_future_expected IN OUT   ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_location        IN       ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
FUNCTION GET_LOC_PACK_COMP_NON_SELLABLE(O_error_message           IN OUT  rtk_errors.rtk_text%TYPE,
                                        O_pack_comp_non_sellable  IN OUT  item_loc_soh.pack_comp_non_sellable%TYPE,
                                        I_item                    IN      item_loc.item%TYPE,
                                        I_loc                     IN      item_loc.loc%TYPE,
                                        I_loc_type                IN      item_loc.loc_type%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_GROUP_QTYS
-- Purpose:       Retrieves stock on hand, in transit qty,
--                tsf reserved qty, tsf expected qty, rtv qty,
--                non sellable qty, customer reserve, customer backorder,
--                pack component customer reserve and pack component customer backorder
--                from item/loc tables for one the group_type/group_id combination.
--

FUNCTION GET_ITEM_GROUP_QTYS
        (O_error_message        IN OUT  VARCHAR2,
         O_stock_on_hand        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_soh        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_in_transit_qty       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_intran     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_tsf_reserved_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_resv       IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_tsf_expected_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_exp        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_rtv_qty              IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_non_sellable_qty     IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_customer_resv        IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_customer_backorder   IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_cust_resv  IN OUT  item_loc_soh.stock_on_hand%TYPE,
         O_pack_comp_cust_back  IN OUT  item_loc_soh.stock_on_hand%TYPE,
         I_item                 IN      item_loc.item%TYPE,
         I_group_id             IN      partner.partner_id%TYPE,
         I_group_type           IN      code_detail.code%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name: GET_ON_ORDER_GROUP
-- Purpose:      This function will retrieve the total stock on order for
--               an item for a group type/id. 

FUNCTION GET_ON_ORDER_GROUP(I_item           IN     ITEM_LOC.ITEM%TYPE,
                            I_group_id       IN     PARTNER.PARTNER_ID%TYPE,
                            I_group_type     IN     CODE_DETAIL.CODE%TYPE,
                            I_date           IN     DATE,
                            I_all_orders     IN     VARCHAR2,
                            O_quantity       IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            O_pack_quantity  IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name: GET_ALLOC_IN_GROUP
-- Purpose:       This function will retrieve the total stock on order
--                allocations coming into a group of locations for an item for the
--                input group type.

FUNCTION GET_ALLOC_IN_GROUP(I_item          IN     ITEM_LOC.ITEM%TYPE,
                            I_group_id      IN     PARTNER.PARTNER_ID%TYPE,
                            I_group_type    IN     CODE_DETAIL.CODE%TYPE,
                            I_date          IN     DATE,
                            I_all_orders    IN     VARCHAR2,
                            O_quantity      IN OUT ITEM_LOC_SOH.STOCK_ON_HAND%TYPE,
                            O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------------
END ITEMLOC_QUANTITY_SQL;
/


