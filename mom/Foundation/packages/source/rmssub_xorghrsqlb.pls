
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY RMSSUB_XORGHR_SQL AS

-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
-- Function Name:  CHAIN
-- Purpose      :  Inserts, updates, or deletes a record
--                 depending on the message type
-------------------------------------------------------------------------------------------------------
FUNCTION CHAIN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
               I_message_type    IN       VARCHAR2,
               I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  AREA
-- Purpose      :  Inserts, updates, or deletes a record
--                 depending on the message type
-------------------------------------------------------------------------------------------------------
FUNCTION AREA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
              I_message_type    IN       VARCHAR2,
              I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  REGION
-- Purpose      :  Inserts, updates, or deletes a record
--                 depending on the message type
-------------------------------------------------------------------------------------------------------
FUNCTION REGION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_message_type    IN       VARCHAR2,
                I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  DISTRICT
-- Purpose      :  Inserts, updates, or deletes a record
--                 depending on the message type
-------------------------------------------------------------------------------------------------------
FUNCTION DISTRICT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message_type    IN       VARCHAR2,
                  I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  LOC_TRAITS
-- Purpose      :  Inserts or deletes a record
--                 depending on the message type
-------------------------------------------------------------------------------------------------------
FUNCTION LOC_TRAITS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_message_type    IN       VARCHAR2,
                    I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_hier_level      IN       VARCHAR2,
                 I_message_type    IN       VARCHAR2,
                 I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XORGHIER_SQL.PERSIST';

BEGIN

   if I_message_type in (RMSSUB_XORGHR.LP_cre_type,
                                RMSSUB_XORGHR.LP_mod_type,
                                RMSSUB_XORGHR.LP_del_type) then
      if I_hier_level = ORGANIZATION_SQL.LP_chain then
         if not CHAIN(O_error_message,
                      I_message_type,
                      I_org_hier_rec) then
            return FALSE;
         end if;
      elsif I_hier_level = ORGANIZATION_SQL.LP_area then
         if not AREA(O_error_message,
                     I_message_type,
                     I_org_hier_rec) then
            return FALSE;
        end if;
      elsif I_hier_level = ORGANIZATION_SQL.LP_region then
         if not REGION(O_error_message,
                       I_message_type,
                       I_org_hier_rec)  then
            return FALSE;
         end if;
      elsif I_hier_level = ORGANIZATION_SQL.LP_district then
         if not DISTRICT(O_error_message,
                         I_message_type,
                         I_org_hier_rec) then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   if I_org_hier_rec.loc_trait_ids is NOT NULL and I_org_hier_rec.loc_trait_ids.count > 0 then
      if not LOC_TRAITS(O_error_message,
                        I_message_type,
                        I_org_hier_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHAIN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
               I_message_type    IN       VARCHAR2,
               I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)

   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XORGHIER_SQL.CHAIN';

BEGIN

   if I_message_type = RMSSUB_XORGHR.LP_cre_type then
      if not ORGANIZATION_SQL.INSERT_CHAIN(O_error_message,
                                           I_org_hier_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORGHR.LP_mod_type then
      if not ORGANIZATION_SQL.UPDATE_CHAIN(O_error_message,
                                           I_org_hier_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORGHR.LP_del_type then
      if not ORGANIZATION_SQL.DELETE_CHAIN(O_error_message,
                                           I_org_hier_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHAIN;
-------------------------------------------------------------------------------------------------------
FUNCTION AREA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
              I_message_type    IN       VARCHAR2,
              I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)

   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XORGHIER_SQL.AREA';

BEGIN

   if I_message_type = RMSSUB_XORGHR.LP_cre_type then
      if not ORGANIZATION_SQL.INSERT_AREA(O_error_message,
                                          I_org_hier_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORGHR.LP_mod_type then
      if not ORGANIZATION_SQL.UPDATE_AREA(O_error_message,
                                          I_org_hier_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORGHR.LP_del_type then
      if not ORGANIZATION_SQL.DELETE_AREA(O_error_message,
                                          I_org_hier_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END AREA;
-------------------------------------------------------------------------------------------------------
FUNCTION REGION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                I_message_type    IN       VARCHAR2,
                I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)

   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XORGHIER_SQL.REGION';

BEGIN
   if I_message_type = RMSSUB_XORGHR.LP_cre_type then
      if not ORGANIZATION_SQL.INSERT_REGION(O_error_message,
                                            I_org_hier_rec) then
         return FALSE;
      end if;
      ---
      if not LOC_TRAITS_SQL.INSERT_LOC_TRAITS(O_error_message,
                                              I_org_hier_rec) then
         return FALSE;
      end if;

   elsif I_message_type = RMSSUB_XORGHR.LP_mod_type then
      --Delete loc_trait records for the region
      --before updating the region table
      --when parent id is not equal to the old parent id
      if I_org_hier_rec.parent_id != I_org_hier_rec.old_parent_id and
         I_org_hier_rec.old_parent_id is not NULL then
         if not LOC_TRAITS_SQL.DELETE_PARENT_AREA(O_error_message,
                                                  I_org_hier_rec.hier_value,
                                                  I_org_hier_rec.old_parent_id) then
            return FALSE;
         end if;
      end if;
      ---
      if not ORGANIZATION_SQL.UPDATE_REGION(O_error_message,
                                            I_org_hier_rec) then
         return FALSE;
      end if;
      ---
      if I_org_hier_rec.parent_id != I_org_hier_rec.old_parent_id and
         I_org_hier_rec.old_parent_id is not NULL then
         if not LOC_TRAITS_SQL.INSERT_LOC_TRAITS(O_error_message,
                                                 I_org_hier_rec) then
            return FALSE;
         end if;
      end if;

   elsif I_message_type = RMSSUB_XORGHR.LP_del_type then
      if not ORGANIZATION_SQL.DELETE_REGION(O_error_message,
                                            I_org_hier_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END REGION;
-------------------------------------------------------------------------------------------------------
FUNCTION DISTRICT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_message_type    IN       VARCHAR2,
                  I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)

   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XORGHIER_SQL.DISTRICT';

BEGIN

   if I_message_type = RMSSUB_XORGHR.LP_cre_type then
      if not ORGANIZATION_SQL.INSERT_DISTRICT(O_error_message,
                                              I_org_hier_rec) then
         return FALSE;
      end if;
      ---
      if not LOC_TRAITS_SQL.INSERT_LOC_TRAITS(O_error_message,
                                              I_org_hier_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORGHR.LP_mod_type then

      ---
      if I_org_hier_rec.parent_id != I_org_hier_rec.old_parent_id then
         if not LOC_TRAITS_SQL.DELETE_PARENT_REGION(O_error_message,
                                                    I_org_hier_rec.hier_value,
                                                    I_org_hier_rec.old_parent_id) then
            return FALSE;
         end if;
         ---
      end if;
      ---
      if not ORGANIZATION_SQL.UPDATE_DISTRICT(O_error_message,
                                              I_org_hier_rec) then
         return FALSE;
      end if;
      ---
      if I_org_hier_rec.parent_id != I_org_hier_rec.old_parent_id then
         if not LOC_TRAITS_SQL.INSERT_LOC_TRAITS(O_error_message,
                                                 I_org_hier_rec) then
            return FALSE;
         end if;
      end if;
   elsif I_message_type = RMSSUB_XORGHR.LP_del_type then
      if not ORGANIZATION_SQL.DELETE_DISTRICT(O_error_message,
                                              I_org_hier_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DISTRICT;
-------------------------------------------------------------------------------------------------------
FUNCTION LOC_TRAITS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_message_type    IN       VARCHAR2,
                    I_org_hier_rec    IN       ORGANIZATION_SQL.ORG_HIER_REC)

   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XORGHIER_SQL.LOC_TRAITS';

BEGIN

   if I_message_type in (RMSSUB_XORGHR.LP_cre_type,
                                RMSSUB_XORGHR.LP_loctrt_cre_type)  then
      if not LOC_TRAITS_SQL.INSERT_LOC_TRAITS(O_error_message,
                                              I_org_hier_rec) then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XORGHR.LP_loctrt_del_type then
      if not LOC_TRAITS_SQL.DELETE_LOC_TRAITS(O_error_message,
                                              I_org_hier_rec) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOC_TRAITS;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XORGHR_SQL;
/
