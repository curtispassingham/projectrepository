create or replace PACKAGE CORESVC_ENTRY_STT_TP AUTHID CURRENT_USER AS
   template_key           CONSTANT VARCHAR2(255):='ENTRY_STT_TP_DATA';
   action_new             VARCHAR2(25)          := 'NEW';
   action_mod             VARCHAR2(25)          := 'MOD';
   action_del             VARCHAR2(25)          := 'DEL';
   
   ENTRY_TYPE_TL_sheet                           VARCHAR2(255)         := 'ENTRY_TYPE_TL';
   ENTRY_TYPE_TL$Action                          NUMBER                :=1;
   ENTRY_TYPE_TL$ENTRY_TYPE_DESC                 NUMBER                :=5;
   ENTRY_TYPE_TL$IMT_CNTRY_ID                    NUMBER                :=4;
   ENTRY_TYPE_TL$ENTRY_TYPE                      NUMBER                :=3;
   ENTRY_TYPE_TL$LANG                            NUMBER                :=2;

   sheet_name_trans S9T_PKG.trans_map_typ;
   action_column                                VARCHAR2(255) := 'ACTION';
   template_category CODE_DETAIL.CODE%TYPE := 'RMSIMP';

   TYPE ENTRY_TYPE_TL_rec_tab IS TABLE OF ENTRY_TYPE_TL%ROWTYPE;
   ENTRY_TYPE_sheet                             VARCHAR2(255)         := 'ENTRY_TYPE';
   ENTRY_TYPE$Action                            NUMBER                :=1;
   ENTRY_TYPE$ENTRY_TYPE_DESC                   NUMBER                :=4;
   ENTRY_TYPE$IMPORT_COUNTRY_ID                 NUMBER                :=3;
   ENTRY_TYPE$ENTRY_TYPE                        NUMBER                :=2;

   TYPE ENTRY_TYPE_rec_tab IS TABLE OF ENTRY_TYPE%ROWTYPE;
   ENTRY_STATUS_TL_sheet                        VARCHAR2(255)         := 'ENTRY_STATUS_TL';
   ENTRY_STATUS_TL$Action                       NUMBER                :=1;
   ENTRY_STATUS_TL$ENTRY_STT_DESC               NUMBER                :=5;
   ENTRY_STATUS_TL$IMT_COUNTRY_ID               NUMBER                :=4;
   ENTRY_STATUS_TL$ENTRY_STATUS                 NUMBER                :=3;
   ENTRY_STATUS_TL$LANG                         NUMBER                :=2;

   TYPE ENTRY_STATUS_TL_rec_tab IS TABLE OF ENTRY_STATUS_TL%ROWTYPE;
   ENTRY_STATUS_sheet                           VARCHAR2(255)         := 'ENTRY_STATUS';
   ENTRY_STATUS$Action                          NUMBER                :=1;
   ENTRY_STATUS$ENTRY_STATUS_DESC               NUMBER                :=4;
   ENTRY_STATUS$IMPORT_COUNTRY_ID               NUMBER                :=3;
   ENTRY_STATUS$ENTRY_STATUS                    NUMBER                :=2;

   TYPE ENTRY_STATUS_rec_tab IS TABLE OF ENTRY_STATUS%ROWTYPE;

   FUNCTION CREATE_S9T(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                       I_template_only_ind IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;

   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count        OUT   NUMBER,
                        I_file_id         IN       s9t_folder.file_id%TYPE,
                        I_process_id      IN       NUMBER
                        )
   RETURN BOOLEAN;

   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count        OUT   NUMBER,
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;

   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR)
   RETURN VARCHAR2;


   END CORESVC_ENTRY_STT_TP;
   /