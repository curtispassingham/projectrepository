-- File Name : CORESVC_VAT_CODES_SPEC.pls
CREATE OR REPLACE PACKAGE CORESVC_VAT AUTHID CURRENT_USER AS
   template_key          CONSTANT VARCHAR2(255)  :='VAT_CODES_REGION_DATA';
   template_category     CONSTANT VARCHAR2(255)  :='RMSFIN';
   action_new                     VARCHAR2(25)   :='NEW';
   action_mod                     VARCHAR2(25)   :='MOD';
   action_del                     VARCHAR2(25)   :='DEL';
   VAT_CODES_sheet                VARCHAR2(255)  :='VAT_CODES';
   VAT_CODES$Action               NUMBER         := 1;
   VAT_CODES$VAT_CODE             NUMBER         := 2;
   VAT_CODES$VAT_CODE_DESC        NUMBER         := 3;
   VAT_CODES$INCL_NIC_IND         NUMBER         := 4;
   
   VAT_CODES_TL_sheet             VARCHAR2(255)  :='VAT_CODES_TL';
   VAT_CODES_TL$Action            NUMBER         := 1;
   VAT_CODES_TL$LANG              NUMBER         := 2;
   VAT_CODES_TL$VAT_CODE          NUMBER         := 3;
   VAT_CODES_TL$VAT_CODE_DESC     NUMBER         := 3;
   
   VAT_CODE_RATES_sheet           VARCHAR2(255)  :='VAT_CODE_RATES';
   VAT_CODE_RATES$Action          NUMBER         := 1;
   VAT_CODE_RATES$VAT_CODE        NUMBER         := 2;
   VAT_CODE_RATES$ACTIVE_DATE     NUMBER         := 3;
   VAT_CODE_RATES$ACTIVE_DATE_S9T NUMBER         := 4;
   VAT_CODE_RATES$VAT_RATE        NUMBER         := 5;

   VAT_CODES_CFA_EXT_sheet        VARCHAR2(255)  :='VAT_CODES_CFA_EXT';
   VAT_CODES_CFA_EXT$ACTION       NUMBER         :=1;
   VAT_CODES_CFA_EXT$VAT_CODE     NUMBER         :=2;
   VAT_CODES_CFA_EXT$GROUP_ID     NUMBER         :=3;
   VAT_CODES_CFA_EXT$VARCHAR2_1   NUMBER         :=4;
   VAT_CODES_CFA_EXT$VARCHAR2_2   NUMBER         :=5;
   VAT_CODES_CFA_EXT$VARCHAR2_3   NUMBER         :=6;
   VAT_CODES_CFA_EXT$VARCHAR2_4   NUMBER         :=7;
   VAT_CODES_CFA_EXT$VARCHAR2_5   NUMBER         :=8;
   VAT_CODES_CFA_EXT$VARCHAR2_6   NUMBER         :=9;
   VAT_CODES_CFA_EXT$VARCHAR2_7   NUMBER         :=10;
   VAT_CODES_CFA_EXT$VARCHAR2_8   NUMBER         :=11;
   VAT_CODES_CFA_EXT$VARCHAR2_9   NUMBER         :=12;
   VAT_CODES_CFA_EXT$VARCHAR2_10  NUMBER         :=13;
   VAT_CODES_CFA_EXT$NUMBER_11    NUMBER         :=14;
   VAT_CODES_CFA_EXT$NUMBER_12    NUMBER         :=15;
   VAT_CODES_CFA_EXT$NUMBER_13    NUMBER         :=16;
   VAT_CODES_CFA_EXT$NUMBER_14    NUMBER         :=17;
   VAT_CODES_CFA_EXT$NUMBER_15    NUMBER         :=18;
   VAT_CODES_CFA_EXT$NUMBER_16    NUMBER         :=19;
   VAT_CODES_CFA_EXT$NUMBER_17    NUMBER         :=20;
   VAT_CODES_CFA_EXT$NUMBER_18    NUMBER         :=21;
   VAT_CODES_CFA_EXT$NUMBER_19    NUMBER         :=22;
   VAT_CODES_CFA_EXT$NUMBER_20    NUMBER         :=23;
   VAT_CODES_CFA_EXT$DATE_21      NUMBER         :=24;
   VAT_CODES_CFA_EXT$DATE_22      NUMBER         :=25;
   VAT_CODES_CFA_EXT$DATE_23      NUMBER         :=23;
   VAT_CODES_CFA_EXT$DATE_24      NUMBER         :=24;
   VAT_CODES_CFA_EXT$DATE_25      NUMBER         :=25;

   VAT_REG_sheet                 VARCHAR2(255) :='VAT_REGION';
   VAT_REG$Action                NUMBER        :=1;
   VAT_REG$VAT_REGION            NUMBER        :=2;
   VAT_REG$VAT_REGION_NAME       NUMBER        :=3;
   VAT_REG$VAT_REGION_TYPE       NUMBER        :=4;
   VAT_REG$ACQUISITION_VAT_IND   NUMBER        :=5;
   VAT_REG$REVERSE_VAT_THRESHOLD NUMBER        :=6;
   VAT_REG$VAT_CALC_TYPE         NUMBER        :=7;

   VAT_REG_Tl_sheet              VARCHAR2(255) :='VAT_REGION_TL';
   VAT_REG_Tl$Action             NUMBER        :=1;
   VAT_REG_Tl$LANG               NUMBER        :=2;
   VAT_REG_Tl$VAT_REGION         NUMBER        :=3;
   VAT_REG_Tl$VAT_REGION_NAME    NUMBER        :=4;
  
   action_column                  VARCHAR2(255)  := 'ACTION';

   TYPE VAT_CODES_rec_tab         IS TABLE OF VAT_CODES%ROWTYPE;

   TYPE VAT_CODE_RATES_rec_tab    IS TABLE OF VAT_CODE_RATES%ROWTYPE;

   TYPE VAT_CODES_CFA_EXT_rec_tab IS TABLE OF VAT_CODES_CFA_EXT%ROWTYPE;

   sheet_name_trans  S9T_PKG.trans_map_typ;

-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2;
--------------------------------------------------------------------------------------------------------
END CORESVC_VAT;
/