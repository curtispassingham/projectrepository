
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PRIVILEGE_SQL AUTHID CURRENT_USER AS

role_tabl  userid_tbl;

-------------------------------------------------------------------------------
-- Function: GET_USER_ACCESS
-- Purpose:  This function will determine if the security group
--           has the privilege passed to it.  The Privilege is created
--           as a CODE_TYPE and the Secuity is assigned by adding the
--           the GROUP_ID as a CODE on the CODE_DETAIL table.
--           To make privileges easier to identify in CODE_DETAIL the CODE_TYPE
--           will start with P and contain 'Privilege' in the description.
-- Returns:  O_valid is set to TRUE if the user has the privilege
--           otherwise O_valid is set to FALSE.
--           This funciton returns TRUE if no error occurs. If an Error
--           does occur O_error_message will contain the error text and
--           FALSE is returned.
-------------------------------------------------------------------------------

FUNCTION GET_USER_ACCESS (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_valid         IN OUT BOOLEAN,
                          I_privilege     IN     VARCHAR2)
   RETURN BOOLEAN;
--------------------------------------------------------------------
-- FUNCTION NAME: COMPARE_USER_SECURITY
-- Purpose      : Validates whether a user attempting to update a deal has
--              : at least the security privileges of the user that created
--              : the deal ... or not.
--------------------------------------------------------------------
FUNCTION COMPARE_USER_SECURITY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_update_user_id_ok IN OUT BOOLEAN,
                               I_create_user_id    IN     VARCHAR2,
                               I_update_user_id    IN     VARCHAR2)
RETURN BOOLEAN;
PRAGMA RESTRICT_REFERENCES(COMPARE_USER_SECURITY,WNDS,WNPS);
--------------------------------------------------------------------
-- FUNCTION NAME: ALLOW_FIXED_DEAL_SEC
-- Purpose      : Validates if a user has sufficient security
--                privileges to perform an action on a given deal.
-- Returns      : The function returns 1 if the user has the security
--                privilege to perform an action on a given deal.
--                Otherwise, the function returns 0.
--------------------------------------------------------------------
FUNCTION ALLOW_FIXED_DEAL_SEC(I_deal_no        IN FIXED_DEAL.DEAL_NO%TYPE,
                              I_merch_ind      IN FIXED_DEAL.MERCH_IND%TYPE,
                              I_create_user_id IN FIXED_DEAL.USER_ID%TYPE,
                              I_update_user_id IN FIXED_DEAL.USER_ID%TYPE)
RETURN NUMBER;
--PRAGMA RESTRICT_REFERENCES(ALLOW_FIXED_DEAL_SEC,WNDS,WNPS);
--------------------------------------------------------------------
-- FUNCTION NAME: ALLOW_COMPLEX_DEAL_SEC
-- Purpose      : Validates if a user has sufficient security
--                privileges to perform an action on a given deal.
-- Returns      : The function returns O_allow_deal_access as TRUE if
--                the user has the security privilege to perform an
--                action on a given deal.  Otherwise, the value is
--                returned as FALSE.
--------------------------------------------------------------------
FUNCTION ALLOW_COMPLEX_DEAL_SEC(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_allow_deal_access IN OUT BOOLEAN,
                                I_deal_id           IN     DEAL_HEAD.DEAL_ID%TYPE,
                                I_create_id         IN     DEAL_HEAD.CREATE_ID%TYPE,
                                I_order_no          IN     DEAL_HEAD.ORDER_NO%TYPE,
                                I_security_ind      IN     DEAL_HEAD.SECURITY_IND%TYPE,
                                I_update_id         IN     DEAL_HEAD.CREATE_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------
-- FUNCTION NAME: POP_DBA_ROLE_PRIVS
-- Purpose      : This will select the granted_role for the
--                current session's user.
--------------------------------------------------------------------
FUNCTION POP_DBA_ROLE_PRIVS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------
END PRIVILEGE_SQL;
/
