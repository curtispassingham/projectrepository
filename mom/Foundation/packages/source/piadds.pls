CREATE OR REPLACE PACKAGE PACKITEM_ADD_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
-- PUBLIC RECORD TYPE
-----------------------------------------------------------------------------
TYPE ISC_RECTYPE IS RECORD
(supplier     NUMBER(10),
 country      VARCHAR(3));

TYPE ISC_TBLTYPE is TABLE of ISC_RECTYPE INDEX BY BINARY_INTEGER;
-----------------------------------------------------------------------------
--- FUNCTION: VALID_ITEM
--- PURPOSE:  This function checks the item that is being added to
---           the pack to make sure that all pack suppliers and countries
---           supply the item.
---
FUNCTION VALID_ITEM (O_error_message  IN OUT VARCHAR2,
                     O_valid          IN OUT BOOLEAN,
                     O_item_desc      IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                     I_pack_no        IN     PACKITEM.PACK_NO%TYPE,
                     I_item           IN     ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: UPDATE_SUPP_COST
--- PURPOSE:  This function will update the unit
---           cost for each supplier of the pack.
---
FUNCTION UPDATE_SUPP_COST (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_pack_no       IN     PACKITEM.PACK_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: PARENT_PACK_TMPL
--- PURPOSE:  This function will insert the proper records into
---           PACKITEM when a PARENT/PACK_TMPL combination is chosen.
---           If the combination creates items that do not yet exist
---           it will create them based on the template.
---
FUNCTION PARENT_PACK_TMPL(O_error_message  IN OUT VARCHAR2,
                          O_item_rejected     IN OUT BOOLEAN,
                          I_pack_no        IN     ITEM_MASTER.ITEM%TYPE,
                          I_item_parent    IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                          I_pack_tmpl_id   IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                          I_qty            IN     PACKITEM.PACK_QTY%TYPE,
                          I_supplier       IN     SUPS.SUPPLIER%TYPE,
                          I_origin_country IN     COUNTRY.COUNTRY_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: INNER_PACK
--- PURPOSE:  This function will insert the proper records into PACKSKU
---           when a pack is selected as an inner pack.
---
FUNCTION INNER_PACK (O_error_message IN OUT VARCHAR2,
                     I_pack_no    IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                     I_inner_pack    IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                     I_qty           IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: DELETE_PACKITEM_BREAKOUT
--- PURPOSE:  This function will delete from packitem_breakout based on the
---           pack_no, item (item or item_parent) and pack_ind.
---
FUNCTION DELETE_PACKITEM_BREAKOUT (O_error_message IN OUT VARCHAR2,
                                   I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                                   I_item          IN     PACKITEM_BREAKOUT.ITEM%TYPE,
                                   I_item_parent   IN     PACKITEM_BREAKOUT.ITEM_PARENT%TYPE,
                                   I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: UPDATE_PACKITEM_BREAKOUT
--- PURPOSE:  This function will update packitem_breakout based on the
---           pack_no, item, pack_ind and pack_tmpl_id.
---
FUNCTION UPDATE_PACKITEM_BREAKOUT (O_error_message IN OUT VARCHAR2,
                                   I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                                   I_item          IN     PACKITEM_BREAKOUT.ITEM%TYPE,
                                   I_pack_tmpl_id  IN     PACKITEM_BREAKOUT.PACK_TMPL_ID%TYPE,
                                   I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                                   I_qty           IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: INSERT_PACKITEM_BREAKOUT
--- PURPOSE:  This function will insert into packitem_breakout based on the
---           pack_no, item, pack_ind and pack_tmpl_id.
---
FUNCTION INSERT_PACKITEM_BREAKOUT (O_error_message IN OUT VARCHAR2,
                                   I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                                   I_item          IN     PACKITEM_BREAKOUT.ITEM%TYPE,
                                   I_pack_tmpl_id  IN     PACKITEM_BREAKOUT.PACK_TMPL_ID%TYPE,
                                   I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                                   I_qty           IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: EXISTS_IN_PACK
--- PURPOSE:  This function will check to see if the item already
---           exists in the pack.
---
FUNCTION EXISTS_IN_PACK (O_error_message IN OUT VARCHAR2,
                         O_exist         IN OUT BOOLEAN,
                         I_pack_no       IN     PACKITEM.PACK_NO%TYPE,
                         I_item          IN     PACKITEM.ITEM%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: PARENT_TMPL_COST_RETAIL
--- PURPOSE:  This function will sum unit_cost and retail of the SKU's
---           within the style/pack_tmpl
---
FUNCTION PARENT_TMPL_COST_RETAIL (O_error_message       IN OUT VARCHAR2,
                                  O_unit_retail_prim    IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                  O_unit_cost_prim      IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                  I_pack_no             IN     PACKITEM.PACK_NO%TYPE,
                                  I_item_parent         IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                                  I_pack_tmpl_id        IN     PACKITEM.PACK_TMPL_ID%TYPE,
                                  I_pack_orderable_ind  IN     ITEM_MASTER.ORDERABLE_IND%TYPE,
                                  I_currency_code_prim  IN     SYSTEM_OPTIONS.CURRENCY_CODE%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: COMP_COST_RETAIL
--- PURPOSE: This function will calculate the cost and retail for an item or
---          totals for items in an inner pack using the main pack's primary
---          supplier's currency.
---          If the pack is not orderable (there is no primary supplier) the
---          item's primary supplier will be used.
---
FUNCTION COMP_COST_RETAIL(O_error_message     IN OUT VARCHAR2,
                          O_unit_retail_prim  IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                          O_unit_cost_prim    IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          I_pack_no           IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                          I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                          I_pack_tmpl_id      IN     PACKITEM_BREAKOUT.PACK_TMPL_ID%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: BUILD_COMP_COST
--- PURPOSE:  This function will sum unit_cost of the items within the pack.
---
FUNCTION BUILD_COMP_COST_RETAIL (O_error_message    IN OUT VARCHAR2,
                                 O_unit_cost_prim   IN OUT ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                 O_unit_retail_prim IN OUT ITEM_LOC.UNIT_RETAIL%TYPE,
                                 I_pack_no          IN     PACKITEM.PACK_NO%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: NEW_PACK_TMPL
--- PURPOSE:  This function adds a new pack template item to the system for the
---           given supplier, style, and template_id.  Called from ordmtxws when
---           user chooses menu option to add new style template.
---
FUNCTION NEW_PACK_TMPL(O_error_message       IN OUT   VARCHAR2,
                       O_item_rejected       IN OUT   BOOLEAN,
                       I_pack_no             IN       PACKITEM.PACK_NO%TYPE,
                       I_item_parent         IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_template_id         IN       PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                       I_pack_type           IN       PACK_TMPL_HEAD.PACK_TYPE%TYPE,
                       I_supplier            IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                       I_description         IN       ITEM_MASTER.ITEM_DESC%TYPE,
                       I_short_desc          IN       ITEM_MASTER.SHORT_DESC%TYPE,
                       I_vendor_desc         IN       SUPS_PACK_TMPL_DESC.SUPP_PACK_DESC%TYPE,
                       I_order_as_type       IN       ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                       I_origin_country      IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_unit_cost           IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                       I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE DEFAULT NULL)
   return BOOLEAN;

FUNCTION NEW_PACK_TMPL(O_error_message       IN OUT   VARCHAR2,
                       O_item_rejected       IN OUT   BOOLEAN,
                       I_pack_no             IN       PACKITEM.PACK_NO%TYPE,
                       I_item_parent         IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                       I_template_id         IN       PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                       I_pack_type           IN       PACK_TMPL_HEAD.PACK_TYPE%TYPE,
                       I_supplier            IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                       I_description         IN       ITEM_MASTER.ITEM_DESC%TYPE,
                       I_description_sec     IN       ITEM_MASTER.ITEM_DESC_SECONDARY%TYPE,
                       I_short_desc          IN       ITEM_MASTER.SHORT_DESC%TYPE,
                       I_vendor_desc         IN       SUPS_PACK_TMPL_DESC.SUPP_PACK_DESC%TYPE,
                       I_order_as_type       IN       ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                       I_origin_country      IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       I_unit_cost           IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                       I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE DEFAULT NULL)
   RETURN BOOLEAN;

-----------------------------------------------------------------------------
--- FUNCTION: UPDATE_FASHPACK
--- PURPOSE:  This function rebuilds the packs components, updates the suppliers
---           cost, and associates new items with locations.  Called from fashpack.
---

FUNCTION UPDATE_FASHPACK(O_error_message     IN OUT VARCHAR2,
                         O_item_rejected     IN OUT BOOLEAN,
                         I_item_parent       IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_no           IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_tmpl_id      IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                         I_order_no          IN     ORDSKU.ORDER_NO%TYPE,
                         I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                         I_item_parent_desc  IN     ITEM_MASTER.ITEM_DESC%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_unit_cost         IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: INSERT_FASHPACK
--- PURPOSE:  This function creates new pack_tmpl and packs associated with them.
---

FUNCTION INSERT_FASHPACK(O_error_message     IN OUT VARCHAR2,
                         O_item_rejected     IN OUT BOOLEAN,
                         I_order_no          IN     ORDSKU.ORDER_NO%TYPE,
                         I_item_parent       IN     ITEM_MASTER.ITEM_PARENT%TYPE,
                         I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                         I_pack_type         IN     PACK_TMPL_HEAD.PACK_TYPE%TYPE,
                         I_order_as_type     IN     ITEM_MASTER.ORDER_AS_TYPE%TYPE,
                         I_receive_as_type   IN     ITEM_LOC.RECEIVE_AS_TYPE%TYPE,
                         I_item_parent_desc  IN     ITEM_MASTER.ITEM_DESC%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_unit_cost         IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: CHECK_INSERT
--- PURPOSE: This function will check if a passed in parent item/pack template
---          will result in any detail level records (packitem_breakout) being inserted.
-----------------------------------------------------------------------------
FUNCTION CHECK_INSERT(O_error_message  IN OUT VARCHAR2,
                      O_valid          IN OUT BOOLEAN,
                      I_item_parent    IN     ITEM_MASTER.ITEM%TYPE,
                      I_pack_tmpl_id   IN     PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: VALID_ITEM
--- PURPOSE:  This function checks the item that is being added to
---           the pack to make sure that all pack suppliers and countries
---           supply the item.  Overloaded for item API.  Pack item is not
---           yet in item_master; so, API message item info for pack is
---           provided in the I_pack_rec, I_supplier, I_country
-----------------------------------------------------------------------------
FUNCTION VALID_ITEM (O_error_message  IN OUT VARCHAR2,
                     O_valid          IN OUT BOOLEAN,
                     O_item_pack_ind  IN OUT VARCHAR2,
                     I_pack_rec       IN     ITEM_MASTER%ROWTYPE,
                     I_isc_tbl        IN     ISC_TBLTYPE,
                     I_pack_no        IN     PACKITEM.PACK_NO%TYPE,
                     I_item           IN     ITEM_MASTER.ITEM%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
--- FUNCTION: INSERT_PACKITEM
--- PURPOSE:  This function inserts rows for 1 pack into the packitem table
------------------------------------------------------------------------------------------
FUNCTION INSERT_PACKITEM(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_packitem_rec  IN PACKITEM%ROWTYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
--- FUNCTION: INSERT_PI_BREAKOUT_NOLOCS
--- PURPOSE:  This function inserts rows for 1 pack into the packitem_breakout
---           table.  It does NOT call NEW_ITEM_LOC.  Used for xitem subscription
---           API.
-----------------------------------------------------------------------------------------------
FUNCTION INSERT_PI_BREAKOUT_NOLOCS(O_error_message IN OUT VARCHAR2,
                                   I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE,
                                   I_item          IN     PACKITEM_BREAKOUT.ITEM%TYPE,
                                   I_pack_ind      IN     ITEM_MASTER.PACK_IND%TYPE,
                                   I_qty           IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------------------------------
--- FUNCTION: PACK_COMP_EXISTS
--- PURPOSE:  This function will check if a record already exists in the
---           PACKITEM table for the given pack item.
-----------------------------------------------------------------------------------------------
FUNCTION PACK_COMP_EXISTS(O_error_message   IN OUT   VARCHAR2,
                          O_exists          IN OUT   BOOLEAN,
                          I_pack_item       IN       PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- FUNCTION: PACK_COSTING_EXISTS
--- PURPOSE:  This function will check if a record already exists in the
---           PACKITEM table for the given pack item.
-----------------------------------------------------------------------------------------------
FUNCTION PACK_COSTING_EXISTS(O_error_message   IN OUT   VARCHAR2,
                             O_exists          IN OUT   BOOLEAN,
                             I_pack_item       IN       PACKITEM.PACK_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
--- FUNCTION: CHECK_INNER_EXISTS
--- PURPOSE:  This function will check if component item of a simple pack exists as inner in
---           a complex pack.
---           API.
------------------------------------------------------------------------------------------------
FUNCTION CHECK_INNER_EXISTS (O_error_message IN OUT VARCHAR2,
                             O_exists        IN OUT VARCHAR2,
                             I_pack_no       IN     PACKITEM_BREAKOUT.PACK_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- FUNCTION: UPD_ISC_PACK_TMPL
--- PURPOSE:  This function is used to update the costs for the item created by NEW_PACK_TMPL
---           when default_tax_type is in GTAX or SVAT
------------------------------------------------------------------------------------------------
FUNCTION UPD_ISC_PACK_TMPL (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)

  RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
--- FUNCTION: INSERT_COST_BUYER_PACK
--- PURPOSE:  This function is used to calculate the cost of complex buyer pack by using the
---           component item cost details.
------------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_BUYER_PACK(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item               IN     ITEM_COST_HEAD.ITEM%TYPE,
                                I_component_costs    IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                                I_base_country_id    IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
--- FUNCTION: DELETE_SUPS_PACK_TMPL_DESC_TL
--- PURPOSE:  This function deletes from SUPS_PACK_TMPL_DESC_TL table.
------------------------------------------------------------------------------------------------
FUNCTION DELETE_SUPS_PACK_TMPL_DESC_TL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                             I_supplier       IN       SUPS_PACK_TMPL_DESC_TL.SUPPLIER%TYPE,
                                             I_pack_tmpl_id   IN       SUPS_PACK_TMPL_DESC_TL.PACK_TMPL_ID%TYPE )
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END;
/