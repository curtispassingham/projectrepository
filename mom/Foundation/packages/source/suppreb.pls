
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY SUPP_PREISSUE_SQL IS
--------------------------------------------------------------------------------
FUNCTION CHECK_SUPP_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists        IN OUT BOOLEAN,
                           I_supplier      IN     SUPS.SUPPLIER%TYPE) RETURN BOOLEAN IS

   L_dummy    VARCHAR2(1);

   cursor C_SUPP is
      select 'x'
        from supp_preissue
       where supplier = I_supplier;

BEGIN
   open C_SUPP;
   fetch C_SUPP into L_dummy;
   close C_SUPP;

   if L_dummy = 'x' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SUPP_PREISSUE_SQL.CHECK_SUPP_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_SUPP_EXISTS;
---------------------------------------------------------------------------
FUNCTION APPLY_SUPP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_supplier      IN     SUPP_PREISSUE.SUPPLIER%TYPE,
                    I_qty           IN     SUPP_PREISSUE.QTY%TYPE,
                    I_expiry_days   IN     SUPP_PREISSUE.EXPIRY_DAYS%TYPE,
                    I_frequency     IN     SUPP_PREISSUE.FREQUENCY%TYPE,
                    I_next_gen_date IN     SUPP_PREISSUE.NEXT_GEN_DATE%TYPE,
                    I_create_id     IN     SUPP_PREISSUE.CREATE_ID%TYPE,
                    I_exists        IN     BOOLEAN) RETURN BOOLEAN IS

BEGIN
   if I_exists then
      update supp_preissue
         set qty = I_qty,
             expiry_days = I_expiry_days,
             frequency = I_frequency,
             next_gen_date = I_next_gen_date,
             create_id = I_create_id
       where supplier = I_supplier;
   else
      insert into supp_preissue(supplier,
                                qty,
                                expiry_days,
                                frequency,
                                next_gen_date,
                                create_id)
                         values(I_supplier,
                                I_qty,
                                I_expiry_days,
                                I_frequency,
                                I_next_gen_date,
                                I_create_id);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SUPP_PREISSUE_SQL.APPLY_SUPP',
                                             to_char(SQLCODE));
      return FALSE;
END APPLY_SUPP;
--------------------------------------------------------------------------------
FUNCTION GET_SUPP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_supplier      IN OUT ORD_PREISSUE.SUPPLIER%TYPE,
                  I_order_no      IN     ORD_PREISSUE.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_SUPP is
      select supplier
        from ord_preissue
       where order_no = I_order_no;

BEGIN
   open C_SUPP;
   fetch C_SUPP into O_supplier;
   close C_SUPP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'SUPP_PREISSUE_SQL.GET_SUPP',
                                             to_char(SQLCODE));
      return FALSE;
END GET_SUPP;
----------------------------------------------------------------------------------
--Function Name: ORDSUPP_FILTER_LIST
--Purpose      : Check if any suppliers from supp_preissue table does not exists 
--               in v_sups.
--               If suppiler doesn't exist, it means the User doesnot have access 
--               to those suppliers.
----------------------------------------------------------------------------------
FUNCTION ORDSUPP_FILTER_LIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_diff_ind        IN OUT VARCHAR2)

RETURN BOOLEAN IS

   cursor C_GET_DIFFERENCE is
   select 'Y'
     from supp_preissue isu
    where supplier not in (select supplier
                             from v_sups);

   L_program            VARCHAR2(64) := 'SUPP_PREISSUE_SQL.ORDSUPP_FILTER_LIST';

BEGIN
   open C_GET_DIFFERENCE;

   fetch C_GET_DIFFERENCE into O_diff_ind;

   if C_GET_DIFFERENCE%NOTFOUND then
      O_diff_ind := 'N';
   end if;

   close C_GET_DIFFERENCE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ORDSUPP_FILTER_LIST;
---------------------------------------------------------------------------------------------------------

END SUPP_PREISSUE_SQL;
/
