
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE COMPETITOR_PRICE_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------------
-- Name:       CHECK_IF_DEFAULT
-- Purpose:    This function will be called after competitor shopping list 
--             records have been entered.  This function will check if records 
--		   were not updated based on the passed in search criteria; competitor, 
--		   shopped date, competitor store, shopper (only the competitor and 
--		   shopped date are required fields).  If there are records that were 
--		   not updated (meaning no retail price was associated to them), and 
--		   those records have retail history, the user will be given the option 
--		   to default the most recent retail information for those records.
-----------------------------------------------------------------------------
FUNCTION CHECK_IF_DEFAULT(O_error_message IN OUT  VARCHAR2,
                          O_default       IN OUT  BOOLEAN,
                          I_shopper       IN      COMP_SHOP_LIST.SHOPPER%TYPE,
                          I_comp_store    IN      COMP_SHOP_LIST.COMP_STORE%TYPE,
                          I_shop_date     IN      COMP_SHOP_LIST.REC_DATE%TYPE,
                          I_competitor    IN      COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       COMP_STORE_VALIDATION
-- Purpose:    This function will validate the competitor store and return the 
--		   store description, and currency.  
-----------------------------------------------------------------------------
FUNCTION COMP_STORE_VALIDATION(O_error_message IN OUT VARCHAR2,
                               O_store_name    IN OUT COMP_STORE.STORE_NAME%TYPE,
                               O_currency      IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                               I_comp_store    IN     COMP_SHOP_LIST.COMP_STORE%TYPE,
                               I_shopper       IN     COMP_SHOP_LIST.SHOPPER%TYPE,
                               I_item          IN     COMP_SHOP_LIST.ITEM%TYPE,
                               I_shop_date     IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                               I_competitor    IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       CURRENCY_VALIDATION
-- Purpose:    This function will validate the currency exists for the 
--		    competitor/shopped date/shopper/item combination.
-----------------------------------------------------------------------------
FUNCTION CURRENCY_VALIDATION(O_error_message IN OUT VARCHAR2,
                             I_shopper       IN     COMP_SHOP_LIST.SHOPPER%TYPE,
                             I_item          IN     COMP_SHOP_LIST.ITEM%TYPE,
                             I_currency      IN     CURRENCIES.CURRENCY_CODE%TYPE,
                             I_shop_date     IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                             I_competitor    IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------- 
-- Name:       DEFAULT_COMP_RETAIL
-- Purpose:    This function will update the current COMP_SHOP_LIST records with 
--		   the most recent data on COMP_PRICE_HIST for the passed in the shopped 
--		   date (required), competitor (required), competitor store, and shopper.
--		   The purpose of this function is to allow the user to not enter competitor
--		   retail information for item's that did not incur a retail change during
--		   the shopping period.  Instead of entering a retail price, the user will
--		   leave the competitor retail field NULL meaning that the price did no 
--		   change.  On exit of the competitive shopping entry screen, any fields 
--		   with NULL as the retail will have the most recent retail carried forward 
--		   to the current week.
-----------------------------------------------------------------------------
FUNCTION DEFAULT_COMP_RETAIL(O_error_message IN OUT VARCHAR2,
                             I_comp_store    IN     COMP_SHOP_LIST.COMP_STORE%TYPE,
                             I_shop_date     IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                             I_competitor    IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------- 
-- Name:       GET_COMP_RETAIL
-- Purpose:    This function will retrieve the most recent single unit retail,
--		   multi units, multi unit retail, and price type for the passed in 
--		   item/ref item/competitor store if they exist.
-----------------------------------------------------------------------------
FUNCTION GET_COMP_RETAIL(O_error_message     IN OUT VARCHAR2,
                         O_unit_retail       IN OUT COMP_SHOP_LIST.COMP_RETAIL%TYPE,
                         O_multi_unit_retail IN OUT COMP_SHOP_LIST.MULTI_UNIT_RETAIL%TYPE,
                         O_multi_units       IN OUT COMP_SHOP_LIST.MULTI_UNITS%TYPE,
                         O_retail_type       IN OUT COMP_SHOP_LIST.COMP_RETAIL_TYPE%TYPE,
                         I_item              IN     COMP_SHOP_LIST.ITEM%TYPE,
                         I_ref_item          IN     COMP_SHOP_LIST.REF_ITEM%TYPE,
                         I_comp_store        IN     COMP_SHOP_LIST.COMP_STORE%TYPE,
                         I_shop_date         IN     COMP_SHOP_LIST.REC_DATE%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------- 
-- Name:       INSERT_DELETE_COMP_TEMP
-- Purpose:    This function will insert records onto COMP_SHOP_LIST_TEMP based 
--		   on the passed in search criteria.  The purpose of this function is 
--		   to only maintain the records the user chooses to update in the 
--		   competitive price entry dialogue, which is based on the temp table.
--		   After the user completes their updates, all the updated records will
--		   be updated on COMP_SHOP_LIST and all the records will be deleted from
--		   COMP_SHOP_LIST_TEMP.
-----------------------------------------------------------------------------
FUNCTION INSERT_DELETE_COMP_TEMP(O_error_message IN OUT VARCHAR2,
                                 I_shopper       IN     COMP_SHOP_LIST.SHOPPER%TYPE,
                                 I_comp_store    IN     COMP_SHOP_LIST.COMP_STORE%TYPE,
                                 I_shop_date     IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                                 I_competitor    IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:       ITEM_VALIDATION
-- Purpose:    This function will validate the item and return the item description.
-----------------------------------------------------------------------------
FUNCTION ITEM_VALIDATION(O_error_message IN OUT VARCHAR2,
                         O_item_desc     IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                         I_shopper       IN     COMP_SHOP_LIST.SHOPPER%TYPE,
                         I_comp_store    IN     COMP_SHOP_LIST.COMP_STORE%TYPE,
                         I_currency      IN     CURRENCIES.CURRENCY_CODE%TYPE,
                         I_item          IN     COMP_SHOP_LIST.ITEM%TYPE,
                         I_ref_item      IN     COMP_SHOP_LIST.REF_ITEM%TYPE,
                         I_shop_date     IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                         I_competitor    IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------- 
-- Name:       REF_ITEM_VALIDATION
-- Purpose:    This function will validate the reference item and return the 
--  		   item description.
-----------------------------------------------------------------------------
FUNCTION REF_ITEM_VALIDATION(O_error_message IN OUT VARCHAR2,
                             O_ref_item_desc IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                             O_item_desc     IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                             IO_item         IN OUT COMP_SHOP_LIST.ITEM%TYPE,
                             I_shopper       IN     COMP_SHOP_LIST.SHOPPER%TYPE,
                             I_comp_store    IN     COMP_SHOP_LIST.COMP_STORE%TYPE,
                             I_currency      IN     CURRENCIES.CURRENCY_CODE%TYPE,
                             I_ref_item      IN     COMP_SHOP_LIST.REF_ITEM%TYPE,
                             I_shop_date     IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                             I_competitor    IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:    UPDATE_ALL_ITEM_STORES
-- Purpose: This function will be used to mass update all of the stores for a 
--          competitor (required), item (required), shopped date (required), 
--		currency (required), and shopper combination.
-----------------------------------------------------------------------------
FUNCTION UPDATE_ALL_ITEM_STORES(O_error_message     IN OUT VARCHAR2,
                                I_comp_retail       IN     COMP_SHOP_LIST.COMP_RETAIL%TYPE,
                                I_multi_unit_retail IN     COMP_SHOP_LIST.MULTI_UNIT_RETAIL%TYPE,
                                I_multi_units       IN     COMP_SHOP_LIST.MULTI_UNITS%TYPE,
                                I_comp_retail_type  IN     COMP_SHOP_LIST.COMP_RETAIL_TYPE%TYPE,
                                I_rec_date          IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                                I_offer_type        IN     COMP_SHOP_LIST.OFFER_TYPE%TYPE,
                                I_prom_start_date   IN     COMP_SHOP_LIST.PROM_START_DATE%TYPE,
                                I_prom_end_date     IN     COMP_SHOP_LIST.PROM_END_DATE%TYPE,
                                I_shopper           IN     COMP_SHOP_LIST.SHOPPER%TYPE,
                                I_item              IN     COMP_SHOP_LIST.ITEM%TYPE,
                                I_ref_item          IN     COMP_SHOP_LIST.REF_ITEM%TYPE,
                                I_currency          IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                I_shop_date         IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                                I_competitor        IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:    UPDATE_COMP_SHOP_LIST
-- Purpose: This Competitor number sequence generator will return to the calling 
--          program/procedure.  Upon success (TRUE) a competitor number will be returned.
--          Upon failure (FALSE) an appropriate 
--          error message for display purposes by the calling program/procedure.
-----------------------------------------------------------------------------
FUNCTION UPDATE_COMP_SHOP_LIST(O_error_message     IN OUT VARCHAR2,
                               I_shopper           IN     COMP_SHOP_LIST.SHOPPER%TYPE,
                               I_comp_store        IN     COMP_SHOP_LIST.COMP_STORE%TYPE,
                               I_shop_date         IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                               I_competitor        IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Name:    UPDATE_SINGLE_ITEM_STORE
-- Purpose: This function will be used to update the COMP_SHOP_LIST table for a 
--		competitor, item, shopped date, competitor store, and shopper combination.
-----------------------------------------------------------------------------
FUNCTION UPDATE_SINGLE_ITEM_STORE(O_error_message     IN OUT VARCHAR2,
                                  I_comp_retail       IN     COMP_SHOP_LIST.COMP_RETAIL%TYPE,
                                  I_multi_unit_retail IN     COMP_SHOP_LIST.MULTI_UNIT_RETAIL%TYPE,
                                  I_multi_units       IN     COMP_SHOP_LIST.MULTI_UNITS%TYPE,
                                  I_comp_retail_type  IN     COMP_SHOP_LIST.COMP_RETAIL_TYPE%TYPE,
                                  I_rec_date          IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                                  I_offer_type        IN     COMP_SHOP_LIST.OFFER_TYPE%TYPE,
                                  I_prom_start_date   IN     COMP_SHOP_LIST.PROM_START_DATE%TYPE,
                                  I_prom_end_date     IN     COMP_SHOP_LIST.PROM_END_DATE%TYPE,
                                  I_shopper           IN     COMP_SHOP_LIST.SHOPPER%TYPE,
                                  I_item              IN     COMP_SHOP_LIST.ITEM%TYPE,
                                  I_ref_item          IN     COMP_SHOP_LIST.REF_ITEM%TYPE,
                                  I_comp_store        IN     COMP_SHOP_LIST.COMP_STORE%TYPE,
                                  I_shop_date         IN     COMP_SHOP_LIST.REC_DATE%TYPE,
                                  I_competitor        IN     COMP_SHOP_LIST.COMPETITOR%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
END COMPETITOR_PRICE_SQL;
/


