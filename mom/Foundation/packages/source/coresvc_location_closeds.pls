-- File Name : CORESVC_LOCATION_CLOSED_spec.pls
CREATE OR REPLACE PACKAGE CORESVC_LOCATION_CLOSED AUTHID CURRENT_USER AS

   template_key        CONSTANT VARCHAR2(255) :='LOCATION_CLOSED_DATA';
   template_category   CONSTANT VARCHAR2(255) :='RMSFND';
   action_new                   VARCHAR2(25)  :='NEW';
   action_mod                   VARCHAR2(25)  :='MOD';
   action_del                   VARCHAR2(25)  :='DEL';
   LOCATION_CLOSED_sheet        VARCHAR2(255) :='LOCATION_CLOSED';
   LOCATION_CLOSED$Action       NUMBER        :=1;
   LOCATION_CLOSED$LOCATION     NUMBER        :=2;
   LOCATION_CLOSED$CLOSE_DATE   NUMBER        :=3;
   LOCATION_CLOSED$LOC_TYPE     NUMBER        :=4;
   LOCATION_CLOSED$SALES_IND    NUMBER        :=5;
   LOCATION_CLOSED$RECV_IND     NUMBER        :=6;
   LOCATION_CLOSED$SHIP_IND     NUMBER        :=7;
   LOCATION_CLOSED$REASON       NUMBER        :=8;
   
   LOCATION_CLOSED_TL_sheet        VARCHAR2(255) :='LOCATION_CLOSED_TL';
   LOCATION_CLOSED_TL$Action       NUMBER        :=1;
   LOCATION_CLOSED_TL$LANG         NUMBER        :=2;
   LOCATION_CLOSED_TL$LOCATION     NUMBER        :=3;
   LOCATION_CLOSED_TL$CLOSE_DATE   NUMBER        :=4;
   LOCATION_CLOSED_TL$REASON       NUMBER        :=5;

   TYPE LOCATION_CLOSED_rec_tab is TABLE OF LOCATION_CLOSED%ROWTYPE;

   sheet_name_trans S9T_PKG.trans_map_typ;

   action_column    VARCHAR2(255) := 'ACTION';
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count   IN OUT NUMBER,
                     I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN     NUMBER )
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT NUMBER,
                  I_process_id    IN     NUMBER,
                  I_chunk_id      IN     NUMBER )
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2;
--------------------------------------------------------------------------------
END CORESVC_LOCATION_CLOSED;
/
