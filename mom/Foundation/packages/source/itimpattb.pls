
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ITEM_IMPORT_ATTR_SQL AS

--------------------------------------------------------------------------------------------
FUNCTION ITEM_CHILD_EXISTS(O_error_message   IN OUT     VARCHAR2,
                           O_exists          IN OUT     BOOLEAN,
                           I_item            IN         ITEM_IMPORT_ATTR.ITEM%TYPE)
			RETURN BOOLEAN IS

   L_item_exists      VARCHAR2(1);

   CURSOR C_SELECT_CHILDREN IS
      select 'X'
        from item_import_attr iia, item_master im
       where im.item = iia.item
         and (im.item_parent = I_item
          or im.item_grandparent = I_item)
         and im.tran_level >= im.item_level;


BEGIN
   SQL_LIB.SET_MARK('open',
                    'C_SELECT_CHILDREN',
                    'item_import_attr',
                    NULL);
   open C_SELECT_CHILDREN ;

   SQL_LIB.SET_MARK('fetch',
                    'C_SELECT_CHILDREN',
                    'item_import_attr',
                    NULL);
   fetch C_SELECT_CHILDREN INTO L_item_exists;

   if C_SELECT_CHILDREN%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('close',
                    'C_SELECT_CHILDREN',
                    'item_import_attr',
                    NULL);
   close C_SELECT_CHILDREN;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_IMPORT_ATTR_SQL',
                                            to_char(SQLCODE));
	return FALSE;

END ITEM_CHILD_EXISTS;
--------------------------------------------------------------------------------------------     
FUNCTION DELETE_INSERT_ITEM_CHILD(O_error_message    IN OUT     VARCHAR2,
                                  I_item             IN         ITEM_IMPORT_ATTR.ITEM%TYPE,
                                  I_tooling          IN         ITEM_IMPORT_ATTR.TOOLING%TYPE,
                                  I_first_order_ind  IN         ITEM_IMPORT_ATTR.FIRST_ORDER_IND%TYPE,
                                  I_amortize_base    IN         ITEM_IMPORT_ATTR.AMORTIZE_BASE%TYPE,
                                  I_open_balance     IN         ITEM_IMPORT_ATTR.OPEN_BALANCE%TYPE,
                                  I_commodity        IN         ITEM_IMPORT_ATTR.COMMODITY%TYPE,
                                  I_import_desc      IN         ITEM_IMPORT_ATTR.IMPORT_DESC%TYPE)
			      RETURN BOOLEAN IS

   L_item          ITEM_IMPORT_ATTR.ITEM%TYPE;
   L_table         VARCHAR2(255) := 'item_import_attr';
   RECORD_LOCKED   EXCEPTION;

   CURSOR C_LOCK_IIA IS
      select 'X'
        from item_master im, item_import_attr iia
       where im.item = iia.item
         and (im.item_parent = I_item
          or im.item_grandparent = I_item)
         and im.tran_level >= im.item_level
         for update nowait;

BEGIN       
   SQL_LIB.SET_MARK('open',
                    'C_LOCK_IIA',
                    'item_import_attr',
                    NULL);
   open C_LOCK_IIA;

   SQL_LIB.SET_MARK('close',
                    'C_LOCK_IIA',
                    'item_import_attr',
                    NULL);
   close C_LOCK_IIA;
   ---

   SQL_LIB.SET_MARK('delete',
                    'C_APPLY_CHILDREN',
                    'item_import_attr',
                    NULL);
   delete from item_import_attr iia
    where exists (select 'X'
                    from item_master im
                   where (im.item_parent = I_item
                      or im.item_grandparent = I_item)
                     and im.tran_level >= im.item_level
                     and im.item = iia.item);
   ---
   SQL_LIB.SET_MARK('insert',
                    'NULL',
                    'item_import_attr',
                    NULL);
   insert into item_import_attr (item,
                                 tooling,
                                 first_order_ind,
                                 amortize_base,
                                 open_balance,
                                 commodity,
                                 import_desc)
                          select im.item,
                                 I_tooling,         
                                 I_first_order_ind,
                                 I_amortize_base,
                                 I_open_balance,
                                 I_commodity,
                                 I_import_desc
                            from item_master im
                           where (im.item_parent = I_item
                              or im.item_grandparent = I_item)
                             and im.tran_level >= im.item_level;
  
   return TRUE;
   
   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               L_table,
                                               L_item,
                                               NULL);
         return FALSE;

      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'ITEM_IMPORT_ATTR_SQL',
                                               to_char(SQLCODE));
	return FALSE;

END DELETE_INSERT_ITEM_CHILD;
--------------------------------------------------------------------------------------------
END ITEM_IMPORT_ATTR_SQL;
/  


